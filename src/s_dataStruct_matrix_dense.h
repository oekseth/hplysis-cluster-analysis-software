#ifndef s_dataStruct_matrix_dense_h
#define s_dataStruct_matrix_dense_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


/**
   @file s_dataStruct_matrix_dense
   @brief provide data-structure and logics for accessing elements in a dense matrix.
   @author Ole Kristian Ekseth (oekseth)
 **/

#include "configure_cCluster.h" //! Whcih specifies the optmizaitons and macro-configurations
#include "def_intri.h"



/**
   @file parse_main.h
   @struct s_dataStruct_matrix_dense
   @brief provide lgocis for parsing files
   @author Ole Kristian Ekseth (oekseth)
 **/
typedef struct s_dataStruct_matrix_dense {
  //! Note: if ["elow] data-structure is updated, then rembmer to also update our "mine.pyx" (eosekth, 06. sept. 2016).
  char **nameOf_rows; char **nameOf_columns;
  t_float **matrixOf_data;
  uint cnt_columns; uint cnt_rows;
  uint cnt_columns_allocated; uint cnt_rows_allocated;
  uint _word_max_length;
  const char *stringOf_defaultRowPrefix;
  char *__allocated_stringOf_defaultRowPrefix;
} s_dataStruct_matrix_dense_t;


/**
   @enum s_dataStruct_matrix_dense_typeOf_sampleFunction
   @brief identify the type of pre-defined data-sets to use
 **/
typedef enum s_dataStruct_matrix_dense_typeOf_sampleFunction {
  s_dataStruct_matrix_dense_typeOf_sampleFunction_flat,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_flat_fixedNrows,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_undef
} s_dataStruct_matrix_dense_typeOf_sampleFunction_t;


/**
   @enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise
   @brief extends enum "s_dataStruct_matrix_dense_typeOf_sampleFunction" wrt. functions 'holding' noise-values.
 **/
typedef enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise {
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_inverse_x,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_circle,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid_curved,

  //s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef
} s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t;

/**
   @enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel
   @brief identifies the nosie-level in the "s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise" enum
 **/
typedef enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel {
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_large,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef
} s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t;


//! @return the string-representation of enum-type s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t
const char *static_getStrOf_enum_function(const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function);
//! @return the string-representation of enum-type s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t
const char *static_getStrOf_enum_NoiseFunction(const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function);
//! @return the string-representation of enum-type s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t
const char *static_getStrOf_enum_noise(const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_function);


//! Intiate (ie, set to empty) the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void init_s_dataStruct_matrix_dense_t_setToEmpty(s_dataStruct_matrix_dense_t *obj);


//! @return true if "obj" is set to empty, eg, through a call to our "init_s_dataStruct_matrix_dense_t_setToEmpty(obj)".
bool is_setTo_empty(const s_dataStruct_matrix_dense_t *obj);
/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj,  uint cnt_rows,  uint cnt_columns,  uint word_max_length); 
//void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const uint word_max_length); 
/**
   @brief transpose the given matrix.
   @param <self> is the matrix to transpose.
 **/
void s_dataStruct_matrix_dense_transpose(s_dataStruct_matrix_dense_t *self);
/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object, and intiate the column-headers and row-headers to 'sample-texts'.
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
 **/
void init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns);
//! De-allocat the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void free_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj);

//! Copy a row to a spcified row-index.
void copyRow_intoPre_initiatedObject__s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj, const t_float *row, const uint row_index);

//! Get an usnafe 'copy' of the result-data
t_float *get_unsafeRow__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *obj, const uint row_index);
//! Get a 'copy' of the result-data
t_float *get_row__s_dataStruct_matrix_dense(s_dataStruct_matrix_dense_t *obj, uint row_index);
//const t_float *get_row__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *obj, const uint row_index);

//! @return the value assicated to the given cell.
const t_float get_cell__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *obj, const uint x, const uint y);


/**
   @brief collect the results into a 1d-array, a 1d arrya expected to represent a 2d-martix.
   @param <self> is the object which hold the results.
   @param <arrOf_2d_result> is a 2d-array to store the resutls
   @param <arrOf_2d_result_size>  which is used as a 'cotnrol', ie, to valdiate that "arrOf_2d_result" has the expected size.
   @remarks function is (among others) used in our Matlab-wrapper of "mine_mexMatrix.c" to simplify/standize data-ccess of resutls.
 **/
void collect_scoresInto_1dArray__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *self, t_float *arrOf_2d_result, const uint arrOf_2d_result_size);

/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
//s_dataStruct_matrix_dense_t *
void init_listOf_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t **arrOf_1d, const uint list_size, const uint cnt_rows, const uint cnt_columns, const uint word_max_length); 
//! De-allocates a list of "s_dataStruct_matrix_dense_t" objects.
void free_listOf_s_dataStruct_matrix_dense_t(const uint list_size, s_dataStruct_matrix_dense_t *arrOf_1d);


//! Build a sample-data-set using random values.
void constructSample_data_random(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns);
//! Build a sample-data-set using uniform values, valeus which are gneerated using the algorithm implemneted in the "cluster.c" library.
void constructSample_data_uniform(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns);
/**
   @brief build a sample-data-set using bionomial values,
   // @param <config_cnt_trials> is the max-value to be used: is the number of trials to be used in comptuation/'drawing' of a number from the bionomial distribution.
   @param <config_probability> the likelyhood/probality of a single event: expected to be in range [0, 0.5].
   @remakrs the values are generated using the algorithm implemneted in the "cluster.c" library.
**/
void constructSample_data_bionmial(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const t_float config_probability);
//! Compute the function-scores (oekseth, 06. arp. 2017).
void static__constructSample_data_by_function__s_dataStruct_matrix_dense_t(t_float **matrix, const uint nrows, const uint row_startPos, const uint ncols, const uint column_startPos, const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function);
//void constructSample_data_bionmial(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const uint config_cnt_trials, const t_float config_probability);
//! Build a sample-data-set using different functions 'to construct the data'.
void constructSample_data_by_function(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function);
//! Build a sample-data-set using different functions 'to construct the data'.
void static__constructSample_data_by_function_noise(t_float **matrix, const uint nrows, const uint row_startPos, const uint ncols, const uint column_startPos, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise, const t_float y_adjustment_index);
//! Build a sample-data-set using different functions 'to construct the data'.
void constructSample_data_by_function_noise(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise);





/**
   @brief construct a sampel-date-set in value-ranges identified by stringOf_sampleData_type param.
   @param <data_obj> the object to hold the new-cosntructed data-set.
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @return true on success.
 **/
bool constructSample_data_identifiedFromString(s_dataStruct_matrix_dense_t *data_obj, const uint cnt_rows, uint cnt_columns, const char *stringOf_sampleData_type);

/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <stringOf_measureId> which is used to 'name' the Java-script matrix-varaible (whcih is 'written out').
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const bool include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders);
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <stringOf_measureId> 
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders) ;
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <isLast> which if set to true impleis that we 'finalize' the JSON-array.
   @param <isTo_explcitlyInclude_rowAndColumnHeaders> which if set impleis that we explcutly inlcude the row and column-hders: an example-usage is for the case where we cocnate the inptu-matrix with an 'adjsuted' data-sets, eg, where the 'previosuly fucniton-calls' describes a corelation-matrix-set with 50 per-cent threshodls.
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders, const bool isLast, const char *isTo_explcitlyInclude_rowAndColumnHeaders) ;
/**
   @brief write out the obj to the stream as set of relations.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stringOf_predicate> which if set is used as predicate when writing out the data, eg, to simplify interpreation of the relationships.
   @param <stream> is the poitner to the restul-stream.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a java-script syntax.
   @param <isTo_writeOut_pairsSeperately> which is used if were are only interestinged in rpitning otu the scores
 **/
void export_dataSet_s_dataStruct_matrix_dense_relations(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, const char *stringOf_predicate, FILE *stream, const bool isTo_useJavaScript_syntax, const bool isTo_writeOut_pairsSeperately);

#endif
