#include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hp_ccm.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"
#include "hp_clusterShapes.h"
#include "hp_clusterFileCollection.h"
/* #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy */
/* #include "export_ppm.c" //! which is used for *.ppm file-export. */
/* #endif */

#include "kt_resultS_ccmTime.h" //! which provide lgocis for storing and exporting a combaiotn of time-matrix and clsuter-quality (CCM) matrix.

static void __tut_time_4_ccm_rand__ccmSynt__apply(s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config, const char *resultPrefix, 
						  const uint nrows_base, const uint ncols_base, const uint cnt_clusters, 
						  const uint cnt_blocks, const bool isFixed_row, const uint isFixed_col) {
  //!
  //! Allcoate result matrices: 
  s_kt_resultS_ccmTime_t obj_ccm_matrix = init__s_kt_resultS_ccmTime_t();

  //!
  //! Iterate through cases wrt. matrix-cluster-seperation:
  //uint cnt_clusters = cnt_clusterCases;
  for(uint block_id = 0; block_id < cnt_blocks; block_id++) {
    uint nrows = nrows_base;
    uint ncols = ncols_base;
    if(isFixed_row == false) {nrows = nrows_base * (block_id+1);}
    if(isFixed_col == false) {ncols = ncols_base * (block_id+1);}
    //! 
    //! Then itniate the amtrix and clsuter-colusmn:
    s_kt_matrix_t mat_inp = initAndReturn__s_kt_matrix(nrows, ncols);
    const uint empty_0 = 0;
    uint *map_clusterMembers = allocate_1d_list_uint(nrows, empty_0);
    for(uint i = 0; i < nrows; i++) {
      for(uint k = 0; k < ncols; k++) {
	mat_inp.matrix[i][k] = rand();
      }
      map_clusterMembers[i] = rand() % cnt_clusters;
    }
    //! Fetch ferecne-facts: 
    s_kt_matrix_base_t vec_sim = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_inp);
    //!
    start_time_measurement();
    const s_kt_correlationMetric_t obj_sim = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none); 
    s_kt_matrix_base_t vec_2 = initAndReturn__empty__s_kt_matrix_base_t();    
    const bool is_ok = apply__hp_distance(obj_sim, &vec_sim, NULL, &vec_2, init__s_hp_distance__config_t());
    assert(is_ok);
    //! 
    //! We weepxe ctan all-agaisnt-all- paris to have been comptued.
    assert(vec_2.nrows == vec_sim.nrows);
    assert(vec_2.ncols == vec_sim.nrows);
    {
      const char *str_local = "time-sim-metric"; 		const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);		
	//!
	//! Store results:
    }


#define __MiFLocal__setScore(obj, row_id, col_id) ({	     \
	    assert(row_id < obj.mat_result_ccm.nrows);	      \
	    assert(row_id < obj.mat_result_time.nrows);	      \
	    assert(col_id < obj.mat_result_ccm.ncols);	      \
	    assert(col_id < obj.mat_result_time.ncols);	      \
	    /*! Store results: */					\
	    obj.mat_result_ccm.matrix[row_id][col_id] = ccm_score;	\
	    obj.mat_result_time.matrix[row_id][col_id] = time_result; })
	
#define __MiF__setString__row(obj, index) ({				\
	    char tag[1000]; memset(tag, '\0', 1000); sprintf(tag, "nrows=%u", nrows); \
	    assert(index < obj.mat_result_ccm.nrows);			\
	    assert(index < obj.mat_result_time.nrows);			\
	    set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/false); \
	    set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/false); } )
#define __MiF__setString__col(obj, index, tag) ({	    \
	    assert(index < obj.mat_result_ccm.ncols);	     \
	    assert(index < obj.mat_result_time.ncols);			\
	    set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/true); \
	    set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/true); } )
	//const uint cnt_clusters = case_id*config_clusterBase;

	//!
    
	//!
    { //! Apply logics: compute the matrix-based CCMs:
      // s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
      // -------------------------------
      if(obj_ccm_matrix.mat_result_ccm.nrows == 0) { //! then we intiate: 
	uint cnt_cols = e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
	obj_ccm_matrix.mat_result_ccm  = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
	obj_ccm_matrix.mat_result_time = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
	//! 
	//! Set strings:
	for(uint block_id = 0; block_id < cnt_blocks; block_id++) {		  
	  __MiF__setString__row(obj_ccm_matrix, block_id);
	}
	for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
	  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i;
	  __MiF__setString__col(obj_ccm_matrix, /*index=*/i, getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_enum));
	}
      }
      // -------------------------------
      for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
	start_time_measurement();
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i;
	t_float ccm_score = T_FLOAT_MAX;
	bool is_ok = ccm__advanced__singleMatrix__hp_ccm(ccm_enum, &vec_2, map_clusterMembers, ccm_config, &ccm_score);
	//is_ok = ccm__singleMatrix__hp_ccm(ccm_enum, &vec_2, map_clusterMembers, &ccm_score);
	//is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
	assert(is_ok);    
	//! ----------
	const char *str_local = "matrix-based"; 		const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);		
	//!
	//! Store results:
	__MiFLocal__setScore(obj_ccm_matrix, block_id, i);
      }
    }
    //! 
    //! De-allocate:
    assert(mat_inp.matrix != vec_2.matrix); //! ie,a s we asusemt aht the second ius Not a shallow copy of the first.
    free__s_kt_matrix(&mat_inp);
    free__s_kt_matrix_base_t(&vec_2);
    assert(map_clusterMembers);
    if(map_clusterMembers) {
      free_1d_list_uint(&map_clusterMembers); map_clusterMembers = NULL;
    }
  }
  // -------------------------------------------
  //! 
  //! 
  { //! Write out result: 
    /* assert(fileTag); */
    /* assert(strlen(fileTag));  */
    assert(resultPrefix); 
    assert(strlen(resultPrefix));
    assert(strlen(resultPrefix) < 1000);
    {	      
      char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_matrix_default.tsv", resultPrefix, "_", 0, cnt_clusters); 
      free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_matrix, file_name);
    }
    /* { */
    /*   char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_gold.tsv", resultPrefix, fileTag, case_id, clusterCase);  */
    /*   free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_matrix_config, file_name); */
    /* } */
  }
  // -------------------------------------------
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief  compute [ccm-centrality-type=[vertex-vertex, vertex-global, vertex-centroid, centroid-vertex, centroid-global, centroid-centroid]][data-id][ccm-metric] = [ccm-score, time]
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks to store both predion-accurayc and exueciont-time, when evlauating a data-set [data-id][configation-id].
   -- we identify the best simalrity-emtrics to be sued for a given data-set.
   @remarks related-tut-examples:
   -- "tut_kd_3_data_simMetric.c". 
   -- "hp_evalHypothesis_algOnData.c": an API for data-anlaysis.
   -- "tut_kd_1_cluster_multiple_simMetrics.c": min-max-evaluation-accuracy wrt. CCMs and simalrityu-emtrics.
   -- "tut_time_1_data_syntetic_wellDefinedClusters.c": latter provide an advanced tempalte where a subset is sued in this funciton. 
**/
int main(const int array_cnt, char **array) 
#else
  int tut_time_4_ccm_rand(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //! 
  //! Logics: 
  {
    { //! matrix-CCMs: 
      //! Note: capture the isolatoeted time-cost assotied to CCM-cotmpatuion, ie, for which cCM-comptaution is not invovled/integrated. 
      const uint cnt_cases = 10; const uint cnt_clusterCases = 500;
      //      const uint cnt_cases = 10; const uint cnt_clusterCases = 50;
      //      const uint cnt_cases = 10; const uint cnt_clusterCases = 5;
      const uint nrows_increase = 10*1000; 
	const uint ncols = 1; 	const uint cnt_blocks = 1;
      { //! Use deult cofnigurations:      
	const char *resultPrefix = "tut_time_3_c1_vertexVertex_";
	//const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
	  //	const uint cnt_blocks = 20;
	//      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
	s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	ccm_config.centroidMetric__vertexVertex__networkReference__between    = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex;
	ccm_config.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex;
	//! 
	//! The call:
	__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, 
					      nrows_increase, ncols, cnt_clusterCases,
					      cnt_blocks, /*isFixed_row=*/true, /*isFixed_col=*/false);
      }
      { //! Use deult cofnigurations:      
	const char *resultPrefix = "tut_time_3_c2_vertexGlobal_";
	//const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
	const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
	const uint nrows_increase = 100; const uint cnt_blocks = 20;
	//      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
	s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	ccm_config.centroidMetric__vertexVertex__networkReference__between    = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal;
	ccm_config.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal;
	//! 
	//! The call:
	//! 
	//! The call:
	__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, 
					      nrows_increase, ncols, cnt_clusterCases,
					      cnt_blocks, /*isFixed_row=*/true, /*isFixed_col=*/false);
	//__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, cnt_cases, cnt_clusterCases, nrows_increase, cnt_blocks);
      }
      { //! Use deult cofnigurations:      
	const char *resultPrefix = "tut_time_3_c3_vertexCentroid_";
	//const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
	const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
	const uint nrows_increase = 100; const uint cnt_blocks = 20;
	//      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
	s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	ccm_config.centroidMetric__vertexVertex__networkReference__between    = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexCentroid;
	ccm_config.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexCentroid;
	//! 
	//! The call:
	//! 
	//! The call:
	__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, 
					      nrows_increase, ncols, cnt_clusterCases,
					      cnt_blocks, /*isFixed_row=*/true, /*isFixed_col=*/false);
	//__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, cnt_cases, cnt_clusterCases, nrows_increase, cnt_blocks);
      }
      { //! Use deult cofnigurations:      
	const char *resultPrefix = "tut_time_3_c4_centroidVertex_";
	//const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
	const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
	const uint nrows_increase = 100; const uint cnt_blocks = 20;
	//      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
	s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	ccm_config.centroidMetric__vertexVertex__networkReference__between    = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidVertex;
	ccm_config.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidVertex;
	//! 
	//! The call:
	//! 
	//! The call:
	__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, 
					      nrows_increase, ncols, cnt_clusterCases,
					      cnt_blocks, /*isFixed_row=*/true, /*isFixed_col=*/false);
	//__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, cnt_cases, cnt_clusterCases, nrows_increase, cnt_blocks);
      }
      { //! Use deult cofnigurations:      
	const char *resultPrefix = "tut_time_3_c5_centroidGlobal_";
	//const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
	const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
	const uint nrows_increase = 100; const uint cnt_blocks = 20;
	//      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
	s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	ccm_config.centroidMetric__vertexVertex__networkReference__between    = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal;
	ccm_config.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal;
	//! 
	//! The call:
	__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, 
					      nrows_increase, ncols, cnt_clusterCases,
					      cnt_blocks, /*isFixed_row=*/true, /*isFixed_col=*/false);
	//__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, cnt_cases, cnt_clusterCases, nrows_increase, cnt_blocks);
      }
      { //! Use deult cofnigurations:      
	const char *resultPrefix = "tut_time_3_c6_centroidCentroid_";
	//const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
	const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
	const uint nrows_increase = 100; const uint cnt_blocks = 20;
	//      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
	s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	ccm_config.centroidMetric__vertexVertex__networkReference__between    = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid;
	ccm_config.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid;
	//! 
	//! The call:
	//! 
	//! The call:
	__tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, 
					      nrows_increase, ncols, cnt_clusterCases,
					      cnt_blocks, /*isFixed_row=*/true, /*isFixed_col=*/false);
	// __tut_time_4_ccm_rand__ccmSynt__apply(ccm_config, resultPrefix, cnt_cases, cnt_clusterCases, nrows_increase, cnt_blocks);
      }
    }
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}

#undef __MiF__isTo_evaluate__local
#undef __MiFLocal__setScore
#undef __MiF__setString__row
#undef __MiF__setString__col
