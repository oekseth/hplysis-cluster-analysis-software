{

  const t_dType default_value_dType = 0;
#if(__compute_inverse__fastImplementaiton_memAlloc == 0)
  t_dType **c = (t_dType**)malloc((n-1)*sizeof(double *));
  for (uint i=0;i<n-1;i++)
    c[i] = (t_dType*)malloc((n-1)*sizeof(double));
#else
  t_dType **c = allocate_2d_list_dType(n, n, default_value_dType);
#endif

  // FIXME: write a new tempalte-file suing [”elowe].


  for (uint j=0;j<n;j++) {
    for (uint i=0; i<n; i++) {
#if(__compute_inverse__fastImplementaiton == 0)      
      /* Form the adjoint a_ij */
      uint i1 = 0;
      for (uint ii=0; ii<n; ii++) {
	if (ii == i)
	  continue;
	uint j1 = 0;
	for (uint jj=0; jj<n; jj++) {
	  if (jj == j)
	    continue;
	  c[i1][j1] = a[ii][jj];
	  j1++;
	}
	i1++;
      }
#else //! then we use a fact 'permtuation' of this.
      const uint ncols_remaining = n - 1;
      const uint i_plussOne = i + 1;
      for (uint ii=0; ii<i; ii++) {      
	//! Copy the 'lower' part:
	memcpy(c[ii], a[ii], sizeof(t_dType)*i);
	//! Copy the 'upper' part:
	memcpy(&c[ii][i], &a[ii][i_plussOne], sizeof(t_dType)*ncols_remaining);	
      }
      for (uint ii = i+1; ii<n; ii++) {      
	//! Copy the 'lower' part:
	memcpy(c[ii-1], a[ii], sizeof(t_dType)*i);
	//! Copy the 'upper' part:
	memcpy(&c[ii-1][i], &a[ii][i_plussOne], sizeof(t_dType)*ncols_remaining);	
      }
#endif

      /* Calculate the determinate */
      const uint nrows_inner = n - 1;
      t_dType det = c[0][0]; //! ie, for the case where (nrows_inner == 1)
      if(nrows_inner == 2) { det = __macro__get_determinant_case__2x2x(c); }
      else {
#if(__compute_inverse__fastImplementaiton__usePreComputedPowTable == 0)
	det = __compute_inverse__functNameCofactor(c,nrows_inner);
#else
	det = __compute_inverse__functNameCofactor(c,nrows_inner, mapOf_pow);
#endif
}
      //      else {det = __determinant(c,nrows_inner);}

      /* Fill in the elements of the cofactor */

      // FIXME: write a new log-function to idneitfy the realtive improtance of each 'step' ... eg, wrt. the "pow" funciton ... ie, after we have implemente dht LU-comptaution ... and after we have tested teh 'generic' cases.
#if(__compute_inverse__fastImplementaiton__usePreComputedPowTable == 1)
      assert(mapOf_pow);
      b[i][j] = mapOf_pow[i+j+2] * det;
#else
      b[i][j] = pow(-1.0, i+j+2.0) * det;
#endif
    }
  }

  //! De-allcoat ethe tmepraorily allcoated list:
#if(__compute_inverse__fastImplementaiton_memAlloc == 0)
  for (uint i=0;i<n-1;i++)
    free(c[i]);
  free(c);
#else
  free_2d_list_dType(&c, (n-1));
#endif
}


#undef __compute_inverse__functNameCofactor
#undef __compute_inverse__fastImplementaiton_memAlloc
#undef __compute_inverse__fastImplementaiton
#undef __compute_inverse__fastImplementaiton__usePreComputedPowTable
#undef t_dType
#undef allocate_1d_list_dType
#undef allocate_2d_list_dType
#undef free_1d_list_dType
#undef free_2d_list_dType
