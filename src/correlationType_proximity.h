#ifndef correlationType_altAlgortihmComputation_h
#define correlationType_altAlgortihmComputation_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlationType_proximity
   @brief an API to ifner proximity based on a metric defined in our e_correlationType_proximity_altAlgortihmComputation_type enum (oekseth, 06. sept. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/


#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "s_kt_correlationConfig.h"
#include "e_correlationType.h"
//! ----
#include "correlationType_proximity_altAlgortihmComputation.h"




#endif //! EOF
