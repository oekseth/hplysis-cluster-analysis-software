char **mask = NULL;   const bool isTo_useImplictMask = true;     

  // FIXME: [”elow]:
const uint mapOf_initType_size = 1;
//const uint mapOf_initType_size = 6;
//assert((mapOf_initType_size -1 )== e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_outgoing_sortedByRank_decrease); //! ie, as we expec thta tlter to be the 'last'.
const e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_t mapOf_initType[mapOf_initType_size] = {
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder,
  // FIXME: [”elow]:
  /* e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes, */
  /* e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_increase, */
  /* e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_decrease, */
  /* e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_outgoing_sortedByRank_increase, */
  /* e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_outgoing_sortedByRank_decrease, */
};
const char *stringOf_initType[mapOf_initType_size+5] = {
  "default-index-order",
  "root-notes",
  "count-of-arcs::sort-by-rank::incoming--incrase",
  "count-of-arcs::sort-by-rank::incoming--decrease",
  "count-of-arcs::sort-by-rank::outgoing--increase",
  "count-of-arcs::sort-by-rank::outgoing--decrease",
};
  // FIXME: [”elow]:
//const uint mapOf_traverseType_size = 1;
const uint mapOf_traverseType_size = 2;
//assert((mapOf_traverseType_size -1) == e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping); //! ie, as we expec thte latter to be the last.
const e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_t mapOf_traverseType[mapOf_traverseType_size] = {
  // FIXME: [”elow]:
  /* e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology, */
  /* e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology, */
  /* e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping */
  // e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping,
  e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology,
  e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping,
};
const char *stringOf_traverseType[mapOf_traverseType_size+1] = {"traverse-by-topology", "traverse-by-direct-nonTopology-access"};
//const char *stringOf_traverseType[mapOf_traverseType_size] = {"traverse-by-topology", "traverse-by-direct-nonTopology-access"};


//! Allocate a temproary matrix-stuructre, ie, to exlcitly ensure that we do Not adjsut/ovewrite the matrix 'during the process'.
t_float **cpy_matrix = allocate_2d_list_float(nrows, ncols, default_value_float);
      /** A safe-guard to ensure that our matrix is consistent: **/ assert(matrix); assert(cpy_matrix); for(uint i = 0; i < nrows; i++) {for(uint j = 0; j < ncols; j++) {cpy_matrix[i][j] = matrix[i][j];}}

uint cnt_disjoint = 0;
//! *****************************************************************************************************
{ //! Test for disjointness: sparse input-matrix::explicti-mask
  for(uint enum_init_index = 0; enum_init_index < mapOf_initType_size; enum_init_index++) {

    for(uint enum_traverse_index = 0; enum_traverse_index < mapOf_traverseType_size; enum_traverse_index++) {
    // FIXME: [below]:
    //const uint enum_traverse_index = 0; {
      char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
      sprintf(stringOf_measureText, "copmute-disjoint-forests(sparse-input-relations w/init=\"%s\" and traverse=\"%s\"): %s; we evaluate the correspnanded between the number of 'hidden values' adn the disjoint-forest-exeuction-time", stringOf_initType[enum_init_index], stringOf_traverseType[enum_traverse_index], stringOf_measureText_base);
      /** A safe-guard to ensure that our matrix is consistent: **/ assert(matrix); assert(cpy_matrix); for(uint i = 0; i < nrows; i++) {for(uint j = 0; j < ncols; j++) {matrix[i][j] = cpy_matrix[i][j];}}
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      { //! The operation:
	s_kt_set_2dsparse_t listOf_relations; allocate__s_kt_set_2dsparse_t(&listOf_relations, nrows, ncols, matrix, mask, isTo_useImplictMask, /*isTo_allocateFor_scores=*/false);    
	s_kt_forest_findDisjoint self; allocate__s_kt_forest_findDisjoint(&self, nrows, &listOf_relations);
	//! Configure the matrix-compuation:
	const bool is_ok = metaConfigure__s_kt_forest_findDisjoint(&self, mapOf_initType[enum_init_index], mapOf_traverseType[enum_traverse_index], false);
	if(false == is_ok) {
	  free_s_kt_forest_findDisjoint(&self);	  
	  continue; //! ie, as we then assume theis case is Not to be evlauated.
	}
	//! -----------------------------
	//!
	//! Infer the disjotin sets:
	graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);
	      
	//! -----------------------------
	//!
	//! Identify the number of itneresting clusters:
	const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
	const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
	if(cnt_disjoint != 0) {
	  if(cnt_interesting_disjointForests != cnt_disjoint) {
	    fprintf(stderr, "!!\t Seems like an incossntecy: while we in 'this' identifed %u disjotin-ofroests we in earlier calls cnt-disjoint=%u, ie, please investigate your call, ie, for: %s; an observat ion at %s:%d\n", cnt_interesting_disjointForests, cnt_disjoint, stringOf_measureText, __FILE__, __LINE__);
	  }
	  assert(cnt_interesting_disjointForests == cnt_disjoint);
	}
	cnt_disjoint = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
	//printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
	//! Validate the result:
	//! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
	/* assert(cnt_total_vertices == 6); //! ie, the totla number of unique vertices inserted in [above] */
	/* assert(cnt_interesting_disjointForests == 2); //! ie, what we expect.     */
	//! -----------------------------
	//! De-allocate:
	free_s_kt_forest_findDisjoint(&self);
	free_s_kt_set_2dsparse_t(&listOf_relations);    
      }
      //! Write out the exeuciotn-time:
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__cntDisjoint(stringOf_measureText, nrows, size_of_array, cnt_disjoint, mapOf_timeCmp_forEachBucket[chunk_index]); 	
      printf("\t\t\t cnt-disjtoin-regions=%u given cnt-interesting-vertex-faction=%.2f, at %s:%d\n------------------------\n", cnt_disjoint, (t_float)(cntOf_interest / (nrows*ncols)), __FILE__, __LINE__);
    }
  }
}
//! *****************************************************************************************************
// if(false)
{ char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
  // uint cnt_disjoint = 0;
  for(uint enum_init_index = 0; enum_init_index < mapOf_initType_size; enum_init_index++) {   
      /** A safe-guard to ensure that our matrix is consistent: **/ assert(matrix); assert(cpy_matrix); for(uint i = 0; i < nrows; i++) {for(uint j = 0; j < ncols; j++) {cpy_matrix[i][j] = matrix[i][j];}}
    //for(uint enum_traverse_index = 0; enum_traverse_index < mapOf_traverseType_size; enum_traverse_index++) 
    uint enum_traverse_index = 1; {
      /* char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000); */
      /* sprintf(stringOf_measureText, "copmute-disjoint-forests(dense-input-matrix w/init=\"%s\" and traverse=\"%s\"): %s; we evaluate the correspnanded between the number of 'hidden values' adn the disjoint-forest-exeuction-time", stringOf_initType[enum_init_index], stringOf_traverseType[enum_traverse_index], stringOf_measureText_base); */
      /* //sprintf(stringOf_measureText, "copmute-disjoint-forests(dense-input-relations): %s; we evaluate the correspnanded between the number of 'hidden values' adn the disjoint-forest-exeuction-time", stringOf_measureText_base); */
      /* //! ------------------------------- */
      /* //! Start the clock: */
      /* start_time_measurement();  //! ie, start measurement for a 'compelte matrix' */
      /* { //! A similar test where we use a dense matrix as nput: */
	
      /* 	s_kt_forest_findDisjoint self; allocate__denseMatrix__s_kt_forest_findDisjoint(&self, nrows, ncols, matrix, mask, /\*empty-value=*\/T_FLOAT_MAX); */
      /* 	//! Configure the matrix-compuation: */
      /* 	const bool is_ok = metaConfigure__s_kt_forest_findDisjoint(&self, mapOf_initType[enum_init_index], mapOf_traverseType[enum_traverse_index], false); */
      /* 	if(false == is_ok) { */
      /* 	  //if(false == isCorrectlySet__s_kt_forest_findDisjoint(&self, /\*printWarnignAndHandleObject_ifNotprperly_set=*\/false)) { */
      /* 	  free_s_kt_forest_findDisjoint(&self);	   */
      /* 	  continue; //! ie, as we then assume theis case is Not to be evlauated. */
      /* 	} */
      /* 	//! ----------------------------- */
      /* 	//! */
      /* 	//! Infer the disjotin sets: */
      /* 	graph_disjointForests__s_kt_forest_findDisjoint(&self, /\*isTo_identifyCentrlaityVertex=*\/true);       */
      /* 	//! ----------------------------- */
      /* 	//! */
      /* 	//! Identify the number of itneresting clusters: */
      /* 	const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self); */
      /* 	const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /\*countThreshold_min=*\/UINT_MAX, /\*countThreshold_max=*\/UINT_MAX); */
      /* 	if(cnt_disjoint != 0) { */
      /* 	  if(cnt_interesting_disjointForests != cnt_disjoint) { */
      /* 	    fprintf(stderr, "!!\t Seems like an incossntecy: while we in 'this' identifed %u disjotin-ofroests we in earlier calls cnt-disjoint=%u, ie, please investigate your call, ie, for: %s; an observat ion at %s:%d\n", cnt_interesting_disjointForests, cnt_disjoint, stringOf_measureText, __FILE__, __LINE__); */
      /* 	  } */
      /* 	  assert(cnt_interesting_disjointForests == cnt_disjoint); */
      /* 	} */
      /* 	cnt_disjoint = cnt_interesting_disjointForests; */
      /* 	if(cnt_disjoint > 0) {assert(cnt_total_vertices > 0);} */
      /* 	//printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__); */
      /* 	//! ----------------------------- */
      /* 	//! De-allocate: */
      /* 	free_s_kt_forest_findDisjoint(&self); */
      /* } */
      /* //! Write out the exeuciotn-time: */
      /* __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__cntDisjoint(stringOf_measureText, nrows, size_of_array, cnt_disjoint, mapOf_timeCmp_forEachBucket[chunk_index]); 	 */
      /* printf("\t\t\t cnt-disjtoin-regions=%u given cnt-interesting-vertex-faction=%.2f, at %s:%d\n------------------------\n", cnt_disjoint, (t_float)(cntOf_interest / (nrows*ncols)), __FILE__, __LINE__); */
    }
    //!!!--------------
    // FIXME: [below]:
    enum_traverse_index = 1; {
      /** A safe-guard to ensure that our matrix is consistent: **/ assert(matrix); assert(cpy_matrix); for(uint i = 0; i < nrows; i++) {for(uint j = 0; j < ncols; j++) {matrix[i][j] = cpy_matrix[i][j];}}
      char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
      sprintf(stringOf_measureText, "copmute-disjoint-forests(dense-input-matrix w/init=\"%s\" and traverse=\"%s\"): %s; we evaluate the correspnanded between the number of 'hidden values' adn the disjoint-forest-exeuction-time", stringOf_initType[enum_init_index], stringOf_traverseType[enum_traverse_index], stringOf_measureText_base);
      //sprintf(stringOf_measureText, "copmute-disjoint-forests(dense-input-relations): %s; we evaluate the correspnanded between the number of 'hidden values' adn the disjoint-forest-exeuction-time", stringOf_measureText_base);
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      { //! A similar test where we use a dense matrix as nput:
	
	s_kt_forest_findDisjoint self; allocate__denseMatrix__s_kt_forest_findDisjoint(&self, nrows, ncols, matrix, mask, /*empty-value=*/T_FLOAT_MAX);
	//! Configure the matrix-compuation:
	const bool is_ok = metaConfigure__s_kt_forest_findDisjoint(&self, mapOf_initType[enum_init_index], mapOf_traverseType[enum_traverse_index], false);
	if(false == is_ok) {
	  //if(false == isCorrectlySet__s_kt_forest_findDisjoint(&self, /*printWarnignAndHandleObject_ifNotprperly_set=*/false)) {
	  free_s_kt_forest_findDisjoint(&self);	  
	  continue; //! ie, as we then assume theis case is Not to be evlauated.
	}
	//! -----------------------------
	//!
	//! Infer the disjotin sets:
	graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);      
	//! -----------------------------
	//!
	//! Identify the number of itneresting clusters:
	const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
	const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
	if(cnt_disjoint != 0) {
	  if(cnt_interesting_disjointForests != cnt_disjoint) {
	    fprintf(stderr, "!!\t Seems like an incossntecy: while we in 'this' identifed %u disjotin-ofroests we in earlier calls cnt-disjoint=%u, ie, please investigate your call, ie, for: %s; an observat ion at %s:%d\n", cnt_interesting_disjointForests, cnt_disjoint, stringOf_measureText, __FILE__, __LINE__);
	  }
	  assert(cnt_interesting_disjointForests == cnt_disjoint);
	}
	cnt_disjoint = cnt_interesting_disjointForests;
	if(cnt_disjoint > 0) {assert(cnt_total_vertices > 0);}
	//printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
	//! -----------------------------
	//! De-allocate:
	free_s_kt_forest_findDisjoint(&self);
      }
      //! Write out the exeuciotn-time:
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__cntDisjoint(stringOf_measureText, nrows, size_of_array, cnt_disjoint, mapOf_timeCmp_forEachBucket[chunk_index]); 	
      printf("\t\t\t cnt-disjtoin-regions=%u given cnt-interesting-vertex-faction=%.2f, at %s:%d\n------------------------\n", cnt_disjoint, (t_float)(cntOf_interest / (nrows*ncols)), __FILE__, __LINE__);
    }
  }
}
//! ***************************************************************

// FIXME: include [below]
if(false)
{ //! Test for disjointness: sparse input-matrix::implciit-mask
  for(uint enum_init_index = 0; enum_init_index < mapOf_initType_size; enum_init_index++) {
    for(uint enum_traverse_index = 0; enum_traverse_index < mapOf_traverseType_size; enum_traverse_index++) {
      /** A safe-guard to ensure that our matrix is consistent: **/ assert(matrix); assert(cpy_matrix); for(uint i = 0; i < nrows; i++) {for(uint j = 0; j < ncols; j++) {matrix[i][j] = cpy_matrix[i][j];}}
      char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
      sprintf(stringOf_measureText, "copmute-disjoint-forests(explicit-mask::sparse-input-relations w/init=\"%s\" and traverse=\"%s\"): %s; we evaluate the correspnanded between the number of 'hidden values' adn the disjoint-forest-exeuction-time", stringOf_initType[enum_init_index], stringOf_traverseType[enum_traverse_index], stringOf_measureText_base);
      //! -------------------------------
      const char default_value_char = 0;
      const bool isTo_useImplictMask = false;
      char **mask = allocate_2d_list_char(nrows, ncols, default_value_char);
      for(uint i = 0; i < nrows; i++) {for(uint j = 0; j < ncols; j++) {mask[i][j] = isOf_interest(matrix[i][j]);}} //! ie, cosntruct an implict mask-set from the set of explcit-masks.
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      { //! The operation:
	//s_kt_set_2dsparse_t listOf_relations; allocate__s_kt_set_2dsparse_t(&listOf_relations, nrows, ncols, matrix, mask, isTo_useImplictMask, /*isTo_allocateFor_scores=*/false);    
	s_kt_forest_findDisjoint self; allocate__denseMatrix__s_kt_forest_findDisjoint(&self, nrows, ncols, /*matrix=*/NULL, mask, /*empty-value=*/T_FLOAT_MAX);
	//! Configure the matrix-compuation:
	const bool is_ok = metaConfigure__s_kt_forest_findDisjoint(&self, mapOf_initType[enum_init_index], mapOf_traverseType[enum_traverse_index], false);
	if(false == is_ok) { //if(false == isCorrectlySet__s_kt_forest_findDisjoint(&self, /*printWarnignAndHandleObject_ifNotprperly_set=*/false)) {
	  free_s_kt_forest_findDisjoint(&self);	  
	  free_2d_list_char(&mask, nrows);
	  continue; //! ie, as we then assume theis case is Not to be evlauated.
	}
	//! -----------------------------
	//!
	//! Infer the disjotin sets:
	graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);
	      
	//! -----------------------------
	//!
	//! Identify the number of itneresting clusters:
	const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
	const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
	if(cnt_disjoint != 0) {
	  if(cnt_interesting_disjointForests != cnt_disjoint) {
	    fprintf(stderr, "!!\t Seems like an incossntecy: while we in 'this' identifed %u disjotin-ofroests we in earlier calls cnt-disjoint=%u, ie, please investigate your call, ie, for: %s; an observat ion at %s:%d\n", cnt_interesting_disjointForests, cnt_disjoint, stringOf_measureText, __FILE__, __LINE__);
	  }
	  assert(cnt_interesting_disjointForests == cnt_disjoint);
	}
	cnt_disjoint = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
	//printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
	//! Validate the result:
	//! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
	/* assert(cnt_total_vertices == 6); //! ie, the totla number of unique vertices inserted in [above] */
	/* assert(cnt_interesting_disjointForests == 2); //! ie, what we expect.     */
	//! -----------------------------
	//! De-allocate:
	free_s_kt_forest_findDisjoint(&self);
	//free_s_kt_set_2dsparse_t(&listOf_relations);    
      }
      //! Write out the exeuciotn-time:
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__cntDisjoint(stringOf_measureText, nrows, size_of_array, cnt_disjoint, mapOf_timeCmp_forEachBucket[chunk_index]); 	
      printf("\t\t\t cnt-disjtoin-regions=%u given cnt-interesting-vertex-faction=%.2f, at %s:%d\n------------------------\n", cnt_disjoint, (t_float)(cntOf_interest / (nrows*ncols)), __FILE__, __LINE__);

      //! -----------------------------
      //! De-allocate:
      assert(mask); free_2d_list_char(&mask, nrows);
    }
  }
}

assert(cpy_matrix); free_2d_list_float(&cpy_matrix, nrows);
