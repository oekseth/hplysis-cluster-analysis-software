{
  const s_allAgainstAll_config_t config = self;





  assert(config.CLS > 0);
  //const enum e_typeOf_optimization_distance typeOf_optimization  = e_typeOf_optimization_distance_asFastAsPossible;

  if(transposed) {
    t_float **data1_transposed = allocate_2d_list_float(ncols, nrows, /*default-value=*/0);
    char **data1_transposed_mask = (mask1) ? allocate_2d_list_char(ncols, nrows, /*default-value=*/0) : NULL;
    t_float **data2_transposed = data1_transposed;
    char **data2_transposed_mask = NULL;
    //printf("(transpose-matrix)\t at %s:%d\n", __FILE__, __LINE__);
    matrix__transpose__compute_transposedMatrix(nrows, ncols, data_input1, mask1, data1_transposed, data1_transposed_mask, /*isTo_useIntrisinistic=*/true);
    if(data_input1 != data_input2) {
      data2_transposed = allocate_2d_list_float(ncols, nrows, /*default-value=*/0);
      const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
      data2_transposed_mask = (mask2) ? allocate_2d_list_char(ncols, nrows_2, /*default-value=*/0) : NULL;
      // FIXME: udpate [”elow] boolean aprameter after/hwen we have doe some benchmarkt-esting.
      //printf("(transpose-matrix)\t at %s:%d\n", __FILE__, __LINE__);
      matrix__transpose__compute_transposedMatrix(nrows_2, ncols, data_input2, mask2, data2_transposed, data2_transposed_mask, /*isTo_useIntrisinistic=*/true);
    }
    //! Compute the result, calling this funciton once more:
    //! Note: the 'two-step' call is sued to simplify the logics fo this fucntion, ie, to ese maitnainance and verifciaiton of crrectness (wrt. the implemnetiaton).
    self.mask1 = data1_transposed_mask;     self.mask2 = data2_transposed_mask;  
    self.transposed = false; //! ie, to avodi a 'recursive call' (oekseht, 06. nov. 2016).
    
    //printf("(start-computaiotn)\t\t\t at %s:%d\n", __FILE__, __LINE__);
    kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, ncols, nrows, data1_transposed, data2_transposed, resultMatrix, self, NULL); //data1_transposed_mask, data2_transposed_mask, /*transpose=*/0, iterationIndex_2, config);
    //! De-allcoat ehte locally stored matrices:
    free_2d_list_float(&data1_transposed, ncols); data1_transposed = NULL;
    if(data1_transposed_mask) {free_2d_list_char(&data1_transposed_mask, ncols); data1_transposed_mask = NULL;}
    if(data_input1 != data_input2) {
      free_2d_list_float(&data2_transposed, ncols); data2_transposed = NULL;
      if(data2_transposed_mask) {free_2d_list_char(&data2_transposed_mask, ncols); data2_transposed_mask = NULL;}
    }
    /* //! Allocate a temporal result-matrix */
    /* t_float **result_transposed = allocate_2d_list_float(ncols, ncols, /\*default-value=*\/0);     */

    /* //! Tranpose the result: */
    /* printf("(transpose-matrix)\t at %s:%d\n", __FILE__, __LINE__); */
    /* matrix__transpose__compute_transposedMatrix(ncols, ncols, resultMatrix, NULL, result_transposed, NULL, /\*isTo_useIntrisinistic=*\/true); */
    /* //! Copy the result to the output: */
    /* if(config.isTo_use_continousSTripsOf_memory) { */
    /*   memcpy(resultMatrix[0], result_transposed[0], sizeof(t_float)*ncols*ncols); */
    /* } else { //! then we need to tierate seperately through each 'of the cases': */
    /*   for(uint i = 0; i < ncols; i++) { */
    /* 	memcpy(resultMatrix[i], result_transposed[i], sizeof(t_float)*ncols); */
    /*   } */
    /* } */
    /* //! De-allcoate the temoprarily reserved result-matrix: */
    /* free_2d_list_float(&result_transposed); */
    //! Re-set the masks:
    self.mask1 = mask1;     self.mask2 = mask2;
    return; //! ie, as we at this eceuction-poitn has completed out 'task'.
  }
  
  

  //! --------------
  bool needTo_useMask_evaluation = (self.masksAre_used == e_cmp_masksAre_used_true); //! ie, investigate if the caller has declared that 'all valeus are of itnerest', eg, for the case where the "euclidc distance metric" is used in the k-means procedure, ie, where a acell-dsitance=0 is 'enough' to cpature/descirbe a non-set cell (ie, as the latter value will then be ingored form the comptaution).
  // TODO: validate that [ªbov€] is correct and [”elow] is worng ... ie, 'acorss' different funcitosn (oekseth, 06. jan. 2017).
  //bool needTo_useMask_evaluation = (self.masksAre_used == e_cmp_masksAre_used_false); //! ie, investigate if the caller has declared that 'all valeus are of itnerest', eg, for the case where the "euclidc distance metric" is used in the k-means procedure, ie, where a acell-dsitance=0 is 'enough' to cpature/descirbe a non-set cell (ie, as the latter value will then be ingored form the comptaution).
  if(self.masksAre_used == e_cmp_masksAre_used_undef) {
    assert(data_input1); assert(data_input2);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncols, data_input1, data_input2, self.mask1, self.mask2, /*transpose=*/false, self.iterationIndex_2);
    self.masksAre_used = (needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
    // printf("needTo_useMask_evaluation=%d, self.masksAre_used=%d, e_cmp_masksAre_used_true=%d, at %s:%d\n", needTo_useMask_evaluation, self.masksAre_used, e_cmp_masksAre_used_true, __FILE__, __LINE__);
  } 
  // printf("needTo_useMask_evaluation=%d, self.masksAre_used=%d, e_cmp_masksAre_used_true=%d, at %s:%d\n", needTo_useMask_evaluation, self.masksAre_used, e_cmp_masksAre_used_true, __FILE__, __LINE__);


  assert(self.transposed == 0); //! ie, what we expect at thsi exueciton-point.

  //! --------------


// FIXME: wrt. MPI ... write a seperate function which does Not apply post-processing ... ie, update our "template_correlation_tile.cxx" <-- write a sperate fucntion 'for this' ... moving 'teh cotnent of thsi' into a enw file ... suing "include "....c"


  
  //! Note: difference between "tranpose==1" and "transpose==0" is found not wrt. the 'mulitplication of entites': instead the difference is seen/found wrt. how the elments are summed/lumbed together. To examplify the latter, we for ...??... 

  // FIXME: ensure that the for-loops are easily seperatble ... ie, that parallisaiton is easy/trivial.

  // FIXME: given teh symmetric property of summing ... consider to use/limit the summation to " J <= i"
  
  //printf("dist='%c', at %s:%d\n", dist, __FILE__, __LINE__);
  
  // struct s_allAgainstAll_config config_allAgainstAll; init__s_allAgainstAll_config(&config_allAgainstAll, config.CLS, config.isTo_use_continousSTripsOf_memory, iterationIndex_2, config.typeOf_postProcessing_afterCorrelation, config.s_inlinePostProcess, config.masksAre_used, /*isTo_use_SIMD=*/true, typeOf_optimization);
  


  t_float **data1 = data_input1; t_float **data2 = data_input2;
  assert(data1);     assert(data2);

  t_float **matrix_tmp_1 = NULL;  t_float **matrix_tmp_2 = NULL;
  if(isTo_use_kendall__e_kt_correlationFunction(metric_id) == false) {
    if( (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) && !self.inAllAgainstAll__inputIsAlreadySorted_forSpearman) {
      //! Allocate memory and compute the rank for the matrices.
      //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
      ktCorrelation_compute_rankFor_matrices(nrows, ncols, data_input1, data_input2, self.mask1, self.mask2, &matrix_tmp_1, &matrix_tmp_2, /*transpose=*/false, needTo_useMask_evaluation, self.iterationIndex_2);

      //! --------
      //! Update the internal references:
      self.mask1 = NULL; self.mask2 = NULL; //! ie, as the 'column-postion' refers to a differnet psotion in the sorted list.    
      data1 = matrix_tmp_1; data2 = matrix_tmp_2;
      assert(data1); 
      assert(data2);
    } else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {
      //t_float **matrix_tmp_1 = NULL;  t_float **matrix_tmp_2 = NULL;
      matrix_tmp_1 = construct_binaryMatrix__mask_api(data_input1, nrows, ncols);    
      matrix_tmp_2 = matrix_tmp_1; //! ie, a simple 'copy' wrt. the pointer.
      if(data_input2 != data_input1) {
	assert(transposed == false); //! ie, as we otherwise need udpating [”eow] ... currently omtited to simplify de-bugging (oekseth, 06. des. 2016).
	const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
	matrix_tmp_2 = construct_binaryMatrix__mask_api(data_input2, nrows_2, ncols);
      }
      //! --------
      //! Update the internal references:
      // self.mask1 = NULL; self.mask2 = NULL; //! ie, as the 'column-postion' refers to a differnet psotion in the sorted list.    
      data1 = matrix_tmp_1; data2 = matrix_tmp_2;
      assert(data1); 
      assert(data2);
    }
  } //! else we assume 'this case' is specifcally handled by our Kendall-procedure (oekseth, 06. des. 2016).


  if(isTo_use_kendall__e_kt_correlationFunction(metric_id)) {
    ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, ncols, data1, data2, self.mask1, self.mask2, self.weight, resultMatrix, self.transposed, metric_id, &self); //! defined in "correlation_inCategory_rank_kendall.h"
  } else {
    
    if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == false) {

      if(isTo_use_directScore__e_kt_correlationFunction(metric_id)) {
	//! Then we call the function found in our "kt_distance__groupOf_direct.c" (oekseth, 06. des. 2016).
	if(nrows == ncols) {
	  t_float **resultMatrix_tmp = resultMatrix;
	  const uint nrows_1 = nrows;
	  const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
	  if(resultMatrix == NULL) {
	    // TODO: cosnider 'dropping' this step .. ie, to instead update our code explcits ... which is currently omitted iot. reduce the number of 'cases' we have to evlauate (oekseth, 06. des. 2016).
	    const t_float empty_value = 0;
	    resultMatrix_tmp = allocate_2d_list_float(nrows_1, nrows_2, empty_value);
	    setMatrixTo_defaultEmptyValue__s_allAgainstAll_config_t(&self, resultMatrix_tmp, nrows_1, nrows_2);
	    
	  }
	  corr__computeDirectScore__manyToMany(metric_id, nrows_1, nrows_2, 
							   data1, data2,
							   /*weight=*/weight,
							   self.mask1, //(self.mask1) ? self.mask1[index1] : NULL,
							   self.mask2, // (self.mask2) ? self.mask2[index2] : NULL,
							   ///*weight=*/(self-weight && (nrows == ncols)) ? &(self.weight[index2]), 
							   resultMatrix_tmp,
							   /*masksAre_used=*/self.masksAre_used);


#include "kt_distance__stub__mineValueUseMatrix__after.c" //! which is used if "resultMatrix == NULL"  (oekseth, 06. des. 2016).
	} else {
	  fprintf(stderr, "!!\t Seems like your inptu-data in diemsnions [%u, %u] are Not symmetric, ie, pelase validate correctness of your topology-speciciaiotn. For questions please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
	}	

	
      } else {
	
	void (*metric_manyToMany_nonRank) (config_nonRank_manyToMany) = setmetric__correlationComparison__manyToMany(metric_id, self.weight, self.mask1, self.mask2, needTo_useMask_evaluation);  
	assert(data1); assert(data2); 
	
	//! 
	//! Compute the score:
	config_nonRank_manyToMany_t obj_config; init__config_nonRank_manyToMany(&obj_config, nrows, ncols, data1, data2,  self.mask1, self.mask2,  self.weight, resultMatrix, metric_id, e_typeOf_metric_correlation_pearson_undef, &self, obj_resultsFromTiling);      
	assert(obj_config.data1);
	assert(obj_config.data2);
	//! The call:
	metric_manyToMany_nonRank(obj_config);
      }
    } else {
      t_float **resultMatrix_tmp = resultMatrix;
      const uint nrows_1 = nrows;
      const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
      if(resultMatrix == NULL) {
	// TODO: cosnider 'dropping' this step .. ie, to instead update our code explcits ... which is currently omitted iot. reduce the number of 'cases' we have to evlauate (oekseth, 06. des. 2016).
	const t_float empty_value = 0;
	resultMatrix_tmp = allocate_2d_list_float(nrows_1, nrows_2, empty_value);
	setMatrixTo_defaultEmptyValue__s_allAgainstAll_config_t(&self, resultMatrix_tmp, nrows_1, nrows_2);
	
      }
      // TODO: try imrpvoing [”elow] costnatns:
      const t_float power = 0.7; //! ie, a default value.
      const t_float eps = 0.07; //! ie, a default value.
      if(metric_id == e_kt_correlationFunction_groupOf_MINE_mic) {
	compute_2d_scoreMatrix__matrixAsInput__forType__MIC(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, self.sparseDataResult);
      } else if(metric_id == e_kt_correlationFunction_groupOf_MINE_mas) {
	compute_2d_scoreMatrix__matrixAsInput__forType__MAS(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, self.sparseDataResult);
      } else if(metric_id == e_kt_correlationFunction_groupOf_MINE_mev) {
	compute_2d_scoreMatrix__matrixAsInput__forType__MEV(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, self.sparseDataResult);
      } else if(metric_id == e_kt_correlationFunction_groupOf_MINE_mcn) {
	compute_2d_scoreMatrix__matrixAsInput__forType__MCN(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, eps, self.sparseDataResult);
      } else if(metric_id == e_kt_correlationFunction_groupOf_MINE_mcn_general) {
	compute_2d_scoreMatrix__matrixAsInput__forType__MCN_GENERAL(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, self.sparseDataResult);
      } else if(metric_id == e_kt_correlationFunction_groupOf_MINE_gmic) {
	compute_2d_scoreMatrix__matrixAsInput__forType__GMIC(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, power, self.sparseDataResult);
      } else if(metric_id == e_kt_correlationFunction_groupOf_MINE_tic) {
	compute_2d_scoreMatrix__matrixAsInput__forType__TIC(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, self.sparseDataResult);
      } else {
	fprintf(stderr, "!!\t Add support for enum=\"%s\", at [%s]:%s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), __FUNCTION__, __FILE__, __LINE__);
	assert(false); // ie, as we then need adding support for this case
	//! A fall-back:
	compute_2d_scoreMatrix__matrixAsInput__forType__MIC(data1, data2, nrows_1, nrows_2, ncols, resultMatrix_tmp, self.sparseDataResult);
      }
      if(resultMatrix != NULL) {
#include "kt_distance__stub__mineValueUseMatrix__after.c" //! which is used if "resultMatrix == NULL"  (oekseth, 06. des. 2016).
      }
    }
  }
  

  if( 
     (
      (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) //typeOf_correlationPreStep 
      || (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary)
      )
     && !self.inAllAgainstAll__inputIsAlreadySorted_forSpearman) {
    const bool data1_eq_data2 = (data_input1 == data_input2);
    const bool deAlloc_2 = (data1_eq_data2 == false) && (data2 != data_input2);
    if(data1 != data_input1) {
      // assert(deAlloc_2 == false); //! ie, as we otherwise may have an icnostency.
      free_2d_list_float(&data1, nrows); data1 = NULL;
      if(data1_eq_data2) {data2 = NULL;} //! ie, as it is not neccesary to de-allcoate boht (oesketh, 06. jul. 2019).
    }
    if(deAlloc_2 && (data2 != NULL)) {
      assert(data2 != NULL);
      const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
      free_2d_list_float(&data2, nrows_2);
      data2 = NULL;
    }
  } 
}

if(resultMatrix) { //! Investigate for case="T_FLOAT_MIN" (oekseth, 06. jan. 2017):
  const uint nrows_1 = nrows;
  const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
  for(uint i = 0; i < nrows_1; i++) {
    assert(resultMatrix[i]);
    for(uint k = 0; k < nrows_2; k++) {
      if(resultMatrix[i][k] == T_FLOAT_MIN) {resultMatrix[i][k] = T_FLOAT_MAX;}
    }
  }
 }

/* if(self.sparseDataResult) { */
/*   // FIXME: cosnider dropping this */
/*  } */
