
// ***********************************************************
#include "kt_storage_resultOfClustering.h"
#include "kt_distributionTypesC.h" //! which simplfies access to distrubion-types.
// ***********************************************************



#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief examplifies the impact of analsysing high-diemsioanl data, when viwed throguht hte lsene of the 16+ different dsitrbuion-times (inlcuded into C++).
   @author Ole Kristian Ekseth  (oekseth,  06. aug. 2020).
   @remarks 
   -- compile: g++ -O2 -g tut-bias-3-histogram-sensitvity-variance-in-randomness-multipleDistributions.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_3.exe
**/
int main() 
#else
  static void tut_bias_3_histogram_sensitvity_variance_in_randomness_multipleDistributions
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif


  // FIXME: InfoVis: how to visaulize these results?
  // FIXME: value: what isnight do they povide?
  // FIXME:
  // FIXME: code: extend code with the distrubions foiund/used in tut-bias-2 <-- the ealrier vverion took approx 300 mintues, whiel the udpated code-verion takes....??...
  // FIXME: code: ... clean this coe ... emove commend-out-blocks ... fix FIXMEs ... 
  // FIXME: exec: ... apply this code ... fix bugs .. idneitfy the fasible of adding exta eval-stpes (Based on the runinng-time).
  //
  // FIXME: code: ... automtically explore for nclust-values=[2, ...]
  // FIXME: code: ...  
  // FIXME: code: apply the post-clusteirng-stpes in/fom tut-2. ... then udpate latter tut-eval-code with a seperate entropy-step ... <-- nessary, ie, as we sue histograms?
  // FIXME: exec:
  //
  // FIXME: code:   
  // FIXME: code: explroe for all sim-metrics?
  // FIXME: code: ...  
  // FIXME: exec:
  //
  // FIXME: code:
  // FIXME: code:
  // FIXME: code: InfoVis
  // FIXME: 


  //const e_kt_normType_t config_duringBuildingOfDistributions_apply_normalization = e_kt_normType_undef; //! which is the defualt stategy
  //const e_kt_normType_t config_duringBuildingOfDistributions_apply_normalization = e_kt_normType_avgRelative; //! as defined in "kt_aux_matrix_norm.h".
  //const e_kt_normType_t config_duringBuildingOfDistributions_apply_normalization = e_kt_normType_relative_STD_abs; //! as defined in "kt_aux_matrix_norm.h"
  //const e_kt_normType_t config_duringBuildingOfDistributions_apply_normalization = e_kt_normType_relative_STD; //! as defined in "kt_aux_matrix_norm.h".
  const e_kt_normType_t config_duringBuildingOfDistributions_apply_normalization = e_kt_normType_rankRelative; //! as defined in "kt_aux_matrix_norm.h".

  //! ----------------------------------------
  //! ---------------------------------------- configuraitons:  
  // ----
  const uint center_relative_max = 4; //! which is used as the center-point when generating randomzied functions (Eq. \ref{}) .... Eq.: $nrows / center$ .... 
  //const uint cnt_times_random = 100;
  //! ----
  const uint weights_map_size = 5;
  // FIXME: correlate below into a seperate matrix.
  const uint weights_map[weights_map_size][2] = {{1,10},{1,100},{1,1000}, {1, 1000} , {1, 10000} };
  //! ----
  const uint dimensions_cnt = 20;
  const uint dimensions_stepSize = 200;
  //!
  //! The histogram setup ("kt_list_2d.h");
  const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
  // FIXME: meausrement: ... use below histo-type to estiamte the seperatbility across different dimensions ... apply a row-based data-nromsaiton before rpign the socreos out (to get an approxmiate of the seperaily, rhater thant he sim-scores themself) .... hyptoesis: if the sepraility decreases with icnreasing number of dimeisons, then ... 
  //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
  //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;    
  //! ---------------------------------------- configuraitons:completed.


  const bool globalConfig_sRandReset = false; //! which if set to 'true' implies that each vector gets the same randomzied values (hence, increases accuracy of predciton-comparison).
  //! Configure the pawisie ssimlairty-metric
  //const e_kt_categoryOf_correaltionPreStep_t list_metrics_preStep = e_kt_categoryOf_correaltionPreStep_binary;   const char *list_metrics_preStep_str = "preBinary";
  //const e_kt_categoryOf_correaltionPreStep_t list_metrics_preStep = e_kt_categoryOf_correaltionPreStep_rank;   const char *list_metrics_preStep_str = "preRank";
  const e_kt_categoryOf_correaltionPreStep_t list_metrics_preStep = e_kt_categoryOf_correaltionPreStep_none;   const char *list_metrics_preStep_str = "preNone";
  
  const bool isTo_writeOut_temporaryData = false;
  //const uint cnt_times_random = 2;
  //const uint dimensions_cnt = 2;
  const uint cnt_times_random = 100;
  //const uint cnt_times_random = 1000;
  //const uint dimensions_cnt = 20;
  //! Then specify the set of exepcted ranks:
  const t_float dimensions_curve[dimensions_cnt] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,  9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
  //const uint dimensions_stepSize = 200;
  //const uint dimensions_stepSize = 1000;
  {
#define tutBiasConfig_testAllBaseMetrics
#ifndef tutBiasConfig_testAllBaseMetrics
    const uint list_metrics_size = 6;
    const s_kt_correlationMetric_t list_metrics[list_metrics_size] = {
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_chebychev, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_cityblock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)
    };
#else
    const uint list_metrics_size =   (uint)e_kt_correlationFunction_undef;
#endif //! #ifndef tutBiasConfig_testAllBaseMetrics
    

    //!
    //! The histogram setup ("kt_list_2d.h");
    const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
    // FIXME: meausrement: ... use below histo-type to estiamte the seperatbility across different dimensions ... apply a row-based data-nromsaiton before rpign the socreos out (to get an approxmiate of the seperaily, rhater thant he sim-scores themself) .... hyptoesis: if the sepraility decreases with icnreasing number of dimeisons, then ... 
    //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
    //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;    
    const uint config_cnt_histogram_bins = 10;
    //!
    //! Specify the differnet types of randomzaiton-fucntisoinw we are to explore:
    const uint config_maxValue = 100; //! ie, the value-spread.
    /* const uint setOfRandomNess_size = 3; */
    /* const e_kt_randomGenerator_type_t setOfRandomNess[setOfRandomNess_size] = { */
    /*   e_kt_randomGenerator_type_hyLysisDefault, */
    /*   // e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)" */
    /*   // e_kt_randomGenerator_type_posixRan__srand__time__modulo, */
    /*   // e_kt_randomGenerator_type_binominal__incrementalInList */
    /*   e_kt_randomGenerator_type_posixRan__srand__time__modulo, */
    /*   e_kt_randomGenerator_type_posixRan__srand__time__uniform, */
    /*   // e_kt_randomGenerator_type_binominal__pst__07, */
    /*   //e_kt_randomGenerator_type_binominal__pst__15, */
    /*   //e_kt_randomGenerator_type_binominal__pst__35,       */
    /* }; */
    /* const char *setOfRandomNess_str[setOfRandomNess_size] = { */
    /*   "random-by-multinomial-distribution", */
    /*   "random-modulo", */
    /*   "random-uniform", */
    /* }; */
    //! -------------------------- end of configuration.
    //!
    //! Initaite the matrix used to as reference-frame when comapred to the other matrices
    //s_kt_matrix_t mat_first = setToEmptyAndReturn__s_kt_matrix_t();
    //s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(/*nrows=*/, /*ncols=*/nrows);
    s_kt_matrix_t mat_corrAll = initAndReturn__s_kt_matrix(/*cnt-alg=*/e_distributionTypesC_undef*center_relative_max * list_metrics_size, /*ncols=*/dimensions_cnt);
    uli mat_corrAll_index = 0; //! which is the row-index used during insertions in mat_corrAll; for details of the "uli" type, see "types_base.h"
    { //! Set the column-headers:
      assert(mat_corrAll.ncols == dimensions_cnt);
      for(uint dim_index = 0; dim_index < dimensions_cnt; dim_index++) {
	//for(uint i = 0; i < mat_corrAll.ncols; i++) {
	const uint ncols = (dim_index+1)*dimensions_stepSize;	    
	allocOnStack__char__sprintf(2000, str, "dim-%u", ncols);
	set_stringConst__s_kt_matrix(&mat_corrAll, /*index=*/dim_index, str, /*addFor_column=*/true);
      }
    }


    
    //!
    //! Explroe for differnet randomzation-strategies:
    //! 
    // uint mat_result_index = 0;
    //!
    for(uint dist_id = 0; dist_id < e_distributionTypesC_undef; dist_id++) {
      for(uint center_relative = 1; center_relative <= center_relative_max; center_relative++) {
      //for(uint center_relative = 1; center_relative <= center_relative_max; center_relative++, mat_result_index++) {
	//for(uint rand_id = 0; rand_id < setOfRandomNess_size; rand_id++) {      
	//! Construct: the object holding the randomizatin-distributions:
	const uint ncols = config_cnt_histogram_bins;
	const uint center_index = (uint)(/*center=*/(t_float)ncols/(t_float)center_relative);

	// const e_kt_randomGenerator_type_t type_randomness_in = setOfRandomNess[rand_id];
	//! Traverse different types of simliarty-metrics:
	for(uint i = 0; i < list_metrics_size; i++, mat_corrAll_index++) {
	  assert(mat_corrAll_index < mat_corrAll.nrows);
	  printf("\t(info) rand=%u/%u, metric=%u/%u, at %s:%d\n", dist_id, e_distributionTypesC_undef, i, list_metrics_size, __FILE__, __LINE__);
#ifndef tutBiasConfig_testAllBaseMetrics
	  const s_kt_correlationMetric_t curr_metric = list_metrics[i];
#else
	  const s_kt_correlationMetric_t curr_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/(e_kt_correlationFunction_t)i, /*prestep=*/list_metrics_preStep);
#endif //! #ifndef tutBiasConfig_testAllBaseMetrics
	
	  s_distributionTypesC_t rand_1 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, center_index);
	  //s_distributionTypesC_t rand_2 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, center_index);	    

	  { //! Set the row-header:
	    assert(mat_corrAll_index < mat_corrAll.nrows);
	    const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id);
	    allocOnStack__char__sprintf(2000, str, "%s-%u-%s-%s", getStringOfDistribution_s_distributionTypesC(&rand_1), center_index, str_metric, list_metrics_preStep_str);
	    //allocOnStack__char__sprintf(2000, str, "%s-%s-%s", setOfRandomNess_str[rand_id], str_metric, list_metrics_preStep_str);
	    set_stringConst__s_kt_matrix(&mat_corrAll, /*index=*/mat_corrAll_index, str, /*addFor_column=*/false);
	  }
	  //! Initaite the histogram-matrix
	  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(/*nrows=*/dimensions_cnt, /*ncols=*/config_cnt_histogram_bins);
	  //!
	  //! Construct vectors, and for each vector comptue scores:
	  for(uint dim_index = 0; dim_index < dimensions_cnt; dim_index++) {
	    //! Note: the below "srand(0)" step is important: it ensures that the random values are the same in the beginning of each radnomied sample-trial.
	    // FIXME: what is the effect of 'fixing' the randomzaiton for each run? do we observe an icnrease in prediciton-variance if the andomzaiton is not fixed? If so, may this prediciton-differences (attributable to differrent randomized vectors) be used to assess the acuray-threshold of predcitons relating to randomzied sampling?
	    // FIXME[paper]: describe the differnet ranozmaiton-strategies we apply (for this testing) ... does the randomization-types give rise to different predictions?
	    if(globalConfig_sRandReset)  {
	      srand(0);
	    }
	    //! 
	    const uint ncols = (dim_index+1)*dimensions_stepSize;
	    //! Initaite the local placeholder for the results, as used as input to the result-matrix:
	    s_kt_matrix_base_t mat_scores = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, ncols);

	    //! Get inputs for the histogram
	    for(uint rand_trial = 0; rand_trial < cnt_times_random; rand_trial++) {
	      //! Construct the random vector, ie, load randomness:
	      //s_kt_randomGenerator_vector_t rand_1 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	      //s_kt_randomGenerator_vector_t rand_2 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	      //! Call randomness:
	      //randomassign__speicficType__math_generateDistributions(&rand_1);
	      //randomassign__speicficType__math_generateDistributions(&rand_2);	    
	      //! 
	      //! Compute the similairty:
	      s_kt_list_1d_float_t vec_scores_1 = get_values_s_distributionTypesC_t(&rand_1, ncols);
	      s_kt_list_1d_float_t vec_scores_2 = get_values_s_distributionTypesC_t(&rand_1, ncols);	    
	      //s_kt_list_1d_float_t vec_scores_2 = get_values_s_distributionTypesC_t(&rand_2, ncols);	    
	      t_float sim_score = T_FLOAT_MAX; //! ie, set to empty.
	      //const bool is_ok = apply__uint_rows_hp_distance(curr_metric, rand_1.mapOf_columnToRandomScore, rand_2.mapOf_columnToRandomScore, ncols, &sim_score);
	      assert(ncols > 0);
	      const bool is_ok = apply__rows_hp_distance(curr_metric, vec_scores_1.list, vec_scores_2.list, ncols, &sim_score);
	      assert(is_ok);
	      //! Store the result:
	      assert(mat_scores.matrix[0]);
	      assert(mat_scores.ncols > rand_trial);
	      mat_scores.matrix[/*row-id=*/0][/*col-id=*/rand_trial] = sim_score;
	      //! De-allocate:
	      free__s_kt_list_1d_float_t(&vec_scores_1);
	      free__s_kt_list_1d_float_t(&vec_scores_2);	  	  	    
	    }
	    //! Compute the historgram:
	    const bool config_isTo_forMinMax_useGlobal = true;
	    s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(&mat_scores, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	    //! 
	    //! Store the result:
	    assert(mat_result.matrix[dim_index]);
	    assert(mat_result.ncols == config_cnt_histogram_bins);
	    //! Handle issues relating to dynamic resizeing of the sparse 1d-list:
	    uint b_size = list_histo.list_size;
	    if(b_size > config_cnt_histogram_bins) {
	      b_size = config_cnt_histogram_bins;
	    }
	    //! Then the copy-operation:
	    for(uint b = 0; b < b_size; b++) {
	      mat_result.matrix[dim_index][b] = list_histo.list[b];
	    }
	    { //! Compute the similiarty between the histogram and the exepcted bell-shaped curve, and then update the result-matrix:
	      //! Idea: Comptue a matrix \textit{algorithm-combination} x \textit{ideal-bell-shaped-curve} .... in the process, we ... seperately for each row in the result-set compute Kendall's Tau; the distance is computed from the bell-shaped histogram to the other histogram, seprately for each row.   .... then compare the different aglroithsm to each other, for which the 'number-of-steps' algorithm fo Kendall's Tau is used (Fig. ref{}) ... and then take itno accound the effects of slight shifts in the distrrubiotns for thwch the MINE algorithm is used  (Fig. ref{}). 
	      //! ... idea: randomly selecting vertices for comparison ... implies that their simliarty should ahve a bell-shape ... as it is more unlikely that two vectors are extemly different ... An example is seen for dice tossing, where there is a $1/6$ proability of getting a given number (eg, \textit{six}), hence there is a $1/6 * 1/6$ proability for fetching the numbers with the highest spreads (ie, number=1 and number=6). ... This illustrates why the bell-shape arises when comparing vectors generated from a random sampling of data .... This gives an increased insight into the capability of analysing high-diemsioanl data-sets: if a given metric amnages to generate bell-shaped curves for larger datasets, then ... seperability of different ... 
	      //! 
	      //! Compute the similairty:
	      t_float sim_score = T_FLOAT_MAX; //! ie, set to empty.
	      const s_kt_correlationMetric_t metric_compareHistoToExpectedOrder = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, e_kt_categoryOf_correaltionPreStep_rank);
	      if(list_histo.list_size > 0) {
		assert(/*ncols=*/list_histo.list_size > 0);
		const bool is_ok = apply__rows_hp_distance(metric_compareHistoToExpectedOrder, list_histo.list, (t_float*)dimensions_curve, list_histo.list_size, &sim_score);
		//const bool is_ok = apply__rows_hp_distance(curr_metric, row_1, row_2, ncols, &scalar_result);
		assert(is_ok);
		//! Update the result-strcuture:
		mat_corrAll.matrix[mat_corrAll_index][dim_index] = sim_score;
	      }
	    }
	  
	    // FIXME: store into differnet 1d-lists ... and then test the depednecy ... eg, through a Kruskal--Wallis test?

	    //! De-allocate:
	    free__s_kt_matrix_base_t(&mat_scores);
	    free__s_kt_list_1d_float_t(&list_histo);
	  }
	  { //! Pre (before writing)
	    //! Task: add infomrative row-names and column-names
	    assert(dimensions_cnt == mat_result.nrows);
	    //! Strings for rows:
	    for(uint dim_index = 0; dim_index < dimensions_cnt; dim_index++) {
	      //for(uint k = 0; k < mat_result.nrows; k++) {
	      const uint ncols = (dim_index+1)*dimensions_stepSize;	    
	      allocOnStack__char__sprintf(2000, str, "dim-%u", ncols);
	      set_stringConst__s_kt_matrix(&mat_result, /*index=*/dim_index, str, /*addFor_column=*/false);
	    }
	    //! Strings for columns:
	    for(uint k = 0; k < mat_result.ncols; k++) {
	      allocOnStack__char__sprintf(2000, str, "bucket-%u", k);
	      set_stringConst__s_kt_matrix(&mat_result, /*index=*/k, str, /*addFor_column=*/true);
	    }	  
	  }
	  /* if(isTo_writeOut_temporaryData) { */
	  /*   //! Write out the histogram-matrix: */
	  /*   {	   */
	  /*     const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id); */
	  /*     allocOnStack__char__sprintf(2000, resultPrefix_local, "result-score-tut-bias-%s-%s.tsv", setOfRandomNess_str[rand_id], str_metric); */
	  /*     export__singleCall__s_kt_matrix_t(&mat_result, resultPrefix_local, NULL); */
	  /*   } */
	  /*   { //! Compute the Ranks:	   */
	  /*     const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id); */
	  /*     s_kt_matrix_t mat_rank = initAndReturn_ranks__s_kt_matrix(&mat_result); */
	  /*     allocOnStack__char__sprintf(2000, resultPrefix_local, "result-rank-tut-bias-%s-%s.tsv", setOfRandomNess_str[rand_id], str_metric); */
	  /*     export__singleCall__s_kt_matrix_t(&mat_rank, resultPrefix_local, NULL); */
	  /*     //!  */
	  /*     //! De-allocate: */
	  /*     free__s_kt_matrix(&mat_rank); */
	  /*   } */
	  /* } */
	  //! 
	  //! De-allocate:
	  free__s_kt_matrix(&mat_result);
	} //! end-of-metric-traversal
      } //! end-of-randomess-traversal-center-point
    } //! end-of-randomess-traversal-distribution
    //! --------------------------------------------------
    { //! Make the scores psoivie:
      //! Note: background: the ppm-exprt does not eseme to work when teh scores are negative
      // FIXME: is below pre-step correct?
      t_float score_min = T_FLOAT_MAX;
      for(uint row_id = 0; row_id < mat_corrAll.nrows; row_id++) {
	for(uint col_id = 0; col_id < mat_corrAll.ncols; col_id++) {
	  const t_float score = mat_corrAll.matrix[row_id][col_id];
	  if(isOf_interest(score)) {
	    // score_max = macro_max(score_max, score);
	    score_min = macro_min(score_min, score);
	  }
	}
      }
      if(score_min < 0) {
	score_min *= -1.0; //! ie, remove the sign.
	for(uint row_id = 0; row_id < mat_corrAll.nrows; row_id++) {
	  for(uint col_id = 0; col_id < mat_corrAll.ncols; col_id++) {
	    mat_corrAll.matrix[row_id][col_id] += score_min; //! hence using the offset to assure that all values are >= 0
	  }
	}
      }
    } //! end of prestep:positive-property.
    //! 

    
    { //! Analsye prediciotns
      // FIXME: export for differrent clsuter-count-props ... autoamte this ... 
      uint cluster_count = 2; //! ie, the ncluster-option.
      // FIXME: a comparison of cluster-count=$2,|distributiosn|/2$ reveals how ...??... % FIXME: udpate after we have completed our eval.
      //uint cluster_count = e_distributionTypesC_undef;
      // FIXME: set the cluster-count object dynamcially.
      s_kt_storage_resultOfClustering_t obj_cluster = initAndReturn_s_kt_storage_resultOfClustering_t(/*ncluster=k-cluster=*/cluster_count);
      s_kt_storage_resultOfClustering_t obj_cluster_transp = initAndReturn_s_kt_storage_resultOfClustering_t(/*ncluster=k-cluster=*/cluster_count);
      //! Note: export-result: transposed: <none>
      const char *resultPrefix_local = "result-3-randomDistributions";
      allocOnStack__char__sprintf(2000, str, "%s-n%u-%u-nclust%u", resultPrefix_local, dimensions_stepSize, dimensions_stepSize*dimensions_cnt, cluster_count);
      //! Step: apply Clustering --- movation is  to compare the resemablance to the exepected outcome, and aim at explrining tis difference.
      //! Apply: 
      assert(obj_cluster.data.mat_clusterIds.nrows == 0); //! as we expect this matrix to be emtpy at this time-point.
      analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(mat_corrAll, str, /*isTo_transpose=*/false, &obj_cluster, &obj_cluster_transp);
      //! -------------------------------------------------------------
      assert(obj_cluster.data.mat_clusterIds.matrix != obj_cluster_transp.data.mat_clusterIds.matrix);
      assert(obj_cluster_transp.data.mat_clusterIds.matrix != NULL);
      {
	//! Note: 
	//! Note: 
	const char *resultPrefix_local = "result-3-postCCM-randomDistributions";
	allocOnStack__char__sprintf(2000, str, "%s-n%u-%u-nclust%u", resultPrefix_local, dimensions_stepSize, dimensions_stepSize*dimensions_cnt, cluster_count);
	s_kt_storage_resultOfClustering_t l_obj_cluster = initAndReturn_s_kt_storage_resultOfClustering_t(/*k-cluster=*/e_distributionTypesC_undef);
	s_kt_storage_resultOfClustering_t l_obj_cluster_transp = initAndReturn_s_kt_storage_resultOfClustering_t(/*k-cluster=*/e_distributionTypesC_undef);
	//! Step: apply Hierarhcical Clustering --- movation is  to compare the resemablance to the exepected outcome, and aim at explrining tis difference.
	//! Apply: 
	assert(l_obj_cluster.data.mat_clusterIds.nrows == 0); //! as we expect this matrix to be emtpy at this time-point.
	analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(obj_cluster.data.mat_clusterIds, str, /*isTo_transpose=*/false, &l_obj_cluster, &l_obj_cluster_transp);
	// ---------------
	//! De-allocate:
	free__s_kt_storage_resultOfClustering_t(&l_obj_cluster);
	free__s_kt_storage_resultOfClustering_t(&l_obj_cluster_transp);    
      }
      // ---------------
      //! De-allocate:
      free__s_kt_storage_resultOfClustering_t(&obj_cluster);
      free__s_kt_storage_resultOfClustering_t(&obj_cluster_transp);    	  
    }
    //assert(false); // FIXME: add an exprot-step
    
    //! 
    //! De-allocate:
    //free__s_kt_matrix(&mat_first);
    free__s_kt_matrix(&mat_corrAll);
  }  

  
  //! --------------------------------------------------------------------
	
  ////!
  ////! @return
#ifndef __M__calledInsideFunction
  ////! *************************************************************************
  ////! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  ////! *************************************************************************  
  return true;
#endif
}

  
