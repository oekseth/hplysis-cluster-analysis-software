 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_euclid) { return "e_kt_correlationFunction_groupOf_minkowski_euclid";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_cityblock) { return "e_kt_correlationFunction_groupOf_minkowski_cityblock";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_minkowski) { return "e_kt_correlationFunction_groupOf_minkowski_minkowski";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne) { return "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3) { return "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4) { return "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5) { return "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_chebychev) { return "e_kt_correlationFunction_groupOf_minkowski_chebychev";}
 if(enum_id  == e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max) { return "e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen) { return "e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_Gower) { return "e_kt_correlationFunction_groupOf_absoluteDifference_Gower";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_Soergel) { return "e_kt_correlationFunction_groupOf_absoluteDifference_Soergel";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski) { return "e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_Canberra) { return "e_kt_correlationFunction_groupOf_absoluteDifference_Canberra";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian) { return "e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4";}
 if(enum_id  == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5) { return "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_intersection) { return "e_kt_correlationFunction_groupOf_intersection_intersection";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_intersection_altDef) { return "e_kt_correlationFunction_groupOf_intersection_intersection_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_WaveHedges) { return "e_kt_correlationFunction_groupOf_intersection_WaveHedges";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt) { return "e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Czekanowski) { return "e_kt_correlationFunction_groupOf_intersection_Czekanowski";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef) { return "e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Motyka) { return "e_kt_correlationFunction_groupOf_intersection_Motyka";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Motyka_altDef) { return "e_kt_correlationFunction_groupOf_intersection_Motyka_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Kulczynski) { return "e_kt_correlationFunction_groupOf_intersection_Kulczynski";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Ruzicka) { return "e_kt_correlationFunction_groupOf_intersection_Ruzicka";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Tanimoto) { return "e_kt_correlationFunction_groupOf_intersection_Tanimoto";}
 if(enum_id  == e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef) { return "e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_innerProduct) { return "e_kt_correlationFunction_groupOf_innerProduct_innerProduct";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_harmonicMean) { return "e_kt_correlationFunction_groupOf_innerProduct_harmonicMean";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_cosine) { return "e_kt_correlationFunction_groupOf_innerProduct_cosine";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook) { return "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_Dice) { return "e_kt_correlationFunction_groupOf_innerProduct_Dice";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef) { return "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef) { return "e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance) { return "e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian) { return "e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian";}
 if(enum_id  == e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator) { return "e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_fidelity) { return "e_kt_correlationFunction_groupOf_fidelity_fidelity";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya) { return "e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_Hellinger) { return "e_kt_correlationFunction_groupOf_fidelity_Hellinger";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_Matusita) { return "e_kt_correlationFunction_groupOf_fidelity_Matusita";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef) { return "e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef) { return "e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_squaredChord) { return "e_kt_correlationFunction_groupOf_fidelity_squaredChord";}
 if(enum_id  == e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef) { return "e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Euclid) { return "e_kt_correlationFunction_groupOf_squared_Euclid";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Pearson) { return "e_kt_correlationFunction_groupOf_squared_Pearson";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Neyman) { return "e_kt_correlationFunction_groupOf_squared_Neyman";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_squaredChi) { return "e_kt_correlationFunction_groupOf_squared_squaredChi";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_probabilisticChi) { return "e_kt_correlationFunction_groupOf_squared_probabilisticChi";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_divergence) { return "e_kt_correlationFunction_groupOf_squared_divergence";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Clark) { return "e_kt_correlationFunction_groupOf_squared_Clark";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi) { return "e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic) { return "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute) { return "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered) { return "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered";}
 if(enum_id  == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute) { return "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute";}
 if(enum_id  == e_kt_correlationFunction_groupOf_shannon_KullbackLeibler) { return "e_kt_correlationFunction_groupOf_shannon_KullbackLeibler";}
 if(enum_id  == e_kt_correlationFunction_groupOf_shannon_Jeffreys) { return "e_kt_correlationFunction_groupOf_shannon_Jeffreys";}
 if(enum_id  == e_kt_correlationFunction_groupOf_shannon_kDivergence) { return "e_kt_correlationFunction_groupOf_shannon_kDivergence";}
 if(enum_id  == e_kt_correlationFunction_groupOf_shannon_Topsoee) { return "e_kt_correlationFunction_groupOf_shannon_Topsoee";}
 if(enum_id  == e_kt_correlationFunction_groupOf_shannon_JensenShannon) { return "e_kt_correlationFunction_groupOf_shannon_JensenShannon";}
 if(enum_id  == e_kt_correlationFunction_groupOf_shannon_JensenDifference) { return "e_kt_correlationFunction_groupOf_shannon_JensenDifference";}
 if(enum_id  == e_kt_correlationFunction_groupOf_combinations_Taneja) { return "e_kt_correlationFunction_groupOf_combinations_Taneja";}
 if(enum_id  == e_kt_correlationFunction_groupOf_combinations_KumarJohnson) { return "e_kt_correlationFunction_groupOf_combinations_KumarJohnson";}
 if(enum_id  == e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock) { return "e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges) { return "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax) { return "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin) { return "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric) { return "e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax) { return "e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax) { return "e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax";}
 if(enum_id  == e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin) { return "e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin";}
 if(enum_id  == e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine) { return "e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine";}
 if(enum_id  == e_kt_correlationFunction_groupOf_rank_kendall_coVariance) { return "e_kt_correlationFunction_groupOf_rank_kendall_coVariance";}
 if(enum_id  == e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock) { return "e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock";}
 if(enum_id  == e_kt_correlationFunction_groupOf_rank_kendall_Dice) { return "e_kt_correlationFunction_groupOf_rank_kendall_Dice";}
 if(enum_id  == e_kt_correlationFunction_groupOf_rank_kendall_Goodman) { return "e_kt_correlationFunction_groupOf_rank_kendall_Goodman";}
 if(enum_id  == e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized) { return "e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_mic) { return "e_kt_correlationFunction_groupOf_MINE_mic";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_mas) { return "e_kt_correlationFunction_groupOf_MINE_mas";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_mev) { return "e_kt_correlationFunction_groupOf_MINE_mev";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_mcn) { return "e_kt_correlationFunction_groupOf_MINE_mcn";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_mcn_general) { return "e_kt_correlationFunction_groupOf_MINE_mcn_general";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_gmic) { return "e_kt_correlationFunction_groupOf_MINE_gmic";}
 if(enum_id  == e_kt_correlationFunction_groupOf_MINE_tic) { return "e_kt_correlationFunction_groupOf_MINE_tic";}
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_avg) { return "e_kt_correlationFunction_groupOf_directScore_direct_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_sum) { return "e_kt_correlationFunction_groupOf_directScore_direct_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_direct_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_mul) { return "e_kt_correlationFunction_groupOf_directScore_direct_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_min) { return "e_kt_correlationFunction_groupOf_directScore_direct_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_max) { return "e_kt_correlationFunction_groupOf_directScore_direct_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_direct_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_direct_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_abs) { return "e_kt_correlationFunction_groupOf_directScore_direct_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_direct_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_direct_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs";}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if(enum_id  == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus) { return "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus";}
#endif
