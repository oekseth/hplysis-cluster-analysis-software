#!/usr/bin/perl -w
use strict; 
use Carp;
my @arrOf_marks = (
"asterisk",
"star",
"oplus*",
"oplus",
"otimes",
"otimes*",
"square",
"square*",
"triangle*",
"triangle",
"diamond",
"diamond*",
"pentagon",
"pentagon*",
#"",
#"",
"-",
"|"
    );

my @arrOf_colors = split(/\s+/, qq(Apricot     Aquamarine  
Bittersweet     Black  
Blue     BlueGreen  
BlueViolet     BrickRed  
Brown     BurntOrange  
CadetBlue     CarnationPink  
Cerulean     CornflowerBlue  
Cyan     Dandelion  
DarkOrchid     Emerald  
ForestGreen     Fuchsia  
Goldenrod     Gray  
Green     GreenYellow  
JungleGreen     Lavender  
LimeGreen     Magenta  
Mahogany     Maroon  
Melon     MidnightBlue  
Mulberry     NavyBlue  
OliveGreen     Orange  
OrangeRed     Orchid  
Peach     Periwinkle  
PineGreen     Plum  
ProcessBlue     Purple  
RawSienna     Red  
RedOrange     RedViolet  
Rhodamine     RoyalBlue  
RoyalPurple     RubineRed  
Salmon     SeaGreen  
Sepia     SkyBlue  
SpringGreen     Tan  
TealBlue     Thistle  
Turquoise     Violet  
VioletRed     White  
WildStrawberry     Yellow  
YellowGreen
));

my $matrixGlobal_prevScores = [];
sub __reset_matrix() {
    $matrixGlobal_prevScores = [];
}

sub initConfig__fileRead {
    return {
	isTo_transposeMatrix => 0,
	isTo_takeRelate_lastColumn => 0,
	thresh_maxScore_toIgnore => 200, #! which is the max-score to be ingored
	thresh_maxScore_toIgnore_setToFixedValue => undef, #! which if set impleis that we sue the value when a ceirtna thresohld is 'passed'.
	thresh_score_isTo_ingore0 => 1, #! ie, as the '0' score is often used to denote an 'unkown' case.
	isTo_onlyReset_matrixGlobal_prevScores_OnResetCall => 0,
	dataInput => {
	    mat_ref => undef, 
	    arr_rowHead => undef,
	    arr_colHead => undef,
	    nrows => undef, 
	    ncols => undef,
	}
    };
}
sub initConfig__fileRead__setData {
    my ($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols) = @_;
    my $config = initConfig__fileRead();
    #!
    #!
    $config->{dataInput}->{mat_ref} = $mat_ref;
    $config->{dataInput}->{arr_rowHead} = $arr_rowHead;
    $config->{dataInput}->{arr_colHead} = $arr_colHead;
    $config->{dataInput}->{nrows} = $nrows;
    $config->{dataInput}->{ncols} = $ncols;
    #!
    #!
    return $config;
}


my $fileName_global_meta = "meta_fromCollection.tsv";
open(FILE_OUT_global_meta, ">$fileName_global_meta") or die("An error in opning \"$fileName_global_meta\"");

#! @return an object ot hold summairze-proeprties wrt. column-scores.
sub __columnsCollection__init {
    return {
	arrOf_columns_min => [],
 	arrOf_columns_max => [], #! ie, the min-max-scores wrt. the column-scores.
	arrOf_columns_min_rowId => [],
 	arrOf_columns_max_rowId => [], #! ie, the min-max-scores wrt. the column-scores.
    };
}


#! Udpate the score-proeprty:
sub __columnsCollection__updateCell {
    my ($self, $row_id, $col_id, $score) = @_;
    my $size_list = scalar(@{$self->{arrOf_columns_max}});
    if($col_id < $size_list) { #! 
	if(defined($self->{arrOf_columns_min}->[$col_id])) {
	    if($self->{arrOf_columns_min}->[$col_id] > $score) {
		#! Then we update:
		$self->{arrOf_columns_min}->[$col_id] = $score;
		$self->{arrOf_columns_min_rowId}->[$col_id] = $row_id;
	    }
	    if($self->{arrOf_columns_max}->[$col_id] < $score) {
		#! Then we update:
		$self->{arrOf_columns_max}->[$col_id] = $score;
		$self->{arrOf_columns_max_rowId}->[$col_id] = $row_id;
	    }
	} else {
	    #!
	    #! The set the score:
	    $self->{arrOf_columns_min}->[$col_id] = $score;
	    $self->{arrOf_columns_min_rowId}->[$col_id] = $row_id;
	    $self->{arrOf_columns_max}->[$col_id] = $score;
	    $self->{arrOf_columns_max_rowId}->[$col_id] = $row_id;
	}
    } else { #! then we udapte both scores, ie, initiate: 
	for(my $i = $size_list; $i < ($col_id+1); $i++) {
	    push(@{$self->{arrOf_columns_min}}, undef);	    
	    push(@{$self->{arrOf_columns_max}}, undef);	    
	    push(@{$self->{arrOf_columns_min_rowId}}, undef);	    
	    push(@{$self->{arrOf_columns_max_rowId}}, undef);	    
	}
	#!
	#! The set the score:
	$self->{arrOf_columns_min}->[$col_id] = $score;
	$self->{arrOf_columns_min_rowId}->[$col_id] = $row_id;
	$self->{arrOf_columns_max}->[$col_id] = $score;
	$self->{arrOf_columns_max_rowId}->[$col_id] = $row_id;
    }
}

#! 
#! Write out the summary:
sub __columnsCollection__writeOut {
    my ($self, $name_result, $stringId, $arrOf_rows, $arrOf_columns) = @_;
    my $result_tex = $name_result . "_" . $stringId . "_result_colSum.tex";
    printf("result-col=\"%s\"\n", $result_tex);
    printf(STDERR "\t Export to result=\"%s\", at %s:%d\n", $result_tex, __FILE__, __LINE__);
    open(FILE_OUT_tex, ">$result_tex") or die("!!\t result(ordinary): An error in opning file=\"$result_tex\"");
    printf(FILE_OUT_tex "The row-mappings are: \n \\begin{enumerate}\n");
    for(my $i = 0; $i < scalar(@{$arrOf_rows}); $i++) {
	printf(FILE_OUT_tex "\\item (%d): %s\n", $i, $arrOf_rows->[$i]);
    }
    printf(FILE_OUT_tex "\\end{enumerate}\n");
    printf(FILE_OUT_tex "\n\nThe column-mappings are: \n \\begin{enumerate}\n");
    for(my $i = 0; $i < scalar(@{$arrOf_columns}); $i++) {
	printf(FILE_OUT_tex "\\item (%d): %s\n", $i, $arrOf_columns->[$i]);
    }
    printf(FILE_OUT_tex "\\end{enumerate}\n");
    { #! Write out the latex-table
	my $size_list = scalar(@{$self->{arrOf_columns_max}});    
	printf(FILE_OUT_tex "\n\nThe column-properties are: \n \\begin{tabular}{cccccc}\n");
	printf(FILE_OUT_tex "%s & %s & %s & %s & %s & %s \\\\ \\hline \n", 
	       "Column-name", 
	       "score(min)", "score(max)", 
	       "score(max-min/max)",
	       "row-min", 
	        "row-max");
	for(my $col_id = 0; $col_id < $size_list; $col_id++) {
	    my $score_min = $self->{arrOf_columns_min}->[$col_id];
	    my $score_min_index = $self->{arrOf_columns_min_rowId}->[$col_id];
	    if(defined($score_min)) { #! then the row is defined:
		my $score_max = $self->{arrOf_columns_max}->[$col_id];
		my $score_max_index = $self->{arrOf_columns_max_rowId}->[$col_id];
		#!
		#! Write out:
		my $score_relative = 0;
		if( ($score_max != 0) && ($score_max != 0) && ($score_min != 0) ) {$score_relative = ($score_max - $score_min)/$score_min;}
		printf(FILE_OUT_tex "%s & %.2f & %.2f & %.2f & %.2f & %.2f \\\\\n", $arrOf_columns->[$col_id],
		       $score_min, $score_max, $score_relative,
		       $score_min_index,   $score_max_index
		    );
	    }
	}
	printf(FILE_OUT_tex "\\end{tabular}\n");
    }
    #! ---------------
    #!
    #! Close:
    close(FILE_OUT_tex);
}

sub __readFile__convertTo_relativeFirstLine__writeOut {
    my ($input_file) = @_;    
    open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
    my $result_file = $input_file . "_result.tsv";
    open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");
    my $line_count= 0;
    { #! Open the file: 
	my @arr_first = ();
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.    while (my $line = <FILE_IN>) {
	    my @arrOf_cols = split("\t", $line);
#	    my @arrOf_cols = split($line, "\t");
	    #push(@matrix_input, \@arrOf_cols);
	    if($line_count == 0) {
		printf(FILE_OUT "%s\n", $line);
		#@arrOf_rowHead = @arrOf_cols;
		#printf(STDERR "rows=\"%s\", and line=\"$line\", at %s:%d\n", join(", ", @arrOf_rowHead), __FILE__, __LINE__);
	    } else {
		if(scalar(@arr_first) != 0) {
		    printf(FILE_OUT "%s\n", $arrOf_cols[0]);
		    for(my $i = 1; $i < scalar(@arrOf_cols); $i++) {
			my $score_num = $arrOf_cols[$i];
			my $score_denum = $arr_first[$i];
			my $is_added = 0;
			if(defined($score_num) && ($score_num != 0) ) {
			    if(defined($score_denum) && ($score_denum != 0) ) {
				printf(FILE_OUT "%f\t", $score_num / $score_denum);
				$is_added = 1;
			    } 
			}
			if($is_added == 0) {
			    printf(FILE_OUT "0\t", $line);
			}
		    }
		    printf(FILE_OUT "\n");
		} else {@arr_first = @arrOf_cols;}
	    }
	    # my $ncols = scalar(@arrOf_cols);
	    # if($ncols > $matrix_input_max_cnols) {
	    # 	$matrix_input_max_cnols = $ncols;
	    # }
	    # $matrix_input_max_nrows++;
	    $line_count++;
	}
	close(FILE_IN);
    }    
    
    close(FILE_OUT);
    return $result_file;
}

sub __readFile {
    my ($input_file, $isTo_transposeMatrix, $isTo_takeRelate_lastColumn) = @_;
    #!
    #!
    my @matrix_input;
    my $matrix_input_max_nrows = 0;
    my $matrix_input_max_cnols = 0;
    my @matrix;
    my $line_count = 0;
    my @arrOf_rowHead; 
    { #! Open the file: 
	open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.    while (my $line = <FILE_IN>) {
	    my @arrOf_cols = split("\t", $line);
#	    my @arrOf_cols = split($line, "\t");
	    push(@matrix_input, \@arrOf_cols);
	    if($line_count == 0) {
		@arrOf_rowHead = @arrOf_cols;
		# printf(STDERR "rows=\"%s\", and line=\"$line\", at %s:%d\n", join(", ", @arrOf_rowHead), __FILE__, __LINE__);
	    }
	    my $ncols = scalar(@arrOf_cols);
	    if($ncols > $matrix_input_max_cnols) {
		$matrix_input_max_cnols = $ncols;
	    }
	    $matrix_input_max_nrows++;
	    $line_count++;
	}
	close(FILE_IN);
    }
    #! 
    #! 
    if($isTo_transposeMatrix == 1) { #! then we transpose the input-matrix, ie, before evaluating the data:
	my @mat_tmp;
	for(my $col_id = 0; $col_id < $matrix_input_max_cnols; $col_id++) {
	    my @row; # = ();
	    for(my $row_id = 0; $row_id < $matrix_input_max_nrows; $row_id++) {
		push(@row, $matrix_input[$row_id][$col_id]); #! ie, a tranpsoed 'version'.
	    }
	    push(@mat_tmp, \@row);
	}	
	@matrix_input = @mat_tmp; #! ie, copy.
	# printf("matrix=@matrix_input\n");
	# foreach my $row (@matrix_input) {
	#     my @arrOf_cols = @{$row};
	#     printf("\t row:: %s\n", join(",", @arrOf_cols));
	# }
	# croak("validate [ªbove] for file=\"$input_file\"");
    }
    my @arrOf_colHead;
    my @mat_col_row;
#    while (my $line = <FILE_IN>) {
    my $nrows = 0; 
    $line_count = 0;
    foreach my $row (@matrix_input) {
	my @arrOf_cols = @{$row};
	if(scalar(@arrOf_cols) > 1) {
	    if($line_count == 0) {
		my $head = shift(@arrOf_cols);
		@arrOf_rowHead = @arrOf_cols;		
		#!
		#! Write otu the file-anems to the meta-file, eg, to simplify docuemtantion-efforts:
		# printf(STDERR "nrowS=$nrows, rows=\"%s\", given row=\"$row\", at %s:%d\n", join(", ", @arrOf_cols), __FILE__, __LINE__);
#		printf(STDERR "nrowS=$nrows, rows=\"%s\", given row=\"$row\", at %s:%d\n", join(@arrOf_cols, ","), __FILE__, __LINE__);
		#printf(STDERR "\n\n The file=\\hpFile{%s} has meta-headers: \n", $input_file);
		printf(FILE_OUT_global_meta "\n\n The file=\\hpFile{%s} has meta-headers: \n", $input_file);
		my $index = 0;
		foreach my $id (@arrOf_cols) {
		    printf(FILE_OUT_global_meta "index[%u]=\"%s\", \n", $index, $id);
		    $index++;
		}
	    } else {
		my $head = shift(@arrOf_cols);
		push(@arrOf_colHead, $head);
#		push(@arrOf_rowHead, $head);
		#push(@arrOf_x_value, $head);
		# printf("for head=\"$head\" set-cols: %s\n", join(",", @arrOf_cols));
#		printf("for head=\"$head\" set-cols: %s\n", join(",", @arrOf_cols));
		#push(@matrix, \@arrOf_cols);
		if($isTo_takeRelate_lastColumn == 1) {
		    my $nrows = scalar(@arrOf_cols);
		    my $score_inv = ($arrOf_cols[$nrows-1] != 0)  ? 1.0/$arrOf_cols[$nrows-1] : 0;
		    for(my $i = 0; $i < $nrows; $i++) {
			$arrOf_cols[$i] *= $score_inv;
		    }
		}
		push(@mat_col_row, \@arrOf_cols);
		if($nrows < scalar(@arrOf_cols)) {
		    $nrows = scalar(@arrOf_cols);
		}
	    }
	}
	$line_count++;
    }
#my $ncols = scalar(@matrix);
    my $ncols = scalar(@mat_col_row);
#    printf("ncols=$ncols\n"); 
    { #! Initate matrix:
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my @row = ();
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		#! Insert into the matrix:
		push(@row, $mat_col_row[$col_id][$row_id]);
	    }
	    push(@matrix, \@row);
	}
    }
    #!
    # printf(STDERR "nrowS=$nrows, rows=\"%s\"at %s:%d\n", join( ",", @arrOf_rowHead), __FILE__, __LINE__);
    #!
    return (\@matrix, \@arrOf_rowHead, \@arrOf_colHead, $nrows, $ncols);
}

sub __readFile__unifyFiles__sameRowIDS__initEach {
    my($tag, $file_name, $opt_columnToChoose) = @_;
    return {
	file_name => $file_name,
	tag => $tag,
	opt_columnToChoose => $opt_columnToChoose,
    };
}

=head
    @use-case to idnietyf the min--max scores from sperate data-sets, eg, to evlauate the combiend infleucne fo data-nroamzioant and pariwise simalirty-emtrics for the KD-tree clsuter-algorithm.
    -- 
    @use-case
    -- 
=cut
sub __readFile__unifyFiles__sameRowIDS {
    my ($arr_inputFileObj, $isTo_transposeMatrix, $isTo_takeRelate_lastColumn) = @_;
    #! 
    #! Intiaite varriables: 
    my @matrix; 
    my @arrOf_rowHead;
    my @arrOf_colHead;
    my $nrows = 0; 
    my $ncols = 0; #! ie, 
    #! 
    #! Loop through the files: 
    foreach my $obj (@{$arr_inputFileObj}) {
	my $file_name = $obj->{file_name};
	my ($loc_mat_ref, $loc_arr_rowHead, $loc_arr_colHead, $loc_nrows, $loc_ncols) = __readFile($file_name, $isTo_transposeMatrix, $isTo_takeRelate_lastColumn);	
	if($nrows != 0) {
	    if($nrows != $loc_nrows) {
		croak("!!\t Expected nrows=$nrows, though \"$file_name\" had nrows=$loc_nrows\n");
	    }
	} else {
	    $nrows = $loc_nrows;
	    #! 
	    #! Add rows:
	    for(my $row_id = 0; $row_id < $nrows; $row_id++) {
		push(@matrix, ());
	    }
	    @arrOf_rowHead = @{$loc_arr_rowHead};
	}
	
	#! 
	#! Find min-max-scores and add these:
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my $score_min = undef; my $score_max = undef;
	    my $string_debug = $obj->{opt_columnToChoose};
	    for(my $col_id = 0; $col_id < $loc_ncols; $col_id++) {
		my $score = $loc_mat_ref->[$row_id]->[$col_id];
		if(defined($obj->{opt_columnToChoose}) && length($obj->{opt_columnToChoose}) ) {
		    my $str_cmp = $loc_arr_colHead->[$col_id];
		    if(lc($str_cmp) ne lc($obj->{opt_columnToChoose}) ) {$score = undef; $string_debug .= " \"" . $str_cmp . "\"";} #! ie, as we then assuem the score is Not to be part of the result set.
		}
		if(defined($score) && ($score =~ /^\s*([\d\.]+)\s*$/) ) {
#		    printf("\t $score\n");
		    $score = $1;
		    if(!defined($score_min) || ($score_min > $score) || ($score_min == 0) ) {
			$score_min = $score;
		    }
		    if(!defined($score_max) || ($score_max < $score) ) {
			$score_max = $score;
		    }
		}
	    }
	    #! 
	    #! Add:
	    push(@{$matrix[$row_id]}, $score_min);
	    push(@{$matrix[$row_id]}, $score_max);
	    if($row_id == 0) { #! Update column-header:
		my $tag = $obj->{tag};
		# printf("(%s)\t (%f, %fu), at %s:%d\n", $tag, $score_min, $score_max, __FILE__, __LINE__);
		push(@arrOf_colHead, $tag . ":min");
		push(@arrOf_colHead, $tag . ":max");
	    }
	    #if(!defined($score_min)) {croak(".... Investigate ... given ignore-filed-stirng=\"$string_debug\" ... ");}
	}
	#! 
	#! Increment based on [ªbove]
	$ncols += 2;
    }
    return (\@matrix, \@arrOf_rowHead, \@arrOf_colHead, $nrows, $ncols);    
}

sub compute {
    my ($input_file, $config, $result_directory) = @_;
    if(defined($result_directory) && length($result_directory)) {
	system("mkdir -p $result_directory");
    }
    if(!defined($config)) {
	$config = initConfig__fileRead();
    }
    if(!defined($config)) {croak("!!\t ivnestiate this issue");}
    my $name_result = "tmp";
    #my 
    if(defined($result_directory) && length($result_directory)) {
	# if(defined($result_directory) && length($result_directory)) {
	#     system("mkdir -p $result_directory");
	# }
	my @arrOf_filePArts = split($name_result, "/");
#	    my @arrOf_filePArts = split("/", $name_result);
	if(scalar(@arrOf_filePArts) > 1) {$name_result = $arrOf_filePArts[scalar(@arrOf_filePArts)-1];} #! ie, to remove any folder-file-rpefixes.
	# if($name_result =~/^\/([a-z_\d\.]+.tsv)$/) {
	#     $name
	# }
	$name_result = $result_directory . $name_result;
    }
    if(defined($input_file)) {
	printf("-----------------\nfile-name=\"%s\", at %s:%d\n", $input_file, __FILE__, __LINE__);
	my($name, $suffix) = ($input_file =~/^(.+)\.([a-z]+)$/);
	$name_result = $name;
	if(!defined($suffix)) { # || ($suffix ne "tex")) {
	    printf(STDERR "!!\t Expected a latex-file as inptu, ie Not the case given inptu-file=\"$input_file\" and suffix=\"$suffix\": aborts\n");
	    exit;
	}
    } else {
	$name_result = $result_directory . "_file_";
    }



#!
#! Parse the data-set: 
    my @arrOf_x_value; 
    my @matrix;
    my $nrows = 0; my $ncols = 0;    
    #! -------------- READ the file: 
    my @arrOf_colHead;     my @arrOf_rowHead;
    if(defined($input_file)) {
	my ($mat_ref, $arr_rowHead, $arr_colHead, $_nrows, $_ncols) = __readFile($input_file, $config->{isTo_transposeMatrix}, $config->{isTo_takeRelate_lastColumn});
	$nrows = $_nrows;
	$ncols = $_ncols;
	# printf(STDERR "nrowS=$nrows, rows=\"%s\"at %s:%d\n", join( ",", @{$arr_rowHead}), __FILE__, __LINE__);
	@matrix = @{$mat_ref};
	@arrOf_rowHead = @{$arr_rowHead};
	@arrOf_colHead = @{$arr_colHead};
    } else { #! then we asusme the data is 'loaded' into the config-object
	if(!defined($config->{dataInput}->{mat_ref})) {
	    croak("!!\t investigate why no inptu-data is set, ie, as neither an input-fiel nor a amcris is speifiec!");
	}
	@matrix = @{$config->{dataInput}->{mat_ref}};	
	@arrOf_rowHead = @{$config->{dataInput}->{arr_rowHead}};
	@arrOf_colHead = @{$config->{dataInput}->{arr_colHead}};
	$nrows = $config->{dataInput}->{nrows};
	$ncols = $config->{dataInput}->{ncols};
	# printf(STDERR "nrowS=$nrows, at %s:%d\n", __FILE__, __LINE__);
	if($config->{isTo_transposeMatrix} == 1) { #! then we need to transpose the matrix:
	    my @mat_tmp;
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		my @row; # = ();
		for(my $row_id = 0; $row_id < $nrows; $row_id++) {
		    push(@row, $matrix[$row_id][$col_id]); #! ie, a tranpsoed 'version'.
		}
		push(@mat_tmp, \@row);
	    }	
	    @matrix = @mat_tmp; #! ie, copy.
	    #! 
	    #! Update the 'names' and size-props:
	    my $tmp = $nrows; $nrows = $ncols; $ncols = $tmp;
	    my @tmp_ar = @arrOf_colHead; @arrOf_colHead = @arrOf_rowHead; @arrOf_rowHead = @tmp_ar;
	  #  printf("new-dim: (%u, %u), w/matrix-matrix-dism=[(%u, %u) columns=(@arrOf_rowHead), at %s:%d\n", $nrows, $ncols, scalar(@matrix), scalar(@{$matrix[0]}), __FILE__, __LINE__);
	}
    }
    # printf(STDERR "nrowS=$nrows, at %s:%d\n", __FILE__, __LINE__);
    { #! WRite out scores to LAtex-table:
	my $result_tex = $name_result . "_result.table.tex";	
	printf(STDERR "\t Export to result=\"%s\", at %s:%d\n", $result_tex, __FILE__, __LINE__);
	open(FILE_OUT_tex, ">$result_tex") or die("!!\t result(ordinary): An error in opning file=\"$result_tex\"");
	#!
	#! Iterate:
	printf(FILE_OUT_tex "#! row-name:\t");
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    if( ($col_id +1) != $ncols) {
		printf(FILE_OUT_tex "%s\t", $arrOf_colHead[$col_id]);
	    } else {
		printf(FILE_OUT_tex "%s\n", $arrOf_colHead[$col_id]);
	    }
	}
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    printf(FILE_OUT_tex "%s\t", $arrOf_rowHead[$row_id]);
	    for(my $col_id = 0; $col_id < scalar(@{$matrix[$row_id]}); $col_id++) {
		my $score = $matrix[$row_id][$col_id];
		
		# my $str_row = $arrOf_rowHead[$row_id];
		if(length($score) && ($score !~ /^([\s\-]+)|inf|nan$/i) ) { #! then we asusem the score is of itnerst.
		    printf(FILE_OUT_tex "%f", $score);
		} else {
		    printf(FILE_OUT_tex "-", $score);
		}
		if( ($col_id +1) != $ncols) {
		    printf(FILE_OUT_tex "\t");
		} else {
		    printf(FILE_OUT_tex "\n");
		}
	    }
	}
	#! 
	#! Close
	close(FILE_OUT_tex);
    }
#! Construct reulst-file-names:
    my $result_tex = $name_result . "_result.tex";
    printf(STDERR "\t Export to result=\"%s\", at %s:%d\n", $result_tex, __FILE__, __LINE__);
    open(FILE_OUT_tex, ">$result_tex") or die("!!\t result(ordinary): An error in opning file=\"$result_tex\"");
    my $result_tex_relative = $name_result . "_result.relative.tex";
    printf(STDERR "\t Export to result=\"%s\", given nrows=$nrows, at %s:%d\n", $result_tex_relative, __FILE__, __LINE__);
    open(FILE_OUT_tex_relative, ">$result_tex_relative") or die("!!\t result(relative): An error in opning file=\"$result_tex_relative\"");

    {
	my $index_color = 0; my $index_mark = 0;    #! where these are place d'inside' to give the same legned the same color.
	my $obj_columnProp = __columnsCollection__init(); #! ie, get an object ot hold summairze-proeprties wrt. column-scores.
	my $obj_columnProp_relative = __columnsCollection__init(); #! ie, get an object ot hold summairze-proeprties wrt. column-scores.
	#!
	#! Wirte out seperate for the legends:	
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my $str_measurements = ""; 
	    my $str_measurements_relative = ""; 
#	   printf("for head=\"%s\" set-cols:\n", $arrOf_rowHead[$row_id]); #, join(",", @arrOf_cols));
	    for(my $col_id = 0; $col_id < scalar(@{$matrix[$row_id]}); $col_id++) {
		my $score = $matrix[$row_id][$col_id];
		if(length($score) && ($score !~ /^([\s\-]+)|inf|nan$/i) ) { #! then we asusem the score is of itnerst.
		    if(defined($config->{thresh_maxScore_toIgnore_setToFixedValue}) && ($score > $config->{thresh_maxScore_toIgnore_setToFixedValue}) ) {
			$score = $config->{thresh_maxScore_toIgnore_setToFixedValue}; #! ie, to avodi 'clutter' int he result-plot
		    }
		    if($score > $config->{thresh_maxScore_toIgnore}) {next;} #! ie, then omit this value/case (oekseth, 06. aug. 2017).
		    if( ($config->{thresh_score_isTo_ingore0} == 1) && (0 == $score) )  {next;} #! ie, then omit this value/case (oekseth, 06. aug. 2017).
		    $str_measurements .= sprintf("(%d, %f) ", $col_id, $score);
		    #! Update the column---min-max-proerpty:		    
		    __columnsCollection__updateCell($obj_columnProp, $col_id, $row_id, $score); #! ie, in the ivnerse order.
#		    __columnsCollection__updateCell($obj_columnProp, $row_id, $col_id, $score);
#	    $str_measurements .= sprintf("(%d, %f) ", $arrOf_x_value[$col_id], $matrix[$row_id][$col_id]);
		    if(defined($matrixGlobal_prevScores) && defined($matrixGlobal_prevScores->[$row_id][$col_id]) ) {		    
			#printf("local-score[%u][%u]=%f\n", $row_id, $col_id, $matrix[$row_id][$col_id]);
			if($matrixGlobal_prevScores->[$row_id][$col_id] ne "-") {
			    if(defined($config->{thresh_maxScore_toIgnore_setToFixedValue}) && ($matrixGlobal_prevScores->[$row_id][$col_id] > $config->{thresh_maxScore_toIgnore_setToFixedValue}) ) {
				$matrixGlobal_prevScores->[$row_id][$col_id] = $config->{thresh_maxScore_toIgnore_setToFixedValue}; #! ie, to avodi 'clutter' int he result-plot
			    }
			    if($matrixGlobal_prevScores->[$row_id][$col_id] > $config->{thresh_maxScore_toIgnore}) {next;} #! ie, then omit this value/case (oekseth, 06. aug. 2017).
			    if($matrixGlobal_prevScores->[$row_id][$col_id] != 0) {
				my $score_rel = 
				    #$matrix[$row_id][$col_id] 
				    $score 
				    / $matrixGlobal_prevScores->[$row_id][$col_id];
#			    printf("score[%u][%u]=%f/%f=%f\n", $row_id, $col_id, 			       
				# 	   $score, 
				# 	   $matrixGlobal_prevScores->[$row_id][$col_id],
				# 	   $score_rel
				# );
				$str_measurements_relative .= sprintf("(%d, %f) ", $col_id, $score_rel);
				#! Update the column---min-max-proerpty:		    
				__columnsCollection__updateCell($obj_columnProp_relative, $col_id, $row_id, $score_rel); #! ie, in the ivnerse order.
#				__columnsCollection__updateCell($obj_columnProp_relative, $row_id, $col_id, $score_rel);
			    }
			}
		    }
		}
	    }
	    #!
	    #! Write out: 
	    my $color = $arrOf_colors[$index_color];
	    #   printf("[$index_color]=$color\n");
	    my $mark = $arrOf_marks[$index_mark];
	    my $str_row = $arrOf_rowHead[$row_id];
	    #printf("row[legend]=\"%s\", at %s:%d\n", $str_row, __FILE__, __LINE__);
	    my $comment_tag = "#! --------------";
	    printf(FILE_OUT_tex qq(
       \\addplot[color=$color,mark=$mark,solid] coordinates { 
             $str_measurements
       } node [left] {}; \\addlegendentry{$str_row}; 
$comment_tag));
	    if(defined($matrixGlobal_prevScores)) {
		# printf("relScores:\t %s\n", $str_measurements_relative);
	    printf(FILE_OUT_tex_relative qq(
       \\addplot[color=$color,mark=$mark,solid] coordinates { 
             $str_measurements_relative
       } node [left] {}; \\addlegendentry{$str_row}; 
$comment_tag));
	    }
	    { #! Reset the markers:
		$index_color++; $index_mark++;
		if($index_color >= scalar(@arrOf_colors)) {
		    $index_color = 0; #! ie, reset.
		}
		if($index_mark >= scalar(@arrOf_marks)) {
		    $index_mark = 0;
		}
	    }
	}
	#!
	#! Write out the column-proeprties:
	__columnsCollection__writeOut($obj_columnProp, $name_result, "direct", \@arrOf_colHead, \@arrOf_rowHead); #$arrOf_columns);
	__columnsCollection__writeOut($obj_columnProp_relative, $name_result, "relative", \@arrOf_colHead, \@arrOf_rowHead); #$arrOf_columns);
#	__columnsCollection__writeOut($obj_columnProp_relative, $name_result, "relative", $arrOf_rowHead, $arrOf_columns);
	# printf("cols=\"%s\" for input=\"$input_file\"\n", join(",", @arrOf_rowHead));
	# printf("rows=\"%s\" for input=\"$input_file\"\n", join(",", @arrOf_colHead));
#	croak("..........");
    }
#!
#! Complete: 
    close(FILE_OUT_tex);
    close(FILE_OUT_tex_relative);
    if(!defined($config)) {croak("!!\t ivnestiate this issue");}
    if( defined($matrixGlobal_prevScores) && ((scalar(@{$matrixGlobal_prevScores}) == 0) 
					      # || (scalar(@{$matrixGlobal_prevScores->[0]}) == 0) 
					       || ($config->{isTo_onlyReset_matrixGlobal_prevScores_OnResetCall} == 0) 
	)
 ) {
	$matrixGlobal_prevScores = \@matrix;
    } #! else we asusemt eh amtrix in quesiotn is used as arefere-ce-matrix.
}

sub compute_setOf_files {
    my ($arrOf_input_file, $result_directory, $base_path, $config) = @_;
    if(defined($base_path)) {
	for(my $i = 0; $i < scalar(@{$arrOf_input_file}); $i++) {
	    $arrOf_input_file->[$i] = $base_path . $arrOf_input_file->[$i];
	    printf("(sets-file)\t \"%s\", at %s:%d\n", $arrOf_input_file->[$i], __FILE__, __LINE__);
	}
    }
    if(defined($result_directory) && length($result_directory)) {
	system("mkdir -p $result_directory");
    }
    foreach my $file_name (@{$arrOf_input_file}) {
	if(-f $file_name) {
	    #!
	    #! The compute:
	    compute($file_name, $config, $result_directory);
	} else {
	    printf(STDERR "!!\t Unable to fidn file=\"%s\", ie, ingored, at %s:%d\n", $file_name, __FILE__, __LINE__);
	    #croak("investigate-this");
	}
    }
}

#! -------------------
#! 
#! 
# (1) quit unless we have the correct number of command-line args
# my $num_args = $#ARGV + 1;
# if ($num_args != 1) {
#     print "\nUsage: <script> inputFile \n";
#     exit;
# }

# # (2) we got two command line args, so assume they are the
# # first name and last name
# my $input_file = $ARGV[0];
#my $last_name=$ARGV[1];
if(1 == 2) {
    __reset_matrix();
    compute("back-err_result_C.log.tsv");
    compute("back-err_result_R.log.tsv");
# #! ---
# __reset_matrix();
#  compute("back-err_result_C.sum.tsv");
#  compute("back-err_result_R.sum.tsv");
#! ---
# __reset_matrix();
# compute("back-err_result_C.log.tsv");
# compute("back-err_result_C.sum.tsv");
# ---------
    __reset_matrix();
    compute("back-err_result_C.log.diffColSize.tsv");
    compute("back-err_result_R.log.diffColSize.tsv");
# ---------
    __reset_matrix();
    compute("back-err_result_R.log.xmtMatrixIter.tsv"); #! cosntructed from "tut__altLAng_entropy_log.R"
    compute("back-err_result_C.log.xmtMatrixIter.tsv"); #! cosntructed from "./x_measure simple-cmp-logComputeCost_narrow"
# ---------
    __reset_matrix();
    compute("back-err_result_R.entropy.xmtMatrixIter.tsv"); #! cosntructed from "tut_call_entropyPackage.R"
    compute("back-err_result_C.entropy.xmtMatrixIter.tsv"); #! cosntructed from "./x_measure entropy-time"
#compute("");
# ---------
    __reset_matrix();
    compute("sim_featureScores_euclidInside_1.tsv"); #! #! "./x_measures ccm-singleDataSet"
    __reset_matrix();
    compute("sim_featureScores_euclidInside_1_nonTranspRank.tsv");
    __reset_matrix();
    compute("sim_featureScores_euclidInside_1_rank.tsv"); #! #! "./x_measures ccm-singleDataSet"
    __reset_matrix();
    compute("sim_ccm_fixedClusterAlg_euclidInside_1.tsv"); #! #! "./x_measures ccm-singleDataSet"
    compute("sim_ccm_fixedClusterAlg_euclidInside_0.tsv"); #! #! "./x_measures ccm-singleDataSet"
__reset_matrix();
    compute("sim_ccm_fixedClusterAlg_euclidInside_1_nonTranspRank.tsv");
    compute("sim_ccm_fixedClusterAlg_euclidInside_0_nonTranspRank.tsv");
    __reset_matrix();
#compute("sim_featureScores_euclidInside_0.tsv"); #! #! "./x_measures ccm-singleDataSet"
    compute("sim_clusterAlg_fixedCCM_euclidInside_1.tsv"); #! #! "./x_measures ccm-singleDataSet"
    compute("sim_clusterAlg_fixedCCM_euclidInside_0.tsv"); #! #! "./x_measures ccm-singleDataSet"
    __reset_matrix();
    compute("sim_ccm_fixedClusterAlg_euclidInside_1.transp.tsv"); #! #! "./x_measures ccm-singleDataSet"
    compute("sim_ccm_fixedClusterAlg_euclidInside_0.transp.tsv"); #! #! "./x_measures ccm-singleDataSet"
    __reset_matrix();
#compute("sim_featureScores_euclidInside_0.tsv"); #! #! "./x_measures ccm-singleDataSet"
    compute("sim_clusterAlg_fixedCCM_euclidInside_1.transp.tsv"); #! #! "./x_measures ccm-singleDataSet"
    compute("sim_clusterAlg_fixedCCM_euclidInside_0.transp.tsv"); #! #! "./x_measures ccm-singleDataSet"
}



if(1 == 2) { #! DB-scan and kd-tree .... and algorithms in general ... wrt. simlarity-emtrics .... compare [”elow] to the 'complete' evlauation-appraoch .... eg, wrt. evalauting if [”elow] differs from the 'default' appraoches  .... 
    # FIXME[articles]: update our "sim-eval", "db-scan"-permtuatiuons and "k-emans-eval" wrt. [below] .... 
    my $base_path = "tmp_toEval/dbScan_data/";
    { #! How kd-tree and DB-scan relates/fares when compared to atlerntive clsuter-algorithms ... we compare/evlauate different data-sets wrt. min---max simliarty-metric-scores
=head artilec="sim-eval"
... we observe how the hcoise of acucrate simalrity-emtrics imrpoved predicont-accuracy with a factor greather than recently rpopseod clsuter-algorithms ... 
... we observe how the CCM-scores needs to be emausred seperately for each data-set: to use 'max/min' instead of 'max-min' (when comparign the impclation-effect of data-sets. To udnerstand/relaize the altter note that the matrix-CCMs use information of socre-difference to dineitfy/meausre the impact/-degree of between--within clsuter-aseperation-accurayc, ie, for which the concrete matrix-CCM-score difference reflects proerpteis of a data-set. Therefore a meaningful evlauation of a cluster-algorhtms prediocnt-accurayc need to consider the relative improvements, ie, in order to measure/capture the accurayc of a given clsuter-rpediocnt-reuslts. 
... From a users point-fo-viedw the high/large difference in min--max simlairty-scores 'allwos' the construciton fo new clsuter-algorithms simpliy by changing the smalrity-emtric, ie, as the disttion between cluster-algorithm and an estlibhed osftware-implemltantion-pattern is found/seen wrt. the thresholds used for idneityfing/setting the within--outside clsuter-memberhsip-trhesholds, eg, wrt. "DB-SCAN" veruss the "disjoitn-forest" software-impemtantion-pattern. 
... 
=cut
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
	"dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_simId_features.tsv",
	"dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_simId_features.tsv",    
], "results_tmp_toEval_tex/alg_difference_norm1/", $base_path);
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
	#! ---- 
	"dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_simId_features.tsv",
	"dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_simId_features.tsv",    
], "results_tmp_toEval_tex/alg_difference_norm1/", $base_path);
# 	#! ----
    __reset_matrix();
compute_setOf_files([
	#! ---- 
	"dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_features.tsv",
	"dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_features.tsv"
	#! ----     
], "results_tmp_toEval_tex/alg_difference_norm1/", $base_path);
    }
#! 
    #! *****************************************
    { #! How normalizaiton inlfuence max-CCM-scores of clsuter-algorithms .... for the iterative clsuter-algorithms we use a HCA-rpe-step (to dientify the correct number fo clsuters) .... the stnrehgt (of the altteR) cosnerns how ..... ... in our applciaotn/elvuaiton of iteriaotve clsuter-algorithms (eg, k-means-permtuations) we idneitfy "numbe-rof-clsuters' thorugh applciaotn of a pre-HCA-step (ie, in combiaotn with cCM-comptautions). For the latter task we use/call the \hpFile{cluster__kMeansTwoPass__hpLysis_api} (\hpFile{hpLysis_api.h}). Our two-pass-k-means-appraoch is novel/intersting wrt. how  .....??....
=head artilce="normalization"
... 
... 
... 
=cut
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_features.tsv",
    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_Euclid_features.tsv",
#	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_features.tsv",
], "results_tmp_toEval_tex/alg_difference_normCnt_3_ccmMax/", $base_path);
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
#	    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_8_case_ccm_max_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_8_case_ccm_max_features.tsv",
], "results_tmp_toEval_tex/alg_difference_normCnt_3_ccmMax/", $base_path);
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
	    #! ---- #! raw-non-normaziaiotn-appraoch:
#	    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_12_case_ccm_max_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_12_case_ccm_max_features.tsv",
		    ], "results_tmp_toEval_tex/alg_difference_normCnt_3_ccmMax/", $base_path);
    }
    { #! How normalizaiton inlfuence Min CCM-scores of clsuter-algorithms .... Difference in k-means-score seperately for each .... 
=head artile=["normalization"]:
... 
... 
... 
=cut
=head artile=["db-scan", "kd-tree", "db-scan-eval", "cluster-lib", "ccm-lib"] ... where a subset is used ... (eg, wrt. combinin min--max-scores into a combiend figure).
... 
... 
... 
=cut
# 	#! -------------------------
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
	    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_features.tsv",
  ],  "results_tmp_toEval_tex/alg_difference_normCnt_3_ccmMin/", $base_path);
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_features.tsv",
	    #! ---- #! column-centered normalizaiton
#	    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_8_case_ccm_min_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_8_case_ccm_min_features.tsv",
  ],  "results_tmp_toEval_tex/alg_difference_normCnt_3_ccmMin/", $base_path);
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files([
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_features.tsv",
	    #! ---- #! raw-non-normaziaiotn-appraoch:
#	    "dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_12_case_ccm_min_features.tsv",
	    "dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_12_case_ccm_min_features.tsv",
	    #! ---- 
  ],  "results_tmp_toEval_tex/alg_difference_normCnt_3_ccmMin/", $base_path);
    }
}
if(1 == 2) { #! artilce=["eval-sim", "k-means-wheels", "", "", ""]:
    #!
    #! *****************************************
    { #! ... how cluster-algorithsm differes wrt. the use/applcioant of accurate silairty-emtircs .... we observe/infer how the prediciton-difference between differnet clsuter-algorithms is fixed/cosntant, eg, as observed wrt. the STD-differnece ofrr the 100+ evlauated real-life data-sets .... the latter observaiotn cleary implies/indicates that ......??.... 
=head
.... evalaute difference wrt. different cluster-alorithm-configuraitons .... 
... capture overall patterns in data-sets 
... idneitfy variance wrt. different configuraiton-options .... 
... we observe how the infleucne of simlairty-emtircs is cosnsitent across all data-sets (eg, as observed wrt. the aprpxoiimate consntat size of STD and skewness). 
... 
... 
=cut
	my @arrOf_pref = ("dataRealMine", "dataRealKT");
	{#! max
	    my $base_path = "tmp_toEval/algEval_deviation_realLife_ccm_max/";
	    my $file_prefix = "_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_";	
	    my @arrOf_file_midfix = ("_case_ccm_max_deviations.tsv");
	    foreach  my $id (@arrOf_pref) {
# 	#! -------------------------
		__reset_matrix();
		foreach my $file_midfix (@arrOf_file_midfix) {
		    for(my $norm_id = 0; $norm_id <= 12; $norm_id++) {
			my $file_name = $base_path . $id . $file_prefix . $norm_id . $file_midfix; # . $norm_id . ".tsv";
			if(-f $file_name) { #! then the file exists:
			    #! 
			    #! Write out the results: 
			    my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
			    my $result_directory = "results_tmp_toEval_tex/alg_summary_variance_norm_max/";
			    compute($file_name, $config, $result_directory);
			}
		    }
		}
	    }
	}
	{ #! min:
	    my $base_path = "tmp_toEval/algEval_deviation_realLife_ccm_min/";
	    my $file_prefix = "_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_";	
=head

=cut
	    my @arrOf_file_midfix = ("_case_ccm_min_deviations.tsv");
	    foreach  my $id (@arrOf_pref) {
# 	#! -------------------------
		__reset_matrix();
		foreach my $file_midfix (@arrOf_file_midfix) {
		    for(my $norm_id = 0; $norm_id <= 12; $norm_id++) {
			my $file_name = $base_path . $id . $file_prefix . $norm_id . $file_midfix; # . $norm_id . ".tsv";
			if(-f $file_name) { #! then the file exists:
			    #! 
			    #! Write out the results: 
			    my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
			    my $result_directory = "results_tmp_toEval_tex/alg_summary_variance_norm_min/";
			    compute($file_name, $config, $result_directory);
			}
		    }
		}
	    }
	}
    }
}

if(1 == 2) {  #! artilce="":
=head


Figures:
(1) input-data: a small esy-to-itnerpret data-set .... to conter-argue views/standpoints asserting that different in metrics are onlye valid/interesting for large/compelx data-sets;
(2) gold-standard-CCM: we compare different permtuations to a HCA-based comparison-appraoch .... thereofre the gold-stand-CCM-scores captures/describes the variance wrt. a referecne-cluster-distribution. From the sub-figures we observe/derive how .... ;
(3) fixed-normalization: a comparison of different cluster-algorithms ... ;
() : ;
() : ;
() : ;

=cut
    #! 
    my $base_path = "tmp_toEval/iris_differentCCM_inConvergence/";
    #! 
    #! *****************************************
    { #! .... a real-life data-set we use to simplify the coneptual vlidation-comparison ..... when the data-set is comarped to our 'syneatc ensamble' of dat-aset we observe how the "fish-growht" data-set .....
	my @arrOf = (
	    "tests/data/kt_mine/fish_growth.tsv",
    );
#! 
#! Write out the results: 
	my $base_path = "";
# 	#! -------------------------
    __reset_matrix();
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataSmall_seperateAlg_input/", $base_path);
    }
    #! 
    #! *****************************************
    { #! gold-standard-CCM:: Comapre kd-tree and DB-scan: 	
=head

=cut
	my @arrOf = (
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_12.tsv",
	    #! ---- 
	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRand_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRand_norm_12.tsv",
	    #! ---- 
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_12.tsv",
	    );
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataSmall_seperateAlg_differentNorm_goldCCM/", $base_path, $config);
    }
    #! 
    #! *****************************************
    { #! fixed-normalization: ... Differnet permtautions of clsuter-algorithms ... the inflcuence of using asimalirty-emtrics both before and after comptautions of clusters .... 
=head
... compare the accurayc-imapct of different data-distribtiiosn, ie, where each simalrity-meitrc permtue/change the data-distribiotn .... From the figures we observe/infer how ..... 
... 
... 
... 
... 
... 
=cut
	my @arrOf = (
	    "./result_tutEval_1_data_0__ccmGold_randomWorst_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRandSimDuring_norm_1.tsv",
#	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRandSimBefore_norm_1.tsv",
	    #! ---
	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRandSimBoth_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmMatrix_hcaCCMsSilRandSimDuring_norm_1.tsv",
	    #! ---
#	    "result_tutEval_1_data_0__ccmMatrix_hcaCCMsSilsRandSimBoth_norm_1.tsv",	    
	    "result_tutEval_1_data_0__ccmMatrix_hcaCCMsDunnsRand_norm_1.tsv",
	    );
	#! 
	#! Write out the results: matrix-ccm
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataSmall_seperateAlg_norm1_matrixCCM/", $base_path, $config);	
=head
... 
... 
... 
=cut
	my @arrOf_tmp;
	foreach my $name (@arrOf) {
	    $name =~ s/ccmMatrix/ccmGold/g; #! ie, to 'use' the gold-standard-data-set-matrix:
	    push(@arrOf_tmp, $name);
	}
	#!
	#! Call: Write out the results: gold-ccm:
#my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf_tmp, "results_tmp_toEval_tex/dataSmall_seperateAlg_norm1_goldCCM/", $base_path, $config);	
    }
}


if(1 == 2) { #! artilce="":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "tmp_toEval/algEval_norm_0_minMax_ccm/";
=head

=cut
	my @arrOf = split("\n", qq(
));
	#! 
	#! Write out the results: 
    }
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "tmp_toEval/algEval_norm_0_minMax_ccm/";
=head

=cut
	my @arrOf = split("\n", qq(dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_features.tsv
));
	#! 
	#! Write out the results: 
    }
    #! 
    #! *****************************************
    { #! ... simalrity-emtric ...
	my $base_path = "tmp_toEval/algEval_norm_0_minMax_ccm_normCases/";
=head

=cut
	my @arrOf = split("\n", qq(dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features_avgNormalization.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_features_avgNormalization.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_2_case_ccm_max_features_avgNormalization.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_3_case_ccm_max_features_avgNormalization.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_4_case_ccm_max_features_avgNormalization.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_5_case_ccm_max_features_avgNormalization.tsv
));
	#! 
	#! Write out the results: 
    }
    #! 
    #! *****************************************
    { #! ... simalrity-emtric  ...
	my $base_path = "tmp_toEval/algEval_norm_0_minMax_simId/";
=head

=cut
	my @arrOf = split("\n", qq(
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_2_case_ccm_max_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_2_case_ccm_min_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_3_case_ccm_max_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_3_case_ccm_min_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_4_case_ccm_max_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_4_case_ccm_min_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_5_case_ccm_max_simId_features.tsv
dataRealKT_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_5_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_10_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_10_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_11_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_11_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_12_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_12_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_1_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_2_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_2_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_3_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_3_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_4_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_4_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_5_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_5_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_6_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_6_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_7_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_7_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_8_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_8_case_ccm_min_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_9_case_ccm_max_simId_features.tsv
dataRealMine_tut_whyResultsDiffershcaAndKMeans_simMetricResult_summary_norm_9_case_ccm_min_simId_features.tsv
));
	#! 
	#! Write out the results: 
    }
}


if(1 == 2) { #! artilce="": .... normalization .... 
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "tmp_toEval/80DataSets__vincentarelbundock/";
=head

=cut
#!
#!
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; $config->{isTo_onlyReset_matrixGlobal_prevScores_OnResetCall} = 1;  $config->{thresh_maxScore_toIgnore} = 100;
#!
#!
# ---------
{
__reset_matrix();
	my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_disjoint_ccm_dbSCAN_Silhoutette_euclid.tsv
tut_80DataSets__vincentarelbundock_disjoint__kdTree__CCM_Silhoutette_euclid.tsv
tut_80DataSets__vincentarelbundock_hca_single_Silhoutette_euclid.tsv
)); 	#! 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
# ---------
__reset_matrix();
{
	#! Write out the results: 
my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_kdDynamic_DBI(Euc)_minkowski_euclid.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_DBI(Euc)_absoluteDifference_Canberra.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_DBI(Euc)_MINE_mic.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_DBI(Euc)_minkowski_cityblock.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_DBI(Euc)_rank_kendall_coVariance.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_DBI(Euc)_squared_Pearson.tsv
)); 	#! 
# ---------
__reset_matrix();
	#! Write out the results: 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
{
my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_kdDynamic_Dunn_minkowski_euclid.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Dunn_absoluteDifference_Canberra.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Dunn_MINE_mic.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Dunn_minkowski_cityblock.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Dunn_rank_kendall_coVariance.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Dunn_squared_Pearson.tsv
)); 	#! 
# ---------
__reset_matrix();
	#! Write out the results: 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
{
	my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_minkowski_euclid.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_absoluteDifference_Canberra.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_MINE_mic.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_minkowski_cityblock.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_rank_kendall_coVariance.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_squared_Pearson.tsv
)); 	#! 
# ---------
__reset_matrix();
	#! Write out the results: 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
{
	my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_kdDynamic_SSE_minkowski_euclid.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_SSE_absoluteDifference_Canberra.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_SSE_MINE_mic.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_SSE_minkowski_cityblock.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_SSE_rank_kendall_coVariance.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_SSE_squared_Pearson.tsv
)); 	#! 
# ---------
__reset_matrix();
	#! Write out the results: 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
{
	my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_kdDynamic_VNND_minkowski_euclid.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VNND_absoluteDifference_Canberra.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VNND_MINE_mic.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VNND_minkowski_cityblock.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VNND_rank_kendall_coVariance.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VNND_squared_Pearson.tsv
)); 	#! 
# ---------
__reset_matrix();
	#! Write out the results: 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
{
	my @arrOf = split("\n", qq(
tut_80DataSets__vincentarelbundock_kdDynamic_VRC_minkowski_euclid.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VRC_absoluteDifference_Canberra.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VRC_MINE_mic.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VRC_minkowski_cityblock.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VRC_rank_kendall_coVariance.tsv
tut_80DataSets__vincentarelbundock_kdDynamic_VRC_squared_Pearson.tsv
	    ));
	#! 
	#! Write out the results: 
	#! 
	#! Write out the results: 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/norm_category_fixedAlgSim/", $base_path, $config);
}
    }

    #! 
    #! *****************************************
    { #! ... 
=head

=cut
	my @arrOf = (
	    "",
	    "",
	    "",
	    "",
	    );
	#! 
	#! Write out the results: 
    }
}

if(1 == 2) { #! artilce="ccm":
    #! 
    #! *****************************************
    {
	my $base_path = "tmp_toEval/iris_differentCCM_inConvergence/";
=head
... differnet clsuter-algorithsm for the same data-normaizoant and same CCM-quality-metric
=cut
	my @arrOf = (
	    "result_tutEval_1_data_0__ccmMatrix_dbScanBruteCCMsDunnsRand_norm_1.tsv", 
	    "result_tutEval_1_data_0__ccmMatrix_hcaCCMsDunnsRand_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmMatrix_dbScanDirectCCMsDunnsRand_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsDunnsRand_norm_1.tsv"
    );
#! 
#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataSmall_differentCCM_inConvergence_differentAlg/", $base_path, $config);	
    }
    { #! Permtuations of kd-tree-appraoches  .... we observe how two differetn Rand-CCM-interpretiations clearly infleucne prediocnt-accurayc 
	my $base_path = "tmp_toEval/iris_differentCCM_inConvergence/";
=head

=cut
	my @arrOf = (
	    "result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRand_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmMatrix_dbScanBruteCCMsSilRand_norm_1.tsv",
	    #! ---- 
	    "result_tutEval_1_data_0__ccmGold_dbScanKdCCMsSilRand_norm_12.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanKdCCMsSilRand2_norm_12.tsv",
	    #! ---- 
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand_norm_12.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_12.tsv",
	    #! ---- 	    
	    );
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataSmall_differentCCM_inConvergence_norm1_12_case1/", $base_path, $config);	
    }
    #! 
    #! *****************************************
    { #! 
=head

=cut
	my $base_path = "tmp_toEval/iris_differentCCM_inConvergence/";
	my @arrOf = (
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_1.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanKdCCMsDunnsRand_norm_1.tsv",
	    #! ---------
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_2.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanKdCCMsDunnsRand_norm_2.tsv",
	    #! ---------
	    "result_tutEval_1_data_0__ccmGold_dbScanBruteCCMsSilRand2_norm_12.tsv",
	    "result_tutEval_1_data_0__ccmGold_dbScanKdCCMsDunnsRand_norm_12.tsv",
	    );
	#! 
	#! Write out the results: 
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataSmall_differentCCM_inConvergence_norm1_12_case2/", $base_path, $config);	
    }
}

#! 
#! *****************************************
if(1 == 2) { #! ... Differnet permtautions of clsuter-algorithms ... 
=head
... 
... 
... 
=cut
{
    my @arrOf = (
	# "./result_tutEval_1_data_0__ccmGold_randomWorst_norm_1.tsv",
	#! ---
	"./result_tutEval_1_data_0__ccmMatrix_disjointCCMsSilRandSimDuring_norm_1.tsv",
	    "./result_tutEval_1_data_0__ccmMatrix_dbScanBruteCCMsSilRandSimDuring_norm_1.tsv",
	    #! ---
	    "./result_tutEval_1_data_0__ccmMatrix_dbScanBruteCCMsSilRandSimBefore_norm_1.tsv",
	    "./result_tutEval_1_data_0__ccmMatrix_dbScanBruteCCMsSilRandSimBoth_norm_1.tsv",
	    #! ---
	    "./result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRandSimDuring_norm_1.tsv",
	    "./result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRandSimBefore_norm_1.tsv",
	    # #! ---
	    # "./result_tutEval_1_data_0__ccmMatrix_dbScanKdCCMsSilRandSimBoth_norm_1.tsv",
	# "./result_tutEval_1_data_0__ccmMatrix_hcaCCMsSilRandSimDuring_norm_1.tsv",
	# #! ---
	# "./result_tutEval_1_data_0__ccmMatrix_hcaCCMsSilsRandSimBoth_norm_1.tsv",	    
	# "./result_tutEval_1_data_0__ccmMatrix_hcaCCMsDunnsRand_norm_1.tsv",
	);
    #! 
    #! Write out the results: matrix-ccm
    
    #! 
    #! Write out the results: gold-ccm:
    my @arrOf_tmp;
    foreach my $name (@arrOf) {
	$name =~ s/ccmMatrix/ccmGold/g; #! ie, to 'use' the gold-standard-data-set-matrix:
    }
    #!
    #! Call:
}
}


if(1 == 2) { #! artilce="":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ... where we in [below] evalaute for the "inside" option ..... different algorithms .... nraomziaotns-trategies ..... 
	my $base_path = "tmp_toEval/single_simCat/";
=head

=cut
	my @arrOf = (
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_12_simAppType1.tsv",
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_1_simAppType1.tsv",
	    #! --- 
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_dbScanKdCCMsSilRandSimDuring_norm_12_simAppType1.tsv",
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_dbScanKdCCMsSilRandSimDuring_norm_1_simAppType1.tsv",
	    #! --- 
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_12_simAppType1.tsv",
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_1_simAppType1.tsv",
	    );
	# FIXME: merge [ªbove] into one file .... where the file cpatures/rpesentes the IRIS-data-set .... where each row-name reprensts/describes the given file 
	#! 
	#! Write out the results: 
    }
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ... 
	my $base_path = "tmp_toEval/single_simCCM/";
=head

=cut
	my @arrOf = (
	    #! --- 
	    "result_tutEval_3_single_simCCM_ccmMatrix_ccm_dbScanKdCCMsSilRandSimBoth_norm_1_simAppType2.tsv",
	    "result_tutEval_3_single_simCCM_ccmMatrix_ccm_dbScanKdCCMsSilRandSimBoth_norm_12_simAppType2.tsv",
	    #! --- 
	    "result_tutEval_3_single_simCCM_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_12_simAppType1.tsv",
	    "result_tutEval_3_single_simCCM_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_12_simAppType2.tsv",
	    #! --- 
	    "result_tutEval_3_single_simCCM_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_1_simAppType1.tsv",
	    "result_tutEval_3_single_simCCM_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_1_simAppType2.tsv",
	    #! --- 
	    );
	#! 
	#! Write out the results: 
    }
    #!
    #! *****************************************
    { #! ... for data-sets with same degree of normalizaiton .... how simalirty-metircs are influenced by ..... 
        my $base_path = "tmp_toEval/norm_0_ccmMatrix_simAppType/";
=head

=cut
	my @arrOf = split("\n", qq(result_tutEval_3_single_dataSim_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimBefore_norm_1_simAppType0.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimBoth_norm_1_simAppType2.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_1_simAppType1.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_dbScanKdCCMsSilRandSimBefore_norm_1_simAppType0.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_dbScanKdCCMsSilRandSimBoth_norm_1_simAppType2.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_dbScanKdCCMsSilRandSimDuring_norm_1_simAppType1.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_disjointCCMsSilRandSimDuring_norm_1_simAppType0.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_1_simAppType1.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_hcaCCMsDunnsRand_norm_1_simAppType2.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_hcaCCMsSilRandSimDuring_norm_1_simAppType1.tsv
result_tutEval_3_single_dataSim_ccmMatrix_ccm_hcaCCMsSilsRandSimBoth_norm_1_simAppType2.tsv
result_tutKd3_dataSim_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_1_simAppType1.tsv
result_tutKd3_dataSim_ccmMatrix_ccm_disjointCCMsSilRandSimDuring_norm_1_simAppType0.tsv));
	#! 
	# FIXME: merge [ªbove] into one file .... where the file cpatures/rpesentes the IRIS-data-set .... where each row-name reprensts/describes the given file  .... 
	#! Write out the results ... 
    }
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ... 
	my $base_path = "tmp_toEval/";
=head

=cut
	my @arrOf = (
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_hcaCCMsSilRandSimDuring_norm_12_simAppType1.tsv",
"result_tutEval_3_single_dataSimCatSmall_ccmMatrix_ccm_hcaCCMsSilRandSimDuring_norm_1_simAppType1.tsv",
	    "",
	    "",
	    "",
	    "",
	    );
	#! 
	#! Write out the results: 
    }
    #! 
    #! *****************************************
    { #! ... infleucne of dat-anroamzioant ... differne tpaproahces ..... evaluate the perofmrance-imapct seperately for different use-cases ... Simalirty-metirc-score-spread for the same data-set .... different pariwise simalirty-emtics .... we observe how the simalrity-emtric-pattern is disticntely different (for the same data-set), hence the cofngiruation-senitive algrotihsm such as distjoint-forest-permtautiosn (eg, DB-SCAN-flavours) are highly senitivte wrt. prediocnt-accurayc .... 
	my $base_path = "tmp_toEval/single_ccmMatrix/" ;
=head

=cut
	my @arrOf_pref = ("dbScanBruteCCMsSilRandSimDuring", "dbScanBruteCCMsSilRandSimBefore", "dbScanBruteCCMsSilRandSimBoth", "dbScanKdCCMsSilRandSimDuring", "dbScanKdCCMsSilRandSimBefore", "dbScanKdCCMsSilRandSimBoth");
	foreach  my $id (@arrOf_pref) {
	    for(my $norm_id = 0; $norm_id <= 12; $norm_id++) {
		my $file_name = $base_path . "result_tutEval_1_summary_ccmMatrix_ccm_" . $id . "_norm_" . $norm_id . ".tsv";
		if(-f $file_name) { #! then the file exists:
		    # FIXME: first merge [above] .... then .....
		    ; 
		} 
	    }
	    #! 
	    #! Write out the results: 
	    
	}
    }
}

if(1 == 2) { #! artilce="": 
    #!
    #! *****************************************
    { #! ... 
	my $base_path = "tmp_toEval/single_ccmMatrix/" ;
	my $file_prefix = "realLife_1_eachDataSet_ccmMatrix_ccmSim/tut_evalKd__realLife_1_isToUseHCAMINE_"; #! eg, with suffix="guinePigs".
	my $file_midfix = "_ccmMatrix_dbScanKdCCMsSilRand_norm_"; #! eg, with suffisz="8.tsv".
=head

 .... infleucne of dat-anroamzioant for different data-sets 

=cut
	my @arrOf_pref = ("guinePigs", "limesOxygen", "spottingGrade_2", "weedLength", "birthWeight", "butterFat", "fishGrowth", "gallsThorax");
	foreach  my $id (@arrOf_pref) {
	    for(my $norm_id = 0; $norm_id <= 12; $norm_id++) {
		my $file_name = $file_prefix . $id . $file_midfix . $norm_id . ".tsv";
		if(-f $file_name) { #! then the file exists:
		    #! 
		    #! Write out the results: 
		    ;
		}
	    }
	}
    }
}




if(1 == 2) { #! artilce="":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "tmp_toEval/realLife_1_summary/";
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; $config->{isTo_onlyReset_matrixGlobal_prevScores_OnResetCall} = 1;
	{
	    my @arrOf = (
		"tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanBruteCCMsSilRand2_norm_12.tsv",
		"tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanBruteCCMsSilRand_norm_12.tsv",
		"tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanDirectCCMsSilRand2_norm_12.tsv",
		"tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanDirectCCMsSilRand_norm_12.tsv",
		"tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_12.tsv",
		"tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand_norm_12.tsv",
		);
	    #! 
	    #! Write out the results: 
	    __reset_matrix();
	    compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/realLife_1_summary_ccmGold_ccm/", $base_path, $config);	
	}
	{ #! Influence of data-nraomziaont on KD-tree
	    my $base_path = "";
	    my @arrOf = (
"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_0.tsv",
"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_1.tsv",
"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_2.tsv",
"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_3.tsv",
"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_4.tsv",
"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmGold_ccm_dbScanKdCCMsSilRand2_norm_12.tsv",
		);
	    #! 
	    #! Write out the results: 
	    __reset_matrix();
	    compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/realLife_1_summary_ccmGold_ccm_KD_sil/", $base_path, $config);	
	}
	{
	    my $base_path = "";
	    my @arrOf = (
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanBruteCCMsSilRand2_norm_12.tsv",
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanBruteCCMsSilRand_norm_12.tsv",
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanDirectCCMsSilRand2_norm_12.tsv",
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanKdCCMsSilRand_norm_12.tsv",
		);
	    #! 
	    #! Write out the results: 
	    my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
	    __reset_matrix();
	    compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/realLife_1_summary_ccmMatrix_ccm_differentAlg_norm12/", $base_path, $config);	
	}
	{
	    my $base_path = "";
	    my @arrOf = (
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanBruteCCMsSilRand2_norm_1.tsv",
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanBruteCCMsSilRand_norm_1.tsv",
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanDirectCCMsSilRand2_norm_1.tsv",
		"tmp_toEval/realLife_1_summary/tut_evalKd__realLife_1_isToUseHCAMINE_summary_ccmMatrix_ccm_dbScanKdCCMsSilRand_norm_1.tsv",
		);
	    #! 
	    #! Write out the results: 
	    my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
	    __reset_matrix();
	    compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/realLife_1_summary_ccmMatrix_ccm_differentAlg_norm1/", $base_path, $config);	
	}
	if(1 == 2)
	{
	    my @arrOf = (
		"",
		"",
		"",
		);
	    #! 
	    #! Write out the results: 
	    my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
	    __reset_matrix();
	    compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/realLife_1_summary_ccmGold_ccm_KD", $base_path, $config);	
	}
	{
	    my @arrOf = (
		"",
		"",
		"",
		);
	    #! 
	    #! Write out the results: 
	    my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
	    __reset_matrix();
	    compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/realLife_1_summary_ccmGold_ccm_KD", $base_path, $config);	
	}
    }
}
if(1 == 2) { #! artilce="sim::eval":
    #! 
    #! *****************************************
    my $base_path = "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/";
    { #! ... simalrity-emtric=[before, inside, both] ...
	my @arrOf = (
	    "dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_Euclid_features.tsv",
	    "dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features.tsv",
	    "dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_features.tsv",
	    "dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_simId_features.tsv",
	    "dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_simId_features.tsv",
	    );
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/dataRealKT_tut_whyResultsDiffers_all_norm_0/", $base_path, $config);	
    }
}
if(1 == 2) { #! artilce="":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "";
	my @arrOf = (
	    "",
	    "",
	    "",
	    "",
	    );
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/", $base_path, $config);	
    }
}
if(1 == 2) { #! then we write out data generated from our "x_measures tut_norm_3_cluster_kd_dbScan_80DataSets ("tut_norm_3_cluster_kd_dbScan_80DataSets.c") (oekseth, 06. jul. 2017).
    compute("tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_minkowski_euclid.tsv"); #! #! "./x_measures ccm-singleDataSet"
}
if(1 == 2) { #! artilce="supercomputers-JCM---":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "";
	my @arrOf = (
	    "vilje_overhead_raw_goldXgold.tsv",
	    "vilje_overhead_raw_ccmMatrix_hyp.tsv",
	    #! --- 
	    "vilje_overhead_Euclid_goldXgold.tsv",
	    "vilje_overhead_Euclid_ccmMatrix_hyp.tsv",
	    #! --- 
	    "vilje_overhead_Kendall_coVariance_goldXgold.tsv",
	    "vilje_overhead_Kendall_coVariance_ccmMatrix_hyp.tsv",
	    #! --- 
	    "vilje_overhead_absDiff_Sorensen_goldXgold.tsv",
	    "vilje_overhead_absDiff_Sorensen_ccmMatrix_hyp.tsv",
	    );
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; 
compute_setOf_files(\@arrOf, "results_tmp_toEval_tex_superComp/", $base_path, $config);	
    }
}
if(1 == 2) { #! artilce="syntetic-query-times---":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "";
	my @arrOf = (
	    #! --- 
	    "result_eval_dbEngine/data2__searchIsIncremental_data_search.tsv", 
	    "result_eval_dbEngine/data2__searchIsRandom_data_search.tsv", 
	    );
	#! 
	#! Write out the results: 
my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; 
compute_setOf_files(\@arrOf, "results_tmp_queryTime_synt/", $base_path, $config);	
    }
}
if(1 == 2) { #! artilce="Klatremus--SKB---":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "";
	my @arrOf = (
	    #! --- 
	    "result_eval_klatremusCont/mat_pred_ntc_allDistances.tsv", 
	    "result_eval_klatremusCont/mat_pred_ntc_atDist_1.tsv", 
	    #! --- 
	    "result_eval_klatremusCont/mat_pred_vertexDB_allDistances_allDist.tsv", 
	    "result_eval_klatremusCont/mat_pred_vertexDB_allDistances_ge2.tsv", 
	    #! --- 
	    "result_eval_klatremusCont/mat_relDB_to_rt_countAll.tsv", 
	    "result_eval_klatremusCont/mat_relDB_to_rt_count_distRange_1_1.tsv", 
	    #! --- 
	    "result_eval_klatremusCont/mat_rt_count_atInfDist_ge2.tsv", 
	    "result_eval_klatremusCont/mat_rt_count_atInfDist.tsv", 
	    );
	#! 
	#! Write out the results: 
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; 
$config->{thresh_maxScore_toIgnore} = 1000000;  
compute_setOf_files(\@arrOf, "results_tmp_skb_reactomeDataSet/", $base_path, $config);	
    }
}
if(1 == 2) { #! artilce="Klatremus--SKB---":
    #! 
    #! *****************************************
    { #! ... simalrity-emtric=[before, inside, both] ...
	my $base_path = "";
	my @arrOf = (
	    #! --- 
	    # "result_eval_klatremusCont/mat_pred_ntc_allDistances.tsv", 
	    # "result_eval_klatremusCont/mat_pred_ntc_atDist_1.tsv", 
	    # #! --- 
	    # "result_eval_klatremusCont/mat_pred_vertexDB_allDistances_allDist.tsv", 
	    #    "result_eval_klatremusCont/mat_pred_vertexDB_allDistances_ge2.tsv", 
	    # #! --- 
	    # "result_eval_klatremusCont/mat_relDB_to_rt_countAll.tsv", 
	    # "result_eval_klatremusCont/mat_relDB_to_rt_count_distRange_1_1.tsv", 
	    #! --- 
	    "result_eval_klatremusCont/mat_rt_count_atInfDist_ge2.tsv", 
	    "result_eval_klatremusCont/mat_rt_count_atInfDist.tsv", 
	    );
	#! 
	#! Write out the results: 
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1; $config->{thresh_maxScore_toIgnore} = 1000000;  
compute_setOf_files(\@arrOf, "results_tmp_skb_reactomeDataSet_trans/", $base_path, $config);	
    }
}
if(1 == 2) {
{
  my $base_path = "";
    my @arrOf = (
	__readFile__unifyFiles__sameRowIDS__initEach("Euclid", "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_Euclid_features.tsv"),
	__readFile__unifyFiles__sameRowIDS__initEach("all-min", "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_features.tsv"), 
	__readFile__unifyFiles__sameRowIDS__initEach("disjoint-KD:min", "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_min_features.tsv", "disjoint__kdTree"), 
	__readFile__unifyFiles__sameRowIDS__initEach("all-max", "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features.tsv"),
	__readFile__unifyFiles__sameRowIDS__initEach("disjoint-KD", "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features.tsv", "disjoint__kdTree"),
	__readFile__unifyFiles__sameRowIDS__initEach("disjoint-KD-alt2", "tmp_toEval/dataRealKT_tut_whyResultsDiffers_all_norm_0/dataRealKT_tut_whyResultsDiffers_all_hcaAndKMeans_simMetricResult_summary_norm_0_case_ccm_max_features.tsv", "disjoint__kdTree__CCM"),
	# __readFile__unifyFiles__sameRowIDS__initEach("", ""),
	# __readFile__unifyFiles__sameRowIDS__initEach("", ""),
	# __readFile__unifyFiles__sameRowIDS__initEach("", ""),
   	);
   my $isTo_transposeMatrix = 1; my $isTo_takeRelate_lastColumn = 0;
   my ($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols) = __readFile__unifyFiles__sameRowIDS(\@arrOf, $isTo_transposeMatrix, $isTo_takeRelate_lastColumn);
   {
       my $config = initConfig__fileRead__setData($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols);
       $config->{thresh_score_isTo_ingore0} = 0;  
       $config->{thresh_maxScore_toIgnore} = 100000000;  
       compute(undef, $config, "results_tmp_all_normNone");
   }
   { #! then a tranpsoed veirosn of the output:
       my $config = initConfig__fileRead__setData($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols);
       $config->{thresh_score_isTo_ingore0} = 0;  
       $config->{thresh_maxScore_toIgnore} = 100000000;  
       $config->{thresh_maxScore_toIgnore_setToFixedValue} = 1000;   #! ie, the max-size
       $config->{isTo_transposeMatrix} = 1;
       compute(undef, $config, "results_tmp_all_normNone_transp");
   }
}
if(1 == 2)
{
    #! Note: idneitfy the effects of confugriaotns by combining perspectives of data-normalizaiton and simlairty-emtrics. Used to illsutrate the infleuce of accurate simalirty-emtircs wrt. applciaotni/evlauation oif KD-trees. 
   my $base_path = "";
    my @arrOf = (
	__readFile__unifyFiles__sameRowIDS__initEach("Cityblock", "tmp_toEval/80DataSets__vincentarelbundock/tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_minkowski_cityblock.tsv"),
   	__readFile__unifyFiles__sameRowIDS__initEach("Euclid",	"tmp_toEval/80DataSets__vincentarelbundock/tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_minkowski_euclid.tsv"),
   	__readFile__unifyFiles__sameRowIDS__initEach("Pearson",    	"tmp_toEval/80DataSets__vincentarelbundock/tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_squared_Pearson.tsv"),
   	__readFile__unifyFiles__sameRowIDS__initEach("Kendall",    	"tmp_toEval/80DataSets__vincentarelbundock/tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_rank_kendall_coVariance.tsv"),
   	__readFile__unifyFiles__sameRowIDS__initEach("Canberra", "tmp_toEval/80DataSets__vincentarelbundock/tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_absoluteDifference_Canberra.tsv"),
	__readFile__unifyFiles__sameRowIDS__initEach("MINE", "tmp_toEval/80DataSets__vincentarelbundock/tut_80DataSets__vincentarelbundock_kdDynamic_Silhouette_MINE_mic.tsv"),
   	);
   my $isTo_transposeMatrix = 1; my $isTo_takeRelate_lastColumn = 0;
   my ($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols) = __readFile__unifyFiles__sameRowIDS(\@arrOf, $isTo_transposeMatrix, $isTo_takeRelate_lastColumn);
   {
       my $config = initConfig__fileRead__setData($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols);
       $config->{thresh_maxScore_toIgnore} = 100000000;  
 #      compute_setOf_files(\@arrOf, "results_tmp_tut_time_2_synt/", $base_path, $config);	
   }
   { #! then a tranpsoed veirosn of the output:
       my $config = initConfig__fileRead__setData($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols);
#       $config->{thresh_maxScore_toIgnore} = 1000;  
       $config->{thresh_maxScore_toIgnore} = 100000000;  
       $config->{isTo_transposeMatrix} = 1;
#compute_setOf_files(\@arrOf, "results_tmp_tut_time_2_real/", $base_path, $config);	
   }
#   compute_setOf_files(\@arrOf, "results_tmp_skb_reactomeDataSet/", $base_path, $config);	
   #  #! 
   #  #! Write out the results: 
   #  my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0;
   # $config->{obj_input} 
   #  compute_setOf_files(\@arrOf, "results_tmp_toEval_tex/", $base_path, $config);	
}
}
if(1 == 2) 
{ 
if(1 == 2)
  { #! then a tranpsoed veirosn of the output:
      my $base_path = "";
      my @arrOf = (
	  "results/tut_time_2__synt/tut_time_2_linear_1_100_minMax_valueSplitCase_0_clusterCountCase_0_ccm_matrix_config.tsv_time.tsv"
	  );
  	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; 
      # $config->{thresh_score_isTo_ingore0} = 0;  
      # $config->{thresh_maxScore_toIgnore} = 100000000;  
      #$config->{thresh_maxScore_toIgnore_setToFixedValue} = 1000;   #! ie, the max-size
       $config->{isTo_transposeMatrix} = 0;
compute_setOf_files(\@arrOf, "results_tmp_tut_time_2_synt/", $base_path, $config);	
#       compute(undef, $config, "results/tut_time_2__synt_afterEval");
   }
#if(0 == 1)
  { #! then a tranpsoed veirosn of the output:
      my $base_path = "";
      my @arrOf = (
	  #"results/tut_time_2__realLife/tut_evalKdSim3_eachEnsamble_vincentarelbundock_linear_1_100_minMax_valueSplitCase_0_clusterCountCase_0_ccm_matrix_config.tsv_time.tsv"
	  __readFile__convertTo_relativeFirstLine__writeOut("results/tut_time_2__realLife/tut_evalKdSim3_eachEnsamble_vincentarelbundock_linear_1_100_minMax_valueSplitCase_0_clusterCountCase_0_ccm_matrix_config.tsv_time.tsv")
	  );
  	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; 
#       my $config = initConfig__fileRead__setData($mat_ref, $arr_rowHead, $arr_colHead, $nrows, $ncols);
#       $config->{thresh_score_isTo_ingore0} = 0;  
#       $config->{thresh_maxScore_toIgnore} = 100000000;  
#       $config->{thresh_maxScore_toIgnore_setToFixedValue} = 1000;   #! ie, the max-size
      #$config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "results_tmp_tut_time_2_real/", $base_path, $config);	
#       compute(undef, $config, "results/tut_time_2__synt_afterEval");
   }
}
if(1 == 2)
{ 
  { #! then a tranpsoed veirosn of the output:
      my $base_path = "";
      my @arrOf = (
         # ../useCase_bibEval/test_result_meta_cat_x_countYearfeatures.tsv
	  # "../useCase_bibEval/test_result_meta_cat_x_countYearfeatures.tsv", #! results/cite_bib_cat_year_transp.tex
	  #! Noteted fhtrough:; x_hp -ops=e_kt_correlationFunction_groupOf_minkowski_euclid -file1=../useCase_bibEval/test_result_journ_merged__features_.tsv > results/useCase_bibEval__test_result_journ_merged__features__simEval__euc.tsv
	  #"results/useCase_bibEval__test_result_journ_merged__features__simEval__MINE.tsv", #! results/useCase_bibEval__test_result_journ_merged__features__simEval__MINE_graph.tsv
	  #"results/useCase_bibEval__test_result_journ_merged__features__simEval__euc.tsv", #! results/useCase_bibEval__test_result_journ_merged__features__simEval__euc_graph.tsv
	  #"results/useCase_bibEval__test_result_journ_merged__features__simEval__kendall.tsv", #! results/useCase_bibEval__test_result_journ_merged__features__simEval__kendall_graph.tsv
	  #"../useCase_bibEval/test_result_meta_cat_x_countYearfeatures.tsv", #! results/cite_bib_cat_year.tex
	  "../useCase_bibEval/test_result_meta_cat_x_countEachDocufeatures.tsv", #! results/cite_bib_countMult_cat.tex
	  #"../useCase_bibEval/test_result_journ_merged__features_.tsv", #! results/cite_bib_journ_cat.tex
	  );
  	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 0; 
      # $config->{thresh_score_isTo_ingore0} = 0;  
      # $config->{thresh_maxScore_toIgnore} = 100000000;  
      #$config->{thresh_maxScore_toIgnore_setToFixedValue} = 1000;   #! ie, the max-size
       $config->{isTo_transposeMatrix} = 1;
compute_setOf_files(\@arrOf, "", $base_path, $config);	
#       compute(undef, $config, "results/tut_time_2__synt_afterEval");
   }
}
{
if(1 == 2)
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./result_tutKd3_simCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimBoth_norm_12_simAppType2.tsv",
	    "./result_tutKd3_simCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_12_simAppType1.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
    #if(1 == 2)
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_simCCM_ccmMatrix_ccm_dbScanKdCCMsSilRandSimBefore_norm_1_simAppType0.tsv",
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_simCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimBoth_norm_1_simAppType2.tsv",
	    "./result_tutKd3_simCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimBefore_norm_1_simAppType0.tsv",
	    "./result_tutKd3_simCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_1_simAppType1.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
     if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_dataSimCatSmall_ccmMatrix_ccm_dbScanKdCCMsSilRandSimDuring_norm_12_simAppType1.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
     if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_dataSim_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimBoth_norm_12_simAppType2.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
     if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_dataCCM_simId_dbScanKdCCMsSilRandSimBefore_norm_1_simAppType0.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
     if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_dataCCM_simId_dbScanBruteCCMsSilRandSimDuring_norm_1_simAppType1.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
     if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_dataCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_1_simAppType1.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
     if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "./results/tut_evalKdSim3_combine3DataSets_fixedCCM/tut_evalKdSim3__realLife_1_isToUseHCAMINE_dataCCM_ccmMatrix_ccm_dbScanBruteCCMsSilRandSimDuring_norm_12_simAppType1.tsv",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
    if(1 == 2) 
    {
	my $config = initConfig__fileRead(); $config->{isTo_transposeMatrix} = 1;
	compute_setOf_files([
	    #! -------------------
	    "",
	    #! -------------------
			    ], "results_tmp_toEval_tex/", "", $config);	
    }
}
#compute(""); #! #! "./x_measures ccm-singleDataSet"
#compute("back-err_result_C.log.list.tsv");  #! cosntructed from: ./x_measures simple-cmp-logComputeCost 2>err.txt
#compute("back-err_result_C.SSE.log.list.tsv");  #! cosntructed from:
# ---------
__reset_matrix();
#compute("");  #! cosntructed from:
#compute("");
# ---------
__reset_matrix();
#compute("");  #! cosntructed from:
#compute("");
# ---------
__reset_matrix();
#compute("");  #! cosntructed from:
#compute("");
# ---------
#!
#! Close global apramters:
close(FILE_OUT_global_meta);
