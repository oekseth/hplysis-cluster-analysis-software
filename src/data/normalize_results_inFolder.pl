use strict;

my $forClustMmberships_eachLine = 1;
my $metric_sim = "e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine";
#! Note: the "hpLysis" is used to compute difference between a given hypothes (ie, ranked order of data-files) versus a collection of CCM scores. To exemplify: my $cmd = $path_hpLysis . " -ops=$metric_sim -file2=$file_hypo -file1=$fileOut_txt_rows > $tmp_result_file";
my $path_hpLysis = "/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/x_hp";
if(!(-x $path_hpLysis)) {
    printf(STDERR "!!\t unable to find executable=\"%s\", at %s:%d\n", $path_hpLysis, __FILE__, __LINE__);
}

sub init__arrOf_metaPropClusters {
    my ($str) = @_;
    my $self = {
	str => $str,
	min => undef,
	max => undef,
	cnt => 0,
	score => 0,
	arrOf => [],
    };
    return $self;
}

sub __convertFile {  
    my ($directory, $file_pattern, $file_pattern_notMatch, $row_pattern, $result_prefix, $config) = @_;
    # my $file_namePrefix = $directory;
#    my $file_namePrefixRes = $globalConfig__resultFolder . $globalCounter++ . "_" . $file_namePrefix; #! ie, the 'new path'.
    my $fileOut_txt_rows = $result_prefix . "_rows_hpLysis.tsv";
    my $fileOut_txt_rows_id = $result_prefix . "_rowsID_hpLysis.tsv";
    my $fileOut_txt_rows_clustMemb = $result_prefix . "_rowClusterMemberships_hpLysis.tsv";
    #if(!(-f $fileOut_txt_meta)) #! then we re-create the file:
    {
	open(my $FILE_OUT_txt_rows, ">$fileOut_txt_rows") or die("Unable to open the file $fileOut_txt_rows\n");
	open(my $FILE_OUT_txt_rows_id, ">$fileOut_txt_rows_id") or die("Unable to open the file $fileOut_txt_rows_id\n");
	open(my $FILE_OUT_txt_rows_clustMemb, ">$fileOut_txt_rows_clustMemb") or die("Unable to open the file $fileOut_txt_rows_clustMemb\n");
	{ #! Write out headers: 
	    printf($FILE_OUT_txt_rows "#! %s\t%s\n", "file-index", join("\t", @{$config->{arrOf_hypo}}));
	    printf($FILE_OUT_txt_rows_id "#! File-name\tfile-index\n");
	    if($forClustMmberships_eachLine) {
		#printf($FILE_OUT_txt_rows_clustMemb "#! %s\t%s\n",  "cluster-name", "cluster-id");
		printf($FILE_OUT_txt_rows_clustMemb "#! %s\t%s\t%s\n", "file-index", "cluster-name", "cluster-id");
	    } else {
		printf($FILE_OUT_txt_rows_clustMemb "#! %s\t", "file-index");
		for(my $i = 0; $i < $config->{fileName_clusterCount}; $i++) {
		    if(($i + 1) != $config->{fileName_clusterCount}) {
			printf($FILE_OUT_txt_rows_clustMemb "case-%d\t", $i);
		    } else {
			printf($FILE_OUT_txt_rows_clustMemb "case-%d\n", $i); #! ie, line-end.
		    }
		}
	    }
	}

	my @arrOf_metaPropClusters; my @arrOf_rows_memberships;

	#! Open the folder contiant the files: 
	opendir(DH, $directory);
	my @files = readdir(DH); 	closedir(DH);
	my $global_fileIndex = 0;
	foreach my $_file_name (@files)  	{
	    my $file_name = $directory . $_file_name;
	    #printf("(info)\t \"%s\", at %s:%d\n", $file_name, __FILE__, __LINE__);
	    next if($file_name !~ $file_pattern);
	    next if($file_name =~ $file_pattern_notMatch); #! ie, a 'not-match'.
	    #printf("(info)\t \"%s\", at %s:%d\n", $file_name, __FILE__, __LINE__);
	    open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
	    my $row_id = 0;
	    #!
	    #! Fetches row from file: 
	    while (my $line = <FILE_IN>) {
		chomp($line); #! ie, remvoe the trail-newline.
		$row_id++;		    
		next if( ($line =~ /^\#/) || ($row_id == 1));
		#printf("(info)\t \"%s\", at %s:%d\n", $file_name, __FILE__, __LINE__);
		next if($line !~ /$row_pattern/);
		#printf("(info)\t \"%s\", at %s:%d\n", $file_name, __FILE__, __LINE__);
		my @arrOf_files_ccms = split("\t", $line);
		my $header = shift @arrOf_files_ccms; #! ie, remove the first value.
		#printf("(info)\t \"%s\", at %s:%d\n", $file_name, __FILE__, __LINE__);
		
		{ #! write otu the relative score, using the last CCM-score as a reference
		    my @arrOf;
		    my $ccm_last = $arrOf_files_ccms[$config->{gold_index}]; #scalar(@arrOf_files_ccms)-1];
		    # printf("(info)\t %s\t ccm_last[%d]=%f, \"%s\", at %s:%d\n", $file_name, 
		    # 	   $config->{gold_index},
		    # 	   $arrOf_files_ccms[$config->{gold_index}],
		    # 	   __FILE__, __LINE__);
		    if($ccm_last != 0) {
			my $index = 0;
			foreach my $ccm (@arrOf_files_ccms) {
			    if($index != $config->{index_toIgnore}) {
				if(defined($ccm) && ($ccm != 0)) {
				    push(@arrOf, ($ccm / $ccm_last));
				} else { push(@arrOf, $ccm);}
			    }
			    $index++;
			}
			#!
			#! Update the mapping table:
			printf($FILE_OUT_txt_rows_id "%s\t%s\n", $file_name, $global_fileIndex++);
			#!
			#!
			#!  
			printf($FILE_OUT_txt_rows "row:%d\t%s\n", $global_fileIndex-1, join("\t", @arrOf));
			#printf($FILE_OUT_txt_rows "%s\t%s\n", $file_name, join("\t", @arrOf));
			#!
			#! Set differnet hypothesis mappings:
			my $fileName_regex = $config->{fileName_regex};
			my @arrOf_cluster_groups = ($file_name =~  $fileName_regex);
			#my @arrOf_cluster_groups = ($file_name =~  /$fileName_regex/i);
			if(scalar(@arrOf_cluster_groups) == 0) {
			    printf("%s\t%s\n", $file_name, join("\t", @arrOf_cluster_groups));
			    croak("..");
			}
			printf("%s\t%s\n", ".", join("\t", @arrOf_cluster_groups));
			#printf("%s\t%s\n", $file_name, join("\t", @arrOf_cluster_groups));
			my @arrOf_localClusterIds;
			my $cluster_str_added = 0;		     
			foreach my $cluster_str (@arrOf_cluster_groups) {
			    my $cluster_id = undef; #scalar(@arrOf_metaPropClusters); #metaPropClusters);
			    for(my $cluster_index = 0; $cluster_index < scalar(@arrOf_metaPropClusters); $cluster_index++) {
			    #foreach my $cluster_index (@arrOf_metaPropClusters) {
				if($cluster_str eq $arrOf_metaPropClusters[$cluster_index]->{str}) {
				    $cluster_id = $cluster_index;
				}
			    }
			    if($cluster_id == undef) {
				my $cluster_id = scalar(@arrOf_metaPropClusters); #metaPropClusters);
				push(@arrOf_metaPropClusters, init__arrOf_metaPropClusters($cluster_str));
			    }
			    push(@arrOf_localClusterIds, $cluster_id);
			    #!
			    #! Write out cluster memberships:
			    if($forClustMmberships_eachLine) {
				#printf($FILE_OUT_txt_rows_clustMemb "%d\t%s\t%d",
				#printf($FILE_OUT_txt_rows_clustMemb "%s\t%d\n", 
				#$global_fileIndex -1, 
				printf($FILE_OUT_txt_rows_clustMemb "%d\t%s\t%d\n", $global_fileIndex -1, 
				       $arrOf_metaPropClusters[$cluster_id]->{str}, #! which simplifies a 'grep' for each case.
				       $cluster_id
				    ) ;
			    } else {
				if($cluster_str_added == 0) { #! then we add a 'header':
				    printf($FILE_OUT_txt_rows_clustMemb "%d\t", $global_fileIndex -1);
				}
				if(($cluster_str_added +1) != scalar(@arrOf_cluster_groups)) {
				    printf($FILE_OUT_txt_rows_clustMemb "%d\t", $cluster_id);
				} else {
				    printf($FILE_OUT_txt_rows_clustMemb "%d\n", $cluster_id); #! then a enw-line
				}
			    }
			    $cluster_str_added++;
			}
			#!
			#! Update the "arrOf_rows_memberships" data-set:
			push(@arrOf_rows_memberships, \@arrOf_localClusterIds);
		    }
		}
	    }
	    close(FILE_IN);
	}
	#!
	#! 
	close($FILE_OUT_txt_rows);
	close($FILE_OUT_txt_rows_id);
	close($FILE_OUT_txt_rows_clustMemb);
	#!
	#!
	my $file_hypo = "test_hypFile.tsv";
	{ #! Construct different hypothesis: 
	    my @arrOf_hypo = @{$config->{arrOf_hypo}};
	    #! Cosntruct the hypothesis file:
	    open(my $FILE_OUT_txt_meta, ">$file_hypo") or die("Unable to open the file $file_hypo\n");
	    printf($FILE_OUT_txt_meta "#! %s\t%s\n", "file-index", join("\t", @{$config->{arrOf_hypo}}));
	    printf($FILE_OUT_txt_meta "%s\t%s\n", "hypo", join("\t", @arrOf_hypo));
	    close($FILE_OUT_txt_meta);	    
	}
	#!
	#! Get scalar for each row: compute simliarty (between them):
	#! Compute similarity: 
	my $tmp_result_file = "tmp_result.tsv";
	my $cmd = $path_hpLysis . " -ops=$metric_sim -file2=$file_hypo -file1=$fileOut_txt_rows > $tmp_result_file";
#	my $cmd = $path_hpLysis . " -ops=$metric_sim -file1=$file_hypo -file2=$fileOut_txt_rows > $tmp_result_file";
	printf(STDERR "compute\t $cmd\t at %s:%d\n", $cmd, __FILE__, __LINE__);
	system($cmd);
	#!
	#! Fetch subset, and the store this into the result file:
	my @arrOf_scores;
	{ 
	    open(FILE_IN, "<$tmp_result_file") or die("Unable to open the input-file $tmp_result_file\n");
	    my $row_id = 0;
	    while (my $line = <FILE_IN>) {
		chomp($line); #! ie, remvoe the trail-newline.
		#printf("$line\n");
		if( ($line !~ /^\#/) && ($row_id > 0) ) {
		    #if($row_id > 0) {
		    #printf("$line\n");
		    my @arrOf_cols = split("\t", $line);
		    my $header = shift @arrOf_cols; #! ie, remove the first value.
		    if($header ne "(null)") {
			push(@arrOf_scores, $arrOf_cols[0]); #! ie, the score.
		    }
		    #$ccm_score = @arrOf_cols[1];
		    #printf("%d\t%f\n", $file_id, $ccm_score);
		}
		$row_id++;
	    }
	    close(FILE_IN);
	}
	{ #! Update min--max for different hypothesis
	    my $row_id = 0;
	    #my $header = shift @arrOf_cols; #! ie, remove the first value.
	    foreach my $score (@arrOf_scores) {
		my $row_ref = $arrOf_rows_memberships[$row_id];
		if(!defined($row_ref)) {
		    printf(STDERR "!!\t Not any members for vertex, at %s:%d\n", __FILE__, __LINE__);
		    next;
		}
		my @arrOf_clusterIds = @{$row_ref};
		#for(my $cluster_index = 0; $cluster_index < scalar(@arrOf_metaPropClusters); $cluster_index++) {
		foreach my $cluster_index (@arrOf_clusterIds) { #! ie, iterate through the cluster membrships assoited to the "row_id"
		    $arrOf_metaPropClusters[$cluster_index]->{cnt}++;
		    $arrOf_metaPropClusters[$cluster_index]->{score} += $score;
		    if(!defined($arrOf_metaPropClusters[$cluster_index]->{min}) || ($arrOf_metaPropClusters[$cluster_index]->{min} > $score)) {
			$arrOf_metaPropClusters[$cluster_index]->{min} = $score;
		    }
		    if(!defined($arrOf_metaPropClusters[$cluster_index]->{max}) || ($arrOf_metaPropClusters[$cluster_index]->{max} < $score)) {
			$arrOf_metaPropClusters[$cluster_index]->{max} = $score;
		    }
		    push(@{$arrOf_metaPropClusters[$cluster_index]->{arrOf}}, $score); #! ie, the total colleciton.
		}
		$row_id++;
	    }
	    #!
	    #! Write out the summary properties:
	    { 
		my $fileOut_txt_summary       = $result_prefix . "_summary_hpLysis.tsv";
		my $fileOut_txt_summary_each  = $result_prefix . "_groupMembershipScores_hpLysis.tsv";
		open(my $FILE_OUT, ">$fileOut_txt_summary") or die("Unable to open the file $fileOut_txt_summary\n");
		open(my $FILE_OUT_EACH, ">$fileOut_txt_summary_each") or die("Unable to open the file $fileOut_txt_summary_each\n");
		printf($FILE_OUT "#! row-id: %s\t%s\t%s\t%s\t%s\n",
		       "metric-space",
		       "minimum", "average", "maximum", "count"
		    );
		for(my $cluster_index = 0; $cluster_index < scalar(@arrOf_metaPropClusters); $cluster_index++) {
		    my $obj = $arrOf_metaPropClusters[$cluster_index];
		    my $avg = 0;
		    if( ($obj->{score} != 0) && ($obj->{cnt} != 0)) {
			$avg = $obj->{score}  / $obj->{cnt};
		    }
		    printf($FILE_OUT "%s\t%f\t%f\t%f\t%f\n", 
			   $obj->{str},
			   $obj->{min},
			   $avg,
			   $obj->{max},
			   $obj->{cnt});
		    #!
		    #! 
		    #! Write out the global counts (eg, to be visaulized in a line-plot figure):
		    printf($FILE_OUT_EACH "%s\t%s\n", $obj->{str}, join("\t", @{$obj->{arrOf}}));
		}
		close($FILE_OUT);
		close($FILE_OUT_EACH);
	    }
	}	
    }
}




{
    my $file_pattern = qr/\.tsv$/;
    my $file_pattern_notMatch = qr/(rank|relLast|clusterProp)\.tsv$/;
    my $config = {};
    #! ccm-type     data/anthony_missingData/wine.csv.hpLysis.tsv   data/anthony_missingData/wine1.csv.hpLysis.tsv  data/anthony_missingData/wine2.csv.hpLysis.tsv  data/anthony_missingData/wine3.csv.hpLysis.tsv  data/anthony_missingData/wine4.csv.hpLysis.tsv  data/anthony_missingData/wine5.csv.hpLysis.tsv  data/anthony_missingData/wine5.csv.hpLysis.tsv  data/anthony_missingData/wine6.csv.hpLysis.tsv  data/anthony_missingData/wine7.csv.hpLysis.tsv  data/anthony_missingData/wine8.csv.hpLysis.tsv  data/anthony_missingData/wine9.csv.hpLysis.tsv    
    $config->{arrOf_hypo}  = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];  #! ie, depends on the numer of columns in the inptu data sets.
    #! Note: 
    #! Note: we need to epxlreo for diffeenet combinionat s of [below], ie, to 'only' evaluate for the use-case where we test for inlfuence of simlairty metrics.
    $config->{fileName_clusterCount} = 5; #! where the count should reflec t[beloow9] "fileName_regex"
    # $pcode = qr/^\s*($local)\d\w?\s+\d[A-Z]{2}\s*$/;
    $config->{fileName_regex} = qr/matrix_(.+?)_(\d_norm)_(.+?)_simMetric_e_kt_correlationFunction_groupOf_(.+)inClust_(.+)\.tsv/i; # inClust_0_algorithm::(.+?)\.tsv/;
    #!
    # if(1 == 2)
    { 
	my $row_pattern = "Silhouette-Index";
	my $directory = "result_wine/";    
	my $result_prefix = "eachCase_wine_sil";
	$config->{gold_index} = 0;
        $config->{index_toIgnore} = 5;
	#! Apply:
	__convertFile($directory, $file_pattern, $file_pattern_notMatch, $row_pattern, $result_prefix, $config);
    }
    #if(1 == 2)
    {
	my $directory = "result_fire/";    
	my $result_prefix = "eachCase_fire";
	$config->{gold_index} = 0;
        $config->{index_toIgnore} = 5000; #! ie, Not ignore.
	foreach my $row_pattern ("SSE", "Silhouette", "Dunn", "VRC") {
	    my $result_prefix_loc = $result_prefix .= "_" . $row_pattern;
	    #! Apply:
	    __convertFile($directory, $file_pattern, $file_pattern_notMatch, $row_pattern, $result_prefix_loc, $config);
	}
    }
    # if(1 == 2)
    {
	my $directory = "result_stock/";    
	my $result_prefix = "eachCase_stock";
	$config->{gold_index} = 0;
        $config->{index_toIgnore} = 5; 
	foreach my $row_pattern ("SSE", "Silhouette", "Dunn", "VRC") {
	    my $result_prefix_loc = $result_prefix .= "_" . $row_pattern;
	    #! Apply:
	    __convertFile($directory, $file_pattern, $file_pattern_notMatch, $row_pattern, $result_prefix_loc, $config);
	}
    }
}
