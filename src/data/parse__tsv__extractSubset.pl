#!/usr/bin/perl -w

=head
    @brief provide logics for extracting a subet of a matrix (oekseth, 06. mar. 2017).
=cut

my @arrOf_strings_regex = (

    );
my $arrOf_strings_regex__useRegex = 1;
my $globalConfig__allRowsOf_interest = 1;
my $globaConfig__headerTag__remove = 1; #! which is used iot. satisfy the 'requitments' by the "Highcharts,js" java-vosiu-lib
my $globaConfig__isTo_transpose = 1;

sub __isOf_interest__rowHeader {
    my $string = shift;
    if(!scalar(@arrOf_strings_regex)) {return 1;} #! ie, as 'all cases' are then of interest
    else {
	foreach my $cmp (@arrOf_strings_regex) {
	    if($arrOf_strings_regex__useRegex) {
		if($string =~ /$cmp/i) {
		    return 1; #! ie, as the string matched the 'search'.
		}
	    } elsif($cmp eq $string) {return 1;}
	}
    }
    return 0; #! ie, a fall-back
}

#! A siple parser to convert an SKB-file into a set of triplets:
sub __parser {
    my($file_name_in, $file_name_result_rel) = @_;
    open(FILE_IN, "<$file_name_in") or die("Unable to open the input-file $file_name_in\n");
    if(!defined($file_name_result_rel) || !length($file_name_result_rel)) {
	$file_name_result_rel = "tmp.tsv";
	printf("generates result in file=\"%s\", at %s:%d\n", $file_name_result_rel, __FILE__, __LINE__);
    }
    open(FILE_OUT_rel, ">$file_name_result_rel") or die("Unable to open the result<-file \"$file_name_result_rel\"\n");
    my $cnt_rows = 0;
    my @columnsOf_interst;
    my @resultMatrix = ();
    while (my $line = <FILE_IN>) {
	chomp($line); #! ie, remvoe the trail-newline.
	# if($cnt_rows > 100) {last;} # TODO: remove.
	my @cols = split("\t", $line);
	if($cnt_rows != 0) {
	    # printf(STDERR "(rows) line=\"$line\"\n");
	    if(scalar(@cols) && length($cols[0])) {
		if($globalConfig__allRowsOf_interest || __isOf_interest__rowHeader($cols[0])) {	
		    my @cols_updated = @cols;
		    if($globalConfig__allRowsOf_interest == 1) { #! then we evlauate wrt. the columsn:
			@cols_updated = ($cols[0]);
			for(my $i = 1; $i < scalar(@cols); $i++) {
			    if($columnsOf_interst[$i]) {
				push(@cols_updated, $cols[$i]);
			    }
			}
		    }
		    #printf("--- %s, at %s:%d\n", join("\t", @cols_updated), __FILE__, __LINE__);
		    printf(FILE_OUT_rel "%s\n", join("\t", @cols_updated));
		    if($globaConfig__isTo_transpose) {
			push(@matrix, \@cols_updated);
		    }
		}
	    }
	} else {
	    my $tmp = "";
	    #($tmp, $line) = ($line =~ /^(.+ )(.+)$/);
	    # printf(STDERR "line=\"$line\"\n");
	    #printf("tmp=\"$tmp\"\n");
	    #($line, $tmp) = ($line =~ /^(.*# )?(.+)$/);
	    #($line, $tmp) = ($line =~ /^(\!\#)?(.+)$/);
	    my @cols = split("\t", $line);
	    if(scalar(@cols) && length($cols[0])) {	    
		if($cols[0] =~/^\!#(.+)$/) {
		    $cols[0] = $1;
		}
		push(@columnsOf_interst, 1);
		for(my $i = 1; $i < scalar(@cols); $i++) {
		    if($globalConfig__allRowsOf_interest == 0) { #! then we evlauate wrt. the columsn:
			my $status = __isOf_interest__rowHeader($cols[$i]);
			push(@columnsOf_interst, $status);
		    } else {
			push(@columnsOf_interst, 1); #! ie, as we then 'for simplicty' asusmes that all column-headers 'are of interest'.
		    }
		}
		#printf("--- %s, at %s:%d\n", join("\t", @cols), __FILE__, __LINE__);
		if($globaConfig__headerTag__remove) {
		    printf(FILE_OUT_rel "%s\n", join("\t", @cols));
		} else {
		    printf(FILE_OUT_rel "#! %s\n", join("\t", @cols));
		}
		if($globaConfig__isTo_transpose) {
		    push(@matrix, \@cols);
		}
	    } else {
		if(length($line)) {
		    printf(STDERR "!!\t inveigtgate case for line=\"$line\"\n");
		}
	    }
	}
	$cnt_rows++;
    }
    close(FILE_IN);
    close(FILE_OUT_rel);
    if($globaConfig__isTo_transpose) { #! then we write out a transpsoed verison:
	$file_name_result_rel .= "_transposed.tsv";
	#printf("generates trasnpseod-result in file=\"%s\", at %s:%d\n", $file_name_result_rel, __FILE__, __LINE__);
	open(FILE_OUT_rel, ">$file_name_result_rel") or die("Unable to open the result<-file \"$file_name_result_rel\"\n");
	my @trasnposed = ();
	my $cnt_cols = scalar(@{$matrix[0]});
	for(my $k = 0; $k < $cnt_cols; $k++) {
	    my @col = ();
	    for(my $i = 0; $i < scalar(@matrix); $i++) {
		my $score = $matrix[$i]->[$k];
		push(@col, $score);
	    }
	    #printf("[%u]\ycol_size=%u: @col\n", $k, scalar(@col));
	    push(@trasnposed, \@col);
	}
	#printf("cnt_cols=$cnt_cols=%u\n", scalar(@trasnposed));
	for(my $i = 0; $i < scalar(@trasnposed); $i++) {
	    my @cols = @{$trasnposed[$i]};
	    if($i == 0) {		
		if($globaConfig__headerTag__remove) {
		    printf(FILE_OUT_rel "%s\n", join("\t", @cols));
		} else {
		    printf(FILE_OUT_rel "#! %s\n", join("\t", @cols));
		}
	    } else {
		# printf( "\"%s\"\n", join("\t", @cols));
		printf(FILE_OUT_rel "%s\n", join("\t", @cols));
	    }
	}
	    
	close(FILE_OUT_rel);
    }
}
#! -------------------------------------------------------------------------------------
#! -------------------------------------------------------------------------------------
#! -------------------------------------------------------------------------------------

my $numArgs = $#ARGV + 1;
#printf("argvv=\"%s\", numArgs=$numArgs\n", join("\t",  @argv));
if ($numArgs > 1) {
# if(1 == 2) 
#	printf("argvv=\"%s\", numArgs=$numArgs\n", join("\t",  @argv));
    my $file_name_in = $ARGV[0];
    my $file_name_out = $ARGV[1];
    if(!(-f $file_name_in)) {
	printf(STDERR "!!\t Unable to find the ipnut-file=\"%s\", at %s:%d\n", $file_name_in, __FILE__, __LINE__);
    }
 #   printf("file_name_in=\"%s\", at %s:%d\n", $file_name_in, __FILE__, __LINE__);
    { #! Compute:
	@arrOf_strings_regex = (
	    
	    );
	$arrOf_strings_regex__useRegex = 1;
	$globalConfig__allRowsOf_interest = 0;
	$globaConfig__headerTag__remove = 1; #! which is used iot. satisfy the 'requitments' by the "Highcharts,js" java-vosiu-lib
	#! -----------
	__parser($file_name_in, $file_name_out);
	#! -----------
    }
}
#! -------------------------------------------------------------------------------------
#! -------------------------------------------------------------------------------------
#! -------------------------------------------------------------------------------------
if(1 == 2) 
{ #! Compute:
    @arrOf_strings_regex = (
	
	);
    $arrOf_strings_regex__useRegex = 1;
    $globalConfig__allRowsOf_interest = 1;
    $globaConfig__headerTag__remove = 1; #! which is used iot. satisfy the 'requitments' by the "Highcharts,js" java-vosiu-lib
    #! -----------
    __parser("", "");
    #! -----------
}
