#!/usr/bin/perl
#
#----------------------------------------------------------------------------#
#   PROGRAM: most-hits.pl
#
#   PURPOSE: This program serves one purpose.  It reads an Apache
#   access_log file in standard ECLF format,
#   and print out the number of hits that have been 
#   recorded for each file and/or directory.
#
#   The user can control the number of files that are
#   displayed in the output by changing NUM_RECS_TO_PRINT.
#
#   This tool lets you analyze your web site so you can
#   understand what content your viewers are interested in.
#
#   USAGE:
#
#   most-hits.pl access_log > results
#   perl most-hits.pl access_log > results
#
#----------------------------------------------------------------------------#

#----------------------------------------------------------------------------#
# COPYRIGHT:                                                                 #
#                                                                            #
# This sample program is provided free of charge under the terms of the      #
# GNU GPL.                                                                   #
#----------------------------------------------------------------------------#

use File::Basename;
use strict;
use strict;
use Time::Local;
use POSIX qw(strftime);
use DateTime;
use Carp;


my $globalConfig__resultFile__map = "data/apache_strToId.tsv";
#! --------------------------------- 
#! --------------------------------- 
#! Note: in [”elow] ex epct expect our "parse__apacheLog.pl" and our "tut_sparseSim_7_realLife_apacheLog_compareMultipleFiles.c" to be realted (oekseht, 06. mar. 2017)
#! --------------------------------- 
my $globalConfig__resultFile__ip_bySoftware        = "data/apache_ip_bySoftware.tsv";
my $globalConfig__resultFile__ip_bySoftware_robots = "data/apache_ip_bySoftware.robots.tsv";
#! --------------------------------- 
my $globalConfig__resultFile__ip_by_dayOf_week         = "data/apache_ip_by_dayOf_week.tsv";
my $globalConfig__resultFile__ip_by_dayOf_week_robots  = "data/apache_ip_by_dayOf_week.robots..tsv";
#! --------------------------------- 
my $globalConfig__resultFile__ip_accessedFile        = "data/apache_ip_accessedFile.tsv";
my $globalConfig__resultFile__ip_accessedFile_robots = "data/apache_ip_accessedFile.robots.tsv";
#! --------------------------------- 
# my $globalConfig__resultFile__ = "data/apache_.tsv";
# my $globalConfig__resultFile__ = "data/apache_.tsv";
# my $globalConfig__resultFile__ = "";
# my $globalConfig__resultFile__ = "";
#! -------------------------------------
#! 
#! Open files:
open(FILE_OUT_ip_bySoftware, ">$globalConfig__resultFile__ip_bySoftware") or die("An error in opning");
open(FILE_OUT_ip_bySoftware_robots, ">$globalConfig__resultFile__ip_bySoftware_robots") or die("An error in opning");
#! --------------------------------- 
open(FILE_dayOf_week_client , ">$globalConfig__resultFile__ip_by_dayOf_week") or die("An error in opning");
open(FILE_dayOf_week_client_robot, ">$globalConfig__resultFile__ip_by_dayOf_week_robots") or die("An error in opning");
#! --------------------------------- 
open(FILE_ip_accessedFile , ">$globalConfig__resultFile__ip_accessedFile") or die("An error in opning");
open(FILE_ip_accessedFile_robots , ">$globalConfig__resultFile__ip_accessedFile_robots") or die("An error in opning");
#! --------------------------------- 

#------------------------------------------------------------------------------#
#  Global variables that control the program action and output.                #
#------------------------------------------------------------------------------#

my $NUM_RECS_TO_PRINT = 100;   # num of output recs to print per section

#---------------------------------------------------------------------#
#  Change this array to include index filenames used on your system.  #
#---------------------------------------------------------------------#

my @indexFilenames = ('index.htm', 'index.html', 'index.shtml');


#----------------------------------------------------------------------#
# don't change anything below here unless you're comfortable with Perl #
#----------------------------------------------------------------------#

my %numFileRequests;

#----------------------------------------------------------#
#  These are two helper routines for the 'sort' function.  #
#----------------------------------------------------------#

sub fileNumericAscending {
    $numFileRequests{$a} <=> $numFileRequests{$b};
}

sub fileNumericDescending {
    $numFileRequests{$b} <=> $numFileRequests{$a};
}

sub trim($) {
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
}


=head
    @brief convert a set of triplets into a simplifed data-set where indces represent strings (oekseth, 06. mar. 2017)
    @remarks 
    -- Idea: ide is generate two different matrices: (a) 'synonym-relations' and (b) 'ordinary' SKB-relations ... new Perl-script named "aux_relations_to_db.pl"
=cut


my %globalHash; my $globalHash__cnt = 0;

sub __getInternalIndex {
    my $name = lc(shift);
    $name =~ s/[ \t\n]//g; #! ie, remove all 'hidden' chars.
    if(length($name)) {
	my $value = $globalHash{$name};
	if(!defined($value)) {
	    $value = $globalHash{$name} = $globalHash__cnt++;
	}
	#! @return
	return $value;
    } else {return undef;}
}


#! Note: the "robot" inforaiton provides information about crawl-endgines which are 'open of their arictivty', ie, provide the 'means' to accurately idneityf/'name' a sepcific tape of ativity (ie, the indexing of WWWW).
sub __writeOut__triplet {
    my($head, $predicate, $tail, $predicate_tag) = @_;
    my $ind_h     = __getInternalIndex($head);
    my $ind_t     = __getInternalIndex($tail);
    if(!defined($ind_h) || !defined($ind_t)) {return;} #! ie, as we then asusme triplet is Not of interest.
    #!
    #! Descide the file-set to 'print reuslt to':
    my $score = 1;

    #!
    {     #! Swap: Then we 'invert the triplets', ie, as a 'maingifuyl simlairty-comarpigon-appraoch' sohulb involve the same column-features' (eg, ip-addresses).
	my $tmp = $ind_t; $ind_t = $ind_h; $ind_h = $tmp;
    }
    # TODO: cosndier d'ropping thio'.

    # printf(FILE_OUT_ip_bySoftware "a test\n");

    if($predicate eq "ip_bySoftware") {
	if(!defined($predicate_tag)) {
	    #printf("%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	    printf(FILE_OUT_ip_bySoftware "%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	} elsif($predicate_tag eq "robot") {
	    printf(FILE_OUT_ip_bySoftware_robots "%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	} else {
	    croak("Add support for predicate_tag=\"$predicate_tag\"");
	}
	;	
    } elsif($predicate eq "ip_by_dayOf_week") {
	if(!defined($predicate_tag)) {
	    printf(FILE_dayOf_week_client "%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	} elsif($predicate_tag eq "robot") {
	    printf(FILE_dayOf_week_client_robot "%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	} else {
	    croak("Add support for predicate_tag=\"$predicate_tag\"");
	}
    } elsif($predicate eq "ip_accessedFile") {
	if(!defined($predicate_tag)) {
	    printf(FILE_ip_accessedFile "%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	} elsif($predicate_tag eq "robot") {
	    printf(FILE_ip_accessedFile_robots "%u\t%u\t%f\n", $ind_h, $ind_t, $score);
	} else {
	    croak("Add support for predicate_tag=\"$predicate_tag\"");
	}
    # } elsif($predicate eq "") {
    # 	if(!defined($predicate_tag)) {
    # 	    ;
    # 	} elsif($predicate_tag eq "robot") {
    # 	    ;
    # 	}
    # 	;
    } else {
	croak("Add support for this");
    }
}

# $numArgs = $#ARGV + 1;
# if ($numArgs != 1) {
#     &usage;
#     exit 1;
# }

sub __parseFile {
    my($logFile) = @_;
    #$logFile = $ARGV[0];    
    open (LOGFILE,"$logFile") || die "  Error opening log file $logFile.\n";

    #------------------------------------------------------------------#
    #  Start reading and processing the access_log file in this loop.  #
    #------------------------------------------------------------------#
    
#printf "<pre>\n";
    my $line_count = 0;

    while(<LOGFILE>) {	
	chomp;
	s/\s+/ /go;

      #----------------------------------------------------------#
      #  the next line breaks each line of the access_log into   #
      #  nine variables                                          #
	#----------------------------------------------------------#
	my ($clientAddress,    $rfc1413,      $username, 
	 $localTime,         $httpRequest,  $statusCode, 
	 $bytesSentToClient, $referer,      $clientSoftware) =
	     /^(\S+) (\S+) (\S+) \[(.+)\] \"(.+)\" (\S+) (\S+) \"(.*)\" \"(.*)\"/o;
	#--------------------------------------------------------------------#
	# take care of problem where the $httpRequest may simply be a hyphen #
	#--------------------------------------------------------------------#
	next if ($httpRequest =~ '^-$');	
	#-----------------------------------------#
	#  Determine the value of $fileRequested  #
	#-----------------------------------------#	
	my ($getPost, $fileRequested, $junk) = split(' ', $httpRequest, 3);	
	my $isA_robot = ($fileRequested =~ /robots\.txt$/i);	
	if ($fileRequested =~ /\.gif$/i) {
	    next;
	}
	if ($fileRequested =~ /\.jpg$/i) {
	    next;
	}
	if ($fileRequested =~ /\.css$/i) {
	    next;
	}
	if ($fileRequested =~ /\.png$/i) {
	    next;
	}
	if ($fileRequested =~ /\.java$/i) {
	    next;
	}
	if ($fileRequested =~ /favicon\.ico$/i) {
	    next;
	}
	#-----------------------------------------------------------------#
	#  if the base filename is something like index.htm, index.html,  #
	#  or index.shtml, interpret this to be the same as the path by   #
	#  itself.  This way, '/java/' is the same as '/java/index.html'. #
	#-----------------------------------------------------------------#	
	foreach my $indexFile (@indexFilenames) {
	    chomp($fileRequested);
	    $fileRequested = trim($fileRequested);
	    if ($fileRequested =~ /^\s+$/) {
		next;
	    }
	    if ($fileRequested =~ /^$/) {
		next;
	    }
	    if (basename($fileRequested) =~ /$indexFile/i) {
		$fileRequested = dirname($fileRequested);
		last;
	    }
	}	
	#----------------------------------------------------------------#
	#  If the last character in $fileRequested is a '/', remove it.  #
	#  This makes /perl/ equal to /perl.                             #
	#----------------------------------------------------------------#	
	if (length($fileRequested) > 1) {
	    if (substr($fileRequested,length($fileRequested)-1,1) eq '/') {
		chop($fileRequested);
	    }
	}
	# if ($fileRequested =~ /robots\.txt$/i) {
	#     # printf("a robot, at %s:%d\n", __FILE__, __LINE__);
	#     next;
	# }

	#! --------------------------------------------------------------------------------
	#! --------------------------------------------------------------------------------
	#! Build triplets: 
	#! --------------------------------------------------------------------------------
	__writeOut__triplet($clientAddress, "ip_bySoftware", $clientSoftware); #! ie, to 'see' if there are ... relationships between the ip-address and sufotware sued by client: among others we expec thtat 'Baidu' use a large number od fiferent IPs.
	if($isA_robot) {
	    __writeOut__triplet($clientAddress, "ip_bySoftware", $clientSoftware, "robot"); #! ie, to 'see' if there are ... relationships between the ip-address and sufotware sued by client: among others we expec thtat 'Baidu' use a large number od fiferent IPs.
	}
	#! --------------------------
	{ #! Get 'day in week', and 'see' the the week-days rpovide  a strong relationship for the different data-accesses.
	    #my $date = '08/15/2012';
	    my $date = $localTime;
	    if($date =~ /^(.+?)\:/) { $date = $1;} #! eg, to handle "data: 05/Mar/2017:06:28:17 +0000"
	    my $month_index = 0;
	    my $date_beforeChange = $date;
	    foreach my $month ("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec") {
		if($date =~ /^(.+)($month)(.+)/i) { $date = $1 . $month_index . $3;}
		if($month_index > 12) {croak("An issue");}
		$month_index++;
	    }
	    my ($day, $month, $year) = split '/', $date;
	    #printf("data: \"$date\", where date_beforeChange=\"$date_beforeChange\"\n");
	    my $dt = DateTime->new(year => $year, month => $month, day => $day);
	    my $dayOf_week = $dt->day_name;
	    #! Add the triplet
	    __writeOut__triplet($clientAddress, "ip_by_dayOf_week", $dayOf_week); #! ie, to 'see' if there are ... 
	    if($isA_robot) {
		__writeOut__triplet($clientAddress, "ip_by_dayOf_week", $dayOf_week, "robot"); #! ie, to 'see' if there are ... 
		#__writeOut__triplet($dayOf_week, "ip_by_dayOf_week", $clientAddress, "robot"); #! ie, to 'see' if there are ... 
	    }
	    #my $epoch = timelocal( 0, 0, 0, $day, $month - 1, $year - 1900 ); my $week  = strftime( "%U", localtime( $epoch ) )
	}
	#! --------------------------
	__writeOut__triplet($clientAddress, "ip_accessedFile", $fileRequested); #! ie, to 'see' if there are ... 
	if($isA_robot) {
	    __writeOut__triplet($clientAddress, "ip_accessedFile", $fileRequested, "robot"); #! ie, to 'see' if there are ... 
	}
	#! --------------------------
	#__writeOut__triplet($clientAddress, "", $clientSoftware); #! ie, to 'see' if there are ... 
	#! --------------------------
	#! --------------------------------------------------------------------------------

	#printf("addr=\"%s\", rfc1413=\"%s\", localTime=\"%s\", httpRequest=\"%s\", clientSoftware=\"%s\", referer=\"%s\"\n", $clientAddress, $rfc1413, $localTime, $httpRequest, $clientSoftware,  $referer);
	#if($line_count++ > 20) {croak("a tesT2");} # FIXME: remove.
	
	#-----------------------------------------------------#
	#  here's where we count the number of hits per file  #
	#-----------------------------------------------------#
	
	$numFileRequests{$fileRequested}++;
	
    }

    close (LOGFILE);
    
    #--------------------------------------#
    #  Output the number of hits per file  #
    #--------------------------------------#
    
    print "TOP $NUM_RECS_TO_PRINT MOST-REQUESTED FILES:\n";
    print "-----------------------------\n\n";
    my $count=0;
    foreach my $key (sort fileNumericDescending (keys(%numFileRequests))) {
	last if ($count >= $NUM_RECS_TO_PRINT);
	print "$numFileRequests{$key} \t\t $key\n";
	$count++;
    }
    print "\n\n";

    printf "</pre>\n";

{ #! Generate a mapping-file 'between the strings and the integers':
    my $file_name_result_syn = $globalConfig__resultFile__map;
    open(FILE_OUT_syn, ">$file_name_result_syn") or die("Unable to open the input-file $file_name_result_syn\n");
    printf("generate mappings \"$file_name_result_syn\"\n");
    printf(FILE_OUT_syn "#! legend-id:\tkey\n"); #! a 'field' expected by our "parse_main.c"
    foreach my $key (keys(%globalHash)) {
	printf(FILE_OUT_syn "%s\t%d\n", $key, $globalHash{$key});
    }
    close(FILE_OUT_syn);
#! --------------------------------- 
    close(FILE_OUT_ip_bySoftware);
    close(FILE_OUT_ip_bySoftware_robots);
#! --------------------------------- 
    close(FILE_dayOf_week_client);
    close(FILE_dayOf_week_client_robot);
#! --------------------------------- 
    close(FILE_ip_accessedFile);
    close(FILE_ip_accessedFile_robots);
#! --------------------------------- 
}

}

#! ****************************************************************
#! ****************************************************************
#!
#! Apply lgocis for a givne 'concatnation' of log-files:
__parseFile("data/apache_result_access.txt");


