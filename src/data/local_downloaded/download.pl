
 # use strict;
# use warnings;
 
sub make_call {
    my($local_destination, $path, $extraPattern) = @_;
    my $wget_header = "wget -r";
    if(defined($extraPattern)) {
	$wget_header .= " " . $extraPattern;
    }
    $wget_header .= " -np -nH index.html";
    
    my $call = $wget_header . " -P $local_destination $path";
    system($call);
}


my @arrOf_downloads = (
    {path => "http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data", format => "nameInLastCol"},
    );

#!
#! Download
foreach my $path (@arrOf_downloads) {
    my $local_destination = ".";
    my $path = $obj->{path};
    #$path = "ftp://ftp.biochem.ucl.ac.uk/pub/cath/v3_5_0/";
    make_call($local_destination, $path);    
}
