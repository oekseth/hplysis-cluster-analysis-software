#!/usr/bin/perl
use strict;
use Carp;
#use Posix;

my $globalConfig__resultFolder = "data/local_downloaded/";
my $globalCounter = 0; #! which is used to 'ensure' a unieu file-name for cases where muliple calls are 'made' tot eh same file (eg, for the case where different cofniguairoants are used).

my $GLOBAL__resultFile__meta__name = $globalConfig__resultFolder . "metaObj.c";
open(GLOBAL__resultFile__meta, ">$GLOBAL__resultFile__meta__name") or die("Unable to open $GLOBAL__resultFile__meta__name");

my $__spec__fileRead_config = ["fileRead_config", "fileRead_config__transpose"];
my $__spec__e_hp_clusterFileCollection_simMetricSet = [ "e_hp_clusterFileCollection_simMetricSet_MINE_Euclid", 
							"e_hp_clusterFileCollection_simMetricSet_subset__small", 
							"e_hp_clusterFileCollection_simMetricSet_subset__medium", 
							"e_hp_clusterFileCollection_simMetricSet_all_notPostProcess", 
							"e_hp_clusterFileCollection_simMetricSet_all", 
							"e_hp_clusterFileCollection_simMetricSet_undef", #! ie, Not apply sim-metrics. 
    ];
my $__spec__e_hp_clusterFileCollection__clusterAlg = [ "e_hp_clusterFileCollection__clusterAlg_kMeans",
						       "e_hp_clusterFileCollection__clusterAlg_undef"];
my $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t = ["e_hp_clusterFileCollection__goldClustersDefinedBy_rowString",
								   "e_hp_clusterFileCollection__goldClustersDefinedBy_undef"];

sub __initConfig {
    return {
	seperator => '\t',
	#! --- 
	bool__columnsAtRow0 => 0,
	#! --- 
	arrOf_names_rows => undef,
	arrOf_names_cols => undef,
	arrOf_names_rows__atIndex__first => 0,
	arrOf_names_rows__atIndex__last => 1,
	#! --- 
	# FIXME: use [”elow].
	mapOf_toIgnore__columns => undef, #! which if set is used to idneityf the columsn to omit from evlauation: while '0' implies 'use', '1' impleis 'ignore' while '2' is 'add-to-first-col and otehrwise ignroe'
	mapOf_toIgnore__columns__isToEvaluateInReverseOrder => 0, #! which may be sued if we are intereted in ingoring the last columns
	mapOf_toIgnore__rowPrefix => undef,
	#!
	#! The ANSI-C struct-object to generate:
	configANSI => {
	    #tag => undef,
	    k_clusterCount => 2,	    
	    fileRead_config => $__spec__fileRead_config->[0],
	    #! --- 
	    alt_clusterSpec => $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t->[1],
	    #! --- 
	    metric__beforeClust => $__spec__e_hp_clusterFileCollection_simMetricSet->[0],
	    metric__insideClust => $__spec__e_hp_clusterFileCollection_simMetricSet->[0],
	    clusterAlg => $__spec__e_hp_clusterFileCollection__clusterAlg->[0],
	},
    };
}

sub __initConfig__spec__datavincentarelbundock {
    my ($local_config, $read_config) = @_;
    my $config = __initConfig();
    $config->{bool__columnsAtRow0} = 1;
    $config->{seperator} = ',';
    $config->{arrOf_names_rows__atIndex__first} = 1;
    $config->{arrOf_names_rows__atIndex__last} = 0;
    #$config->{} = ;
    #! @return:
    if(defined($read_config)) {
	foreach my $key (keys(%{$read_config})) {
	    #printf("(read)\tset-key=\"$key\"=\"" . $local_config->{$key} . "\"\n");
	    $config->{$key} = $read_config->{$key};
	    if(defined($config->{mapOf_toIgnore__columns})) {
		#printf("-------- ingore-a-se-tof-cols\n");
	    }

	}
    }
    if(defined($local_config)) {
	foreach my $key (keys(%{$local_config})) {
	    # printf("(ansiC)\tset-key=\"$key\"\n");
	    $config->{configANSI}->{$key} = $local_config->{$key};
	}
    }
    # if(defined($config->{mapOf_toIgnore__columns})) {
    #     #printf("-------- ingore-a-se-tof-cols\n");
    # }

    return $config;
}

#! Covnertes a file_name described by $config into an s_kt_matrix_t comaptible file (oekseht, 06. feb. 2017).
sub __convertFile {
    my ($file_name, $config) = @_;
    my $file_namePrefix = $file_name;
    if($file_namePrefix =~ /\/([a-z_\.\d]+)$/i) {
	$file_namePrefix = $1; #! ie, 'chop off' the directory-path.
    }    

    # printf("file_namePrefix=\"$file_namePrefix\"\n");
    my $file_namePrefixRes = $globalConfig__resultFolder . $globalCounter++ . "_" . $file_namePrefix; #! ie, the 'new path'.
    my $fileOut_txt_meta = $file_namePrefixRes . ".hpLysis.tsv";
    open(my $FILE_OUT_txt_meta, ">$fileOut_txt_meta") or die("Unable to open the file $fileOut_txt_meta\n");
    open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
    my $colHeader__isAdded = 0;
    if(defined($config->{arrOf_names_cols})) {
	my @arrOf_cols = @{$config->{arrOf_names_cols}};
	printf($FILE_OUT_txt_meta "#! row-name:\t%s\n", join("\t", @arrOf_cols));	
	$colHeader__isAdded = 1;
    }
    #! --------------------
    { #! Update the result-ANSI-C-object:
	my $fileRead_config = $config->{configANSI}->{fileRead_config};
	my $k_clusterCount = $config->{configANSI}->{k_clusterCount};
	my $alt_clusterSpec = $config->{configANSI}->{alt_clusterSpec};
	my $metric__beforeClust = $config->{configANSI}->{metric__beforeClust};
	my $metric__insideClust = $config->{configANSI}->{metric__insideClust};
	my $clusterAlg = $config->{configANSI}->{clusterAlg};
	#my $ = $config->{configANSI}->{};
	printf(GLOBAL__resultFile__meta qq({/*tag=*/"$file_namePrefix", /*file_name=*/\"$fileOut_txt_meta\", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/$k_clusterCount, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, $alt_clusterSpec,/*metric__beforeClust=*/$metric__beforeClust, /*metric__insideClust=*/$metric__insideClust, /*clusterAlg=*/$clusterAlg},\n));
    }
    #! --------------------

    my %hashOf_uniqueGroups; my $hashOf_uniqueGroups__cnt = 0;
    # if(defined($config->{mapOf_toIgnore__columns})) {
    #     printf("-------- ingore-a-se-tof-cols\n");
    # }

    my $row_id = 0;
    while (my $line = <FILE_IN>) {
	chomp($line); #! ie, remvoe the trail-newline.
	my @arrOf_cols = split($config->{seperator}, $line);
	if(defined($config->{mapOf_toIgnore__columns})) {
	    my @mapOf_toIgnore = @{$config->{mapOf_toIgnore__columns}};
	    my @arrOf_altCol = ();
	    my $val_first__append = "";
	    if($config->{mapOf_toIgnore__columns__isToEvaluateInReverseOrder} == 0) {
		my $i = 0;
		for(; $i < scalar(@mapOf_toIgnore); $i++) {
		    #printf("mapOf_toIgnore[$i]=" . $mapOf_toIgnore[$i] . "\n"); 
		    if($mapOf_toIgnore[$i] == 1) {$arrOf_cols[$i] = undef;}
		    #if($mapOf_toIgnore[$i] == 1) {push(@arrOf_altCol, $arrOf_cols[$i]);}
		    elsif($mapOf_toIgnore[$i] == 2) {$val_first__append .= $arrOf_cols[$i]; $arrOf_cols[$i] = undef;}
		}
		# #! Then add the remainind columsn to the set, ie, as we assuemt he 'ramainer' is also of interest. 
		# for(; $i < scalar(@arrOf_cols); $i++) {
		#     push(@arrOf_altCol, $arrOf_cols[$i]);
		# }
	    } else { #! then we use a reverse traversl, 'enaglbed' to simplfiy the 'speficiaotn' of columns to ingore
		my $global_index = scalar(@arrOf_cols)-1;
		my $i = 0;
		for(; $i < scalar(@mapOf_toIgnore); $i++) {
		    if($mapOf_toIgnore[$i] == 0) {$arrOf_cols[$global_index] = undef;}
		    elsif($mapOf_toIgnore[$i] == 2) {$val_first__append .= $arrOf_cols[$global_index]; $arrOf_cols[$global_index] = undef;}
		    $global_index--;
		}
	    }
	    #! Then add the remainind columsn to the set, ie, as we assuemt he 'ramainer' is also of interest. 
	    for(my $i = 0; $i < scalar(@arrOf_cols); $i++) {
		if(defined($arrOf_cols[$i])) {
		    push(@arrOf_altCol, $arrOf_cols[$i]);
		} #! else we ingore the column.
	    }
	    if(length($val_first__append)) {
		if(defined($config->{mapOf_toIgnore__rowPrefix})) {
		    $arrOf_altCol[0] = $config->{mapOf_toIgnore__rowPrefix} . $val_first__append; #! eg, for the case where we are itnerestined in 'simplifying' the dienificaiton of clsuter-groups.
		} else {
		    $arrOf_altCol[0] = $val_first__append; #! eg, for the case where we are itnerestined in 'simplifying' the dienificaiton of clsuter-groups.
		}
		my $prefix = $hashOf_uniqueGroups{$arrOf_altCol[0]};
		if(!defined($hashOf_uniqueGroups{$arrOf_altCol[0]})) {
		    $prefix =  $hashOf_uniqueGroups{$arrOf_altCol[0]} = $hashOf_uniqueGroups__cnt++;
		} 
		#! Set the prefix, a rpefix used as an 'iindex-reference' when we use the group-proerpty ofr a gold-clsuter-spec-proerpty':
		$arrOf_altCol[0] = $arrOf_altCol[0] . "_BR_" . $prefix;
	    }
	    #$arrOf_altCol[0] .= "_" . $val_first__append; #! ie, to 'e
	    #! Update the array:
	    @arrOf_cols = @arrOf_altCol;

	    
	    #printf("val_first__append: |$val_first__append| and [0]=|%s|, [last]=|%s|\n", $arrOf_cols[0], $arrOf_cols[scalar(@arrOf_cols)-1]);
	    #croak("FXIME: validate [ªbove]");
	}
	#! Ensure corrrect format of the columns:
	for(my $i = 0; $i < scalar(@arrOf_cols); $i++) {
	    $arrOf_cols[$i] =~ s/"//g;#  {
	    # if($arrOf_cols[$i] =~ /"(.+)"/) {
	    # 	$arrOf_cols[$i] = $1;
	    # }
	    if($arrOf_cols[$i] eq "NA") {
		$arrOf_cols[$i] = '-';
	    }
	}
	#printf("cnt-cols: %u, at %s:%d\n", scalar(@arrOf_cols), __FILE__, __LINE__);
	if(scalar(@arrOf_cols) != 0) {
	    if( ($row_id == 0) && $config->{bool__columnsAtRow0}) {
		shift(@arrOf_cols); #! ie, as we then assume the 'frist column' describes the row-id-column, ie, ignore. 
		printf($FILE_OUT_txt_meta "#! row-name:\t%s\n", join("\t", @arrOf_cols));	
		$colHeader__isAdded = 1;		
	    } else {
		my $row_name = undef;
		if(defined($config->{arrOf_names_rows})) {
		    my $cnt_names = scalar(@{$config->{arrOf_names_rows}});
		    if($row_id < $cnt_names) {
			$row_name = $config->{arrOf_names_rows}->[$row_id];
		    } else {
			printf(STDERR "!!\t Ivnestigate why the row_id='$row_id' isoutside the max-row-names-size='%'u, at %s:%d\n", $cnt_names, __FILE__, __LINE__);
		    }
		} elsif($config->{arrOf_names_rows__atIndex__first} == 1) {
		    $row_name = shift(@arrOf_cols); #! "Shifts the first value of the array off and returns it, shortening the array by 1" ("http://perldoc.perl.org/functions/shift.html")
		} elsif($config->{arrOf_names_rows__atIndex__last} == 1) {
		    $row_name = pop(@arrOf_cols); 
		}
		if(defined($row_name)) {
		    #printf("adds-row-name \"$row_name\"\n");
		    unshift(@arrOf_cols, $row_name);
		} elsif($colHeader__isAdded) {
		    #! then we add a 'sytntietc' row-name:
		    $row_name = sprintf("row(%u)", $row_id);
		    printf("adds-row-name \"$row_name\"\n");
		    unshift(@arrOf_cols, $row_name);
		}
		#!
		#! Add the values:
		printf($FILE_OUT_txt_meta "%s\n", join("\t", @arrOf_cols));
	    }
	} elsif(length($line) > 2) {
	    printf(STDERR "!!\t Ivnestigate why the following line did not have Any columns: %s\n", $line);
	}
	$row_id++;
    }
    close(FILE_IN);
    close($FILE_OUT_txt_meta);
}

#if(1 == 2)
{ #! Convert the iris-file:
    #! Src: "http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.names"
    my $config = __initConfig();
    $config->{seperator} = ',';
    #$config->{} = ;
    $config->{arrOf_names_cols} = [
	#! Src: "http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.names"
	"sepal-length (cm)",
	"sepal width (cm)",
	"petal length (cm)",
	"petal width (cm)",
	];
    #! Apply:
    my $file_name = "data/local_downloaded/iris.data";
    __convertFile($file_name, $config);
}
#if(1 == 2)
{
    #! Note: in [below] éxtract'the content from the "group" columns (eg, "control" and "obese") ... and use the latter as a clsuter-embmership ... while Not inlcuding the latter in the data-matrix itlse:
    my $config = __initConfig__spec__datavincentarelbundock({alt_clusterSpec => $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t->[0]}, {mapOf_toIgnore__columns => [0, 2]});
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HSAUR/phosphate.csv", $config);;
}
#if(1 == 2)
{ #! Convert the iris-file:
    #! Src: "http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.names"
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/airquality.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/attitude.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/crimtab.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/LifeCycleSavings.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/randu.csv", __initConfig__spec__datavincentarelbundock({k_clusterCount => 16}));
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/rock.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/swiss.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/datasets/USJudgeRatings.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/alcohol.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/ambientNOxCH.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/coleman.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/milk.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/NOxEmissions.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/pulpfiber.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/salinity.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/toxicity.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/robustbase/wood.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/boot/neuro.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/car/Ginzberg.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/car/Hartnagel.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/cluster/chorSub.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/cluster/votes.repub.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/COUNT/nuts.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/Airline.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/BudgetItaly.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/BudgetUK.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/Cigar.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/Cigarette.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/Crime.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/Electricity.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("./data/vincentarelbundock-Rdatasets-7133d7c/csv/Ecdat/Forward.csv", __initConfig__spec__datavincentarelbundock());


    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/gap/aldh2.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/gap/cf.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HistData/Arbuthnot.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HistData/Cavendish.csv", __initConfig__spec__datavincentarelbundock());

    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/lattice/environmental.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/MASS/Boston.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/MASS/gilgais.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/MASS/Melanoma.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/MASS/UScrime.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/plm/Hedonic.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/survival/bladder.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/survival/cancer.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/survival/pbc.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HSAUR/heptathlon.csv", __initConfig__spec__datavincentarelbundock());
    #! Note: in [below] éxtract'the content from the "group" columns (eg, "control" and "obese") ... and use the latter as a clsuter-embmership ... while Not inlcuding the latter in the data-matrix itlse:
    my $config = __initConfig__spec__datavincentarelbundock({alt_clusterSpec => $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t->[0]}, {mapOf_toIgnore__columns => [0, 2]});
	if(defined($config->{mapOf_toIgnore__columns})) {
	    printf("-------- ingore-a-se-tof-cols\n");
	}

    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HSAUR/phosphate.csv", $config);;
    # -- 
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HSAUR/pottery.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/HSAUR/smoking.csv", __initConfig__spec__datavincentarelbundock());

    #! Note: in [”elow] merge the first string-columns, ie, 'remove' both "Study" and "Film" ... and construct different data-set-cases where either "Study" OR "Film" is used as a 'gold-standard-hyptoitecal-clsuter-distriburtion'.... and then use the CCMs to assess the closenss of the predicited clsuters:
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/affect.csv", __initConfig__spec__datavincentarelbundock({alt_clusterSpec => $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t->[0]}, {mapOf_toIgnore__columns => [0, 1, 2]}));
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/affect.csv", __initConfig__spec__datavincentarelbundock({alt_clusterSpec => $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t->[0]}, {mapOf_toIgnore__columns => [0, 2, 1]}));
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/affect.csv", __initConfig__spec__datavincentarelbundock({alt_clusterSpec => $__spec__e_hp_clusterFileCollection__goldClustersDefinedBy_t->[0]}, {mapOf_toIgnore__columns => [0, 2, 2]}));
    #! ---- 
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/bfi.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/burt.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/epi.bfi.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/Holzinger.csv", __initConfig__spec__datavincentarelbundock());
    #! Note: use the last two columsn sepeatly as a cluster-base-case:
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/msq.csv", __initConfig__spec__datavincentarelbundock(undef, {mapOf_toIgnore__columns => [1, 2], mapOf_toIgnore__columns__isToEvaluateInReverseOrder => 1}));
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/msq.csv", __initConfig__spec__datavincentarelbundock(undef, {mapOf_toIgnore__columns => [2, 1], mapOf_toIgnore__columns__isToEvaluateInReverseOrder => 1}));
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/msq.csv", __initConfig__spec__datavincentarelbundock(undef, {mapOf_toIgnore__columns => [2, 2], mapOf_toIgnore__columns__isToEvaluateInReverseOrder => 1}));
    #! ---- 
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/psych/neo.csv", __initConfig__spec__datavincentarelbundock());


    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/quantreg/barro.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/Stat2Data/Fertility.csv", __initConfig__spec__datavincentarelbundock());
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/Stat2Data/MetabolicRate.csv", __initConfig__spec__datavincentarelbundock());
    #! Note: the last column (descirbing "Rural") as a nullhjyptoesis .... testing the implication/effect or "rural" wr.t healt-care (uisng the elvuated data-set):
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/Stat2Data/Nursing.csv", __initConfig__spec__datavincentarelbundock(undef, {mapOf_toIgnore__columns => [2], mapOf_toIgnore__columns__isToEvaluateInReverseOrder => 1, mapOf_toIgnore__rowPrefix => "rural_"}));
    #! Note: ingore column "Row" and "Col" ... and use Fert="0 = no, 1 = yes" as a null-hypotheis
    ## TODO: consider to 'also' ingore column "Row" and "Col" ... and use "Fert Indicator for fertilizer: 0 = no, 1 = yes" as a null-hypotheis
    __convertFile("data/vincentarelbundock-Rdatasets-7133d7c/csv/Stat2Data/Pines.csv", __initConfig__spec__datavincentarelbundock(undef, {mapOf_toIgnore__columns => [0, 2], mapOf_toIgnore__columns__isToEvaluateInReverseOrder => 1, mapOf_toIgnore__rowPrefix => "fertilizer_"}));


 #     __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
 #     __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
 # __convertFile("", __initConfig__spec__datavincentarelbundock());
}


#! -------------------------------------------------------------------------
#! GlobalTask: Complete:
close(GLOBAL__resultFile__meta);


#! -------------------------------------------------------------------------
