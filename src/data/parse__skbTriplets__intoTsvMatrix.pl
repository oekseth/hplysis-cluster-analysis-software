#!/usr/bin/perl

=head
    @brief convert a set of triplets into a simplifed data-set where indces represent strings (oekseth, 06. mar. 2017)
    @remarks 
    -- Idea: ide is generate two different matrices: (a) 'synonym-relations' and (b) 'ordinary' SKB-relations ... new Perl-script named "aux_relations_to_db.pl"
=cut


my %globalHash; my $globalHash__cnt = 0;

sub __getInternalIndex {
    my $name = lc(shift);
    my $value = $globalHash{$name};
    if(!defined($value)) {
	$value = $globalHash{$name} = $globalHash__cnt++;
    }
    #! @return
    return $value;
}
sub __getInternalIndex__predicate {
    my $name = shift;
    if($name =~/syn|union|assim/i) {
	return undef; #! ie, to 'state' taht the 
    }
    return __getInternalIndex($name);
}

#! A siple parser to convert an SKB-file into a set of triplets:
sub __parser {
    my($file_name_in, $file_name_result_rel, $file_name_result_syn) = @_;
    open(FILE_IN, "<$file_name_in") or die("Unable to open the input-file $file_name_in\n");
    open(FILE_OUT_rel, ">$file_name_result_rel") or die("Unable to open the input-file $file_name_result_rel\n");
    open(FILE_OUT_syn, ">$file_name_result_syn") or die("Unable to open the input-file $file_name_result_syn\n");
    my $cnt_rows = 0;
    while (my $line = <FILE_IN>) {
	chomp($line); #! ie, remvoe the trail-newline.    while (my $line = <FILE_IN>) {
	##chomp($line); #! ie, remvoe the trail-newline.
	# if($cnt_rows > 100) {last;} # TODO: remove.
	my @cols = split("\t", $line);
	if(scalar(@cols) && length($cols[0])) {
	    my($head, $rt, $tail) = (__getInternalIndex($cols[0]), __getInternalIndex__predicate($cols[1]), __getInternalIndex($cols[2]));
	    if(defined($rt)) {
		printf(FILE_OUT_rel "%d\t%d\t%d\n", $head, $rt, $tail);
	    } else {
		printf(FILE_OUT_syn "%d\t%d\n", $head, $tail);
	    }	    
	}
	$cnt_rows++;
    }
    close(FILE_IN);
    close(FILE_OUT_rel);
    close(FILE_OUT_syn);
    { #! Generate a mapping-file 'between the strings and the integers':
	my $file_name_result_syn = $file_name_result_syn . ".mapping.tsv";
	open(FILE_OUT_syn, ">$file_name_result_syn") or die("Unable to open the input-file $file_name_result_syn\n");
	printf("generate mappings \"$file_name_result_syn\"\n");
	foreach my $key (keys(%globalHash)) {
	    printf(FILE_OUT_syn "%s\t%d\n", $key, $globalHash{$key});
	}
	close(FILE_OUT_syn);

    }
}

{ #! Initalise, ie, to 'set some relation-tyeps to a fixed index', terheby simpliying our 'writign' of example-use-cases:
    foreach my $pred ("is_annotated_by_ntc", "activates", "regulates", "inhibits", "activation", "positively_regulates", "negatively_regulates") {
	__getInternalIndex__predicate($pred);
    }
    foreach my $node ("protein", "gene", "pathway", "enzyme") {
	__getInternalIndex($pred);
    }
} 

#! A call:
my $input_file = "data/relations.tmp.tab.txt"; #! which may recived if a request is sent to the senior devleoper [oesketh@gmail.com].
#my $input_file = "../../../knittingTooolsInterface/result/relations.tmp.tab.txt"; #! which may recived if a request is sent to the senior devleoper [oesketh@gmail.com].
__parser($input_file, "data/skb_large.rel.tsv", "data/skb_large.syn.tsv");

