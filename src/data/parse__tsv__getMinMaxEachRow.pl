#!/usr/bin/perl -w
use strict; 

my $fileName_global_meta = "meta_fromCollection.tsv";
open(FILE_OUT_global_meta, ">$fileName_global_meta") or die("An error in opning \"$fileName_global_meta\"");

my $hashOf_prevScores = {};
sub __reset_matrix() {
    $hashOf_prevScores = {};
}

sub __initRow {
    return {
	id => undef, 
	min => {
	    #index => undef, #! ie, the local id of "score", eg, the column-index for the clsuter-algorithm.
	    index_string => undef, #! ie, the string-repsentaiotn of the "index", eg, the name of the clsuter-algorithm.
	    score => undef, #! eg, the CCM-score
	    key_aux => undef, #! eg, teh siamrlity-emtirc-id.
	},
	max => {
	    #index => undef, #! ie, the local id of "score", eg, the column-index for the clsuter-algorithm.
	    index_string => undef, #! ie, the string-repsentaiotn of the "index", eg, the name of the clsuter-algorithm.
	    score => undef, #! eg, the CCM-score
	    key_aux => undef, #! eg, teh siamrlity-emtirc-id.
	},
    };
}

sub __updateScore {
    my($stringOf_row, $stringOf_col, $score, $updateFor_min) = @_;
    #!
    #! Searhc for the string:
    my $isFound = 0;
    if(defined($hashOf_prevScores->{$stringOf_row})) {
	my $key = $stringOf_row;
#    foreach my $key (keys(%{$hashOf_prevScores})) {
	my $obj = $hashOf_prevScores->{$key};
	#if($obj->{id} eq $stringOf_row) {
	#if($obj->{id} eq $stringOf_row) {
	if($updateFor_min) {
	    
	} else {
	    
	}
	#! -----------------------------
	    $isFound = 1; 
#    }
    }
    if($isFound == 0) { #! then add the etntiy:
	my $obj = __initRow();
	$hashOf_prevScores->{$stringOf_row}  = $obj;
	if($updateFor_min) {
	    $obj->{min}->{index_string} = $stringOf_col;
	    $obj->{min}->{score} = $score;
	} else {
	    $obj->{max}->{index_string} = $stringOf_col;
	    $obj->{max}->{score} = $score;	    
	}	
    }
}


sub compute {
    my ($input_file, $describe_idList) = @_;
    my($name, $suffix) = ($input_file =~/^(.+)\.([a-z]+)$/);
    if(!defined($suffix)) { # || ($suffix ne "tex")) {
	printf(STDERR "!!\t Expected a latex-file as inptu, ie Not the case given inptu-file=\"$input_file\" and suffix=\"$suffix\": aborts\n");
	exit;
    }
    printf("-----------------\nfile-name=\"%s\", at %s:%d\n", $input_file, __FILE__, __LINE__);
#! 
#! Construct reulst-file-names:
    my $result_tex = $name . "_result.tex";
    open(FILE_IN, "<$input_file") or die("An error in opning");
    open(FILE_OUT_tex, ">$result_tex") or die("An error in opning");
    my $result_tex_relative = $name . "_result.relative.tex";
    open(FILE_OUT_tex_relative, ">$result_tex_relative") or die("An error in opning");
#! 
#! Parse the data-set: 
    my $line_count = 0;
    my @arrOf_x_value; my @arrOf_rowHead;
    my @matrix;
    my @mat_col_row;
    my $nrows = 0; 
    while (my $line = <FILE_IN>) {
	chomp($line); #! ie, remvoe the trail-newline.    while (my $line = <FILE_IN>) {
	my @arrOf_cols = split("\t", $line);
	if(scalar(@arrOf_cols) > 1) {
	    if($line_count == 0) {
		my $head = shift(@arrOf_cols);
		@arrOf_rowHead = @arrOf_cols;		
		#!
		#! Write otu the file-anems to the meta-file, eg, to simplify docuemtantion-efforts:
		printf(FILE_OUT_global_meta "\n\n The file=\hpFile{%s} has meta-headers: \n", $input_file);
		my $index = 0;
		foreach my $id (@arrOf_cols) {
		    printf(FILE_OUT_global_meta "index[%u]=\"%s\", \n", $index, $id);
		    $index++;
		}
	    } else {
		my $head = shift(@arrOf_cols);
		push(@arrOf_rowHead, $head);
		#push(@arrOf_x_value, $head);
		# printf("for head=\"$head\" set-cols: %s\n", join(",", @arrOf_cols));
		#push(@matrix, \@arrOf_cols);
		push(@mat_col_row, \@arrOf_cols);
		if($nrows < scalar(@arrOf_cols)) {
		    $nrows = scalar(@arrOf_cols);
		}
	    }
	}
	$line_count++;
    }
#my $ncols = scalar(@matrix);
    my $ncols = scalar(@mat_col_row);
    printf("ncols=$ncols\n"); 
    { #! Initate matrix:
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my @row = ();
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		#! Insert into the matrix:
		push(@row, $mat_col_row[$col_id][$row_id]);
	    }
	    push(@matrix, \@row);
	}
    }
    close(FILE_IN);
    close(FILE_OUT_tex);
    close(FILE_OUT_tex_relative);
}

# ---------
__reset_matrix();
#compute("");  #! cosntructed from:
#compute("");

# ---------
#!
#! Close global apramters:
close(FILE_OUT_global_meta);
