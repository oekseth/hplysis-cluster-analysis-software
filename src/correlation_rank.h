#ifndef correlation_rank_h
#define correlation_rank_h

#include "correlation_sort.h"

  //! @return the rank of the assicated data data-poiner.
  t_float* getrank (const uint n, const t_float *data, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse /* = e_distance_rank_typeOf_computation_ideal */);
//! @return a rank-list for a given vector-row (oekseth, 06. des. 2016).
static t_float* get_rank__correlation_rank(const uint n, const t_float *data) {
  return getrank(n, data, e_distance_rank_typeOf_computation_ideal);
}
//! @return a rank-list for a given vector-row (oekseth, 06. des. 2016).
static uint *get_rank__uint__correlation_rank(const uint n, const uint *data__uint) {
  // TODO: consider writing a differnet funciton 'for thsi' if 'this' becomes performance-snseityive (oekseth, 06. feb. 2017).
  t_float empty_0 = 0;
  t_float *data = allocate_1d_list_float(n, empty_0);
  for(uint i = 0; i < n; i++) {data[i] = (t_float)data__uint[i];}
  t_float *ranks = getrank(n, data, e_distance_rank_typeOf_computation_ideal);
  if(ranks) {
    uint *ranks__uint = allocate_1d_list_uint(n, empty_0);
    for(uint i = 0; i < n; i++) {ranks__uint[i] = ranks[i];}
    free_1d_list_float(&data);
    free_1d_list_float(&ranks);    
    //! @return ranks:
    return ranks__uint;
  } else {
    free_1d_list_float(&data);
    return NULL;
  }
}

//! @return a rank-list for a given vector-row (oekseth, 06. des. 2016).
static t_float *get_rank__float__correlation_rank(const uint n, const t_float *data__float) {
  // TODO: consider writing a differnet funciton 'for thsi' if 'this' becomes performance-snseityive (oekseth, 06. feb. 2017).
  t_float empty_0 = 0;
  t_float *data = allocate_1d_list_float(n, empty_0);
  for(uint i = 0; i < n; i++) {data[i] = data__float[i];}
  t_float *ranks = getrank(n, data, e_distance_rank_typeOf_computation_ideal);
  free_1d_list_float(&data);
  
  return ranks;
}

//! @return the rank of the assicated data data-poiner.
void getrank_memoryRe_use(const uint n, const t_float *arr_input, t_float *arr_result, uint *tmp_index, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse/* = e_distance_rank_typeOf_computation_ideal*/, t_float *optional_arrOf_sorted/*= NULL*/);
//! Identify the rank for a given vector, using a matrix as 'input'.
  void ktCorrelation_compute_rank_forVector(const uint index1, const uint size, t_float **data, char **mask, t_float *tmp_array,  uint *tmp_array_index,  t_float *result_array, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation, t_float *optional_arrOf_sorted);
//! Identify the rank for a given vector, using a transposed matrix as 'input'.
  void ktCorrelation_compute_rank_forVector_firstTranspose(const uint index1, const uint size, t_float **data, char **mask, t_float *tmp_array, uint *tmp_array_index, t_float *result_array, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation, t_float *optional_arrOf_sorted);


//! Extends "ktCorrelation_compute_rankFor_matrices(..)" with an option "onlyCompute_data1_index" to only comptue for a particular index in "data1_input": for the latter iniput we expect that the data is transpsoed (oekseth, 06. setp. 2016).
//! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
void ktCorrelation_compute_rankFor_matrices_andVector(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char** mask1, char** mask2, t_float ***matrix_tmp_1_, t_float ***matrix_tmp_2_, const uint transpose, const bool needTo_useMask_evaluation, const uint onlyCompute_data1_index, const uint iterationIndex_2);

  //! Allocate memory and compute the rank for the matrices.
  //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
  void ktCorrelation_compute_rankFor_matrices(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char** mask1, char** mask2, t_float ***matrix_tmp_1, t_float ***matrix_tmp_2, const uint transpose, const bool needTo_useMask_evaluation, const uint iterationIndex_2);

#endif //! EOF
