  //! --------------------------------------------
  //! 
  //! Apply the filtering:
  const s_kt_matrix_t *matrix_input_1 = obj_1__;
  const s_kt_matrix_t *matrix_input_2 = (obj_2__ != NULL) ? obj_2__ : obj_1__;
  s_kt_matrix_t obj_cpy_ranksEachRow_1; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow_1);
  s_kt_matrix_t obj_cpy_ranksEachRow_2; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow_2);
  if(isTo_applyPreFilters) {
    printf("(info)\t Applies pre-filters, at %s:%d\n", __FILE__, __LINE__);
    bool isTo_transposeMatrix = self->config.clusterConfig.isTo_transposeMatrix;
    __applyPosteriori_filters__hpLysis_api(self, &matrix_input_1, &obj_cpy_ranksEachRow_1, isTo_transposeMatrix, self->config.config__valueMask);
    if(obj_2__ && (obj_2__ != obj_1__) ) {
      __applyPosteriori_filters__hpLysis_api(self, &matrix_input_2, &obj_cpy_ranksEachRow_2, isTo_transposeMatrix, self->config.config__valueMask);
    }
    isTo_transposeMatrix = false; //! ie, as we expect htis to have been 'handled' in [above]
  }

  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  // printf("# Compute correlationi-matrix, at %s:%d\n", __FILE__, __LINE__);
  assert(matrix_input_1->matrix);
  assert(matrix_input_2->matrix);
  s_kt_matrix_t *obj_result = &(self->matrixResult__inputToClustering);
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //t_float *matrixInput__weight_1 = NULL;   t_float *matrixInput__weight_2 = NULL;
  { //! Valdiate thaat the weight-tables (if used) are Not all set to 'emaingless' valeus (ie, to detect errors in use):
    if(matrix_input_1 && matrix_input_1->weight) { 
      assert(matrix_input_1->ncols);
      uint cnt_zero = 0; uint cnt_extreme_min = 0; uint cnt_extreme_max = 0; uint cnt_1 = 0;
      for(uint col_id = 0; col_id < matrix_input_1->ncols; col_id++) {
	const t_float score = matrix_input_1->weight[col_id];
	if(score == 0) {cnt_zero++;}
	else if(score == T_FLOAT_MIN_ABS) {cnt_extreme_min++;}
	else if(score == T_FLOAT_MAX) {cnt_extreme_max++;}
	else if(score == 1) {cnt_1++;}
      }
      if(cnt_1 == matrix_input_1->ncols) { //! then we assuem the 'weight is Not needed'.
	fprintf(stderr, "(get-performance-increase)\t In your configuration we observe you have set all weights to '1', ie, a pointless call, ie, a call whcih introduce an unnceccessary complexity in your call). What we reccoment is to Not use a weight-table (in your input), thereby reducing the exueciotn-tiem of your operation (while keeping your rsults higly accurate). To summarsie please update your API call. For quesiton please cotnact the seionor developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	//matrixInput__weight_1 = matrix_input_1->weight;
	//matrix_input_1 = matrix_input_1->weight;
	//matrix_input_1->weight = NULL; 
      } else {
	const uint sum_empty = (cnt_zero + cnt_1 + cnt_extreme_min + cnt_extreme_max);
	if(sum_empty == matrix_input_1->ncols) {
	  fprintf(stderr, "!!\t Seems like all weigths are set to 'not-to-use' values, ie, please validate correctness of your call: indicates errornous use of your configuration-API. For quesiton please cotnact the seionor developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	  return false;
	}
      }
    }
    //! -----------------------------
    //! 
    if(matrix_input_2 && (matrix_input_1 != matrix_input_2) && matrix_input_2->weight) { 
      assert(matrix_input_2->ncols);
      uint cnt_zero = 0; uint cnt_extreme_min = 0; uint cnt_extreme_max = 0; uint cnt_1 = 0;
      for(uint col_id = 0; col_id < matrix_input_2->ncols; col_id++) {
	const t_float score = matrix_input_2->weight[col_id];
	if(score == 0) {cnt_zero++;}
	else if(score == T_FLOAT_MIN_ABS) {cnt_extreme_min++;}
	else if(score == T_FLOAT_MAX) {cnt_extreme_max++;}
	else if(score == 1) {cnt_1++;}
      }
      if(cnt_1 == matrix_input_2->ncols) { //! then we assuem the 'weight is Not needed'.
	fprintf(stderr, "(get-performance-increase)\t In your configuration we observe you have set all weights to '1', ie, a pointless call, ie, a call whcih introduce an unnceccessary complexity in your call). What we reccoment is to Not use a weight-table (in your input), thereby reducing the exueciotn-tiem of your operation (while keeping your rsults higly accurate). To summarsie please update your API call. For quesiton please cotnact the seionor developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	/* matrixInput__weight_2 = matrix_input_2->weight; */
	/* matrix_input_2->weight = NULL;  */
      } else {
	const uint sum_empty = (cnt_zero + cnt_1 + cnt_extreme_min + cnt_extreme_max);
	if(sum_empty == matrix_input_2->ncols) {
	  fprintf(stderr, "!!\t Seems like all weigths are set to 'not-to-use' values, ie, please validate correctness of your call: indicates errornous use of your configuration-API. For quesiton please cotnact the seionor developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	  return false;
	}
      }
    }

  }

