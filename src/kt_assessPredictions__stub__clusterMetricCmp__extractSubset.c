/**
   @brief builds a subset usign 'naive emtics' for a speicfic data-set:
 **/


uint row_id_global = 0;  uint __obj__localMatrix__defaultMetrics_cntRows = 0; 
s_kt_matrix_t __obj__localMatrix__defaultMetrics; 
#ifndef __internalDataUpdate__transposeOrder
init__s_kt_matrix(&__obj__localMatrix__defaultMetrics, matrixDimensionsCnt__subsetSet__default_cntRows, /*ncols=*/matrixDimensionsCnt__cols, /*isTo_allocateWeightColumns=*/false);
//fprintf(stderr, "tranpsose=false, __obj__localMatrix__defaultMetrics_cntRows=%u, at %s:%d\n", __obj__localMatrix__defaultMetrics_cntRows, __FILE__, __LINE__);
#else
init__s_kt_matrix(&__obj__localMatrix__defaultMetrics, matrixDimensionsCnt__cols, /*ncols=*/matrixDimensionsCnt__subsetSet__default_cntRows, /*isTo_allocateWeightColumns=*/false);
//fprintf(stderr, "tranpsose=true,  __obj__localMatrix__defaultMetrics_cntRows=%u, at %s:%d\n", __obj__localMatrix__defaultMetrics_cntRows, __FILE__, __LINE__);
#endif //! (__internalDataUpdate__transposeOrder)
assert(__obj__localMatrix__defaultMetrics.nrows > 0);
assert(__obj__localMatrix__defaultMetrics.ncols > 0);

//fprintf(stderr, "## matrix=[%u, %u], at %s:%d\n", __obj__localMatrix__defaultMetrics.nrows, __obj__localMatrix__defaultMetrics.ncols, __FILE__, __LINE__);

#define __localConfig__isToApplyLogics 2 

//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
#include "kt_assessPredictions__stub__clusterMetricCmp__buildMatrix.c"
//fprintf(stderr, "ok: at %s:%d\n", __FILE__, __LINE__);
#undef __localInputMatrix    
    //! What we expect:
//fprintf(stderr, "nrows=%u, nrows-expected=%u, matrixDimensionsCnt__subsetSet__default_cntRows=%u, for base_string=\"%s\", at %s:%d\n", __obj__localMatrix__defaultMetrics_cntRows, __obj__localMatrix__defaultMetrics.nrows, matrixDimensionsCnt__subsetSet__default_cntRows, base_string, __FILE__, __LINE__);
assert(__obj__localMatrix__defaultMetrics_cntRows > 0); 
assert(row_id_global > 0); 
#ifndef __internalDataUpdate__transposeOrder
assert(__obj__localMatrix__defaultMetrics_cntRows == __obj__localMatrix__defaultMetrics.nrows);
#else 
assert(__obj__localMatrix__defaultMetrics_cntRows == __obj__localMatrix__defaultMetrics.ncols);
#endif //! (__internalDataUpdate__transposeOrder)
assert(__obj__localMatrix__defaultMetrics_cntRows <= row_id_global);
//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
{
  //! Export the matrix:
  char string_local[2000]; sprintf(string_local, "%s.subset_naiveMetrics.tsv", base_string);
  printf("# Export: to the result-file=\"%s\", at %s:%d\n", string_local, __FILE__, __LINE__);      
  export__singleCall__s_kt_matrix_t(&__obj__localMatrix__defaultMetrics, string_local, fileHandler__global);

#ifdef __clusterConfig__k_cluster
  const uint __maxThresh_performance = 1000;  
  //!
  //! Frist correate+cluster+exprot for the non-transposed:
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  if( (__obj__localMatrix__defaultMetrics.nrows < __maxThresh_performance) &&  (__obj__localMatrix__defaultMetrics.ncols < __maxThresh_performance) ) {
    char string_local__corr_prior[2000]; sprintf(string_local__corr_prior, "%s.subset_naiveMetrics.correlationBeforeClust.tsv", base_string);
    char string_local__corr[2000]; sprintf(string_local, "%s.subset_naiveMetrics.afterClust.tsv", base_string);
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
 s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(string_local__corr, fileHandler__global);
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Cluster:
    const bool is_ok = cluster__hpLysis_api(&hp_config, e_hpLysis_clusterAlg_kCluster__AVG, &__obj__localMatrix__defaultMetrics, /*k-cluster=*/__clusterConfig__k_cluster, /*npass=*/1000);
    assert(is_ok);

    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);
  }
  //!
  //! Thereafter correate+cluster+exprot for the transposed:
  if( (__obj__localMatrix__defaultMetrics.nrows < __maxThresh_performance) &&  (__obj__localMatrix__defaultMetrics.ncols < __maxThresh_performance) ) {
    char string_local__corr_prior[2000]; sprintf(string_local__corr_prior, "%s.subset_naiveMetrics.correlationBeforeClust.transposed.tsv", base_string);
    char string_local__corr[2000]; sprintf(string_local__corr, "%s.subset_naiveMetrics.afterClust.transposed.tsv", base_string);
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(string_local__corr, fileHandler__global);
    hp_config.config.clusterConfig.isTo_transposeMatrix = true;
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
    //!
    //! Cluster:
    const bool is_ok = cluster__hpLysis_api(&hp_config, e_hpLysis_clusterAlg_kCluster__AVG, &__obj__localMatrix__defaultMetrics, /*k-cluster=*/__clusterConfig__k_cluster, /*npass=*/1000);
    assert(is_ok);

    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);
  }
#endif
}
free__s_kt_matrix(&__obj__localMatrix__defaultMetrics);
__obj__localMatrix__defaultMetrics_cntRows = 0; //! ie, to reset.


//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
//! -----------------
