#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_evalHypothesis_algOnData.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "hpLysis_api.h" //! which is used for intiating and de-allcoating the MINE-optmized-dat-astructures.
//#include "aux_sysCalls.h"
/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks examplifeis appliciaotn of two CCMs, mulipel clsuter-algorithms and and muliple data-sets.
   @remarks may be seen as an evlauation-script which 'rpoduce' a [iteration x clusterAlg=[none, hcaBefore]x[kmeans, kmedoid]][data_out] for different rela-life example-sets ... produce a CCM-result-amtrix which may be sued as input to a "highcharts" data-set. ... 
   @remarks a motvaiton 'behidn this script' is to investigate resutl-accracy wrt. the appraoch to compute hca-k-means, ie, for which we evlauate a number of data-sets-files. In this example we dmonstrate how HCA-pre-step signicntly improves the result-accruacy and exeuciton-time for several cases data-input-cases. 
   @remarks results: (among others) compare different "k-means++" impmenetaiton-strategies, eg, wrt. the exeuction-time-effect and and result-accuracy of different param,eters wrt. [sim-metric-preStep, randomizaiton-before-clusterAlg, clusterAlg, ranodmziaiotn-inside-clsuter-alg, cluster-centroid-identivicaiton, etc.]. 
   @remarks Demonstrates features wrt.:
   -- input: merge two 'matemcialcal' fucntiosn into one matrix
   -- computation(a): HCA+kmeans: use of two differnet permtautiosn for the same 'main algorithm' call, wher ehte latter is described by "clustAlg"
   -- computation(b): "kmeans++": for k-means algorithms use tow different randomenss-intlaitions, ie, to examplify the 'initaiton' and applciaiton fo different k-means permtautions.
   -- computation(c): CCM: (a) idneitfy the clust-result-accuracy through applicoatn ot two speciifc/defiend clsuter-resutls and then (b) evaluate the simlairty-matrices (cosnisteing of CCM-scores) through applicioant of CCMs and 'inside-for-loop-consturcted' golst-stadnard-null-hypothesis;
   -- result(a): how results from mmuliple clusterings may be exported into one single-unified java-script file.
   -- result(b): write out the CCM-score for a givne comptaution.
   -- result(c): write out the CCM-scores wrt. the CCM-matrics simlairty with different gold-standard-hypothesis;
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  const bool isTo__testHCA_komb__inKmeans = false;
  const char *stringOf_resultDir = "";
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************

#endif


  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------


#if(__Mi__useSmall == 1)
const uint arg_npass = 1000;
const uint cntIterations__iterativeAlgs = 10;
const uint setOfAlgsToEvaluate__size = 3 + 4;
const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  /*     ---    */
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
};
 const uint sizeOf__nrows = 100;
 const uint sizeOf__ncols = 100;
#else
const uint arg_npass = 10000;
const uint cntIterations__iterativeAlgs = 5;
const uint setOfAlgsToEvaluate__size = 9;
const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  // --- 
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
};
// TODO: try to incrmenetally icnrase [below]:
 const uint sizeOf__nrows = 10; const uint sizeOf__ncols = 10;
//const uint sizeOf__nrows = 100; const uint sizeOf__ncols = 100;
#endif
  const bool config__isTo__useDummyDatasetForValidation = false; //! a aprameter used to reduce the exeuction-tiem when 'rpoking' assert(false) and memory-errors (oekseth, 06. feb. 2017).
  if(config__isTo__useDummyDatasetForValidation == true) {
    fprintf(stderr, "!!\t Uses a dummy data-set for validation, ie, where the reuslt of this call is pointless: do Not use this option if you are interested in clsuter-results (and Not dummy exzeuciton-time emasurements), an observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
 const uint setOfRandomNess_in_size = 4;
 const e_kt_randomGenerator_type_t setOfRandomNess_in[setOfRandomNess_in_size] = {
  e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random,
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss,
  e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss
 };
 const uint setOfRandomNess_out_size = 7;
 const e_kt_randomGenerator_type_t setOfRandomNess_out[setOfRandomNess_out_size] = {
  e_kt_randomGenerator_type_hyLysisDefault,
  e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
  e_kt_randomGenerator_type_posixRan__srand__time__modulo,
  e_kt_randomGenerator_type_binominal__incrementalInList,
  //! Different 'explicit' procuedrew chih makes use of 'partial randomness' to idneitfy clusters, ie, (a) 'randomness for a subset ov ertices' adn thereafter (b) 'set clsuter-embership to the closest cadnidate/random-assignec-sltuer-vertex':
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random,
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss,
  e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss
 };

const uint arrOf_simAlgs_size = 2;
const e_kt_correlationFunction_t arrOf_simAlgs[arrOf_simAlgs_size] = {
  e_kt_correlationFunction_groupOf_MINE_mic,
  e_kt_correlationFunction_groupOf_minkowski_euclid,
};

  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  // printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! We are interested in a more performacne-demanind approach: 

  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
//s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();




#if(__Mi__useSmall == 1)
  const uint mapOf_realLife_size = 6;
#elif(__Mi__dataCaseLarge != 2)
  const uint mapOf_realLife_size = 6 + 11 + 3; //! ie, the number of data-set-objects in [”elow]
#else //! then we use a 'large data-set-case' wrt. "IRIS":
  const uint mapOf_realLife_size = 4; //! ie, "linear-differentCoeff-b" + [iris, iris, iris]
#endif
  assert(fileRead_config__syn.fileIsRealLife == false);
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    // FIXME[article]: update our aritlce wrt. [”elow] 'note' tags/descipritons.
    // FIXME[article]:  ... 
    // FIXME[article]:  ... 
    //! -------------------------------------------------- 
    //! Note[<syntetic> w/cnt=6]: .... 
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,     metric__beforeClust, metric__insideClust, clusterAlg
     },
    //#endif
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
#if(__Mi__useSmall == 0)
#if(__Mi__dataCaseLarge != 2)
    //! -------------------------------------------------- 
    //! 
    //! 
    //! Note[<real-life-small-data-sets> w/cnt=11]: 
    //! ---- 
    //! cnt_clusters=2, size=15; seperation=ambiguous-overlapping; Trait="curved, intersecting"; Euclid oversimplifies/'extremifies' the speration, while canberra is uanble to capture the 'curved relationships': the other metrics are unable to capture the relationjships. 
    {/*tag=*/"birthWeight_chinese", /*file_name=*/"tests/data/kt_mine/birthWeight_chineseChildren.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ... cnt_clusters=1, size=; seperation=periodic; Trait="curved, intersecting"; Euclid 
    {/*tag=*/"butterFat_cows", /*file_name=*/"tests/data/kt_mine/butterFat_percentage_cows.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! cnt_clusters=1, size=11; seperation='well-defined'; Trait="curved, x-axis-start-point differs"; Euclid 
    {/*tag=*/"fishGrowth", /*file_name=*/"tests/data/kt_mine/fish_growth.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ... cnt_clusters=2, size=4; seperation=ambiguous; Trait="curved, fluctating, discrete-sinus"; Euclid 
    // FIXME: consider using [below] as an example of 'periodic clsuters which are diicuflt to clsuter'.
    // FIXME: consider to transpose [below] .... figure out how to 'hanlde' the case where 'a ranking' would result in isngificnat seperation 
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ------ 
    //! 
    //! "Spottinggrade": cnt_clusters=2, size=4; seperation='change VS not-change, ie, a non-linear comparison'; Trait="curved, intersecting"; ....??... <-- asusmes other metrics will find thios challenging
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
  //! ... cnt_clusters=2, size=3; seperation='ambigious: silamirlty in curve-increase'; Trait="curved, simliar-peaks"; 
    {/*tag=*/"guinePigs", /*file_name=*/"tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_housed_pregnancy_first.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! "limesOxygen": if transposed nearly-all (ie, xmt. "species 2, 75 seawater") seems to have a simlar flucation
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ------ 
    //! 
    //! "Weedlength": cnt_clusters=1, size=~20; seperation='clear: samve dsicrete sinus-vurve wrt. flcutaitons ... Metrics: none of the compared metric manages to idneitfy/describe this relationship
    {/*tag=*/"weedLength", /*file_name=*/"tests/data/kt_mine/weed_length.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //{/*tag=*/"", /*file_name=*/"", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! -------------------------------------------------- 
    //! Note[]: 
    //! -------------------------------------------------- 
    //! Note[iris]: the results produced by/through MINE results in a considerably better/higher CCM-score (when comapred to Euclid), an observation which is consernsent between/across k-means-pmeruatiosn such as [mean, rank, medoid]. However, a challenge/sekenss wrt. the evlauatin of the IRIS data-set conserns the apriory-interpetaiton/expectaiton (where the researhcers states/assuems tha tehre are three clsuters i IIRS, while currnet cluster-metrics/CCMs indicates that the latter is Not supported), an observaiotn which may imply that (when considering real-lfie dat-asets) we ....??... <--- try to use the altter observiaotn as a jumping-store to ....??... 
    //! Note[iris]: k-means: "rank" VS "avg": while the use of a simlairty-pre-step results in the "avg" and "rank" to 'produce' exaclty the same results, the results (converted to sclar-numbers through CCMs) indicates that the algorithsm 'produce' a 2x-difference (when comptuign sepreately for "rank" and "avg" while Not using/appliying an Eculidcian sim-metric-pre-step). In brief, when comparing/evaluating the dfiferences between k-means with--withouth the 'correlation-pre-step', we clearly observe that the clsuter-algrotihms are stronlgy influence by any 'data-normalizaiton-steps' (illustrated in the latter through our applicaiton/use of Euclid as a simalrity-pre-step appraoch). 
    //! Note[iris]: ..     
    //! Note[iris, cluster="kMeans--[AVG, rank, emdoid]"]: metric="Mine": clusters are in either group(0) OR group(1); metric="Euclid": result simialr to "scikit-learn" (ie, where group(0) in MINE is exaclty simliar, while group(1 =~ 2) (a result which is simlair to the 'defualt answer'); <-- FIXME: try expalinign/arguing for why the MINE-impelmetnation may be correct.
#endif
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/4, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //{/*tag=*/, /*file_name=*/, /*fileRead_config=*/fileRead_config,/*k_clusterCount=*/, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/, /*metric__insideClust=*/, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_},
    //! Note[iris, cluster="kMeans--medoid"]: metric="Mine": ... ; metric="Euclid": ... 
    //! Note[iris, cluster=""]: metric="Mine": ... ; metric="Euclid": ... 
    //! Note[iris, cluster=""]: metric="Mine": ... ; metric="Euclid": ... 
    //! -------------------------------------------------- 
#endif
  };

  //! ---------------------------------------
  //!
  //! The 'call':
  s_hp_evalHypothesis_algOnData_t obj_eval = init__s_hp_evalHypothesis_algOnData_t(mapOf_realLife, mapOf_realLife_size);
  obj_eval.isTo__testHCA_komb__inKmeans = isTo__testHCA_komb__inKmeans;
  obj_eval.arg_npass = arg_npass;
  obj_eval.sizeOf__nrows = sizeOf__nrows;
  obj_eval.sizeOf__ncols = sizeOf__ncols;
  obj_eval.cntIterations__iterativeAlgs = cntIterations__iterativeAlgs;
  obj_eval.config__isTo__useDummyDatasetForValidation = config__isTo__useDummyDatasetForValidation;
  obj_eval.setOfAlgsToEvaluate__size = setOfAlgsToEvaluate__size;
  obj_eval.setOfAlgsToEvaluate = setOfAlgsToEvaluate;
  obj_eval.setOfRandomNess_in_size = setOfRandomNess_in_size;
  obj_eval.setOfRandomNess_in = setOfRandomNess_in;
  obj_eval.setOfRandomNess_out_size = setOfRandomNess_out_size;
  obj_eval.setOfRandomNess_out = setOfRandomNess_out;
  obj_eval.arrOf_simAlgs_size = arrOf_simAlgs_size;
  obj_eval.arrOf_simAlgs = arrOf_simAlgs;
  //!
  //! Apply logics:
  const bool is_ok = apply__s_hp_evalHypothesis_algOnData_t(&obj_eval, stringOf_resultDir);
  assert(is_ok);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
