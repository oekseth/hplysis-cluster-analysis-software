#include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hp_ccm.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"
#include "hp_clusterShapes.h"
#include "hp_clusterFileCollection.h"
/* #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy */
/* #include "export_ppm.c" //! which is used for *.ppm file-export. */
/* #endif */

#include "kt_resultS_ccmTime.h" //! which provide lgocis for storing and exporting a combaiotn of time-matrix and clsuter-quality (CCM) matrix.
//#include "kt_resultS_ccmTime.h"

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief  compute [data-id][sim-id] = max(ccmScore(ccm-id, cluster-result)) wrt. sim-id, gold-ccm, matrix-ccm (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks to store both predion-accurayc and exueciont-time, when evlauating a data-set [data-id][configation-id].
   -- we identify the best simalrity-emtrics to be sued for a given data-set.
   @remarks related-tut-examples:
   -- "tut_kd_3_data_simMetric.c". 
   -- "hp_evalHypothesis_algOnData.c": an API for data-anlaysis.
   -- "tut_kd_1_cluster_multiple_simMetrics.c": min-max-evaluation-accuracy wrt. CCMs and simalrityu-emtrics.
**/
int main(const int array_cnt, char **array) 
#else
  int tut_time_1_data_syntetic_wellDefinedClusters(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  { //! Investigate for different 'base' CCM metrics: 
    const char *resultPrefix = "tut_time_1";
    { //! matrix-CCMs: 
      //! Note: capture the isolatoeted time-cost assotied to CCM-cotmpatuion, ie, for which cCM-comptaution is not invovled/integrated. 

      const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
      //      const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
      const uint nrows_increase = 100; const uint cnt_blocks = 20;
      //      const uint nrows_increase = 1000; const uint cnt_blocks = 100;

      //!
      //! Iterate through cases wrt. matrix-cluster-seperation:
      for(uint case_id = 0; case_id < cnt_cases; case_id++) {
	printf("case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
	//!
	//! Iterate through cases wrt. relative cluster-ocunt-inlfucne
	for(uint clusterCase = 0; clusterCase < cnt_clusterCases; clusterCase++) {
	  //!
	  //! Allcoate result matrices: 
	  s_kt_resultS_ccmTime_t obj_ccm_matrix = init__s_kt_resultS_ccmTime_t();
	  s_kt_resultS_ccmTime_t obj_ccm_matrix_config = init__s_kt_resultS_ccmTime_t();
	  s_kt_resultS_ccmTime_t obj_ccm_gold = init__s_kt_resultS_ccmTime_t();
	  s_kt_resultS_ccmTime_t obj_ccm_clusterAlg = init__s_kt_resultS_ccmTime_t();
	  //	s_kt_resultS_ccmTime_t obj_ = init__s_kt_resultS_ccmTime_t();
	  
	  const char *fileTag = "";	
	  //! Construct a givne matrix: 
	  for(uint block_id = 0; block_id < cnt_blocks; block_id++) {
	    const uint nrows = nrows_increase + (block_id * nrows_increase);
	    assert(nrows != UINT_MAX);
	    //! 
	    //! 
	    uint cnt_clusters = 2;
	    if(clusterCase == 0) {
	    } else if(clusterCase == 1) {
	      cnt_clusters = 2; 
	    } else if(clusterCase == 2) {
	      cnt_clusters = nrows/4; 
	      assert(cnt_clusters > 0);
	      assert(cnt_clusters <= nrows);
	    } else if(clusterCase == 3) {
	      cnt_clusters = nrows / 2; 
	      assert(cnt_clusters > 0);
	      assert(cnt_clusters <= nrows);
	    } else {
	      assert(false); //! ie, as this option is Not supported
	    }

	    //! 
	    //! Wrap result-object into an s_kt_matrix_base object
	    // s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_shape.matrix.nrows, obj_shape.matrix.ncols, obj_shape.matrix.matrix);
	    //! ----------------------------------------------------------------------
	    //!
	    //! Allocate result-matrix:
	    // s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	    //!
	    //! Build a vertex-clsuter-membership-set:
	    s_hp_clusterShapes_t obj_shape_2 = setToEmptyAndReturn__s_hp_clusterShapes_t();
	    if(case_id == 0) {
	      obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	      fileTag = "linear_1_100_minMax";
	    } else if(case_id == 1)  {
	      obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/80, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	      fileTag = "linear_1_80_minMax";
	    } else if(case_id == 2)  {
	      obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/40, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	      fileTag = "linear_1_40_minMax";
	    } else if(case_id == 3)  {
	      obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/20, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	      fileTag = "linear_1_20_minMax";
	    } else if(case_id < cnt_blocks)  { //! then we use a 'random case-seliont', where muliple 'calls' are sued to explroe differetn 'random calls'.
	      obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	      fileTag = "random";
	    } else {
	      assert(false); //! ie, as this option is Not supported
	    }
#define __MiFLocal__setScore(obj, row_id, col_id) ({ \
		  assert(row_id < obj.mat_result_ccm.nrows); \
		  assert(row_id < obj.mat_result_time.nrows); \
		  assert(col_id < obj.mat_result_ccm.ncols); \
		  assert(col_id < obj.mat_result_time.ncols); \
		  /*! Store results: */ \
		  obj.mat_result_ccm.matrix[row_id][col_id] = ccm_score; \
		  obj.mat_result_time.matrix[row_id][col_id] = time_result; })

#define __MiF__setString__row(obj, index) ({ \
	      char tag[1000]; memset(tag, '\0', 1000); sprintf(tag, "nrows=%u", nrows); \
	      assert(index < obj.mat_result_ccm.nrows);			\
	      assert(index < obj.mat_result_time.nrows);		\
	      set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/false); \
	      set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/false); } )
#define __MiF__setString__col(obj, index, tag) ({ \
		  assert(index < obj.mat_result_ccm.ncols); \
		  assert(index < obj.mat_result_time.ncols); \
		  set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/true); \
		  set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/true); } )
	    //const uint cnt_clusters = case_id*config_clusterBase;
	    bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	    assert(is_ok);    
	    //!
	    //! Fetch ferecne-facts: 
	     uint *map_clusterMembers = obj_shape_2.map_clusterMembers;
	    s_kt_matrix_base_t vec_2 = getShallow_matrix_base__s_hp_clusterShapes_t(&obj_shape_2);
	    //!
	    //!
	    { //! Apply logics: compute the matrix-based CCMs:
	      // s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	      // -------------------------------
	      if(obj_ccm_matrix.mat_result_ccm.nrows == 0) { //! then we intiate: 
		uint cnt_cols = e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
		obj_ccm_matrix.mat_result_ccm  = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		obj_ccm_matrix.mat_result_time = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		//! 
		//! Set strings:
		for(uint block_id = 0; block_id < cnt_blocks; block_id++) {		  
		  __MiF__setString__row(obj_ccm_matrix, block_id);
		}
		for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
		  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i;
		  __MiF__setString__col(obj_ccm_matrix, /*index=*/i, getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_enum));
		}
	      }
	      // -------------------------------
	      for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
		start_time_measurement();
		e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i;
		t_float ccm_score = T_FLOAT_MAX;
		is_ok = ccm__singleMatrix__hp_ccm(ccm_enum, &vec_2, map_clusterMembers, &ccm_score);
		//is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
		assert(is_ok);    
		//! ----------
		const char *str_local = "matrix-based"; 		const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);		
		//!
		//! Store results:
		__MiFLocal__setScore(obj_ccm_matrix, block_id, i);
	      }
	    }
	    //! *************************************************************************************** 
	    { //! Apply logics: compute the Gold-based CCMs:
	      //! 
	      // -------------------------------
	      if(obj_ccm_gold.mat_result_ccm.nrows == 0) { //! then we intiate: 
		uint cnt_cols = e_kt_matrix_cmpCluster_metric_undef;
		obj_ccm_gold.mat_result_ccm  = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		obj_ccm_gold.mat_result_time = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		//! 
		//! Set strings:
		for(uint block_id = 0; block_id < cnt_blocks; block_id++) {		  
		  __MiF__setString__row(obj_ccm_gold, block_id);
		}
		for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
		  e_kt_matrix_cmpCluster_metric_t ccm_enum = (e_kt_matrix_cmpCluster_metric_t)i;
		  __MiF__setString__col(obj_ccm_gold, /*index=*/i, getStringOf__e_kt_matrix_cmpCluster_metric_t__short(ccm_enum));
		}
	      }
	      // -------------------------------
	      //! First cosntruct a godl-clsuter .... where we compare with the asusmption that 'vertieces are spliut intow two aprts wrt. the input-csluter'.
	      s_hp_clusterShapes_t obj_shape_1 = setToEmptyAndReturn__s_hp_clusterShapes_t();
	      obj_shape_1 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	      const uint cnt_clusters_1 = 2; 
	      is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_1, nrows, cnt_clusters_1, /*cnt_clusterStartOffset=*/0);
	      assert(is_ok);    
	      //!
	      //! Fetch ferecne-facts: 
	      uint *map_clusterMembers_1 = obj_shape_1.map_clusterMembers;
	      s_kt_matrix_base_t vec_1 = getShallow_matrix_base__s_hp_clusterShapes_t(&obj_shape_1);
	      //! 
	      //! Loop through the cases: 
	      for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
		start_time_measurement(); //! ie, start timer.
		e_kt_matrix_cmpCluster_metric_t ccm_enum = (e_kt_matrix_cmpCluster_metric_t)i;
		t_float ccm_score = T_FLOAT_MAX;
		is_ok = ccm__twoHhypoThesis__hp_ccm(ccm_enum, map_clusterMembers_1, map_clusterMembers, nrows, vec_1.matrix, vec_2.matrix, &ccm_score);
		//is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
		assert(is_ok);    
		//! ----------
		const char *str_local = "gold-based"; const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);  //! ie, stop timer.
		//!
		//! Store results:
		__MiFLocal__setScore(obj_ccm_gold, block_id, i);
	      }

	      //! 
	      //! De-allocate:
	      free__s_hp_clusterShapes_t(&obj_shape_1);
	    }
	    //! *************************************************************************************** 
	    //	    printf("at %s:%d\n", __FILE__, __LINE__);
	    { //! Apply logics: compute for: cluster-algorithms: 
	      // -------------------------------
	      if(obj_ccm_clusterAlg.mat_result_ccm.nrows == 0) { //! then we intiate: 
		uint cnt_cols = (e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef * e_hpLysis_clusterAlg_undef);
		obj_ccm_clusterAlg.mat_result_ccm  = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		obj_ccm_clusterAlg.mat_result_time = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		//! 
		//! Set strings:
		for(uint block_id = 0; block_id < cnt_blocks; block_id++) {		  
		  __MiF__setString__row(obj_ccm_clusterAlg, block_id);
		}
		uint col_id_local = 0;
		for(uint sim_metric = 0; sim_metric < e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; sim_metric++) {
		  const s_kt_correlationMetric_t obj_sim = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
		  //! Iterate throguh clsuter-algroithsm: 
		  for(uint clust_alg = 0; clust_alg < e_hpLysis_clusterAlg_undef; clust_alg++) {
		    const e_hpLysis_clusterAlg_t enum_clust = (e_hpLysis_clusterAlg_t)clust_alg;
		    char tag_local[4000]; memset(tag_local, '\0', 4000); sprintf(tag_local, "%s_%s", get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(obj_sim.metric_id), get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust));
		    __MiF__setString__col(obj_ccm_clusterAlg, /*index=*/col_id_local, tag_local);
		    col_id_local++;
		  }
		}
	      }
	    printf("at %s:%d\n", __FILE__, __LINE__);
	      // -------------------------------
	      //! 
	      //! Iterate throguh sim-metrics: 
	      uint col_id_local = 0;
	      for(uint sim_metric = 0; sim_metric < e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; sim_metric++) {
		const s_kt_correlationMetric_t obj_sim = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
		assert(obj_sim.metric_id != e_kt_correlationFunction_undef);
		//! Iterate throguh clsuter-algroithsm: 
		for(uint clust_alg = 0; clust_alg < e_hpLysis_clusterAlg_undef; clust_alg++) {
		  const e_hpLysis_clusterAlg_t enum_clust = (e_hpLysis_clusterAlg_t)clust_alg;
		  if( (enum_clust != e_hpLysis_clusterAlg_disjoint_MCL) 
		      && (enum_clust != e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) 
		      ) {
		    //!
		    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
		    
		    // (Symmetric) adjacency matrix                                                                                                                            
		    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; // inputMatrix__isAnAdjecencyMatrix;
		    obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.
		    //obj_hp.config.corrMetric_prior_use = true; //! ie, use data-as-is.
		    if(false) {
		      //! Specify that we are to use the k-means++-implmeantion defined/used in the "fast_Kmeans" software:
		      obj_hp.randomConfig.config_init.typeOf_randomNess = e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss; //! where the latter enum is define d in our "math_generateDistribution.h"
		    }
		    
		    //! 
		    //! Compute the cluster:
		    printf("%s\t, at %s:%d\n", get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust), __FILE__, __LINE__);
		    start_time_measurement(); //! ie, start timer.
		    const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_clust, &(obj_shape_2.matrix), cnt_clusters, /*npass=*/100);
		    //		  const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_clust, &(obj_shape_2.matrix), cnt_clusters, /*npass=*/1000);
		    assert(is_ok);
		    //! 
		    //! Evlauate accurayc of clsuter-prediction, where we for simplicyt use the defualt/eslibehd Silhouette CCM: 
		    s_kt_matrix_t mat_shallow = obj_shape_2.matrix; 
		    // get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&(obj_shape_2.matrix)); 
		    const t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, &mat_shallow);
		    const char *str_local = "cluster-algorithm"; const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);  //! ie, stop timer.
		    //!
		    //! Store results:
		    __MiFLocal__setScore(obj_ccm_clusterAlg, block_id, col_id_local);
		    
		    //!
		    //! De-allcote:
		    free__s_hpLysis_api_t(&obj_hp);
		  }
		  col_id_local++;
		}
	      }
	    }
	    printf("at %s:%d\n", __FILE__, __LINE__);
	    //! *************************************************************************************** 	    
	    { //! Apply logics: compute for: CCM-permtautions: 
	      // -------------------------------
	      if(obj_ccm_matrix_config.mat_result_ccm.nrows == 0) { //! then we intiate: 
		uint cnt_cols = 0;
		for(uint use_kd = 0; use_kd < 2; use_kd++) {
		  if(use_kd == 1) {continue;} //! FIXME: remove.
		  //! 
		  //! Evlauate different centrlaity-strategeis: 
		  for(uint ccm_case_central_1 = 0; ccm_case_central_1 < e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef; ccm_case_central_1++) {
		    e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t enum_central_1 = (e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t)ccm_case_central_1;
		    for(uint ccm_case_central_2 = 0; ccm_case_central_2 < e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef; ccm_case_central_2++) {
		      e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t enum_central_2 = (e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t)ccm_case_central_2;
		      //! 
		      //! Different entropy-strategeis: 
		      for(uint strat_entropy = 0; strat_entropy < e_kt_matrix_cmpCluster_clusterDistance__config_metric__undef; strat_entropy++) {
			e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_entropy = (e_kt_matrix_cmpCluster_clusterDistance__config_metric__t)strat_entropy;
			
			//! 
			//! Evlauate if the inptu-atmrix is to be used:
			for(uint use_inputMatrix = 0; use_inputMatrix < 2; use_inputMatrix++) {
			  for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
			    cnt_cols++;
			  }
			}
		      }
		    }
		  }
		}
		obj_ccm_matrix_config.mat_result_ccm  = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		obj_ccm_matrix_config.mat_result_time = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
		//! 
		//! Set strings:
		for(uint block_id = 0; block_id < cnt_blocks; block_id++) {		  
		  __MiF__setString__row(obj_ccm_matrix_config, block_id);
		}
		uint cnt_cols_local = 0;
		for(uint use_kd = 0; use_kd < 2; use_kd++) {
		  if(use_kd == 1) {continue;} //! FIXME: remove.
		  //! 
		  //! Evlauate different centrlaity-strategeis: 
		  for(uint ccm_case_central_1 = 0; ccm_case_central_1 < e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef; ccm_case_central_1++) {
		    e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t enum_central_1 = (e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t)ccm_case_central_1;
		    for(uint ccm_case_central_2 = 0; ccm_case_central_2 < e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef; ccm_case_central_2++) {
		      e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t enum_central_2 = (e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t)ccm_case_central_2;
		      //! 
		      //! Different entropy-strategeis: 
		      for(uint strat_entropy = 0; strat_entropy < e_kt_matrix_cmpCluster_clusterDistance__config_metric__undef; strat_entropy++) {
			e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_entropy = (e_kt_matrix_cmpCluster_clusterDistance__config_metric__t)strat_entropy;
			
			//! 
			//! Evlauate if the inptu-atmrix is to be used:
			for(uint use_inputMatrix = 0; use_inputMatrix < 2; use_inputMatrix++) {
			  for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
			    //! Note: 
			    //! Note: we use 's' to describ ehe 'select type' (eg, "STD" or "max").
			    char tag_local[1000]; memset(tag_local, '\0', 1000); sprintf(tag_local, "kd%u_refB%u_refW%u_e%u_d%u_s%u", use_kd, ccm_case_central_1, ccm_case_central_2, strat_entropy, use_inputMatrix, i); 
			    __MiF__setString__col(obj_ccm_matrix_config, /*index=*/cnt_cols_local, tag_local);
			    cnt_cols_local++;
			  }
			}
		      }
		    }
		  }
		}
	      }
	      //! 
	      //! 
	      //! 
	      //! -------------------------------
	      uint col_id_local = 0;
	      for(uint use_kd = 0; use_kd < 2; use_kd++) {
		if(use_kd == 1) {continue;} //! FIXME: remove.
		//! 
		//! Evlauate different centrlaity-strategeis: 
		for(uint ccm_case_central_1 = 0; ccm_case_central_1 < e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef; ccm_case_central_1++) {
		  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t enum_central_1 = (e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t)ccm_case_central_1;
		  for(uint ccm_case_central_2 = 0; ccm_case_central_2 < e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef; ccm_case_central_2++) {
		    e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t enum_central_2 = (e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t)ccm_case_central_2;
		    //! 
		    //! Different entropy-strategeis: 
		    for(uint strat_entropy = 0; strat_entropy < e_kt_matrix_cmpCluster_clusterDistance__config_metric__undef; strat_entropy++) {
		      e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_entropy = (e_kt_matrix_cmpCluster_clusterDistance__config_metric__t)strat_entropy;
		      
		      //! 
		      //! Evlauate if the inptu-atmrix is to be used:
		      for(uint use_inputMatrix = 0; use_inputMatrix < 2; use_inputMatrix++) {
			//! 
			//! 
			//! 
			//! Configure: 
			s_kt_matrix_cmpCluster_clusterDistance_config_t conf_obj = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
			conf_obj.centroidMetric__vertexVertex__networkReference__between = enum_central_1;
			conf_obj.centroidMetric__vertexVertex__networkReference__within = enum_central_2;
			conf_obj.metric_clusterDistance__vertexVertex = enum_entropy;
			conf_obj.metric_clusterDistance__cluster_cluster = enum_entropy;
			assert(use_kd < 2);
			conf_obj.isTo_use_KD_treeInSimMetricComputations = (bool)use_kd;
			assert(use_inputMatrix < 2);
			conf_obj.metric_complexClusterCmp_obj_isTo_use = (bool)use_inputMatrix;
			assert(conf_obj.metric_complexClusterCmp_obj.metric_id != e_kt_correlationFunction_undef);
			//! 
			//! Apply logics: 
			for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
			  start_time_measurement();
			  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i;
			  t_float ccm_score = T_FLOAT_MAX;
			  is_ok = ccm__advanced__singleMatrix__hp_ccm(ccm_enum, &vec_2, map_clusterMembers, conf_obj, &ccm_score);
			  //is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
			  assert(is_ok);    
			  //! ----------
			  const char *str_local = "matrix-based";
			  const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);		
			  //! 
			  //! Store results:
			  //!
			  //! Store results:
			  __MiFLocal__setScore(obj_ccm_matrix_config, block_id, col_id_local);
			  col_id_local++;
			}
		      }
		    }
		  }
		}
	      }
	    }
	    //! *************************************************************************************** 
	    //! 
	    //! De-allocate: 
	    free__s_hp_clusterShapes_t(&obj_shape_2);
	  }
	  // -------------------------------------------
	  //! 
	  //! 
	  { //! Write out result: 
	    assert(fileTag);
	    assert(strlen(fileTag)); 
	    assert(resultPrefix); 
	    assert(strlen(resultPrefix));
	    assert(strlen(resultPrefix) < 1000);
	    {	      
	      char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_matrix_default.tsv", resultPrefix, fileTag, case_id, clusterCase); 
	      free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_matrix, file_name);
	    }
	    {
	      char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_gold.tsv", resultPrefix, fileTag, case_id, clusterCase); 
	      free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_matrix_config, file_name);
	    }
	    {
	      char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_clusterAlg.tsv", resultPrefix, fileTag, case_id, clusterCase); 
	      free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_gold, file_name);
	    }
	    {
	      char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_matrix_config.tsv", resultPrefix, fileTag, case_id, clusterCase); 
	      free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_clusterAlg, file_name);
	    }
	  }
	  // -------------------------------------------
	}
      }
    }
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}

#undef __MiF__isTo_evaluate__local
#undef __MiFLocal__setScore
#undef __MiF__setString__row
#undef __MiF__setString__col
