//! Idneitfy the max-number-of-combiatnions:
const t_float max_cntPairs = (t_float)__computeCombinatorics__by2(cnt_vertices);
assert(max_cntPairs > 0);

t_float cnt_symmetricIntersections = 0;
//!
//! Copmtue for the left-side, ie, for 'cluster 1':
for(uint row_id = 0; row_id < nrows; row_id++) {
  assert_possibleOverhead(matrix[row_id]);
  t_float local_sum = 0;
  for(uint col_id = 0; col_id < ncols; col_id++) {
    const t_float score = matrix[row_id][col_id];
    assert_possibleOverhead(score != T_FLOAT_MAX);
    local_sum += score;
    //local_sum += (score*score);
  }
  // printf("\t\tlocal_sum=%f, at %s:%d\n", local_sum, __FILE__, __LINE__);
  cnt_symmetricIntersections += (local_sum * local_sum);
 }
//!
//! Copmtue for the right-side, ie, for 'cluster 2':
for(uint col_id = 0; col_id < ncols; col_id++) {
  t_float local_sum = 0;
  for(uint row_id = 0; row_id < nrows; row_id++) {      
    const t_float score = matrix[row_id][col_id];
    assert_possibleOverhead(score != T_FLOAT_MAX);
    local_sum += score;
    //local_sum += (score*score);
  }
  //printf("\t\tlocal_sum=%f, at %s:%d\n", local_sum, __FILE__, __LINE__);
  cnt_symmetricIntersections += (local_sum * local_sum);
 }

//! 
//! Compute for the 'total count'
t_float cnt_totalOverlaps = 0;
for(uint row_id = 0; row_id < nrows; row_id++) {
  assert_possibleOverhead(matrix[row_id]);
  for(uint col_id = 0; col_id < ncols; col_id++) {
    const t_float score = matrix[row_id][col_id];
    assert_possibleOverhead(score != T_FLOAT_MAX);
    cnt_totalOverlaps += (score*score);
  }
 }
assert(cnt_totalOverlaps != 0); // TODO: if this does not hold then consider dorpping 'this'.

//!
//! Combine:
const t_float cnt_actual = max_cntPairs - (
					   (0.5*cnt_symmetricIntersections) - cnt_totalOverlaps
					   );
if(cnt_actual != 0) {score = cnt_actual / max_cntPairs;} //! ie, the 'relative importance of the pair-simliarty when compared to the possilbe number of pairs adjusted wr.t the symmetry-attribute'.

// (score-computation) score=1.000000, cnt_actual=2.000000, max_cntPairs=3.000000, cnt_symmetricIntersections=8.000000, cnt_totalOverlaps=3.000000, at /home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/kt_matrix_cmpCluster__stub__metric__Rand.c:50

//printf("(score-computation)\t score=%f, cnt_actual=%f, max_cntPairs=%f (given cnt_vertices=%u), cnt_symmetricIntersections=%f, cnt_totalOverlaps=%f, at %s:%d\n", score, cnt_actual, max_cntPairs, cnt_vertices, cnt_symmetricIntersections, cnt_totalOverlaps, __FILE__, __LINE__);

/* assert(cnt_actual >= 0); // TODO: consider remvoing 'this. */
/* assert(false); // FIXME: remove */
