#ifndef metricMacro_directScore_h
#define metricMacro_directScore_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */



/**
   @file metricMacro_innerProduct_directScore
   @brief provide support for difference "directScore" correlation-metrics, metrics described by oekseth (oekseth, 06. des. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016). 
 **/

#define correlation_macros__distanceMeasures__directScore__avg(score_1, score_2) ({0.5*(score_1+score_2);})
#define correlation_macros__distanceMeasures__directScore__sum(score_1, score_2) ({score_1+score_2;})
//! ---
#define correlation_macros__distanceMeasures__directScore__minus(score_1, score_2) ({score_1-score_2;})
#define correlation_macros__distanceMeasures__directScore__abs_minus(score_1, score_2) ({mathLib_float_abs(score_1-score_2);})
//! ---
#define correlation_macros__distanceMeasures__directScore__sq_sum(score_1, score_2) ({t_float sum_local = score_1+score_2; sum_local =  sum_local*sum_local; sum_local;}) //! ie (x1+k2)^2
#define correlation_macros__distanceMeasures__directScore__sq_minus(score_1, score_2) ({t_float sum_local = score_1-score_2; sum_local =  sum_local*sum_local; sum_local;}) //! ie (x1-k2)^2
#define correlation_macros__distanceMeasures__directScore__sq_abs_minus(score_1, score_2) ({t_float sum_local = mathLib_float_abs(score_1-score_2); sum_local = sum_local*sum_local; sum_local;}) //! ie (|x1-k2|)^2
//! ---
#define correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus(score_1, score_2) ({ \
      t_float sum_local = mathLib_float_abs(score_1-score_2); sum_local = sum_local*sum_local; \
      /*! Compute: (|x1-k2|)^2/max(x1, x2)**/ \
      const t_float val_max = macro_max(score_1, score_2); metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(sum_local, val_max);}) 
#define correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus(score_1, score_2) ({ \
      t_float sum_abs = mathLib_float_abs(score_1-score_2); t_float sum_local = sum_abs*sum_abs; \
      /*! Compute: (|x1-k2|)^2/abs(|x1 - x2|)**/ \
      metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(sum_local, sum_abs);}) 
//! ---
#define correlation_macros__distanceMeasures__directScore__mul(score_1, score_2) ({0.5*score_1*score_2;})
//! ---
#define correlation_macros__distanceMeasures__directScore__div_headIsNumerator(score_1, score_2) ({metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(score_1, score_2);})
#define correlation_macros__distanceMeasures__directScore__div_tailIsNumerator(score_1, score_2) ({metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(score_2, score_1);})
//! ---
#define correlation_macros__distanceMeasures__directScore__div_maxIsNumerator(score_1, score_2) ({ \
  const t_float val_min = macro_min(score_1, score_2); const t_float val_max = macro_max(score_1, score_2); \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(val_max, val_min);})
#define correlation_macros__distanceMeasures__directScore__div_minIsNumerator(score_1, score_2) ({ \
  const t_float val_min = macro_min(score_1, score_2); const t_float val_max = macro_max(score_1, score_2); \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(val_min, val_max);})
//! ---
#define correlation_macros__distanceMeasures__directScore__min(score_1, score_2) ({macro_min(score_1,score_2);})
#define correlation_macros__distanceMeasures__directScore__max(score_1, score_2) ({macro_max(score_1,score_2);})
//! ---
#define correlation_macros__distanceMeasures__directScore__useScore_1(score_1, score_2) ({score_1;})
#define correlation_macros__distanceMeasures__directScore__useScore_2(score_1, score_2) ({score_2;})
//! ---
#define correlation_macros__distanceMeasures__directScore__abs(score_1, score_2) ({mathLib_float_abs(score_1 - score_2);})
#define correlation_macros__distanceMeasures__directScore__log_abs(score_1, score_2) ({mathLib_float_log(mathLib_float_abs(score_1 -score_2));})
#define correlation_macros__distanceMeasures__directScore__2log_abs(score_1, score_2) ({mathLib_float_log_2_abs(mathLib_float_abs(score_1 - score_2));})
//! ---
#define correlation_macros__distanceMeasures__directScore__sqrt_abs(score_1, score_2) ({mathLib_float_sqrt(mathLib_float_abs(score_1 - score_2));})
#define correlation_macros__distanceMeasures__directScore__sqrt_abs_minus(score_1, score_2) ({mathLib_float_sqrt(mathLib_float_abs(score_1 - score_2));})


#endif //! EOF
