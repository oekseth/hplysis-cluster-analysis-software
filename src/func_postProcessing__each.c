{
  
  if(typeOf_postProcessing_afterCorrelation != e_allAgainstAll_SIMD_inlinePostProcess_undef) { //listOf_inlineResults) { //! Then we use temproary logics to update the result-set:
    if(typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) {
      assert(s_inlinePostProcess);
      s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *obj = (s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t*)s_inlinePostProcess;
      assert(obj->listOf_result_afterCorrelation);
      //! Then compute the result and udpate the result-list:
      macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(distance, obj->cutoff, obj->exponent, index1, index2, obj->listOf_result_afterCorrelation, isTo_updateAlsoFor_symmetric_elements);	      
    }  
  } else if(typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {
    s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
    if(objCaller_postProcessResult->return_distance > distance) {
      objCaller_postProcessResult->return_distance = distance;
      objCaller_postProcessResult->index2_best = index2;
    }
  } else if(typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {
    s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
    if(objCaller_postProcessResult->return_distance < distance) {
      objCaller_postProcessResult->return_distance = distance;
      objCaller_postProcessResult->index2_best = index2;
    }

  } else if(typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) {
    s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
    //for(uint index2 = 0; index2 < ncols; index2++) {
    
    if(isTo_updateAlsoFor_symmetric_elements == true) {
      objCaller_postProcessResult->return_distance += distance;
    } else {
      objCaller_postProcessResult->return_distance += (distance + distance);
    }
      //} 
    // } else if(typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum_dividedBy_sumOf_elements) {
    //   assert(false); // FIXME[jc]: update this code when we have optimized our itnristnicts wrt. the scalar-sum-value
  } else { //! then we ened to add support f'ro this case' (oekseth, 06. okt. 2016).
    if(typeOf_postProcessing_afterCorrelation != e_allAgainstAll_SIMD_inlinePostProcess_undef) {
      fprintf(stderr, "!!\t Add support for this case w/enum-id=%u, at [%s]:%s:%d\n", typeOf_postProcessing_afterCorrelation, __FUNCTION__, __FILE__, __LINE__);
      assert(false);
    } else {
      fprintf(stderr, "!!\t enum=undef, ie, a pointless calls, ie, could be an in-cosnsitnecy in your caller-funciton. Add support for this case w/enum-id=%u, at [%s]:%s:%d\n", typeOf_postProcessing_afterCorrelation, __FUNCTION__, __FILE__, __LINE__); //! (eosketh, 06. jul. 2017)
      assert(false);
    }
  }
}
