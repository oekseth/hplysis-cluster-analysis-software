#include "log_hca.h"

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


//! Intiaite the object.
void s_log_hca_init(s_log_hca_t *self) {
  assert(self);
  for(uint enum_id = 0; enum_id < (uint)e_log_hca_typeOf_undef; enum_id++) {
    self->mapOf_times[enum_id] = 0;
    self->mapOf_counter[enum_id] = 0;
  }
}

/**
   @brief Start the measurement with regard to time.
   @param <self> is the selfect to update
   @param <enum_id> is attribute to update in self.
**/
void start_time_measurement_log_hca(s_log_hca_t *self, const e_log_hca_typeOf_t enum_id) {
  assert(self); assert(enum_id != e_log_hca_typeOf_undef);
  self->_mapOf_times_internal_clock[enum_id] = times(&self->_mapOf_times_internal_tms[enum_id]); //tms_start);
}
/**
   @brief Ends the measurement with regard to time.
   @param <self> is the selfect to update
   @param <enum_id> is attribute to update in self.
**/
void end_time_measurement_log_hca(s_log_hca_t *self, const e_log_hca_typeOf_t enum_id) { //, const char *msg, double &user_time, double &system_time, const float naive_tickTime = FLT_MAX) {
  assert(self); assert(enum_id != e_log_hca_typeOf_undef);
  struct tms tms_end;
  const clock_t clock_time_start = self->_mapOf_times_internal_clock[enum_id]; 
  const clock_t clock_time_end = times(&tms_end); 
  const long clktck = sysconf(_SC_CLK_TCK);
  const clock_t t_real = clock_time_end - clock_time_start;
  const t_float time_in_seconds = (t_real != 0) ? t_real/(float)clktck : 0;
  //! Udpate:
  // self->_mapOf_times_internal_clock[enum_id] = t_real;
  self->mapOf_times[enum_id] += time_in_seconds; //_real; //! ie, icnrement wrt. the exeuction-time


  /* const float diff_user = tms_end.tms_utime - self->_mapOf_times_internal_tms[enum_id].tms_utime; */
  /* const t_float user_time = (diff_user) ? (diff_user)/((t_float)clktck) : 0; */
  /* printf("diff_user=%f, at %s:%d\n", user_time, __FILE__, __LINE__); */
  /*  */
  /* const float diff_system = tms_end.tms_stime - tms_start.tms_stime; */
  /* system_time = diff_system/((float)clktck); */
  /* if(msg) { */
  /*   if( (naive_tickTime == FLT_MAX) || (naive_tickTime == 0) || (time_in_seconds == 0) ) { */
  /*     if(naive_tickTime != 0) { */
  /* 	printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg); */
  /*     } //! else we assume the measurements are of insiginft valeu, ie, omit printing. */
  /*   } else { */
  /*     const float diff = naive_tickTime / time_in_seconds; */
  /*     printf("%.4fx performance-difference: tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg); */
  /*   } */
  /* } */
  /* //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg); */
  /* return time_in_seconds; */
}


/**
   @brief update the "s_log_hca_t" object with counter (oekseth, 06. july 2016).
   @param <self> the object to hold the s_log_hca_t object.
   @param <enum_id> the log-type to update
   @param <cnt> if not set to "0" nor "UINT_MAX" then wwe use this valeu to update the assciated s_log_hca_t structure     
**/
void log_updateCounter(s_log_hca_t *self, const e_log_hca_typeOf_t enum_id, const uint cnt) {
  assert(self); assert(enum_id != e_log_hca_typeOf_undef);
  if(cnt != UINT_MAX)  {
    self->mapOf_counter[enum_id] += cnt;
    //printf("counter=%u, after cnt=%u, at %s:%d\n", self->mapOf_counter[enum_id], cnt, __FILE__, __LINE__);
    // assert(self->mapOf_counter[enum_id] < (1000*1000*100));
    //assert(false);
  }
}
/**
   @brief write out the resutls of the log_hca selfect.
   @param <self> is the object to write out the contents for
   @param <stream> is the stream to write the result to, eg, STDOUT
**/
void writeOut_object(const s_log_hca_t *self, FILE *stream) {
  assert(self); assert(stream); 
  assert(e_log_hca_typeOf_undef == 22); //! ie, for [”elow] to be correctn.
  
  fprintf(stream,
	  "#! Result: [all]='%f's"
	  //! Summary-attributes to idneitfy overall exeuction-time-properties:
	  ", [hca-init]='%f's"
	  ", [hca-init::columnBuckets.local]='%f's"
	  ", [hca-init::columnBuckets.global]='%f's"
#if(config_useLog_main_signiOfSameBucket == 1)
	  ", [logic-hca]='%f's w/%u indexes along the main diagonal"
#else
	  ", [logic-hca]='%f's w/%u block-calls"
#endif
	  ", [logic-dataConsistency]='%f's w/%u block-calls"
	  ", [logic-dataConsistency-minPos]='%f's w/%u block-calls"
	  //! Attributes used to get details wrt. the 'default proerpteis' of an Hiearchical Clusterng Algorithm (HCA):
	  ", [hca-max]='%f's"
	  ", [hca-cpy]='%f's" ", [hca-cpy-partialBuckets]='%f's"
	  ", [hca-copy-to-sp]='%f's"
	  //! Default and standrized functiosn:
	  ", [func-updateRow]='%f's w/%u columns updated"
	  ", [func-updateColumn]='%f's w/%u rows updated"
	  ", [func-updateColumn::row-loop]='%f's"
	  ", [func-updateColumn::is-better]='%f's"
	  ", [func-updateColumn::find-betterMinBucket]='%f's w/%u buckets updated"
	  ", [func-getShortestPair]='%f's w/%u colummns updated"
	  ", [func-getShortestPair::updateRow]='%f's"
	  ", [func-getShortestPair::updateColumn]='%f's"
	  ", [func-getShortestPair::findMinRow]='%f's"
	  ", [updates-globalColumnTiles]='%f's w/%u column-buckets updated"
	  ", [updates-copyContinousSections]='%f's w/%u columns updated"
	  ", at %s:%d\n",
	  (float)self->mapOf_times[e_log_hca_typeOf_all],
	  //! Initaiteion:
	  (float)self->mapOf_times[e_log_hca_typeOf_init], 
	  (float)self->mapOf_times[e_log_hca_typeOf_init_details_minPairsEachColumnBucket_local], 
	  (float)self->mapOf_times[e_log_hca_typeOf_init_details_minPairsEachColumnBucket_global], 
	  //! Summary-attributes to idneitfy overall exeuction-time-properties:
	  (float)self->mapOf_times[e_log_hca_typeOf_all_hcaSpecific], self->mapOf_counter[e_log_hca_typeOf_all_hcaSpecific],
	  (float)self->mapOf_times[e_log_hca_typeOf_all_dataUpdateCalls], self->mapOf_counter[e_log_hca_typeOf_all_dataUpdateCalls],
	  (float)self->mapOf_times[e_log_hca_typeOf_all_dataUpdateCalls_getMinPos], self->mapOf_counter[e_log_hca_typeOf_all_dataUpdateCalls_getMinPos],
	  //! Attributes used to get details wrt. the 'default proerpteis' of an Hiearchical Clusterng Algorithm (HCA):
	  (float)self->mapOf_times[e_log_hca_typeOf_all_hcaSpecific_max], 
	  (float)self->mapOf_times[e_log_hca_typeOf_all_hcaSpecific_cpy],  (float)self->mapOf_times[e_log_hca_typeOf_all_hcaSpecific_cpy_partialBuckets], 
	  (float)self->mapOf_times[e_log_hca_typeOf_all_hcaSpecific_copyToHcaMem], 

	  //! Default and standrized functiosn:
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_updateRow], self->mapOf_counter[e_log_hca_typeOf_numberOf_updates_fromFunc_updateRow],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn], self->mapOf_counter[e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_isBetter],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate], self->mapOf_counter[e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate],
	  //! --
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath], self->mapOf_counter[e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateRow],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateColumn],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_findMinRow],
	  //! --
	  //! Special cases:
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_globalColumnTile], self->mapOf_counter[e_log_hca_typeOf_numberOf_updates_globalColumnTile],
	  (float)self->mapOf_times[e_log_hca_typeOf_numberOf_updates_copyContinousSections], self->mapOf_counter[e_log_hca_typeOf_numberOf_updates_copyContinousSections],
	  //! Idnetifiers to simplify future chagnes:
	  __FILE__, __LINE__);
}
