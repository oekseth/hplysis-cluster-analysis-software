#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks examplifeis appliciaotn of two CCMs, mulipel clsuter-algorithms and and muliple data-sets.
   @remarks examplfies different strategies for randomc cosntruction of numbers: in this example we demonstates/inveestigates the seperatebility between different 'randomized data-sets': we comapre each 'ranomdized vector' to 's' samples (for the 'same' randomized funciotn) and to 'm' other randomziation-ralgorithms. The result (of our comparison) is a simliarty-matrix between the 'm' different randomzied functions. The result is evaluated through applicaiotn of CCMs: we comptue the simalrity-matrics wrt.t eh simlarities, and thereafter evaluate the simliarty-matrx 'towards' our null-hypothesisie (ie, with out 'godl-standard-assumption'): to investigate/test that "the randomizaiton-type results 'ends in' clearly distinct distrubiotns", an assunption/hypothesis we expect will Not hold. In order to further invesitgate the latter we have uploaded the results to a web-based GUI at ...??.. 
   @TODO: try to 'figure otu' a parcital applciaiton wr.t the latter ... and udpate our appendix and web/js  
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------

  const uint config__nclusters = 3;
  const uint config__nelements = 2000;

  const uint arrOf_simAlgs_size = 2;
  const e_kt_correlationFunction_t arrOf_simAlgs[arrOf_simAlgs_size] = {
    e_kt_correlationFunction_groupOf_MINE_mic,
    e_kt_correlationFunction_groupOf_minkowski_euclid,
  };


  const uint setOfRandomNess_cntInner = 3;
  const uint setOfRandomNess_cntEachOuter = 10;
  const uint setOfRandomNess_size = 4;
  const e_kt_randomGenerator_type_t setOfRandomNess[setOfRandomNess_size] = {
    e_kt_randomGenerator_type_hyLysisDefault,
    e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
    e_kt_randomGenerator_type_posixRan__srand__time__modulo,
    e_kt_randomGenerator_type_binominal__incrementalInList
  };
  const uint nrows = setOfRandomNess_size * setOfRandomNess_cntInner;
  const uint ncols = setOfRandomNess_size * setOfRandomNess_cntEachOuter;
  //! Allocate:
  s_kt_matrix_t mat_sim[arrOf_simAlgs_size] = {
    initAndReturn__s_kt_matrix(nrows, ncols),
    initAndReturn__s_kt_matrix(nrows, ncols),
    //initAndReturn__s_kt_matrix(nrows, ncols),
  };
  //s_kt_matrix_t mat_time  = initAndReturn__s_kt_matrix(nrows, ncols); //! where we measure the exec-tiem of the randomness-comptuation (and Not the simalrity-compautaiton).
  //!
  //! Allcoate space for gold-hyptoesesis:
  const uint empty_0 = 0;
  uint *mapOf_goldCluster__truthIs__radnomness = allocate_1d_list_uint(nrows, empty_0);

  //! ends: configuration ------------------------------------------------------------------------------------
  //! ----------------------------------------------------------------------------

  //! -------------------------------------------------------
  //!
  //! Consturct the mappings:
  uint row_id = 0;
  //! Iterate:
  for(uint rand_id = 0; rand_id < setOfRandomNess_size; rand_id++) {
    const e_kt_randomGenerator_type_t type_randomness_in = setOfRandomNess[rand_id];
    //!
    //! Load randomness:
    s_kt_randomGenerator_vector_t rand_in = initAndReturn__s_kt_randomGenerator_vector_t(config__nclusters, config__nelements, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
    //! Apply different iterations:
    for(uint rand_iter = 0; rand_iter < setOfRandomNess_cntInner; rand_iter++) {
      mapOf_goldCluster__truthIs__radnomness[row_id] = type_randomness_in;
      //! Call randomness:
      randomassign__speicficType__math_generateDistributions(&rand_in);
      { //! Set the row-header:
	char str_local[3000]; sprintf(str_local, "ra3_%u.iter_%u", rand_id, rand_iter);
	for(uint sim_id = 0; sim_id < arrOf_simAlgs_size; sim_id++) {
	  set_string__s_kt_matrix(&(mat_sim[sim_id]), row_id, str_local, /*addFor_column=*/false);
	}
      }
      //!
      //! Iterate through the 'feature-vectors':
      uint col_id = 0;
      for(uint rand_id_out = 0; rand_id_out < setOfRandomNess_size; rand_id_out++) {      
	const e_kt_randomGenerator_type_t type_randomness_out = setOfRandomNess[rand_id_out];
	//!
	//! Load randomness:
	s_kt_randomGenerator_vector_t rand_out = initAndReturn__s_kt_randomGenerator_vector_t(config__nclusters, config__nelements, type_randomness_out, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	//! Apply different iterations:
	for(uint rand_iter_out = 0; rand_iter_out < setOfRandomNess_cntEachOuter; rand_iter_out++) {
	  //! Call randomness:
	  randomassign__speicficType__math_generateDistributions(&rand_out);
	  //! Evlaaute hte randomness-results:
	  for(uint sim_id = 0; sim_id < arrOf_simAlgs_size; sim_id++) {
	    const e_kt_correlationFunction_t sim_pre = arrOf_simAlgs[sim_id];
	    //!
	    //! Compute simliarty-metric using our "hp_distance" interface:
	    s_kt_correlationMetric_t metric = setTo_empty__andReturn__s_kt_correlationMetric_t();
	    metric.metric_id = sim_pre;
	    t_float sim_score = T_FLOAT_MAX;
	    const bool is_ok = apply__uint_rows_hp_distance(metric, rand_in.mapOf_columnToRandomScore, rand_out.mapOf_columnToRandomScore, config__nelements, &sim_score);
	    assert(is_ok);
	    assert(mat_sim[sim_id].matrix[row_id]);
	    assert(mat_sim[sim_id].ncols > col_id);
	    mat_sim[sim_id].matrix[row_id][col_id] = sim_score;
	  }
	  col_id++;
	}
	free__s_kt_randomGenerator_vector_t(&rand_out);
      }
      //! -----------------
      row_id++;
    }
    free__s_kt_randomGenerator_vector_t(&rand_in);
  }

  //! -------------------------------------------------------
  //!
  {//! Evalaute the accuracy of the different 'predictors'
    const uint mat_case_size = arrOf_simAlgs_size;
    const uint gold_case_size = 1;
    const uint nrows = mat_case_size * gold_case_size;
    const uint ncols = e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
    s_kt_matrix_t mat_gold  = initAndReturn__s_kt_matrix(nrows, ncols);
    uint row_id = 0;

    for(uint sim_id = 0; sim_id < arrOf_simAlgs_size; sim_id++) {
      s_kt_matrix_t *obj_matrixInput = &(mat_sim[sim_id]);
      for(uint gold_case = 0; gold_case < gold_case_size; gold_case++) {
	uint *list_hypo = NULL;
	if(gold_case == 0) {list_hypo = mapOf_goldCluster__truthIs__radnomness;} 
	//else if(gold_case == 1) {list_hypo = mapOf_goldCluster__truthIs__algorithm;} 
	//else if(gold_case == 2) {list_hypo = mapOf_goldCluster__truthIs__radnomness;}
	else {assert(false);}
	{ //! Set the row-header:
	  char str_local[3000]; sprintf(str_local, "sim_%u.hypo_%u", sim_id, gold_case);
	  set_string__s_kt_matrix(&mat_gold, row_id, str_local, /*addFor_column=*/false);
	}
	//! -------------------------------------
	//!
	//! iterate throguh the data:
	//! -------------------------------------
	//!
       
	uint nclusters = 0;   assert(list_hypo); for(uint i = 0; i < obj_matrixInput->nrows; i++) {nclusters = macro_max(nclusters, list_hypo[i]);}
	if( (nclusters < 2) || (nclusters >= obj_matrixInput->nrows)) {nclusters = 2;}
	assert(nclusters != UINT_MAX);
	assert(nclusters >= 2);
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	obj_hp.config.corrMetric_prior_use = true; //! ie, applky a 'defualt simalrity-emtric'
	//obj_hp.config.corrMetric_prior.metric_id = sim_pre;
       
	const bool is_ok = filterAmdCorrelate__hpLysis_api(&obj_hp, obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
	assert(is_ok);
	/* const bool is_also_ok = cluster__hpLysis_api ( */
	/* 						     &obj_hp, e_hpLysis_clusterAlg_kCluster__medoid, obj_matrixInput, */
	/* 						     /\*nclusters=*\/ nclusters, /\*npass=*\/ arg_npass */
	/* 						     ); */
	assert(is_ok);
	//! ------------------------------------------
	//! 
	//! Compute CCMs:
	for(uint ccm_id = 0; ccm_id < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_id++) {	 
	  if(row_id == 0) { //! Set the row-header:
	    char str_local[3000]; sprintf(str_local, "%s", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id));
	    set_string__s_kt_matrix(&mat_gold, ccm_id, str_local, /*addFor_column=*/true);
	  }
	  t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id, list_hypo, NULL);
	  if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
	  mat_gold.matrix[row_id][ccm_id] = CCM_score;
	}

	//!
	//! De-allocate
	free__s_hpLysis_api_t(&obj_hp);
	row_id++;
      }
    }
    //!
    //! Epxoert the [ªbove] mappigns of randomness-gneerated scores:
    bool is_ok = export__singleCall__s_kt_matrix_t(&mat_gold, "result__hypothesis.tsv", NULL); assert(is_ok);
    free__s_kt_matrix(&mat_gold);  
  }

  //!
  //! De-allocate:
  //free__s_kt_matrix(&mat_time);
  for(uint sim_id = 0; sim_id < arrOf_simAlgs_size; sim_id++) {
    //! First export:
    char str_local[2000]; sprintf(str_local, "randomNess_sim_%u.tsv", sim_id);
    bool is_ok = export__singleCall__s_kt_matrix_t(&mat_sim[sim_id], str_local, NULL); assert(is_ok);
    //! Then de-allocate:
    free__s_kt_matrix(&(mat_sim[sim_id]));
  }
  free_1d_list_uint(&mapOf_goldCluster__truthIs__radnomness);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
