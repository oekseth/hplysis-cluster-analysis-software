 #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_sim_string.h" //! ie, the simplifed API for hpLysis
#include "kt_sparse_sim.h" //! where the latter is used as an altneritve to the "kt_sim_string", ie, to afcilaitet/supprot comptaution of "Pairwise simalrity-meitrcs (PSMs)"
#include <time.h>    // time()

/**
   @brief a simple use-case-example where we dmeosntrate the use/applicaiton of "string-simliarty-metrics (StSM)" and validate their correctness
   @author Ole Kristian Ekseth (oekseth, 06. aug. 2017).
   @remarks
   --- we extend the default set of StSMs with "Pairwise simalrity-meitrcs (PSMs)", ie, as the latter is (for many cotnenxts) sued as the deuflat reference-frame wrt. simalrity-emtric-comtpatuions. 
**/
int main() 
#else
  void tut_string_sim_1_singlePair()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  const uint list_str_size = 3+3;
  char *list_str[list_str_size] = {
    "hamsun", "hamsum", "gamsun",
    //! Note[below]: examplify for use-cases where the strings are 'xtremely dis-simliar .... to goal/Motivaiotn is to vlaidte and exmply cases where score-differences is profunc/makred .... from the [”elow] results we observe how (among others) the "levenshtein-distance" manages to capture our itnal/prioer expeciotan/assumptiosnw rt. silairty.
    "lavransdatter", "lavran", "la",
  };
  //! 
  //! 
  for(uint i = 0; i < list_str_size; i++) {
    for(uint k = 0; k < list_str_size; k++) {      
      //    if(i == k) {continue;} // TODO: consider removing this.
      //! 
      //! Simply access to the strings:
      const char *str1 = list_str[i];       const char *str2 = list_str[k];      
      //! Validate that the strings are set:
      assert(str1); assert(strlen(str1));
      assert(str2); assert(strlen(str2));
      //!
      //! Apply logics: iterate through the different metrics:
      //      for(uint metric_id = 0; metric_id < e_kt_sim_string_type_undef; metric_id++) {
      { uint metric_id = e_kt_sim_string_type_LevenshteinDistance;
	//!
	//! The call:
	s_kt_sim_string_t obj_sim = init__s_kt_sim_string_t();
	if(true) {
	  obj_sim.isTo_normalize_result = true;
	}
	const t_float score = compute__kt_sim_string(&obj_sim, (e_kt_sim_string_type_t)metric_id, str1, str2);
	//! Get hte metic-sctring:	
	const char *str_metric = getString__e_kt_sim_string_type_t((e_kt_sim_string_type_t)metric_id);
	  //! Write out the result:
	if(score != T_FLOAT_MAX) { //! then we assuemt eh score was comptued:
	  //!
	  fprintf(stdout, "\"%s\": \"%s\"--\"%s\": %f, at %s:%d\n", str_metric, str1, str2, score, __FILE__, __LINE__);
	} else { //! then the score was Not computed: 
	  fprintf(stdout, "\"%s\": \"%s\"--\"%s\": <NA>, at %s:%d\n", str_metric, str1, str2, __FILE__, __LINE__);
	}
	//assert(false);
      }
    }
  }
  if(false) { //! then we coomptue for PSMs: 
    //!
    //!
    s_kt_list_1d_string_t strMap_1 = init_andReturn__s_kt_list_1d_string_t(/*nrows=*/list_str_size);
    for(uint i = 0; i < list_str_size; i++) {
      //! 
      //! Simply access to the strings:
      const char *str1 = list_str[i];   
      //! Validate that the strings are set:
      assert(str1); assert(strlen(str1));
      //! Insert:
      set_stringConst__s_kt_list_1d_string(&strMap_1, /*index=*/i, str1);
    }

    //!
    //! ITerate through each of the metircs:
    for(uint metric_id = 0; metric_id < e_kt_correlationFunction_undef; metric_id++) {
      const e_kt_correlationFunction_t enum_id = (e_kt_correlationFunction_t)metric_id;
      if(
	 isTo_use_MINE__e_kt_correlationFunction(enum_id) || 
	 isTo_use_kendall__e_kt_correlationFunction(enum_id)) {continue;} //! ie, as the latter is Not deifned (by defualt)= for sparse data-sets.
      const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(enum_id);
      fprintf(stdout, "\n\n#! metric: %s\n", str_metric);
      //! 
      //! 
      s_kt_sparse_sim_t obj_sim = init__s_kt_sparse_sim_t(initAndReturn__s_kt_correlationMetric_t(enum_id, e_kt_categoryOf_correaltionPreStep_none));
      //! 
      //! Initate the post-simarlity-seleciton-fitler: 
      s_kt_list_2d_kvPair_filterConfig_t config_filter = init__s_kt_list_2d_kvPair_filterConfig_t(
												  /*config_cntMin_afterEach=*/2, 
												  /*config_cntMax_afterEach=*/5,
												  /*config_ballSize_max=*/T_FLOAT_MAX, 
												  /*isTo_ingore_zeroScores=*/false);
      //! 
      //!
      //! Compute: 
      s_kt_list_2d_kvPair_t obj_sparse  = strings__computeStoreIn__2dList__s_kt_sparse_sim_t(&obj_sim, 
											     /*isTo_sortBy_char=*/false,
											     &config_filter, &strMap_1, &strMap_1);
      //!
      //! Write out: 
      //      assert(obj_sparse.current_pos > 0);
      const s_kt_list_1d_string_t *strObj_1 = &strMap_1; //! which is sued to simplify future geneirc changes ot the code, eg, to mvoe [”elow] to a new fucniton.
      for(uint head_id = 0; head_id < obj_sparse.current_pos; head_id++) {
	uint cnt_tails = obj_sparse.list[head_id].current_pos;
	if(cnt_tails > 0) {
	  const char *str_1 = strObj_1->nameOf_rows[head_id];
	  assert(str_1); assert(strlen(str_1));
	  fprintf(stdout, "# %s\t", str_1);
	  for(uint k = 0; k < cnt_tails; k++) {
	    const s_ktType_kvPair_t obj_this = obj_sparse.list[head_id].list[k];
	    if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	      const uint tail_id = obj_this.head;
	      assert(tail_id != UINT_MAX); 
	      assert(tail_id < strObj_1->nrows);
	      const char *str_2 = strObj_1->nameOf_rows[tail_id];
	      assert(str_2); assert(strlen(str_2));	    
	      fprintf(stdout, "%s\t", str_2);
	    }
	  }
	  fprintf(stdout, "\n");
	}
      }
      //!
      //! De-allcoate: 
      free__s_kt_sparse_sim_t(&obj_sim);
      free__s_kt_list_2d_kvPair_t(&obj_sparse);
    }
    //!
    //! De-allcoate: 
    free__s_kt_list_1d_string(&strMap_1);
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

