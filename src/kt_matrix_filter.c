#include "kt_matrix_filter.h"
#include "correlation_macros__distanceMeasures.h"
#include "matrix_deviation.h"
#include "matrix_transpose.h"
#include "correlation_sort.h"

//! Intiaite the object
void setTo_empty__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self) {
  assert(self);
  //! --------------------------------
  //!
  //! Thresholds which are to be applied to rows: filter rows wrt. 'either-or':
  self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[0] = 0;
  self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[1] = 0;
  //! --
  self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[0] = T_FLOAT_MIN_ABS;
  self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[1] = T_FLOAT_MIN_ABS;
  //! --
  self->thresh_minMax__eitherOr_valueDiff_rows[0] = T_FLOAT_MIN_ABS;  self->thresh_minMax__eitherOr_valueDiff_rows[1] = T_FLOAT_MAX;
  self->thresh_minMax__eitherOr_valueDiff_cols[0] = T_FLOAT_MIN_ABS;  self->thresh_minMax__eitherOr_valueDiff_cols[1] = T_FLOAT_MAX;
  //! --
  self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent_scalarAbsMin = T_FLOAT_MIN_ABS;
  self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[0] = T_FLOAT_MIN_ABS; self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[1] = T_FLOAT_MIN_ABS;
  //! --
  self->thresh_minMax__eitherOr_STD_rows[0] = T_FLOAT_MIN_ABS; self->thresh_minMax__eitherOr_STD_rows[1] = T_FLOAT_MAX;
  self->thresh_minMax__eitherOr_STD_cols[0] = T_FLOAT_MIN_ABS; self->thresh_minMax__eitherOr_STD_cols[1] = T_FLOAT_MAX;
  //! --
  self->thresh_minMax__eitherOr_kurtosis_rows[0]  = T_FLOAT_MIN_ABS;   self->thresh_minMax__eitherOr_kurtosis_rows[1] = T_FLOAT_MAX;
  self->thresh_minMax__eitherOr_kurtosis_cols[0]  = T_FLOAT_MIN_ABS;   self->thresh_minMax__eitherOr_kurtosis_cols[1] = T_FLOAT_MAX;
  //! --
  self->thresh_minMax__eitherOr_skewness_rows[0]  = T_FLOAT_MIN_ABS;   self->thresh_minMax__eitherOr_skewness_rows[1] = T_FLOAT_MAX;
  self->thresh_minMax__eitherOr_skewness_cols[0]  = T_FLOAT_MIN_ABS;   self->thresh_minMax__eitherOr_skewness_cols[1] = T_FLOAT_MAX;

  //! --------------------------------
  //!
  //! Thresholds which are to be applied to rows: filter rows and columns wrt. 'speicfic masking of each cell', ie, where parts of rous--columns may be of interest.
  self->thresh_minMax__cellMask_valueUpperLower_matrix[0] = T_FLOAT_MIN_ABS; self->thresh_minMax__cellMask_valueUpperLower_matrix[1] = T_FLOAT_MAX;
  //! Note: in [below] we use the "T_FLOAT_MAX" as min-valeu as we use the percent-valeu as a threshold, eg, as seen in our "kt_matrix_filter__func__cellMask__mean_percent.c"
  self->thresh_minMax__cellMask_meanAbsDiff_row[0]    = T_FLOAT_MAX;        self->thresh_minMax__cellMask_meanAbsDiff_row[1] = T_FLOAT_MAX;
  self->thresh_minMax__cellMask_meanAbsDiff_column[0] = T_FLOAT_MAX;     self->thresh_minMax__cellMask_meanAbsDiff_column[1] = T_FLOAT_MAX; //! ie, use the difference from the "mean" to filter values
  self->thresh_minMax__cellMask_medianAbsDiff_row[0] = T_FLOAT_MAX;      self->thresh_minMax__cellMask_medianAbsDiff_row[1] = T_FLOAT_MAX;
  self->thresh_minMax__cellMask_medianAbsDiff_column[0] = T_FLOAT_MAX; self->thresh_minMax__cellMask_medianAbsDiff_column[1] = T_FLOAT_MAX;  //! ie, use the difference from the "median" to filter values
  

  
  // FIXME: (a) update our web-interface wrt. [ªbove] 'enw-added features' ... and thereafter (b) write a FAQ-scaffold which dewscribed/otuliens the cosnierations wrt. [ªbove].
  // FIXME: write lgocis wrt. 'this'  (a) compute for the rows ... (b) try to 'codnecne/remap the rows into a new matrix' ... (c) copmtue for the columns ... and update a mask-table ... (d) ''remove columsn which are never of interest, budilng a new consdnesed matrix with back\-traking index-table-look-ups' ... (e) write test-cases using different random-generated data-sets .... (f) test wrt. combiantion of different/mulipel matrices .... (g) test wrt. comptuation fo dsjoitn-forests

  //! --------------------------------
  //!
  //! Configuraiton-settings to adjust he valeus after thresholds are applied:
  /* self->isToAdjust_useLogTransform = false; */
  /* self->isToNormalize_rows = false; */
  /* self->isToNormalize_cols = false; */
  self->typeOf_adjust_subtractOrAdd_rows = e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none;
  self->typeOf_adjust_subtractOrAdd_cols = e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none;

  //! --------------------------------
  //!
  //! Temporary data:
  self->matrix = NULL; self->nrows = 0; self->ncols = 0;
  self->mapOf_interesting_rows = NULL;   
  self->mapOf_interesting_cols = NULL; 
  self->orderOf_filter_eitherOr_pos = 0;
  self->orderOf_filter_cellMask_pos = 0;
  self->orderOf_adjustment_pos = 0;
  //! Intalise the cofngiruation-containers:
  for(uint i = 0; i < e_kt_matrix_filter_orderOfFilters_eitherOr_undef; i++) {
    self->orderOf_filter_eitherOr[i] = e_kt_matrix_filter_orderOfFilters_eitherOr_undef;
  }
  for(uint i = 0; i < e_kt_matrix_filter_orderOfFilters_cellMask_undef; i++) {
    self->orderOf_filter_cellMask[i] = e_kt_matrix_filter_orderOfFilters_cellMask_undef;
  }
  for(uint i = 0; i < e_kt_matrix_filter_adjustValues_undef; i++) {
    self->orderOf_adjustment[i] = e_kt_matrix_filter_adjustValues_undef;
  }
}
//! Intiaite the object
void allocate__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const bool isTo_init, t_float **matrix, const uint nrows, const uint ncols) {
  assert(self);
  if(isTo_init) {setTo_empty__s_kt_matrix_filter_t(self);}
  assert(matrix);
  assert(ncols > 0);
  assert(nrows > 0);
  self->matrix = matrix; 
  self->nrows = nrows;
  self->ncols = ncols;
  assert(self->mapOf_interesting_rows == NULL);
  assert(self->mapOf_interesting_cols == NULL);
  //! Then allcoate:
  const bool default_value_char = true; //! ie, default assumption is taht all valeus are of interest.
  if(!isTo_init && (self->mapOf_interesting_rows != NULL) ) {
    free_1d_list_char(&(self->mapOf_interesting_rows)); self->mapOf_interesting_rows = NULL;
  }
  if(!isTo_init && (self->mapOf_interesting_cols != NULL) ) {
    free_1d_list_char(&(self->mapOf_interesting_cols)); self->mapOf_interesting_cols = NULL;
  }
  //! Allocate:
  self->mapOf_interesting_rows = allocate_1d_list_char(nrows, default_value_char);
  self->mapOf_interesting_cols = allocate_1d_list_char(ncols, default_value_char);
}
//! Use pre-defined configuraitons to intiate the s_kt_matrix_filter_t object.
s_kt_matrix_filter_t init_setDefaultValues__s_kt_matrix_filter(t_float **matrix, const uint nrows, const uint ncols) {
  s_kt_matrix_filter_t self;
  allocate__s_kt_matrix_filter_t(&self, /*isTo_init=*/true, matrix, nrows, ncols);
  //! -------------
  //! Identify the min-val and max-val:
  t_float min = T_FLOAT_MAX;   t_float max = T_FLOAT_MIN_ABS;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {  
      min = macro_min(min, matrix[row_id][col_id]);
      max = macro_max(min, matrix[row_id][col_id]);
    }
  }
  const t_float relation_min_max = (min && max) ? min/max : 0;

  //! --------------------------------
  //!
  //! Thresholds which are to be applied to rows: filter rows wrt. 'either-or':
  printf("min-max=[%f, %f], at [%s]:%s:%d\n", min, max, __FUNCTION__, __FILE__, __LINE__);
  self.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[0] = 20;
  self.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[1] = 100;
  //! --
  self.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[0] = 20;
  self.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[1] = 80;
  //! --
  self.thresh_minMax__eitherOr_valueDiff_rows[0] = 1.2*min;  self.thresh_minMax__eitherOr_valueDiff_rows[1] = 0.8 * max;
  self.thresh_minMax__eitherOr_valueDiff_cols[0] = 1.2*min;  self.thresh_minMax__eitherOr_valueDiff_cols[1] = -0.8*max;
  //! --
  self.thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent_scalarAbsMin = 0.8*max; //! ie, the absolute value.
  self.thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[0] = 70; self.thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[1] = 70;
  //! --
  self.thresh_minMax__eitherOr_STD_rows[0] = 0; self.thresh_minMax__eitherOr_STD_rows[1] = 0.5;
  self.thresh_minMax__eitherOr_STD_cols[0] = 0; self.thresh_minMax__eitherOr_STD_cols[1] = 0.5;
  //! --
  self.thresh_minMax__eitherOr_kurtosis_rows[0]  = 0;   self.thresh_minMax__eitherOr_kurtosis_rows[1] = 0.9;
  self.thresh_minMax__eitherOr_kurtosis_cols[0]  = 0;   self.thresh_minMax__eitherOr_kurtosis_cols[1] = 0.9;
  //! --
  self.thresh_minMax__eitherOr_skewness_rows[0]  = 0;   self.thresh_minMax__eitherOr_skewness_rows[1] = 0.6;
  self.thresh_minMax__eitherOr_skewness_cols[0]  = 0;   self.thresh_minMax__eitherOr_skewness_cols[1] = 0.6;

  //! --------------------------------
  //!
  //! Thresholds which are to be applied to rows: filter rows and columns wrt. 'speicfic masking of each cell', ie, where parts of rous--columns may be of interest.
  self.thresh_minMax__cellMask_valueUpperLower_matrix[0] = 1.4*min; self.thresh_minMax__cellMask_valueUpperLower_matrix[1] = 0.8*max;
  //! Note: in [below] we use the "T_FLOAT_MAX" as min-valeu as we use the percent-valeu as a threshold, eg, as seen in our "kt_matrix_filter__func__cellMask__mean_percent.c"
  self.thresh_minMax__cellMask_meanAbsDiff_row[0]    = 30;        self.thresh_minMax__cellMask_meanAbsDiff_row[1] = 40;
  self.thresh_minMax__cellMask_meanAbsDiff_column[0] = 30;     self.thresh_minMax__cellMask_meanAbsDiff_column[1] = 40; //! ie, use the difference from the "mean" to filter values
  self.thresh_minMax__cellMask_medianAbsDiff_row[0] = 30;      self.thresh_minMax__cellMask_medianAbsDiff_row[1] = 40;
  self.thresh_minMax__cellMask_medianAbsDiff_column[0] = 30; self.thresh_minMax__cellMask_medianAbsDiff_column[1] = 40;  //! ie, use the difference from the "median" to filter values
  

  
  // FIXME: (a) update our web-interface wrt. [ªbove] 'enw-added features' ... and thereafter (b) write a FAQ-scaffold which dewscribed/otuliens the cosnierations wrt. [ªbove].
  // FIXME: write lgocis wrt. 'this'  (a) compute for the rows ... (b) try to 'codnecne/remap the rows into a new matrix' ... (c) copmtue for the columns ... and update a mask-table ... (d) ''remove columsn which are never of interest, budilng a new consdnesed matrix with back\-traking index-table-look-ups' ... (e) write test-cases using different random-generated data-sets .... (f) test wrt. combiantion of different/mulipel matrices .... (g) test wrt. comptuation fo dsjoitn-forests

  //! --------------------------------
  //!
  //! Configuraiton-settings to adjust he valeus after thresholds are applied:
  /* self.isToAdjust_useLogTransform = true; */
  /* self.isToNormalize_rows = true; */
  /* self.isToNormalize_cols = true; */
  self.typeOf_adjust_subtractOrAdd_rows = e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median;
  self.typeOf_adjust_subtractOrAdd_cols = e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median;


  //! -------------
  //! @return
  return self;
}
//! De-allocate the object
void free__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self) {
  assert(self);
  if(self->mapOf_interesting_rows) {free_1d_list_char(&(self->mapOf_interesting_rows)); self->mapOf_interesting_rows = NULL;}
  if(self->mapOf_interesting_cols) {free_1d_list_char(&(self->mapOf_interesting_cols)); self->mapOf_interesting_cols = NULL;}
}


//! -------------------------------------------------------------
//! -------------------------------------------------------------
//! Apply either-or filters
void applyFilter__eitherOr__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_id) {
  assert(self); assert(self->matrix); 
  assert(self->nrows); assert(self->ncols);
  //! --------
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent) {
#include "kt_matrix_filter__func__eitherOr__percentPrecent.c" //! ie, apply the logics.
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_absoluteValueAboveThreshold) {
#include "kt_matrix_filter__func__eitherOr__aboveThreshold.c"
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff) {
#define __macro__computeFor__rows
#define __macro__computeFor__cols
#include "kt_matrix_filter__func__eitherOr__minMaxDiff.c"
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_STD) {
#define __macro_getValue() ({config_deviation.variance;}) //! ie, the distance-type to used.
    if( (self->thresh_minMax__eitherOr_STD_rows[0] != T_FLOAT_MIN_ABS) || (self->thresh_minMax__eitherOr_STD_rows[1] != T_FLOAT_MAX) ) {
      //! -----------------------------
      //! 
      //! Cofnigure the operation:
      t_float min_cnt_rows    = self->thresh_minMax__eitherOr_STD_rows[0];  t_float max_cnt_rows    = self->thresh_minMax__eitherOr_STD_rows[1];
      const uint cnt_rows = self->nrows;       const uint cnt_cols = self->ncols;
      char *local_mapOf_interesting = self->mapOf_interesting_rows;
      t_float **local_matrix = self->matrix;
#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[row_id][col_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest'
      //! Apply the logics:
#include "kt_matrix_filter__func__eitherOr__stdPermutations.c"
    }
    if( (self->thresh_minMax__eitherOr_STD_cols[0] != T_FLOAT_MIN_ABS) || (self->thresh_minMax__eitherOr_STD_cols[1] != T_FLOAT_MAX) ) {
      t_float min_cnt_rows    = self->thresh_minMax__eitherOr_STD_cols[0];  t_float max_cnt_rows    = self->thresh_minMax__eitherOr_STD_cols[1];
#include "kt_matrix_filter__func__eitherOr__stdPermutations__transposed.c" //!§ ie, apply the logics.
    }    
    //! Reset the 'if-wide-macro':
#undef __macro_getValue
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis) {
#define __macro_getValue() ({config_deviation.kurtosis;}) //! ie, the distance-type to used.
    if( (self->thresh_minMax__eitherOr_kurtosis_rows[0] != T_FLOAT_MIN_ABS) || (self->thresh_minMax__eitherOr_kurtosis_rows[1] != T_FLOAT_MAX) ) {
      //! -----------------------------
      //! 
      //! Cofnigure the operation:
      t_float min_cnt_rows    = self->thresh_minMax__eitherOr_kurtosis_rows[0];  t_float max_cnt_rows    = self->thresh_minMax__eitherOr_kurtosis_rows[1];
      const uint cnt_rows = self->nrows;       const uint cnt_cols = self->ncols;
      char *local_mapOf_interesting = self->mapOf_interesting_rows;
      t_float **local_matrix = self->matrix;
#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[row_id][col_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest'
      //! Apply the logics:
#include "kt_matrix_filter__func__eitherOr__stdPermutations.c"
    }
    if( (self->thresh_minMax__eitherOr_kurtosis_cols[0] != T_FLOAT_MIN_ABS) || (self->thresh_minMax__eitherOr_kurtosis_cols[1] != T_FLOAT_MAX) ) {
      t_float min_cnt_rows    = self->thresh_minMax__eitherOr_kurtosis_cols[0];  t_float max_cnt_rows    = self->thresh_minMax__eitherOr_kurtosis_cols[1];
#include "kt_matrix_filter__func__eitherOr__stdPermutations__transposed.c" //!§ ie, apply the logics.
    }    
    //! Reset the 'if-wide-macro':
#undef __macro_getValue
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_skewness) {
#define __macro_getValue() ({config_deviation.skewness;}) //! ie, the distance-type to used.
    if( (self->thresh_minMax__eitherOr_skewness_rows[0] != T_FLOAT_MIN_ABS) || (self->thresh_minMax__eitherOr_skewness_rows[1] != T_FLOAT_MAX) ) {
      //! -----------------------------
      //! 
      //! Cofnigure the operation:
      t_float min_cnt_rows    = self->thresh_minMax__eitherOr_skewness_rows[0];  t_float max_cnt_rows    = self->thresh_minMax__eitherOr_skewness_rows[1];
      const uint cnt_rows = self->nrows;       const uint cnt_cols = self->ncols;
      char *local_mapOf_interesting = self->mapOf_interesting_rows;
      t_float **local_matrix = self->matrix;
#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[row_id][col_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest'
      //! Apply the logics:
#include "kt_matrix_filter__func__eitherOr__stdPermutations.c"
    }
    if( (self->thresh_minMax__eitherOr_skewness_cols[0] != T_FLOAT_MIN_ABS) || (self->thresh_minMax__eitherOr_skewness_cols[1] != T_FLOAT_MAX) ) {
      t_float min_cnt_rows    = self->thresh_minMax__eitherOr_skewness_cols[0];  t_float max_cnt_rows    = self->thresh_minMax__eitherOr_skewness_cols[1];
#include "kt_matrix_filter__func__eitherOr__stdPermutations__transposed.c" //!§ ie, apply the logics.
    }    
    //! Reset the 'if-wide-macro':
#undef __macro_getValue
  } else {
    fprintf(stderr, "!!\t We have not written any support for your enum_id=%u, ie, please inveetigate your call: if your call was intentiaonl then please forward a feature-request to [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); // ie, an heads-up in debug-mode.  
  }  
}
//! Apply cell-mask filters
void applyFilter__cellMask__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id)  {
  assert(self); assert(self->matrix); 
  assert(self->nrows); assert(self->ncols);
  //! --------
  if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_minMax) {
#include "kt_matrix_filter__func__cellMask__minMax.c"
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent) {
#define __macro__computeFor__rows
#define __macro__computeFor__cols
#include "kt_matrix_filter__func__cellMask__mean_percent.c"
  } else if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent) {
    if( (self->thresh_minMax__cellMask_medianAbsDiff_row[0] != T_FLOAT_MAX) || (self->thresh_minMax__cellMask_medianAbsDiff_row[1] != T_FLOAT_MAX) ) {
      //! -----------------------------
      //! 
      //! Cofnigure the operation:
      const t_float default_value_float = 0;
      t_float min_cnt_rows    = self->thresh_minMax__cellMask_medianAbsDiff_row[0];  t_float max_cnt_rows    = self->thresh_minMax__cellMask_medianAbsDiff_row[1];
      const uint cnt_rows = self->nrows;       const uint cnt_cols = self->ncols;
      char *local_mapOf_interesting = self->mapOf_interesting_rows;
      t_float **local_matrix = self->matrix;
#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[row_id][col_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest'
      //! Apply the logics:
#include "kt_matrix_filter__func__cellMask__median_percent.c"
    }
    if( (self->thresh_minMax__cellMask_medianAbsDiff_column[0] != T_FLOAT_MAX) || (self->thresh_minMax__cellMask_medianAbsDiff_column[1] != T_FLOAT_MAX) ) {

      //!
      //! Build a transpsoed matrix, ie, to simplyf our comptaution-logics:
      const t_float default_value_float = 0;
      t_float **local_matrix = allocate_2d_list_float(self->ncols, self->nrows, default_value_float);
      matrix__transpose__compute_transposedMatrix_float(self->nrows, self->ncols, self->matrix, local_matrix, /*isTo_useIntrisinistic=*/true);
      //! -----------------------------      
      //! Cofnigure the operation:
      t_float min_cnt_rows    = self->thresh_minMax__cellMask_medianAbsDiff_column[0];  t_float max_cnt_rows    = self->thresh_minMax__cellMask_medianAbsDiff_column[1];
      const uint cnt_rows = self->ncols;       const uint cnt_cols = self->nrows;
      char *local_mapOf_interesting = self->mapOf_interesting_cols;
#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[col_id][row_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest': we assume that the 'col_id' and 'row:_id' asre inverted as we xpect hte matrix-diemsniosn to be transposed
      //! Apply the logics:
#include "kt_matrix_filter__func__cellMask__median_percent.c"
      //!
      //! De-allocate locally reserved memory:
      free_2d_list_float(&local_matrix, self->ncols);      
    }
  } else {
    fprintf(stderr, "!!\t We have not written any support for your enum_id=%u, ie, please inveetigate your call: if your call was intentiaonl then please forward a feature-request to [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); // ie, an heads-up in debug-mode.  
  }
}
//! Apply value-adjustment filters
void adjustValue__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_adjustValues_t enum_id)  {
  assert(self); assert(self->matrix); 
  assert(self->nrows); assert(self->ncols);

  if(enum_id == e_kt_matrix_filter_adjustValues_log) {
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      if(self->mapOf_interesting_rows[row_id]) {
	t_float *__restrict__ row = self->matrix[row_id];
	//! ---
	for(uint col_id = 0; col_id < self->ncols; col_id++) {
	  //assert(!isnan(row[col_id]));
	  if(isOf_interest(row[col_id]) && (row[col_id] != 0) ) {
	    row[col_id] = mathLib_float_log(mathLib_float_abs(row[col_id]));
	    //assert(!isnan(row[col_id]));
	  }
	}
      }
    }
  } else if(enum_id == e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows) {
    //! -----------------------------
    //!     
    const t_float default_value_float = 0;
    const uint cnt_rows = self->nrows;       const uint cnt_cols = self->ncols;
    char *local_mapOf_interesting = self->mapOf_interesting_rows;
    t_float **local_matrix = self->matrix;
#define __macro__getValueFromMatrix(row_id, col_id) ({self->matrix[row_id][col_id];})
#define __macro__setMatrixValue(row_id, col_id, value) ({ self->matrix[row_id][col_id] = value;}) //! ie, then mark the amsk as 'not-of-itnerest'
    //! ----------------------------
    if(self->typeOf_adjust_subtractOrAdd_rows == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median) {
      //! -----------------------------
      //! 
      //! Apply the logics:
#include "kt_matrix_filter__func__cellMask__median_percent.c"
    } else if(self->typeOf_adjust_subtractOrAdd_rows == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean) {
#define __macro__computeFor__rows
#undef __macro__computeFor__cols
#define __macro__adjustValueInsteadOfMask
#include "kt_matrix_filter__func__cellMask__mean_percent.c"
    } else if(self->typeOf_adjust_subtractOrAdd_rows == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD) {
      //! -----------------------------
      //! 
      //! Cofnigure the operation:
#define __macro_getValue() ({config_deviation.variance;}) //! ie, the distance-type to used.
      //! Apply the logics:
#include "kt_matrix_filter__func__eitherOr__stdPermutations.c"
    } else if(self->typeOf_adjust_subtractOrAdd_rows != e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none) {
      fprintf(stderr, "!!\t We have not written any support for your local-spec=%u given enum_id=%u, ie, please inveetigate your call: if your call was intentiaonl then please forward a feature-request to [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", (uint)(self->typeOf_adjust_subtractOrAdd_rows), (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
      assert(false); // ie, an heads-up in debug-mode.  
    }
#undef __macro__getValueFromMatrix
#undef __macro__setMatrixValue
  } else if(enum_id == e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_cols) {
    //! -----------------------------
      //!
      //! Build a transpsoed matrix, ie, to simplyf our comptaution-logics:
      const t_float default_value_float = 0;
      t_float **local_matrix = NULL;
      if(self->typeOf_adjust_subtractOrAdd_cols != e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean) {
	local_matrix = allocate_2d_list_float(self->ncols, self->nrows, default_value_float);
	matrix__transpose__compute_transposedMatrix_float(self->nrows, self->ncols, self->matrix, local_matrix, /*isTo_useIntrisinistic=*/true);
      }
      //! -----------------------------      
      //! Cofnigure the operation:
      t_float min_cnt_rows    = self->thresh_minMax__cellMask_medianAbsDiff_column[0];  t_float max_cnt_rows    = self->thresh_minMax__cellMask_medianAbsDiff_column[1];
      const uint cnt_rows = self->ncols;       const uint cnt_cols = self->nrows;
      char *local_mapOf_interesting = self->mapOf_interesting_cols;
#define __macro__getValueFromMatrix(row_id, col_id) ({self->matrix[col_id][row_id];})
#define __macro__setMatrixValue(row_id, col_id, value) ({ self->matrix[col_id][row_id] = value;}) //! ie, then mark the amsk as 'not-of-itnerest'
    //! ----------------------------
    if(self->typeOf_adjust_subtractOrAdd_cols == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median) {
      //! -----------------------------
      //! 
      //! Apply the logics:
#include "kt_matrix_filter__func__cellMask__median_percent.c"
    } else if(self->typeOf_adjust_subtractOrAdd_cols == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean) {
#undef __macro__computeFor__rows
#define __macro__computeFor__cols
#define __macro__adjustValueInsteadOfMask
#include "kt_matrix_filter__func__cellMask__mean_percent.c"
    } else if(self->typeOf_adjust_subtractOrAdd_cols == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD) {
      //! -----------------------------
      //! 
      //! Cofnigure the operation:
#define __macro_getValue() ({config_deviation.variance;}) //! ie, the distance-type to used.
      //! Apply the logics:
#include "kt_matrix_filter__func__eitherOr__stdPermutations.c"
    } else if(self->typeOf_adjust_subtractOrAdd_rows != e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none) {
      fprintf(stderr, "!!\t We have not written any support for your local-spec=%u given enum_id=%u, ie, please inveetigate your call: if your call was intentiaonl then please forward a feature-request to [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", (uint)(self->typeOf_adjust_subtractOrAdd_rows), (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
      assert(false); // ie, an heads-up in debug-mode.  
    }
    if(local_matrix) {
      //!
      //! De-allocate locally reserved memory:
      free_2d_list_float(&local_matrix, cnt_rows);      
    }
#undef __macro__getValueFromMatrix
#undef __macro__setMatrixValue
  } else if(enum_id == e_kt_matrix_filter_adjustValues_center_normalization_rows) {
#define __macro__computeFor__rows
#undef __macro__computeFor__cols
#define __macro__adjustValueInsteadOfMask
#include "kt_matrix_filter__func__eitherOr__minMaxDiff.c"
  } else if(enum_id == e_kt_matrix_filter_adjustValues_center_normalization_cols) {
#undef __macro__computeFor__rows
#define __macro__computeFor__cols
#define __macro__adjustValueInsteadOfMask
#include "kt_matrix_filter__func__eitherOr__minMaxDiff.c"
  } else {
    fprintf(stderr, "!!\t We have not written any support for your enum_id=%u, ie, please inveetigate your call: if your call was intentiaonl then please forward a feature-request to [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); // ie, an heads-up in debug-mode.      
  }  
}
//! -------------------------------------------------------------
//! -------------------------------------------------------------
/**
   @brief Apply the iether-or filters, cell-mask filters and the adjustment-filters (oekseth, 06. otk. 2016).
   @param <orderOf_filter_eitherOr> is the an enum of type "orderOf_filter_eitherOr" where the [index_id] is the filter-id to apply and the "index_id" describes the applicaiton-order and the
   @param <orderOf_filter_cellMask> is the an enum of type "e_kt_matrix_filter_orderOfFilters_cellMask_t" where the [index_id] is the filter-id to apply and the "index_id" describes the applicaiton-order and the 
   @param <orderOf_adjustment> is the an enum of type "e_kt_matrix_filter_adjustValues_t" where the [index_id] is the filter-id to apply and the "index_id" describes the applicaiton-order and the 

   @remarks the result is an updated s_kt_matrix_filter_t object wrt.: "mapOf_interesting_rows", "mapOf_interesting_cols" and "matrix", where the "matrix" is udpated wrt. empty-valeus using the "T_FLOAT_MAX" proerpty to speicfy 'emptyness'.
**/
 void apply_s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_eitherOr_t orderOf_filter_eitherOr[e_kt_matrix_filter_orderOfFilters_eitherOr_undef], const e_kt_matrix_filter_orderOfFilters_cellMask_t orderOf_filter_cellMask[e_kt_matrix_filter_orderOfFilters_cellMask_undef], const e_kt_matrix_filter_adjustValues_t orderOf_adjustment[e_kt_matrix_filter_adjustValues_undef]) {
  assert(self);
  //! 
  //! Investigate for the eiterhOr filters:
  for(uint index_id = 0; index_id < e_kt_matrix_filter_orderOfFilters_eitherOr_undef; index_id++) {
    const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_id = (e_kt_matrix_filter_orderOfFilters_eitherOr_t)orderOf_filter_eitherOr[index_id];
    if(enum_id != e_kt_matrix_filter_orderOfFilters_eitherOr_undef) {
      applyFilter__eitherOr__s_kt_matrix_filter_t(self, enum_id);
    } //! otehrwise we asussmte the filter-type shoudl Not be applied.
  }
  //! 
  //! Investigate for the cell-mask filters:
  for(uint index_id = 0; index_id < e_kt_matrix_filter_orderOfFilters_cellMask_undef; index_id++) {
    const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id = (e_kt_matrix_filter_orderOfFilters_cellMask_t)orderOf_filter_cellMask[index_id];
    if(enum_id != e_kt_matrix_filter_orderOfFilters_cellMask_undef) {
      applyFilter__cellMask__s_kt_matrix_filter_t(self, enum_id);
    } //! otehrwise we asussmte the filter-type shoudl Not be applied.
  }
  //! 
  //! Investigate for the value-adjustment filters:
  for(uint index_id = 0; index_id < e_kt_matrix_filter_adjustValues_undef; index_id++) {
    const e_kt_matrix_filter_adjustValues_t enum_id = (e_kt_matrix_filter_adjustValues_t)orderOf_adjustment[index_id];
    if(enum_id != e_kt_matrix_filter_adjustValues_undef) {
      adjustValue__s_kt_matrix_filter_t(self, enum_id);
    } //! otehrwise we asussmte the filter-type shoudl Not be applied.
  }
}


//! -------------------------------------------------------------
//! -------------------------------------------------------------

//! Update the filter-applicaiton-order or the "e_kt_matrix_filter_orderOfFilters_eitherOr_t" enum.
//! @remarks is only updated if there does not exist any previous 'enums' for this, ie, applied only once.
void updateOrderOf_filter__eitherOr__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_id) {
  bool isFound = false;
  uint i = 0;
  for(; i <= self->orderOf_filter_eitherOr_pos; i++) {
    if(isFound == false) {
      if(self->orderOf_filter_eitherOr[i] == enum_id) {
	isFound = true;
      }
    }
  }
  if(!isFound && (i < e_kt_matrix_filter_orderOfFilters_eitherOr_undef) ) {
    self->orderOf_filter_eitherOr[i] == enum_id; //! ie, then update at the 'next index'.
  }
}
//! Update the filter-applicaiton-order or the "e_kt_matrix_filter_orderOfFilters_cellMask_t" enum.
//! @remarks is only updated if there does not exist any previous 'enums' for this, ie, applied only once.
void updateOrderOf_filter__cellMask__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id) {
  bool isFound = false;
  uint i = 0;
  for(; i <= self->orderOf_filter_cellMask_pos; i++) {
    if(isFound == false) {
      if(self->orderOf_filter_cellMask[i] == enum_id) {
	isFound = true;
      }
    }
  }
  if(!isFound && (i < e_kt_matrix_filter_orderOfFilters_eitherOr_undef) ) {
    self->orderOf_filter_cellMask[i] == enum_id; //! ie, then update at the 'next index'.
  }
}
//! Update the filter-applicaiton-order or the "e_kt_matrix_filter_adjustValues_t" enum.
//! @remarks is only updated if there does not exist any previous 'enums' for this, ie, applied only once.
void updateOrderOf_adjust__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_adjustValues_t enum_id) {
  bool isFound = false;
  uint i = 0;
  for(; i <= self->orderOf_adjustment_pos; i++) {
    if(isFound == false) {
      if(self->orderOf_adjustment[i] == enum_id) {
	isFound = true;
      }
    }
  }
  if(!isFound && (i < e_kt_matrix_filter_orderOfFilters_cellMask_undef) ) {
    self->orderOf_adjustment[i] == enum_id; //! ie, then update at the 'next index'.
  }
}
