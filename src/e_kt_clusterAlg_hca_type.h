#ifndef e_kt_clusterAlg_hca_type_h
#define e_kt_clusterAlg_hca_type_h


/**
   @file e_kt_clusterAlg_hca_type
   @brief provide lgoics for mapping back-and-forth between heirharhcicla-clsuter-algorithm-types (HCAs) (oekseth, 06. nov. 2016).
 **/

/**
   @enum e_kt_clusterAlg_hca_type
   @brief identifies the enums which are supported by Knitting-Tools hierahriccial-clsuter-impelmentaions (oekseth, 06. nov. 2016).
 **/
typedef enum e_kt_clusterAlg_hca_type {
  e_kt_clusterAlg_hca_type_single,
  e_kt_clusterAlg_hca_type_max,
  e_kt_clusterAlg_hca_type_average,
  e_kt_clusterAlg_hca_type_centroid
} e_kt_clusterAlg_hca_type_t;

//! Investigate if the string equals "single|maximum|average|centroid" and thereafter return an internla char-id used in our procedure for the latter (oekseth, 06 otk. 2016).
//! @return the itnernal char-id corresponding to the enum-enum-option in quesiton.
char get_charConfigurationEnum_fromString__kt_clusterAlg_hca(const char *stringOf_enum);

//! Idneitfies the char-value for an internal enum-id
//! @return the itnernal char-id corresponding to the enum-enum-option in quesiton.
char get_charConfigurationEnum_fromEnum__kt_clusterAlg_hca(const e_kt_clusterAlg_hca_type_t enum_id);
//! Idneitfies the char-value for an internal enum-id
//! @return the itnernal char-id corresponding to the enum-enum-option in quesiton.
e_kt_clusterAlg_hca_type_t get_enum_fromChar__kt_clusterAlg_hca(const char method);


#endif //! EOF
