#include "hpLysis_api.h"
#include "alg_dbScan_brute.h"
#include "hp_ccm.h"
#include "hp_exportMatrix.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.

struct s_tut_disjointCluster_4_algs {
  const char *str; int alg_id; const bool ofType_dbScan_brute;
};



static void __compute__tut_disjointCluster_4(const char *resultPrefix, const struct s_tut_disjointCluster_4_algs obj_alg, const uint mat_dim, t_float execTime[2]) {
const uint arrOf_sim_ccmCorr_postProcess_size = 14;
s_kt_correlationMetric_t arrOf_sim_ccmCorr_postProcess[arrOf_sim_ccmCorr_postProcess_size] = {
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none), //! ie, Pearson
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute, e_kt_categoryOf_correaltionPreStep_none), //! ie, Perason w/eslibhed permtaution
      // Absolute difference::Canberra 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_absoluteDifference_Canberra, e_kt_categoryOf_correaltionPreStep_none),
      // Fidelity::Hellinger:
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_categoryOf_correaltionPreStep_none),
      // Downward mutaiblity::symmetric-max
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax, e_kt_categoryOf_correaltionPreStep_none),
      // Inner product
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none),
      // Squared distance for Euclid
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Euclid, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_rank), //! ie, spearman.
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      //initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
    };
  //! Define the export-objecgt
  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_ccmCorr_postProcess, arrOf_sim_ccmCorr_postProcess_size);  
  //!
  //! ***************************************************************************************
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  { //! Apply logics:
    //! ----------------------------------- default configurations:
    const t_float score_strong = 1;
    const t_float score_weak = 100;
    const uint cnt_clusters = 5;
    const uint nrows = mat_dim; //1000;
    const uint list_metrics_size = 4;
    const s_kt_correlationMetric_t list_metrics[list_metrics_size] = {
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)
    };
    const char *list_metrics_str[list_metrics_size] = {"Euclidan", "Pearson", "Spearman", "Kendall"};
    //! -----------------------------------
    //! ----------------------------------------------------------------------
    //!
    //! Allocate result-matrix:
    const uint config_cnt_casesToEvaluate = (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef + /*ccm-matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef + list_metrics_size;
    //  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
    enum {
      c_name,
      //! --- 
      c_relative_ne_eq,
      c_eq_relative,
      c_ne_relative,
      //! 
      c_eq_worst,
      c_eq_best,
      c_ne_worst,
      c_ne_best,
      //! 
      c_size,
    };
    mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, /*cols=*/c_size);
    //!
    //!  
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_name, "Metric", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_relative_ne_eq, "nEqual / Equal", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_eq_relative, "Equal: Worst/Best", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_eq_best, "Equal: Score(best)", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_eq_worst, "Equal: Score(worst)", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_ne_relative, "Different: Worst/Best", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_ne_best, "Different: Score(best)", /*addFor_column=*/true);      
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_ne_worst, "Different: Score(worst)", /*addFor_column=*/true);      
    //! Epxliclty itnaitlise the matrix:
    for(uint i = 0; i < mat_result.nrows; i++) {   for(uint k = 0; k < mat_result.ncols; k++) {mat_result.matrix[i][k] = T_FLOAT_MAX;}}
    //!
    //! Iterate through: data-cases:
    for(uint data_id = 0; data_id < 2; data_id++) {
      s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
      assert(is_ok);
      assert(obj_shape.map_clusterMembers);
      start_time_measurement();
      { //! Compute the cluster: 
	s_kt_list_1d_uint_t  clusters = setToEmpty__s_kt_list_1d_uint_t();
	if(obj_alg.ofType_dbScan_brute) { //! then the logics in "alg_dbScan_brute.h" is used:
	  const  e_alg_dbScan_brute_t alg_id = (e_alg_dbScan_brute_t)obj_alg.alg_id;
	  assert(alg_id != e_alg_dbScan_brute_undef); //! was this would indicate an error.
	  s_kt_matrix_base_t mat_shallow = getShallow_matrix_base__s_hp_clusterShapes_t(&obj_shape);
	  //	  s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_adj);
	  const t_float epsilon = 100; const uint minpts = 0; //! which is used to set thresholds for sigicant (ie, central) rows (hence, transalting a dense amtrix into a sparse array). 
	  clusters = computeAndReturn__alg_dbScan_brute(alg_id, &mat_shallow, epsilon, minpts);
	} else {
	  const e_hpLysis_clusterAlg_t alg_id = (e_hpLysis_clusterAlg_t)obj_alg.alg_id;
	  assert(alg_id != e_hpLysis_clusterAlg_undef); //! was this would indicate an error.
	  //! Note: the resutls differes as different default cofniguraiton-settings are used. For details of the latter, see the "obj_hp.config.kdConfig" variable
	  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/true; 
	  //obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
	  obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = false; // true;
	  //obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
	  if(false) {
	    //! Note: below invovles additional default steps wrt. simliarty-metrics, hence the results may differ.
	    const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
	    obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
	    obj_hp.config.corrMetric_prior_use = true;
	  }
	  
	  //! 
	  //! Apply logics:
	  // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_id, &(obj_shape.matrix), /*nclusters=*/UINT_MAX, /*npass=*/100);
	  assert(is_ok);
	  //! Get the CCM-score:
	  // ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
	  //! Get the clusters:
	  clusters = getClusters__hpLysis_api(&obj_hp);
	  //! De-allocte:
	  free__s_hpLysis_api_t(&obj_hp);
	}
	const char *str_local = "alg-eval"; 
	const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);
	if((time_result != FLT_MAX) && (time_result != 0) ) { //! then update the execim-time-esmruements.
	  if(time_result < execTime[0]) {execTime[0] = time_result;}
	  if(time_result > execTime[1]) {execTime[1] = time_result;}
	}
	//! Copy the cluster-memberships: 
	const uint cnt_vertices = macro_min(clusters.list_size, obj_shape.map_clusterMembers_size);
	for(uint row_id = 0; row_id < cnt_vertices; row_id++) {
	  if(clusters.list[row_id] != UINT_MAX) {
	    obj_shape.map_clusterMembers[row_id] = clusters.list[row_id];
	  }
	}
	//! De-allocate:   
	free__s_kt_list_1d_uint_t(&clusters);	
      }
      
      //! Set the indexes to update:
      const uint internalIndex_best  = (data_id == 0) ? c_eq_best  : c_ne_best;
      const uint internalIndex_worst = (data_id == 0) ? c_eq_worst : c_ne_worst;
      assert(internalIndex_worst != internalIndex_best);
      //!
      //! Explroe differnet hypo-data-ids: 
      for(uint hypo_id = 0; hypo_id < 2; hypo_id++) {
	//    for(uint hypo_id = 0; hypo_id < 3; hypo_id++) {
	//!
	//! 
	//! Select the hypothesis:
	bool is_ok = true;
	s_hp_clusterShapes_t obj_shape_2  = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	//! Construct for set=1:
	assert(is_ok);	 
	if(data_id == 0) { //! then the two sets should be equal: 
	  is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	  assert(is_ok);	
	} else if(data_id == 1) { //! then all vertices are in the same cluster:
	  is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, /*cnt_clusters=*/1, /*cnt_clusterStartOffset=*/0);
	  assert(is_ok);	
	} else {assert(false);} //! ie, as we then need adding support for this case.
	//! Construct the comparison-basis:
	if(hypo_id == 0) { //! then we do noting, ie, leve the clusters as-is.
	} else if(hypo_id == 1) {	//! then we change the cluster-memberships:
	  for(uint k = 0; k < obj_shape_2.map_clusterMembers_size; k++) {
	    assert(obj_shape_2.map_clusterMembers[k] != UINT_MAX);
	    obj_shape_2.map_clusterMembers[k] *= 5; //! ie, change the cluster-ids (while keeping the grousp unchanged, ie, as the id-change is cosnitent for all groups).
	  }
	  //} else if(hypo_id == 2) {	
	} else {assert(false);} //! ie, as we then need adding support for this case.
	//! ---------------------------------
	//! 
	//! Apply measurements:
	assert(obj_shape.map_clusterMembers);
	assert(obj_shape_2.map_clusterMembers);
	s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_shape_2.matrix.nrows, obj_shape_2.matrix.ncols, obj_shape_2.matrix.matrix);      
	uint case_id = 0;
	{ //! Apply for matrix-based: 
	  //!
	  //! Apply logics: compute the matrix-based CCMs:
	  s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	  is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, obj_shape.map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
	  assert(is_ok);    
	  assert(obj_result.list_size != 0);
	  assert(mat_result.nrows >= obj_result.list_size);
	  assert(obj_result.list_size <= (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	  //!
	  //! Insert results, updating the worst scores:
	  for(uint i = 0; i < obj_result.list_size; i++) {
	    //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
	    // for(uint i = 0; i < mat_result.ncols; i++) {
	    const t_float score = obj_result.list[i];
	    if(score != T_FLOAT_MAX) {
	      if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best])  || (score < mat_result.matrix[i+case_id][internalIndex_best])) {
		mat_result.matrix[i+case_id][internalIndex_best] = score;
	      }
	      // printf("cmp: %f ?> %f, at %s:%d\n", score , mat_result.matrix[i+case_id][internalIndex_worst], __FILE__, __LINE__);
	      if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) {
		// printf("[data:%u]\tbetter:\tcmp: %f ?> %f, at %s:%d\n", data_id, score , mat_result.matrix[i+case_id][internalIndex_worst], __FILE__, __LINE__);
		mat_result.matrix[i+case_id][internalIndex_worst] = score;
	      }
	    }
	    { //! Then we 'insert' for the columns
	      if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		char str_local[3000]; sprintf(str_local, "%s", getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i));
		set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
	      }
	    }
	  }
	  //!
	  //! De-allocate:
	  free__s_kt_list_1d_float_t(&obj_result);
	  //! Increment:
	  case_id += (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
	}
	{ //! Apply for gold-based: 
	  { //! Set-based CCMs: 
	    //!
	    //! Apply logics: compute the matrix-based CCMs:
	    s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_metric_undef);
	    is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(obj_shape.map_clusterMembers, 
							     obj_shape_2.map_clusterMembers, 
							     /*vertices=*/obj_shape.map_clusterMembers_size,
							     obj_shape.matrix.matrix,
							     obj_shape_2.matrix.matrix, 							   
							     &obj_result); //! defined in our "hp_ccm.h"
	    assert(is_ok);    
	    assert(obj_result.list_size != 0);
	    assert(mat_result.nrows >= obj_result.list_size);
	    assert(obj_result.list_size <= (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef);
	    //!
	    //! Insert results, updating the worst scores:
	    for(uint i = 0; i < obj_result.list_size; i++) {
	      //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
	      // for(uint i = 0; i < mat_result.ncols; i++) {
	      const t_float score = obj_result.list[i];
	      if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best]) || (score < mat_result.matrix[i+case_id][internalIndex_best])) {
		mat_result.matrix[i+case_id][internalIndex_best] = score;
	      }
	      if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) {
		mat_result.matrix[i+case_id][internalIndex_worst] = score;
	      }
	      { //! Then we 'insert' for the columns
		if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		  char str_local[3000]; sprintf(str_local, "%s", getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i));
		  set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		}
	      }
	    }
	    //!
	    //! De-allocate:
	    free__s_kt_list_1d_float_t(&obj_result);
	    //! Increment:
	    case_id += (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef;
	    assert(case_id <= mat_result.nrows);
	  }
	  { //! Vector-based CCMs: 
	    //!
	    //! Apply logics: compute the matrix-based CCMs:
	    assert(is_ok);    
	    //!
	    //! Insert results, updating the worst scores:
	    for(uint i = 0; i < list_metrics_size; i++) {
	      //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
	      // for(uint i = 0; i < mat_result.ncols; i++) {
	      t_float score = applyAndReturn__uint_rows_hp_distance(list_metrics[i], obj_shape.map_clusterMembers, obj_shape_2.map_clusterMembers, obj_shape_2.map_clusterMembers_size); // &score, init__s_hp_distance__config_t());
	      if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best]) || (score < mat_result.matrix[i+case_id][internalIndex_best])) {
		mat_result.matrix[i+case_id][internalIndex_best] = score;
	      }
	      if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) {
		mat_result.matrix[i+case_id][internalIndex_worst] = score;
	      }
	      { //! Then we 'insert' for the columns
		if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		  const char *str_local = list_metrics_str[i];
		  set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		}
	      }
	    }
	    //!
	    //! Increment:
	    case_id += (uint)/*ccm-gold=*/list_metrics_size;
	    assert(case_id <= mat_result.nrows);
	  }	
	
	}
	//!
	//! De-allocate:
	free__s_hp_clusterShapes_t(&obj_shape_2);      
      }
      //! De-allocate:
      free__s_hp_clusterShapes_t(&obj_shape);            
    }
    //!
    //! Compute the relative scores
#define use(col) ({ (mat_result.matrix[i][col]  != 0) && (mat_result.matrix[i][col] != T_FLOAT_MAX);})
    for(uint i = 0; i < mat_result.nrows; i++) {
      if(use(c_ne_best) && use(c_eq_best)) {
	mat_result.matrix[i][c_relative_ne_eq] = mat_result.matrix[i][c_ne_best] / mat_result.matrix[i][c_eq_best];
      }
      if(use(c_eq_best) && use(c_eq_worst)) {
	mat_result.matrix[i][c_eq_relative] = mat_result.matrix[i][c_eq_worst] / mat_result.matrix[i][c_eq_best];
      }
      if(use(c_ne_best) && use(c_ne_worst)) {
	mat_result.matrix[i][c_ne_relative] = mat_result.matrix[i][c_ne_worst] / mat_result.matrix[i][c_ne_best];
      }    
    }
#undef use      
  } //! end: logics ***************************************************************************************
  
  {//! Write out proepteis wrt. latter:
    //! Note: [alg]x[cluster-memberships], and (raw,  rank)[alg]x[CCM-scores]
    //! Buidl a file-path: 
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_featureMatrix_merged", resultPrefix);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_result, resultPrefix_local, NULL, NULL, /*isTo_writeOutTransposed=*/true);
    //!
    //! De-allocate
    free__s_kt_matrix(&mat_result);
  }
  /* //! De-allocate:    */
  /* if(fOut) {fclose(fOut); fOut = NULL;} */
  /* if(mat_data.matrix != mat_data_raw.matrix) { */
  /*    free__s_kt_matrix(&mat_data); */
  /* } */
  /* free__s_kt_matrix(&mat_adj); */
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief examplifies how to use different DBSCAN permutations.
   @author Ole Kristian Ekseth  (oekseth,  06. jan. 2017).
   @remarks 
   - extends "tut_disjointCluster_2_algComparisonSingleAlg.c" wrt. evaluation of mulitple algorithms, and the construction of result data.
   - compile: g++ -O0 -g tut_disjointCluster_4_differentAlgDifferentSyntheticDataSets.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_disjointCluster_4.exe; ./tut_disjointCluster_4.exe
**/
int main() 
#else
  static void tut_disjointCluster_4_differentAlgDifferentSyntheticDataSets()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
const uint arrOf_sim_ccmCorr_postProcess_size = 14;
s_kt_correlationMetric_t arrOf_sim_ccmCorr_postProcess[arrOf_sim_ccmCorr_postProcess_size] = {
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none), //! ie, Pearson
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute, e_kt_categoryOf_correaltionPreStep_none), //! ie, Perason w/eslibhed permtaution
      // Absolute difference::Canberra 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_absoluteDifference_Canberra, e_kt_categoryOf_correaltionPreStep_none),
      // Fidelity::Hellinger:
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_categoryOf_correaltionPreStep_none),
      // Downward mutaiblity::symmetric-max
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax, e_kt_categoryOf_correaltionPreStep_none),
      // Inner product
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none),
      // Squared distance for Euclid
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Euclid, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_rank), //! ie, spearman.
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      //initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
    };
  //! Define the export-objecgt
  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_ccmCorr_postProcess, arrOf_sim_ccmCorr_postProcess_size);  

  const uint map_algList_size = 8;
  struct s_tut_disjointCluster_4_algs map_algList[map_algList_size] = {
    //    e_alg_dbScan_brute_linkedListSet,
    {"disjoint-dbScan-direct", e_alg_dbScan_brute_sciKitLearn, true},
    {"disjoint-hpCluster-direct", e_alg_dbScan_brute_hpCluster, true},
    {"k-means", e_hpLysis_clusterAlg_kCluster__AVG, false},
    {"miniBatch", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, false},
    {"HCA-single", e_hpLysis_clusterAlg_HCA_single, false},
    {"HCA-centroid", e_hpLysis_clusterAlg_HCA_centroid, false},
    {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, false},
    {"disjoint-dbScan", e_hpLysis_clusterAlg_disjoint_DBSCAN, false},     
    // {, false},   
  };

  const uint map_matDims_size = 7+1;
  const uint map_matDims[map_matDims_size] = {10, 20, 40, 80, 160, 320, 640, 1280}; //,2560,5120}; //! ie, a doubling
  const char *str_tut = "tut_disjointCluster_4";

  //! Store the results:
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(map_matDims_size, map_algList_size*2);
  
  //! ----------
  for(uint dim_index = 0; dim_index < map_matDims_size; dim_index++) { //! ie, explroe the effects od fiffernet matrix-sizes
    allocOnStack__char__sprintf(2000, str_row, "dim_%u", map_matDims[dim_index]);
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/dim_index, str_row, /*addFor_column=*/false);      
    //!
    uint mat_result_colPos = 0;
    for(uint alg_id = 0; alg_id < map_algList_size; alg_id++) {
      //! Construct the default prefix: 
      if(dim_index == 0) { //! then update the column-header: 
	{
	  allocOnStack__char__sprintf(2000, str, "min:%s", map_algList[alg_id].str);
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/mat_result_colPos, str, /*addFor_column=*/true);
	}
	{
	  allocOnStack__char__sprintf(2000, str, "max:%s", map_algList[alg_id].str);
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/mat_result_colPos+1, str, /*addFor_column=*/true);
	}	
      }
      t_float execTime[2] = {T_FLOAT_MAX, T_FLOAT_MIN_ABS}; //! ie, the worst boundaries wrt. exeuction-time
      allocOnStack__char__sprintf(2000, resultPrefix, "%s_n%u_%s", str_tut, map_matDims[dim_index], map_algList[alg_id].str);
      //! Compute: 
      __compute__tut_disjointCluster_4(resultPrefix, map_algList[alg_id], map_matDims[dim_index], execTime);
      { //! Update min--max:
	for(uint x = 0; x < 2; x++) { //! then apply logis to avoid inseritng 'empty' values:
	  if( (execTime[x] != T_FLOAT_MIN_ABS) && (execTime[x] != T_FLOAT_MAX) ) {
	    
	    mat_result.matrix[dim_index][mat_result_colPos+x] = execTime[x];
	  }
	}
	// mat_result.matrix[dim_index][mat_result_colPos+1] = execTime[1];
      }
      mat_result_colPos += 2;
      assert(mat_result_colPos <= mat_result.ncols);
    }
    /* {//! Write out proepteis wrt. latter: */
    /*   //! Note: [alg]x[cluster-memberships], and (raw,  rank)[alg]x[CCM-scores] */
    /*   //! Buidl a file-path:  */
    /*   allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_%s_time_topology_alg", str_tut, str_row); */
    /*   //! The data-consturciton-call: */
    /*   writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_result, resultPrefix_local, NULL, NULL, /\*isTo_writeOutTransposed=*\/true); */
    /*   //! */
    /*   //! De-allocate */
    /*   free__s_kt_matrix(&mat_result); */
    /* }   */
  }
  {//! Write out proepteis wrt. latter:
    //! Note: [alg]x[cluster-memberships], and (raw,  rank)[alg]x[CCM-scores]
    //! Buidl a file-path: 
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_time_topology_alg", str_tut);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_result, resultPrefix_local, NULL, NULL, /*isTo_writeOutTransposed=*/true);
    //!
    //! De-allocate
    free__s_kt_matrix(&mat_result);
  }  
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

  
