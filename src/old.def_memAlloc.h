#ifndef def_memAlloc_h
#define def_memAlloc_h

/**
   @file def_memAlloc
   @brief describes and generalises a set of memory-allocations.
   @remarks used to simplify siwthcing between different 'states'.
 **/


// FIXME[JC]: do you belive there is a differnece in exueciton-time (of memory-accesses) assicated to "new [] " and "malloc(..)" if we muse "memset(..)" (or for-loops) .... ie, wrt. 'locality in time of requested/required memory-access'? <-- if the latter is the case, would we then expect this 'issue'/'case' to be mroe signfinat if paralll/muliple thresas are allocating meory? <-- if so, are there anby known low-level approach to effciently allocate memory (eg, to avoid 'low-level mamory-locks', ie, if such exist)

//! A standarized approach to ensure that memory is correctly allocated into chunks:
// FIXME[jc]: what are the 'correct values' to be used in the "aligned(..)" call?
#define memAlign_preAllocated  __attribute__ ((aligned (16))) 

#ifndef macroConfig_use_posixMemAlign
#define macroConfig_use_posixMemAlign 0 //! ie, our default assumption
//#define macroConfig_use_posixMemAlign 1 //! ie, our default assumption
#endif

#if macroConfig_use_posixMemAlign == 1 //! then we assume that Intel's intrisntics (eg, SSE code-standards) is ot be used/applied.
//! Start: "Posix memalgin" ----------------------------------------------------------

// FIXME: updatE:


//! complete: "Posix memalgin" ----------------------------------------------------------
#elif macroConfig_use_posixMemAlign == 0

#ifdef __cplusplus
//! Start: "C++" ----------------------------------------------------------

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_float(size, default_value) ({ assert((char)default_value < CHAR_MAX); float *tmp = new float[size]; memset(tmp, (char)default_value, size*sizeof(float)); tmp; })
// FIXME[article]: consider to include [”elow] code as an examplficiaiton of possible 'traps' when convering from functiosn to macros
// #define allocate_1d_list_float(size, default_value) ({ float *tmp = new float[size]; memset(tmp, default_value, size*sizeof(float)); return tmp; })


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_float(list) ({assert(list); float *tmp = *list; delete [] tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_uint(size, default_value) ({uint *tmp = new uint[size]; memset(tmp, default_value, size*sizeof(uint)); tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_uint(list) ({assert(list); uint *tmp = *list;  delete [] tmp; })
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_char(size, default_value) ({ char *tmp = new char[size]; memset(tmp, default_value, size*sizeof(char)); tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_char(list) ({assert(list); char *tmp = *list;  delete [] tmp; })

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_2d_list_uint(size_x, size_y, default_value) ({ \
  uint **tmp = new uint*[size_x]; \
  tmp[0] = new uint[size_x * size_y]; \
  assert((char)default_value < CHAR_MAX); \
  memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(uint)); \
  uint offset = size_y; uint current = size_y; \
  for(uint i = 1; i < size_x; i++) { \
    tmp[i] = tmp[0] + current; current += size_y; \
  } \
  tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_uint(list) ({assert(list); uint **tmp = *list; delete [] tmp[0];   delete [] tmp;})

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_2d_list_char(nrows, ncols, default_value) ({ \
  char **tmp = new char*[nrows]; \
  assert((char)default_value < CHAR_MAX); \
  tmp[0] = new char[ncols*nrows]; \
  memset(tmp[0], (char)default_value, /*size=*/nrows*ncols*sizeof(char)); \
  for(uint i = 0; i < nrows; i++) { \
    tmp[i] = tmp[0] + i*ncols;  \
  } \
  const uint cnt_cells = nrows * ncols;  \
  tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_char(list) ({assert(list);  char **tmp = *list;  delete [] tmp[0];   delete [] tmp;})

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_2d_list_float(size_x, size_y, default_value) ({ \
  float **tmp = new float*[size_x]; \
  tmp[0] = new float[size_x * size_y]; \
  assert((char)default_value < CHAR_MAX); \
  memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(float)); \
  uint offset = size_y; uint current = size_y; \
  for(uint i = 1; i < size_x; i++) { \
    tmp[i] = tmp[0] + current; current += size_y; \
  } \
  tmp;}) //! ie, return
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_float(list) ({ assert(list);  float **tmp = *list;  delete [] tmp[0];   delete [] tmp; })


//! complete: "C++" ----------------------------------------------------------
#else 
//! Start: "ANSI C" ----------------------------------------------------------
//! ----------------------------------------------------------
//! Then in C compialtion-mode, ie, use "malloc(..)" and "free(..)"
assert(false) // ie, a compielr warning to attract our attetinion to writing these.
// FIXME: then write support fo this <-- FIXME[jc]: may cyou consider altenraitve combnations/optimiosn as well ... eg, wrt. the intri-memory-allcoaitonrotunes?

//! complete: "ANSI C" ----------------------------------------------------------
#endif

//! Then we 'contniue' our elvuvation of the macroConfig_use_posixMemAlign parameer
#else 
#warn "Add support for this case" //! ie, as we then need to add soemthing (oekseth, 06. june 2016).
assert(false) //! ie, to syntax-error to 'ensure' a compilation-abort-case.
#endif



//! complete: <file> wrt. "def_memAlloc_h" ----------------------------------------------------------
#endif //! ie, EOF
