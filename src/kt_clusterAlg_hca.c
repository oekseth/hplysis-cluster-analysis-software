#include "kt_clusterAlg_hca.h"

#include "kt_distance.h"

#include "hca_cluster.c" //! where 'this file' is explcitly included in order to 'enable' the implict use of [ªbove] "kt_distance" in the "hca_cluster" appraoch.


// //! @return true if we asusem teh float-value is of interest.
// static float isOf_interest(const float value) {
//   return ( (value != T_FLOAT_MAX) && (value != T_FLOAT_MIN_ABS) );
// }

/**
   @brief translates a tree of vertices into clusters based on the hierarchical clutering-results.
   @param <nelements> The number of elements that were clustered.
   @param <tree> The clustering solution. Each node in the array describes one linking event, with tree[i].left and tree[i].right representig the elements that were joined. The original elements are numbered 0..nelements-1, nodes are numbered -1..-(nelements-1). Size set to "nelements - 1"
   @param <nclusters> The number of clusters to be formed.
   @param <clusterid> The number of the cluster to which each element was assigned. Space for this array should be allocated before calling the cuttree routine. If a memory error occured, all elements in clusterid are set to -1. Size set to "nelements"
   @remarks The cuttree routine takes the output of a hierarchical clustering routine, and divides the elements in the tree structure into clusters based on the hierarchical clustering result. The number of clusters is specified by the user.
**/
void cuttree(const uint nelements, Node_t* tree, const uint nclusters, uint clusterid[]) {

  //printf("\n\n Apply the cuttree(..) proceud,re at %s:%d\n", __FILE__, __LINE__);
  // FIXME: does the tree cotnain references to vertices or clsuters? ... <-- write a code-correctness-test wrt. this function ... ie, in conjuction with tests/applciaitons of cluster-bliraries. in the 'assert-fucniton-call'.
  
#define __Mi__ofInterest(val) (( (val >= 0) && (val != INT_MAX) ))
  uint icluster = 0;
  const uint n = nelements-nclusters; /* number of nodes to join */
  assert(tree);
  assert(nelements >= 2);
  for(int i = (int)nelements-2; i >= n; i--) {
    if(i < 0) {continue;}
    if(i >= nelements){continue;}
    assert(i < nelements);
    //assert(i >= 0);
    //assert(i >= 0);
    const int k1 = tree[i].left;
    if(__Mi__ofInterest(k1)) {
      assert(k1 >= 0);
      if(k1 < (int)nelements) {
	clusterid[k1] = icluster;
	if((icluster+1) < nclusters) {  // TODO: validate correctness of this 'assigment'/if-clause (oekseth, 06. arp. 2017).
	  icluster++;
	}
      }
    }
    const int k2 = tree[i].right;
    if(__Mi__ofInterest(k2)) {
    //if(k2 != INT_MAX) {
      assert(k2 >= 0);
      if(k2 >= (int)nelements) {
	fprintf(stderr, "k2=%d, nelements=%u, at %s:%d\n", k2, nelements, __FILE__, __LINE__);
      }
      if(k2 < (int)nelements) {
	assert(k2 < (int)nelements);
	clusterid[k2] = icluster;
	if((icluster+1) < nclusters) {  // TODO: validate correctness of this 'assigment'/if-clause (oekseth, 06. arp. 2017).
	  icluster++;
	}
      } //! else we assume the latter describes a sytnteic node.
    }
  }

  const uint empty_value_uint = 0;
  uint *nodeid =   allocate_1d_list_uint(/*size=*/n, /*empty-value=*/empty_value_uint); //UINT_MAX);
  for(uint i = 0; i < (uint)n; i++) {nodeid[i] = UINT_MAX;}

  for(int i = (int)n-1; i >= 0; i--) {
    if(i < 0) {continue;}
    assert(nodeid[i] >= 0);
    uint j = nodeid[i];
    assert(i >= 0);
    assert(i < n); //! ie, to be 'insidee' the allcoated size
    if(nodeid[i] == UINT_MAX) {
      j = icluster;
      nodeid[i] = j;
      if((icluster+1) < nclusters) {  // TODO: validate correctness of this 'assigment'/if-clause (oekseth, 06. arp. 2017).
	icluster++;  
      }
    } else {;}
    //assert(tree[i].left >= 0);
    const int k1 = tree[i].left;
    if(k1 < (int)n) {
      if(__Mi__ofInterest(k1) == false) { //! then the latter vertex describes an imginary entit.
	//if(k1 == UINT_MAX) {
	// FIXME: seems like [below] is 'coisnsitent' ... ie, consider using 'negative' "int" numbers in the "Node" structure.      
	const int index_tmp_1 = -k1 - 1; //! eg, as seen wrt. "nodeid[-k-1] = j;" in "cluster.c" wrt. the "cuttree(..)" function.
	assert(index_tmp_1 >= 0);
	assert(index_tmp_1 < n);
	nodeid[index_tmp_1] = j;
      } else clusterid[k1] = j;
    }
    const int k2 = tree[i].right;
    if(k2 < (int)n) {
      if(__Mi__ofInterest(k2) == false) { //! then the latter vertex describes an imginary entit.
	/* const uint k2 = tree[i].right; */
	/* if(k2 == UINT_MAX) { */
	// FIXME: seems like [below] is 'coisnsitent' ... ie, consider using 'negative' "int" numbers in the "Node" structure.
	const int index_tmp_2 = -k2 -1;
	assert(index_tmp_2 < n);
	assert(index_tmp_2 >= 0);
	nodeid[index_tmp_2] = j; 
      } else clusterid[k2] = j;
    }
  }
  
  free(nodeid);
  return;
}



//#define __free__memRef(mem_ref_data, mem_ref_mask) ({if(mem_ref_data) {free_generic_type_2d_wrapper(mem_ref_data); mem_ref_data = NULL;} if(mem_ref_mask) {free_generic_type_2d_wrapper(mem_ref_mask, nrows); mem_ref_mask = NULL;}})

/* ******************************************************************** */

/**
   @brief ...
   @remarks clusters using pairwise centroid-linking on a given set of gene expression data, using the distance metric given by dist.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix" The distance matrix. This matrix is precalculated by the calling routine treecluster. The pclcluster routine modifies the contents of distmatrix, but does not deallocate it.
   @return  a pointer to a newly allocated array of Node_t structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. See src/cluster.h for a description of the Node_t structure.
**/
Node_t* pclcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool needTo_useMask_evaluation) {
  

  assert(transpose == 0); //! ie, what we expect

  const uint nelements = (transpose==0) ? nrows : ncolumns;

  const uint ndata = transpose ? nrows : ncolumns;
  const uint nnodes = nelements - 1;
  const uint ncols = ncolumns;

  const char default_value_char = 0;


  // assert(false); // FIXME: describe how may use 'partiality' wrt. index-ranks ... ie, update our cofnig-object.
  // assert(false); // FIXME: update [above] using our "isTo_useSpearman_rankPreComputation" ... instead 'accessing' a wrapper-function 'of this'.

  const uint default_value_uint = 0;   const t_float default_value_float = 0;

  uint* distid = allocate_1d_list_uint(/*size=*/nelements, default_value_uint); 
  Node_t empty_node; init_Node_t(&empty_node);
  //init__= /*empty-value=*/Node();
  assert(nnodes <= nelements); //! ie, to genrlaize this allcoaiton-proceudre: 
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, empty_node);
  
  t_float *arrOf_tmp_tranposeOld_1 = allocate_1d_list_float(/*size=*/ncolumns, default_value_float);
  t_float *arrOf_tmp_tranposeOld_2 = allocate_1d_list_float(/*size=*/ncolumns, default_value_float);

  t_float** newdata = allocate_2d_list_float(nrows, ncolumns, default_value_float);
  char** newmask_tmp = NULL;
  
  //maskAllocate__makedatamask(nelements, ndata, &newdata, &newmask_tmp, isTo_use_continousSTripsOf_memory);
  //! Allcoate masks wrt. unit-elements
  uint** newmask_uint = allocate_2d_list_uint(nrows, ncolumns, default_value_uint); 
  //newmask_uint[0] = new uint[ndata*nelements];
  //memset(newmask_uint[0], 0, sizeof(uint)*ndata * nelements);
  //for(uint i = 0, offset=0; i < nelements; i++) {newmask_uint[i] = newmask_uint[0] + offset; offset += ndata;}

  for(uint i = 0; i < nelements; i++) {distid[i] = i;}   /* To remember which row/column in the distance matrix contains what */


  /* Storage for node data */

  assert(transpose == 0); //! ei, wehat we expect whenc omptuign for the optimal case.

  /* if(isTo_use_continousSTripsOf_memory) { */
  /*   const uint cnt_total = nelements * ndata; */
  /*   memcpy(newdata[0], data[0], cnt_total*sizeof(t_float)); */
  /*   if(mask) { */
  /*     memcpy(newmask_uint[0], mask[0], cnt_total*sizeof(uint)); */
  /*   } */
  /* } else */ {
    for(uint i = 0; i < nelements; i++) {
      memcpy(newdata[i], data[i], ndata*sizeof(t_float));
      if(mask) {memcpy(newmask_uint[i], mask[i], ndata*sizeof(uint));}
    }
  }
  data = newdata;   
  mask = newmask_uint;
  //if(mask) {assert(ma
  assert(mask); //! ie, what we expect.
  char **mask_internal = NULL;
  if(mask) { //! then allcoate and copy the mask:
    mask_internal = allocate_2d_list_char(nrows, ncolumns, default_value_char);
    for(uint i = 0; i < nrows; i++) { for(uint out = 0; out < ncolumns; out++) { mask_internal[i][out] = mask[i][out]; } } 
  }

  const uint ndata_innerOptimized = (ndata > VECTOR_FLOAT_ITER_SIZE) ? ndata - VECTOR_FLOAT_ITER_SIZE : 0;


  /* Set the metric function as indicated by dist */
  //float (*metric) (uint, float**, float**, char**, char**, const float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);
  void (*metric_oneToMany) (config_nonRank_oneToMany) = setmetric__correlationComparison__oneToMany_maskType__uint(metric_id, weight, mask, mask, needTo_useMask_evaluation);
    const e_cmp_masksAre_used_t  masksAre_used = (mask || needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  t_float (*metric_each) (config_nonRank_each_t) = setmetric__correlationComparison__each(metric_id, weight, mask_internal, mask_internal, needTo_useMask_evaluation);
  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask_internal, mask_internal, weight, transpose, masksAre_used, nrows, ncolumns);
  
  assert(isTo_use_continousSTripsOf_memory); // TODO: cosndier removing 'this'.


  //! Allcoate a tempraory lost to hold the memory-pointers:
  t_float **mem_ref_data = MF__getMemRef_data(data, nrows);
  uint **mem_ref_mask = (mask) ?  MF__getMemRef_mask(mask, nrows) : NULL;

  for (uint inode = 0; inode < nnodes; inode++) { /* Find the pair with the shortest distance */
    uint is = 1;     uint js = 0;
    result[inode].distance = __local__find_minPair(nelements - inode, ncols, distmatrix, &is, &js); //! ie, identify the pair which has the lowest weight.
    //result[inode].distance = find_closest_pair(nelements-inode, distmatrix, &is, &js);
    result[inode].left = distid[js];
    result[inode].right = distid[is];

    /* Make node js the new node */
    for(uint i = 0; i < ndata; i++) { 
      data[js][i] = data[js][i]*mask[js][i] + data[is][i]*mask[is][i];
      mask[js][i] += mask[is][i];
      if (mask[js][i]) data[js][i] /= mask[js][i];
    }

    /* if(isTo_use_continousSTripsOf_memory) { */
    /*   if(is != (nnodes - inode)) { */
    /* 	memcpy(data[is], data[nnodes-inode], sizeof(t_float)*ncolumns); //! ie, as an alternative to ' */
    /*   } */
    /*   if(mask) { */
    /* 	if(is != (nnodes - inode)) { */
    /* 	  memcpy(mask[is], mask[nnodes-inode], sizeof(uint)*ncolumns); //! ie, as an alternative to ' */
    /* 	  assert(mask_internal); */
    /* 	  memcpy(mask_internal[is], mask_internal[nnodes-inode], sizeof(char)*ncolumns); //! ie, as an alternative to ' */
    /* 	} */
    /*   } */
    /* } else */ {
      // FIXME: validate correctness of [below] wrt. the 'continous' meory-allcioation
      // FIXME: replace all corruecnes of "delete []"
      assert(data[is]); 
      //      free_1d_list_float(&data[is]); 
      //      if(mask) {free_1d_list_uint(&mask[is]);}

      data[is] = data[nnodes-inode];
      if(mask) {
	mask[is] = mask[nnodes-inode];
	mask_internal[is] = mask_internal[nnodes-inode];
      }
    }

    // free(data[is]);
    // free(mask[is]);
    // data[is] = data[nnodes-inode];
    // mask[is] = mask[nnodes-inode];
  
    /* Fix the distances */
    distid[is] = distid[nnodes-inode];
    for (uint i = 0; i < is; i++)
      distmatrix[is][i] = distmatrix[nnodes-inode][i];
    for (uint i = is + 1; i < nnodes-inode; i++)
      distmatrix[i][is] = distmatrix[nnodes-inode][i];

    distid[js] = -inode-1;

    //! Comptue for: for (i = 0; i < js; i++) 
    assert(js < nrows);
    /* if(mask_internal) { */
    /*   // FIXME: remove [”elow] block. */
    /*   char sum = 0; */
    /*   for(uint i = 0; i < nrows; i++) { */
    /* 	for(uint j = 0; j < ncolumns; j++) { */
    /* 	  sum += mask_internal[i][j]; */
    /* 	} */
    /*   }	   */
    /* } */
    /* printf("nrows=%u, ncolumns=%u, at %s:%d\n", nrows, ncolumns, __FILE__, __LINE__); */
    //config_metric.mask1 = mask_internal;     config_metric.mask2 = mask_internal;
    if(mask_internal) {
      assert(config_metric.mask1);
    }
    kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, data, /*index1=*/js,  &config_metric, 
			  NULL, //(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute,
			  /*arrOf_result=*/distmatrix[js],
			  metric_oneToMany);

    //assert(false); // fIXME: include [”elow]:
    // for (i = 0; i < js; i++) 
    //   distmatrix[js][i] = metric(ndata,data,data,mask,mask,weight,js,i,0);
    //assert(false); // fIXME: include [”elow]:
    for (uint i = js + 1; i < nnodes-inode; i++) {
      distmatrix[i][js] =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ndata, data, data, /*index1=*/js, /*index2=*/i, config_metric, metric_each,
					    NULL); //(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
    }

    //   distmatrix[i][js] = metric(ndata,data,data,mask,mask,weight,js,i,0);
  }


  

//   for(uint inode = 0; inode < nnodes; inode++) { //! ie, iterate through each of the 'rows':
//     /* Find the pair with the shortest distance */
//     uint is = 1; uint js = 0;
//     assert(nelements >= inode); //! ie, as otherwise would indicate an incosnsitnecy.
//     // FIXME: in [below] try to 'remember' the changes to the 'squares' limited by "local_matrixSize"
//     const uint local_matrixSize = nelements - inode;
    
//     result[inode].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=*/local_matrixSize, /*ncols_threshold=*/local_matrixSize); //  graphAlgorithms_distance::find_closest_pair(local_matrixSize, distmatrix, &is, &js);
//     result[inode].left = distid[js]; result[inode].right = distid[is];
// #ifndef NDEBUG
//     { //! Then we validate correctenss of our "s_obj_closestPair" algorithm-implementaiton:
//       uint is_tmp = 1; uint js_tmp = 0;

//       assert(false); // FIXME: .... consider to both update [”elow] and our procedure wr.t the 'zero' case ... eg, to ingore 'zero-values' from evlauation.
//       const t_float distance_cmp = find_closest_pair(local_matrixSize, distmatrix, &is_tmp, &js_tmp);
//       assert(distance_cmp == result[inode].distance);
//       assert(is_tmp == is); assert(js_tmp == js);
//     }
// #endif
//     /* Make node js the new node */
//     { //! First update for "data"
//       for(uint i = 0; i < ndata; i++) {
// 	data[js][i] = data[js][i]*mask[js][i] + data[is][i]*mask[is][i];
// 	// mask[js][i] += mask[is][i];
// 	// 
//       }
//     }    
//     if(mask) { //! Then update for the mask:
//       for(uint i = 0; i < ndata; i++) {
// 	mask[js][i] += mask[is][i];
// 	if (mask[js][i]) data[js][i] /= mask[js][i];
//       }
//     }


//     if(isTo_use_continousSTripsOf_memory) {
//       memcpy(data[is], data[nnodes-inode], sizeof(float)*ncolumns); //! ie, as an alternative to '
//       if(mask) {
// 	memcpy(mask[is], mask[nnodes-inode], sizeof(uint)*ncolumns); //! ie, as an alternative to '
//       }
//     } else {
//       // FIXME: validate correctness of [below] wrt. the 'continous' meory-allcioation
//       data[is] = data[nnodes-inode];
//       mask[is] = mask[nnodes-inode];
//       // FIXME: replace all corruecnes of "delete []"
//       free_1d_list_float(&data[is]); free_1d_list_uint(&mask[is]);
//     }
  
//     /* Fix the distances */
//     distid[is] = distid[nnodes-inode];

//     //! Copy 'horizontal':
//     assert(false); // FIXME: update ... "arrOf_minValueAtRow" and "arrOf_minValueAtRow_columnIndex" ... at row-index "is" ... and handle cases where we need to udpate for "distmatrix[is]"
//     memcpy(distmatrix[is], distmatrix[nnodes-inode], sizeof(float)*is);
//     //!Copy 'transposed':    
//     for(uint i = is + 1; i < nnodes-inode; i++) {
//       arrOf_tmp_tranposeOld_1[i] = distmatrix[i][is]; //! ie, as we need to remember the old/previous value when calling our "s_dense_closestPair" c-structu-function.
//       distmatrix[i][is] = distmatrix[nnodes-inode][i];
//     }


//     { 
//       // FIXME: add a parallel wrapper for [below] 
//       // FIXME: update our all-against-all distance-matrix-comptuation to only compute for one row .. and then udpate [”elow] ... may result in up to "n*log(n)" performacne-increase.

//       const int new_value = -inode-1;;
//       assert(new_value >= 0); // FIXME: if this does not hold, then consider to use an "int" data-type instead.
//       distid[js] = (uint)new_value;

//       //! Note: as the [above] "data[js][]" is modified, we compute the score wrt. ...??... 
//       //! Note: ...
//       assert(false); // FIXME: wrt. 'euclid' .... write an optimizaiton for distance-computations of a 'fixed' 'index1' <-- optimizaiton found wrt. ...??..
//       assert(false); // FIXME: write a specail case for "kendall" and "spearman" ... where we pre-comptue the ranks wrt. the vertices.
//       assert(false); // FIXME: is [”elow] correct?  <-- 'add seperate torutiens' for "spearman" and  store the rank or sorted matrix for "Kendalls" ... and then write an udpated toruytine 'which udpate for "js" <-- use 
      
            
      
//       //! Then we need to re-compute the distance-matrix, ie, as the 'internal values' has changed:
      
//       assert(false); // FIXME: include a permutation of [”elow]:

//       // for(uint i = 0; i < js; i++) {
//       // 	assert(false); // FIXME: consider to use T_FLOAT_MIN_ABS wrt. 'empty' "0" values, ie, to avoid the wrong values to be inferred wrt. the "s_dense_closestPair" logics.
//       // 	distmatrix[js][i] = metric(ndata,data,data, /*mask=*/NULL, /*mask=*/NULL, weight,js,i, /*transpose=*/0);
//       // }
//       assert(false); // FIXME: include a permutation of [”elow]:
//       // for(uint i = js + 1; i < nnodes-inode; i++) {
//       // 	assert(false); // FIXME: consider to use T_FLOAT_MIN_ABS wrt. 'empty' "0" values, ie, to avoid the wrong values to be inferred wrt. the "s_dense_closestPair" logics.
//       // 	const t_float new_value = metric(ndata,data,data, /*mask=*/NULL, /*mask=*/NULL, weight,js,i, /*transpose=*/0);
//       // 	arrOf_tmp_tranposeOld_2[i] = distmatrix[i][js]; //! ie, as we need to remember the old/previous value when calling our "s_dense_closestPair" c-structu-function.
//       // 	distmatrix[i][js] = new_value; //! ie, update: we use an 'itnermeidate' value "new_value" to decrease the memory-cache-miss-rate, ie, to avodi an explcit look-up of the vlaeu 'before the update'.
//       //      }


//       assert(false); // FIXME: include [below], ie, after we have udpate our HCA algorithm.

//       // //! Update the closest-distance matrix:
//       // if(local_matrixSize > 0) { //! then we udpate 'for the next iteration':	
//       // 	if(js != (local_matrixSize-1)) {
//       // 	  //! Update wrt. the 'vertically updated column':
//       // 	  if(is != js) {
//       // 	    s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/is+1, /*row-endPos-PlussOne*/min(local_matrixSize-1, nnodes-inode), /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1);	  
//       // 	    s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/js, /*row-start-pos=*/js+1, /*row-endPos-PlussOne*/min(local_matrixSize-1, nnodes-inode), /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_2);	  
//       // 	  } else {
//       // 	    s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/is+1, /*row-endPos-PlussOne*/min(local_matrixSize-1, nnodes-inode), /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1);	  
//       // 	  }
//       // 	} //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
		

//       // 	if(is != (local_matrixSize-1)) {
//       // 	  //! Udpate wrt. the two 'hroizontally updated rows':
//       // 	  if(is != js) {
//       // 	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/js, /*column-start-pos=*/0, min(/*column-end-pos-plussOne=*/(local_matrixSize-1), js), /*row-end-pos-plussOne=*/(local_matrixSize-1));
//       // 	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*row-end-pos-plussOne=*/(local_matrixSize-1));
//       // 	  } else { //! then we update for only the 'copied region':
//       // 	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*row-end-pos-plussOne=*/(local_matrixSize-1));
//       // 	  }
//       // 	} //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
//       // }

//     }
//   }

  /* Free temporarily allocated space */
  // FIXME: validate [”elow] ... ie, consider to uset he isTo_use_continousSTripsOf_memory parameter ... and then 'in the caller' store a reference to the 'base-membory-pointer'.
  //s_dense_closestPair_free(&s_obj_closestPair);
  //maskAllocate__freedatamask(nelements, newdata, newmask_tmp, isTo_use_continousSTripsOf_memory);
  MF__free__memRef(mem_ref_data, mem_ref_mask);
  //if(newmask_uint) {assert(newmask_uint); free_2d_list_uint(&newmask_uint, nrows);}
     //  if(mask_internal) {  free_2d_list_char(&mask_internal, nrows); }
  free_1d_list_uint(&distid);
  free_1d_list_float(&arrOf_tmp_tranposeOld_1);
  free_1d_list_float(&arrOf_tmp_tranposeOld_2);
  
  return result;
}

/* ******************************************************************** */


//! Inlcude teh code holdignt eh 'back-compatible' slow functions:
#include "clusterAlg_hca_slow.c"


/* ---------------------------------------------------------------------- */
 
/**
   @brief ... 
   @remarks The pslcluster routine performs single-linkage hierarchical clustering, using either the distance matrix directly, if available, or by calculating the distances from the data array. 
   - This implementation is based on the SLINK algorithm, described in: Sibson, R. (1973). SLINK: "An optimally efficient algorithm for the single-link cluster method". The Computer Journal, 16(1): 30-34.
   - The output of this algorithm is identical to conventional single-linkage hierarchical clustering, but is much more memory-efficient and faster. Hence, it can be applied to large data sets, for which the conventional single-linkage algorithm fails due to lack of memory.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix": The distance matrix. If the distance matrix is passed by the calling routine treecluster, it is used by pslcluster to speed up the clustering calculation. The pslcluster routine does not modify the contents of distmatrix, and does not deallocate it. Ifdistmatrix is NULL, the pairwise distances are calculated by the pslcluster routine from the gene expression data (the data and mask arrays) and stored in temporary arrays. Ifdistmatrix is passed, the original gene expression data (specified by the data and mask arguments) are not needed and are therefore ignored.
   @return A pointer to a newly allocated array of Node_t structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* pslcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, const uint transpose,const bool needTo_useMask_evaluation) {

  //if(isTo_copmuteResults_optimal == false) {return pslcluster_slow(nrows, ncolumns, data, mask, weight, distmatrix, dist, transpose);}

  // FIXME[benchmark]: instead of "int ** mask" use "boolean **mask" ... and then test/evalaute the use of meomry-effiecnt allcoation 'using one strip of allcaitons'.

  // FIXME: explain why and how [below] algorithm is correct.



  const uint nelements = transpose ? ncolumns : nrows;
  const uint nnodes = nelements - 1;
  //! Allcoate and initaite:
  const uint default_value_uint = 0;
  uint* vector  = allocate_1d_list_uint(/*size=*/nnodes, /*empty-value=*/default_value_uint);
  float* temp  = allocate_1d_list_float(/*size=*/nnodes, /*empty-value=*/default_value_uint);
  int* index   = allocate_1d_list_int(/*size=*/nelements, /*empty-value=*/default_value_uint);
  Node_t empty_node; init_Node_t(&empty_node);
  //init__= /*empty-value=*/Node();
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, empty_node);

  //Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/Node());
  for(uint i = 0; i < nnodes; i++) {vector[i] = i;}

  t_float **distanceMatrix_preComputed = NULL;  char **mask_dummy = NULL;
  t_float **local_matrix = distmatrix;
  if(distmatrix == NULL) { 
    // FIXME[docu]: investigate the time-effect of [below] ... ie, when compute to 'inline' computation of distance-matrix ... ie, to evaluate the 'infusion of noise caused by non-seperation between tasks'.
    //maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncolumns, &distanceMatrix_preComputed, &mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);	  
    //! TODO: consider to remove [below] asserts ... adn then add the parameters to [below] funciotn:
    assert(transpose == 0);

    //!
    //! Configure:
    const bool isTo_invertMatrix_transposed = true; const uint CLS = 64; const bool  isTo_use_continousSTripsOf_memory = true;
    const e_cmp_masksAre_used_t  masksAre_used = (mask || needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
    distanceMatrix_preComputed = allocate_2d_list_float(nelements, nelements, default_value_uint);
    assert(distanceMatrix_preComputed);
    for(uint i = 0; i < nelements; i++) {for(uint k = 0; k < nelements; k++) {distanceMatrix_preComputed[i][k] = T_FLOAT_MAX;}}
#ifndef NDEBUG
/* #if(configureDebug_useExntensiveTestsIn__tiling == 1)  */
/*     { //! then we vlaidate wrt. 'specific inptu-values': */
/*       assert(data); */
/*       for(uint i = 0; i < nrows; i++) { */
/* 	for(uint j = 0; j < ncolumns; j++) { */
/* 	  const t_float score = data[i][j]; */
/* 	  assert(isinf(score) == false); */
/* 	  assert(isnan(score) == false); */
/* 	} */
/*       } */
/*     } */
/* #endif */
#endif
    //!
    //! Compute:
    // printf("(start) comptue all-against-all, masksAre_used=%u, at %s:%d\n", masksAre_used, __FILE__, __LINE__);
    kt_compute_allAgainstAll_distanceMetric__extensiveInputParams_maskType__uint(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, data, weight, distanceMatrix_preComputed, mask, mask, transpose, isTo_invertMatrix_transposed, CLS, isTo_use_continousSTripsOf_memory, /*iterationIndex_2=*/UINT_MAX, /*typeOf_postProcessing_afterCorrelation=*/e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*s_kt_computeTile_subResults_t=*/NULL, 
										 // FIXME: add for [”elow].
										 NULL);
    // printf("(finish) comptue all-against-all, at %s:%d\n", __FILE__, __LINE__);
    local_matrix = distanceMatrix_preComputed;
    if(false)
      // FIXME: set this block to "false"
      {
	for(uint i = 0; i < nrows; i++) {
	  for(uint j = 0; j < nrows; j++) {if(local_matrix[i][j] == 0) {local_matrix[i][j] = T_FLOAT_MAX;}}
	}
      }
    if(false)
      {
	printf("matrix:[\n");
	for(uint i = 0; i < nrows; i++) {
	  printf("%u:[", i);
	  for(uint j = 0; j < nrows; j++) {
	    if(local_matrix[i][j] != T_FLOAT_MAX) {
	      printf("%f, ", local_matrix[i][j]);
	    } else {printf("-, ");}
	  }
	  printf("],\n");
	}
	printf("]; at %s:%d\n", __FILE__, __LINE__);
      }
  }


  
  /* Set the metric function as indicated by dist */
  const uint ndata = transpose ? nrows : ncolumns;

  //! Indiefy/suggest 'candidates' below the 'diagonal' of the 'sub-matrix' which each vertex-index 'infers'.
  // FIXME: instead of [below] then consider to comptue the distance-matrix seperately.
  assert(nelements == nrows);
  assert(nrows == ncolumns); //! ie, as we assert the we have a nieghtbour-matrix.

  for(uint i = 0; i < nrows; i++) {
    result[i].distance = T_FLOAT_MAX;
    //! Update the 'suggeestive distances':
    memcpy(temp, local_matrix[i], sizeof(t_float)*i);



    // FIXME[performance]: try to genralise [”elow] procedure .... ie, try to imrpvoe 'this' perofmrance by: case(a) "tmp" update wrt. "k < j" (ie, where we expec teh "tmp" case to not be used/evaluated); case(b) wrt. the "result[j].distance >= result[vector[j]].distance" case where we ....??.... 


    //! Investigate the vertices 'below' "i":
    for(uint j = 0; j < i; j++) {
      const uint k = vector[j];      
      if(isOf_interest(temp[j])) {
	if(result[j].distance >= temp[j]) { //! then the 'current row' has a shorter distance to the feature, ie, "|e(k, j)| <= |e(i, j)|".
	  if(result[j].distance < temp[k]) {temp[k] = result[j].distance;} //! then we have a 'local' improvement, ie, "|e(i, j)| < |e(k, j)|".
	  //! Then update the distance assicated, and the 'preferred' vector:
	  result[j].distance = temp[j];
	  vector[j] = i; //! ie, as the 'current row' is then closer.
	  // printf("\t\titeration[%u]\t vector[j=%u]=%u w/distance=%f, at %s:%d\n", i, j, i, temp[j], __FILE__, __LINE__);
	} else if(temp[j] < temp[k]) {temp[k] = temp[j];} //! then we have a 'local' improvement.
      }
    }

    //! Indeitnfy the best case:
    for(uint j = 0; j < i; j++) {
      if(isOf_interest(result[j].distance) && isOf_interest(result[vector[j]].distance)) {
	if(result[j].distance >= result[vector[j]].distance) {vector[j] = i;}
      }
    }
  }
  
  free_1d_list_float(&temp);

  //! Label each node based on the current node-index:
  for(uint i = 0; i < nnodes; i++) result[i].left = i;
  //! Order the nodes based on the assicated distance (to each node):
  qsort(result, /*result-size=*/nnodes, sizeof(Node_t), nodecompare__Node_t);
  //! Initaite the index:
  for(uint i = 0; i < nelements; i++) index[i] = i;
  //! Update the mapping:
  for(uint i = 0; i < nnodes; i++) {
    const int j = result[i].left; //! ie, the 'real-world-index-id of this node before sorting'.
    const int k = vector[j];
    result[i].left = index[j];
    result[i].right = index[k];
    // printf("[%d] given i=%d, k=%d, w/distance=%f, at %s:%d\n", j, i, k, result[i].distance, __FILE__, __LINE__);
    //printf("node[%u] .left=%u, .right=%u, at %s:%d\n", i, result[i].left, result[i].right, __FILE__, __LINE__);
    //index[k] = i;
    // FIXME: figure out if we may set 'i' instead <-- cahllenge conserns the different name-spaces to use when enumerating .... (oekseth, 06. jan. 2017).
    //index[k] = UINT_MAX;
    
    /* assert(result[i].right != i); //! ie, as we in the 'local name-spac'e does Not expect reflesive relationships (esketh, 06. jan. 2017). */
    /* assert(result[i].left != i); //! ie, as we in the 'local name-spac'e does Not expect reflesive relationships (esketh, 06. jan. 2017). */
    /* const int tmp = i; //! -i-1; */
    const int tmp = -i-1;
    index[k] = tmp;
    /* // printf("[%u]\t tmp=%d, at %s:%d\n", i, tmp, __FILE__, __LINE__); */
    /* /\* assert(tmp >= 0); *\/ */
    /* if(tmp >= 0) { */
    /*   index[k] = (uint)tmp; */
    /*   printf("[%u]\t tmp=%d, index[%u]=%u, at %s:%d\n", i, tmp, k, index[k], __FILE__, __LINE__); */
    /* } else {index[k] = UINT_MAX;} */
    //index[k] = i;
    //index[k] = (uint)tmp;
  }
  /* for(uint i = 0; i < nelements; i++) { */
  /*   //! */
  /*   //! Handle the case where the relationship is reflexive (oesketh, 06. jan. 2017). */
  /*   // TODO: validate corrnectess of [below] 'interpration' (oesketh, 06. jan. 2017). */
  /*   /\* if(result[i].left == i) {result[i].left = INT_MAX;} *\/ */
  /*   /\* if(result[i].right == i) {result[i].right = INT_MAX;} *\/ */
  /*   printf("node[%u] .left=%d, .right=%d, at %s:%d\n", i, result[i].left, result[i].right, __FILE__, __LINE__); */
  /* } */
  free_1d_list_uint(&vector);   free_1d_list_int(&index);
  if(distanceMatrix_preComputed) {
    free_2d_list_float(&distanceMatrix_preComputed, nelements); distanceMatrix_preComputed = NULL;
  }
  return result;
}
/* ******************************************************************** */


/**
   @brief ..
   @remarks The pmlcluster routine performs clustering using pairwise maximum- (complete-) linking on the given distance matrix.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix": The distance matrix, with nelements rows, each row being filled up to the diagonal. The elements on the diagonal are not used, as they are assumed to be zero. The distance matrix will be modified by this routine.
   @return A pointer to a newly allocated array of Node_t structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* pmlcluster(const uint nelements, float** distmatrix) {
  //if(isTo_copmuteResults_optimal == false) {return pmlcluster_slow(nelements, distmatrix);}

  const uint empt0_0 = 0;
  int* clusterid = allocate_1d_list_int(/*size=*/nelements, empt0_0);
  Node_t empty_node; init_Node_t(&empty_node);
  //init__= /*empty-value=*/Node();
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, empty_node);
  //Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/Node());

  /* Setup a list specifying to which cluster a gene belongs */
  for(uint j = 0; j < nelements; j++) clusterid[j] = (int)j;


  // FIXME: explain [below] funciton ... adnt ehn use the latter explanation/description to update the 'extended' "graphAlgorithms_tree::palcluster(..)" funciton/algorithm description.

  for(uint n = nelements; n > 1; n--) {
    uint is = 1;
    uint js = 0;

    result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js);
    
    { 
      /* Fix the distances */
      {
	uint j = 0;
	/* const uint js_intri = VECTOR_FLOAT_maxLenght_forLoop(js); */
	/* if(js_intri > 0) { */
	/*   for(; j < js_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
	/*     VECTOR_FLOAT_storeAnd_max_data(&distmatrix[js][j], &distmatrix[js][j]); */
	/*   } */
	/* } */
	for(; j < js; j++) {
	  distmatrix[js][j] = max(distmatrix[is][j], distmatrix[js][j]);
	}
      }
      //! Then a set of transposed updates, ie, which are not 'sutied' for data-updates.
      // TODO[jc]: do you amange to optmize [”elow] ... ie, to store only one "float" .... ??
      for(uint j = js+1; j < is; j++) {
	distmatrix[j][js] = max(distmatrix[is][j], distmatrix[j][js]);
      }
      for(uint j = is+1; j < n; j++) {
	distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);
      }
      //! Copy:
      if(is != (n-1)) {
	memcpy(distmatrix[is], distmatrix[n-1], sizeof(t_float)*is); //! ie, which represetns: for(uint j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
      }
      for(uint j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];
    }

    /* Update clusterids */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];
    //assert(n >= (nelements - 1)); // IFMX:E if [below] does not hold, then consider to update/elvaute [below].
    clusterid[js] = n-nelements-1;
    clusterid[is] = clusterid[n-1];
  }
  //! De-allcoate the locally reserved memory:
  free_1d_list_int(&clusterid);

  return result;
}

/* ******************************************************************* */


/**
   @brief 
   @remarks The palcluster routine performs clustering using pairwise average linking on the given distance matrix.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix": The distance matrix, with nelements rows, each row being filled up to the diagonal. The elements on the diagonal are not used, as they are assumed to be zero. The distance matrix will be modified by this routine.
   @return A pointer to a newly allocated array of Node_t structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* palcluster(const uint nelements, float** distmatrix) {
  //if(isTo_copmuteResults_optimal == false) {return palcluster_slow(nelements, distmatrix);}
  assert(nelements > 0);
  if(nelements == 0) {return NULL;}
  /* Setup a list specifying to which cluster a gene belongs, and keep track
   * of the number of elements in each cluster (needed to calculate the
   * average). */
  const uint empty_0 = 0;
  int* clusterid = allocate_1d_list_int(/*size=*/nelements, /*empty-value=*/empty_0);
  uint* number = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/empty_0);
  Node_t empty_node; init_Node_t(&empty_node);
  //init__= /*empty-value=*/Node();
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, empty_node);
  //Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/Node());
  for(uint j = 0; j < nelements; j++) {
    number[j] = 1; clusterid[j] = j;
  }


  uint min_distance = UINT_MAX;

  for(uint n = nelements; n > 1; n--) {
    // FIXME: use uitn instead of int, and tghen update the "find_closest_pair(..)" function.
    uint is = 1;     uint js = 0;
    //! Note: [”elow] call is expected to have a 'dratiatic' time-complexity.
    // FIXME: consider instead to use a different strucutre for [below] ... eg, to 'compute the min-value only once' ... and then udpate all other calls to the "find_closest_pair(..)" funciton.
    // FIXME[JC]: call a parallel version of [below] funciton. <-- may you write/us the OpenMP-paralle verison?
    result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js); //! ie, identify the pair which has the lowest weight.
    if(min_distance != UINT_MAX) {
      // FIXME: when we have understood this algorithm, then try to suggest a new approach to 'collec't the valeus for each [0 ... nelements] range ... and validate ath this updated approach provude the correct results.
      // assert(
    }
    /* Save result */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];

    /* Fix the distances */
    const uint sum = number[is] + number[js]; //! ie, the combined number/signfiance of the pair.
    //! Identify the relative importance of the pair-distance, ie, assicated to the 'minimal' column wrt. the rows:
    //! Step(1): ...
    // FIXME: complete [below] "note".
    //! Note: in [below] the 'iteration-threshold is set to "js": ... 
    {
      for(uint j = 0; j < js; j++) { //! ie, [js][0...js] , which for all js>0 will result in updates.
	distmatrix[js][j] = distmatrix[is][j]*number[is]  	  + distmatrix[js][j]*number[js];
	distmatrix[js][j] /= sum;
      }
    }

    //! Note: in [below] the 'iteration-threshold is set to "is":
    for(uint j = js+1; j < is; j++) { //! ie, [js...is][js] , which implies that condtion (js < is) will not be evalauted/applied/updated.
      distmatrix[j][js] = distmatrix[is][j]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;
    }

    //! Note: in [below] the 'iteration-threshold is set to "n":
    for(uint j = is+1; j < n; j++) { //! ie, [is...n][js] , which implies that 
      distmatrix[j][js] = distmatrix[j][is]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;
    }

    //! Copy:
    //assert( is != (n-1));
    if(is != (n-1)) {
      memcpy(distmatrix[is], distmatrix[n-1], sizeof(t_float)*is); //! ie, which represents: for(uint j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
    }
    for(uint j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];

    /* Update number of elements in the clusters */
    number[js] = sum; //! ie, the signfiance in 'this' iteration.
    number[is] = number[n-1]; //! ie, the sigifance/importance/number in the previous iteration.

    /* Update clusterids */
    //assert(n >= (nelements - 1)); // IFMX:E if [below] does not hold, then consider to update/elvaute [below].
    clusterid[js] = (int)n - (int)nelements - 1; //! ie, an incremntal increase.
    clusterid[is] = clusterid[n-1]; //! ie, inverse of "clusterid[js]".
  }
  //! De-allcoate the locally reserved memory:
  free_1d_list_int(&clusterid); free_1d_list_uint(&number);

  return result;
}

/* ******************************************************************* */

/**
   @brief ...
   @remarks The treecluster routine performs hierarchical clustering using pairwise single-, maximum-, centroid-, or average-linkage, as defined by method, on a given set of gene expression data, using the distance metric given by dist. If successful, the function returns a pointer to a newly allocated Tree struct containing the hierarchical clustering solution, and NULL ifa memory error occurs. The pointer should be freed by the calling routine to prevent memory leaks.
   @remarks in below we describe a few number of the parameters:
   # "method": Defines which hierarchical clustering method is used:
   - method=='s': pairwise single-linkage clustering
   - method=='m': pairwise maximum- (or complete-) linkage clustering
   - method=='a': pairwise average-linkage clustering
   - method=='c': pairwise centroid-linkage clustering
   - For the first three, either the distance matrix or the gene expression data is sufficient to perform the clustering algorithm. For pairwise centroid-linkage clustering, however, the gene expression data are always needed, even if the distance matrix itself is available.
   # "distmatrix": The distance matrix. Ifthe distance matrix is zero initially, the distance matrix will be allocated and calculated from the data by treecluster, and deallocated before treecluster returns. Ifthe distance matrix is passed by the calling routine, treecluster will modify the contents of the distance matrix as part of the clustering algorithm, but will not deallocate it. The calling routine should deallocate the distance matrix after the return from treecluster.
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
   @return A pointer to a newly allocated array of Node_t structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* treecluster__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, float** data, uint** mask, float weight[], uint transpose, char method, float** distmatrix, const bool isTo_use_continousSTripsOf_memory, const bool isTo_useImplictMask, const bool isTo_use_fastFunction) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description; ... 
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption. 

  if(distmatrix == NULL) {
    if(nrows != ncolumns) {
      fprintf(stderr, "!!\t(criticla-error)\t We will now abort: you have provided a matrix which has different number of columns VS rows, ie, an iniput-matrix with diemsions=(%u, %u). From the latter we infer that the input-matrix is Not an adjcency-matricx, ie, in cotnrast to what we expect: pelase update your call (eg, by first calling our \"kt_matrix__distancematrix(..)\" function). For quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nrows, ncolumns, __FUNCTION__, __FILE__, __LINE__);
      return NULL;
    }
  }

  // FIXME: udpat ethe callers of this funciton with the isTo_use_continousSTripsOf_memory parameter ... adnt valdiate taht "delete" is used instead of "free"

  // FIXME: write peroffmrance-evalaution wrt. the isTo_useImplictMask parameter ... and isTo_use_fastFunction

  const bool isTo_invertMatrix_transposed = true;

  const uint nelements = (transpose==0) ? nrows : ncolumns;
  const uint ldistmatrix = (distmatrix==NULL && method!='s') ? 1 : 0;


  if(nelements < 2) {
    assert(false);
    return NULL;
  }



  bool needTo_useMask_evaluation = true;
  if(isTo_useImplictMask) {
  //if(isTo_useImplictMask && mask) {
    assert(data);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask__uint(nrows, ncolumns, data, NULL, mask, NULL, transpose, /*iterationIndex_2=*/UINT_MAX);
    if(needTo_useMask_evaluation) {
      s_mask_api_config_t config_mask; setTo_empty__s_mask_api_config(&config_mask);
      applyImplicitMask_fromUintMask__updateData__mask_api(data, mask, nrows, ncolumns, config_mask);
    }
    //needTo_useMask_evaluation = graphAlgorithms_distance::update_distanceMatrix_withImplicitMask_fromMaskMatrix(nrows, ncolumns, data, mask);
  } /* else if(!mask) { */
  /*   assert(false); // FIXME: write a routien to ivnestgiate if T_FLOAT_MAX si set <-- use one of the existing rotuines ... named ...??.. */
  /* } */


  float **distanceMatrix_intermediate = NULL; uint **mask_dummy = NULL;  //! Then we a transposed matrix out of the input-tranpose
  if((transpose == 1) && isTo_invertMatrix_transposed) {
    //! Allocate memory, ie, using the 'inverse settings' as [above]:
    maskAllocate__makedatamask__uint(/*nrows=*/ncolumns, /*ncols=*/nrows, &distanceMatrix_intermediate, &mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);	  
    
    //! TODO: consider to remove [below] asserts ... adn then add the parameters to [below] funciotn:
    if(mask) {
      mask_dummy = allocate_2d_list_uint(ncolumns, nrows, 0);
      matrix__transpose__compute_transposedMatrix_uint(nrows, ncolumns, mask, mask_dummy, /*isTo_useIntrisinistic=*/true);
    }
    if(distmatrix) { //! then we also invert the distnace-matrix.
      // FIXME: veiorfuy correctness of this assumtpion ... ie, wrt. the dimesniosn in teh caller of this funciton.
      matrix__transpose__compute_transposedMatrix_float(nrows, ncolumns, distmatrix, /*mask=*/NULL, /*isTo_useIntrisinistic=*/true);
    }
    //! Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, NULL, distanceMatrix_intermediate, NULL, /*use_SSE=*/true);
    //! Update, ie, 'invert' the variables:
    transpose = 0; const uint tmp = nrows; 
    nrows = ncolumns; ncolumns = tmp;    
    float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    uint **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;    
  }



  /* Calculate the distance matrix if the user didn't give it */
  if(ldistmatrix) {
    //! Note: for [”elow] we for "transpose=0" has a time-complexity of: "nrows" * "ncolumns" * "ncolumns".
    // if(isTo_useImplictMask) {
    //   distmatrix = graphAlgorithms_distance::distancematrix(nrows, ncolumns, data, /*mask=*/NULL, weight, dist, transpose, isTo_use_continousSTripsOf_memory, isTo_use_fastFunction);
    // } else {      
    //   distmatrix = graphAlgorithms_distance::distancematrix(nrows, ncolumns, data, /*mask=*/mask, weight, dist, transpose, isTo_use_continousSTripsOf_memory, isTo_use_fastFunction);
    // }
    
    const bool isTo_invertMatrix_transposed = true;
    t_float **matrix_result = NULL;
    const t_float default_value_float = 0;
    if(transpose == 0) {matrix_result = allocate_2d_list_float(nrows, ncolumns, default_value_float);}
    else {matrix_result = allocate_2d_list_float(ncolumns, nrows, default_value_float);}
    s_allAgainstAll_config_t config = get_init_struct_s_allAgainstAll_config();
    set_metaMatrix(&config, /*isTo_init=*/true, /*mask=*/NULL, /*mask=*/NULL, weight, transpose, /*masksAre_used=*/e_cmp_masksAre_used_undef, nrows, ncolumns);
    const bool isTo_use_fastFunction = true;
    if(isTo_use_fastFunction == true) {init_useFast__s_allAgainstAll_config(&config, /*masksAre_used=*/e_cmp_masksAre_used_undef);}
    config.isTo_invertMatrix_transposed = isTo_invertMatrix_transposed;
    
    if(mask && isTo_useImplictMask) {
      set_masks_typeOfMask_uint__s_allAgainstAll_config(&config, /*isTo_init=*/false, mask, mask, /*isTo_translateMask_intoImplictDistanceMatrix=*/true, nrows, ncolumns, data, data, /*isTo_use_continousSTripsOf_memory=*/true);
    }
    
    kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, data, /*result-matrix=*/matrix_result, config, NULL);

    distmatrix = matrix_result;
    assert(distmatrix);
    if(!distmatrix) return NULL; /* Insufficient memory */
  }

  //printf("at %s:%d\n", __FILE__, __LINE__);

#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif
#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif

  t_float **local_distmatrix = (distmatrix != NULL) ? distmatrix : data;

  const bool __isTo_use_fastFunction = false; //! ie, override the inptu-settings.

  Node_t* result = NULL;
  switch(method) 
    { case 's': //! e_kt_clusterAlg_hca_type_single,
      //! Note[time]: in [below] we have two cases: for case(distmatrix != NULL) the cost is "|n|"; for case case(distmatrix != NULL) "metric(..)" is called "n*n", ie, with a time-cost "|n*n*n|"
	
	result = pslcluster(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, distmatrix, transpose, needTo_useMask_evaluation);
	//}
  	break;
    case 'm': //! e_kt_clusterAlg_hca_type_max,
      //! Note[time]: in [below] the "graphAlgorithms_distance::find_closest_pair(..)" call for "[0, ... (nnodes-inode)]" 'results' in the comptuation to take |n*n| exeuctione-time
      assert(sizeof(hca_node_t) == sizeof(Node_t)); //! ie, as we expec tthey describe the same structure

      if(__isTo_use_fastFunction == true) { result = (Node_t*)hca_computeCluster_pml(nrows, ncolumns, local_distmatrix); }
      else {result = pmlcluster(nelements, local_distmatrix);}
      break;
    case 'a': //! e_kt_clusterAlg_hca_type_average,
      //! Note[time]: in [below] the "graphAlgorithms_distance::find_closest_pair(..)" call for "[0, ... (nnodes-inode)]" 'results' in the comptuation to take |n*n| exeuctione-time
      assert(sizeof(hca_node_t) == sizeof(Node_t)); //! ie, as we expec tthey describe the same structure
      if(__isTo_use_fastFunction == true) { result = (Node_t*)hca_computeCluster_pal(nrows, ncolumns, local_distmatrix); }
      else {result = palcluster(nelements, local_distmatrix);}
      break;
    case 'c': //! e_kt_clusterAlg_hca_type_centroid
      assert(isTo_use_continousSTripsOf_memory == true); //! ie, to simplify our allocation-process.
      //! Note[time]: in [below] the "metric(..)" call for "[0, ... (nnodes-inode)]" 'results' in the comptuation to take |n*n| exeuctione-time
      assert(sizeof(hca_node_t) == sizeof(Node_t)); //! ie, as we expec tthey describe the same structure
      if(__isTo_use_fastFunction) { result = (Node_t*)hca_computeCluster_pcl(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, local_distmatrix, isTo_use_continousSTripsOf_memory, needTo_useMask_evaluation); } 
      else { result = pclcluster(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, local_distmatrix, transpose, isTo_use_continousSTripsOf_memory, needTo_useMask_evaluation); }
      break;
    }

  //printf("at %s:%d\n", __FILE__, __LINE__);

  /* Deallocate space for distance matrix, ifit was allocated by treecluster */
  if(ldistmatrix) {
    //    if(isTo_use_continousSTripsOf_memory) {
    free_2d_list_float(&distmatrix, nrows);
    /* } else { */
    /*   free_2d_float(&distmatrix); distmatrix = NULL; */
    /*   /\* for(uint i = 1; i < nelements; i++) free_1d_list_float(&distmatrix[i]); *\/ */
    /*   /\* delete [] distmatrix; *\/ */
    /* } */
  }
  //! Ivnestigate if we are to 'invert' the results:
  if((transpose == 1) && isTo_invertMatrix_transposed) {
    assert(distanceMatrix_intermediate);
    //! 'Back-translate' the distance-matrix, ie, compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    assert(data != distanceMatrix_intermediate); //! Note: at this exeuction-point we assume that "data" is the transpsoied matrix, ie, given our precivous/earlier call to [below] function
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, NULL, distanceMatrix_intermediate, NULL, /*use_SSE=*/true);    

    float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    uint **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;    
    maskAllocate__freedatamask__uint(/*nelements=*/ncolumns, distanceMatrix_intermediate, mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);    
  }
 


  return result;
}




// #include "measure.h"
// #include "bm_vertex.h"
// #include "list_for_bm.h"


// //! Ends the time measurement for the array:
// static void __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const uint nclusters, const uint transpose, class measure &time, const class measure *measure_linear) {
//   // FIXME: mvoe tyhis fucniton to our test-bed-time-measure-file.
//   assert(stringOf_measureText);
//   assert(strlen(stringOf_measureText));
//   //! Complte the measurement:
//   time.end_time_measurement();
//   //! Get the measurement, given that we are interested in them:
//   {
//     //! Provides an identficator for the string:
//     char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
//     sprintf(string, "(finding %u relations|nclusters=%u with %s, where transpose='%s')", size_of_array, nclusters, stringOf_measureText, (transpose) ? "yes" : "no");  
//     //! Prints the data:
//     time.print_formatted_result(string); 
//     delete [] string; string = NULL; 
//   } 
//   if(measure_linear) {
//     //! Provides an identficator for the string:
//     char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
//     sprintf(string, "(ideal comparison: finding %u relations|nclusters=%u with %s, where transpose='%s')", size_of_array, nclusters, stringOf_measureText, (transpose) ? "yes" : "no");  
//     //! Prints the data:
//     time.compare_with_linear_version(measure_linear, string);
//     delete [] string; string = NULL; 
//   }
//   time.free_memory();
// }


// /**
//    @brief the system test for class rule_set.
//    @param <print_info> if set a limited status information is printed to STDOUT.
//    @return true if test completed.
// **/
// bool graphAlgorithms_tree::assert_class(const bool print_info) {
//   { //! Test signficance of pre-computing the distnace-amtrix before the 'clsutering':
//     const uint size_of_array = 1000; const uint nrows = size_of_array; const uint ncols = 100; 
//     float **distmatrix = NULL; char **mask = NULL;
//     //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
//     const bool isTo_use_continousSTripsOf_memory = true;
//     maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
//     //! Start:
//     class measure time = measure(); time.start_time_measurement();	    
//     // FIXME: test [”elow] with and without 'pre-computation fot he tidstnace-matrix'.
//     graphAlgorithms_tree::pslcluster(nrows, ncols, distmatrix, /*mask=*/NULL, /*weight=*/NULL, /*distmatrix==*/NULL, /*dist=*/'e', /*transpose=*/0);
//     __assertClass_generateResultsOf_timeMeasurements("centroid-computation::init-random::median", size_of_array, /*nclusters=*/UINT_MAX, /*transpose=*/0, time, NULL);
//     //! De-allocate locally reseerved memory:
//     maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
//   }
// }
