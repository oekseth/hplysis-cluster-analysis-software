/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


#include "def_intri.h"
#include "libs.h"
#include "dense_closestPair_base.h"
#include "s_dense_closestPair.h"

//! A local ANSI C style funciton to find the min-staince
static t_float __local__find_minPair(const uint nrows, const uint ncols, t_float** distmatrix, uint* ip, uint* jp) {
  t_float min_value = distmatrix[0][0]; 
  *ip = 1;
  *jp = 0;
  for(uint i = 0; i < nrows; i++) {
    for(uint j = 0; j < ncols; j++) {
      const t_float temp = distmatrix[i][j];
      if(temp <= min_value) {      
	min_value = temp;
        *ip = i;
        *jp = j;
      }
    }
  }
  return min_value;
}


//! Provides examples (and correctness-tests) to demonstrate/illsutrat ethe use of our "s_dense_closestPair" ANIS C structure/logics.
int main() {

  { //! Valdiate the intaiton:
    const uint nrows = 4; const uint ncols = 5; const t_float default_value_float = 0;
    t_float **distmatrix = allocate_2d_list_float(nrows, ncols, default_value_float);
    //! Set values, ie, to simplify testing wrt. correctness:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	distmatrix[row_id][col_id] = (t_float)col_id;
      }
    }

    //! Intiate the object:
    const uint cnt_bucketsEachRows = ncols; //! ie, to simplify testing.
    // const uint cnt_bucketsEachRows = ncols / /*block-size=*/1024;
    s_dense_closestPair_t s_obj_closestPair; s_dense_closestPair_init(&s_obj_closestPair, nrows, ncols, cnt_bucketsEachRows, distmatrix, /*config_useIntrisintitcs=*/false, /*isTo_use_fast_minTileOptimization=*/true);

    { //! For "__forAll_update_globalMin_atBucketIndex(..)" ... write correctness-tests and perofrmance-tests seperately for each of the [above] funcitons:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	uint bucket_id = 0;
	for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
	  const t_float min_value = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  assert(min_value == (t_float)bucket_id); //! ie, as we expect 'each bucket to correspond to the column-pos'
	}
      }
    }
    { //! For "__init_minPairs_eachBucket(..)" ... write correctness-tests and perofrmance-tests seperately for each of the [above] funcitons.
      uint bucket_id = 0;
      for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
	const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id];
	assert(min_value == (t_float)bucket_id); //! ie, as we expect 'each bucket to correspond to the column-pos'
	const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	assert(indexAt_min_value == (nrows -1 )); //! ie, as we (a) expect each column to be the same for all rows and (b) that the "<=" is used to updat ethe rows, ie, that the last row is used 'as a frame of reference'.
      }
    }
    { //! Validate "s_dense_closestPair_getClosestPair(..)"
      uint expected_index1 = 0;  uint expected_index2 = 0;
      const t_float minValue_expected = __local__find_minPair(nrows, ncols, distmatrix, &expected_index1, &expected_index2);
      uint scalar_index1 = UINT_MAX; uint scalar_index2 = UINT_MAX;
      const t_float minValue = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &scalar_index1, &scalar_index2, nrows, ncols);
      if(true) {printf("\t result: index1=(%u, %u), index2=(%u, %u), minValue=(%f, %f), at %s:%d\n", expected_index1, scalar_index1, expected_index2, scalar_index2, minValue_expected, minValue, __FILE__, __LINE__);}
      assert(expected_index1 == scalar_index1);
      assert(expected_index2 == scalar_index2);
      assert(minValue == minValue_expected);
    }
    { //! Test row-updates:
      { //! Evalaute the case where the input-data is unchanged:
	const uint row_id = 0; const uint column_startPos = 0; const uint column_endPosPlussOne = ncols; const uint row_endPosPlussOne = nrows;
	s_dense_closestPair_updateAt_row(&s_obj_closestPair, row_id, column_startPos, column_endPosPlussOne, row_endPosPlussOne);
	{ //! What we expect:
	  //! Note: [”elow] is a copy-paste of [above] wrt. correctneess-tests:
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    uint bucket_id = 0;
	    for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
	      const t_float min_value = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	      assert(min_value == (t_float)bucket_id); //! ie, as we expect 'each bucket to correspond to the column-pos'
	    }
	  }
	  uint bucket_id = 0;
	  for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
	    const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id];
	    assert(min_value == (t_float)bucket_id); //! ie, as we expect 'each bucket to correspond to the column-pos'
	    const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	    assert(indexAt_min_value == (nrows -1 )); //! ie, as we (a) expect each column to be the same for all rows and (b) that the "<=" is used to updat ethe rows, ie, that the last row is used 'as a frame of reference'.
	  }
	}
      }
      { //! Validate "s_dense_closestPair_getClosestPair(..)"
	uint expected_index1 = 0;  uint expected_index2 = 0;
	const t_float minValue_expected = __local__find_minPair(nrows, ncols, distmatrix, &expected_index1, &expected_index2);
	uint scalar_index1 = UINT_MAX; uint scalar_index2 = UINT_MAX;
	const t_float minValue = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &scalar_index1, &scalar_index2, nrows, ncols);
	if(true) {printf("\t result: index1=(%u, %u), index2=(%u, %u), minValue=(%f, %f), at %s:%d\n", expected_index1, scalar_index1, expected_index2, scalar_index2, minValue_expected, minValue, __FILE__, __LINE__);}
	assert(expected_index1 == scalar_index1);
	assert(expected_index2 == scalar_index2);
	assert(minValue == minValue_expected);
      }
      { //! Evalaute the case where the data is changed:
	const uint row_id = nrows-1; const uint column_startPos = 1; const uint column_endPosPlussOne = 3; const uint row_endPosPlussOne = nrows;
	assert(ncols == cnt_bucketsEachRows); //! ie, to simplify testing.
	assert(row_id < nrows);
	assert((column_startPos+0) < ncols);
	assert((column_startPos+1) < ncols);
	//! Set the test-values, ie, update the data, both wrt. 'new min' and 'new max':
	//! Note: givne teh "<=" operand used in our "__init_minColumns_eachBucket(..)" we expect "ncols-1" to be the 'best-matching' row, ie, for which we assume that 'this call' will provide/ensure testing of both 'new min' and 'better min' test-cases.
	const t_float test_minValue = T_FLOAT_MIN_ABS + 100; const t_float test_maxValue = T_FLOAT_MAX-100; //! ie, new max-value
	distmatrix[row_id][column_startPos] = test_minValue; //! ie, new min-value.
	distmatrix[row_id][column_startPos+1] = test_maxValue;
	//! Teh call:
	s_dense_closestPair_updateAt_row(&s_obj_closestPair, row_id, column_startPos, column_endPosPlussOne, row_endPosPlussOne);

	//! What we expect:
	{ //! Valdiate for teh new min-case:
	  const uint bucket_id = column_startPos;
	  const t_float min_value_inner = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  assert(min_value_inner == test_minValue); //! ie, that the latter is corectly updated.
	  const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	  const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	  assert(min_value == test_minValue);
	  assert(indexAt_min_value == row_id);
	}
	{ //! Validate that the max-index has changed:
	  const uint bucket_id = column_startPos+1;
	  const t_float min_value_inner = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  assert(min_value_inner == test_maxValue); //! ie, that the latter is corectly updated.
	  const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	  const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	  if(true) {printf("compare: (found, !expected) = (%f VS !%f), at %s:%d\n", min_value, test_maxValue, __FILE__, __LINE__);}
	  assert(min_value != test_maxValue);
	  assert(min_value == (t_float)bucket_id);
	  assert(indexAt_min_value == (row_id-1));
	}
	{ //! Validate "s_dense_closestPair_getClosestPair(..)"
	  uint expected_index1 = 0;  uint expected_index2 = 0;
	  const t_float minValue_expected = __local__find_minPair(nrows, ncols, distmatrix, &expected_index1, &expected_index2);
	  uint scalar_index1 = UINT_MAX; uint scalar_index2 = UINT_MAX;
	  const t_float minValue = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &scalar_index1, &scalar_index2, nrows, ncols);
	  if(true) {printf("\t result: index1=(%u, %u), index2=(%u, %u), minValue=(%f, %f), at %s:%d\n", expected_index1, scalar_index1, expected_index2, scalar_index2, minValue_expected, minValue, __FILE__, __LINE__);}
	  assert(expected_index1 == scalar_index1);
	  assert(expected_index2 == scalar_index2);
	  assert(minValue == minValue_expected);
	}
      }
      { //! bakc-track teh cahgnesEvalaute the case where the data is changed:
	if(true) {printf("\n\n\n----------------, at %s:%d\n", __FILE__, __LINE__);}
	const uint row_id = nrows-1; const uint column_startPos = 1; const uint column_endPosPlussOne = 3; const uint row_endPosPlussOne = nrows;
	assert(ncols == cnt_bucketsEachRows); //! ie, to simplify testing.
	assert(row_id < nrows);
	assert((column_startPos+0) < ncols);
	assert((column_startPos+1) < ncols);
	//! Set the test-values, ie, update the data, both wrt. 'new min' and 'new max':
	//! Note: givne teh "<=" operand used in our "__init_minColumns_eachBucket(..)" we expect "ncols-1" to be the 'best-matching' row, ie, for which we assume that 'this call' will provide/ensure testing of both 'new min' and 'better min' test-cases.
	const t_float test_minValue = (t_float)column_startPos; const t_float test_maxValue = (t_float)(column_startPos+1); //! ie, new max-value
	distmatrix[row_id][column_startPos] = test_minValue; //! ie, new min-value.
	distmatrix[row_id][column_startPos+1] = test_maxValue;
	//! Teh call:
	s_dense_closestPair_updateAt_row(&s_obj_closestPair, row_id, column_startPos, column_endPosPlussOne, row_endPosPlussOne);

	//! What we expect:
	{ //! Valdiate for teh new min-case:
	  const uint bucket_id = column_startPos;
	  const t_float min_value_inner = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  assert(min_value_inner == test_minValue); //! ie, that the latter is corectly updated.
	  const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	  const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	  assert(min_value == test_minValue);
	  assert(indexAt_min_value == row_id);
	}
	{ //! Validate that the max-index has changed:
	  const uint bucket_id = column_startPos+1;
	  const t_float min_value_inner = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  assert(min_value_inner == test_maxValue); //! ie, that the latter is corectly updated.
	  const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	  const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	  if(true) {printf("compare: (found, expected) = (%f VS %f), at %s:%d\n", min_value, test_maxValue, __FILE__, __LINE__);}
	  assert(min_value == test_maxValue);
	  assert(min_value == (t_float)bucket_id);
	  assert(indexAt_min_value == (row_id-0));
	  //assert(indexAt_min_value == (row_id-1));
	}
	{ //! Validate "s_dense_closestPair_getClosestPair(..)"
	  uint expected_index1 = 0;  uint expected_index2 = 0;
	  const t_float minValue_expected = __local__find_minPair(nrows, ncols, distmatrix, &expected_index1, &expected_index2);
	  uint scalar_index1 = UINT_MAX; uint scalar_index2 = UINT_MAX;
	  const t_float minValue = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &scalar_index1, &scalar_index2, nrows, ncols);
	  if(true) {printf("\t result: index1=(%u, %u), index2=(%u, %u), minValue=(%f, %f), at %s:%d\n", expected_index1, scalar_index1, expected_index2, scalar_index2, minValue_expected, minValue, __FILE__, __LINE__);}
	  assert(expected_index1 == scalar_index1);
	  assert(expected_index2 == scalar_index2);
	  assert(minValue == minValue_expected);
	}
      }      
    }

    { //! Validate "s_dense_closestPair_updateAt_column(..)":
      //! Allcoate a temproary list of values:
      t_float *arrOf_previouslyUsedValues = allocate_1d_list_float(nrows, default_value_float);
      { //! Test-case: set the valeus to 'our defualt test-setting':
	const uint column_pos = 1; const t_float test_minValue = (t_float)column_pos; 
	const uint row_startPos = 0; const uint row_endPosPlussOne = nrows; const uint column_endPosPlussOne = ncols;
	//! Set the 'previous' values:
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  arrOf_previouslyUsedValues[row_id] = test_minValue;
	  distmatrix[row_id][column_pos] = test_minValue; //! ie, a for cosnsitency.
	}
	//! The call:
	s_dense_closestPair_updateAt_column(&s_obj_closestPair, column_pos, row_startPos, row_endPosPlussOne, column_endPosPlussOne, arrOf_previouslyUsedValues, /*uint *arrOf_condensed_columnPos_toUse=*/NULL, NULL, 0);

	//! Valdite the results, ie, what we expect:
	{ //! Valdiate for teh new min-case:
	  const uint bucket_id = column_pos;
	  for(uint row_id = row_startPos; row_id < row_endPosPlussOne; row_id++) {
	    const t_float min_value_inner = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	    assert(min_value_inner == test_minValue); //! ie, that the latter is corectly updated.
	    const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	    const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	    assert(min_value == test_minValue);
	  }
	  //assert(indexAt_min_value == row_id);
	  { //! Validate "s_dense_closestPair_getClosestPair(..)"
	    uint expected_index1 = 0;  uint expected_index2 = 0;
	    const t_float minValue_expected = __local__find_minPair(nrows, ncols, distmatrix, &expected_index1, &expected_index2);
	    uint scalar_index1 = UINT_MAX; uint scalar_index2 = UINT_MAX;
	    const t_float minValue = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &scalar_index1, &scalar_index2, nrows, ncols);
	    if(true) {printf("\t result: index1=(%u, %u), index2=(%u, %u), minValue=(%f, %f), at %s:%d\n", expected_index1, scalar_index1, expected_index2, scalar_index2, minValue_expected, minValue, __FILE__, __LINE__);}
	    assert(expected_index1 == scalar_index1);
	    assert(expected_index2 == scalar_index2);
	    assert(minValue == minValue_expected);
	  }
	}	
      }
      { //! Test-case: all min-valeus are updated:
	const uint column_pos = 1; const t_float test_minValue_base = -10; const t_float offset = -100.0; //! ie, 'icnremntally' smaller value.
	const uint row_startPos = 0; const uint row_endPosPlussOne = nrows; const uint column_endPosPlussOne = ncols;
	//! Set the 'previous' values:
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  const t_float test_minValue = test_minValue_base + (row_id*offset);
	  arrOf_previouslyUsedValues[row_id] = (t_float)column_pos; //! ie, the 'old' value.
	  distmatrix[row_id][column_pos] = test_minValue; //! ie, a for cosnsitency: the latter 'describe' the enw value to set.
	}
	//! The call:
	s_dense_closestPair_updateAt_column(&s_obj_closestPair, column_pos, row_startPos, row_endPosPlussOne, column_endPosPlussOne, arrOf_previouslyUsedValues, /*uint *arrOf_condensed_columnPos_toUse=*/NULL, NULL, 0);

	//! Valdite the results, ie, what we expect:
	{ //! Valdiate for teh new min-case:
	  const uint bucket_id = column_pos;
	  for(uint row_id = row_startPos; row_id < row_endPosPlussOne; row_id++) {
	    const t_float test_minValue = test_minValue_base + (row_id*offset);
	    //! ----
	    const t_float min_value_inner = s_obj_closestPair.matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	    if(false) {printf("[%u]\t cmp(expected, actual) = (%f, %f), at %s:%d\n", row_id, test_minValue, min_value_inner, __FILE__, __LINE__);}
	    assert(min_value_inner == test_minValue); //! ie, that the latter is corectly updated.
	    if( (row_id +1) < row_endPosPlussOne) {
	      const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	      const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	      if(false) {printf("[%u]\t cmp(expected, actual) = (%f, %f), at %s:%d\n", row_id, test_minValue, min_value, __FILE__, __LINE__);}
	      assert(min_value < test_minValue); //! ie, as we expect each row to 'have  alower value' than the preivosu, ie, to simplify testing
	      assert(indexAt_min_value != row_id);
	    } else { //! thenw e investigate the last row, which we from our test-setting expets to be set to the 'lowest' value.
	      const t_float min_value = s_obj_closestPair.mapOf_minTiles_value[bucket_id]; 
	      const uint indexAt_min_value = (uint)s_obj_closestPair.mapOf_minTiles_value_atRowIndex[bucket_id];
	      assert(min_value == test_minValue);
	      assert(indexAt_min_value == row_id);
	    }
	  }
	  { //! Validate "s_dense_closestPair_getClosestPair(..)"
	    uint expected_index1 = 0;  uint expected_index2 = 0;
	    const t_float minValue_expected = __local__find_minPair(nrows, ncols, distmatrix, &expected_index1, &expected_index2);
	    uint scalar_index1 = UINT_MAX; uint scalar_index2 = UINT_MAX;
	    const t_float minValue = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &scalar_index1, &scalar_index2, nrows, ncols);
	    if(true) {printf("\t result: index1=(%u, %u), index2=(%u, %u), minValue=(%f, %f), at %s:%d\n", expected_index1, scalar_index1, expected_index2, scalar_index2, minValue_expected, minValue, __FILE__, __LINE__);}
	    assert(expected_index1 == scalar_index1);
	    assert(expected_index2 == scalar_index2);
	    assert(minValue == minValue_expected);
	  }
	}	      
      }      
     
      //! De-allcoate locally rserved moery:
      free_1d_list_float(&arrOf_previouslyUsedValues);
    }


    //! De-allcoate:
    s_dense_closestPair_free(&s_obj_closestPair);
    free_2d_list_float(&distmatrix);
  }


  return 0;
}
