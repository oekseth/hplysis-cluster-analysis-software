#ifndef graphAlgorithms_aux_h
#define graphAlgorithms_aux_h


//#include "type_2d_float_nonCmp_uint.h"
#include "configure_cCluster.h"
/* #include "list_uint.h" */
#include "def_intri.h"
#include "log_clusterC.h"
#include "mask_api.h"

#include "maskAllocate.h"
#include "math_generateDistribution.h"
/**
   @namespace graphAlgorithms_aux
   @brief provide functions in support of memory allocation and random numbers.
   @remarks the "c clustering library" is used as template.
   @author Ole Kristian Ekseth (oekseth).
 **/


  //! Indetify the order of the old indices ifthe 'input' valeus/scroes are sorted.
  //! @remarks time-complexity estimated to eitehr constand or n*log(n), ie, depending on the list being sorted (or not).

  //void sort_indexes(const list_generic<type_float> &arrOf_old, list_uint &arrOf_result_sortedIndex);

/* namespace graphAlgorithms_aux { */

/* }; */

#endif
