//! --------

//sdfsdf

char stringOf_resultFile__metricScoresAsIs[1000] = {'\0'};
char stringOf_resultFile__metricScores_ranks[1000] = {'\0'};
char stringOf_resultFile__metricScores_ranks_adjusted_toGold[1000] = {'\0'};
char stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold[1000] = {'\0'};
char stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold__disjointMasks[1000] = {'\0'};
//! --------
sprintf(stringOf_resultFile__metricScoresAsIs, "%s.subCategory_%s", stringOf_resultPrefix__sub, "metricXdata");
sprintf(stringOf_resultFile__metricScores_ranks, "%s.subCategory_%s", stringOf_resultPrefix__sub, "metricXdata.ranks");
sprintf(stringOf_resultFile__metricScores_ranks_adjusted_toGold, "%s.subCategory_%s", stringOf_resultPrefix__sub, "metricXdata.ranks_adjustedWrtGoldStandard.tsv");
sprintf(stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold, "%s.subCategory_%s", stringOf_resultPrefix__sub, "dataXmetrics.ranks_adjustedWrtGoldStandard.tsv");
sprintf(stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold__disjointMasks, "%s.subCategory_%s", stringOf_resultPrefix__sub, "dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv");
const bool  __internalDataUpdate__transposeOrder__inputIsAlsoTransposed = false; //! ie, defualt asusmption
/* const char *stringOf_resultFile__metricScoresAsIs = "clusterCmpEvaluation.metricXdata.tsv"; */
/* const char *stringOf_resultFile__metricScores_ranks = "clusterCmpEvaluation.metricXdata.ranks.tsv"; */
/* const char *stringOf_resultFile__metricScores_ranks_adjusted_toGold = "clusterCmpEvaluation.metricXdata.ranks_adjustedWrtGoldStandard.tsv"; */
/* const char *stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold = "clusterCmpEvaluation.dataXmetrics.ranks_adjustedWrtGoldStandard.tsv"; */
/* const char *stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold__disjointMasks = "clusterCmpEvaluation.dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv"; */

//! ----------------------------------------------
//!
//! Idenitfy the number of metrics:    
uint cnt_metricsToEvaluate = 0; uint matrixDimensionsCnt__cols = 0;
uint matrixDimensionsCnt__subsetSetBased__nrows = 0; //! describe-subset: "the set-based cmp-metrics (eg, Rands Index and Adjsuted-rand-index (ARI))"
uint matrixDimensionsCnt__subsetSet__default_cntRows = 0; //! describe-subset: "the popular metics which are oftne used, ie, under the asusmptiaotn that the 'default equations are being applied non-modifed'.
//uint cnt_w_nonMatrix = 0; uint cnt_w_matrix = 0; uint cnt_w_matrixAndCorrMetric = 0;
{
  assert(cnt_data_nrows > 0);
  assert(cnt_data_ncols > 0);
  uint row_id_global = 0; 
#define __localConfig__isToApplyLogics 0 //! ie, as we are Not intereested in 'applying the logics', ie, weere we are  rather intersted in counting the number of rows and columns) (oekseth, 06. des. 2016).
  //  fprintf(stderr, "-----------------\n\n\nat %s:%d\n", __FILE__, __LINE__);
#include "kt_assessPredictions__stub__clusterMetricCmp__buildMatrix.c"
  assert(row_id_global > 0);
  assert(matrixDimensionsCnt__cols > 0);
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  cnt_metricsToEvaluate = row_id_global;
  //! Identify the number of corrleaiton-meitrcs: iterate through the different cluster-comparison-metrics: the corrleation-emtrics themself:
  /* uint cnt_corrMetrics = 0; */
  /* for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) { */
  /* 	e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index;  */
  /* 	if( */
  /* 	   (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) */
  /* 	   || describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id) */
  /* 	   || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id) */
  /* 	   ) {continue;} //! ie, as we then assuem 'this is Not of interest'. */
  /* 	cnt_corrMetrics++; */
  /* } */
  /* assert(cnt_corrMetrics <= (uint)e_kt_correlationFunction_undef); */
  /* for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) { */
  /* 	const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
  /* 	if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) { */
  /* 	  //! Then assume the metric is a matrix-base ematric (eg, Silhhoutte index): */
  /* 	  cnt_w_matrix += cnt_matricesToEvaluate; */
  /* 	  cnt_w_matrixAndCorrMetric += ((uint)e_kt_clusterComparison_undef * (uint)(e_kt_categoryOf_correaltionPreStep_none+1) * cnt_corrMetrics * cnt_matricesToEvaluate); //! ie, 'for each matrix-metric we inwestigate wrt. each inptu-matrix and each each correlation-metric. */
  /* 	} else { */
  /* 	  cnt_w_nonMatrix++; */
  /* 	} */
  /* } */
}
//! ----
assert(cnt_matricesToEvaluate > 0); assert(matrixDimensionsCnt__cols > 0);
//assert(cnt_w_matrix != 0); assert(cnt_w_matrixAndCorrMetric != 0); assert(cnt_w_nonMatrix != 0);
//const uint cnt_metricsToEvaluate = cnt_w_nonMatrix + cnt_w_matrix + cnt_w_matrixAndCorrMetric;
//! Allocate the matrix to hold the set of scores:
s_kt_matrix_t obj_metricDataScores; init__s_kt_matrix(&obj_metricDataScores, /*nrows=*/cnt_metricsToEvaluate, /*ncols=*/matrixDimensionsCnt__cols, /*isTo_allocateWeightColumns=*/false);
s_kt_matrix_t obj_metricDataScores__subsetSetBased; 
if(matrixDimensionsCnt__subsetSetBased__nrows > 0) {
  init__s_kt_matrix(&obj_metricDataScores__subsetSetBased, /*nrows=*/matrixDimensionsCnt__subsetSetBased__nrows, /*ncols=*/matrixDimensionsCnt__cols, /*isTo_allocateWeightColumns=*/false);
 } else {
  setTo_empty__s_kt_matrix_t(&obj_metricDataScores__subsetSetBased);
 }
//s_kt_matrix_t obj_metricDataScores; init__s_kt_matrix(&obj_metricDataScores, /*nrows=*/cnt_metricsToEvaluate, /*ncols=*/(cnt_data * cnt_data), /*isTo_allocateWeightColumns=*/false);
{ //! Add the name of the columns:
  uint local_col_id = 0;
  //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  assert(obj_metricDataScores.ncols > 0);
  for(uint data_1 = 0; data_1 < cnt_data_nrows; data_1++) {
    for(uint data_2 = 0; data_2 < cnt_data_ncols; data_2++) {
      char string_local[1000] = {'\0'}; sprintf(string_local, "%s.%u.%u", prefix__clusterSets, data_1, data_2);
      set_string__s_kt_matrix(&obj_metricDataScores, local_col_id, string_local, /*addFor_column=*/true);
      if(obj_metricDataScores__subsetSetBased.ncols > 0) {
	set_string__s_kt_matrix(&obj_metricDataScores__subsetSetBased, local_col_id, string_local, /*addFor_column=*/true);
      }
      local_col_id++;
      assert(local_col_id <= obj_metricDataScores.ncols);
    }
  }
}

//  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    
//! ----------------------------------------
uint row_id_global = 0;
uint row_id_subsetSetBased__cnt = 0; //! describe-subset: "the set-based cmp-metrics (eg, Rands Index and Adjsuted-rand-index (ARI))"
uint row_id_subsetSet__default_cntRows = 0; //! describe-subset: "the popular metics which are oftne used, ie, under the asusmptiaotn that the 'default equations are being applied non-modifed'.
/* if(true) { */
/*   fprintf(stderr, "FIXME: include [”elow], at %s:%d\n", __FILE__, __LINE__); */
/*   for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) {	 */
/* 	const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
/* 	if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp) ) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index) */
/* 	uint row_id = cmpMetric_id; */
/* 	//! */
/* 	{ //! Add the name of this row: */
/* 	  char string_local[1000] = {'\0'}; sprintf(string_local, "%s", getStringOf__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)); */
/* 	  assert(row_id < obj_metricDataScores.nrows); */
/* 	  set_string__s_kt_matrix(&obj_metricDataScores, row_id, string_local, /\*addFor_column=*\/false); */
/* 	} */
/* 	uint local_col_id = 0; */
/* 	//! */
/* 	//! Iterate through the data: */
/* 	for(uint data_1 = 0; data_1 < cnt_data; data_1++) { */
/* 	  //! */
/* 	  //! Iterate, comparing 'this' to the other/laterntive data-sets: */
/* 	  for(uint data_2 = 0; data_2 < cnt_data; data_2++) { */
/* 	  } */
/* 	} */
/*   }       */
/* } else { */
#define __localConfig__isToApplyLogics 1 //! ie, as we are intereested in 'applying the logics' (rather than merely coutnign the number of rows and columns) (oekseth, 06. des. 2016).
//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
#include "kt_assessPredictions__stub__clusterMetricCmp__buildMatrix.c"
//  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
//    }
    

//! ------------------------------------------
//! 
//! Construct a 'ranked version' of [ªbove]:
s_kt_matrix_t obj_cpy_ranksEachRow; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);
//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
const bool isOk_rankCpy = init__copy__useRanksEachRow__s_kt_matrix(&obj_cpy_ranksEachRow, &obj_metricDataScores, /*isTo_updateNames=*/true);
assert(isOk_rankCpy);     assert(obj_metricDataScores.nrows == obj_cpy_ranksEachRow.nrows);


//  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    
//! ------------------------------------------
//! 
//!
{ //! Export the results:
  //! 
  //! Export the scores as-is:
  printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile__metricScoresAsIs, __FILE__, __LINE__);
  export__singleCall__s_kt_matrix_t(&obj_metricDataScores, stringOf_resultFile__metricScoresAsIs, fileHandler__global);
  //!
  //! Convert to 'ranks and thereafter export the scores:
  printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile__metricScores_ranks, __FILE__, __LINE__);
  //! Export the matrix:
  export__singleCall__s_kt_matrix_t(&obj_cpy_ranksEachRow, stringOf_resultFile__metricScores_ranks, fileHandler__global);
  { //! Build subset-matrices for: 
    const char *base_string = stringOf_resultFile__metricScores_ranks;
#define __localInputMatrix obj_cpy_ranksEachRow
#define __clusterConfig__k_cluster cnt_data_nrows // TODO: consider setting a different vlaue.
#include "kt_assessPredictions__stub__clusterMetricCmp__extractSubset.c"
#undef __clusterConfig__k_cluster
  }
  /*   export_config__s_kt_matrix_t(&obj_cpy_ranksEachRow, stringOf_resultFile__metricScores_ranks); */
  /*   export__s_kt_matrix_t(&obj_cpy_ranksEachRow); */
  /*   closeFileHandler__s_kt_matrix_t(&obj_cpy_ranksEachRow);			 */
  /* } */
  if(obj_metricDataScores__subsetSetBased.nrows && obj_metricDataScores__subsetSetBased.ncols)  { 
    //! Conver the subset (of metircs) to a rank-based file:
    //!
    //! Build the ranks:
    s_kt_matrix_t obj_cpy_ranksEachRow; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);
    const bool isOk_rankCpy = init__copy__useRanksEachRow__s_kt_matrix(&obj_cpy_ranksEachRow, &obj_metricDataScores__subsetSetBased, /*isTo_updateNames=*/true);
    assert(isOk_rankCpy);     assert(obj_metricDataScores__subsetSetBased.nrows == obj_cpy_ranksEachRow.nrows);
    //! Export:
    char string_local[2000]; sprintf(string_local, "%s.subset_setBased.tsv", stringOf_resultFile__metricScores_ranks);
    printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile__metricScores_ranks, __FILE__, __LINE__);
    //! First convifugre the result-process:
    export__singleCall__s_kt_matrix_t(&obj_cpy_ranksEachRow, string_local, fileHandler__global);
    /* export_config__s_kt_matrix_t(&obj_cpy_ranksEachRow, string_local); */
    /* export__s_kt_matrix_t(&obj_cpy_ranksEachRow); */
    /* closeFileHandler__s_kt_matrix_t(&obj_cpy_ranksEachRow); */
    free__s_kt_matrix(&obj_cpy_ranksEachRow);
  }
}
{ //! Assess the accuracy of [ªbove] rpediction-metic-results: compare the results to a 'manually curated gold-stadnard' and idneityf the differentce--smlairty-score.
  //! Note: in this 'procedure' we iterate through the results, and for each row copmtue the 'simliarty' to the 'manually curated gold-standard'. Thereafter we first idneitfy the 'ranks' (of each metric), before we 'print the results' (ie, a simple for-loop).
  assert(cnt_metricsToEvaluate == obj_cpy_ranksEachRow.nrows);
  const t_float default_value_0 = 0;
  t_float *scoreOf_internalMetrics = allocate_1d_list_float(cnt_metricsToEvaluate, default_value_0);
  for(uint internalMetric_id = 0; internalMetric_id < cnt_metricsToEvaluate; internalMetric_id++) {
    t_float *row_gold = arrOf_1d_dataXdata_goldStandard;
    t_float *row_cmp = obj_metricDataScores.matrix[internalMetric_id];
    //! 
    //! Configure and comptue the score:
    const e_kt_correlationFunction metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance;
    const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_none;
    const t_float score = kt_compare__each__maskImplicit__useRowPointersAsInput(metric_id, typeOf_correlationPreStep, /*ncols=*/obj_metricDataScores.ncols, row_gold, row_cmp);
    //printf("score[%u] = %f, at %s:%d\n", internalMetric_id, score, __FILE__, __LINE__);
    assert(score != T_FLOAT_MAX);
    //! Update:
    scoreOf_internalMetrics[internalMetric_id] = score;
  }

  //! Compute the rank:
  t_float *list_rank = get_rank__correlation_rank(cnt_metricsToEvaluate, scoreOf_internalMetrics); 
  assert(list_rank);      
      
  //! Print the results (ie, a simple for-loop).
  printf("\n--------\n# The ranks of each metric are (at %s:%d\n) are:\n", __FILE__, __LINE__);
  uint max_rank = 0;
  if(obj_result) { //! then we update the "s_kt_assessPredictions_differentMetricSets" object:
    if(obj_result->global_cntMeasurements_currentPos == 0) {
      //! Then we invesgitae if we are to allocate:
      if(obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].nrows == 0) {
	assert(obj_result->global_cntMeasurements_size > 0);
	assert(cnt_metricsToEvaluate > 0);
	printf("------------- intiates[%u], at %s:%d\n", enum_clusterResultAsInput,  __FILE__, __LINE__);
	init__s_kt_matrix(&(obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput]), /*nrows=*/obj_result->global_cntMeasurements_size, /*ncols=*/cnt_metricsToEvaluate, /*isTo_allocateWeightColumns=*/false);
      }
    }
    //!
    //! What we expect:
    assert(obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].ncols == cnt_metricsToEvaluate);
    //! ----------------------------------
    //! start: valdiate-range -----------
    if(!(obj_result->global_cntMeasurements_currentPos < obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].nrows)) {
      fprintf(stderr, "!!\t (enum=%u)\t Seems like we are inserting more rows than you expected, ie, (%u VS %u): could be be due an 'implict' use of an \"e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef\" 'assiated' iteration, ie, please investigate. In this cotnext if the latter error-message makes you confused, then please contact the seionor devleoperm at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", enum_clusterResultAsInput, obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].nrows, obj_result->global_cntMeasurements_size, __FUNCTION__, __FILE__, __LINE__);
    }
    assert(obj_result->global_cntMeasurements_currentPos < obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].nrows);
    //assert( (obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].nrows + 1) < obj_result->global_cntMeasurements_currentPos);
    //! finish: valdiate-range -----------
    //!
    //! Update the ranks:
    for(uint internalMetric_id = 0; internalMetric_id < cnt_metricsToEvaluate; internalMetric_id++) {
      const t_float rank = list_rank[internalMetric_id];
      obj_result->m_ranks_inCatFor__metrics[enum_clusterResultAsInput].matrix[obj_result->global_cntMeasurements_currentPos][internalMetric_id] = rank;
    }
  }

  for(uint internalMetric_id = 0; internalMetric_id < cnt_metricsToEvaluate; internalMetric_id++) {
    max_rank = macro_max((uint)list_rank[internalMetric_id], max_rank);
  }
  for(uint internalMetric_id = 0; internalMetric_id < cnt_metricsToEvaluate; internalMetric_id++) {
    char *stringOf_entry = NULL; get_string__s_kt_matrix(&obj_metricDataScores, /*row=*/internalMetric_id, /*for-columns=*/false, &stringOf_entry);
    printf("%s\t rank=\t%u/%u\tscore=\t%f\n", stringOf_entry, (uint)list_rank[internalMetric_id], max_rank, scoreOf_internalMetrics[internalMetric_id]);
  }      
  if(max_rank != 0) 
    { //! Adjust the internal ranks to the 'ranks resemablance with the gold-standard': in order to increase the visual result-perception of the 'descirptive pwoer of the ranks' we adjust each values (assicated to the data-set-scores) using the 'closeness' to the ideal-sollution.
      //! Note: in [below] we cosntruct a new object where 'we for each row adjsut the column-ranks wrt. the gold-standard-closeness', and thereafter 'write/export otu the result'.
      s_kt_matrix_t obj_local_ranksAdjusted; setTo_empty__s_kt_matrix_t(&obj_local_ranksAdjusted); init__copy__s_kt_matrix(&obj_local_ranksAdjusted, &obj_cpy_ranksEachRow, /*isTo_updateNames=*/true); //! ie, copy.
      assert(obj_local_ranksAdjusted.nrows == cnt_metricsToEvaluate);
      for(uint internalMetric_id = 0; internalMetric_id < cnt_metricsToEvaluate; internalMetric_id++) {
	const t_float rankAdjustment = list_rank[internalMetric_id] / (t_float)max_rank;
	for(uint col_id = 0; col_id < obj_local_ranksAdjusted.ncols; col_id++) {
	  const t_float score = obj_local_ranksAdjusted.matrix[internalMetric_id][col_id];
	  //if(score != 0) {
	  obj_local_ranksAdjusted.matrix[internalMetric_id][col_id] = score*rankAdjustment;
	  //	  }
	}
      }
      { //! Export the scores as-is:
	printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile__metricScores_ranks_adjusted_toGold, __FILE__, __LINE__);
	//! First convifugre the result-process:
	export__singleCall__s_kt_matrix_t(&obj_local_ranksAdjusted, stringOf_resultFile__metricScores_ranks_adjusted_toGold, fileHandler__global);
	{ //! Build subset-matrices for: 
	  const char *base_string = stringOf_resultFile__metricScores_ranks_adjusted_toGold;
#define __localInputMatrix obj_local_ranksAdjusted
#define __clusterConfig__k_cluster cnt_data_nrows // TODO: consider setting a different vlaue.
#include "kt_assessPredictions__stub__clusterMetricCmp__extractSubset.c"
#undef __clusterConfig__k_cluster
	}
	/* export__s_kt_matrix_t(&obj_local_ranksAdjusted); */
	/* closeFileHandler__s_kt_matrix_t(&obj_local_ranksAdjusted);	 */
      }
      if(obj_local_ranksAdjusted.nrows && obj_local_ranksAdjusted.ncols) { 
	//! Identify data-set-combinatiosn which are regarded as simlair by our evaluated data-set-metics.       
	s_kt_matrix_t obj_local_dataSet; setTo_empty__s_kt_matrix_t(&obj_local_dataSet); init__copy_transposed__thereafterRank__s_kt_matrix(&obj_local_dataSet, &obj_local_ranksAdjusted, /*isTo_updateNames=*/true); //! ie, copy.
	//! Validat the 'transpsoed proeprty':
	assert(obj_local_dataSet.ncols == cnt_metricsToEvaluate);  assert(obj_local_dataSet.nrows == obj_local_ranksAdjusted.ncols); 
	//! ---------------------------------
	//!	  	  
	{ //! Write out the data-set:	  
	  printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold, __FILE__, __LINE__);
	  //! First convifugre the result-process:
	  export__singleCall__s_kt_matrix_t(&obj_local_dataSet, stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold, fileHandler__global);
	  { //! Build subset-matrices for: 
	    assert( (cnt_data_nrows*cnt_data_ncols) == obj_local_dataSet.nrows); //! ie, what we expect.
	    const char *base_string = stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold;
#define __internalDataUpdate__transposeOrder 1	    
	    const bool  __internalDataUpdate__transposeOrder__inputIsAlsoTransposed = true;
#define __localInputMatrix obj_local_dataSet
#define __clusterConfig__k_cluster cnt_data_nrows // TODO: consider setting a different vlaue.
#include "kt_assessPredictions__stub__clusterMetricCmp__extractSubset.c"
#undef __internalDataUpdate__transposeOrder
#undef __clusterConfig__k_cluster
	  }
	  /* export__s_kt_matrix_t(&obj_local_dataSet); */
	  /* closeFileHandler__s_kt_matrix_t(&obj_local_dataSet);	 */
	}

	
	//! ---------------------------------
	//!
	//! Apply the disjont-filter-clustering-procedure:
	//! ----
	//!
	// dummy_compiler_test();
	
	//s_kt_api_config_t ktApi__afterCorrelation;
	//assert(false); // FIXME: inlcu the followiung //
		s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, fileHandler__global); 
	ktApi__afterCorrelation.stringOf_exportResult__heatMapOf_clusters = stringOf_resultFile__metricScores_dataSets_ranksRanks_adjusted_toGold__disjointMasks;
	//! Make the call:
	const t_float min_rank = (config_min_rankThreshold != T_FLOAT_MAX) ? obj_local_dataSet.nrows * config_min_rankThreshold : T_FLOAT_MIN_ABS;
	const t_float max_rank = T_FLOAT_MAX;
	assert(obj_local_dataSet.nrows > 0);
	assert(obj_local_dataSet.ncols > 0);
	s_kt_clusterAlg_fixed_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
	const bool is_ok = kt_matrix__disjointClusterSeperation(&obj_local_dataSet, &obj_result, min_rank, max_rank, /*useRelativeScore_betweenMinMax=*/false, /*isTo_adjustToRanks_beforeTrehsholds_forRows=*/true, NULL, ktApi__afterCorrelation);
	assert(is_ok);


	//!
	//! De-allocate:
	free__s_kt_matrix(&obj_local_dataSet);
	free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result); //! ie, as we expect that the 'result of this operaiton' was wrtiten to "ktApi__afterCorrelation.stringOf_exportResult__heatMapOf_clusters"
      }
      //! De-allcoate:
      free__s_kt_matrix(&obj_local_ranksAdjusted);
    }
  //!
  //! De-allocate:
  free_1d_list_float(&scoreOf_internalMetrics); scoreOf_internalMetrics = NULL;
  free_1d_list_float(&list_rank); list_rank = NULL; //! ie, de-allcoate.
}


//! -----------------------------
//!
//! De-allocate:
free__s_kt_matrix(&obj_metricDataScores);    
free__s_kt_matrix(&obj_cpy_ranksEachRow);
free__s_kt_matrix(&obj_metricDataScores__subsetSetBased);
#undef arrOf_2d_data__set1
#undef arrOf_2d_data__set2
#undef arrOf_1d_dataXdata_goldStandard
#undef distMatrix_0
#undef distMatrix_1
#undef distMatrix_2

