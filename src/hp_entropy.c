#include "hp_entropy.h"

void compute_entropy__list__hp_entropy(const s_kt_list_1d_uint_t *list_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, t_float *scalar_result) {
  //! Intailize.
  if( (max_theoreticCountSize == 0) || (max_theoreticCountSize == UINT_MAX) ) {max_theoreticCountSize = 0; for(uint i = 0; i < list_count->list_size; i++) {
      if(list_count->list[i] != UINT_MAX) {
	max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count->list[i]);}
    }
  } //! ie, then idneitfy the max-size.
  assert(max_theoreticCountSize != UINT_MAX);
  assert(max_theoreticCountSize < UINT_MAX);
  if((int)max_theoreticCountSize <= 0) {
    *scalar_result = T_FLOAT_MAX; //! ie, as we then assume that there is nothing to compute (eg, as all values ahve a '0').
    return;
  }
  // printf("(hp-entropy)\t max-size:%d\n", (int)max_theoreticCountSize);
  if(max_theoreticCountSize > 0) {
    s_kt_entropy_t obj_entropy = initSimple__s_kt_entropy(max_theoreticCountSize, /*vector_length=*/list_count->list_size, enum_entropy);
    //! Apply lgoicis:
    kt_entropy__generic__vector(&obj_entropy, list_count, enum_entropy, isTo_applyPostScaling, /*lambda_default=*/0, scalar_result);
    //! De-allcote: 
    free__s_kt_entropy(&obj_entropy);
  }
}

void compute_entropy__matrix__hp_entropy(const s_kt_matrix_base_uint_t *mat_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_list_1d_float_t *list_result) {
  //! Intailize.
  if( (max_theoreticCountSize == 0) || (max_theoreticCountSize == UINT_MAX) ) {max_theoreticCountSize = 0; for(uint i = 0; i < mat_count->nrows; i++) {for(uint k = 0; k < mat_count->ncols; k++) {assert(isOf_interest(mat_count->matrix[i][k])); max_theoreticCountSize = macro_max(max_theoreticCountSize, mat_count->matrix[i][k]);}}} //! ie, then idneitfy the max-size.
  s_kt_entropy_t obj_entropy = initSimple__s_kt_entropy(max_theoreticCountSize, /*vector_length=*/mat_count->ncols, enum_entropy);  
  //!
  //! Allocate result-matrix: 
  // *mat_result = initAndReturn__s_kt_matrix_base_t(mat1_freq->nrows, mat2_freq->nrows);
  *list_result = init__s_kt_list_1d_float_t(mat_count->nrows);
  // FIXME[tut+eval]: evlauat etyime-beneift of [below] aprpaoch .... 
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
#endif
  for(uint row_id = 0; row_id < mat_count->nrows; row_id++) {
    //! Apply lgoicis:
    t_float result = 0;
    s_kt_list_1d_uint_t obj_shallow = setToEmpty__s_kt_list_1d_uint_t();
    obj_shallow.list = mat_count->matrix[row_id];
    obj_shallow.list_size = mat_count->ncols;
    //! Compute:
    kt_entropy__generic__vector(&obj_entropy, &obj_shallow, enum_entropy, isTo_applyPostScaling, /*lambda_default=*/0, &result);
    list_result->list[row_id] = result;
  }
  //! Apply lgoicis:
  //  kt_entropy__generic__vector(&obj_entropy, list_count, enum_entropy, /*lambda_default=*/0, scalar_result);
  //! De-allcote: 
  free__s_kt_entropy(&obj_entropy);
}

void compute_freq__list__hp_entropy(const s_kt_list_1d_uint_t *list_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_list_1d_float_t *list_result) {
    //! Intailize.
  if( (max_theoreticCountSize == 0) || (max_theoreticCountSize == UINT_MAX) ) {max_theoreticCountSize = 0; for(uint i = 0; i < list_count->list_size; i++) {max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count->list[i]);}} //! ie, then idneitfy the max-size.
  s_kt_entropy_t obj_entropy = initSimple__s_kt_entropy(max_theoreticCountSize, /*vector_length=*/list_count->list_size, enum_entropy);
  //! Apply lgoicis:
  kt_entropy__generic__vector__frequency(&obj_entropy, list_count, enum_entropy, isTo_applyPostScaling, /*lambda_default=*/0, list_result);
  //! De-allcote: 
  free__s_kt_entropy(&obj_entropy);
}

void compute_freq__matrix__hp_entropy(const s_kt_matrix_base_uint_t *mat_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_matrix_base_t *mat_result) {
    //! Intailize.
  if( (max_theoreticCountSize == 0) || (max_theoreticCountSize == UINT_MAX) ) {max_theoreticCountSize = 0; for(uint i = 0; i < mat_count->nrows; i++) {for(uint k = 0; k < mat_count->ncols; k++) {assert(isOf_interest(mat_count->matrix[i][k])); max_theoreticCountSize = macro_max(max_theoreticCountSize, mat_count->matrix[i][k]);}}} //! ie, then idneitfy the max-size.
  s_kt_entropy_t obj_entropy = initSimple__s_kt_entropy(max_theoreticCountSize, /*vector_length=*/mat_count->ncols, enum_entropy);  
  //!
  //! Allocate result-matrix: 
  *mat_result = initAndReturn__s_kt_matrix_base_t(mat_count->nrows, mat_count->nrows);
  for(uint row_id = 0; row_id < mat_count->nrows; row_id++) {
    //! Apply lgoicis:
    t_float result = 0;
    s_kt_list_1d_uint_t obj_shallow = setToEmpty__s_kt_list_1d_uint_t();
    obj_shallow.list = mat_count->matrix[row_id];
    obj_shallow.list_size = mat_count->ncols;
    //! Compute:
    s_kt_list_1d_float_t obj_dummy = setToEmpty__s_kt_list_1d_float_t();
    obj_dummy.list_size = mat_count->ncols;
    obj_dummy.list = mat_result->matrix[row_id];
    //! The call:
    kt_entropy__generic__vector__frequency(&obj_entropy, &obj_shallow, enum_entropy, isTo_applyPostScaling, /*lambda_default=*/0, &obj_dummy);
    //list_result->list[row_id] = result;
  }
  //! Apply lgoicis:
  //  kt_entropy__generic__vector(&obj_entropy, list_count, enum_entropy, /*lambda_default=*/0, scalar_result);
  //! De-allcote: 
  free__s_kt_entropy(&obj_entropy);
}

s_hp_entropy_fileReadConf_t init__s_kt_list_fileReadConf_t(const char *input_file, const char *result_file, const char config_col_sep) {
  //! --------
  s_hp_entropy_fileReadConf_t self;
  //! --------
  self.input_file = input_file;
  self.result_file = result_file;
  //! --------
  self.config_file_sep = config_col_sep;
  self.config_file_sep_newLine = '\n';
  //! --------
  self.config_rows__allHaveSameLength = true;
  self.config_rows__ignoreFirstRow = false;
  self.config_rows__ingoreFirstColumn = false; //! which si to be set ot true if the first column in each row describes a stirng-idntifer.   
  //! --------
  //!
  self.isTo_exportForEach = true;
  self.result_summary_sum = 0;
  self.result_summary_sum_cntRows = 0;
  //! --------
  //!
  //!
  return self;
}

void fromFile__compute_entropy__matrix__hp_entropy( s_hp_entropy_fileReadConf_t *file_config, const uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_matrix_base_t *mat_result) {
  //!
  //! Logics for each parsed row:
#define __MiF__parseRow() ({ \
      t_float result = 0; obj_current_row.list_size = cnt_tabs; kt_entropy__generic__vector(&obj_entropy, &obj_current_row, enum_entropy, isTo_applyPostScaling, /*lambda_default=*/0, &result); \
    /*! Write out: */ \
      /*printf("\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);*/ \
      if(file_result) {if(file_config->isTo_exportForEach || (file_result != stdout) ) {fprintf(file_result, "%f\n", result);} if(isOf_interest(result)) {file_config->result_summary_sum += (t_float)result; file_config->result_summary_sum_cntRows++;}} if(mat_result && (cnt_rows < mat_result->ncols) ) {mat_result->matrix[0][cnt_rows] = result;} })
  //!
  //! Apply logics:
#include "hp_entropy__parseFile.c"
}

void fromFile__compute_freq__matrix__hp_entropy(s_hp_entropy_fileReadConf_t *file_config, const uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_matrix_base_t *mat_result) {
  //!
  //! Logics for each parsed row:
#define __MiF__parseRow() ({ \
    s_kt_list_1d_float_t list_result = setToEmpty__s_kt_list_1d_float_t(); \
    t_float result = 0; kt_entropy__generic__vector__frequency(&obj_entropy, &obj_current_row, enum_entropy, isTo_applyPostScaling, /*lambda_default=*/0, &list_result); \
    /*! Write out: */ \
    if(file_result) {for(uint i = 0; i < list_result.list_size; i++) {fprintf(file_result, "%f%c", list_result.list[i], file_config->config_file_sep);} fputc(file_config->config_file_sep_newLine, file_result); free__s_kt_list_1d_float_t(&list_result);} \
    if(mat_result && (cnt_rows < mat_result->ncols) ) {for(uint i = 0; i < list_result.list_size; i++) {mat_result->matrix[cnt_rows][i] = list_result.list[i];} } })
  //! 
  //! Apply logics:
#include "hp_entropy__parseFile.c"
}

