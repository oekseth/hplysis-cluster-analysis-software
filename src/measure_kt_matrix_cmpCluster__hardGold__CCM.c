{ //!
  //! Intiate the 'data to remember' 
  printf("cnt_clusters_total=%u, cnt_cases=%u, at %s:%d\n", cnt_clusters_total, cnt_cases, __FILE__, __LINE__);
  uint cntClusters_curr__in
    = (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
  if(cntClusters_curr__in == 0) { cntClusters_curr__in = 2;}
  //assert(cntClusters_curr__in > 0);
  //! Intiate result-object:
  const uint empty_0 = 0;
  const uint obj_resultCollection_in_size = cnt_cases*cnt_cases;
  uint *obj_resultCollection_in__currentPos = allocate_1d_list_uint(obj_resultCollection_in_size, empty_0);
  s_kt_matrix_setOf_t obj_resultCollection_in = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_resultCollection_in_size);
  //!
  //! Iterate through the different data-sets:
  for(uint case_id_in = 0; case_id_in < cnt_cases; case_id_in++) {
    //!
    //! Budil an 'itnla' gold-data-set:
    s_hp_clusterShapes_t obj_gold_in = __Mi__init__in();
    //! Apply rotuine:
    const uint cnt_clusterStartOffset__in = cntClusters_curr__in;
    uint cnt_clusters_local_in = cntClusters_curr__in++;
    if(cnt_clusters_local_in > nrows_total) {cnt_clusters_local_in = nrows_total;}
    // printf("#\t cnt_clusters_local=%u, at %s:%d\n", cnt_clusters_local, __FILE__, __LINE__);
    assert(cnt_clusters_local_in <= nrows_total);
    assert(cnt_clusters_local_in >= 1);
    const bool is_ok = __Mi__buildSample__in(obj_gold_in, cnt_clusters_local_in, cnt_clusterStartOffset__in);
    assert(is_ok);
    //!
    //! Intiate the 'data to remember' 
    /* s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput); */
    /* init__s_kt_matrix(&obj_matrixInput, cnt_cases, nrows_total, /\*isTo_allocateWeightColumns=*\/false); */
    uint cntClusters_curr__out 
      = (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
    if(cntClusters_curr__out == 0) {cntClusters_curr__out = 2;}
    assert(cntClusters_curr__out > 0);
    //! Intiate result-object:
    s_kt_matrix_setOf_t obj_resultCollection_out = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/cnt_cases);
    //assert(cnt_cases == obj_resultCollection_out.list_size);
    //!
    //! Iterate through the different data-sets:
    uint case_id_out = 0;
    for(; case_id_out < cnt_cases; case_id_out++) {
      s_hp_clusterShapes_t obj_gold_out = __Mi__init__out();
      //! Apply rotuine:
      const uint cnt_clusterStartOffset__out = cntClusters_curr__out;
      uint cnt_clusters_local_out = cntClusters_curr__out;
      if(isTo_incrementClusterCountInInner) {cntClusters_curr__out++;}
      if(cnt_clusters_local_out > nrows_total) {cnt_clusters_local_out = nrows_total;}
      // printf("#\t cnt_clusters_local=%u, at %s:%d\n", cnt_clusters_local, __FILE__, __LINE__);
      assert(cnt_clusters_local_out <= nrows_total);
      assert(cnt_clusters_local_out >= 1);
      assert(cnt_clusters_local_in <= nrows_total); //! assumption: there are more vertices than clsuters, ie, as otehrwise would be odd.
      assert(cnt_clusters_local_out <= nrows_total); //! assumption: there are more vertices than clsuters, ie, as otehrwise would be odd.
      const bool is_ok = __Mi__buildSample__out(obj_gold_out, cnt_clusters_local_out, cnt_clusterStartOffset__out);
      assert(is_ok);
      //!
      //! Update the result-matrix:
      assert(obj_gold_out.map_clusterMembers);
      //assert(obj_gold_out.map_clusterMembers_size == obj_matrixInput.ncols);
      //assert(case_id_out < obj_matrixInput.ncols);
      //!
      //! Compute the clsuter--simalrity for all of hpLyssis CCMs:
      //! Object: Innermost:
      //assert(case_id_out < obj_resultCollection_out[0].nrows);
      computeForAllCmpMetrics__kt_matrix_cmpCluster(&obj_resultCollection_out, case_id_out, obj_gold_in.map_clusterMembers, obj_gold_out.map_clusterMembers, nrows_total, nrows_total, obj_gold_in.matrix.matrix, obj_gold_out.matrix.matrix);
      
      
      if(resultDir_path__inner && strlen(resultDir_path__inner)) { //! then we expor the matrices used as input.
	/* s_kt_matrix_t obj_matrixInput; init__s_kt_matrix(&obj_matrixInput, nrows_total, nrows_total, false); */
	/* for(uint row_id = 0; row_id < nrows_total; row_id++) { */
	/*   const uint cluster_id = obj_gold_out.map_clusterMembers[row_id]; */
	/*   assert(cluster_id != UINT_MAX); //! ie, as we expec thte latter to have been set. */
	/*   obj_matrixInput.matrix[case_id_out][row_id] = cluster_id; */
	/* } */
	// tagLocalIndex_0.tagNrows_10.tagNclusters_3.tagExperiment_all.tagSample_clusterCmpEvaluation_case2.localMetricData_.subCategory_dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv
	//if(case_id_out == 0) 
	{
	  //! Then write out the matrix:
	  char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__case_%u.%s.matrix.tsv", resultDir_path__inner, dataBlock_currentPos, case_id_out, case_id_in, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads
	  const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&(obj_gold_out.matrix), path_w_dir, NULL);
	  assert(is_ok_ex);
	}
	//free__s_kt_matrix(&obj_matrixInput);
      }
      //!
      //! De-allocate:
      free__s_hp_clusterShapes_t(&obj_gold_out);
    }
    {
      if(resultDir_path__inner && strlen(resultDir_path__inner)) {
	{ //! Export the input-matrix:
	  /* s_kt_matrix_t obj_matrixInput; init__s_kt_matrix(&obj_matrixInput, nrows_total, nrows_total, false); */
	  /* for(uint row_id = 0; row_id < nrows_total; row_id++) { */
	  /*   const uint cluster_id = obj_gold_in.map_clusterMembers[row_id]; */
	  /*   assert(cluster_id != UINT_MAX); //! ie, as we expec thte latter to have been set. */
	  /*   obj_matrixInput.matrix[case_id_out][row_id] = cluster_id; */
	  /* } */
	  // tagLocalIndex_0.tagNrows_10.tagNclusters_3.tagExperiment_all.tagSample_clusterCmpEvaluation_case2.localMetricData_.subCategory_dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv
	  //if(case_id_out == 0) 
	  {
	    //! Then write out the matrix:
	    char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__case_%u.%s.matrix.tsv", resultDir_path__inner, dataBlock_currentPos, case_id_out, case_id_in, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads
	    case_id_out++; //! ie, to 'nesure' unqiuness wrt. [below]
	    const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&(obj_gold_in.matrix), path_w_dir, NULL);
	    assert(is_ok_ex);
	  }	  
	  //free__s_kt_matrix(&obj_matrixInput);
	}
	//!
	//! Write out: 
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__case_%u.%s", resultDir_path__inner, dataBlock_currentPos, case_id_out, case_id_in, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads    
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_out, path_w_dir, &obj_resultCollection_in, obj_resultCollection_in__currentPos, typeOf_extractToPrint);      
      } else {
	//!
	//! Write out: 
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__case_%u.%s", resultDir_path, dataBlock_currentPos, cnt_cases, case_id_in, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads    
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_out, path_w_dir, &obj_resultCollection_in, obj_resultCollection_in__currentPos, typeOf_extractToPrint);      
      }
      //!
      //! Update the global counter:
      dataBlock_currentPos++;
    }
  }
  { //!
    //! Write out: 
    char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__2xGoldCases.%s", resultDir_path, dataBlock_currentPos, cnt_cases, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads    
    //! Note: in [”elow] we obht exprots and de-allocates:
    exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_in, path_w_dir, &obj_resultCollection_goldCaseClusters, obj_resultCollection_goldCaseClusters__currentPos, typeOf_extractToPrint);
    free_1d_list_uint(&obj_resultCollection_in__currentPos);  obj_resultCollection_in__currentPos = NULL;
  }
}
