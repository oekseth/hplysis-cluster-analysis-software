# -----------------------------------

#! Note: idea: comapre differnt clsuter-algorithms for sytneitc extrem data-sets, and use latter to assert the impact of idffernet configuraiton-aprpaoches for kd-tree .... fosuing on applicaiont of different simalirty-metrics .... use latter to assert that the "DB-scan" amanges to correctly assign clsuter-memberships for a number of cluster-cases ... 

from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq
from sklearn.metrics import euclidean_distances

#! Srs: "http://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html#sphx-glr-auto-examples-cluster-plot-cluster-comparison-py"
print(__doc__)

import time

import numpy as np
import matplotlib.pyplot as plt

from sklearn import cluster, datasets
from sklearn.datasets import fetch_kddcup99, fetch_covtype, fetch_mldata
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler

np.random.seed(0)

# Generate datasets. We choose the size big enough to see the scalability
# of the algorithms, but not too big to avoid too long running times
n_samples = 3*1500
noisy_circles = datasets.make_circles(n_samples=n_samples, factor=.5,
                                      noise=.05)
noisy_moons = datasets.make_moons(n_samples=n_samples, noise=.05)
blobs = datasets.make_blobs(n_samples=n_samples, random_state=8)
no_structure = np.random.rand(n_samples, 2), None
#iris = datasets.load_iris()

colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)

clustering_names = [
    'MiniBatchKMeans', 'AffinityPropagation', 
    'MeanShift',
    'SpectralClustering', 'Ward', 'AgglomerativeClustering',
    'DBSCAN', 
    'DBSCAN-brute', 
    'DBSCAN-canberra', 
    'DBSCAN-canberra-brute', 
    'Birch']

plt.figure(figsize=(len(clustering_names) * 2 + 3, 9.5))
plt.subplots_adjust(left=.02, right=.98, bottom=.001, top=.96, wspace=.05,
                    hspace=.01)

plot_num = 1
# class C(object):
#     def __getitem__(self, k):
#         return k
# __getitem__
hardCoded_centers = ([
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40],
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40], 
        [1, 1, 10, 10, 2, 10, 40]
        ])

#a = np.array([1, 2, 3])  # Create a rank 1 array

#A = np.random.rand(nrows, ncols)
datasets = [noisy_circles
            #iris
            #, hardCoded_centers
            , noisy_moons, blobs, no_structure
            ]
datasets_local = [""#'http', 'smtp', 'SA', 'SF', 'shuttle', 
                  #'forestcover'
                  ]
for dat in datasets_local:
    print('loading data')
    X = None
    if dat in ['http', 'smtp', 'SA', 'SF']:
        dataset = fetch_kddcup99(subset=dat, shuffle=True, percent10=True)
        X = dataset.data
        y = dataset.target

    if dat == 'shuttle':
        dataset = fetch_mldata('shuttle')
        X = dataset.data
        y = dataset.target
        X, y = sh(X, y)
        # we remove data with label 4
        # normal data are then those of class 1
        s = (y != 4)
        X = X[s, :]
        y = y[s]
        y = (y != 1).astype(int)

    if dat == 'forestcover':
        dataset = fetch_covtype(shuffle=True)
        X = dataset.data
        y = dataset.target
        # normal data are those with attribute 2
        # abnormal those with attribute 4
        s = (y == 2) + (y == 4)
        X = X[s, :]
        y = y[s]
        y = (y != 2).astype(int)
    if(X != None):
        print X
        #datasets.append(X)
        #print('vectorizing data')



cnt_inverseCase = 0;
cnt_data = len(datasets)
for data_id in range(cnt_data):
    for k in xrange(cnt_inverseCase):
        matrix = []
        data = datasets[data_id]    
        #! step(1): copy the matrix:
        for l in range(len(data)):
            #print "row", data[l]
            if(data[l] != None):
                row = []
                for i in range(len(data[l])):
                    row.append(data[l][i])
                matrix.append(row)
        #! Step(2): swap the order:
        cnt_rows = len(data)/2 - 1
        for l in xrange(cnt_rows):
            old_row = matrix[l]
            matrix[l] = matrix[cnt_rows+l]
            matrix[cnt_rows + l] = old_row

        #! Append:
        datasets.append(matrix)

for i_dataset, dataset in enumerate(datasets):
    #print "datasets " , dataset;
    print "data-type", type(dataset)
    X = dataset
    #if(type(X) == "tuple"):
    bandwidth = 1.31904031577
    if(type(X) is tuple):
        global X, bandwidth
        X, y = dataset
    # normalize dataset for easier parameter selection
        X = StandardScaler().fit_transform(X)
        bandwidth = cluster.estimate_bandwidth(X, quantile=0.3)
    # estimate bandwidth for mean shift
    
    print "bandwidth, ", bandwidth
    # connectivity matrix for structured Ward
    connectivity = kneighbors_graph(X, n_neighbors=10, include_self=False)
    # make connectivity symmetric
    connectivity = 0.5 * (connectivity + connectivity.T)

    # create clustering estimators
    ms = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
    two_means = cluster.MiniBatchKMeans(n_clusters=2)
    ward = cluster.AgglomerativeClustering(n_clusters=2, linkage='ward',
                                           connectivity=connectivity)
    spectral = cluster.SpectralClustering(n_clusters=2,
                                          eigen_solver='arpack',
                                          affinity="nearest_neighbors")
    dbscan = cluster.DBSCAN(eps=.2)
    #! Note: see "http://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html"
    dbscan_brute_1 = cluster.DBSCAN(eps=.2, metric='euclidean', algorithm='brute')
    dbscan_canberra_1 = cluster.DBSCAN(eps=.2, metric='canberra', algorithm='brute')
    dbscan_canberra_brute_1 = cluster.DBSCAN(eps=.2, metric='canberra', algorithm='brute')
    #eps=0.5, min_samples=5, metric='euclidean', algorithm='auto', leaf_size=30, p=None, n_jobs=1
    affinity_propagation = cluster.AffinityPropagation(damping=.9,
                                                       preference=-200)

    average_linkage = cluster.AgglomerativeClustering(
        linkage="average", affinity="cityblock", n_clusters=2,
        connectivity=connectivity)

    birch = cluster.Birch(n_clusters=2)
    clustering_algorithms = [
        two_means, affinity_propagation, 
        ms, 
        spectral, ward, average_linkage,
        dbscan, 
        dbscan_brute_1, 
        dbscan_canberra_1,
        dbscan_canberra_brute_1,
        birch]

    for name, algorithm in zip(clustering_names, clustering_algorithms):
        # predict cluster memberships
        t0 = time.time()
        algorithm.fit(X)
        t1 = time.time()
        if hasattr(algorithm, 'labels_'):
            y_pred = algorithm.labels_.astype(np.int)
        else:
            y_pred = algorithm.predict(X)

        # plot
        plt.subplot(4, len(clustering_algorithms), plot_num)
        if i_dataset == 0:
            plt.title(name, size=18)
        # print "colors=", colors[y_pred]
        plt.scatter(X[:, 0], X[:, 1], color=colors[y_pred].tolist(), s=10)

        if hasattr(algorithm, 'cluster_centers_'):
            centers = algorithm.cluster_centers_
            center_colors = colors[:len(centers)]
            plt.scatter(centers[:, 0], centers[:, 1], s=100, c=center_colors)
        plt.xlim(-2, 2)
        plt.ylim(-2, 2)
        plt.xticks(())
        plt.yticks(())
        plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                 transform=plt.gca().transAxes, size=15,
                 horizontalalignment='right')
        plot_num += 1

plt.show()
