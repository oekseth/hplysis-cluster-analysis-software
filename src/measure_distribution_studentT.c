#include "measure_distribution_studentT.h"
#include "matrix_deviation.h"


#include "measure_base.h"

/* //! Ends the time measurement for the array: */
/* static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) { */
/*   assert(stringOf_measureText); */
/*   assert(strlen(stringOf_measureText)); */
/*   //! Get knowledge of the memory-intiaition/allocation pattern: */
/*   { */
/*     //! Provides an identficator for the string: */
/*     char *string = new char[1000]; assert(string); memset(string, '\0', 1000); */
/*     sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);   */
/*     //! Prints the data: */
/*     //! Complte the measurement: */
/*     const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds); */
/*     //time.print_formatted_result(string);  */
/*     delete [] string; string = NULL;  */
    
/*     return time_in_seconds; */
/*   }  */
/* } */




static void apply_tests() {
  const uint arrOf_cnt_buckets_size = 7; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40,
												  80, 160
												  //, 320, 740, 1000
  };
  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * kilo*1000;
    printf("----------\n\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    
    //! Tes tthe 'ideal' case:
    const char *stringOf_measureText = "Multiplication: ideal case";
    start_time_measurement();  //! ie, start measurement
    //! -------------
    t_float result = 0;
    for(uint i = 0; i < size_of_array; i++) {
      result = (i * i);
    }
    //! -------------        
    const float prev_time_inSeconds = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, FLT_MAX);
    printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);

    {
      //! --------------------------------
      //const t_float score_possible = 10; 	const uint ncols = 100;
      // const t_float score_possible = 0.010; 	const uint ncols = 100; //! a case which is considerably harder toc ompteu.
      //const t_float score_possible = 0.010; 	const uint ncols = 1000; //! when comarepd to { 0.010, n=100 } the tiem-cost chagnes from 200..400 to 2000..4000
      //const t_float score_possible = 0.10; 	const uint ncols = 1000; //! when comarepd to { 0.010, n=1000 } the tiem-cost is un-changed.
      //const t_float score_possible = 0.10; 	const uint ncols = 2500; //! when comarepd to { 0.10, n=1000 } differences increases to 10,800x
      //const t_float score_possible = 0.10; 	const uint ncols = 5000; //! when comarepd to { 0.10, n=1000 } differences increases to 20,800x; when compared to n=2500 we observe a 2x-formance-increase, ie, "when dobulign the size we aslo dobles the 
      // const t_float score_possible = 0.10; 	const uint ncols = 10000; //! when comarepd to { 0.10, n=5000 } ... changes into a difference of 40,000x ... and for n=15000 difference is 60,617x

      
      //! --------------------------------
      { const char *stringOf_measureText = "correlation: [0...1]: pearson";
	start_time_measurement();  //! ie, start measurement
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  t_float bothtails = 0;
	  t_float lefttails = 0; 
	  t_float righttails = 0;
	  //t_float score_possible = size_of_array/(1+i);
	  const t_float score_possible = 0.010; 	const uint ncols = 15000; 
	  //! Compute:
	  get_signficanceFromCorrelation__Pearson__matrix_deviation(score_possible, ncols, &bothtails, &lefttails, &righttails);
	  
	  //! Update:
	  result += righttails;
	  
	  ; //result += __get_artimetnic_results(i, i);
	}
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "correlation: [0...1]: spearman";
      	start_time_measurement();  //! ie, start measurement
      	t_float result = 0;
      	for(uint i = 0; i < size_of_array; i++) {
      	  t_float bothtails = 0;
      	  t_float lefttails = 0;
      	  t_float righttails = 0;
      	  const t_float score_possible = 0.010; 	const uint ncols = 15000; //!
      	  //t_float score_possible = size_of_array/(1+i);
      	  //! Compute:
      	  get_signficanceFromCorrelation__Spearman__matrix_deviation(score_possible, ncols, &bothtails, &lefttails, &righttails);
	  
      	  //! Update:
      	  result += righttails;
	  
      	  ; //result += __get_artimetnic_results(i, i);
      	}
      	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);
      	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      
      //! --------------------------------
      /* { const char *stringOf_measureText = "correlation: [0...1]: data-access w/allocation"; */
      /* 	start_time_measurement();  //! ie, start measurement */
      /* 	/\* const t_float default_value_float = 0; *\/ */
      /* 	/\* t_float *arr_tmp = allocate_1d_list_float(ncols, default_value_float); *\/ */
      /* 	for(uint i = 0; i < size_of_array; i++) { */
      /* 	t_float result = 0; */
      /* 	for(uint i = 0; i < size_of_array; i++) { */
      /* 	  t_float bothtails = 0; */
      /* 	  t_float lefttails = 0;  */
      /* 	  t_float righttails = 0; */
      /* 	  //t_float score_possible = size_of_array/(1+i); */
      /* 	  //! Compute: */
      /* 	  get_signficanceFromCorrelation__Pearson__matrix_deviation(score_possible, ncols, &bothtails, &lefttails, &righttails); */
	  
      /* 	  //! Update: */
      /* 	  result += righttails; */
	  
      /* 	  ; //result += __get_artimetnic_results(i, i); */
      /* 	} */
      /* 	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);       */
      /* 	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__); */
      /* } */
    }
  }
  

  assert(false); // FXIME: add seomthing.
}

//! the main-fucntionf ro this.
int test_measure_distribution_studentT() {

  apply_tests();

  assert(false); // FXIME: add seomthing.
}
