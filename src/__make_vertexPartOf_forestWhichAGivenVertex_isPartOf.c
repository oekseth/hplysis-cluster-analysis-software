 {
  //! What we expect:
  assert(new_vertex != UINT_MAX);
  assert(vertexKnowing_disjointForestId != UINT_MAX);
  assert(new_vertex != vertexKnowing_disjointForestId);

  //! Then we use the forest-id of the child:
  const uint forest_id = mapOf_vertexMembershipId[vertexKnowing_disjointForestId]; 
  /* { //! What we expect: */
  /*   assert(forest_id != UINT_MAX); // ie, what we expect at this exeuciton-point */
  /*   assert(forest_id < self->listOf_membership_vertexCollection.get_current_pos()); // ie, tha thte forest-id is actually found in the set. */
  /*   assert(self->listOf_membership_vertexCollection.get_unsafe_list_object(forest_id)); // ie, as we xpect the lsit ot hav ebeen intated at this exeuciton-point. */
  /*   //! Validate that the child-key is found in the set (whre we do to memory-cache-optimalizaiton do not expect the sroted proeprty to always hold): */
  /*   assert(UINT_MAX != self->listOf_membership_vertexCollection.get_unsafe_list_object(forest_id)->get_index_unsorted(vertexKnowing_disjointForestId)); */
  /* } */
  { //! Perform the update:
    mapOf_vertexMembershipId[new_vertex] = forest_id;
    assert(forest_id != UINT_MAX);
    assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
    push__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id, new_vertex);
    assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
    //self->listOf_membership_vertexCollection.get_unsafe_list_object(forest_id)->push(new_vertex);
  } 
  //! Validate that the [ªbove] inserted went as we expected:
  //assert(UINT_MAX != self->listOf_membership_vertexCollection.get_unsafe_list_object(forest_id)->get_index_unsorted(new_vertex));
  assert(forest_id == mapOf_vertexMembershipId[new_vertex]); // ie, that the [below] update word as we expected.
}
