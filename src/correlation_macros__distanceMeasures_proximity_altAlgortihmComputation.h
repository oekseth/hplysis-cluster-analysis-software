#ifndef correlation_macros__distanceMeasures_proximity_altAlgortihmComputation_h
#define correlation_macros__distanceMeasures_proximity_altAlgortihmComputation_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlation_macros__distanceMeasures_proximity_altAlgortihmComputation_h
   @brief etends our "correlation_macros__distanceMeasures_proximity.h" wrt. alternative itnerpreation-strategies for prxoimity-measures
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks 
   - for a brief introduction wrt. proxmity-measures, please see http://www.ibm.com/support/knowledgecenter/SSLVMB_20.0.0/com.ibm.spss.statistics.help/alg_proximities_binary.htm
   - the funciton is based on our "lib_algo_proximities.js" java-script library for copmtuation of proxmity-measures.
 **/

//! A generic procedure to hadnle 'empty' elements.
#define __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident) ({ \
      t_float result = numerator / divident;				\
      if( (!numerator && divident) || (!numerator && !divident) ) {result = 0;} else {result = T_FLOAT_MAX;} \
      result;}) //! ie, return.

// FIXME: wrtie a perofrmance-comparison of [”elow] impelemtantion-strategies.

//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Russel-Rao.
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__RusselRao(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = case_1a; t_float divident = case_1a + case_1b + case_2a + case_2b; \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Compute the distances based on 'Simple Matching' (oekseth, 01. nov. 2015):
//! @remarks when compared to "Russel and Rao" difference is found/is seen wrt. the use of 'positive weight' to 'common-no-matches'.
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Simple Matching.
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SimpleMatching(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = 2*(case_1a + case_2b); t_float divident = (2*case_1a + case_1b + case_2a + case_2b); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Comptue the distances based on Jaccard (oekseth, 01. nov. 2015):
//! @remarks compare the set of 'joint' matches against knowledge assoicated to each vertex (for a given pair).
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Jaccard
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__Jaccard(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = case_1a;    t_float divident = (case_1a + case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.


//! Comptue the distances based on 'Dice, Czekanowski or Sorenson' (oekseth, 01. nov. 2015):
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Dice, Czekanowski, Sorenson
//! @remarks measure/identify distance and not similarity, ie, on contrast to Jaccard: when compared to the latter, we observe how Dice results in increased 'centrality'/'weight' given to the 'found in both the sets' case
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__Dice_Czekanowski_Sorenson(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = 2*case_1a; 	    t_float divident = (2*case_1a + case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Comptue the distances based on the Sokal and Sneath proximity (oekseth, 01. nov. 2015):
//! @remarks difference to Dice is seen by the use/'inclusion' of the 'no-match' property: gives/provides 'equal' significance to 'what they both have in common' as 'what they do not have in common', which for 'open-world-assumptions' (eg, for knowledge-based networks) should be used with carefulness, ie, as it is hard to evaluate significance (ie, 'identify')  every measurement/entity/compoent which two proteins are not described by.
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Sokal-Sneath
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SokalSneath(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = 2*(case_1a + case_2b);   t_float divident = 2*(case_1a + case_2b) + (case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Comptue the distances based on Rogers and Tanimoto (oekseth, 01. nov. 2015):
//! @remarks difference to 'Sokal and Smith' is seen by the decreased with given to 'matches and no-matches', while increasing the 'reduction-factor' associated to 'internal differences': therefore expects the results (to be produced by this proximity-algorithm/approach) to be similar to both Jaccard and Sokal-Smith.
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Rogers-Tanimoto
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__RogersTanimoto(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = (case_1a + case_2b); 	    t_float divident = (case_1a + case_2b) + 2*(case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Comptue the distances based on Sokal and Sneath Similarity 'Measure 2' (oekseth, 01. nov. 2015).
//! @remarks 'give' increased weight to no-matches: when compared to Jaccard difference is observed/found wrt. the doubling of 'penalty' (for matches found in only one of the sets).
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Sokal Sneath measure-2
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SokalSneath_measure2(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = case_1a; t_float divident = (case_1a) + 2*(case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Comptue the distances based on Sokal and Sneath Similarity 'Measure 3' (oekseth, 01. nov. 2015).
//! @remarks similar to Kulczynski wrt. the identification of relationships between 'none-matches' and 'matches': difference found wrt. the weighting/'inclusion' of none-matches. For 'controlled data-sets' (eg, defined selections) this 'inclusion of the unknown' may be of interest/importance, ie, to identify separation of vertex-clusters, eg through the comparison of 'clusters identified through disjointness' VS 'clusters identified through the 'forced-algorithm' of <i>Barnes-Hut</i>.
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Sokal Sneath measure-2
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SokalSneath_measure3(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = case_1a + case_2b;   t_float divident = (case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.

//! Comptue the distances based on Kulczynski (oekseth, 01. nov. 2015):
//! @remarks 'provide'/identifies a direct realtionships/correspondence between 'matched by both' and 'matched by one', ie, does not include/use 'what is unknown for both'; difference to Jaccard is seen by omission of the 'matches-match' count in the divisor, ie, results in an increased sensitivity wrt. the 'matches-only-one' case.
//! @return the correlation-scores of a 2x2 contingency table for the proxmity-metric of: Kulczynski
#define correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__Kulczynski(case_1a, case_1b, case_2a, case_2b) ({ \
      t_float numerator = case_1a; t_float divident = (case_1b + case_2a); \
      __correlation_macros__distanceMeasures_proximity_altAlgortihmComputation__divideHandle_zeros(numerator, divident);}) //! ie, return.


#endif //! EOF
