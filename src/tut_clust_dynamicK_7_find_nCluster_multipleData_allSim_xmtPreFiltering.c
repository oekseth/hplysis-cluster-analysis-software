#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h" //! ie, the 'geneeralized' API for hpLysis
#include "hp_categorizeMatrices_syn.h" 


/**
   @brief demosntrates applciality of our "standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(..)" to idneitfy clsuter-coutn and clsuter-accurayc for mulipel dat-asets.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks extend our "tut_clust_dynamicK_6_find_nCluster_multipleData_Euclid.c" wrt. result-gneeraiton.
   @remarks in this example we:
   -- input: use a 'wrapper-object-spec' to simplify handling, integration, and copmparison of different-formatted data.
   -- evlauation: combines HCA with simlarity-emtics and CCMs to 'get' scalar valeus to simplify cluster-ocmaprison: in this exampel we (for each data-set) writes our ccm-score and nclsuter-count.
   @remarks
   -- discussion: see our "alg_dynamic.html"
**/
int main() 
#else
  void tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, e_hpLysis_clusterAlg_t clustAlg, e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse, const char *str_filePath, const char *stringOf_resultPath)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
  //!
  //! Specify the data-set to use: 

#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
  e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
  //const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  //e_kt_correlationFunction_t = e_kt_correlationFunction_groupOf_minkowski_euclid;
  const char *str_filePath = "result_tut_clust_";
  const char *stringOf_resultPath = ""; //! which we use as the path to write out tehte temproary data-sets used during the comptuations. 
#endif 
  
  uint cnt_interestingSims = 0;
  for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
    const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
    if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;}
    if(isTo_use_directScore__e_kt_correlationFunction(metric_id)) {continue;}
    //! Incrmenet:
    cnt_interestingSims++;
  }
  //! Allocate two result-matrices:
  const uint nrows = cnt_interestingSims; const uint ncols = mapOf_realLife_size;
  s_kt_matrix_t mat_ncluster = initAndReturn__s_kt_matrix(nrows, ncols);
  s_kt_matrix_t mat_ccm = initAndReturn__s_kt_matrix(nrows, ncols);
  //!
  //! Iterate:
  uint sim_pos = 0;
  for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
    const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
    if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;}
    //if(metric_id != e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5) {continue;} //! fixme:reomve this ... inclued to 'figure out why' a isnan || inf-case occurs (oesketh, 06. arp. 2017).
    if(isTo_use_directScore__e_kt_correlationFunction(metric_id)) {continue;}
    {//! Set string:
      const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id); 
      fprintf(stderr, "sim-metric=\"%s\", at %s:%d\n", str_local, __FILE__, __LINE__); 
      //allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data2[%u]", data_id_out); //! ie, intate a new variable "str_local". 
      set_stringConst__s_kt_matrix(&mat_ncluster, /*index=*/sim_pos, str_local, /*addFor_column=*/false);
      set_stringConst__s_kt_matrix(&mat_ccm, /*index=*/sim_pos, str_local, /*addFor_column=*/false);
    }
    //! 
    //! Swet the data-strings:
    for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
      const s_hp_clusterFileCollection data_obj = mapOf_realLife[data_id];
      const char *stringOf_tagSample = mapOf_realLife[data_id].file_name;
      if(stringOf_tagSample == NULL) {stringOf_tagSample =  mapOf_realLife[data_id].tag;}
      //char str_local[3000]; sprintf(str_local, "%u_%s", data_id, stringOf_tagSample);
      //!
      //! Load data:
      s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
      s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
      if(data_obj.file_name != NULL) {		
	obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	  const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	  assert(is_ok);
	  if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
	} else {
	  fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
	}
      }
      if( (stringOf_resultPath != NULL) && (mapOf_realLife[data_id].tag != NULL) ) {
	const char *tag_base = mapOf_realLife[data_id].tag;
	const char *tag = strrchr(tag_base, '/');
	if(tag && strlen(tag)) {tag++;} //! ie, to icnrement past the "/"! symbol
	else {tag = tag_base;} //! ie, as we for the latter asusmes that no direcotyr-prefix is given.
	allocOnStack__char__sprintf(2000, str_filePath, "%s_%u_%s.tsv", stringOf_resultPath, data_id, tag);  //! ie, allcoate the "str_filePath".
	//!
	//! Write out to the "str_filePath" result-file:
	const bool is_ok = export__singleCall__s_kt_matrix_t(&obj_matrixInput, str_filePath, NULL);
	assert(is_ok);
      }
      assert(obj_matrixInput.nrows > 0); //! ie, as we exepct data to have been loaded.
      { //! Validate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
	const s_kt_matrix_t *matrix_input_1 = &obj_matrixInput;
	assert(matrix_input_1);
	assert(matrix_input_1->nrows);
	assert(matrix_input_1->ncols);
	long long int cnt_interesting = 0;
	for(uint i = 0; i < matrix_input_1->nrows; i++) {
	  for(uint j = 0; j < matrix_input_1->ncols; j++) {
	    cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
	assert(cnt_interesting > 0); //! ie, as we otherwse have a poitnless input to the clsutering
      }
      //!
      //! Apply the logics:
      const bool isAn__adjcencyMatrix = data_obj.inputData__isAnAdjcencyMatrix;
      t_float result__ccm = 0; uint result__ncluster = 0;
      const e_hpLysis_clusterAlg_t input_clustAlg = clustAlg;
      if(isOf_type_HCA__e_hpLysis_clusterAlg_t(clustAlg) == false) {
	clustAlg = e_hpLysis_clusterAlg_HCA_centroid;
	assert(isOf_type_HCA__e_hpLysis_clusterAlg_t(clustAlg));
      }
      assert(isOf_type_HCA__e_hpLysis_clusterAlg_t(clustAlg));
      const bool is_ok = standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_matrixInput, isAn__adjcencyMatrix, metric_id, config__ccmMatrixBased__toUse, clustAlg, &result__ccm, &result__ncluster);
      assert(is_ok);
      
      if( (input_clustAlg != clustAlg) && (result__ncluster > 1) && (result__ncluster != UINT_MAX) ) {
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = isAn__adjcencyMatrix; 
	/* obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = config__ccmMatrixBased__toUse; */
	/* obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = false; */
	obj_hp.config.corrMetric_prior.metric_id = metric_id;
	obj_hp.config.corrMetric_prior_use = true;
	//!
	//! Apply logics, ie, comptue clusters: 
	const bool is_ok = cluster__hpLysis_api(&obj_hp, input_clustAlg, &obj_matrixInput, result__ncluster, /*npass=*/200);
	assert(is_ok);
	//! Comptue CCMs:
	result__ccm = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, config__ccmMatrixBased__toUse, NULL, NULL);
	//! De-allocate: 
	free__s_hpLysis_api_t(&obj_hp);
      }
      //!
      //! Print result:
      if(true) {
	printf("\t [%u]:\"%s\" ccm='%f' for |cluster|='%u', at %s:%d\n", data_id, stringOf_tagSample, result__ccm, result__ncluster, __FILE__, __LINE__);
      } else {
	printf("\t [%u]:\"%s\" has 'ideal' cluster-count=%u, a cluster-count associated to a result-accurayc='%f', at %s:%d\n", data_id, stringOf_tagSample, result__ncluster, result__ccm, __FILE__, __LINE__);
      }
      //! ----------------------------------
      //!
      //! Update score:
      if(result__ncluster != UINT_MAX) {
	mat_ncluster.matrix[sim_pos][data_id] = (t_float)result__ncluster;
      } //! otherwise we asusem an eror has been 'observed', and w arning pritned, ie, do Not update latter. 
      mat_ccm.matrix[sim_pos][data_id] = result__ccm;
      //!
      //!
      if(sim_id == 0) {//! Set string:
	const char *str_local = stringOf_tagSample; 
	//allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data2[%u]", data_id_out); //! ie, intate a new variable "str_local". 
	set_stringConst__s_kt_matrix(&mat_ncluster, /*index=*/data_id, str_local, /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_ccm, /*index=*/data_id, str_local, /*addFor_column=*/true);
      }      
      //! ----------------------------------
      //! De-allcoate:
      free__s_kt_matrix(&obj_matrixInput);
    }
    //! Increment:
    sim_pos++;
  }
  //! Specificy an 'itnernal' wrapper-matcro to export the reuslts:
#define __Mi__export(matrix, suffix) ({ \
    char str_conc[2000] = {'\0'}; sprintf(str_conc, "%s%s.tsv", str_filePath, suffix); \
    bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); \
    memset(str_conc, '\0', 2000); sprintf(str_conc, "%s%s.transposed.tsv", str_filePath, suffix); \
    /*! Then export a tranpsoed 'verison': */ \
    is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); })

  //! -------------------------------------------------------------------------
  //!
  //! Write out the result-matrices, both 'as-is' and 'transposed':
  __Mi__export(&mat_ccm, "result__ccm");
  __Mi__export(&mat_ncluster, "result__nCluster");
  //!
  //! De-allocate
  free__s_kt_matrix(&mat_ccm);   free__s_kt_matrix(&mat_ncluster);

#undef __Mi__export
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
    return true;
#endif
}

//! Dynamically evlauate the acucrayc of differnet simliarty-metrics and cluster-algortihsm wrt. each of the data-sets, ie, producing (for each clsutering-algorithm) a data-matrix [simMetric][data_id] (oekseth, 06. arp. 2017)
static void tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix) {
  //! Cofngiure parameters:
  const uint cnt_ccmsToEval = 4;
  const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t arrOf_CCMs[cnt_ccmsToEval] = {
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin,
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns,
  };
  for(uint ccm_id = 0; ccm_id < cnt_ccmsToEval; ccm_id++) {
    // const e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
    // const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
    // const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    //! 
    //! Apply logics: 
    for(uint alg = 0; alg < e_hpLysis_clusterAlg_undef; alg++) {
      const e_hpLysis_clusterAlg_t clustAlg = (e_hpLysis_clusterAlg_t)alg;
      if(isOf_type_HCA__e_hpLysis_clusterAlg_t(clustAlg) || isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(clustAlg)) {
	// TOdO[ccm]: cosnider to add 'accurate' support for CCM-based elvuaiaotn-support for SOM ... and then update [below] (oekseth, 06. apr. 2017).
	if(clustAlg == e_hpLysis_clusterAlg_kCluster__SOM) {continue;} //! ie, to simplify our CCM-based evlauation-appraoch.
	//if(clustAlg != e_hpLysis_clusterAlg_kruskal_hca) {continue;} // FIXME: remove!
	//! Buidl a file-path: 
	allocOnStack__char__sprintf(2000, str_local, "%s_ccm_%u_%s", resultPrefix, ccm_id, get_stringOf__short__e_hpLysis_clusterAlg_t(clustAlg));
	//! Then we evlauate: 
	tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering(mapOf_realLife, mapOf_realLife_size, clustAlg, config__ccmMatrixBased__toUse, str_local, resultPrefix);
      }
    }
  }
}
