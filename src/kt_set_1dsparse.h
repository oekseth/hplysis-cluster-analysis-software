#ifndef kt_set_1dsparse_h
#define kt_set_1dsparse_h

 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_set_1dsparse
   @brief a data-structure for holding a spase matrix of elements
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
#include "types.h"
#include "def_intri.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_kt_set_sparse_colAttribute.h"
//#include "log_clusterC.h"

/**
   @struct s_kt_set_1dsparse
   @brief defines a 2d-struct to hold sparse data-sets
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016)
 **/
typedef struct s_kt_set_1dsparse {
  // uint _nrows; //! ie, the size of the rowOf_scores and rowOf_id matrices
  t_float *rowOf_scores;
  uint *rowOf_id;
  //uint *mapOf_columnSize; //! ie, the number of elments in "mapOf_columnSize" (and rowOf_scores is the latter is used);  
} s_kt_set_1dsparse_t;



//! De-allcoates an object of type s_kt_set_1dsparse_t
static void free_s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self) {
  assert(self);
  if(self->rowOf_scores) {
    free_1d_list_float(&self->rowOf_scores);
  }
  if(self->rowOf_id) {
    free_1d_list_uint(&self->rowOf_id);
  }
}

//! Clear the insert-content (oesekth, 06. aug. 2017).
static inline __attribute__((always_inline)) void clearData__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self) {
  assert(self);
  assert(self->rowOf_id);
  self->rowOf_id[e_kt_set_sparse_colAttribute_currentPos] = 0; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.  
}

/**
   @brief push an element to the row, ie, to the stack
   @param <self> is the object to udpate
   @param <vertex_to_add> is the element to push.
**/
static inline __attribute__((always_inline)) void push__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self, const uint vertex_to_add) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->rowOf_id);
  assert(vertex_to_add != UINT_MAX);
  //! ----------------------------------------------------------
  //!
  uint *rowOf_id = self->rowOf_id;   t_float *rowOf_scores = self->rowOf_scores;
#ifndef NDEBUG  //! then validate:
  const uint current_pos_old = (rowOf_id) ? rowOf_id[e_kt_set_sparse_colAttribute_currentPos] : 0; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.  
#endif
  //!
  //! Apply the logics:
  const uint row_size = 1; //! ie, as we are only to add one elemnet
#define __localConfig__isTo__addRow 0
#include "kt_set_parse__func__add_rowSparse_andEnlargeIfNeccecary.c" //! ie, the udpate-logics for insertion and memory-enarlgement.
#undef __localConfig__isTo__addRow
  self->rowOf_id = rowOf_id;   self->rowOf_scores = rowOf_scores; //! ie, update the references
#ifndef NDEBUG  //! then validate:
  const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  assert(current_pos == (current_pos_old + 1));
  assert(current_pos != UINT_MAX);
  // printf("current_pos=%u, current_pos_old=%u, at %s:%d\n", current_pos, current_pos_old, __FILE__, __LINE__);
#endif
  /* if(true) { */
  /*   const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos] - 1; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size. */
  /*   const uint last_inserted = rowOf_id[2+current_pos]; */
  /*   printf("(top-element) vertex=%u, (given current-pos=%u) at %s:%d\n", last_inserted, current_pos, __FILE__, __LINE__); */
  /*   assert(last_inserted == vertex_to_add); //! ie, what we expect. */
  /* } */
}
/**
   @brief push an element to the row, ie, to the stack
   @param <self> is the object to udpate
   @param <vertex_to_add> is the element to push.
**/
static inline __attribute__((always_inline)) void push__pair__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self, const uint vertex_to_add, const t_float scoreTo_set) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->rowOf_id);
  assert(vertex_to_add != UINT_MAX);
  //! ----------------------------------------------------------
  //!
  uint *rowOf_id = self->rowOf_id;   t_float *rowOf_scores = self->rowOf_scores;
#ifndef NDEBUG  //! then validate:
  const uint current_pos_old = (rowOf_id) ? rowOf_id[e_kt_set_sparse_colAttribute_currentPos] : 0; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.  
#endif
  //!
  //! Apply the logics:
  const uint row_size = 1; //! ie, as we are only to add one elemnet
#define __localConfig__isTo__addRow 0
#define __localConfig__isTo__setScore 1
#include "kt_set_parse__func__add_rowSparse_andEnlargeIfNeccecary.c" //! ie, the udpate-logics for insertion and memory-enarlgement.
#undef __localConfig__isTo__addRow
  self->rowOf_id = rowOf_id;   self->rowOf_scores = rowOf_scores; //! ie, update the references
#ifndef NDEBUG  //! then validate:
  const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  assert(current_pos == (current_pos_old + 1));
  // printf("current_pos=%u, current_pos_old=%u, at %s:%d\n", current_pos, current_pos_old, __FILE__, __LINE__);
#endif
#undef __localConfig__isTo__setScore
  /* if(true) { */
  /*   const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos] - 1; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size. */
  /*   const uint last_inserted = rowOf_id[2+current_pos]; */
  /*   printf("(top-element) vertex=%u, (given current-pos=%u) at %s:%d\n", last_inserted, current_pos, __FILE__, __LINE__); */
  /*   assert(last_inserted == vertex_to_add); //! ie, what we expect. */
  /* } */
}

/**
   @brief pop an element to the row, ie, remove an elment.
   @param <self> is the object to udpate
   @param <scalar_score> is the assicated score (ie any) to set.
   @return the previous/removed/old top-element.
**/
static inline __attribute__((always_inline)) uint pop__s_kt_set_1dsparse_t(const s_kt_set_1dsparse_t *self, t_float *scalar_score) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->rowOf_id);
  assert(self->rowOf_id  != NULL); assert(scalar_score != NULL);
  //! ----------------------------------------------------------
  //!
  uint *rowOf_id = self->rowOf_id;   t_float *rowOf_scores = self->rowOf_scores;   
  *scalar_score = T_FLOAT_MAX;
  //!
  //! Apply the logics:
  //printf("current_pos=%u, at %s:%d\n", rowOf_id[e_kt_set_sparse_colAttribute_currentPos], __FILE__, __LINE__);
  if(rowOf_id[e_kt_set_sparse_colAttribute_currentPos] > 0) { 
    rowOf_id[e_kt_set_sparse_colAttribute_currentPos]--;
    const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    *scalar_score = (rowOf_scores) ? rowOf_scores[current_pos] : T_FLOAT_MAX;
    return rowOf_id[2+current_pos];
  } else {return UINT_MAX;} //! ie, as the stack-element was then 'not found'.
}


/**
   @brief Update the row with the defined set of values, ie, insert a new condensed row
   @param <self> is the object to udpate
   @param <row> ist eh sparse row (ie, where alle lements are of itnerest)
   @param <row_size> is the nubmer of itnerestign elements in row
   @param <row_score> which if set is the set of scores to allocate: should reflect the "isTo_allocateFor_scores" object used when intiated the self object.
**/
static inline __attribute__((always_inline)) void add_rowSparse__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self, const uint *__restrict__ row, const uint row_size, const t_float *__restrict__ row_score) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->rowOf_id);
  assert(row_size);
  //! ----------------------------------------------------------
  //!
  uint *rowOf_id = self->rowOf_id;   t_float *rowOf_scores = self->rowOf_scores;
  //!
  //! Apply the logics:
#define __localConfig__isTo__addRow 1
#include "kt_set_parse__func__add_rowSparse_andEnlargeIfNeccecary.c" //! ie, the udpate-logics for insertion and memory-enarlgement.
#undef __localConfig__isTo__addRow
  self->rowOf_id = rowOf_id;   self->rowOf_scores = rowOf_scores;
}

/**
   @brief Update the row with the defined set of values, ie, insert a new condensed row
   @param <self> is the object to udpate
   @param <row> is a row of values which intersting valeus are Not set to T_FLOAT_MAX
   @param <ncols> is the nubmer of itnerestign elements in row
   @remarks we only insert wrt. weights if the "isTo_allocateFor_scores" was set to trie when intiated the self object.
**/
static inline __attribute__((always_inline)) void add_row_maskImplicit__s_kt_set_1dsparse_t( s_kt_set_1dsparse_t *self, const t_float *__restrict__ row, const uint ncols) {
  assert(self); assert(row);
#define __localConfig__mask__useMaskExplciit 0
#define __localConfig__isCalledFrom_2dObject 0
  t_float *rowOf_scores = self->rowOf_scores;
#include "kt_set_sparse__func__add_maskRow.c" //! ie, the udpate-logics.
#undef __localConfig__mask__useMaskExplciit
#undef __localConfig__isCalledFrom_2dObject
}


/**
   @brief Update the row with the defined set of values, ie, insert a new condensed row
   @param <self> is the object to udpate
   @param <row> is a row of values which intersting valeus are Not set to T_FLOAT_MAX
   @param <ncols> is the nubmer of itnerestign elements in row
   @param <mask> is the mask to be used wrt. idneitfying/knowing fi the row is of interest.
   @remarks we only insert wrt. weights if the "isTo_allocateFor_scores" was set to trie when intiated the self object.
**/
static inline __attribute__((always_inline)) void add_row_maskExplicit__s_kt_set_1dsparse_t( s_kt_set_1dsparse_t *self, const t_float *__restrict__ row, const uint ncols, const char *__restrict__ mask) {
  assert(self); assert(row);
#define __localConfig__mask__useMaskExplciit 1
#define __localConfig__isCalledFrom_2dObject 0
t_float *rowOf_scores = self->rowOf_scores;
#include "kt_set_sparse__func__add_maskRow.c" //! ie, the udpate-logics.
#undef __localConfig__mask__useMaskExplciit
#undef __localConfig__isCalledFrom_2dObject
}


//! @return a sparse row of related entitee assicated to row_id
static inline __attribute__((always_inline)) uint *get_sparseRow_ofIds__kt_set_1dsparse(const s_kt_set_1dsparse_t *self, uint *scalar_rowSize) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->rowOf_id);
  *scalar_rowSize = self->rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  return &self->rowOf_id[2];
}

//! @return a sparse row of related entitee assicated to row_id
static inline __attribute__((always_inline)) uint *get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(const s_kt_set_1dsparse_t *self, uint *scalar_rowSize) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->rowOf_id);
  *scalar_rowSize = self->rowOf_id[e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  return &self->rowOf_id[2];
}
//! @return a sparse row of related entitee assicated to row_id
static inline __attribute__((always_inline)) t_float *get_sparseRow_ofScores__kt_set_1dsparse(const s_kt_set_1dsparse_t *self, uint *scalar_rowSize) {
  assert(self);
  assert(self->rowOf_scores);
  *scalar_rowSize = self->rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  return self->rowOf_scores; //[row_id];
}

//! @return true if both the row-id is found in the same list
 static inline __attribute__((always_inline)) bool has_vertex__s_kt_set_1dsparse_t(const s_kt_set_1dsparse_t *self, const uint head_id) {
  assert(self);
  assert(head_id != UINT_MAX);
  //if(row_id < self->_nrows) 
  {
    //! Iterate:
    const uint size = self->rowOf_id[e_kt_set_sparse_colAttribute_size];
    const uint *__restrict__ row = &(self->rowOf_id[e_kt_set_sparse_colAttribute_currentPos]);
    assert(row);
    const uint search_id = head_id;
    for(uint i = 0; i <= size; i++) {
      if(row[i] == search_id) {return true;}
    }
  }
  return false;
 }
//! @return true if both the row-id and the tail-id is found in the same list
 static inline __attribute__((always_inline)) bool has_pair__s_kt_set_1dsparse_t(const s_kt_set_1dsparse_t *self, const uint head_id, const uint tail_id) {
  assert(self);
  assert(head_id != UINT_MAX);
  assert(tail_id != UINT_MAX);  

  //if(row_id < self->_nrows) 
  {
    //! Iterate:
    const uint size = self->rowOf_id[e_kt_set_sparse_colAttribute_size];
    const uint *__restrict__ row = &(self->rowOf_id[e_kt_set_sparse_colAttribute_currentPos]);
    assert(row);
    const uint search_id = head_id;
    for(uint i = 0; i <= size; i++) {
      if(row[i] != search_id) {return false;}
    }
  }
  //if(col_id < self->_nrows) 
  {
    //! Iterate:
    const uint size = self->rowOf_id[e_kt_set_sparse_colAttribute_size];
    const uint *__restrict__ row = &(self->rowOf_id[e_kt_set_sparse_colAttribute_currentPos]);
    assert(row);
    const uint search_id = head_id;
    for(uint i = 0; i <= size; i++) {
      if(row[i] != search_id) {return false;}
    }
  }

  return true;
}



//! Intiates an empty object
static void setTo_Empty__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self) {
  assert(self);
  self->rowOf_id = NULL;
  self->rowOf_scores = NULL;
  //self-> = 
}

/**
   @brief allocates an s_kt_set_1dsparse_t object
   @param <self> is the object to allocate
   @param <ncols> is the number of columsn in the matrix input, ie, ignroed if matrix==NULL
   @param <row> if set then we itnaite the object using 'this' input-parameter
   @param <mask> if set then we assume the makss are eplxict, ie, only inlcude for "mask[i][j] == 0"
   @param <isTo_useImplictMask> which if set implies that we investigate for T_FLOAT_MAX
   @param <isTo_allocateFor_scores> which if set to false implies that we do Not expect the scores to be of itnerest, ie, an parameter used to reduce the memory-consutmpion
 **/
static void allocate__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self, const uint ncols, t_float *row, char *mask, const bool isTo_useImplictMask, const bool isTo_allocateFor_scores) {
  assert(self); setTo_Empty__s_kt_set_1dsparse_t(self);
  if(ncols) {
    const uint min_cnt_cols = 100;
    const uint min_cnt_cols_plussOne = min_cnt_cols + 2;
    //self->_nrows = nrows;
    //! Allcoate for the vertex-ids:
    // self->matrixOf_id = alloc_generic_type_1d(uint, self->matrixOf_id, nrows);
    //! The allcoate iteratly for each of the rows, ie, to simplify 'cahgnign the size of each row seperatley':
    //for(uint row_id = 0; row_id < nrows; row_id++) {
    const uint default_value_uint = 0;
    self->rowOf_id = allocate_1d_list_uint(min_cnt_cols_plussOne, default_value_uint);
    self->rowOf_id[e_kt_set_sparse_colAttribute_currentPos] = 0;
    self->rowOf_id[e_kt_set_sparse_colAttribute_size] = min_cnt_cols; //! ie, set the 'percieved size'.
    //}
    //! ---------------------------------------------------------------        
    //! ---------------------------------------------------------------
    if(isTo_allocateFor_scores) { //! then allcoate for the scores:
      const t_float default_value_float = 0;
      self->rowOf_scores = allocate_1d_list_float(min_cnt_cols, default_value_float);
    }

    if(row) { //! Then copy the data:
      if(!isTo_allocateFor_scores && !mask) {
	//! Allocate a temproary list of vertex-dis to be used:
	const uint default_value_uint = 0;
	uint *tmpList_ids = allocate_1d_list_uint(ncols, default_value_uint);
	for(uint col_id = 0; col_id < ncols; col_id++) {tmpList_ids[col_id] = col_id;} //! ie, as we thena ssuemt aht all valeus are of itnerest:
	if(self->rowOf_scores) {
	  add_rowSparse__s_kt_set_1dsparse_t(self, tmpList_ids, ncols, row);
	} else {
	  add_rowSparse__s_kt_set_1dsparse_t(self, tmpList_ids, ncols, NULL);
	}
	//! De-allcoate the lcoally reserved list ov values:
	free_1d_list_uint(&tmpList_ids);
      } else if(!mask && isTo_allocateFor_scores) {
	add_row_maskImplicit__s_kt_set_1dsparse_t(self, row, ncols);
      } else if(mask) {
	assert(isTo_useImplictMask == false);
	add_row_maskExplicit__s_kt_set_1dsparse_t(self, row, ncols, mask);
      }
    }
  } else {
    assert(ncols == 0);
    assert(mask == 0);
  }
}

/**
   @brief allocates a simple 1d-list used for data-traversal (oekseth, 06. des. 2016)
   @param <self> is the object to allocate
   @param <ncols> is the number of columsn in the matrix input, ie, ignroed if matrix==NULL
 **/
static void allocate__simple__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self, const uint ncols) {
  allocate__s_kt_set_1dsparse_t(self, ncols, NULL, NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/false);
}


/* //! Intiates an empty object */
/* void setTo_Empty__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *self); */

/* /\** */
/*    @brief allocates an s_kt_set_1dsparse_t object */
/*    @param <self> is the object to allocate */
/*    @param <ncols> is the number of columsn in the matrix input, ie, ignroed if matrix==NULL */
/*    @param <row> if set then we itnaite the object using 'this' input-parameter */
/*    @param <mask> if set then we assume the makss are eplxict, ie, only inlcude for "mask[i][j] == 0" */
/*    @param <isTo_useImplictMask> which if set implies that we investigate for T_FLOAT_MAX */
/*    @param <isTo_allocateFor_scores> which if set to false implies that we do Not expect the scores to be of itnerest, ie, an parameter used to reduce the memory-consutmpion */
/*  **\/ */
/* void allocate__s_kt_set_1dsparse_t(s_kt_set_1dsparse_t *obj, const uint ncols, const t_float *row, char *mask, const bool isTo_useImplictMask, const bool isTo_allocateFor_scores); */

//! @return an empty object of this type
static s_kt_set_1dsparse_t get_empty__s_kt_set_1dsparse_t() {
  s_kt_set_1dsparse_t obj;
  setTo_Empty__s_kt_set_1dsparse_t(&obj);
  return obj;
}
  //! Sort the data-set (oekseth, 06. jul. 2017).
#define MF__sort_forScores__s_kt_set_1dsparse_t(obj_sparse) ({ \
  const uint current_pos = obj_sparse->rowOf_id[e_kt_set_sparse_colAttribute_size]; /*! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.*/ \
  if(current_pos > 0) { \
    uint *row_id = &(obj_sparse->rowOf_id[2]);	    \
    t_float *row_scores = obj_sparse->rowOf_scores; \
    const t_float empty_0 = 0;						\
    t_float *row_tmp_float = allocate_1d_list_float(current_pos, empty_0); \
    quicksort(current_pos, row_scores, row_id, row_tmp_float); \
    /*! Unify the results: */ \
    assert(row_tmp_float); \
    memcpy(row_scores, row_tmp_float, sizeof(t_float)*current_pos); \
    assert(row_tmp_float); \
    free_1d_list_float(&row_tmp_float); }})

#endif //! EOF
