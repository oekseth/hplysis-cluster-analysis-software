#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"

/**
   @brief demonstrates how a similartiy-metirc may be used to describe the simlairty between two vectors. 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a simple permtaution of "tut_sim_3_vec_MINE.c" where we comptue for "row(1) x row(2)" wrt. the tow input-matrices.
   @remarks general: use logics in our "hp_distance" to comptue the simalrit between two vectors.
   @remarks simliartiy: comptue the sim-matrix="Chebychev" between two vectors using our "hp_distance.h" API 
   @remarks "hp_distance": we call the "apply__hp_distance(..)" function;
   @remarks generic: we use our "initSampleOfValues__rand__s_kt_matrix_base_t(..)" (defined in our "kt_matrix_base.h") to simplify the writign of 'statnized' code-examples: simplifeis the 'constuciton' of two uqniue vectors to compare.
   @remarks generalizaiton(example): to simplify the understanidng and re-use of our appraoch we have strucutred our "tut_sim_*" examples to use the same 'example-seutp', ie, where differneces is found wrt. the API-calls in to our "hp_distance", and wrt. different "s_kt_correlationMetric" calls.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif
  const uint nrows = 5; const uint ncols = 100; 
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
  //! Specify/define the simlairty-metric:
  s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_chebychev, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
  //s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/ /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"

  //! Allocate two vectors, ie, matrices with demsinos [1, ncols]:
  //! Note: for simplicty we use [”elow] fucntion, defined in our "kt_matrix_base.h", ie, to set the vectors to default valeus, therby reducing the nubmer of code-lines in this use-case-example; in the latter randomenss is used, ie, for which we expect the vectors to be different.
  assert(nrows > 2);   assert(ncols > 0); //! ie, as we otherise have a poinbtelss' call
  s_kt_matrix_base vec_1 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  s_kt_matrix_base vec_2 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  s_kt_matrix_base vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.

  //!
  //! Apply logics:
  s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
  //! Compute for row[0] x row[1]:
  hp_config.obj_1_index1 = 0;   hp_config.obj_2_index2 = 1;
  const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_2,  &vec_result, /*config=*/hp_config);
  assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
  //!
  //! 'Fetch' out the result:
  assert(vec_result.nrows == 1);   assert(vec_result.ncols == 1);
  assert(vec_result.matrix);   assert(vec_result.matrix[0]);
  const t_float scalar_result = vec_result.matrix[0][0]; 
  //!
  //! Write out the result:
  fprintf(stdout, "# Result: sim-score=\"%f\", at [%s]:%s:%d\n", scalar_result, __FUNCTION__, __FILE__, __LINE__);

  //!
  //! De-allocate:
  free__s_kt_matrix_base_t(&vec_1);
  free__s_kt_matrix_base_t(&vec_2);
  free__s_kt_matrix_base_t(&vec_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
 
