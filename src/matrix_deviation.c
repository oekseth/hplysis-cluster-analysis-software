#include "matrix_deviation.h" 
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "e_template_correlation_tile_maskType.h"
#include "math_baseCmp_operations_alglib.h"
#include "correlation_macros__distanceMeasures.h"
#include "math_generateDistribution_incompleteBeta.h"
#include "correlation_rank.h"

// FIXME: write perfomrance-tests for these functiosn (oekseth, 06. setp. 2016) <-- test the tiem-eprformacne-cost of sepearitng 'inner and outer for-loops' 

/**
   FIXME: compare our 'proceudre to comoptue STD and kurtocis with ALGLIB ... eg, to ivnestigate the overehad of using muliple for-loops to comptue the reusøts
   //! Extract from "samplemoments(..)" in "/home/klatremus/poset_src/externalLibs/alglib/src/statistics.cpp"
        for(i=0; i<=n-1; i++)
        {
            v1 = v1+ae_sqr(x->ptr.p_t_float[i]-(*mean), _state);
        }
        v2 = (t_float)(0);
        for(i=0; i<=n-1; i++)
        {
            v2 = v2+(x->ptr.p_t_float[i]-(*mean));
        }
 **/

/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation_maskUint(const t_float *row, const uint *mask, const uint ncols, s_matrix_deviation_std_row_t *self) {
  assert(self); assert(row);  assert(ncols > 1);
  //! Itniate, ie, to be 'on the safe side'.
  setTo_empty__s_matrix_deviation_std_row(self);
  t_float mean = 0;


#define __localConfig__typeOf_mask e_template_correlation_tile_maskType_mask_explicit

#include "matrix_deviation___stub__sampleDeviationTwoPass.c" //! ie, the computation.

  //!
  //! Update the result-object:
  assert(self); //! ie, ot be 'ont he safe side'.
  self->mean = mean;
  self->variance = variance;
  self->skewness = skewness;
  self->kurtosis = kurtosis;
}
/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation_maskChar(const t_float *row, const char *mask, const uint ncols, s_matrix_deviation_std_row_t *self) {
  assert(self); assert(row);  assert(ncols > 1);
  //! Itniate, ie, to be 'on the safe side'.
  setTo_empty__s_matrix_deviation_std_row(self);
  t_float mean = 0;


#define __localConfig__typeOf_mask e_template_correlation_tile_maskType_mask_explicit

#include "matrix_deviation___stub__sampleDeviationTwoPass.c" //! ie, the computation.

  //!
  //! Update the result-object:
  assert(self); //! ie, ot be 'ont he safe side'.
  self->mean = mean;
  self->variance = variance;
  self->skewness = skewness;
  self->kurtosis = kurtosis;
}

/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation_implicitMask(const t_float *row, const uint ncols, s_matrix_deviation_std_row_t *self) {
  assert(self); assert(row);  assert(ncols > 1);
  //! Itniate, ie, to be 'on the safe side'.
  setTo_empty__s_matrix_deviation_std_row(self);
  t_float mean = 0;


#define __localConfig__typeOf_mask 2 //e_template_correlation_tile_maskType_mask_implicit

#include "matrix_deviation___stub__sampleDeviationTwoPass.c" //! ie, the computation.


  //!
  //! Update the result-object:
  assert(self); //! ie, ot be 'ont he safe side'.
  self->mean = mean;
  self->variance = variance;
  self->skewness = skewness;
  self->kurtosis = kurtosis;
}
/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation(const t_float *row, const uint ncols, s_matrix_deviation_std_row_t *self) {
  assert(self); assert(row);  assert(ncols > 1);
  //! Itniate, ie, to be 'on the safe side'.
  setTo_empty__s_matrix_deviation_std_row(self);
  t_float mean = 0;


#define __localConfig__typeOf_mask e_template_correlation_tile_maskType_mask_none

#include "matrix_deviation___stub__sampleDeviationTwoPass.c" //! ie, the computation.

  //!
  //! Update the result-object:
  assert(self); //! ie, ot be 'ont he safe side'.
  self->mean = mean;
  self->variance = variance;
  self->skewness = skewness;
  self->kurtosis = kurtosis;
}

void get_forMatrix_sampleDeviation__matrix_deviation(t_float **matrix, const uint nrows, const uint ncols, s_matrix_deviation_std_row_t *self) {
  assert(self); assert(matrix);
  assert(nrows > 1);
  assert(ncols > 1);
  //! Itniate, ie, to be 'on the safe side'.
  setTo_empty__s_matrix_deviation_std_row(self);
  t_float mean = 0;


#define __localConfig__typeOf_mask e_template_correlation_tile_maskType_mask_none


  // assert(false); // FIXME: compelte bewlow ... after we have completed our current meausrmenet-setups ... 
  #include "inputMatrix_matrix_deviation___stub__sampleDeviationTwoPass.c" //! ie, the computation.

  //!
  //! Update the result-object:
  assert(self); //! ie, ot be 'ont he safe side'.
  self->mean = mean;
  self->variance = variance;
  self->skewness = skewness;
  self->kurtosis = kurtosis;
}

//! @returnt the signficance-score for: "pearson" and "student-t-distribution":
void get_signficanceFromCorrelation__Pearson__matrix_deviation(const t_float r, const uint n, t_float* bothtails, t_float* lefttail, t_float* righttail) {
    t_float t;
    t_float p;

    *bothtails = 0;
    *lefttail = 0;
    *righttail = 0;

    
    /*
     * Some special cases
     */
    if( r >= 1) {
      *bothtails = 0.0;
      *lefttail = 1.0;
      *righttail = 0.0;
      return;
    }
    if( r <= -1 ) {
      *bothtails = 0.0;
      *lefttail = 0.0;
      *righttail = 1.0;
      return;
    }
    if( n < 5 ) {
      *bothtails = 1.0;
      *lefttail = 1.0;
      *righttail = 1.0;
      return;
    }
    
    /*
     * General case
     */
    t = r * mathLib_float_sqrt((n-2)/(1- r*r));
    p = studenttdistribution(n-2, t);

    //! Update result:
    *bothtails = 2*macro_min(p, 1-p);
    *lefttail = p;
    *righttail = 1-p;
}


#include "math_stat_spearman.c"

void get_signficanceFromCorrelation__Spearman__matrix_deviation(const t_float r, const uint n, t_float* bothtails, t_float* lefttail, t_float* righttail) {
    t_float t;
    t_float p;

    *bothtails = 0;
    *lefttail = 0;
    *righttail = 0;

    /*
     * Special case
     */
    if( n<5 ) {
        *bothtails = 1.0;
        *lefttail = 1.0;
        *righttail = 1.0;
        return;
    }
    
    /*
     * General case
     */
    if( (r >=(t_float)(1)) )   {
      t = T_FLOAT_MAX; // 1.0E10;
    } else {
      if( (r <= (t_float)(-1)) )  {
	t = T_FLOAT_MIN_ABS; //-1.0E10;
      } else {
	t = r*ae_sqrt((n-2)/(1-(r * r)));
      }
    }

    // assert(false); 
    // FIXME: valdiate correnctess of [”elow] .... ie, wrt. [ªbove] if-claues consienring T_FLOAT_MIN_ABS and T_FLOAT_MAX

    if( (t < (t_float)(0)) ) {
      p = correlationtests_spearmantail(t, n);
      *bothtails = 2*p;
      *lefttail = p;
      *righttail = 1-p;
    } else {
        p = correlationtests_spearmantail(-t, n);
        *bothtails = 2*p;
        *lefttail = 1-p;
        *righttail = p;
    }
}



// FIXME: include support for [”elow] wehn we suppor thte "ae_matrix" attribute

#include "math_stat_jarquebera.c"



/*************************************************************************
Jarque-Bera test

This test checks hypotheses about the fact that a  given  sample  X  is  a
sample of normal random variable.

Requirements:
    * the number of elements in the sample is not less than 5.

Input parameters:
    X   -   sample. Array whose index goes from 0 to N-1.
    N   -   size of the sample. N>=5

Output parameters:
    P           -   p-value for the test

Accuracy of the approximation used (5<=N<=1951):

p-value  	    relative error (5<=N<=1951)
[1, 0.1]            < 1%
[0.1, 0.01]         < 2%
[0.01, 0.001]       < 6%
[0.001, 0]          wasn't measured

For N>1951 accuracy wasn't measured but it shouldn't be sharply  different
from table values.

  -- ALGLIB --
     Copyright 09.04.2007 by Bochkanov Sergey
*************************************************************************/
void jarqueberatest(/* Real    */ ae_vector* x,
     ae_int_t n,
     t_float* p)
{
    t_float s;

    *p = 0;

    
    /*
     * N is too small
     */
    if( n<5 )
    {
        *p = 1.0;
        return;
    }
    
    /*
     * N is large enough
     */
    jarquebera_jarqueberastatistic(x, n, &s);
    *p = jarquebera_jarqueberaapprox(n, s);
}


//! @return the jarquebera given skewness and kurtosis
void jarqueberatest_ifSkewnessAndKurtosis_isKnown__matrix_deviation(const uint n, const t_float skewness, const t_float kurtosis, t_float *result) {
  const t_float score = (t_float)n/(t_float)6*(ae_sqr(skewness)+ae_sqr(kurtosis)/4);
  *result = jarquebera_jarqueberaapprox(n, score);
}


//! @return the average of teh ranks which are 'inside' the "AVG +- STD" (oekseth, 06. jan. 2017).
static t_float get_avgAmongRanks__row_deviation(s_matrix_deviation_std_row_t obj, const t_float *row, const t_float *aux__rowRanks, const uint ncols) {
  t_float avg_amongRanks = 0; uint cnt_counts = 0;
  assert(aux__rowRanks); //! ie, what we expect.
  if( (obj.variance != 0) && (obj.variance != T_FLOAT_MAX) ) { //! then we asusem the atlatter ise 'set.
    const t_float variantce_root = mathLib_float_sqrt(obj.variance);
    
    for(uint col_id = 0; col_id < ncols; col_id++) {
      if(row[col_id] != T_FLOAT_MAX) {
	const t_float diff = row[col_id] - obj.mean;
	const t_float diff_abs = mathLib_float_abs(diff);
	if(diff_abs < variantce_root) { //! then udpate the ranks:
	  cnt_counts++; avg_amongRanks += aux__rowRanks[col_id]; //! it, hten use the rank-row.
	}
      }
    }
  }
  //! Adjust:
  if( (cnt_counts != 0) && (avg_amongRanks != 0) ) {
    avg_amongRanks = avg_amongRanks / cnt_counts;
  }
  //! @return:
  return avg_amongRanks;
}

//! Comptue deviation for the s_matrix_deviation_skewnessDescription_t object (oekseth, 06. des. 2016).
bool compute__s_matrix_deviation_skewnessDescription_t(s_matrix_deviation_skewnessDescription_t *self, const t_float *row, const uint row_size, const t_float *row_ranks) {
  if(!self || !row || !row_size) {return false;} //! ie, as no inptu where then set.
  //!
  //! Comptue the 'baisc' proerpties:
  get_forRow_sampleDeviation__matrix_deviation(row, row_size, &(self->obj_deviation));
  //!
  //! Covnert the rank-list to a sorted list:
  const t_float empty_0 = 0;
  uint arrOf_sorted_size = row_size;
  t_float *arrOf_sorted = allocate_1d_list_float(arrOf_sorted_size, empty_0);
  assert(arrOf_sorted);
  sort_array(row, arrOf_sorted, row_size, e_typeOf_sortFunction_ideal); //! a function declared in our "correlation_sort.h"
  { uint local_cntWithValues = 0;
    for(uint col_id = 0; col_id < row_size; col_id++) {
      if(arrOf_sorted[col_id] != T_FLOAT_MAX) {
	local_cntWithValues++;
      }
    }
    arrOf_sorted_size = local_cntWithValues;
  }

 #if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
    //! Validate that [ªbov€] result is correct:
    for(uint col_id = 1; col_id < arrOf_sorted_size; col_id++) {
      if(isOf_interest(arrOf_sorted[col_id]) && isOf_interest(arrOf_sorted[col_id-1])) {
	assert(arrOf_sorted[col_id-1] <= arrOf_sorted[col_id]); //! ie, validate correctness of [above] soritng-routine.
      }
    }
#endif
	     //for(uint col_id = 0; col_id < row_size; col_id++) {arrOf_sorted[col_id] = T_FLOAT_MAX;} //! ie, 'maks' the 'value' as Not-set.

  //!
  { //! Fill the sorted-list with meaninful values:
    uint cnt_overlapping = 0; //! which is used to handle 'merged ranks'.
    const t_float *local_row_ranks = row_ranks;
    t_float *tmp_ranks = NULL;
    if(row_ranks == NULL) {
      tmp_ranks = get_rank__correlation_rank(row_size, row);
      local_row_ranks = tmp_ranks;
    }
    self->quartile_2__andVar = get_avgAmongRanks__row_deviation(self->obj_deviation, row, local_row_ranks, row_size);
    /* for(uint col_id = 0; col_id < row_size; col_id++) { */
    /*   if(local_row_ranks[col_id] != T_FLOAT_MAX) { */
    /* 	const uint rank_id = local_row_ranks[col_id]; */
    /* 	if(arrOf_sorted[rank_id] == T_FLOAT_MAX) {arrOf_sorted[rank_id] = local_row_ranks[col_id];} */
    /* 	else {cnt_overlapping++;} */
    /* 	//local_cntWithValues++; */
    /*   } */
    /* } */
    if(tmp_ranks) { //! then de-allcoate:
      free_1d_list_float(&tmp_ranks); tmp_ranks = NULL;} local_row_ranks = NULL;

/*     //! Remove valeus which are Not set; */
/*     uint local_cntWithValues = 0; */
/*     for(uint col_id = 0; col_id < row_size; col_id++) { */
/*       if(arrOf_sorted[col_id] != T_FLOAT_MAX) { */
/* 	arrOf_sorted[local_cntWithValues] = arrOf_sorted[col_id]; //! which implies that we ignore amsekd/'empty' cells. */
/* 	local_cntWithValues++; */
/*       } */
/*       //else if(col_id) { arrOf_sorted[col_id-1] = */
/*     } */
/* #if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
/*     //! Validate that [ªbov€] result is correct: */
/*     for(uint col_id = 0; col_id < row_size; col_id++) { */
/*       if(col_id < local_cntWithValues) {assert(arrOf_sorted[col_id] != T_FLOAT_MAX);} */
/*       else {assert(arrOf_sorted[col_id] == T_FLOAT_MAX);} */
/*     } */
/* #endif */

/*     //! Update: */
/*     arrOf_sorted_size = local_cntWithValues; */
  }
  if(arrOf_sorted_size != 0) { //! then we udpate our object:
    //!   
    // const t_float avg_amongRanks = get_avgAmongRanks__matrix_deviation(obj, row_id, matrix, aux__matrixRanks, ncols);
    self->valueExtreme_min = arrOf_sorted[0];     self->valueExtreme_max = arrOf_sorted[arrOf_sorted_size-1];
    //! Find the differnet midpoints:
    const uint midIndex_1 = (uint)((t_float)arrOf_sorted_size * (1.0/3.0));
    const uint midIndex_2 = (uint)((t_float)arrOf_sorted_size * 0.5);
    assert(midIndex_2 >= midIndex_1);
    const uint midIndex_3 = (uint)((t_float)arrOf_sorted_size * (2.0/3.0));
    assert(midIndex_3 >= midIndex_2);
    //!
    //! Update:
    self->quartile_1 = arrOf_sorted[midIndex_1];
    self->quartile_2 = arrOf_sorted[midIndex_2];
    assert(midIndex_2 >= midIndex_1);
    if(self->quartile_1 != T_FLOAT_MAX) {
      // assert(self->quartile_2 >= self->quartile_1);
    }
    self->quartile_3 = arrOf_sorted[midIndex_3];
    //!  What we expect:
    if(self->quartile_2 != T_FLOAT_MAX) {
      // assert(self->quartile_3 >= self->quartile_2);
    }
  } //! else we assuemt ehte compelte list is 'maksed', ie, a case which may be plausible.
 

  //!
  //! De-allocate:
  free_1d_list_float(&arrOf_sorted); arrOf_sorted = NULL;

 
  //!
  //! 
  //! At thsi exec-point we asusme the oepraiton was a success.
  return true;
}
 
//! Export result to a file-desriptor (oekseth, 06. jan. 2017).
bool exportToFormat__matrix__matrix_deviation( t_float **matrix, const uint nrows, const uint ncols, FILE *file_out, const char *column_sep, const char *row_sep, const bool config_isToTranspose__whenGeneratingOutput, t_float **aux__matrixRanks, const e_kt_matrix__exportFormats___extremeToPrint_t typeOf_extractToPrint, char **arrOf_names_head) {
  assert(matrix); assert(nrows); assert(ncols); assert(file_out);  
  //!
  //! 
  s_matrix_deviation_skewnessDescription_t *list_result = NULL;
  if(config_isToTranspose__whenGeneratingOutput) { //! then we need to store tmeproary results:
    list_result = alloc_generic_type_1d_xmtMemset(s_matrix_deviation_skewnessDescription_t, list_result, nrows);
    assert(list_result);
    if(!list_result) {
      fprintf(stderr, "!!\t Not enough memroy for nrows=%u, at [%s]:%s:%d\n", nrows, __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
  }
  
  t_float mapOf_extremes_row_id = T_FLOAT_MIN_ABS;
  //t_float *mapOf_extremes = NULL;
  /* if(typeOf_extractToPrint == e_kt_matrix__exportFormats___extremeToPrint_STD_max) { */
  /*   t_float emty_0 = 0; */
  /*   mapOf_extremes = allocate_1d_list_float(nrows, emty_0); */
  /*   assert(mapOf_extremes); */
  /*   for(uint i = 0; i < nrows; i++) { */
  /*     mapOf_extremes[i] = T_FLOAT_MIN_ABS; */
  /*   } */
  /* } else if(typeOf_extractToPrint != e_kt_matrix__exportFormats___extremeToPrint_undef) {assert(false);} //! ie, as we then need adding support 'for this'. */

  for(uint row_id = 0; row_id < nrows; row_id++) {
    assert(matrix[row_id]);
    //! Compute deivaiotn-attributes:
    s_matrix_deviation_skewnessDescription_t obj = setToEmptyAndReturns_matrix_deviation_skewnessDescription_t(); // setTo_empty__s_matrix_deviation_std_row(&obj);
    const bool is_ok = compute__s_matrix_deviation_skewnessDescription_t(&obj, matrix[row_id], ncols, (aux__matrixRanks) ? aux__matrixRanks[row_id] : NULL);
    assert(is_ok);

    if(typeOf_extractToPrint == e_kt_matrix__exportFormats___extremeToPrint_STD_max) {
      //assert(mapOf_extremes);
      if( (obj.obj_deviation.skewness != T_FLOAT_MAX) && (obj.obj_deviation.skewness != 0) ) {
	mapOf_extremes_row_id = macro_max(mapOf_extremes_row_id, obj.obj_deviation.skewness);
      }
      /* assert(mapOf_extremes); */
      /* mapOf_extremes[row_id] = macro_max(mapOf_extremes[row_id], obj.obj_deviation.skewness); */
    } else if(typeOf_extractToPrint != e_kt_matrix__exportFormats___extremeToPrint_undef) {assert(false);} //! ie, as we then need adding support 'for this'.
    
    if(false == config_isToTranspose__whenGeneratingOutput) { //! then we need to store tmeproary results:
      //! Write out row:
      /* if(aux__matrixRanks == NULL) { */
      /* 	fprintf(file_out, "row::%u%s %f%s" "%f%s" "%f%s" "%f" "%s", row_id, column_sep, obj.obj_deviation.mean, column_sep, obj.obj_deviation.variance, column_sep, obj.obj_deviation.skewness, column_sep, obj.obj_deviation.kurtosis, row_sep); */
      /* 	//__Mi__writeOut__row(obj); */
      /* } else { */
      //! Then write out the average of the rank-scores which are 'inside' an "AVG += STD" score
      //fprintf(file_out, "row::%u%s %f%s" "%f%s" "%f%s" "%f%s" "%f" "%s", row_id, column_sep, obj.obj_deviation.mean, column_sep, obj.obj_deviation.variance, column_sep, obj.obj_deviation.skewness, column_sep, obj.obj_deviation.kurtosis, column_sep, obj.quartile_2__andVar, row_sep);
      if(row_id == 0) { //! then we write otu the header-file (oekseth, 06. arp. 2017):
	fprintf(file_out, "%s%s" 
		"%s%s" "%s%s" "%s%s" "%s%s" 
		"%s%s" "%s%s" "%s%s"
		"%s%s"  "%s%s" 		
		"%s" "%s",
		//! --- 
		"#! row_id", column_sep, 
		"mean", column_sep, "variance", column_sep, "skewness", column_sep, "kurtosis", column_sep,
		"quartile-1", column_sep, "quartile-2", column_sep, "quartile-3", column_sep,
		"valueExtreme-min", column_sep, "valueExtreme-max", column_sep
		, "quartile-2-variance"
		, row_sep
		//! --- 
		);
      }
      if(arrOf_names_head != NULL && arrOf_names_head[row_id]) {
	fprintf(file_out, "%s%s", arrOf_names_head[row_id], column_sep);
      } else {
	fprintf(file_out, "row::%u%s", row_id, column_sep);
      }
	fprintf(file_out, //"row::%u%s" 
	      "%f%s" "%f%s" "%f%s" "%f%s" 
	      "%f%s" "%f%s" "%f%s"
	      "%f%s"  "%f%s" 
	      "%f" "%s", 
	      obj.obj_deviation.mean, column_sep, obj.obj_deviation.variance, column_sep, obj.obj_deviation.skewness, column_sep, obj.obj_deviation.kurtosis, column_sep,
	      obj.quartile_1, column_sep, obj.quartile_2, column_sep, obj.quartile_3, column_sep,
	      obj.valueExtreme_min, column_sep, obj.valueExtreme_max, column_sep,
	      obj.quartile_2__andVar, row_sep);
      
      //      }
    } else {list_result[row_id] = obj;}
  }

  if(config_isToTranspose__whenGeneratingOutput) { //! then we need to store tmeproary results:
    //! Write out, seperately for each vertex:
    { //! then we write otu the header-file (oekseth, 06. arp. 2017):
      fprintf(file_out, "%s%s", "#! Row-header", column_sep);
      if(arrOf_names_head) {
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  //const char *str = "";
	  if(arrOf_names_head[row_id]) {
	    const char *str = arrOf_names_head[row_id];
	    fprintf(file_out, "%s%s", str, column_sep);
	  } else {
	    fprintf(file_out, "row::%u%s", row_id, column_sep);
	  }	  
	}
      }
      fprintf(file_out, "%s", row_sep);
    }
    //! -----------------------
    fprintf(file_out, "%s%s", "Mean", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].obj_deviation.mean, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------
    fprintf(file_out, "%s%s", "Variance", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].obj_deviation.variance, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------
    fprintf(file_out, "%s%s", "Skewness", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].obj_deviation.skewness, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------
    fprintf(file_out, "%s%s", "Kurtosis", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].obj_deviation.kurtosis, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------    
    fprintf(file_out, "%s%s", "quartile(1)", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].quartile_1, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------    
    fprintf(file_out, "%s%s", "quartile(2)", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].quartile_2, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ------------------------------------------------------
    fprintf(file_out, "%s%s", "quartile(3)", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].quartile_3, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------    
    fprintf(file_out, "%s%s", "Sample(min)", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].valueExtreme_min, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------    
    fprintf(file_out, "%s%s", "Sample(max)", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if(list_result[row_id].obj_deviation.mean != T_FLOAT_MAX) {
	fprintf(file_out, "%f%s", list_result[row_id].valueExtreme_max, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------    
    //if(aux__matrixRanks != NULL) {
    fprintf(file_out, "%s%s", "avgRanks(insideSTD)", column_sep);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //! Then write out the average of the rank-scores which are 'inside' an "AVG += STD" score      
      const t_float avg_amongRanks = list_result[row_id].quartile_2__andVar;
      //get_avgAmongRanks__matrix_deviation(list_result[row_id], row_id, matrix, aux__matrixRanks, ncols);
      //! Wruite out:
      const char *local_sep = column_sep; if((row_id+1) == nrows) {local_sep = row_sep;}
      if( (avg_amongRanks != 0) && (avg_amongRanks != T_FLOAT_MAX) ) {
	fprintf(file_out, "%f%s", avg_amongRanks, local_sep);
      } else {
	fprintf(file_out, "%c%s", '-', local_sep);
      }
    } //! ---------------------------------------------------------
    //}
    
  }
  if(list_result) {
    //! 
    //! De-allcoate:
    free_generic_type_1d(list_result);
  }
  if(typeOf_extractToPrint == e_kt_matrix__exportFormats___extremeToPrint_STD_max) {
    //assert(mapOf_extremes);
    //! 
    //! De-allcoate:
    if( (mapOf_extremes_row_id != T_FLOAT_MIN_ABS)  && (mapOf_extremes_row_id != T_FLOAT_MAX) ) {
      fprintf(file_out, "#! extreme_STD_Case_max=%f at matrix_deviation.c\n", mapOf_extremes_row_id);
      //free_1d_list_float(&mapOf_extremes); mapOf_extremes = NULL;
    }
  } else if(typeOf_extractToPrint != e_kt_matrix__exportFormats___extremeToPrint_undef) {assert(false);} //! ie, as we then need adding support 'for this'.
  
  //! ---------------------------------------------------------    
  //!
  //! @return
  return true; //! ie, as we asusemt eh oepraiton was a success.
  // #undef __Mi__writeOut__row
}
