assert(self->cluster_cluster); //! ie, as we then asusem the latter ia allocated/set.
//assert(self->cluster_cluster != UINT_MAX); //! ie, as we then asusem the latter ia allocated/set.
assert(sumOf_result__within != T_FLOAT_MAX);

//printf("compute cluster--cluster, a %s:%d\n", __FILE__, __LINE__);

    for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) {
#if(__Mi__update__within)
      const t_float innerClusterDist__in = self->cluster_cluster[cluster_id_in][cluster_id_in];
      if(innerClusterDist__in != T_FLOAT_MAX) {
	sumOf_result__within += innerClusterDist__in;
#ifdef __Mi_isToUpdate__s_kt_matrix_ccm_matrixBased_t
	/* ret_obj.between_min = macro_min(ret_obj.between_min, min_score); */
	/*   ret_obj.between_max = macro_max(ret_obj.between_max, min_score); */
	const t_float min_score = innerClusterDist__in;
	ret_obj.within_min = macro_min(ret_obj.within_min, min_score);
	ret_obj.within_max = macro_max(ret_obj.within_max, min_score);
#endif
      }
#endif
#if(__Mi__update__between)
      uint cnt_udpated = 0;
      //! Identify the 'default' score:    
      t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:
      for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	if(cluster_id_out != cluster_id_in) {
	  const t_float innerClusterDist__between = self->cluster_cluster[cluster_id_in][cluster_id_out];
	  if(innerClusterDist__between != T_FLOAT_MAX) {
	    const t_float candidate_score = innerClusterDist__between;
	    //sumOf_result__between += innerClusterDist__between;
	    //! Udpate the score:
	    min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used.
	    // printf("cluster[%u][%u]\tmin_score=%f, candidate_score=%f, at %s:%d\n", cluster_id_in, cluster_id_out, min_score, candidate_score, __FILE__, __LINE__);
	  }
	}
	if(cnt_udpated > 0) {
	  __clusterConsistency__postProcessScore(); 
	  assert(min_score != T_FLOAT_MAX);
	  sumOf_result__between += min_score;
#ifdef __Mi_isToUpdate__s_kt_matrix_ccm_matrixBased_t
	  ret_obj.between_min = macro_min(ret_obj.between_min, min_score);
	  ret_obj.between_max = macro_max(ret_obj.between_max, min_score);
	  //ret_obj.within_min = macro_min(ret_obj.within_min, min_score);
	  //ret_obj.within_max = macro_max(ret_obj.within_max, min_score);
#endif
	}
      }
#endif
    }
#ifdef __Mi_isToUpdate__s_kt_matrix_ccm_matrixBased_t
//printf("ok\tcompute cluster--cluster, [min, max]=[%f, %f], a %s:%d\n", ret_obj.within_min, ret_obj.within_max,  __FILE__, __LINE__);
//#else
#endif
#if(__Mi__update__within)
//printf("ok\tcompute cluster--cluster, sumOf_result__within=%f, a %s:%d\n", sumOf_result__within, __FILE__, __LINE__);
#endif
#if(__Mi__update__between)
//printf("ok\tcompute cluster--cluster, sumOf_result__within=%f, a %s:%d\n", sumOf_result__between, __FILE__, __LINE__);
#endif

//!
//! REset amcros
#undef __Mi__update__within
#undef __Mi__update__between
#undef __Mi_isToUpdate__s_kt_matrix_ccm_matrixBased_t
