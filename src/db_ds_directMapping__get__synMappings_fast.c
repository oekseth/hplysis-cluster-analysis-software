
assert(self->map_syn_head);
if(relation.head < self->map__size) {
  const uint alt_head = self->map_syn_head[relation.head];
  if(alt_head != UINT_MAX) {
    relation.head = alt_head;
  }
 }
//! ---
assert(self->map_syn_rt);
if(relation.tailRel.rt < self->map__size) {
  const uint alt_rt = self->map_syn_rt[relation.tailRel.rt];
  if(alt_rt != UINT_MAX) {
    relation.tailRel.rt = alt_rt;
  }
 }
//! ---
assert(self->map_syn_tail);
if(relation.tailRel.tail < self->map__size) {
  const uint alt_tail = self->map_syn_tail[relation.tailRel.tail];
  if(alt_tail != UINT_MAX) {
    relation.tailRel.tail = alt_tail;
  }
 }
