  //!
  //!
t_type result__baseCmp = t_type_max;
  if(obj_metric.metric_id_base  == e_kt_semanticSim_base_IC_depth) {
    // FIXME[cocneptaul]: vlaidte correctenss of [”elow] .... eg, wrt. the 'interptaiton' of IC ... eg, by suign a pre-step to adjcust all scores to their 'correct' IC (eg, d(v1, c)/average(d(*, c)))
    const uint v1 = row_size; //self->matrixOf_id[head_1][2];
    const uint v2 = row_size_2; // self->matrixOf_id[head_2][2];
    //!  
    //! Compute:
    const t_type result = MiF__kt_semanticSim__postAdjust_simScore(v1, v2);
    result__baseCmp = result;
    //return result; //! ie, reutrn.
  } else if(obj_metric.metric_id_base  == e_kt_semanticSim_base_IC_score) {
    // FIXME[cocneptaul]: vlaidte correctenss of [”elow] .... eg, wrt. the 'interptaiton' of IC ... eg, by suign a pre-step to adjcust all scores to their 'correct' IC (eg, d(v1, c)/average(d(*, c)))
    t_type v1 = 0;  t_type v2 = 0; 
    //    const uint size_1 = self->matrixOf_id[head_1][2]; 
    for(uint i = 0; i < row_size; i++) {v1 += rowOf_scores_1[i];}
    //const uint size_2 = self->matrixOf_id[head_2][2]; 
    for(uint i = 0; i < row_size_2; i++) {v2 += rowOf_scores_2[i];}
    //!  
    //! Compute:
    const t_type result = MiF__kt_semanticSim__postAdjust_simScore(v1, v2);
    result__baseCmp = result;
    //return result; //! ie, reutrn.
  } else if(obj_metric.metric_id_base  == e_kt_semanticSim_base_LCS_depth_min) { //! then find: min-depth=(min(d(head1, v), d(head2, v))) where "v" is the lcs-subsumber.
    if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_default) { //! then we use a 'simple' min-appraoch:
      t_type ret_result = t_type_max;
#define __MiF__selectFunction(ind1, ind2) ({				\
	  const t_type score_1 = rowOf_scores_1[cntLocal_1]; const t_type score_2 = rowOf_scores_2[cntLocal_2]; \
	  const t_type tmp_score = macro_min(score_1, score_2); \
	  ret_result = macro_min(ret_result, tmp_score); ret_result;})    
      //!
      //! Include logics:
#define __MiF__stopIterationWhen_verticesNoLongerMatches 1
#include "kt_semanticSim__stub__compareSorted.c"
      //!  
      //! @return 
      result__baseCmp = ret_result;
      //      printf("..., at %s:%d\n", __FILE__, __LINE__);
    } else if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_sum) { //! then we use a 'simple' min-appraoch:
      t_type ret_result = 0;
      //! Note: An example of [below] is seen for "Wang's method" \cite{}, eg, as seen/examplfied at ["https://academic.oup.com/bioinformatics/article/23/10/1274/197095/A-new-method-to-measure-the-semantic-similarity-of"]. 
#define __MiF__selectFunction(ind1, ind2) ({				\
	  const t_type score_1 = rowOf_scores_1[cntLocal_1]; const t_type score_2 = rowOf_scores_2[cntLocal_2]; \
	  const t_type tmp_score = macro_pluss(score_1, score_2); \
	  ret_result = macro_pluss(ret_result, tmp_score); ret_result;})    
      //!
      //! Include logics:
#define __MiF__stopIterationWhen_verticesNoLongerMatches 0
#include "kt_semanticSim__stub__compareSorted.c"
      //!  
      //! @return 
      result__baseCmp = ret_result;
    } else {
      t_type ret_result = t_type_max;
#define __MiF__selectFunction(ind1, ind2) ({				\
	  const t_type score_1 = rowOf_scores_1[cntLocal_1]; const t_type score_2 = rowOf_scores_2[cntLocal_2]; \
	  const t_type tmp_score = MiF__kt_semanticSim__postAdjust_simScore(score_1, score_2); \
	  ret_result = macro_min(ret_result, tmp_score); ret_result;})    
      //!
      //! Include logics:
#define __MiF__stopIterationWhen_verticesNoLongerMatches 1
#include "kt_semanticSim__stub__compareSorted.c"
      //!  
      //! @return 
      result__baseCmp = ret_result;
    }
    //return ret_result;
  } else if(obj_metric.metric_id_base  == e_kt_semanticSim_base_LCS_depth_max) { //! then find: min-depth=(min(d(head1, v), d(head2, v))) where "v" is the lcs-subsumber.
    t_type ret_result = t_type_min_abs;
    if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_default) { //! then we use a 'simple' min-appraoch:
      // FIXME[conceptaul]: valdiate correctness of [”elow] ... eg, wrt. the RESNIK sim-metirc-emausre .... where  [below] is based on the assumptioni that onlyt he first item/cvertiex will be sed .... ie, for the first mathcing pari in a sorted list .... 
#define __MiF__selectFunction(ind1, ind2) ({				\
	  const t_type score_1 = rowOf_scores_1[cntLocal_1]; \
      /*const t_type score_2 = rowOf_scores_2[cntLocal_2];*/	\
	  /*const t_type tmp_score = macro_max(score_1, score_2);	*/ \
	  ret_result = score_1;})    
	  //ret_result = macro_max(ret_result, score_1); ret_result;})    
	  //	  ret_result = macro_max(ret_result, tmp_score); ret_result;})    
      //!
      //! Include logics:
#define __MiF__stopIterationWhen_verticesNoLongerMatches 1
#define __MiF__stopIterationWhen_verticesNoLongerMatches__breakAtFirst 1 //! which we use as we asusem input is osrted wrt. distances.
#include "kt_semanticSim__stub__compareSorted.c"
      result__baseCmp = ret_result;
      //      printf("..., at %s:%d\n", __FILE__, __LINE__);
    } else {
#define __MiF__selectFunction(ind1, ind2) ({				\
	  const t_type score_1 = rowOf_scores_1[cntLocal_1]; const t_type score_2 = rowOf_scores_2[cntLocal_2]; \
	  const t_type tmp_score = MiF__kt_semanticSim__postAdjust_simScore(score_1, score_2); \
	  ret_result = macro_max(ret_result, tmp_score); ret_result;})    
      //!
      //! Include logics:
#define __MiF__stopIterationWhen_verticesNoLongerMatches 1
#define __MiF__stopIterationWhen_verticesNoLongerMatches__breakAtFirst 1 //! which we use as we asusem input is osrted wrt. distances.
#include "kt_semanticSim__stub__compareSorted.c"
      result__baseCmp = ret_result;
      //      printf("..., at %s:%d\n", __FILE__, __LINE__);
    }
    //!  
    //! @return 
    //return ret_result;m    result__baseCmp = ret_result;
  } else if(obj_metric.metric_id_base  == e_kt_semanticSim_base_LCS_sum) {
    t_type ret_result = 0;
#define __MiF__selectFunction(ind1, ind2) ({				\
	const t_type score_1 = rowOf_scores_1[cntLocal_1]; const t_type score_2 = rowOf_scores_2[cntLocal_2]; \
	const t_type tmp_score = MiF__kt_semanticSim__postAdjust_simScore(score_1, score_2); \
	ret_result = macro_pluss(ret_result, tmp_score); ret_result;})    
    //!
    //! Include logics:
#define __MiF__stopIterationWhen_verticesNoLongerMatches 1
#define __MiF__stopIterationWhen_verticesNoLongerMatches__breakAtFirst 1 //! which we use as we asusem input is osrted wrt. distances.
#include "kt_semanticSim__stub__compareSorted.c"
    //!  
    //! @return 
    //return ret_result;
    result__baseCmp = ret_result;
  } else if(obj_metric.metric_id_base  == e_kt_semanticSim_base_spath) {
    
    assert(false); // FIXME[conceptual]: hat is the difference between 'this'  and LCS ... ie, consider udpat e[ªob]“e ... wrt. "Information Content (IC)" ... 
    //  } else if(obj_metric.metric_id_base  == e_kt_semanticSim_) {
  } else {
    fprintf(stderr, "!!\t Option Not supported: pelase add supprot for thsi option=%u, at %s:%d\n", obj_metric.metric_id_base, __FILE__, __LINE__);
  assert(false); // FIXME: compelte [”elow] .... "tmp.c" ... add a new enum 'for such'.
  }
