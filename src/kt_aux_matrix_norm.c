#include "kt_aux_matrix_norm.h"

//! @return a humanr-eadalbe string-representition of the normalziiaotn-enum (oesketh, 60. jul. 2017).
const char *get_str__humanReadable__e_kt_normType_t(const e_kt_normType_t enum_id) {
#if(true)
  if(enum_id == e_kt_normType_avgRelative) {return "row-Relative-Avg";}
  if(enum_id == e_kt_normType_avgRelative_abs) {return "row-Relative-Avg-Abs";}
  // --- 
  if(enum_id == e_kt_normType_rankRelative) {return "row-Relative-Rank";}
  if(enum_id == e_kt_normType_rankRelative_abs) {return "row-Relative-Rank-Abs";}
  if(enum_id == e_kt_normType_relative_STD) {return "row-RelativeSTD";}
  if(enum_id == e_kt_normType_relative_STD_abs) {return "row-Relative-STD-Abs";}
  // --- 
  else if(enum_id == e_kt_normType_avg_row) {return "row-Avg";}
  else if(enum_id == e_kt_normType_avg_row_abs) {return "row-Avg-Abs";}
  else if(enum_id == e_kt_normType_avg_col) {return "column-Avg";}
  // --- 
  else if(enum_id == e_kt_normType_max_row_relative) {return "row-Max-Relative";}
  else if(enum_id == e_kt_normType_min_row_relative) {return "row-Min-Relative";}
  // --- 
  else if(enum_id == e_kt_normType_STD_row) {return "row-STD";}
#else
  if(enum_id == e_kt_normType_avgRelative) {return "RowRelativeAvg";}
  if(enum_id == e_kt_normType_avgRelative_abs) {return "RowRelativeAvgAbs";}
  // --- 
  if(enum_id == e_kt_normType_rankRelative) {return "RowRelativeRank";}
  if(enum_id == e_kt_normType_rankRelative_abs) {return "RowRelativeRankAbs";}
  if(enum_id == e_kt_normType_relative_STD) {return "RowRelativeSTD";}
  if(enum_id == e_kt_normType_relative_STD_abs) {return "RowRelativeSTDAbs";}
  // --- 
  else if(enum_id == e_kt_normType_avg_row) {return "RowAvg";}
  else if(enum_id == e_kt_normType_avg_row_abs) {return "RowAvgAbs";}
  else if(enum_id == e_kt_normType_avg_col) {return "RowAvgCol";}
  // --- 
  else if(enum_id == e_kt_normType_max_row_relative) {return "RowMaxRelative";}
  else if(enum_id == e_kt_normType_min_row_relative) {return "RowMinRelative";}
  // --- 
  else if(enum_id == e_kt_normType_STD_row) {return "RowSTD";}
#endif
  // --- 
  else if(enum_id == e_kt_normType_undef) {return "Raw";}
  else {
    fprintf(stderr, "!!\t Add support for this option, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return "";
  }
}

//! @return a humanr-eadalbe string-representition of the normalziiaotn-enum (oesketh, 60. jul. 2017).
const char *get_description__humanReadable__e_kt_normType_t(const e_kt_normType_t enum_id) {
  //! Note: for "adjust-by-max": score(i) = (score(i) - AVG(score))/(max(score)-AVG(score))
  /* */if(enum_id == e_kt_normType_avgRelative)      {return "row-Relative-Avg:      Median=false, Absolute=false,  STD=false, adjust-by-max=true,  rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_avgRelative_abs)  {return "row-Relative-Avg-Abs:  Median=false, Absolute=true,   STD=false, adjust-by-max=true,  rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_rankRelative)     {return "row-Relative-Rank:     Median=true,  Absolute=false,  STD=false, adjust-by-max=true,  rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_rankRelative_abs) {return "row-Relative-Rank-Abs: Median=true,  Absolute=true,   STD=false, adjust-by-max=true,  rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_relative_STD)     {return "row-Relative-STD:      Median=false, Absolute=false,  STD=true,  adjust-by-max=true,  rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_relative_STD_abs) {return "row-Relative-STD-ABS:  Median=false, Absolute=true,   STD=true,  adjust-by-max=true,  rows=true,   extreme=false";}
  // --- 
  else if(enum_id == e_kt_normType_avg_row)          {return "row-Avg:               Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_avg_row_abs)      {return "row-Avg-Abs:           Median=false, Absolute=true,   STD=false, adjust-by-max=false, rows=true,   extreme=false";}
  else if(enum_id == e_kt_normType_avg_col)          {return "column-Avg:            Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=false,  extreme=false";}
  // --- 
  else if(enum_id == e_kt_normType_max_row_relative) {return "row-Max-Relative:      Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=true,   extreme=true";}
  else if(enum_id == e_kt_normType_min_row_relative) {return "row-Min-Relative:      Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=true,   extreme=true";}
  // --- 
  else if(enum_id == e_kt_normType_STD_row)          {return "row-STD:               Median=false, Absolute=false,  STD=true,  adjust-by-max=false, rows=true,   extreme=false";}
  // --- 
  else if(enum_id == e_kt_normType_undef)            {return "Raw";}
  else {
    fprintf(stderr, "!!\t Add support for this option, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return "";
  }
}


//! Normalise socres: score(i) = (score(i) - AVG(score))/(max(score)-AVG(score)) ... ie, to avodi 'large differences in scales' from clustteirng the 'ability' of our 'signature-comarpsion-appraoch' (oekseth, 06. mai. 2017).
void apply_norm__kt_aux_matrix_norm(const t_float *old_data, t_float *result, const uint ncols, const bool isTo_useMedian, const bool isTo_useAbs, const bool isTo_useSTD) {
  assert(old_data); assert(result); assert(ncols > 0);
  t_float avg = 0; t_float max = T_FLOAT_MIN_ABS;
  uint n = 0;
  t_float de_numerator = T_FLOAT_MAX; //max - avg;
  if(isTo_useMedian) {
    const t_float empty_0 = 0;
    t_float *arr_tmpToBeUsedIn_sorting = allocate_1d_list_float(ncols, empty_0);
    t_float *arr_cpy = allocate_1d_list_float(ncols, empty_0);
    assert(arr_tmpToBeUsedIn_sorting);
    assert(arr_cpy);
    //! Copy:
    memcpy(arr_cpy, old_data, sizeof(t_float)*ncols);
    if(isTo_useAbs) {
      for(uint i = 0; i < ncols; i++) {
	arr_cpy[i] = T_FLOAT_MAX;
	if( (old_data[i] != T_FLOAT_MAX) && !isinf(old_data[i])) {
	  arr_cpy[i] = mathLib_float_abs(old_data[i]);      
	} 
      }
    }
    //! Compute:
    avg = get_median(arr_cpy, arr_tmpToBeUsedIn_sorting, ncols); //! where latter fucntion is defined in our "correlation_sort.h".
    free_1d_list_float(&arr_tmpToBeUsedIn_sorting);
    free_1d_list_float(&arr_cpy);
    //printf("\t avg=%f, at %s:%d\n",avg, __FILE__, __LINE__);
  } else if(isTo_useSTD) {
    if(isTo_useAbs) {
      const t_float empty_0 = 0;
      t_float *arr_cpy = allocate_1d_list_float(ncols, empty_0);
      assert(arr_cpy);
      for(uint i = 0; i < ncols; i++) {
	arr_cpy[i] = T_FLOAT_MAX;
	if( (old_data[i] != T_FLOAT_MAX) && !isinf(old_data[i])) {
	  arr_cpy[i] = mathLib_float_abs(old_data[i]);      
	} 
      }
      //! Apply:
      avg = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(arr_cpy, ncols);
      free_1d_list_float(&arr_cpy);
      //printf("\t avg=%f, at %s:%d\n",avg, __FILE__, __LINE__);
    } else {
      avg = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(old_data, ncols);
      //printf("\t avg=%f, at %s:%d\n",avg, __FILE__, __LINE__);
    }
  } else {
    for(uint i = 0; i < ncols; i++) {
      if( (old_data[i] != T_FLOAT_MAX) && !isinf(old_data[i])) {
	max = macro_max(max, old_data[i]);
	// min = macro_min(min, old_data[i]);
	//! --- 
	t_float score = old_data[i];
	if(isTo_useAbs) {
	  score = mathLib_float_abs(score);
	} 
	avg += score;
	n++;
      }
    }
    if((avg!= 0) && n) {avg = avg / n;}
    de_numerator = max - avg;
    // printf("\t avg=%f, at %s:%d\n",avg, __FILE__, __LINE__);
  }
  if(de_numerator == T_FLOAT_MAX) {    de_numerator = avg; }
  //!
  //! Set the data:
  if( (avg != 0) && isOf_interest(avg)) {
    //printf("\t avg=%f, at %s:%d\n",avg, __FILE__, __LINE__);
    for(uint i = 0; i < ncols; i++) {
      if( (old_data[i] != T_FLOAT_MAX) && !isinf(old_data[i])) {
	t_float numerator = old_data[i] - avg;
	t_float score = 1; //! ie, a center-point. old_data[i]; //! ie, 
	if( (numerator != 0) && (de_numerator != 0) ) {
	  score = numerator / de_numerator;
	  assert(score != T_FLOAT_MAX);
	}
	//printf("\t\t [%u]=%f, given num=%f, deNum=%f, at %s:%d\n", i, score, numerator, de_numerator, __FILE__, __LINE__);
	result[i] = score;
      } else {result[i] = T_FLOAT_MAX;} //! ie, mark as empty.
    }  
  } else {
    //printf("\t avg=%f, at %s:%d\n",avg, __FILE__, __LINE__);
    for(uint i = 0; i < ncols; i++) {
      result[i] = T_FLOAT_MAX;
    }
  }
}

//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
s_kt_matrix_base_t initAndReturn__copy__normalized__kt_aux_matrix_norm(const s_kt_matrix_base_t *superset) {
  assert(superset);
  const bool isTo_useMedian = false; const bool isTo_useAbs = true; const bool isTo_useSTD = false;
  s_kt_matrix_base_t self = initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix); //, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
  
  for(uint i = 0; i < self.nrows; i++) {
    apply_norm__kt_aux_matrix_norm(superset->matrix[i], self.matrix[i], self.ncols, isTo_useMedian, isTo_useAbs, isTo_useSTD);
  }
  return self;
}

//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
static s_kt_matrix_base_t __initAndReturn__copy__normalized__kt_aux_matrix_norm(const s_kt_matrix_base_t *superset, const bool isTo_useMedian, const bool isTo_useAbs, const bool isTo_useSTD, const bool isTo_allocate) {
  assert(superset);
  s_kt_matrix_base_t self = (isTo_allocate == false)
    ? initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix) 
    : initAndReturn__allocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix);
    //, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
  
  if(isTo_allocate) {
    assert(self.matrix != NULL);
    assert(self.matrix != superset->matrix);  
  }
  for(uint i = 0; i < self.nrows; i++) {
    apply_norm__kt_aux_matrix_norm(superset->matrix[i], self.matrix[i], self.ncols, isTo_useMedian, isTo_useAbs, isTo_useSTD);
  }
  return self;
}


//! Normalise socres: score(i) = (score(i) - AVG(score))/(max(score)-AVG(score)) ... ie, to avodi 'large differences in scales' from clustteirng the 'ability' of our 'signature-comarpsion-appraoch' (oekseth, 06. mai. 2017).
void apply_norm__list_matrix_norm(const t_float *old_data, t_float *result, const uint ncols,  e_kt_normType_t enum_id) {
  assert(old_data); assert(ncols > 0); assert(result); assert(ncols != UINT_MAX);
  //if(enum_id == e_kt_normType_undef) {enum_id = e_kt_normType_avgRelative;}
  //!
  //!
  if(enum_id == e_kt_normType_avgRelative_abs) {apply_norm__kt_aux_matrix_norm(old_data, result, ncols, /*isTo_useMedian=*/false, /*isTo_useAbs=*/true, /*isTo_useSTD=*/false);}
  else if(enum_id == e_kt_normType_avgRelative) {apply_norm__kt_aux_matrix_norm(old_data, result, ncols, /*isTo_useMedian=*/false, /*isTo_useAbs=*/false, /*isTo_useSTD=*/false);}
  else if(enum_id == e_kt_normType_rankRelative) {apply_norm__kt_aux_matrix_norm(old_data, result, ncols, /*isTo_useMedian=*/true, /*isTo_useAbs=*/false, /*isTo_useSTD=*/false);}
  else if(enum_id == e_kt_normType_rankRelative_abs) {apply_norm__kt_aux_matrix_norm(old_data, result, ncols, /*isTo_useMedian=*/true, /*isTo_useAbs=*/true, /*isTo_useSTD=*/false);}
  else if(enum_id == e_kt_normType_relative_STD) {apply_norm__kt_aux_matrix_norm(old_data, result, ncols, /*isTo_useMedian=*/false, /*isTo_useAbs=*/false, /*isTo_useSTD=*/true);}
  else if(enum_id == e_kt_normType_relative_STD_abs) {apply_norm__kt_aux_matrix_norm(old_data, result, ncols, /*isTo_useMedian=*/false, /*isTo_useAbs=*/true, /*isTo_useSTD=*/true);}
  else {
    fprintf(stderr, "!!\t NOT supported enum:\"%s\". Please ivnestgiate, at [%s]:%s:%d\n", get_str__humanReadable__e_kt_normType_t(enum_id), __FUNCTION__, __FILE__, __LINE__);
  //else if( (enum_id == e_kt_normType_STD_row)  ) {
  }
}


//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
s_kt_matrix_base_t initAndReturn__copy__normalizedByEnum__kt_aux_matrix_norm(const s_kt_matrix_base_t *superset, e_kt_normType_t enum_id, const bool isTo_allocate) {
  assert(superset); assert(superset->nrows); assert(superset->ncols);
  //if(enum_id == e_kt_normType_undef) {enum_id = e_kt_normType_avgRelative;}
  //!
  //!
  if(enum_id == e_kt_normType_avgRelative_abs) {return __initAndReturn__copy__normalized__kt_aux_matrix_norm(superset, /*isTo_useMedian=*/false, /*isTo_useAbs=*/true, /*isTo_useSTD=*/false, isTo_allocate);}
  else if(enum_id == e_kt_normType_avgRelative) {return __initAndReturn__copy__normalized__kt_aux_matrix_norm(superset, /*isTo_useMedian=*/false, /*isTo_useAbs=*/false, /*isTo_useSTD=*/false, isTo_allocate);}
  else if(enum_id == e_kt_normType_rankRelative) {return __initAndReturn__copy__normalized__kt_aux_matrix_norm(superset, /*isTo_useMedian=*/true, /*isTo_useAbs=*/false, /*isTo_useSTD=*/false, isTo_allocate);}
  else if(enum_id == e_kt_normType_rankRelative_abs) {return __initAndReturn__copy__normalized__kt_aux_matrix_norm(superset, /*isTo_useMedian=*/true, /*isTo_useAbs=*/true, /*isTo_useSTD=*/false, isTo_allocate);}
  else if(enum_id == e_kt_normType_relative_STD) {return __initAndReturn__copy__normalized__kt_aux_matrix_norm(superset, /*isTo_useMedian=*/false, /*isTo_useAbs=*/false, /*isTo_useSTD=*/true, isTo_allocate);}
  else if(enum_id == e_kt_normType_relative_STD_abs) {return __initAndReturn__copy__normalized__kt_aux_matrix_norm(superset, /*isTo_useMedian=*/false, /*isTo_useAbs=*/true, /*isTo_useSTD=*/true, isTo_allocate);}
  else if( (enum_id == e_kt_normType_STD_row)  ) {
  s_kt_matrix_base_t self = (isTo_allocate == false)
    ? initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix) 
    : initAndReturn__allocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix);
    // s_kt_matrix_base_t self = initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix); //, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
    //s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t(); init__copy__s_kt_matrix(&self, superset, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
    //s_kt_matrix_t self = initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);    
    for(uint row_id = 0; row_id < self.nrows; row_id++) {
      const t_float sum = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(superset->matrix[row_id], superset->ncols);
      if( (sum != 0) && isOf_interest(sum) && (sum != T_FLOAT_MAX) ) {
	// printf("sum[%u]=%f, at %s:%d\n", row_id, sum, __FILE__, __LINE__);
	const t_float sum_inv = 1.0 / sum;
	for(uint col_id = 0; col_id < self.ncols; col_id++) {
	  if(isOf_interest(superset->matrix[row_id][col_id]) && !isinf(superset->matrix[row_id][col_id])) {
	    self.matrix[row_id][col_id] = superset->matrix[row_id][col_id]*sum_inv;
	    assert(isOf_interest(self.matrix[row_id][col_id]));
	  } else {self.matrix[row_id][col_id] = T_FLOAT_MAX;} //! ie, mark as empty.
	}
      }
    }
    return self;
  } else if( (enum_id == e_kt_normType_avg_row) 
	   || (enum_id == e_kt_normType_avg_row_abs) 
	   || (enum_id == e_kt_normType_min_row_relative) 
	   || (enum_id == e_kt_normType_max_row_relative) 
	     ) {
    s_kt_matrix_base_t self = (isTo_allocate == false)
      ? initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix) 
      : initAndReturn__allocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix);
    //s_kt_matrix_base_t self = initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix); //, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
    //s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t(); init__copy__s_kt_matrix(&self, superset, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
    //s_kt_matrix_t self = initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);    
    for(uint row_id = 0; row_id < self.nrows; row_id++) {
      t_float sum = 0; uint cnt_eval = 0;
      if(enum_id == e_kt_normType_min_row_relative) {
	sum = T_FLOAT_MAX;
      } else if(enum_id == e_kt_normType_max_row_relative) {
	sum = T_FLOAT_MIN_ABS;
      }
      for(uint col_id = 0; col_id < self.ncols; col_id++) {
	if(isOf_interest(superset->matrix[row_id][col_id]) && !isinf(superset->matrix[row_id][col_id])) {
	  cnt_eval++;
	  if(enum_id == e_kt_normType_avg_row_abs) {
	    sum += mathLib_float_abs(superset->matrix[row_id][col_id]); //! ie, to avodi valeus from converging around zero.
	  } else if(enum_id == e_kt_normType_min_row_relative) {
	    if(superset->matrix[row_id][col_id] != 0) {
	      sum = macro_min(sum, superset->matrix[row_id][col_id]); //! ie, to avodi valeus from converging around zero.
	    }
	  } else if(enum_id == e_kt_normType_max_row_relative) {
	    if(superset->matrix[row_id][col_id] != 0) {
	      sum = macro_max(sum, superset->matrix[row_id][col_id]); //! ie, to avodi valeus from converging around zero.
	    }
	  } else { //! where [”elow] is observed to rpdocue in-accurate reuslts for a number of use-cases(oesketh, 06. jul. 2017).
	    sum += superset->matrix[row_id][col_id]; //! ie, to avodi valeus from converging around zero.
	  }
	}
      }
      if( (sum != 0) && (cnt_eval > 0) ) {
	// printf("sum[%u]=%f, at %s:%d\n", row_id, sum, __FILE__, __LINE__);
	const t_float sum_inv = 1.0 / sum;
	for(uint col_id = 0; col_id < self.ncols; col_id++) {
	  if(isOf_interest(superset->matrix[row_id][col_id]) && !isinf(superset->matrix[row_id][col_id])) {
	    self.matrix[row_id][col_id] = superset->matrix[row_id][col_id]*sum_inv;
	  } else {self.matrix[row_id][col_id] = T_FLOAT_MAX;} //! ie, mark as empty.
	}
      }
    }
    return self;
  } else if(enum_id == e_kt_normType_avg_col) { //! then first tranpsoe:
    assert(superset->nrows > 0);
    assert(superset->ncols > 0);
    s_kt_matrix_base_t self_transp = initAndReturn__s_kt_matrix_base_t(superset->ncols, superset->nrows); //! ie, 'tanpsoed- dimensions.
    for(uint row_id = 0; row_id < self_transp.nrows; row_id++) {
      for(uint col_id = 0; col_id < self_transp.ncols; col_id++) {
	self_transp.matrix[row_id][col_id] = superset->matrix[col_id][row_id];
      }
    }    
    //s_kt_matrix_t self_transp = setToEmptyAndReturn__s_kt_matrix_t(); init__copy_transposed__s_kt_matrix(&self_transp, superset, true);
    assert(self_transp.nrows > 0);
    //! Comptut ehte varge for the transposed matrix:
    s_kt_matrix_base_t self_transp_norm = initAndReturn__copy__normalizedByEnum__kt_aux_matrix_norm(&self_transp, e_kt_normType_avg_row, /*is-to-allocate=*/false);
    assert(self_transp_norm.nrows > 0);
    //! De-allcoate local matrix:
    // free__s_kt_matrix_base_t(&self_transp); // FIXME: correct to not include this ... to avodi emrooy-erorr (question, oekseth, 06.08.2020)??
    //!
    //! 'Tranpsoe-back': 
    s_kt_matrix_base_t self = (isTo_allocate == false)
      ? initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix) 
      : initAndReturn__allocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix);
    // s_kt_matrix_base_t self = initAndReturn__s_kt_matrix_base_t(superset->nrows, superset->ncols); 
    assert(self.nrows == self_transp_norm.ncols); //! ie, as we expectr 'transposed'.
    assert(self.ncols == self_transp_norm.nrows); //! ie, as we expectr 'transposed'.
    for(uint row_id = 0; row_id < self_transp_norm.nrows; row_id++) {
      for(uint col_id = 0; col_id < self_transp_norm.ncols; col_id++) {
	assert(col_id < self.nrows);
	assert(row_id < self.ncols);	
	assert(row_id < self_transp_norm.nrows);
	assert(col_id < self_transp_norm.ncols);
	//! 
	self.matrix[col_id][row_id] = self_transp_norm.matrix[row_id][col_id];
      }
    }    
    free__s_kt_matrix_base_t(&self_transp_norm);
    assert(self.nrows > 0);
    //! 
    //! @return 
    return self;
  } else if(enum_id == e_kt_normType_undef) { //! thena  simple copy-operaiton
  s_kt_matrix_base_t self = (isTo_allocate == false)
    ? initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix) 
    : initAndReturn__allocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix);
    //s_kt_matrix_base_t self = initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix); //, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
    /* s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t();  */
    /* const bool is_ok = init__copy__s_kt_matrix(&self, superset, true); */
    // assert(is_ok);
    //! 
    //! @return 
    return self;
  } else {
    fprintf(stderr, "!!\t Add support for this option, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    //!
    //! A fall-back;
    s_kt_matrix_base_t self = initAndReturn__s_kt_matrix_base_t(superset->nrows, superset->ncols);
    return self;
  }
  
}
