
/**
   @remarks in our code-stub we make use of the amcros and vriarbles wrt. our "kt_disjoint__stub__applyLogics_insdieEach.c"
**/

//! Apply the logics:
assert(local_matrix);
uint max_rows = local_matrix->nrows;   uint max_cols = local_matrix->ncols; uint iterationIndex_2 = UINT_MAX;

//! Allocate a local matrix_
t_float **matrix_result_local = NULL;
uint matrix_result_local_size = 0;
if(isTo_transposeMatrix == 0) {matrix_result_local = allocate_2d_list_float(max_rows, max_rows, default_value_float); matrix_result_local_size = max_rows;}
 else {matrix_result_local = allocate_2d_list_float(max_cols, max_cols, default_value_float); matrix_result_local_size = max_cols;}

//! The call:
kt_compute_allAgainstAll_distanceMetric(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, max_rows, max_cols, local_matrix->matrix, local_matrix->matrix, /*result-matrix=*/matrix_result_local, config_self, NULL);

//!
//! Merge the result:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
assert(arrOf_result_row); assert(arrOf_result_row_size != 0);
//const uint cnt_headsToEvaluate = arrOf_result_row_size;
assert(isTo_transposeMatrix == false); //! ie, as we otherwise need toa ad a 'new' "arrof_result_col_2" mappping-list
const uint *__localMapping__head = arrOf_result_row;
const uint *__localMapping__tail = arrOf_result_row_2;
const uint cnt_headsToEvaluate = arrOf_result_row_size;
const uint cnt_tailssToEvaluate = arrOf_result_row_size_2;
//const uint cnt_tailssToEvaluate = arrOf_result_col_size;
#else //! then we assuemt ath 'no mappgin-table is needed to be used':
assert(isTo_transposeMatrix == false); //! ie, to simplify our logis
const uint cnt_headsToEvaluate = nrows_1; //(isTo_transposeMatrix == false) ? global_max_rows : global_max_cols;
const uint cnt_tailssToEvaluate = nrows_2; //
//const uint cnt_headsToEvaluate = (isTo_transposeMatrix == false) ? global_max_rows : global_max_cols;
//const uint cnt_tailssToEvaluate = (isTo_transposeMatrix == false) ? global_max_cols : global_max_rows;
#endif
for(uint head_id = 0; head_id < cnt_headsToEvaluate; head_id++) {
  for(uint tail_id = 0; tail_id < cnt_tailssToEvaluate; tail_id++) {
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
    const uint global_head = __localMapping__head[head_id];
    const uint global_tail = __localMapping__tail[tail_id]; //! ie, as 
    //const uint global_tail = arrOf_result_col[tail_id];
#else //! then we assuemt ath 'no mappgin-table is needed to be used':
    const uint global_head = head_id;
    const uint global_tail = tail_id;
#endif
    assert(global_head < global_max_rows);
    assert(global_tail < global_max_rows);
    //    assert(global_tail < global_max_cols);
    //!
    //! Update:
    const t_float score = matrix_result_local[head_id][tail_id];
    matrix_result[global_head][global_tail] = score;
  }
 }

//! 
//! De-allocate the local matrix-variable:
assert(matrix_result_local); 
assert(matrix_result_local != matrix_result); //! ie, as it otherwise would be wrong to 'apply' [below] de-allcoation.
free_2d_list_float(&matrix_result_local, matrix_result_local_size); matrix_result_local = NULL;
