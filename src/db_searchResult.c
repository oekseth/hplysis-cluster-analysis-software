#include "db_searchResult.h"

//! @return an intlised version of our "s_db_searchResult_rel__config_t" struct.
s_db_searchResult_rel__config_t setToEmpty__s_db_searchResult_rel__config_t() {
  s_db_searchResult_rel__config_t self;
  self.search_preSelectionCriteria = NULL;
  //self.search_preSelectionCriteria__isToUse = false;
  self.search_inRecursive = NULL; //init__matchAll__s_db_searchNode_rel_t();
  self.threshold__scalar__cntVerticesToInvestigate__max = UINT_MAX;
  self.configLogics__isToApply__synonymMappings__inFunction = true;
  //self.search_inRecursive__isToUse = false;
  //! @return
  return self;
}

//! @returns an 'empty and itnlaied version' of our "s_db_searchResult_rel_t" struct (oekseth, 06. mar. 2017)
s_db_searchResult_rel_t setToEmpty__s_db_searchResult_rel_t() {
  s_db_searchResult_rel_t self;
  self.config = setToEmpty__s_db_searchResult_rel__config_t();
  self.mapOf_visistedVertices = init__s_kt_list_1d_float_t(/*size=*/0);
  self.result_cnt_nodesExpanded = 0;
  //! @return
  return self;
}
//! @returns an 'itnlaied version' of our "s_db_searchResult_rel_t" struct (oekseth, 06. mar. 2017)
s_db_searchResult_rel_t init__s_db_searchResult_rel_t(const uint cnt_vertices, const s_db_searchNode_rel_t *search_preSelectionCriteria, const s_db_searchNode_rel_t *search_inRecursive) {
  s_db_searchResult_rel_t self;
  self.config = setToEmpty__s_db_searchResult_rel__config_t();
  self.config.search_preSelectionCriteria = search_preSelectionCriteria;
  self.config.search_inRecursive = search_inRecursive;
  self.mapOf_visistedVertices = init__s_kt_list_1d_float_t(/*size=*/cnt_vertices);
  self.result_cnt_nodesExpanded = 0;
  MF__setAllVertices__toNotBeOfInterest((&self)); //! ie, intiate to 'not known being of interest'.
  //! @return
  return self;
}
//! De-allcoates our "s_db_searchResult_rel_t" structure (oekseht, 06. mar. 2017).
void free__s_db_searchResult_rel_t(s_db_searchResult_rel_t *self) {
  assert(self);
  free__s_kt_list_1d_float_t(&(self->mapOf_visistedVertices));
  self->config = setToEmpty__s_db_searchResult_rel__config_t(); //! ie, 'clear' the cofngiraution-object.
  self->result_cnt_nodesExpanded = 0;
}

//! construct to new search-objects for different evlauation-test-cases (oekseth, 06. mar. 2017). 
//! @return the new-cosntructed object.
s_db_searchResult_rel_t buildSample__s_db_searchResult_rel_t(const uint cnt_vertices, const e_db_searchNode_rel__sampleUseCases_t enum_id, const s_kt_list_1d_uint_t *listOf_heads, const s_kt_list_1d_uint_t *listOf_predicates, const s_kt_list_1d_uint_t *listOf_tails,  const s_kt_list_1d_uint_t *listOf_preSelection_predictes,  const s_kt_list_1d_uint_t *listOf_preSelection_tails, s_db_searchNode_rel_t *retObj_filterBeforeRecursion, s_db_searchNode_rel_t *retObj_inRecursionTraversal) {
  assert(cnt_vertices > 0);
  //! 'Construct' the sample-cases:
  bool retObj_filterBeforeRecursion_isToUse = true;   bool retObj_inRecursionTraversal_isToUse = true;
  buildSampleCases__s_db_searchNode_rel_t(enum_id, listOf_heads, listOf_predicates, listOf_tails, listOf_preSelection_predictes, listOf_preSelection_tails, retObj_filterBeforeRecursion, &retObj_filterBeforeRecursion_isToUse, retObj_inRecursionTraversal, &retObj_inRecursionTraversal_isToUse);
  //! Build the sample-objects:
  if(retObj_filterBeforeRecursion_isToUse && retObj_inRecursionTraversal_isToUse) {
    s_db_searchResult_rel_t self = init__s_db_searchResult_rel_t(cnt_vertices, retObj_filterBeforeRecursion, retObj_inRecursionTraversal);
    //! @return 
    return self;
  } else if(retObj_filterBeforeRecursion_isToUse) {
    s_db_searchResult_rel_t self = init__s_db_searchResult_rel_t(cnt_vertices, retObj_filterBeforeRecursion, NULL);
    //! @return 
    return self;
  } else if(retObj_inRecursionTraversal_isToUse) {
    s_db_searchResult_rel_t self = init__s_db_searchResult_rel_t(cnt_vertices, NULL, retObj_inRecursionTraversal);
    //! @return 
    return self;
  } else {
    assert(false);
    s_db_searchResult_rel_t self = init__s_db_searchResult_rel_t(cnt_vertices, retObj_filterBeforeRecursion, retObj_inRecursionTraversal);
    //! @return 
    return self;
  } 
}
