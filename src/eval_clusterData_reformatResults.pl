use strict;
use Carp;

sub _to_tex {
    my ($input_file, $result_file) = @_;    
    open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
    open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");
    { #! Open the file: 
	my $line_count= 0;
	# my @arr_first = ();
	my @arr_str_prev = (); #! which is sued to address/handle the case where muliple data-rows are rerronsly writtne for the same-file input (which is due to the input-data).
	# my $arr_str_prev_id = "";
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.   
	    my @arrOf_cols = split("\t", $line);
	    #!
	    if($line_count == 0) { #! ie, remvoe the trail-newline.  
		my $str_tab_sep = ""; # eg, "cccc cccc cccc cccc cc"
		my $index = 0;
		foreach my $col (@arrOf_cols) {
		    $str_tab_sep .= "c"; #! ie, the lates-seperator.
		    if($index > 0) {
			$str_tab_sep .= "c"; #! ie, the lates-seperator.
		    }
		    $index += 1;
		}
		printf(FILE_OUT '\begin{tabular}{' . $str_tab_sep . '}' . "\n");
		$index = 0;
		my @arr_str = ();
		foreach my $col_header (@arrOf_cols) {
		    if($index > 0) {
			if($col_header =~/^algorithm\:\:(.+)$/) {
			    $col_header = $1;
			}
			$col_header =~ s/\:\:/ /g;
			$col_header =~ s/-/ /g;			
			foreach my $prefix ("Low", "High") {
			    push(@arr_str, '\rotatebox{90}{ ' . $col_header . " (" . $prefix . ")" . '}');
#			    push(@arr_str, '\rotatebox{90}{ ' . $prefix . $col_header . '}');
			}
			#			push(@arr_str, '\rotatebox{90}{' . $col_header '}');
		    } else {
			$col_header =~ s/\#\!/ /g;			
			push(@arr_str, $col_header);
		    }
		    $index += 1;
		}
		printf(FILE_OUT join(" & ", @arr_str) . '\\\\' . "\n" . '\hline' . "\n");
	    } else { #! then a data-column
		#! data/local_downloaded/56_msq.csv.hpLysis.tsv	0.044444	0.888889
		my $index = 0;
		my @arr_str = ();
		my $is_min = 0; my $use_color = 0;
		foreach my $col_header (@arrOf_cols) {
		    if($index > 0) {
			$col_header = sprintf("%.1f",  $col_header);
			#!
			#! Figure out if a color is to be used:
			my $use_color_local = 0;
			#printf("is_min:%d\n", $is_min);
			if($is_min == 0) {
			    $is_min = 1;
			    if($use_color != 0) {
				$use_color_local = 1;
			    }
			} else {
			    # print("A");
			    $is_min = 0;
			    if($use_color == 0) {
				$use_color = 1; #! ie, turn on (for the next time).
			    } else {
				$use_color = 0; #! ie, turn off (for the next time).
				$use_color_local = 1;
			    }
			}
			# printf("is_min:A:%d\n", $is_min);			
			if($use_color_local) {
			    $col_header = '\cellcolor{lightgrey}' . $col_header;
			}

			push(@arr_str, $col_header); #! ie, remove msot of the decimal places, hence simplfiying readaility.
		    } else {
			#! To simpliyf readaiblitym, remvoe the file-rpefix-parts.
			my @arr_parts = split("/", $col_header);
			if(scalar(@arr_parts) == 0) {croak("!!\t Investigate this case: we expected at least on file-seprator to be found.");}
			$col_header = $arr_parts[scalar(@arr_parts)-1];
			#! To simpliyf readaiblitym, remvoe the '.' suffixes.
			#print("col:B:" . $col_header . "\n");
			@arr_parts = split(/\./, $col_header);
			if(scalar(@arr_parts) == 0) {croak("!!\t Investigate this case: we expected at least on file-seprator to be found: input='$col_header'");}

			$col_header = $arr_parts[0]; #! ie, the first 'item',			
			#print("col:C:" . $col_header . "\n");
			if($col_header =~ /^\d+_(.+)$/) {$col_header = $1;} #! ie, remove the councter-tag.
			#print("col:D:" . $col_header . "\n");
			#!
			#! Add:
			push(@arr_str, $col_header);			
		    }
		    $index += 1;
		}
		if((scalar(@arr_str_prev) > 0) && ($arr_str_prev[0] == $arr_str[0]) && (scalar(@arr_str_prev) < scalar(@arr_str)) ) {
		    ; #! then we ignore the last data.
		} else { #! then we writ eout the data:		    
		    if(scalar(@arr_str_prev) > 0) {
			if($arr_str_prev[0] ne "l") {
			    printf(FILE_OUT join(" & ", @arr_str_prev) . '\\\\' . "\n" );
			}
		    }
		}
		#! remember the previosu data:
		@arr_str_prev = @arr_str;
	    }
	    $line_count += 1;		
	}
	if(scalar(@arr_str_prev) > 0) {
	    printf(FILE_OUT join(" & ", @arr_str_prev) . '\\\\' . "\n" );
	}
    }	
    close(FILE_IN);
    close(FILE_OUT);    
}
    
_to_tex("eval_data_result_0.tsv", "eval_data_result_Rand.tex");
_to_tex("eval_data_result_1.tsv", "eval_data_result_Rand_alt.tex");
_to_tex("eval_data_result_2.tsv", "eval_data_result_ARI.tex");
_to_tex("eval_data_result_3.tsv", "eval_data_result_CHI.tex");
#_to_tex("eval_data_result_.tsv", "eval_data_result_.tex");
# _to_tex("", "");
