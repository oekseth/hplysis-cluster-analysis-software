#include "s_log_approx.h"

// FIXME: update [”elow]:
union f4 {
  int32_t i[4];
  uint32_t u[4];
  float f[4];
  __m128 m;
  __m128i mi;
};

//! Creates the ICSILog lookup table. Must be called once before any call to icsi_log().
static void __preCompute_table_mantissa_float(s_log_approx_t *obj) {
  assert(obj);
  assert(obj->lookup_table_size > 0);
  assert(obj->lookup_table);
  float numlog;
  int *const exp_ptr = ((int*)&numlog);
  int x = *exp_ptr; //x is the float treated as an integer
  x = 0x3F800000; //set the exponent to 0 so numlog=1.0
  *exp_ptr = x;

  // FIXME: dexplain this code-chunk

  const int incr = 1 << (23-(int)obj->lookup_table_size); //amount to increase the mantissa
  const uint p = (uint)pow(2, (uint)obj->lookup_table_size);
  for(uint i=0; i<p; i++) {
    obj->lookup_table[i] = obj->log_function(numlog); //save the log value
    x += incr;
    *exp_ptr = x; //update the float value
  }
}


//! PRe-comptues for [0, n].
static void __preCompute_table_log_uint(s_log_approx_t *obj) {
  assert(obj);
  assert(obj->lookup_table_size > 0);
  assert(obj->lookup_table);
  //! The operation:
  const uint n = obj->lookup_table_size;
  for(uint i = 1; i < n; i++) {
    obj->lookup_table[i] = obj->log_function(i);
  }
}



//! PRe-comptues for [min, max] using an accuracy described by "n".
static void __preCompute_table_log_float_range(s_log_approx_t *obj, const float range_minValue, const float range_maxValue) {
  assert(obj);
  assert(obj->lookup_table_size > 0);
  assert(obj->lookup_table);

  assert(false); // FIXME: update [”elow] .... 

  //! The operation:
  const uint n = obj->lookup_table_size;
  for(uint i = 1; i < n; i++) {
    obj->lookup_table[i] = obj->log_function(i);
  }
}
//! PRe-comptues for [min, max] using an accuracy described by "n".
static void __preCompute_table_log_float_range_between_0_1(s_log_approx_t *obj, const float range_minValue, const float range_maxValue) {
  assert(obj);
  assert(obj->lookup_table_size > 0);
  assert(obj->lookup_table);

  assert(false); // FIXME: cosndier to mergte this funciton and the [ªbopve].

  const float value_range = range_maxValue - range_minValue; //! eg, "0.1 - 0.01 = 0.09"
  assert(value_range > 0);
  const float sizeOf_buckets = value_range / (float)obj->lookup_table_size;

  //! The operation:
  const uint n = obj->lookup_table_size;
  float current_value = range_minValue; 
  for(uint i = 0; i < n; i++) {
    obj->lookup_table[i] = obj->log_function(current_value);
    current_value += sizeOf_buckets;
  }
}


/**
   @brief Allocate the valeus in the object.
   @remarks if the "table_matissa_float" 'option' is used, then "n" is the number of bits to be taken from the mantissa  (0<=n<=23), ie, the accuracy 
**/
void allocate_obj(s_log_approx_t *obj, const uint n, const enum e_s_log_approx_typeOf_log typeOf_logFunction, const e_s_log_approx_t typeOf_init, const float range_minValue, const float range_maxValue) {
  assert(obj);
  obj->lookup_table_size = n;
  obj->lookup_table = allocate_1d_list_float(n, /*default-value=*/0);
  if(typeOf_logFunction == e_s_log_approx_typeOf_log_log) {
    obj->log_function = &log;
  } else if(typeOf_logFunction == e_s_log_approx_typeOf_log_log2) {
    obj->log_function = &log2;
  } else {
    fprintf(stderr, "!!\t Used an option which is currently not supported, an option at enum-index=%u, ie, please verify your call. Observation at [%s]:%s:%d\n", (uint)typeOf_logFunction, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, as we then need to add support for this
  }
  //! What we expect:
  assert(obj->lookup_table_size > 0);
  assert(obj->lookup_table);
  //! The 'filling' operation:
  if(typeOf_init == e_s_log_approx_log_mantissa_float_inStruct) {
    __preCompute_table_mantissa_float(obj);  
  } else if(typeOf_init == e_s_log_approx_log_uint) {
    __preCompute_table_log_uint(obj);  
  } else if(typeOf_init == e_s_log_approx_log_float_range) {
    __preCompute_table_log_float_range(obj);  
  } else if(typeOf_init == e_s_log_approx_log_float_range_between_0_1) {
    __preCompute_table_log_float_range_between_0_1(obj);  
  }  else {
    fprintf(stderr, "!!\t Used an option which is currently not supported, an option at enum-index=%u, ie, please verify your call. Observation at [%s]:%s:%d\n", (uint)typeOf_init, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
  }
}
//! De-allcoate the locally reserved memory for the s_log_approx_t object.
void free_obj(s_log_approx_t *obj) {
  assert(obj);
  obj->lookup_table_size = 0;
  free_1d_list_float(&(obj->lookup_table));
}



/* Computes an approximation of log(val) quickly.
   val is a IEEE 754 float value, must be >0. lookup_table and n must be the same values as provided to fill_icsi_table.
   returns: log(val). No input checking performed.
*/
inline float icsi_log(register float val,
		      register const float *lookup_table, register const int n)
{
  register int *const  exp_ptr = ((int*)&val);
  register int         x = *exp_ptr; //x is treated as integer
  register const int   log_2 = ((x >> 23) & 255) - 127;//exponent

  // FIXME: dexplain this code-chunk

  // FIXME: when this is compling ... then move this to the header-file as a macro.

  // FIXME: how is the non-matisa handled?

  x &= 0x7FFFFF; //mantissa
  x = x >> (23-n); //quantize mantissa
  val = lookup_table[x]; //lookup precomputed value
  return ((val + log_2)* 0.69314718); //natural logarithm
}


static __m128 log2f4(const s_log_approx_t *obj, __m128 x) {

  // FIXME: udpat ehtis fucntion ... validate correctness .. and then re-write into a new macro.

  __m128i exp = _mm_set1_epi32(0x7F800000);
  __m128i mant = _mm_set1_epi32(0x007FFFFF);

  __m128i i = _mm_castps_si128(x);
  __m128  e = _mm_cvtepi32_ps(_mm_sub_epi32(_mm_srli_epi32(_mm_and_si128(i, exp), 23), _mm_set1_epi32(127)));

  const uint LOG2_TABLE_SIZE_LOG2 = obj->lookup_table_size; //! eg, size=8
  const uint LOG2_TABLE_SIZE = (1 << LOG2_TABLE_SIZE_LOG2);
  const uint LOG2_TABLE_SCALE = ((float) ((LOG2_TABLE_SIZE)-1));

  const float *log2_table = obj->lookup_table; //! eg, for x in [1.0, 2.0].
  // static float log2_table[2*LOG2_TABLE_SIZE];

  // FIXME: udpate [”elow] ... using our udpated code.
  union f4 index, p;
  //! Note: 32 - 8 =24, ie, which explains ...??...
  // FIXME: why is "23" used in [below]? <-- first write a sytnetic example wrt. "_mm_srli_epi32(..)", a function which "shift packed 32-bit integers in a right by imm8 while shifting in zeros, and store the results in dst".
  index.mi = _mm_srli_epi32(_mm_and_si128(i, mant), 23 - LOG2_TABLE_SIZE_LOG2);

  // FIXME: write syntetic tests for [below]:

  p.f[0] = log2_table[index.u[0]];
  p.f[1] = log2_table[index.u[1]];
  p.f[2] = log2_table[index.u[2]];
  p.f[3] = log2_table[index.u[3]];

  // FIXME: write a sytnetic test to validte [below]

  return _mm_add_ps(p.m, e);
}


// FIXME: in [above] ... writte a code-test to einvestigate if using itnristnicts would result in a speed-up ... 

// FIXME: understand 'this' ... and then get this and a to-be-wrttien "assert_s_log_approx.cxx" to compile ... rew-rtie this to a struct ... and thereafter write correctness-tests

// FIXME: cosnider to add support for ... pre-computing the 'log-table' as a static array <-- cost is isngivncnt for "20,000,000" elements, ie, no point 'for such'.

// FIXME: seperately support "log(..)" 

// FIXME: use "http://jrfonseca.blogspot.no/2008/09/fast-sse2-pow-tables-or-polynomials.html" as template
    // FIXME: re-write [ªbove] to a new lib-function similar to  "https://github.com/VinInn/FPOptimization/blob/master/vecfunBench/icsiLog.h" .. and "http://www.icsi.berkeley.edu/pubs/techreports/TR-07-002.pdf" <--- re-write and manage to get the following to compile: "s_log_approx.cxx"

    // FIXME: test the appraoch at "http://www.ssw.uni-linz.ac.at/Teaching/Lectures/Sem/2002/reports/Priewasser/index.html" <-- (a) write a fucntion, and test correctenss ... (b) test performance .. (c) compare with [ªbove] 'bucket-appraoch' (for 'closely packed' value-ranges). <-- try making use of the latter to comptue exactly for the 'non-mantissa' ... and then combine 'this' with 'the approximate mantissa-method'.


// FIXME: when this is working ... then include a copy of this file into "/home/klatremus/poset_src/externalLibs/minepy/minepy-master/libmine/" ... and then udpat ethe Makefile

    // FIXME: theafter write test-cases wrt. "hp2q(..)" and "hp3q(..)" and .. in our "/home/klatremus/poset_src/externalLibs/minepy/minepy-master/libmine/backup.mine.c" <-- update our "graphAlgorithms_timeMeasurementsExternalTool_mine.cxx" wrt. the latter.
