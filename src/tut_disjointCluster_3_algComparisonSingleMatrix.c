#include "hpLysis_api.h"
#include "alg_dbScan_brute.h"
#include "hp_ccm.h"
#include "hp_exportMatrix.h"

static s_kt_matrix_t __applyRandomization(const bool setToRand, const t_float fraction, const s_kt_matrix_t *mat_data, const t_float score_notMatch) {
  assert(mat_data);
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(mat_data->nrows, mat_data->ncols);
  const long int cnt_rand_total = (long int)((t_float)(mat_data->nrows * mat_data->ncols) * fraction);
  long int cnt_rand = 0;
  for(uint i = 0; i < mat_data->nrows; i++) {
    for(uint j = 0; j < mat_data->ncols; j++) {
      if(cnt_rand < cnt_rand_total) {
	//!
	if(setToRand == false) {
	  const uint row_id = rand() % mat_data->nrows;
	  const uint col_id = rand() % mat_data->ncols;
	  assert(row_id < mat_data->nrows);
	  assert(col_id < mat_data->ncols);
	  const t_float old_value = mat_data->matrix[i][j];
	  mat_result.matrix[i][j] = mat_data->matrix[row_id][col_id];
	  mat_result.matrix[row_id][col_id] = mat_data->matrix[i][j];
	} else {
	  const t_float old_value = mathLib_float_mod((t_float)rand(), score_notMatch);
	  mat_result.matrix[i][j] = old_value;
	}
	//!
	cnt_rand++;
      }
    }
  }
  return mat_result;
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy



/**
   @brief examplifies how to use different DBSCAN permutations.
   @author Ole Kristian Ekseth  (oekseth,  06. jan. 2017).
   @remarks 
   - extends "tut_disjointCluster_2_algComparisonSingleAlg.c" wrt. evaluation of mulitple algorithms, and the construction of result data.
   - compile: g++ tut_disjointCluster_3_algComparisonSingleMatrix.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC
**/
int main() 
#else
  static void tut_disjointCluster_3_algComparisonSingleMatrix()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
const uint arrOf_sim_ccmCorr_postProcess_size = 14;
s_kt_correlationMetric_t arrOf_sim_ccmCorr_postProcess[arrOf_sim_ccmCorr_postProcess_size] = {
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none), //! ie, Pearson
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute, e_kt_categoryOf_correaltionPreStep_none), //! ie, Perason w/eslibhed permtaution
      // Absolute difference::Canberra 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_absoluteDifference_Canberra, e_kt_categoryOf_correaltionPreStep_none),
      // Fidelity::Hellinger:
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_categoryOf_correaltionPreStep_none),
      // Downward mutaiblity::symmetric-max
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax, e_kt_categoryOf_correaltionPreStep_none),
      // Inner product
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none),
      // Squared distance for Euclid
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Euclid, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_rank), //! ie, spearman.
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      //initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
    };



  //!
  const uint nrows = 10;     const uint ncols = 10; 
  s_kt_matrix_t mat_data_raw = initAndReturn__s_kt_matrix(nrows, ncols);
  //const t_float score_notMatch = T_FLOAT_MAX;  const t_float score_match = 1;
  const t_float score_notMatch = 10;  const t_float score_match = 1;
  //  const t_float score_notMatch = 100;  const t_float score_match = 1;
  //  const t_float score_notMatch = 100;  const t_float score_match = 1;
  const t_float epsilon = 100; const uint minpts = 0; //! which is used to set thresholds for sigicant (ie, central) rows (hence, transalting a dense amtrix into a sparse array). 
  //const t_float epsilon = 100; const uint minpts = nrows;
  //! 
  //! Set defult score:
  for(uint i = 0; i < mat_data_raw.nrows; i++) {
    for(uint k = 0; k < mat_data_raw.ncols; k++) {mat_data_raw.matrix[i][k] = score_notMatch;}}
  //! Add relationships:
#define __addRel(head, tail) ({assert(head < nrows); assert(tail < ncols); mat_data_raw.matrix[head][tail] = score_match;})
  //! Not: for illustrative purposes we avoid inlcuding vertex=0 in any of the clusters.
  //  __addRel(1, 2);
  __addRel(1, 5);
  __addRel(5, 6);
  //__addRel(2, 5);
  __addRel(6, 7);
  __addRel(7, 8);
  __addRel(8, 9);
  //!
  //! A seperate sub-tree:
  __addRel(2, 3);
  __addRel(3, 4);
  //  __addRel(3, 6);
  // __addRel(2, 4);
  __addRel(4, 5);  
#undef __addRel
  //! Compute:
  const uint map_algList_size = 3;
  e_alg_dbScan_brute_t map_algList[map_algList_size] = {
    e_alg_dbScan_brute_linkedListSet,
    e_alg_dbScan_brute_sciKitLearn,
    e_alg_dbScan_brute_hpCluster
  };


  
  
  //! Define the export-objecgt
  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_ccmCorr_postProcess, arrOf_sim_ccmCorr_postProcess_size);
  //! ----------
  const char *str_tut = "tut_disjointCluster_3_algComparisonSingleMatrix";
  const uint data_size = 8;
  const char *str_data[data_size] = {
    "raw",
    "high-HighSoreNotMatch",
    "coVarMatrix-Euclid",
    "coVarMatrix-Pearson",
    "rand-20pst",
    "rand-swap-50pst",
    "rand-swap-100pst",
    "rand-noise",
  };
  
  for(uint d = 0; d < data_size; d++) {
    //! 
    s_kt_matrix_t mat_data = setToEmptyAndReturn__s_kt_matrix_t();
    if(d > 0) {
      if(d == 1) {
	mat_data = initAndReturn__s_kt_matrix(nrows, ncols);  
	for(uint i = 0; i < mat_data_raw.nrows; i++) {
	  for(uint j = 0; j < mat_data_raw.ncols; j++) {
	    if(mat_data_raw.matrix[i][j] == score_notMatch) {
	      mat_data.matrix[i][j]*=1000;
	    }}}
      } else if(d == 2) {
	s_kt_matrix_base_t mat_result = initAndReturn__empty__s_kt_matrix_base_t();
	assert(mat_data_raw.nrows > 0);
	assert(mat_data_raw.ncols > 0);
	assert(mat_data_raw.matrix);
	s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data_raw);
	bool is_ok = apply__hp_distance(/*metric=*/initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none),
					&mat_shallow, &mat_shallow, &mat_result, init__s_hp_distance__config_t());
	assert(is_ok);
	s_kt_matrix_t mat_result_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result);
	//! Copy the result:
	is_ok = init__copy__s_kt_matrix(&mat_data, &mat_result_shallow, /*isTo_updateNames=*/false);
	assert(is_ok);
	//! De-allocate:
	free__s_kt_matrix_base_t(&mat_result);
      } else if(d == 3) { //! Pearson:
	s_kt_matrix_base_t mat_result = initAndReturn__empty__s_kt_matrix_base_t();
	s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data_raw);
	bool is_ok = apply__hp_distance(/*metric=*/initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none),
					&mat_shallow, &mat_shallow, &mat_result, init__s_hp_distance__config_t());
	assert(is_ok);
	s_kt_matrix_t mat_result_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result);
	//! Copy the result:
	is_ok = init__copy__s_kt_matrix(&mat_data, &mat_result_shallow, /*isTo_updateNames=*/false);
	assert(is_ok);
	//! De-allocate:
	free__s_kt_matrix_base_t(&mat_result);	
      } else if(d == 4) {
	mat_data = __applyRandomization(/*setToRand=*/false, /*fraciton=*/.20, &mat_data_raw, score_notMatch);	
      } else if(d == 5) {
	mat_data = __applyRandomization(/*setToRand=*/false, /*fraciton=*/.50, &mat_data_raw, score_notMatch);	
      } else if(d == 6) {
	mat_data = __applyRandomization(/*setToRand=*/false, /*fraciton=*/1, &mat_data_raw, score_notMatch);	
      } else if(d == 7) {
	mat_data = __applyRandomization(/*setToRand=*/true, /*fraciton=*/1, &mat_data_raw, score_notMatch);	
      } else {
	assert(false); // FIXME: add support for this.
      }
    } else {
      mat_data = mat_data_raw; //! then a shallwo copy
    }
    //!
    //! Construct an ajdency-matrix:
    //! Note the below approach resolve the isuse wrt. thresholdiing (ie, by Not updating the adjuency-matrix).
    s_kt_matrix_t mat_adj = initAndReturn__s_kt_matrix(nrows, ncols);  
    for(uint i = 0; i < mat_data_raw.nrows; i++) {
      for(uint j = 0; j < mat_data_raw.ncols; j++) {
	if(mat_data.matrix[i][j] < score_notMatch) {
	  mat_adj.matrix[i][j] = mat_data_raw.matrix[i][j];}}}
  
    //! Allocate a matrix for 'ufniciaotn' fo the elvauaiton-results:
    s_kt_matrix_t matrix_merged = initAndReturn__s_kt_matrix(map_algList_size, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef); //setToEmptyAndReturn__s_kt_matrix_t();
    { //! Set the names of the rows and columns
      for(uint m = 0; m < map_algList_size; m++) {
	const char *str = get_strShort_e_alg_dbScan_brute_t(map_algList[m]);
	assert(str); assert(strlen(str));
	set_stringConst__s_kt_matrix(&matrix_merged, /*index=*/m, str, /*addFor_column=*/false);      
      }
      for(uint m = 0; m < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; m++) {
	const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)m);
	assert(str); assert(strlen(str));
	set_stringConst__s_kt_matrix(&matrix_merged, /*index=*/m, str, /*addFor_column=*/true);      
      }
    }
    //! Construct the default prefix: 
    allocOnStack__char__sprintf(2000, resultPrefix, "%s_%s", str_tut, str_data[d]);
    //! Open the file:
    FILE *fOut = NULL;
    { //! The file to hold the cluster-ids:
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_clusterIds.tsv", resultPrefix);
      fOut = fopen(resultPrefix_local, "w");
      assert(fOut);    
    }
    //!
    //! Iterate:  
    for(uint m = 0; m < map_algList_size; m++) {
      e_alg_dbScan_brute_t alg_id = map_algList[m]; //(e_alg_dbScan_brute_t)m;
      s_kt_list_1d_uint_t clusters = setToEmpty__s_kt_list_1d_uint_t();
      //t_float ccm_score = T_FLOAT_MAX; //!< hold the CCM-score.
      //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      { //! Compute the clusters: 
	s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_adj);
	clusters = computeAndReturn__alg_dbScan_brute(alg_id, &mat_shallow, epsilon, minpts);
	{ //! Set each 'not-part-of-any-cluster' to their own clusters:
	  //! Note: the interptaiton is correct for some use-cases, though not all, hence explaisn why 'the asusmption' is not globally enforced.
	  uint max_clusterId = 0;
	  for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
	    if(clusters.list[row_id] != UINT_MAX) {
	      max_clusterId = macro_max(max_clusterId, clusters.list[row_id]);
	    }
	  }
	  max_clusterId++;
	  for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
	    if(clusters.list[row_id] == UINT_MAX) {
	      clusters.list[row_id] = max_clusterId++;
	    }
	  }
	}
	//! Get the CCM-score:
	s_kt_matrix_base_t mat_shallow_data = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data);
	for(uint ccm_id = 0; ccm_id < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_id++) {
	  const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
	  t_float ccm_score = T_FLOAT_MAX;
	  const bool is_ok = ccm__singleMatrix__hp_ccm(ccm_enum, &mat_shallow_data, clusters.list, &ccm_score);
	  assert(is_ok);
	  if(is_ok) {
	    assert(m  < matrix_merged.nrows);
	    assert(ccm_id  < matrix_merged.ncols);
	    matrix_merged.matrix[m][ccm_id] = ccm_score;
	  }
	}
      } //! and then the cluster-id's are computed.      
      assert(clusters.list_size > 0);
      //! 
      //! Write out the clusters:
      /* fprintf(fOut, "clusters(\"%s\":%f):\t",  */
      /* 	      getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_enum), */
      /* 	      ccm_score); */
      fprintf(fOut, "%s\t", get_strShort_e_alg_dbScan_brute_t(map_algList[m]));
      for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
	if(clusters.list[row_id] != UINT_MAX) {
	  fprintf(fOut, "%u\t", clusters.list[row_id]);
	} else {
	  fprintf(fOut, "-\t");
	}	   
      }
      fprintf(fOut, "#! at %s:%d\n", __FILE__, __LINE__);
      //! De-allocate: 
      free__s_kt_list_1d_uint_t(&clusters);
    } //! end-of-algorithm-eval
    //!
    {//! Wirte out proepteis wrt. latter:
      //! Note: [alg]x[cluster-memberships], and (raw,  rank)[alg]x[CCM-scores]
      //! Buidl a file-path: 
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_featureMatrix_merged", resultPrefix);
      //! The data-consturciton-call:
      writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, matrix_merged, resultPrefix_local, NULL, NULL, /*isTo_writeOutTransposed=*/true);
      //!
      //! De-allocate
      free__s_kt_matrix(&matrix_merged);
    }
    //! De-allocate:   
    if(fOut) {fclose(fOut); fOut = NULL;}
    if(mat_data.matrix != mat_data_raw.matrix) {
       free__s_kt_matrix(&mat_data);
    }
    free__s_kt_matrix(&mat_adj);
  }
  //! De-allocate:   
  free__s_kt_matrix(&mat_data_raw);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

