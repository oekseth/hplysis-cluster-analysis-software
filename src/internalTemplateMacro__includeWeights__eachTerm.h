//! start: distance-computation: ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) )
t_float result_tmp = TEMPLATE_distanceConfiguration_macroFor_metric(rmul1[j2], rmul2[j2]);
// FIXME:[citcila]: [”elow]

//#define internalTemplateMacro__computeDistance__SSE() ((VECTOR_FLOAT_TYPE mul = TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2)))
#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 1) )
	    //! then we include knowledge of the power to 'raise' a number 'into':
t_float result_tmp = TEMPLATE_distanceConfiguration_macroFor_metric(rmul1[j2], rmul2[j2], self.configMetric_power;
								    VECTOR_FLOAT_TYPE mul = TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2, self.configMetric_power);
#else //! then we 'provide' a complex resutl-object intot eh caller, eg, to handle mulipel result-set which may first be merged 'when all valeus are comptued':
	    //#elif(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator)
	    //! then we asusem that the macro-fucntion called in below udpated both a "nuermator" and a "denumerator" field:
								    TEMPLATE_distanceConfiguration_macroFor_metric(term1, term2, objLocalComplex_nonSSE);
#define internalTemplateMacro__computeDistance_SSE() (TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2, objLocalComplex_sse))
#endif //! endif("TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate")
//! end: distance-computation: ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
