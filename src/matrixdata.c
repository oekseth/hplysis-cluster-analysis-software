#include "matrixdata.h"


int
mdata_initf ( mdata_t *init, const char *filename )
{
    struct stat filestatus;
    if ( stat ( filename, &filestatus ) == -1 )
    {
        fprintf(stderr, "Can't use file '%s': %s\n",filename,strerror(errno));
        exit ( EXIT_FAILURE );
    }
    init->n = (size_t)floor ( sqrt ( filestatus.st_size/(2*sizeof(double)) ) );
    init->ts = (double*)malloc ( init->n * init->n * sizeof(double) );
    init->tl = (double*)malloc ( init->n * init->n * sizeof(double) );
    FILE *in = fopen ( filename, "r" );
    int read = fread ( init->ts, sizeof(double), init->n * init->n, in );
    read = fread ( init->tl, sizeof(double), init->n * init->n, in );
    fclose ( in );
    return 0;
}


int
mdata_initz ( mdata_t *init, size_t n )
{
    init->n = n;
    init->ts = (double*)malloc ( n * n * sizeof(double) );
    init->tl = (double*)malloc ( n * n * sizeof(double) );
    memset ( init->ts, 0, n*n*sizeof(double) );
    memset ( init->tl, 0, n*n*sizeof(double) );
    return 0;
}


void
mdata_finalize ( mdata_t *remove )
{
    free ( remove->tl );
    free ( remove->ts );
    remove->n = 0;
}


static int
compare_doubles ( const void *a, const void *b )
{
    return (*((double*)a) <= *((double*)b)) ? -1 : 1;
}


void
mdata_sort ( mdata_t *out, mdata_t *in )
{
    memcpy ( out->ts, in->ts, (in->n)*(in->n)*sizeof(double) );
    qsort ( out->ts, (in->n)*(in->n), sizeof(double), &compare_doubles );
    memcpy ( out->tl, in->tl, (in->n)*(in->n)*sizeof(double) );
    qsort ( out->tl, (in->n)*(in->n), sizeof(double), &compare_doubles );
}
