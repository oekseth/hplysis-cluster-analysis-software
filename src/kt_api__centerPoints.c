#include "kt_api__centerPoints.h"
#include "kt_clusterAlg_fixed.h"
/**
   @brief assicated the centriods (eg, from clustering) to the vertices.
   @remarks a wrapper-method to compute/calculcate the cluster centroids, given to which cluster each element belongs. Depending on the argument method, the centroid is defined as either the mean or the median for each dimension over all elements belonging to a cluster.
   @remarks a subset of the parameters are described below:
   # "cdata":  Result: this array contains the cluster centroids.
   # "cmask": Result: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
   # "method": Input: 
   - For method=='a', the centroid is defined as the mean over all elements belonging to a cluster for each dimension.
   - For method=='m', the centroid is defined as the median over all elements belonging to a cluster for each dimension.
 **/
int getclustercentroids(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, const uint transpose, const char method) {
  t_float **cmask_tmp = NULL;
  const int ret_val = getclustercentroids__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, cmask_tmp, transpose, method, 
								/*isTo_copmuteResults_optimal=*/true, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation*/true);
  assert(cmask_tmp == NULL); //! ie, as we expect 'this' to have been reset
  return ret_val;
}

/**
   @brief assicated the centriods (eg, from clustering) to the vertices.
   @param <obj_1> is the data-matrix to evalaute
   @param <obj_result_clusterId> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterId_columnValue> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <isTo_transposeMatrix> which is set to true if the matrix-object is to be transposed
   @param <isTo_useMean> which if set to flase implies that we use the "median" (ie, the mid-point), which wrt. exeuciton-time results 'in an increased exeuciton-tiem for the same data-set wehn compared to the mean (or: average) option.
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
 **/
bool kt_matrix__getclustercentroids(const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result_clusterId, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const bool isTo_transposeMatrix, const bool isTo_useMean) {
  //! Spedify the fucntion to be called:
  //! Note: input-cluster-dis are defined as: "uint *clusterid = obj_result_clusterId->mapOf_rowIds;":
#define __internalMacro__funcCall() ({getclustercentroids__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, cmask_tmp, isTo_transposeMatrix, method, /*isTo_copmuteResults_optimal=*/true, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation*/true);})
  const char method = (isTo_useMean) ? 'a' : 'm';
  //!
  //! The call:
#include "kt_api__func__kmeans__findCentralElements.c"
}

//! identify the centriods (eg, from clustering) to the vertices (oekseth, 06. feb. 2017).
bool kt_matrix__centroids(const s_kt_matrix_t *obj_1, uint *mapOf_vertexToCluster, s_kt_matrix_t *obj_result_clusterToVertex, const uint nclusters, const e_kt_api_dynamicKMeans_t typeOf_centroidAlg) {
  const bool isTo_transposeMatrix = false;
  //! 
  //! What we expect:
  assert(obj_1); assert(obj_1->nrows);
  assert(mapOf_vertexToCluster);
  assert(obj_result_clusterToVertex->nrows == 0); //! ie, as we expect 'this object' to Not have been intalized.
  assert(obj_result_clusterToVertex->matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized.
  //!
  //! Note(obj_result_clusterToVertex::medoid): matrix=[cluster-id][0] (describibing the sum-of-distance-s fromt eh cenotrid to each asiscated cluster-vertex), mapOf_rowIds=[clusterId-->vertexId]
  //! Note(obj_result_clusterToVertex::else): matrix=[cluster-id][row-id], ie, describing the 'center-point' (of each cluster-center for the set of vertices in a givne clsuter).

  //!
  //!
  const uint nelements = obj_1->nrows;   const uint nrows = obj_1->nrows;
  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) {
    assert(isTo_transposeMatrix == false);
    //!
    const uint empty_value = 0;
    uint **arrOf_clusterBuckets_tmp = allocate_2d_list_uint(nclusters, nelements, empty_value);
    for(uint i = 0; i < nclusters; i++) {
      for(uint k = 0; k < nrows; k++) {arrOf_clusterBuckets_tmp[i][k] = UINT_MAX;}}
    //!
    //! itniate result-object:
    assert(nclusters > 0);
    init__rowMappingTableAndMatrix__s_kt_matrix(obj_result_clusterToVertex, /*isTo_initCompelte_object=*/true, nclusters, /*ncols=*/nelements);
    assert(obj_result_clusterToVertex->matrix);
    assert(obj_result_clusterToVertex->mapOf_rowIds);
    uint *centroids = obj_result_clusterToVertex->mapOf_rowIds;
    t_float *errors = obj_result_clusterToVertex->matrix[0];
    assert(errors);
    //! Initate the 'errors' list nad 'centroids' list:
    for(uint i = 0; i < nclusters; i++) {centroids[i] = UINT_MAX; errors[i] = T_FLOAT_MAX;}
    //! 
    //! Compute:
    getclustermedoids__extensiveInputParams(nclusters, nelements, obj_1->matrix, mapOf_vertexToCluster, centroids, errors, /*isTo_useOptimizedVersion=*/true, arrOf_clusterBuckets_tmp);
    //    getclustermedoids__extensiveInputParams(nclusters, nrows, data, mapOf_vertexToCluster, mapOf_clusterToVertex__distance, /*isTo_useOptimizedVersion=*/true,  arrOf_clusterBuckets_tmp);
    //!
    //! De-allcoate locallly reserved memory:
    assert(arrOf_clusterBuckets_tmp);
    free_2d_list_uint(&arrOf_clusterBuckets_tmp, nclusters); arrOf_clusterBuckets_tmp = NULL;
    //getclustermedoids__extensiveInputParams(nclusters, obj_1->nrows, obj_1->matrix, obj_result->vertex_clusterId, obj_result->cluster_vertexCentroid, obj_result->cluster_errors,/* isTo_useOptimizedVersion=*/true, /*arrOf_clusterBuckets_tmp=*/NULL);
  } else if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_AVG) {
    s_kt_matrix_t obj_result_clusterId; setTo_empty__s_kt_matrix_t(&obj_result_clusterId);
    obj_result_clusterId.nrows = obj_1->nrows; obj_result_clusterId.mapOf_rowIds = mapOf_vertexToCluster;
    // init__rowMappingTable__s_kt_matrix(&obj_result_clusterId, obj_1->nrows);
    assert(obj_result_clusterId.mapOf_rowIds);
    assert(obj_result_clusterId.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized.
    const bool is_ok = kt_matrix__getclustercentroids(obj_1, &obj_result_clusterId, obj_result_clusterToVertex, nclusters, isTo_transposeMatrix, /*isTo_useMean=*/false);
    //! Clear 'temproary cotnaienrs':
    setTo_empty__s_kt_matrix_t(&obj_result_clusterId); //! ie, 'reset'.
    //free__s_kt_matrix(&obj_result_clusterId);
    return is_ok;
  } else if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_rank) {
    s_kt_matrix_t obj_result_clusterId; setTo_empty__s_kt_matrix_t(&obj_result_clusterId);
    obj_result_clusterId.nrows = obj_1->nrows; obj_result_clusterId.mapOf_rowIds = mapOf_vertexToCluster;
    // init__rowMappingTable__s_kt_matrix(&obj_result_clusterId, obj_1->nrows);
    assert(obj_result_clusterId.mapOf_rowIds);
    assert(obj_result_clusterId.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized.
    const bool is_ok = kt_matrix__getclustercentroids(obj_1, &obj_result_clusterId, obj_result_clusterToVertex, nclusters, isTo_transposeMatrix, /*isTo_useMean=*/true);
    //! Clear 'temproary cotnaienrs':
    setTo_empty__s_kt_matrix_t(&obj_result_clusterId); //! ie, 'reset'.
    //free__s_kt_matrix(&obj_result_clusterId);
    return is_ok;
  } else {
    fprintf(stderr, "!!\t(option-not-supported)\t Add support for the typeOf_centroidAlg=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)typeOf_centroidAlg, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up
    return false; //! ie, as 'lgocis' were Not comptued.
  }
  //! 
  //! @return the status:
  return true; //! ie, as we at this euction-point assume sthe operaiton was a cussess.
}



  /**
     @brief identify the center-vertex fo each cluster: calculates the cluster centroids, given to which cluster each element belongs.
     @remarks The  The centroid is defined as the mean over all elements for each dimension.
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
     @remarks a subset of the parameters are described below:
     # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
     # cdata: used 'in funciton-return': updated with the cluster centroids.
     # cmask: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid i fall corresponding data values of the cluster members are missing.
  **/
void getclustermeans(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, float **cmask_tmp,  uint transpose) {
  getclustermeans__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, /*cmask_tmp=*/cmask_tmp,  transpose, /*isTo_copmuteResults_optimal=*/true, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation=*/true);
}

/**
   @brief assicated the centriods (eg, from clustering) to the vertices: use "mean" (or: average)
   @param <obj_1> is the data-matrix to evalaute
   @param <obj_result_clusterId> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterId_columnValue> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <isTo_transposeMatrix> which is set to true if the matrix-object is to be transposed
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
 **/
bool kt_matrix__getclustermeans(const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result_clusterId, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const bool isTo_transposeMatrix) {
  //! Spedify the fucntion to be called:
#define __internalMacro__funcCall() ({getclustermeans__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, /*cmask_tmp=*/cmask_tmp,  isTo_transposeMatrix, /*isTo_copmuteResults_optimal=*/true, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation=*/true); })

  //!
  //! The call:
#include "kt_api__func__kmeans__findCentralElements.c"
}




  /**
     @brief idetnfy the median scores/weights of each cluster
     @remarks Calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the median over all elements for each dimension.
     @remarks a subset of the parameters are described below:
     # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
     # cdata: used 'in funciton-return': updated with the cluster centroids.
     # cmask: an array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
  **/
void getclustermedians(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, float **cmask_tmp, uint transpose) {
  getclustermedians__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, /*cmask_tmp=*/NULL, transpose, /*isTo_copmuteResults_optimal=*/true, /*arrOf_valuesFor_clusterId_global=*/NULL, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation=*/true);
}
/**
   @brief assicated the centriods (eg, from clustering) to the vertices: use "Median"
   @param <obj_1> is the data-matrix to evalaute
   @param <obj_result_clusterId> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterId_columnValue> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <isTo_transposeMatrix> which is set to true if the matrix-object is to be transposed
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
**/
bool kt_matrix__getclustermedians(const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result_clusterId, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const bool isTo_transposeMatrix) {
  //! Spedify the fucntion to be called:
#define __internalMacro__funcCall() ({getclustermedians__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, /*cmask_tmp=*/cmask_tmp, isTo_transposeMatrix, /*isTo_copmuteResults_optimal=*/true, /*arrOf_valuesFor_clusterId_global=*/NULL, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation=*/true); })

  //!
  //! The call:
#include "kt_api__func__kmeans__findCentralElements.c"
}


/*   /\** */
/*      @brief identify the cluster centroids. */
/*      @remarks a wrapper-method to compute/calculcate the cluster centroids, given to which cluster each element belongs. Depending on the argument method, the centroid is defined as either the mean or the median for each dimension over all elements belonging to a cluster. */
/*      @remarks a subset of the parameters are described below: */
/*      # "cdata":  Result: this array contains the cluster centroids. */
/*      # "cmask": Result: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing. */
/*      # "method": Input:  */
/*      - For method=='a', the centroid is defined as the mean over all elements belonging to a cluster for each dimension. */
/*      - For method=='m', the centroid is defined as the median over all elements belonging to a cluster for each dimension. */
/*   **\/ */
/* int getclustercentroids(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, const uint transpose, const char method) { */
/* //int getclustercentroids(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, float **cmask_tmp, uint transpose, const char method) { //, const bool isTo_copmuteResults_optimal, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation/\*= true*\/) { */
/*   return getclustercentroids__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, /\*cmask_tmp=*\/NULL, transpose, method, /\*isTo_copmuteResults_optimal=*\/true, /\*mayUse_zeroAs_emptySign_inComputations=*\/true, /\*needTo_useMask_evaluation=*\/true); */
/* } */
  /**
     @brief idnetify the centroids of each cluster.
     @param <nclusters> The number of clusters.
     @param <nelements> The total number of elements.
     @param <distance> Number of rows is nelements, number of columns is equal to the row number: the distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
     @param <clusterid> The cluster number to which each element belongs.
     @param <centroids> The index of the element that functions as the centroid for each cluster.
     @param <errors> The within-cluster sum of distances between the items and the cluster centroid.
     @remarks 
     - idea: identify the sum of distances to all members in a given cluster: for each clsuter 'allcoate' the centorid which has the minimum/lowest 'sum of distance to other members' (in the same cluster).
     - this function calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the element with the smallest sum of distances to the other elements.
  **/
void getclustermedoids(const uint nclusters, const uint nelements, float** distance, uint clusterid[], uint centroids[], float errors[]) {
  getclustermedoids__extensiveInputParams(nclusters, nelements, distance, clusterid, centroids, errors,/* isTo_useOptimizedVersion=*/true, /*arrOf_clusterBuckets_tmp=*/NULL);
}
/**
   @brief idnetify the cenotrids (and assicated errors) of each cluster.
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @return true upon success.
**/
bool kt_matrix__getclustermedoids(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters) {
  assert(obj_1); assert(obj_result); assert(nclusters > 0);
  
  //! Intaite the result-opbject:
  init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, nclusters, obj_1->nrows);
  //!
  //! Compute:
  getclustermedoids__extensiveInputParams(nclusters, obj_1->nrows, obj_1->matrix, obj_result->vertex_clusterId, obj_result->cluster_vertexCentroid, obj_result->cluster_errors,/* isTo_useOptimizedVersion=*/true, /*arrOf_clusterBuckets_tmp=*/NULL);

  //! @return the status:
  return true; //! ie, as we at this euction-point assume sthe operaiton was a cussess.
}
