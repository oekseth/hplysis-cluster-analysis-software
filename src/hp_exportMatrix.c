#include "hp_exportMatrix.h"


static const uint arrOf_sim_postProcess_size = 14;
static s_kt_correlationMetric_t arrOf_sim_postProcess[arrOf_sim_postProcess_size] = {
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none), //! ie, Pearson
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute, e_kt_categoryOf_correaltionPreStep_none), //! ie, Perason w/eslibhed permtaution
      // Absolute difference::Canberra 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_absoluteDifference_Canberra, e_kt_categoryOf_correaltionPreStep_none),
      // Fidelity::Hellinger:
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_categoryOf_correaltionPreStep_none),
      // Downward mutaiblity::symmetric-max
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax, e_kt_categoryOf_correaltionPreStep_none),
      // Inner product
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none),
      // Squared distance for Euclid
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Euclid, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_rank), //! ie, spearman.
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      //initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
    };


//! @return an intlized verison of the s_hp_exportMatrix_t object (oesketh, 06. jun. 2017).
s_hp_exportMatrix_t init__s_hp_exportMatrix_t(const s_kt_correlationMetric_t *__arrOf_sim_postProcess, const uint __arrOf_sim_postProcess_size) {
  s_hp_exportMatrix_t self;
  self.arrOf_sim_postProcess = __arrOf_sim_postProcess;
  self.arrOf_sim_postProcess_size = __arrOf_sim_postProcess_size;
  if(self.arrOf_sim_postProcess == NULL) {
    self.arrOf_sim_postProcess = arrOf_sim_postProcess;
    self.arrOf_sim_postProcess_size = arrOf_sim_postProcess_size;
  }
  self.isTo_writeOutTransposed = false; //! ie, the defualt interptation.
  //!
  //! @return
  return self;
}

static void __writeOutFacts_matrix__local(const s_hp_exportMatrix_t *obj_exportMatrix, s_kt_matrix_t mat_result, const char *resultPrefix, t_float *rowOf_summary) {
  assert(obj_exportMatrix);
  if(rowOf_summary) { //! then we comptue the average for each row, and udpate the latter vecotr.
    for(uint i = 0; i < mat_result.nrows; i++) {
      t_float sum = 0; uint cnt_interesting = 0;
      for(uint k = 0; k < mat_result.ncols; k++) {
	const t_float score = mat_result.matrix[i][k];
	if(isOf_interest(score)) {
	  cnt_interesting++; sum += score;
	}
      }
      if(cnt_interesting > 0) {
	if(sum != 0) {sum = sum / (t_float)cnt_interesting;}
	rowOf_summary[i] = sum;
      }
    }
  }

  

  //! ---------------------------------------------------
  //! 
  //! Write out the result: write out both the ranks and the deviaitons wrt. results.
  { //! Write out 'as-is'
    allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_%s.tsv", resultPrefix, "features");  //! ie, allcoate the "str_filePath".
    export__singleCall__s_kt_matrix_t(&mat_result, fileName_toExportTo, NULL);
  }
  { //! Write out for ranks:
    allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_%s.tsv", resultPrefix, "features_ranks");  //! ie, allcoate the "str_filePath".
    s_kt_matrix_t mat_ranks = initAndReturn_ranks__s_kt_matrix(&mat_result);
    export__singleCall__s_kt_matrix_t(&mat_ranks, fileName_toExportTo, NULL);
    free__s_kt_matrix(&mat_ranks);
  }
  { //! Write out wrt. normalizaiton:
    allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_%s.tsv", resultPrefix, "features_avgNormalization");  //! ie, allcoate the "str_filePath".
    s_kt_matrix_t mat_ranks = initAndReturn__copy__normalized__s_kt_matrix_t(&mat_result);
    export__singleCall__s_kt_matrix_t(&mat_ranks, fileName_toExportTo, NULL);
    free__s_kt_matrix(&mat_ranks);
  }
  { //! Write out wrt. deviaiotns:
    allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_%s.tsv", resultPrefix, "deviations");  //! ie, allcoate the "str_filePath".
    extractAndExport__deviations__singleCall__tsv__s_kt_matrix_t(&mat_result, fileName_toExportTo);
  }	  
  //!
  {//! Analyse the simlairty between diffenret feature-vector-combiantions .... where we compute 'STD-among-the-STDs' (for both diemsions wrt. our rsulst-matrix) ... and then isnert the results into our ...??... 
    //! Motivation. 

    for(uint sim_id = 0; sim_id < obj_exportMatrix->arrOf_sim_postProcess_size; sim_id++) {
      //s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t();
      //! Comptue the simlarity-metic.
      s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t();
      const bool is_ok = apply__simMetric_dense__extraArgs__hp_api_fileInputTight(obj_exportMatrix->arrOf_sim_postProcess[sim_id], &mat_result, NULL, &mat_result_simMetric);
      /* s_kt_matrix_base_t mat_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result); */
      /* s_kt_matrix_base_t mat_result_simMetric = initAndReturn__empty__s_kt_matrix_base_t(); */
      /* assert(mat_1.matrix); */
      /* const bool is_ok = apply__hp_distance(arrOf_sim_postProcess[sim_id], &mat_1, NULL, &mat_result_simMetric, init__s_hp_distance__config_t()); */
      assert(is_ok);

      //!
      //! Write otu the result:
      allocOnStack__char__sprintf(8000, fileName_toExportTo, "%s_sim_%s_%s.tsv", resultPrefix, get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(arrOf_sim_postProcess[sim_id].metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(arrOf_sim_postProcess[sim_id].typeOf_correlationPreStep));
      export__singleCall__s_kt_matrix_t(&mat_result_simMetric, fileName_toExportTo, NULL);
      //!
      //! De-allcoate:
      free__s_kt_matrix(&mat_result_simMetric);
    }
  }

  // FIXME: add support for [”elow] .... ie, after we have FIRST completed our elvuaiaotn of simlairty-emtrics .... and written an tiantla aritlce wrt. [bleow] ... 
  if(false) {
    //!
    assert(false); // FIXME[eval::conceptaul]:  ... new API for our "__writeOutFacts_matrix__local(..)" ... and aruge for how the altter may provide intereistng/novel fidnigns wrt. relationshisop ... udpate orua aritlce and orua rtilces scope ... 
    //! Evlauate centrality for outliers: comptue seperatley for 'improtant' and 'insicigant' cases, ie, where we for the latter 'invert' the 
    //! Motivation. ... to idneitfy extrem cases: to find both 'sigcant' and 'isngicant' vertices/relationhips. An example-use-case is to idneitfy the vertices with the hgihest degree of variation (ie, for which vertices with a lwo vairon-dgegree and high variation-degree is of importnace, an appraoch/train-of-though simliar to a large number of appraoches, eg, wrt. the Uffizi-appraoch). In order to elvuate the accurayc of using HCA (eg, in cotnrast to both a direct PCA-usage, and implcit use through software-tools such as Uffizi) we inpsect/evlauate/compare wrt.  ....??...
    //! Note: ocneptaully this appraoch is a pre-step wrt. a PCA-evlauation of HCA: we investigate/test the applcaiblity (ie, how well) HCA may be sued to derive/describe cetnral nodes/vertices (for a matrix of features). 
    for(uint isTo_invertScores = 0; isTo_invertScores < 2; isTo_invertScores++) {
      if(isTo_invertScores) { //! Invert the [ªbove] feature-simalrty-matrix
	assert(false); // FIXME: add somethign	... 
	assert(false); // FIXME: add somethign	... 
      }
      //!
      //! Iterate through differnet simlarity-metrics: use oru "hp_categorizeMatrices_syn.c" as a tmeplate:
      //! Motivation. To investigate simlairties in predicoitns between different data.sets.
      assert(false); // FIXME: add somethign	... 
      assert(false); // FIXME: add somethign	... 
      {
	//!
	//! Comptue simlarity-matrix and HCA-dynamic-clusters, both for the non-transpsoed ans transposed feature-matrix. Write out the lcuster-result to both TSV and java-script. 
	//! Motivation. 
	assert(false); // FIXME: add somethign	.... using oru "hp_categorizeMatrices_syn.c" as a template.
	
	//!
	//! insert the "sim-metic" simliarty-scores into a 'unifed' matrix ... and then 'in a psot-step' (after we have comptued for all of teh data-set-cases) use CCMs to evlauate seperaiton-hypotehsis wrt.: [factorMul, factor, simMetric, simMetric-gorup-broad, simMetric-group-narrow]
	//! Motivation. ...
	assert(false); // FIXME: add somethign
	
	//!
	//! ... apply HCA (on the data-set which 'hold' the STD-variaonts) ... find the most central vertices  (both wrt. sim-metrics and wrt. syn-data-sets) ... 'from' latter write out the ranks ... 
	//! Motivation. ... 
	assert(false); // FIXME: add somethign	
	
	
	//!
	//! HCA as a repalcement for PCA:
	//! Motivation
	//! Method. .... Our goal (ie, convergence-citeria) is to select the mincaml number of features which correctly amnages to describe 'the other' features, ie, to apply accurate/despcctive feature-seelction. In our method we apply the following steps: (a) select the hight-threshold in the PCA-tree, (b) construct a new subset-matrix from the 'feature-inptu-amtrix' (where we select features based on their height in combaiton with the height-threshold-ctieria, using the 'tranpsoed' verison of the amtrix we currently evlaute), (c) apply simliiarty-metric and dyanmic-HCA-kMeans, (d) re-comptue STD and CCM-metrics (where results are used to search/fidn/'reach' the cases with the highest degree of sepraiton). A challnge (wrt. latter approach) conserns the convegence-critera: to accuratly know when/how a feature-subset correctly catpure the core-traits of a data-set. The latter expalisn the mtoviation behind our CCM-post-evlauation-step: to find/idneityf the subset of features which identifes a clsuter approx. simliar to the appraoch/case where 'complete set of features are used'. Based on the latter observaiton we observe how our PCA-permtuation-aprpaoch may be used to: (a) accurately find a subset/sub-space of fetures (eg, the most influenctial genes/drivers in gene-expressions) and (b) reduce exeuciton-time (and increase result-accuracy) of iterative cluster-algorithms (eg, k-means-clustering through appliciaotn of our feature-reduction-appraoch) .... 
	assert(false); // FIXME: add somethign 
	assert(false); // FIXME: add somethign 
	assert(false); // FIXME: add somethign 
	
	
	//!
	//! 
	//! Motivation. 
	assert(false); // FIXME: add somethign	
	
	
	//!
	//!
	//! Motivation. 
	assert(false); // FIXME: add somethign	
      }
    }
  }
}



void writeOut__s_hp_exportMatrix_t(const s_hp_exportMatrix_t *obj_exportMatrix, s_kt_matrix_t mat_result, const char *resultPrefix, t_float *rowOf_summary, t_float *rowOf_summary_transposed, const bool isTo_writeOutTransposed) {
  { //! Foisr for the non-tanpsoed:
    __writeOutFacts_matrix__local(obj_exportMatrix, mat_result, resultPrefix, rowOf_summary);
  }
  if(isTo_writeOutTransposed) { //! Tehreafter tof the tranpsoed:
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_transposed", resultPrefix);
    //! TRanspose matrix:
    s_kt_matrix_t mat_transposed = initAndReturn_transpos__s_kt_matrix(&mat_result);
    __writeOutFacts_matrix__local(obj_exportMatrix, mat_transposed, resultPrefix_local, rowOf_summary_transposed);
    free__s_kt_matrix(&mat_transposed);
  }
}


static void export__s_kt_list_2d_kvPair__hp_exportMatrixClusterMemberships(const char *stringOf_resultFile, const s_kt_list_1d_uint_t map_vertexToClusterId, const s_kt_list_1d_string_t *strObj_1) {
	FILE *fileP_result = NULL;
	fileP_result = fopen(stringOf_resultFile, "w");
	if (fileP_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
	//!
	//! Export:
	s_kt_list_2d_uint_t map_clusterToVertex = setToEmpty__s_kt_list_2d_uint_t();	
	for(uint head_id = 0; head_id < map_vertexToClusterId.list_size; head_id++) {
	  //	  printf("%s\t, at %s:%d\n", strObj_1->nameOf_rows[head_id], __FILE__, __LINE__);
	  const uint cluster_id = map_vertexToClusterId.list[head_id];
	  if(cluster_id != UINT_MAX) { //! then add the clusters:
	    push__s_kt_list_2d_uint_t(&map_clusterToVertex, cluster_id, head_id);
	  }
	}
	uint cluster_id_local = 0;

	for(uint cluster_id = 0; cluster_id < map_clusterToVertex.list_size; cluster_id++) {
	  const uint cnt_tails = map_clusterToVertex.list[cluster_id].list_size;
	  //!
	  //! Write out the clusters: 
	  if(cnt_tails > 0) {
	    fprintf(fileP_result, "%s:%u\t", "cluster", cluster_id_local);
	    for(uint k = 0; k < cnt_tails; k++) {
	      const uint tail_id = map_clusterToVertex.list[cluster_id].list[k];
	      if(tail_id != UINT_MAX) {
		assert(tail_id != UINT_MAX); 
		if(tail_id >= strObj_1->nrows) {
		  fprintf(stderr, "!!\t tail=%u >= count=%u, at %s:%d\n", tail_id, strObj_1->nrows, __FILE__, __LINE__);
		} else {
		  assert(tail_id < strObj_1->nrows);	      
		  const char *str_2 = strObj_1->nameOf_rows[tail_id];
		  assert(fileP_result);
		  fprintf(fileP_result, "%s\t", str_2);
		  assert(str_2); assert(strlen(str_2));
		}
	      }
	    }
	    //! Increment clsuter-count:
	    fprintf(fileP_result, "\n");
	    cluster_id_local++;
	  }
	}
	if(true) { //! Then add the vertices which are Not addited to any cluster,s ie, to ease 'itnerptaiton' wr.t the data-set:
	  uint nrows = map_vertexToClusterId.list_size;
	  if(nrows < strObj_1->nrows) { //! then we write out a subset covering/cpatugint the latter:
	    nrows = strObj_1->nrows;
	  }
	  //	  printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
	  for(uint head_id = 0; head_id < nrows; head_id++) {
	    const uint cluster_id = (head_id < map_vertexToClusterId.list_size) ? map_vertexToClusterId.list[head_id] : UINT_MAX;
	    //	    printf("%s\t, w/cluster_id=%u, at %s:%d\n", strObj_1->nameOf_rows[head_id], cluster_id, __FILE__, __LINE__);
	    if(cluster_id == UINT_MAX) { //! then add the clusters:
	      //	      printf("(not-set)\t %s\t, w/cluster_id=%u, at %s:%d\n", strObj_1->nameOf_rows[head_id], cluster_id, __FILE__, __LINE__);
		const uint tail_id = head_id;
	      const char *str_2 = strObj_1->nameOf_rows[tail_id];
	      assert(fileP_result);
	      if(str_2 && strlen(str_2)) {		
		fprintf(fileP_result, "%s:%u\t", "cluster", cluster_id_local);
		assert(tail_id < strObj_1->nrows);	      
		
		fprintf(fileP_result, "%s\t", str_2);
		assert(str_2); assert(strlen(str_2));
		//! Increment clsuter-count:
		fprintf(fileP_result, "\n");
		cluster_id_local++;
	      }
	    }
	  }
	}
	//!
	//! De-allcoate: 
	free__s_kt_list_2d_uint_t(&map_clusterToVertex);
	//! Close:
	fclose(fileP_result);
}

//! Export the clsuter-emmbershisp to different reuslt-files (oekseth, 06. sep. 2017).
void export__s_kt_list_2d_kvPair__hp_exportMatrix(const char *stringOf_resultFile_prefix, s_kt_list_2d_kvPair_t obj_sparse, const s_kt_list_1d_string_t *strObj_1, const bool isTo_storeInMatrix) {
      /* char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); */
      /* sprintf(stringOf_resultFile, "%s.tsv", stringOf_resultFile_prefix); */
      //      assert(obj_sparse.current_pos > 0);
      //      const s_kt_list_1d_string_t *strObj_1 = &strMap_1; //! which is sued to simplify future geneirc changes ot the code, eg, to mvoe [”elow] to a new fucniton.
      s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
      char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
      sprintf(stringOf_resultFile, "%s.tsv", stringOf_resultFile_prefix);
      if(true) {printf("(info): export to result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__); }
      FILE *fileP_result = NULL;
      if(isTo_storeInMatrix) {
	const uint nrows = strObj_1->nrows;
	assert(nrows > 0);
	mat_result = initAndReturn__s_kt_matrix(nrows, nrows);
	//! Set the header--tail strings: 
	for(uint i = 0; i < nrows; i++) {
	  const char *str_1 = strObj_1->nameOf_rows[i];
	  if(str_1 && strlen(str_1)) {
	    //assert(str_1); assert(strlen(str_1));
	    set_stringConst__s_kt_matrix(&mat_result, i, str_1, /*addFor_column=*/false);
	    set_stringConst__s_kt_matrix(&mat_result, i, str_1, /*addFor_column=*/true);
	  }
	}
      } else {
	fileP_result = fopen(stringOf_resultFile, "w");
	if (fileP_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
      }
      //!
      //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      for(uint head_id = 0; head_id < obj_sparse.current_pos; head_id++) {
	uint cnt_tails = obj_sparse.list[head_id].current_pos;
	assert(cnt_tails <= obj_sparse.list[head_id].list_size); //! ie, as we otehriwse have an in-conssitnecy.
	if(cnt_tails > 0) {
	  const char *str_1 = strObj_1->nameOf_rows[head_id];
	  assert(str_1); assert(strlen(str_1));
	  if(isTo_storeInMatrix == false) {
	    assert(fileP_result != NULL);
	    fprintf(fileP_result, "%s\t", str_1);
	  }
	  const char *str_prev = NULL;
	  for(uint k = 0; k < cnt_tails; k++) {
	    const s_ktType_kvPair_t obj_this = obj_sparse.list[head_id].list[k];
	    if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	      const uint tail_id = obj_this.head;
	      assert(tail_id != UINT_MAX); 
	      if(tail_id >= strObj_1->nrows) {
		fprintf(stderr, "!!\t tail=%u >= count=%u, at %s:%d\n", tail_id, strObj_1->nrows, __FILE__, __LINE__);
	      } else {
		assert(tail_id < strObj_1->nrows);
		if(isTo_storeInMatrix == false) {
		  const char *str_2 = strObj_1->nameOf_rows[tail_id];
		  if(str_2 && strlen(str_2) ) {
		    assert(str_2); assert(strlen(str_2));	    
		    //{
		    if(!str_prev ||
		       (
			(strlen(str_2) != strlen(str_prev)) ||
			(0 != strcmp(str_2, str_prev) ) ) ) {
		      fprintf(fileP_result, "%s\t", str_2);
		    } //! else we assume the relationship has already been written out. 
		    str_prev = str_2;
		  }
		} else { //! then we store result into amtrix:
		  const t_float score_pair = obj_this.tail;
		  assert(head_id != UINT_MAX);
		  assert(mat_result.nrows != 0);
		  assert(head_id < mat_result.nrows);
		  assert(tail_id < mat_result.ncols);
		  mat_result.matrix[head_id][tail_id] = score_pair;
		}
	      }
	    }
	  }
	  if(isTo_storeInMatrix == false) {
	    fprintf(fileP_result, "\n");
	  }
	}
      }    
      //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      if(isTo_storeInMatrix == true) { //! then we write out the result: 
	export__singleCall__s_kt_matrix_t(&mat_result, stringOf_resultFile, NULL);
	free__s_kt_matrix(&mat_result);
      } else { //! then close the file-pointer: 
	//! Close:
	assert(fileP_result);
	fclose(fileP_result); fileP_result = NULL;
      }
      //! ***********************************************************************************************
      //!
      //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      { //! Idnetify disjtoint clsuters for the latter set (uisng our DB-SCAN-permtuation-algorithm):
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s.clusterPartitions_dbScanVersion.tsv", stringOf_resultFile_prefix);
	if(true) {printf("(info): export to result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__); }
	FILE *fileP_result = NULL;
	fileP_result = fopen(stringOf_resultFile, "w");
	if (fileP_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
	//!
	//! Identify clsuters: 
	s_kt_list_1d_uint_t map_vertexToClusterId = search__relationsFromDataStruct__kd_tree(&obj_sparse);
	//! Write out:
	export__s_kt_list_2d_kvPair__hp_exportMatrixClusterMemberships(stringOf_resultFile, map_vertexToClusterId, strObj_1);
	//!
	//! De-allcoate: 
	free__s_kt_list_1d_uint_t(&map_vertexToClusterId);
	//! Close:
	fclose(fileP_result);
      }
      //! ***********************************************************************************************
      //!
      { //! Idnetify disjtoint clsuters for the latter set (uisng our hpLysis-impemtantion of disjoitn-sets):
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s.clusterPartitions_hpLsysisAlgVersion.tsv", stringOf_resultFile_prefix); //, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	if(true) {printf("(info): export to result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__); }

	//!
	//! Identify clsuters: 
	s_kt_list_1d_uint_t map_vertexToClusterId = search__relationsFromDataStruct__altVersion__kd_tree(&obj_sparse);
	//! Write out:
	export__s_kt_list_2d_kvPair__hp_exportMatrixClusterMemberships(stringOf_resultFile, map_vertexToClusterId, strObj_1);
	//	export__s_kt_list_2d_kvPair__hp_exportMatrixClusterMemberships(&map_vertexToClusterId);
	//!
	//! De-allcoate: 
	free__s_kt_list_1d_uint_t(&map_vertexToClusterId);
      }
}
