#include "kt_randomGenerator_vector.h"
# ifndef not_include_headerFiles
#include "kt_set_2dsparse.h" //! ie, a 2d-stack.
#include "hp_distance.h"
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).

//! @return an inaited s_kt_randomGenerator_vector__config_t object
s_kt_randomGenerator_vector__config_t init__s_kt_randomGenerator_vector__config_t(const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks) {
  s_kt_randomGenerator_vector__config_t self;
  self.typeOf_randomNess = typeOf_randomNess;   
  self.isTo_initIntoIncrementalBlocks = isTo_initIntoIncrementalBlocks;
  //! 
  //! Handle specific cases:
  if(self.typeOf_randomNess == e_kt_randomGenerator_type_undef) {self.typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault;} //! ie, the defulat apprahc
  self.obj_metric = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1);
  self.n_local_trials = UINT_MAX;  
  //! @return
  return self;
}


//! Initliazes an object used to idnetyf a subset of cases to evluate (oekseth, 06. jan. 2017).
s_kt_randomGenerator_vector_t initAndReturn__s_kt_randomGenerator_vector_t(const uint nclusters, const uint nelements, const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks, s_kt_matrix_base_t *matrix_input) { //; //! which if set is assumed tor epresent an inptu-matrix.) {
  s_kt_randomGenerator_vector_t self;
  self.__counter__cntRandomCalls = 0;
  self.config_init = init__s_kt_randomGenerator_vector__config_t(typeOf_randomNess, isTo_initIntoIncrementalBlocks);
  self.config = init__s_kt_randomGenerator_vector__config_t(typeOf_randomNess, isTo_initIntoIncrementalBlocks);
  self.nclusters = nclusters;
  self.nelements = nelements;
  self.mapOf_columnToRandomScore = NULL;
  if(nelements > 0) {
    assert(nelements != UINT_MAX);
    const uint empty_0 = 0;
    self.mapOf_columnToRandomScore = allocate_1d_list_uint(nelements, empty_0);
    assert(self.mapOf_columnToRandomScore);
    self.mapOf_columnToRandomScore[0] = UINT_MAX; //! ie, 'mark' the vertex as not-intiated.
  }

  self.matrix_input = matrix_input; //! which if set is assumed tor epresent an inptu-matrix.
  //! @return
  return self;
}

//! Initalise the s_kt_matrix_base_t object to defualt settings, and then return.
s_kt_randomGenerator_vector_t initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t() {
  return initAndReturn__s_kt_randomGenerator_vector_t(
						      /*nclusters=*/0,
						      /*nelements=*/0,
						      /*typeOf_randomNess=*/e_kt_randomGenerator_type_undef,
						      /*isTo_initIntoIncrementalBlocks=*/false,
						      /*matrix_input=*/NULL
						      );
}

//! Note the funciton used in "driver.cpp" for the "kemans++"

/**
   @remarks the "kmeans++" fast_kmeans appraoch:
   Note(algoritm): similar to "scikit-learn::step(4.b)" if "n_local_trials=1", eg, as exmpalifed in step(2.2) in [”elow] where 'this implemetantion' only evalutes/considers the case of/where 'a centrodi may alreayd have beens selected'
   step(1): select 'at random' the centorid for the first cluster (ie, simliar to scikit-approach);
   step(2): Loop: pick the 'remaining clusters', ie, as we are 'only' interested in fidnign candidate/intital/'potential' clsuters for the "nclusters" case: 
   step(2.1): Loop[for(vertex, features)]: for each vertex ivnestgiate if the  the [i, centroid(current)]  hold/represent/describe a longer/higher distance (when compared to  'pervious/curretn cluster-centroid':) in "fast_kmeans" the Minkowski-distance is used wrt. the similarty-emtric; 
   step(2.1.note): similar to "scikit-learn::step(4.b)";
   step(2.2): Loop(while): 
   step(2.2.note): similar to "scikit-learn::step(4.c)";
   step(2.2.1): select at random a dsitribution-trehshold "r"; 
   step(2.2.2): idneitfy the number of vertices "index_next = cdf_index_next" with a sum('max-distance-to-centroids') which are less than "r"; 
   step(2.2.3): set "unique = false" if "chosen_pts[nds == chosen_pts[i < |clusters|]"
   step(2.2.end): if "unique == true" compelt ehf rot-loop: otherwise continue until the vertex is found.
   step(3): Loop(for(rows, clusters, features)): use a {simliarty-emtirc} to speifyc/idnietyf the cluster-centroid-vertex which each vertex 'is closest to' (ie, the 'reverse approach' of the 'find-centoid-appraoch' in 'ordinary' k-means-algorithms). 
   step(3.note):  exactly similar to "scikit-learn::step(5)";
   step(end): return the "centers" array.
**/
# ifndef not_include_headerFiles
static bool __fastCluster__init_centers_kmeanspp__s_kt_matrix_base_t(s_kt_matrix_base_t *matrix_input, const uint k_clusterCount, const bool isTo__useUniformNumbers__inSleeciton, uint *chosen_pts) {
  assert(matrix_input); assert(k_clusterCount);
//Dataset *init_centers_kmeanspp_v2(Dataset const &x, unsigned short k) {
  //const uint empty_0 = 0;
  //uint *chosen_pts = allocate_1d_list_uint(k_clusterCount, empty_0); //new int[k];
  struct __pair { t_float first; uint second;};
  struct __pair *dist2 = NULL;
  alloc_generic_type_1d_xmtMemset(__pair, dist2, matrix_input->nrows);
  assert(dist2);
  //std::pair<double, int> *dist2 = new std::pair<double, int>[x.n];

  // initialize dist2
  for (int i = 0; i < matrix_input->nrows; ++i) {
    dist2[i].first = T_FLOAT_MAX; //std::numeric_limits<double>::max();
    dist2[i].second = i;
  }
  const uint nrows = matrix_input->nrows;


  uint index_next = 1;
  //! ------------------------------------------
  //! 
  { //! Idneitfy 'at random' the first cluster-cneotrid-vertex:
    uint rand_val = 0;
    if(isTo__useUniformNumbers__inSleeciton) {
      const t_float p = 1.0/k_clusterCount;
      rand_val = math_generateDistribution__binomial(nrows, p);
      assert_possibleOverhead(rand_val < nrows);
    } else { rand_val = rand() % nrows;} //! where 'this approach' will bw skwed towards lwo numbers, ie, 'deu to' the 'mdouloo-based appraoch'.
    chosen_pts[index_next - 1] = rand_val;
  }

  //!
  //! Iterate throught eh reamdingin set of clsuters:
  while (index_next < k_clusterCount) {
    t_float sum_distribution = 0.0;
    const uint row_out = chosen_pts[index_next - 1];
    assert(row_out < nrows);
    const t_float *row_2 = matrix_input->matrix[row_out];
    assert(row_2);
/* #ifndef NDEBUG */
/*     //! Valdiate the the row has at elast one interesting vertex: */
/*     uint cnt_interesting = 0; */
/*     for (uint j = 0; j < matrix_input->ncols; ++j) { */
/*       const t_float score_2 = row_2[j]; */
/*       cnt_interesting += isOf_interest(score_2); */
/*     } */
/*     assert(cnt_interesting > 0); */
/* #endif */

    //!
    //! Find the point vertex/point farthest away from the 'current' cluster-center "row_2":
    t_float max_dist = T_FLOAT_MIN_ABS;
    for (uint i = 0; i < nrows; ++i) {
      const uint index_sample = dist2[i].second;
      assert_possibleOverhead(index_sample != UINT_MAX);
      assert_possibleOverhead(index_sample < nrows);
      t_float d2 = 0.0, diff;
      const t_float *row_1 = matrix_input->matrix[index_sample];
      assert(row_1);
      for (uint j = 0; j < matrix_input->ncols; ++j) {
	const t_float score_1 = row_1[j];
	const t_float score_2 = row_2[j];
	if(isOf_interest(score_1) && isOf_interest(score_2)) {
	  diff = score_1 - score_2;
	  d2 += diff * diff;
	}
      }
      // TODO[perf]: consider to extent && test differnet simliarty-metrics ... eg, to idnetify the 'vertex closest to "row_2" .... ie, usign the simlairty-metrics in Knitting-Toosl 2kt_distance.h".
      
      //!
      //! Update the sum-properties:
      if (d2 < dist2[i].first) { dist2[i].first = d2; }      
      if (dist2[i].first > max_dist) { max_dist = dist2[i].first; }      
      sum_distribution += dist2[i].first;
    }
    
    //!
    //! Identify a 'center-point' which has Not been earlier selected/used:
    bool unique = true;    
    const uint max_cntIterations = nrows * nrows; // TODO: cosnider using a different threshold
    uint cnt_iterations = 0;
    do {
      //! Use the enw-found distribution: Idenitfy a treshhold which is 'inside' the current 'sum of distance' (ie, the current dsitributioon):
      const t_float r = sum_distribution * (t_float)rand() / (t_float)RAND_MAX;
      t_float sum_cdf = dist2[0].first;
      uint cdf_index_next = 0;
      while (sum_cdf < r) {
	sum_cdf += dist2[cdf_index_next++].first;
	if(cdf_index_next >= nrows) {
	  sum_cdf = r;  //! ie, to 'prokke' the break-trheshold.
	  cdf_index_next--; //! ie, to 'esnrue' the count-threshold to be inside th e 'allcoated space'.
	}
      }
      chosen_pts[index_next] = cdf_index_next;      
      //!
      //! Investigate uniqueness:
      for (uint i = 0; i < index_next; ++i) {
	unique = unique && (chosen_pts[index_next] != chosen_pts[i]);
      }
      if(unique) { //! then valdite that the couln has interesting relationships/values:
	uint cnt_interesting = 0;
	const t_float *__restrict__ row_1 = matrix_input->matrix[cdf_index_next];
	for (uint j = 0; j < matrix_input->ncols; ++j) {
	  cnt_interesting += isOf_interest(row_1[j]);
	}
	unique = (cnt_interesting > 0);
      }
      assert(cnt_iterations <= max_cntIterations);
      cnt_iterations++;
    } while ( (unique == false) && (cnt_iterations < max_cntIterations) );
    //!    
    if(unique == false) {
      assert_possibleOverhead(cnt_iterations >= max_cntIterations);
      //! Then we need to 'add' a manual iteraiton-appraoch wrt. the 'assignment':
      // TODO[article]: update our reseharc-aritlce wr.t [ªbov€] observiaton ... ie, that the "fast_kmeans" (and simalrity the "cluster.c" lbirary) is Not deisgned/cosntucted to handløe 'worst-case random csnevarios ... where an 'acceptable index' is 'nver' found (oekseth, 06. feb. 2017).
      
      for(uint row_id = 0; (row_id < nrows) && (unique == false); row_id++) {
	bool isFound_local = false;
	for (uint j = 0; j < index_next; ++j) {
	  if(chosen_pts[j] == row_id) {isFound_local = true;}
	}
	if(isFound_local == false) { //! then a 'unique poitn' is found:
	  chosen_pts[index_next] = row_id;
	  unique = true;
	}
      }
    }
      
    index_next++;
  }  
  //!
  //! De-allcoate:
  free_generic_type_1d(dist2); dist2 = NULL;
  //!
  //! @return:
  return true;
}
  /**
     @remarks 
     @remarks the "kmeans++" scikit-learn-appraoch ("https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/cluster/k_means_.py"); "Selects initial cluster centers for k-mean clustering in a smart way to speed up convergence". see: Arthur, D. and Vassilvitskii, S."k-means++: the advantages of careful seeding". ACM-SIAM symposium on Discrete algorithms. 2007;
     step(1): select 'at random' the centorid for the first cluster (ie, simliar to fast_kmeans);
     step(2): compute distance: Euclid(centroid(cluster=0) x "matrix"): .... and then compute/construct a "current_pot" = sum(Euclid(centroid(cluster=0), "Matrix")). 
     step(3): Set (if not alreayd set) the number of seeding trials/iteration-cases to investigate (a work/appraochspecifially applied/used in "Arthur/Vassilvitskii"): (n_local_trials == UINT_MAX) {n_local_trials = 2 + (uint)log(|clusters|);}. 
     step(4): Loop: pick the 'remaining clusters', ie, as we are 'only' interested in fidnign candidate/intital/'potential' clsuters for the "nclusters" case: 
     step(4.a): Select cadnidate-center-vertices to investigate: 
     step(4.a.1): build a vector "rand_vals" holding "n_local_trials" values selected at random: "rand_vals = random_state.random_sample(n_local_trials) * current_pot":
     step(4.a.2): Copmtue the 'cumliaritve sum' of elements/scores $\sum_{i ... j}$ wrt. the "closest_dist_sq" updated in our "step(4.c.end)": the correctnes (of/to use "cumulative sum of elements" (ie, "cumsum(..)") to represent/describe the distnace to the center) is based on the observaiton that we 'need' a 'sink' to denote the dsitance between the 'randomly selected vertices', ie, hence the use [0] as a 'sink' <-- todo: try improving the latter descption.        
     step(4.a.3): Identify a set of candiate-centoid-ids: find the vertices which are closest to the randomly constructed/idneitfed/hyptohesed distnace-thresholds/center-points: "candidate_ids = np.searchsorted(stable_cumsum(closest_dist_sq), rand_vals)";
     step(4.b): Find the distance betwen the 'candiate-centroid-ids' and input-matrix, ie, apply/call a "many-to-many-distance-call" between "X[candidate_ids] x X"; 
     step(4.b.1) build a sub-matrix based on the randomly selected values:
     step(4.b.2) apply an all-agaisnt-all-comparison;
     step(4.c): Loop: Idneitfy the best-scoring center-dandidate: iterate through each of the candate-centers and sleect the minimium/shortest pair: 
     step(4.c.init): init: intiate/sepcify local variables to hold the min-distance wrt. the dcandiates 'to investigate' in the for-loop: "best_candidate = null, best_pot = null, best_dist_sq = null";
     step(4.c.1): 'merge' a global-distance-vector and each 'local-dsitance-vector': for each centorid-vertex comptue a vector holdn the 2localMinVector = [min-score] = [closest_dist_sq[vertex=i], distance_to_candidates[trial][vertex=i]"
     step(4.c.2): compute "new_pot = sum(localMinVector)"
     step(4.c.3): Update the "best_pot, best_candidate, best_dist_sq" if "new_pot < best_pot"
     step(4.c.end): Set "centers[c] = X[best_candidate]; current_pot = best_pot; closest_dist_sq = best_dist_sq";
     step(5): Loop(for(rows, clusters, features)): use a {simliarty-emtirc} to speifyc/idnietyf the cluster-centroid-vertex which each vertex 'is closest to' (ie, the 'reverse approach' of the 'find-centoid-appraoch' in 'ordinary' k-means-algorithms). 
   **/
static bool ____init_centers_pySciKitLearn__kmeanspp__s_kt_matrix_base_t(const s_kt_randomGenerator_vector__config_t config, s_kt_matrix_base_t *matrix_input, const uint k_clusterCount, const bool isTo__useUniformNumbers__inSleeciton, uint *chosen_pts, uint n_local_trials) {
  assert(matrix_input); assert(k_clusterCount); 
  const s_kt_correlationMetric_t obj_metric = config.obj_metric;
  //! ------------------------------------------
  //! 
  const uint nrows = matrix_input->nrows;
  { //! Idneitfy 'at random' the first cluster-cneotrid-vertex:
    uint rand_val = 0;
    if(isTo__useUniformNumbers__inSleeciton) {
      const t_float p = 1.0/k_clusterCount;
      rand_val = math_generateDistribution__binomial(nrows, p);
      assert_possibleOverhead(rand_val < nrows);
    } else { rand_val = rand() % nrows;} //! where 'this approach' will bw skwed towards lwo numbers, ie, 'deu to' the 'mdouloo-based appraoch'.
    chosen_pts[0] = rand_val;
  }

  //! step(2): compute distance: Euclid(centroid(cluster=0) x "matrix"): .... and then compute/construct a "current_pot" = sum(Euclid(centroid(cluster=0), "Matrix")). 
  const t_float empty_0 = 0;
  s_kt_matrix_base_t obj_closest_dist_sq = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/nrows); 
  //s_kt_matrix_base_t obj_closest_dist_sq = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/1); //! where ncols=1 as we assume 'the result-row is to be condensed into one scalar' (oekseht, 06. mar. 2017)
  { //! Compute the distance from vertex[0]
    assert(obj_closest_dist_sq.matrix);
    s_hp_distance__config_t local_config_init = init__s_hp_distance__config_t();
    //printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
    local_config_init.obj_1_index1 = chosen_pts[0]; //! ie, as we comptue "[0] x [all]" for "matrix_input".
    assert(local_config_init.obj_1_index1 < matrix_input->nrows);
    // local_config_init.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index; //! ie, to idneitfy the 'closest vertex'
    const bool is_ok = apply__hp_distance(obj_metric, /*obj_1=*/matrix_input, /*obj_2=*/matrix_input, /*obj_result=*/&obj_closest_dist_sq, local_config_init);
    assert(is_ok);
  }
  t_float *closest_dist_sq = obj_closest_dist_sq.matrix[0]; //allocate_1d_list_float(nrows, empty_0);
  t_float current_pot = 0; for(uint i = 0; i < nrows; i++) {if(isOf_interest(closest_dist_sq[i])) {current_pot += closest_dist_sq[i];}} //! ie, the sum of distances to the center.
  if(current_pot == 0) {current_pot = 1;} // TODO[eval]: evlauate correctness of this 'handling-appraoch' .... (oekseth, 06. jun. 2017)
  //assert(current_pot != 0);
  //! step(3): Set (if not alreayd set) the number of seeding trials/iteration-cases to investigate (a work/appraochspecifially applied/used in "Arthur/Vassilvitskii"): (n_local_trials == UINT_MAX) {n_local_trials = 2 + (uint)log(|clusters|);}. 
  if( (n_local_trials == 0) || (n_local_trials > nrows)) {
    //! Note: the [below] appraoch is sinpdred by teh "scipt appraoch"
    // TODO[perf]: wrttie an exaluation-example investgiating the effect of dfiferent appraoches wrt [below].
    n_local_trials = 2 + (uint)mathLib_float_log((t_float)k_clusterCount);
  }


  //! step(4): Loop: pick the 'remaining clusters', ie, as we are 'only' interested in fidnign candidate/intital/'potential' clsuters for the "nclusters" case: 
//! Note[ok]: in below we pick the 'remaining clusters', ie, as we are 'only' interested in fidnign candidate/intital/'potential' clsuters for the "nclusters" case.
  uint index_next = 1;
  for(uint k_index = 1; k_index < k_clusterCount; k_index++) {    
    uint *candidate_ids = allocate_1d_list_uint(n_local_trials, empty_0);
    { //! step(4.a): Select cadnidate-center-vertices to investigate:       
      //! step(4.a.1): build a vector "rand_vals" holding "n_local_trials" values selected at random: "rand_vals = random_state.random_sample(n_local_trials) * current_pot":
      const t_float empty_0 = 0;
      t_float *list_randVals = allocate_1d_list_float(n_local_trials, empty_0);
      /* { //! Initaite the list with candaite-centrodis which have Not been earlier selected: */
      /* 	// TODO[article]: udpate our airtlce wtih 'this' ... ie, as the scikit_learn appraoch 'may result in a centorid being the centoid of mulipe clsuters', ie, in cotnast to the "fast_kmeans" which hadnle the latter issue. */
      /* 	uint n_local_trials__current = 0; */
      /* 	for(uint row_id = 0; (row_id < nrows) && (n_local_trials__current < n_local_trials); row_id++) { */
      /* 	  assert_possibleOverhead(current_clusterId < k_index); */
      /* 	  bool is_found = false; */
      /* 	  for(uint clus = 0; (clus < k_index) && (is_found == false); clus++) { */
      /* 	    if(chosen_pts[clus] == row_id) {is_found = true;} */
      /* 	  } */
      /* 	  if(is_found == false) { */
      /* 	    list_randVals[n_local_trials__current] = row_id; */
      /* 	    n_local_trials__current++; */
      /* 	  } */
      /* 	} */
      /* } */
      for(uint k = 0; k < n_local_trials; k++) {
	//! Idneitfy 'at random' the first cluster-cneotrid-vertex:
	t_float rand_val = 0;
	if(isTo__useUniformNumbers__inSleeciton) {
	  t_float local_adjustment = 0.5 - 0.5*((k+1.0)/n_local_trials);
	  assert(local_adjustment > 0); 	  assert(local_adjustment < 1);
	  const t_float p = local_adjustment; //1.0/local_adjustment;
	  rand_val = math_generateDistribution__binomial(nrows, p);
	  assert_possibleOverhead(rand_val < nrows);
	} else { rand_val = rand() % nrows;} //! where 'this approach' will bw skwed towards lwo numbers, ie, 'deu to' the 'mdouloo-based appraoch'.
	//! Set the random-generated threshold:
	list_randVals[k] = rand_val*current_pot;
      }
      //! step(4.a.2): Copmtue the 'cumliaritve sum' of elements/scores $\sum_{i ... j}$ wrt. the "closest_dist_sq" updated in our "step(4.c.end)": the correctnes (of/to use "cumulative sum of elements" (ie, "cumsum(..)") to represent/describe the distnace to the center) is based on the observaiton that we 'need' a 'sink' to denote the dsitance between the 'randomly selected vertices', ie, hence the use [0] as a 'sink' <-- todo: try improving the latter descption.        
      assert(closest_dist_sq);
      t_float *list_cumSum = allocate_1d_list_float(nrows, empty_0);
      list_cumSum[0] = closest_dist_sq[0];
      for(uint i = 1; i < nrows; i++) {
	/* t_float sum = 0; */
	/* for(uint m = 0; m <= i; m++) { */
	/*   if(isOf_interest(closest_dist_sq[m])) { */
	/*     sum += list_cumSum[m]; */
	/*   } */
	/* } */
	list_cumSum[i] = closest_dist_sq[i] + list_cumSum[i-1]; //! ie, the sum of elmenets.
	// FIXME: validate correnctess of [ªbove] 'cum-sum' prefix-sum.
      }

      //! step(4.a.3): Identify a set of candiate-centoid-ids/indexes: find the vertices which are closest to the randomly constructed/idneitfed/hyptohesed distnace-thresholds/center-points: "candidate_ids = np.searchsorted(stable_cumsum(closest_dist_sq), rand_vals)";
      assert(n_local_trials < 500); //! ie, as we otehrwise could consider a sort-appraoch to [”elow]:
      //! Note: the [”elow] logics are intended to 'reflect' the NumPy logics: ">>> np.searchsorted([1,2,3,4,5], [-10, 10, 2, 3]) => array([0, 5, 1, 2])" .... for each "search-array" find the index with the closet difference/simliarty 
      for(uint k = 0; k < n_local_trials; k++) { //! ie, iterate through the randomzly gneerated values:
	const t_float search_value = list_randVals[k];
	assert(isOf_interest(search_value)); //! ie, as we otehrwise need adding support for this case.
	uint max_index = 0; //! ie, 'find the biggest index' which search_value is greater than
	for(uint i = 0; i < nrows; i++) {
	  const t_float score = list_cumSum[k];
	  //if(isOf_interest(score)) { 
	  // TODO[article]: udpate our airtlce wtih 'this' "is_found" approach ... ie, as the scikit_learn appraoch 'may result in a centorid being the centoid of mulipe clsuters', ie, in cotnast to the "fast_kmeans" which hadnle the latter issue. 	  
	  bool is_found = false; 
	  for(uint clus = 0; (clus < k_index) && (is_found == false); clus++) {
	    if(chosen_pts[clus] == /*row_id=*/i) {is_found = true;}
	  }
	  if(!is_found && (score < search_value)) {
	    max_index = i;
	  }
	}
	candidate_ids[k] = max_index; //! ie, update.
      }      
      //!
      //! De-allcoate lcoally allcoated variables:
      free_1d_list_float(&list_randVals); list_randVals = NULL;
      free_1d_list_float(&list_cumSum); list_cumSum = NULL;
    }
    s_kt_matrix_base_t distance_to_candidates = initAndReturn__empty__s_kt_matrix_base_t();
    { //! step(4.b): Find the distance betwen the 'candiate-centroid-ids' and input-matrix, ie, apply/call a "many-to-many-distance-call" between "X[candidate_ids] x X"; 
      //! Note: teh 'sckit-py-appraoch' of [”elow] is: distance_to_candidates = euclidean_distances(X[candidate_ids], X, Y_norm_squared=x_squared_norms, squared=True)
      //! step(4.b.1) build a sub-matrix based on the randomly selected values:
      s_kt_matrix_base_t mat_seeds = initAndReturn__fromSubset__s_kt_matrix_base_t(matrix_input, candidate_ids, n_local_trials); //! ie, the subset of feature-vectors.
      assert(mat_seeds.nrows > 1); assert(mat_seeds.ncols > 1);
      assert(mat_seeds.ncols == matrix_input->ncols);
      //! 
      //! step(4.b.2) apply an all-agaisnt-all-comparison: 
      const s_hp_distance__config_t local_config = init__s_hp_distance__config_t();
      /* local_config.obj_1_index1 = chosen_pts[0]; //! ie, as we comptue "[0] x [all]" for "matrix_input". */
      /* assert(local_config.obj_1_index1 < matrix_input->nrows); */
      const bool is_ok = apply__hp_distance(obj_metric, /*obj_1=*/&mat_seeds, /*obj_2=*/matrix_input, /*obj_result=*/&distance_to_candidates, local_config);
      assert(distance_to_candidates.nrows == n_local_trials);
      assert(distance_to_candidates.ncols == nrows); //! ie, as we expect to have comptued [seeds] x [input-matrix]
      assert(is_ok);
      //assert(distance_to_candidates.ncols == obj_closest_dist_sq.ncols);
      //!
      //! De-allcoate lcoally allcoated variables:
      free__s_kt_matrix_base_t(&mat_seeds);
    }    
    { //! step(4.c): Loop: Idneitfy the best-scoring center-dandidate: iterate through each of the candate-centers and sleect the minimium/shortest pair: 
      //! step(4.c.init): init: intiate/sepcify local variables to hold the min-distance wrt. the dcandiates 'to investigate' in the for-loop: "best_candidate = null, best_pot = null, best_dist_sq = null";
      t_float best_pot = T_FLOAT_MAX; uint best_pot__index = 0;
      t_float *list__localMinVector = allocate_1d_list_float(distance_to_candidates.ncols, empty_0);
      t_float *list__localMinVector__best = allocate_1d_list_float(distance_to_candidates.ncols, empty_0);
      const t_float *row_1 = obj_closest_dist_sq.matrix[0];
      assert(row_1);
      for(uint k = 0; k < n_local_trials; k++) { //! ie, iterate through the randomzly gneerated values:
	//! step(4.c.1): 'merge' a global-distance-vector and each 'local-dsitance-vector': for each centorid-vertex comptue a vector holdn the 2localMinVector = [min-score] = [closest_dist_sq[vertex=i], distance_to_candidates[trial][vertex=i]":
	//! step(4.c.2): compute "new_pot sum_local = sum(localMinVector)":
	t_float sum_local = 0;
	const t_float *row_2 = distance_to_candidates.matrix[k];	
	assert(row_2);
	//!
	//! Find the 'max(..)' for each row:
	for(uint col_id = 0; col_id < distance_to_candidates.ncols; col_id++) {
	  const t_float score_1 = row_1[col_id];
	  const t_float score_2 = row_2[col_id];
	  if(isOf_interest(score_1) && isOf_interest(score_2)) {
	    list__localMinVector[col_id] = macro_min(score_1, score_2);
	    // TODO[artilce]: .. the "scikit_learn" lbirary does not 'handle [”elow ] case ... ie, a nnvoely of our appraoch ... therefore try/consider updating our aitlce-text wrt. [”elow] 'noveltly' (oekseth, 06. feb. 2017).
	    sum_local += mathLib_float_abs(list__localMinVector[col_id]); //! ie, to 'handløe' neagive values.
	  }
	}
	//assert(sum_local != 0); // TODO: cosndier removing 'this'.:
	//! step(4.c.3): Update the "best_pot, best_candidate, best_dist_sq" if "new_pot < best_pot":
	if(sum_local < best_pot) {
	  best_pot = sum_local;
	  best_pot__index = k;
	  for(uint col_id = 0; col_id < distance_to_candidates.ncols; col_id++) {
	    list__localMinVector__best[col_id] = list__localMinVector[col_id];
	  }
	}
      }
      //! 
      //! step(4.c.end): Set "chosen_pts = centers[c] = X[best_candidate]; current_pot = best_pot; closest_dist_sq = best_dist_sq";
      chosen_pts[k_index] = candidate_ids[best_pot__index];
      assert(chosen_pts[k_index] < nrows);
      current_pot = best_pot;
      for(uint col_id = 0; col_id < distance_to_candidates.ncols; col_id++) {
	closest_dist_sq[col_id]  = list__localMinVector__best[col_id];
      }
      //!
      //! De-allocate locally reserved memory:
      free_1d_list_float(&list__localMinVector__best); list__localMinVector__best = NULL;
      free_1d_list_float(&list__localMinVector); list__localMinVector = NULL;
    }
    //!
    //! De-allcoate
    free_1d_list_uint(&candidate_ids); candidate_ids = NULL;
    free__s_kt_matrix_base_t(&distance_to_candidates);
  }
  free_1d_list_float(&closest_dist_sq); closest_dist_sq = NULL;

  //!
  //! @return:
  return true;
}

#else 
#pragma warn ("option not supported")
//#pragma GCC warning ("option not supported")
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).

//! De-allocates the s_kt_randomGenerator_vector_t object (oekseth, 06. jan. 2017)
void free__s_kt_randomGenerator_vector_t(s_kt_randomGenerator_vector_t *self) {
  if(self->mapOf_columnToRandomScore) {free_1d_list_uint(&(self->mapOf_columnToRandomScore)); self->mapOf_columnToRandomScore = NULL;}
}

# ifndef not_include_headerFiles
static void __investigateAndHAndle__caseWhereNotAllClustersAreSet(uint *map_clusterid, const uint map_clusterid_size, const uint nclusters) {
  assert(nclusters > 1);   assert(nclusters < map_clusterid_size); assert(map_clusterid); assert(map_clusterid_size > 0); assert(map_clusterid_size != UINT_MAX); assert(nclusters != UINT_MAX);
  // TODO[article]: update our artilce-test wrt. the nvoelty of 'this appraoch' ... ie, a 'case not handled by clsuter-software such as "clsuter.c" and "fast_kmeans"
  const uint empty_0 = 0;
  //!
  //! Intiate:
  s_kt_set_2dsparse_t stack2d;
  allocate__s_kt_set_2dsparse_t(&stack2d, /*nrows=*/nclusters, /*ncols-init=*/20, /*matrix=*/NULL, NULL, false, /*isTo_allocateFor_scores=*/false);
  uint *map_clusterToCentroid = allocate_1d_list_uint(nclusters, empty_0);
  //uint *map_clusterToCentroid_2 = allocate_1d_list_uint(nclusters, empty_0);
  for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
    map_clusterToCentroid[cluster_id] = UINT_MAX;
    //map_clusterToCentroid_2[cluster_id] = UINT_MAX;
  }
  //!
  //! Identify the cases which have 'mroe than one centroid-assignment':
  for(uint row_id = 0; row_id < map_clusterid_size; row_id++) {
    assert(map_clusterid[row_id] != UINT_MAX);
    const uint cluster_id = map_clusterid[row_id];
    assert(cluster_id != UINT_MAX);
    assert(cluster_id < nclusters);
    // fprintf(stderr, "\t row_id[%u]=%u, at %s:%d\n", row_id, cluster_id, __FILE__, __LINE__);
    if(map_clusterToCentroid[cluster_id] != UINT_MAX) {
      push__s_kt_set_2dsparse_t(&stack2d, /*row=*/cluster_id, /*vertex=*/row_id); //! ie, the 'candiate which may be distrbitued'.
      /* if(map_clusterToCentroid_2[cluster_id] == UINT_MAX) { */
      /* 	map_clusterToCentroid_2[cluster_id] = row_id; */
      /* } */
    } else {
      map_clusterToCentroid[cluster_id] = row_id;
    }
  }
  //fprintf(stderr, "map_clusterid_size=%u, nclusters=%u, at %s:%d\n", map_clusterid_size, nclusters, __FILE__, __LINE__);
  //!
  //! Handle 'non-cluster-asisgned' cases:
  const uint empty_value = UINT_MAX - 1; //!" ie, to 'know' if a cluster-set does Not have any children.
  for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
    if(map_clusterToCentroid[cluster_id] == UINT_MAX) { // empty_value) {
      //assert(map_clusterToCentroid_2[cluster_id] == UINT_MAX); //! ie, as we 'expec thte first to be set first'.
      //!
      //! Iterate through the 'different cases':
      bool is_found = false;

      for(uint cluster_id_2 = 0; (cluster_id_2 < nclusters) && (is_found == false); cluster_id_2++) {
	// fprintf(stderr, "\t\t clust(%u) VS clust(%u->[%u]), at %s:%d\n", cluster_id, cluster_id_2, map_clusterToCentroid[cluster_id_2], __FILE__, __LINE__);
	if( (cluster_id_2 != cluster_id) && (map_clusterToCentroid[cluster_id_2] < empty_value) ) {
	  //! Then we investigate 'pontial' candates:
	  uint list_of_candiates_size = 0;
	  uint *list_of_candiates = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&stack2d, /*row=*/cluster_id_2, &list_of_candiates_size);
	  
	  if(list_of_candiates_size > 0) { //map_clusterToCentroid_2[cluster_id_2] != UINT_MAX) {
	    assert(list_of_candiates_size != UINT_MAX);
	    for(uint i = 0; (i < list_of_candiates_size) && (is_found == false); i++) {	      
	      const uint row_id_center = list_of_candiates[i]; //map_clusterToCentroid_2[cluster_id_2]; 
	      if(row_id_center != UINT_MAX) {
		list_of_candiates[i] = UINT_MAX; //! ie, grab/steal the 'cesond center-vertex'.
		map_clusterToCentroid[cluster_id] = empty_value;
		map_clusterid[row_id_center] = cluster_id; //! ie, the 'prupose' wrt. this 3d-loop.
		is_found = true;
	      }
	    }
	  }
	}
      }
      assert(is_found);
    }
  }
  //!
  //! Valdiate our ª[bove] appraoch:
  for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
    assert(map_clusterToCentroid[cluster_id] != UINT_MAX); //! ie, where "empty_value" idnciates that we have 'handled the cluster-set'.
  }
  //!
  //! De-allocate and return:
  free_1d_list_uint(&map_clusterToCentroid);
  free_s_kt_set_2dsparse_t(&stack2d);
  //free_1d_list_uint(&map_clusterToCentroid_2);
  return; //! ie, as we asusmehte operaiton 'was a succsess'.
}
#else 
#pragma warn ("option not supported")
//#pragma GCC warning ("option not supported")
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).



//! @return true if we are to apply randomzaiton in the first step of the algorithm.
bool isToUse__randomziaitonInFirstIteration__s_kt_randomGenerator_vector_t(const s_kt_randomGenerator_vector_t *self) {
  const uint *tclusterid = self->mapOf_columnToRandomScore;
  const uint nelements = self->nelements;
  //! Add supprot for using a pre-specified vertex-ampping in k-means-and-k-median-appraoches:
  uint clusterId_max = 0; uint clusterId_min = UINT_MAX;
  for(uint j = 0; j < nelements; j++) {
    clusterId_max = macro_max(clusterId_max, tclusterid[j]);
    clusterId_min = macro_min(clusterId_min, tclusterid[j]);
  } //! ie, update the container 'which is not reset in each while-loop'.
  const uint nclusters = self->nclusters;
  // printf("clusterId-range=[%u, %u], given nclusters=%u, at %s:%d\n", clusterId_min, clusterId_max, nclusters, __FILE__, __LINE__);
  if( clusterId_max < clusterId_min) {return true;} //! ie, as the 'set' has then Not been intialised.
  if( (clusterId_max != UINT_MAX) && (clusterId_max != clusterId_min)) { //! then our 'random fucntion' has suggested all vertices to be 'in the same', ie, then 'explcit update our allcoation':
    return false;
    //isToUse__randomziaitonInFirstIteration = false;
  } else {return true;} //! ie, as all veritces are se tto be baprt of the same clsuter.
}

void randomassign__speicficType__math_generateDistributions(s_kt_randomGenerator_vector_t *self) {
  assert(self->nelements > 0); assert(self->nclusters > 0); assert(self->mapOf_columnToRandomScore);
  s_kt_randomGenerator_vector__config_t config_local = self->config;
  if(self->__counter__cntRandomCalls == 0) {
    config_local = self->config_init;
  }
  self->__counter__cntRandomCalls++; //! ie, increment
  e_kt_randomGenerator_type_t typeOf_randomNess = config_local.typeOf_randomNess;
  

  //! --------------------
  static configure_function_uniform_dataType s1 = 0; if(s1 == 0) {srand(time(NULL));  s1++;}
  const uint max_cnt_minusOne = self->nelements-1;
  //! --------------------
  const uint nclusters = self->nclusters;
  const uint nclusters_minusOne = self->nclusters - 1;
  const uint nelements = self->nelements;
  assert(nelements > 0);
  uint *mapOf_columnToRandomScore = self->mapOf_columnToRandomScore;
  assert(mapOf_columnToRandomScore);

  { //! Handle special cases:
    assert(nelements != 0);
    assert(nclusters != 0);
    if(nclusters == 1) {
      for(uint row_id = 0; row_id < nelements; row_id++) {
	mapOf_columnToRandomScore[row_id] = 0; //! ie, as tehy then are 'part of the same clsuter'.
      }
      return;
    } else if(nclusters == nelements) {
      for(uint row_id = 0; row_id < nelements; row_id++) {
	mapOf_columnToRandomScore[row_id] = row_id; //! ie, as tehy then are 'part of the distinctly different clsuter'.
      }
      return;
    }
  }

  //const uint nclusters_minsOne = nclusters - 1;
  if(config_local.isTo_initIntoIncrementalBlocks || (self->mapOf_columnToRandomScore[0] == UINT_MAX) ) {
    const uint cnt_each = nelements/nclusters;
    if(cnt_each > 0) {
      uint k = 0; uint row_id = 0;
      for(; k < nclusters-1; k++) {
	for(uint i = 0; i < cnt_each; i++) {
	  mapOf_columnToRandomScore[row_id++] = k;
	  assert_possibleOverhead(row_id < nelements);
	}
      }
      //! Update for the 'trailing part':
      assert_possibleOverhead(row_id < nelements);
      for(; row_id < nelements; row_id++) {
	mapOf_columnToRandomScore[row_id] = k;
	assert_possibleOverhead(row_id < nelements);
	assert_possibleOverhead(row_id < nelements);
      }
    } else {
      uint step_size = nclusters/nelements; //! eg, for the case where we are sub-samplign rows from a collection.
      if(step_size == 0) {step_size = 1;}
      mapOf_columnToRandomScore[0] = step_size;
      const uint nelements_minsOne = nelements-1;
      //const uint nclusters_minsOne = nclusters - 1;
      uint row_id = 1;
      for(; row_id < nelements_minsOne; row_id++) {
	mapOf_columnToRandomScore[row_id] = mapOf_columnToRandomScore[row_id-1] + step_size;
	assert(mapOf_columnToRandomScore[row_id] < nclusters);
      }
      //assert(mapOf_columnToRandomScore[row_id] < nclusters);
      if(row_id < nelements_minsOne) {
	const uint nclusters_minusOne = nclusters - 1;
	mapOf_columnToRandomScore[row_id] = macro_min(mapOf_columnToRandomScore[row_id], nclusters_minusOne);
	row_id++;
	assert(row_id < nelements);
	mapOf_columnToRandomScore[row_id] = nclusters-1;
	assert(mapOf_columnToRandomScore[row_id] < nclusters);
      }
    }
  }

  // FIXME: when this funciton'compiels' ... then write a permtuation using [below]
  const bool isTo__useUniformNumbers__inSleeciton = false;
  
# ifndef not_include_headerFiles
#define __Mi__postProcess() ({assert(nelements > 0); if(nclusters < nelements) {__investigateAndHAndle__caseWhereNotAllClustersAreSet(mapOf_columnToRandomScore, nelements, nclusters);} return;})
#else 
#pragma warn ("option not supported")
#define __Mi__postProcess() ({return;})
//#pragma GCC warning ("option not supported")
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).


  if(typeOf_randomNess == e_kt_randomGenerator_type_hyLysisDefault) {
    math_generateDistribution__randomassign(nclusters, nelements, mapOf_columnToRandomScore); __Mi__postProcess();}
  else if(typeOf_randomNess == e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame) {
    //printf("start-iteraiotn, at %s:%d\n", __FILE__, __LINE__);
    math_generateDistribution__randomassign(nclusters, nelements, mapOf_columnToRandomScore); 
    //printf("cmpl-iteraiotn, at %s:%d\n", __FILE__, __LINE__);
    math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(nclusters, nelements, mapOf_columnToRandomScore);
    //printf("cmpl-sim-assignment, at %s:%d\n", __FILE__, __LINE__);

     __Mi__postProcess();}
  else if(typeOf_randomNess == e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random) {
    const uint empty_0 = 0;
    uint *chosen_pts = mapOf_columnToRandomScore;
    // FIXME: write an evlauation-test which evlauate differences wrt. [”elow]
    uint cnt_evaluations = nelements;
    uint nrows = nelements;     uint ncols = nelements;
    if(self->matrix_input) {
      chosen_pts = allocate_1d_list_uint(nclusters, empty_0);
      uint nrows = self->matrix_input->nrows;     uint ncols = self->matrix_input->ncols;
      cnt_evaluations = nclusters;
    } 
    
    assert(chosen_pts);
    assert(nclusters >= 1);
    const uint nclusters_local = nrows; //! ie, as we are intersted 'in searhcing for points' (and Not ptuative clsutermebershisp), ie, an 'impclit cluster-emthdo-assignment-apprach'.
    const uint nclusters_local_maxVal = nrows - 1; 
    const uint max_cntIterations = nclusters_local; // * nclusters_local;
    //printf("start-iteraiotn, at %s:%d\n", __FILE__, __LINE__);
    for (uint i = 0; i < cnt_evaluations; i++) {
      uint cnt_iterations = 0;
      bool acceptable = true;
      //! Ensure that a vertex is 'center' of only one cluster:
      //uint cnt_iterations = 0;
      do {
	acceptable = true;
	//const uint nrows = x.n; //! where "x.n" is the number 
	uint rand_val = 0;
	if(isTo__useUniformNumbers__inSleeciton) {
	  const t_float p = 1.0/nclusters_local;
	  rand_val = math_generateDistribution__binomial(nclusters_local, p);
	  assert_possibleOverhead(rand_val < nclusters_local);
	} else { rand_val = macro_min(rand() % nclusters, nclusters_local_maxVal);} //! where 'this approach' will bw skwed towards lwo numbers, ie, 'deu to' the 'mdouloo-based appraoch'.
	assert_possibleOverhead(rand_val <= nclusters_local_maxVal);
	assert_possibleOverhead(rand_val < nclusters_local);
	chosen_pts[i] = rand_val;
	for (uint j = 0; j < i; ++j) {
	  if (chosen_pts[i] == chosen_pts[j]) {
	    acceptable = false;
	    break;
	  }
	}
	assert_possibleOverhead(cnt_iterations < max_cntIterations);
	cnt_iterations++;
      } while ( (acceptable == false) && (cnt_iterations < max_cntIterations) );
      if(acceptable == false) {
	assert_possibleOverhead(cnt_iterations >= max_cntIterations);
	//! Then we need to 'add' a manual iteraiton-appraoch wrt. the 'assignment':
	// TODO[article]: update our reseharc-aritlce wr.t [ªbov€] observiaton ... ie, that the "fast_kmeans" (and simalrity the "cluster.c" lbirary) is Not deisgned/cosntucted to handløe 'worst-case random csnevarios ... where an 'acceptable index' is 'nver' found (oekseth, 06. feb. 2017).
	
	for(uint row_id = 0; (row_id < nrows) && (acceptable == false); row_id++) {
	  bool isFound_local = false;
	  for (uint j = 0; j < i; ++j) {
	    if(chosen_pts[j] == row_id) {isFound_local = true;}
	  }
	  if(isFound_local == false) { //! then a 'unique poitn' is found:
	    chosen_pts[i] = row_id;
	    acceptable = true;
	  }
	}
      }
    }
    //printf("cmpl-iteraiotn, at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Identify the memberhsisp assicated to each vertex:
    if(self->matrix_input) {
# ifndef not_include_headerFiles
      findExtremeIndex__inSecondMatrix__aux__hp_distance(config_local.obj_metric, chosen_pts, self->matrix_input, nclusters, mapOf_columnToRandomScore, /*isto__findmin=*/true);
#else 
#pragma warn ("option not supported")
#define __Mi__postProcess() ({return;})
//#pragma GCC warning ("option not supported")
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).      
    }
/* printf("cmpl-iteraiotn, at %s:%d\n", __FILE__, __LINE__); */
/*     printf("cmpl-sim-assignment, at %s:%d\n", __FILE__, __LINE__); */
    //!
    //! De-allcoat ehte locally rserved memory-cotnaienrs:
    if(self->matrix_input) {
      free_1d_list_uint(&chosen_pts); chosen_pts = NULL;
    }
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss) {
    if(self->matrix_input == NULL) {
      fprintf(stderr, "!!\t The amtrix was Not set, ie, pelase ivnestgiate correctness of our call. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return; // false;
    } else {
      const uint empty_0 = 0;
      uint *chosen_pts = allocate_1d_list_uint(nclusters, empty_0);
      //uint *chosen_pts = mapOf_columnToRandomScore;
      assert(chosen_pts);
      //printf("start-iteraiotn::find-centers, at %s:%d\n", __FILE__, __LINE__);
# ifndef not_include_headerFiles
      const bool is_ok = __fastCluster__init_centers_kmeanspp__s_kt_matrix_base_t(self->matrix_input, /*k_clusterCount=*/nclusters, isTo__useUniformNumbers__inSleeciton, chosen_pts);
      assert(is_ok);
      //!
      //! Identify the memberhsisp assicated to each vertex:
      //printf("start-iteraiotn, at %s:%d\n", __FILE__, __LINE__);
      findExtremeIndex__inSecondMatrix__aux__hp_distance(config_local.obj_metric, chosen_pts, self->matrix_input, nclusters, mapOf_columnToRandomScore, /*isto__findmin=*/true);
#else 
#pragma warn ("option not supported")
#define __Mi__postProcess() ({return;})
//#pragma GCC warning ("option not supported")
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).
      //printf("cmpl-iteraiotn, at %s:%d\n", __FILE__, __LINE__);
      //__assign__vertices__toClusters(chosen_pts, self->matrix_input, nclusters, mapOf_columnToRandomScore);
      //!
      //! De-allcoat ehte locally rserved memory-cotnaienrs:
      assert(chosen_pts != mapOf_columnToRandomScore);
      free_1d_list_uint(&chosen_pts); chosen_pts = NULL;
    }
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss) {
    if(self->matrix_input == NULL) {
      fprintf(stderr, "!!\t The amtrix was Not set, ie, pelase ivnestgiate correctness of our call. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return; // false;
    } else {
      const uint empty_0 = 0;
      uint *chosen_pts = allocate_1d_list_uint(nclusters, empty_0);
      //uint *chosen_pts = mapOf_columnToRandomScore;
      assert(chosen_pts);
# ifndef not_include_headerFiles
      const bool is_ok = ____init_centers_pySciKitLearn__kmeanspp__s_kt_matrix_base_t(config_local, self->matrix_input, /*k_clusterCount=*/nclusters, isTo__useUniformNumbers__inSleeciton, chosen_pts, config_local.n_local_trials);
      assert(is_ok);      

      //!
      //! Identify the memberhsisp assicated to each vertex:
      findExtremeIndex__inSecondMatrix__aux__hp_distance(config_local.obj_metric, chosen_pts, self->matrix_input, nclusters, mapOf_columnToRandomScore, /*isto__findmin=*/true);
      //__assign__vertices__toClusters(chosen_pts, self->matrix_input, nclusters, mapOf_columnToRandomScore);
#pragma warn ("option not supported")
#undef __Mi__postProcess
#define __Mi__postProcess() ({return;})
//#pragma GCC warning ("option not supported")
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).      
      //!
      //! De-allcoat ehte locally rserved memory-cotnaienrs:
      assert(chosen_pts != mapOf_columnToRandomScore);
      free_1d_list_uint(&chosen_pts); chosen_pts = NULL;
    }
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_posixRan__srand__time__modulo) {
    for(uint i = 0; i < nelements; i++) { 
      const uint value = (uint)rand() % nclusters; mapOf_columnToRandomScore[i] = value;
    } 
    __Mi__postProcess(); //    return;
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_posixRan__srand__time__uniform) {
    for(uint i = 0; i < nelements; i++) { const uint rand_value = (uint)(i + (nclusters-i))*math_generateDistribution__uniform(); mapOf_columnToRandomScore[i] = rand_value;}
    //! Then swap:
    for(uint i = 0; i < nelements; i++) { const uint rand_value = (uint)(i + (nclusters-i))*math_generateDistribution__uniform(); mapOf_columnToRandomScore[i] = mapOf_columnToRandomScore[macro_min(rand_value, max_cnt_minusOne)];}
    __Mi__postProcess(); //    return;
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_binominal__incrementalInList) {
    //printf("start::compute randomness, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    for(uint i = 0; i < nelements; i++) { 
      t_float p_prob = (i+1)/((t_float)(nelements*0.9)); if(p_prob <= 0) {p_prob = 0.3;}
      uint rand_value = macro_min(math_generateDistribution__binomial(nclusters_minusOne, p_prob), nclusters_minusOne);
      assert(rand_value < nclusters);
      if(rand_value >= nclusters) {rand_value = 0;} //! ie, as the 'convergencr-critiera' has then 'not been met'.
      mapOf_columnToRandomScore[i] = rand_value;
    }
    //printf("cmpl::compute randomness, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    __Mi__postProcess(); //return;
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_binominal__pst__07) {
    for(uint i = 0; i < nelements; i++) { 
      t_float p_prob = (i+1)/((t_float)(nelements*0.9)); if(p_prob <= 0) {p_prob =  0.07;}
      const uint rand_value = macro_min(math_generateDistribution__binomial(nclusters_minusOne, p_prob), nclusters_minusOne);
      assert(rand_value < nclusters);
      mapOf_columnToRandomScore[i] = rand_value;
    }
    __Mi__postProcess(); //return;
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_binominal__pst__15) {
    for(uint i = 0; i < nelements; i++) { 
      t_float p_prob = (i+1)/((t_float)(nelements*0.9)); if(p_prob <= 0) {p_prob = 0.15;}
      const uint rand_value = macro_min(math_generateDistribution__binomial(nclusters_minusOne, p_prob), max_cnt_minusOne);
      assert(rand_value < nclusters);
      mapOf_columnToRandomScore[i] = rand_value;
    }
    __Mi__postProcess(); //return;
  } else if(typeOf_randomNess == e_kt_randomGenerator_type_binominal__pst__35) {
    for(uint i = 0; i < nelements; i++) { 
      t_float p_prob = (i+1)/((t_float)(nelements*0.9)); if(p_prob <= 0) {p_prob = 0.35;} 
      const uint rand_value = macro_min(math_generateDistribution__binomial(nclusters_minusOne, p_prob), max_cnt_minusOne);
      assert(rand_value < nclusters);
      mapOf_columnToRandomScore[i] = rand_value;
    }
    __Mi__postProcess(); //return;
  } else {
    assert(false); //! ie, as we have Not yet added supprot for this case.
  }
}

//! Construct a subset of the vertices using the [abov€] mapping-table (oekseth, 06. feb. 2017).
s_kt_randomGenerator_vector_t initAndReturn__fromSuper__s_kt_randomGenerator_vector_t(const uint *keyMap_localToGlobal, const s_kt_randomGenerator_vector_t *superset, s_kt_matrix_base_t *matrix_alreadyCompressed, const uint nclusters) {
  assert(matrix_alreadyCompressed);
  s_kt_randomGenerator_vector_t self = initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t();
  self.matrix_input = matrix_alreadyCompressed;
  //! ------------
  //! 
  //! Copy the defualt cofnigruation-settings:
  self.config_init = superset->config_init;
  self.config = superset->config;
  self.nclusters = nclusters;

  if(superset->mapOf_columnToRandomScore && matrix_alreadyCompressed->nrows) {
    assert(self.mapOf_columnToRandomScore == NULL);
    //!
    //! Initaite:
    self = initAndReturn__s_kt_randomGenerator_vector_t(nclusters, matrix_alreadyCompressed->nrows, self.config.typeOf_randomNess, self.config.isTo_initIntoIncrementalBlocks, matrix_alreadyCompressed);
    self.config_init = superset->config_init;
    self.config = superset->config;
    assert(self.nclusters == nclusters);    
    //! -------------------------------------------
    //! 
    //! Constuct a subset of the itntla cluster-mappings:
    //! 
    //! Iterate:
    const uint nrows = matrix_alreadyCompressed->nrows;
    uint biggest_cluster_id = 0; bool uintMax__isFound = false;
    for(uint i = 0; i < nrows; i++) {
      const uint global_id_head = (keyMap_localToGlobal) ? keyMap_localToGlobal[i] : i;
      assert(global_id_head != UINT_MAX);
      assert(global_id_head < superset->nelements);
      const uint cluster_vertex = superset->mapOf_columnToRandomScore[global_id_head];
      if(cluster_vertex != UINT_MAX) {
	biggest_cluster_id = macro_max(biggest_cluster_id, cluster_vertex);
      } else {uintMax__isFound = true;}
      //self.mapOf_columnToRandomScore[i] = cluster_vertex;
    }
    if(uintMax__isFound) {biggest_cluster_id++;} biggest_cluster_id++;
    const uint empty_0 = 0;
    uint *__mapGlobaClusters__toLocal = allocate_1d_list_uint(biggest_cluster_id, empty_0);
    for(uint clust_id = 0; clust_id < biggest_cluster_id; clust_id++) {
      __mapGlobaClusters__toLocal[clust_id] = UINT_MAX;
    }
    //!
    //! 'Re-name' the clsuter-vertex-memberships, ie, to handle 'emptyness' wr.t the clsuter-vertex-memberships
    //! Note: in [”elow] we re-use lgocis form our "kt_matrix_cmpCluster.c"--->"kt_matrix_cmpCluster__studb__vertexClusters_to_clusterVertexMatrix.c"
    uint cnt_localClusterIdMappingsAdded = 0; //UINT_MAX;
    for(uint i = 0; i < nrows; i++) {
      const uint global_id_head = (keyMap_localToGlobal) ? keyMap_localToGlobal[i] : i;
      assert(global_id_head != UINT_MAX);
      assert(global_id_head < superset->nelements);
      const uint cluster_vertex = superset->mapOf_columnToRandomScore[global_id_head];
      if(cluster_vertex != UINT_MAX) {
	uint cluster_vertex_local = UINT_MAX;
	
	for(uint clust_id = 0; clust_id < cnt_localClusterIdMappingsAdded; clust_id++) {
	  if(__mapGlobaClusters__toLocal[clust_id] == cluster_vertex) {cluster_vertex_local = clust_id;} //! ie, as we hav ethen found the latter
	}	
	//! Handle the 'odd' not-fooudn-case:
	if(cluster_vertex_local == UINT_MAX) {
	  const uint addAt__index = cnt_localClusterIdMappingsAdded;
	  __mapGlobaClusters__toLocal[addAt__index] = cluster_vertex;
	  cluster_vertex_local = addAt__index;
	  cnt_localClusterIdMappingsAdded++; //! ie, increment.
	}
	if(cluster_vertex_local == UINT_MAX) {
	  assert(false); //! ie, an heads-up.
	  cluster_vertex_local = biggest_cluster_id-1;
	}
	self.mapOf_columnToRandomScore[i] = cluster_vertex_local;
      } else {self.mapOf_columnToRandomScore[i] = biggest_cluster_id-1;} //! ie, a 'defualt mapping'
    }    
    free_1d_list_uint(&__mapGlobaClusters__toLocal); __mapGlobaClusters__toLocal = NULL;
  }

  //! @return
  return self;
}

