/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "correlation_macros__distanceMeasures.h"



//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string);  
    delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}


/**
   @brief compare the exeuction-time for differnet impelmetnaitons of the PCA algorithm
 **/
int main() {

  assert(false); // FIXME: compare our to-be-written NIPALS-impelemtnation with (a) "svd_LapackMatrixOperation", "svd_naive(..)" and teh 'NIPALS-impelmetantion which is to be compelcted'.
  
  assert(false); // FIXME: add seomthing
  
}
