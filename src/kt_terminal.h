#ifndef kt_terminal_h
#define kt_terminal_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_terminal
   @brief a parser to the input-tmerinal-arguemtns, argumetns which may be sued to cofnigure this object (oekseth, 06. okt. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "kt_terminal_clusterResult.h"

/**
   @enum e_kt_terminal_whenToMerge
   @brief provide a 'lgocal interfac'e to specify when the two inptu-matrices are to be merged (ie, if more than one inptu-matrix is given) (oekseth, 06. nov. 2016).
 **/
typedef enum e_kt_terminal_whenToMerge {
  e_kt_terminal_whenToMerge_beforeFilter,
  e_kt_terminal_whenToMerge_afterFilter,
  e_kt_terminal_whenToMerge_beforeClustering,
  e_kt_terminal_whenToMerge_inClustering,
} e_kt_terminal_whenToMerge_t;

//! @returnt the enum assicated ot the styring.
const e_kt_terminal_whenToMerge_t get_enumFromString__e_kt_terminal_whenToMerge_t(const char *stringOf_enum);

/**
   @struct s_kt_terminal
   @brief a wrapper-object to store the compelte set of input-data, logic-configuraitons and filters wrt. lgocial operations.
   @remarks overall idea wrt. this structure is to add parameter-support for:
   (1) two inptu-matrices: use ... {data-matrix, weight-list}
   (2) filtering 
   (2.a) transpose = {0, 1}
   (2.b) (as described in our "kt_matrix_filter.h"): ... 
   (2.c) centrality-metrics (as described in our ...??...)
   (3) correlation-metrics
   (4.a) k-clustering: k-means configuration: ... 
   (4.b) HCA configuration: ... 
   (4.c) SOM
   (4.d) PCA
   
   A compelxity wrt. our appraoch cosnerns tehy large number of result-foramts and resutrl-topolgoeuis to exports: 
   -- for details wrt. teh result-topologeis (which we have exprot-support for) please see our "s_kt_clusterResult_data_t"
      
**/
typedef struct s_kt_terminal {
  //! ---------------------
  //! 
  //! Itnernal configurations
  bool object_is_usedInternally_resultdoesThereforeNotNeedBeExported;
  //! ---------------------
  //!
  //! The set of input-matrices:
  s_kt_matrix_t inputMatrix_1;   
  s_kt_matrix_t inputMatrix_2;
  s_kt_matrix_t inputMatrix_result;
  
  //! Data-formats:
  uint nrows; uint ncols; 
  bool isTo_transposeMatrix;
  const char *stringOf_input_file_1;   const char *stringOf_input_file_2;
  const char *stringOf_input_file_1_weightList;   const char *stringOf_input_file_2_weightList;
  const char *stringOf_input_file_1_mask;   const char *stringOf_input_file_2_mask;
  bool isTo_readData_fromStream; bool isTo_include_stringIdentifers_in_matrix;
  const char *stringOf_sampleData_type; //const char *stringOf_sampleData_type_2;
  const char *stringOf_sampleData_type_secondMatrix; //! which if set implies taht we 'tehn' include a differnet dat-distribution into the second data-matrix: otehrwise
  const char *stringOf_sampleData_type_realLife; //const char *stringOf_sampleData_type_realLife_2;
  uint fractionOf_toAppendWith_sampleData_type_rows; uint fractionOf_toAppendWith_sampleData_type_columns;
  //bool include_stringIdentifers_in_matrix_1;   bool include_stringIdentifers_in_matrix_2;
  //!
  //! Result
  const char *stringOf_resultFile; const char *result_format;  
  bool isTo_inResult_isTo_useJavaScript_syntax;
  bool isTo_inResult_isTo_useJSON_syntax; 
  bool isTo_inResult_isTo_exportInputData;

  //! ----------------------------------
  //! 
  //! 
  s_kt_correlationMetric_t metric_beforeFilter;
  s_kt_correlationMetric_t metric_afterFilter;
  s_kt_correlationMetric_t metric_insideClustering;
  //! ---
  s_kt_matrix_filter_t filter_middleOf_correlationApplication; //! which hold teh cofnigruatiosn wrt. the matrix-filters: if 
  bool isTo_applyDataFilter;   bool isTo_applyDataAdjust; 

  //! ----------------------------------
  //! 
  //! Switches for the filtering and correlation-metrics
  bool isToApply_correlation_beforeFilter;    bool isToApply_correlation_afterFilter; 
  bool isToApply_correlation_beforeFilter_includeInResult;    bool isToApply_correlation_afterFilter_includeInResult; 
  e_kt_terminal_whenToMerge_t whenToMerge_differentMatrices;
  /* bool beforeFiltering_includeInresult;  */
  /* bool isTo_applyMAtrixFilter_beforeClustering; */
  /* s_kt_matrix_filter_t matrix_filter_beforeClustering;  */
  //! ----------------------------------
  //! 
  //! 

  /* //! ---------------------------------- */
  /* //!  */
  /* //! Compute cluster-distance: */
  /* char  */

  s_kt_clusterResult_config_t clusterConfig;

} s_kt_terminal_t;

//! Initaite the s_kt_terminal_t object to default values.
void init__s_kt_terminal_t(s_kt_terminal_t *self);
//! De-allcoate the s_kt_terminal_t object.
void free__s_kt_terminal_t(s_kt_terminal_t *self);

//! Export the data-sets to a singular file, ie, for each of the matrices 'which has data' (oekseth, 06. des. 2016).
//! @return true if no internel in-cosnsitencies were dsicovered.
//! @remarks to reduce the compelxtiy wwrt. data-marging we write out seperate fiels for the differnet result-cases:
bool exportDataSets__toACombinedFile__kt_terminal(s_kt_terminal_t *self, const char *stringOf_dataLabel, const bool isTo_useMatrixFormat);


//! From the configuraiton-object perform the cluster-analysis, eg, wrt. appluication of correlation-metics and k-means clustering.
int hpLysis_computeCorrelations__kt_terminal(s_kt_terminal_t *obj_dataInput);

//! Relate the string-input-params to an internal cofniguraiton-object (oekseth, 06. des. 2016).
//! @return an intaited object of the configuraitons assicated to the tmerinal-inptu-params
//! @remarks the first input-arguemtn is assumed to be the tmerinal-software-name, ie, for which we ingore the first tmerinal-input-argument
s_kt_terminal_t parse_cmdTerminalInputAgs__kt_terminal(char **array, const int array_cnt, bool *mandaroryInputParams_are_set);

/**
   @brief relate the terminal-bashs-string-parameters to Knitting-Tools hpLysis-software (oekseth, 06. opkt. 2016)
   @remarks is a terminal-based front-end to our hpLysis software: 
   - handle both configuraiton-string-parsing, euxeciton of algorithms and resutl-generationi
 **/
int add_params__kt_terminal(int array_cnt, char **array);

#endif // !EOF
