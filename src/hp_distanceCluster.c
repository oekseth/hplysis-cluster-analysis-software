#include "hp_distanceCluster.h"


bool apply__s_hp_distanceCluster_t(s_hp_distanceCluster_t *self, s_kt_list_1d_uint_t *obj_mapOf_disjtointForestIDs, const s_kt_matrix_t *mat_input, s_kt_list_1d_uint_t *retList) {
  e_kt_api_dynamicKMeans_t metric_centroid = self->metric_centroid;
  s_kt_correlationMetric_t objMetric__postMerge = self->objMetric__postMerge;
  assert(mat_input);
  assert(obj_mapOf_disjtointForestIDs);
  assert(obj_mapOf_disjtointForestIDs->list);

  //  assert(mat_input->nrows > obj_mapOf_disjtointForestIDs->list_size);
  //! --------------------------------------------------------
  //!
  //! configure the interptaiton of these reuslts:
  // FIXME: validate correnctess of [”elwo] assignmetns/correctness ... 
  //  e_kt_api_dynamicKMeans_t metric_centroid = self->config.centroidMetric__vertexVertex__center;
  if(metric_centroid == e_kt_api_dynamicKMeans_undef) {metric_centroid = e_kt_api_dynamicKMeans_AVG;}
  //! 
  //! 
  s_kt_matrix_t obj_result_clusterToVertex; setTo_empty__s_kt_matrix_t(&obj_result_clusterToVertex);
    //!
    //!
    //! Idneitfy number of vertices which needs to be inveitated, and then cosnturct a shallow compressed matrix, ie, to avoid any ovehread in cotmpatuiosn of un-neeessary relatiosnhips.:
#define MiF__getShallowMatrix(isTo_selectFor__marked) ({ \
      uint cnt_notInCenter = 0; uint cnt_marked = 0;   for(uint row_id = 0; row_id < mat_input->nrows; row_id++) {if(obj_mapOf_disjtointForestIDs->list[row_id] == UINT_MAX) {cnt_notInCenter++;} else {cnt_marked++;}} \
      const uint cnt_toEvaluate = (isTo_selectFor__marked) ? cnt_marked : cnt_notInCenter; \
      s_kt_matrix_base_t tmp_matInput = initAndReturn__notAllocate__s_kt_matrix_base_t(cnt_toEvaluate, mat_input->ncols, mat_input->matrix); \
      /*! Allcoate a temproary wrapper-matrix to avodi alreayd-evlauated-center-vertices from cluttering our results: */ \
      localMatrix__pointerBased = alloc_generic_type_2d(t_float, localMatrix__pointerBased, cnt_toEvaluate); tmp_matInput.matrix = localMatrix__pointerBased; \
      /*! Apply a shallwo-copy-proceudre:*/ \
      uint row_id_local = 0; for(uint row_id = 0; row_id < mat_input->nrows; row_id++) {if( (!isTo_selectFor__marked && (obj_mapOf_disjtointForestIDs->list[row_id] == UINT_MAX) ) || (isTo_selectFor__marked && (obj_mapOf_disjtointForestIDs->list[row_id] != UINT_MAX) ) ) {localMatrix__pointerBased[row_id_local++] = mat_input->matrix[row_id];}}  tmp_matInput;})

  { //! Apply logics: 'each clsuter':
    /* s_kt_matrix_t obj_1 = *mat_input; //; setTo_empty__s_kt_matrix_t(&obj_1); */
    /* assert(self->input_matrix != NULL); */
    /* obj_1.nrows = self->nrows;  obj_1.ncols = self->ncols; obj_1.matrix = self->input_matrix; */
    uint *mapOf_vertexClusterIds = obj_mapOf_disjtointForestIDs->list;
    uint cnt_cluster = 0;
    for(uint i = 0; i < obj_mapOf_disjtointForestIDs->list_size; i++) {
      const uint cluster_id = obj_mapOf_disjtointForestIDs->list[i];
      if(cluster_id != UINT_MAX) {
	cnt_cluster = macro_max(cnt_cluster, cluster_id);
      }
    }
    cnt_cluster++; //! ie, to 'walk past' the alst id.
    //!
    //! The oepraiton/call: 
    assert(mapOf_vertexClusterIds);
    assert(obj_result_clusterToVertex.nrows == 0); //! ie, as we expect 'this object' to Not have been intalized.
    assert(obj_result_clusterToVertex.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized.
    //!
    //!
    //! Idneitfy number of vertices which needs to be inveitated, and then cosnturct a shallow compressed matrix, ie, to avoid any ovehread in cotmpatuiosn of un-neeessary relatiosnhips.:
    const bool isTo_selectFor__marked = true; t_float **localMatrix__pointerBased = NULL; 
    s_kt_matrix_base_t tmp_matInput = MiF__getShallowMatrix(isTo_selectFor__marked);
    s_kt_matrix_t tmp_matInput_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&tmp_matInput);
    const bool is_ok = kt_matrix__centroids(&tmp_matInput_shallow, mapOf_vertexClusterIds, &obj_result_clusterToVertex, cnt_cluster, /*typeOf_centroidAlg=*/metric_centroid); //! deifned in our "kt_api.h"
    assert(is_ok);
    assert(localMatrix__pointerBased); free_generic_type_2d(localMatrix__pointerBased); localMatrix__pointerBased = NULL;
  }
  //! Allcoate result-lsit:
  s_kt_list_1d_uint_t obj_disjointForestId = init__s_kt_list_1d_uint_t(mat_input->nrows);
  { //! Post-step: use 'means' to idneityf the centers for each vertex:
    s_hp_distance__config_t local_config_init = init__s_hp_distance__config_t();
    //! Choose the cluster-id which is closest:
    local_config_init.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index;
    //!
    //!
    //! Idneitfy number of vertices which needs to be inveitated, and then cosnturct a shallow compressed matrix, ie, to avoid any ovehread in cotmpatuiosn of un-neeessary relatiosnhips.:
    const bool isTo_selectFor__marked = false; t_float **localMatrix__pointerBased = NULL; 
    s_kt_matrix_base_t tmp_matInput = MiF__getShallowMatrix(isTo_selectFor__marked);
    //! Allocate the result-matrix: 
    s_kt_matrix_base_t result_vec = initAndReturn__empty__s_kt_matrix_base_t();
    //! Apply:
    //    assert(mat_localCluster.nrows == cnt_clusters); //! ie, what we expect (oesketh, 06. jul. 2017).
    s_kt_matrix_base_t obj_result_clusterToVertex_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&obj_result_clusterToVertex);
    assert(tmp_matInput.ncols == obj_result_clusterToVertex_shallow.ncols); //! ie, as we otheriwse have an eror wrt. our comparison-appraoch (oekseth, 06. aug. 2017).
    const bool is_ok = apply__hp_distance(objMetric__postMerge, &tmp_matInput, &obj_result_clusterToVertex_shallow, &result_vec, local_config_init);
    //    const bool is_ok = apply__hp_distance(self->objMetric__sim, &tmp_matInput, &mat_clusterMeans, &result_vec, local_config_init);
    if(result_vec.nrows > 0) {
      if(result_vec.nrows != 1) {fprintf(stderr, "!!\t Result-set Not as epxected: result_vec has dimensions=[%u, %u], at [%s]:%s:%d\n", result_vec.nrows, result_vec.ncols, __FUNCTION__,  __FILE__, __LINE__);}
      if(result_vec.ncols != tmp_matInput.nrows) {fprintf(stderr, "!!\t Result-set Not as epxected: result_vec has dimensions=[%u, %u] while tmp_matInput has dimensions=[%u, %u], at [%s]:%s:%d\n", result_vec.nrows, result_vec.ncols, tmp_matInput.nrows, tmp_matInput.ncols, __FUNCTION__, __FILE__, __LINE__);}
      assert(result_vec.nrows == 1);
      assert(result_vec.ncols == tmp_matInput.nrows);
      //!
      //! Update the result-object with the [ªbov€] identified relationships:
      /* assert(obj_result); */
      /* if(obj_result->vertex_clusterId == 0) { */
      /*   init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, cnt_clusters, mat_input->nrows); */
      /* } */
      
      //assert(obj_result->vertex_clusterId);
      uint global_row_id = 0;
      for(uint i=0; i<result_vec.ncols; i++) { //! ie, the n copy the mappings:
	const uint cluster_id = result_vec.matrix[0][i];
	//      const uint cluster_id = UINT_MAX; assert(cluster_id != UINT_MAX); // FIXME: replace this .... figure out how to correctly set the clstuer-id.
	assert(cluster_id != UINT_MAX);
	//assert(cluster_id < cnt_clusters);
	obj_disjointForestId.list[global_row_id] = cluster_id;
	//obj_result->vertex_clusterId[i] = cluster_id;
	//! 
	global_row_id++;
      }
    }
    //!
    //! De-allcoate:
    free__s_kt_matrix_base_t(&result_vec);
    assert(localMatrix__pointerBased); free_generic_type_2d(localMatrix__pointerBased); localMatrix__pointerBased = NULL;
  }
  free__s_kt_matrix(&obj_result_clusterToVertex);
  //!
  //! @return 
  *retList = obj_disjointForestId;
  return true;
}
