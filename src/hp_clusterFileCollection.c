#include "hp_clusterFileCollection.h"
#include "aux_sysCalls.h" //! provide generic routiens for system-calls (oekseth, 06. feb. 2017)   

//!    @return a new-allcoated list of cluster-algorithm-specs
s_hp_clusterFileCollection__clusterAlg_t init__s_hp_clusterFileCollection__clusterAlg_t(const e_hp_clusterFileCollection__clusterAlg_t enum_id, const bool includeEmptyCase) {
  s_hp_clusterFileCollection__clusterAlg_t self_base;
  s_hp_clusterFileCollection__clusterAlg_t *self = &self_base;
  self->list_size = 0;
  //!
  //! Identify the number of metrics to allcoate:
  if(enum_id == e_hp_clusterFileCollection__clusterAlg_kMeans) {
    self->list_size = 3;
  } else if(enum_id == e_hp_clusterFileCollection__clusterAlg_kMeans) {
    self->list_size = 5;
 /* { */
 /*    self->list_size = 3; */
  } else  if(enum_id != e_hp_clusterFileCollection__clusterAlg_undef) {
    fprintf(stderr, "!!\t(option-not-supported)\t Add support for the measure=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
  }
  //! ---- 
  if(includeEmptyCase || (enum_id == e_hp_clusterFileCollection__clusterAlg_undef) ) {
    self->list_size++;
  }
  //! -----------------------------------------------------------------------------------------
  //!
  //! Allcoate memroy-space:
  assert(self->list_size > 0);
  self->list = alloc_generic_type_1d_xmtMemset(e_hpLysis_clusterAlg_t, self->list, self->list_size);
  assert(self->list);
  //!
  //! Set the simlairty-emtrics, ie, itnate the objects:
  uint current_pos = 0;
  if(enum_id == e_hp_clusterFileCollection__clusterAlg_kMeans) {
    assert(self->list_size >= 3);
    self->list[current_pos] = e_hpLysis_clusterAlg_kCluster__AVG; current_pos++; assert(current_pos <= self->list_size);
    self->list[current_pos] = e_hpLysis_clusterAlg_kCluster__rank; current_pos++; assert(current_pos <= self->list_size);
    self->list[current_pos] = e_hpLysis_clusterAlg_kCluster__medoid; current_pos++; assert(current_pos <= self->list_size);
  } else if(enum_id == e_hp_clusterFileCollection__clusterAlg_HCA) {
    assert(self->list_size >= 3);
    self->list[current_pos] = e_hpLysis_clusterAlg_HCA_single; current_pos++; assert(current_pos <= self->list_size);
    self->list[current_pos] = e_hpLysis_clusterAlg_HCA_max; current_pos++; assert(current_pos <= self->list_size);
    self->list[current_pos] = e_hpLysis_clusterAlg_HCA_average; current_pos++; assert(current_pos <= self->list_size);    
    // --
    self->list[current_pos] = e_hpLysis_clusterAlg_HCA_centroid; current_pos++; assert(current_pos <= self->list_size);
    self->list[current_pos] = e_hpLysis_clusterAlg_kruskal_hca; current_pos++; assert(current_pos <= self->list_size);
    //self->list[current_pos] = ; current_pos++; assert(current_pos <= self->list_size);
  } else   if(enum_id != e_hp_clusterFileCollection__clusterAlg_undef) {
    fprintf(stderr, "!!\t(option-not-supported)\t Add support for the measure=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
  }
  //! ---- 
  if(includeEmptyCase || (enum_id == e_hp_clusterFileCollection__clusterAlg_undef) ) {
    assert(current_pos < self->list_size); //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
    self->list[current_pos] = e_hpLysis_clusterAlg_undef; current_pos++; assert(current_pos <= self->list_size);
  }
  //! Validate consistency:
  assert(current_pos == self->list_size);
  //! -----------------------------------------------------------------------------------------
  //!
  //! @return
  return self_base;
}

//! De-allocate the s_hp_clusterFileCollection_simMetricSet_t object (oekseth, 06. feb. 2017).
void free__s_hp_clusterFileCollection_simMetricSet_t(s_hp_clusterFileCollection__clusterAlg_t *self) {
  if(self->list) {
    assert(self->list_size > 0);
    self->list_size = 0;
    free_generic_type_1d((self->list));  self->list = NULL; //! ie, de-allocate.
  }
}

//! @return an itnernal string-representaiton of an itnenrla clsuter-alg-object.
const char *getStringOfMAppingToFile__s_hp_clusterFileCollection_t(const uint data_id, const uint cnt_insideDataLoop,  const s_hp_clusterFileCollection_t data_obj, const uint cluster_index, const uint met_prior, const uint met_inside, char *str_suff) {
  //char str_suff[2000] = {'\0'}; 
  const bool tran = data_obj.fileRead_config.isTo_transposeMatrix;
  sprintf(str_suff, "id_%u_%u.%s.t_%s.a_%u.c_%u_%u", data_id, cnt_insideDataLoop, data_obj.tag, (tran) ? "true" : "false", cluster_index, met_prior, met_inside);
//static const char *getStringOfMAppingToFile__s_hp_clusterFileCollection_t(const s_hp_clusterFileCollection_t obj, const e_hpLysis_clusterAlg_t config__clusterAlg, const s_kt_correlationMetric_t corrMetric_prior, const s_kt_correlationMetric_t corrMetric_inside) {
  //char str_suff[2000] = {'\0'}; sprintf(str_suff, "%s.a_%u.c_%u_%u", data_obj.tag, config__clusterAlg, , met_prior__inside);
  return str_suff;
}
//! @return an itnernal string-representaiton of an itnenrla clsuter-alg-object.
const char *getStringOfMAppingToFile__simMetric__s_hp_clusterFileCollection_t(const s_hp_clusterFileCollection_t data_obj, const uint cluster_index, const uint met_prior, const uint met_inside, char *str_suff) {
  //char str_suff[2000] = {'\0'}; 
  const bool tran = data_obj.fileRead_config.isTo_transposeMatrix;
  sprintf(str_suff, "a_%u.c_%u_%u", cluster_index, met_prior, met_inside);
//static const char *getStringOfMAppingToFile__s_hp_clusterFileCollection_t(const s_hp_clusterFileCollection_t obj, const e_hpLysis_clusterAlg_t config__clusterAlg, const s_kt_correlationMetric_t corrMetric_prior, const s_kt_correlationMetric_t corrMetric_inside) {
  //char str_suff[2000] = {'\0'}; sprintf(str_suff, "%s.a_%u.c_%u_%u", data_obj.tag, config__clusterAlg, , met_prior__inside);
  return str_suff;
}

//! @return an intiated counter:
static s_hp_clusterFileCollection_localCounter_t init__s_hp_clusterFileCollection_localCounter() {
  s_hp_clusterFileCollection_localCounter_t obj_counter;
  obj_counter.cnt_data = 0;
  obj_counter.cnt__data_x_cluster_x_metPri = 0;
  obj_counter.cnt__data_x_cluster_x_metPri__x__metInside = 0;
  obj_counter.cnt__cluster_x_metPri__x__metInside = 0;
  //! @return
  return obj_counter;
}

//! Apply logics using a gneeirc approach.
s_hp_clusterFileCollection_localCounter_t applyLogics__insideChunk__s_hp_clusterFileCollection_t(FILE *file_out, const s_hp_clusterFileCollection_t *list, const uint list_size, e_hp_clusterFileCollection___internalTraverseLogic_t typeOf_logic) {
  assert(list); assert(list_size);
  s_hp_clusterFileCollection_localCounter_t obj_counter = init__s_hp_clusterFileCollection_localCounter();
  //! Apply logics for each data-set:
  if(typeOf_logic == e_hp_clusterFileCollection___internalTraverseLogic__writeOut) {
    assert(file_out);
    //! Add a header-tag:
    fprintf(file_out, "/*\n#! shortHand\t tag\t data\t isTo_transpose\t clusterAlg\t metricPrior\t metricInside \t kClusterCount\n");
  }
  uint __cntWarnings__headsUp = 0;
  for(uint data_id = 0; data_id < list_size; data_id++) {
    uint cnt_matrices_exported = 0;
    uint __local__cnt__cluster_x_metPri__x__metInside = 0;
    const s_hp_clusterFileCollection data_obj = list[data_id];
    if(typeOf_logic == e_hp_clusterFileCollection___internalTraverseLogic__updateCounters) {obj_counter.cnt_data++;}
    //fprintf(stderr, "data_obj.clusterAlg=%u, at %s:%d\n", data_obj.clusterAlg, __FILE__, __LINE__);
    const bool clustAlg__inlcudeEmptyCases = true; //! which should be consistnet with our "traverse__s_hp_clusterFileCollection_traverseSpec(..)"
    s_hp_clusterFileCollection__clusterAlg_t obj_clusterAlg = init__s_hp_clusterFileCollection__clusterAlg_t(data_obj.clusterAlg, /*includeEmptyCase=*/clustAlg__inlcudeEmptyCases); //! wher ehte latter is set to false as we asusme it is of itnerest to 'not only' compute wrt. the simalrity-amtrix.
#ifndef NDEBUG
    if(data_obj.clusterAlg == e_hp_clusterFileCollection__clusterAlg_kMeans) {
      const uint def_size = 3; //! ie, the asusmption based on the current cofniguraiton.
      if(clustAlg__inlcudeEmptyCases == false) {assert(obj_clusterAlg.list_size == def_size);}  
      else {assert(obj_clusterAlg.list_size == (1+def_size));}  
    }
    assert(obj_clusterAlg.list != NULL);     assert(obj_clusterAlg.list_size > 0); //! ie, as we expect the object to have been set.
#endif

    //!
    //! Set the simliarty/correlation matrix: "prior":
    s_hp_clusterFileCollection_simMetricSet_t objMetrics__prior = init__hp_clusterFileCollection_simMetricSet(data_obj.metric__beforeClust, /*includeEmptyCase=*/true);
    assert(objMetrics__prior.list_size > 0);     assert(objMetrics__prior.list != NULL);  //! whe expec thte object 'to hold content':
    //!
    //! Set the simliarty/correlation matrix: "inside-clustering":
    s_hp_clusterFileCollection_simMetricSet_t objMetrics__inside = init__hp_clusterFileCollection_simMetricSet(data_obj.metric__insideClust, /*includeEmptyCase=*/false);
    assert(objMetrics__inside.list_size > 0);     assert(objMetrics__inside.list != NULL);  //! whe expec thte object 'to hold content':

    //! 
    //! Iterate throguh the clustering-algorithms:
    for(uint cluster_index = 0; cluster_index < obj_clusterAlg.list_size; cluster_index++) {
      const e_hpLysis_clusterAlg_t config__clusterAlg = obj_clusterAlg.list[cluster_index];
      const bool config__typeOfOps__onlySimMetric = (config__clusterAlg == e_hpLysis_clusterAlg_undef);
      //printf("\tdata[%u]\tobjMetrics__prior.list_size=%u, at %s:%d\n", data_id, objMetrics__prior.list_size, __FILE__, __LINE__);
      //! 
      //!
      //! Handle different cases wrt. simlairty-metrics:
      for(uint met_prior = 0; met_prior < objMetrics__prior.list_size; met_prior++) {
	//! Set the metric:
	const s_kt_correlationMetric_t corrMetric_prior = objMetrics__prior.list[met_prior]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
	//const bool config_corrMetric_prior_use = (corrMetric_prior.metric_id != e_kt_correlationFunction_undef);
	if(cluster_index == 0) {
	  if(corrMetric_prior.metric_id != e_kt_correlationFunction_undef) {
	    if(typeOf_logic == e_hp_clusterFileCollection___internalTraverseLogic__updateCounters) {obj_counter.cnt__data_x_cluster_x_metPri++;} //! ie, then we increment the couunter
	  }
	}
	//printf("\t\tobjMetrics__inside.list_size=%u, at %s:%d\n", objMetrics__inside.list_size, __FILE__, __LINE__);
	for(uint met_prior__inside = 0; met_prior__inside < objMetrics__inside.list_size; met_prior__inside++) {
	  const s_kt_correlationMetric_t corrMetric_inside = objMetrics__inside.list[met_prior__inside]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
	  if(typeOf_logic == e_hp_clusterFileCollection___internalTraverseLogic__writeOut) {
	    //! Spefiy the string-object used to idneity the metic: 	  
	    char str_suff[2000] = {'\0'}; 
	    //const char *str_suff = 
	    getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_id, cnt_matrices_exported++, data_obj, cluster_index, met_prior, met_prior__inside, str_suff);
	    const bool tran = data_obj.fileRead_config.isTo_transposeMatrix;
	    fprintf(file_out, "%s"
		    "\t %s \t %s \t"
		    " %s \t %s \t"
		    "%s:%s \t %s:%s \t %u\n",
	    //fprintf(file_out, "%s\t\ttag=%s, data=%s\t clusterAlg=%s, metricPrior=%s:%s, metricInside=%s:%s, kClusterCount=%u\n",
		    str_suff, 
		    data_obj.tag, data_obj.file_name, (tran) ? "true" : "false",
		    get_stringOf__e_hpLysis_clusterAlg_t(config__clusterAlg),
		    get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep),
		    get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id),
		    //! ---
		    get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_inside.typeOf_correlationPreStep),
		    get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id),
		    //! ---
		    data_obj.k_clusterCount
		    );
	  } else {
	    obj_counter.cnt__data_x_cluster_x_metPri__x__metInside++;
	    //if(!cluster_index && !met_prior && !met_prior__inside) {
	    //if(data_id == 0) {
	    __local__cnt__cluster_x_metPri__x__metInside++; //! ie, as we then asusme: firs tieraiton from the 'outer block'
	      //printf("\t\t\tdata_id[%u] w/counter=%u, at %s:%d\n", data_id, __local__cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__);
	    /* } else { */
	    /*   // printf("\t\t\tdata_id[%u] w/counter=%u, -- ommits -- at %s:%d\n", data_id, obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__); */
	    /* } */
	  }
	}
      }
    }
    //! 
    //! Update the config-object:
    //! Note we 'macro_max(..)' call is used to 'handle' the case where the first 'data-object' have a 'metric-fogniratuion' which is different from some of the octer data-set-cofngirautions. An example of thed løatter conserns the case where we for 'data=0' evlauate only Euclid and MINe, while we for 'data=2' evaluate the complete number of sim-metrci-cobmatinos supproted by hpLysis (oeksth, 06. feb. 2017).
    obj_counter.cnt__cluster_x_metPri__x__metInside = macro_max(obj_counter.cnt__cluster_x_metPri__x__metInside, __local__cnt__cluster_x_metPri__x__metInside); 
    /* fprintf(stderr, "obj_counter.cnt__cluster_x_metPri__x__metInside=%u, at %s:%d\n", obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__); */
    /* fprintf(stderr, "data[%u]\tdims=[%u, %u, %u]=|%u|, at %s:%d\n",  */
    /* 	    data_id, */
    /* 	    obj_clusterAlg.list_size, objMetrics__prior.list_size, objMetrics__inside.list_size,  */
    /* 	    obj_clusterAlg.list_size * objMetrics__prior.list_size * objMetrics__inside.list_size,  */
    /* 	    __FILE__, __LINE__); */
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&objMetrics__prior);
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&objMetrics__inside);
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&obj_clusterAlg);
    if(obj_counter.cnt__cluster_x_metPri__x__metInside != __local__cnt__cluster_x_metPri__x__metInside) {
      if(__cntWarnings__headsUp == 0) {
	fprintf(stderr, "(info)\t Seems like you have different sets of metric-combiantions for your data-sets: %u VS %u. Implication is that the 'generated reuslt-matrix' will be in-complete for some of your dat-asets. Whiel this 'observaiton' is not ciritcal, ie, may influence the result-set. If this warning was not as expected then pelase read the coumentaiotn: if the latter does not help, we suggest cotnacting the senior devleoper at [eosketh@gamil.com]. Observaiton at [%s]:%s:%d\n", obj_counter.cnt__cluster_x_metPri__x__metInside, __local__cnt__cluster_x_metPri__x__metInside, 
__FUNCTION__, __FILE__, __LINE__);
	__cntWarnings__headsUp++; //! to aovid too many erorr-messages for clsuttinger the result-set
      }
    }

  }
  if(typeOf_logic == e_hp_clusterFileCollection___internalTraverseLogic__writeOut) {
    assert(file_out);
    //! Add a tail-tag:
    fprintf(file_out, "\n*/\n");
  }
  //! @return the coutner-object:
  return obj_counter;
}

//! Write out the mappign betwene the file-name-indices and their 'actual meaning':
void writeOut__mappingFile__s_hp_clusterFileCollection_t(FILE *file_out, const s_hp_clusterFileCollection_t *list, const uint list_size) {
#define __Mi_c_mapOf_realLife__isStruct 1
  assert(file_out);
  //! Apply logics:
  applyLogics__insideChunk__s_hp_clusterFileCollection_t(file_out, list, list_size, e_hp_clusterFileCollection___internalTraverseLogic__writeOut);
}

//! Perofrm a minimal intiation of the "pathTo_localFolder_storingEachSimMetric" object (oekseth, 06. feb. 2017)
s_hp_clusterFileCollection_traverseSpec_t initAndReturn__s_hp_clusterFileCollection_traverseSpec_t() {
  s_hp_clusterFileCollection_traverseSpec_t self;
  self.config__isToPrintOut__iterativeStatusMessage = true;
  self.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  self.pathTo_localFolder_storingEachSimMetric = NULL; //"results/tutEx__difficultData/";
  self.nameOf_resultFile__clusterMemberships = "tutResult_simMetrics.js";
  self.nameOf_resultFile__clusterMemberships__deviation = "tutResult_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  self.config_arg_npass = 10000; 
  //!
  //! Result data:
  self.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
  //bool config__isToPrintOut__iterativeStatusMessage; 
  self.isToStore__inputMatrix__inFormat__csv = false; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.  
  self.postProcessing__clusterResultMatrices = true; //! if "true": then we apply clustierng wrt. the 'data collected in result-amtrices': (a) apply sim-matrix+cluster; (b) write out both clsuter-results and a 'maksed' matrix ... both for transposed and non-transposed ... a result which may be used to assert/idnetify/descirb ethe simlairty between: (a) CCMs; (b) cluster-algorithms; (c) simlairty-metrics; (d) improtance/impact of data-sets (when evaluating simlairty between clsuter-algorithms).
  self.postProcessing__clusterResultMatrices__clustAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  self.postProcessing__clusterResultMatrices__nClusters = 3; //! ie, that the clusters may be seperated based on 2-3 groups, a genericd assumption which should be tuned by the caller.
  //! ------------------ 
  self.option_scoreFrequency__cntBuckets = 20; //! which if Not set to '0' is used to 'descride' the number of 'buckets' used to describe the score-freuqncy of a pre-clustering-computed simliarty-matrix (ie, if ore-clstuering-simalrity-amtrices-options are used in at least one of the data--configuation-objects).
  //! -------------------------------
  return self;
}

//! 
//! Apply clustiering using generidc cofnigurations and thenw rite out the result:
static void applyDefaultClustering__andExport(const s_hp_clusterFileCollection_traverseSpec *self, s_kt_matrix_t *obj__1, const bool isTo_transposeMatrix, const char *stringOf_data) {
  const char *__dirPath = self->pathTo_localFolder_storingEachSimMetric;
  if( (self->pathTo_localFolder_storingEachSimMetric == NULL) || !strlen(self->pathTo_localFolder_storingEachSimMetric)) {
    __dirPath = ""; //! ie, store 'in the lcoal user-direcotry'.
    //fprintf(stderr, "!!\t("
  }
  { //! Export the inptu-matrix:
    char str_local[2000] = {'\0'}; sprintf(str_local, "%s_simMatrix_input.%s.tsv", __dirPath, stringOf_data);
    const bool is_ok = export__singleCall__s_kt_matrix_t(obj__1, str_local, NULL);
    assert(is_ok);
  }
  //! Configure:
  s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
  hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
  hp_config.config.clusterConfig.isTo_transposeMatrix = isTo_transposeMatrix;
  //! then we export the result-matrix:
  char str_local[2000] = {'\0'}; sprintf(str_local, "%s_simMatrix.%s.tsv", __dirPath, stringOf_data);
  char str_local__clusters[2000] = {'\0'}; sprintf(str_local__clusters, "%s_meta.%s.tsv", __dirPath, stringOf_data);
  hp_config.stringOfResultPrefix__exportCorr__prior = str_local;
  const uint nelements = (isTo_transposeMatrix == false) ? obj__1->nrows : obj__1->ncols;
  const uint k_clusterCount = self->postProcessing__clusterResultMatrices__nClusters;
  if(obj__1->nrows <= k_clusterCount) { //! ie, where the altter would imply a 'poitnelss-clsutering' wrt. k-means.
    //!
    //! Correlate: 
    const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, obj__1, NULL, /*isTo_applyPreFilters=*/false);
    assert(is_ok);
  } else {
    //!
    //! Apply clsutering:
    const bool is_ok = cluster__hpLysis_api(&hp_config, self->postProcessing__clusterResultMatrices__clustAlg, obj__1, k_clusterCount, self->config_arg_npass);
    assert(is_ok);
    { //! Export a masked verison of the simliarty-matrix: use t he clsuter-result to filter wrt. the socres:
      { //! FORMAT="TSV":
	char str_local__maskedMatrix[2000] = {'\0'};    sprintf(str_local__maskedMatrix, "%s_maskedMatrix.%s.tsv", __dirPath, stringOf_data);
	assert(strlen(str_local__maskedMatrix));
	const e_hpLysis_export_formatOf_t exportFormat = e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_TSV; 
	const bool is_ok = export__hpLysis_api(&hp_config, str_local__maskedMatrix, /*file_out=*/NULL, exportFormat, NULL);
	assert(is_ok);
      }
      { //! FORMAT="JS":
	char str_local__maskedMatrix[2000] = {'\0'}; sprintf(str_local__maskedMatrix, "%s_maskedMatrix.%s.js", __dirPath, stringOf_data);
	assert(strlen(str_local__maskedMatrix));
	const e_hpLysis_export_formatOf_t exportFormat = e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS; 
	const bool is_ok = export__hpLysis_api(&hp_config, str_local__maskedMatrix, /*file_out=*/NULL, exportFormat, NULL);
	assert(is_ok);
      }
    }
    { //! Export: traverse teh generated result-matrix-object and write out the results:    
      FILE *result_file = fopen(str_local__clusters, "wb");
      if(result_file == NULL) {
	fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", str_local__clusters, __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up.
      }
      const uint *vertex_clusterId = hp_config.obj_result_kMean.vertex_clusterId;
      assert(vertex_clusterId);
      const uint cnt_vertex = hp_config.obj_result_kMean.cnt_vertex;
      assert(cnt_vertex > 0);
      //!
      //! 
      fprintf(result_file, "\n//! Write out the cluster-memberships for case=\"%s\":\n", stringOf_data);
      fprintf(result_file, "clusterMemberships_%s=[\n", stringOf_data);
      uint max_cnt = 0;
      for(uint i = 0; i < cnt_vertex; i++) {
	if( (hp_config.config.clusterConfig.isTo_transposeMatrix == false) && obj__1->nameOf_rows) {
	  assert(i < obj__1->nrows);
	  fprintf(result_file, "\"%s\"\t->\t%u,\n", obj__1->nameOf_rows[i], vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);
	} else if( (i < obj__1->ncols) && (hp_config.config.clusterConfig.isTo_transposeMatrix == true) && obj__1->nameOf_columns) {
	  assert(i < obj__1->ncols);
	  assert(obj__1->nameOf_columns[i]);
	  fprintf(result_file, "\"%s\"\t->\t%u,\n", obj__1->nameOf_columns[i], vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);
	} else {
	  fprintf(result_file, "%u\t->\t%u,\n", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);
	}
      }	  
      fprintf(result_file, "];\n");
      //!
      //! Write out the CCMs wrt. the clustering:
      //!
      //! 
      fprintf(result_file, "\n//! Write out the Clsuter-comparison-metrics (CCMs) for case=\"%s\":\n", stringOf_data);
      fprintf(result_file, "ccm_%s=[\n", stringOf_data);
      //!
      //! Apply different matrix-based CCMs:
      for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
	const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
	const t_float score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&hp_config, ccm_type, NULL, NULL); //matrix_cmp);
	//!
	//! Write out the score:
	/* const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
	/* if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name */	
	if(score != T_FLOAT_MAX) {
	  fprintf(result_file, "ccm[\"%s\"] \t->\t %f\n", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), score);
	}
      }
      fprintf(result_file, "];\n");
      //!
      //! Close the result-file:
      fclose(result_file);
    }
  }
  /* { //! For the 'complete' simlairty-result-matrix: */
  /*   fprintf(fileDesriptorOut__clusterResults, "\n//! Export data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep); */
  /*   export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults, self->config_exportFormat, &obj_matrixInput); */
  /* } */
  //!
  //! De-allocate the clsuter-object:
  free__s_hpLysis_api_t(&hp_config);
}


//! Apply the lgoics described in our s_hp_clusterFileCollection_traverseSpec_t self object (oesketh, 06. feb. 2017).
bool traverse__s_hp_clusterFileCollection_traverseSpec(const s_hp_clusterFileCollection_traverseSpec *self, const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size) {
  assert(self); assert(mapOf_realLife); assert(mapOf_realLife_size);
  //! ----------------------------------------------------------
  FILE *fileDesriptorOut__clusterResults = NULL;    FILE *fileDesriptorOut__clusterResults__deviation = NULL; 
  //! finish: configuration ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! Start: logics ------------------------------------------------------------------------------------
  //! 
  //! 
  { //! Open the file used for writing of 'named' cluster-emberships':      
    fileDesriptorOut__clusterResults = fopen(self->nameOf_resultFile__clusterMemberships, "wb");
    if(!fileDesriptorOut__clusterResults) {
      fprintf(stderr, "!!\t Unable to open the result-file=\"%s\": pelase vlaidate correctenss of both path, disk-space and dist-write-permissions. Observaiton at [%s]:%s:%d\n", self->nameOf_resultFile__clusterMemberships, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up. 
    }
    fileDesriptorOut__clusterResults__deviation = fopen(self->nameOf_resultFile__clusterMemberships__deviation, "wb");
    if(!fileDesriptorOut__clusterResults__deviation) {
      fprintf(stderr, "!!\t Unable to open the result-file=\"%s\": pelase vlaidate correctenss of both path, disk-space and dist-write-permissions. Observaiton at [%s]:%s:%d\n", self->nameOf_resultFile__clusterMemberships__deviation, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up. 
    }
  }
  const char *__dirPath = self->pathTo_localFolder_storingEachSimMetric;
  if( (self->pathTo_localFolder_storingEachSimMetric == NULL) || !strlen(self->pathTo_localFolder_storingEachSimMetric)) {
    __dirPath = ""; //! ie, store 'in the lcoal user-direcotry'.
    //fprintf(stderr, "!!\t("
  }
  //! ----------------------------------------------------------------------------
  //!
  { //! Validate that the path(s) are correct:
    if(self->pathTo_localFolder_storingEachSimMetric != NULL) {
      //! Tehbn: delete all files in a folder (oekseth, 06. feb. 2017):
      const bool is_ok = removeFiles_inFolder__aux_sysCalls(self->pathTo_localFolder_storingEachSimMetric);
      //assert(false);
      assert(is_ok);
    }
  }
  //! --------------------------------------------------
  //!
  s_kt_matrix_t matResult__result_x_ccm; setTo_empty__s_kt_matrix_t(&matResult__result_x_ccm);
  s_kt_matrix_t matResult__data_x_simAndAlg__time; setTo_empty__s_kt_matrix_t(&matResult__data_x_simAndAlg__time); //! ie, 'hold' the exeuciton-time, 'buildng' a legend-row for each data-set: simplifes the comaprin of exeuction-time betweend fifernet algoritm-appraoches.
  //{ //! Allcoate space for the matrix:
#if( __Mi_c_mapOf_realLife__isStruct == 1)
  //! 
  //! Identify the matrix-dimensions:
  const s_hp_clusterFileCollection_localCounter_t obj_counter = applyLogics__insideChunk__s_hp_clusterFileCollection_t(NULL, mapOf_realLife, mapOf_realLife_size, e_hp_clusterFileCollection___internalTraverseLogic__updateCounters);
  assert(obj_counter.cnt_data > 0);
  assert(obj_counter.cnt__data_x_cluster_x_metPri__x__metInside > 0);
  assert(obj_counter.cnt__cluster_x_metPri__x__metInside > 0);
  const uint cnt_ccm = (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;     assert(cnt_ccm > 0);
  //!
  //! Allcoate space:
  matResult__result_x_ccm = initAndReturn__s_kt_matrix(obj_counter.cnt__data_x_cluster_x_metPri__x__metInside, cnt_ccm);
  matResult__data_x_simAndAlg__time = initAndReturn__s_kt_matrix(obj_counter.cnt_data, obj_counter.cnt__cluster_x_metPri__x__metInside);    
  assert(matResult__result_x_ccm.nrows > 0);     assert(matResult__data_x_simAndAlg__time.nrows > 0);
  //! 
  //! Specify strings for non-complex cases:
  for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
    set_stringConst__s_kt_matrix(&matResult__result_x_ccm, ccm_type_index, /*string=*/getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), /*addFor_column=*/true);
  }
#endif

  //!
  //! Allcoate (if requested) memory-space for the frequency-matrix:
  // TODO[code::bonus::conceptual]: scaffold an appraoch where we make use of jcm-appraoch and then update our "hp_clusterFileCollection" code: "Because the absolute numeric value of a given similarity score is inherently tied to its expression, we   normalize each result to the range of its output values, and   obtain a distribution of frequencies of values from 0 to 1, with a histogram bin width of 2 significant figures." <.. dicussed this with jcm: seems like he has no sound argumentaiton for why his appraoch 'is better thant he appraoch suggested by oekseht' ... ie, dealyed uft.
  s_kt_matrix_t matResult__scoreFrequency; setTo_empty__s_kt_matrix_t(&matResult__scoreFrequency); uint matResult__scoreFrequency__currentPos = 0;
  if( (obj_counter.cnt__data_x_cluster_x_metPri > 0) && (self->option_scoreFrequency__cntBuckets != 0) && (self->option_scoreFrequency__cntBuckets != UINT_MAX) )  {
    matResult__scoreFrequency = initAndReturn__s_kt_matrix(/*nrows=*/obj_counter.cnt__data_x_cluster_x_metPri, /*cnt-buckets=*/self->option_scoreFrequency__cntBuckets);
    /* //! */
    /* //! Set the string-names for the columns: */
    /* for */
  }

    //  }


  uint currentCounter__data_x_cluster_x_metPri__x__metInside = 0;

  //! --------------------------------------------------
  //!
  //! Apply logics for each data-set:
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    uint currentCounter__cluster_x_metPri__x__metInside = 0;
    uint cnt_matrices_exported = 0;
    //!
    //! Load data:
#if( __Mi_c_mapOf_realLife__isStruct == 1)
    const char *stringOf_tagSample = mapOf_realLife[data_id].file_name;
    if(stringOf_tagSample == NULL) {stringOf_tagSample =  mapOf_realLife[data_id].tag;}
    assert(stringOf_tagSample);
    const s_hp_clusterFileCollection data_obj = mapOf_realLife[data_id];
    const uint config_kMeans_nclusters = data_obj.k_clusterCount;
    assert(config_kMeans_nclusters > 0);
    const bool config__clusterConfig__inputMatrix__isAnAdjecencyMatrix = data_obj.inputData__isAnAdjcencyMatrix;
    /* if(config__clusterConfig__inputMatrix__isAnAdjecencyMatrix) { */
    /*   fprintf(stderr, "(note)\t matrix is expected to be an ajdcnecy-matrix, at %s:%d\n", __FILE__, __LINE__); */
    /*   assert(false); // FIXME: remove */
    /* } */
    if(self->config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "\n\n# New file: \"%s\"\n", stringOf_tagSample);}
    //!
    //! Set the simliarty/correlation matrix: "prior":
    s_hp_clusterFileCollection_simMetricSet_t objMetrics__prior = init__hp_clusterFileCollection_simMetricSet(data_obj.metric__beforeClust, /*includeEmptyCase=*/true);
    assert(objMetrics__prior.list_size > 0);     assert(objMetrics__prior.list != NULL);  //! whe expec thte object 'to hold content':
    //!
    //! Set the simliarty/correlation matrix: "inside-clustering":
    s_hp_clusterFileCollection_simMetricSet_t objMetrics__inside = init__hp_clusterFileCollection_simMetricSet(data_obj.metric__insideClust, /*includeEmptyCase=*/false);
    assert(objMetrics__inside.list_size > 0);     assert(objMetrics__inside.list != NULL);  //! whe expec thte object 'to hold content':
    

    //!
    //! Add string:
    set_stringConst__s_kt_matrix(&matResult__data_x_simAndAlg__time, data_id, /*string=*/(data_obj.tag) ? data_obj.tag : data_obj.file_name, /*addFor_column=*/false);
    //! 
    //! Read the input-file:
#if( __Mi_c_mapOf_realLife__isStruct == 1)
    s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
#endif
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    if(mapOf_realLife[data_id].file_name != NULL) {
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
    if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) {
      fprintf(stderr, "!!\t File=\"%s\" resulsts in emtpy matrix: pelase validate your input-file. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      continue;
    }
    assert(obj_matrixInput.matrix); assert(obj_matrixInput.matrix[0]);
    if(self->pathTo_localFolder_storingEachSimMetric != NULL) {
      if(self->isToStore__inputMatrix__inFormat__csv) { //! then we export the inptu-atmrix to the result-folder
	assert(stringOf_tagSample);
	const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
	if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name	
	else {str_suff = stringOf_tagSample;}
	char __str_resultFile__[1000] = {'\0'}; sprintf(__str_resultFile__, "%s%s__input.csv",  self->pathTo_localFolder_storingEachSimMetric, str_suff);
	export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(&obj_matrixInput, __str_resultFile__, NULL);
      }
    }

    //!
    //! Allcoate (if requested) memory-space for the frequency-matrix:
    s_kt_matrix_t matResult__scoreFrequency__local; setTo_empty__s_kt_matrix_t(&matResult__scoreFrequency__local); uint matResult__scoreFrequency__local__currentPos = 0;

    if( (self->option_scoreFrequency__cntBuckets != 0) && (self->option_scoreFrequency__cntBuckets != UINT_MAX) )  {
      assert(obj_counter.cnt__data_x_cluster_x_metPri > 0);
      assert(matResult__scoreFrequency.nrows > 0);
      //! Investigate 'if this option' has any pre-simlairty-emtrics:
      uint cnt_local = 0;
      for(uint met_prior = 0; met_prior < objMetrics__prior.list_size; met_prior++) {
	//printf("\t\t- config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__);
	//! Set the metric:
	//printf("met_prior=%u <%u, at %s:%d\n", met_prior, objMetrics__prior.list_size,  __FILE__, __LINE__);
	const s_kt_correlationMetric_t corrMetric_prior = objMetrics__prior.list[met_prior]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
	cnt_local += (corrMetric_prior.metric_id != e_kt_correlationFunction_undef);
      }
      if(cnt_local > 0) {
	matResult__scoreFrequency__local = initAndReturn__s_kt_matrix(/*nrows=*/cnt_local, /*cnt-buckets=*/self->option_scoreFrequency__cntBuckets);
      }
    }


    //! 
    //! Specify the clustering-algorithm to use:
    //! Note: we have chosen to 'palce' the clustierng-algorithm 'outsid ethe otehr loops' as the clustering-algorithm is (in reseharc-ltiterature) oftne the mina-focus (ie, in cotnrast to our observaiotns indaiting that the silmairty-emtric is of even higher improtance wrt. the clsuter-reuslts).
    // FIXME[article]:  ... visually evlauate [ªbove] 'assumption' ... using oru web-itnefac e... and then update both [ªbove] aand oru aritlce-text.
    const bool clustAlg__inlcudeEmptyCases = true;
    s_hp_clusterFileCollection__clusterAlg_t obj_clusterAlg = init__s_hp_clusterFileCollection__clusterAlg_t(data_obj.clusterAlg, /*includeEmptyCase=*/clustAlg__inlcudeEmptyCases); //! wher ehte latter is set to true as we (a) are itnereted in evlauting the 'explcit' time-cost of clsuteirng (by not doing it) and (b) as we asusme it is of itnerest to 'not only' compute wrt. the simalrity-amtrix.
    /* fprintf(stderr, "data_obj.clusterAlg=%u, at %s:%d\n", data_obj.clusterAlg, __FILE__, __LINE__);  */
    /* fprintf(stderr, "data[%u]\tdims=[%u, %u, %u]=|%u|, at %s:%d\n",  */
    /* 	    data_id, */
    /* 	    obj_clusterAlg.list_size, objMetrics__prior.list_size, objMetrics__inside.list_size,  */
    /* 	    obj_clusterAlg.list_size * objMetrics__prior.list_size * objMetrics__inside.list_size,  */
    /* 	    __FILE__, __LINE__); */
#ifndef NDEBUG
    if(data_obj.clusterAlg == e_hp_clusterFileCollection__clusterAlg_kMeans) {
      const uint def_size = 3; //! ie, the asusmption based on the current cofniguraiton.
      if(clustAlg__inlcudeEmptyCases == false) {assert(obj_clusterAlg.list_size == def_size);}  
      else {assert(obj_clusterAlg.list_size == (1+def_size));}  
    }
    assert(obj_clusterAlg.list != NULL);     assert(obj_clusterAlg.list_size > 0); //! ie, as we expect the object to have been set.
#endif

    //! 
    { //! Iterate throguh the clustering-algorithms:
#include "traverse__s_hp_clusterFileCollection_traverseSpec__stub__traverseCases.c"
    }
    //! 
    if(matResult__scoreFrequency__local.nrows > 0) {
      //! Finalize the udpate the of he local frquency-amtrix, and in this contect update the 'glboally mergted' frqunecy-amtrix:
#include "traverse__s_hp_clusterFileCollection_traverseSpec__stub__scoreFrequency__local__finalize.c"
      //!
      //! De-allcoate hte local object:
      free__s_kt_matrix(&matResult__scoreFrequency__local);
    }
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&objMetrics__prior);
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&objMetrics__inside);
    //printf("- de-allcoates: objMetrics__inside, at %s:%d\n", __FILE__, __LINE__);
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&obj_clusterAlg);
#endif
    //! De-allcoat ethe input-file-matrix:
    free__s_kt_matrix(&obj_matrixInput);
  }


  if(fileDesriptorOut__clusterResults) {
#if( __Mi_c_mapOf_realLife__isStruct == 1)    
    //!
    //! Write out a mapping-file descriibng the 'properties' of [ªbov€] data-set:
    writeOut__mappingFile__s_hp_clusterFileCollection_t(fileDesriptorOut__clusterResults, mapOf_realLife, mapOf_realLife_size);
#endif
  }


#if( __Mi_c_mapOf_realLife__isStruct == 1)
  if(matResult__scoreFrequency.nrows > 0) {
    //!
    //! Compute a simlairty-amtrix for the object and export: export for for 'input', sim-metric(input), sim-metric(transposed(input)), and 'masked versions' of the latter:
    // TODO: cosnider writing a modiciton of [below] funciton 'dedicated for this purpose' ... using a diffenre tlcuster-alg and testing a set of differetn asusmptions ... eg, wrt. different 'silamirty-schems' for the simalrity-emtrics.
    applyDefaultClustering__andExport(self, &matResult__scoreFrequency, /*isTo_transposeMatrix=*/false, /*tag=*/"frequencyMatrix_global");      
    free__s_kt_matrix(&matResult__scoreFrequency);
  }


  {
    assert(matResult__result_x_ccm.nrows > 0);     assert(matResult__data_x_simAndAlg__time.nrows > 0);
    const char *input_file = self->nameOf_resultFile__clusterMemberships;
    { //! Then we erite out the result-files: for CCM "matResult__result_x_ccm"
      { //! JS: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.js", input_file, "dataSiAlg__ccm"); 
	export__singleCall__toFormat_javaScript__s_kt_matrix_t(&matResult__result_x_ccm, fileName_local, NULL, "dataSiAlg__ccm");
      }
      { //! TSV: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.tsv", input_file, "dataSiAlg__ccm"); 
	export__singleCall__s_kt_matrix_t(&matResult__result_x_ccm, fileName_local, NULL);
      }      
    }
    { //! Then we erite out the result-files: for "exeuction-time"
      { //! JS: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.js", input_file, "data_x_simAndAlg"); 
	export__singleCall__toFormat_javaScript__s_kt_matrix_t(&matResult__data_x_simAndAlg__time, fileName_local, NULL, "data_x_simAndAlg");
      }
      { //! TSV: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.tsv", input_file, "data_x_simAndAlg"); 
	export__singleCall__s_kt_matrix_t(&matResult__data_x_simAndAlg__time, fileName_local, NULL);
      }
    }
  }
  if(self->postProcessing__clusterResultMatrices) {
    if(self->pathTo_localFolder_storingEachSimMetric) {
      //! Then we apply clustienr and genrate result-matrices:
      //! ---
      applyDefaultClustering__andExport(self, &matResult__result_x_ccm, /*isTo_transposeMatrix=*/false, /*tag=*/"matResult__result_x_ccm");
      applyDefaultClustering__andExport(self, &matResult__result_x_ccm, /*isTo_transposeMatrix=*/true, /*tag=*/"matResult__result_x_ccm__transposed");
      //! --- 
      applyDefaultClustering__andExport(self, &matResult__data_x_simAndAlg__time, /*isTo_transposeMatrix=*/false, /*tag=*/"matResult__data_x_simAndAlg__time");
      applyDefaultClustering__andExport(self, &matResult__data_x_simAndAlg__time, /*isTo_transposeMatrix=*/true, /*tag=*/"matResult__data_x_simAndAlg__time__transposed");
      //! ------------------------------------------------------------------------------------------
    }
  }
#endif


  //!
  //! De-allcoate and clsoe file-desicprtors:
  assert(fileDesriptorOut__clusterResults);
  fclose(fileDesriptorOut__clusterResults);
  assert(fileDesriptorOut__clusterResults__deviation);
  fclose(fileDesriptorOut__clusterResults__deviation);
  free__s_kt_matrix(&matResult__result_x_ccm);
  free__s_kt_matrix(&matResult__data_x_simAndAlg__time);

  //! ------------------------------------
  //! @return 
  return true;
}
