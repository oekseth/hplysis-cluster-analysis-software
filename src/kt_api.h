#ifndef kt_api_h
#define kt_api_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_api
   @brief The function-api to our high-performance clustering-methods (oekseth, 06. setp. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "e_kt_correlationFunction.h"
// FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty'
#include "e_kt_eigenVectorCentrality.h"
#include "kt_centrality.h"
#include "kt_clusterAlg_hca__node.h"
#include "kt_matrix.h"
#include "e_kt_clusterComparison.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_clusterAlg_hca_resultObject.h"
#include "kt_clusterAlg_SOM_resultObject.h"
#include "e_kt_clusterAlg_hca_type.h"
#include "kt_clusterAlg_fixed__alg__miniBatch.h"
#include "kt_api__centerPoints.h"
#include "kt_matrix_cmpCluster.h"
#include "kt_clusterAlg_config__ccm.h"
#include "kt_randomGenerator_vector.h"
#include "kt_api_config.h"
#include "hp_distanceCluster.h"
/* //! An 'tuomateically called' inti-funciton for Shared-library-file (eg, *.so on Linux) genrated for 'this' (oekseth, 06. des. 2016). */
/* static __attribute__((constructor)) void hpLysis__init(void) { */
/*   fprintf(stderr, "(info)\t intiates the hpLysis memory-object, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
/*   hpLysis__globalInit__kt_api(); */
/* } */
/* //! An 'tuomateically called' inti-funciton for Shared-library-file (eg, *.so on Linux) genrated for 'this' (oekseth, 06. des. 2016). */
/* static __attribute__((destructor)) void hpLysis__fini(void) { */
/*   fprintf(stderr, "(info)\t frees the hpLysis memory-object, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
/*   hpLysis__globalInit__kt_api(); */
/* } */





//! @return the mean of the list
t_float mean(const uint n, t_float *x);
//! @return the median of the list
t_float median (const uint n, t_float *x);

//! @return true if 'empty valeus' are foudn in both obj_1 and obj_2 (oekseth, 06. des. 2016).
bool hasEmptyValues__auxFunction__kt_api(const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2);



/**
     @brief provide logics for translating a set of features into a distance-matrix.
     @param <nrows> The number of rows in the gene expression data matrix (i.e., the number of genes)
     @param <ncolumns> The number of columns in the gene expression data matrix (i.e., the number of microarrays)
     @param <data> The array containing the 'input local distance-matrix' (eg, the gene expression data).
     @param <mask_uint> This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     @param <weights> The weights that are used to calculate the distance. The length of this vector is equal to the number of columns if the distances between genes are calculated, or the number of rows if the distances between microarrays are calculated.
     @param <transpose> If transpose is equal to zero, the distances between the rows is calculated. Otherwise, the distances between the columns is calculated. The former is needed when genes are being clustered; the latter is used when microarrays are being clustered.
     @return a new-allocated (and computed) distance-matrix.
     @remarks the distance-matrix routine calculates the distance matrix between genes or microarrays using their measured gene expression data. Several distance measures can be used.
     - The routine returns a pointer to a ragged array containing the distances between the genes.
     - As the distance matrix is symmetric, with zeros on the diagonal, only the lower triangular half of the distance matrix is saved.
     - The distancematrix routine allocates space for the distance matrix. If the parameter transpose is set to a nonzero value, the distances between the columns (microarrays) are calculated, otherwise distances between the rows (genes) are calculated.
     - If sufficient space in memory cannot be allocated to store the distance matrix, the algorithm will throw an expection, where details are found in the "list_generic" tempalte-class.
  **/
t_float** distancematrix(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, uint **mask_uint, t_float weights[], const uint transpose);

//! A wrapper to call the "distancematrix" function using "s_kt_matrix" objects (oekseth, 06. otk. 2016).
bool kt_matrix__distancematrix(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2, s_kt_matrix_t *obj_result, const s_kt_api_config_t config); //const bool isTo_transposeMatrix);


/**
   @brief copmute the correlation-score for a given matrix.
   @param <metric_id> is the type of correlation to apply
   @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
   @param <nrows> is the number of rows
   @param <ncols> is the number of columns
   @param <data> is the data-set to be evaluated
   @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
   @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
   @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
   @param <cutoff> the curt-off which is used in computation of weights, ie, in the post-processing after the correlation-metric is applied
   @param <exponent> The exponent to be used to calculate the weights. 
   @param <isTo_use_fastFunction> which if set to false imples that we use the 'legacy' slow function.
   @param <CLS> which is the block-sise of the tiles to use
   @param <iterationIndex_2> which if set 'narrows' the ros (of columns) to be indvestigated for data2.
   @return a pointer to a newly allocated array containing the calculated weights for the rows (iftranspose==0) or columns (if transpose==1). 
   @remarks This function calculates the weights using the weighting scheme proposed by Michael Eisen: 
   - w[i] = 1.0 / sum_{j where d[i][j]<cutoff} (1 - d[i][j]/cutoff)^exponent
   - where the cutoff and the exponent are specified by the user.
**/
t_float* calculate_weights(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const t_float cutoff, const t_float exponent);
//, const uint CLS, const uint iterationIndex_2
/**
   @param <obj_metric> is the matrix to apply.
   @param <obj_1> is the object to comptue weights for.
   @param <obj_result> expected to be an empty object: to access the weights (comptued for the rows) c
   @return true if the oepration awas a success.
 **/
bool kt_matrix__calculate_weights(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result, const bool isTo_transposeMatrix, const t_float cutoff, const t_float exponent);

/**
   @brief identifes the distance between two clusters.
   @param <metric_id> is the type of correlation to apply
   @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
   @param <nrows> is the number of rows
   @param <ncols> is the number of columns
   @param <data> is the data-set to be evaluated
   @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
   @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
   @param <n1> is the number of elements in the first clsuter/matrix.
   @param <n2> is the number of elements in the first clsuter/matrix.
   @param <index1> identifies the vertices/rows in the first cluster
   @param <index2> identifies the vertices/rows in the second cluster
   @param <method> idneties the method to be sued wrt. comparion between teh lucters
   @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
   @remarks for the "methdo" param we support the following optioins:
   - method=='a': the distance between the arithmetic means of the two clusters
   - method=='m': the distance between the medians of the two clusters
   - method=='s': the smallest pairwise distance between members of the two clusters
   - method=='x': the largest pairwise distance between members of the two clusters
   - method=='v': average of the pairwise distances between members of the clusters
 **/
t_float clusterdistance(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t method, const uint transpose);
/**
   @brief identify teh clsuter-distance between two sets of valeus (oekseth, 06. nov. 2016).
   @param <obj_metric> is the metric to be used wrt. the ddistnace-comparison
   @param <obj_1> is the data-object which hold the matrix to evlauate
   @param <obj_indexOfValues_1> is epxected to hold a 'mapping' (ie, an index-table) for the rows to be part of cluster(1)
   @param <obj_indexOfValues_2> is epxected to hold a 'mapping' (ie, an index-table) for the rows to be part of cluster(2)
   @param <method_id> descirbes the type of cluster-comparison to be sued/applied
   @param <isTo_transposeMatrix> to be set to true if the data is to be transposed 'before evlauation': if so then we expec thte inex-tables to desribe the columsn (and Not the rows).
   @param <scalar_result> is the scalar flaoting-poitn result of the comparison (bertween the two index-sets in quesiton).
   @remarks the obj_indexOfValues_1 and obj_indexOfValues_2 input-arpaemter-vairalbes may be build using the "push_row_member__s_kt_member(..)" functioni, a deufntion defined in our "kt_matrix.h" header-file.
 **/
bool kt_matrix__clusterdistance(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_indexOfValues_1, const s_kt_matrix_t *obj_indexOfValues_2, const e_kt_clusterComparison_t method_id, const bool isTo_transposeMatrix, t_float *scalar_result);

  /* /\** */
  /*    @brief return a reference to the corresponding function. */
  /*    @param <dist> identifes the function-reference to retreive/return */
  /*    @return a fucntion-reference. */
  /* **\/ */
  /* static t_float(*setmetric(char dist)) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) {  */

  /*   assert(false); // FXIME: udpate ... using our to-be-writtne Perl-data-set. */

  /*   /\* switch(dist)  *\/ */
  /*   /\*   { *\/ */
  /*   /\*   case 'e': return &euclid; *\/ */
  /*   /\*   case 'b': return &cityblock; *\/ */
  /*   /\*   case 'c': return &correlation; *\/ */
  /*   /\*   case 'a': return &acorrelation; *\/ */
  /*   /\*   case 'u': return &ucorrelation; *\/ */
  /*   /\*   case 'x': return &uacorrelation; *\/ */
  /*   /\*   case 's': return &spearman; *\/ */
  /*   /\*   case 'k': return &kendall; *\/ */
  /*   /\*   default: return &euclid; *\/ */
  /*   /\*   } *\/ */
  /*   return NULL; /\* Never get here *\/ */
  /* } */


  /**
     @brief compute the kmeans clustering of the vertices.
     @remarks 
     # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
     # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
     # "":
  **/
#ifndef SWIG
int kmeans_or_kmedian(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, float weight[], const uint transpose, const uint npass, float** cdata, char **cmask, uint clusterid[], float* error, uint tclusterid[], uint counts[], uint mapping[], const bool isTo_use_mean);
#endif // #ifndef SWIG
/**
   @brief comptue k-means clsuters using Lloyds k-means EM clsutering-algorithm
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nclusters> is the number of clusters to sub-divide the data-set into.   
   @param <npass> is the number of 'passes' to be sued in the k-means algorithm
   @param <isTo_useMean>  which if set to false implies that "median" is used.
   @param <config> is the genrelaized configuraiton for this funcciont
   @return true upon success.
**/
bool kt_matrix__kmeans_or_kmedian(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const uint npass, const bool isTo_useMean, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init);
/**
   @brief comptue k-means clsuters using the Mini-batch k-means clsutering-algorithm
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nclusters> is the number of clusters to sub-divide the data-set into.   
   @param <config> is the genrelaized configuraiton for this funcciont
   @return true upon success.
**/
bool kt_matrix__kmeans_nonDefaultAlg__miniBatch(const s_kt_clusterAlg_fixed__alg__miniBatch_t config_kMeans, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init);
  /**
     @brief compute the clustering of the vertices, using a a 'fixed number of clsuters'.
     @remarks The kcluster routine performs k-means or k-median clustering on a given set of elements, using the specified distance measure. The number of clusters is given by the user. Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
     @remarks
     - For other values of dist, the default (Euclidean distance) is used.
     # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
     # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
     # "iffound": The number of times the optimal clustering solution was found. The value of ifound is at least 1; its maximum value is npass. If the number of clusters is larger than the number of elements being clustered, *ifound is set to 0 as an error code. Ifa memory allocation error occurs, *ifound is set to -1.
     # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
     # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
  **/
void kcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nclusters, uint nrows, uint ncolumns, float** data, char** mask, float weight[], uint transpose, uint npass, char method, uint clusterid[], float* error, uint* ifound);
/**
   @brief comptue k-means or k-mesian clusters (oekseth, 06. nov. 2016).
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_result_clusterId_columnValue> if not set to NULL then this object hold data of the cluster-distance to each of the columns
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <isTo_useMean> which if set to false impleis that we use Median (instead or mean (or: average));
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
**/
bool kt_matrix__kluster(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters, const uint npass, const bool isTo_useMean, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init);



/**
   @brief use disjoitn-froests combined with 'masks' to idneitfy clusters (oekseth, 06. des. 2016).
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_result_clusterId_columnValue> if not set to NULL then this object hold data of the cluster-distance to each of the columns
   @param <threshold_min> which is the min-thresohld to be sued: if the latter is to be ingored then set "threshold_min = T_FLOAT_MIN_ABS"
   @param <threshold_max> which is the max-thresohld to be sued: if the latter is to be ingored then set "threshold_min = T_FLOAT_MAX"
   @param <isTo_adjustToRanks_beforeTrehsholds_forRows> which if set to true implies that we 'replace' the valeus (in each row) by the rows rank
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
   @remarks Idea:
   -- instead of k-means 'count the number of disjoitn-forests at different rank-thresholds' use 'the groups which are idneited as an idnication of central/signficant entries'. In this cotnext a gernalized approacgh (when features at different 'scales' are used) is to a) is to construct a copy of the matrix and apply diffnere rank-thresholds, ie, before we then make use of our code-support for seperate comptuations of disjoitn-froests': the latter 'use-case' is supported through our "isTo_adjustToRanks_beforeTrehsholds_forRows" input-parameter. 
   @remarks in cotnrast to k-means clustering the strenght of disjoint-forest-sepration cosnerns:
   -- exeuction-time: result is comptued in infinitniteestimal time (when compared to k-means clsutering)
   -- accuracy: if the inptu-data may be correctly seperated throguht its values (eg, wrt. its ranks) we assert that 'the dsijotinf-roest-procedure may procue as-correct results as k-means clsuter-classificoatns'
   -- usability: as there is no need to specfiy inptu-parameters (eg, "niter" and "k clsuters to compute") we hypothese that the accurate results (produced by this fucntion) may be mroe straight-forward to produce (eg, when comapred to k-means API-cofnguraiton-calls).
**/
bool kt_matrix__disjointClusterSeperation(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, t_float threshold_min, t_float threshold_max, const bool useRelativeScore_betweenMinMax, const bool isTo_adjustToRanks_beforeTrehsholds_forRows, s_hp_distanceCluster_t *objMetric__postMerge, const s_kt_api_config_t config);



  /**
     @brief compute the k-medioids
     @remarks "For some data sets there may be more than one medoid, as with medians. A common application of the medoid is the k-medoids clustering algorithm, which is similar to the k-means algorithm but works when a mean or centroid is not definable. This algorithm basically works as follows. First, a set of medoids is chosen at random. Second, the distances to the other points are computed. Third, data are clustered according to the medoid they are most similar to. Fourth, the medoid set is optimized via an iterative process" ["https://en.wikipedia.org/wiki/Medoid"]. Key-poitns:
     - the algorithm "is more robust to noise and outliers as compared to k-means because it minimizes a sum of pairwise dissimilarities instead of a sum of squared Euclidean distances" ["https://en.wikipedia.org/wiki/K-medoids"]. The latter is due to the training-phase in the SOM: "because in the training phase weights of the whole neighborhood are moved in the same direction, similar items tend to excite adjacent neurons" ["https://en.wikipedia.org/wiki/Self-organizing_map"]. In breif, "SOM may be considered a nonlinear generalization of Principal components analysis (PCA)" ["https://en.wikipedia.org/wiki/Self-organizing_map"].
     - 
     @remarks from the documetnaiton:
     - The kmedoids routine performs k-medoids clustering on a given set of elements, using the distance matrix and the number of clusters passed by the user. 
     - Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
     @remarks a subset of the parameters are described below:
     # "nelements": The number of elements to be clustered.
     # "distmatrix":  The distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
     - number of rows is nelements, number of columns is equal to the row number
     # "npass": The number of times clustering is performed. 
     - Clustering is performed npass  times, each time starting from a different (random) initial assignment of genes to clusters. 
     - The clustering solution with the lowest within-cluster sum of distances is chosen.
     - Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": the cluster-identites assicated to each vertex
     - On input, ifnpass==0, then clusterid contains the initial clustering assignment from which the clustering algorithm starts; all numbers in clusterid should be between zero and nelements-1 inclusive. Ifnpass!=0, clusterid is ignored on input.
     - On output, clusterid contains the clustering solution that was found: clusterid contains the number of the cluster to which each item was assigned. On output, the number of a cluster is defined as the item number of the centroid of the cluster.
     # "error": The sum of distances to the cluster center of each item in the optimal k-medoids clustering solution that was found.
     # "iffound": If kmedoids is successful: the number of times the optimal clustering solution was found. 
     - The value of ifound is at least 1; its maximum value is npass. 
     - If the user requested more clusters than elements available, ifound is set to 0. If kmedoids fails due to a memory allocation error, ifound is set to -1.
  **/
void kmedoids(uint nclusters, uint nelements, float** distmatrix, uint npass, uint clusterid[], t_float* error, uint* ifound);
/**
   @brief comptue k-means or k-mesian clusters (oekseth, 06. nov. 2016).
   @param <obj_1> hold the inptu-data: input-matrix is expected to be an adjcency-matirx, ie, a matrix with the nsame number of columsn and rows.
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_result_clusterId_columnValue> if not set to NULL then this object hold data of the cluster-distance to each of the columns
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @param <isTo_transposeMatrix> which if set to true impleis that we first trnaspose the matrix (before comptuing the clusters)
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <isTo_useMean> which if set to false impleis that we use Median (instead or mean (or: average));
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
**/
bool kt_matrix__kmedoids(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters, const uint npass, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init);


/**
   @brief compute dynamic-k-means clustering (oekseth, 06. des. 2016)
   @param <alg_type> is the type of k-means-permtuation-algorithm to use
   @param <obj_metric> is the type of metric to use 'inside' the clustering
   @param <obj_1> is the data-set to cluster
   @pram <opt_matrix_filtered> optional: if set then we use the latter amtrix to infer the disjoint-forests, ie, 'ingoreding the input-arpemters of "threshold_min, threshold_max" (oekseht, 06. feb. 2017).
   @param <obj_result> store the result of the clustering
   @param <npass> is the maximal number of 'passes' to use iot find an 'ideal sollution'.
   @param <threshold_min> which is the min-thresohld to be sued: if the latter is to be ingored then set "threshold_min = T_FLOAT_MIN_ABS"
   @param <threshold_max> which is the max-thresohld to be sued: if the latter is to be ingored then set "threshold_min = T_FLOAT_MAX"
   @param <isTo_adjustToRanks_beforeTrehsholds_forRows> which if set to true implies that we 'replace' the valeus (in each row) by the rows rank
   @param <countThreshold_min> is the min-number of vertex-elements to be in a disjoitn-clsuter 'for the clsuter to be interesting enough to be incldued in the k-cluster-count';
   @param <countThreshold_max> is the max-number of vertex-elements to be in a disjoitn-clsuter 'for the clsuter to be interesting enough to be incldued in the k-cluster-count';
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success
   @remarks idea is to appxoximat enumber of disjoitn-sets through use of dsijoitn-forest-algorithms and therafre apply a more sensitive/accurate clsuter-algorithm
   @remarks in this proceudre we identify the 'k-count' of clsutering using the 'mask-proerpties (ie, "threshold_min" and "threshold_max") combined with the disjoitn-filrter-appraoch (where "countThreshold_min" and "countThreshold_max" are used to identify 'when a clsuter has a size relfecting an itnersting proerpty')
 **/
bool kt_matrix__kCluster__dynamicDisjoint(const e_kt_api_dynamicKMeans_t alg_type, const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *opt_matrix_filtered, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_kt_matrix_t *obj_result_clusterId_columnValue,  const uint npass, t_float threshold_min, t_float threshold_max, const bool useRelativeScore_betweenMinMax, const uint countThreshold_min, const uint countThreshold_max, const s_kt_api_config_t config);

  /**
     @brief combined a 'trainer' and a 'model-applicaiton' to orngaise/cluster a dataset.
     @remarks The somcluster routine implements a self-organizing map (Kohonen) on a rectangular grid, using a given set of vectors. The distance measure to be used to find the similarity between genes and nodes is given by dist.
     - "SOM forms a semantic map where similar samples are mapped close together and dissimilar ones apart" ["https://en.wikipedia.org/wiki/Self-organizing_map"]
     @remarks in below we describe a few number of the parameters:
     # "nxgrid": The number of grid cells horizontally in the rectangular topology of clusters.
     # "nygrid": The number of grid cells horizontally in the rectangular topology of clusters.
     # "inittau": The initial value of tau, representing the neighborhood function.
     # "niter": The number of iterations to be performed.
     # "celldata": float[nxgrid][nygrid][ncolumns] iftranspose==0; float[nxgrid][nygrid][nrows]    iftranpose==1; The gene expression data for each node (cell) in the 2D grid. This can be interpreted as the centroid for the cluster corresponding to that cell. If celldata is NULL, then the centroids are not returned. Ifcelldata is not NULL, enough space should be allocated to store the centroid data before callingsomcluster.
     # "clusterid": iftranspose==0; int[ncolumns][2] iftranspose==1; For each item (gene or microarray) that is clustered, the coordinates of the cell in the 2D grid to which the item was assigned. Ifclusterid is NULL, the cluster assignments are not returned. Ifclusterid is not NULL, enough memory should be allocated to store the clustering information before calling somcluster.
  **/
void somcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask,  t_float *weight, uint transpose, uint nxgrid, uint nygrid, t_float inittau, const uint niter, t_float*** celldata, uint **clusterid);
/**
   @brief comptue a Self Orgnaising Map (SOM) (oekseth, 06. nov. 2016).
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nxgrid> is the number of cluster-'postiosn' alogn the first diemsnion
   @param <nygrid> is the number of cluster-'postiosn' alogn the second diemsnion
   @param <inittau> The initial value of tau, representing the neighborhood function.
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
**/
bool kt_matrix__som(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_SOM_resultObject_t *obj_result, uint nxgrid, uint nygrid, const t_float inittau, const uint npass, const s_kt_api_config_t config);
/**
   @brief comptue teh ROC for a matrix.
   @param <arrOf_truth> is the truth-values to compare with.
   @param <nrows>  is the number of rows in the input-matrxi
   @param <arrOf_truth_size>  is the num,ber of column in arrOf_truth and data
   @param <data>  is the data we comptue for: is the data to compare the truth-values with: if arrOf_masks is not set, then we assume that T_FLOAT_MAX or FLT_MIN is used to 'mark' empty values.
   @param <mask> which if set is used to idneitfy the set of interesting masks.
   @param <isTo_useImplictMask> which if set to true impleis that we use implcit masks
   @param <arrOf_minThresholds> if set is used to infer the poitns which are to be evaluated/used.
   @param <arrOf_minThresholds_size> is the number of elments in the feature-row: expected to equal arrOf_truth_size
   @param <matrixOfResult__spec> is the idnetifed/'found' "specificity" value.
   @param <matrixOfResult__sens> is the idnetifed/'found' "sensitivity" value.
 **/
void get_matrix_ROC__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const char *mask, const bool isTo_useImplictMask, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens);


/**
   @namespace graphAlgorithms_svd
   @brief provide implementaitons of algorithms using the "Single VAlue Decomposition" (SVD) as a 'basis'.
   @remarks the "c clustering library" is used as template.
   @author Ole Kristian Ekseth (oekseth).
 **/
//namespace graphAlgorithms_svd {


/* /\** */
/*    @brief Determine the singular value decomposition (SVD) by a real m by n rectangular matrix. */
/*    @param <m> the number of rows of A (and u); */
/*    @param <n> is the number of columns of A (and u) and the order of v; */
/*    @param <u> contains the rectangular input matrix A to be decomposed: in the result represents/holds the orthogonal column vectors of the decomposition; */
/*    @param <w> result: the non-negative diagonal singular values; */
/*    @param <vt> result: the orthogonal column-matrix of the decomposition; */
/*    @return true upon succsess. */
/* **\/ */
/*   uint svd(int m, uint n, float** u, float w[], float** vt); */
/**
   @brief use SVD to perform principal components analysis of a real nrows by ncolumns rectangular matrix.
   @param <nrows> The number of rows in the matrix u.
   @param <ncolumns> The number of columns in the matrix v.
   @param <u> is the distance-matricx: on input, the array containing the data to which the principal component analysis should be applied. The function assumes that the mean has already been subtracted of each column, and hence that the mean of each column is zero. On output, see below.
   @param <v> (not used in input)
   @param <mapOf_w> (not used in input)
   @return true upon success
   @remarks 
   On output:   
   # The eigenvalues of the covariance matrix are returned in w.
   # Ifnrows >= ncolumns, then   
   - u contains the coordinates with respect to the principal components;
   - v contains the principal component vectors.         
   # Ifnrows < ncolumns, then   
   - u contains the principal component vectors;
   - v contains the coordinates with respect to the principal components.   
   # For the above sets, the dot product v . u reproduces the data that were passed in u.
   # The arrays u, v, and w are sorted according to eigenvalue, with the largest eigenvalues appearing first.   
 **/
  const bool pca(int nrows, int ncolumns, t_float** u, t_float** v, t_float* w);


/**
   @brief copmtue centrality, centrlaity-measures speifeid in the self object.
   @param <matrix> is the input-data to compute for: an adjencey matrix hwere weights may be used to 'describe' distance: if a relationship is Not to be inlcucded, the set toe 'distance' to "0";
   @param <nrows> is the number of rows in the matrix
   @param <ncols> is the number of columns in the matrix: as we expect an ajdcency-matrix ncols==nrows is what we expect
   @param <arrOf_result> hold the set of centrliaty-scores identifed in this fucntion.
   @param <self> is the configuration- and sepficiaotn-object for copmtaution of centrlaity-scores
   @return true if operation was a success, ie, if configuraiton confired to our possible range of 'value-support'.
 **/
const bool computeVector_centrality(t_float **matrix, const uint nrows, const uint ncols, t_float *arrOf_result, const s_kt_centrality_config_t self);

  /**
     @brief comptues the clusters using a specified Hierarhcical Clustering Algorithm (HCA).
     @param <metric_id> is the type of correlation to apply
     @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
     @param <nrows> is the number of rows
     @param <ncols> is the number of columns
     @param <data> is the data-set to be evaluated
     @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
     @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
     @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
     @param <method> which describes the tree-clsutering procedure to use.
     @param <distmatrix> which if Not set to NULL is the distance-matrix we use for comptuations.
     @remarks The treecluster routine performs hierarchical clustering using pairwise single-, maximum-, centroid-, or average-linkage, as defined by method, on a given set of gene expression data, using the distance metric given by dist. If successful, the function returns a pointer to a newly allocated Tree struct containing the hierarchical clustering solution, and NULL ifa memory error occurs. The pointer should be freed by the calling routine to prevent memory leaks.
     @remarks in below we describe a few number of the parameters:
     # "method": Defines which hierarchical clustering method is used:
     - method=='s': pairwise single-linkage clustering
     - method=='m': pairwise maximum- (or complete-) linkage clustering
     - method=='a': pairwise average-linkage clustering
     - method=='c': pairwise centroid-linkage clustering
     - For the first three, either the distance matrix or the gene expression data is sufficient to perform the clustering algorithm. For pairwise centroid-linkage clustering, however, the gene expression data are always needed, even if the distance matrix itself is available.
     # "distmatrix": The distance matrix. Ifthe distance matrix is zero initially, the distance matrix will be allocated and calculated from the data by treecluster, and deallocated before treecluster returns. Ifthe distance matrix is passed by the calling routine, treecluster will modify the contents of the distance matrix as part of the clustering algorithm, but will not deallocate it. The calling routine should deallocate the distance matrix after the return from treecluster.
     # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
     # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
     @return A pointer to a newly allocated array of Node structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
  **/
Node_t* treecluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, float** data, uint** mask, float weight[], uint transpose, char method, float** distmatrix);
/**
   @brief comptue a hiearhcical tree (or: dendogram)
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <method_id> is the hierahricla clsuter algorithm (HCA) to apply.
   @param <config> provides variaous cofniguraiton-proeprties for the cluster-call.
   @return true upon success.
**/
bool kt_matrix__hca(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_hca_resultObject_t *obj_result, const e_kt_clusterAlg_hca_type_t method_id, const s_kt_api_config_t config); //, const bool isTo_transposeMatrix);

/**
   @brief identify the number of disjoitn clusters in a matrix (oekseth,. 06. okt. 2016)
   @param <nrows> is the number of row in the amtrix
   @param <ncols> is the number of columns in the matrix
   @param <matrix> is score-matrix of vertices: note that oru assumption of 'score matrix' is a generalizaiton of an adjcency-matrix, ie, where '0' is considered to reflect a relationship: in order to validate that thios interpretation is proerply understood we inlcud ehte "isTo_useImplictMask", where this proerpty si to be set to true if "mask == NULL", ie, to increase thte change that the funciton is properly used.
   @param <mask> if set then we combien the '0' filter with the "mask[i][j] == 1" test
   @param <isTo_useImplictMask> which if set to true impleis that we investigate for valeus which have "T_FLOAT_MAX" (and if a cell is set to the altter valeu then we asusme the lvaue is Not of interest);
   @param <countThreshold_min> which is the numumum number of vertices which are to be in a given "idsjoitn forest" (ie, for the forest to be 'counted'): ignroed if countThreshold_min == countThreshold_max
   @param <countThreshold_max> similar to "countThreshold_min" with difference that we evlauate for the max-count:  ignroed if countThreshold_min == countThreshold_max
   @param <inputMatrix__isAnAdjecencyMatrix> which is used to investgate/'find' the 'interpretiaotn-case' wrt. the matrix-names-space to use
   @return the number of interesting disjoitn clsuters: if a critial erorr we return UINT_MAX in NDEBUG while call an "assert(false);" in #ifndef NDEBUG
   @remarks 
   - if valeu-fitlering is of itnerest then we suggest using oru rich library of mask-filters, eg, wrt. functions found in our "mask_api.h";
   - no-match: use T_FLOAT_MAX if "mask == NULL", ie, a '0' is interpred as a relationship (ie, as stated in above paramter-description);
 **/
uint find_cntClusters_inMatrix__kt_api(const uint nrows, const uint ncols, t_float **matrix, char **mask, const bool isTo_useImplictMask, const uint countThreshold_min, const uint countThreshold_max, const bool inputMatrix__isAnAdjecencyMatrix);



/**
   @brief compute Kruskal MST using a matrix obj_1 as input (oekseth, 06. feb. 2017).
   @param <obj_1> is the input-matrix: expected to be an adjcency-matrix, ie, where matrix[i][k] describes the dsitance between vertex(i)--vertex(k) (ie, in cotrnast to ffeature-matrix where the namespace of the rows and columns differs)
   @param <obj_result_hca> if set is updated with the consturcted/inferred tree of relationiships
   @param <obj_result_kMeans> if set is updated witht the distjoitn set of vertices.
   @param <config> if used then we use a mofidefed Kruskal-impelmetnation iot. try optmising the inferred set of clsuters: to split the MST into descrptive sub-units cahracterisisgn core-traits of the entwork
   @param <isAn__adjcencyMatrix> a contral-parameter: used to make the caller explcitly aware of oru expectiaont that the matrix is an ajdency-matrix: if set to false an error is returend.
   @return true if the cofngiruaiton went smooth.
   @remarks in thsi implementaiotn we asusme taht "obj_1" is a matrix with a large number of 'holes' (expected to be described using the "T_FLOAT_MAX" value). If the latter asusmption does Not hold then we expect a sgiciant slwo-down wrt. the exeuction. An example-case wrt. the latter 'maksed proeprty' is to: (a) comptue an all-agaisnt-all-amtrix using MINE; (b) comptue the ranks (for each vertex-row); (c) apply a max-rank-filter; (d) call this fucntion.
 **/
bool kt_matrix__cluster_inputMatrix__kt_clusterAlg_kruskal(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_hca_resultObject_t *obj_result_hca, s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMeans, const s_kt_clusterAlg_config__ccm_t *config, const bool isAn__adjcencyMatrix);

/**
   @struct s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm
   @brief configures the clusteirng-oerpation for the "kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm(..)" clsuter-algorithm (eosekth, 06. feb. 2017).
 **/
typedef struct s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm {
  // FIXME: cosnider merging [”elow] with the 'ccm-trehshold-set' in our "s_kt_clusterAlg_kruskal" ... ie, "s_kt_clusterAlg_config__ccm_t"
  s_kt_clusterAlg_config__ccm_t ccm_config; //! is, the ccm
  /* uint k_clusterCount__min; */
  /* uint k_clusterCount__max; */
  /* /\* t_float threshold_min; *\/ */
  /* /\* t_float threshold_max; *\/ */
  /* /\* const bool useRelativeScore_betweenMinMax; *\/ */
  /* /\* const bool isTo_adjustToRanks_beforeTrehsholds_forRows; *\/ */
  /* /\* const s_kt_api_config_t config; *\/ */
  /* e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type; */
  /* s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config; */
  //! --------------------------------------
  //!
  //! Local result-structures:
  t_float *mapOf_scoreAtMaxRank;
  uint mapOf_scoreAtMaxRank_size;
} s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm_t;




//! @return an intiated object using default cofnigruaitonis (oesketh, 06. feb. 2017).
static s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm_t init__s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm_t() {
  s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm_t self;
  self.ccm_config = initAndReturn__s_kt_clusterAlg_config__ccm_t(NULL);
  //! --------------------------------------
  //!
  //! Local result-structures:
  self.mapOf_scoreAtMaxRank = NULL; 
  self.mapOf_scoreAtMaxRank_size = 0;
  

  //! --------------------------------------
  //!
  //! @return
  return self;
}


/**
   @brief Apply an iterative algorithm to dientify lcusters with the best clsuter-seperation (oekseth, 06. feb. 2017)
   @param <self> is he local configuraiton-object to use.
   @param <obj_1> is the input-matrix to evlauate.
   @param <obj_result> is the result-data-set
   @param <config> is the dgeneric cluster-cofngi object used in our "kt_api.h"
   @return true upon success.
**/
#ifndef SWIG
bool kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm(const s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm_t *self, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const s_kt_api_config_t config);
#endif // #ifndef SWIG


#endif //! EOF
