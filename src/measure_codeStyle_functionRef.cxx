#include "measure_codeStyle_functionRef.h"

/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */


//! -------------------------------------

void applyAritmetics_muliplication(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result) {
  result->sumOf = val1 * val2;
}
void applyAritmetics_muliplication_alwaysInline(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result) {
  result->sumOf = val1 * val2;
}


//! -------------------------------------
void applyAritmetics_muliplication_structLargeOverhead(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result) {
  result->sumOf = val1 * val2;
}
//inline __attribute__((always_inline)) 
void applyAritmetics_muliplication_structLargeOverhead_alwaysInline(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result) {
  result->sumOf = val1 * val2;
}


//! @remarks this function is taken from the "ALGLIB" package and is sued to test (a) the time-cost of the 'static' operaonds for muliple/numerous operations and (b) the 'beneift' of using the "volatile" key-word
bool ae_fp_greater_eq_nonVolatile(double v1, double v2) {
  double x = v1;
  double y = v2;
  return x>=y;
}

//! @remarks this function is taken from the "ALGLIB" package and is sued to test (a) the time-cost of the 'static' operaonds for muliple/numerous operations and (b) the 'beneift' of using the "volatile" key-word
bool ae_fp_greater_eq(double v1, double v2) {
    /* IEEE-strict floating point comparison */
    volatile double x = v1;
    volatile double y = v2;
    return x>=y;
}
