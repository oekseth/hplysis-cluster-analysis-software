#include "kt_clusterAlg_fixed__alg__miniBatch.h"
#include "hp_distance.h"
#include "kt_randomGenerator_vector.h"
/* //! Note: 'visally' evlauate/compare the effects/implications of randomziation: $error(row(i)) = \sum_{j \in Features} (centroids[i][j] - means[i][j])^2$; */
/* static void calculateError() { */
/*   int i, j; */
/*   // FIXME: cosnider to 'make use of' the results in this funciton/comptuation. */
/*   for(i = 0; i < MEANS; i++)     { */
/*     t_float dist = 0; */
/*     for(j = 0; j < FEATURES; j++)	{ */
/*       const t_float diff = centroids[i][j] - means[i][j]; */
/*       dist += diff * diff; */
/*     } */
/*     printf("%f ", dist); */
/*   } */
/*   printf("\n"); */
/* } */

//! Note: use the ranomziaotn-process for "mean"--init to intiatie the "batch" 2d-list.
//void selectBatch()



//! Intiaite the "s_kt_clusterAlg_fixed__alg__miniBatch_t" object.
//! @return an itnaited object.
s_kt_clusterAlg_fixed__alg__miniBatch_t initAndReturn__s_kt_clusterAlg_fixed__alg__miniBatch_t() {
  s_kt_clusterAlg_fixed__alg__miniBatch_t self;
  self.cnt_means_realative = 0.1;
  self.cnt_iterations = 1000;
  // FIXME: write a maacro/test to comarpe the 'effect' of [”elow] ... ie, different ranodmzioant-appraoches <-- write+use a new enum for thuis 'case'.
  self.typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault;
  self.isTo_initIntoIncrementalBlocks = false; //! ie, re-sue ealrier randomziaotn.
  self.config__perf__useFastDistFunction = true;
  self.objMetric__sim = setTo_empty__andReturn__s_kt_correlationMetric_t(); //! which is the corlreionat/silmiarty-emtric to be sued.
  /* //! Specfiyyf the type of  */
  /* self.typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame; */
  //! ------------------------------------------------------
  //!
  //! @return the self-object:
  return self;
}


//void updateMeans() }

// FIXME[algorithm]: how do we manage to 'finid the clustere-mberships of/for each vertex'? <-- consider to 'remember' the 'best/closest cluster-center-id assicated to eahc vertex'.
// FIXME[algorithm]: will it be possilbe to 'use this' as a seleciton-cirtier wrt. disjtoint-forest-seleciton (ie, DbScan) and/or HCA-clsuter-paritionis? <-- try to cocneptualise/scaffold 'scuh an appraoch'


//! Infers clusters uisng the mini-batch algorithm (oesketh, 06. feb. 2017).
const bool kt_clusterAlg_fixed__alg__miniBatch(const s_kt_clusterAlg_fixed__alg__miniBatch_t *self, const s_kt_matrix_t *matrix_input, const uint cnt_clusters, s_kt_clusterAlg_fixed_resultObject_t *obj_result) {
  const uint nrows = matrix_input->nrows;
  const uint ncols = matrix_input->ncols;
  uint MEANS = matrix_input->nrows * self->cnt_means_realative;
  if(MEANS < 10) {MEANS = 0.1*nrows;}
  else if(MEANS >= nrows) {MEANS =  0.7*nrows;}
  assert(MEANS < (uint)T_FLOAT_MAX); //! ie, as we otehrsie need to update our [”elow] rotuien to 'accept' the sue of an uitn R(ather than oru current simplicaiton wr.t the float-appropach).
  if(MEANS >= (uint)T_FLOAT_MAX) {
    fprintf(stderr, "!!\t Mini-batch will behave inconsisten: you have requested %u 'sub-samples' >= rows(%u) >= %max(%u) expected in our algorithm-design-phase: if the latter is indeded then please provide a desciption of your use-case (and expected researhc-article), sending a code-update-request to the senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", MEANS, nrows, (uint)T_FLOAT_MAX, __FUNCTION__, __FILE__, __LINE__);
  }
  if(MEANS <= 4) {
    //fprintf(stderr, "!!\t Seems like an "
    MEANS = 4; //! ie, our 'current minimum'.
  }
  
  //const uint cnt_iterations = self->cnt_iterations;
  //const uint BATCH_SIZE = cnt_clusters; //! ie, number-of-clusters
  //! -----------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------
  float **data = matrix_input->matrix;
  //! -----------------------------------------------------------------------------------------------------------------
  const uint empty_0 = 0;
  //float **means = allocate_2d_list_float(MEANS, ncols, empty_0); //! init: randomzie the rows: means[i<MEANS][j<ncols] = data[index][j], where "index = rand() % nrows;"
  s_kt_matrix_base_t mat_clusterMeans = initAndReturn__s_kt_matrix_base_t(MEANS, ncols); //! ie, "means"
  float **centroids = allocate_2d_list_float(MEANS, ncols, empty_0);; //! size: "|MEANS|x|ncols|". Usage: remember the 'old-previous-comptued' "means", and thereafter used to evlauate/compare the effects/implications of randomziation: $error(row(i)) = \sum_{j \in Features} (centroids[i][j] - means[i][j])^2$;
  uint *cluster_counts = allocate_1d_list_uint(MEANS, empty_0);; //! size: MEANS
  //t_float **batch = allocate_2d_list_float(cnt_clusters, ncols, empty_0); //! size: |cnt_clusters|x|ncols|. Init: <simliar to "means">.
  //! -----------------------------------------------------------------------------------------------------------------

  //! Note: in [”elow] we generate an object to structurally comptue a corret version of: "for(i = 0; i < cnt_clusters; i++){index = rand() % nrows;}"  
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__empty__s_kt_matrix_base_t(); //initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, ncolumns, data);
  // FIXME: consider using [below] instead of [above]
  //s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, ncolumns, data);
  s_kt_randomGenerator_vector_t obj_rand__init = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nrows, /*nelements=*/cnt_clusters, self->typeOf_randomNess, self->isTo_initIntoIncrementalBlocks, &obj_matrix_input); 
  //s_kt_randomGenerator_vector_t obj_rand__init = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nrows, /*nelements=*/MEANS, self->typeOf_randomNess, self->isTo_initIntoIncrementalBlocks); 
 

  // assert(false); // FIXME: instead of "mapOf_rands" use the "obj_rand__init" object
  //uint *mapOf_rands = allocate_1d_list_float(MEANS, empty_0);
  //assert(mapOf_rands);

  //randomassign__speicficType__math_generateDistributions(/*cnt-to-evaluate:*/nrows, /*nelements=*/MEANS, self->typeOf_randomNess, self->isTo_initIntoIncrementalBlocks); 
  { //! Build a subset-matrix: select 'at random' a subset of 'means' rows:
    //! Note: interesting step is: "index = rand() % nrows; means[i<MEANS][j<ncols] = data[index][j];", ie, where the 'ranozmaition-init' is used only once in the algorithm.
    //! Compute: for(i = 0; i < MEANS; i++) {index = rand() % nrows;}
    s_kt_matrix_base_t obj_matrix_input = initAndReturn__empty__s_kt_matrix_base_t(); //initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, ncolumns, data);
    // FIXME: consider using [below] instead of [above]
    //s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, ncolumns, data);
    s_kt_randomGenerator_vector_t obj_rand__init__before = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nrows, /*nelements=*/MEANS, self->typeOf_randomNess, self->isTo_initIntoIncrementalBlocks, &obj_matrix_input); 
    randomassign__speicficType__math_generateDistributions(&obj_rand__init__before);
    for(uint i = 0; i < MEANS; i++)     {
      const uint index = obj_rand__init__before.mapOf_columnToRandomScore[i]; assert(index < nrows);
      memcpy(mat_clusterMeans.matrix[i], data[index], sizeof(t_float)*ncols);
    }
    free__s_kt_randomGenerator_vector_t(&obj_rand__init__before);
  }

  const t_float val_0 = 0;
  s_kt_matrix_base_t mat_localCluster = initAndReturn__s_kt_matrix_base_t(cnt_clusters, ncols); //! ie, "batch"
  //t_float **batch = allocate_2d_list_float(cnt_clusters, ncols, val_0);
  s_kt_matrix_base_t obj_local__resultMatrix = initAndReturn__empty__s_kt_matrix_base_t(); //s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/cnt_clusters);
  //t_float **local__resultMatrix = NULL;
  const s_hp_distance__config_t local_config = init__s_hp_distance__config_t();
  s_hp_distance_t obj_distanceInternal = init__s_hp_distance(self->objMetric__sim, /*needTo_useMask_evaluation__input=*/true, &mat_localCluster, &mat_clusterMeans, local_config);
  obj_distanceInternal.static_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index;
  t_float *clusters = NULL;
  if(self->config__perf__useFastDistFunction) {
    obj_local__resultMatrix = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/cnt_clusters);
      //initAndReturn__s_kt_matrix_base_t(cnt_clusters, MEANS);
    //local__resultMatrix = allocate_2d_list_float(cnt_clusters, MEANS, val_0);
    assert(obj_local__resultMatrix.matrix);
    assert(obj_local__resultMatrix.matrix[0]);
    clusters = obj_local__resultMatrix.matrix[0];
  } else {
    clusters = allocate_1d_list_float(cnt_clusters, empty_0);  //! size: cnt_clusters
  }
  //for(uint i = 0; i < cnt_clusters; i++) { for(uint j = 0; j < MEANS; j++) {local__resultMatrix[i][j] = T_FLOAT_MAX;}} //! ie, intalize.
  
  for(uint i_iter = 0; i_iter < self->cnt_iterations; i_iter++) {
    { //! Randomly select/identify a 'random set of rows':
      //! Note: where we use the ranomziaotn-process for "mean"--init to intiatie the "batch" 2d-list (ie, using "data" read from the input-file): while the 'rows' are ranomzied the 'columsn are used-as-is' (ie, No 'merging' of features).            
      randomassign__speicficType__math_generateDistributions(&obj_rand__init);
      //randomassign__speicficType__math_generateDistributions(/*cnt-to-evaluate:*/nrows, /*nelements=*/MEANS, self->typeOf_randomNess, self->isTo_initIntoIncrementalBlocks);
      //! Update:
      for(uint i = 0; i < cnt_clusters; i++) {
	const uint index = obj_rand__init.mapOf_columnToRandomScore[i]; assert(index < nrows);
	assert(index < cnt_clusters);
	//const int index = rand() % nrows;
	memcpy(/*batch=*/mat_localCluster.matrix[i], data[index], sizeof(t_float)*ncols);
      }
    }    
    { //! Find vertices in the 'intal means-matrix-batch' which are closest to the 'randomly selected vertices': idneitfy the 'ranozmied clsuter-selection' which has the minimum-deivaiton when compared to the 'itnal randomzaiton-appraoch':
      //! Note: the "clusters[i]" cluster-centorid is the vertex which is closest to 'the ranomized center'.
      //! Note: find min-distance between 'init-randomzied-sample' and 'current-randomzied-sample': $cluster[i<cnt_clusters] = min_{j}\{$\sum (d_{batchRandom}(i, k) - d_{initRandom}(j, k))^2 \}$.
      if(self->config__perf__useFastDistFunction) {
	//for(uint i = 0; i < cnt_clusters; i++) { for(uint j = 0; j < MEANS; j++) {local__resultMatrix[i][j] = T_FLOAT_MAX;}} //! ie, intalize.
	//assert(false); // FIXME: udpate [”elwo] call using our new-added "kt_distance" fucntioanitliy. (oekseth, 06. sept. 2016).
	const bool is_ok = apply__extensive__hp_distance(&obj_distanceInternal, &mat_localCluster, &mat_clusterMeans, &obj_local__resultMatrix);
	assert(is_ok);
	//kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(self->objMetric__sim.metric_id, self->objMetric__sim.typeOf_correlationPreStep, /*nrows_1=*/cnt_clusters, /*ncolumns=*/ncols, /*data1=*/batch, /*data2=*/means, /*weight=*/NULL, /*resultMatrix=*/obj_local__resultMatrix.matrix, /*mask1=*/NULL, /*mask2=*/NULL, /*transpose=*/false, /*isTo_invertMatrix_transposed=*/true, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/MEANS, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*(void*)&obj_spec=*/NULL, e_cmp_masksAre_used_undef, NULL);	
	//!
	//! Update our "clusters" list wrt. [ªbofe]:
	assert(obj_local__resultMatrix.nrows == 1);
	assert(obj_local__resultMatrix.ncols == cnt_clusters);
	/* for(uint i = 0; i < cnt_clusters; i++)    { */
	/*   const t_float *__restrict__ row_local = obj_local__resultMatrix.matrix[i]; */
	/*   assert(row_local); */
	/*   for(uint j = 0; j < MEANS; j++)	{ */
	/*     if(isOf_interest(row_local[j])) { */
	/*       clusters[i] = macro_min(clusters[i], row_local[j]); */
	/*     } */
	/*   } */
	/* } */
      } else { //! then we use a non-otpmzied appraoch with reduced accuracy-expresive-pwoer 
	for(uint i = 0; i < cnt_clusters; i++)    {
	  t_float dist_min = T_FLOAT_MAX;
	  //! Compute: $cluster[i<cnt_clusters] = min_{j}\{\sum (d_{batchRandom}(i, k) - d_{initRandom}(j, k))^2 \}$
	  for(uint j = 0; j < MEANS; j++)	{
	    t_float dist = 0;
	    for(uint k = 0; k < ncols; k++) {
	      const t_float diff = /*batch=*/mat_localCluster.matrix[i][k] - mat_clusterMeans.matrix[j][k]; //! ie, "means"
	      dist += diff * diff;
	    }

	    if(dist < dist_min)   {
	      dist_min = dist;
	      clusters[i] = (t_float)j;
	    }
	  }
	}      
      }
    }
    //! ------------------------------------------------------------------------------------------------------------------------------------
    //! 
    { //! Take gradient-step: get per-center learning-rate by 1/#(found); update the 'mean' to capture central vertices in the data-set: 
      copy_2d_list_float(centroids, /*means=*/mat_clusterMeans.matrix, MEANS, ncols); //! remember the old seleciton.
      //!
      //! Adjsut each column:
      for(uint i = 0; i < cnt_clusters; i++)     { //! ie, iterate through each of the clusters:
	const uint index = (uint)clusters[i]; //! ie, the row-id.
	cluster_counts[index] += 1;  //! ie, the number-of-time "row=index"  has been 'ranomly closest' to the cluster-center
	// TODO[kmeans]: consider to 'merge' [below] and our k-means ... ie, where we find/idnietyf the vertex which is closest to the 'current cosntructeed kmeans--mean-vector' .... and then use the 'mini-batch-algorithm-appraoch' to 'merge' the 'new-inferred' cluster-centrodi(cluster=i) with/into the 'old' clsuter-feature-vector .... <--- given the 'arbirtary seleciton' of the 'kmenas-cluster-ids' we first idneitfy the 'closest previosu clsuter-id' and then 'chose the latter cluster-id' (ie, a simplet iteraiton through "nclusters") ... ie, where difference is that we 'use a prior step to nroamlise/adjsut the clstuer-feature-vector (before comptuing memberships) <--- todo: try arguing for why the latter algroithm-permtaution may icnrease accuracy of clsutering.
	const t_float eta = 1.0 / cluster_counts[index];    
	const t_float prob_cluster = (1.0 - eta); //! ie, the probalbility of "row(index)" Not being a clsuter-center  by chance.
	//!
	//! Iterate: 
	//! Note: while "i" in [below] refers to the cluster-id the "mat_localCluster" is udpated to reflect the lcoal clsuter-apritons, hence the correctenss (of our appraoch) (okeseht, 06. jul. 2017).
	const t_float *__restrict__ row_batch = /*batch=*/mat_localCluster.matrix[i]; 
	t_float *__restrict__ row_means = /*means=*/mat_clusterMeans.matrix[index]; 
	assert(row_means); assert(row_batch);
	for(uint j = 0; j < ncols; j++)       {
	  //! Compute the chance of the 'current feature' to Not be the result of a random/conincidental selection/'happening'.
	  //! Note: the row "batch[i]" is a randomly selected from from the input-data "data":
	  //! Note(2): from 'adjustment-equation' we observe how the "k-means-miniBatch' algorithm 'reslts' in a high-score if a row is selected as a cluster-center 'by a factor greater than by randomness', ie, for which the 'random selection-phase' needs to capture the 'speciics' fo the input-data-set.
	  // TODO: try to suggest how an HCA-based appraoch (eg, ioni combianton with centliaty) may manage to 'capture' the imporatn/sigicnat 'vectors of change' in the data-set <-- would it be possilbe to use PCA-results (or HCA-clsuter-sas-vectors) to provide a set of 'cases  to ivnestigate' (ie, an esnabmle of combiatnions, which would be in cotnrast to a floating-poitn-random-comptuation-of-possible-cases)?
	  const bool use_1 = isOf_interest(row_means[j]);
	  const bool use_2 = isOf_interest(row_batch[j]);
	  if(use_1 & use_2) {
	    row_means[j] = prob_cluster * row_means[j] + (eta * row_batch[j]); //! ie, $ p*row_{bestCenter}(j) + p_{not}*row_{random}(j) $
	  } else if(use_2) {
	    // TODO: validate correctness of [belwo].
	    row_means[j] = row_batch[j]; //! ie, a simple copy-oepration.
	  }
	}
      }
    }

    // TODO: consider to 'figure out' how we may use [”elow] to ivnestiate/extrapolate a covnergence-rate-factor.
    // TODO: consider including a permtaution of [”elow] ... to be used as a convergence-step.

    // TOOD[code]: consider to include optioanl support for 'testing' convergence-critiera using a user-defiend CCM (eg, Silhoutte) <--- first describe a scaffold/article ... and scaffold a result-evaluation ... and a new tut-example 'investgaitign tdifferent criteiras' ... keywords [randomnessSeleciton(init), randomnessSeleciton(inside), CCM]
    // TODO[code]: consider to merge [above] and the "k-means-medoid-appraoch" .... ie, as a strategy to 'idneitfy' a 'merged cluster' ... <-- how 'may we use this' in the evalaution of clsute-remberships in "k-means-medoid"??

    //! Note: 'visally' evlauate/compare the effects/implications of randomziation: $error(row(i)) = \sum_{j \in Features} (centroids[i][j] - means[i][j])^2$;
    // if(false) {calculateError();}
  }
  //! ------------------------
  //!
  { //! Post-step: use 'means' to idneityf the centers for each vertex:
    s_hp_distance__config_t local_config_init = init__s_hp_distance__config_t();
    //! Choose the cluster-id which is closest:
    local_config_init.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index;
    //!
    //! Compute distance:
    s_kt_matrix_base_t tmp_matInput = initAndReturn__notAllocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix);
    s_kt_matrix_base_t result_vec = initAndReturn__empty__s_kt_matrix_base_t();
    //! Apply:
    assert(mat_localCluster.nrows == cnt_clusters); //! ie, what we expect (oesketh, 06. jul. 2017).
    const bool is_ok = apply__hp_distance(self->objMetric__sim, &tmp_matInput, &mat_localCluster, &result_vec, local_config_init);
    //    const bool is_ok = apply__hp_distance(self->objMetric__sim, &tmp_matInput, &mat_clusterMeans, &result_vec, local_config_init);
    assert(result_vec.nrows == 1);
    assert(result_vec.ncols == matrix_input->nrows);
    //!
    //! Update the result-object with the [ªbov€] identified relationships:
    assert(obj_result);
    if(obj_result->vertex_clusterId == 0) {
      init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, cnt_clusters, matrix_input->nrows);
    }

    assert(obj_result->vertex_clusterId);
    for(uint i=0; i<result_vec.ncols; i++) { //! ie, the n copy the mappings:
      const uint cluster_id = result_vec.matrix[0][i];
      //      const uint cluster_id = UINT_MAX; assert(cluster_id != UINT_MAX); // FIXME: replace this .... figure out how to correctly set the clstuer-id.
      assert(cluster_id != UINT_MAX);
      assert(cluster_id < cnt_clusters);
      obj_result->vertex_clusterId[i] = cluster_id;
    }
    //!
    //! De-allcoate:
    free__s_kt_matrix_base_t(&result_vec);
  }




  //! ------------------------------------------------------------------------------------------------------------------------------------
  //! 
  //! De-allocate locally reseved data-structure:
  free__s_kt_matrix_base_t(&mat_clusterMeans);
  //free_2d_list_float(&means);    
  free_2d_list_float(&centroids, MEANS);
  //! ---- 
  if(self->config__perf__useFastDistFunction == false) {
    free_1d_list_float(&clusters);
  } else {
    assert(clusters == obj_local__resultMatrix.matrix[0]);
  }
  free_1d_list_uint(&cluster_counts);
  free__s_kt_matrix_base_t(&mat_localCluster);
  //free_2d_list_float(&batch);
  //free_1d_list_uint(&mapOf_rands); mapOf_rands = NULL;
  free__s_kt_randomGenerator_vector_t(&obj_rand__init);
  //if(local__resultMatrix) {free_2d_list_float(&local__resultMatrix); local__resultMatrix = NULL;}  
  free__s_kt_matrix_base_t(&obj_local__resultMatrix);
  //! 
  //! @return
  return true;
}
