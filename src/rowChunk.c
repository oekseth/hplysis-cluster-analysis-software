#include "rowChunk.h"

//! @return the max-bucket-size.
static uint __get_local_bucket_sizeOfRows(const uint nrows, const uint bucket_row_size) {
  uint bucket_size = nrows / bucket_row_size;
  // FIXME: mvoe [below] into a new macor .. and make use of 'this' wrt. 'the otehr fucntiosn'.
  if( (bucket_size*bucket_row_size) < nrows) {
    bucket_size += 1;
    // printf("(set-bucket) size=%u given ncols=%u and bucket_column_size=%u, at %s:%d\n", bucket_size, ncols, bucket_column_size, __FILE__, __LINE__);
  }
  return bucket_size;
}


//! Intialises the s_rowChunk_t object
void init_s_rowChunk(s_rowChunk_t *self, const uint cnt_bucketsEachRows, const uint nrows) {
  assert(self); assert(cnt_bucketsEachRows > 0); assert(nrows > 0);
  
  // FIXME: consdier to experement wrt. this value.
  self->_cnt_rowBuckets = cnt_bucketsEachRows*1;
  //self->_cnt_rowBuckets = cnt_bucketsEachRows*4;
  self->_cnt_columnBuckets = cnt_bucketsEachRows; 
  self->_bucket_sizeOfRows = nrows / self->_cnt_rowBuckets;
  const uint local_bucket_size = __get_local_bucket_sizeOfRows(nrows, self->_bucket_sizeOfRows);
  if(local_bucket_size != self->_cnt_rowBuckets) {
    self->_bucket_sizeOfRows += 1;
    const uint cnt_expected = __get_local_bucket_sizeOfRows(nrows, self->_bucket_sizeOfRows);
    assert_local(self->_cnt_rowBuckets >= cnt_expected);
    //! Then adjust the number of buckets to compute for:
    self->_cnt_rowBuckets = cnt_expected;
#if(config_FixedNumberOf_bucketsFor_rows != 0)  //! then we simplify the work of the compiler wrt. the 'loop-expansions':
    if(self->_cnt_rowBuckets != cnt_expected) {
      fprintf(stderr, "!!\t From your compiler-macro-paraemter we observe that %u number of buckets are to be used: in contrast we derive '%u' column-buckets from the |%u| expected column in your input-matrix: we expect the number of column-buckets (inferred to be of size=|bucket|=%u) to be 'reflect' the bucket-size, ie, please update your configuration. For quesitons, please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", self->_cnt_rowBuckets, cnt_expected, nrows, self->_bucket_sizeOfRows, __FUNCTION__, __FILE__, __LINE__);
      assert(false);
      return;
    }
#endif
  } else {
    assert_local(local_bucket_size == self->_cnt_rowBuckets);
  }

  if(false) {printf("#(init)\t rowChunk={#(rowBuckets)=%u, #(columnBuckets)=%u, |columnBucket|=%u}, given |rows|=%u, at %s:%d\n", self->_cnt_rowBuckets, self->_cnt_columnBuckets, self->_bucket_sizeOfRows, nrows, __FILE__, __LINE__);}

  //assert(false); // FIXME: remvoe

  const t_float default_value_float = 0; const uint default_value_uint = 0; 
  const char default_value_char = 1; //! ie, mark all valeus as up-to-date', ie, our expectation after the inti-prcoeudre
  //const char default_value_char = 1; //! ie, mark all valeus as up-to-date', ie, our expectation after the inti-prcoeudre
  self->matrixOf_globalColumnTiles = allocate_2d_list_float(self->_cnt_columnBuckets, self->_cnt_rowBuckets, default_value_float);
  self->matrixOf_globalColumnTiles_rowId = allocate_2d_list_uint(self->_cnt_columnBuckets, self->_cnt_rowBuckets, default_value_uint);
  self->matrixOf_globalColumnTiles_bucketIsUpTo_date = allocate_2d_list_char(self->_cnt_columnBuckets, self->_cnt_rowBuckets, default_value_char);
}

//! De-allcoates an s_rowChunk_t object.
void free_s_rowChunk(s_rowChunk_t *self) {
  assert(self);
  free_2d_list_float(&self->matrixOf_globalColumnTiles, self->_cnt_columnBuckets);
  free_2d_list_uint(&self->matrixOf_globalColumnTiles_rowId, self->_cnt_columnBuckets);
  free_2d_list_char(&self->matrixOf_globalColumnTiles_bucketIsUpTo_date, self->_cnt_columnBuckets);
}


