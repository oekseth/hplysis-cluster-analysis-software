#include "correlation_s_allAgainstAll_SIMD_inlinePostProcess.h"


//! A wrapper-function to compute correlations for a matrix.
void ktCorr__postProcess_afterCorrelation_for_value(const uint index1, const uint index2, const t_float distance, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const bool isTo_updateAlsoFor_symmetric_elements) {

  //! ie, apply the lgoics:
#include "func_postProcessing__each.c"
}


