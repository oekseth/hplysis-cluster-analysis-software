t_dType det = 0;
{

  //int i,j,j1,j2;

  //t_dType **m = NULL;

#if(__compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause == 0)
  if (n < 1) { /* Error */
    
  } else if (n == 1) { /* Shouldn't get used */
    det = a[0][0];
  } else if (n == 2) {
    det = __macro__get_determinant_case__2x2x(a);
  } else 
#else
    assert(n > 2);
  //printf("n=%u, at %s:%d\n", (uint)n, __FILE__, __LINE__);
#endif
    {      
    det = 0;
    for (uint j1=0;j1<n;j1++) {
#if(__compute_inverse__fastImplementaiton_memAlloc == 0)
      t_dType **m = (t_dType**)malloc((n-1)*sizeof(t_dType *));
      for (uint i=0;i<n-1;i++)
	m[i] = (t_dType*)malloc((n-1)*sizeof(t_dType));
#else
      const t_dType default_value_dType = 0;
      const uint m_size = n-1;
      t_dType **m = allocate_2d_list_dType(m_size, m_size, default_value_dType);
#endif

#if(__compute_inverse__fastImplementaiton == 0)
      for (uint i=1;i<n;i++) {
	uint j2 = 0;
	for (uint j=0;j<n;j++) {
	  if (j == j1)
	    continue;
	  m[i-1][j2] = a[i][j];
	  j2++;
	}
      }
#else
      const uint j1_plussOne = j1+1;
      for (uint i=1;i<j1;i++) {
	//! The copy the valeus 'driectly' usign two seperate calls:
	memcpy(m[i-1], a[i], sizeof(t_dType)*j1);
	memcpy(&m[i-1][j1], &a[i][j1_plussOne], sizeof(t_dType)*j1);
      }
#endif

      const uint nrows_inner = n -1;
#if(__compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause == 0)
      const t_dType det_local = __compute_inverse__functNameCofactor(m, nrows_inner); // __determinant(m, nrows_inner);
#if(__compute_inverse__fastImplementaiton__usePreComputedPowTable == 1)
      assert(mapOf_pow);
      det += mapOf_pow[j1 + 2.0]* a[0][j1] * det_local;
#else
      det += pow(-1.0,j1+2.0) * a[0][j1] * det_local;
#endif
      //det += pow(-1.0,j1+2.0) * a[0][j1] * det_local;
#else
      t_dType det_local = m[0][0];
      if(nrows_inner != 2) {
#if(__compute_inverse__fastImplementaiton__usePreComputedPowTable == 1)
	// printf("(add-recuvsive-call)\t %u--->%u, at %s:%d\n", n, nrows_inner, __FILE__, __LINE__);
	det_local = __compute_inverse__functNameCofactor(m, nrows_inner, mapOf_pow); //__determinant(m, nrows_inner);
	assert(mapOf_pow);
	//const t_dType value_pow = mapOf_pow[2.0];
	//const t_dType value_pow = mapOf_pow[j1 + 2.0];
	det += mapOf_pow[j1 + 2] * a[0][j1] * det_local;
#else
	det_local = __compute_inverse__functNameCofactor(m, nrows_inner); //__determinant(m, nrows_inner);
	det += pow(-1.0,j1+2.0) * a[0][j1] * det_local;
#endif
      } else {
	det += __macro__get_determinant_case__2x2x(m);
      }
#endif
      
      // FIXME: write a measure-emtn-log-object to test the relative time-cost of [”elow] "pow(..)"
#if(__compute_inverse__fastImplementaiton_memAlloc == 0)
      for (uint i=0;i<n-1;i++)
	free(m[i]);
      free(m);
#else
      free_2d_list_dType(&m, (n-1));
#endif
    }
  }
}

#undef __compute_inverse__functNameCofactor
#undef __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause
#undef __compute_inverse__fastImplementaiton_memAlloc
#undef __compute_inverse__fastImplementaiton
#undef __compute_inverse__fastImplementaiton__usePreComputedPowTable
#undef t_dType
#undef allocate_1d_list_dType
#undef allocate_2d_list_dType
#undef free_1d_list_dType
#undef free_2d_list_dType



return det;
