
//! A slow permtuation fo teh "pcl-clsuter" HCA-algorithm
Node_t *pclcluster_slow(uint nrows, uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, char dist, uint transpose, const bool isTo_use_continousSTripsOf_memory) { 

  const uint nelements = (transpose==0) ? nrows : ncolumns;

  const uint ndata = transpose ? nrows : ncolumns;
  const uint nnodes = nelements - 1;

  /* Set the metric function as indicated by dist */
  float (*metric) (uint, float**, float**, char**, char**, const float[], uint, uint, uint) = setmetric_slow(dist);

  const uint empty_value_uint = 0;
  uint* distid = allocate_1d_list_uint(/*size=*/nelements, empty_value_uint); ///*empty-value=*/0);
  Node_t empty_node; init_Node_t(&empty_node);
  //Node_t empty_node; init_Node_t(&empty_node);
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/empty_node);
  //Node_t* result = allocate_1d_list_Node(/*size=*/nnodes, empty_node); ///*empty-value=*/Node());

  float** newdata = NULL;  char** newmask_tmp = NULL;
  maskAllocate__makedatamask(nelements, ndata, &newdata, &newmask_tmp, isTo_use_continousSTripsOf_memory);
  //! Allcoate masks wrt. unit-elements
  const uint default_value_uint = 0;
  uint** newmask_uint = allocate_2d_list_uint(nelements, ndata, default_value_uint); //new uint*[nelements];
  /* newmask_uint[0] = new uint[ndata*nelements]; */
  /* memset(newmask_uint[0], 0, sizeof(uint)*ndata * nelements); */
  /* for(uint i = 0, offset=0; i < nelements; i++) {newmask_uint[i] = newmask_uint[0] + offset; offset += ndata;} */

  for(uint i = 0; i < nelements; i++) {distid[i] = i;}   /* To remember which row/column in the distance matrix contains what */


  /* Storage for node data */

  if(transpose == 0) {
    for(uint i = 0; i < nelements; i++) {
      {
	if(!mask) {
	  for(uint j = 0; j < ndata; j++) {
	    newdata[i][j] = data[j][i];
	    if(isOf_interest(data[j][i])) {
	      newmask_uint[i][j] = 1;
	    } else {
	      newmask_uint[i][j] = 0;
	    }
	  }
	} else {
	  for(uint j = 0; j < ndata; j++) {
	    newdata[i][j] = data[j][i];
	    if(mask[i][j]) {
	      newmask_uint[i][j] = 1;
	      if(mask) {
		newmask_uint[i][j] = (uint)mask[j][i];
	      }
	    } else {
	      newmask_uint[i][j] = 0;
	    }
	  }
	}
      }
      // if(mask) {
      // 	// FIXME: instead of [”elow] use uint ... and then udpate all callers.
      // 	memcpy(newmask_uint[i], mask[i], ndata*sizeof(uint));
      // }
    }
    data = newdata;
    mask = newmask_uint;
  } else {
    {
      for(uint i = 0; i < nelements; i++) {
	for(uint j = 0; j < ndata; j++) {
	  newdata[i][j] = data[j][i];
	  if(isOf_interest(data[j][i])) {
	    newmask_uint[i][j] = 1;
	    if(mask) {
	      newmask_uint[i][j] = (uint)mask[j][i];
	    }
	  } else {
	    newmask_uint[i][j] = 0;
	  }
	}
      }
    }
    data = newdata;
    mask = newmask_uint;
  }
  assert(mask);

  for(uint inode = 0; inode < nnodes; inode++) { //! ie, iterate through each of the 'rows':
    /* Find the pair with the shortest distance */
    uint is = 1; uint js = 0;
    assert(nelements >= inode); //! ie, as otherwise would indicate an incosnsitnecy.
    // FIXME: in [below] try to 'remember' the changes to the 'squares' limited by "local_matrixSize"
    const uint local_matrixSize = nelements - inode;

    assert(false); // FIXME: figure out if the assumption in [”elow] "find_closest_pair(..)"  is correct ... ie, the assumpåtion of 'symemtic proerpty' wrt. updated matrix. 
    result[inode].distance = find_closest_pair(local_matrixSize, distmatrix, &is, &js);
    result[inode].left = distid[js];
    result[inode].right = distid[is];

    /* Make node js the new node */
    for(uint i = 0; i < ndata; i++) {
      data[js][i] = data[js][i]*mask[js][i] + data[is][i]*mask[is][i];
      mask[js][i] += mask[is][i];
      if (mask[js][i]) data[js][i] /= mask[js][i];
    }

    // TODO: consider to update our code to aovid [below].
    if(isTo_use_continousSTripsOf_memory) {
      memcpy(data[is], data[nnodes-inode], sizeof(float)*ncolumns); //! ie, as an alternative to '
      if(mask) {
	memcpy(mask[is], mask[nnodes-inode], sizeof(uint)*ncolumns); //! ie, as an alternative to '
      }
    } else {
      // FIXME: validate correctness of [below] wrt. the 'continous' meory-allcioation
      {
	free_1d_list_float(&data[is]);
	free_1d_list_uint(&mask[is]);
      }
      data[is] = data[nnodes-inode];
      mask[is] = mask[nnodes-inode];
    }
    /* Fix the distances */
    distid[is] = distid[nnodes-inode];

    for(uint i = 0; i < is; i++) {
      distmatrix[is][i] = distmatrix[nnodes-inode][i];
    }
    for(uint i = is + 1; i < nnodes-inode; i++) {
      distmatrix[i][is] = distmatrix[nnodes-inode][i];
    }

    assert(false); // FIXME: in [”elow] make use of the "mask" attribute ... allocating a 'temporal data-chunk' ... ie, to 'ensure' that we are able to compare with the naive/original implemetnation.

    //! Then we need to re-compute the distance-matrix, ie, as the 'internal values' has changed:
    for(uint i = 0; i < js; i++) {
      distmatrix[js][i] = metric(ndata,data,data, /*mask=*/NULL, /*mask=*/NULL, weight,js,i, /*transpose=*/0);
    }
    for(uint i = js + 1; i < nnodes-inode; i++) {
      distmatrix[i][js] = metric(ndata,data,data, /*mask=*/NULL, /*mask=*/NULL, weight,js,i, /*transpose=*/0);
    }
  }

  /* Free temporarily allocated space */
  // FIXME: validate [”elow] ... ie, consider to uset he isTo_use_continousSTripsOf_memory parameter ... and then 'in the caller' store a reference to the 'base-membory-pointer'.
  maskAllocate__freedatamask(nelements, newdata, newmask_tmp, isTo_use_continousSTripsOf_memory);
  assert(newmask_uint); free_2d_list_uint(&newmask_uint, nelements); free_2d_list_uint(&newmask_uint, nelements);
  free_1d_list_uint(&distid);
 
  return result;    
}


//! A slow permtaution of the PSL cluster algorithm
Node_t* pslcluster_slow(const uint nrows, const uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, const char dist, const uint transpose){
  const uint nelements = transpose ? ncolumns : nrows;
  const uint nnodes = nelements - 1;
  //! Allcoate and initaite:
  const uint empty_value_uint = 0; const t_float empty_value_float = 0;
  uint* vector  = allocate_1d_list_uint(/*size=*/nnodes, /*empty-value=*/empty_value_uint);
  float* temp  = allocate_1d_list_float(/*size=*/nnodes, /*empty-value=*/empty_value_float);
  uint* index   = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/empty_value_uint
);
  Node_t empty_node; init_Node_t(&empty_node);
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/empty_node);
  //Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/Node());
  for(uint i = 0; i < nnodes; i++) {vector[i] = i;}

/* Set the metric function as indicated by dist */
  float (*metric) (uint, float**, float**, char**, char**, const float[], uint, uint, uint) = (distmatrix) ? NULL : setmetric_slow(dist);
  const uint ndata = transpose ? nrows : ncolumns;
  
  for(uint i = 0; i < nelements; i++) {
    result[i].distance = T_FLOAT_MAX;
    //! Update the 'suggeestive distances':
    //const float *tmp_array = NULL;
    if(distmatrix) {
      for(uint j = 0; j < i; j++) {
	temp[j] = distmatrix[i][j]; //! then we assuemt eh distances are specified 'explictly', ie, not ned for 'intermediate' computation.
      }
    } else {
      // FIXME[article]: why does the authros only copmute the distances to "j < i" ...  ie, what if the users has provided a non-syemmetirc distance-matrix?
      for(uint j = 0; j < i; j++) {
	temp[j] =  metric(ndata, data, data, /*mask=*/NULL, /*mask=*/NULL, weight, i, j, transpose);
      }
    }
    //! Investigate the vertices 'below' "j":
    for(uint j = 0; j < i; j++) {
      const uint k = vector[j];      
      if(result[j].distance >= temp[j]) {
	// const uint value_j = (tmp_array) ? tmp_array[j] : temp[j];
	// const uint value_k = (tmp_array) ? tmp_array[k] : temp[k];
	//	if(result[j].distance < value_k) {value_k = result[j].distance;} //! then we have a 'local' improvement.
	if(result[j].distance < temp[k]) {temp[k] = result[j].distance;} //! then we have a 'local' improvement.
	//! Then update the distance assicated, and the 'preferred' vector:
	result[j].distance = temp[j];
	vector[j] = i;
      } else if(temp[j] < temp[k]) {temp[k] = temp[j];} //! then we have a 'local' improvement.
    }
    // FIXME: why [below]?
    for(uint j = 0; j < i; j++) {
      if(result[j].distance >= result[vector[j]].distance) {vector[j] = i;}
    }
  }

  free_1d_list_float(&temp);

  //! Label each node based on the current node-index:
  for(uint i = 0; i < nnodes; i++) result[i].left = i;
  //! Order the nodes based on the assicated distance (to each node):
  qsort(result, nnodes, sizeof(Node_t), nodecompare__Node_t);
  //! Initaite the index:
  for(uint i = 0; i < nelements; i++) index[i] = i;
  // FIXME: explain [below]:
  for(uint i = 0; i < nnodes; i++) {
    const uint j = result[i].left;
    const uint k = vector[j];
    result[i].left = index[j];
    result[i].right = index[k];
    const int tmp = -i-1;
    assert(tmp >= 0);
    index[k] = (uint)tmp;
  }
  free_1d_list_uint(&vector);   free_1d_list_uint(&index);
  return result;
}



//! A slow permtautioni of the PML-cluster algorithm
Node_t* pmlcluster_slow(const uint nelements, float** distmatrix) {
  const uint default_value_uint = 0;   
  uint* clusterid = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/default_value_uint);
  Node_t empty_node; init_Node_t(&empty_node);
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/empty_node);
  /* Setup a list specifying to which cluster a gene belongs */
  for(uint j = 0; j < nelements; j++) clusterid[j] = j;
  for(uint n = nelements; n > 1; n--) {
    uint is = 1;
    uint js = 0;
    result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js);
    
    { // FIXME: instead of [”elow] use _mm_storeu_ps
      /* Fix the distances */
      for(uint j = 0; j < js; j++) {
	distmatrix[js][j] = max(distmatrix[is][j], distmatrix[js][j]);
      }
      for(uint j = js+1; j < is; j++) {
	distmatrix[j][js] = max(distmatrix[is][j], distmatrix[j][js]);
      }
      for(uint j = is+1; j < n; j++) {
	distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);
      }
      for(uint j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
      for(uint j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];
    }

    /* Update clusterids */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];
    assert(n >= (nelements - 1)); // IFMX:E if [below] does not hold, then consider to update/elvaute [below].
    clusterid[js] = n-nelements-1;
    clusterid[is] = clusterid[n-1];
  }
  //! De-allcoate the locally reserved memory:
  free_1d_list_uint(&clusterid);

  return result;
}


//! A slow permtuation of the PAL HCA clsuter-algorithm
Node_t* palcluster_slow(const uint nelements, float** distmatrix) {
  assert(nelements > 0);
  if(nelements == 0) {return NULL;}
  /* Setup a list specifying to which cluster a gene belongs, and keep track
   * of the number of elements in each cluster (needed to calculate the
   * average). */
  const uint default_value_uint = 0;   
  uint* clusterid = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/default_value_uint);
  uint* number = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/default_value_uint);
  Node_t empty_node; init_Node_t(&empty_node);
  Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/empty_node);
  //Node_t* result = allocate_1d_list_Node(/*size=*/nelements, /*empty-value=*/Node());
  for(uint j = 0; j < nelements; j++) {
    number[j] = 1; clusterid[j] = j;
  }
  
  uint min_distance = UINT_MAX;

  for(uint n = nelements; n > 1; n--) {
    uint is = 1;     uint js = 0;
    result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js); //! ie, identify the pair which has the lowest weight.
    /* Save result */
    result[nelements-n].left  = clusterid[is];
    result[nelements-n].right = clusterid[js];

    /* Fix the distances */
    const uint sum = number[is] + number[js]; //! ie, the combined number/signfiance of the pair.
    //! Identify the relative importance of the pair-distance, ie, assicated to the 'minimal' column wrt. the rows:
    //! Note: in [below] the 'iteration-threshold is set to "js": ... 

    for(uint j = 0; j < js; j++) { //! ie, [js][0...js] , which for all js>0 will result in updates.
      distmatrix[js][j] = distmatrix[is][j]*number[is] 
                        + distmatrix[js][j]*number[js];
      distmatrix[js][j] /= sum;
    }


    //! Note: in [below] the 'iteration-threshold is set to "is":
    for(uint j = js+1; j < is; j++) { //! ie, [js...is][js] , which implies that condtion (js < is) will not be evalauted/applied/updated.
      distmatrix[j][js] = distmatrix[is][j]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;
    }
    //! Note: in [below] the 'iteration-threshold is set to "n":
    for(uint j = is+1; j < n; j++) { //! ie, [is...n][js] , which implies that 
      distmatrix[j][js] = distmatrix[j][is]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;
    }

    for(uint j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
    for(uint j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];

    /* Update number of elements in the clusters */
    number[js] = sum; //! ie, the signfiance in 'this' iteration.
    number[is] = number[n-1]; //! ie, the sigifance/importance/number in the previous iteration.

    /* Update clusterids */
    assert(n >= (nelements - 1)); // IFMX:E if [below] does not hold, then consider to update/elvaute [below].
    clusterid[js] = n - nelements - 1; //! ie, an incremntal increase.
    clusterid[is] = clusterid[n-1]; //! ie, inverse of "clusterid[js]".
  }
  //! De-allcoate the locally reserved memory:
  free_1d_list_uint(&clusterid); free_1d_list_uint(&number);

  return result;
}

