#include "db_searchNode_rel.h"

//! Inititates the "s_db_searchNode_rel_t" object.
s_db_searchNode_rel_t init__s_db_searchNode_rel_t(uint searchCnt__heads, uint searchCnt__predicates, uint searchCnt__tails, const bool isTo__ifMatch__preSelectionCriteria, const bool isTo_allocteMem) { //, const bool isTo__ifMatch__recursiveExpansion) {
  s_db_searchNode_rel_t self_ref;   s_db_searchNode_rel_t *self = &self_ref;
  const uint min_size = (isTo_allocteMem) ? 1 : 0;
  if(searchCnt__heads != 0) {
    self->listOf_heads      = init__s_kt_list_1d_uint_t(searchCnt__heads);
  } else {
    searchCnt__heads = min_size;
    self->listOf_heads      = init__s_kt_list_1d_uint_t(searchCnt__heads);
    if(isTo_allocteMem) {
      self->listOf_heads.list[0] = __M__wildCard__s_db_rel_t; //! ie, 'mark' the node as 'atibrary'.
    }
  }
  //! ---
  if(searchCnt__predicates != 0) {
    self->listOf_predicates      = init__s_kt_list_1d_uint_t(searchCnt__predicates);
  } else {
    searchCnt__predicates = min_size;
    self->listOf_predicates      = init__s_kt_list_1d_uint_t(searchCnt__predicates);
    if(isTo_allocteMem) {
      self->listOf_predicates.list[0] = __M__wildCard__s_db_rel_t; //! ie, 'mark' the node as 'atibrary'.
    }
  }
  //! ---
  if(searchCnt__tails != 0) {
    self->listOf_tails      = init__s_kt_list_1d_uint_t(searchCnt__tails);
  } else {
    searchCnt__tails = min_size;
    self->listOf_tails      = init__s_kt_list_1d_uint_t(searchCnt__tails);
    if(isTo_allocteMem) {
      self->listOf_tails.list[0] = __M__wildCard__s_db_rel_t; //! ie, 'mark' the node as 'atibrary'.
    }
  }
  //! 
  //! ------------------------------------------------------------------
  self->isTo__ifMatch__preSelectionCriteria = isTo__ifMatch__preSelectionCriteria;
  //self->isTo__ifMatch__recursiveExpansion = isTo__ifMatch__recursiveExpansion;
  //! ------------------------------------------------------------------
  self->isTo__replace__head = false;
  self->isTo__replace__rt = false;
  self->isTo__replace__tail = true;
  //! ------------------------------------------------------------------
  //! @return:
  return self_ref;
}

//! Inititates the "s_db_searchNode_rel_t" object.
s_db_searchNode_rel_t init__mulitpleVertices__s_db_searchNode_rel_t(const s_kt_list_1d_uint_t *listOf_heads, const s_kt_list_1d_uint_t *listOf_predicates, const s_kt_list_1d_uint_t *listOf_tails, const bool isTo__ifMatch__preSelectionCriteria) { //, const bool isTo__ifMatch__recursiveExpansion) {

  s_db_searchNode_rel_t self = init__s_db_searchNode_rel_t(
							   (listOf_heads) ? listOf_heads->list_size : 0,
							   (listOf_predicates) ? listOf_predicates->list_size : 0,
							   (listOf_tails) ? listOf_tails->list_size : 0,
							   isTo__ifMatch__preSelectionCriteria,
							   /*isTo_allocteMem=*/true
							   //, isTo__ifMatch__recursiveExpansion
							   );

  //! Add vertices:
  if(listOf_heads) {
    for(uint i = 0; i < listOf_heads->list_size; i++) {
      self.listOf_heads.list[i] = listOf_heads->list[i];
    }
  }
  if(listOf_predicates) {
    for(uint i = 0; i < listOf_predicates->list_size; i++) {
      self.listOf_predicates.list[i] = listOf_predicates->list[i];
    }
  }
  if(listOf_tails) {
    for(uint i = 0; i < listOf_tails->list_size; i++) {
      self.listOf_tails.list[i] = listOf_tails->list[i];
    }
  }
  //! @return
  return self;
}

//! De-allocates the "s_db_searchNode_rel_t" object.
void free__s_db_searchNode_rel_t(s_db_searchNode_rel_t *self) {
  assert(self);
  free__s_kt_list_1d_uint_t(&(self->listOf_heads));
  free__s_kt_list_1d_uint_t(&(self->listOf_predicates));
  free__s_kt_list_1d_uint_t(&(self->listOf_tails));
}


//! @brief consturct a set of searhc-triplets based/for the 'current' searhc-vertex (oesketh, 06. mar. 2017).
s_db_rel_t *getSearchNodes__s_db_searchNode_rel_t(const s_db_searchNode_rel_t *self, const uint vertex_id, uint *scalar_retSize, const bool isTo__applyRecursiveCall) {
  assert(self); assert(scalar_retSize);
  const uint cnt_heads = self->listOf_heads.list_size;
  const uint cnt_predicates = self->listOf_predicates.list_size;
  const uint cnt_tails = self->listOf_tails.list_size;
  // printf("search-space\t [heads, pred, tails]=[%u, %u, %u], at %s:%d\n", cnt_heads, cnt_predicates, cnt_tails, __FILE__, __LINE__);
  assert(false == __M__isWildCart__s_db_rel_t(vertex_id)); //! ie, as we 'oterhsie may end up' with a search-string which 'matches all case', ie, a pointless earch-stirng.

  if(self->isTo__ifMatch__preSelectionCriteria) {
    if(isTo__applyRecursiveCall == true) { //! then we assuemt aht 'this cofniguraiotnobject' describes a different ctieria.
      *scalar_retSize = 0;
      return NULL;
    }
  } else {
    if(isTo__applyRecursiveCall == false) { //! then we assuemt aht 'this cofniguraiotnobject' describes a different ctieria.
      *scalar_retSize = 0;
      return NULL;
    }
  }
  assert(cnt_heads); assert(cnt_predicates); assert(cnt_tails);
  //! ----------------------------------------------------------------------------
  *scalar_retSize = cnt_heads * cnt_predicates * cnt_tails;
  const uint size_local = *scalar_retSize;
  assert(size_local > 0); // TODO: validate this assumption ... whci 'is expected to become wrong' when we increse the search-compelxity of ouyr "db_searchNode_rel.h" API.
  s_db_rel_t *listOf_ret = __MF__allocate__s_db_rel(size_local); assert(listOf_ret);
  //!
  //! Indietyf the search-triplets to 'ivnestigate':
  uint current_pos = 0;
  for(uint h = 0; h < cnt_heads; h++) {
    uint int_headId = UINT_MAX;
    if(self->isTo__replace__head && (vertex_id != UINT_MAX) ) {
      int_headId = vertex_id;
    } else {
      int_headId = self->listOf_heads.list[h];
    } 
    // printf("sets-head=%u, isWild=%u, at %s:%d\n", int_headId, __M__isWildCart__s_db_rel_t(int_headId),  __FILE__, __LINE__);
    for(uint p = 0; p < cnt_predicates; p++) {
      for(uint t = 0; t < cnt_tails; t++) {
	//! 
	//! Construct the search-object-string:
	s_db_rel_t local_rel = __M__init__s_db_rel_t();
	local_rel.head = int_headId;
	//! --
	if(self->isTo__replace__rt && (vertex_id != UINT_MAX) ) {
	  local_rel.tailRel.rt = vertex_id;
	} else {
	  local_rel.tailRel.rt = self->listOf_predicates.list[p];
	} 
	//! --
	if(self->isTo__replace__tail  && (vertex_id != UINT_MAX) ) {
	  local_rel.tailRel.tail = vertex_id;
	} else {
	  local_rel.tailRel.tail = self->listOf_tails.list[t];
	} 
	//! --
	//! Noite: by defualt do Not search for relation-ids:
	local_rel.tailRel.relation_id = __M__wildCard__s_db_rel_t; //! ie, 'set' a wild-card'.
	//! 
	//! Add to list:
	assert(current_pos < size_local);
	if(false == __M__allValuesInObj__hasArbitraryClauses__s_db_rel_t(local_rel)) { //! ie, as otehrise we woudl have a 'pointless' search-operation.
	  listOf_ret[current_pos] = local_rel;
	  current_pos++;
	} //! else we ingore the 'search-spec'.
      }
    }
  }
  assert(current_pos > 0); //! ie, which fails if 'all searches' are 'marked as wild-card'.
  *scalar_retSize = current_pos;  
  return listOf_ret;
}


//! @brief construct to new search-objects for different evlauation-test-cases (oekseth, 06. mar. 2017). 
void buildSampleCases__s_db_searchNode_rel_t(const e_db_searchNode_rel__sampleUseCases_t enum_id, const s_kt_list_1d_uint_t *listOf_heads, const s_kt_list_1d_uint_t *listOf_predicates, const s_kt_list_1d_uint_t *listOf_tails,  const s_kt_list_1d_uint_t *listOf_preSelection_predictes,  const s_kt_list_1d_uint_t *listOf_preSelection_tails, s_db_searchNode_rel_t *retObj_filterBeforeRecursion, bool *scalar_retObj_filterBeforeRecursion_isToUse, s_db_searchNode_rel_t *retObj_inRecursionTraversal, bool *scalar_retObj_inRecursionTraversal_isToUse) {
  //!
  //! intiaite:
  assert(retObj_inRecursionTraversal);
  if(enum_id == e_db_searchNode_rel__sampleUseCases_wildCard_isTail) {
    //! Allocate: 
    *retObj_inRecursionTraversal = init__mulitpleVertices__s_db_searchNode_rel_t(listOf_heads, listOf_predicates, listOf_tails, true);
    //! --------------------------------------------------------
    //! Do Not use the pre-seleciton-filter-object:
    *scalar_retObj_filterBeforeRecursion_isToUse = false;
    *scalar_retObj_inRecursionTraversal_isToUse = true;
    //! Cofnig:
    retObj_inRecursionTraversal->isTo__ifMatch__preSelectionCriteria = false;
    retObj_inRecursionTraversal->isTo__replace__head = true; //! ie, as we searhes for an 'optnally defined/wildcard' node.
    retObj_inRecursionTraversal->isTo__replace__rt = false;
    retObj_inRecursionTraversal->isTo__replace__tail = false;
    //! We expect at least some 'heads' to have been set:
    assert(listOf_heads);     assert(listOf_heads->list_size > 0);
    assert(false == __M__isWildCart__s_db_rel_t(listOf_heads->list[0])); //! ie, as at least the first vertex should Not be a wild-card <-- tODO: consider to dorp this assert.
    //retObj_inRecursionTraversal-> = ;
  } else if(enum_id == e_db_searchNode_rel__sampleUseCases_wildCard_isHead) {
    //! Allocate: 
    *retObj_inRecursionTraversal = init__mulitpleVertices__s_db_searchNode_rel_t(listOf_heads, listOf_predicates, listOf_tails, true);
    //! --------------------------------------------------------
    //! Do Not use the pre-seleciton-filter-object:
    *scalar_retObj_filterBeforeRecursion_isToUse = false;
    *scalar_retObj_inRecursionTraversal_isToUse = true;
    //! Cofnig:
    retObj_inRecursionTraversal->isTo__ifMatch__preSelectionCriteria = false;
    retObj_inRecursionTraversal->isTo__replace__head = false; 
    retObj_inRecursionTraversal->isTo__replace__rt = false;
    retObj_inRecursionTraversal->isTo__replace__tail = true; //! ie, as we searhes for an 'optnally defined/wildcard' node.
    //! We expect at least some 'tails' to have been set:
    assert(listOf_tails);     assert(listOf_tails->list_size > 0);
    //retObj_inRecursionTraversal-> = ;
  } else if(enum_id == e_db_searchNode_rel__sampleUseCases_wildCard_isTail_preSearch_rt) {
    //! Allocate: 
    *retObj_filterBeforeRecursion = init__mulitpleVertices__s_db_searchNode_rel_t(NULL, listOf_preSelection_predictes, listOf_preSelection_tails, true);
    *retObj_inRecursionTraversal  = init__mulitpleVertices__s_db_searchNode_rel_t(listOf_heads, listOf_predicates, listOf_tails, true);
    //! --------------------------------------------------------
    //! Use the pre-seleciton-filter-object:
    *scalar_retObj_filterBeforeRecursion_isToUse = true;
    *scalar_retObj_inRecursionTraversal_isToUse = true;
    //! Cofnig:
    retObj_filterBeforeRecursion->isTo__ifMatch__preSelectionCriteria = true; //! ie, the 'important part' of this configuring
    retObj_filterBeforeRecursion->isTo__replace__head = true; //! ie, as we searhes for an 'optnally defined/wildcard' node.
    retObj_filterBeforeRecursion->isTo__replace__rt = false;
    retObj_filterBeforeRecursion->isTo__replace__tail = false; 
    //! --------------------- 
    //! Cofnig:
    retObj_inRecursionTraversal->isTo__ifMatch__preSelectionCriteria = false;
    retObj_inRecursionTraversal->isTo__replace__head = true; //! ie, as we searhes for an 'optnally defined/wildcard' node.
    retObj_inRecursionTraversal->isTo__replace__rt = false;
    retObj_inRecursionTraversal->isTo__replace__tail = false; 
    //retObj_inRecursionTraversal-> = ;
  } else if(enum_id == e_db_searchNode_rel__sampleUseCases_preSearch_rtAndTail) {
    //! Allocate: 
    *retObj_filterBeforeRecursion = init__mulitpleVertices__s_db_searchNode_rel_t(NULL, listOf_preSelection_predictes, listOf_preSelection_tails, true);
    //*retObj_inRecursionTraversal  = init__mulitpleVertices__s_db_searchNode_rel_t(listOf_heads, listOf_predicates, listOf_tails, true);
    //! --------------------------------------------------------
    //! Use the pre-seleciton-filter-object:
    *scalar_retObj_filterBeforeRecursion_isToUse = true;
    *scalar_retObj_inRecursionTraversal_isToUse = false;
    //! Cofnig:
    retObj_filterBeforeRecursion->isTo__ifMatch__preSelectionCriteria = true; //! ie, the 'important part' of this configuring
    retObj_filterBeforeRecursion->isTo__replace__head = true; //! ie, as we searhes for an 'optnally defined/wildcard' node.
    //! Note: an example-case wrt. [”elow] is to search for '(*, "is_annotated_by_NTC", "protein")'. 
    retObj_filterBeforeRecursion->isTo__replace__rt  = false;
    retObj_filterBeforeRecursion->isTo__replace__tail = false; 
    //retObj_inRecursionTraversal-> = ;
    //} else if(enum_id == e_db_searchNode_rel__sampleUseCases_preSearch_rtAndTail) {
  } else {
    fprintf(stderr, "!!\t Not supportred, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, as we then ned 'adding supprot for this sue-case'.
  }
}
