
if(config.typeOf_postProcessing_afterCorrelation != e_allAgainstAll_SIMD_inlinePostProcess_undef) { //listOf_inlineResults) { //! Then we use temproary logics to update the result-set:

  // FIXME: describe a performance-comparison-stategy to: (a) assess the 'perofmrance-benefit' of our SSE-procedure and (b) the 'fesiablity' of using an 'internal sub-matrix' rater than a post-global-'traversion':
	
  void *s_inlinePostProcess = config.s_inlinePostProcess;

  /* assert(false); // FIXME: instead of [”elow] 'explict' function-calls use a 'file-uinlcude' for these tasks'. */
  /* assert(false); // FIXME:  */
	
  s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t*)s_inlinePostProcess;

  if(config.typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) {
    assert(s_inlinePostProcess);
    //! Compute the weights of a correlation-matrix, using temporal resutls (eg, from tiling) as input.
    //! Note: for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
    __ofsquaredTileCalcuclate_weights(objCaller_postProcessResult, listOf_inlineResults, chunkSize_index1, chunkSize_index2, index1, index2);					    
    // } else if(config.typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_inverted) {
    //   assert(s_inlinePostProcess);
    //   //! Compute the weights of a correlation-matrix, using temporal resutls (eg, from tiling) as input.
    //   //! Note: for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
    //   __ofsquaredTileCalcuclate_weights_inverted(objCaller_postProcessResult, listOf_inlineResults, chunkSize_index1, chunkSize_index2, index1, index2);					    
  } else if(config.typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {
    __ofsquaredTileCalcuclate_minVal(objCaller_postProcessResult, listOf_inlineResults, chunkSize_index1, chunkSize_index2, index1, index2);					    
  } else if(config.typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {
    __ofsquaredTileCalcuclate_maxVal(objCaller_postProcessResult, listOf_inlineResults, chunkSize_index1, chunkSize_index2, index1, index2);
  } else if(config.typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) {
    __ofsquaredTileCalcuclate_sumOf(objCaller_postProcessResult, listOf_inlineResults, chunkSize_index1, chunkSize_index2, index1, index2);     
    // #include "correlation__codeStub__allAgainstAllPostProcess__sum.c"
  }
 }
