

typedef unsigned int uint;

// FIXME[jc]: ... as a generic throguth ... how may we theretoically suggestion/match/compare/infer the performance/time cost-overehad wrt. usign a number of itnrisitnicts lgocicall calls 'when compared to simpler atihmetic operations' ... ie, for a cetian type of logical operations ... is there a max-limit 'before the standard C-style-prograimming-style' becomes/is faster?
// FIXME[jc]: ... as a generic throguth ... are tryign/workign on identifying 'classes' of operations (ie, a taxonomoy/ontology of 'logical chunks' ... if you ahve the time, may you try to use [”elow] example-codes (and your own code) to suggest different 'classifcioatns' to be sued (eg, as 'compoents' to be used in/whenw riting a enw query-lanague?)?
// FIXME[jc]: ... as a generic throguth ... may you try to itnrospect uppon how we may use ”[elow] exampel-cases 'as the base for an artilce directed towardsa a tutorial bio-informaticans wrt. vector-programming'?
// FIXME[jc]: ... as a generic throguth ... 

void simple_examples() {
  
  { //! Identify the sum of elemnts for both the y-axis and the x-axis.
    // FIXME: update the post-processing in our "graphAlgorithms_distance::calculate_weights(..)" when [”elow] is optimized
    const uint list_size = 5; const float list[list_size] = {2 ,7, 3, 3, 12};
    const float cutoff = 5.0; const float exponent = 10;
    float result[list_size] = {0, 0, 0, 0, 0}; const uint nelements = list_size;
    const uint i = list_size - 1; //! ie, the 'inner' for-loop.
    { //! Naive code:
      for(uint j = 0; j < i; j++) {
	const float distance = list[j];
	if(distance < cutoff) {
	  const float log_value = 1 - distance/cutoff; //! adjsut the dataset to the cut-off value.
	  assert(log_value != 0);
	  const float dweight = exp(exponent*log(log_value));
	  /* pow() causes a crash on AIX */
	  result[i] += dweight;
	  result[j] += dweight;
	}
      }
      //! "re-weight" the results:
      for(uint i = 0; i < nelements; i++) result[i] = 1.0/result[i];
    }
    {
      // FIXME[jc]: may you try to optimize [”elow]? <-- in this copntext, may you try to suggest a code-tmeplate/macro to simplify [”elow] <-- Note: a wild thought, ie, nore sure 'what I am asking for'.
      uint j = 0; const uint size_first = (i > 4) ? i - 4 : 0; //! ie, to simplify our for-loop.
      if(size_first) {
	__mi128 vec_cutoff = _mm_load1_ps(&cutoff); //! ie, set the cutoff.	
	// FXIME[jc]: is there any perofrmance-difference between "_mm_load1_ps(..)" and "_mm_set1_ps(..)" ??
	__mi128 vec_exponent = _mm_load1_ps(&exponent); //! ie, set the cutoff.
	float result_tmp[4] = {0, 0, 0, 0};
	for(j = 0; j < size_first; j += 4) {
	  __mi128 vec_distance = _mm_load_ps(&list[j]);
	  //! Handle the cutoff:
	  assert(false); // FIXME: [below] seems to prodcue teh erronosu reuslsts ... ie, due to the "nan" resutl-values.
	  const __mi128 vec_belowThreshold = _mm_cmplt_ps(vec_distance, vec_cutoff);
	  //! Update the values, ie, under the assumption that 'all valeus above thresholds' are set to '0':
	  vec_distance = _mm_mul_ps(vec_belowThreshold, vec_distance);
	  //! Compute the log-value:
	  // FIXME[jc]: wrt. [”elow] ... how may we handle "nan" and "0" ... ie, any need to specific handling wrt. [below]? <-- possible to use 'wrappers' to simplify
	  const __mi128 vec_logValue = _mm_sub_ps(_mm_set1_ps(1), _mm_div_ps(vec_distance, vec_cutoff));
	  //! Compute the exponent:
	  vec_distance = _mm_exp_ps(_mm_mul(vec_exponent, vec_logValue));  //! ie, exp(exponent*log(log_value));
	  //! Update the result, first wrt. the 'inner' value:
	  _mm_store_ps(&result[j], //! which is the sum of the 'previous' and the 'new-identifed value'
		       _mm_add_ps(_mm_load_ps(&result[j]), vec_distance)); //! ie, " += ".
	  
	  //! Then wrt. the 'outer' value:
	  _mm_store_ps(&result_tmp[0], //! which is the sum of the 'previous' and the 'new-identifed value'
		       _mm_add_ps(_mm_load_ps(&result_tmp[0]), vec_distance)); //! ie, " += ".
	  
	}
	// FIXME[jc]: is there any better stragy than [”elow]?
	for(uint k = 0; k < 4; k++) {
	  result[i] += result_tmp[k];
	}
      }
      //! Then complete [ªbove].
      for(; j < i; j++) {
	const float distance = list[j];
	if(distance < cutoff) {
	  const float log_value = 1 - distance/cutoff; //! adjsut the dataset to the cut-off value.
	  assert(log_value != 0);
	  const float dweight = exp(exponent*log(log_value));
	  /* pow() causes a crash on AIX */
	  result[i] += dweight;
	  result[j] += dweight;
	}
      }
      //! "re-weight" the results:
      uint i = 0; const uint size_first = (i > 4) ? nelements - 4 : 0; //! ie, to simplify our for-loop.
      if(size_first) {
	for(i = 0; i < size_first; i += 4) {
	  //! Compute: result[i] = 1.0/result[i];
	  // FIXME[jc]: wrt. [”elow] ... how may we handle "nan" and "0" ... ie, any need to specific handling wrt. [below]?
	  _mm_store_ps(&result[i], //! which is the sum of the 'previous' and the 'new-identifed value'
		       _mm_div_ps(_mm_set1_ps(1), _mm_load_ps(&result[i]))); //! ie, "1 / value"
	}
      }
      //! Then complete [ªbove].
      for(; i < nelements; i++) {
	result[i] = 1.0/result[i];
      }
    }
  }
}

// Note[jc]: this funciton is a subset of the "graphAlgorithms_distance::compute_rank_forVector(..)" call ... and similar to "compute_distance_array_implicitMask(..)" and "compute_distance_array_mask(..)" ... and when completed then update our "graphAlgorithms_distance::spearman(..)" <-- wrt. the latter, consider to update some of the 'naive legacy-funcitons' to 'hold' both optimized nav teh 'old naive impelmetnaitons'.
void compute_rank_forVector(const uint index1, const uint size, float **data, char **mask, float *tmp_array) {
  assert(size); assert(result_array); assert(tmp_array); assert(data);
  //! Idnetify the interesting vertices:
  if(mask) {
    for(uint i = 0; i < size; i++) {
      if(mask[index1][i]) {
	tmp_array[i] = data[index1][i];
      } else {tmp_array[i] = FLT_MAX;}
    }
  } else {
    for(uint i = 0; i < size; i++) {
      if(isOf_interest(data[index1][i])) {
	tmp_array[i] = data[index1][i];
      } else {tmp_array[i] = FLT_MAX;}
    }
  }
}


//! Idnetify a the assicated transpsoed array.
//! Note[jc]: this funciton is similar to "compute_rank_forVector(..)" (ie, also found in our "graphAlgorithms_distance.cxx") 
// FIXME[jc] is it possible to access mask[i][index1] and data[i][index1] with 'jumps' ... eg, to use "size*sizeof(float)" when 'reading each chunk into the vector? <-- if so, then try/consider to update our "graphAlgorithms_distance::compute_rank_forVector_firstTranspose(..)"
static void get_transposed_column(const uint index1, const uint size, float **data, char **mask, float *tmp_array) {
  //! Idnetify the interesting vertices:
  if(mask) {
    for(uint i = 0; i < size; i++) {
      if(mask[i][index1]) {
	tmp_array[i] = data[i][index1];
      } else {tmp_array[i] = FLT_MAX;}
    }
  } else {
    for(uint i = 0; i < size; i++) {
      if(isOf_interest(data[i][index1])) {
	tmp_array[i] = data[i][index1];
      } else {tmp_array[i] = FLT_MAX;}
    }
  }
}

//! Note[jc]: this funciotn refers to "graphAlgorithms_distance::find_closest_pair(..)"
//! @return the identifed distance for the pair in a symmetric matrix
//! @remarks This function searches the distance matrix to find the pair with the shortest distance between them. 
//!          The indices of the pair are returned in ip and jp; 
//! @remarks time-complexity estimated to be "n*n", ie, quadratic.
//! @remarks Memory-access-pattern of this funciton is:  two for-loops which in intship identify/find the pair and distance with the smallest value; in 'ideal' solutions/appraoches one cache-miss, otherwise "n" cache-misses. 
float find_closest_pair(const uint n, float** distmatrix, uint* ip, uint* jp) {
  //! What we expect:
  assert(distmatrix);
  assert(n > 0);
  //! Intialize:
  float distance = distmatrix[1][0];
  *ip = 1;  *jp = 0;
  //! Find the 'shortest' pair:

  // FIXME: write emmory-benchmarks to test the effects of intinitstics on/for this function

  if(true) {

    // FIXME[optimize]: may you jan-christiaon write an optmization of [”elow] code-chunk? <-- 

    float bestScores_distance[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};
    float bestScores_point_i[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};
    float bestScores_point_j[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

    // uint idx = 0; __m128 max = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

    for(uint i = 1; i < n; i++) { 

      assert(false); // FIXME: try to resovle errors in [”elow] 'optimzaiton-code'.
      
      uint j = 0;
      if((i+4) > 8) {
	const uint last_pos = i-4; //! ie, to 'allow' for overflow.
	for(uint j = 0; j < last_pos; j += 1) { 
	  // const __m128 value = _mm_load_ps(&distmatrix[i][j]); // can either use an array or __m128 value calculated in loop
	  // const __m128 temp1 = _mm_shuffle_ps(value, value, _MM_SHUFFLE(1, 0, 3, 2));
	  // const __m128 temp2 = _mm_max_ps(value, temp1);
	  // const __m128 temp3 = _mm_shuffle_ps(temp2, temp2, _MM_SHUFFLE(2, 3, 0, 1));
	  // const __m128 temp4 = _mm_min_ps(temp3, temp2);
	  // max = _mm_min_ps(temp4, max);
        
	  // unsigned long index;
	  // const unsigned long mask = _mm_movemask_ps(_mm_cmpeq_ps(value, max));
	  // const unsigned char bit = _BitScanForward(&index, mask);

	  // if (bit)
          //   idx = i * 4 + index;

	  __m128 vec_result_tmp = _mm_loadu_ps(&bestScores_distance[0]);
	  const __m128 arr_data = _mm_load_ps(&distmatrix[i][j]);
	  //! Find the maximum value:
	  __m128 vec_max = _mm_max_ps(vec_result_tmp, arr_data);

	  // FIXME: is there a more 'precie' methiod to infer/'get' the assicated index-i and index-k?

	  //! Figure out if the values are in the max-set:
	  __m128 vec_isInMax_data1 = _mm_cmplt_ps(vec_max, arr_data);
	  __m128 vec_isNotInMax_result = _mm_cmpge_ps(vec_result_tmp, arr_data);
	  //__m128 vec_isInMax_inverted = _mm_and_ps(vec_result_tmp, _mm_setzero_ps());
	  
	  //! Identify the indices assicated to the max-index, first for the 'new set':
	  float flt_tmp_i = (float)i;
	  __m128 vec_data_data_index_i = _mm_mul_ps(vec_isInMax_data1, _mm_load1_ps(&flt_tmp_i));
	  // FIXME: instead of [”elow] try using the _mm_shuffle(...)
	  float flt_tmp_j[4] = {(float)(j+0), (float)(j+1), (float)(j+2), (float)(j+3)};
	  __m128 vec_data_data_index_j = _mm_mul_ps(vec_isInMax_data1, _mm_load_ps(&flt_tmp_j[0]));

	  //! Identify the indices assicated to the max-index, thereafter for the 'best-fit set':
	  __m128 vec_data_maxUpdated_index_i = _mm_mul_ps(vec_isNotInMax_result, _mm_load_ps(&bestScores_point_i[0]));
	  __m128 vec_data_maxUpdated_index_j = _mm_mul_ps(vec_isNotInMax_result, _mm_load_ps(&bestScores_point_j[0]));

	  //! Combine the results: as we expect only one of the indeices to be set, we use 'add':
	  _mm_store_ps(&bestScores_distance[0], vec_max); //! ie, sotre the max-values.
	  _mm_store_ps(&bestScores_point_i[0], _mm_add_ps(vec_data_maxUpdated_index_i, vec_data_data_index_i)); //! ie, max-valeus for store for index=i
	  _mm_store_ps(&bestScores_point_j[0], _mm_add_ps(vec_data_maxUpdated_index_j, vec_data_data_index_j)); //! ie, max-valeus for store for index=j
	}
      }
      //! Handle the not-yet-covered cases:
      for(; j < i; j++) { 
	const float temp = distmatrix[i][j];
	if(temp < bestScores_distance[0]) { //! then a shorter distance is found.
	  bestScores_distance[0] = temp;
	  bestScores_point_i[0] = i;
	  bestScores_point_j[0] = j;
	}
      }
    }
    //! Identify the min-point:
    for(uint i = 0; i < 4; i++) {
      const float temp = bestScores_distance[i];
      if(temp < distance) { //! then a shorter distance is found.
	distance = temp;
	*ip = bestScores_point_i[i];
	*jp = bestScores_point_i[i];
      }
    }
  } else {
    for(uint i = 1; i < n; i++) { 
      for(uint j = 0; j < i; j++) { 
	const float temp = distmatrix[i][j];
	if(temp < distance) { //! then a shorter distance is found.
	  distance = temp;
	  *ip = i;
	  *jp = j;
	}
      }
    }
  }
  return distance;
}
#include "e_kt_correlationFunction.h"

/* typedef enum e_optimal_compute_allAgainstAll_type { */
/*   e_optimal_compute_allAgainstAll_type_euclid, */
/*   e_optimal_compute_allAgainstAll_type_cityblock, */
/*   e_optimal_compute_allAgainstAll_type_spearman_or_correlation, */
/*   // FIXME: make use of [”elow] and then update our article witht he new-itnroduced correlation-emtric ...   and consider to include/inroroporate other correlation-metrics ("http://people.sc.fsu.edu/~jburkardt/c_src/correlation/correlation.html") ... writtten for an aritcle with title "A Review of Gaussian Random Fields and Correlation Functions" */

/*   e_optimal_compute_allAgainstAll_type_brownian_correlation, //! "Petter Abrahamsen,     A Review of Gaussian Random Fields and Correlation Functions,     Norwegian Computing Center, 1997." and "https://en.wikipedia.org/wiki/Distance_correlation" */
  
/* e_optimal_compute_allAgainstAll_type_undef */
/* } e_optimal_compute_allAgainstAll_type_t; */

//! @return the distances assicated to "e_kt_correlationFunction_t"
      /* else if(typeOf_metric == e_kt_correlationFunction_brownian_correlation) { \ */
      /* const float max_val = max(val1, val2); \ */
      /* if(0 < max_val) {term = sqrt(min(val1, val2) / max_val);} else {term = 0;} \ */
      /* }									\ */
#define __macro__get_distance(val1, val2, weight, weight_index, typeOf_metric) ({ \
      float term = 0;							\
      if(typeOf_metric == e_kt_correlationFunction_spearman_or_correlation) { term = val1 * val2;} \
      else if(typeOf_metric == e_kt_correlationFunction_cityblock) {term = fabs(val1 - val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_euclid) { term = val1 - val2; term = term*term; } \
      else {assert(false);}						\
const float weight_local = (weight) ? weight[weight_index] : 1;		\
const float result_local = weight_local*term;				\
result_local;								\
})


//! @return the distances assicated to "e_kt_correlationFunction_t"
      /* else if(typeOf_metric == e_kt_correlationFunction_brownian_correlation) { \ */
      /* 	const __m128 vec_max = _mm_max_ps(_mm_sub_ps(_mm_setzero_ps(), term2), term1); \ */
      /* 	const __m128 vec_min = _mm_min_ps(_mm_sub_ps(_mm_setzero_ps(), term2), term1); \ */
      /* 	const __m128 vec_divFactor = _mm_div_ps(vec_min, vec_max); \ */
      /* 	const __m128 vec_compareMax_zeros = _mm_cmplt_ps(vec_max, _mm_setzero_ps()); \ */
      /* 	const __m128 vec_ofInterest = _mm_andnot_ps(vec_compareMax_zeros, vec_max); \ */
      /* 	const __m128 vec_valuesWithAnswer = _mm_and_ps(vec_compareMax_zeros, _mm_sqrt_ps(vec_divFactor)); \ */
      /* 	mul = _mm_or_ps(vec_ofInterest, vec_valuesWithAnswer); \ */
      /* }									\ */
#define __macro__get_distance_SIMD(val1, val2, weight, weight_index, typeOf_metric) ({ \
  __m128 mul; \
      if(typeOf_metric == e_kt_correlationFunction_spearman_or_correlation) { mul = _mm_mul_ps(term1, term2); } \
      else if(typeOf_metric == e_kt_correlationFunction_cityblock) {mul = _mm_sub_ps(term1, term2); mul = _mm_max_ps(_mm_sub_ps(_mm_setzero_ps(), mul), mul); ;} \
      else if(typeOf_metric == e_kt_correlationFunction_euclid) { mul = _mm_sub_ps(term1, term2); mul = _mm_mul_ps(mul, mul); } \

      else {assert(false);}						\
if(weight) {mul = _mm_mul_ps(mul, _mm_set1_ps(weight[weight_index]));}	\
mul;								\
})




// FIXME[jc]: compare the correctness and degree of optimzaliation wrt. __macro__get_distance(..) and "__macro__get_distance_SIMD(..)" ... and when the "__macro__get_distance_SIMD(..)" is correct, then consider/try writing an optimized 'verison' of the latter.


//! Note: use FLT_MAX to identify the valeus to compare.
static void compute_distance_array_implicitMask(const float *rmul1, const float *rmul2, const uint chunkSize_cnt_i, float *rres_scalar) const {
  // FIXME: when [below] is udpated/opimized then update __optimal_compute_allAgainstAll_SIMD(..)" in our "graphAlgorithms_distance.cxx"
  float result[4] = {0, 0, 0, 0}; //! which is used to reduce teh compleity of the summing-operaiton.
  const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1);
  for (uint j2 = 0; j2 < chunkSize_cnt_i; j2 += 4) {
    // TODO: update our documetnation wwtih [below] observaiton.		    
    //! Get the values:
    __m128 term1  = _mm_load_ps(&rmul1[j2]); __m128 term2  = _mm_load_ps(&rmul2[j2]);  
    //! Compute the AND mask:		    
    // FIXME[jc]: ask jan-christian to vlaidate correctness of [”elow] ... where intail correctnes-stests are seen/found int ehs tart of our evaluate_cmpOf_two_vectors_wrt_intrisinitstics() in our "graphAlgorithms_timeMeasurements_syntetic.cxx" 
    __m128 vec_cmp_1 = _mm_cmplt_ps(term1, vec_empty_empty); 
    vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); // FIXME[jc]: may omit/drop this call .. eg, wrt. the "nan" values?
    __m128 vec_cmp_2 = _mm_cmplt_ps(term2, vec_empty_empty);
    vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones); // FIXME[jc]: may omit/drop this call .. eg, wrt. the "nan" values?
    __m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1
    mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order. // FIXME[jc]: may omit/drop this call  ... ie, any funcitons which 'does not by default rever the resulsts?
    
    //! Get the distance:
    __m128 mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
    //! Make use of the mask:
    mul = _mm_mul_ps(mask, mul);
    //! Load the result-vector
    //! Add the values:
    __m128 arr_result_tmp = _mm_add_ps(mul, _mm_loadu_ps(&result[0])); //! ie, combien the sums.
    //! Store the result:
    _mm_store_ps(&result[0], arr_result_tmp);
  }
  //! Combine teh vectors:
  // FIXME: try to dientify a SIMD instruction which 'does [”elow] atuotmatically' <-- do joun jan-chrsitain ahve anys gugestiosn?
  for(uint i = 0; i < 4; i++) {rres_scalar += result[i];}		
}

//! Note: use "rmask1" and "rmask2" to identify the valeus to compare.
static void compute_distance_array_mask(const float *rmul1, const float *rmul2, const char *rmask1, const char *rmask2, const uint chunkSize_cnt_i, float *rres_scalar) const {
  // FIXME: when [below] is udpated/opimized then update __optimal_compute_allAgainstAll_SIMD(..)" in our "graphAlgorithms_distance.cxx"
		
  //! The operation:
  float result[4] = {0, 0, 0, 0}; //! which is used to reduce teh compleity of the summing-operaiton.
  const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1);
  //! Note: we expect every valeu 'ot fit onto' chunkSize_cnt_i, ie, as the latter is tob e divisible by "4", ie, which explains why we do not sue a ssperate for-loop 'after [”elow]'.
  for (uint j2 = 0; j2 < chunkSize_cnt_i; j2 += 4) {
    // TODO: update our documetnation wwtih [below] observaiton.		    
    //! Get the values:
    __m128 term1  = _mm_loadu_ps(&rmul1[j2]); __m128 term2  = _mm_loadu_ps(&rmul2[j2]);  

    // FIXME: try to optimzie [below] code. <-- ask jan-crhistain for suggestions.
    const __m128 vec_mask1 = _mm_set_ps((float)rmask1[j2+0], (float)rmask1[j2+1], (float)rmask1[j2+2], (float)rmask1[j2+3]);
    const __m128 vec_mask2 = _mm_set_ps((float)rmask2[j2+0], (float)rmask2[j2+1], (float)rmask2[j2+2], (float)rmask2[j2+3]);
    __m128 mask = _mm_and_ps(vec_mask1, vec_mask2); //! ie, where onlu [0]=3 has a value=1
    // const __m128 vec_data2 = _mm_set_ps(FLT_MAX, FLT_MAX, FLT_MAX, 3);     

    // FIXME[jc]: [”elow] is included in order to examplify one of our 'approaches', ie, to 'unpack' the chars ... a copy-paste from the web ... ie, have not yet managed to fully grasp/understand [”elow].
    // 		  //! Compute the AND mask:		    
    // 		  // FIXME: ask jan-christian to vlaidate correctness of [”elow] ... where intail correctnes-stests are seen/found int ehs tart of our evaluate_cmpOf_two_vectors_wrt_intrisinitstics() in our "graphAlgorithms_timeMeasurements_syntetic.cxx"
    // 		  __m128i vec_mask_toFloat_1 = _mm_cvtsi32_si128(*(const int*)&mask1[j2]); 
    // 		  vec_mask_toFloat_1 = _mm_unpacklo_epi8(vec_mask_toFloat_1, _mm_cmplt_epi8(vec_mask_toFloat_1, _mm_setzero_si128()));
    // 		  __m128i vec_mask_toFloat_2 = _mm_cvtsi32_si128(*(const int*)&mask2[j2]);
    // 		  vec_mask_toFloat_2 = _mm_unpacklo_epi8(vec_mask_toFloat_2, _mm_cmplt_epi8(vec_mask_toFloat_2, _mm_setzero_si128()));
    // 		  //__m128 vec_cmp_1 = _mm_loadu_ps(&rmask1[j2]);  __m128 vec_cmp_2 = _mm_loadu_ps(&rmask2[j2]);
    // 		  //m128  (__m128i v1)		    __m128d 

    // 		  //! First convert from int to float, and then compare:
    // 		  __m128 mask = _mm_and_ps(_mm_cvtepi32_ps(vec_mask_toFloat_1), _mm_cvtepi32_ps(vec_mask_toFloat_2)); //! ie, where onlu [0]=3 has a value=1
    //__m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1
    mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
		  
    //! Get the distance:
    __m128 mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
    //! Make use of the mask:
    mul = _mm_mul_ps(mask, mul);

    //! Load the result-vector
    __m128 arr_result_tmp = _mm_loadu_ps(&result[0]);
    //! Add the values:
    arr_result_tmp = _mm_add_ps(mul, arr_result_tmp);
    //! Store the result:
    _mm_store_ps(&result[0], arr_result_tmp);
  }
  //! Combine teh vectors:
  // FIXME: try to dientify a SIMD instruction which 'does [”elow] atuotmatically' <-- do joun jan-chrsitain ahve anys gugestiosn?
  for(uint i = 0; i < 4; i++) {rres[k2] += result[i];}
}

// FIXME: 
