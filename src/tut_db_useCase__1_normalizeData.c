#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
//#include "db_ds_directMapping.h"
#include "db_inputData_matrix.h"
//#include "db_ds_bTree_rel.h"
#include "kt_matrix.h"
#include "measure_base.h"
/**
   @brief examplfies how to load a set of relations and synonysm from files int our "db_ds_directMapping.h" data-structue(s).
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks
   -- Idea: examplifes a normlization-strategy for data-sets: produce a mapping-table based on a forest of synonyms, and then (a) write out this mapping-table, (b) noramlizes an input-matrix, and (c) examplfies how latter may be used to update+access our "db_ds_directMapping.h" (eg, wrt. b-tree-insertiosn) .... this mapping-table  ... Take 'as input' two different matrices: (a) 'synonym-relations' and (b) 'ordinary' SKB-relations ... ... API: we use our "get_centralityVertex_inDisjointforest__s_kt_forest_findDisjoint(..)" ("kt_forest_findDisjoint.h") to 'build' a mapping-table (or: a synonym-normalizaiton-list), ie, for which our nromalizaton-appraoch becomes stragith-forward.
   @remarks provides interesting fucntioantliy wrt.:
   (a) input: how to laod a tsv-file-set 'directly' into our "db_ds_directMapping.c" data-set;
   (b) synonym-normalizaiton: how to 'merge' relatiosnhisp using our "kt_forest_findDisjoint.h" distjoint-forest-implementantion;
   (c) search: different search-appraoches.
   @remarks observations from our measurements:
   -- .. 
   -- 1d-list-traversal VS poitner-based list-travesal: a 'fixed' exueciotn-time-difference of approx. 170x (ie, where the latter 'describes' the difference betwen a pointer-based B-tree-iteraiton VS a list-based traversal). In brief latter may be explained by the hpLysis appraoch of increased spaticla and temproal locality of accessed memory. 
   -- .. 
   -- .. 
   @remarks mmemory-access-evlauation through CacheGrind:
   -- in order to evlauate oru aprpaoch for emmory-accesses we makes use of Valgridns "Cacehgrind" software-extenions. From the results (included in our extensive web-page-evlauation) we observe how 'our appraoch' manages an improved utilziaiton of instruciton-cahce and memory-cache. 
   @remarks in our 'defulat evlauation' we evlauate either for "B-tree(relations)" or hpLysis database. This simplfied elvauaiton-appraoch implies that the MySQL-nested-searches is not explcitly evlauated. However, if we combine the 'number of evlauated tripelts' from our elvauaiton with the MySQL-perofmrance-evaluation (which idneites relationships slower than the B-tree-appraoch) we infer/eluciddate taht MySQL ahs a perofmrance sigicntly slwoer that our hpLysis database-appraoch.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const bool isTo_useRandomAccess = true;
  const bool config__init__onlyUseNorm = true; //! which if set to false impleis that we 'compare' the xeuciton-time for 'non-data-nroamlizaiton-pre-step' VS 'data-normalization-pre-step'
  const uint cnt_heads = 100; const uint cnt_pred = 4; const uint nrows_each = 10; const t_float fraction_syns = 0.2; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases.
  //! COfniguring the search for 'relationships':
  const e_db_searchNode_rel__sampleUseCases_t typeOf_useCase = e_db_searchNode_rel__sampleUseCases_wildCard_isTail;
  const bool config__isTo_preCompute_preSearchConditions = true;
  const e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__synMappings;
  const bool configLogics__isToApply__synonymMappings__inFunction = true; //! which sie used to vinestiate time-effects/improtance of the synonym-re-mapping-step.
      //! Diemsniosn fot eh searhc-spac:
  const uint inSearchCntToEvaluateMax__for__head      = 20;
  const uint inSearchCntToEvaluateMax__for__predicate = 20;
  const uint inSearchCntToEvaluateMax__for__tail      = 20;
  const uint inSearchCntToEvaluateMax__for__preSelection__predicate  = 20;
  const uint inSearchCntToEvaluateMax__for__preSelection__tail       = 20;
  const uint inSearchCntToEvaluateMax__stepMult = 10; //! ie, mulitply by "inSearchCntToEvaluateMax__stepMult" for the [ªbove] 'size-properties'.
  const uint inSearchCntToEvaluateMax__each__cnt = 10; //! ie, '10' differten calls using our "inSearchCntToEvaluateMax__stepMult" to 'enlarge' the data-size 'for each call'.
  const uint threshold__scalar__cntVerticesToInvestigate__max = UINT_MAX; //! which if set is use das a 'stop-trheshodl' wrt. vertex-inveistgaitons.
#endif
  
  int initseed = (int)0; //! ie, for which 'different calls' whill 'rpdocue' the same number of numbers/results.
  srand(initseed);

  const char *fName__rel = "__tutSample_rel.tsv";
  const char *fName__syn = "__tutSample_syn.tsv";
  //!
  //! Use a gneerliaed funciton to simplifyin building of sample-matrices:
  generateSample__storeInFiles__db_inputData_matrix(cnt_heads, cnt_pred, nrows_each, fraction_syns, fName__rel, fName__syn);

  //! ----------------------------------------------------------------------------------
  //!
  //! The 'main call': load relationships and inclue into the search-object:  
  for(uint useNorm = 0; useNorm < 2; useNorm++) { //! ie, to investigat ethe time-overhead wrt. 'normalizaiton' VS 'no-noramlizaiton' appraoch: we expect that the nroamlziioant-step has aisngicnfat time-cost when compared to 'data-isneriton-rpcoess' itself (ie, givne the itme-compleixty of oru disjoitn-froeset-algorithm-implementiaotn).
    if(!useNorm && config__init__onlyUseNorm) {continue;} //! ie, as we then assume 'this case' is Not of interest.
    start_time_measurement(); //! ie, start the time-emasurement:
    //! Load the data from file, and insert boht the synonysm and relationshisp into the strucutre:
    s_db_ds_directMapping_t obj = loadData__s_db_ds_directMapping__db_inputData_matrix(fName__rel, (fraction_syns > 0) ? fName__syn : NULL, /*isTo_applyPreNormalizaiotnStep__onSynonyms=*/(bool)useNorm, objConfig);
    { //! Complete time-measurement:
      char str_local[2000] = {'\0'}; sprintf(str_local, "insert::loadFromFile(%s):useNorm=%s", getString__e_db_ds_directMapping__initConfig(objConfig), (useNorm == 1) ? "true" : "false");
      float cmp_time_search = 0;
      const float endTime = end_time_measurement(str_local, cmp_time_search);
      //! Update oru result-set:
    }
    const uint nrows = obj.listOf_rel_size; s_db_ds_directMapping_t obj_direct = obj;
#define __MiF__constructRandomNumbers_head(cnt_base) ({const uint size = cnt_base * (cnt_eval+1); s_kt_list_1d_uint_t obj = init__s_kt_list_1d_uint_t(size); for(uint i = 0; i < size; i++) {obj.list[i] = __MF__getHead__s_db_rel((findRelAtRandom__s_db_ds_directMapping(&obj_direct))); assert(obj.list[i] != UINT_MAX); assert(__M__isWildCart__s_db_rel_t(obj.list[i]) == false); } obj;}) //! ie, return the enw-consturcted object.
#define __MiF__constructRandomNumbers_rt(cnt_base) ({const uint size = cnt_base * (cnt_eval+1); s_kt_list_1d_uint_t obj = init__s_kt_list_1d_uint_t(size); for(uint i = 0; i < size; i++) {obj.list[i] = __M__wildCard__s_db_rel_t; } obj;}) //! ie, return the enw-consturcted object.
#define __MiF__constructRandomNumbers_tail(cnt_base) ({const uint size = cnt_base * (cnt_eval+1); s_kt_list_1d_uint_t obj = init__s_kt_list_1d_uint_t(size); for(uint i = 0; i < size; i++) {obj.list[i] = __MF__getTail__s_db_rel((findRelAtRandom__s_db_ds_directMapping(&obj_direct))); assert(obj.list[i] != UINT_MAX); } obj;}) //! ie, return the enw-consturcted object.


    //const uint nrows = cnt_heads;
    { //! Search: find the diameter (ie, max-path-dsitance) seperately for 'k' different 'start-vertices' ... 'as input' use a two files (for relations and synonyms)
      for(uint cnt_eval = 0; cnt_eval < inSearchCntToEvaluateMax__each__cnt; cnt_eval++) {
	s_kt_list_1d_uint_t listOf_heads = __MiF__constructRandomNumbers_head(inSearchCntToEvaluateMax__for__head);
	s_kt_list_1d_uint_t listOf_predicates = __MiF__constructRandomNumbers_rt(inSearchCntToEvaluateMax__for__predicate);
	s_kt_list_1d_uint_t listOf_tails = __MiF__constructRandomNumbers_tail(inSearchCntToEvaluateMax__for__tail);
	s_kt_list_1d_uint_t listOf_preSelection_predictes = __MiF__constructRandomNumbers_rt(inSearchCntToEvaluateMax__for__preSelection__predicate);
	s_kt_list_1d_uint_t listOf_preSelection_tails = __MiF__constructRandomNumbers_tail(inSearchCntToEvaluateMax__for__preSelection__tail);
	//!
	//! Construct the search-object:
	s_db_searchNode_rel_t retObj_filterBeforeRecursion = setToEmpty__s_db_searchNode_rel_t();
	s_db_searchNode_rel_t retObj_inRecursionTraversal = setToEmpty__s_db_searchNode_rel_t();
	const uint cnt_heads = obj.cnt_head;
	//assert(cnt_heads == obj.cnt_head);
	s_db_searchResult_rel_t obj_search = buildSample__s_db_searchResult_rel_t(cnt_heads, typeOf_useCase, &listOf_heads, &listOf_predicates, &listOf_tails, &listOf_preSelection_predictes, &listOf_preSelection_tails, &retObj_filterBeforeRecursion, &retObj_inRecursionTraversal);
	obj_search.config.threshold__scalar__cntVerticesToInvestigate__max = threshold__scalar__cntVerticesToInvestigate__max; 
	obj_search.config.configLogics__isToApply__synonymMappings__inFunction = configLogics__isToApply__synonymMappings__inFunction;
	//!
	//! What we expecT:
	assert(cnt_heads == obj_search.mapOf_visistedVertices.list_size);
	{ //! Validate configuraiton:
	  if(obj_search.config.search_preSelectionCriteria) {
	    assert(obj_search.config.search_preSelectionCriteria->listOf_heads.list_size >= 1);
	    assert(obj_search.config.search_preSelectionCriteria->listOf_predicates.list_size >= 1);
	    assert(obj_search.config.search_preSelectionCriteria->listOf_tails.list_size >= 1);
	  }
	}
	if(false) { //! Then write out the researhc which we 'apply':
	  //! Note: in [”elow] we examplify how to 'access' our generated search-triplets.
	  const uint vertex_id = UINT_MAX;
	  printf("at %s:%d\n", __FILE__, __LINE__);
	  if(obj_search.config.search_preSelectionCriteria) { //! Frist for the search-pre-conditiosn:
	    uint arrOf_search_size = 0;
	    s_db_rel_t *arrOf_search = getSearchNodes__s_db_searchNode_rel_t(obj_search.config.search_preSelectionCriteria, /*vertex=*/vertex_id, &arrOf_search_size, /*isTo__applyRecursiveCall=*/false);
	    printf("\n#! The search-booleanConditions which we use for each vertex are:\n");
	    assert(arrOf_search); assert(arrOf_search_size);
	    for(uint i = 0; (i < arrOf_search_size); i++) {
	      __MF__print__stdOut__s_db_rel(arrOf_search[i]);
	    }
	    if(arrOf_search) {__MF__free__s_db_rel(arrOf_search);} //! ie, then de-allocate the latter.
	  }
	  //! ------
	  if(obj_search.config.search_inRecursive) { //! Frist for the 'recusive steps':
	    uint arrOf_search_size = 0;
	    s_db_rel_t *arrOf_search = getSearchNodes__s_db_searchNode_rel_t(obj_search.config.search_inRecursive, /*vertex=*/vertex_id, &arrOf_search_size, /*isTo__applyRecursiveCall=*/true); //! where the latter "true" param is of impotance, ie, to 'make the caller' awayre of the fact that we are using a 'ressucive object'.
	    printf("\n#! The recursive-search which we apply/use for each vertex are:\n");
	    assert(arrOf_search); assert(arrOf_search_size);
	    for(uint i = 0; (i < arrOf_search_size); i++) {
	      __MF__print__stdOut__s_db_rel(arrOf_search[i]);
	    }
	    if(arrOf_search) {__MF__free__s_db_rel(arrOf_search);} //! ie, then de-allocate the latter.
	  }
	}
	assert(cnt_heads == obj_search.mapOf_visistedVertices.list_size);
	assert(cnt_heads == obj.cnt_head);
	//!
	//! Apply the search:
	printf("# Starts search, at %s:%d\n", __FILE__, __LINE__);
	start_time_measurement(); //! ie, start the time-emasurement:
	const bool is_ok = searchFor__nestedSet__s_db_ds_directMapping_t(&obj, &obj_search, config__isTo_preCompute_preSearchConditions);
	assert(is_ok);
	if(true) {
	  printf("Result(query)\t cntNodesExpanded=%u, at %s:%d\n", obj_search.result_cnt_nodesExpanded, __FILE__, __LINE__);
	  assert(obj_search.result_cnt_nodesExpanded > 0); // TODO: cosndier removign 'this'.
	}
	//! De-allocate:
	free__s_db_searchResult_rel_t(&obj_search);
	free__s_kt_list_1d_uint_t(&listOf_heads);
	free__s_kt_list_1d_uint_t(&listOf_predicates);
	free__s_kt_list_1d_uint_t(&listOf_tails);
	free__s_kt_list_1d_uint_t(&listOf_preSelection_predictes);
	free__s_kt_list_1d_uint_t(&listOf_preSelection_tails);
	{ //! Complete time-measurement:
	  char str_local[2000] = {'\0'}; sprintf(str_local, "[%u]search::loadFromFile(%s):useNorm=%s", cnt_eval, getString__e_db_ds_directMapping__initConfig(objConfig), (useNorm == 1) ? "true" : "false");
	  float cmp_time_search = 0;
	  const float endTime = end_time_measurement(str_local, cmp_time_search);
	  //! Update oru result-set:
	  fprintf(stdout, "endTime:%f\n", endTime);
	}
	//s_kt_list_1d_uint_t listOf_heads = __MiF__constructRandomNumbers(inSearchCntToEvaluateMax__for__head);
      } 
      //! Case: (head, *, *)

      // FIXME: add seomthing.
    }
    //! De-allocate
    free__s_db_ds_directMapping_t(&obj);
  }
  


  //!
  //! De-allocate:


  //!
  //! @return
#ifndef __M__calledInsideFunction
#undef __Mi__getRandVal
  return true;
#endif
}
