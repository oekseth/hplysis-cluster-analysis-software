#ifndef kt_clusterAlg_SOM_resultObject_h
#define kt_clusterAlg_SOM_resultObject_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_kt_clusterAlg_exportFormat.h" //! which is used to enumerate diffferent cluster-export-foramts (oekseth, 06. jan. 2016).

/**
   @file kt_clusterAlg_fixed_resultObject
   @brief a result-object for the SOM clustering (oesketh, 06. nov. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/


/**
   @struct s_kt_clusterAlg_SOM_resultObject
   @brief provide a structured access to the result-objects generated in our k-means clustering-procedure (oekseth, 06. nov. 2016).
 **/
typedef struct s_kt_clusterAlg_SOM_resultObject {
  t_float*** celldata;
  uint **vertex_clusterid;
  uint **cluster_2d_to_centroidVertex;
  uint nxgrid; uint nygrid; uint nrows;
} s_kt_clusterAlg_SOM_resultObject_t;

//! Intiate the s_kt_clusterAlg_SOM_resultObject_t object
s_kt_clusterAlg_SOM_resultObject_t setToEmptyAndreturn__s_kt_clusterAlg_SOM_resultObject_t();
//! Intiate the s_kt_clusterAlg_SOM_resultObject_t object
void setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(s_kt_clusterAlg_SOM_resultObject_t *self, const uint nrows, const uint nxgrid, const uint nygrid);
//! De-allcoates the s_kt_clusterAlg_SOM_resultObject_t object
void free__s_kt_clusterAlg_SOM_resultObject_t(s_kt_clusterAlg_SOM_resultObject_t *self);

//! Get the cluster-errorr assicated to the cluster-id in question.
//! @remarks if the vlaue is not set then we assuem there is a 'zero cluste-errors', ie, for which the scalar_result is then set to "0"
//! @return true if the operation went smooth
static inline __MC__inline bool get_vertex_clusterPair__s_kt_clusterAlg_SOM_resultObject_t(const s_kt_clusterAlg_SOM_resultObject_t *self, const uint row_id, uint *scalar_x, uint *scalar_y, uint *centroid_id, t_float *scalar_score) {
  assert(self);
  assert(row_id < self->nrows);
  assert(self->vertex_clusterid);
  assert(self->cluster_2d_to_centroidVertex);
  if(self && (row_id < self->nrows)) {
    *scalar_x = self->vertex_clusterid[row_id][0];
    *scalar_y = self->vertex_clusterid[row_id][1];
    assert(*scalar_x < self->nxgrid);
    assert(*scalar_y < self->nygrid);
    *centroid_id = self->cluster_2d_to_centroidVertex[*scalar_x][*scalar_y];
    printf("(fetch)\tgrid[%u][%u] has centralVertex=%u, at %s:%d\n", *scalar_x, *scalar_y, *centroid_id, __FILE__, __LINE__);
    assert(*centroid_id != UINT_MAX);
    assert(self->celldata);
    assert(self->celldata[*scalar_x]);
    assert(self->celldata[*scalar_x][*scalar_y]);
    *scalar_score = self->celldata[*scalar_x][*scalar_y][row_id];
    return 1;
  } else {return 0;} //! ie, as we assumet erhe was a 'bug' in teh operiaotn to 'fetch' the result.    
}
/**
   @brief merge the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @param <previousCount__nxGrid> the number opf 'previous' nx-grids which have been added (eg, forest_id*nxgrid): used to provide a speration betwene the clsuter-results, ie, as we assume each clsuter-result to be 'dsijoint': expected to 'correspond' to "nxgrid*forestId" in a disjoitn-forest-iteraiotn-parition-stragy (oekseth, 06. des. 2016).
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void mergeResult__s_kt_clusterAlg_SOM_resultObject_t(s_kt_clusterAlg_SOM_resultObject_t *self, s_kt_clusterAlg_SOM_resultObject_t *clusterResults, const uint *keyMap_localToGlobal, const uint previousCount__nxGrid);

//! Update the SOM-result object with the centroids (of eahc SOM-clsuter), thereby simplifying the reuslt-gernation (oekseth, 06. des. 2016)
void updateObj__afterSOM_computation__s_kt_clusterAlg_SOM_resultObject(s_kt_clusterAlg_SOM_resultObject_t *self, t_float **corrMatrix, const bool transpose);

/**
   @brief Print the resutls of the clsutering in a 'human-redable-format' to stream_out (oesketh, 06. jan. 2017).
   @param <mapOf_names>  which if set is used to 'name' the indeanl indices.
   @param <stream_out> sit he file-desctiopror to be used in exprot.
   @param <seperator_cols>  is the seperator to be used to 'dsintish each column, eg, "\t"
   @param <seperator_newLine> is the seperator between each line, eg "\n"
   @param <enum_id> identifies the data-sets which are to be epxortedb
   @return true upon scucess.
**/
bool printHumanReadableResult__extensive__s_kt_clusterAlg_SOM_resultObject_t(const s_kt_clusterAlg_SOM_resultObject_t *self, char **mapOf_names, FILE *stream_out, const char *seperator_cols, const char *seperator_newLine, const e_kt_clusterAlg_exportFormat_t enum_id);


//! Print the resutls of the clsutering in a 'human-redable-format' to STDOUt.
void printHumanReadableResult__toStdout__s_kt_clusterAlg_SOM_resultObject(const s_kt_clusterAlg_SOM_resultObject_t *self, char **mapOf_names);


#endif //! EOF
