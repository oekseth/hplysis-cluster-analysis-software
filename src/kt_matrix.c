#include "kt_matrix.h"
#include "parse_main.h"
#include "def_memAlloc.h"
#include "correlation_rank.h"
//! Include a file from JCs library:
#include "export_ppm.c" //! which is used for *.ppm file-export.
#include "matrixdata.c"
#include "matrix_deviation.h" //! which is used in our "extractAndExport__deviations__singleCall__s_kt_matrix_t(..)" (oekseth, 06. jan. 2016)

//! SEt the s_kt_matrix_t object to empty.
void setTo_empty__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self);
  self->nrows = 0; self->ncols = 0; self->weight = NULL; self->matrix = NULL;
  //! ----
  self->mapOf_rowIds = NULL;
  self->mapOf_rowIds_biggestIndexInserted = 0;
  //! ----
  self->nameOf_rows = NULL; self->nameOf_columns = NULL; self->nameOf__allocationScheme__allocatedSeperatelyInEach = false;
  //! ----
  self->stream_out = NULL;
  self->isTo_useMatrixFormat  = false;
  self->isTo_inResult_isTo_useJavaScript_syntax  = false;
  self->isTo_inResult_isTo_useJSON_syntax  = false;
  self->stringOf_header  = NULL;
  self->include_stringIdentifers_in_matrix  = false;
  self->isTo_writeOut_stringHeaders  = false;
  self->cnt_dataObjects_toGenerate = 0;
  self->cnt_dataObjects_written = 0;
  self->isMemoryOwnerOf_attribute_nameOf = true; //! whihc wehn used is 'used' to avoid 'the strucutre' from de-allocating the "nameOf_columns" and "nameOf_rows" attributes
}

//! @return an nitaited amtrix with No memory-allcoatiosn:
s_kt_matrix_t setToEmptyAndReturn__s_kt_matrix_t() {
  s_kt_matrix_t self; setTo_empty__s_kt_matrix_t(&self); 
  return self;
}

//! Initates an object
void init__s_kt_matrix(s_kt_matrix_t *self, const uint nrows, const uint ncols, const bool isTo_allocateWeightColumns) {
  assert(self); assert(nrows > 0); assert(ncols > 0);
  //! Default allocations:
  setTo_empty__s_kt_matrix_t(self);

  //! Then the 'operation':
  const t_float default_value_float = 0;
  self->matrix = allocate_2d_list_float(nrows, ncols, default_value_float); 
  //! Set each element to 'no-of-itnerest':
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      self->matrix[row_id][col_id] = T_FLOAT_MAX; //! ie, 'mark' the cell as 'not of itnerest'
    }
  }
  if(isTo_allocateWeightColumns) {
    const t_float def_weight = 1; //! ie, as we then assume taht 'all vertices are of interest'.
    self->weight = allocate_1d_list_float(ncols, def_weight);
  } else {self->weight = NULL;}
  self->nrows = nrows; self->ncols = ncols; 
}



//! Initates an object 'nly' wrt. the mapOf_rowIds data-set.
void init__rowMappingTable__s_kt_matrix(s_kt_matrix_t *self, const bool isTo_initCompelte_object, const uint nrows) {
  assert(self); 
  if(isTo_initCompelte_object) {setTo_empty__s_kt_matrix_t(self);}
  assert(nrows > 0);
  self->nrows = nrows;
  const uint default_value_uint = 0;
  self->mapOf_rowIds = allocate_1d_list_uint(nrows, default_value_uint);
  self->mapOf_rowIds_biggestIndexInserted = 0;
  for(uint i = 0; i < nrows; i++) {self->mapOf_rowIds[i] = UINT_MAX;} //! ie, itniate
}


//! Initates an object both wrt. the mapOf_rowIds and wrt. the matrix (oekseth, 06. feb. 2017).
void init__rowMappingTableAndMatrix__s_kt_matrix(s_kt_matrix_t *self, const bool isTo_initCompelte_object, const uint nrows, const uint ncols) {
  assert(self); 
  if(isTo_initCompelte_object) {setTo_empty__s_kt_matrix_t(self);}
  assert(nrows > 0);
  self->nrows = nrows;
  const uint default_value_uint = 0;
  self->mapOf_rowIds = allocate_1d_list_uint(nrows, default_value_uint);
  self->mapOf_rowIds_biggestIndexInserted = 0;
  for(uint i = 0; i < nrows; i++) {self->mapOf_rowIds[i] = UINT_MAX;} //! ie, itniate
  if(nrows && ncols) {
    const t_float default_value_float = 0;
    assert(self->matrix == NULL);
    self->matrix = allocate_2d_list_float(nrows, ncols, default_value_float);
  }
}


/**
   @brief builds a sub-set-matrix from an existing superset (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'sper-matrix' to copy data from
   @param <arrOf_local_vertexMembers> is the lsit of vertices to build a sub-set for.
   @param <arrOf_local_vertexMembers_size> which is the number of vertex-idneitfies fond in arrOf_local_vertexMembers
   @param <mapOf_globalTo_local> which if != NULL is updated with the 'enw' mapping from 'global' to 'local' vertex-mappings.
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__buildSubset_fromMatrix__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const uint *arrOf_local_vertexMembers, const uint arrOf_local_vertexMembers_size, uint *mapOf_globalTo_local, const bool isTo_updateNames) {
  assert(self); assert(superset);
  assert(arrOf_local_vertexMembers_size < superset->nrows); //! ie, as callign this object will iotherwise pose an overhead wrt. exeuction-time.
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.

#define __internalMacro__getGlobalVertexFromLocal(local_id) ({arrOf_local_vertexMembers[local_id];})
#define __internalMacro__rowsSameAsColumns 1
  //! Apply the logics:
#include "kt_matrix__stub__buildSubset.c"  
#undef __internalMacro__getGlobalVertexFromLocal
#undef __internalMacro__rowsSameAsColumns
  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;
}

/**
   @brief builds a sub-set-matrix from an existing superset (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy__useRanksEachRow__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames) {
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  assert(self); assert(superset);
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.
  //!
  //! Apply the logics:
#define __localConfig__copyWeights 0
#define __localConfig__useRanks 1
#include "kt_matrix__stub__buildSubset__useRanks.c"

  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;
}

//! a simple-logic fitler-strategy to increase the specififty of the data.
bool kt_matrix__filter__simpleMask__kt_api(s_kt_matrix_t *matrix_input, t_float threshold_min, t_float threshold_max, const bool useRelativeScore_betweenMinMax) {
  if(useRelativeScore_betweenMinMax) {
    t_float minValue = T_FLOAT_MAX; t_float maxValue = T_FLOAT_MIN_ABS;
    t_float avg = 0;
    uint cnt_interesting = 0;
    for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
      for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	const t_float score = matrix_input->matrix[row_id][col_id];
	if(isOf_interest(score) && !isnan(score)) {
	//if(isOf_interest(score)) {
	  minValue = macro_min(minValue, score);
	  maxValue = macro_max(maxValue, score);
	  avg += score;
	  cnt_interesting++;
	}
      }
    }
    if(cnt_interesting == 0) {
      fprintf(stderr, "!!\t All cells are masked aways, ie, please valdiate that your call was 'with intention': if the latter message sounds surprising, it could be that you have forgotten to specify your input-matrix. An observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
    
    const t_float diff = maxValue - minValue; 
    if(minValue > maxValue) {
      fprintf(stderr, "!!\t Seems like the sum of valeus was grather than the float-dataype w/size=%u ahs the ability to handle. Hence, consider using a different tydatatype (which may be adjsuted during the isntallation of the hpLysis softare). Observaiton at [%s]:%s:%d\n", (uint)sizeof(t_float), __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
    if(diff < 0) {
      fprintf(stderr, "!!\t Seems like the sum of valeus was grather than the float-dataype w/size=%u ahs the ability to handle; min=%f > max=%f. Hence, consider using a different tydatatype (which may be adjsuted during the isntallation of the hpLysis softare). Observaiton at [%s]:%s:%d\n", (uint)sizeof(t_float), maxValue, minValue, __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
    fprintf(stderr, "info: \t min=%f > max=%f. Observaiton at [%s]:%s:%d\n", maxValue, minValue, __FUNCTION__, __FILE__, __LINE__);
    assert(diff >= 0); //! ie, asw we otherwise have an erorr in [ªbov€] min-max-macros
    if( (threshold_min != T_FLOAT_MIN) && (threshold_min != T_FLOAT_MAX) ) {
      if( (threshold_min < 0) || (threshold_min > 1) ) {
	fprintf(stderr, "!!\t Seems like your  have an on-ocnsistnecy in your threshold_min=%f value, ie, as we for 'relative valeus' assuems a value in range [0, 1], ie, pelase udpate your code. For quesitons please cotnat the senhior devleoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", threshold_min, __FUNCTION__, __FILE__, __LINE__);
	//! 
	//! Use a 'work-around':
	threshold_min = 0.1;
      } else {
	const t_float avg_adjusted = avg - threshold_min;
	threshold_min = avg + avg_adjusted * threshold_min;
      }
    }
    if( threshold_max != T_FLOAT_MAX) {
      if( (threshold_max < 0) || (threshold_max > 1) ) {
	fprintf(stderr, "!!\t Configuraiton-error: we expected a relatie score-valeus to be used in threshodling of cells. Seems like your  have an on-ocnsistnecy in your threshold_max=%f value, ie, as we for 'relative valeus' assuems a value in range [0, 1], ie, pelase udpate your code. For quesitons please cotnat the senhior devleoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", threshold_max, __FUNCTION__, __FILE__, __LINE__);
	assert(false);
	//! 
	//! Use a 'work-around':
	threshold_max = 0.1;
      } else {
	const t_float avg_adjusted = maxValue - avg;
	threshold_max = avg + avg_adjusted * threshold_max;
      }
    }
  }
  if(threshold_min == T_FLOAT_MAX) {threshold_min = T_FLOAT_MIN_ABS;}

  for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
    for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
      const t_float score = matrix_input->matrix[row_id][col_id];
      if( (score <= threshold_min) || (score >= threshold_max) ) {
	//! Then 'apply' the mask:
	matrix_input->matrix[row_id][col_id]  = T_FLOAT_MAX;
      }
    }
  }
  //! --------------------------
  //!
  //! @return true, ie, as we assuemt he operaiton was a sucd3ss 'at this exueciton-point'.
  return true;
}



/**
   @brief copies a given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames) {
  assert(self); assert(superset);
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.
  //!
  //! Apply the logics:
#define __localConfig__copyWeights 1
#define __localConfig__useRanks 0
#include "kt_matrix__stub__buildSubset__useRanks.c"

  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;
}

/**
   @brief copies a trasnposed version of a given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy_transposed__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames) {
  assert(self); assert(superset);
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.
  //!
  //! Apply the logics:
  //#define __localConfig__copyWeights 1
#define __localConfig__useRanks 0
#include "kt_matrix__stub__buildSubset__useRanks__transposed.c"

  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;  
}
/**
   @brief copies a trasnposed version of a given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy_transposed__thereafterRank__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames) {
  assert(self); assert(superset);
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.
  //!
  //! Apply the logics:
  //#define __localConfig__copyWeights 1
#define __localConfig__useRanks 1
#include "kt_matrix__stub__buildSubset__useRanks__transposed.c"

  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;  
}
/**
   @brief builds a sub-set-matrix from an existing superset (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'sper-matrix' to copy data from
   @param <arrOf_local_vertexMembers_head> is the lsit of vertices to build a sub-set for: for the rows/heads
   @param <arrOf_local_vertexMembers_head_size> which is the number of vertex-idneitfies fond in arrOf_local_vertexMembers_head: for the rows/heads
   @param <arrOf_local_vertexMembers_tail> is the lsit of vertices to build a sub-set for: for the columns/tails
   @param <arrOf_local_vertexMembers_tail_size> which is the number of vertex-idneitfies fond in arrOf_local_vertexMembers_tail: for the columns/tails
   @param <mapOf_globalTo_local> which if != NULL is updated with the 'enw' mapping from 'global' to 'local' vertex-mappings.
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks 
   - is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
   - is different from "init__buildSubset_fromMatrix__s_kt_matrix(..)" wr.t the 'fact' that we asusem the rows-ids are different from teh coolumn-ids
   - wrt. the "mapOf_globalTo_local" we only update "mapOf_globalTo_local" for the 'row' case (ie, as we asusem only the 'rows' are used in the result-table, while ie, as the columsn are expected to describe/present the feautres)
**/
bool init__buildSubset_rowsDifferentFromColumns__fromMatrix__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const uint *arrOf_local_vertexMembers_head, const uint arrOf_local_vertexMembers_head_size, const uint *arrOf_local_vertexMembers_tail, const uint arrOf_local_vertexMembers_tail_size, uint *mapOf_globalTo_local, const bool isTo_updateNames) {
  assert(self); assert(superset);
  assert(arrOf_local_vertexMembers_head_size < superset->nrows); //! ie, as callign this object will iotherwise pose an overhead wrt. exeuction-time.
  assert(arrOf_local_vertexMembers_tail_size < superset->ncols); //! ie, as callign this object will iotherwise pose an overhead wrt. exeuction-time.
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.

#define __internalMacro__getGlobalVertexFromLocal_head(local_id) ({arrOf_local_vertexMembers_head[local_id];})
#define __internalMacro__getGlobalVertexFromLocal_tail(local_id) ({arrOf_local_vertexMembers_tail[local_id];})
#define __internalMacro__rowsSameAsColumns 0 //! ie, as we use seperate 'talbes' for the column-mappigns and row-mappins'.
  //! Apply the logics:
#include "kt_matrix__stub__buildSubset.c"  
#undef __internalMacro__getGlobalVertexFromLocal
#undef __internalMacro__rowsSameAsColumns
  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;
}



/**
   @brief merge two matrices and return the new-constructed matrix (oekseth, 06. june. 2017).
   @param <mat_1> is the first inptu-amtrix
   @param <mat_2> is the second inptu-atmrix
   @param <mat_2_prefixInput_rows> optioanl: if set is sued to 'append't eh row-sitrng-naems (ie, if the row-stirng-naems are sued): otehrwise ingored.
   @return the new-sotrnctued matrix.
**/
s_kt_matrix_t merge_twoMatrices__s_kt_matrix_t(const s_kt_matrix_t *mat_1, const s_kt_matrix_t *mat_2, const char *mat_2_prefixInput_rows) {
  assert(mat_1); assert(mat_2);
  s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t();
  const uint ncols = macro_max(mat_1->ncols, mat_2->ncols);
  if(ncols == 0) {return self;} //! ie, as there is then nothign to do.
  const uint nrows = mat_1->nrows + mat_2->nrows; //! ie, as we assuemt eh 'itnerest' is tot uify a matirx wr.t the row-numbers, ie, hence the "mat_2_prefixInput_rows" opnal data-input.
  self = initAndReturn__s_kt_matrix(nrows, ncols);
  //assert(mat_1->isTo_allocateWeightColumns == NULL); //! ie, our assumptioni: otehrwise we ened assding support for this.
  assert(mat_2->weight == NULL); //! ie, our assumptioni: otehrwise we ened assding support for this.
  uint index_offset = 0;
  const bool isTo_updateNames = true;
#define __localConfig__useRanks 0
  if(mat_1->nrows > 0) { //! then we isnert/integrate teh naem-logics:
    const s_kt_matrix_t *superset = mat_1;
    const char *superset_prefixInput_row = NULL;
    assert(superset);
    assert((superset->nrows + index_offset) <= self.nrows);
#include "kt_matrix__stub__mergeMatrix.c" //! ie, where latter appleis teh 'emrge' lgocis using the "index_offset", "supertset" and "superset_prefixInput_row" to crrrectly set/asisgn the row-names (oesketh, 06. june. 2017).
    //!
    //! Increment the global-id-offset:
    // printf("index_offset=%u, insert-nrows=%u, self.nrows=%u, at %s:%d\n", index_offset, superset->nrows, self.nrows, __FILE__, __LINE__);
    index_offset += superset->nrows;
  }
  if(mat_2->nrows > 0) { //! then we isnert/integrate teh naem-logics:
    const s_kt_matrix_t *superset = mat_2;
    assert(superset);
    assert((superset->nrows + index_offset) <= self.nrows);
    const char *superset_prefixInput_row = mat_2_prefixInput_rows;
#include "kt_matrix__stub__mergeMatrix.c" //! ie, where latter appleis teh 'emrge' lgocis using the "index_offset", "supertset" and "superset_prefixInput_row" to crrrectly set/asisgn the row-names (oesketh, 06. june. 2017).
    //!
    //! Increment the global-id-offset:
    //printf("index_offset=%u, insert-nrows=%u, self.nrows=%u, at %s:%d\n", index_offset, superset->nrows, self.nrows, __FILE__, __LINE__);
    index_offset += superset->nrows;
  }
  assert(index_offset == self.nrows); //! ie, what we for cosnistnecy expects. 

//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //!
  //! @return the new-cosntructed matrix:
  return self;
#undef __localConfig__useRanks
}



/**
   @brief merge two matrices, uidpåating the "mat_1" (oekseth, 06. june. 2017).
   @param <mat_1> is the first inptu-amtrix, ie, the "self" object.
   @param <mat_2> is the second inptu-atmrix
   @param <mat_2_prefixInput_rows> optioanl: if set is sued to 'append't eh row-sitrng-naems (ie, if the row-stirng-naems are sued): otehrwise ingored.
 **/
void insertMatrix_intoMatrix__s_kt_matrix_t(s_kt_matrix_t *mat_1, const s_kt_matrix_t *mat_2, const char *mat_2_prefixInput_rows) {
  assert(mat_1); assert(mat_2);
  s_kt_matrix_t self = merge_twoMatrices__s_kt_matrix_t(mat_1, mat_2, mat_2_prefixInput_rows);
  //! udpat ehte "mat_1" object, and then complete.
  free__s_kt_matrix(mat_1); *mat_1 = self;  
}


//! De-allocate sthe nameOf_columns attribute (fi the latter differns from the "nameOf_rows" attribute) (oekseth, 06. des. 2016).
//! @remarks a use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016).
void free_arrOf_names_columns__s_kt_matrix(s_kt_matrix_t *self) {
  assert(self);
  if(self->isMemoryOwnerOf_attribute_nameOf == true) {
    const bool rows_equals_cols = (self->nameOf_columns == self->nameOf_rows);
    if(self->nameOf_columns) {
      if(rows_equals_cols == false) {
	if(self->nameOf__allocationScheme__allocatedSeperatelyInEach == false) {
	  free_2d_list_char(&(self->nameOf_columns), self->ncols); 
	} else { //! then we de-allcoate seperately for each 'chunk':
	  for(uint i = 0; i < self->ncols; i++) {
	    if(self->nameOf_columns[i]) {
	      free_1d_list_char(&(self->nameOf_columns[i]));
	    }
	  }
	  free_generic_type_2d(self->nameOf_columns); 
	}
      } //! otherwise we assuem the nameOf_columns was implictly freed wrt. [ªbove] "nameOf_rows"
    } //! else we asusme a differetn object will 'de-allcoate this objhect'.
    self->nameOf_columns = NULL;
  }
}

//! De-allcoate the s_kt_matrix_t object.
void free__s_kt_matrix(s_kt_matrix_t *self) {
  assert(self);
  //! ---
  if(self->matrix) {free_2d_list_float(&(self->matrix), self->nrows); self->matrix = NULL;}
  if(self->weight) {free_1d_list_float(&(self->weight)); self->weight = NULL;}
  if(self->mapOf_rowIds) {free_1d_list_uint(&(self->mapOf_rowIds)); self->mapOf_rowIds = NULL;}
  //! ---

  if(self->isMemoryOwnerOf_attribute_nameOf == true) {  
    free_arrOf_names_columns__s_kt_matrix(self); //! ie, first de-allcoate the columns: this 'order' given the often-seen-use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016).
    if(self->nameOf_rows) {
      if(self->nameOf__allocationScheme__allocatedSeperatelyInEach == false) {
	free_2d_list_char(&(self->nameOf_rows), self->nrows); 
      } else { //! then we de-allcoate seperately for each 'chunk':
	for(uint i = 0; i < self->nrows; i++) {
	  if(self->nameOf_rows[i]) {
	    free_1d_list_char(&(self->nameOf_rows[i]));
	  }
	}
	free_generic_type_2d(self->nameOf_rows); 
      }
      self->nameOf_rows = NULL;
    }
  } else { //! then we asusme a different object will 'de-allocate tehse':
    self->nameOf_rows = NULL;
    self->nameOf_columns = NULL;
  }
  //! ---
  self->nrows = 0;   self->ncols = 0;
  if(self->stream_out != NULL) {
    if(
       (self->stream_out != stderr) &&
       (self->stream_out != stdout)
       ) {
      fclose(self->stream_out); self->stream_out = NULL;
    }
  }
}

//! Updates "self" with the value-frequency-deisitrubioin in "data" (oekseth, 06. feb. 2017).
bool extractSubset_atIndex_type_frequencyMatrix__s_kt_matrix_t(s_kt_matrix_t *self, const s_kt_matrix_t *data, const uint row_id_input) {
  assert(self); assert(self->nrows > 0); assert(self->ncols > 1);
  assert(self->matrix);   
  if(row_id_input >= self->nrows) {fprintf(stderr, "!!\t The inseritoni is oputside the range: 'row_id=%u < %u' does Not hold, ie, please udpate your API. For qeustions please contact the senior delvleoper at [eosekth@gmail..com]. Observaiotn at [%s]:%s:%d\n", row_id_input, self->nrows, __FUNCTION__, __FILE__, __LINE__); assert(false); return false;}
  assert(self->matrix[row_id_input]);
  //! -----------------------------
  //!
  //! Find min-max-count
  t_float min_score = T_FLOAT_MAX; t_float max_score = T_FLOAT_MIN_ABS;
  for(uint row_id = 0; row_id < data->nrows; row_id++) {
    assert(data->matrix[row_id]);
    for(uint col_id = 0; col_id < data->ncols; col_id++) {
      const t_float score_1 = data->matrix[row_id][col_id];
      if(isOf_interest(score_1)) {
	min_score = macro_min(min_score, score_1);
	max_score = macro_max(max_score, score_1);
      }
    }
  }
  assert(min_score <= max_score);
  //!   
  if( 
     (min_score != T_FLOAT_MAX)
     && (max_score != min_score) // TODO: validate correctness of this requirement.
      ) {
    assert(max_score != T_FLOAT_MIN_ABS);
    assert(min_score != T_FLOAT_MIN_ABS);
    //! 
    //! Find bucket-range:
    const t_float value_diff = max_score - min_score;
    const t_float bucket_size = (value_diff)/(t_float)self->ncols; //! ie, the 'chunk-size' in each bucket: "(score - min_score)/bucket_size" 'gives' the bucket-size:
    /* if(bucket_size <= 1) { */
    /*   //! ----------------------------- */
    /*   //! @return */
    /*   return true; */
    /* } */
    //printf("bucket_size=%f given range=[%f, %f] and cntBuckets=%u, at %s:%d\n", bucket_size, min_score, max_score, self->ncols, __FILE__, __LINE__);
    // assert(bucket_size != 0); // TODO: if this does not hold then figure out how handling the case.
    // TODO[aritcle]: consider updaitng our aritlce wsr.t [”elow] optmziaiton-strategy ... ie, as an example of a coding-stragey which improeves perofmrance.
    const t_float bucket_size__inv = 1.0/bucket_size; //! ie, an optmziaitons-strategy both wrt. "1/bucket_size" and wrt. the no-need for the "if(score_1 != min_score) {bucket_index = (score_1 - min_score)/bucket_size;}" if-calsue-cofnitional-logic-branching.
    assert(bucket_size__inv != 0);
    //! 
    //! Itnite to empty:
    //! Note: we sue the 'loint' data-type ot 'ensure' that we do Not get any voerflows wrt. the max-count for a givne value.
    const loint empty_0 = 0; loint *arrOf_counts = allocate_1d_list_loint(self->ncols, empty_0);

    /* for(uint col_id = 0; col_id < self->ncols; col_id++) { */
    /*   self->matrix[row_id][bucket_index] = 0; */
    /* } */
    //! -----------------------------
    //!
    //! Update frquency matrix:
    const uint ncols_minusOne = self->ncols - 1;
    for(uint row_id = 0; row_id < data->nrows; row_id++) {
      for(uint col_id = 0; col_id < data->ncols; col_id++) {
	const t_float score_1 = data->matrix[row_id][col_id];
	if(isOf_interest(score_1)) {
	  uint bucket_index = (uint)((score_1 - min_score)*bucket_size__inv);
	  // printf("bucket_index=%u given score=%f, diff=%f, bucket_size__inv=%f, ncols=%u, at %s:%d\n", bucket_index, score_1, score_1 - min_score, bucket_size__inv, self->ncols, __FILE__, __LINE__);
	  bucket_index = macro_min(bucket_index, ncols_minusOne);
	  assert_possibleOverhead(bucket_index < self->ncols);
	  //! Update:
	  arrOf_counts[bucket_index]++;
	  //self->matrix[row_id][bucket_index] += 1;
	}
      }      
    }
    //! -----------------------------
    //!
    //! Adjust by the relaive sigicnace of the count;
    //! Note[performnace]: the 'sepration' of [”elow] as a sperate step (ie, instead of including [”elow] in [ªbove]) has several advangates: (a) we use integers rahter than flatoing-point in the '+=' aritmethci (ie, goeds faster) and (b) we avoid loss-of-accuracy (for large data-sets), ie, increased result-rpediciton-accuracy.
    const t_float totalInserted__inv = 1.0/(t_float)(data->nrows * data->ncols);
    assert(totalInserted__inv != 0);
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      self->matrix[row_id_input][col_id] = arrOf_counts[col_id]*totalInserted__inv;
      //self->matrix[row_id][bucket_index] *= totalInserted__inv;
    }
    //!
    //! De-allcoate lcoally reserved list:
    free_1d_list_loint(&arrOf_counts); arrOf_counts = NULL;
  }/*  else { //! then we asusme that no valeus are of interest, a case which may 'hold' for some masked inptu-atmrices. */
  /*   assert(max_score == T_FLOAT_MIN_ABS); */
  /* } */
  //! -----------------------------
  //! @return
  return true;
}



//! Update a matrix to a symmetric strucutre, where the 'average' value is used (when updating two cells with valeus) (oekseht, 06. jan. 2017).
void updateTo_symmetric__useAvg__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self);
  assert(self->nrows); assert(self->ncols);
  if(!self || !self->nrows || !self->ncols) {return;}
  if(self->nrows != self->ncols) {
    fprintf(stderr, "!!\t Expected a symmetric data-input-matrix, which is Not the case: matrix is in diemsions=[%u, %u]. If reading the documentiaotn does Not help then please cotnact the senior develoepr at [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", self->nrows, self->ncols, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
    return;
  }
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      const t_float score_1 = self->matrix[row_id][col_id];
      const t_float score_2 = self->matrix[col_id][row_id];
      if(isOf_interest(score_1) && isOf_interest(score_2)) {
	self->matrix[row_id][col_id] = self->matrix[col_id][row_id] = 0.5*(score_1 * score_2);
      } else if(isOf_interest(score_1)) {
	self->matrix[row_id][col_id] = self->matrix[col_id][row_id] = 0.5*(score_1); //! todo: validate correctness fo this 'halfing'.
      } else if(isOf_interest(score_2)) {
	self->matrix[row_id][col_id] = self->matrix[col_id][row_id] = 0.5*(score_2); //! todo: validate correctness fo this 'halfing'.
      }
    }
  }
}

/**
   @brief Set the default value to all the cells in the matrix
   @param <self> is the object to update
   @param <score_default> is the score to set
**/
void setDefaultValueTo__allCells__kt_matrix(s_kt_matrix_t *self, const t_float score_default) {
  assert(self); assert(self->matrix); assert(self->nrows); assert(self->ncols);
  if(!self || !self->matrix || !self->nrows || !self->ncols) {
    fprintf(stderr, "!!\t Seems like you did not remember to itnaite your object (to a specified size), ie, please remmeber to first call your your init-fucntion: if this emssage sounds odd then please cotnact the developer at [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      self->matrix[row_id][col_id] = score_default;
    }
  }
}
/**
   @brief Set the default value to all the cells in the matrix which are below min_threshold
   @param <self> is the object to update
   @param <min_threshold> is the min-value whicht he scores need to be [ªbove].
**/
void setDefaultValueTo_empty__whichAreBelowThreshold__s_kt_matrix_t(s_kt_matrix_t *self, const t_float min_threshold) {
  assert(self); assert(self->matrix); assert(self->nrows); assert(self->ncols);
  if(!self || !self->matrix || !self->nrows || !self->ncols) {
    fprintf(stderr, "!!\t Seems like you did not remember to itnaite your object (to a specified size), ie, please remmeber to first call your your init-fucntion: if this emssage sounds odd then please cotnact the developer at [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      if(self->matrix[row_id][col_id] < min_threshold) {
	self->matrix[row_id][col_id] = T_FLOAT_MAX;
      }
    }
  }
}

void setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(const s_kt_matrix_t *self, const uint *vertex_clusterId, const uint vertex_clusterId_size, const uint *map_vertexGroups_cols) {
  assert(self); 
  assert(vertex_clusterId);
  assert(self->nrows > 0);
  assert(self->ncols > 0);  
  assert(vertex_clusterId_size <= self->nrows);
  assert(vertex_clusterId_size <= self->ncols);
  if(map_vertexGroups_cols == NULL) {
    assert(self->nrows == self->ncols); // TODO: cosnider providng support 'for this case'.
  }
  for(uint row_id = 0; row_id < vertex_clusterId_size; row_id++) {
    if(map_vertexGroups_cols != NULL) {
      const uint max_cnt = macro_min(self->ncols, vertex_clusterId_size);
      for(uint col_id = 0; col_id < max_cnt; col_id++) {
	if(map_vertexGroups_cols[row_id] != map_vertexGroups_cols[col_id]) {
	  self->matrix[row_id][col_id] = T_FLOAT_MAX;
	}
      }
    } else {
      const uint max_cnt = macro_min(self->ncols, vertex_clusterId_size);
      for(uint col_id = 0; col_id < max_cnt; col_id++) {
	if(vertex_clusterId[row_id] != vertex_clusterId[col_id]) {
	  self->matrix[row_id][col_id] = T_FLOAT_MAX;
	}
      }
    }
  }
  //!
  //! Handle the 'implcitly maksed cases':
  for(uint row_id = vertex_clusterId_size; row_id < self->nrows; row_id++) {
    if(map_vertexGroups_cols != NULL) {
      // TODO: validate correctness of [bleow] (oesketh, 06. des. 2016).
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	self->matrix[row_id][col_id] = T_FLOAT_MAX;
      }
    } else {
      for(uint col_id = vertex_clusterId_size; col_id < self->ncols; col_id++) {
	self->matrix[row_id][col_id] = T_FLOAT_MAX;
      }
    }
  }
}

//! Set the value in the object
void set_cell__s_kt_matrix(s_kt_matrix_t *self, const uint row_id, uint col_id, const t_float value) {
  assert(self); assert(self->matrix);
  self->matrix[row_id][col_id] = value;
}

//! Get the value in the object
void get_cell__s_kt_matrix(const s_kt_matrix_t *self, const uint row_id, uint col_id, t_float *scalar_result) {
  assert(self); assert(self->matrix);
  *scalar_result = self->matrix[row_id][col_id];
}
//! Set the value in the object
void set_weight__s_kt_matrix(s_kt_matrix_t *self, const uint index, const t_float value) {
  assert(self->weight);
  self->weight[index] = value;
  //if( index > self->current_pos) {self->current_pos = index + 1;} //! ie, the max-cnt-tiems in the set.
}

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos>
   @param <stringTo_add> is the string to copy into this strucutre
   @param <addFor_column> which if faset to false (or '0') implies that we insert into the row-set.
   @remarks we expect the index_pos to be less than "nrows" for "addFor_column==0", and index_pos to be less than ncols for "addFor_column==1";   
 **/
void set_stringConst__s_kt_matrix(s_kt_matrix_t *self, const uint index_pos, const char *stringTo_add, const bool addFor_column) {
  assert(self); 
  char **arr_local = NULL;
  if(!stringTo_add || !strlen(stringTo_add)) {
    fprintf(stderr, "!!\t String=\"%s\" seems empty. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if(addFor_column == false) {
    if(index_pos >= self->nrows) {
      fprintf(stderr, "!!\t For string=\"%s\" you have requested a row-string-inseriton at index=%u >= %u=nrows. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, index_pos, self->nrows, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
    if(self->nameOf_rows == NULL) {
      self->nameOf_rows =  (char**)alloc_generic_type_2d(char, self->nameOf_rows, self->nrows);
      for(uint i = 0; i < self->nrows; i++) {
	self->nameOf_rows[i] = NULL; //! ie, intiate.
      }
      //self->nameOf_rows =  (char**)alloc_generic_type_2d(char, &(self->nameOf_rows), self->nrows);
      self->nameOf__allocationScheme__allocatedSeperatelyInEach = true;
    }
    //! Specify the data-structre to update:
    arr_local = self->nameOf_rows;
  } else {
    if(index_pos >= self->ncols) {
      fprintf(stderr, "!!\t For string=\"%s\" you have requested a row-string-inseriton at index=%u >= %u=ncols. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, index_pos, self->ncols, __FUNCTION__, __FILE__, __LINE__);
      assert(false); // TODO: cosnider removing this.
      return;
    }
    if(self->nameOf_columns == NULL) {
      self->nameOf_columns = (char**)alloc_generic_type_2d(char, self->nameOf_columns, self->ncols);
      for(uint i = 0; i < self->ncols; i++) {
	self->nameOf_columns[i] = NULL; //! ie, intiate.
      }
      //	self->nameOf_columns = (char**)alloc_generic_type_2d(char, &(self->nameOf_columns), self->ncols);
      self->nameOf__allocationScheme__allocatedSeperatelyInEach = true;
    }
    //! Specify the data-structre to update:
    arr_local = self->nameOf_columns;
  }
  //! ------------------
  //!
  //! Allocate memory and copy-paste teh strucutre:
  const uint str_len = strlen(stringTo_add) + 1;
  assert(str_len > 0);
  const char emppty_char = 0;
  char *new_string = allocate_1d_list_char(str_len, emppty_char);
  memcpy(new_string, stringTo_add, str_len-1);
  //! Udpate our 'global' struycutre with the new string:
  assert(arr_local);
  arr_local[index_pos] = new_string;
}


//! Identify the index for the given string (ie, if the stirng is found) (oekseht, 06. des. 2016)
//! @return row_id if foudn: otherwise UINT_MAX is retunred (ie, as we then assume the string is Not found).
//! @remarks this fucntion is Not set to 'inline' as we asusme there will be now signficnat perfomrance-costs assicated to thsi funciton (ie, due to the relative low number of access which we expect).
uint getIndexOf__atRow__s_kt_matrix(const s_kt_matrix_t *self, const char *stringOf_value) {
  assert(self); assert(stringOf_value); assert(strlen(stringOf_value));
  if(self->nameOf_rows && self->nrows) {
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      const char *stringOf_cmp = self->nameOf_rows[row_id];
      if(stringOf_cmp && (strlen(stringOf_cmp) == strlen(stringOf_value))) {
	if(0 == strcmp(stringOf_cmp, stringOf_value)) {return row_id;}
      }
    }
  }
  //! At this exec-point we assume the string was Not found:
  return UINT_MAX;
}

//! Copy the names:
void copyNames_intoExistingMatrix__s_kt_matrix_t(s_kt_matrix_t *self, const s_kt_matrix_t *superset) { 
  assert(self->nrows > 0);
  assert(self->ncols > 0);
  assert(self->nrows == superset->nrows);
  assert(self->ncols == superset->ncols);
  //! 
  //! 
  const bool isTo_updateNames = true;
  // s_kt_matrix_t *self = &mat_result;
  if(isTo_updateNames) {
    if(superset->nameOf_rows != NULL) {
      bool addFor_column = false;
      for(uint local_id = 0; local_id < self->nrows; local_id++) {      
	const uint global_vertex = local_id;
	char *stringTo_add = NULL;
	//printf("----\n (get-string), at %s:%d\n", __FILE__, __LINE__);
	get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
	assert(global_vertex < superset->nrows);
	assert(stringTo_add == superset->nameOf_rows[global_vertex]);
	if(stringTo_add && strlen(stringTo_add)) {
	  set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
	}
      }
    }
    //! ---------
    if(superset->nameOf_columns != NULL) {
      bool addFor_column = true;
      for(uint local_id = 0; local_id < self->ncols; local_id++) {      
	const uint global_vertex = local_id;
	char *stringTo_add = NULL;
	get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
	assert(global_vertex < superset->ncols);
	assert(stringTo_add == superset->nameOf_columns[global_vertex]);
	if(stringTo_add && strlen(stringTo_add)) {
	  set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
	}
      }
    }
  }
}

//! Identify the index for the given string (ie, if the stirng is found) (oekseht, 06. des. 2016)
//! @return column_id if foudn: otherwise UINT_MAX is retunred (ie, as we then assume the string is Not found).
//! @remarks this fucntion is Not set to 'inline' as we asusme there will be now signficnat perfomrance-costs assicated to thsi funciton (ie, due to the relative low number of access which we expect).
 uint getIndexOf__atColumn__s_kt_matrix(const s_kt_matrix_t *self, const char *stringOf_value) {
  assert(self); assert(stringOf_value); assert(strlen(stringOf_value));
  if(self->nameOf_columns && self->ncols) {
    for(uint column_id = 0; column_id < self->ncols; column_id++) {
      const char *stringOf_cmp = self->nameOf_columns[column_id];
      if(stringOf_cmp && (strlen(stringOf_cmp) == strlen(stringOf_value))) {
	if(0 == strcmp(stringOf_cmp, stringOf_value)) {return column_id;}
      }
    }
  }
  //! At this exec-point we assume the string was Not found:
  return UINT_MAX;
}




/**
   @brief load a data-set into the data_objh, either using input-data or a value-range-distribution defined by our "stringOf_sampleData_type" param.
   @param <data_obj> the object to hold the new-cosntructed data-set: if set to a string-value, the other function-arpameters are ginored.
   @param <input_file> which if set ideitnfied the input-file to use
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <readData_fromStream> which if set implies that the data-'foudn' in "stream" is used to buidl the data-matrix: has prioerty over the stringOf_sampleData_type funciton-parameter.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @param <fractionOf_toAppendWith_sampleData_typeFor_rows> identifies the percentage of sampel-data-dsitribiton rows to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <fractionOf_toAppendWith_sampleData_typeFor_columns> identifies the percentage of sampel-data-dsitribiton columns to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <stringOf_sampleData_type_realLife> which if set is used to idnentify 'in-line' real-life data-set which are to be used.
   @param <isTo_transposeMatrix> which if set to true implies that we transpose the matrix.
   @return true on success.
 **/
bool readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file, uint cnt_rows, uint cnt_columns, const bool readData_fromStream, const char *stringOf_sampleData_type, uint fractionOf_toAppendWith_sampleData_typeFor_rows, uint fractionOf_toAppendWith_sampleData_typeFor_columns, const char *stringOf_sampleData_type_realLife, const bool isTo_transposeMatrix) {
  s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.

  //printf("default-object-dims are [%u, %u], at %s:%d\n", self->nrows, self->ncols, __FILE__, __LINE__);


  const bool ret_val = build_dataset_fromInputOrSample(&data_obj, input_file, 
						       /*cnt_rows=*/cnt_rows, /*cnt_columns=*/cnt_columns,
						       NULL, stringOf_sampleData_type, 
						       fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns,
						       stringOf_sampleData_type_realLife, isTo_transposeMatrix);

  //assert(data_obj.__allocated_stringOf_defaultRowPrefix != NULL); //! ie, as we do Not expect this to have been de-allcoted

  //printf("data_obj.cnt_rows=%u, at %s:%d\n", data_obj.cnt_rows, __FILE__, __LINE__);
  for(uint row_id = 0; row_id < data_obj.cnt_rows; row_id++) {
    //printf("row_id=%u, at %s:%d\n", row_id, __FILE__, __LINE__);
    assert(data_obj.matrixOf_data[row_id]);
  }
  
  
  //! ---------------------------
  //!
  //! Allocate:
  if(self->nrows != 0) {
    if( false == (
		  (self->nrows <= data_obj.cnt_rows) &&
		  (self->ncols <= data_obj.cnt_columns)
		  ) ) {
      fprintf(stderr, "!!\t Seems like your object-intiaotn holds an in-consistency: whiel you have arleady allcoated your object to a dimension of [%u, %u] your new-read matrxi-data 'hold' a data-set of [%u, %u] elements, ie, please try using this object correctly. For quesitons please cotnact the developerm at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", 
	      self->nrows, self->ncols,
	      data_obj.cnt_rows, data_obj.cnt_columns, __FUNCTION__, __FILE__, __LINE__);
    }
    assert(self->nrows <= data_obj.cnt_rows);
    assert(self->ncols <= data_obj.cnt_columns);
    { //! Valdiate that the rows are consisntecy intiated:
      for(uint row_id = 0; row_id < data_obj.cnt_rows; row_id++) {
	const t_float *__restrict__ row = get_row__s_dataStruct_matrix_dense(&data_obj, row_id);
	assert(row);
      }
    }
    assert(self->nrows > 0);  assert(self->ncols > 0);
  } else { //! Then we iterate through the matrix copying the result:
    assert(self->matrix == NULL);
    assert(self->weight == NULL); //! ie, waht we expect:
    // printf("sets row-sizes to [%u, %u], at %s:%d\n", data_obj.cnt_rows, data_obj.cnt_columns, __FILE__, __LINE__);
    assert(data_obj.cnt_rows > 0);
    assert(data_obj.cnt_columns > 0);
    init__s_kt_matrix(self, data_obj.cnt_rows, data_obj.cnt_columns, /*isTo_allocateWeightColumns=*/true);
    assert(self->nrows > 0);  assert(self->ncols > 0);
    { //! Valdiate that the rows are consisntecy intiated:
      for(uint row_id = 0; row_id < data_obj.cnt_rows; row_id++) {
	const t_float *__restrict__ row = get_row__s_dataStruct_matrix_dense(&data_obj, row_id);
	assert(row);
      }
    }
  }
  //!
  //! Then we copy the result into the 'current set':
  for(uint row_id = 0; row_id < data_obj.cnt_rows; row_id++) {
    assert(data_obj.matrixOf_data[row_id]);
    const t_float *__restrict__ row = get_row__s_dataStruct_matrix_dense(&data_obj, row_id);
    assert(row);
    for(uint col_id = 0; col_id < data_obj.cnt_columns; col_id++) {
      if(row[col_id] != 0) {
	self->matrix[row_id][col_id] = row[col_id];
      } //! else we assume the cell-valeu is not set.
    }
  }
  //! ---------------------------
  //!
  //! De-allocate:
  if(self->nameOf_rows == NULL) {self->nameOf_rows = data_obj.nameOf_rows; data_obj.nameOf_rows = NULL;}
  if(self->nameOf_columns == NULL) {self->nameOf_columns = data_obj.nameOf_columns; data_obj.nameOf_columns = NULL;}
  //! ---------------------------
  //!
  //! De-allocate:
  free_s_dataStruct_matrix_dense_t(&data_obj);


  // printf("matrix-dim=[%u, %u], at %s:%d\n", self->nrows, self->ncols, __FILE__, __LINE__);
  //! ---------------------------
  //!
  //! @return the result:
  return ret_val;
}

static const bool __fileIsInFormat__tsv(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  const uint file_strlen = strlen(input_file);
  if(file_strlen > 4) {
    assert(sizeof(char) == 1);
    const uint start_pos = (file_strlen - 5);
    if(0 == strncmp(input_file + start_pos, ".tsv", 4)) { //! then then file-suffix is in the 'last part' of the file-name:
      return true;
    }
  }
  return false;
}

/**
   @brief read a matrix into the structure.
   @param <self> is the object to udpate
   @param <input_file> is the file to read.
   @return true upon success.
 **/
bool readFrom__file__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  bool is_ok = false;
  if(__fileIsInFormat__tsv(input_file)) {    
    is_ok = readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(self, input_file, UINT_MAX, UINT_MAX, /*readData_fromStream=*/false, NULL, 0, 0, NULL, /*transpoe=*/false);
  } else {
    assert(false); // eg, consider adding support 'for this' (oekseth, 06. des. 2016).
    return false;
  }
  //! @return:
  return is_ok;
}

//! Parse a data-file suign the configurations from the "s_kt_matrix_fileReadTuning_t" object (oekseth, 06. feb. 2017).
s_kt_matrix_t readFromAndReturn__file__advanced__s_kt_matrix_t(const char *input_file, s_kt_matrix_fileReadTuning_t config) {
  assert(input_file); 	    
  assert(strlen(input_file));
  //!
  //! Configure:
  const char *stringOf_sampleData_type = NULL;
  const char *stringOf_sampleData_type_realLife = input_file;
  if(config.fileIsRealLife == false) {stringOf_sampleData_type = input_file; stringOf_sampleData_type_realLife = NULL;}
  const uint fractionOf_toAppendWith_sampleData_typeFor_rows = config.fractionOf_toAppendWith_sampleData_typeFor_rows;
  const uint fractionOf_toAppendWith_sampleData_typeFor_columns = config.fractionOf_toAppendWith_sampleData_typeFor_columns;
  const bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  //  printf("isTo_transposeMatrix=%u, at %s:%d\n", isTo_transposeMatrix, __FILE__, __LINE__);   assert(false);
  //mapOf_realLife_classificaiton[data_id].isTo_transpose;
  //!
  //! Load data-set:
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  uint nrows = config.imaginaryFileProp__ncols;     uint ncols = config.imaginaryFileProp__ncols;  //! which are used as the 'idmensions' if "fileIsRealLife" is specified: toerhwise ignored.
  // printf("generates a matrix with dims=[%u, %u], where synFile=\"%s\", realLifeFile=\"%s\", fileIsRealLife='%u', at %s:%d\n", nrows, ncols, stringOf_sampleData_type, stringOf_sampleData_type_realLife, config.fileIsRealLife, __FILE__, __LINE__);
  s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
  if(config.mat_concat) {
    //printf("(concat)\t\t at %s:%d\n", __FILE__, __LINE__);
    assert(config.mat_concat->ncols > 0);
    assert(config.mat_concat->nrows > 0);
    if( (config.mat_concat->nrows > 0) && (config.mat_concat->ncols > 0) ) {
      obj_baseToInclude = *(config.mat_concat);
    }									    
  }
  //printf("(start)\t import-data \t at %s:%d\n", __FILE__, __LINE__);
  const bool isTo__copyNamesIfSet = config.isTo__copyNamesIfSet;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
  //printf("(cmpl)\t import-data \t at %s:%d\n", __FILE__, __LINE__);

  if(config.isTo_exportInputFileAsIs__toFormat__js) {
    char str_local[2000] = {'\0'}; sprintf(str_local, "%s__%s_locallYparsed.js", input_file, config.isTo_exportInputFileAsIs__toFormat__js); 
    char *stringOf_id = "parsedFile";
    export__singleCall__toFormat_javaScript__s_kt_matrix_t(&obj_matrixInput, str_local, NULL, stringOf_id);
  }

  //! @return
  return obj_matrixInput;
}

/**
   @brief read a matrix into the structure, and then transpsoe the data (before loading the data-set into memory).
   @param <self> is the object to udpate
   @param <input_file> is the file to read.
   @return true upon success.
 **/
bool readFrom__fileTransposed__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  if(__fileIsInFormat__tsv(input_file)) {    
    return readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(self, input_file, UINT_MAX, UINT_MAX, /*readData_fromStream=*/false, NULL, 0, 0, NULL, /*transpoe=*/true);
  } else {
    assert(false); // eg, consider adding support 'for this' (oekseth, 06. des. 2016).
    return false;
  }
}

/**
   @brief read a 1d-weigth-table into the weigth-table-list: we expect each 'weight' to be seperated by a newline "\n".
   @param <self> is the object to udpate
   @param <input_file> is the file to read.
   @return true upon success.
   @remarks we expect the number of 'rows' to be less than-or-equal to the number of columns in your input-fiel (where 'column' refers to 'rows' if you are using the transpsoed topioin')
 **/
bool readFrom_1dWeightTable__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  if(self->ncols == 0) {
    fprintf(stderr, "!!\t Erronous usage: we expects the matirx to be loaded Before the weight-table, ie, to simplify our cosnsitnecy-checkds: for queisotns please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  
  FILE *file = fopen(input_file, "r");
  if (file == NULL) {
    fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", input_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(self->weight == NULL) { //! then we allocate the list.
    const t_float def_weight = 1; //! ie, as we then assume taht 'all vertices are of interest'.
    self->weight = allocate_1d_list_float(self->ncols, def_weight);
  }  
  assert(self->weight != NULL);
  uint cnt_rows = 0; uint cnt_tabs = 0;
  const uint word_buffer_size = 128; uint word_buffer_currentPos = 0;
  char word_buffer[128] = {'\0'}; //! ie, as a number having mroe then 128 digits is aboute the T_FLOAT_MAX limit (oesketh, 06. jul. 2017).
  char c = '\0';
  while ((c = getc(file)) != EOF) {
    if(c == '\t') {cnt_tabs++;}
    else if(c == '\n') {
      const t_float val = (t_float)atof(word_buffer);
      //! Note: for cases where the first column is a string-id, we use an column-offset of "-1":
      if(false) {
	//assert(pos_in_column_0_prev <= 1000);
	char buffer_local[1000]; strncpy(buffer_local, word_buffer, word_buffer_currentPos);
	printf("[%u][%u] = \"%s\"=%f, at %s:%d\n", cnt_rows, cnt_tabs-1, buffer_local, val,  __FILE__, __LINE__);
      }
      assert(val != INFINITY);       assert(val != NAN);
      self->weight[cnt_rows] = val;      
      cnt_rows++;
      if(cnt_rows > self->ncols) {
	fprintf(stderr, "!!\t Your input-weight-file has more elements than the reserved number of columns, ie, where we expected %u <= %u, an error whcih could arise if your are using the 'tranposed option' in-cosnistnely: we expec thte weights to be deifned for the columsn (and not rows) For questions please contact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_rows, self->ncols, __FUNCTION__, __FILE__, __LINE__);
      }
      //! Reset:
      for(uint i = 0; i < word_buffer_size; i++) {word_buffer[i] = '\0';}
      word_buffer_currentPos = 0;
    } else { //! then inser tthe value in the current word-buffer-chunk:
      word_buffer[word_buffer_currentPos] = c; 
      word_buffer_currentPos++;
    }
  }
  if(cnt_tabs > 0) {
    fprintf(stderr, "!!\t Erronous usage: cnt-tabs=%u in a fiel with %u new-lines: as we expectted only one valeu to be found in each row the latter observaiton is belived to indicate an error: for questions please contact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_tabs, cnt_rows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  //! -----------------------------
  //!
  //! Reset:
  fclose(file);
  
  //! -----------------------------
  //!
  //! At this exueciton-poitn we asusme the oepraiotn 'wenth smooth':
  return true;
}



/**
   @brief export a 1d-weigth-table into from weigth-table-list: we expect each 'weight' to be seperated by a newline "\n".
   @return true upon success.
 **/
bool export_1dWeightTable__estensiveInputPArams__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file, const bool isTo_useJavaScript_syntax, const char *stringOf_header, const bool includeAsStandAlone, const bool isTo_append, const char *stringOf_mainStructOwner, const bool isLast, const bool isJSON) {
  assert(input_file); assert(strlen(input_file));
  if(self->ncols == 0) {
    fprintf(stderr, "!!\t Erronous usage: we expects the matirx to be loaded Before the weight-table, ie, to simplify our cosnsitnecy-checkds: for queisotns please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(self->weight == NULL) {
    fprintf(stderr, "!!\t Erronous usage: the weigh-table was Not set, ie, no point in exporting a data-set-table which does not exists: for queisotns please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    return false;
  }  

  FILE *file = NULL;
  if(isTo_append == false) {file = fopen(input_file, "w");}
  else {file = fopen(input_file, "a");}
  if (file == NULL) {
    fprintf(stderr, "!!\t Unable to open the file \"%s\" given isTo_append='%s', at [%s]:%s:%d\n", input_file, (isTo_append) ? "true" : "false", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  assert(self->weight != NULL);
  //uint cnt_rows = 0; uint cnt_tabs = 0;
  //const uint word_buffer_size = 128; uint word_buffer_currentPos = 0;
  //char word_buffer[128] = {'\0'};
  //char c = '\0';
  if(isTo_useJavaScript_syntax == false) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      fprintf(file, "%f\n", self->weight[col_id]);
    }
  } else {
    assert(stringOf_header);
    if(includeAsStandAlone) {fprintf(file, "var %s = [", stringOf_header); //! ie, as we then asusme we are to exprot using a 'simple' ajva-scirpt-wrapper.
    } else {
      if(stringOf_mainStructOwner) { //! then we assuem we are to itnaite
	if(strlen(stringOf_mainStructOwner)) {
	  fprintf(file, "%s = {\n", stringOf_mainStructOwner);
	} else {
	  fprintf(file, "\"%s\" : {\n", stringOf_mainStructOwner); //! ie, as we then assuemt eh result is to be exported into a JSON compaitble reuslt-format:
	}
	//! Include the 'sopecifci' hash-varialbe for 'the table to be exported:
	fprintf(file, "\"%s\" : [", stringOf_header);
      }
    }
    //!
    //! Write out the data:
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      if( (col_id+1) != self->ncols) { fprintf(file, "\"%f\", ", self->weight[col_id]);
      } else {fprintf(file, "\"%f\" ]", self->weight[col_id]);}
    }
    //! Complete the data-set, ie, if 'requested':
    if(isLast) {
      fprintf(file, "\n}");
      if(isJSON == false) { fprintf(file, ";");}
      fprintf(file, "\n}");
    }
  }

  //! -----------------------------
  //!
  //! Reset:
  fclose(file);

  //! -----------------------------
  //!
  //! At this exueciton-poitn we asusme the oepraiotn 'wenth smooth':
  return true;
}


/**
   @brief export an array of valeus to the specified data-format given when the "export_config_extensive_s_kt_matrix_t(..)" funciton is called (oekseth, 06. des. 2016).
   @param <self> is the object to use wrt. cofnigurations
   @param <arrOf_values> is the set of valeus to export
   @param <arrOf_values_size> is the number of elements in the arrOf_values
   @param <stringOf_label> is the lable to use (eg, the viarlab-enmae).
   @param <stringOf_mainStructOwner> which if used is 'used' to itnaite the data-structure, where the latter is of improtance when gnereiating JSON outputs.
 **/
void export_1dWeightTable__typeOf_uint__useConfigObject__s_kt_matrix_t(s_kt_matrix_t *self, const uint *arrOf_values, const uint arrOf_values_size, const char *stringOf_label, const char *stringOf_mainStructOwner) {
#define __internalMacro__formatSpec_cell "%u"
#define __internalMacro__valuesToBeQuoted 0
#include "kt_matrix__sub__export_1dWeightTable.c"
#undef __internalMacro__formatSpec_cell
#undef __internalMacro__valuesToBeQuoted
}

/**
   @brief export an array of valeus to the specified data-format given when the "export_config_extensive_s_kt_matrix_t(..)" funciton is called (oekseth, 06. des. 2016).
   @param <self> is the object to use wrt. cofnigurations
   @param <arrOf_values> is the set of valeus to export
   @param <arrOf_values_size> is the number of elements in the arrOf_values
   @param <stringOf_label> is the lable to use (eg, the viarlab-enmae).
   @param <stringOf_mainStructOwner> which if used is 'used' to itnaite the data-structure, where the latter is of improtance when gnereiating JSON outputs.
 **/
void export_1dWeightTable__typeOf_float__useConfigObject__s_kt_matrix_t(s_kt_matrix_t *self, const float *arrOf_values, const uint arrOf_values_size, const char *stringOf_label, const char *stringOf_mainStructOwner) {
#define __internalMacro__formatSpec_cell "%f"
#define __internalMacro__valuesToBeQuoted 0
#include "kt_matrix__sub__export_1dWeightTable.c"
#undef __internalMacro__formatSpec_cell
#undef __internalMacro__valuesToBeQuoted
}

/**
   @brief export a 1d-weigth-table into a TSV-format from weigth-table-list: we expect each 'weight' to be seperated by a newline "\n".
   @param <self> is the object to udpate
   @param <input_file> is the file to export the data-table into.
   @return true upon success.
 **/
bool export_1dWeightTable__formatOf_tsv__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file) {
  return export_1dWeightTable__estensiveInputPArams__s_kt_matrix_t(self, input_file, /*isTo_useJavaScript_syntax=*/false, /*stringOf_header=*/NULL, /*includeAsStandAlone=*/true, /*isTo_append=*/false, /*stringOf_mainStructOwner=*/NULL, /*isLast=*/true, /*isJONS=*/false);
}

/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <stringOf_measureId> which is used to 'name' the Java-script matrix-varaible (whcih is 'written out').
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, FILE *stream, const bool include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders) {
  if( (self->nrows == 0) || (self->ncols == 0) 
      //|| (self->nameOf_rows == NULL)       || (self->nameOf_columns == NULL)
      || (self->matrix == NULL)
      ) {
    fprintf(stderr, "!!\t The object was not intliased as expected, ie, please update your call: for quesitons please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  assert(self); assert(self->matrix); 
  assert(self->nameOf_rows); assert(strlen(self->nameOf_rows[0]));
  assert(self->nameOf_columns); assert(strlen(self->nameOf_columns[0]));
  //!
  //! Intiate a wrapper-object:
  s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
  data_obj.cnt_rows = self->nrows;
  data_obj.cnt_columns = self->ncols;
  data_obj.matrixOf_data = self->matrix;
  data_obj.nameOf_rows = self->nameOf_rows;
  data_obj.nameOf_columns = self->nameOf_columns;

  //!
  //! Teh call:
  export_dataSet_s_dataStruct_matrix_dense_matrix_tab(&data_obj, NULL, NULL, stringOf_header, stream, include_stringIdentifers_in_matrix, isTo_writeOut_stringHeaders);

  //! Reset the input-object:
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
}
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <stringOf_measureId> 
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders)  {
  if( (self->nrows == 0) || (self->ncols == 0) 
      //|| (self->nameOf_rows == NULL)       || (self->nameOf_columns == NULL)
      || (self->matrix == NULL)
      ) {
    fprintf(stderr, "!!\t The object was not intliased as expected, ie, please update your call: for quesitons please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  assert(self); assert(self->matrix); 
  /* assert(self->nameOf_rows); assert(strlen(self->nameOf_rows[0])); */
  /* assert(self->nameOf_columns); assert(strlen(self->nameOf_columns[0])); */
  //!
  //! Intiate a wrapper-object:
  s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
  data_obj.cnt_rows = self->nrows;
  data_obj.cnt_columns = self->ncols;
  data_obj.matrixOf_data = self->matrix;
  data_obj.nameOf_rows = self->nameOf_rows;
  data_obj.nameOf_columns = self->nameOf_columns;

  //printf("nameOf_rows=%p, nameOf_columns=%p, at %s:%d\n", self->nameOf_rows, self->nameOf_columns, __FILE__, __LINE__);

  //!
  //! Teh call:
  export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript(&data_obj, NULL, NULL, stringOf_header, stream, include_stringIdentifers_in_matrix, isTo_writeOut_stringHeaders);

  //! Reset the input-object:
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
}
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <isLast> which if set to true impleis that we 'finalize' the JSON-array.
   @param <isTo_explcitlyInclude_rowAndColumnHeaders> which if set impleis that we explcutly inlcude the row and column-hders: an example-usage is for the case where we cocnate the inptu-matrix with an 'adjsuted' data-sets, eg, where the 'previosuly fucniton-calls' describes a corelation-matrix-set with 50 per-cent threshodls.
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders, const bool isLast, const char *isTo_explcitlyInclude_rowAndColumnHeaders)  {
  if( (self->nrows == 0) || (self->ncols == 0) 
      //|| (self->nameOf_rows == NULL)       || (self->nameOf_columns == NULL)
      || (self->matrix == NULL)
      ) {
    fprintf(stderr, "!!\t The object was not intliased as expected, ie, please update your call: for quesitons please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  assert(self); assert(self->matrix); 
  /* assert(self->nameOf_rows); assert(strlen(self->nameOf_rows[0])); */
  /* assert(self->nameOf_columns); assert(strlen(self->nameOf_columns[0])); */
  //!
  //! Intiate a wrapper-object:
  s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
  data_obj.cnt_rows = self->nrows;
  data_obj.cnt_columns = self->ncols;
  data_obj.matrixOf_data = self->matrix;
  data_obj.nameOf_rows = self->nameOf_rows;
  data_obj.nameOf_columns = self->nameOf_columns;
  //!
  //! Teh call:
  export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON(&data_obj, NULL, NULL, stringOf_header, stream, include_stringIdentifers_in_matrix, isTo_writeOut_stringHeaders, isLast, isTo_explcitlyInclude_rowAndColumnHeaders);

  //! Reset the input-object:
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
}
/**
   @brief write out the obj to the stream as set of relations.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stringOf_predicate> which if set is used as predicate when writing out the data, eg, to simplify interpreation of the relationships.
   @param <stream> is the poitner to the restul-stream.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a java-script syntax.
   @param <isTo_useMatrixFormat> which if set to false impleis that we exprot using a relation-centered format.
 **/
void export_dataSet_s_dataStruct_matrix_dense_relations__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, const char *stringOf_predicate, FILE *stream, const bool isTo_useJavaScript_syntax, const bool isTo_useMatrixFormat) {
  if( (self->nrows == 0) || (self->ncols == 0) 
      //|| (self->nameOf_rows == NULL)       || (self->nameOf_columns == NULL)
      || (self->matrix == NULL)
      ) {
    fprintf(stderr, "!!\t The object was not intliased as expected, ie, please update your call: for quesitons please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  assert(self); assert(self->matrix); 
  /* assert(self->nameOf_rows); assert(strlen(self->nameOf_rows[0])); */
  /* assert(self->nameOf_columns); assert(strlen(self->nameOf_columns[0])); */
  //!
  //! Intiate a wrapper-object:
  s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
  data_obj.cnt_rows = self->nrows;
  data_obj.cnt_columns = self->ncols;
  data_obj.matrixOf_data = self->matrix;
  data_obj.nameOf_rows = self->nameOf_rows;
  data_obj.nameOf_columns = self->nameOf_columns;

  //!
  //! Teh call:
  //printf("at %s:%d\n", __FILE__, __LINE__);
  if(isTo_useJavaScript_syntax == false) {
    if(isTo_useMatrixFormat) {
      export_dataSet_s_dataStruct_matrix_dense_matrix_tab(&data_obj, self->nameOf_rows, self->nameOf_columns, stringOf_header, stream,  /*include_stringIdentifers_in_matrix=*/true, /*isTo_writeOut_stringHeaders=*/true);
    } else {
      export_dataSet_s_dataStruct_matrix_dense_relations(&data_obj, NULL, NULL, stringOf_header, stringOf_predicate, stream, isTo_useJavaScript_syntax, /*isTo_writeOut_pairsSeperately=*/true);
    }
  } else {
    if(isTo_useMatrixFormat) { //! then we export 'as a matrix and Not as a dense set of rleaitons-triplets' (oekseth, 06. des. 2016).
      export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript(&data_obj, self->nameOf_rows, self->nameOf_columns, stringOf_header, stream, /*include_stringIdentifers_in_matrix=*/true, /*isTo_writeOut_stringHeaders=*/true);
    } else {
      export_dataSet_s_dataStruct_matrix_dense_relations(&data_obj, NULL, NULL, stringOf_header, stringOf_predicate, stream, isTo_useJavaScript_syntax, /*isTo_writeOut_pairsSeperately=*/true);
    }
  }
  //! Reset the input-object:
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
}

//! An extneisve configruation to the file-export-routine.
void export_config_extensive_s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, const bool isTo_useMatrixFormat, const bool isTo_inResult_isTo_useJavaScript_syntax, const bool isTo_inResult_isTo_useJSON_syntax, const char *stringOf_header, const bool include_stringIdentifers_in_matrix, uint cnt_dataObjects_toGenerate) {
  if( (cnt_dataObjects_toGenerate == 0) || (cnt_dataObjects_toGenerate == UINT_MAX)) {
    cnt_dataObjects_toGenerate = 0;}
  self->cnt_dataObjects_toGenerate = cnt_dataObjects_toGenerate; //! ie, update.
  self->cnt_dataObjects_written = 0; //! ie, reset.
  
  //! 
  //! Update the filter-pointer:
  if(stringOf_resultFile != NULL) {
    if(self->stream_out != NULL) {
      if(
	 (self->stream_out != stderr) &&
	 (self->stream_out != stdout)
	 ) {
	fclose(self->stream_out); self->stream_out = NULL;
      }
    }
  }
  //! Idnetify the file-descrptor:
  FILE *stream_out = self->stream_out;
  if(stream_out == NULL) {stream_out = stdout;}
  char str_tmp[1000] = {'\0'};
  if(stringOf_resultFile && strlen(stringOf_resultFile)) {

    
    stream_out = fopen(stringOf_resultFile, "w");
    // printf("given stringOf_resultFile=\"%s\" sets the stream-out variable, at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
    if (stream_out == NULL) {
      struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start);
      sprintf(str_tmp, "%p_%lld.brief.tsv", self, (long long int)clock_time_start); //! ie, build a unique string by combining the memory-address and the unix-time.
      stream_out = fopen(str_tmp, "w");
      if (stream_out != NULL) {      
	fprintf(stderr, "!!\t Seems like your input-file-name \"%s\" was all to long: we have now shorted the file-name into \"%s\", and included the 'original-fiel-name' as a '#! fileName: ' tag in your result-file. For quesitons please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_resultFile, str_tmp, __FUNCTION__, __FILE__, __LINE__);
	//! Add the file-name as a 'tag':
	fprintf(stream_out, "#! fileName: \t%s\n", str_tmp);
      }
      stringOf_resultFile = str_tmp;
    }
    if (stream_out == NULL) {      
      fprintf(stderr, "!!\t Unale to open the file \"%s\" for writing: please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
  } else {self->stream_out = stdout;}
  // printf("sets the stream-out variable, at %s:%d\n", __FILE__, __LINE__);
  self->stream_out = stream_out;
  assert(self->stream_out);
  //! ------------------------
  //!
  //! Udpate generic proerpteis:
  self->isTo_useMatrixFormat  = isTo_useMatrixFormat;
  self->isTo_inResult_isTo_useJavaScript_syntax  = isTo_inResult_isTo_useJavaScript_syntax;
  // printf("isTo_inResult_isTo_useJSON_syntax=%u, at %s:%d\n", isTo_inResult_isTo_useJSON_syntax, __FILE__, __LINE__);
  self->isTo_inResult_isTo_useJSON_syntax  = isTo_inResult_isTo_useJSON_syntax; 
  self->stringOf_header  = stringOf_header;
  self->include_stringIdentifers_in_matrix  = include_stringIdentifers_in_matrix;
  self->isTo_writeOut_stringHeaders  = (stringOf_header != NULL); //isTo_writeOut_stringHeaders;  
}

//! Cofnigure the file-export-routine:
void export_config__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile) {
  // printf("Configures the exprot-rotuine given stringOf_resultFile=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
  assert(self);
  export_config_extensive_s_kt_matrix_t(self, stringOf_resultFile, 
					/*isTo_useMatrixFormat=*/true, 
					/*isTo_inResult_isTo_useJavaScript_syntax=*/false,
					/*isTo_inResult_isTo_useJSON_syntax=*/false,
					/*stringOf_header=*/NULL,
					/*include_stringIdentifers_in_matrix=*/false,
					/*cnt_dataobjects_togenerate=*/1);
}


//! A fucntion where we use the file-out-configuratiosn in "self" and the matrix-data in data_obj to udpate the result-file.
void export__dataObj_seperateFromFileObj__s_kt_matrix_t(s_kt_matrix_t *self, s_kt_matrix_t *data_obj, const char *stringOf_header) {
  assert(self);
  //! ------------------------------------------------
  //! Get access to the configuraitons:
  FILE *stream_out = self->stream_out; assert(stream_out);
  //printf("\t stream_out=%p, at %s:%d\n", stream_out, __FILE__, __LINE__);
  const bool isTo_inResult_isTo_useJavaScript_syntax = self->isTo_inResult_isTo_useJavaScript_syntax;
  //printf("isTo_inResult_isTo_useJSON_syntax=%u, at %s:%d\n", isTo_inResult_isTo_useJSON_syntax, __FILE__, __LINE__);
  const bool isTo_inResult_isTo_useJSON_syntax = self->isTo_inResult_isTo_useJSON_syntax; 
  const bool isTo_useMatrixFormat = (self->isTo_useMatrixFormat || isTo_inResult_isTo_useJSON_syntax || isTo_inResult_isTo_useJavaScript_syntax);
  //const char *stringOf_header; // = self->stringOf_header;
  const bool include_stringIdentifers_in_matrix = self->include_stringIdentifers_in_matrix;
  const bool isTo_writeOut_stringHeaders = (stringOf_header != NULL); //self->isTo_writeOut_stringHeaders;
  const bool isLast = ( (self->cnt_dataObjects_toGenerate + 1) == self->cnt_dataObjects_written);
  //! ------------------------------------------------
  if (stream_out == NULL) {
    fprintf(stderr, "!!\t The result-file was not opened: did you remeber to call the \"export_config__s_kt_matrix_t(..)\" before 'this fucntion-call' was made?: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }  

  
  //! -----------------------
  //!
  //! Handle specific cases wrt. formats:
  /* if(self->isTo_inResult_isTo_useJSON_syntax && (self->cnt_dataObjects_written > 0) ) { */
  /*   //! Then add a speratro for the JSON-data-set: */
  /*   fprintf(stream_out, ",\n"); */
  /* } */
  
  const bool isLast_timeThisObjectIsCalled = ((1 + self->cnt_dataObjects_written) == self->cnt_dataObjects_toGenerate);

  //! -----------------------
  //!
  //! Apply the format requested by the caller:
  if(isTo_useMatrixFormat) {
    if( (isTo_inResult_isTo_useJavaScript_syntax == true) && !isTo_inResult_isTo_useJSON_syntax) { //! ie, to avodi teh 'case' where a suer specifies JSON by 'setting both the JC-varialbe and the JSON-varialbe to true' (oekseth, 06. des. 2016).
      //printf("at %s:%d\n", __FILE__, __LINE__);
      export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript__s_kt_matrix_t(data_obj, /*matrix-variable-suffix=*/stringOf_header, stream_out, (bool)include_stringIdentifers_in_matrix, /*isTo_writeOut_stringHeaders=*/(self->cnt_dataObjects_written == 0));
      //assert(false); // FIXME: add soemthing
    } else if(isTo_inResult_isTo_useJSON_syntax == true) {      
      export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON__s_kt_matrix_t
	(data_obj,  /*matrix-variable-suffix=*/stringOf_header, stream_out, (bool)include_stringIdentifers_in_matrix, /*isTo_writeOut_stringHeaders=*/(self->cnt_dataObjects_written == 0), /*isLAst=*/isLast_timeThisObjectIsCalled, 
	 /*isTo_explcitlyInclude_rowAndColumnHeaders=*/(self->cnt_dataObjects_written == 0) ? stringOf_header : NULL); //! whe twe need to seperately write otu the row and coumn-headersin order to 'correctly' write out the data-set
      //assert(false); // FIXME: add soemthing
    } else {
      //printf("at %s:%d\n", __FILE__, __LINE__);
      export_dataSet_s_dataStruct_matrix_dense_relations__s_kt_matrix_t(data_obj, stringOf_header, stringOf_header, stream_out, isTo_inResult_isTo_useJavaScript_syntax, isTo_useMatrixFormat);
      //export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON__s_kt_matrix_t( data_obj,  stringOf_header, stream_out, (bool)include_stringIdentifers_in_matrix, /*isTo_writeOut_stringHeaders=*/(self->cnt_dataObjects_written == 0), isLast, stringOf_header);
    }
  } else {
    //printf("at %s:%d\n", __FILE__, __LINE__);
    export_dataSet_s_dataStruct_matrix_dense_relations__s_kt_matrix_t(data_obj, stringOf_header, stringOf_header, stream_out, isTo_inResult_isTo_useJavaScript_syntax, isTo_useMatrixFormat);
  }

  //! -----------------------
  //!
  //! Compelte:
  self->cnt_dataObjects_written++; //! ie, udpate the count.
  
}
//! Wirte out the matrix to the format specified through the set fo inptu-aprameters (oekseth, 06. nov. 2016).
void export__setLabel__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header) {
  export__dataObj_seperateFromFileObj__s_kt_matrix_t(self, self, stringOf_header);
}


//! Wirte out the matrix to the format specified through the set fo inptu-aprameters (oekseth, 06. nov. 2016).
void export__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self); 
  //printf("\t stream_out=%p, at %s:%d\n", self->stream_out, __FILE__, __LINE__);
  export__dataObj_seperateFromFileObj__s_kt_matrix_t(self, self, self->stringOf_header);
}

//! An explcit approach to clsoe the file-handler.
//! @rmekars an explampel-applciaiton conserns the sue-case where one is interested in 'using the result-fiel before the compelte proeprties of this object is freeed'.
void closeFileHandler__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self);
  if(self->stream_out != NULL) {
    if(
       (self->stream_out != stderr) &&
       (self->stream_out != stdout)
       ) {
      fclose(self->stream_out); self->stream_out = NULL;
    }
  }
}

/**
   @brief a signlce call to exprot the result-file (oekseth, 06. des. 2016).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @return true upon success.
 **/
bool export__singleCall__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler) {
  assert(self);
  //!
  //! Teh call:
  char *string_alloc = NULL;
  if(fileHandler != NULL) {
    stringOf_resultFile = string_alloc = allocateDenseFileName__s_kt_longFilesHandler_t(fileHandler, stringOf_resultFile, /*prefix=*/"matrix.");
    assert(stringOf_resultFile); assert(strlen(stringOf_resultFile));
  }
  //!
  //! Itnaite the file:
  export_config__s_kt_matrix_t(self, stringOf_resultFile);
  if(self->stream_out != NULL) {
    export__s_kt_matrix_t(self);
    //!
    //! Close:
    closeFileHandler__s_kt_matrix_t(self);
  } else {
    fprintf(stderr, "!!\t Unable to open the file=\"%s\", ie, please investigate: observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as we were Not able to open the result-fiel for writringg, eg, due to 'erronosu direcotyr-path' or 'not enough space on disk to wrte'
  }

  if(stringOf_resultFile && strlen(stringOf_resultFile)) {
    if(false) { //! then we alos write out a PPM image:
      const uint str_local_size = strlen(stringOf_resultFile) + 100;
      const char def_0 = 0;
      char *str_local = allocate_1d_list_char(str_local_size, def_0);
      sprintf(str_local, "%s.image.ppm", stringOf_resultFile);
      ppm_image_t *picture = exportTo_image ( self->matrix, self->nrows, self->ncols, self->nameOf_rows, self->nameOf_columns);
      ppm_image_write ( str_local, picture );
      ppm_image_finalize ( picture );
      free_1d_list_char(&str_local); str_local = NULL;
    }
  } //! else we asusme this is Not of itnerest, ie, to avoid 'minixnxg' the sdtou-stream with an image.
  if(fileHandler != NULL) {
    assert(string_alloc);
    free_1d_list_char(&string_alloc); string_alloc = NULL;
  }  
  //! ---------------
  return true; //! ie, as we asusme the oerpation was a success at this exeuciton-point
}

bool export__singleCall_firstTranspose__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler) {
  s_kt_matrix_t mat_t = setToEmptyAndReturn__s_kt_matrix_t();
  bool is_ok = init__copy_transposed__s_kt_matrix(&mat_t, self, /*isTo_updateNames=*/true);
  assert(is_ok);
  is_ok = export__singleCall__s_kt_matrix_t(&mat_t, stringOf_resultFile, fileHandler);
  free__s_kt_matrix(&mat_t);
  return is_ok;
}

//! a signlce call to exprot the result-file to java-script (oekseth, 06. feb. 2017).
bool export__singleCall__toFormat_javaScript__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler, const char *stringOf_id) {
  assert(self);
  //!
  //! Teh call:
  char *string_alloc = NULL;
  if(fileHandler != NULL) {
    stringOf_resultFile = string_alloc = allocateDenseFileName__s_kt_longFilesHandler_t(fileHandler, stringOf_resultFile, /*prefix=*/"matrix.");
    assert(stringOf_resultFile); assert(strlen(stringOf_resultFile));
  }
  //!
  //! Initate:
  const bool isTo_useMatrixFormat = true;
  const bool isTo_inResult_isTo_useJavaScript_syntax = true;
  const bool isTo_inResult_isTo_useJSON_syntax = false;
  
  //printf("nameOf_rows=%p, nameOf_columns=%p, at %s:%d\n", obj_matrixInput.nameOf_rows, obj_matrixInput.nameOf_columns, __FILE__, __LINE__);
  //printf("# Export: string_alloc=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
  export_config_extensive_s_kt_matrix_t(self, stringOf_resultFile, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, /*matrix-variable-suffix=*/stringOf_id, 
					false, //(bool)(stringOf_id != NULL), 
					/*cnt_dataObjects_toGenerate=*/1); //cnt_objs_toExport); 
  if(self->stream_out != NULL) {
    export__s_kt_matrix_t(self);
    //!
    //! Close:
    closeFileHandler__s_kt_matrix_t(self);
  } else {
    fprintf(stderr, "!!\t Unable to open the file=\"%s\", ie, please investigate: observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as we were Not able to open the result-fiel for writringg, eg, due to 'erronosu direcotyr-path' or 'not enough space on disk to wrte'
  }
  if(fileHandler != NULL) {
    assert(string_alloc);
    free_1d_list_char(&string_alloc); string_alloc = NULL;
  }  
  //! ---------------
  return true; //! ie, as we asusme the oerpation was a success at this exeuciton-point
}

//! A Single call to exprot the result-file (oekseth, 06. jan. 2017).
bool extractAndExport__deviations__singleCall__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler, const bool config_isToTranspose__whenGeneratingOutput, const e_kt_matrix__exportFormats_t exportFormat, const e_kt_matrix__exportFormats___extremeToPrint_t typeOf_extractToPrint) {
  assert(self);
  //!
  //! Teh call:
  char *string_alloc = NULL;
  if(fileHandler != NULL) {
    stringOf_resultFile = string_alloc = allocateDenseFileName__s_kt_longFilesHandler_t(fileHandler, stringOf_resultFile, /*prefix=*/"matrix.");
    assert(stringOf_resultFile); assert(strlen(stringOf_resultFile));
  }
  //!
  //! Itnaite the file:
  if( (stringOf_resultFile != NULL) && strlen(stringOf_resultFile)) {
    export_config__s_kt_matrix_t(self, stringOf_resultFile);
  } else if(self->stream_out == NULL) {
    fprintf(stderr, "!!\t Expected the \"stream_out\" field-esciptor to be set in the input-object, which is Not the case: pelase investigate correctness and cosnstiency of your API-calls. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as there is No point in continuing.
  }
  if(self->stream_out != NULL) {
    const char *column_sep = "\t";     const char *row_sep = "\n";
    if(exportFormat == e_kt_matrix__exportFormats_tsvMatrix) {
      column_sep = "\t";     row_sep = "\n";
    } else if(exportFormat == e_kt_matrix__exportFormats_tex) {
      //! NotE: if [below] changes then remember ot update our "heatmap.pl"
      column_sep = " & ";     row_sep = " \\\\ \n";
      //! Write out a meta-inidiator to 'state' that the format is in a ltex-syntax:
      fprintf(self->stream_out, "#! Format=tex\n");
    } else {
      assert(false); //! ie, as we then need adding support 'for this format'.
    }
    //!
    //! Construct a ranked copy:
    s_kt_matrix_t matrixOf_ranks;
    const bool is_ok_t = init__copy__useRanksEachRow__s_kt_matrix(&matrixOf_ranks, self, /*isTo_updateNames=*/false);
    assert(matrixOf_ranks.nrows == self->nrows);   //! ie, as we otherwise need to update our "matrix_deviation.c"
    assert(matrixOf_ranks.ncols == self->ncols);   //! ie, as we otherwise need to update our "matrix_deviation.c"
    assert(matrixOf_ranks.nrows > 0);
    assert(matrixOf_ranks.ncols > 0);
    assert(is_ok_t);
    //! 
    //! Export:
    const bool is_ok = exportToFormat__matrix__matrix_deviation(self->matrix, self->nrows, self->ncols, self->stream_out, column_sep, row_sep, config_isToTranspose__whenGeneratingOutput, matrixOf_ranks.matrix, typeOf_extractToPrint, self->nameOf_rows);
    assert(is_ok);
    free__s_kt_matrix(&matrixOf_ranks);
    if( (stringOf_resultFile != NULL) && strlen(stringOf_resultFile)) {
      //! 
      //! Close:
      closeFileHandler__s_kt_matrix_t(self);
    }
  } else {
    fprintf(stderr, "!!\t Unable to open the file=\"%s\" (or alterntivly you use your exisistng file-descirptor if the latter is Not set), ie, please investigate: observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as we were Not able to open the result-fiel for writringg, eg, due to 'erronosu direcotyr-path' or 'not enough space on disk to wrte'
  }

  if(fileHandler != NULL) {
    assert(string_alloc);
    free_1d_list_char(&string_alloc); string_alloc = NULL;
  }  
  //! ---------------
  return true; //! ie, as we asusme the oerpation was a success at this exeuciton-point
}


//! A Single call to exprot the result-file using a CSV-syntex without stirng-idneitifers (oekseth, 06. feb. 2017).
bool export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler) {
  assert(self);
  //!
  //! Teh call:
  char *string_alloc = NULL;
  if(fileHandler != NULL) {
    stringOf_resultFile = string_alloc = allocateDenseFileName__s_kt_longFilesHandler_t(fileHandler, stringOf_resultFile, /*prefix=*/"matrix.");
    assert(stringOf_resultFile); assert(strlen(stringOf_resultFile));
  }
  //!
  //! Itnaite the file:
  if( (stringOf_resultFile != NULL) && strlen(stringOf_resultFile)) {
    export_config__s_kt_matrix_t(self, stringOf_resultFile);
  } else if(self->stream_out == NULL) {
    fprintf(stderr, "!!\t Expected the \"stream_out\" field-esciptor to be set in the input-object, which is Not the case: pelase investigate correctness and cosnstiency of your API-calls. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as there is No point in continuing.
  }
  if(self->stream_out != NULL) {
    const char *column_sep = ",";     const char *row_sep = "\n";
    fprintf(self->stream_out, "%u %u%s", self->nrows, self->ncols, row_sep); //! ie, the dimenions of the amtrix, among other r'equrired' in the "fast_kmeans" software.
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      assert(self->matrix[row_id]);
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	t_float score_1 = self->matrix[row_id][col_id];
	if(!isOf_interest(score_1)) {score_1 = 0;} //! a poor sollutioin which is used as many of the libraries we comarpe with are unable to handle empty values.
	const char *sep = column_sep;
	if((col_id+1) == self->ncols) {sep = row_sep;}
	fprintf(self->stream_out, "%f%s", score_1, sep);
      }
    }
    //! 
    //! Close:
    closeFileHandler__s_kt_matrix_t(self);
  } else {
    fprintf(stderr, "!!\t Unable to open the file=\"%s\" (or alterntivly you use your exisistng file-descirptor if the latter is Not set), ie, please investigate: observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as we were Not able to open the result-fiel for writringg, eg, due to 'erronosu direcotyr-path' or 'not enough space on disk to wrte'
  }

  if(fileHandler != NULL) {
    assert(string_alloc);
    free_1d_list_char(&string_alloc); string_alloc = NULL;
  }  
  //! ---------------
  return true; //! ie, as we asusme the oerpation was a success at this exeuciton-point
}

