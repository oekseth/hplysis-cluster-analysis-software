#include "parse_main.h"
#include "hpLysis_api.h"
#include "alg_dbScan_brute.h"
#include "hp_ccm.h"
#include "hp_exportMatrix.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "kt_matrix.h"
#include "export_ppm.h" //! which is used for *.ppm file-export.

//! Export data to ppm
//! Note: the default fucntion exports data using grayscales. In the function we tranlsate numbers inot a range '0 ... 1', hence the need for data copy
static void __tut_histo_1_exportTo_ppm(s_kt_matrix_t &mat_result, const char *file_base, const char *file_pref, const bool isTo_alsoExportTo_tsv) {
  //! Copy:
  s_kt_matrix_t mat_cpy = initAndReturn__copy__s_kt_matrix(&mat_result, true);
  //! Adjust(1): get max.
  t_float score_max = T_FLOAT_MIN_ABS;
  t_float score_min = T_FLOAT_MAX;
  for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
      const t_float score = mat_cpy.matrix[row_id][col_id];
      if(isOf_interest(score)) {
	score_max = macro_max(score_max, score);
	score_min = macro_min(score_min, score);
      }
    }
  }
  printf("# score_max:%f, score_min=%f, given dims=%u,%u, at %s:%d\n", score_max, score_min,  mat_cpy.nrows, mat_cpy.ncols, __FILE__, __LINE__);
  if( (score_max != T_FLOAT_MIN_ABS) && (score_min = T_FLOAT_MAX) ) { // && (score_max != 0) ) {
    //! Adjust(1):
    //t_float score_max_inv = 1/(score_max - score_min); // FIXME: validate this.
    const t_float score_max_inv = 1/(score_max);
    for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
	const t_float score = mat_cpy.matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  mat_cpy.matrix[row_id][col_id] *= score_max_inv; 
	}
      }
    }
    if(isTo_alsoExportTo_tsv) {
      //if(self.isTo_exportResultFiles) {
      char file_name_result[1000]; memset(file_name_result, '\0', 1000); 
      if((file_base != NULL) && (strlen(file_base) > 0) ) {
	sprintf(file_name_result, "%s_%s_img.tsv", file_base, file_pref);
      } else {
	sprintf(file_name_result, "%s_img.tsv", file_pref);
      }
      fprintf(stderr, "(info:export::tsv)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_result, file_name_result, NULL);
      //}
      //export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL);
      //(&mat_cpy, file_name_result, NULL);
      //export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(&mat_cpy, file_name_result, NULL);
    }
    //! Export:
    char file_name_result[1000]; memset(file_name_result, '\0', 1000);
    if((file_base != NULL) && (strlen(file_base) > 0) ) {
	sprintf(file_name_result, "%s_%s_img.ppm", file_base, file_pref);
    } else {
      sprintf(file_name_result, "%s_img.ppm", file_pref);
    }
    //	    const char *file_name_result_img = "tmp_img.ppm";
    fprintf(stderr, "(info:export::ppm)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
    ppm_image_t *picture = exportTo_image ( mat_cpy.matrix, mat_cpy.nrows, mat_cpy.ncols, mat_cpy.nameOf_rows, mat_cpy.nameOf_columns);
    ppm_image_write ( file_name_result, picture );
    ppm_image_finalize ( picture );
    /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL); */
    /* assert(is_ok); */
    //! De-allocate:
  }
  free__s_kt_matrix(&mat_cpy);
}

static void __tut_histo_1_correlate_then_exportTo_ppm(s_kt_matrix_t &mat_corrAll, const char *resultPrefix_local, s_kt_correlationMetric_t metric_local, const char *str_metric) {
  //!
  s_kt_matrix_base_t mat_shallow         = initAndReturn__notAllocate__s_kt_matrix_base_t(mat_corrAll.nrows, mat_corrAll.ncols, mat_corrAll.matrix);
  s_kt_matrix_base_t mat_result        = initAndReturn__empty__s_kt_matrix_base_t();
  s_hp_distance__config_t local_config = init__s_hp_distance__config_t();
  const bool is_ok = apply__hp_distance(metric_local, &mat_shallow, &mat_shallow, &mat_result, local_config);
  // const bool is_ok = apply__rows_hp_distance(list_metrics[i], row_1, row_2, ncols, &scalar_result);
  assert(is_ok);
  printf("\t(correlate-and-export)\t ncols=%u, at %s:%d\n", mat_result.ncols, __FILE__, __LINE__);
  if(mat_result.ncols > 0) { //! then we assume data is allcoated:
    //! 'Steal' the data:
    mat_result.matrix__isAllocated = false; //! ie, to avodi a garbage-rotuein from wrongly clearing the data used in the below "mat_export" struct.
    s_kt_matrix_t mat_export = setToEmptyAndReturn__s_kt_matrix_t();
    mat_export.matrix = mat_result.matrix;
    mat_export.nrows = mat_result.nrows;
    mat_export.ncols = mat_result.ncols;
    //!	  
    { //! Set the row-headers and columns:
      assert(mat_export.ncols  == mat_export.nrows);
      assert(mat_corrAll.nrows == mat_export.nrows);	    
      for(uint i = 0; i < mat_export.nrows; i++) {
	set_stringConst__s_kt_matrix(&mat_export, /*index=*/i, getAndReturn_string__s_kt_matrix(&mat_corrAll, i, /*addFor_column=*/false), /*addFor_column=*/false);
	set_stringConst__s_kt_matrix(&mat_export, /*index=*/i, getAndReturn_string__s_kt_matrix(&mat_corrAll, i, /*addFor_column=*/false), /*addFor_column=*/true);
      }
    }
    //!
    //! Export the result:
    allocOnStack__char__sprintf(2000, str_fileName, "%s--%s", resultPrefix_local, str_metric);
    __tut_histo_1_exportTo_ppm(mat_export, /*file_base=*/"", str_fileName, /*isTo_alsoExportTo_tsv=*/true);
    //! De-allocate:
    free__s_kt_matrix(&mat_export);
  }
}	


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief ... 
   @author Ole Kristian Ekseth  (oekseth,  06. aug. 2020).
   @remarks 
   - compile: g++ -O2 -g tut-bias-1-histogram-sensitvity-variance-in-randomness.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_1.exe; time ./tut_1.exe
**/
int main() 
#else
  static void tut_bias_1_histogram_sensitvity_variance_in_randomness()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif


  const bool globalConfig_sRandReset = false; //! which if set to 'true' implies that each vector gets the same randomzied values (hence, increases accuracy of predciton-comparison).
  //! Configure the pawisie ssimlairty-metric
  //const e_kt_categoryOf_correaltionPreStep_t list_metrics_preStep = e_kt_categoryOf_correaltionPreStep_binary;   const char *list_metrics_preStep_str = "preBinary";
  //const e_kt_categoryOf_correaltionPreStep_t list_metrics_preStep = e_kt_categoryOf_correaltionPreStep_rank;   const char *list_metrics_preStep_str = "preRank";
  const e_kt_categoryOf_correaltionPreStep_t list_metrics_preStep = e_kt_categoryOf_correaltionPreStep_none;   const char *list_metrics_preStep_str = "preNone";
  
  const bool isTo_writeOut_temporaryData = false;
  //const uint cnt_times_random = 2;
  //const uint dimensions_cnt = 2;
  const uint cnt_times_random = 100;
  //const uint cnt_times_random = 1000;
  const uint dimensions_cnt = 20;
  //! Then specify the set of exepcted ranks:
  const t_float dimensions_curve[dimensions_cnt] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,  9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
  const uint dimensions_stepSize = 200;
  //const uint dimensions_stepSize = 1000;
  {
#define tutBiasConfig_testAllBaseMetrics
#ifndef tutBiasConfig_testAllBaseMetrics
    const uint list_metrics_size = 6;
    const s_kt_correlationMetric_t list_metrics[list_metrics_size] = {
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_chebychev, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_cityblock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank),
      initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)
    };
#else
    const uint list_metrics_size =   (uint)e_kt_correlationFunction_undef;
#endif //! #ifndef tutBiasConfig_testAllBaseMetrics
    

    //!
    //! The histogram setup ("kt_list_2d.h");
    const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
    // FIXME: meausrement: ... use below histo-type to estiamte the seperatbility across different dimensions ... apply a row-based data-nromsaiton before rpign the socreos out (to get an approxmiate of the seperaily, rhater thant he sim-scores themself) .... hyptoesis: if the sepraility decreases with icnreasing number of dimeisons, then ... 
        //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
    //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;    
    const uint config_cnt_histogram_bins = 10;
    //!
    //! Specify the differnet types of randomzaiton-fucntisoinw we are to explore:
    const uint config_maxValue = 100; //! ie, the value-spread.
    const uint setOfRandomNess_size = 3;
    const e_kt_randomGenerator_type_t setOfRandomNess[setOfRandomNess_size] = {
      e_kt_randomGenerator_type_hyLysisDefault,
      // e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
      // e_kt_randomGenerator_type_posixRan__srand__time__modulo,
      // e_kt_randomGenerator_type_binominal__incrementalInList
      e_kt_randomGenerator_type_posixRan__srand__time__modulo,
      e_kt_randomGenerator_type_posixRan__srand__time__uniform,
      // e_kt_randomGenerator_type_binominal__pst__07,
      //e_kt_randomGenerator_type_binominal__pst__15,
      //e_kt_randomGenerator_type_binominal__pst__35,      
    };
    const char *setOfRandomNess_str[setOfRandomNess_size] = {
      "random-by-multinomial-distribution",
      "random-modulo",
      "random-uniform",
    };
    //! -------------------------- end of configuration.
    //!
    //! Initaite the matrix used to as reference-frame when comapred to the other matrices
    //s_kt_matrix_t mat_first = setToEmptyAndReturn__s_kt_matrix_t();
    s_kt_matrix_t mat_corrAll = initAndReturn__s_kt_matrix(/*cnt-alg=*/setOfRandomNess_size * list_metrics_size, /*ncols=*/dimensions_cnt);
    uli mat_corrAll_index = 0; //! which is the row-index used during insertions in mat_corrAll; for details of the "uli" type, see "types_base.h"
    { //! Set the column-headers:
      assert(mat_corrAll.ncols == dimensions_cnt);
      for(uint dim_index = 0; dim_index < dimensions_cnt; dim_index++) {
	//for(uint i = 0; i < mat_corrAll.ncols; i++) {
	const uint ncols = (dim_index+1)*dimensions_stepSize;	    
	allocOnStack__char__sprintf(2000, str, "dim-%u", ncols);
	set_stringConst__s_kt_matrix(&mat_corrAll, /*index=*/dim_index, str, /*addFor_column=*/true);
      }
    }
    //!
    //! Explroe for differnet randomzation-strategies:
    for(uint rand_id = 0; rand_id < setOfRandomNess_size; rand_id++) {      
      const e_kt_randomGenerator_type_t type_randomness_in = setOfRandomNess[rand_id];
      //! Traverse different types of simliarty-metrics:
      for(uint i = 0; i < list_metrics_size; i++, mat_corrAll_index++) {
	printf("\t(info) rand=%u/%u, metric=%u/%u, at %s:%d\n", rand_id, setOfRandomNess_size, i, list_metrics_size, __FILE__, __LINE__);
#ifndef tutBiasConfig_testAllBaseMetrics
	const s_kt_correlationMetric_t curr_metric = list_metrics[i];
#else
	const s_kt_correlationMetric_t curr_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/(e_kt_correlationFunction_t)i, /*prestep=*/list_metrics_preStep);
#endif //! #ifndef tutBiasConfig_testAllBaseMetrics	  
	{ //! Set the row-header:
	  assert(mat_corrAll_index < mat_corrAll.nrows);
	  const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id);
	  allocOnStack__char__sprintf(2000, str, "%s-%s-%s", setOfRandomNess_str[rand_id], str_metric, list_metrics_preStep_str);
	  set_stringConst__s_kt_matrix(&mat_corrAll, /*index=*/mat_corrAll_index, str, /*addFor_column=*/false);
	}
	//! Initaite the histogram-matrix
	s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(/*nrows=*/dimensions_cnt, /*ncols=*/config_cnt_histogram_bins);
	//!
	//! Construct vectors, and for each vector comptue scores:
	for(uint dim_index = 0; dim_index < dimensions_cnt; dim_index++) {
	  //! Note: the below "srand(0)" step is important: it ensures that the random values are the same in the beginning of each radnomied sample-trial.
	  // FIXME: what is the effect of 'fixing' the randomzaiton for each run? do we observe an icnrease in prediciton-variance if the andomzaiton is not fixed? If so, may this prediciton-differences (attributable to differrent randomized vectors) be used to assess the acuray-threshold of predcitons relating to randomzied sampling?
	  // FIXME[paper]: describe the differnet ranozmaiton-strategies we apply (for this testing) ... does the randomization-types give rise to different predictions?
	  if(globalConfig_sRandReset)  {
	    srand(0);
	  }
	  //! 
	  const uint ncols = (dim_index+1)*dimensions_stepSize;
	  //! Initaite the local placeholder for the results, as used as input to the result-matrix:
	  s_kt_matrix_base_t mat_scores = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, ncols);

	  //! Get inputs for the histogram
	  for(uint rand_trial = 0; rand_trial < cnt_times_random; rand_trial++) {
	    //! Construct the random vector, ie, load randomness:
	    s_kt_randomGenerator_vector_t rand_1 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	    s_kt_randomGenerator_vector_t rand_2 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	    //! Call randomness:
	    randomassign__speicficType__math_generateDistributions(&rand_1);
	    randomassign__speicficType__math_generateDistributions(&rand_2);	    
	    //! 
	    //! Compute the similairty:
	    t_float sim_score = T_FLOAT_MAX; //! ie, set to empty.
	    const bool is_ok = apply__uint_rows_hp_distance(curr_metric, rand_1.mapOf_columnToRandomScore, rand_2.mapOf_columnToRandomScore, ncols, &sim_score);
	    //const bool is_ok = apply__rows_hp_distance(curr_metric, row_1, row_2, ncols, &scalar_result);
	    assert(is_ok);
	    //! Store the result:
	    assert(mat_scores.matrix[0]);
	    assert(mat_scores.ncols > rand_trial);
	    mat_scores.matrix[/*row-id=*/0][/*col-id=*/rand_trial] = sim_score;
	    //! De-allocate:
	    free__s_kt_randomGenerator_vector_t(&rand_1);
	    free__s_kt_randomGenerator_vector_t(&rand_2);	  
	  }
	  //! Compute the historgram:
	  const bool config_isTo_forMinMax_useGlobal = true;
	  s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(&mat_scores, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	  //! 
	  //! Store the result:
	  assert(mat_result.matrix[dim_index]);
	  assert(mat_result.ncols == config_cnt_histogram_bins);
	  //! Handle issues relating to dynamic resizeing of the sparse 1d-list:
	  uint b_size = list_histo.list_size;
	  if(b_size > config_cnt_histogram_bins) {
	    b_size = config_cnt_histogram_bins;
	  }
	  //! Then the copy-operation:
	  for(uint b = 0; b < b_size; b++) {
	    mat_result.matrix[dim_index][b] = list_histo.list[b];
	  }
	  { //! Compute the similiarty between the histogram and the exepcted bell-shaped curve, and then update the result-matrix:
            //! Idea: Comptue a matrix \textit{algorithm-combination} x \textit{ideal-bell-shaped-curve} .... in the process, we ... seperately for each row in the result-set compute Kendall's Tau; the distance is computed from the bell-shaped histogram to the other histogram, seprately for each row.   .... then compare the different aglroithsm to each other, for which the 'number-of-steps' algorithm fo Kendall's Tau is used (Fig. ref{}) ... and then take itno accound the effects of slight shifts in the distrrubiotns for thwch the MINE algorithm is used  (Fig. ref{}). 
            //! ... idea: randomly selecting vertices for comparison ... implies that their simliarty should ahve a bell-shape ... as it is more unlikely that two vectors are extemly different ... An example is seen for dice tossing, where there is a $1/6$ proability of getting a given number (eg, \textit{six}), hence there is a $1/6 * 1/6$ proability for fetching the numbers with the highest spreads (ie, number=1 and number=6). ... This illustrates why the bell-shape arises when comparing vectors generated from a random sampling of data .... This gives an increased insight into the capability of analysing high-diemsioanl data-sets: if a given metric amnages to generate bell-shaped curves for larger datasets, then ... seperability of different ... 
	    //! 
	    //! Compute the similairty:
	    t_float sim_score = T_FLOAT_MAX; //! ie, set to empty.
	    const s_kt_correlationMetric_t metric_compareHistoToExpectedOrder = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, e_kt_categoryOf_correaltionPreStep_rank);
	    const bool is_ok = apply__rows_hp_distance(metric_compareHistoToExpectedOrder, list_histo.list, (t_float*)dimensions_curve, list_histo.list_size, &sim_score);
	    //const bool is_ok = apply__rows_hp_distance(curr_metric, row_1, row_2, ncols, &scalar_result);
	    assert(is_ok);
	    //! Update the result-strcuture:
	    mat_corrAll.matrix[mat_corrAll_index][dim_index] = sim_score;
	  }
	  

	  //! De-allocate:
	  free__s_kt_matrix_base_t(&mat_scores);
	  free__s_kt_list_1d_float_t(&list_histo);
	}
	{ //! Pre (before writing)
	  //! Task: add infomrative row-names and column-names
	  assert(dimensions_cnt == mat_result.nrows);
	  //! Strings for rows:
	  for(uint dim_index = 0; dim_index < dimensions_cnt; dim_index++) {
	  //for(uint k = 0; k < mat_result.nrows; k++) {
	    const uint ncols = (dim_index+1)*dimensions_stepSize;	    
	    allocOnStack__char__sprintf(2000, str, "dim-%u", ncols);
	    set_stringConst__s_kt_matrix(&mat_result, /*index=*/dim_index, str, /*addFor_column=*/false);
	  }
	  //! Strings for columns:
	  for(uint k = 0; k < mat_result.ncols; k++) {
	    allocOnStack__char__sprintf(2000, str, "bucket-%u", k);
	    set_stringConst__s_kt_matrix(&mat_result, /*index=*/k, str, /*addFor_column=*/true);
	  }	  
	}
	if(isTo_writeOut_temporaryData) {
	  //! Write out the histogram-matrix:
	  {	  
	    const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id);
	    allocOnStack__char__sprintf(2000, resultPrefix_local, "result-score-tut-bias-%s-%s.tsv", setOfRandomNess_str[rand_id], str_metric);
	    export__singleCall__s_kt_matrix_t(&mat_result, resultPrefix_local, NULL);
	  }
	  { //! Compute the Ranks:	  
	    const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id);
	    s_kt_matrix_t mat_rank = initAndReturn_ranks__s_kt_matrix(&mat_result);
	    allocOnStack__char__sprintf(2000, resultPrefix_local, "result-rank-tut-bias-%s-%s.tsv", setOfRandomNess_str[rand_id], str_metric);
	    export__singleCall__s_kt_matrix_t(&mat_rank, resultPrefix_local, NULL);
	    //! 
	    //! De-allocate:
	    free__s_kt_matrix(&mat_rank);
	  }
	}
	//! 
	//! De-allocate:
	free__s_kt_matrix(&mat_result);
      } //! end-of-metric-traversal
    } //! end-of-randomess-traversal
    //! --------------------------------------------------
    { //! Make the scores psoivie:
      //! Note: background: the ppm-exprt does not eseme to work when teh scores are negative
      // FIXME: is below pre-step correct?
      t_float score_min = T_FLOAT_MAX;
      for(uint row_id = 0; row_id < mat_corrAll.nrows; row_id++) {
	for(uint col_id = 0; col_id < mat_corrAll.ncols; col_id++) {
	  const t_float score = mat_corrAll.matrix[row_id][col_id];
	  if(isOf_interest(score)) {
	    // score_max = macro_max(score_max, score);
	    score_min = macro_min(score_min, score);
	  }
	}
      }
      if(score_min < 0) {
	score_min *= -1.0; //! ie, remove the sign.
	for(uint row_id = 0; row_id < mat_corrAll.nrows; row_id++) {
	  for(uint col_id = 0; col_id < mat_corrAll.ncols; col_id++) {
	    mat_corrAll.matrix[row_id][col_id] += score_min; //! hence using the offset to assure that all values are >= 0
	  }
	}
      }
    } //! end of prestep:positive-property.
    //! 
    { //! Export:
      const char *resultPrefix_local = "result-score-algorithm-x-dimension";
      //allocOnStack__char__sprintf(2000, resultPrefix_local, "result-score-algorithm-x-dimension");
      {	  
	//allocOnStack__char__sprintf(2000, resultPrefix_local, "result-score-algorithm-x-dimension.tsv");
	//export__singleCall__s_kt_matrix_t(&mat_corrAll, resultPrefix_local, NULL);
	//! Export to ppm:
	const char *resultPrefix_local = "result-raw-score-algorithm-x-dimension";
	__tut_histo_1_exportTo_ppm(mat_corrAll, /*file-base=*/"", resultPrefix_local, /*isTo_alsoExportTo_tsv=*/true);
	{ //! Genereate a skewness-summary of the data-points
	  //! Note: background: seems like the dimeniosn does not have a strung infleucne on the overall prediction-accuracy
	}
	//! Note: seems like 
      }
      { //! Compute the Ranks:	  
	s_kt_matrix_t mat_rank = initAndReturn_ranks__s_kt_matrix(&mat_corrAll);
	const char *resultPrefix_local = "result-raw-rank-algorithm-x-dimension.tsv";
	//allocOnStack__char__sprintf(2000, resultPrefix_local, "result-rank-algorithm-x-dimensions-simInHistoEach.tsv");
	export__singleCall__s_kt_matrix_t(&mat_rank, resultPrefix_local, NULL);
	//! 
	//! De-allocate:
	free__s_kt_matrix(&mat_rank);
      }
      { //! Compute simliarty, and then export.
	{ //! For: Kendall:
	  const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, e_kt_categoryOf_correaltionPreStep_rank);
	  const char *str_metric = "Kendall";
	  __tut_histo_1_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric);
	}
	{ //! For: MINE:
	  const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none);
	  const char *str_metric = "MINE";
	  __tut_histo_1_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric);
	}      
      }
      { //! Compute simliarty, and then export, for transposed matrix:
	//! Note: motivation is to explore the depdency among the data-dimensions for understanding varialbility: if the correlation between the dimeinsion are smaller than the correlation beteen the different algorithms (ie, metric-comiations), then the use of accurate algorithm-cofnigruaitons amanges to address the known issue in high-dioemsinoal data-analsysi.
	//! Note[observation::result]: the tranpsoed MINE-matrix are much mroe similar than the 'raw' MINE-matrix. Hence, the often overlllokked chise of simliarty-metircs represents an improtant choice (eg, when comapred to the down of high-diemisional data into low-dimesional strategies).
	//! Note[observation::result]:
	//! Note[observation::result]:	
	//! Note[observation]:
	//! Note[observation]: 	
	//!
	//! Tranpose:
	s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_corrAll);
	//!
	//!	
	{ //! For: Kendall:
	  const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, e_kt_categoryOf_correaltionPreStep_rank);
	  const char *str_metric = "transp-Kendall";
	  __tut_histo_1_correlate_then_exportTo_ppm(mat_transp, resultPrefix_local, metric_local, str_metric);
	}
	{ //! For: MINE:
	  const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none);
	  const char *str_metric = "transp-MINE";
	  __tut_histo_1_correlate_then_exportTo_ppm(mat_transp, resultPrefix_local, metric_local, str_metric);
	}
	//!
	//! de-allocate:
	free__s_kt_matrix(&mat_transp);
      }
    }
    //! 
    //! De-allocate:
    //free__s_kt_matrix(&mat_first);
    free__s_kt_matrix(&mat_corrAll);
  }
  ////!
  ////! @return
#ifndef __M__calledInsideFunction
  ////! *************************************************************************
  ////! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  ////! *************************************************************************  
  return true;
#endif
}

  
  
  
