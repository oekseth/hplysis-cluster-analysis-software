#ifdef __MiCo__useLocalVariablesIn__dataRealFileLoading
 const uint sizeOf__nrows = 100;
 const uint sizeOf__ncols = 100;
    const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
    const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
    const bool isTo__testHCA_komb__inKmeans = false;
    const bool isTo__evaluate__simMetric__MINE__Euclid = true;
    const char *stringOf_resultDir = "";
// ---
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  // printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! We are interested in a more performacne-demanind approach: 

  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
#endif


const char *nameOf_experiment = "realLife_1";  //! Data-set orgianly cosntructed for the evlauaiton of the MIN-simlarity-emtic in our hpLysis sfotware-appraoch:
const uint mapOf_realLife_size = 11; // + 3; //! ie, the number of data-set-objects in [”elow]
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"


    //! -------------------------------------------------- 
    //! 
    //! 
    //! Note[<real-life-small-data-sets> w/cnt=11]: 
    //! ---- 
    //! cnt_clusters=2, size=15; seperation=ambiguous-overlapping; Trait="curved, intersecting"; Euclid oversimplifies/'extremifies' the speration, while canberra is uanble to capture the 'curved relationships': the other metrics are unable to capture the relationjships. 
    {/*tag=*/"birthWeight_chinese", /*file_name=*/"tests/data/kt_mine/birthWeight_chineseChildren.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ... cnt_clusters=1, size=; seperation=periodic; Trait="curved, intersecting"; Euclid 
    {/*tag=*/"butterFat_cows", /*file_name=*/"tests/data/kt_mine/butterFat_percentage_cows.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! cnt_clusters=1, size=11; seperation='well-defined'; Trait="curved, x-axis-start-point differs"; Euclid 
    {/*tag=*/"fishGrowth", /*file_name=*/"tests/data/kt_mine/fish_growth.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ... cnt_clusters=2, size=4; seperation=ambiguous; Trait="curved, fluctating, discrete-sinus"; Euclid 
    // FIXME: consider using [below] as an example of 'periodic clsuters which are diicuflt to clsuter'.
    // FIXME: consider to transpose [below] .... figure out how to 'hanlde' the case where 'a ranking' would result in isngificnat seperation 
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ------ 
    //! 
    //! "Spottinggrade": cnt_clusters=2, size=4; seperation='change VS not-change, ie, a non-linear comparison'; Trait="curved, intersecting"; ....??... <-- asusmes other metrics will find thios challenging
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"spottingGrade_2", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
  //! ... cnt_clusters=2, size=3; seperation='ambigious: silamirlty in curve-increase'; Trait="curved, simliar-peaks"; 
    {/*tag=*/"guinePigs", /*file_name=*/"tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_housed_pregnancy_first.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! "limesOxygen": if transposed nearly-all (ie, xmt. "species 2, 75 seawater") seems to have a simlar flucation
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"limesOxygen_2", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ------ 
    //! 
    //! "Weedlength": cnt_clusters=1, size=~20; seperation='clear: samve dsicrete sinus-vurve wrt. flcutaitons ... Metrics: none of the compared metric manages to idneitfy/describe this relationship
    {/*tag=*/"weedLength", /*file_name=*/"tests/data/kt_mine/weed_length.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
  };
