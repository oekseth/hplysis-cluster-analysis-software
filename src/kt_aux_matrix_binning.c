#include "kt_aux_matrix_binning.h"

s_kt_list_1d_float_t applyTo_matrix__kt_aux_matrix_binning(const s_kt_matrix_base_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring) {
  assert(self); assert(cnt_bins != UINT_MAX); assert(cnt_bins > 1);
  //! 
  //! Allocate size
  uint arr_scores_size = 0;
  t_float score_min = T_FLOAT_MAX;   t_float score_max = T_FLOAT_MIN_ABS;
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      const t_float score = self->matrix[row_id][col_id];
      if(isOf_interest(score)) {
	score_min = macro_min(score_min, score);
	score_max = macro_max(score_max, score);
	arr_scores_size++;
      }
    }
  }
  if(arr_scores_size == 0) { return setToEmpty__s_kt_list_1d_float_t(); }
  //const t_float avg_score_each = 1/arr_scores_size;
  //! Allocate:
  s_kt_list_1d_float_t obj_scores = init__s_kt_list_1d_float_t(cnt_bins);
  obj_scores.current_pos = cnt_bins;
  for(uint bin_id = 0; bin_id < cnt_bins; bin_id++) {
    obj_scores.list[bin_id] = 0; //! ie, the defualt count.
  }
  assert(obj_scores.list_size >= cnt_bins);
  if(score_max == score_min) { //! then we reutnr only 'one'
    // printf("[%u]=%f, with score-range=[%f, %f], at %s:%d\n", cnt_bins-1, (t_float)arr_scores_size, score_min, score_max, __FILE__, __LINE__);
    obj_scores.list[cnt_bins-1] = (t_float)arr_scores_size;
  } else {
    const t_float cnt_eachBucket_inv_global = (t_float)cnt_bins / (t_float)(score_max - score_min); //! ie, (cnt=10) / (5-3) = 5. Use-cases: [3: '(3-3)*5 = 0']++, [4: '(4-3)*5 = 5]++, [5: '(5-3)*5 = 10 ]++, ['']++,  
    //! 
    //! Insert scores: 
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      t_float cnt_eachBucket_inv = cnt_eachBucket_inv_global;
      t_float score_min_loc = score_min;   t_float score_max_loc = score_max;
      if(isTo_forMinMax_useGlobal) { //! then we are interested in fidnig the local min--max for the given row:
	for(uint col_id = 0; col_id < self->ncols; col_id++) {
	  const t_float score = self->matrix[row_id][col_id];
	  if(isOf_interest(score)) {	  
	    score_min_loc = macro_min(score_min_loc, score);
	    score_max_loc = macro_max(score_max_loc, score);
	  }
	}
      }
      //!
      //! Update min-max:
      cnt_eachBucket_inv = (t_float)cnt_bins / (t_float)(score_max_loc - score_min_loc); //! ie, (cnt=10) / (5-3) = 5. Use-cases: [3: '(3-3)*5 = 0']++, [4: '(4-3)*5 = 5]++, [5: '(5-3)*5 = 10 ]++, ['']++,  
      if(score_max_loc == score_min_loc) {continue;} //! ie, as all valeus are then equaøl.
      //!
      //!
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	t_float score = self->matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  //! Handle odd effects of 'nan' and 'inf':
	  if(score < score_min_loc) {score = score_min_loc;}
	  if(score > score_max_loc) {score = score_max_loc;}	  
	  // assert(score >= score_min_loc);
	  // assert(score <= score_max_loc);
	  //! Compute the bucket-id:
	  uint bucket_id = 0;
	  if(score != score_min_loc) {
	    bucket_id = (uint)((score - score_min_loc)*cnt_eachBucket_inv);
	    if(bucket_id >= cnt_bins)  {bucket_id = (cnt_bins - 1);}
	  }
	  //! Increment:
	  //	      printf("[%u]++ given score=%f, at %s:%d\n", bucket_id, score, __FILE__, __LINE__);
	  if(enum_id_scoring == e_kt_histogram_scoreType_sum) {
	    obj_scores.list[bucket_id] += score; //! ie, the defualt count.
	  } else {
	    obj_scores.list[bucket_id] += 1; //! ie, the defualt count.
	  }
	}
      }
    }
  }
  if(enum_id_scoring == e_kt_histogram_scoreType_count_averageOnBins) { //! then normalize the results, ie, to avoid 'skwness' wrt. large data-sets: 
    assert(cnt_bins > 0);
    assert(arr_scores_size > 0);
    t_float count_max = 0; 
    for(uint bin_id = 0; bin_id < cnt_bins; bin_id++) {
      count_max = macro_max(count_max, obj_scores.list[bin_id]);
    }
    const t_float average = /*sum-total=*/(t_float)arr_scores_size / /*bins=*/(t_float)cnt_bins;
    //    const t_float average = /*sum-total=*/(t_float)cnt_bins / /*bins=*/(t_float)arr_scores_size;
    //    const t_float average = (t_float)arr_scores_size / (t_float)cnt_bins;
    assert(average != 0);
    const t_float de_numerator = count_max - average;
    for(uint bin_id = 0; bin_id < cnt_bins; bin_id++) {
      t_float numerator = obj_scores.list[bin_id] - average;
      t_float score = 1; //! ie, a center-point. old_data[i]; //! ie, 
      if( (numerator != 0) && (de_numerator != 0) ) {
	score = numerator / de_numerator;
	assert(score != T_FLOAT_MAX);
      }
      //printf("\t\t [%u]=%f, given num=%f, deNum=%f, at %s:%d\n", i, score, numerator, de_numerator, __FILE__, __LINE__);
      obj_scores.list[bin_id] = score;
    }
  }
  return obj_scores;
}

s_kt_list_1d_float_t applyTo_list__kt_aux_matrix_binning(const s_kt_list_1d_float_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring) {
  assert(self->list_size > 0);
  s_kt_matrix_base_t mat_tmp = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/self->list_size);
  for(uint i = 0; i < self->list_size; i++) {
    mat_tmp.matrix[0][i] = self->list[i];
  }
  s_kt_list_1d_float_t res = applyTo_matrix__kt_aux_matrix_binning(&mat_tmp, cnt_bins, isTo_forMinMax_useGlobal, enum_id_scoring);
  free__s_kt_matrix_base_t(&mat_tmp);
  return res;
}
