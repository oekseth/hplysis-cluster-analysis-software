{
  //! Intalize the optmized log-orutine:
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 0) //! ie, as we then expec thte 'data-strucutre' tio Not have been loaded at 'run-time'.
  allocate_optmize_usePReComptued_floats();
#endif

  //! Make it possible for the caller to copmute only for one of the 'inner rows':
  //  const uint cnt_rows_in = (!isTo_copmuteOnly_forOneRow) ? obj->data_obj->cnt_rows : 1;

  //const bool useSymmetricProperty = (matrix_1 == matrix_2);
  // FIXME: consider to 'drop' [below] clasue wrt. the "nrows_1 != nrows_2" <-- if 'we drop this' then how may we then 'correctly habndle this case'? (oekseth, 06. des. 2016)
  bool useSymmetricProperty = (matrix_1 == matrix_2) && (nrows_1 == nrows_2);
  if(sparseDataResult) {
    useSymmetricProperty = false; // FIXME: dorop this when we have reoslved issue wrt. 'transposed' seiront (oesketh, 60. okt. 2017)
  }
  {

    /* int **mask_dummy = NULL; t_float *weight_dummy  = NULL; */
    /* if(isTo_compute_nonMine_correlations == true) { */
    /*   const int default_value_int = 1; //! ie, mark eveyr valeus as of interest. */
    /*   mask_dummy = allocate_2d_list_int(obj->data_obj->cnt_rows, obj->data_obj->cnt_columns, default_value_int); */
    /*   const t_float default_value_float = 1; //! ie, as we expect all veritces have the same weight. */
    /*   weight_dummy = allocate_1d_list_float(obj->data_obj->cnt_columns, default_value_float); */
    /* } */



    const uint nrows_max = (matrix_1 == matrix_2) ? macro_max(nrows_1, nrows_2) : nrows_1;
    int **arrOf_sorted_1 = (int **) malloc(nrows_max * sizeof(int*)); //allocate_2d_list_int(obj->data_obj->cnt_rows, obj->data_obj->cnt_columns);
    int **arrOf_sorted_2 = NULL;

    //! Comptute sthe 'sorted' vlaues:
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#if (optmize_parallel_2dLoop_useFast == 1)
    //printf("cnt-trheads=%u/%u, at %s:%d\n", (uint)omp_get_num_threads(), (uint)omp_get_max_threads(), __FILE__, __LINE__);
    //#pragma omp parallel
#pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
    //#pragma omp parallel for schedule(static, 5) //schedule(dynamic,3) //! where "3" is based on observaitons
#elif (optmize_parallel_2dLoop_useFast_slowScheduling == 1) //! then we make use of a parallel wrapper
#pragma omp parallel //! which we have observe for soem cases results in a signifcnat slow-down.
    //#pragma omp parallel for schedule(guided,4)
    //#pragma omp parallel for schedule(dynamic,4)
#else
    // printf("(info)\t does not make use of parallelsim wr.t the 2d-loop, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif 
#endif
    //! Start the iteration:
    for(uint x = 0; x < nrows_1; x++) {
      arrOf_sorted_1[x] = argsort(matrix_1[x], ncols); 
    }
    if(matrix_2 != matrix_1) {
      arrOf_sorted_2 = (int **) malloc(nrows_2 * sizeof(int*)); //allocate_2d_list_int(obj->data_obj->cnt_rows, obj->data_obj->cnt_columns);
      for(uint x = 0; x < nrows_2; x++) {
	arrOf_sorted_2[x] = argsort(matrix_2[x], ncols); 
      }
    }
    
#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1) //! then we intiate a set of pairs, ie, to ensure that the threads are givne an 'equal' block-size.
    //const uint max_size = (uint)obj->data_obj->cnt_rows * (uint)cnt_rows_in;
    assert(nrows_1 != 0); assert(nrows_2 != 0);
    const uint max_size = nrows_1 * nrows_2;
    s_localPair_t *listOf_pairs = (s_localPair_t*)malloc(max_size*sizeof(s_localPair_t));
    assert(listOf_pairs);
    uint listOf_pairs_size = 0;
    //! Idnetify the ide of the pairs:
    for(uint x = 0; x < nrows_1; x++) {
      for(uint y = 0; y < nrows_2; y++) { //! ie, we comapre a set of rows, ie, not 'row x column' through instead 'roq x row':
#if (configure_parallel_2dLoop_symmetric_useFastScheduling == 1) //! ie, as we otherwise are interested in testing the overhead 'of not using this optmized approach'.
	if( !useSymmetricProperty || (x < y) ) 
#endif
	  {
	    listOf_pairs[listOf_pairs_size].x = x; 
	    listOf_pairs[listOf_pairs_size].y = y;
	    //! Increment the count:
	    listOf_pairs_size++;
	  }	
      }
    }
    //printf("listOf_pairs_size=%u, max_size=%u, at %s:%d\n", listOf_pairs_size, max_size, __FILE__, __LINE__);
    if(listOf_pairs_size == 0) {
      fprintf(stderr, "!!\t Seems like an error: listOf_pairs_size=%u, while nrows_1=%u, nrows_2=%u, useSymmetricProperty='%s', max_size=%u. In brief please provide the senior developer an heads-up (iof the latter seems ambigious). Observation at [%s]:%s:%d\n", listOf_pairs_size, nrows_1, nrows_2, (useSymmetricProperty) ? "true" : "false", max_size, __FUNCTION__,  __FILE__, __LINE__);
    }
    assert(listOf_pairs_size > 0); //! ie, what we expect.
#endif
    //! Start the iteration:
    t_float *tmp_result = NULL; uint tmp_result_size = 0;
    if(sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017).
      assert(nrows_2 > 0);
      const t_float empty_0 = 0;
      tmp_result_size = nrows_2;
      tmp_result = allocate_1d_list_float(tmp_result_size, empty_0);
      KT__memset_1d_list(tmp_result, tmp_result_size, T_FLOAT_MAX); //! ie, to ensure that we only include 'dineitfed' scores.
    }


#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#if (optmize_parallel_2dLoop_useFast == 1)
    //printf("cnt-trheads=%u/%u, at %s:%d\n", (uint)omp_get_num_threads(), (uint)omp_get_max_threads(), __FILE__, __LINE__);
    //#pragma omp parallel
#pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
    //#pragma omp parallel for schedule(static, 2) //schedule(dynamic,3) //! where "3" is based on observaitons
#elif (optmize_parallel_2dLoop_useFast_slowScheduling == 1) //! then we make use of a parallel wrapper
#pragma omp parallel //! which we have observe for soem cases results in a signifcnat slow-down.
    //#pragma omp parallel for schedule(guided,4)
    //#pragma omp parallel for schedule(dynamic,4)
#else
    // printf("(info)\t does not make use of parallelsim wr.t the 2d-loop, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif
#endif

#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1)
    for(uint pair_id = 0; pair_id < listOf_pairs_size; pair_id++) 
#else
      for(uint x = 0; x < cnt_rows_in; x++) 
#endif
	{
	  /* #if (optmize_parallel_2dLoop_useFast == 1) ||  (optmize_parallel_2dLoop_useFast_slowScheduling == 1) //! then we make use of a parallel wrapper */
	  /*       const uint thread_id = (uint)omp_get_thread_num();      */
	  /*       printf("[%u]\t thread_id=%u, at %s:%d\n", x, thread_id, __FILE__, __LINE__); */
	  /* #endif */

#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1)
	  const uint x = listOf_pairs[pair_id].x;
	  const uint y = listOf_pairs[pair_id].y;
	  //bool isSymmetric = false;
	  //if(isTo_use_codeRewritingTo_reduceExeuctionTime()) 
	  //! Compute distance-resutls for cell:

	  mine_problem prob;
	  mine_parameter param;
	  param.alpha = 0.60;
	  param.c = 1;
	  param.est = EST_MIC_E;
	  // TODO: try imrpvoing [”elow] costnatns:
	  const t_float power = 0.7; //! ie, a default value.
	  const t_float eps = 0.07; //! ie, a default value.
	  /* check the parameters */ 
	  t_float ret_val = T_FLOAT_MAX;
	  const char* errorMsg = mine_check_parameter(&param);
	  if(errorMsg == NULL) {
	    prob.n = ncols;
	    prob.x = matrix_1[x];       prob.y = matrix_2[y];
	    //! Compute the score:
	    mine_score *score = mine_compute_score(&prob, &param);       
	    const t_float score_tmp = __internalFunc__();
	    if(resultMatrix != NULL) {
	      resultMatrix[x][y] = score_tmp;
	    } else {
	      assert(tmp_result);
	      assert(y < tmp_result_size);
	      tmp_result[y] = score_tmp;
	    }
#ifndef NDEBUG
	    //(t_float)(uint)resultMatrix[x][y];
	    if( (score_tmp != T_FLOAT_MAX) && (score_tmp != T_FLOAT_MIN_ABS) ) {
	      // if( (score_tmp < 0) || (score_tmp > max_value) ) {fprintf(stderr, "!!\t Investigate why score=%f, at %s:%d\n", score_tmp, __FILE__, __LINE__);}
	      // assert_possibleOverhead((uint)score_tmp <= 1);
	    }
#endif
	    if(useSymmetricProperty) { //! Update the symmetric proeprty:
	      if(resultMatrix != NULL) {
		resultMatrix[y][x] = score_tmp; // resultMatrix[x][y];
	      }
	      //printf("score[%u][%u]\t (%f, %f), at %s:%d\n", x, y, resultMatrix[y][x], resultMatrix[x][y], __FILE__, __LINE__);
	    } else{
	      // printf("score[%u][%u]\t %f\t #! at %s:%d\n", x, y, resultMatrix[x][y],  __FILE__, __LINE__);
	    }
	    

	    /* free score */
	    mine_free_score(&score); 	    
	  } 

	  // __computeFor_cell(obj, x, y, arrOf_sorted, isTo_writeOut_duringComptuation_humanized_dense, isTo_compute_nonMine_correlations, mask_dummy, weight_dummy);
#else 
	  assert(obj->data_obj->matrixOf_data[x]);
	  for(uint y = 0; y < obj->data_obj->cnt_rows; y++) { //! ie, we comapre a set of rows, ie, not 'row x column' through instead 'roq x row':
	    if(!useSymmetricProperty || (x != y) ) { //! ie, to avodi a 'self-comparison':
	      
	      //! Compute distance-resutls for cell:

	      mine_problem prob;
	      mine_parameter param;
	      param.alpha = 0.60;
	      param.c = 1;
	      param.est = EST_MIC_E;
	      // TODO: try imrpvoing [”elow] costnatns:
	      const t_float power = 0.7; //! ie, a default value.
	      const t_float eps = 0.07; //! ie, a default value.
	      /* check the parameters */ 
	      t_float ret_val = T_FLOAT_MAX;
	      const char* errorMsg = mine_check_parameter(&param);
	      if(errorMsg == NULL) {
		prob.n = ncols;
		prob.x = matrix_1[x];       prob.y = matrix_2[y];
		//! Compute the score:
		mine_score *score = mine_compute_score(&prob, &param);       
		const t_float score_tmp = __internalFunc__();
		if(resultMatrix != NULL) {
		  resultMatrix[x][y] = score_tmp;
		} else {
		  assert(tmp_result);
		  assert(y < tmp_result_size);
		  tmp_result[y] = score_tmp;
		}
		
		//printf("score[%u][%u]\t %f\t #! at %s:%d\n", x, y, resultMatrix[x][y],  __FILE__, __LINE__);



#ifndef NDEBUG
		//		const t_float score_tmp = resultMatrix[x][y];
		if( (score_tmp != T_FLOAT_MAX) && (score_tmp != T_FLOAT_MIN_ABS) ) {
		  // if( (score_tmp < 0) || (score_tmp > max_value) ) {fprintf(stderr, "!!\t Investigate why score=%f, at %s:%d\n", score_tmp, __FILE__, __LINE__);}
		  //if( (score_tmp < 0) || (score_tmp > 1) ) && ({fprintf(stderr, "!!\t Investigate why score=%f, at %s:%d\n", score_tmp, __FILE__, __LINE__);}
		  //if(score_tmp > 0) {fprintf(stderr, "!!\t Investigate why score=%f, at %s:%d\n", score_tmp, __FILE__, __LINE__);}
		  assert_possibleOverhead((uint)score_tmp <= 1);
		}
#endif

		/* free score */
		mine_free_score(&score); 
	    
	      } 


	      //assert(false); // FIXME: inlcude a permtatuion of [”elow]
	      //__computeFor_cell(obj, x, y, arrOf_sorted, isTo_writeOut_duringComptuation_humanized_dense, isTo_compute_nonMine_correlations, mask_dummy, weight_dummy);
	    }
	  }
#endif
	  //printf("at %s:%d\n", __FILE__, __LINE__);
	  if(sparseDataResult) {
	    //	  if(resultMatrix == NULL) { //! then we assuemt he sparse-result-set is to be updated: 
	    assert(sparseDataResult);
	    //! Then merge the result: 
/* #if (optmize_2d_loop_makeUseOf_symmetricProperty == 1) */
/* 	    assert(false); // FIXME: vlaidte cornrect eness of [below] "isLast_columnForBlock" propty (oekseth, 06. aug. 2017). */
/* #endif */
	    assert(tmp_result_size > 0);
	    
	    //printf("at %s:%d\n", __FILE__, __LINE__);
	    apply__s_kt_list_2d_kvPair_filterConfig_t((sparseDataResult), &tmp_result, 
						      /*globalStartPos_head=*/x, /*globalStartPos_tail=*/0, 
						      /*chunkSize_index1=*/1, /*chunkSize_index2=*/tmp_result_size, 
#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1)
						      /*isLast_columnForBlock=*/(useSymmetricProperty) ? (x >= (y+1)) : ((y+1)==nrows_2), 
#else
						      true,
#endif
						      /*isTo_updateSyemmtricRelations=*/useSymmetricProperty);
	  }
	} 
    if(sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017).
      assert(tmp_result != NULL);
      free_1d_list_float(&tmp_result); tmp_result = NULL;
      }
    //! Free the optmized log-orutine: 
    /* if(mask_dummy) {free_2d_list_int(&mask_dummy);}  */
    /* if(weight_dummy) {free_1d_list_float(&weight_dummy);} */
    //if(isTo_use_codeRewritingTo_reduceExeuctionTime() == true) 
    {
      for(uint x = 0; x < nrows_max; x++) {
	free(arrOf_sorted_1[x]);
      }
      free(arrOf_sorted_1);
      if(matrix_2 != matrix_1) {
	for(uint x = 0; x < nrows_2; x++) {
	  free(arrOf_sorted_2[x]);
	}
	free(arrOf_sorted_2);
      }
    }
#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1) //! then we intiate a set of pairs, ie, to ensure that the threads are givne an 'equal' block-size.
    assert(listOf_pairs_size > 0); assert(listOf_pairs);
    free(listOf_pairs);
#endif

  }
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 0)
  //! Free the optmized log-orutine: 
  free_optmize_usePReComptued_floats();
#endif
} 
