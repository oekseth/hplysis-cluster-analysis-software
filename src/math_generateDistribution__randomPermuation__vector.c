#ifndef NDEBUG
  for(uint i = 0; i < list_size; i++) {assert(list[i] == result_list[i]);} //! ie, vlaidate to be on the safe side.
#endif
  


  uint *list_cntUpdates_from = NULL;   uint *list_cntUpdates_to = NULL;
  if(config_swapsMayOverlap == false) {
    /* //! ---- */
    /* if(list_size_cntToSwap > list_size) { */
    /*   fprintf(stderr, "!!\t Seems like an infite loop: you have asked the 'calls' to be unique, though" */
    /* } */
    //! ----
    const uint empty_0 = 0;
    list_cntUpdates_from = allocate_1d_list_uint(list_size, empty_0);
    assert(list_cntUpdates_from);
    //! ----
    list_cntUpdates_to = allocate_1d_list_uint(list_size, empty_0);
    assert(list_cntUpdates_to);    
  }
const bool isTo_swap_values = (maxValue_toSetInRandomization == UINT_MAX) || (maxValue_toSetInRandomization == 0);
const uint list_size_max = ( isTo_swap_values == false) ? maxValue_toSetInRandomization : list_size - 1; //! ie, as thte latter is 'used' to 'se the amx-index to be used'.
const uint list_size_max_index = list_size - 1; //! ie, as thte latter is 'used' to 'se the amx-index to be used'.

const bool localConifg__isToUSePRev_index_inMove = (isTo_swap_values) ? true : false;//! ie, a simplfed/genralited assumption

uint prev_oldIndex_row = UINT_MAX;

//bool isTo_break = false; uint cnt_updated = 0;
const uint swap_count_max = (config_swapsMayOverlap) ? list_size_cntToSwap : macro_min(list_size_cntToSwap, list_size); //! ie, to ato 'aovid' an infiten swapping-operaiton
for(uint swap_count = 0; swap_count < swap_count_max; swap_count++) {
//for(uint swap_count = 0; (swap_count < list_size_cntToSwap) && (isTo_break == false); swap_count++) {
    bool is_updated = false;
    // TODO: is [”elow] correct? wrt. teh "p"?
    //const t_float p = 1.0/(list_size_cntToSwap - swap_count);
    const t_float p = 0.4;

    //! 
    //! Idneitfy the 'old' index
    uint old_index = UINT_MAX;
    //printf("[%u]\tstart:first, given p=%f, at %s:%d\n", swap_count, p, __FILE__, __LINE__);
    //    if(maxValue_toSetInRandomization != UINT_MAX) 
    {
      if( (prev_oldIndex_row == UINT_MAX) || !localConifg__isToUSePRev_index_inMove ) 
	{ uint cnt_in_while = 0;
	  do {
	    old_index = math_generateDistribution__binomial(/*n*/list_size_max, p);
	    
	    //! Test for convergence and update:
	    if(maxValue_toSetInRandomization != UINT_MAX) {
	      is_updated = (list_cntUpdates_from) ? (list_cntUpdates_from[old_index] == 0) : true;
	    } else {is_updated = true;}
	    //printf("\t\told_index=%u, list_size_cntToSwap=%u, at %s:%d\n", old_index, list_size_cntToSwap, __FILE__, __LINE__);
	    assert(cnt_in_while < 5*list_size); //! ie, as we otherwise have a cirical bug.
	  } while(is_updated == false);
	} else {old_index = prev_oldIndex_row;}
      //printf("[%u]\tfinish:first, at %s:%d\n", swap_count, __FILE__, __LINE__);
      if(list_cntUpdates_from) {list_cntUpdates_from[old_index]++;}
      assert(old_index != UINT_MAX);
    }

    //! 
    //! Idneitfy the 'new' index
    uint new_index = UINT_MAX;
    { uint cnt_in_while = 0;
      do {	
	new_index = math_generateDistribution__binomial(/*n*/list_size_max_index, p);	
	// printf("\t\tnew_index=%u, at %s:%d\n", new_index, __FILE__, __LINE__);
	//! Test for convergence and update:
	if( (maxValue_toSetInRandomization == UINT_MAX) ) {
	  if( (new_index != old_index) ) {
	    is_updated = (list_cntUpdates_to) ? (list_cntUpdates_to[new_index] == 0) : true;
	  } else {is_updated = false;}
	} else {
	  is_updated = (list_cntUpdates_to) ? (list_cntUpdates_to[new_index] == 0) : true;
	}
	assert(cnt_in_while < 5*list_size); //! ie, as we otherwise have a cirical bug.
      } while(is_updated == false);
    }
    if(list_cntUpdates_to) {list_cntUpdates_to[new_index]++;}
    //printf("[%u]\tfinish:second, at %s:%d\n", swap_count, __FILE__, __LINE__);
    assert(new_index != UINT_MAX);
    if( (maxValue_toSetInRandomization == UINT_MAX) || (maxValue_toSetInRandomization == 0) ) {
      assert(old_index != new_index);
      assert(old_index < list_size);
    } else {
      if(old_index > maxValue_toSetInRandomization) {
	printf("old_index=%u, maxValue_toSetInRandomization=%u, given list_size_max=%u, at %s:%d\n", old_index, maxValue_toSetInRandomization, list_size_max, __FILE__, __LINE__);
      }
      assert(old_index <= maxValue_toSetInRandomization);
    }
    assert(new_index < list_size);
    //printf("new_index=%u, old_index=%u, list_size=%u, at %s:%d\n", new_index, old_index, list_size,  __FILE__, __LINE__);
    //!
    //! Update:
    if(maxValue_toSetInRandomization == UINT_MAX) {
      result_list[new_index] = result_list[old_index];
    } else {
      result_list[new_index] = old_index;
    }

    /* if(config_swapsMayOverlap) { */
    /*   cnt_updated++; */
    /*   if(cnt_updated >= list_size) { */
    /* } */

    //!
    //! Update
    prev_oldIndex_row = new_index;
  }

  //! De-allocate:
  if(list_cntUpdates_from) { free_1d_list_uint(&list_cntUpdates_from); list_cntUpdates_from = NULL;}
  if(list_cntUpdates_to) { free_1d_list_uint(&list_cntUpdates_to); list_cntUpdates_to = NULL;}
