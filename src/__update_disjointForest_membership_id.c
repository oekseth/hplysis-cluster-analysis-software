//static void __update_disjointForest_membership_id(s_kt_forest_findDisjoint *self, const uint this_key, const uint parent_key) 
{
  //! What we expect:
  assert(this_key != UINT_MAX);
  assert(parent_key != UINT_MAX);
  //! Validate that the operation is to be regarded as 'complex': otehrwise we expect the caller to have hadneld the situation/case.
  const uint child_forest_id  = mapOf_vertexMembershipId[this_key]; //.get_key();
  const uint parent_forest_id = mapOf_vertexMembershipId[parent_key]; //.get_key();
  assert(child_forest_id != UINT_MAX);
  assert(parent_forest_id != UINT_MAX);
  assert(child_forest_id != parent_forest_id); // ie, as the operation would otherwise not have a point.
  /* //! Valdiate that the forest-ids actually represent a 'possilbe' id in the set: */
  /* assert(child_forest_id < self->listOf_membership_vertexCollection.get_current_pos()); */
  /* assert(parent_forest_id < self->listOf_membership_vertexCollection.get_current_pos()); */

  //!
  //! The operation:  
  //const list_generic<type_uint> *listOf_children = NULL;
  uint updated_forest_id = parent_forest_id;
  uint prev_forest_id    = child_forest_id;
  /* { //! Sort he set of vertices, ie, as we inserted them 'in an unsorted fasion' to increse the eprformance, ie, as 'merge sort' has a lower time-complexity than insertion-sort (where the altterw ould be the result of soritng the vertices at the 'exeuciton-time' when they are inserted). */
  /*   //! Pre-conditons: */
  /*   assert(self->listOf_membership_vertexCollection.get_sparseRow_ofIds__s_kt_set_2dsparse_t(child_forest_id)); */
  /*   assert(self->listOf_membership_vertexCollection.get_sparseRow_ofIds__s_kt_set_2dsparse_t(parent_forest_id)); */
  /*   //! The opeation: */
  /*   self->listOf_membership_vertexCollection.get_unsafe_list_object(child_forest_id)->apply_merge_sort(/\*remove-redundant-relements=*\/true); */
  /*   self->listOf_membership_vertexCollection.get_unsafe_list_object(parent_forest_id)->apply_merge_sort(/\*remove-redundant-relements=*\/true); */
  /*   //! The result we expect: */
  /*   assert(self->listOf_membership_vertexCollection.get_unsafe_list_object(child_forest_id)->is_sorted()); */
  /*   assert(self->listOf_membership_vertexCollection.get_unsafe_list_object(parent_forest_id)->is_sorted()); */
  /* } */
  uint listOf_children_size = 0; const uint *listOf_children = NULL;
  assert_possibleOverhead(child_forest_id != parent_forest_id);
  { //! Identify the set to get set to 'be chosen', using the 'lowest id' as the selection criteria: from teh fact that the frist ivnestigated vertex (for a given 'disjoint forest' has the lwoest id', we (on average) expects that the tree with the lwoest index has the most number of vertices, ie, to 'keep the tree with the lowest index' is sepxected to give a reduced exueciton-cost, ie, the latter is an optimization-strategy.
    if(child_forest_id < parent_forest_id) { // then we updat ethe 'parents' forest ids.
      listOf_children = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), parent_forest_id, &listOf_children_size); // ie, get the set of identified vertices.
      //! Then insert he 'other' set into this:
      updated_forest_id = child_forest_id;
      prev_forest_id    = parent_forest_id;
      //! What we expect:
      assert(updated_forest_id != parent_forest_id); // ie, what we expect
    } else {
      listOf_children = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), child_forest_id, &listOf_children_size); // ie, get the set of identified vertices.
      updated_forest_id  = parent_forest_id;
      prev_forest_id     = child_forest_id;
      
      assert(updated_forest_id != child_forest_id); // ie, what we expect
      // assert(self->listOf_membership_vertexCollection.get_unsafe_list_object(updated_forest_id) != listOf_children); // as it otherwise would imply that [above] has an erorr.
    }	
    if(listOf_children_size == 0) { //! then investigate wrt. the 'child-size'.
      //! First try the "child_forest_id":
      listOf_children = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), parent_forest_id, &listOf_children_size); // ie, get the set of identified vertices.
      if(listOf_children_size > 0) {
	//! Then insert he 'other' set into this:
	updated_forest_id = child_forest_id;
	prev_forest_id    = parent_forest_id;
	//! What we expect:
	assert(updated_forest_id != parent_forest_id); // ie, what we expect
      } else { //! then investigate wrt. teh "parent_forest_id":
	listOf_children = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), child_forest_id, &listOf_children_size); // ie, get the set of identified vertices.
	updated_forest_id  = parent_forest_id;
	prev_forest_id     = child_forest_id;	
	assert(updated_forest_id != child_forest_id); // ie, what we expect
      }
    }
  }
  if(listOf_children && listOf_children_size)
  { //! Combine the vertex-sets, ie, for each of the vertices in the set, update their forest-id:    
    assert(listOf_children != NULL); assert(listOf_children_size > 0);
    // TODO: try update [”elow] approach, ie, as it may be regareded as both a brute-force approach, and an approach with a high complexity <-- is this observation/need ny longer a consern? (oekseth, 04. july 2015)
    //printf("\t\t\t(::mergeslists)\t new-forst-id=%u, listOf_children_size=%u, given child_forest_id=%u, parent_forest_id=%u, at %s:%d\n", updated_forest_id, listOf_children_size, child_forest_id, parent_forest_id, __FILE__, __LINE__);
    for(uint i = 0; i < listOf_children_size; i++) {
      const uint vertex_id = listOf_children[i]; //->template_get_value(i).get_key();
      assert(vertex_id != UINT_MAX); // ie, what we expect.
      const uint vertex_old_forest_id = mapOf_vertexMembershipId[vertex_id];
      assert(vertex_old_forest_id != UINT_MAX); // ie, as we expect 'every vertex marked to be part of a forest' to have been assinged a forest-id.
      assert(vertex_old_forest_id == prev_forest_id); // as it otherwise would indicate that our update-procedure has inconsistencies.
      //! Then set the updated forest-id fo the vertex in question:
      //printf("\t((update-disjoitn-mapping))\t vertex[%u].mappings: %u -->%u, at %s:%d\n", vertex_id, vertex_old_forest_id, updated_forest_id, __FILE__, __LINE__);
      mapOf_vertexMembershipId[vertex_id] = updated_forest_id;
      // if(false && hash) {printf("(update-forest-id)\t \"%s\" is set to a forest-id=%u , from knowing the relationship of %s\t%s\t at %s:%d\n", hash->get_word(vertex_id), updated_forest_id, hash->get_word(this_key), hash->get_word(parent_key), __FILE__, __LINE__);}
    }
  } else { //! then we assume 'no tails' where assicated to the vertices (oekseth, 06. mar. 2017):
    mapOf_vertexMembershipId[this_key] = mapOf_vertexMembershipId[parent_key] = updated_forest_id;
  }
  { //! Validate that the two input-vertices has been givent ehe same forest-id:
    //! Note: the number of overlappings tests [”elow] is to be able to track errors in our own asserts, ie, a aulity-check of the quality-checks.
    const uint child_forest_id  = mapOf_vertexMembershipId[this_key];
    const uint parent_forest_id = mapOf_vertexMembershipId[parent_key];
    assert(child_forest_id  != UINT_MAX);
    assert(parent_forest_id != UINT_MAX);
    assert(child_forest_id  == updated_forest_id);
    assert(parent_forest_id == updated_forest_id);
    //! Then validate that the ids are actually set to the same forest-id:
    assert(child_forest_id == parent_forest_id);
  }
  
  
  if(listOf_children && listOf_children_size) {
    //! Combine the cluster-sets:
    add_rowSparse__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), updated_forest_id, listOf_children, listOf_children_size, NULL);
    assert(updated_forest_id != UINT_MAX);
    assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), updated_forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
    //! Then 'remove' the old set of elements (oekseth, 06. des. 2016):
    const uint removed_forest_id = (updated_forest_id == parent_forest_id) ? child_forest_id : parent_forest_id;
    assert_possibleOverhead(removed_forest_id != updated_forest_id);
    clear_row__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), removed_forest_id);
    assert(removed_forest_id != UINT_MAX);
    assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), removed_forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
  } //! else we assume 'no tails' where assicated to the vertices (oekseth, 06. mar. 2017).
  //self->listOf_membership_vertexCollection.get_unsafe_list_object(updated_forest_id)->insert_list_sorted(listOf_children);
}

