#include "kt_matrix_cmpCluster.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "kt_api_config.h"
#include "kt_terminal.h"
#include "measure_base.h"
#include "kt_distance.h" //! which (among others) is used for comptatuion of corrleation-atmrix in oru [”elow] exampels.
#include "correlation_base.h"
#include "correlation_macros__distanceMeasures.h"
#include "hpLysis_api.h"
#include "kt_assessPredictions.h"
#include "math_generateDistribution.h" //! which is used iot. 'gave' radnom permtuations.
//! ------------------------
/* //! Libraries for file-copying: */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */
/* #include <unistd.h> */
/* //! ------------------------ */

#include "measure_kt_matrix_cmpCluster__dataSet__cluster__k3.c"
#include "measure_kt_matrix_cmpCluster__dataSet__cluster__k3_v10.c"
#include "hp_clusterShapes.h" //! ie, used in our "testHardCodedDistributions--dataUsedAsInput" and our "testHardCodedDistributions--CCM"
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "matrixdata.h" //! which is sued for the Vilje-matrix-data-loading in our "tut_inputFile_6.c" (eosekth, 60. feb. 2017).
#include "aux_sysCalls.h"

/**
   @remarks for the sytnetic data-sets the sub-groups we evalaute are:
   -- "lines": non-periodic lines with different growth-factor
   -- "sinusoidal": periodic lines which evaluate/identified/assicateds teh compelxity assicated to describing/enumerating/indietfying the simliarty wrt. 'phase-shifting' (for different metrics).
   -- "noise": a 'reference-frame' wrt. the simlairty in predictiosn and agreements: used as a 'basis for comparison' (when evlautignt eh signficant patterns between the differnet data-sets).
   -- "distinct": a set of 'distinct cases', ie, where we use data-math-funcitons which may esialiy be 'modelled'/captured by 'tailed' metrics.

   In below we use the [ªbove] metircs to ....??.... 
**/

/**
   @enum e_measure_cmpCluster__dataCategories_row
   @brief categorize the different sytentic data-types which we explore (in a specific experiement tailored wrt. this purpose) (oekseth, 06. des. 2016).
   @remarks used in the 'collection' of cluster-results. For each of the data-sets (eg, for "lines-different-ax") we seprately apply the following steps:
   -- step(1): task: "describe both a gold-standard and a representative sollution-permtuation-space wrt. a global-coverage of cluster-extreme-cases": a simple strategy/apppraoch/case is to comptue clsuters for a given {data-set, corr-metric} and then write out the heatmap-matrix: in our appraoch we write out sepeately for all data-sets using the 'default configuration' in our "hpLysis_api" and with noise=none; the  results are used to visulaly hypothese gold-stanrds wrt. the results. In this evaluation we address the 'task' wrt. 'representativeness of data-sets' by ....??... <-- todo: try addressing 'this issue'.
   -- step(2): task: "collect clusters and compare to gold-standard": for each 'heatmap-gold-standard' collect the cluster-results, where we for each of below 'permtuations' call our "standAlone__describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__s_kt_assessPredictions_t(..)":
   -- step(2.a): [data-noise]: seperately 'build' for different permtuations assicated to our "e_measure_cmpCluster__dataCategories_col_t";
   -- step(2.b): [corr-metrics]: [simliar as [ªbove], with difference that we use the 'case' secpfed/related to "e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust_t"];
   -- step(2.c): [k-value-permtuations]: test the effect of different non-ideal 'k-cluster-count-permtuations'
   -- step(2.d): [data-noise]x[corr-metrics]x[k-value-permutations]: [combine [above] cases]
   -- step(3): task: "may the use of reprsentative data-subsets (in evlauation of cluster-accuracy) signficnatly perturb/includence the cluster-results, ie, what are the cluster-comparison-metrics which produce consistent results (for data-sets with different size though with the same underlyign cluster-simliarty-proeprty)?" 	  ... where we 'repreat [ªbove] cases for different "nrows" matrix-sizes (and as a 'prior' step update the gold-stadnard): in our 'test-bed' we use the ["nrows_base", "numberOf_matrices", "mult_factor", "numberOf_matrices_stepFactor"] parameter-space/variable-space to idneitfy/describe the size-property of the matrices to evalauate. <-- try describng 'this test-appraoch'?
   -- step(3.a): visual: for each of the cases in "step(2)" copmtue the result and visually inspect/evlauate the result;
   -- step(3.b): visual-gold-map-comparison: compare 'eahc of the resutls' with the first dat-areuslt to hyptoioese/evlaute the differenation with the 'zero-case': in this appraoch we compared heatmap=['first-case']x[each], where we use a (goldMatrix=[metric][metric])x(eachMartix=[metric][metric]) as input. 
   -- step(3.c): task: "collect the signficance-results and use the thereafter visaully compare/idneitfy/evlauate 'patterns' between different the different data-sets". As a 'gold-cluster-standard' we (for simplicty and non-ambibutity) use/evluate the first data-set in the clustering (ie, for which we may use the 'pre-defined' k-count of clusters):
   -- step(3.c.1): task: "how similar are teh prediction-results?": unify the different matrix-cases and generate a 'summary-heat-map': sperately for each of the "e_kt_matrix_cmpCluster_metric_undef" "cluster-comparison-metrics" idneityf a simliarty-score (which is combiend into a simliarty-score-vector for each of the data-sets, and use the reuslt ot genrate+civusally-evlauate a heatmap-matrix: 
   -- step(3.c.2): task: "how much influcence has the matrix-size wrt. cluster-comparison-simlairty-prediction": seperately for each "e_kt_matrix_cmpCluster_metric_undef" build a matrix "[matrix-diemsnioantliy-case][matrix-permtuation]=<cluster-comparison-score>", where the "<cluster-comparison-score>" is inferred from a comparison 'the first matrix-case in each data-set'
   -- step(): 
   -- step(): 
**/
typedef enum e_measure_cmpCluster__dataCategories_row {
  e_measure_cmpCluster__dataCategories_row_each,
  e_measure_cmpCluster__dataCategories_row_combined_lines,
  e_measure_cmpCluster__dataCategories_row_combined_sinus,
  e_measure_cmpCluster__dataCategories_row_noise,
  e_measure_cmpCluster__dataCategories_row_distinct,
  e_measure_cmpCluster__dataCategories_row_realLife_small, //! ie, a real-life test-data-set
} e_measure_cmpCluster__dataCategories_row_t;


// FIXME: update [”elow] k-count of cluster .. ie, after we have 'visually evlauate'd teh heatmap-gold-stnadards:
// FIXME:; wrt. [”elow] try to 'use visual inspection' to dineify diferent cluster-cateogires wrt. "mapOf_realLife_classificaiton" ... updating our "e_measure_cmpCluster__dataCategories_row"
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__inputDataToEvaluate.c"


/**
   @enum e_measure_cmpCluster__dataCategories_col
   @brief  (oekseth, 06. des. 2016).
   @remarks used in the 'collection' of cluster-results.
**/
typedef enum e_measure_cmpCluster__dataCategories_col {
  e_measure_cmpCluster__dataCategories_col_useFixedNoise,
  e_measure_cmpCluster__dataCategories_col_useFixedNoise__andDifferentRandomScores,
  e_measure_cmpCluster__dataCategories_col_useDfferentRandomScores,
  /* e_measure_cmpCluster__dataCategories_col_ */
  /* e_measure_cmpCluster__dataCategories_col_ */
  /* e_measure_cmpCluster__dataCategories_col_ */
} e_measure_cmpCluster__dataCategories_col_t;


/**
   @enum e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust
   @brief  (oekseth, 06. des. 2016).
   @remarks used in the 'collection' of cluster-results.
**/
typedef enum e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust {
  e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust_none,
  e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust_hpLysisCorrMetrics__subset_extremeCases,
  e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust_hpLysisCorrMetrics__all
} e_measure_cmpCluster__dataCategories_col_corrMetric_beforeClust_t;


/* /\** */
/* 	 @enum e_measure_cmpCluster__dataCategories_col */
/* 	 @brief  (oekseth, 06. des. 2016). */
/*  **\/ */
/* typedef enum e_measure_cmpCluster__dataCategories_col { */
/* 	e_measure_cmpCluster__dataCategories_col_ */
/* } e_measure_cmpCluster__dataCategories_col_t; */

static uint cnt_Tests_evalauted = 0;

static void __tut__case__2(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r
  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_2", array[2], strlen("tut_inputFile_2"))) ) {    
    //! Note: in this test-case we examplify how to evlauate/test different randomze-cases: we foucs on time-execution-perofmrnace. Of interest is ot idnetify the wrost-case exec-time and result-accuracy for challing data-serts. Therefre we (in this test-case) use/evlauate large/difficult data-sets (where size is descided using a manual/itnal investigaiton). To facilitate a simplfied evlauation of the clsuter-accuracy we 'merge'/concncatenate each of the well-defined data-sets with a data-set defined by a cofngiruation-variable named config__nameOfDefaultVariable. Our null-hypothesis is that our MINE-approach provide a lower/improved worst-case perofmrance (when compared to alternaitve appraoches). 
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    //if(false)
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2__city/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_2__city_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2__city_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
      //! ----------------------------------- 
#include "tut_inputFile_2.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    //if(false)
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
      //! ----------------------------------- 
#include "tut_inputFile_2.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //!     
    //if(false)
    { //! Then increase the 'complexity' of the data-sets, updating wrt. the random fraciton of columns (i,e radnomzie-columns (rc)):
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut2_rc/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_2rc_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2rc_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
  //! ----------------------------------- 
  //! 
  //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
  //! ----------------------------------- 
#include "tut_inputFile_2.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    //if(false)
    { //! Then increase the 'complexity' of the data-sets, updating wrt. the random fraciton of columns (i,e radnomzie-rows (rr)):
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut2_rr/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_2rr_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2rr_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
      //! ----------------------------------- 
#include "tut_inputFile_2.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    //if(false)
    { //! Then increase the 'complexity' of the data-sets, updating wrt. the random fraciton of columns (i,e radnomzie-columns and random-rows (rcrr)):
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut2_rcrr/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_2rcrr_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2rcrr_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
  //! ----------------------------------- 
#include "tut_inputFile_2.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    //if(false)
    { //! Extend: "rcrr": use an npass=100, ie, where reuslt is expected to have a decreased accuracy:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut2_rcrr_p100/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_2rcrr_p100_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2rcrr_p100_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 100; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
      //! ----------------------------------- 
#include "tut_inputFile_2.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
#undef  __M__calledInsideFunction

    assert(false); // FIXME: remove
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }
}
static void __tut__case__3(const int array_cnt, char **array) {
  //! Idea: evalaute the complete set of sytnatic functiosn defined in our hpLysis: in hpLyssi the sytnatci fucntion-ensabmlel7set is defined to capture cases known to be chellenging in the task/applciaiton of relating feature-vectors with simliar growth-patterns. 
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_3", array[2], strlen("tut_inputFile_3"))) ) {    
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_3/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_3_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_3_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
      printf("at %s:%d\n", __FILE__, __LINE__);
#include "tut_inputFile_3.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    
    //! 
    assert(false); // FIXME: remove
    { //! Then an appraoch werhe we esxntiecly apply randomziaotn on both rows and columns (ie, rr and rc):
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_3_rcrr/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_3_rcrr_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_3_rcrr_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
#include "tut_inputFile_3.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    { //! A permtaution of the  [abvve] "rcrr" appraoch where we use a funciton as an atlernative to a ranomized idsturbiotn:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_3_rcrr_sinus/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_3_rcrr_sinus_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_3_rcrr_sinus_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "sinus";
#include "tut_inputFile_3.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }
}
static void __tut__case__4(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_4", array[2], strlen("tut_inputFile_4"))) ) {    
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_4/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_4_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_4_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
#include "tut_inputFile_4.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    //! 
    { //! Then an appraoch werhe we esxntiecly apply randomziaotn on both rows and columns (ie, rr and rc):
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_4_rcrr/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_4_rcrr_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_4_rcrr_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
#include "tut_inputFile_4.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    { //! A permtaution of the  [abvve] "rcrr" appraoch where we use a funciton as an atlernative to a ranomized idsturbiotn:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_4_rcrr_sinus/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_4_rcrr_sinus_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_4_rcrr_sinus_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "sinus";
#include "tut_inputFile_4.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    { //! A permtaution of the  [abvve] "rcrr_sinus" where we do Not use any 'data-matrix-cocnteatniogn' appraoch:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_4_rcrr_none/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_4_rcrr_none_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_4_rcrr_none_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = NULL;
#include "tut_inputFile_4.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }
}
static void __tut__case__5(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r
  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_5", array[2], strlen("tut_inputFile_5"))) ) {    
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_5/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_5_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_5_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
#include "tut_inputFile_5.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    //! 
    { //! Then an appraoch werhe we esxntiecly apply randomziaotn on both rows and columns (ie, rr and rc):
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_5_rcrr/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_5_rcrr_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_5_rcrr_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
#include "tut_inputFile_5.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    { //! A permtaution of the  [abvve] "rcrr" appraoch where we use a funciton as an atlernative to a ranomized idsturbiotn:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_5_rcrr_sinus/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_5_rcrr_sinus_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_5_rcrr_sinus_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "sinus";
#include "tut_inputFile_5.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
    //! 
    { //! A permtaution of the  [abvve] "rcrr_sinus" where we do Not use any 'data-matrix-cocnteatniogn' appraoch:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_5_rcrr_none/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_5_rcrr_none_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_5_rcrr_none_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 100;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = NULL;
#include "tut_inputFile_5.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }
}
static void __tut__case__6(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  //!
  //!
  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_6_vilje", array[2], strlen("tut_inputFile_6_vilje"))) ) {    
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---- input:
    const char *config__fileName__binaryMatrix = "data/vilje-cost-rack1-half.dat"; //! a matrix which may be given at request to "jancrhis@ntnu.no" at NTNUs super-comptuer-faciltity:
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ---- result:
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_6/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_6_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_6_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid; //! which is 'here' ingored, ie, as [”elow] "clusterAlg" is suet to 'undef'.
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_undef; //! ie, Not perform clustering, ie, only conmptue wrt. the simlairty-metrics
      //! ----------------------------------- 
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
#include "tut_inputFile_6_vilje.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   

      assert(false); // FIXME: remvoe
}    
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ---- result:
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_6_medium/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_6_medium_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_6_result_medium_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_subset__medium; //! ie, use a subset of the simlairty-metics in the 'clsuter-aprt'.
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans; //! ie, perform clustering using k-emans-permtuations of clsuteirng-algorithms.
      //! ----------------------------------- 
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
#include "tut_inputFile_6_vilje.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    
    //! 
    { //! An extensive evalaution where we use an all-agiansta-ll appraoch both wrt. clust-algs and sim-metics:
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ---- result:
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_6__large/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_6_large_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_6_large_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      
      //! ----------------------------------- 
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
#include "tut_inputFile_6_vilje.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    //! 
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }

}
static void __tut__case__7(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_7", array[2], strlen("tut_inputFile_7"))) ) {    
    //! Idea: test effects of matrix-dimensions: build a set of evlauations where we grudally increase the data-size (oekseth, 06. feb. 2017).   
    //! FIXME: update oru airltc-etext wrt. the results generated b'y this' ... evaluate the exec-time-effect wrt. different dimesnions of the input-amtrix .... eg, [300, 300], [3000, 300], [12000,300], [300, 3000], [300, 12000], [12000, 12000] .... 
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ---- result:
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_7/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_7_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_7_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;      
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      const char *config__nameOfDefaultVariable = "uniform";
      //! ---------------------------------------------
      //!
      //! Cofnigruations specific for this example:
      const char *config__nameOfDefaultVariable__2 = "binomial_p005";
      const uint config__matrixDims__minSize = 256;
      const uint config__matrixDims__maxSizeApprox = 12000;
      const uint config__matrixDims__stepMult = 4;
  //! ---------------------------------------------
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
#include "tut_inputFile_7.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    //! 
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }

}

static void __tut__case__8(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_8", array[2], strlen("tut_inputFile_8"))) ) {    
    //! Idea: a permtaution ot "tut_inputFile_4.c" where we elvauat ethe "data/FCPS/01FCPSdata/" data-set (oekseth, 06. feb. 2017).   
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_8/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_8_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_8_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
      //! ----------------------------------- 
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
#include "tut_inputFile_8.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
    }    //! 
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }

}

static void __tut__case__9(const int array_cnt, char **array) {
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

}

static void measure_kt_matrix_cmpCluster__tut__partA(const int array_cnt, char **array) {  
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  s_kt_longFilesHandler fileHandler__global__ = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/NULL, /*file_nameMeta=*/"metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
  s_kt_longFilesHandler *fileHandler__global = &fileHandler__global__;

  //!
  //!
  //! *****************************************************************************************************************************************************
  //!
  //!
  //!
  if(isTo_processAll || (0 == strncmp("tut_inputFile_1_small__gold", array[2], strlen("tut_inputFile_1_small__gold"))) ) {    
    //! Note: in this test-case we examplify how to evlauate/test different randomze-cases: we foucs on time-execution-perofmrnace. Of interest is ot idnetify the wrost-case exec-time and result-accuracy for challing data-serts. Therefre we (in this test-case) use/evlauate large/difficult data-sets (where size is descided using a manual/itnal investigaiton). To facilitate a simplfied evlauation of the clsuter-accuracy we 'merge'/concncatenate each of the well-defined data-sets with a data-set defined by a cofngiruation-variable named config__nameOfDefaultVariable. Our null-hypothesis is that our MINE-approach provide a lower/improved worst-case perofmrance (when compared to alternaitve appraoches). 
#define __M__calledInsideFunction
    const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
    //! ---------------------------------- 
    //! 
    { //! First a simplictic evlauation without the use of random fractions.
      s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
      obj_config.config__isToPrintOut__iterativeStatusMessage = true;
      obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
      //! ----
      obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_1/";
      obj_config.nameOf_resultFile__clusterMemberships = "tut_1_result_simMetrics.js";
      obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_1_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
      //!
      //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
      obj_config.config_arg_npass = 10000; 
      //!
      //! Result data:
      obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;      
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      //! ----------------------------------- 
      //! 
      //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid; //! which is 'here' ingored, ie, as [”elow] "clusterAlg" is suet to 'undef'.
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_undef; //! ie, Not perform clustering, ie, only conmptue wrt. the simlairty-metrics
      //! ----------------------------------- 
#include "tut_inputFile_1_small__gold.c" //! use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017)  
    }
#undef  __M__calledInsideFunction
    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }

  __tut__case__2(array_cnt, array);
  __tut__case__3(array_cnt, array);
  __tut__case__4(array_cnt, array);
  __tut__case__5(array_cnt, array);
  __tut__case__6(array_cnt, array);
}

static void measure_kt_matrix_cmpCluster__tut__partB(const int array_cnt, char **array) {  
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r
  __tut__case__7(array_cnt, array);
  __tut__case__8(array_cnt, array);
  //__tut__case__9(array_cnt, array);
}

//! the main funciton for logic-texting
void measure_kt_matrix_cmpCluster(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  //printf("at %s:%d\n", __FILE__, __LINE__);

  s_kt_longFilesHandler fileHandler__global__ = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/NULL, /*file_nameMeta=*/"metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
  s_kt_longFilesHandler *fileHandler__global = &fileHandler__global__;

  if(NULL != strstr(array[2], "tut_inputFile"))  {
    measure_kt_matrix_cmpCluster__tut__partA(array_cnt, array);
    measure_kt_matrix_cmpCluster__tut__partB(array_cnt, array);
  }
  
  //printf("cnt-args=%u, at %s:%d\n", array_cnt, __FILE__, __LINE__);

  // FIXME: use the metics in ["http://i11www.iti.uni-karlsruhe.de/extra/publications/ww-cco-06.pdf"] as 'basis' for [”elow].

  if(isTo_processAll || (0 == strncmp("simMetrics_x_synteticData", array[2], strlen("simMetrics_x_synteticData"))) ) {    
#define __Mi__objIsOfInterest(obj) {( obj.useInCondensedEvaluation )}
    assert(false); // FIXME: boefre starting with [”elow]: ... update our 'input-data-spec' with gold-standard-assumptions .... and 'as a first/intal result-evlauation' vlaidate that the MIEN-based appraoch outperforms 'the others' .... eg, by writing a new 'tut-example' 'for such'.
    assert(false); // FIXME[concept+code]: write a tut-example for 'one of our evlauated data-sets' .... validating that MINE results in a 'maizezed result-accuracy-perofmrance'.
    assert(false); // FIXME[concept]: ... make use of [”elow] ... draw inferences ... focus upon how k-means-alg differs wrt. different sim-metrics .... udpate our aritce
    assert(false); // FIXME[concept]: ... suggest different strategies for 'evlauaitng all thge psosbile sim-metircs when calling/computing k-means
    assert(false); // FIXME[concept]: ... 
    assert(false); // FIXME: evlauate [above] results ... and thereafter 'also' investigate for different algorithms such as [k-medoid, k-rank, SOM, HCA, ...].


    //! ---------------------------------------------
    //! 
    const e_hpLysis_clusterAlg_t config__clusterAlg = e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG;
    const uint config_arg_npass = 10000;
    const uint config_kMeans__cntIterations = 10; //! which is used (for tierative algorithms with random tests) to get the 'worst possible case' for [aobv€]: by 'defualt' we use the max-exec-time to 'find' the cases where convergence is difficult to idneityf, ie, as (one of our majro) conserns is the exueiction-time-difference between accurate and ina-ccurate algorithms    
    { //! Case: Silhutte && k-means:      
      const char *resultDir_path = "results/simMetrics_x_synteticData/"; 
      const char *resultDir_path_collections = "results/simMetrics_x_synteticData__coll/"; 
      const char *resultDir_path__noPreProcess = "results/simMetrics_x_synteticData__noPreProcess/"; 
      const char *nameOf_resultFile__clusterMemberships = "meta_kMeans_clusterMemberships.tsv";
      //! --------
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      const e_hpLysis_export_formatOf_t config_clusterSets__exportFormat = e_hpLysis_export_formatOf_clusterResults_vertex_toClusterIds; //! ie, write out [vertex-->clusterId] relationships.

      //const uint minNrows__forInterestingDataSet = UINT_MAX;
      //!
      //! Speify a gneierc funciton to ease future updates of our lgoics:
      
      //#define __Mi__objIsOfInterest(obj) {( obj.useInCondensedEvaluation && ( (minNrows__forInterestingDataSet == UINT_MAX) || (nrows > minNrows__forInterestingDataSet) ) )}
      
      //!
      //! Apply logics:
#include "measure_kt_matrix_cmpCluster__stub__clusterMatrix__main.c"
    }
    //! ---------------------------------------------
    { //! Case: Dunn && k-means:      
      const char *resultDir_path = "results/simMetrics_x_synteticData_1b/"; 
      const char *resultDir_path_collections = "results/simMetrics_x_synteticData__coll_1b/"; 
      const char *resultDir_path__noPreProcess = "results/simMetrics_x_synteticData__noPreProcess_1b/"; 
      const char *nameOf_resultFile__clusterMemberships = "meta_kMeans_clusterMemberships_1b.tsv";
      //! --------
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns;
      const e_hpLysis_export_formatOf_t config_clusterSets__exportFormat = e_hpLysis_export_formatOf_clusterResults_vertex_toClusterIds; //! ie, write out [vertex-->clusterId] relationships.
      const uint config_arg_npass = 10000;
      const uint config_kMeans__cntIterations = 10; //! which is used (for tierative algorithms with random tests) to get the 'worst possible case' for [aobv€]: by 'defualt' we use the max-exec-time to 'find' the cases where convergence is difficult to idneityf, ie, as (one of our majro) conserns is the exueiction-time-difference between accurate and ina-ccurate algorithms    
      //const uint minNrows__forInterestingDataSet = UINT_MAX;      
      //!
      //! Apply logics:
#include "measure_kt_matrix_cmpCluster__stub__clusterMatrix__main.c"
    }
    { //! Case: DavisBouldin && k-means:      
      const char *resultDir_path = "results/simMetrics_x_synteticData_1c/"; 
      const char *resultDir_path_collections = "results/simMetrics_x_synteticData__coll_1c/"; 
      const char *resultDir_path__noPreProcess = "results/simMetrics_x_synteticData__noPreProcess_1c/"; 
      const char *nameOf_resultFile__clusterMemberships = "meta_kMeans_clusterMemberships_1c.tsv";
      //! --------
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin;
      const e_hpLysis_export_formatOf_t config_clusterSets__exportFormat = e_hpLysis_export_formatOf_clusterResults_vertex_toClusterIds; //! ie, write out [vertex-->clusterId] relationships.
      const uint config_arg_npass = 10000;
      const uint config_kMeans__cntIterations = 10; //! which is used (for tierative algorithms with random tests) to get the 'worst possible case' for [aobv€]: by 'defualt' we use the max-exec-time to 'find' the cases where convergence is difficult to idneityf, ie, as (one of our majro) conserns is the exueiction-time-difference between accurate and ina-ccurate algorithms    
      //const uint minNrows__forInterestingDataSet = UINT_MAX;      
      //!
      //! Apply logics:
#include "measure_kt_matrix_cmpCluster__stub__clusterMatrix__main.c"
    }




    //! 
    //! ---------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }

 if(isTo_processAll || (0 == strncmp("tut_kMeans_hcaPreStep", array[2], strlen("tut_kMeans_hcaPreStep"))) ) {    
  //! ----------------------------------------------------------------------
  //! Default configurations:
  /* s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t(); */
  /* obj_config.config__isToPrintOut__iterativeStatusMessage = true; */
  /* obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons */
  /* //! ---- */
  /* obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/"; */
  /* obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js"; */
  /* obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix */
  /* //! */
  /* //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"): */
  /* obj_config.config_arg_npass = 10000; */
  /* //! */
  /* //! Result data: */
  /* obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS; */
  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  const char *config__nameOfDefaultVariable_1 = "binomial_p005";
  const char *config__nameOfDefaultVariable_2 = "linear-differentCoeff-b";
  const uint cnt_Calls_max = 6; const uint arg_npass = 1000;
  const char *result_file = "test_tut_kCluster.tsv";
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__medoid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  const e_hpLysis_clusterAlg clusterAlg__firstPass = e_hpLysis_clusterAlg_HCA_centroid;
  //! ----------------------------------------------------------------------
#define __M__calledInsideFunction
#include "tut_kMeans_hcaPreStep.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    assert(false); // FIXME: remove.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_hca", array[2], strlen("tut_hca"))) ) {    
#define __M__calledInsideFunction
#include "tut_hca.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_clust_kMeans__plusspluss_1", array[2], strlen("tut_clust_kMeans__plusspluss_1"))) ) {    
#define __M__calledInsideFunction
#include "tut_clust_kMeans__plusspluss_1.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_clust_kMeans__plusspluss_2", array[2], strlen("tut_clust_kMeans__plusspluss_2"))) ) {    
#define __M__calledInsideFunction
#include "tut_clust_kMeans__plusspluss_1.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_clust_mst_kruskal_2_kmeans", array[2], strlen("tut_clust_mst_kruskal_2_kmeans"))) ) {    
#define __M__calledInsideFunction
#include "tut_clust_mst_kruskal_2_kmeans.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_clust_mst_kruskal_3_ccm_Silhouette", array[2], strlen("tut_clust_mst_kruskal_3_ccm_Silhouette"))) ) {    
#define __M__calledInsideFunction
#include "tut_clust_mst_kruskal_3_ccm_Silhouette.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_clust_mst_kruskal_4_ccm_Dunn", array[2], strlen("tut_clust_mst_kruskal_4_ccm_Dunn"))) ) {    
#define __M__calledInsideFunction
#include "tut_clust_mst_kruskal_4_ccm_Dunn.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //! 
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("tut_clust_kMeans__miniBatch", array[2], strlen("tut_clust_kMeans__miniBatch"))) ) {
#define __M__calledInsideFunction
#include "tut_clust_kMeans__miniBatch.c" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering.
    //!
    //! ---------------------------------------------------------------------
#undef __M__calledInsideFunction
    cnt_Tests_evalauted++;
/*   } else if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) {     */
/* #define __M__calledInsideFunction */
/* #include "" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering. */
/*     //!  */
/*     //! --------------------------------------------------------------------- */
/* #undef __M__calledInsideFunction */
/*     cnt_Tests_evalauted++; */
/*   } else if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) {     */
/* #define __M__calledInsideFunction */
/* #include "" //! ie, examplfy the use of "Hiearchical Cluster Algorithms" (HCA) wrt. clustering. */
/*     //!  */
/*     //! --------------------------------------------------------------------- */
/* #undef __M__calledInsideFunction */
/*     cnt_Tests_evalauted++; */
  }

  //!
  //! 
  if(isTo_processAll || (0 == strncmp("sampleTests--basicAlgorithmCalls", array[2], strlen("sampleTests--basicAlgorithmCalls"))) ) {    
    //! ./x_measures kt_matrix_cmpCluster sampleTests--basicAlgorithmCalls
#define __M__calledInsideFunction
    //#include "tut_hca.c" //! ie,
    
#include "tut_inputFile.c" //! which examplify the use of a 'advanced inptu-file specificaiton' through the "s_kt_matrix_fileReadTuning_t" into hpLysis-clustering.  (oekseth, 06. feb. 2017).   
#include "tut_som.c" //! ie, examplfy the use of "Self Orgnaising Maps" (SOM) wrt. clustering.
#include "tut_disjointKMeansCluster.c"
#include "tut_buildSimilarityMatrix__testSparse.c"
    assert(false); // FIXME: ... update and validate our CCMs based on "Scikit-learn" ("http://scikit-learn.org/stable/modules/clustering.html")
    assert(false); // FIXME: wrt. our "kt_clusterAlg_fixed__alg__miniBatch.c" improve current randomzied-improvmenet-scaffolds, fianlize/imsplify implemnetition, ... and thereafter update our "kt_clusterAlg_fixed", "kt_api" and "hpLysis_api"
    assert(false); // FIXME: use "valgrind" to find both memrory-errors and memory-leaks ... and then fixe 'these'
    // ---- 
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'different categoirs' ... inside-clustering
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'different categoirs' ... inside-clustering && pre-clustering.
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'different categoirs' ... inside-clustering && pre-clustering
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'different categoirs' ... inside-clustering && pre-clustering && 'all the different cenotrid-selection-strateies in our "k-means-fixed' procedure, eg, [median, mean, medoid]
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'complete set' ... inside-clustering
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'complete set' ... inside-clustering && pre-clustering.
    assert(false); // FIXME[new-example]: ... "k-means" for mulitple simliarity-metrics: ... 'complete set' ... inside-clustering && pre-clustering && 'all the different cenotrid-selection-strateies in our "k-means-fixed' procedure, eg, [median, mean, medoid]
    // ----    
    assert(false); // FIXME[new-example]: ...
    assert(false); // FIXME[new-example]: ...
    assert(false); // FIXME[new-example]: ...
    assert(false); // FIXME[new-example]: ...
    assert(false); // FIXME[new-example]: ... 
    assert(false); // FIXME: remove
#include "tut_disjointCluster.c" //! ie, where disjotinf-clsuters are coptued based on our vlaeu-thresholds.
#include "tut_disjointCluster__pointless.c" //! ie, examplify the case where 'all veritces are in the same clsuter by input-sepc', ie, a poitnless call
#include "tut_kCluster.c" //! ie, examplify the 
#include "tut_mine.c" //! ie, test wrt. a simplcitic MINE-strategy
#include "tut_buildSimilarityMatrix.c" //! ie, wrt. testign a large number of simliarty-metrics.
#undef __M__calledInsideFunction
  }
  //!
  //!
  //! *****************************************************************************************************************************************************
  //!
  //!
  //!
  const e_kt_matrix__exportFormats___extremeToPrint_t typeOf_extractToPrint = e_kt_matrix__exportFormats___extremeToPrint_STD_max;
  {
    /* if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_", array[2], strlen("testHardCodedDistributions--CCM_"))) ) { */
    /*   //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.     */
    /*   const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 4;      */
    /*   const char *resultDir_path = "results/testHardCodedDistributions__goldxgold/"; */
    /*   const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner/";     */
    /*   uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
    /*   uint cnt_datasetsGlobal__current = 0; */
    /*   { //! 'Empty' the current oflder: */
    /* 	{ char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  system(str); } */
    /* 	{ char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
    /*   } */

    /*   const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10; */
    /*   const uint clusterCnt_total_min = 2; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4; */
    /*   // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy */
    /*   uint dataBlock_currentPos__casesOf_nrows = 0; */
    /*   uint dataBlock_currentPos__casesOf_dataSets = 0; */
    /*   const t_float score_weak = 100; const t_float score_strong = 1;  */
    /*   /\* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  *\/ */
    /*   /\* const char *resultDir_path = ""; *\/ */
    /*   /\* const uint nrows_total = 80; const uint cnt_clusters_total = 20; *\/ */
    /*   /\* const t_float score_weak = 100; const t_float score_strong = 1;  *\/ */
    /*   //! ---------------------------------------------------------------------------------- */
    /*   //!  */
    /*   //! Intiate result-object: */
    /*   const uint empty_0 = 0; */
    /*   const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal; */
    /*   uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0); */
    /*   s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/\*cnt_datasets=*\/obj_size); */
    /*   //! ---------------------------------------------------------------------------------- */
    /*   //! */
    /*   //! ----------------------------------------------------------------------------- */
    /*   assert(false); // FIXME: add soemthing */

    /*   //! ----------------------------------------------------------------------------- */
    /*   //! Wirte out a 'unified matrix': */
    /*   { //! */
    /* 	//! Write out:  */
    /* 	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM"); */
    /* 	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global"); */
    /* 	//! Note: in [”elow] we obht exprots and de-allocates: */
    /* 	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef); */
    /* 	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL; */
    /*   } */
    /*   //! ----------------------------------------------------------------------------- */
    /*   //! Update count: */
    /*   cnt_Tests_evalauted++; */
    /* } */
    //!
    //! *********************************************************************************************************************************
    //!
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_a1", array[2], strlen("testHardCodedDistributions--CCM_a1"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      printf("at %s:%d\n", __FILE__, __LINE__);
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold_a_1/";
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner_a_1/";    
      const bool isTo_incrementClusterCountInInner = true;
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 2; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 100; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      { const char *stringOf_measurement_base = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_1__weight_decreasedScoreForSmallerClusters_reverseVertices";
	printf("\n\n At %s:%d\n", __FILE__, __LINE__);
	uint cntClusters_curr = 1; 
	const uint total_seperateBuidls = 6;
	//= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init__in() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__init__out() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__buildSample__in(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
#define __Mi__buildSample__out(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
	//!
	//! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM__wrapper.c"
      }
      //assert(false); // FIXME: add soemthing

      //! -----------------------------------------------------------------------------
      //! Wirte out a 'unified matrix':
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM");
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global");
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL;
      }
      //! -----------------------------------------------------------------------------
      //! Update count:
      cnt_Tests_evalauted++;
    }
    //!
    //! *********************************************************************************************************************************
    //!
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_a2", array[2], strlen("testHardCodedDistributions--CCM_a2"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      printf("at %s:%d\n", __FILE__, __LINE__);
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold_a_2/";
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner_a_2/";    
      const bool isTo_incrementClusterCountInInner = true;
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  MF__system(str); } */
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 2; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 100; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      { const char *stringOf_measurement_base = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_1__weight_decreasedScoreForSmallerClusters_reverseVertices";
	printf("\n\n At %s:%d\n", __FILE__, __LINE__);
	uint cntClusters_curr = 1; 
	const uint total_seperateBuidls = 6;
	//= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init__in() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__init__out() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__buildSample__in(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
#define __Mi__buildSample__out(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
	//!
	//! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM__wrapper.c"
      }
      //assert(false); // FIXME: add soemthing

      //! -----------------------------------------------------------------------------
      //! Wirte out a 'unified matrix':
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM");
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global");
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL;
      }
      //! -----------------------------------------------------------------------------
      //! Update count:
      cnt_Tests_evalauted++;
    }
    //!
    //! *********************************************************************************************************************************
    //!
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_b", array[2], strlen("testHardCodedDistributions--CCM_b"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const bool isTo_incrementClusterCountInInner = false;
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold_b/";
      // const bool isTo_incrementClusterCountInInner = true;
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner_b/";    
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  MF__system(str); } */
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 2; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 100; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      //assert(false); // FIXME: add soemthing
      { const char *stringOf_measurement_base = "type_squared.nrows_fixed.kCount_dynamic_randomVertices";            
	assert(cnt_datasetsGlobal__current < cnt_datasetsGlobal); cnt_datasetsGlobal__current++; //! ie, a cosnsitency-check.
	fprintf(stderr, "\n\n Build for \"%s\", at %s:%d\n", stringOf_measurement_base, __FILE__, __LINE__);
	//= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init__in() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__init__out() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample__in(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local_in, cnt_clusterStartOffset);})
#define __Mi__buildSample__out(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
	//!
	//! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM__wrapper.c"
      }
      //! -----------------------------------------------------------------------------
      //! Wirte out a 'unified matrix':
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM");
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global");
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL;
      }
      //! -----------------------------------------------------------------------------
      //! Update count:
      cnt_Tests_evalauted++;
    }
    //!
    //! *********************************************************************************************************************************
    //!
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_x", array[2], strlen("testHardCodedDistributions--CCM_x"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold/";
      const bool isTo_incrementClusterCountInInner = true;
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner/";    
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  MF__system(str); } */
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 2; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 100; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      assert(false); // FIXME: add soemthing

      //! -----------------------------------------------------------------------------
      //! Wirte out a 'unified matrix':
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM");
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global");
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL;
      }
      //! -----------------------------------------------------------------------------
      //! Update count:
      cnt_Tests_evalauted++;
    }
    //!
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_c", array[2], strlen("testHardCodedDistributions--CCM_c"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold_c/";
      const bool isTo_incrementClusterCountInInner = true;
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner_c/";    
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  MF__system(str); } */
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 2; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 20; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      { const char *stringOf_measurement_base = "type_squared.nrows_fixed.kCount_dynamic_linearVertices";            
	assert(cnt_datasetsGlobal__current < cnt_datasetsGlobal); cnt_datasetsGlobal__current++; //! ie, a cosnsitency-check.
	fprintf(stderr, "\n\n Build for \"%s\", at %s:%d\n", stringOf_measurement_base, __FILE__, __LINE__);
	//= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init__in() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__init__out() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample__in(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local_in, cnt_clusterStartOffset);})
#define __Mi__buildSample__out(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
	//!
	//! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM__wrapper.c"
      }
      

      //! -----------------------------------------------------------------------------
      //! Wirte out a 'unified matrix':
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM");
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global");
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL;
      }
      //! -----------------------------------------------------------------------------
      //! Update count:
      cnt_Tests_evalauted++;
    }
    //!
    //! *********************************************************************************************************************************
    //!
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_d", array[2], strlen("testHardCodedDistributions--CCM_d"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold_d/";
      const bool isTo_incrementClusterCountInInner = true;
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner_d/";    
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  MF__system(str); } */
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 40; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 500; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      { const char *stringOf_measurement_base = "type_squared.nrows_fixed.kCount_dynamic_linearVertices";            
	assert(cnt_datasetsGlobal__current < cnt_datasetsGlobal); cnt_datasetsGlobal__current++; //! ie, a cosnsitency-check.
	fprintf(stderr, "\n\n Build for \"%s\", at %s:%d\n", stringOf_measurement_base, __FILE__, __LINE__);
	//= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init__in() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__init__out() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample__in(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local_in, cnt_clusterStartOffset);})
#define __Mi__buildSample__out(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
	//!
	//! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM__wrapper.c"
      }
    //!
    //! *********************************************************************************************************************************
    //!
    }
    if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--CCM_f", array[2], strlen("testHardCodedDistributions--CCM_f"))) ) {
      //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.    
      const uint cnt_datasetsGlobal = 1;      const uint cnt_cases = 10;     
      const char *resultDir_path = "results/testHardCodedDistributions__goldxgold_f/";
      const bool isTo_incrementClusterCountInInner = false;
      const char *resultDir_path__inner = "results/testHardCodedDistributions__goldxgold__inner_f/";
      fprintf(stderr, "resultDir_path__inner=\"%s\", at %s:%d\n", resultDir_path__inner, __FILE__, __LINE__);
      uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
      uint cnt_datasetsGlobal__current = 0;
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path);  MF__system(str); } */
	/* { char str[2000]; sprintf(str, "rm -f %s*", resultDir_path__inner);  MF__system(str); } */
      }

      const uint nrows_total_min = 80; const uint nrows_steps_cnt = 2; const uint nrows_steps_multEach = 10;
      const uint clusterCnt_total_min = 40; const uint clusterCnt_steps_cnt = 3; //uint clusterCnt_steps_multEach =  4;
      // assert(cnt_cases < clusterCnt_steps_cnt); //! ie, an asusmption used when budiling 'geneirc' clsute-rd-ata-comaprsion-strategy
      uint dataBlock_currentPos__casesOf_nrows = 0;
      uint dataBlock_currentPos__casesOf_dataSets = 0;
      const t_float score_weak = 500; const t_float score_strong = 1; 
      /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
      /* const char *resultDir_path = ""; */
      /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
      /* const t_float score_weak = 100; const t_float score_strong = 1;  */
      //! ----------------------------------------------------------------------------------
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt*cnt_datasetsGlobal;
      uint *obj_resultCollection_goldCaseClustersGlobal__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersGlobal = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //! ----------------------------------------------------------------------------------
      //!
      //! -----------------------------------------------------------------------------
      { const char *stringOf_measurement_base = "type_squared.nrows_fixed.kCount_dynamic_linearVertices";            
	assert(cnt_datasetsGlobal__current < cnt_datasetsGlobal); cnt_datasetsGlobal__current++; //! ie, a cosnsitency-check.
	fprintf(stderr, "\n\n Build for \"%s\", at %s:%d\n", stringOf_measurement_base, __FILE__, __LINE__);
	//= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init__in() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__init__out() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample__in(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local_in, cnt_clusterStartOffset);})
#define __Mi__buildSample__out(obj_gold, cnt_clusters_local, cnt_clusterStartOffset) ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
	//!
	//! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM__wrapper.c"
      }
      //! -----------------------------------------------------------------------------

      //! ------------------------------------- 

      printf("--------at %s:%d\n", __FILE__, __LINE__);
      //!
      //!
      //!
      //! *****************************************************************************
      //! -----------------------------------------------------------------------------
      //! Wirte out a 'unified matrix':
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", "testHardCodedDistributions__CCM");
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%s%s__%s", resultDir_path, stringOf_measurement, "global");
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersGlobal, path_w_dir, NULL, NULL, e_kt_matrix__exportFormats___extremeToPrint_undef);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersGlobal__currentPos);  obj_resultCollection_goldCaseClustersGlobal__currentPos = NULL;
      }
      //! -----------------------------------------------------------------------------
      //! Update count:
      cnt_Tests_evalauted++;
    }
    //! *****************************************************************************************************************************************************
    //! *****************************************************************************************************************************************************
    //! *****************************************************************************************************************************************************    
  }
  //!
  //!
  //! *****************************************************************************************************************************************************
  //!
  //!
  //!
  if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--testSingleRandom", array[2], strlen("testHardCodedDistributions--testSingleRandom"))) ) {
    const char *stringOf_measurement = "test_random.tsv";
    printf("\n\n At %s:%d\n", __FILE__, __LINE__);
    uint cntClusters_curr = 1; 
    const uint total_seperateBuidls = 6;
    //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
    //#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
    //!
    //! Apply logics and write out:
    const uint nrows_total = 10;
    const t_float score_weak = 100; const t_float score_strong = 1; 
    s_hp_clusterShapes_t obj_gold = __Mi__init();
    //! Apply rotuine:
    const uint cnt_clusterStartOffset = 0;
    const uint cnt_clusters_local = 2;
    // printf("#\t cnt_clusters_local=%u, at %s:%d\n", cnt_clusters_local, __FILE__, __LINE__);
    assert(cnt_clusters_local <= nrows_total);
    assert(cnt_clusters_local >= 1);
    const bool is_ok = __Mi__buildSample();
    assert(is_ok);
    //	const uint case_id = 0;
    // tagLocalIndex_0.tagNrows_10.tagNclusters_3.tagExperiment_all.tagSample_clusterCmpEvaluation_case2.localMetricData_.subCategory_dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv
    //if(case_id == 0) 
    
    //! Then write out the matrix:
    const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&(obj_gold.matrix), stringOf_measurement, NULL);
    assert(is_ok_ex);
    //!
    //! De-allocate:
    free__s_hp_clusterShapes_t(&obj_gold);
    //#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
  }
  if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--testMultSingleRandom", array[2], strlen("testHardCodedDistributions--testMultSingleRandom"))) ) {
    const uint cnt_cases = 2; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
    const char *resultDir_path = "";
    const uint nrows_total = 10; const uint cnt_clusters_total = 4;
    const t_float score_weak = 100; const t_float score_strong = 1; 
    /* const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity.  */
    /* const char *resultDir_path = ""; */
    /* const uint nrows_total = 80; const uint cnt_clusters_total = 20; */
    /* const t_float score_weak = 100; const t_float score_strong = 1;  */
    { const char *stringOf_measurement = "test_randomMultiple";            
      uint cntClusters_curr = 2;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    printf("--------at %s:%d\n", __FILE__, __LINE__);
    //! -----------------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--testMultSingleReverse", array[2], strlen("testHardCodedDistributions--testMultSingleReverse"))) ) {
    const uint cnt_cases = 2; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
    const char *resultDir_path = "";
    const uint nrows_total = 10; const uint cnt_clusters_total = 4;
    const t_float score_weak = 100; const t_float score_strong = 1; 
    { const char *stringOf_measurement = "test_randomMultiple";            
      uint cntClusters_curr = 2;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    printf("--------at %s:%d\n", __FILE__, __LINE__);
    //! -----------------------------------------------------------------------------
    cnt_Tests_evalauted++;
  }
  //!
  //!
  //! *****************************************************************************************************************************************************
  //!
  //!
  //!
  if(isTo_processAll || (0 == strncmp("testHardCodedDistributions--dataUsedAsInput", array[2], strlen("testHardCodedDistributions--dataUsedAsInput"))) ) {
    //! Note: makes use of API in our "hp_clusterShapes.h" to idneityf and descibe different distirbutions.
    const uint cnt_cases = 10; uint dataBlock_currentPos = 0; //! wher ethe latter is used to egive each image a unqiue ideitnfity. 
    const char *resultDir_path = "results/testHardCodedDistributions__dataUsedAsInput/";
      const bool isTo_incrementClusterCountInInner = true;
    const uint nrows_total = 80; const uint cnt_clusters_total = 20;
    const t_float score_weak = 100; const t_float score_strong = 1; 
    //! ---------------------------------------------------------------------
    //! Start: Reversed order and Random order:
    //! ---------------------------------------------------------------------    
    {  const char *stringOf_measurement = "type_squared.nrows_fixed.kCount_dynamic_randomVertices";            
      uint cntClusters_curr = 2;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_1__weight_decreasedScoreForSmallerClusters_randomVertices";
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 6;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_squared.nrows_fixed.kCount_dynamic_reverseVertices";            
      uint cntClusters_curr = 2;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ------------------------------------- 
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_1__weight_decreasedScoreForSmallerClusters_reverseVertices";
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 6;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    //! Start: Mixed:
    //! ---------------------------------------------------------------------
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_1__weight_decreasedScoreForSmallerClusters";
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 6;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //assert(false); // FIXME: remove
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_2_linearIncrease_1";    
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 2;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_1";    
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 6;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/1);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_2_linearIncrease_6";    
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 2;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/6);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_mixed_linear.nrows_fixed.kCount_dynamic.seperateBuilds_6_linearIncrease_6";    
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      const uint total_seperateBuidls = 6;
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_gold, nrows_total, total_seperateBuidls, /*cnt_clusters_fixedToAdd=*/1, /*type=*/e_hp_clusterShapes_clusterAssignType_linearIncrease, cnt_clusterStartOffset, /*isTo_startOnZero=*/false, /*isTo_incrementDisjoint=*/true, /*vertexCount__constantToAdd=*/6);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    //! Start: Seperate:
    //! ---------------------------------------------------------------------
    //! ------------------------------------- 
    { const char *stringOf_measurement = "type_squared.nrows_fixed.kCount_dynamic";            
      uint cntClusters_curr 
	= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__disjointSquares__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_linear.nrows_fixed.kCount_dynamic";            
      uint cntClusters_curr = 1;
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset, /*vertexCount__constantToAdd=*/0);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    //assert(false); // FIXME: remove
    //! ---------------------------------------------------------------------
    { const char *stringOf_measurement = "type_linear.nrows_fixed.kCount_dynamic.vertexCountToAdd_5";    
      printf("\n\n At %s:%d\n", __FILE__, __LINE__);
      uint cntClusters_curr = 1; 
      //= (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);        
#define __Mi__init() ({init_andReturn__s_hp_clusterShapes_t(nrows_total, score_weak, score_strong, /*vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);})
#define __Mi__buildSample() ({buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_gold, nrows_total, cnt_clusters_local, cnt_clusterStartOffset, /*vertexCount__constantToAdd=*/5);})
      //!
      //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c"
#undef __Mi__init
#undef __Mi__buildSample
    }
    fprintf(stderr, "(Completed)\t Build completed, at %s:%d\n", __FILE__, __LINE__);
    //! ---------------------------------------
    //! Update count:
    cnt_Tests_evalauted++;
  }
  //!
  //!
  //! *****************************************************************************************************************************************************
  //!
  //!
  //!
  if(isTo_processAll || (0 == strncmp("metric--hpLysisMetrics--Cityblock", array[2], strlen("metric--hpLysisMetrics---Cityblock"))) ) {
    const uint nrows = 4;     const uint ncols = 5; 
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	obj_matrixInput.matrix[row_id][col_id] = row_id;
      }
    }

    //!
    //! Export:
    const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&obj_matrixInput, "city_testData_beforeCorr.tsv", NULL);
    assert(is_ok_ex);

    //!
    //! Correlate:
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    hp_config.stringOfResultPrefix__exportCorr__prior = "city_testData.tsv";
    //! Set the corrleation-metric:
    s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_citbyblock);
    printf("#!!\t typeOf_correlationPreStep='%u', at %s:%d\n", corrMetric_prior.typeOf_correlationPreStep, __FILE__, __LINE__);
    hp_config.config.corrMetric_prior = corrMetric_prior;
    // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Correlate and export:
    const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
    assert(is_ok);
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);	    
    free__s_kt_matrix(&obj_matrixInput);


    assert(false); // FIXME: remove
    //! ---------------------------------------
    //! Update count:
    cnt_Tests_evalauted++;
  }

  //printf("string=\"%s\", at %s:%d\n", array[2], __FILE__, __LINE__);
  if(isTo_processAll || (0 == strncmp("metric--hpLysisMetrics---Pearson_VS_Spearman", array[2], strlen("metric--hpLysisMetrics---Pearson_VS_Spearman"))) ) {
    const uint nrows = 4;     const uint ncols = 5; 
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	if(false) {
	  obj_matrixInput.matrix[row_id][col_id] = row_id;
	} else {
	  //struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start);
	  obj_matrixInput.matrix[row_id][col_id] = rand();
	}
      }
    }

    //!
    //! Buidl a subset:
    const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&obj_matrixInput, "spear_testData_beforeCorr.tsv", NULL);
    assert(is_ok_ex);

    { //!
      //! Correlate:
      s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
      hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
      hp_config.stringOfResultPrefix__exportCorr__prior = "spear_pearson_testData.tsv";
      //! Set the corrleation-metric:
      s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson);
      printf("#!!\t typeOf_correlationPreStep='%u', at %s:%d\n", corrMetric_prior.typeOf_correlationPreStep, __FILE__, __LINE__);
      hp_config.config.corrMetric_prior = corrMetric_prior;
      // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
      fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //!
      //! Correlate and export:
      const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
      assert(is_ok);
      //! De-allocates the "s_hpLysis_api_t" object.
      free__s_hpLysis_api_t(&hp_config);	    
    }
    { //!
      //! Correlate:
      s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
      hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
      hp_config.stringOfResultPrefix__exportCorr__prior = "spear_testData.tsv";
      //! Set the corrleation-metric:
      s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman);
      printf("#!!\t typeOf_correlationPreStep='%u', at %s:%d\n", corrMetric_prior.typeOf_correlationPreStep, __FILE__, __LINE__);
      hp_config.config.corrMetric_prior = corrMetric_prior;
      // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
      fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //!
      //! Correlate and export:
      const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
      assert(is_ok);
      //! De-allocates the "s_hpLysis_api_t" object.
      free__s_hpLysis_api_t(&hp_config);	    
    }
    //!
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_kt_matrix(&obj_matrixInput);

    assert(false); // FIXME: remove
    //! ---------------------------------------
    //! Update count:
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("metric--hpLysisMetrics--Euclid", array[2], strlen("metric--hpLysisMetrics---Euclid"))) ) {
    const uint nrows = 20;     const uint ncols = 20; 
    //const uint nrows = 13;     const uint ncols = 3; 
    //const uint nrows = 20;     const uint ncols = 3; 
    //const uint nrows = 20;     const uint ncols = 5; 
    //const uint nrows = 9;     const uint ncols = 5; 
    //const uint nrows = 4;     const uint ncols = 5; 
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    if(true) {
      //!
      { //! Load the data-set:
	const char *stringOf_sampleData_type_realLife = NULL;
	const char *stringOf_sampleData_type = "lines-circle";
	  //"flat";
	const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20;
	s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
	const bool isTo_transposeMatrix = false;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
if(obj_baseToInclude.nrows) {
  for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
    //assert(obj_baseToInclude.matrix[row_id]);
    assert(row_id < obj_baseToInclude.nrows);
    assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
    const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
    //if(row) 
    //{
    assert(row);
  }
 }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"

	/* // FIXME: try drop [”elow] block */
	/* {  */
	/*   for(uint row_id = 0; row_id < obj_matrixInput.nrows; row_id++) { */
	/*     for(uint col_id = 0; col_id < obj_matrixInput.ncols; col_id++) { */
	/*       obj_matrixInput.matrix[row_id][col_id] = row_id; */
	/*     } */
	/*   } */
	/*   printf("weight=%p, at %s:%d\n", obj_matrixInput.weight, __FILE__, __LINE__); */
	/*   if(obj_matrixInput.weight) { */
	/*     for(uint col_id = 0; col_id < obj_matrixInput.ncols; col_id++) { */
	/*       printf("weight[%u]=%f, at %s:%d\n", col_id, obj_matrixInput.weight[col_id], __FILE__, __LINE__); */
	/*     } */
	/*   } */
	/*   //obj_matrixInput.weight = NULL; //! FIXME: remove! */
	/*   /\* const uint row_id = 0; *\/ */
	/*   /\* for(uint col_id = 0; col_id < obj_matrixInput.ncols; col_id++) { *\/ */
	/*   /\*   obj_matrixInput.matrix[row_id][col_id] = row_id; *\/ */
	/*   /\* } *\/ */
	/* } */
      }
    } else { //! then we build using Unix-time, thereby demostnrategin the use of user-explcit valeus (rather than file-loading):
      init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  obj_matrixInput.matrix[row_id][col_id] = row_id;
	}
      }
    }
    //!
    //! Buidl a subset:
    const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&obj_matrixInput, "euc_testData_beforeCorr.tsv", NULL);
    assert(is_ok_ex);

    //!
    //! Correlate:
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    hp_config.stringOfResultPrefix__exportCorr__prior = "euc_testData.tsv";
    //! Set the corrleation-metric:
    s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid);
    if(false) {
      corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman);
    }
    hp_config.config.corrMetric_prior = corrMetric_prior;
    // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Correlate and export:
    const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
    assert(is_ok);
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);	    
    free__s_kt_matrix(&obj_matrixInput);

    //! ---------------------------------------
    //! Update count:
    cnt_Tests_evalauted++;
  }


  if(isTo_processAll || (0 == strncmp("testStub--hpLysis--kCluster", array[2], strlen("testStub--hpLysis--kCluster"))) ) {
    char string_local__corr_prior[2000]; sprintf(string_local__corr_prior, "%s.subset_naiveMetrics.correlationBeforeClust.transposed.tsv", "tmp");
    char string_local__corr[2000]; sprintf(string_local__corr, "%s.subset_naiveMetrics.afterClust.transposed.tsv", "tmp");
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(string_local__corr, fileHandler__global);
    hp_config.config.clusterConfig.isTo_transposeMatrix = true;
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
    //!
    //! Cluster:
    const uint nclusters = 5; 
    const uint nrows = 4; const uint ncols = 4;
    s_kt_matrix_t tmp_matrix; init__s_kt_matrix(&tmp_matrix, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
    const bool is_ok = cluster__hpLysis_api(&hp_config, e_hpLysis_clusterAlg_kCluster__AVG, &tmp_matrix, /*k-cluster=*/nclusters, /*npass=*/1000);
    assert(is_ok);
    //! 
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);
    free__s_kt_matrix(&tmp_matrix);

    //! ---------------------------------------
    //! Update count:
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("metric--Rand", array[2], strlen("metric--Rand"))) ) {

    /* { */
    /*   const uint list_size = 3; */
    /*   t_float list[list_size] = {2, 3, 1}; */
    /*   t_float *list_rank =get_rank__correlation_rank(list_size, list); */
    /*   assert(list_rank); */
    /*   for(uint i = 0; i < list_size; i++) {printf("rank[%u]=%f, at %s:%d\n", i, list_rank[i], __FILE__, __LINE__);} */
    /* } */

    //! -----------------------------------------------------
    //! -----------------------------------------------------
    //!             Generic cofniguraitons:
    const uint cnt_vertices = cnt_defaultScoreTypes_cntVertices_case1; const uint cnt_clusters = cnt_defaultScoreTypes_cntClusters1; const uint cnt_data = cnt_defaultScoreTypes_case1;
    const uint cnt_matricesToEvaluate = 3;
    //! Compare different partitions for the data:
#define arrOf_2d_data mapOf_scoreDistributions_case1
    //! -----------------------------------------------------
    //! -----------------------------------------------------

    //! ----------------------------------------------

    //! ----------------------------------------------
    { //! Compute for the rand-index-score:
      s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
      //! Build a co-occurence-matrix of entites between the two clsuter-sets
      //! Note: the result is stored in the "self->matrixOf_coOccurence" parameter and may be 'used' in/from our "computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(..)" function


      //! -------------------------------------------------------
      //! 
      //! Iterate:
      t_float global_min = T_FLOAT_MAX; t_float global_max = T_FLOAT_MIN_ABS; t_float global_sum = 0; uint cnt_evaluated = 0;
      for(uint data_1 = 0; data_1 < cnt_data; data_1++) {
	//!
	//! Iterate, comparing 'this' to the other/laterntive data-sets:
	t_float local_min = T_FLOAT_MAX; t_float local_max = T_FLOAT_MIN_ABS; t_float local_sum = 0; uint cnt_evaluated_local = 0;
	for(uint data_2 = 0; data_2 < cnt_data; data_2++) {
	  const uint cnt_clusters_1 = cnt_clusters;
	  const uint cnt_clusters_2 = cnt_clusters;
	  uint *mapOf_vertexToCentroid1 = arrOf_2d_data[data_1];
	  uint *mapOf_vertexToCentroid2 = arrOf_2d_data[data_2];
	  /* //      const uint cnt_clusters_2 = 3; */
	  /* const uint cnt_vertices = 3; */
	  /* uint mapOf_vertexToCentroid1[cnt_vertices] = {0, 0, 2}; */
	  /* uint mapOf_vertexToCentroid2[cnt_vertices] = {0, 0, 2}; */
	  //      uint mapOf_vertexToCentroid2[cnt_vertices] = {0, 1, 2};


	  const uint max_cntClusters = macro_max(cnt_clusters_1, cnt_clusters_2);
	  t_float **matrixOf_coOccurence = NULL;
	  s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	  build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence, matrixOf_coOccurence, config_intraCluster);
	  
	  
	  s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
	  e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = e_kt_matrix_cmpCluster_metric_randsIndex;
	  computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
	  const t_float scalar_score = metric_result.score_1;
	  //! ---- 
	  metric_clusterCmp = e_kt_matrix_cmpCluster_metric_randsIndex_alt2;
	  computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
	  const t_float scalar_score_Rand_altImpl = metric_result.score_1;
	  //! ---- 
	  metric_clusterCmp = e_kt_matrix_cmpCluster_metric_randsAdjustedIndex;
	  computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
	  const t_float scalar_score_ARI = metric_result.score_1;

	  printf("-\t # metrics-at-data[%u][%u], at %s:%d\n", data_1, data_2, __FILE__, __LINE__);
	  //! ----------------------------------------
	  //!
	  //! Iterate through the different non-matrix-based measures:
	  for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) {
	    const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id;
	    if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
	    //! ---- 
	    computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
	    const t_float scalar_score_altTest = metric_result.score_1;
	    //! ---- 
	  
	    printf("\t(score)\tdata[%u][%u]\t %f\t, %f\t, %f, %f\t = %s\tat %s:%d\n", data_1, data_2, scalar_score, scalar_score_Rand_altImpl, scalar_score_ARI, scalar_score_altTest, getStringOf__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp), __FILE__, __LINE__);
	  }

	  
	  //!
	  if(scalar_score != 0) {
	    //! Then update the 'overall' proerpteis of 'this':
	    local_sum += scalar_score;
	    local_min = macro_min(local_min, scalar_score);
	    local_max = macro_max(local_max, scalar_score);
	    cnt_evaluated++; 	    cnt_evaluated_local++;
	  }
	  //!
	  //! De-allocate:
	  free__s_kt_matrix_cmpCluster(&obj_cmp);
	}
	//!
	if(local_sum != 0) {
	  //! Then update the 'overall' proerpteis of 'this':
	  global_sum += local_sum;
	  global_min = macro_min(global_min, local_min);
	  global_max = macro_max(global_max, local_max);
	  printf("---\nReport[local::data_id=%u]: values in range=[%f, %f] with avg=%f, at %s:%d\n", data_1, local_min, local_max, local_sum / (t_float)cnt_evaluated_local, __FILE__, __LINE__);
	}
      }
      printf("\n-----------\n Report[global]: values in range=[%f, %f] with avg=%f, at %s:%d\n", global_min, global_max, global_sum / (t_float)cnt_evaluated, __FILE__, __LINE__);



    }
#undef arrOf_2d_data
    //! ----
    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--RandAdjusted", array[2], strlen("metric--RandAdjusted"))) ) {

    assert(false); // FIXME[data-evaluation]: for all of 'our' cluster-comparison-metrics write down a set 'of expecatins', ie, based on the 'equations themself'.
    assert(false); // FIXME[data-evaluation]: comptue for all matrices ... export the result to an image ... 
    assert(false); // FIXME[data-evaluation]: all-metrics: (a) constyruct seperate images 'for each', (b) write a Perl-script where we build an 'image-montage' ("http://www.imagemagick.org/Usage/montage/"), (c) try to hypthese/suggest inferences/(patterns 'which may be drawn'. <-- try to idneitfy metrics 'which seems to be wrongly impelmented' <-- consider to use 'this result' (among others) as an argument for the improtance of a 'formal' algorithm-appraoch for metric-comparison.
    //! --------------------
    assert(false); // FIXME[data-evaluation]: try to idneitfy the 'min-signfifcant-trhesholds' wrt. differnet ["Rand index", "Adjusted Rand index"] scores .... and use the latter results to assess/evlauat if 'the differences in cluster-rpediction-scores' described in ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0508-1" w/title "Classification of bioinformatics workflows using weighted versions of partitioning and hierarchical clustering algorithms" <-- evalaute/describe their 'compariuson-appraoch' (which use a large number of different equations)] ... ie, to assess/identify the 'comparison-cases' which are 'ooutside the random-signficant-trehsholds' <--- todo: how may such a 'trehshold' be deifned (ie, is it possible to relate the differences in clsuter-comparison-metic--score-predicitons to variability in k-means-clsutering-result-staiblity?
    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--ChiSquared", array[2], strlen("metric--ChiSquared"))) ) {


    assert(false); // FIXME[metric]: esnure that we have implmeneted all metrics listed at ("https://clusteval.sdu.dk/1/clustering_quality_measures/18") ... and scaffold how 'the reulsts produced at the latter web-page may be improved through our approach' <-- todo[anlasysis]: try to 'create' comparison-sets which manages to 'present cases where we need mulple/all cluster-compairson-metrics iot. properly describe a data-set'.
    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--FowlkesMallows", array[2], strlen("metric--FowlkesMallows"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--Mirkin", array[2], strlen("metric--Mirkin"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--Wallace", array[2], strlen("metric--Wallace"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--Silhouette-Index", array[2], strlen("metric--Silhouette-Index"))) ) {
    //! Note: an example-application of "Silhouette Index" is seen/foudn in ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"] where ....??..
    assert(false); // FIXME: try desciribng how our work relates to ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"] ... 
    assert(false); // FIXME: ensure that we 'provide' support for all the clsuter-comparison-metrics described in ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"]
    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  /* if(isTo_processAll || (0 == strncmp("metric--", array[2], strlen(""))) ) { */

  /*   assert(false); // FIXME: add something */

  /*   cnt_Tests_evalauted++; */
  /* } */
  /* //! -------------------- */
  /* if(isTo_processAll || (0 == strncmp("metric--", array[2], strlen(""))) ) { */

  /*   assert(false); // FIXME: add something */

  /*   cnt_Tests_evalauted++; */
  /* } */
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--Jaccard", array[2], strlen("metric--Jaccard"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--PartitionDifference", array[2], strlen("metric--PartitionDifference"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--F-Measure", array[2], strlen("metric--F-Measure"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--Maximum-Match-Measure", array[2], strlen("metric--Maximum-Match-Measure"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--vanDogen", array[2], strlen("metric--vanDogen"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--mutualInformation-Strehl&Ghosh", array[2], strlen("metric--mutualInformation-Strehl&Ghosh"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--mutualInformation-Fred&Jain", array[2], strlen("metric--mutualInformation-Fred&Jain"))) ) {


    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--VariationOfInformation", array[2], strlen("metric--VariationOfInformation"))) ) {

    assert(false); // FIXME: add something

    assert(false); // FIXME: implement the different clsuter-comparisn-strategies from ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-7-488"]

    cnt_Tests_evalauted++;
  }


  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--hpLysisMetrics", array[2], strlen("metric--hpLysisMetrics"))) ) {
    //! Seperately apply 'the complete set of hpLysis correlation-emtics' to 'count-of-intersectign-cluster-paris' matrix
    assert(false); // FIXME: build different 'syntetic distance-matrices"  .... and relate the latter to "mapOf_scoreDistributions_case1" ... write/describe hytptical extreme-cases ... and investigate 'how the clsuter-categories change wr.t the differen metrics which use distance-mscore-matrix as part of their direct evaluation'.
    //! --------------------
    assert(false); // FIXME: inveistgate the differenc e'in predicitons' betwene the "Jaccard" implmeneted in "kt_matrix_cmpCluster" VS 'in our hpc-clsuter-comparison-metric-appraoch'.
    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }

  //! --------------------
  if(isTo_processAll || (0 == strncmp("evaluateRelation--CCMs_to_randomPerturbatedDataSets", array[2], strlen("evaluateRelation--CCMs_to_randomPerturbatedDataSets"))) ) {
    //! Note: for each of the exisitng gold-standard-cases: write a seperate routine which 'constuct' a data-matrix with increased radnomness (using using swapping wrt. the matrix-cells) .... expects 'this' simplify 'seeing/finding the relationship between  noise-sensitivty (for a given data-set) of different CCMs' 
    //! Note: in order to 'cover' both set-based CCMS and matirx-based CCMs we 'apply' a randomizaiton wrt. both the "matrixOf_coOccurence" and wrt. "mapOf_vertexToCentroid2"
    //! Note: in [”elow] we 'prodcue' a matrix of [cluster-collection][scores(CCM-default-metrix) and seperately ranks(scores(CCM-default-metrix))], and 'seperateøy' a transposed veriosn fo of the reuslts (ie, both wrt. the 'raw scores' and the 'ranks', where we for the latter 'apply a new psot-ranking-operation to descirbe/idnietyf the 'ranks of the ranks', ie, where the latter may be used to describe/infer the agreement between the different data-sets, ie, to 'tes't teh latter wr.t teh randomziation-degree.
    // FIXME: update our [acm__clustMetricCmp.tex] wrt. [ªbove].    
    uint data_subset_id = 2; //! which refers to the data-set-id in the different emasuremetns.
    //uint data_subset_id = 0; //! which refers to the data-set-id in the different emasuremetns.
    //! ---
    const uint global_cntDataSets = 2; uint global_cntDataSets_current = 0;
    const uint globalOffset__betweeDataSets = 4;
    assert(global_cntDataSets >= 1);
    const uint global_cntRows = (global_cntDataSets* (uint)e_kt_matrix_cmpCluster_metric_undef) + ( (global_cntDataSets-1) * globalOffset__betweeDataSets);
    const uint global_cntCols = /*cntRandomFractions=*/10; //! where the latter is 'desinged' to reflect oru smallest data-set-peutbation
    assert(global_cntRows > 0);
    assert(global_cntCols > 0);
    uint global_cntRows_current = 0;
    //!
    //! 
    //! Intiatie:
    s_kt_matrix_t matrixOf_results; setTo_empty__s_kt_matrix_t(&matrixOf_results); init__s_kt_matrix(&matrixOf_results, global_cntRows, global_cntCols, /*isTo_allocateWeightColumns=*/false);
    
    //! ------------------------------------------------
    //! 
    //! Configure how we are to infer the metrics:
    s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    if(false) { //! then we use/apply a 'specifivc' configuration wrt. the CCM-inference:
      e_kt_clusterComparison_t metric_clusterCmp_between = e_kt_clusterComparison_extreme_min;
      e_kt_correlationFunction_t dist_metric_id = e_kt_correlationFunction_groupOf_MINE_mic;
      e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = e_kt_categoryOf_correaltionPreStep_none;
      //! ---------
      //! state that we are to use a complex comparsin-strategy:
      config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true;
      //! Set the tyep of clsuterign-comparison-strategy to use:
      config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between;
      //! Set the type of corrleaiton-metirc to be used:
      config_intraCluster.metric_complexClusterCmp_obj.typeOf_correlationPreStep = dist_metricCorrType;
      config_intraCluster.metric_complexClusterCmp_obj.metric_id = dist_metric_id;
    }    
    //! Start:: collect for CCMs -------------------------------------------------------------------
    //!
    //! 
    //! Iterate through the metrics:    
    { //! 
      //!
      //! Inlcude the test-data:
#include "measure_kt_matrix_cmpCluster__dataSet__cluster__k3_v10.c"
      const char *stringOf_dataSet = "3x3x_allInSame";
      //const char *stringOf_dataSet = "10x10x_allInSame";
      const uint cnt_clusters_1 = cnt_defaultScoreTypes_cntClusters2; //! defined in [ªbvoe] 'inlcude'.
      const uint cnt_vertices = cnt_defaultScoreTypes_cntVertices_case2; //! defined in [ªbvoe] 'inlcude'.
/* #include "measure_kt_matrix_cmpCluster__dataSet__cluster__k3.c" */
/*       const char *stringOf_dataSet = "3x3x_allInSame"; */
/*       //const char *stringOf_dataSet = "10x10x_allInSame"; */
/*       const uint cnt_clusters_1 = cnt_defaultScoreTypes_cntClusters1; //! defined in [ªbvoe] 'inlcude'. */
/*       const uint cnt_vertices = cnt_defaultScoreTypes_cntVertices_case1; //! defined in [ªbvoe] 'inlcude'. */
      assert(global_cntCols <= cnt_vertices); //! ie, as we otherwise may have a 'complete randomziaiton'.
      assert(data_subset_id < cnt_defaultScoreTypes_case2); //! ie, as we expec thte 'dataset' to be 'inside'æ the speifciton-range' of [ªbov€] included file.
#define __localGold mapOf_scoreDistributions_case2
#define __localMatrix distMatrix_case2_subCase_a
/* #define __localGold mapOf_scoreDistributions_case1 */
/* #define __localMatrix distMatrix_case1_subCase_a */
      //!
      //!
      //! Apply logics:
#include "measure_kt_matrix_cmpCluster__CMP__randomPerturbatedDataSets__wrapper.c" //! which calls "measure_kt_matrix_cmpCluster__CMP__randomPerturbatedDataSets.c"
    }
    //! ------------
    if(global_cntDataSets_current < global_cntDataSets)
    { //! For the second data-set:
      data_subset_id++;
      assert(global_cntDataSets_current < global_cntDataSets); //! ie, as we expect that '[”elow] data-set is expected':
      //!
      //! Inlcude the test-data:
#include "measure_kt_matrix_cmpCluster__dataSet__cluster__k3_v10.c"
      const char *stringOf_dataSet = "3x3x_allInSame";
      //const char *stringOf_dataSet = "10x10x_allInSame";
      const uint cnt_clusters_1 = cnt_defaultScoreTypes_cntClusters2; //! defined in [ªbvoe] 'inlcude'.
      const uint cnt_vertices = cnt_defaultScoreTypes_cntVertices_case2; //! defined in [ªbvoe] 'inlcude'.
      assert(global_cntCols <= cnt_vertices); //! ie, as we otherwise may have a 'complete randomziaiton'.
      assert(data_subset_id < cnt_defaultScoreTypes_case2); //! ie, as we expec thte 'dataset' to be 'inside'æ the speifciton-range' of [ªbov€] included file.
#define __localGold mapOf_scoreDistributions_case2
#define __localMatrix distMatrix_case2_subCase_a
      //!
      //!
      //! Apply logics:
#include "measure_kt_matrix_cmpCluster__CMP__randomPerturbatedDataSets__wrapper.c" //! which calls "measure_kt_matrix_cmpCluster__CMP__randomPerturbatedDataSets.c"
    }
    //! finish:: collect for CCMs -------------------------------------------------------------------
    assert(global_cntDataSets_current == global_cntDataSets); //! ie, as we otehrwise may have 'forotten' to have evlauated the complete number o data-sets.
    //!
    //! 
    { //! Write out the results:
      const char *stringOf_resultFile = "resultOf_CCMs_to_randomPerturbatedDataSets.tsv";
      bool is_ok = export__singleCall__s_kt_matrix_t(&matrixOf_results, stringOf_resultFile, /*fileHandler=*/NULL);
      assert(is_ok);
      //!
      //! Write out a transposed verison:
      s_kt_matrix_t matrix_transposed; setTo_empty__s_kt_matrix_t(&matrix_transposed);
      is_ok = init__copy_transposed__s_kt_matrix(&matrix_transposed, &matrixOf_results, /*copyStrings=*/true);
      assert(is_ok);
      stringOf_resultFile = "resultOf_CCMs_to_randomPerturbatedDataSets.transposed.tsv";      
      is_ok = export__singleCall__s_kt_matrix_t(&matrix_transposed, stringOf_resultFile, /*fileHandler=*/NULL);
      assert(is_ok);
      free__s_kt_matrix(&matrix_transposed);

      //! -----------
      //!
      //! Convert to ranks and thereafter write out:
      s_kt_matrix_t matrix_ranked; setTo_empty__s_kt_matrix_t(&matrix_ranked); is_ok = init__copy__useRanksEachRow__s_kt_matrix(&matrix_ranked, &matrixOf_results, /*isTo_updateNames=*/true);
      assert(is_ok);
      stringOf_resultFile = "resultOf_CCMs_to_randomPerturbatedDataSets.ranked.tsv";
      is_ok = export__singleCall__s_kt_matrix_t(&matrix_ranked, stringOf_resultFile, /*fileHandler=*/NULL);
      assert(is_ok);
      //!
      //! Write out a transposed verison:
      setTo_empty__s_kt_matrix_t(&matrix_transposed);
      //! Note: in constrast to [ªbove] 'transposed export' we here 'apply a rank-cosntruction of the current dat-aset', ie, to simplify the 'internal CCM-ranks' wrrt. the data-sets:
      is_ok = init__copy_transposed__thereafterRank__s_kt_matrix(&matrix_transposed, &matrix_ranked, /*copyStrings==*/true);
      assert(is_ok);
      stringOf_resultFile = "resultOf_CCMs_to_randomPerturbatedDataSets.ranked.transposed.tsv";      
      is_ok = export__singleCall__s_kt_matrix_t(&matrix_transposed, stringOf_resultFile, /*fileHandler=*/NULL);
      assert(is_ok);
      free__s_kt_matrix(&matrix_transposed);


      //!
      //! De-allocate:
      free__s_kt_matrix(&matrix_transposed);
      free__s_kt_matrix(&matrix_ranked);
    }
    printf("global_cntRows_current=%u, global_cntRows=%u, nrows=%u, at %s:%d\n", global_cntRows_current, global_cntRows, matrixOf_results.nrows, __FILE__, __LINE__);
    assert(global_cntRows_current == global_cntRows);
    //! ------------------------------------------------------------------------------------------------------------------------
    free__s_kt_matrix(&matrixOf_results);
    cnt_Tests_evalauted++;
  }

  //! --------------------
  if(isTo_processAll || (0 == strncmp("metric--hpLysisMetrics--MINE", array[2], strlen("metric--hpLysisMetrics---MINE"))) ) {
   //! Test the MINE-correlaiton-emtric for a sytnetic data-set:
    const uint nrows = 20;     const uint ncols = 20; 
    const char *stringOf_sampleData_type = "lines-different-ax";
    const char *stringOf_sampleData_type_realLife = NULL;
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    //!
    { //! Load the data-set:
      const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20;
	const bool isTo_transposeMatrix = false;
      s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
if(obj_baseToInclude.nrows) {
  for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
    //assert(obj_baseToInclude.matrix[row_id]);
    assert(row_id < obj_baseToInclude.nrows);
    assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
    const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
    //if(row) 
    //{
    assert(row);
  }
 }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
    }
    //!
    //! Buidl a subset:
    const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&obj_matrixInput, "testData_beforeCorr.tsv", NULL);
    assert(is_ok_ex);

    //!
    //! Correlate:
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    hp_config.stringOfResultPrefix__exportCorr__prior = "testData.tsv";
    // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Correlate and export:
    const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
    assert(is_ok);
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);	    
    free__s_kt_matrix(&obj_matrixInput);
    //! ------------- 
    cnt_Tests_evalauted++;
  }


  // FIXME: try to evlauate different "config_min_rankThreshold" ... identifying the 'oopmal' 'trheshodl'  <-... 
  const t_float config_min_rankThreshold = 0.7; //! ie, use the 30% most higly-rankged vertices.

  //! --------------------------------------------------------------------------------------
  //! --------------------
  //! date; time valgrind ./x_measures kt_matrix_cmpCluster findExtremeCases-step0  1>out.txt
  if(isTo_processAll || (0 == strncmp("findExtremeCases-hardCodedExtremeClusterResults", array[2], strlen("findExtremeCases-hardCodedExtremeClusterResults"))) ) {   
    //! Idea: to investigate our Null Hypthesis that "the selection of cluster-comparison-emtrics (to evlauate difference swrt. treuslts, eg, dervied from diffenret clustering-algorithms) strongly inlfuence the outcome 'of a comparison-study' (eg, wrt. idnetifying optional clsuter-algorithms for different data-sets)". In order to address this issue we (in this test-section) compare diffnere tcluster-comparison-metrics through (a) a set of pre-defined clsuter-distributions and (b) pre-defined correlation-matrices. To reduce the number fo 'rep-defined cases' (which we evlaute) we choose/build 'extreme clsuter-cases', ie, possible cluster-result-outcomes which are challenging for cluster-comparison-metrics to handle. The use of this 'pre-deifned set of clsuter-results' is founded/based/inspired by the observation that clsuter-algorithsm and correlation-metrics produce differnet results (for the same data-sets), ie, for which we (in this evlauation-strategy) choose to handle/evluate the 'outcomes of corrleaiton-emtrics and clsuter-algorithms' (instead of first 'going through the itnermeidate steps of clsuter-construction'), ie, where our evlauat-strategy (through our use of 'extreme cases') manages (through a relative few number of resutls-sets) to cpature core-traits (of correlation-comaprsion-emtrics). The core-traits (of the clsuter-comaprsion-emtrics are hoped to) idneityf for what clsuter-esutl-cases the different clsuter-comaprsion-emtrics agree: from a 'visaul cosnsitency-evlauation fot he latter resutls' (we belive) it is possible to identify 'overlapping areas of/for the differnet cluster-comparison-metrics (eg, subsets of cluster-comparison-emtrics which always agree). <-- todo: udpate 'this' after we have 'erpforemd' oru WWW-visual-reulst-anlasysi.
    //! Note[WWW-visu]: in 'this layout' we let 'row[1-2]' represnet ["ideaClusterCategoryComparsion(1)", "ideaClusterCategoryComparsion(2)"], and where each column describes a [heatMap-case(row-case(i)) for each "e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t"]. In the llatter 'visaulization' where thereby 'seprately' compare sub-comparison-cases (eg, seperately for "metricXdata" and "metricXdata.ranks"). The result of this comparison (is intended to) describe/idenitfy the 'degree of agreement between our evlauated-hard-coedded-cases with a known pre-deinfed simliarty' (ie, where the 'simalrity' is due to our 'choise' when buidlign the hard-coded clsuter-agreement-sets). An example-case wrt. the latter conserns/is ...??.. <-- todo: udpate 'this' wehn we ahve completed our WWW-visu-appraoch.
    //! Note[weakness]: in many cases the use of 'extreme cases' is not representaive for cocnrete user-applications: for real-life data-sets we assuem/assert that the clsuter-comparsion-appraoches results in lsser 'clear-cut' cluster-resutls' (for which we assume/hyptoehe that soudn cluster-comparison-emtics whould be able to handle both 'extreme' and non-extrme cases, where the latter is idnetifed in our 'large'-scale clsuter-evlauation in our "rowEachData_colDifferentClusterDistributions.heatmap") (<-- todo: vlaidate this assumption in our testCase="findExtremeCases-step0" using a WWW-visual-comarpsin)
    // FIXME: update our [acm__clustMetricCmp.tex] wrt. [ªbove].

    //! -------------------------------
    const uint cnt_rowsToInsert = 2; //*e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef; 
    uint cnt_rowsToInsert_current = 0; //! ie, teh number of old-stadnards which we evluate
    //! -------------------------------

    char string__cmpResults_goldStandard[3000] = {'\0'}; sprintf(string__cmpResults_goldStandard, "meta_goldStandardCmp.tsv");
    {const char *resultDir_path = "results/hardCodedClusterResults_mergeOfCases/"; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
    //{const char *resultDir_path = ""; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
    //{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__inner); assert(is_ok);}
    //system("rm -f results/hardCodedClusterResults_mergeOfCases/*");
    s_kt_assessPredictions_differentMetricSets_t cmpResults_goldStandard = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__cmpResults_goldStandard, /*directory=*/"results/hardCodedClusterResults_mergeOfCases/");

    s_kt_assessPredictions_differentMetricSets_t *obj_result = &cmpResults_goldStandard; //setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/(mapOf_functionStrings_base_size*numberOf_matrices*stringOf_noise_size*fraction_colNoise_cnt), string__metaFileUnique__all );
    {
      assert(cnt_rowsToInsert_current < cnt_rowsToInsert); cnt_rowsToInsert_current++;
      //!             Generic cofniguraitons:
      {const char *resultDir_path = "results/hardCodedClusterResults_case_0/"; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
      //system("rm -f results/hardCodedClusterResults_case_0/*");
      s_kt_longFilesHandler fileHandler__global__ = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/"results/hardCodedClusterResults_case_0", /*file_nameMeta=*/"metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
      s_kt_longFilesHandler *fileHandler__global = &fileHandler__global__;
      const char *stringOf_resultPrefix = "clusterCmpEvaluation";
      fprintf(stderr, "\n\n(info)\t Start for case=\"%s\", at [%s]:%s:%d\n", stringOf_resultPrefix, __FUNCTION__, __FILE__, __LINE__);
      const uint cnt_vertices = cnt_defaultScoreTypes_cntVertices_case1; const uint cnt_clusters = cnt_defaultScoreTypes_cntClusters1; 
      const uint cnt_data_nrows = cnt_defaultScoreTypes_case1;     const uint cnt_data_ncols = cnt_defaultScoreTypes_case1;
      //! -----------------------------------------------------------------
      //! Compare different partitions for the data:
#define arrOf_2d_data__set1 mapOf_scoreDistributions_case1
#define arrOf_2d_data__set2 mapOf_scoreDistributions_case1
#define arrOf_1d_dataXdata_goldStandard mapOf_scoreDistributions_case1_2xgoldStandard
      //! --------------------
      // Define the number-and-value of the distance-correlation-metices in deimsnion [cnt_vertices, cnt_vertices] to use:
      const uint cnt_matricesToEvaluate = 3;
#define distMatrix_0 distMatrix_case1_subCase_a
#define distMatrix_1 distMatrix_case1_subCase_b
#define distMatrix_2 distMatrix_case1_subCase_c
      //! -----------------------------------------------------
      //! -----------------------------------------------------
      //! 
      //! Apply the logics:
      const char *prefix__clusterSets = "data";
#include "kt_assessPredictions__stub__clusterMetricCmp__main__wrapper.c"
      //#include "kt_assessPredictions__stub__clusterMetricCmp__main.c"
      //! ----------------------------------------
      //! 
      //! De-allocate:
      free__s_kt_longFilesHandler_t(fileHandler__global);  fileHandler__global__ = *fileHandler__global;
    } 
    //! -----------------------------------------------------
    //if(false) // FIXME: remove 
    { //! [ A new syntetic-test-case]: 
      assert(cnt_rowsToInsert_current < cnt_rowsToInsert); cnt_rowsToInsert_current++;
      //!             Generic cofniguraitons:
      {const char *resultDir_path = "results/hardCodedClusterResults_case_1/"; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
      //system("rm -f results/hardCodedClusterResults_case_1/*");
      s_kt_longFilesHandler fileHandler__global__ = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/"results/hardCodedClusterResults_case_1", /*file_nameMeta=*/"metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
      s_kt_longFilesHandler *fileHandler__global = &fileHandler__global__;

      const char *stringOf_resultPrefix = "clusterCmpEvaluation_case2";
      fprintf(stderr, "\n\n(info)\t Start for case=\"%s\", at [%s]:%s:%d\n", stringOf_resultPrefix, __FUNCTION__, __FILE__, __LINE__);
      const uint cnt_vertices = cnt_defaultScoreTypes_cntVertices_case2; const uint cnt_clusters = cnt_defaultScoreTypes_cntClusters2; 
      const t_float config_min_rankThreshold = 0.8; //! ie, use the 20% most higly-rankged vertices.
      const uint cnt_data_nrows = cnt_defaultScoreTypes_case2;     const uint cnt_data_ncols = cnt_defaultScoreTypes_case2;
      //! -----------------------------------------------------------------
      //! Compare different partitions for the data:
#define arrOf_2d_data__set1 mapOf_scoreDistributions_case2
#define arrOf_2d_data__set2 mapOf_scoreDistributions_case2
#define arrOf_1d_dataXdata_goldStandard mapOf_scoreDistributions_case2_2xgoldStandard
      //! --------------------
      // Define the number-and-value of the distance-correlation-metices in deimsnion [cnt_vertices, cnt_vertices] to use:
      const uint cnt_matricesToEvaluate = 3;
#define distMatrix_0 distMatrix_case2_subCase_a
#define distMatrix_1 distMatrix_case2_subCase_b
#define distMatrix_2 distMatrix_case2_subCase_c
      //! -----------------------------------------------------
      //s_kt_assessPredictions_differentMetricSets_t *obj_result = NULL; //setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/(mapOf_functionStrings_base_size*numberOf_matrices*stringOf_noise_size*fraction_colNoise_cnt), string__metaFileUnique__all );
      //! -----------------------------------------------------
      //! 
      //! Apply the logics:
      const char *prefix__clusterSets = "data";
#include "kt_assessPredictions__stub__clusterMetricCmp__main__wrapper.c"
      //! ----------------------------------------
      //! 
      //! De-allocate:
      free__s_kt_longFilesHandler_t(fileHandler__global);  fileHandler__global__ = *fileHandler__global;
    }

    //! Complete and write out the results:
    correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResults_goldStandard, NULL, NULL, NULL, UINT_MAX, /*stringOf_measurementSubset=*/"hardCoded_GOLD_cmp", 0, NULL);
    free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResults_goldStandard);

    // TODO: write a 'wrapper' of [ªbove] where we 'comptue seprately for the dfiferent distance-matrix' ... seprately for each distance-matrix ... and update our "kt_assessPredictions.c"    
    assert(false); // TODO: consider 'adding some other' exstreme-cases wrt. [ªbove].


    //! ------------- 
    cnt_Tests_evalauted++;
  } 


  //! --------------------------------------------------------------------------------------
  //! --------------------
  //! date; time valgrind ./x_measures kt_matrix_cmpCluster findExtremeCases-step0  1>out.txt
  if(isTo_processAll || (0 == strncmp("findExtremeCases-step0", array[2], strlen("findExtremeCases-step0"))) ) {    
    //! Idea: to investgiate the Null Hyptohesis that "there are well-defined cateogires of data-sets which 'produce' the same degree/factor of sigficnat clusters (eg, wrt. differneces in the applied correlation-matrics and cluster-comparison-metrics)".
    //! Note[WWW-visu]: ....
    //! Note: in this test we extend the testCase="findExtremeCases-hardCodedExtremeClusterResults" to evalaute cluster-results derived from cluster-algorithms: in contrast to the latter we 'here' 'prodcue' the cluster-results based on correlation-emtics and clsuter-algorithm(s) and from the latter evlaute/idneitfy the agreemetnmens (between the clsuter-comparison-metrics). .... 
    //! Note[gold-standard]: a challenge in evlauation of clstuer-comparison-metircs consers the gold-standard, ie, to idneitfy accurate non-ambiguous cluster-results for both ambbiguous and non-ambgius data (thereby facitlaitatin ana analsysi-coverage of/for the 'span' of real-lfie-data)
    //! Note[weakness]: in this 'test' we seeks to assess/evlauate the diffenret cluster-comparison-metrics by comparing the 'internal order (ie, ranks) of each prediction with a manually hypothesed gold-standard'. While the strenght (of this appraoch) cosnerns the large degree of apriory hypotical assumptiosn (ie, high manual curaiton-degree) a weakness relates/conserns to 'the same', ie, that we inspect only a few number of cobmaitiosn, and that our evlauation-resutls is strongly influenced by our tacit understaidn of 'close clusters'. However, as any 'combiantorical approach to cpature all sides of a combinatorical space' will be infleucned by tacitc/manual evlauations, we belivei that this 'evlauation-step' is critical in understanding of cluster-comparison-metrics, ie, to idenitfy/evlauat ehd different 'critcal cases' where cluster-comparison-rsutls diverges from a researchers 'percpetual understanding'. 
    //! -----------------------------------------------------
    //! -----------------------------------------------------

    { //! Test for the 'synteitc mathemtical fucnitosn'
      //! Note: idea is to mkae use of hpLysis support for 'random noise in sampel-data-feature-vectors', and thereafter compare the cluster-results 'generated' from the 'data-sets with different noise-factor', ie, to compare both the simliarty in the data-sets (between the different cluster-comparison-metrics) and wrt. the 'clusters of metrics and data-sets wrt. the gold-standard-adjusted values'. For this 'taks' we first 'disturb' each feature-vector with 'lesser-predictable-feature-vectors', comptue clusters 'gernated from this' and thereafter compare the results. In this appraoch the following 'procedure' is applied: (a) 
      //#define __macro__buildDataSet() ({constructSample_data_by_function_noise(&

      //! Cases-descirption: in order to explreo different/multiple 'subsets of data-classifciaiton-problems' we in this evalaution address:
      //! --------------
      //! --- case(a): "mathemtical distributions (different noise-ratios)": evaluate 'standardized extreme cases' in noise-data-distributions, ie, to identify a set of 'reference-cases' wrt. 'common classes of data-sets';
      //! --- case(b): "real-life-data-sets": idneitfy simliarty in noise-distrubions (between non-simplifed data-points): provide a 'bridge' (in our evlaution) to cocnrete/'real' use-case-applciatiosn (of our appraoch): in this work we (seeks to) demonstrate that the clsuter-comparison-metics provides in non-constent/ambigious results (when 'comapred to each other'). 
      //! --- case(c): [case(a) x case(b)]: increase the 'task of clsuter enumeration/cateorization' by 'adjsuting' each feature-vector with diffneret data-set-permtuatiosn: identify 'to what extent' a perturbation in itnerpretaiton-silairty (wr.t the clsuter-enumeration-cateogrizaiton) 'change' the agreement betwen cluster-comaprison-metircs: as we are intereested in 'maximizing the speration' between 'agrement in cluster-comparison-emtric-enumeration-clasisficaiotn between different data-sets' this/our data-set-permtuations-trategy offers/provides an opportunity to ifdentify/describe different 'clases' of real-life data-sets wrt. 'how simliarty and dis-simlairty in/betwene clsuter-comparison-metrics shall be interpeted'. To examplify the latter, we for [<real-life-data-set>][<sytnetic-data-permtaution>::<cluster-metric-scores>] expect different "sytnetic-data-permtaution" permtuations to 'move' each "real-life-data-set" into diffenret clusters, ie, for which 'the classifciation of a data-set based on diffneret metric' may relate directly to the 'type of ambigutiy in data-sets': we use muliple heat-maps to both clasisfity/orgnaise this 'ambiguity' and 'relate an agremeent-facctor between differnet cluster-comparison-metrics to data-set-permtautions which has this property', ie, where the result is a 'relatedness between cluster-comparsion-metric-agreements and data-enumeration-clasisficoant-ambiguity'.
      //! --------------
      //! --- 
      //! Note: in order to idneityf the 'ideal k-number of lcusters' we use the higly accurate MINE-corerlation-meatric to consturct ehat-map, and from the heat-map 'identify' the 'ideal k-number of clustes' for each data-set-case. The prcoedure (wrt. the latter) is to: (a) build a sample-data-set; (b) apply a MINE-based correlation-metirc (and genrate a result-file); 'manually' evalaute heat-maps (seperately for each data-set) and identify the k-number of ideal clusters.
      const bool localConfig__isTo___computeKCluster = false;
      const char *stringOf_experimentDescription_base = "corrMetricsDependencyToInputData";
      //! ---      
      const uint nrows_base = 10;       
      //!
      //! Note: as we are itnersted in a 'direct visual comparison of heatmap-metrics' we choose to 'set' the number-of-matrix-diemsions-to-evlauate to the 'count of metics we evlaute': for applicaiotn-detilas wr.t the latter pelase see our "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases.c":
      uint __numberOf_corrMetrics__categoryOf__small = 0;
      for(uint corr_index = 0; corr_index < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; corr_index++) {
	const e_kt_correlationMetric_initCategory_timeComplexitySet__small_t corrEnum = (e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)corr_index;
	assert(corrEnum != e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef);
	s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(corrEnum);
	if(isTo_use_directScore__e_kt_correlationFunction(corrMetric_prior.metric_id)) {continue;} //! ie, as we assume the data-input is Not an adjecnye-matrix.
	__numberOf_corrMetrics__categoryOf__small++;
      }
      assert(__numberOf_corrMetrics__categoryOf__small > 0);

      const uint numberOf_matrices = __numberOf_corrMetrics__categoryOf__small; const uint numberOf_matrices_stepFactor = 2;
      //! ---
      const uint fraction_colNoise_cnt = 5;







      //! ------------------------------------
      //!
      { //! Export the correlation-sets assicated to the different matrices
	//! Note: in this 'call' we build heatmaps for [1...numberOf_matrices]x[mapOf_functionStrings_base] data-set-combinations. Our goal is to both idnetify gold-standards and diffneret cluster-cases (ie, by a visaul comparison of the latter).
	//! Note: a challenge in [”elow] conserns the 'interst' in comparing/evlauating the parameter-space of [matrix-size, data-distributions, real-life-applications, noise]. An example issue/challenge wr.t the latter conserns the interest in/to visualise both 'real-lfie-dat-asets' and 'syntietc data-sets' in the same view: for an evlauation to be 'visaully simple (ie, to 'provide' a comparison of result-sets withotu having to 'shift' between differnt views) we could generate a collection of data-sets where each 'row' represents a data-sets at different matrix-diemsions. However for the latter it is difficult/erronosuly to adjust the size of the real-life data-sets (ie, in cotnrast to the sytneitc data-sets), ie, as the 'size' (of the input-data-sets) is fixed. to overcome the latter issue we 'add' a random-noise (instead of increasing the fixed row-column-size), ie, for which we simplify/enalbe the comparison of the parameter-space=[data-distributions, real-life, matrix-diemsnions, noise]. 
	//! Note: if we choose/use an appraoch usign a fixed corlreaiton-metic we expect our result to be  biased towards the correlation-emtic (rather than the data-sets). To investigate the 'bias-effect' of corlreiaotn-metics we investigate/evlauate s subset of diffneret corlreaiton-metrics. Of interst is therefore to expand/enalrge our data-set-test-horizon to cover/evlaute a number of 'extreme corlreaiton-emtircs', ie, where we evaluate for 'the complete set of sytnetic and real-lfie data-sets, producing a seperate/unique file-set' (ie, an own HTML-web-page). To reduce the comparsion-diemsionality (of our approach) we use/compare a 'fixed count of correlation-meitrcs for the same data-set' (where we evlauate/use the subset of emtrics define din our "e_kt_correlationMetric_initCategory_timeComplexitySet__small_t"). In our result-secion we use (the result genrated by the latter) to evlauate/discuss/describe ambugities wrt. correlation-metrics. An example-applciaotnio-case cosnerns to evlauate the null-hyptoioesis that "our evlauation (wrt. 'ideal' VS the default/naive set of naive corrleation-metrics) demonstrate that the latter [Pearson, Euclid] metrics 'are un-represnetive for the metrics which may be used in clsuteirng-algrotihms'". <-- todo: udpate 'this text' when we have completed our evlauation.
	//! Note[visual::interpretaiton]: ... to compare images it might be of itnerest to 'correlatiosn from random data-sets' as a tempalte-basis: in a 'random data-set for ehatmap-cosntrucitoni' we expect approx. all the 'points' to have a medium-improtnacei, ie, 'slightly improtnat though not sigficnatly improtant/dsitinct')
	{
	  //! Note[result]: In below images we evaluate the challenges in semi-un-supervised clustering. Our motivation is to both establish putative gold-standards (for each of the data-sets) and to hypothese the simplictly wrt. clustering: to assess the ambiguities in identifying the cluster-gold-standards, ie, as a 'gold standard' may depend upon tacit non-pre-producable selection-criteria. In belwo we explroe the parameter-domains of: syntetic-structured data-sets, real-life-data-sets, different feature-vector-lengths, and signficance of distnace-correlation-metics (when describing relatedness). Our null-hypothesis is that there will be no marked (ie, dominant) seperation between the paramter-ranges, ie, that a combination of the latter parameters are needed (when cateorizing data-sets wrt. cluster-gold-standard-distributions and gold-standard-specificaiton-ambiguities).
	  s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
	  {const char *resultDir_path = "results/tmp_1_resultMatrices/"; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	  //system("rm results/tmp_1_resultMatrices/*");
	  s_kt_longFilesHandler fileHandler__localCorrMetrics = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/"results/tmp_1_resultMatrices/", /*file_nameMeta=*/"local_corrMetrics__metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
	  const uint cnt_runs = 2; //! ie, for both ["data-sizes", "corr-metrics", "data1---data2"] (for sytnettic data-sets).
	  uint groupId_max = 0;
	  const bool isTo_transposeMatrix = false;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
	  if(obj_baseToInclude.nrows) {
	    for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
	      //assert(obj_baseToInclude.matrix[row_id]);
	      assert(row_id < obj_baseToInclude.nrows);
	      assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
	      const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
	      //if(row) 
	      //{
	      assert(row);
	    }
	  }
#endif

#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases.c"
	  //! De-allocate:
	  free__s_kt_longFilesHandler_t(&fileHandler__localCorrMetrics);
	}
	{ //! Evaluate the metrics where we 'mere' "linear-differentCoeff-b" with the 'set of differnet data-sets':
	  //! Note[result]: 
	  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);	  
	  //assert(false); // FIXME: remove
	  { //! Build a sample-data-set (which we use to evlauate the 'effect' of fidnign 'patterns' in different data-sets):
	    //! Note: a different 'effect' wrt. this evlauation is that we for 'increased distances' will have 'no values' in the "ncols" field' (ie, as we buidl only for a fixed ncols-count while our tests uses a 'dynamic evlauaiotn-strategy').
	    const char *stringOf_sampleData_type_realLife = NULL;
	    const uint nrows = 6; //nrows_base; 
	    const uint ncols = nrows;
	    const char *stringOf_sampleData_type = "linear-differentCoeff-b";
	    const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	    const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20;
	    s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
	    const bool isTo_transposeMatrix = false;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
	    if(obj_baseToInclude.nrows) {
	      for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
		//assert(obj_baseToInclude.matrix[row_id]);
		assert(row_id < obj_baseToInclude.nrows);
		assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
		const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
		//if(row) 
		//{
		assert(row);
	      }
	    }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
	  }

	  //assert(false); // FIXME: remove
	  assert(obj_matrixInput.nrows > 0);
	  assert(obj_matrixInput.ncols > 0);
	  s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude);
	  const bool is_ok = init__copy__s_kt_matrix(&obj_baseToInclude, &obj_matrixInput, /*isTo_updateNames=*/false);	  
	  assert(is_ok);
	  assert(obj_matrixInput.nrows == obj_baseToInclude.nrows);
	  assert(obj_matrixInput.ncols == obj_baseToInclude.ncols);
	  //s_kt_matrix_t obj_baseToInclude = obj_matrixInput; //! ie, a 'simple copy'.
	  {const char *resultDir_path = "results/tmp_2_resultMatrices/"; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	  //system("rm results/tmp_2_resultMatrices/*");
	  s_kt_longFilesHandler fileHandler__localCorrMetrics = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/"results/tmp_2_resultMatrices/", /*file_nameMeta=*/"local_corrMetrics__metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
	  const uint cnt_runs = 2; //! ie, for both ["data-sizes", "corr-metrics", "data1---data2"] (for sytnettic data-sets).
	  uint groupId_max = 0;
	  const bool isTo_transposeMatrix = false;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
	  if(obj_baseToInclude.nrows) {
	    for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
	      //assert(obj_baseToInclude.matrix[row_id]);
	      assert(row_id < obj_baseToInclude.nrows);
	      assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
	      const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
	      //if(row) 
	      //{
	      assert(row);
	    }
	  }
#endif
	  //assert(false); // FIXME: remove
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases.c"
	  //! De-allocate:
	  free__s_kt_longFilesHandler_t(&fileHandler__localCorrMetrics);
	  assert(obj_matrixInput.matrix != obj_baseToInclude.matrix); //! ie, givne [ªbove] 'explict copy'.
	  free__s_kt_matrix(&obj_matrixInput);
	  free__s_kt_matrix(&obj_baseToInclude);
	}
	{ //! Evaluate the metrics where we 'mere' "linear-differentCoeff-b" with "flat", ie, to evlauate ht e'effects' of 'differnet data-sets':
	  //! Note[result]: Extends the results stored in our "$base_dir_1": combines the 'described data-set' with "linear-differentCoeff-b": what we expect is that the MINE-correlation-metric will provide a better seperation-factor-degree between the clusters (eg, when compared to Euclid). We expect that the 'importance of MINEs accraute classifciaiton-ability' will depend upon each data-set, ie, where we hypothese/assert that there are specific data-sets where Euclid (and other computation-low-cost correlation-metrics) provide an 'as-good-as' clasisficaiotn. The implication (of the latter) is an (expected) classifcaiton of correlation-metrics applicability, ie, where data-permtautions and cluster-deviation is used to hypothese/assert/idneitfy different data-applicaiton-cases (eg, wrt. compelxity in data-inputs).
	  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);	  
	  { //! Build a sample-data-set (which we use to evlauate the 'effect' of fidnign 'patterns' in different data-sets):
	    //! Note: a different 'effect' wrt. this evlauation is that we for 'increased distances' will have 'no values' in the "ncols" field' (ie, as we buidl only for a fixed ncols-count while our tests uses a 'dynamic evlauaiotn-strategy').
	    const char *stringOf_sampleData_type_realLife = NULL;
	    const uint nrows = 6;
	    assert(nrows > 0);
	    const uint ncols = nrows;
	    const char *stringOf_sampleData_type = "linear-differentCoeff-b";
	    const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	    const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20;
	    const bool isTo_transposeMatrix = false;
	    s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
	    if(obj_baseToInclude.nrows) {
	      for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
		//assert(obj_baseToInclude.matrix[row_id]);
		assert(row_id < obj_baseToInclude.nrows);
		assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
		const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
		//if(row) 
		//{
		assert(row);
	      }
	    }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"


	    // assert(false); // FIXME: remove
	    //! 
	    //! Merge [abov€] with "flat":
	    assert(obj_matrixInput.matrix != obj_baseToInclude.matrix); //! ie, givne [ªbove] 'explict copy'.
	    free__s_kt_matrix(&obj_baseToInclude);
	    setTo_empty__s_kt_matrix_t(&obj_baseToInclude);
	    //s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude);
	    const bool is_ok = init__copy__s_kt_matrix(&obj_baseToInclude, &obj_matrixInput, /*isTo_updateNames=*/false);	  
	    assert(is_ok);
	    assert(obj_matrixInput.nrows == obj_baseToInclude.nrows);
	    assert(obj_matrixInput.ncols == obj_baseToInclude.ncols);
	    //obj_baseToInclude = obj_matrixInput; //! ie, 'use [ªbove] as input into [below]:
	    {
	      s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);	  
	      const char *stringOf_sampleData_type = "flat";
	      const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	      const bool isTo_transposeMatrix = false;
	      const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
	      if(obj_baseToInclude.nrows) {
		for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
		  //assert(obj_baseToInclude.matrix[row_id]);
		  assert(row_id < obj_baseToInclude.nrows);
		  assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
		  const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
		  //if(row) 
		  //{
		  assert(row);
		}
	      }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
	    }
	    //! De-allocate:
	    assert(obj_matrixInput.matrix != obj_baseToInclude.matrix); //! ie, givne [ªbove] 'explict copy'.
	    free__s_kt_matrix(&obj_baseToInclude);
	  }
	  //assert(false); // FIXME: remove
	  assert(obj_matrixInput.nrows > 0);
	  assert(obj_matrixInput.ncols > 0);
	  s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude);
	  const bool is_ok = init__copy__s_kt_matrix(&obj_baseToInclude, &obj_matrixInput, /*isTo_updateNames=*/false);	  
	  assert(is_ok);
	  assert(obj_matrixInput.nrows == obj_baseToInclude.nrows);
	  assert(obj_matrixInput.ncols == obj_baseToInclude.ncols);
	  //s_kt_matrix_t obj_baseToInclude = obj_matrixInput; //! ie, a 'simple copy'.
	  {const char *resultDir_path = "results/tmp_3_resultMatrices/"; const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	  //system("rm results/tmp_3_resultMatrices/*");
	  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
	  s_kt_longFilesHandler fileHandler__localCorrMetrics = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/"results/tmp_3_resultMatrices/", /*file_nameMeta=*/"local_corrMetrics__metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
	  const uint cnt_runs = 2; //! ie, for both ["data-sizes", "corr-metrics", "data1---data2"] (for sytnettic data-sets).
	  uint groupId_max = 0;
	  const bool isTo_transposeMatrix = false;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
	  if(obj_baseToInclude.nrows) {
	    for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
	      //assert(obj_baseToInclude.matrix[row_id]);
	      assert(row_id < obj_baseToInclude.nrows);
	      assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
	      const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
	      //if(row) 
	      //{
	      assert(row);
	    }
	  }
#endif
	  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases.c"
	  //! De-allocate:
	  free__s_kt_longFilesHandler_t(&fileHandler__localCorrMetrics);
	  assert(obj_matrixInput.matrix != obj_baseToInclude.matrix); //! ie, givne [ªbove] 'explict copy'.
	  free__s_kt_matrix(&obj_baseToInclude);
	  free__s_kt_matrix(&obj_matrixInput);
	}
      }
      //! ------------------------------------
      //!


      assert(false); // FIXME: ... write a seperate if-clause ... where we 'only' generate an 'initial heat-map' ... 
      assert(false); // FIXME: ........ [above] .... a 'dry-run' where we 'collect the results' ... [data-set-cases][k-means-values::<ideal + alternaitve_permtaution>] ... ... and then use the 'result-matrix' as input to our "standAlone__describeClusterCmpSimilarities_forDataSet__s_kt_assessPredictions_t(..)" where we use the 'gold-standard in data-ambuity' (ie, based on our manual ifnerence/desicpriotns of the ehat-maps)' as funciton-input (to the latter described funciton), ie, to 'comapre our a-prior expectaiosn to the de-facto results. Our null-hypothesis (which we expect to be wrong) is that "the data-sets will be clsutered (through the orrelation-comparison-metircs) wrt. their dgree of data-ambutiy" (a null-hypotehs which is bleived to be wrong as we asusme the data-sets to be unable to capture the in-accuraices). For 'this task' we therefore construct different 'permtuatiosn' of the data-set (ie, to hold different 'asusmed' catoeriges of result-data).
      assert(false); // FIXME: ........ [above] .... for each 'k-ideal-data-set' call "standAlone__describeClusterCmpSimilarities_forDataSet__s_kt_assessPredictions_t(...)" .... and then 'manually' comapre the result of each heat-map: in an 'ideal case' we would expect the 'differnece in the heat-maps to be reflected in the ambiguity-difference in the data-sets'.
      assert(false); // FIXME: ........ [above] .... (try to) idenitfy different 'caterogies of the data-sets ... ie, explreo different data-combinatiosn.
      assert(false); // FIXME: ... 
      assert(false); // FIXME: support [ªbove]
      assert(false); // FIXME: support [ªbove]
      assert(false); // FIXME: [ªbove]
      assert(false); // FIXME: [ªbove]

      //! ---
      assert(false); // FIXME: 'add' a case where set "localConfig__isTo___computeKCluster = true"
      
      
      //! ---     -------------------------------------------------------------------------------------      
      //! ---     -------------------------------------------------------------------------------------      
      //! ---     -------------------------------------------------------------------------------------      

      //! ---     -------------------------------------------------------------------------------------      
      //!
      uint globalRowId__generalizedOutPrint = 0;
      const char *result_directory = "results/ccmCmp__dataSetsSmall/";
      { //! Budil for different noise-permutations:
	const uint npass_default = 1000;
	const char *stringOf_measurementSubset = "clusterCmpMetric_evaluate_influence_of_noise_in_cluster_metric_agreement";
	  
	//!	
	//! Build a comparison of differnet 'ranks' for the complete set of sytnetic-data-sets:
	const uint cnt_rowsToInsert = mapOf_functionStrings_base_size; //! ie, the total number of expected 'udpate-calls'.
	//! ---
	char string__metaFileUnique__allNoiseFixedAndDynamic__STD[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseFixedAndDynamic__STD, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseFixedAndDynamic_syntheticSets__STD");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseFixedAndDynamicdifferentSizes__STD = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseFixedAndDynamic__STD, result_directory );
	//! --- 
	char string__metaFileUnique__allNoiseFixedAndDynamic__Kurtosis[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseFixedAndDynamic__Kurtosis, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseFixedAndDynamic_syntheticSets__Kurtosis");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseFixedAndDynamicdifferentSizes__Kurtosis = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseFixedAndDynamic__Kurtosis, result_directory );
	//! --- 
	char string__metaFileUnique__allNoiseFixedAndDynamic__Skewness[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseFixedAndDynamic__Skewness, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseFixedAndDynamic_syntheticSets__Skewness");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseFixedAndDynamicdifferentSizes__Skewness = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseFixedAndDynamic__Skewness, result_directory );
	//! ------------------------ 
	char string__metaFileUnique__allNoiseDynamic__STD[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseDynamic__STD, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseDynamic_syntheticSets__STD");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseDynamicdifferentSizes__STD = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseDynamic__STD, result_directory );
	//! --- 
	char string__metaFileUnique__allNoiseDynamic__Kurtosis[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseDynamic__Kurtosis, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseDynamic_syntheticSets__Kurtosis");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseDynamicdifferentSizes__Kurtosis = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseDynamic__Kurtosis, result_directory );
	//! --- 
	char string__metaFileUnique__allNoiseDynamic__Skewness[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseDynamic__Skewness, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseDynamic_syntheticSets__Skewness");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseDynamicdifferentSizes__Skewness = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseDynamic__Skewness, result_directory );
	//! ------------------------ 
	char string__metaFileUnique__allNoiseFixed__STD[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseFixed__STD, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseFixed_syntheticSets__STD");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseFixeddifferentSizes__STD = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseFixed__STD, result_directory );
	//! --- 
	char string__metaFileUnique__allNoiseFixed__Kurtosis[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseFixed__Kurtosis, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseFixed_syntheticSets__Kurtosis");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseFixeddifferentSizes__Kurtosis = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseFixed__Kurtosis, result_directory );
	//! --- 
	char string__metaFileUnique__allNoiseFixed__Skewness[3000] = {'\0'}; sprintf(string__metaFileUnique__allNoiseFixed__Skewness, "nrowMin_%u.nrowMax_%u.dataSetName_%s.", (mapOf_functionStrings_base_size*nrows_base*1*numberOf_matrices_stepFactor), (mapOf_functionStrings_base_size*nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), "allNoiseFixed_syntheticSets__Skewness");
	s_kt_assessPredictions_differentMetricSets_t cmpResult_allNoiseFixeddifferentSizes__Skewness = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/cnt_rowsToInsert, string__metaFileUnique__allNoiseFixed__Skewness, result_directory );
	//! ------------------------ 


	for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {
	  const uint global_row_id = base_id;
	  //! --------------------- 
	  //!
	  //! Note: wrt. our "step(3...)" we ....??... 
	  //! 
	  //! Speciy the defualt correlaiton-meticd to use:
	  s_kt_correlationMetric_t corrMetric_prior = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3); //! ie, the 'ideal appraoch.
	  s_kt_correlationMetric_t corrMetric_insideClustering = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1); //! ie, the 'ideal appraoch.
	  const uint nclusters_ideal = mapOf_functionStrings_base__classificaiton[base_id].k_clusterCount;
	  assert( (nclusters_ideal != 0) && (nclusters_ideal != UINT_MAX) ); //! ie, as it otherwise seems like we have not 'yet' updated the ideal-cluster-count in our "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__inputDataToEvaluate.c" (eg, due to a new-added data-set), ie, for whcih the latter would 'need to be udpated'.
	  assert(nclusters_ideal > 0); //! ie, to avoid having a pointlless-clsuter-comparison-case	  	 

	  //! 
	  //! Application: "to quantify how well a cluster-comparison-metrics manages to correctly assign quality-ranks to gold-dat-asets with increased noise-ratios':
	  char string__metaFileUnique__eachDataSet[3000] = {'\0'}; sprintf(string__metaFileUnique__eachDataSet, "nrowMin_%u.nrowMax_%u.dataSetName_%s_both.", (nrows_base*1*numberOf_matrices_stepFactor), (nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), mapOf_functionStrings_base[base_id]);
	  char string__metaFileUnique__eachDataSet__fixed[3000] = {'\0'}; sprintf(string__metaFileUnique__eachDataSet__fixed, "nrowMin_%u.nrowMax_%u.dataSetName_%s_noiseFixed.", (nrows_base*1*numberOf_matrices_stepFactor), (nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), mapOf_functionStrings_base[base_id]);
	  char string__metaFileUnique__eachDataSet__dynamic[3000] = {'\0'}; sprintf(string__metaFileUnique__eachDataSet__dynamic, "nrowMin_%u.nrowMax_%u.dataSetName_%s_noiseDynamic.", (nrows_base*1*numberOf_matrices_stepFactor), (nrows_base*(numberOf_matrices-1)*numberOf_matrices_stepFactor), mapOf_functionStrings_base[base_id]);
	  assert(false); // FIXME: itnaite + 'iterator' + use + de-allcoate [”elow]
	  s_kt_assessPredictions_differentMetricSets_t cmpResult_differentSizes__noiseFixedAndDynamic = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/(numberOf_matrices*stringOf_noise_size*fraction_colNoise_cnt), string__metaFileUnique__eachDataSet, result_directory );
	  s_kt_assessPredictions_differentMetricSets_t cmpResult_differentSizes__noiseFixed = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/(numberOf_matrices*stringOf_noise_size*1), string__metaFileUnique__eachDataSet__fixed, result_directory );
	  s_kt_assessPredictions_differentMetricSets_t cmpResult_differentSizes__noiseDynamic = setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(/*global_cntMeasurements=*/(numberOf_matrices*1*fraction_colNoise_cnt), string__metaFileUnique__eachDataSet__dynamic,  result_directory);
	  /* s_kt_matrix_t obj_clusterResults__allCombinations__firstDataSet; setTo_empty__s_kt_matrix_t(&obj_clusterResults__allCombinations__firstDataSet); //! which is used as the 'data-matrix' wrt. the cluster-comparsion, ie, as the 'gold-case we evlaute'. */
	  /* s_kt_matrix_t obj_clusterResults__allCombinations; setTo_empty__s_kt_matrix_t(&obj_clusterResults__allCombinations); uint obj_clusterResults__allCombinations__currentrowIndex = 0; */


	  for(uint mult_factor = 1; mult_factor <= numberOf_matrices; mult_factor++) {
	    const uint nrows = nrows_base*mult_factor*numberOf_matrices_stepFactor;       
	    const uint ncols = nrows;  //! ie, to 'allow' the use of "_directScore_" corlreaiotn-emtirc in our evlauation.
	    //!
	    //! Note: wrt. our "step(2.a)" we are interested in 'collecting' different cluster-distributions, ie, where we (for the sperate sub-cases in [below]) build for [<cluster-base>][cluster-memberships]
	    //! Note: wehn we compare/evlaute the correctness of the clsuter-predictions we use the 'ideal result' as the input-matrix. This on order to reduce the compelxity wrt. the numbe rof altenraitves to evlauate', ie, for which our focused ins centered on the clsuter-results (rather than [clstuer-results]x[correlation-emtric]). In this evlauation we build different clsuter-collectioni-sets for the cases of [noiseFixed, noise-dynamic, noiseFixed--noise-dynamic]. The goal/hope is to factilate/enalbe the seperation/idnetificaiton of differnet  data-permtaution-cases, thereby 'providing' insight into itnrictiies of cluster-comparison-emtrics.	    
	    //! 
	    //! Application: from a comparison of different "mult_factor matrix-size" cases we are interested in idneitfying the signfance of noise-pertubrations in different [data-sets, matrix-dimesion] cases: our null-hyptoioesis is that "there will be disncint clusters cluster-comparison-metric-results, clusters which amy be seperated based on [noise-factor, data-set, matrix-size]". The latter null-hpyhteiss is based on the observaiton of the correlation-emtrics and cluster-comparison-metrics (ie, where different 'weights' are given to different traits in data-sets). The use-case-applicaiton (of the latter) conserns the dmosntrateion (ie, which we hpyhteiss) of interrpetiaotn-ambugities consernd with cluster-comparison: what we expect is taht the latter R(esults of our evlauation) may be used as a 'selecitoni-criteria to ensure non-bias wrt. comparison of results' (eg, to ensure/gurnatteee that the 'data-sets whioch are comapred iot. esstablish the relative superiroriy of a new-proposed cluster-algorithm is not due a biased sleection in the clustering-comaprsion-metircs').  <-- buidl WWW-heatMaps of "standAlone__describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__s_kt_assessPredictions_t(..)" and thereafter 'udpat this evlauation-text' ... and 'translate' this into an aritcle-text.
	  s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__step2__fixedDataAndDim.c"
	  }
	  //! ---------------------------------------
	  //! 
	  //! What we expect:
	  assert(cmpResult_differentSizes__noiseFixedAndDynamic.global_cntMeasurements_currentPos > 0);
	  assert(cmpResult_differentSizes__noiseFixedAndDynamic.global_cntMeasurements_size > 0);
	  //! --
	  assert(cmpResult_differentSizes__noiseFixed.global_cntMeasurements_currentPos > 0);
	  assert(cmpResult_differentSizes__noiseFixed.global_cntMeasurements_size > 0);
	  //! --
	  assert(cmpResult_differentSizes__noiseDynamic.global_cntMeasurements_currentPos > 0);
	  assert(cmpResult_differentSizes__noiseDynamic.global_cntMeasurements_size > 0);
	  //! ---------------------------------------
	  //! 
	  //! Apply logics: pre-step["rowEachData_colDifferentClusterDistributions.heatmap"]: 
	  uint column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_differentSizes__noiseFixedAndDynamic,
												   &cmpResult_allNoiseFixedAndDynamicdifferentSizes__STD,
												   &cmpResult_allNoiseFixedAndDynamicdifferentSizes__Skewness,
												   &cmpResult_allNoiseFixedAndDynamicdifferentSizes__Kurtosis, 
												   global_row_id, stringOf_measurementSubset, /*column_start_pos=*/0, NULL);
	  //! --- 
	  column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_differentSizes__noiseFixed,
											      &cmpResult_allNoiseFixeddifferentSizes__STD,
											      &cmpResult_allNoiseFixeddifferentSizes__Skewness,
											      &cmpResult_allNoiseFixeddifferentSizes__Kurtosis,
											      global_row_id, stringOf_measurementSubset, column_start_pos, NULL);
	  //! --- 
	  column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_differentSizes__noiseDynamic,
											      &cmpResult_allNoiseDynamicdifferentSizes__STD,
											      &cmpResult_allNoiseDynamicdifferentSizes__Skewness,
											      &cmpResult_allNoiseDynamicdifferentSizes__Kurtosis,
											      global_row_id, stringOf_measurementSubset, column_start_pos, NULL);
	  
	  //! ---------------------------------------
	  //! 
	  //! De-allocate:
	  free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_differentSizes__noiseFixedAndDynamic);
	  free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_differentSizes__noiseFixed);
	  free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_differentSizes__noiseDynamic);
	}

	//! ---------------------------------------
	//! 
	//! Correlate and 'write out' 
	uint column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixedAndDynamicdifferentSizes__STD,      NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/0, "noiseFixedAndDynamic_STD");
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixedAndDynamicdifferentSizes__Kurtosis, NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseFixedAndDynamic_Kurtosis");
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixedAndDynamicdifferentSizes__Skewness, NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseFixedAndDynamic_Skewness");
	//! ---
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseDynamicdifferentSizes__STD,       NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseDynamic_STD");
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseDynamicdifferentSizes__Kurtosis,  NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseDynamic_Kurtosis");
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseDynamicdifferentSizes__Skewness,  NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseDynamic_Skewness");
	//! ---
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixeddifferentSizes__STD,     NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, column_start_pos,  "noiseFixed_STD");
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixeddifferentSizes__Kurtosis,NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseFixed_Kurtosis");
	column_start_pos = correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixeddifferentSizes__Skewness,NULL, NULL, NULL, globalRowId__generalizedOutPrint, stringOf_measurementSubset, /*column_start_pos=*/column_start_pos, "noiseFixed_Skewness");
	

	//! ---------------------------------------
	//! 
	//! De-allocate:
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixedAndDynamicdifferentSizes__STD);
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixedAndDynamicdifferentSizes__Kurtosis);
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixedAndDynamicdifferentSizes__Skewness);
	//! ---- 
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseDynamicdifferentSizes__STD);
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseDynamicdifferentSizes__Kurtosis);
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseDynamicdifferentSizes__Skewness);
	//! ---- 
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixeddifferentSizes__STD);
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixeddifferentSizes__Kurtosis);
	free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(&cmpResult_allNoiseFixeddifferentSizes__Skewness);
	//! ---- 
      }



      assert(false); // FIXME: describe the different matrix-cases which we evluate. 
      assert(false); // FIXME: move [below] into a sperate file .. and use a 'new parameter' where we 'enable only to ifner/get teh 'count of each matrix-case'.
      assert(false); // FIXME: in [below] add a loop where we iterate/traverse wrt. the different 'real-life data-sets'.
      assert(false); // FIXME: complete [”elow] 'logics'.

      for(uint mult_factor = 1; mult_factor <= numberOf_matrices; mult_factor++) {
	const uint nrows = nrows_base*mult_factor*numberOf_matrices_stepFactor;       
	const uint ncols = nrows;  //! ie, to 'allow' the use of "_directScore_" corlreaiotn-emtirc in our evlauation.
	for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {

	  assert(false); // FIXME: 'use' a seperate case where we 'build' a matrix for seperate "base_id" case

	  for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
	    //! Budil the sample-string:
	    char stringOf_sampleData_type[2000]; sprintf(stringOf_sampleData_type, "%s%s", mapOf_functionStrings_base[base_id], stringOf_noise[noise_id]);
	    if( (noise_id > 0) && (NULL == strstr(stringOf_sampleData_type, "lines-"))) { continue;} //! ie, to avoid 'calls which produce the same result as "noise_id == 0".



	    for(uint fraction_colNoise = 0; fraction_colNoise < fraction_colNoise_cnt; fraction_colNoise++) {
	      s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
	      //! --
	      const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	      const uint fractionOf_toAppendWith_sampleData_typeFor_columns = 20*fraction_colNoise;
	      const char *stringOf_sampleData_type_realLife = NULL;	      
	      const bool isTo_transposeMatrix = false;
	      s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
if(obj_baseToInclude.nrows) {
  for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
    //assert(obj_baseToInclude.matrix[row_id]);
    assert(row_id < obj_baseToInclude.nrows);
    assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
    const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
    //if(row) 
    //{
    assert(row);
  }
 }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
		
		
	      //! ----------------------------------------------
	      //!
	      //! 
	      //! Evaluate each of the cases:
	      for(uint enum_clusterResultAsInput_index = 0; enum_clusterResultAsInput_index < (uint)e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef; enum_clusterResultAsInput_index++) {
		const e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t enum_clusterResultAsInput = (e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t)enum_clusterResultAsInput_index;
		const char *stringOf_enum = NULL;
	 	if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__all) {stringOf_enum = "all";}
		else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__setBased) {stringOf_enum = "setBased";}
		else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased) {stringOf_enum = "matrixBased";}
		else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__subset_extremeCases) {stringOf_enum = "matrixBased_andHpLysisCorrMetrics_extreme";}
		else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__all) {stringOf_enum = "matrixBased_andHpLysisCorrMetrics_all";}
		else {assert(false);} //! ie, as we then need to add support 'for this'.
		  
		assert(stringOf_enum);
		char stringOf_resultPrefix__sub[2000] = {'\0'}; sprintf(stringOf_resultPrefix__sub, "%s.%s.noise_%u.%s", 
									(stringOf_sampleData_type_realLife) ? stringOf_sampleData_type_realLife : "_",
									(stringOf_sampleData_type) ? stringOf_sampleData_type : "_",
									fraction_colNoise,
									stringOf_enum);;  
		//! -----------------------------------------------------
		//! -----------------------------------------------------
		//! 
		//! Apply the logics:
		  
		//! ----------------------------------------------
		//!
		//! 
		//! Indiety the clsuter-membership
		if(localConfig__isTo___computeKCluster) {
		  //! Apply k-means-clustering:
		  assert(false); // FIXME: first 'build' a locacl 'map of k-count' for each 'of these combinations'. <-- consider to 'buidl a k-ideal-case sepately for each "math-sysntietc" and each "real-life" data-set.
		  assert(false); // FIXME: add something
		  assert(false); // FIXME: update our results wrt.: ... 
		} else { //! then we are intersted in 'generating' a heat-maps seprately for each of the [”elow] emtrics:
		  assert(false); // FIXME: add something		  
		  assert(false); // FIXME: update our results wrt.: ... 
		}
	      }
	      //! ----------------------------------------------
	      //!
	      //! De-allocate:
	      free__s_kt_matrix(&obj_matrixInput);
	    }
	  }
	}
      }
      assert(false); // FIXME: add soemthing.

      assert(false); // FIXME: consider to build a web-page of 'static images' ... which we use to 'desribe the differnet clasiifcioatn-results' ... ie, to 'simplify the alignments of images'.

      assert(false); // FIXME: iterate+'include' a list of different 'real-life data-sets' ...

      assert(false); // FIXME: 'use' a simlair wrapper as in "kt_assessPredictions__stub__clusterMetricCmp__main__wrapper.c" iot. 'evlaute' the effect of different 'extrmee' corrleation-metric-strategeis wrty. construction of k-means clusters' (ie, 3*3=9x 'extra' cases).... and thereafter 'apply a visual inspection to hypothese cluster-quality of each metric' ... 

    
      assert(false); // FIXME: wr.t [ªbove] try to address the 'issue' wrt. how we may idneitfy/elvuat the 'boundary of uncertainty/exactness wrt. a adats-et-prediction (eg, 'how large a Silhouette-score shoudl be in order to describe a signficant difference in results').
      assert(false); // FIXME: update [ªbove] ... write a 'simple evlauation' wrt. "similartiy in cluster-predictions for/using the complete set of correlation-metrics" <-- update our "k-means proceudre wrt. this" and then 'for each distance-matrix for a particular data-set compare the new-inferred/correlated data-matrix to the ideal-MINE-inferred-cluster' using "standAlone__describeClusterCmpSimilarities_forDataSet__s_kt_assessPredictions_t(..)"
      assert(false); // FIXME: update [ªbove] ... 
      assert(false); // FIXME: how may we 'compare non-dieal k-speciricaitons' with 'ideal k-specificaitonis'? <-- consider writign+calling a 'matrix-based-cluster-comparisn-metirc' ... 
    }


    //! ------------------------
    cnt_Tests_evalauted++;
  }
  //! --------------------------------------------------------------------------------------
  //! --------------------
  if(isTo_processAll || (0 == strncmp("findExtremeCases-step1", array[2], strlen("findExtremeCases-step1"))) ) {    

    //! --------------------
    assert(false); // FIXME[test-MINE-implemnetaiton]: write a test-case where we comptue ["Euclid", "Pearson", "Spearman", "Kendall", "MINE-TIC", "Shannon", "Wavge-Hedges"] for ['syentic data-set with well-defined clsuters', 'a real-life data-sets where Perason does Not manage to dientify the clusters'] ... as a prioer/first step idneitfy the Gold-standard (for each data-set) using MINE and a 'k-cluster' defined by prior knowledge of the data-set (eg, using hpLysis web-based visualziation to hypothese dsitributions), and where we compare the 'ideal k-suggest to our different algorithm-appraoches for predicting an dieal k-value' .... use the Gold-standard to copmtue non-set-based-clsuter-comparsion-metics (eg, "Silhhoutte-index") based-on/using the distance-matrices inferred from each of hpLysiss correlaiton-metrics ... tehreafter apply k-means-clustering (first for "Euclid" in the k-means itself, and thereafter testing/evlauating 'for all'), and compare the metric-accuracy to 'the assumption dervied/hypthesied from the before-clsuter-comparsin-appraoch' 
    assert(false); // FIXME[corr-metric::ex_kMeans::Vilje]: ... write a new function where we: compare the result of all correlation-metrics to the/a gold-standard-clustering using [Silhhoutte, Dunn, ...] ... and then 'extract meaning from the latter (ie, result-export)' through/using .....??....
    assert(false); // FIXME[corr-metric::ex_kMeans::Vilje]: ... consider to 'apply different degrees of noise (eg, random permtuations) to the gold-standard' ... and then use our "findExtremeCases-step0" to identify the cases 'which signficantly deiveres' <-- frist ty to outline/improve desciption of how 'the result from such an analsysis may be used to interpret/weight the cluster-resulsts (eg, wrt. the signficance of each score dervied/copmtued from a given clsuter-comparisn-meitrc)'
    assert(false); // FIXME[corr-metric::ex_kMeans::Vilje]: extend above ... using [none, {corr-metrics}]x[rank-filter=[none, low, medium, high]x[Euclid, CityBlock, Pearson, {Mine}]x[k-means-permtautions] ... and for each row 'write out different metric-scores assicated to each cluster-permtatuion'.
    //! -----------------
    assert(false); // FIXME[JC-data-set]: implmenet/include JCs code for 'image import' and 'image export' .... and update our "kt_matrix.h" wrt. the latter ... and relate 'this' to [above] use-cases'.
    assert(false); // FIXME[JC-data-set]: 'apply' [above] appraoch to JC-data-set ... and try to describe the results.    
    //! ------------------------------------------------------------- 
    assert(false); // FIXME[corr-metric::ex_kMeans::Vilje]: add a new 'test-case' (where a file-name may optionally be sued as param): first build an 'ideal data-set based on a simple matrix-dision into tiles', and thereafter compare a sample-matrix 'to the ideal case' ... compare the reuslts of [Mine, Euclid, Pearson] ... and write out (into a seprate file) the 'rank' of each metrix (when compared ot the 'ideal case').
    assert(false); // FIXME[corr-metric::ex_kMeans::Vilje]: write a new code-class-file where we combine MINE with Vilje-data-set and k-means clsuters, where we write out the Rand-index-score=[AVG, STD] 'for different runs using the same combination' ... where each 'row represents a combination of parameters' ... and 'in a seperate file' include a 'ranking of the worst combiantions and best combinations'. <-- consdier to inlcude 'into the parameter-space' different 'k-cluster settings' (eg, k=4, k=8, ....), and then 'use the Euclidaion distnace-proeprty to sub-divide the matrix into euqal-sizes tiles, ie, where the latter tiles are used as a gold-standard'

    //! Indentify the 'best' and 'worst' metrics for data-sets using a syntetic-data-set as input
    //! Result: a list of top-scoring metrics for each 'inptu-data-set'.
    assert(false); // FIXME: write a new test-example-file named ....??.... where we 'accept as parameters' ["array[2] = result-format (ie, purpose of study)", "array[3] = fileName"] ... and use the latter as a tmepalte wrt. JCs Vilje-data-sets (eg, for the special/rare case where we get as input an adjcency-matrix of cluster-memberships: a use-case-applciation-case for the latter conserns .....??...).
    assert(false); // FIXME: try to build/identify/hyptohese a collection of data-sets hich 'results' in 'each metric being the top-ranked metric in at least one data-set' <-- try improving/expalining/outlining the latter.
    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  //! --------------------
  if(isTo_processAll || (0 == strncmp("findExtremeCases-step2", array[2], strlen("findExtremeCases-step2"))) ) {
    // ------------------------------------------
    assert(false); // FIXME: 'xpeand' [above] 'gold-stadnard-comparison' ... use a 'squared comparison' to compare the 'orderings' of the different cluster-metrics ... apply k-means-clsutering ... and thereafter exprot the cluster-result-data
    // --------------
    assert(false); // FIXME: ... write/include a 'similard gold-standard' using a larger hard-coded-example-data-set ... and test/inveigate if 'increased clsuter-sizes may improve the accuracy of the Rand-permtaution-indexes
    assert(false); // FIXME: validate correctness of [ªbove]. <-- we expect the best cases to be ...??... ... and simliartly the worst-cases to be ...??....
    assert(false); // FIXME: ... try to 'increase the signficance of the different cases .... use a distance-matrix ... and diffenret cluster--distMAtrix-permtaution .... where we evlauate corr-metrics which 'cotnradtics' the cluster-caterogization
    // --------------
    assert(false); // FIXME: write out the data as an image .... using JCs image-export-code.
    // --------------       
    assert(false); // FIXME: comapre the cases ... where we have the 'same clusters through different max-cluster-counts' <-- how do we 'comapre this'?

    //! Cluster the different cluster-comparison-metricds by .....??.....
    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  /* //! -------------------- */
  /* if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) { */

  /*   assert(false); // FIXME: add something */

  /*   cnt_Tests_evalauted++; */
  /* } */
  /* //! -------------------- */
  /* if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) { */

  /*   assert(false); // FIXME: add something */

  /*   cnt_Tests_evalauted++; */
  /* } */


  assert(false); // FIXME: cosnider making use of "/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/external_cpy/k_part_colex.c" downloaded from "http://www.aconnect.de/friends/editions/computer/combinatoricode_e.html" in order t to extend/expand our data-set-analsyssi <-- how do we 'construct represnetative cluster-allcoations which may be paritioned'?

  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }

  //! ----------------------------------------
  //! 
  //! De-allocate:
  free__s_kt_longFilesHandler_t(fileHandler__global);  fileHandler__global__ = *fileHandler__global;

}
