#ifndef matrix_transpose_h
#define matrix_transpose_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file matrix_transpose
   @brief provide logics for transpsoing a matrix
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/

#include "kt_mathMacros.h"
#include "log_clusterC.h"


//! Tranpsoes a t_float-matrix.
void matrix__transpose__compute_transposedMatrix_float(const uint nrows, const uint ncolumns, t_float **data, t_float **resultMatrix, const bool isTo_useIntrisinistic/* = true*/);
//! Tranpsoes a char-matrix.
void matrix__transpose__compute_transposedMatrix_char(const uint nrows, const uint ncolumns, char **data, char **resultMatrix, const bool isTo_useIntrisinistic/* = true*/);
void matrix__transpose__compute_transposedMatrix_uint(const uint nrows, const uint ncolumns, uint **mul1, uint **res, const bool isTo_useIntrisinistic/* = true*/);


  /**
      @brief Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
      @param <nrows> is the number of rows in data, ie, the number of columns in resultMatrix.
      @param <ncolumns> is the number of columns in data, ie, the number of rows in resultMatrix
      @param <data> is the input-meatrix to transpose.
      @param <mask> if set if the mask-matrix to transpose.
      @param <resultMatrix> is the matrix which 'contain' the inverse data-set.
      @param <resultMatrix_mask> is the char-matrix which 'contain' the inverse data-set: only updated if data is set.
      @param <isTo_useIntrisinistic> which if set to false implies that we use default C lanauge-specific operations: mainly included for benchmarking.
   **/
  void matrix__transpose__compute_transposedMatrix(const uint nrows, const uint ncolumns, t_float **data, char **mask, t_float **resultMatrix, char **resultMatrix_mask, const bool isTo_useIntrisinistic/* = true*/);


//! @return a enw-allcoated transpoed row from an input-matrix (oekseth, 06. setp. 2016)
t_float *get_transposed_row_fromMatrixFloat__matrix_transpose(const uint index1, const uint nrows, const uint ncolumns, t_float **data);

#endif //! EOF
