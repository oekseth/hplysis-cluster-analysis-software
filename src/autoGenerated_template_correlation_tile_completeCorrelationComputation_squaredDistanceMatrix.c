
//!
//! Build for euclid:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__euclid; }
//!
//! Build for cityBlock:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__cityBlock; }
//!
//! Build for minkowski:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski; }
//!
//! Build for minkowski__subCase__zeroOne:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__zeroOne; }
//!
//! Build for minkowski__subCase__pow3:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__pow3; }
//!
//! Build for minkowski__subCase__pow4:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__pow4; }
//!
//! Build for minkowski__subCase__pow5:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__pow5; }
//!
//! Build for chebychev:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_chebychev) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__chebychev; }
//!
//! Build for chebychev_minInsteadOf_max:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__chebychev_minInsteadOf_max; }
//!
//! Build for Sorensen:
//! @remarks the 'Sorensen' metric is widely used in ecology: a different name for the 'Sorensen' metric is the 'Bray-Curtis' metric. 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Sorensen; }
//!
//! Build for Gower:
//! @remarks Gower differs from Chebychev with respect to the normalization of the entites signficance in the data-set, ie, (1/d). In the 'default' verison we assume that the error-rate is set to zero, ie, as explained in the work of Gower 	tep{http://cbio.mines-paristech.fr/~jvert/svn/bibli/local/Gower1971general.pdf}.  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Gower) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Gower; }
//!
//! Build for Soergel:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Soergel) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Soergel; }
//!
//! Build for Kulczynski_absDiff:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Kulczynski_absDiff; }
//!
//! Build for Canberra:
//! @remarks difference between 'Canberra' and 'Sorensen' conserns how 'Canberra' normalizes the absoltue difference at the individual level. 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Canberra) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Canberra; }
//!
//! Build for Lorentzian:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Lorentzian; }
//!
//! Build for oekseth_forMasks_mean:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean; }
//!
//! Build for oekseth_forMasks_mean_standardDeviation_userDefinedPower:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_userDefinedPower; }
//!
//! Build for oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne; }
//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow2:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow2; }
//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow3:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow3; }
//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow4:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow4; }
//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow5:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow5; }
//!
//! Build for intersection:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_intersection) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__intersection; }
//!
//! Build for intersectionInverted:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_intersection_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__intersectionInverted; }
//!
//! Build for WaveHedges:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_WaveHedges) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__WaveHedges; }
//!
//! Build for WaveHedges_alt:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__WaveHedges_alt; }
//!
//! Build for Czekanowski:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Czekanowski) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Czekanowski; }
//!
//! Build for Czekanowski_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Czekanowski_altDef; }
//!
//! Build for Motyka:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Motyka) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Motyka; }
//!
//! Build for Motyka_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Motyka_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Motyka_altDef; }
//!
//! Build for Kulczynski:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Kulczynski) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Kulczynski; }
//!
//! Build for Ruzicka:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Ruzicka) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Ruzicka; }
//!
//! Build for Tanimoto:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Tanimoto) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Tanimoto; }
//!
//! Build for Tanimoto_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Tanimoto_altDef; }
//!
//! Build for innerProduct:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_innerProduct) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__innerProduct; }
//!
//! Build for harmonicMean:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_harmonicMean) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__harmonicMean; }
//!
//! Build for cosine:
//! @remarks The 'cosine' metric (or: 'angular metric')  measure the angle between two vectors 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_cosine) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__cosine; }
//!
//! Build for HumarHasseBrook_PCA_or_Jaccard:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__HumarHasseBrook_PCA_or_Jaccard; }
//!
//! Build for Dice:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Dice) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Dice; }
//!
//! Build for Jaccard_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Jaccard_altDef; }
//!
//! Build for Dice_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Dice_altDef; }
//!
//! Build for sampleCoVariance:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__sampleCoVariance; }
//!
//! Build for Brownian:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Brownian; }
//!
//! Build for Brownian_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Brownian_altDef; }
//!
//! Build for fidelity:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_fidelity) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__fidelity; }
//!
//! Build for Bhattacharyya:
//! @remarks the metric is adjusted in order to 'handle' empty valeus by taking the 'abs' of the v*u product, an appraoch also applifed for 'fidelity', 'Hellinger', 'Matusitat' and 'squared chord'; in order to simplify the indiciation of special cases 'using' 'abs(..)' in combiaton with the 'sqrt(..)' funciton please search for all applications of the 'mathLib_float_sqrt_abs(..)' call in the soruce-code of hpLysis. 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Bhattacharyya; }
//!
//! Build for Hellinger:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Hellinger) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Hellinger; }
//!
//! Build for Matusita:
//! @remarks Matusita 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Matusita) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Matusita; }
//!
//! Build for Hellinger_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Hellinger_altDef; }
//!
//! Build for Matusita_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Matusita_altDef; }
//!
//! Build for squaredChord:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_squaredChord) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__squaredChord; }
//!
//! Build for squaredChord_altDef:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__squaredChord_altDef; }
//!
//! Build for Euclid_squared:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Euclid) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Euclid_squared; }
//!
//! Build for Pearson_squared:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_squared; }
//!
//! Build for Neyman:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Neyman) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Neyman; }
//!
//! Build for squaredChi:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_squaredChi) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__squaredChi; }
//!
//! Build for probabilisticChi:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_probabilisticChi) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__probabilisticChi; }
//!
//! Build for squared_divergence:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_divergence) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__squared_divergence; }
//!
//! Build for Clark:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Clark) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Clark; }
//!
//! Build for addativeSymmetricSquaredChi:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__addativeSymmetricSquaredChi; }
//!
//! Build for Pearson_productMoment_generic:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_generic; }
//!
//! Build for Pearson_productMoment_absolute:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_absolute; }
//!
//! Build for Pearson_productMoment_uncentered:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_uncentered; }
//!
//! Build for Pearson_productMoment_uncenteredAbsolute:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_uncenteredAbsolute; }
//!
//! Build for Kullback_Leibler:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_shannon_KullbackLeibler) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Kullback_Leibler; }
//!
//! Build for Jeffreys:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_shannon_Jeffreys) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Jeffreys; }
//!
//! Build for kDivergence:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_shannon_kDivergence) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__kDivergence; }
//!
//! Build for Topsoee:
//! @remarks A different name for the 'Topsoee' metric is 'information theory'. 
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_shannon_Topsoee) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Topsoee; }
//!
//! Build for JensenShannon:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_shannon_JensenShannon) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__JensenShannon; }
//!
//! Build for JensenDifference:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_shannon_JensenDifference) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__JensenDifference; }
//!
//! Build for Taneja:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_combinations_Taneja) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__Taneja; }
//!
//! Build for KumarJohnson:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_combinations_KumarJohnson) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__KumarJohnson; }
//!
//! Build for averageOfChebyshevAndCityblock:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__averageOfChebyshevAndCityblock; }
//!
//! Build for VicisWaveHedges:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__VicisWaveHedges; }
//!
//! Build for VicisWaveHedgesMax:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__VicisWaveHedgesMax; }
//!
//! Build for VicisWaveHedgesMin:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__VicisWaveHedgesMin; }
//!
//! Build for VicisSymmetric:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__VicisSymmetric; }
//!
//! Build for VicisSymmetricMax:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__VicisSymmetricMax; }
//!
//! Build for downwardMutability_symmetricMax:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__downwardMutability_symmetricMax; }
//!
//! Build for downwardMutability_symmetricMin:
//! @remarks  
     if( (typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin) ) { return &kt_computeForMetric_postProcessTemporalCorrelationScores__downwardMutability_symmetricMin; }