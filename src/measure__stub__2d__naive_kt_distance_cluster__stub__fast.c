{
  for(uint dist_enum_id = 0; dist_enum_id < e_kt_correlationFunction_undef; dist_enum_id++) {
    assert(e_kt_categoryOf_correaltionPreStep_none == 2); //! ie, waht we expect wrt. [below] "3" threshold
    for(uint dist_enum_preStep = 0; dist_enum_preStep < 3; dist_enum_preStep++) {
      for(uint clusterMetric_index = 0; clusterMetric_index < mapOf_clusterCmpMetrics_size; clusterMetric_index++) {
	for(char config_weight = 0; config_weight < 2; config_weight++) {
	  for(char config_mask = 0; config_mask < 2; config_mask++) {
	    //for(char config_useTwoMatricesAsInput = 0; config_useTwoMatricesAsInput < 2; config_useTwoMatricesAsInput++) {
	    for(char config_transpose = 0; config_transpose < 2; config_transpose++) {
	      for(uint fraction_index  = 1; fraction_index < mapOf_clusterFactions_size; fraction_index++) {
		//for(char config_useFast = 0; config_useFast < 2; config_useFast++) {
		//if(config_useFast && (for_fastVersion == false) ) {continue;} //! ie, as we then assume taht the 'fast verison' si not of interst.
		assert(fraction_index != 0); //! ie, to detect any future ernrorus changes/code-mofticioants.
		const t_float fractionIn_n1 = 1/(t_float)fraction_index; //! which is used to ivnestigate the tiem-dfifernece for different combinations block-sizes.
		//! -------------
		assert(fractionIn_n1 >= 1);   assert(fractionIn_n1 < 1);
		const uint n1 = nrows * fractionIn_n1;    const uint n2 = nrows * ( 1- fractionIn_n1);
		const uint default_value_uint = 1;
		uint *mapOf_index1 = allocate_1d_list_uint(n1, default_value_uint); __localConfig__constructIndex1();
		uint *mapOf_index2 = allocate_1d_list_uint(n2, default_value_uint); __localConfig__constructIndex2();
		
		char **local_mask = mask1;
		//t_float *local_weight = weight;
		t_float **local_matrix_1 = matrix;  // t_float **local_matrix_2 = matrix;
		if(config_transpose == true) {
		  local_matrix_1 = matrix_transposed; //local_matrix_2 = matrix_transposed;
		}
		t_float *local_weight = NULL;
		if(config_weight) {local_weight = weight;}
		char **local_mask1 = NULL; 	//      char **local_mask2 = NULL;
		bool masks_isAllocated = false;
		if(config_mask == 1) {
		  if(config_transpose == false) {
		    local_mask1 = mask1; //local_mask2 = mask2;
		  } else {
		    masks_isAllocated = true;
		    local_mask1 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
		    // local_mask2 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
		  }
		}
		
		//! -------------------------------------------
		//! Generate the measurement-test:
		char stringOf_measureText[2000]; memset(stringOf_measureText, '\0', 2000);
		sprintf(stringOf_measureText, "%s: correlation-metric: \"%s -- %s\" AND cluster-metric: %s; transpose='%s', use-weight='%s', mask-type='%s', fraction-of-entites-in-first-cluster=%.3f", 
			stringOf_measureText_base, 
			get_stringOf_enum__e_kt_correlationFunction_t((e_kt_correlationFunction_t)dist_enum_id),
			get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t((e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep),
			mapOf_clusterCmpMetrics_stringOfDesc[clusterMetric_index],
			(config_transpose) ? "true" : "false",
			//(config_useFast) ? "true" : "false",
			(config_weight) ? "true" : "false",
			(config_mask == 1) ? "maskExplicit" :  "mask-none",
			fractionIn_n1
			);


		//! 
		//! Start the clock:
		start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
		//!
		//!
		//! The experiemnt:
		/* const char dist = mapOf_slowMetricsDistance[dist_index];	 */	
		const char method = mapOf_clusterCmpMetrics[clusterMetric_index];

		/* //! Get the 'full-scale' cofnigurations fromt eh 'naive' "dist" distance-metirc-pseicficiaotn: */
		e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)dist_enum_id; e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;
		/* get_enumParams_fromBackCompaitbleConfigFor_clusterC_distanceSpec(dist, &dist_metric_id, &dist_metricCorrType); */
		//! Teh call:
		clusterdistance__extensiveParams(dist_metric_id, dist_metricCorrType, nrows, ncols, local_matrix_1, local_mask1, local_weight, n1, n2, mapOf_index1, mapOf_index2, method, config_transpose, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_invertMatrix_transposed=*/true, /*CLS=*/64, /*isTo_useImplictMask=*/false);
		/* } else { */
		/*   const t_float score_result = clusterdistance_slow(nrows, ncols, local_matrix_1, local_mask1, local_weight, n1, n2, mapOf_index1, mapOf_index2, dist, method, config_transpose, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*isTo_invertMatrix_transposed=*\/false); */
		/* } */
		
		//! --------------
		__assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
				
		//! ----------------------------
		//! De-allcoate:
		free_1d_list_uint(&mapOf_index1);   free_1d_list_uint(&mapOf_index2);
		if(masks_isAllocated) {
		  free_2d_list_char(&local_mask1, size_of_array);
		  //free_2d_list_char(&local_mask2);
		}
	      }
	      //}
	    }
	  }
	}
      }
    }
  }
#undef __localConfig__constructIndex1
#undef __localConfig__constructIndex2
}

