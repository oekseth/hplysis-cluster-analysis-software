
      { //! 'Empty' the current oflder:
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path); assert(is_ok);}
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(resultDir_path__noPreProcess); assert(is_ok);}
      }

    //!
    //! Opent ehe file-descitor:
    FILE *fileDesriptorOut__clusterResults = NULL; 
    { //! Open the file used for writing of 'named' cluster-emberships':      
      fileDesriptorOut__clusterResults = fopen(nameOf_resultFile__clusterMemberships, "wb");
      if(!fileDesriptorOut__clusterResults) {
	fprintf(stderr, "!!\t Unable to open the result-file=\"%s\": pelase vlaidate correctenss of both path, disk-space and dist-write-permissions. Observaiton at [%s]:%s:%d\n", nameOf_resultFile__clusterMemberships, __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up. 
      }
    }
    //!
    //! 
    const uint groupMedium__cnt_simMetrics = e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef; assert(groupMedium__cnt_simMetrics >= 1);
    uint groupMedium__cnt_dataSets = 0;
    { //! Idnetify the count of data-sets to evalaute:
      for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
	for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {
	  const s_classOf_dataSet obj_dataConfig = mapOf_functionStrings_base__classificaiton[base_id];
	  const bool isTo_evaluate = __Mi__objIsOfInterest(obj_dataConfig);
	  if(isTo_evaluate) {groupMedium__cnt_dataSets++;}
	}
      }
      for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {	  
	const bool isTo_evaluate = __Mi__objIsOfInterest(mapOf_realLife_classificaiton[data_id]);
	if(isTo_evaluate) {groupMedium__cnt_dataSets++;}
      }
      assert(groupMedium__cnt_dataSets > 1);
    }
    //!
    //! Allocate 
    //! Note[use-case]: ... 
    s_kt_matrix_t matResult__execTime__min = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    s_kt_matrix_t matResult__execTime__max = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    s_kt_matrix_t matResult__execTime__avg = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    //! Note[use-case]: ... 
    s_kt_matrix_t matResult__execTimeSimAndCluster__min = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    s_kt_matrix_t matResult__execTimeSimAndCluster__max = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    s_kt_matrix_t matResult__execTimeSimAndCluster__avg = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    //! Note[use-case]: ... 
    s_kt_matrix_t matResult__ccm__min = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    s_kt_matrix_t matResult__ccm__max = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    s_kt_matrix_t matResult__ccm__avg = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    { //! Include/set the column-names: 
      uint local_colPos = 0;
      for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
	for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {
	  const s_classOf_dataSet obj_dataConfig = mapOf_functionStrings_base__classificaiton[base_id];
	  const bool isTo_evaluate = __Mi__objIsOfInterest(obj_dataConfig);
	  if(isTo_evaluate) { //! then add the name:
	    const char *data_tag = obj_dataConfig.tag;
	    //! ----------------------------------------------------------------------------------------------
	    set_stringConst__s_kt_matrix(&matResult__execTime__min, local_colPos, data_tag, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&matResult__execTime__max, local_colPos, data_tag, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&matResult__execTime__avg, local_colPos, data_tag, /*addFor_column=*/true);
	    //! --- 
	    set_stringConst__s_kt_matrix(&matResult__execTimeSimAndCluster__min, local_colPos, data_tag, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&matResult__execTimeSimAndCluster__max, local_colPos, data_tag, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&matResult__execTimeSimAndCluster__avg, local_colPos, data_tag, /*addFor_column=*/true);
	    //! --- 
	    set_stringConst__s_kt_matrix(&matResult__ccm__min, local_colPos, data_tag, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&matResult__ccm__max, local_colPos, data_tag, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&matResult__ccm__avg, local_colPos, data_tag, /*addFor_column=*/true);
	    //! ----------------------------------------------------------------------------------------------
	    //! Increment:
	    local_colPos++;	    
	  }
	}
      }
      for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {	  
	const bool isTo_evaluate = __Mi__objIsOfInterest(mapOf_realLife_classificaiton[data_id]);

      }
      assert(local_colPos == groupMedium__cnt_dataSets);
    }
    //! ----------------------------------------------------------------------------------------------------------------------------------------
    const e_kt_matrix_cmpCluster_categoriesOfMetrics_t ccm_category = e_kt_matrix_cmpCluster_categoriesOfMetrics_all; //getEnumCategory__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm);
    const uint ccm_category_cnt_cols = getCntIn__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm_category);
    //! Note[use-case]: find the ccmPredictions=[min, max] 'for the complete chunk of data', and then use the min--max observations/ctierias (for each CCM) both useCase(a) as an indicator of accuracy for the simlairty-metric-catgegory (and the clsuter-algorithm in question), and useCase(b) to depict/describe sensitivyt of CCMs (ie, as we expect a given data-set-chunk will have traits which in ideal condicitons will have a high degree of silairty to each other). 
    s_kt_matrix_t matResult__simXccm__min = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/ccm_category_cnt_cols);
    s_kt_matrix_t matResult__simXccm__max = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/ccm_category_cnt_cols);
    //! ---- 
    //! Note[use-case]: 
    s_kt_matrix_t matResult__simXccmSTD__min = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/ccm_category_cnt_cols);
    s_kt_matrix_t matResult__simXccmSTD__max = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/ccm_category_cnt_cols);
    s_kt_matrix_t matResult__simXccmAVG__min = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/ccm_category_cnt_cols);
    s_kt_matrix_t matResult__simXccmAVG__max = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/ccm_category_cnt_cols);
    { //! Include/set the column-names: 
      for(uint col_id = 0; col_id < ccm_category_cnt_cols; col_id++) {
	char str_local[100] = {'\0'}; sprintf(str_local, "ccm(%u)", col_id);
	//! ----------------------------------------------------------------------------------------------
	set_stringConst__s_kt_matrix(&matResult__simXccm__min, col_id, str_local, /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&matResult__simXccm__max, col_id, str_local, /*addFor_column=*/true);
	//! --
	set_stringConst__s_kt_matrix(&matResult__simXccmSTD__min, col_id, str_local, /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&matResult__simXccmSTD__max, col_id, str_local, /*addFor_column=*/true);
	//! --
	set_stringConst__s_kt_matrix(&matResult__simXccmAVG__min, col_id, str_local, /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&matResult__simXccmAVG__max, col_id, str_local, /*addFor_column=*/true);
	//! ----------------------------------------------------------------------------------------------	
      }
    }

    //! ----------------------------------------------------------------------------------------------------------------------------------------
    //s_kt_matrix_t matResult__ccm__std = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_simMetrics, /*cols=*/groupMedium__cnt_dataSets);
    
    //!
    //! Apply logics:
    uint cnt_counter_y = 0; uint cnt_counter_y__sub = 0;
    const uint fraction_colNoise = 0;
    //! Iterate through the different cluster-comparison-metrics: the post-process-types:
    for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
      for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	//! Iterate through the different cluster-comparison-metrics: the corrleation-emtrics themself:
	e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;
	uint cnt_counter_x = 0; 	uint cnt_counter_x__sub = 0; 	
	if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	   (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	   ||
#endif
 describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	   || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	   ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
	s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
	const uint nrows = 20;     const uint ncols = 20; 
	//!
	//! Set the simliarty/correlation matrix:
	s_kt_correlationMetric_t corrMetric_prior; init__s_kt_correlationMetric_t(&corrMetric_prior, dist_metric_id, dist_metricCorrType);
	//!
	//! Get the 'group' which the simlairty-metric is part of:
	const e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t metric_isInGroup = getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(corrMetric_prior);
	assert(metric_isInGroup != e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef); //! ie, what we expect.
	
	uint mapOf_time__pos = 0;
	const t_float empty_0 = 0;
	s_kt_matrix_t matResult__ccm__internal = initAndReturn__s_kt_matrix(/*rows=*/groupMedium__cnt_dataSets, /*cols=*/ccm_category_cnt_cols);	

	//!
	//! Sytrnetic iteration:
	for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
	  for(uint data_id = 0; data_id < mapOf_functionStrings_base_size; data_id++) {
	    const s_classOf_dataSet obj_dataConfig = mapOf_functionStrings_base__classificaiton[data_id];
	    const bool isTo_evaluate = __Mi__objIsOfInterest(obj_dataConfig);
	    if(isTo_evaluate) {
	      assert(obj_dataConfig.mapOf_vertexClusterId); //! ie, as we then expec tthe clsuter-mappigns to have b een set.
	      const char *data_tag = obj_dataConfig.tag;
	      //!
	      //! Configure:
	      char stringOf_sampleData_type[2000]; sprintf(stringOf_sampleData_type, "%s%s", mapOf_functionStrings_base[data_id], stringOf_noise[noise_id]);
	      //const char *stringOf_tagSample = mapOf_functionStrings_base[data_id]; const char *stringOf_sampleData_type = stringOf_tagSample;
	      const bool isTo_transposeMatrix = obj_dataConfig.isTo_transpose;
	      const char *stringOf_sampleData_type_realLife = NULL;
	      //!
	      //! Load data-set:
	      assert(stringOf_sampleData_type); 	    
	      assert(strlen(stringOf_sampleData_type));
	      s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
	      const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	      const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20*fraction_colNoise;
 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
	      //! 
	      //! Comupute various emasure and collec tthe results:
	      const char *data_prefix = "syntethic";
#include "measure_kt_matrix_cmpCluster__stub__clusterMatrix.c"
	    }
	  }
	}
	//!
	//! Simliarty for the real-life-data-set:
	for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {	  
	  const s_classOf_dataSet obj_dataConfig = mapOf_realLife_classificaiton[data_id]; //mapOf_functionStrings_base__classificaiton[base_id];
	 
	  const bool isTo_evaluate = __Mi__objIsOfInterest(obj_dataConfig);
	  //const bool isTo_evaluate = __Mi__objIsOfInterest(
	  if(isTo_evaluate) {
	    assert(obj_dataConfig.mapOf_vertexClusterId); //! ie, as we then expec tthe clsuter-mappigns to have b een set.
	    //if(mapOf_realLife_classificaiton[data_id].useInCondensedEvaluation) {
	    const char *data_tag = mapOf_realLife_classificaiton[data_id].tag;
	    const char *stringOf_tagSample = mapOf_realLife[data_id];
	    assert(stringOf_tagSample);
	    assert(strlen(stringOf_tagSample));
	    //!
	    //! Configure:
	    const char *stringOf_sampleData_type = NULL;
	    const char *stringOf_sampleData_type_realLife =stringOf_tagSample;
	    const bool isTo_transposeMatrix = mapOf_realLife_classificaiton[data_id].isTo_transpose;
	    //!
	    //! Load data-set:
	    assert(stringOf_sampleData_type_realLife); 	    
	    assert(strlen(stringOf_sampleData_type_realLife));
	    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
	    const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
	    const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20*fraction_colNoise;
 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
	    //! 
	    //! Comupute various emasure and collec tthe results:
	    const char *data_prefix = "realLife";
#include "measure_kt_matrix_cmpCluster__stub__clusterMatrix.c"
	  }
	}
	//! ------------------------------------------------------------------------------------------------------
	//! 
	{ //! Update our glboal data-contaienrs based on the data inserted in our "matResult__ccm__internal":
	  //! Note: in "matResult__ccm__internal" we expect each row=[ccm0, ccm1, ccm2, ccm3, ...]. Therefore, we 'need'/choose to transpose the matrix iot. 'capture' the avg and skewness between differetn data-sets.
	  // TODO: consider to comapre the input-matirx to an 'ideal matrix' ... and then evlauate the differences in rpedictiosn-simlairties ... eg, to 'rank' the different appraoches'.
	  s_kt_matrix_t mat_transposed; const  bool is_ok = init__copy_transposed__s_kt_matrix(&mat_transposed, &matResult__ccm__internal, /*isTo_updateNames=*/false); assert(is_ok);
	  assert(mat_transposed.ncols == matResult__ccm__internal.nrows);
	  assert(mat_transposed.nrows == matResult__simXccm__min.ncols); //! ie, what we expect.
	  assert(mat_transposed.nrows == matResult__simXccm__max.ncols); //! ie, what we expect.
	  assert(metric_isInGroup < matResult__simXccm__min.nrows);
	  //	  assert(metric_isInGroup == matResult__simXccm__min.nrows);
	  //!
	  //! Iterate:
	  for(uint ccm_id = 0; ccm_id < mat_transposed.nrows; ccm_id++) {
	    const t_float *row = mat_transposed.matrix[ccm_id]; assert(row);
	    //! 
	    //! Update the 'summary-tables' for the sim-metric-cateogires:
	    //! Note: idea (in [below]) is to find the ccmPredictions=[min, max] 'for the complete chunk of data', and then use the min--max observations/ctierias (for each CCM) both useCase(a) as an indicator of accuracy for the simlairty-metric-catgegory (and the clsuter-algorithm in question), and useCase(b) to depict/describe sensitivyt of CCMs (ie, as we expect a given data-set-chunk will have traits which in ideal condicitons will have a high degree of silairty to each other). 
	    for(uint data_id = 0; data_id < mat_transposed.ncols; data_id++) {
	      const t_float score = mat_transposed.matrix[ccm_id][data_id];
	      if( (score != T_FLOAT_MAX) && (score != 0) ) {
		//!
		//! Update the min-max-properties:
		matResult__simXccm__min.matrix[metric_isInGroup][ccm_id] = macro_min(matResult__simXccm__min.matrix[metric_isInGroup][ccm_id], score);
		if(matResult__simXccm__max.matrix[metric_isInGroup][ccm_id] != T_FLOAT_MAX) {
		  matResult__simXccm__max.matrix[metric_isInGroup][ccm_id] = macro_max(matResult__simXccm__max.matrix[metric_isInGroup][ccm_id], score);
		} else {matResult__simXccm__max.matrix[metric_isInGroup][ccm_id] = score;}
	      }
	    }

	    //! 
	    //!	    
	    //! Extract STD and avg from the 'lcoal matrix', store the reuslt in a glboal matrix, a matrix which is later exported to both "*.js" and "*.tsv":
	    s_matrix_deviation_std_row_t obj_dev = setToEmptyAndReturn__s_matrix_deviation_std_row();
	    //! Comptu the internal variance:
	    get_forRow_sampleDeviation__matrix_deviation_implicitMask(row, mat_transposed.ncols, &obj_dev);
	    //!
	    //! Update variance:
	    if( (obj_dev.variance != T_FLOAT_MAX) && (obj_dev.variance != 0) ) {
	      matResult__simXccmSTD__min.matrix[metric_isInGroup][ccm_id] = macro_min(matResult__simXccmSTD__min.matrix[metric_isInGroup][ccm_id], obj_dev.variance);
	      if(matResult__simXccmSTD__max.matrix[metric_isInGroup][ccm_id] != T_FLOAT_MAX) {
		matResult__simXccmSTD__max.matrix[metric_isInGroup][ccm_id] = macro_max(matResult__simXccmSTD__max.matrix[metric_isInGroup][ccm_id], obj_dev.variance);
	      } else {matResult__simXccmSTD__max.matrix[metric_isInGroup][ccm_id] = obj_dev.variance;}
	    }
	    //!
	    //! Update mean:
	    if( (obj_dev.mean != T_FLOAT_MAX) && (obj_dev.mean != 0) ) {
	      matResult__simXccmAVG__min.matrix[metric_isInGroup][ccm_id] = macro_min(matResult__simXccmAVG__min.matrix[metric_isInGroup][ccm_id], obj_dev.mean);
	      if(matResult__simXccmAVG__max.matrix[metric_isInGroup][ccm_id] != T_FLOAT_MAX) {
		matResult__simXccmAVG__max.matrix[metric_isInGroup][ccm_id] = macro_max(matResult__simXccmAVG__max.matrix[metric_isInGroup][ccm_id], obj_dev.mean);
	      } else {matResult__simXccmAVG__max.matrix[metric_isInGroup][ccm_id] = obj_dev.mean;}
	    }
	  }
	  //!
	  //! De-allocate:
	  free__s_kt_matrix(&matResult__ccm__internal); free__s_kt_matrix(&mat_transposed);
	}
	//!
	//! Increment:
	cnt_counter_y++;
	if(dist_metricCorrType == e_kt_categoryOf_correaltionPreStep_none) {
	  cnt_counter_y__sub++;
	}
      }
    }
    //! --------------------------------------------------------------------------------------------
    //!
    { //! Write out the result-matrices:
      assert(strlen(resultDir_path_collections));
      char str_local_js[2000] = {'\0'}; sprintf(str_local_js, "%s%s.js", resultDir_path_collections, "meta_result");
      FILE *stream_out_js = fopen(str_local_js, "wb");
      assert(stream_out_js);
      char str_local_tsv[2000] = {'\0'}; sprintf(str_local_tsv, "%s%s.tsv", resultDir_path_collections, "meta_result");
      FILE *stream_out_tsv = fopen(str_local_tsv, "wb");
      assert(stream_out_tsv);
      //!
      //! Define a gneierc export-rotuiens, ie, a Delata-inlined-function:
#define __Mi__export(self, stringOf_header) ({ \
	  assert((self)->nrows == groupMedium__cnt_simMetrics); /*! ie, to simplify 'inclusion' of row-names */ \
	  for(uint row_id = 0; row_id < groupMedium__cnt_simMetrics; row_id++) {const char *str = getStringOf__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t((e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t)row_id); assert(str); assert(strlen(str)); set_stringConst__s_kt_matrix(self, row_id, str, /*addFor_column=*/false); } \
	  export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript__s_kt_matrix_t(self, stringOf_header, stream_out_js, /*include_stringIdentifers_in_matrix=*/true, /*isTo_writeOut_stringHeaders=*/true); assert(stream_out_js); \
	  export_dataSet_s_dataStruct_matrix_dense_matrix_tab__s_kt_matrix_t(self, stringOf_header, stream_out_tsv, /*include_stringIdentifers_in_matrix=*/true, /*isTo_writeOut_stringHeaders=*/true); assert(stream_out_tsv);})
      //! -------------------------------------------
      //! Export:
      __Mi__export(&matResult__execTime__min, "matResult__execTime__min");
      __Mi__export(&matResult__execTime__max, "matResult__execTime__max");
      __Mi__export(&matResult__execTime__avg, "matResult__execTime__avg");
      //! ---- 
      __Mi__export(&matResult__execTimeSimAndCluster__min, "matResult__execTimeSimAndCluster__min");
      __Mi__export(&matResult__execTimeSimAndCluster__max, "matResult__execTimeSimAndCluster__max");
      __Mi__export(&matResult__execTimeSimAndCluster__avg, "matResult__execTimeSimAndCluster__avg");
      //! --- 
      __Mi__export(&matResult__ccm__min, "matResult__ccm__min");
      __Mi__export(&matResult__ccm__max, "matResult__ccm__max");
      __Mi__export(&matResult__ccm__avg, "matResult__ccm__avg");
      //! --- 
      __Mi__export(&matResult__simXccm__min, "matResult__simXccm__min");
      __Mi__export(&matResult__simXccm__max, "matResult__simXccm__max");
      //! --- 
      __Mi__export(&matResult__simXccmSTD__min, "matResult__simXccmSTD__min");
      __Mi__export(&matResult__simXccmSTD__max, "matResult__simXccmSTD__max");
      //! --- 
      __Mi__export(&matResult__simXccmAVG__min, "matResult__simXccmAVG__min");
      __Mi__export(&matResult__simXccmAVG__max, "matResult__simXccmAVG__max");
      //! 
      //! ------------------------------------------------------------------------ 
      //! Close:
      fclose(stream_out_js);
      fclose(stream_out_tsv);

      
      //! -----------------------------------------------------------------
      //!
      //! De-allcoate:
      free__s_kt_matrix(&matResult__execTime__min);
      free__s_kt_matrix(&matResult__execTime__max);
      free__s_kt_matrix(&matResult__execTime__avg);
      free__s_kt_matrix(&matResult__execTimeSimAndCluster__min);
      free__s_kt_matrix(&matResult__execTimeSimAndCluster__max);
      free__s_kt_matrix(&matResult__execTimeSimAndCluster__avg);
      free__s_kt_matrix(&matResult__ccm__min);
      free__s_kt_matrix(&matResult__ccm__max);
      free__s_kt_matrix(&matResult__ccm__avg);
      free__s_kt_matrix(&matResult__simXccm__min);
      free__s_kt_matrix(&matResult__simXccm__max);
      free__s_kt_matrix(&matResult__simXccmSTD__min);
      free__s_kt_matrix(&matResult__simXccmSTD__max);
      free__s_kt_matrix(&matResult__simXccmAVG__min);
      free__s_kt_matrix(&matResult__simXccmAVG__max);
    }
    //!
    //! De-allcoate and clsoe file-desicprtors:
    assert(fileDesriptorOut__clusterResults);
    fclose(fileDesriptorOut__clusterResults);
