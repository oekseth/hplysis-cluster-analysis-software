#ifndef hp_exportMatrix_h
#define hp_exportMatrix_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_exportMatrix
   @brief a wrapper to export matrices using different interptaitons/focue
   @author Ole Kristian Ekseth (oekseth, 06. jun. 2017).
**/


/* #include "configure_cCluster.h" */
/* //#include "list_uint.h" */
/* #include "def_intri.h" */
/* #include "kt_matrix.h" */
/* #include "e_kt_correlationFunction.h" */
#include "hp_api_fileInputTight.h"

/**
   @strust s_hp_exportMatrix
   @brief a wrapper to export a data-set into a number of permtautions (oekseth, 06. jun. 2017).
 **/
typedef struct s_hp_exportMatrix {
  uint arrOf_sim_postProcess_size;
  const s_kt_correlationMetric_t *arrOf_sim_postProcess;
  bool isTo_writeOutTransposed;
} s_hp_exportMatrix_t;


//! @return an intlized verison of the s_hp_exportMatrix_t object (oesketh, 06. jun. 2017).
s_hp_exportMatrix_t init__s_hp_exportMatrix_t(const s_kt_correlationMetric_t *arrOf_sim_postProcess, const uint arrOf_sim_postProcess_size);

//! Export the given matrix:
void writeOut__s_hp_exportMatrix_t(const s_hp_exportMatrix_t *obj_exportMatrix, s_kt_matrix_t mat_result, const char *resultPrefix, t_float *rowOf_summary, t_float *rowOf_summary_transposed, const bool isTo_writeOutTransposed);

//! Export the clsuter-emmbershisp to different reuslt-files (oekseth, 06. sep. 2017).
void export__s_kt_list_2d_kvPair__hp_exportMatrix(const char *stringOf_resultFile_prefix, s_kt_list_2d_kvPair_t obj_sparse, const s_kt_list_1d_string_t *strObj_1, const bool isTo_storeInMatrix);

#endif //! EOF
