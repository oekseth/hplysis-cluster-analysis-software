//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"
#include "tut_wrapper_image_segment.h"
#include "hp_image_segment.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "export_ppm.h"
#include <climits>
#include <cfloat>
//#endif
//#include "hp_distance.h"
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
//#include "aux_sysCalls.h"
//#include "kt_aux_matrix_norm.h" //! which hold the data-nraomizoant-enums.
//#include "hp_image_segment.h"

//! Apply logics:
static void _apply(const s_hp_image_segment_t &o_img, s_kt_matrix_t &mat_rgb, s_kt_correlationMetric_t corrMetric_insideClustering, const e_hpLysis_clusterAlg clusterAlg, const char *new_file_base) {
  const char *str_simMetric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(corrMetric_insideClustering.metric_id);
  const char *str_alg = get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg);
  //const bool inputMatrix__isAnAdjecencyMatrix = false;
  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);	
  // A (Symmetric) adjacency matrix?
  //obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
  obj_hp.config.corrMetric_insideClustering = corrMetric_insideClustering;
  obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.
  //const uint cnt_Calls_max = 3; 
  const uint arg_npass = 100;
  printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kruskal_fixed;
  for(uint ncluster_ind = 2; ncluster_ind < 10; ncluster_ind++) {
    // ----------------
    //!
    //! Step: Cluster:
    const uint nclusters = ncluster_ind;
    const bool is_also_ok = cluster__hpLysis_api (
						  &obj_hp, clusterAlg, &mat_rgb,
						  /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						  );
    assert(is_also_ok);
    // ---------------------------------
    //!
    //! Step: adjust the input-data:	 
    s_hp_image_segment_t o_img_adjusted = init__s_hp_image_segment_t();

    //!
    { //! Step: logics: adjsutments of the image:
      s_kt_list_1d_uint_t list_clusters = getClusters__hpLysis_api(&obj_hp);
      if(list_clusters.list_size == 0) {
	//! 
	//! De-allocate:
	free__s_hpLysis_api_t(&obj_hp);
	printf("(Info)\t clusters:%u\tNot any clsuters were detected, given alg(base:\"%s\", sim(internal):\"%s\"): drops performing operation, at %s:%d\n", nclusters, str_alg, str_simMetric, __FILE__, __LINE__);
	continue;
      } 
      //!
      //! Step: pre: a copy of the "o_img"
      o_img_adjusted = copy__s_hp_image_segment_t(&o_img);
	  
      assert(list_clusters.list_size > 0); //! ie, as we expect at least some mbers to be allcoated to clusters
      s_kt_list_1d_float_t map_rgbColor[3];
      s_kt_list_1d_uint_t  map_rgbColor_cnt[3];
      assert(mat_rgb.ncols == 3); //! ie, what we expect
      for(uint i = 0; i < 3; i++) {
	map_rgbColor[i]     = init__s_kt_list_1d_float_t(nclusters);
	map_rgbColor_cnt[i] = init__s_kt_list_1d_uint_t(nclusters);	      
	for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
	  map_rgbColor[i].list[cluster_id] = 0.0; //! ie, default value.
	  map_rgbColor_cnt[i].list[cluster_id] = 0; //! ie, zero occurences
	}
      }
      assert(list_clusters.list_size <= mat_rgb.nrows);
      for(uint cell_id = 0; cell_id < list_clusters.list_size; cell_id++) {
	if(list_clusters.list[cell_id] != UINT_MAX) {
	  const uint cluster_id = list_clusters.list[cell_id];
	  for(uint i = 0; i < 3; i++) {
	    map_rgbColor[i].list[cluster_id] += mat_rgb.matrix[cell_id][i];
	    map_rgbColor_cnt[i].list[cluster_id]++; //! ie, the count-incrementor
	  }
	}
      }
      //!
      //! Step: average the scores:
      for(uint i = 0; i < 3; i++) {
	for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
	  if(map_rgbColor_cnt[i].list[cluster_id] != 0) 
	    if(map_rgbColor[i].list[cluster_id]  != 0) {
	      map_rgbColor[i].list[cluster_id] = map_rgbColor[i].list[cluster_id]  / (t_float)map_rgbColor[i].list[cluster_id] ;
	    }
	}
      }
      //!
      //!	    
      { //! Step: Adjust the scores:
	loint global_row_id = 0;
	for (uint row_id = 0; row_id < o_img.mat_RGB[0].nrows; row_id++)  {
	  for (uint col_id = 0; col_id < o_img.mat_RGB[0].ncols; col_id++, global_row_id++)    {
	    for(uint i = 0; i < 3; i++) {
	      uint cluster_id = UINT_MAX;
	      if(global_row_id < list_clusters.list_size) { cluster_id = list_clusters.list[global_row_id];}
	      //!
	      if(cluster_id != UINT_MAX) {
		o_img_adjusted.mat_RGB[i].matrix[row_id][col_id] = map_rgbColor[i].list[cluster_id];
	      }
	    }
	  }
	}
      }	    
      //! 
      //! De-allocate:
      free__s_kt_list_1d_uint_t(&list_clusters);
      for(uint i = 0; i < 3; i++) {
	free__s_kt_list_1d_float_t(&(map_rgbColor[i]));
	free__s_kt_list_1d_uint_t(&(map_rgbColor_cnt[i]));
      }
    }

    // ----------------
    //!
    //! Step: Export:
    char new_file[10000];
    sprintf(new_file, "%s-%s-ncluster%u-%s-%s.ppm", new_file_base, get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg), nclusters, str_simMetric, str_alg);
    export__s_hp_image_segment_t(&o_img_adjusted, new_file);   //! ie, the call.
    //! 
    //! De-allocate:
    free__s_hp_image_segment_t(&o_img_adjusted);
    free__s_hpLysis_api_t(&obj_hp);
    // ----------------
  }
}

/**
   @brief examplfies strategies for basic operaitons on images.
   @author Ole Kristian Ekseth (oekseth, 06. nov. 2020).
   @remarks
   --- exemplifes strategies for image-parsing, and simplfied lgocis to validat coissiten in export-lgocis with import-logics    
   @remarks
   -- compile: g++ -I.  -g -O0  -mssse3   -L .  tut_image_3_exploreImpactOfConfigurations_simMetric.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_imgSim; ./tut_imgSim
   @remarks possible bugs when copling this:
   -- ivnestigate: dmesg -T| grep -E -i -B100 'killed process'
**/
int main() { // const int array_cnt, char **array) {
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif


  const char *result_file = "tmp-1.ppm";
  //!
  { //!
    //! Step: Construct a sytentic matrix:
    //! Note: we here construct a clsuter-shape. This to provide the stepping-stoens for future, and mroe compelx, examples
    const uint nrows = 100;
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/1000, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/0);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/20);    
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/40,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/60);    
    //if(true)
    { //! Step: Export to PPM:
      //s_kt_matrix_t matrix = &(obj_shape.matrix); //! ie, a shallow copy.
      ppm_image_t *o_image = exportTo_image(obj_shape.matrix.matrix, obj_shape.matrix.nrows, obj_shape.matrix.ncols, NULL, NULL);
      ppm_image_write(result_file, o_image);
      //! De-allcote:
      ppm_image_finalize(o_image);
      //! De-allocate:
      // free__s_hp_image_segment_t(&obj_image);
    }
    //!
    //! De-alloctate:
    free__s_hp_clusterShapes_t(&obj_shape);
  }
  //! ------------------------------------------------------
  //! Step: Import from PPM:
  const char *input_file = result_file;
  //const char *input_file = "example-image-autumnBeach.ppm"; //! from: convert ~/Downloads/IMG_20200905_193718.jpg example-image-autumnBeach.ppm
  // const char *input_file = "tmp-blur.ppm";
  s_hp_image_segment_t o_img = ppm_import__s_hp_image_segment_t(input_file);      
  //! ------------------------------------------------------
  
  // FIXME: adjsut this tut to .... cluster data ... 
  // FIXME: manage to get below meitc-epxlreoaitons less crude, ie, mroe ifned-grained
  
  
  // assert(false); // FIXME: complete!  

  //if(false)  // FIXME: remove
  { //! Apply Clustering
    const char *new_file_base = "tmp-after-clustering";
    // FIXME: new tut ... "tut_image_3_cluster.c" ....  explore effects of different clustering-algoriithms <-- what permtuatiosn to explore?
    
      
    //!
    //! Pre: turn iamge into a matrix: for simplity, we use the RGB-values:
    const uint nrows = o_img.mat_RGB[0].nrows;
    const uint ncols = o_img.mat_RGB[0].ncols;
    assert(nrows > 0);
    assert(ncols> 0);      
    s_kt_matrix_t mat_rgb = initAndReturn__s_kt_matrix(nrows * ncols, /*cols=*/3);
    loint global_row_id = 0;
    for (uint row_id = 0; row_id < o_img.mat_RGB[0].nrows; row_id++)  {
      for (uint col_id = 0; col_id < o_img.mat_RGB[0].ncols; col_id++, global_row_id++)    {
	for(uint i = 0; i < 3; i++) {
	  mat_rgb.matrix[global_row_id][i] =  o_img.mat_RGB[i].matrix[row_id][col_id];
	}
      }
    }


        // FIXME: add new for-loop ... clustering-algorithms
  //! Note: in [below] we examplify different permtauaions wrt. HCA: algorithm-calls:
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kruskal_hca;
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG; //! ie, k-means
    // FIXME: add new for-loop ...     

  
  // FIXME: data: use a syntietic data-set ... of feature-vecotr-triplets ... then cluster these ... 
  // FIXME: data: use a simpler cartoon-derived image ... an extremly samll data-set ... uyse 'mogrify' to make the image evens amller. 
  
    //! Iterate across cofnigruations:
    for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
      const s_kt_correlationMetric_t corrMetric_insideClustering = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none);
      //!
      //! Apply logics for clustering:

      // FIXME: explore different clsutering-algorithms ... and related cofnigruations 
      
      _apply(o_img, mat_rgb, corrMetric_insideClustering, clusterAlg, new_file_base);
    }

    // FIXME: update "tut_image_1_helloWorld.c" based on above

    // FIXME: pdate bleow ... apply for all sim-metrics
    
    //!
    //!  
    //! De-allcoate:      
    free__s_kt_matrix(&mat_rgb);	  
  }
  free__s_hp_image_segment_t(&o_img);      
      
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif  
}
  
