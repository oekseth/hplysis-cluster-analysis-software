=head
@brief provides logiccs for construiton of tables
=cut
package module_table;
use strict;
use Carp;
# -------------------------------------------


sub init_self {
    #my () = @_;
    #my ($result_file) = @_;
    return {
	#result_file => $result_file,
	#! where 'this' si a temrpaory-strut, to be nsued for future code-modivcaitons.
    };
}

sub _score_is_set {
    my ($col) = @_;
    my $isTo_drop = 0;
    if ($col =~ /^-?\d+\.?\d*$/) { if($col > 10000000) { $isTo_drop = 1; } }
    if( ($col eq "inf") || ($col eq "4294967296.000000") || ($col eq "nan") || ($col eq "-nan") || ($col eq "-") || ($col eq "") ) {$isTo_drop = 1;}
    if($isTo_drop == 0) {
	if ($col !~ /^-?\d+\.?\d*$/) { 
	    croak("!!\t : val=\"$col\" is NOT a number"); #, given line=\"$line\".");
	} else {
	    #if($col < $cutoff_min) { $isTo_drop = 1; }
	    #if($col > $cutoff_max) { $isTo_drop = 1; }
	    if( ($col > -0.001) &&  ($col < 0.001) ) { #! ie, test for value='0'
		# print("Drop\t score:$col\n");
		$isTo_drop = 1; } #! ie, as we assuemt eh valeu is set to '0' ... ie, to get emanignful differences in repdcitions.
	}
    }
    return ($isTo_drop == 0);    
}

sub toTable {
    my ($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr, $result_file) = @_;
    if(0 == scalar(@{$arr})) {
	printf("\t(module-table)\t input array is empty; drops cosntructing a table\n");
	return;
    }
    my $val_min = 10000.0;
    my $val_max = -10000.0;

    my $str_columns_wNumbers  = "";    
    my $str_result = "";
    my $index_line = 0;
    my $cnt_cols_updated = 0; #! which is used as a boolean to know n if data was added.
    #! 
    my $row_header = "Row,";
    my $before_rows = "";
    my $property_numbeOf_columns = undef;
    #!
    #!     
    foreach my $line (@{$arr}) {
	if($line eq "") {next;} #! then we ignore the line
	chomp($line); #! ie, remove the trail-newline.
	my @arrOf_cols = split("\t", $line);
	if(ref($line) eq ref([])) { #! then we assuem that the 'line' sia ctually an array of column-cells.
	    @arrOf_cols = @{$line};
	    $line = join("\t", @{$line}); #! ie, to handle the case where input is an array-reference
	}	
	#my $row_header = shift(@arrOf_cols);
	#$row_header =~ s/_/ /g; #! ie, to avoid Latex from complaining.	
	# 
	#print("(module-table:[$index_line])\t $line\n");
	my @cols_updated = (); #! which is to hold the formatted row-names.
	my $index = 0;	
	#! -----------------
	my $p_min = 10000.0; my $p_max = -10000.0;
	#! -----------------
	foreach my $col (@arrOf_cols) {
	    $col =~ s/[_\#\!]/ /g; #! ie, to avoid Latex from complaining.	
	    my $score_updated = "";
	    if($index_line == 0) {
		if($index > 0) { #! ie, do NOT color the table-row-name:
		    $str_columns_wNumbers  .= 'columns/' . $index . '/.style={color cells},' . "\n";
		    $row_header  .= $index . ",";
		}
	    }
	    if($index == 0) { #! then it is the row-name:
		if($col =~ /^(.+)\-(\d+)$/) { #! eg, for "dim=900,bernoulli-900"
		    $col = $1 . "," . $2; #! ie, to ease readilbity.
		}
	    }
	    if ($col =~ /^-?\d+\.?\d*$/) { 
		if($col > 10000000) {
		    $col = "inf"; #! ie, then asusem it is an infite number, hence ignoring it.
		}
	    }
	    if( ($col ne "inf") && ($col ne "4294967296.000000") && ($col ne "nan") && ($col ne "-nan") && ($col ne "-")) {
		#! Avodi too large vlaues from cluttering the results:
		if ($col =~ /^-?\d+\.?\d*$/) { 
		    #print("number\t $col\n");
		    if($col > $cutoff_max) {
			$col = $cutoff_max;
		    } elsif($col < $cutoff_min) {
			$col = $cutoff_min;
		    }
		    #!
		    #! Remember the extreme values (sued to adjsut the plot):		
		    if($col > $val_max) {
			$val_max = $col;
		    } 
		    if($col < $val_min) {
			$val_min = $col;
		    } 		    
		    $score_updated = sprintf("%.1f", $col);
		    $cnt_cols_updated++;
		} else {
		    $score_updated = $col; #! eg, for case where valeu si a string.
		    $score_updated =~ s/_/ /g;
		}
		#$str_result .= sprintf("(%u, %.1f) ", $index, $col);
	    } else {
		$score_updated = "";
	    }
	    push(@cols_updated, $score_updated);
	    $index += 1;
	}
	if($index_line != 0) {
	    my @tmp = ();
	    for(my $k = 0; $k < $property_numbeOf_columns; $k++) {
		push(@tmp, $cols_updated[$k]);
	    }
	    $str_result .= join(", ", @tmp);
	    #$str_result .= join(", ", @cols_updated);
	    # if($str_result =~ /^(.+), $/){
	    # 	$str_result = $1; #! ie, then remove the tailing ','.
	    # }
	    $str_result .= "\n";
	    my $cnt_curr = scalar(@cols_updated) -1; #! ie, remove the header
	    if($property_numbeOf_columns != $cnt_curr) {
		#if($property_numbeOf_columns != scalar(@cols_updated)) {
		printf(STDERR "(module-table)\t Number of columns in table is NOT cosnistent: |expectaiotn=rowHeader|=$property_numbeOf_columns, while |line|=$cnt_curr, given line: \t '$line'\t\t fileHeader:\"$before_rows\"\n");
	    }
	} else { #! then it is the row-header, for whcich specila care is requreied.
	    # fIXME: below seems wrognly set ... eg, for ...         dim=200,poisson-2-squared probabilisticChi-preNone & 1.1 & 0.1 & 0.1 & 0.6 & 1.1 & 1.2 & 1.2 & 0.9 & 1.3 & 9.5\\ 
	    $before_rows = join(" & ", @cols_updated) . "\\\\ \n";	    
	    $property_numbeOf_columns = scalar(@cols_updated);
	    #print("\t(set-col-headers)\t $before_rows \t from: \t $line\n");
	}

	$index_line += 1;
    }

    if($cnt_cols_updated == 0) {
	printf("\t(module-table)\t no data found; drops cosntructing a table\n");
	return;} #! ie, as no data were idneitfed.

    if($str_columns_wNumbers eq "") {
	croak("!!\t Why is str_columns_wNumbers=\"$str_columns_wNumbers\"??");
    }
    # FIXME: wronly set valeus for ... /color cells/min=$val_min, /color cells/max=$val_max
    if($val_min =~ /_/) {	croak("!!\t (erorr-in-parsing)\t why is val_min=\"$val_min\"??");    }
    print("\n\n\n*'''''''''''''''''''''''''''''********$keyword**************'\n#\n# $title \n # Min=$val_min, Max=$val_max, cutoff=($cutoff_min, $cutoff_max):");
    $str_columns_wNumbers =~ s/\n$//; #! ie, remove the last enwline.
    my $str_file =     sprintf(qq( 
\\documentclass[border=5mm]{standalone}
\\usepackage{pgfplotstable}
\\usepgfplotslibrary{colormaps}
\\input{tex-aux/def_heatMap_bw}
\\pgfplotsset{
    % this *defines* a custom colormap ...
  colormap={mycolor}{
    rgb255=(255,0,0)
    rgb255=(255,255,0)
    rgb255=(0,255,0)
  },
}

\\begin{document}
\\placetextbox{0.86}{0.35}{\\texttt{}}%
\\pgfplotstabletypeset[col sep=comma,
  every head row/.style={%
    output empty row,
    before row={%
        $before_rows
    },
  },  
 /color cells/min=$val_min, /color cells/max=$val_max ,
  /color cells/textcolor=white,
  fixed, %! used to avoid exponentail numbers: for "0.04" to Not be written as "4*10^-2", and instead as "0.0"
  precision=2,
%  /color cells/precision=2,
$str_columns_wNumbers
  columns/row-id/.style={string type},
  columns/Row/.style={string type},
  columns/Row1/.style={string type},
 /pgfplots/colormap/spring, %% set different colormap -- pink
  /pgfplots/colormap name=mycolor,
]{
));

    $row_header =~ s/\,$//g; #! ie, remove the last ','.
    $str_file .= "$row_header\n$str_result";
$str_file .= sprintf(qq(  }
%\\vspace{1cm}
\\end{document}\n));
    if(defined($result_file)) {
	printf("\n");
	#$result_file =~ s/\.tex/-exported\.tex/;
	$result_file =~ s/[$:\$|\| \t\(\)& ]//g;
	if($result_file =~ /^(.+)-tex$/) {$result_file = $1 . ".tex";} #! ie, as we then assume that a regex has failed to set the corrrect line-ending.
	#!
	open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");
	#!    
	print(FILE_OUT $str_file);
	close(FILE_OUT);
	#!
	#if(1 == 2)
	{
	    my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	    if($result_file =~/^(.+)\/(.+?)$/) {
		#! Then we need to exiplciuty speciy the result-direcotry [https://tex.stackexchange.com/questions/268997/pdflatex-seems-to-ignore-output-directory]
		my $dir_result = $1;
		my $f_name = $2;
		$cmd = "pdflatex -output-directory $dir_result  $result_file 1>out_latex.txt";
	    }
	    printf("\t(table-compile)\t %s\n", $cmd);
	    system($cmd); #! ie, to simplfiy viwing of the data
	    if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	    my $file_fig = $result_file;
	    $file_fig =~ s/\.tex/\.pdf/;
	    if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$file_fig \t Was Not able to generate a latex-files; please investigate\n"); 	}

	}
    } else {
	print $result_file;
    }
}



sub __filter_row {
    my($line, $arr_rowColsToUse) = @_;
    if(defined($arr_rowColsToUse) && scalar(@{$arr_rowColsToUse})) {
	my @cols = split("\t", $line);
	my @res = ();
	for(my $i = 0; $i < scalar(@{$arr_rowColsToUse}); $i++) {
	    push(@res, $cols[$arr_rowColsToUse->[$i]])
	}
	return join("\t", @res);
    } else {return $line;} #! ie, as the compelte line may be sued.
}

sub toTable_fromFile {
    my ($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row, $linepos_start, $result_file, $arr_rowColsToUse, $regex_firstRowToSkip) = @_;

    my $arr = [];
    my $file_index = 0;
    my $firstLineNext_match = undef;
    my $debug_cntAdded_inner = 0;
    #my $line_first = undef; #! which needs to be sued in combaiton with "$linepos_start"
    foreach my $file_name (@{$arr_file_name}) {
	open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
	my $row_prefix = "";
	{
	    $row_prefix = "file-" . $file_index;
	    if($file_name =~ /\-n(\d+)-/) {
		$row_prefix = "dim=" . $1 . ": ";
	    }
	    if($file_name =~ /\-n\d+\-\-([a-zA-Z]+)-corrDeviation/) { 
		$row_prefix .=  "" . $1 . ": "; #! eg, PearsonProductMoment
	    }
	}
	my $cnt_lines = 0;
	my $cnt_lines_total = 0;
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.
	    my $row_matches_req = 0;
	    if(defined($regex_firstRowToSkip) && ($line =~ /$regex_firstRowToSkip/i)) {
		$row_matches_req = 1;
	    }
	    #!
	    #!
	    $line = __filter_row($line, $arr_rowColsToUse);
	    #!
	    #!	      
	    if(defined($linepos_start) && ($cnt_lines_total < $linepos_start) ) {
		if($cnt_lines_total  == 0) {
		    $line =~ s/\#\!//g; #! ie, remvoe the '#!' prefix.
		    push(@{$arr}, $line); #! ie, to remmeber the row-header: row-ehader is used in the gernated table, hence tis improtance
		    if($linepos_start != 0) {
			$cnt_lines += 1; #! ie, as we then asusme that the 'added row-header' counts as a 'new' row-header.
		    }
		}
		$cnt_lines += 1;
		$cnt_lines_total += 1;
		next; #! ie, as we then assume that the line-pos is to be ignored.
	    }
	    if( (($cnt_lines_total < $max_lineCount) || ($row_matches_req == 0)) && !defined($firstLineNext_match)){
		if(!defined($regex_row) || ( $line =~ /$regex_row/) || (scalar(@{$arr}) == 0) ) {
		    if(($cnt_lines > 0) && ($row_prefix ne "") ) { #! then increase the acucryacy of the row-name
			$line = $row_prefix . $line;
		    } else {
			$line =~ s/\#\!//g; #! ie, remvoe the '#!' prefix.
		    }

		    #printf("[%u = %u]\tadd-line\t" . $line . "\n", $cnt_lines_total, scalar(@{$arr}));
		    push(@{$arr}, $line);	    
		    $cnt_lines += 1;
		    $debug_cntAdded_inner += 1;
		}
	    }
	    if( !defined($firstLineNext_match) && ($cnt_lines_total >= $max_lineCount) && ($row_matches_req == 0) ) {print("\t[$cnt_lines_total >= $max_lineCount]\t $line\n");}
	    #!
	    if( ($cnt_lines_total >= $max_lineCount) && ($row_matches_req == 1) )
	    {
		if(!defined($firstLineNext_match)) {
		    $firstLineNext_match = $cnt_lines_total;
		    #print("\t firstLineNext_match:'$firstLineNext_match'\t $line\t givven \t (cnt:$cnt_lines_total < max:$max_lineCount) \n");
		}		
	    }
	    $cnt_lines_total += 1;
	}
	close(FILE_IN);
	$file_index += 1;
    }
    #! Costruct the Latex-table:
    #! 
    printf("(covnert-to-table)\t \t |arr|=%u \t     addedInner:$debug_cntAdded_inner \t max:$max_lineCount\n", scalar(@{$arr}));
    toTable($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr, $result_file);
    #!
    return $firstLineNext_match;
}


#! ****************************************************** EOF:
1;
