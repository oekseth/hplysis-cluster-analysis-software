=head
@brief provides logiccs for construiton of heat-maps
=cut
package module_heatMap;
use strict;
use Carp;
# -------------------------------------------

sub init_self {
    my ($result_file) = @_;
    return {
	file_result => $result_file,
	#xlabel => "Feature-index \$i\$",
	#ylabel => "Feature-score \$f_x(i)\$",
    };
}


sub heatMap {
    my ($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr) = @_;
    my $val_min = 10000.0;
    my $val_max = -10000.0;

    #my $str_columns_wNumbers  = "";    
    my $str_result = "";
    my $index_line = 0;
    #!
    # FIXME: udpate:
    # FIXME: udpate: row_header with \node[darkColor,left] at (axis cs:-2,4){\small{'12 MTT}};
    my $row_header = "";
    # ----------------------
    my $mark_size = 5;
    my $config_legendPos_x = -15;
    if(scalar(@{$arr}) > 20) {
	$mark_size = 3; # TODO: consider adding even finer resolution.
	$config_legendPos_x = -7;
    }
    # --------------------    
    # my $before_rows = "";
    #!
    #!     
    my $index_max_y = 0;
    #my $index_max_x = 0;
    foreach my $line (@{$arr}) {
	chomp($line); #! ie, remove the trail-newline.
	if($line eq "") {next;} #! then we ignore the line
	$line =~ s/_/ /g; #! ie, to avoid Latex from complaining.	
	if($line =~ /^\#/) {
	    $index_line += 1;
	    next; #! as we then assuemt aht this is a header-row.
	}
	my @arrOf_cols = split("\t", $line);
	my $l_row_header = shift(@arrOf_cols);
	$l_row_header =~ s/[$:\$|\| \t\(\)& ]/ /g;
	if(scalar(@arrOf_cols) < 2) {
	    croak("!!\t Numer ofColumsn Not as epcted: row-header: $l_row_header");
	}
	# 
	my @cols_updated = (); #! which is to hold the formatted row-names.
	my $index = 0;
	#! -----------------
	my $p_min = 10000.0; my $p_max = -10000.0;
	#! -----------------
	foreach my $col (@arrOf_cols) {
	    my $score_updated = "";
	    # if($index_line == 0) {
	    # 	if($index > 0) { #! ie, do NOT color the table-row-name:
	    # 	    $str_columns_wNumbers  .= 'columns/' . $index . '/.style={color cells},' . "\n";
	    # 	    $row_header  .= $index . ",";
	    # 	}
	    # }
	    if($index == 0) { #! then it is the row-name:
		if($col =~ /^(.+)\-(\d+)$/) { #! eg, for "dim=900,bernoulli-900"
		    $col = $1 . "," . $2; #! ie, to ease readilbity.
		}
	    }
	    if ($col =~ /^-?\d+\.?\d*$/) { 
		if($col > 10000000) {
		    $col = "inf"; #! ie, then asusem it is an infite number, hence ignoring it.
		}
	    } 
	    if( ($col ne "inf") && ($col ne "4294967296.000000") && ($col ne "nan") && ($col ne "-nan") && ($col ne "-")) {
		if ($col !~ /^-?\d+\.?\d*$/) { 
		    if($col !~ /e\-\d+/) {
			croak("!!\t : val=\"$col\" is NOT a number, given line=\"$line\".");
		    } #! else: it coudl e a negative exponent, eg, "4.15555688755732e-05"
		}
		#! Avodi too large vlaues from cluttering the results:
		if($col > $cutoff_max) {
		    $col = ""; # $cutoff_max;
		} elsif($col < $cutoff_min) {
		    $col = "";
		    #$col = $cutoff_min;
		}
		if ($col =~ /^-?\d+\.?\d*$/) { 
		    $score_updated = sprintf("%.1f", $col);
		} else {
		    $score_updated = $col; #! eg, for case where valeu si a string.
		}
		#$str_result .= sprintf("(%u, %.1f) ", $index, $col);
		#!
		#! Remember the extreme values (sued to adjsut the plot):
		if($col > $val_max) {
		    $val_max = $col;
		} elsif($col < $val_min) {
		    $val_min = $col;
		}
		if($score_updated != "") {
		    # FIXME: ... set cocrrect row-format.
		    #! Note: to ease readiblity, we transpose the data before isnerting this ... for cosnistency, teh ymax,xmax attributs (in the bleow) needs to be updated as well.
		    push(@cols_updated, sprintf("(%u,%u) [%.1f]", $index, $index_line, $score_updated));
		    # push(@cols_updated, sprintf("(%u,%u) [%.1f]", $index_line, $index, $score_updated));
		}
		#! Update the max-x-count-prop
		if($index_max_y < $index) {
		    $index_max_y = $index;
		}		
	    }
	    $index += 1;
	}
	if($index_line != 0) {
	    $str_result .= '%---------' . " $l_row_header  \n     " . join("\n     ", @cols_updated) . "\n";
	    if(scalar(@cols_updated) > 0) {
		my $textSize = '\small';
		if(scalar(@{$arr}) > 20) {
		    $textSize = '\tiny'; # TODO: consider adding even finer resolution.
		}
		#! Note: horis=x-pos = first column, .... vert=y-pos=second-col
		$row_header .= '\node[darkColor,left] at (axis cs:' . $config_legendPos_x . ',' . $index_line . '){' . $textSize . '{' . $l_row_header . '}};' . "\n";
	    }
	} else { #! then it is the row-header, for whcich specila care is requreied.
	    # FIXME: ... 
	    ; # $before_rows = join(" & ", @cols_updated) . "\\\\ \n";	    
	}

	$index_line += 1;
    }
    if(length($row_header) < 10) {
	printf("\t(module-heatMap)\t no data found; drops cosntructing a heamap\n");
	return;} #! ie, as no data were idneitfed.


    #!
    #! Add offset:
    $index_max_y += 5;
    #!
    #! Print results: 
    #my $xlabel = $self->{xlabel};
    #my $ylabel = $self->{ylabel};
    my $str_file = qq(\n\n\n%%\%*'''''''''''''''''''''''''''''********$keyword**************'\n%%\n%% $title \n %% Min=$val_min, Max=$val_max, cutoff=($cutoff_min, $cutoff_max):");
    #print(qq( 

    $str_file .= qq(
\\documentclass[border=5mm]{standalone}
\\usepackage[table,x11names,usenames, dvipsnames]{xcolor}
% \\input{color_defs}
\\usepackage{pgfplotstable}
\\usepgfplotslibrary{colormaps}
%\definecolor{lightgrey}{rgb}{0.9,0.9,0.9}
\\definecolor{lightgrey}{rgb}{0.9,0.9,0.9}
\\definecolor{keywords}{HTML}{8A4A0B}
\\definecolor{background}{HTML}{EEEEEE}
\\definecolor{comments}{HTML}{868686}
\\definecolor{lengthcolor}{RGB}{200,40,150}
\\definecolor{myblue}{RGB}{20,105,176}
 \\definecolor{mygreen}{rgb}{0,0.6,0}
 \\definecolor{mygray}{rgb}{0.5,0.5,0.5} 
 \\definecolor{mymauve}{rgb}{0.58,0,0.82}
\\definecolor{listinggray}{gray}{0.9}
\\definecolor{lbcolor}{rgb}{0.9,0.9,0.9}
\\definecolor{red}{rgb}{0.9,0,0} % for strings
\\definecolor{javared}{rgb}{0.6,0,0} % for strings
\\definecolor{javagreen}{rgb}{0.25,0.5,0.35} % comments
\\definecolor{javapurple}{rgb}{0.5,0,0.35} % keywords
\\definecolor{javadocblue}{rgb}{0.25,0.35,0.75} % javadoc
 \\definecolor {processblue}{cmyk}{0.96,0,0,0}
\\definecolor{nodeBlue}{RGB}{207,226, 243} % javadoc
    \\definecolor{pinegreen}{cmyk}{0.92,0,0.59,0.25}
    \\definecolor{royalblue}{cmyk}{1,0.50,0,0}
    \\definecolor{lavander}{cmyk}{0,0.48,0,0}
    \\definecolor{violet}{cmyk}{0.79,0.88,0,0}

\\definecolor{darkColor}{rgb}{0.102,0,0.2} %darkPurple 
\\definecolor{mediumColor}{rgb}{0.2,0,0.4} %purple 
\\definecolor{lightColor}{rgb}{0.413,0.315,0.615} %lightPurple

%\\input{def_heatMap_bw}
\\begin{document}

\\begin{tikzpicture}
  \\begin{axis}[
      width=0.75\\textwidth, height=0.75\\textwidth,
      tick align=inside, unbounded coords=jump,
      xmin=-5, xmax=$index_max_y, ymin=-1, ymax=$index_line,
        point meta min=$val_min, point meta max=$val_max, colorbar,
        colormap/bluered]
    \\addplot[mark=square*,only marks, scatter, scatter src=explicit,
      mark size=$mark_size]
    coordinates {
);

    $str_file .= $str_result;
    $str_file .= qq(      };
    \\pgfplotsset{
      after end axis/.code={
           $row_header
      }
    }
  \\end{axis}
  \\end{tikzpicture}
\\end{document}
);

    if(defined($self->{file_result})) {
	my $result_file = $self->{file_result};
	$result_file =~ s/[$:\$|\| \t\(\)& ]//g;
	open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");
	print(FILE_OUT $str_file);
	close(FILE_OUT);
	{
	    my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	    if($result_file =~/^(.+)\/(.+?)$/) {
		#! Then we need to exiplciuty speciy the result-direcotry [https://tex.stackexchange.com/questions/268997/pdflatex-seems-to-ignore-output-directory]
		my $dir_result = $1;
		my $f_name = $2;
		$cmd = "pdflatex -output-directory $dir_result  $result_file 1>out_latex.txt";
	    }
	    printf("\t(heatMap-compile)\t %s\n", $cmd);
	    system($cmd); #! ie, to simplfiy viwing of the data
	    my $file_fig = $result_file;
	    $file_fig =~ s/\.tex/\.pdf/;
	    if(!(-f $file_fig)) {printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\t CMD:\t $cmd\n"); }
	}    
    } else { #! then we pritn the countent (of the file) to STDOUT:
	print($str_file);
    }    
}

sub toHeatMap_fromFile {
    my ($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row) = @_;

    my $arr = [];
    my $file_index = 0;
    foreach my $file_name (@{$arr_file_name}) {
	open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
	my $row_prefix = "file-" . $file_index;
	if(scalar(@{$arr_file_name}) == 1) {$row_prefix = "";} #! ie, as we then asusme that the row-previs is not enssary.
	if($file_name =~ /\-n(\d+)-/) {
	    $row_prefix = "dim=" . $1 . ",";
	} elsif($file_name =~/postCCM-randomDistributions-features_img/) {
	    $row_prefix = "clusterIDs" . ",";
	}
	if($file_name =~ /\-n\d+\-\-([a-zA-Z]+)-corrDeviation/) { 
	    $row_prefix .=  "" . $1 . ", "; #! eg, PearsonProductMoment
	}	
	my $cnt_lines = 0;
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.
	    if($cnt_lines == 0) {
		$cnt_lines += 1;
		next; #! ie, as we asusmet the file-row-header si NOT to be used in the heatmpa-generaiotn (we in this script cconstruct).
	    }
	    if($cnt_lines < $max_lineCount) {
		if(!defined($regex_row) || ( $line =~ /$regex_row/) || (scalar(@{$arr}) == 0) ) {
		    if($row_prefix ne "") { #! then increase the acucryacy of the row-name
			$line = $row_prefix . $line;
		    }
		    #!
		    #!
		    my @arrOf_cols = split("\t", $line);
		    my @arrOf_cols_to_use = ();
		    for(my $i = 0; $i < scalar(@arrOf_cols); $i++) {
			if( !defined($max_colCount) || ($i < $max_colCount) ) {
			    push(@arrOf_cols_to_use, $arrOf_cols[$i]);
			}
		    }
		    #!
		    #!
		    # print "add-line\t" . $line . "\n";
		    push(@{$arr}, join("\t", @arrOf_cols_to_use));
		    $cnt_lines += 1;
		}
	    }
	}
	close(FILE_IN);
	$file_index += 1;
    }
    #! Costruct the Latex-table:
    heatMap($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr);
}




#! ****************************************************** EOF:
1;
