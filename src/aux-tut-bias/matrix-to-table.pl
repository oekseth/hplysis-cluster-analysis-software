use Carp;
use strict;
#! 
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
#use lib '.'; #! which is used for transposing data, and loading strings of data into an array.
require aux_matrix; #! which is used for transposing data, and loading strings of data into an array.
require module_scatter; #! which provide lgocics for generating specilied scatter-plots
require module_barChart; #! which provide lgocics for generating specilied scatter-plots
require module_dotTree; #! which provide lgocics for generating specilied scatter-plots
require module_table; #! which provide lgocics for generating specilied tables



# ********************************************************
sub _construct_2d_infoVis_from_hash {
    my ($result_file_base, $hash, $filePrefix) = @_;
    my %hash_unique_g1 = %{$hash}; #! ie, to simplfiy code-readbility.


    my $file_nameGlobal = "report-summary-" . $filePrefix . ".tex";
    open(FILE_OUT_META, ">$file_nameGlobal") or die("(init)\t Unable to open the input-file $file_nameGlobal\n");

    # FIXME: update our figure-caption with below trheshodls ... sue din the figure-genrraiton
    my $cutoff_min = 0.1; #! ie, as we assuem theat variance=0 is NOT of itnerest.
    my $cutoff_max = 1.9; #! as we asusemt aht an extrem variance (ie, a varaince > 2) is not of itnerest.
    #!
    
    my @arr_keys = keys(%hash_unique_g1);
    #! 	
    while (my ($key_x, $o_x) = each (%hash_unique_g1)) {
	my @arr_fileNames = ();
	printf("iterate\t key:$key_x\n");	    
	foreach my $key_y (@arr_keys) {
	    my $o_y = $hash_unique_g1{$key_y};
	    #	    while (my ($key_y, $o_y) = each (%hash_unique_g2)) { # FIXME: remove
	    #while (my ($key_y, $o_y) = each (%hash_unique_g1)) {
	    my $arr_x = $o_x->{arr_1};
	    # FIXME: validate correctnes of below x--y seperation.
	    my $ylabel = $key_y; 		    my $xlabel = $key_x;
	    #!
	    #!
	    #if(1 == 2) { # FIXME: remove!
	    if($key_y ne $key_x)  {
		#!
		#!
		my $arr_y = $o_y->{arr_1};
		if(scalar(@{$arr_x}) == 0) {printf("\t drop\tkey:$key_x has zero scores"); next;}
		if(scalar(@{$arr_y}) == 0) {printf("\t drop\tkey:$key_y has zero scores"); next;}		    
		#! 
		#! Merge the secodn two arrays
		#! Note: result: idea is to use the relative difference to capture difference in center-points.
		my @arr_z = ();
		for(my $k = 0; $k < scalar(@{$arr_y}); $k++) {
		    my $s1 = $o_x->{arr_2}->[$k];
		    my $s2 = $o_y->{arr_2}->[$k];
		    my $div = 1;
		    if(_score_is_set($s1) && _score_is_set($s2) ) {
			if($s1 > $s2) {
			    $div = abs($s1) / abs($s2);
			} else {	$div = abs($s2) / abs($s1); }
		    }
		    push(@arr_z, $div);
		}
		#!
		#!		    
		#! Comptute the infoVis:
		my $result_file = $result_file_base . "-" . $filePrefix . sprintf("-scatter-key%s-%s", $key_x, $key_y);
		my $self_scatter = module_scatter::init_self($result_file);
		my $arr_scoreIndex_toUse = undef;
		my $title = sprintf("%s %s %s", $filePrefix, $key_x, $key_y);
		#printf("\t (scatter)\t%s \t %s \n", $title, $result_file);
		my $file_result = module_scatter::scatterPlot_typeOfStyle_heatMap($self_scatter, $title, $ylabel, $xlabel, $arr_x, $arr_y, \@arr_z,  $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse);
		if(defined($file_result)) {
		    push(@arr_fileNames, {file => $file_result, name => $key_y});
		}
	    } elsif($key_y eq $key_x) { #! then cosntruct a histogram-bar-plot.
		#!
		#! Pre: Construct a matrix-version of the data:
		my $matrix = [[]];
		printf("\t pre-bar-chart\n");
		for(my $k = 0; $k < scalar(@{$arr_x}); $k++) {
		    my $s1 = $o_x->{arr_1}->[$k];			
		    if(_score_is_set($s1)) {push(@{$matrix->[0]}, $s1);}
		}
		my $result_file = $result_file_base . "-" . $filePrefix . sprintf("-histoBar-key%s-%s", $key_x, $key_y);
		my $title = $xlabel;
		if(scalar(@{$matrix->[0]}) > 5) {
		    my $arr_rowNames = [$key_x];
		    #!
		    #! InfoVis:
		    my $self_bar = module_barChart::init_self($result_file);
		    printf("\t (barChart)\t%s \t %s \n", $title, $result_file);
		    #! Compute:
		    module_barChart::barChart_typeOfStype_histogram($self_bar, $matrix, $arr_rowNames, $title, $xlabel, $ylabel);
		} else {
		    printf("\t (isEmpty::barChart)\t%s \t %s \n", $title, $result_file);
		}
	    }
	}
	{ #! Generate meta-detaisl to be used whenc ostnructing a Latex-report:
	    #!
	    #! Construyct label-caption-templates:
	    my $str_fileLabel = sprintf("\\textbf{%s}: ", $key_x);
	    foreach my $f_obj (@arr_fileNames) {
		#!
		$str_fileLabel .= sprintf("\\textit{%s}; \\hspace{1cm}", $f_obj->{name});
		printf(FILE_OUT_META qq(
%% ------------
\\begin{minipage}{.98\\textwidth}
{\\footnotesize
     \\vspace{0.2cm}
 $str_fileLabel
     \\newline
}
\\rule{\\textwidth}{1pt}
\\vspace{0.05cm}
\\end{minipage}\\hfill
    %% ---------------------------------------------------------------------
		       ));

		#!
		my $width = '0.23';
		#my $width = 0.9 * (1.0/scalar(@arr_fileNames));
		$width .= '\\linewidth';
		printf(FILE_OUT_META "\\includegraphics[trim = 0mm 0mm 0mm 0mm, clip,keepaspectratio=true,width=%s]{%s}\n", $width, $f_obj->{file});
	    }
	}		
    }
    close(FILE_OUT_META); #! wheere latter file is used to  construct infomative Latex-image-captions ... three images/row ... to idneityf the apprxoiarte number of figures/lines the figures are first amnually isnerted into ... 
}

# ***********************************************************
if(1 == 0)
{ #! File: 
    #! Idea: ... when finding the midpoint of groups (eg, clusters of data, how hyptoesis intersects clsuters, etc.), there is a need for a scalar 1d-approxmation for capturing the entropy of mulitple numbers. While Fig. \ref{} depcits the ambigites relating to this task, Seciton \ref{} exempfleis a few of these metrics. ... This task assumes taht we have metrics which captures the simlairties of data ... Estalisehd emasures for divegence are Stdanrda DEviation (STD), Krutosis, Skewness, etc. Fig. \ref{} provides a table of ... exploring how the estlibhed metrics for 1d-variance cpature differences in data .... Aim is to answer questions relating to their applcailbity, such as: may the eslihed emtrics for 1d-variance be usedd to classify data-sets? ... the vairance in predicctions for all the features ... When compared to a talbe focued on the covariance simlairty (between each distrubion) we observe how the vairance between the cases (eg, dstiributiosn) are cosnierlaby larger than the variance inside each case/distribution. 
    my $keyword = "fig-tut2-nclusterHalf-n900-featureDeviation.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $regex_row = undef;
    my $title = "Deviation in features for distributions, at feature-size=900";
    my $arr_file_name = ["f-result-tut-bias-2/result-2-randomDistributions-n900-featureDeviation.tsv"];
    my $cutoff_min = -10.0;  my $cutoff_max = 1400.0; my $max_lineCount = 10000;    
    # FIXME: use below summary-prop to assert attributes of
    # FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
    my $self = module_table::init_self();
    module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row);
}
# -----------------
if(1 == 0)
{ #! File: 
    #! Idea: ... explore how the midpoint in distributions are donsistent across dimensions ... use difference in prediction-scores to argue for the percentage-difference which is acceptable ... for which the PAreto Boudnary of clsutering algorithms is derived ... 
    my $keyword = "fig-tut2-nclusterHalf-featureDeviation-student.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "How the data size, and difference in distrubiton midpoint, influences the seperability of features";
    my $arr_file_name = [
	"f-result-tut-bias-2/result-2-randomDistributions-n3-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n600-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n900-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n300-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n1200-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n1500-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n1800-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n2100-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n2400-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n2700-featureDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n3000-featureDeviation.tsv",
	];
    my $cutoff_min = -10.0;  my $cutoff_max = 1000.0; my $max_lineCount = 10000;    
    my $regex_row = 'student';
    # FIXME: use below summary-prop to assert attributes of
    # FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
    my $self = module_table::init_self();
    module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row);
}
# -----------------
if(1 == 2) # FIXME: incldue this ... when we have muliple measuremetns ... idntify a case where "result-*-randomDistributions-n*--Euclid-corrDeviation.tsv" does NOT have a variance=0.
{ #! File: 
    #! Idea: ... 
    my $keyword = ""; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "";
    my $arr_file_name = [""];
    my $cutoff_min = -10.0;  my $cutoff_max = 400.0; my $max_lineCount = 10000;    
    my $regex_row = undef;
    # FIXME: use below summary-prop to assert attributes of
    # FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
    my $self = module_table::init_self();
    module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row);
}


# -----------------
if(1 == 2) # FIXME: incldue this ... when we have muliple measuremetns ... idntify a case where "result-*-randomDistributions-n*--Euclid-corrDeviation.tsv" does NOT have a variance=0.
{ #! File: 
    # FIXME: heatmap: generate a alrger heatmap-permtuation of this figure.
    #! Idea: ... ... similarites in correlation-scores ... Not any difference in Ecudleian correlations-score, which is due to ...??... ... fucos on how the ddistrubiotns differ ... use min--max seperability as an indciation of accuracy-thresholds relating to the distrubiotns themself ... this provides an dinciation of the PAreto Boudnary for clsutering-algorithms ... Hence, an aopprunity for applying aprpoxmaite comptuing ... such as randomly evaluting a subset of features ... apply accurate evalaution for only a subset of the emasurement rows (eg, k-means for central entites, and a threshodl-based dsijoitn-appraoch for the reaminingd enties (Eq. \ref{}) ... % FIXME: could the prolesm with Euclidean be due too 'too large numbers' ... <-- ivnestigate this by using a pre-nromasiaotn apåpraoch ... then make this part of the scope-contributiosn fot his paper (ie, issues with default impelmtations, as computers does not ahve the number-range to handle the large sum of numbers) ... and effects of alleviaitng this sisue (eg, through data-nrosmaiton) ... a tanalizign qusiotn is to ask how .... 
    my $keyword = "fig-tut2-nclusterHalf-corrDeviation-Pearson-student.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "The table evalautes the spread in values between student-t distrubiotns towards all the distrubiotns listed in Table ... While thsi table presents a sumamry of the measuremetsn, the resutls are cosnsient across distrubiotns and metrics. A study of the dsitrubiotn, evaluted through emtrics for correlation, reveals how the apsect of diemsionnality, is less sever than other contributing factors";
    my $arr_file_name = [
	"f-result-tut-bias-2/result-2-randomDistributions-n3--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n300--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n600--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n900--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n1200--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n1500--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n1800--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n2100--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n2400--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n2700--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n3000--PearsonProductMoment-corrDeviation.tsv",
	];
    my $cutoff_min = -10.0;  my $cutoff_max = 4000.0; my $max_lineCount = 10000;    
    my $regex_row = 'student';
    # FIXME: use below summary-prop to assert attributes of
    # FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
    my $self = module_table::init_self();
    module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row);
}
# -----------------

# -----------------
if(1 == 0)
{ #! File: 
    #! Idea: ... how differnces in simalrities between multiple distrubtions are impacted by the choise of how we understand simalrities ... Hence, for predictions to be truswowrthy they need to bridge knowledge of the domain with the attributes of the algorithm (Fig. _to-plane-figure_). Section __ref__ explores a subset of the emtriccs, exemplfiying how the choise of pariwse simlairty, and udnerstanding of onvergence, incrases the accruaccy of .... 
    my $keyword = "fig-tut2-nclusterHalf-corrDeviation-Pearson-Kendall-MINE-n3000-student.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "Difference between distrubiotns at feature-sizee=3000, when seen through the lense of Pearosn, Kendall's Tau, and MINE";
    my $arr_file_name = [
	"f-result-tut-bias-2/result-2-randomDistributions-n3000--PearsonProductMoment-corrDeviation.tsv",
	"f-result-tut-bias-2/result-2-randomDistributions-n3000--Kendall-corrDeviation.tsv",	
	"f-result-tut-bias-2/result-2-randomDistributions-n3000--MINE-corrDeviation.tsv",
	];
    my $cutoff_min = -10.0;  my $cutoff_max = 400.0; my $max_lineCount = 10000;    
    my $regex_row = 'student';
    # FIXME: use below summary-prop to assert attributes of
    # FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
    my $self = module_table::init_self();
    module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row);
}
# -----------------




# -----------------
#if(1 == 0)
{ #! File: 
    # Idea: ... evaluate the hypteosis of Eq-?? ... are there distrubtions with higher degree of density than others (eg, as measured through \textit{variance})? are there any random scoeres which are more similar to the bell-curve (Eq. \ref{}) than others? are there any ccosnsitees in wheee the midpoitn (ie, \textit{mean}) are found (acorss differnet distrubionts, and across different similarty-meitrcs)? ...
    my $title = "Feature-deviations ";
    my $keyword_base = "fig-tut-bias-3-table-n200-4000-nclust2-featureDeviation-";
    my $arr_file_name = ["f-result-tut-bias-3/result-3-randomDistributions-n200-4000-nclust2-featureDeviation.tsv"];
    my $cnt_lines_in_block = 90; #! ie, the base-number of pariise simlarity-metrics.
    my $max_lineCount      = $cnt_lines_in_block;
    #my $max_lineCount = 94; #! ie, the base-number of pariise simlarity-metrics.
    #my $max_lineCount = 100;
    my $cutoff_min = -10.0;  my $cutoff_max = 400.0; 
    my $cnt_blocks = 20; #! ie, the number of blocks to generate.
    my $linepos_start = undef; #! where 'undef' is used to simplify the lgoics wrt. where to start the parsing.
    for(my $block_index = 0; $block_index < $cnt_blocks; $block_index += 1) {
	my $keyword =  $keyword_base . sprintf("b%u", $block_index) . ".tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
	my $regex_row = undef;
	# FIXME: use below summary-prop to assert attributes of
	# FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
	my $result_file = $keyword;
	my $arr_rowColsToUse = [ #! a lsit of columsn used to reduece the score-clutter of extrme outlier.valeus,
				 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	    ];
	my $regex_firstRowToSkip = 'kendal';
	print("# \t(parse:main)\t linePos(atCall)=[$linepos_start, $max_lineCount]\n");
	my $self = module_table::init_self();
	my $firstLineNext_match = module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row, $linepos_start, $result_file, $arr_rowColsToUse, $regex_firstRowToSkip);
	if(!defined($linepos_start)) {
	    $linepos_start = 0;
	}
	if(defined($firstLineNext_match)) {
	    $linepos_start = $firstLineNext_match +1 ;
	    #print("\t(parse:main)\t update\t linePos=[$linepos_start, $max_lineCount]\n");
	} else {
	    $linepos_start += $max_lineCount + 1;
	}
	$max_lineCount += $cnt_lines_in_block;
	#$linepos_start += $max_lineCount;
	# print("#\t(parse:main)\t update\t linePos=[$linepos_start, $max_lineCount]\n");
	#$max_lineCount += $max_lineCount; 
    }    
}
if(1 == 2)
{
    # --------- FIXME: from "appendix-benchmark-results.tex": Fig. \ref{figure::tut-3-variance-versus-mean-for-similarity-metrics}: ... code: figures: Perl: ... remove "preNone",   ... and remove the metic-group-name (eg, Minkowski, fidelity, ...)
    # --------- FIXME: from "appendix-benchmark-results.tex": Fig. \ref{figure::tut-3-variance-versus-mean-for-similarity-metrics}: ... 
    # --------- FIXME: from "appendix-benchmark-results.tex": 
    # --------- FIXME: from "appendix-benchmark-results.tex": 
    #! Idea: .... 
    # .... 
    # .... 
    # .... 
    # ....
    # .... 
    # ....     
    my $result_file_base = "fig-tut-bias-3-variance-";
    my $title = "";
    my $arr_file_name = [{name => "f-result-tut-bias-3/result-3-randomDistributions-n200-4000-nclust2-featureDeviation.tsv"}];
    my $colIndex_score_1 = 1; # FIXME: what does the 'z-score' refer to? <-- idea is to use the 'average' (or: 'mean'), ie, to infer the center-point of the data.
    my $colIndex_score_2 = 2; #! ie, the 'variance'.
    #!
    #!
    ; # FIXME: get working for .... module_scatter.pm ...
    #! Fetch the rows in the input-file:
    #!
    #! Fetch the lines:
    my $isTo_transpose = 0;
    my $regex_group_row = '(.+)';
    my $inRowNameRegex_useGroupAtIndex = 0; #! ie, the dimension-group in $regex_group_row
    #my $regex_group_row = '^([a-zA-Z]+\-\d+)\-(.+)$';
    my $max_lineCount = 1000000;
    my $isTo_returnLines_notColumns	= 0;
    my ($arr_lines, $arr_colHeader) = aux_matrix::getMatrix_representedAsLines($arr_file_name, $isTo_transpose, $regex_group_row,  $inRowNameRegex_useGroupAtIndex, $max_lineCount, $isTo_returnLines_notColumns);
    my $config_useColIndex_1 = 1; #! ie, 'average'
    my $config_useColIndex_2 = 2; #! ie, 'variance'    
    #!
    #!
    # my @arr_scores = ();
    my %hash_unique_g1;
    my %hash_unique_g2;    
    { #! Pre: construct an array which hold: {dataGroup => , simGroup =>, score_1 =>,  score_2 =>  }.
	#! Note: Construct subsets for teh scores in question 
	my $regex_group_row = '^([a-zA-Z\-]+)\-\d+\-(.+)$'; #! where we merge different eprmtautison of each distrubion, ie, to reduce the number of gorups.
	#my $regex_group_row = '^([a-zA-Z\-]+\-\d+)\-(.+)$';
	#!
	for(my $i = 0; $i < scalar(@{$arr_lines}); $i++) {
	    my $v = $arr_lines->[$i]->[0];	      
	    if($v =~/$regex_group_row/) {
		my $g1 = $1; 		my $g2 = $2;
		{ #! Group=1:
		    my $o1 = $hash_unique_g1{$g1};
		    if(!defined($o1)) {
			$o1 = $hash_unique_g1{$g1} = { arr_1 => [], arr_2 => [] };
		    }
		    #!
		    #! Add scores:
		    push(@{$o1->{arr_1}}, $arr_lines->[$i]->[$config_useColIndex_1]);
		    push(@{$o1->{arr_2}}, $arr_lines->[$i]->[$config_useColIndex_2]);
		}
		{ #! Group=2:
		    my $o2 = $hash_unique_g2{$g2};
		    if(!defined($o2)) {
			$o2 = $hash_unique_g2{$g2} = { arr_1 => [], arr_2 => [] };
		    }
		    #!
		    #! Add scores:
		    push(@{$o2->{arr_1}}, $arr_lines->[$i]->[$config_useColIndex_1]);
		    push(@{$o2->{arr_2}}, $arr_lines->[$i]->[$config_useColIndex_2]);
		}		
	    } else {
		printf("!!\t investigate rowHead\t%s\n", $v);;
	    }
	}
    }
    if(1 == 2)
    { #! Task: Construct bar+satter plots; Iterate throiugy 2d-regex-filtes spanning different data-dsitrubtions
	{
	    my $filePrefix = "sortedOnDistribution";
	    _construct_2d_infoVis_from_hash($result_file_base, \%hash_unique_g1, $filePrefix);
	}
	{
	    my $filePrefix = "sortedOnSimMetric";
	    _construct_2d_infoVis_from_hash($result_file_base, \%hash_unique_g2, $filePrefix);
	}	
    }
    { #! Task: apply HCA-based algorithms
	#! ------------------------------
	#! Idea: ... in tut-3 ... how does the $[distribtion]x[distribtuion]$ pairs relate? what are their depdencency? 
	#! Idea: scope .... 
	# ----------------
	## FIXME: what is the usefulness of making below comparison? 
	#! Idea: ... the scatter-plots in Fig. \ref{} describes the relationship between variance, and mean, of correlation-scores (between different sitributions). The motiviation (of the measurements unlderying the depcited figures) is to reveal the ...??... ... The figures gives idnciatiosn that the dsitribiotns of ...??... have a stronger relationship than ...??... To investigate this assumption, the shortest linkage (compute throught the \textit{Shortest Linkage} (SLINK) \citep{} algorithm) is applied (Fig. \ref{}). The results demonstrte how ...??... 
	# ----------------
	## FIXME: how does this comparison shed light into the influence/impact of using/applying different CCMs?
	#! Idea: ...
	# ----------------
	## FIXME: what clues does these measuremtns ... reveals/provides of how algorithm-cofngirues influences the perofmranc eof AI for Big Data?
	#! Idea: ...
	# ----------------
	## FIXME: what is the point of these visualisiations? 
	## FIXME(InfoVis): fig-concanation:  HCA-figs from ....??...
	## FIXME(InfoVis): fig-concanation: seperately for each gnerated HCA-dot-plot, and then scatter-plots depciting 'relationships in HCA', and relationsshisp NOT in HCA ... use this to: a) tacit ambigity: reveal/exemplify the ambigity in dimesionalty-regudion (which clsutering-algorithms are an exmaple of); b) prior assumption: how similarites in distrihbions depends on the prior assumptions (eg, the chosie of silmairty-metric); c) ...??...
	#! Idea ... researhc such as \cite{} asserts that a) clsutienr-algorithms produce nearly the same clsuter-rpedicitons, b) different CCM-metrics are cosistnet (in their predictions), and c) high-diemsioanl data strongly infleunces the rpedciotn-accuracy of clsuteirng-algorithms. In contrast (to this widely understood view), Fig. \ref{} reveals how ...??...
	# ----------------
	## FIXME: 
	## FIXME(InfoVis): fig-concanation:  
	#! Idea: ...	
	# ----------------
	## FIXME: 
	#! Idea: ...		
	#! ------------------------------
	my $resultFile_base = "tmp-f/";
	#!
	#! Pre: remvove any old files:
	system("rm $resultFile_base"  . "*"); #! ie, clea rthe odl content.		      
	{ #! export seperately to file seprately for diferent columns in the ata.
	    my $resultFile_tmpStorage = "tmp-f-tmpStorage/";
	    #!
	    my $path_conversionScript = "tut_clust_1.exe"; #! eexpected to be gnereated from: "tut_clust-from-folder-1-hca.c"; #! which we expeect is onstructed by: g++ -O2 -g tut-aux-matrixToPPM.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_ppm.exe
	    { #!
		#! Step(0): construc thte result-dir holing temrpaory files generat in this call:
		my $cmd_0 = "mkdir -p $resultFile_tmpStorage/";
		system($cmd_0);
		#! Step(1): construc thte result-dir:
		my $cmd = "mkdir -p $resultFile_base";
		system($cmd);
		#!
		#! Step(2): find the path to the hpLsysi-script:
		#!
		my $found = 0;
		foreach my $prefix ("./", "../", "../../", "../../../") {
		    my $p = $prefix . $path_conversionScript;
		    if( ($found == 0) && (-f $p) && (-x $p) ) {
			$path_conversionScript = $p; #! ie, the complete path to the script.
			$found = 1;
		    }
		}
		if($found == 0) {
		    croak("!!\t Was NOT able to find the covnersion-scrtip \"$path_conversionScript\". Did you  remember compiling it? ... If not, then try: g++ -O2 -g tut_clust-from-folder-1-hca.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_clust_1.exe (found in the hpLysis root/src folder).");
		    
		}
		#if(!(-f $input_file)) {	    printf(STDERR "!!\t Did NOT find input-file=\t$input_file ; please investigate\n");  return -1;	}		
	    }
	    #!
	    #!
	    #! Start construction of [ [data] => clusters => InfoVis ]
	    my @arrFileMeta_useColIndex = (" mean", "variance", "skewness", "kurtosis", "quartile-1"); #! which is sued to imrpvoe redablity of the file-name
	    my $arrFileMeta_useColIndex_index = -1;
	    foreach my $config_useColIndex (1, 2, 3, 4) { #! ie, a subset of the columns in the input-data:
		$arrFileMeta_useColIndex_index += 1;
		my %hash_unique_g1;
		my %hash_unique_g2;    
		#! Note: Construct subsets for teh scores in question 
		my $regex_group_row = '^([a-zA-Z\-]+)\-\d+\-(.+)$'; #! where we merge different eprmtautison of each distrubion, ie, to reduce the number of gorups.
		#my $regex_group_row = '^([a-zA-Z\-]+\-\d+)\-(.+)$';
		#!
		for(my $i = 0; $i < scalar(@{$arr_lines}); $i++) {
		    my $v = $arr_lines->[$i]->[0];	      
		    if($v =~/$regex_group_row/) {
			my $g1 = $1; 		my $g2 = $2;
			my $o1 = $hash_unique_g1{$g1};
			{ #! group=1
			    if(!defined($o1)) {
				$o1 = $hash_unique_g1{$g1} = { arr_1 => [], arr_2 => [] };
			    }
			    #!
			    #! Add scores:
			    push(@{$o1->{arr_1}}, $arr_lines->[$i]->[$config_useColIndex]);
			}
			{ #! Group=2:
			    my $o2 = $hash_unique_g2{$g2};
			    if(!defined($o2)) {
				$o2 = $hash_unique_g2{$g2} = { arr_1 => [], arr_2 => [] };
			    }
			    #!
			    #! Add scores:
			    push(@{$o2->{arr_1}}, $arr_lines->[$i]->[$config_useColIndex_1]);
			    #push(@{$o2->{arr_2}}, $arr_lines->[$i]->[$config_useColIndex_2]);
			}				
		    }
		}
		#!
		#! 
		{ #! Step: Apply Clustering seperately for each of the two cases:
		    #! Substep: iterate through eahc of the hashes, and export a result-file:
		    my @arr_hashCase = ("sortedOnDistribution", "sortedOnSimMetric");
		    my $h_index = -1;
		    foreach my $h (\%hash_unique_g1, \%hash_unique_g2) {
			$h_index++;
			my $hashCase = $arr_hashCase[$h_index];
			#!
			#!
			my @arr_keys = keys(%{$h});
			#! 
			#!
			#!
			#! Find min-column-count:
			my $cnt_min_features = 1000000;
			foreach my $key (@arr_keys) {
			    my $o = $h->{$key};
			    my $cnt = scalar(@{$o->{arr_1}});
			    if($cnt > 5) {
				if($cnt_min_features > $cnt) {
				    $cnt_min_features = $cnt;
				}
			    }
			}
			#!
			#!  Generate the feature-data:
			my @arr_h = ();
			my @arr_r = ();
			foreach my $key (@arr_keys) {
			    my $o = $h->{$key};
			    my $cnt = scalar(@{$o->{arr_1}});
			    if($cnt > 5) {
				if(scalar(@arr_h) == 0) { #! then we add the ehader.
				    @arr_h = ("#! row-name");
				    for(my $i = 0; $i < $cnt_min_features; $i++) {
					push(@arr_h, "col-" . $i);
				    }
				}
				my @row = ($key);
				foreach my $c (@{$o->{arr_1}}) {
				    push(@row, $c);
				}
				push(@arr_r, join("\t", @row));
			    }
			}
			#!
			#! Export the tempraory data:
			if(scalar(@arr_r) == 0) {
			    printf("(empty)\t No data for config_useColIndex:$config_useColIndex, case=$hashCase\n");
			    next;
			}
			#!
			#! Pre: remvove any old files:
			system("rm $resultFile_tmpStorage"  . "*"); #! ie, clea rthe odl content.			
			#!
			#! Export data from this:
			my $result_file = $resultFile_tmpStorage . $hashCase . "-" . $arrFileMeta_useColIndex[$arrFileMeta_useColIndex_index] . ".tsv";
			#my $result_file = $resultFile_tmpStorage . $hashCase . "atColumn" . $config_useColIndex . ".tsv";
			open(FILE_OUT, ">$result_file") or die("(init)\t Unable to open the input-file $result_file\n");
			#! Print out: header:			
			print(FILE_OUT join("\t", @arr_h) . "\n");
			#! Print out: data: 
			print(FILE_OUT join("\n", @arr_r));
			close(FILE_OUT);
			#!
			#!
			{  #! Substep: Apply clustering (eg, "../tut_clust_1.exe tmp-f r-res-tut-3"):
			    #! Note: a call to:  g++ -O2 -g tut_clust-from-folder-1-hca.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_1.exe; valgrind ./tut_1.exe tmp-f r-tut-1
			    #! Note(at-time-of-writing): the MINE metric is sued as a rep-step before the HCA-SLINK-applicaiton:
			    my $cmd = $path_conversionScript . " $resultFile_tmpStorage $resultFile_base";
			    printf("\t(tcluster-feature-file)\t %s\n", $cmd);
			    system($cmd); #! ie, to simplfiy viwing of the data
			    #if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$file_fig \t Was Not able to generate a ppm-file; please investigate\n"); 	}
			}
		    }
		}
	    }
	}
	#!
	#! InfoVis: export result-files to dot-graphviz-figures: 
	{  #! Substep: generate the dot-figure:	    
	    #!
	    #! Fetchc the files:
	    my $regex_files = '(.+)';
	    my $regex_files_cntGroups = 1;
	    my $hash_remapProp = undef;
	    my $dir = $resultFile_base;
	    printf("\tInfoVis(pre): read fiels from dir=\"$dir\"\n");
	    my $res_files = aux_matrix::getFiles_fromDir($dir, $regex_files, $regex_files_cntGroups, $hash_remapProp);
	    if(scalar(@{$res_files}) == 0) {
		croak("!!\t Not any files found in dir=\"$resultFile_base\"");
	    }
	    my $arr_file_name = [];
	    foreach my $o (@{$res_files}) {
		print("\t Add path: " . $o->{path} . "\n");
		my $title = $o->{local_name};
		my @parts = split(/-/, $title);
		shift(@parts); #! ie, remove the part beilvied to be the describing the "tut_clust-from-folder-1-hca.c" custer-lidentife
		$title = join(" ", @parts);
		$title =~ s/[$:\$|\| \t\(\)& _]/ /g;
		$title =~ s/\.tsv//; #! ie, remvoe the .tsv file-ending
		push(@{$arr_file_name}, {path => $o->{path}, title => $title } );
	    }
	    #!
	    #! 
	    my $regex_row = undef;
	    my $folder_path = 'f-result-tut-bias-3-dotfiles-';
	    #! 
	    my $self = module_dotTree::init_self(undef);
	    $self->{regex_group_vertex} = '(.+)'; #! ie, a genralised/tempalteable strategy for goruping. 
	    module_dotTree::module_dotTree_exportAllFiles($self, $arr_file_name, $regex_row, $folder_path);	
	}	
	#!
	#!		

    }
}

# -----------------


# -----------------
if(1 == 0)
{ #! File: 
    #! Idea: ... 
    my $keyword = ""; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "";
    my $arr_file_name = [""];
    my $cutoff_min = -10.0;  my $cutoff_max = 400.0; my $max_lineCount = 10000;    
    my $regex_row = undef;
    # FIXME: use below summary-prop to assert attributes of
    # FIXME: Perl: use our variance-script to summarise all of these files ... use restuls to ....??...
    my $self = module_table::init_self();
    module_table::toTable_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $regex_row);
}


