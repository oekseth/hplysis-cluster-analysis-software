use strict;
use Carp;
#! 
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
#use lib '.'; #! which is used for transposing data, and loading strings of data into an array.
require aux_matrix; #! which is used for transposing data, and loading strings of data into an array.
require module_table;
require module_heatMap;


    

# ***********************************************************

#! -------------------------------------
if(1 == 2) # FIXME: explroe for below:
{ #! File: 
    #! Idea: ... Explore the signifiance of grouping data into disjoint partiions ... To analyse the relationship, the CCM-algorithm methodlogy for autaotmed emtric-configuration is applied (Algorithm \ref{}). ... Table \ref{} depicts the cluster-memberships of different distrubiotns. When comparing the results, we observe how the importance of focsuing on data-distirions (in data-analytics) stronlgly depends on the improtance of related aspects. Examples inlcude the choice of algrotihm-cofngiratuion, understanding of what it takes for a predicted cclsuter-ensamble to be soudn, etc) . .... % FIXME: update a concept-summary-table with these observations ... relating algroithm-buldign-blcosk to teir infleucne ... then incldue the table idnitfying the cosntrutions of different parts when seen theroguh the measure of exeuciton-time.
    my $keyword = "fig-tut2-nclusterHalf-clusterIDs"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = 'Affinity of distributions at $|features|=3000$, interpreted throguh the lense of ... clsutering algrotihms (Table \ref{})'; #! Note: the naming of the f-result-tut-bias-2/result-2-postCCM-randomDistributions-features_img.tsv impleis taht this data-set described a data-set with 3000 features ... as seen from the column-headrs (eg, "uniform-int-3000").
    my $cutoff_min = -1.0;  my $cutoff_max = 400.0;
    my $max_lineCount = 10000;    my $max_rowCount = 50; my $max_colCount = 1000000;
    my $arr_file_name = [
	#	  "#! row-name:	student-t-3000	student-t-1500	student-t-1000	student-t-750	bernoulli-3000	bernoulli-1500	bernoulli-1000	bernoulli-750	binomial-3000	binomial-1500	binomial-1000	binomial-750	cauchy-3000	cauchy-1500	cauchy-1000	cauchy-750	chi-squared-3000	chi-squared-1500	chi-squared-1000	chi-squared-750	discrete-3000	discrete-1500	discrete-1000	discrete-750	exponential-3000	exponential-1500	exponential-1000	exponential-750	extreme-value-3000	extreme-value-1500	extreme-value-1000	extreme-value-750	fisher-f-3000	fisher-f-1500	fisher-f-1000	fisher-f-750	gamma-3000	gamma-1500	gamma-1000	gamma-750	geometric-3000	geometric-1500	geometric-1000	geometric-750	lognormal-3000	lognormal-1500	lognormal-1000	lognormal-750	negative-binomial-3000	negative-binomial-1500	negative-binomial-1000	negative-binomial-750	normal-3000	normal-1500	normal-1000	normal-750	piecewise-constant-3000	piecewise-constant-1500	piecewise-constant-1000	piecewise-constant-750	poisson-3000	poisson-1500	poisson-1000	poisson-750	uniform-int-3000	uniform-int-1500	uniform-int-1000	uniform-int-750	uniform-real-3000	uniform-real-1500	uniform-real-1000	uniform-real-750	weibull-3000	weibull-1500	weibull-1000	weibull-750",
	# "Euclid-k_avg	13.000000	9.000000	7.000000	17.000000	15.000000	13.000000	3.000000	13.000000	10.000000	6.000000	15.000000	12.000000	2.000000	0.000000	11.000000	1.000000	13.000000	17.000000	5.000000	11.000000	16.000000	3.000000	5.000000	8.000000	4.000000	6.000000	2.000000	6.000000	6.000000	18.000000	8.000000	8.000000	1.000000	0.000000	2.000000	7.000000	2.000000	14.000000	11.000000	15.000000	11.000000	11.000000	17.000000	8.000000	10.000000	8.000000	8.000000	11.000000	4.000000	8.000000	3.000000	13.000000	0.000000	4.000000	0.000000	12.000000	9.000000	16.000000	4.000000	17.000000	6.000000	0.000000	1.000000	0.000000	15.000000	12.000000	6.000000	14.000000	1.000000	14.000000	9.000000	0.000000	17.000000	5.000000	0.000000	1.000000",
	"f-result-tut-bias-2/result-2-postCCM-randomDistributions-features_img.tsv",
	];
    foreach my $str_metric ("Euclid", "Pearson", "Kendall", "MINE") {
	my $regex_row = $str_metric; 
	# FIXME: write below function ... add support for column-names ... 
	# FIXME: construct a dondcnced veriosn of below table ... zommed/focced on the ...??..
        my $self = module_heatMap::init_self();
	 module_heatMap::toHeatMap_fromFile($self, $keyword . "-" . $str_metric . ".tex", $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
    }
}
#! -------------------------------------
if(1 == 2) # FIXME: explroe for below:
{ #! File: 
    #! Idea: ... Explore the \textit{power of seperability} for the k-means algorithm. The \textit{power of seperability} refers to the task of idneitfying when differnece in predicted eistmators (eg, score of SSE and Silhouette) indicates a True Positive seperation of clusters. Table \ref{} exemplfies different congitve cases where succh a sepration occure. To explore this, subseciton \ref{} stenciled CCMs (through use of the $\cup, \cap$ operators). ... to explore the boudaries of preciion accuracy, we compare a \textit{wild guess} (denoted as a \textit{random algorithm}) with an algorithm making mulitple iteraitons to incremetnally icnrease sepciity of predictions. ... Fig. \ref{} makes use of well-deifned randomeid feautre-veocts as input (Fig. \ref{}), distrubiotns which are then clustered based on their similairties. The strehgtn of usign well-deinfdd distrubiotns (rahter than images) regards their unambigous interpretaiton, as seen for ... This is in cotnrast to the clsutering of real-life data-seets, where local proeprties (eg, issues in outliers, tacit assumptions of soudn cluster rpedionts, etc.) skews the acucrayc of such an analysis. The results (presetned in this work) provvid soudn/strong arugemtns for how .... 
    #! Note: In the idneitficaiton of PAreto Boudnary for predictions ... regarding the \textit{Euclid-random-worst} algorithm ... the randomness- resutls are used to ... idneitfy the error-trheshold of clusteirng-algorithms ... ie, a wild-guess ... hence, a sound algorithm should otuperofrm this ... 
    my $keyword = "fig-tut2-nclusterHalf-clusterIDs-kmeans-and-random.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = 'The \textit{power of seperaility}, explored through the k-means clustering algorithm';
    my $cutoff_min = -10.0;  my $cutoff_max = 100.0;
    my $max_lineCount = 20;    my $max_rowCount = 50; my $max_colCount = 1000000;
    #my $cutoff_min = -1.0;  my $cutoff_max = 400000.0;
    #my $max_lineCount = 10000;    my $max_rowCount = 50; my $max_colCount = 1000000;
    my $regex_row = 'k_avg|random'; 
    my $arr_file_name = [
	"f-result-tut-bias-2/result-2-postCCM-randomDistributions-features_img.tsv"
	];
    my $self = module_heatMap::init_self();
     module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
}
# ***********************************
#! -------------------------------------
if(1 == 2) # FIXME: explroe for below:
{ #! File: 
    # ... Fig. \ref{} depcits the vairance in feature-scores (for a subset of the points). The feature-scores of the dsitrubitons used as input, first to metrics for covearine-gneeraiotn (Fig. \ref{}). The covariance is sued as input to the clsuteirng-algorithm (Fig. \ref{}), before being analsysed (Fig. \ref{}). 
    #! Idea: ... depcit the peridoic variance in how the feature scores are changing, for whcih we observe there is \textit{No Free lunch} concerning the choise of metrics. To exemplify, the alrge difference between ... indicates that ... Hower, we argue that the issue of \textit{No Free lunch} could be translated into a strategy for high-quality cluster-repdictiosn, ie, as the use of well-defined metris prvodies a congitve bassis for udnerstanding, vand valditing, the acucrayc of lsuter algorithms.
    my $keyword = "fig-tut2-nclusterHalf-features-n300.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = 'Feature-scores at dimension=300 for well-defined distrubitons (Table \ref{})';
    my $cutoff_min = -10.0;  my $cutoff_max = 100;
    my $max_lineCount = 20;    my $max_rowCount = 50; my $max_colCount = 1000000;
    my $regex_row = 'student|bernoulli|discrete|fisher';
    my $arr_file_name = [
	"f-result-tut-bias-2/result-2-randomDistributions-n300-features_img.tsv"
	];
    my $self = module_heatMap::init_self();
     module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
}
#! -------------------------------------
if(1 == 2) # FIXME: explroe for below:
{ #! File: 
    # .... < a permtuation of above >
    #! Idea: ... 
    my $keyword = "fig-tut2-nclusterHalf-features-n1800.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "";
    my $cutoff_min = -10.0;  my $cutoff_max = 100.0;
    my $max_lineCount = 20;    my $max_rowCount = 50; my $max_colCount = 300;
    my $regex_row = 'student|bernoulli|discrete|fisher';
    #my $regex_row = undef;
    my $arr_file_name = [
	"f-result-tut-bias-2/result-2-randomDistributions-n1800-features_img.tsv"
	];
        my $self = module_heatMap::init_self();
     module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
}

# ***********************************
#! -------------------------------------
if(1 == 2) # FIXME: explroe for below:
{ #! File: 
    #! Idea: 
# FIXME: write a enw heatmpa-scirpt which loads "==> f-result-tut-bias-2/result-2-randomDistributions-n1500-features_img.tsv <==" .... both the complete matrix, and a small 'redable' subset (of dim= 20 x 20) .... make use of a pre-avg-normsaiton-step
# FIXME: then write a permtaution .... extract of dstri=[...] across all the differnet feature-dimenions .... 
# FIXME: permtuation: ... a line-plot ... where we aim at seeing the curves of values up--down 
    my $keyword = "fig-tut2-nclusterHalf-differentDims"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "";
    #my $cutoff_min = -10.0;  my $cutoff_max = 400000.0;
    my $cutoff_min = -10.0;  my $cutoff_max = 100.0;
    my $max_lineCount = 20;    my $max_rowCount = 50; my $max_colCount = 300;
    #my $max_lineCount = 50;    my $max_rowCount = 50; my $max_colCount = 1000000;
    my $regex_row = undef;
    my $folder_path = 'f-result-tut-bias-2/';
    my $arr_file_name = [
	# FIXME: what of below props are we to select?
	#$folder_path . "result-2-randomDistributions-n100--Euclid_img.tsv",
	$folder_path .    "result-2-randomDistributions-n900--Euclid_img.tsv",
	$folder_path .    "result-2-randomDistributions-n1800--Euclid_img.tsv",
	$folder_path .    "result-2-randomDistributions-n3000--Euclid_img.tsv",
    # --
	#$folder_path .    "result-2-randomDistributions-n100--PearsonProductMoment_img.tsv",
	$folder_path .    "result-2-randomDistributions-n900--PearsonProductMoment_img.tsv",
	$folder_path .    "result-2-randomDistributions-n1800--PearsonProductMoment_img.tsv",
	$folder_path .    "result-2-randomDistributions-n3000--PearsonProductMoment_img.tsv",
    # --
	#$folder_path .    "result-2-randomDistributions-n100--Kendall_img.tsv",
	$folder_path .    "result-2-randomDistributions-n900--Kendall_img.tsv",
	$folder_path .    "result-2-randomDistributions-n1800--Kendall_img.tsv",
	$folder_path .    "result-2-randomDistributions-n3000--Kendall_img.tsv",
    # --
	#$folder_path .    "result-2-randomDistributions-n100--MINE_img.tsv",
	$folder_path .    "result-2-randomDistributions-n900--MINE_img.tsv",
	$folder_path .    "result-2-randomDistributions-n1800--MINE_img.tsv",
	$folder_path .    "result-2-randomDistributions-n3000--MINE_img.tsv",    
	];
    foreach my $regex_row ('student', 'binomial', 'student-t-3000', 'binomial-3000') {
        my $self = module_heatMap::init_self();
	 module_heatMap::toHeatMap_fromFile($self, $keyword . "-" . $regex_row . ".tex", $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
    }
}




# -----------------------------------------    
if(1 == 2) # FIXME: explroe for below:
{
    #! Idea: ... depict ... how different distributions correlate. Construct seprate subfigures for: [combined, MINE, Euclid, Pearson]. Use this infomraiton to evalaute/deipct issues, and potential, in seperability of metrics. Imprtoantly, the issue in sentity of differnet metriccs are realy attriuted in research. The unawrenss of how \textit{metrics ifnleucnes learning} could be due to the litmiatiosn offered by today's software for Big Data anlayics. The \textit{hpLysis} softare aims at addresisng this issue, as seen for .. 
    # ... idea: depict .. sentity to increaing high-diemsional data versus distruion and sorrelaiton-emtric ... construct figures sperately for cases=[unaffected, partially affected when not normalised/adjsuted, partially affected (unadjusted),  affected]
    # ... .... In the elvauiaotn, the followign InfoVis appraoches are appleid .... when compared to the line-pltos (eg, Fig. \ref{}) the heat-maps provides a zommed-in view. To exemplify, while the line-plot in Fig. \ref{} ehxmapses the trends in ... , the heatmap in Fig. \ref{} provides a direct map between ... and ..., while the birds-eye heat-maps/PPMs (Fig. \ref{}) exemplfies the ...  . Hence, the combiantion of these ... tehqnieus enables an singith into how ... To summarize, ... % FIXME: sumamrizee these train of throughs into a table descirnbg he diffenr aspects of ... 
# % FIXME: ... the resutls are used to explain where rpediciton-differences are observed ... for both the ppm-figure-collecitons ... eg, differnet grey-areas ... relate the heatmaps to the two ppm-figure-collections ... explain what we obseve ... 
    my $keyword = "fig-tut2-nclusterHalf-randomDistributions-";
    my $title = "An evaluation of how high-diemsianltiy of data impairs prediction acucrayc";
    my $cutoff_min = -10.0;  my $cutoff_max = 400000.0;
    my $max_lineCount = 50;    my $max_rowCount = 50; my $max_colCount = 1000000;
    my $regex_row = 'student';
    my $folder_path = 'f-result-tut-bias-2/';
    { $keyword .= "Euclid.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
	my $arr_file_name = [
	    # FIXME: fixec corr-metirc
	    #$folder_path .    "result-2-randomDistributions-n100--Euclid_img.tsv",
	    $folder_path .    "result-2-randomDistributions-n900--Euclid_img.tsv",
	    $folder_path .    "result-2-randomDistributions-n1800--Euclid_img.tsv",
	    $folder_path .    "result-2-randomDistributions-n3000--Euclid_img.tsv",
	    ];
        my $self = module_heatMap::init_self();
	 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
    }
    { $keyword .= "Pearson.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
	my $arr_file_name = [
	    # FIXME: fixec corr-metirc
	#$folder_path .    "result-2-randomDistributions-n100--PearsonProductMoment_img.tsv",
	$folder_path .    "result-2-randomDistributions-n900--PearsonProductMoment_img.tsv",
	$folder_path .    "result-2-randomDistributions-n1800--PearsonProductMoment_img.tsv",
	$folder_path .    "result-2-randomDistributions-n3000--PearsonProductMoment_img.tsv",
	    ];
        my $self = module_heatMap::init_self();
	 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
    }
    { $keyword .= "Kendall.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
	my $arr_file_name = [
	    # FIXME: fixec corr-metirc
	#$folder_path .    "result-2-randomDistributions-n100--Kendall_img.tsv",
	$folder_path .    "result-2-randomDistributions-n900--Kendall_img.tsv",
	$folder_path .    "result-2-randomDistributions-n1800--Kendall_img.tsv",
	$folder_path .    "result-2-randomDistributions-n3000--Kendall_img.tsv",
	    ];
        my $self = module_heatMap::init_self();
	 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
    }
    { $keyword .= "MINE.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
	my $arr_file_name = [
	    # FIXME: fixec corr-metirc
	    #$folder_path .    "result-2-randomDistributions-n100--MINE_img.tsv",
	    $folder_path .    "result-2-randomDistributions-n900--MINE_img.tsv",
	    $folder_path .    "result-2-randomDistributions-n1800--MINE_img.tsv",
	    $folder_path .    "result-2-randomDistributions-n3000--MINE_img.tsv",
	    ];
        my $self = module_heatMap::init_self();
	 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
    }        
}
# -----------------------------------------    
# -----------------------------------------
if(1 == 2) # FIXME: explroe for below:
{ #! Export data from tut-1:
    #! ----
    my $arr_file_name_raw = [
	# FIXME: fixec corr-metirc
	{name => "f-result-tut-bias-1/result-raw-rank-algorithm-x-dimension.tsv",      midfix => "rank"},
	{name => "f-result-tut-bias-1/result-raw-score-algorithm-x-dimension_img.tsv", midfix => "score"},
	];
    #! ----
    if(1 == 2) # FIXME: udpdate below, and then remove this.
    {
	#! Note: the below data-points are also plotted thoruh line-plots ("matrix-to-tex-coordinates.pl")
	#! Note(result): ... seems rather chatoric ... no clear patterns ... 
	#! Note(result): ... 
	#! Note(result): ... 	
	my $keyword = "fig-res-tut-1-heatmap-algorithm-x-dimensixon-";
	my $title = "How true is it that separability in feature-vectors are determined by the feature-count (rather than other contributing factors)?";
	my $cutoff_min = -990.0;  my $cutoff_max = 400000.0;
	my $max_lineCount = 80;    my $max_rowCount = 80; my $max_colCount = 1000000;
	my $regex_row = '.+';
	#!
	foreach my $o (@{$arr_file_name_raw}) {	    
	    #!
	    #! To increase reability, splti the heatmpa into mulitple chunks ... Split object into buckets, ie, ro ease reability:
	    #!
	    #! Step: fetch the lines:
	    #!
	    #! Transpose input-data, and then consstruct an array of row-lines:
	    my $isTo_transpose = 0;
	    my $regex_group_row = "(.+)"; 
	    my $inRowNameRegex_useGroupAtIndex = 0;
	    my $max_lineCount = 10000;
	    my $isTo_returnLines_notColumns = 1;
	    my ($arr_lines, $arr_colHeader) = aux_matrix::getMatrix_representedAsLines([$o], $isTo_transpose, $regex_group_row,  $inRowNameRegex_useGroupAtIndex, $max_lineCount, $isTo_returnLines_notColumns);	    
	    #!
	    #!
	    my $cnt_groups = 20;
	    my $title       = $keyword . $o->{midfix};
	    my $file_prefix = $keyword . $o->{midfix} . ".tex";
	    my $optional_writeToTemporaryFile = 1;
	    my $o_files = aux_matrix::splitLinesArr_intoGroup($arr_lines,  $cnt_groups, $title, $file_prefix, $optional_writeToTemporaryFile);
	    foreach my $s (@{$o_files}) {
		#my $arr_lines = $s->{arr_lines};
		#!
		#!
		my $self = module_heatMap::init_self();
		$self->{file_result} = $s->{result_file};
		#$self->{file_result} = $keyword . $o->{midfix} . ".tex";
		 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, [$s->{result_file}], $max_lineCount, $max_colCount, $regex_row);
	    }
	}
    }
    if(1 == 2) # FIXME: udpdate below, and then remove this.
    { #! ... a pemtuation of above, where we isolate sepeately for ... Minkowski and MINE
	#! Note(result): ... as seen when comparing the 'ranked scores' to the 'scores' 
	#! Note(result): "fig-res-tut-1-heatmap-algorithm-x-dimension-Pearson-score.pdf" ... how diferent permutaitons of Pearon's corrleation coefifcnet (whcih is in use, thoguh not docuemtned) infleuces the understanding of ... the imapct of high-deismoanl data-anlayssi % FIXME: relate this observaiotn to issues in metric-ambigyut <-- % fIXME: what is the eq- which is covered by this?
	#! Note(result): ... 
	#! Note(result): ... 	
	my $keyword_base = "fig-res-tut-1-heatmap-algorithm-x-dimension-";
	my $title = "How true is it that separability in feature-vectors are determined by the feature-count (rather than other contributing factors)?";
	my $cutoff_min = -990.0;  my $cutoff_max = 400000.0;
	#! Note: we rpting out only a subsete of the results (as seen from the $max_lineCount attribute). This to simplfiyt he clutterr/numbe rof figures to tinerpret for the readrs.
	my $max_lineCount = 30;    my $max_rowCount = 30; my $max_colCount = 1000000;
	my $regex_row = '.+';
	foreach my $filter ("minkowski", "Pearson", "MINE", "multinomial", "modulo", "uniform") {
	    #!
	    my $keyword = $keyword_base . $filter . "-";
	    #!
	    my $isTo_transpose = 0;
	    my $regex_group_row = "(.+$filter.+)";
	    my $regex_row = $regex_group_row;
	    my $inRowNameRegex_useGroupAtIndex = 0;
	    
	    my $isTo_returnLines_notColumns = 1;	
	    #!
	    foreach my $o (@{$arr_file_name_raw}) {	    
		#!
		#!
		my $self = module_heatMap::init_self();
		#$self->{file_result} = $s->{result_file};
		$self->{file_result} = $keyword . $o->{midfix} . ".tex";
		 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, [$o->{name}], $max_lineCount, $max_colCount, $regex_row);
	    }
	}	    
    }
    #if(1 == 2) # FIXME: udpdate below, and then remove this.
    { #! Correlation: 
	my $arr_file_name = [
	    {name => "f-result-tut-bias-1/result-score-algorithm-x-dimension--Kendall_img.tsv", midfix => "Kendall-corrDimension"},
	    {name => "f-result-tut-bias-1/result-score-algorithm-x-dimension--MINE_img.tsv", midfix => "MINE-corrDimension"},
	    #{name => "", midfix => "-corrDimension"},
	    ];
	#! Note(result): ...
	#! Note(result): ... 
	#! Note(result): ... 
	#! Note(result): ... 	
	my $keyword_base = "fig-res-tut-1-heatmap-corr-algorithm-x-dimension-";
	my $title = "Strenghts in score-overlap, as measured by correlation metric. How true is it that separability in feature-vectors are determined by the feature-count (rather than other contributing factors)?";
	my $cutoff_min = -990.0;  my $cutoff_max = 400000.0;
	#! Note: we rpting out only a subsete of the results (as seen from the $max_lineCount attribute). This to simplfiyt he clutterr/numbe rof figures to tinerpret for the readrs.
	my $max_lineCount = 30;    my $max_rowCount = 30; my $max_colCount = 1000000;
	my $regex_row = '.+';
	foreach my $filter ("minkowski", "Pearson", "MINE", "multinomial", "modulo", "uniform") {
	    #!
	    my $keyword = $keyword_base . $filter . "-";
	    #!
	    my $isTo_transpose = 0;
	    my $regex_group_row = "(.+$filter.+)";
	    my $regex_row = $regex_group_row;
	    my $inRowNameRegex_useGroupAtIndex = 0;
	    
	    my $isTo_returnLines_notColumns = 1;	
	    #!
	    foreach my $o (@{$arr_file_name}) {	    
		#!
		#!
		my $self = module_heatMap::init_self();
		#$self->{file_result} = $s->{result_file};
		$self->{file_result} = $keyword . $o->{midfix} . ".tex";
		 module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, [$o->{name}], $max_lineCount, $max_colCount, $regex_row);
	    }
	}	    
    }
    #if(1 == 2) # FIXME: udpdate below, and then remove this.
    { #! Correlation: 
	#! Note(result): ... "fig-res-tut-1-heatmap-corr-dimension-x-algorithmdims-Kendall-corrDimension.tex", "fig-res-tut-1-heatmap-corr-dimension-x-algorithmdims-MINE-corrDimension.pdf" ... not any clear pattern in decreased speariblity at higher feature-dimsions. If this boservaiotn holds for the data-distubionts found in real-life data-sets (Fig. \ref{}), then the estlibhed view of .. chalelnges in habdblign high-dmeiaonsl data-sets (Table \ref{}, eq. \ref{}) could be due to \textit{confirmation bias} (eg, that results cofnirming rsearhc-aasusmptions are given a higher weight of signfiancce).
	#! Note(result): ... 
	; # FIXME: ...
	my $arr_file_name = [
	    {name => "f-result-tut-bias-1/result-score-algorithm-x-dimension--transp-Kendall_img.tsv", midfix => "Kendall-corrDimension"},
	    {name => "f-result-tut-bias-1/result-score-algorithm-x-dimension--transp-MINE_img.tsv", midfix => "MINE-corrDimension"},
	    #{name => "", midfix => "-corrDimension"},
	    ];
	#! Note(result): ...
	#! Note(result): ... 
	#! Note(result): ... 
	#! Note(result): ... 	
	my $keyword_base = "fig-res-tut-1-heatmap-corr-dimension-x-algorithm";
	my $title = "Strenghts in score-overlap, as measured by correlation metric: How true is it that separability in feature-vectors are determined by the feature-count (rather than other contributing factors)?";
	my $cutoff_min = -990.0;  my $cutoff_max = 400000.0;
	#! Note: we rpting out only a subsete of the results (as seen from the $max_lineCount attribute). This to simplfiyt he clutterr/numbe rof figures to tinerpret for the readrs.
	my $max_lineCount = 30;    my $max_rowCount = 30; my $max_colCount = 1000000;
	my $regex_row = '.+';
	#foreach my $filter ('.+') 
	{
	    
	    #!
	    my $keyword = $keyword_base . "dims" . "-";
	    #!
	    my $isTo_transpose = 0;
	    my $regex_group_row = "(.+)";
	    #my $regex_group_row = "(.+$filter.+)";
	    my $regex_row = $regex_group_row;
	    my $inRowNameRegex_useGroupAtIndex = 0;
	    
	    my $isTo_returnLines_notColumns = 1;	
	    #!
	    foreach my $o (@{$arr_file_name}) {	    
		#!
		#!
		my $self = module_heatMap::init_self();
		#$self->{file_result} = $s->{result_file};
		$self->{file_result} = $keyword . $o->{midfix} . ".tex";
		module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, [$o->{name}], $max_lineCount, $max_colCount, $regex_row);
	    }
	}	    	
    }    

}

# -----------------------------------------    
# -----------------------------------------
#if(1 == 2) # FIXME: udpdate below, and then remove this.
{
    my $dir = "../f-result-tut-CCM-16";
    #
    #
    if(1 == 2) # FIXME: udpdate below, and then remove this.
    { #! Pre: export artifcal ts-data-sets to PDF:
	my $result_file_meta = "meta-tut-16-ppm.tex";
	open(FILE_OUT, ">$result_file_meta") or die("Unable to open the result-file $result_file_meta\n");
	#my $regex_files = "r-*";
	my $regex_files = 'r-.*artif.*.tsv';
	#my $regex_files = "r-*artificalData*.tsv";
	my $regex_files_cntGroups = 1;	
	my $hash_remapProp = undef; # TODO: cosnider being mroe specific.
	my $arr_files = aux_matrix::getFiles_fromDir($dir, $regex_files, $regex_files_cntGroups, $hash_remapProp);
	foreach my $path (@{$arr_files}) {
	    printf("file\t%s\n", $path->{path});
	    my $result_file = $path->{path};
	    $result_file =~ s/\.tsv/\.ppm/;
	    my $isTo_exportToPDF = 1;
	    aux_matrix::matrixFile_to_ppm($path->{path}, $result_file, $isTo_exportToPDF);
	    my $result_file = $path->{path};
	    $result_file =~ s/\.tsv/\.pdf/;
	    my $str_res = "\\includegraphics[trim = 0mm 0mm 0mm 0mm, clip,keepaspectratio=true,width=widthVariable]{$result_file}\n";
	    print(FILE_OUT $str_res);
	    print($str_res);
	}
	#!
	#!
	close(FILE_OUT);
    }

    #!
    #!
    my %hash_direction_default_boundaryConditions; #! which is used to generate [TP--FP--TN---FN] matrix ... amog others used as input to "tut_ccm_16_compareMatrixAndGold_twoVectors.c"
    #!
    #!    
    #if(1 == 2) # FIXME: udpdate below, and then remove this.    
    my $resultPrefix_base = "fig-tut-16-MinMaxEachCell";
    my %hashOf_keys_direction_transpose;
    my %hashOf_keys_direction_default;    
    #{ my $isTo_transpose = 1; # FIXME: remove this, and inlcue below:
    foreach(my $isTo_transpose = 0; $isTo_transpose < 2; $isTo_transpose += 1)     {
	# FIXME: infoVis[heatMap]: ... load all files into one array, then construct for ... heatmap: "$self->{merge_eachRowAndNotColumns} = 1, $self->{arr_scoreIndex_toUse = []" .... .... idea: ... describe min---max difference for the different CCMs ... how large is the overlap between the different groups (eg, True Postive versus False Psotives)?
	#!
	#!
	# FIXME: permtuation of below ... when parsing the tranpsoed matrices ... eg, for use-cases??
	#!	
	#! Note: file-direciotnaltiy: the "_ccm.tsv" suffix is sued to esnure that we do NOT mix tranpsoed-files with untrasposned/raw files.
	#! Note:r estuls: heatmaps(tranpsoed) are sueful to evlaute/demtierne the influence of differnet bounding-conditions (eg, whtin--between relationshsip versus diemsionality of data)
	my $o_filter_cnt = -1;
	foreach my $o_fileFilter (
	    { regex => '(.+_ccm.tsv)' , title => "All files"},
	    { regex => '(.*(n100)-.*_ccm.tsv)' , title => "Dimension100"}, 	    
	    { regex => '(.*(n500)-.*_ccm.tsv)' , title => "Dimension500"}, 	    
	    { regex => '(.*(n1000)-.*_ccm.tsv)' , title => "Dimension1000"},
	    { regex => '(.*(c2)-.*_ccm.tsv)' , title => "Clusters2"}, 	    	    
	    { regex => '(.*(c1000)-.*_ccm.tsv)' , title => "Clusters1000"},
	    { regex => '(.*(f1t10)res.*_ccm.tsv)' , title => "WithinBeteenSep1"}, 	    	    	    
	    { regex => '(.*(f1t10000)res.*_ccm.tsv)' , title => "WithinBeteenSep10000"}, 	    	    	    
	    ) {
	    $o_filter_cnt += 1;
	    #!
	    if($isTo_transpose) {
		#! eg, for: r-16-n900-c900-f1t10result__ccm.transposed.tsv
		$o_fileFilter->{regex}  =~ s/ccm\.tsv/ccm\.transposed\.tsv/;
		$o_fileFilter->{title} .= "-transposed";
	    }
	    my $regex_files = $o_fileFilter->{regex};
	    # my $regex_files = '(' . $regex_files . ')'; #! ie, as we assuem there is nly one group of ...
	    my $regex_files_cntGroups = 1;	
	    my $hash_remapProp = undef; # TODO: cosnider being mroe specific.
	    my $arr_files = aux_matrix::getFiles_fromDir($dir, $regex_files, $regex_files_cntGroups, $hash_remapProp);
	    #!
	    #! Convert the arr_files into corrrect input for below funciton:
	    my $arr_file_name = []; 
	    foreach my $path (@{$arr_files}) {
		#printf("file\t%s\n", $path->{path});
		my $result_file = $path->{path};
		push(@{$arr_file_name}, {name => $result_file});
	    }
	    #! --------------------------------------
	    #!
	    #!
	    # FIXME: permtuation of below ... when parsing the tranpsoed matrices ... eg, for use-cases??
	    my $local_isTo_transpose = 0; 
	    # FIXME: cosntruct a new tieraiton where: $isTo_transpose = 1;
	    my $regex_group_row = "(.*)";
	    my $inRowNameRegex_useGroupAtIndex = undef;
	    my $max_lineCount = 1000000;
	    my $isTo_returnLines_notColumns	= 1;
	    my $cutoff_min = -100000;
	    my $cutoff_max =  100000;

	    #!
	    #! Fetch the lines:
	    my $emptyColumnsAreToBeAdded = 1;
	    my ($arr_lines, $arr_colHeader) = aux_matrix::getMatrix_representedAsLines($arr_file_name, $local_isTo_transpose, $regex_group_row,  $inRowNameRegex_useGroupAtIndex, $max_lineCount, $isTo_returnLines_notColumns, $emptyColumnsAreToBeAdded);
	    #!
	    #!
	    my $arr_coll_lines = $arr_lines; 
	    #! 
	    #!
	    #! ----------------------------	    
	    my @arr_generateForCases = ();
	    
	    #push(@arr_generateForCases, 1); # FIXME: remove

	    
	    # FIXME: include below:
	    
	    if($o_filter_cnt == 0) { #! then print 'raw' data (ie, all columns), through InfoVis=[heatmap, table].
	    	push(@arr_generateForCases, 1);
	    	push(@arr_generateForCases, 2);
	    	push(@arr_generateForCases, 3);						
	    }
	    push(@arr_generateForCases, 0);						
	    #!
	    #!
	    foreach my $infoVisCase (@arr_generateForCases) {		
		#!
		#!
		my $resultPrefix = $resultPrefix_base;
		#! --------------------------------------
		#!
		#!
		{ #! Merge the rows:
		    my $self = aux_matrix::init_extract_minMax_fromLines();
		    #!
		    #!
		    # FIXME:
		    my $regex_group_row = '(.+)'; #! ie, match call rows.
		    #! Note: below configuraiotn ensures that rows/lines with the same row-name merged?
		    my $arr_nameGroups  = undef; #!< then we assuem that we are intersted in merging lines with the same row-header (an option sueful when the lien-aray ocnissts of lines from mulitple files)
		    $self->{config_mergeRowsWithSameName}    = 1; #! which is used as a snaity-check when the "my $arr_nameGroups  = undef" option is applied+used.
		    #!
		    #!
		    $self->{merge_eachRowAndNotColumns}                   = 0; #! ie, comptue the min--max seperately for each cell.
		    #$self->{inExpert_keysToUse}              = ["diff_max_min"]; #! ie, the data to include.
		    #$self->{format_isTo_returnAsMatrix}      = 1;  #! ie, get the lines as a result-object.
		    # $self->{}      =    ;		   
		    #!
		    #!
		    my $res_object = {}; #! ie, an empty hash (to hold the result-data).
		    # FIXME: include below!!
		    # FIXME: ensure that the column-header is added 
		    # FIXME: ensurre that the column-proeprties are alwyas set
		    #! ----------------------------------
		    #!
		    #! Note: we assume the rows are: row-name:    nEqual / Equal  Equal: Worst/Best       Different: Worst/Best   Equal: Score(worst)     Equal: Score(best)      Different: Score(worst) Different: Score(best)
		    my $T_index_best = 3;
		    my $F_index_best = 6;		    
		    #! ----------------------------------
		    if($infoVisCase == 1) { #! then print 'raw' data (ie, all columns), through InfoVis=[heatmap, table].
			$self->{inExpert_keysToUse}              = ["min"]; #, "max", "cnt_cells"]; #! ie, the data to include.
			#$self->{inExpert_keysToUse}              = ["diff_max_min", "min", "max", "cnt_cells"]; #! ie, the data to include.
			$self->{format_isTo_returnAsMatrix}      = 1;  #! ie, get the lines as a result-object.
			#! 
			($arr_coll_lines) = aux_matrix::extract_minMax_fromLines($self, $res_object, $arr_colHeader, $arr_lines, $regex_group_row, $arr_nameGroups, $cutoff_min, $cutoff_max);
			#! Update the result-file-name
			$resultPrefix .= "-detailedView-min";
			#!
			#!  ---------------------------
			if($isTo_transpose == 0) { #! then update the [TP--FP--TN---FN] matrix:
			    foreach my $line (@{$arr_coll_lines}) {
				my $key = undef;
				my @arrOf_cols = split("\t", $line);
				{ #! find the key:
				    if(ref($line) eq ref([])) { #! then we assuem that the 'line' sia ctually an array of column-cells.
					@arrOf_cols      = @{$line};
					$line = join("\t", @{$line}); #! ie, to handle the case where input is an array-reference
				    }
				    $key = $arrOf_cols[0];
				}
				if($line =~ /^\#/) { next;} #! ie, then ignore.
				if($line =~ /[#]/) {
				    croak("\t\t !!\t\t Why is the following line included?? line=$line");
				}
				if($line =~ /row/) {
				    croak("\t\t !!\t-2\t Why is the following line included?? line=$line");
				}						
				#!
				#!
				my $o = $hash_direction_default_boundaryConditions{$key};
				if(!defined($o)) {
				    $o = {T_min => undef, T_max => undef, F_min => undef, F_max => undef};
				    $hash_direction_default_boundaryConditions{$key} = $o;
				}
				$o->{T_min} = $arrOf_cols[$T_index_best];
				$o->{F_min} = $arrOf_cols[$F_index_best];				
			    }
			}
			#!  ---------------------------
		    } elsif($infoVisCase == 2) { #! then print 'raw' data (ie, all columns), through InfoVis=[heatmap, table].
			$self->{inExpert_keysToUse}              = ["max"]; #, "max", "cnt_cells"]; #! ie, the data to include.
			#$self->{inExpert_keysToUse}              = ["diff_max_min", "min", "max", "cnt_cells"]; #! ie, the data to include.
			$self->{format_isTo_returnAsMatrix}      = 1;  #! ie, get the lines as a result-object.
			#! 
			($arr_coll_lines) = aux_matrix::extract_minMax_fromLines($self, $res_object, $arr_colHeader, $arr_lines, $regex_group_row, $arr_nameGroups, $cutoff_min, $cutoff_max);			
			#! Update the result-file-name			
			$resultPrefix .= "-detailedView-max";
			#!
			#!  ---------------------------
			#!  ---------------------------
			if($isTo_transpose == 0) { #! then update the [TP--FP--TN---FN] matrix:
			    foreach my $line (@{$arr_coll_lines}) {
				my $key = undef;
				my @arrOf_cols = split("\t", $line);
				{ #! find the key:
				    if(ref($line) eq ref([])) { #! then we assuem that the 'line' sia ctually an array of column-cells.
					@arrOf_cols      = @{$line};
					$line = join("\t", @{$line}); #! ie, to handle the case where input is an array-reference
				    }
				    $key = $arrOf_cols[0];
				}
				if($line =~ /^\#/) { next;} #! ie, then ignore.
				if($line =~ /[#]/) {
				    croak("\t\t !!\t\t Why is the following line included?? line=$line");
				}
				if($line =~ /\#/) {
				    croak("\t\t !!\t-1\t Why is the following line included?? line=$line");
				}
				if($line =~ /row/) {
				    croak("\t\t !!\t-2\t Why is the following line included?? line=$line");
				}										
				if($key =~ /row/) {
				    croak("!!\t ivnestigate this case ... as it could be an erorr somehwere");
				}
				; # FIXME: complete.
				my $o = $hash_direction_default_boundaryConditions{$key};
				if(!defined($o)) {
				    $o = {T_min => undef, T_max => undef, F_min => undef, F_max => undef};
				    $hash_direction_default_boundaryConditions{$key} = $o;
				}
				$o->{T_max} = $arrOf_cols[$T_index_best+1];
				$o->{F_max} = $arrOf_cols[$F_index_best+1];				
			    }
			}
			#!  ---------------------------			
		    } elsif($infoVisCase == 3) { #! then print 'raw' data (ie, all columns), through InfoVis=[heatmap, table].
			$self->{inExpert_keysToUse}              = ["avg"]; #, "max", "cnt_cells"]; #! ie, the data to include.
			#$self->{inExpert_keysToUse}              = ["diff_max_min", "min", "max", "cnt_cells"]; #! ie, the data to include.
			$self->{format_isTo_returnAsMatrix}      = 1;  #! ie, get the lines as a result-object.
			#! 
			($arr_coll_lines) = aux_matrix::extract_minMax_fromLines($self, $res_object, $arr_colHeader, $arr_lines, $regex_group_row, $arr_nameGroups, $cutoff_min, $cutoff_max);			
			#! Update the result-file-name
			$resultPrefix .= "-detailedView-avg";			
		    } else {
			$self->{inExpert_keysToUse}              = ["diff_max_min"]; #! ie, the data to include.
			$self->{format_isTo_returnAsMatrix}      = 1;  #! ie, get the lines as a result-object.
			#! 
			($arr_coll_lines) = aux_matrix::extract_minMax_fromLines($self, $res_object, $arr_colHeader, $arr_lines, $regex_group_row, $arr_nameGroups, $cutoff_min, $cutoff_max);
		    }
		    if(ref($arr_coll_lines) != ref([])) {
			my $str_ref = ref($arr_coll_lines);
			croak("!!\t (after-fiulter-min-max-on-lines)\t Error: ref(return-object)='$str_ref'.");
		    }
		    printf("\t (after-fiulter-min-max-on-lines)\t ref(return-object)='%s'\n", ref($arr_coll_lines));
		}
		#! ----------------------------
		{ #! Pre InfoVis: ensure that the rows are ordered cosntienly betwene the figures, ie, to ease the vsiaul comparison between different data-sets:		    
		    my $hash = \%hashOf_keys_direction_default;
		    if($isTo_transpose) {
			$hash = \%hashOf_keys_direction_transpose
		    } 
		    {
			if(scalar(keys(%{$hash})) == 0) { #! then we initate the list:
			    my $index = 1; #! where we start with '1' as the row-name should always be at the first line.
			    foreach my $line (@{$arr_coll_lines}) {
				my $key = undef;
				{ #! find the key:
				    my @arrOf_cols = split("\t", $line);
				    if(ref($line) eq ref([])) { #! then we assuem that the 'line' sia ctually an array of column-cells.
					@arrOf_cols      = @{$line};
					$line = join("\t", @{$line}); #! ie, to handle the case where input is an array-reference
				    }				    
				    $key = $arrOf_cols[0];
				}
				if($line =~ /^\#/) {
				    $hash->{$key} = 0; #! ie, the first index				    
				} else { #! then a data-line:				
				    $hash->{$key} = $index;
				    $index++;
				}
			    }
			} else { #! then we re-order the array based on the key-list.
			    #! Init:
			    my $old_arr = $arr_coll_lines;
			    $arr_coll_lines = []; #! ie, intate it.
			    foreach my $line (@{$old_arr}) {
				push(@{$arr_coll_lines}, "");
			    }
			    #! Set values:
			    foreach my $line (@{$old_arr}) {
				my $key = undef;
				{ #! find the key:
				    my @arrOf_cols = split("\t", $line);
				    if(ref($line) eq ref([])) { #! then we assuem that the 'line' sia ctually an array of column-cells.
					@arrOf_cols =      @{$line};
					$line = join("\t", @{$line}); #! ie, to handle the case where input is an array-reference
				    }
				    $key = $arrOf_cols[0];
				}
				my $index = $hash->{$key};
				if(!defined($index)) {
				    $index = scalar(keys(%{$hash}));
				    $hash->{$key} = $index;
				}
				if($key eq "Jaccard") {    printf("map\t$key\t$index\n");  }
				if($index > scalar(@{$arr_coll_lines})) {
				    #!
				    #! Extend the exisitng array:
				    my $size = scalar(@{$arr_coll_lines});
				    for(my $k = $size; $k < $index+1; $k++) {
					push(@{$arr_coll_lines}, "");
				    }
				}
				#!
				#! Place the lien at the correct location:
				$arr_coll_lines->[$index] = $line;
			    }
			}
		    } 

		}
		#! ----------------------------

		#!
		{ #! Table: export results as a table:
		    #!
		    #!
		    my $title = $o_fileFilter->{title};		    
		    my $result_file = $resultPrefix . $title . "-table.tex";
		    $result_file =~ s/[ \(\)_\.]/-/g; #! ie, make the file-names accesptable to Latex-cmpilation.
		    #!
		    #!
		    my $max_lineCount = 100000;
		    my $regex_row = '(.+)';
		    # #! Add the row-header in front of the other columns:
		    # FIXME: 
		    #unshift(@{$arr_lines}, join("\t", @{$arr_colHeader}));
		    if(ref($arr_lines->[0]) eq ref([])) {
			croak("(referee-error)\t figure out why thi case coccrus!!");			     
		    }
		    my $self = module_table::init_self();
		    printf("\t(generate-result-file:.before-call)\t $result_file \n");
		    my $keyword = $title; 
		    my $cutoff_min = -1000; #! used to imrpvoe redability.
		    my $cutoff_max = 10000; #! to avodi a 'dimeison too large' error.
		    print("\t(pre-generate-table)\t $result_file\n");
		    module_table::toTable($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_coll_lines, $result_file);		
		}
		#! -----------------------------------------------------
		#
		{ #! Export: as a heat-map:
		    my $title = $o_fileFilter->{title};		    
		    my $result_file = $resultPrefix . $title . "-heatMap.tex";
		    $result_file =~ s/[ \(\)_\.]/-/g; #! ie, make the file-names accesptable to Latex-cmpilation.
		    my $cutoff_min = -1000; #! used to imrpvoe redability.
		    my $cutoff_max = 1000; #! used to imrpvoe redability.
		    my $self = module_heatMap::init_self($result_file);
		    my $keyword = $title; 
		    module_heatMap::heatMap($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_coll_lines);
		}

		#
		# FIXME: InfoVis. ... matrix-to-tex-coordinates.pl

		#
		#
		# FIXME: InfoVis. ... 		

		#
		# FIXME: InfoVis. ... scatter-plot
		# case: ... extract only for column= ... 
		# case:  ... use the 'arr_scores' attribuyte (axu_matrix.pm)
		# case: 		
		#
		if($isTo_transpose && ($o_filter_cnt == 0) ) { 
		    #! Note: idea is to construct a bar-chart .... which shows the number of 

		    # FIXME: add a cofnig-filter to our "tut_ccm_16_compareMatrixAndGold_twoVectors.c" .... which prints to a file ... [data-id, CCM] which matches the scoreRange=[...] .... use these results to demonstrate/exemplfiy the abirtaryness of cases where the same CCM is applied <-- FIXME: instead: compute ARI ... list data-cases where the value falls inside a given range ... 

		    # FIXME: add something
		}
		# FIXME: InfoVis. ... bar-chart
		
		#
		# FIXME: InfoVis. ... 	   
		
		#
		# FIXME: InfoVis. ... 	   
		
		#
		# FIXME: InfoVis. ... 	    
	    } 
	}
    }
    #!
    #!
    { #! Export: the [TP--FP--TN---FN] matrix:
	#! Open file:
	my $result_file_meta = $resultPrefix_base . "-boundaries.tsv";
	open(FILE_OUT, ">$result_file_meta") or die("Unable to open the result-file $result_file_meta\n");
	#! 
	my @arr_lines = ();
	#!
	my @arr_cols = ("#! Metric", "True-Min",  "True-Max",  "False-Min",  "False-Max");
	#my @arr_cols = ("#! Row-name: Metric", "True-Min",  "True-Max",  "False-Min",  "False-Max");
	my $str = sprintf("%s\n", join("\t", @arr_cols));
	printf(FILE_OUT $str . "\n");
	push(@arr_lines, $str);
	my @keys = keys(%hash_direction_default_boundaryConditions);
	foreach my $k (@keys) {
	    if($k =~ /row/) {
		croak("!!\t ivnestigate this case ... as it could be an erorr somehwere ... given key='$k'");
	    }
	    # FIXME: are below row-header-names cosnistent with those expected by hpLysis?
	    my $h = $hash_direction_default_boundaryConditions{$k};
	    my $k_renamed = undef;
	    { #! rename the metrics, ie, to ensure consitency:
		#! Note: for detaqiles of below, see "kt_matrix_cmpCluster.c"
		my @arr_remap = (
		    ["Jaccard sensitivty", "Jaccard_sensitivty"],
		    ["Jaccard F1", "Jaccard_F1"],
		    ["DBI Euc", "DBI(Euc)"],
		    ["VariationOfInformation alt2", "VariationOfInformationalt2"],
		    ["vanDogen alt2", "vanDogen_alt2"],
		    ["vanDogen alt3", "vanDogen_alt3"],
		    ["vanDogen alt4", "vanDogen_alt4"],
		    ["vanDogen alt5", "vanDogen_alt5"],		    
		    ["PR alt", "PR_alt"],
		    #["", ""],
		    #["", ""],
		    #["", ""],
		    #["", ""],		    
		    );
		#!
		#!
		foreach my $r (@arr_remap) {
		    if($r->[0] eq $k) {
			$k_renamed = $r->[1]; #! ie, the remap.
		    }
		}
		#!
		#!
		if(!defined($k_renamed)) {
		    $k_renamed = $k; #! ie, then assum that the 'defualt mapping' is accepted by hpLysis.
		}
	    }
	    my @r = ($k_renamed, $h->{T_min}, $h->{T_max}, $h->{F_min}, $h->{F_max});
	    my $str = sprintf("%s", join("\t", @r));
	    printf( FILE_OUT $str . "\n");
	    push(@arr_lines, $str);
	}
	close(FILE_OUT);
	{ #! Step: InfoVis of the above table:
	    #!
	    my $title = "summary of Bounding Conditions";
	    { #! Table: export results as a table:
		    #!
		#!
		my $result_file = $resultPrefix_base . $title . "-table.tex";
		$result_file =~ s/[ \(\)_\.]/-/g; #! ie, make the file-names accesptable to Latex-cmpilation.
		#!
		#!
		my $max_lineCount = 100000;
		my $regex_row = '(.+)';
		# #! Add the row-header in front of the other columns:
		# FIXME: 
		#unshift(@{$arr_lines}, join("\t", @{$arr_colHeader}));
		my $self = module_table::init_self();
		printf("\t(generate-result-file:.before-call)\t $result_file \n");
		my $keyword = $title; 
		my $cutoff_min = -1000; #! used to imrpvoe redability.
		my $cutoff_max = 10000; #! to avodi a 'dimeison too large' error.
		print("\t(pre-generate-table)\t $result_file\n");
		module_table::toTable($self, $keyword, $title, $cutoff_min, $cutoff_max, \@arr_lines, $result_file);		
	    }
	    #! -----------------------------------------------------
		#
	    { #! Export: as a heat-map:
		my $result_file = $resultPrefix_base . $title . "-heatMap.tex";
		$result_file =~ s/[ \(\)_\.]/-/g; #! ie, make the file-names accesptable to Latex-cmpilation.
		my $cutoff_min = -1000; #! used to imrpvoe redability.
		my $cutoff_max = 1000; #! used to imrpvoe redability.
		my $self = module_heatMap::init_self($result_file);
		my $keyword = $title; 
		module_heatMap::heatMap($self, $keyword, $title, $cutoff_min, $cutoff_max, \@arr_lines);
	    }
	    ; # FIXME: udpate our aritlce with above ... ie, when the row-name-issue is resolsved!
	}
    }
}
# -----------------------------------------    
# -----------------------------------------
if(1 == 2) # FIXME: udpdate below, and then remove this.
{
    #! Idea: ... 
    my $keyword = "fig-tut2-nclusterHalf-.tex"; #! used to simplfiy the mapping between Latex-files versus this Perl-script.
    my $title = "";
    my $cutoff_min = -10.0;  my $cutoff_max = 400000.0;
    my $max_lineCount = 50;    my $max_rowCount = 50; my $max_colCount = 1000000;
    my $regex_row = undef;
    my $arr_file_name = [
	""
	];
    #$self->{ylabel}      = "Correlation to bell-shaped hypothesis";
    #$self->{xlabel}      = "The id of \textit{data-distribution} x \textit{pairwise similarity metrics}.";		
    #$self->{file_result} = $s->{file_name};
    my $self = module_heatMap::init_self();
    module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row);
}
# -----------------------------------------    


