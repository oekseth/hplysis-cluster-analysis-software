\\begin{tikzpicture}
  \\begin{axis}[legend pos=south east,
      title={ $title },
      ylabel={ $ylabel },
      xlabel={ $xlabel }, 
]

    \\addplot[
      scatter,only marks,scatter src=explicit symbolic,
      scatter/classes={
         $str_header_internal
      }
    ]
    table[x=x,y=y,meta=label]{
      x    y    label
      $str_rows};
    \\legend{$str_legend}
  \\end{axis}    
