# -MCarp=verbose
use strict;
use strict;
use Carp;
#! 
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
#use lib '.'; #! which is used for transposing data, and loading strings of data into an array.
require aux_matrix; #! which is used for transposing data, and loading strings of data into an array.
require module_linePlot; #! which provides lgoics for genraitng line-plots
require module_heatMap;

sub __convert {
    my ($arr) = @_;
    for my $fileName_img (@{$arr}) {
	my ($f_new) = ($fileName_img =~ /^(.+)\./) ;
	$f_new  =~ s/[\.]/-/g;
	#$f_new  =~ s/[$:\$|\| \t\(\)& \.]/-/g;
	$f_new .= ".pdf";
	$f_new =~ s/_/-/g;
	system("convert $fileName_img $f_new");
	print('\includegraphics    [page=1, width=0.23\textwidth,       trim={0cm 0.0cm 0.0cm 0.0cm},clip    ]    {../hpLysis-handling-of-high-diemensionality/figs/' . $f_new . '}' . "\n");
#	system("convert $fileName_img -resize $globalProp_htmlBuilding__useImageMagic__tileSize $fileName_img_small");
    }
}

if(1 == 2) {
    #! Idea: 
    __convert([
	"result-2-postCCM-randomDistributions--MINE_img.ppm",
	"result-2-postCCM-randomDistributions--Kendall_img.ppm",
	"result-2-postCCM-randomDistributions--PearsonProductMoment_img.ppm",
	"result-2-postCCM-randomDistributions--Euclid_img.ppm",
	      ]);    
    #! Idea: 
    __convert([
	"result-2-randomDistributions-n100--Euclid_img.ppm",
	"result-2-randomDistributions-n900--Euclid_img.ppm",
	"result-2-randomDistributions-n1800--Euclid_img.ppm",
	"result-2-randomDistributions-n3000--Euclid_img.ppm",
	# --
	"result-2-randomDistributions-n100--PearsonProductMoment_img.ppm",
	"result-2-randomDistributions-n900--PearsonProductMoment_img.ppm",
	"result-2-randomDistributions-n1800--PearsonProductMoment_img.ppm",
	"result-2-randomDistributions-n3000--PearsonProductMoment_img.ppm",
	# --
	"result-2-randomDistributions-n100--Kendall_img.ppm",
	"result-2-randomDistributions-n900--Kendall_img.ppm",
	"result-2-randomDistributions-n1800--Kendall_img.ppm",
	"result-2-randomDistributions-n3000--Kendall_img.ppm",
	# --
	"result-2-randomDistributions-n100--MINE_img.ppm",
	"result-2-randomDistributions-n900--MINE_img.ppm",
	"result-2-randomDistributions-n1800--MINE_img.ppm",
	"result-2-randomDistributions-n3000--MINE_img.ppm",    
	      ]);
}
{
    #! Note: below resutls are genreated from "tut_ccm_16_compareMatrixAndGold_twoVectors.c"
    # -------------
    #! Idea:
    # .... configurion: $|clusters|=2, |rows|=|columns|=100, score(within)=100, score(between)=1$, topology=\textit{minInsideMaxOutsideLinearChanging-constantToAdd100-twoNestedLinear} (as depcited in Fig. \ref{}). 
    # --- results:
    # ... improtance of testing lienary of CCMs ...
    # ... ... permtautions of van-Dogen decreases its prediciotn-accucrayc, hence ... 
    # ... ... seems linear for the CCMs ...            
    # ...    
    # ------------------------
    { #! Generate: the aritial ata used for this meausrerment.
	my $regex_files = '(.+\.ppm)$';
	# my $regex_files = '(' . $regex_files . ')'; #! ie, as we assuem there is nly one group of ...
	my $regex_files_cntGroups = 1;	
	my $hash_remapProp = undef; # TODO: cosnider being mroe specific.
	my $dir = "f-result-tut-CCM-16-baseLineCase";
	my $arr_files = aux_matrix::getFiles_fromDir($dir, $regex_files, $regex_files_cntGroups, $hash_remapProp);
	#!
	#! Convert the arr_files into corrrect input for below funciton:
	my $arr_file_name = []; 
	foreach my $path (@{$arr_files}) {
	    # printf("file\t%s\n", $path->{path});
	    my $result_file = $path->{path};
	    push(@{$arr_file_name}, $result_file);
	    #push(@{$arr_file_name}, {name => $result_file});
	}
	__convert($arr_file_name);
    }
    { #! Generate: line-plots of the nroamlized CCM-scores. Provides insight into the trend-lines for the data
	my $file_name = "f-result-tut-CCM-16-baseLineCase/r-16-linearTestCase-n100-c2-f1t100-d20result__ccmDataIdAvgRowNormalized.tsv";
	#! 
	#! result: ... 
	#! result: ... 	
	#!
	#!

	#!
	#! Consstruct an array of row-lines:
	my $cutoff_min = -10.0;  my $cutoff_max = 4000.0;
	for(my $isTo_transpose = 0; $isTo_transpose < 2; $isTo_transpose++) {
	    my $regex_group_row = "(.+)"; 
	    my $inRowNameRegex_useGroupAtIndex = 0;
	    my $max_lineCount = 10000;
	    my $isTo_returnLines_notColumns = 1;
	    #!
	    #!
	    my ($arr_lines, $arr_colHeader) = aux_matrix::getMatrix_representedAsLines([{name => $file_name}], $isTo_transpose, $regex_group_row,  $inRowNameRegex_useGroupAtIndex, $max_lineCount, $isTo_returnLines_notColumns);
	    #!
	    #! Split object into buckets, ie, ro ease reability.
	    my  $cnt_groups = 5;
	    my $title       = "Linearity of CCM predictions";
	    my $file_prefix = "f-result-tut-CCM-16-baseLineCase/r-16-linearTestCase-n100-c2-f1t100-d20result";
	    my $o_files = aux_matrix::splitLinesArr_intoGroup($arr_lines,  $cnt_groups, $title, $file_prefix);
	    my $bucket_index = 0;
	    foreach my $s (@{$o_files}) {
		my $arr_lines = $s->{arr_lines};
		#!
		#!
		#my $file_result = 
		#! 	
		#! Compute: 
		my $self = module_linePlot::init_self();
		$self->{ylabel}      = "The normalized CCM-score";
		$self->{xlabel}      = "An image from the Artificial Data Ensamble";
		$self->{file_result} = "f-result-tut-CCM-16-baseLineCase/baseline-measurements-" . "normalizedData" . "-bucket-" . $bucket_index;
		# = "f-result-tut-CCM-16-baseLineCase/baseline-measurements-" . "normalizedData" . "-bucket-" . $bucket_index;
		if($isTo_transpose) {
		    $self->{xlabel}      = "The id of Cluster Comparison/Convergence Metrics (CCMs).";
		    $self->{file_result} .= "-transposed";
		}
		module_linePlot::construct_basicLinePlot($self, $title, $cutoff_min, $cutoff_max, $arr_lines);
		#!
		$bucket_index++;
	    }
	}
    }
    { #! Generate: a heatmap scowing the CCM-scores
	#my $file_name = "";
	my $title = "A test of linearity of well-defined Artifical Data";
	my $file_name = "f-result-tut-CCM-16-baseLineCase/r-16-linearTestCase-n100-c2-f1t100-d20result__ccmDataId.tsv";
	my $keyword = $file_name;
	#! result: heatmap ... exemplify the large difference in scores (when non-nrosmeided data are sued as input). Provides an example of how the CCMs-scores amy not be interhcangley sued (when comapring resutls, and resehaqrrc, accross dfiiferent research-papers). This holds for both the relative $score_i / score_k $ scores (Eq. \ref{}) and for emausremnts forcucing on a $score_i - score_k $ score-difference (Eq. \ref{}).
	#! result: ... 
	#! result: ...
	my $self = module_heatMap::init_self();
	#$self->{file_result} = $s->{result_file};
	$self->{file_result} = "f-result-tut-CCM-16-baseLineCase/baseline-measurements-" . "raw-CCM-data" . ".tex";	
	my $cutoff_min = -10.0;  my $cutoff_max = 4; # 4000.0;
	#my $cutoff_min = -10.0;  my $cutoff_max = 2; # 4000.0;
	#my $cutoff_min = -10.0;  my $cutoff_max = 10; # 4000.0;
	#my $cutoff_min = -10.0;  my $cutoff_max = 30; # 4000.0;
	#my $cutoff_min = -10.0;  my $cutoff_max = 90; # 4000.0;
	my $max_lineCount = 40;    my $max_rowCount = 30; my $max_colCount = 1000000;
	my $regex_row = '.+';
	module_heatMap::toHeatMap_fromFile($self, $keyword, $title, $cutoff_min, $cutoff_max, [$file_name], $max_lineCount, $max_colCount, $regex_row);
    }    
}
=head
    #! Idea: 
    __convert([
	    "",
    "",
    "",
    "",
    ]);    
#! Idea: 
__convert([
    "",
    "",
    "",
    "",
    ]);    
=cut
