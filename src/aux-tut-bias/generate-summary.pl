use strict;
use Carp;

my $path_hpLysis = "../x_hp";
if(!(-x $path_hpLysis)) {
    printf(STDERR "!!\t unable to find executable=\"%s\", at %s:%d\n", $path_hpLysis, __FILE__, __LINE__);
}
my $extreme_min = -1000000.0;
my $extreme_max = 1000000.0;
#

# FIXME: consider moving the gloal ssited to "globalMetaFile" intot he 'self' object-lgoics
my $file_nameGlobal = "report-summary-prop-figures.tex";
open(FILE_IN_GLOBAL_META, ">$file_nameGlobal") or die("(init)\t Unable to open the input-file $file_nameGlobal\n");

sub update_globalMetaFile {
    my ($fileName, $cnt_legends) = @_; #, $cnt_measurements) = @_,
    printf(FILE_IN_GLOBAL_META "%s\t&%s\n", $fileName, $cnt_legends);
    #printf(FILE_IN_GLOBAL_META "%s\t&%s\t&%s\t\n", $fileName, $cnt_legends, $cnt_measurements);
}

my @arr_props_color = (
    #! Note: for more systmeobls, see: https://no.overleaf.com/learn/latex/Pgfplots_package
    'color=purple,mark=triangle*',
    'color=MidnightBlue,mark=square',
    # ---
    'color=YellowOrange,mark=oplus',        
    # ---
    'color=black,mark=x,solid,dashed',
    #
    'color=pink,mark=-,solid',
    #
    'color=MidnightBlue,mark=-,solid,dashed',
    # ---
    'color=ForestGreen,mark=square',
    # ---
    'color=purple,mark=oplus',        
    'color=black,mark=oplus, thick, densely dashed',        
    'color=blue,mark=x,dotted', 
    #
    'color=MidnightBlue,mark=halfcircle',    
    'color=ForestGreen,mark=pentagon',
    #
    'color=YellowOrange,mark=oplus*',
    'color=black,mark=*,solid',
    'color=pink,mark=x,solid',
    'color=MidnightBlue,mark=oplus,solid,dashed',
    'color=ForestGreen,mark=-',        
    #
    'mark=o,draw=black,fill=black',
    'color=ForestGreen,mark=x',
    'color=YellowOrange,mark=triangle',
    'color=ForestGreen,mark=*',
    'color=purple,mark=diamond,solid',           
    'color=YellowOrange,mark=triangle*',    
    #
    'color=purple,mark=x,solid',
    );


#! Initalize an itnernal structure holding the preorpties to export:
sub init_self {
    my ($regex_group_row, $self_globalSpec, $name_subGroup) = @_;
    my $self = {
	#!
	self_globalSpec => $self_globalSpec,
	dataContext     => "",
	latexString_includeIntoReport => "", #! which is used in the report-genriaootn-rpcoiedure to exprot data.
	#!
	#!
	regex_group_row => $regex_group_row,
	name_subGroup   => $name_subGroup, #! which is the name of the subgroup to be used, ie, to iincrease speivty of the rpedicted relationships (eg, to avodi "spltipoint" to be sued when desciribng algorithms).
	arr_rows => [],
	hash_colNameToIndex => {}, #! which is used to ensure that column-names have a sconsitent idnex %!< Note: roginally dropepd (as we expect that each group is found in all of the input-data we evalaute). However, givne the range of differnelty cosntructed input-file, this atribute become useful (for ensuring cosnistency)
	arr_colName => [], #! which is used in conjucntion with "hash_colNameToIndex" to iantie columns
	arr_uniqueGroupMetricNames => [], #! which is used to hold the set of unique group=>metric names ... of importance during construcitng of informative scatter-plots ... 
	#
	global_row_id => 0, #! which is used to ensure that column-scores refers to the same measurement; increment for eveyr new row.
	#!
	inRowNameRegex_useGroupAtIndex => undef, #! which is used to simplify speciciaotns of which aprt of the row-id to be used.
	#!
	isTo_computeCCM_gold => 0, #! which if set to true implies that cluster-IDs are first sendt to hpLysis, which compute CCM-gold for a given mertric; the result-file is used in 'this anlayssi' as input to the score-fetching-routine, which then 'contiues from where the default valduat-condation-appraoch' works.
	metric_ccm_gold => "Rands-index", #! eg, "Rands-index", "Rands-index (alternative-implementation)", "Adjusted Rand Index (ARI)", "Chi-squared".
	#!
	#! Config-settings used in InfoVis
	config_cntBuckets => 10,
	config_infoVisType_useBarChartNotScatterplot => 1, #! which if set to '1' implies that a BarChart is gnerrated (instead of a Scatterplot) ---- for cases where there is not a 1:1 correpsoncae betwene the axis (ie, not a symmeic proerpty), then both a barchart and a scatter-plot si pritned.
	#!
	#! Data used in Latex-report-generaiton:
	arr_exportedLatex_scatter => [],
	#! Hold a summary of extreme min--max preorpteis for each proerpty:
	hash_xy_score => {}, #! which holds a {xlabel-ylabel} = {min_score => extreme_max, max_score => extreme_min}
	table_tableFiles_extremeProps => [], #! which hold extreme properties for the constructed tables.
    };
    #!
    #!
    return $self;
}


sub _init_column_self {
    my ($col_name) = @_;
    return {
	name => $col_name, cases_evaluated => 0,
	#! Extreme proeprties:
	min_score => undef, min_name => undef,
	max_score => undef, max_name  => undef,
	arr_scores => [], #! which hold all of the scores (for the column in qustion).
    };
}

sub _assign_score_toGroup {
    my ($self, $row_header, $str_group, $score) = @_; #! eg, "distribution=Fisher at split=3000 score=1.5"
    #!
    #! Step: Idnetify the object to update:
    my $row = undef;
    { #! Apply logics:
	my $index_found = undef;
	my $index = 0;
	#! Search:
	foreach my $obj (@{$self->{arr_rows}}) {
	    if(!defined($index_found) && ($obj->{name} eq $row_header) ) {
		$index_found = $index;
	    }
	    $index += 1;
	}
	if(!defined($index_found)) { #! then we intaite it:
	    push(@{$self->{arr_rows}}, 
		 {
		     name => $row_header,
		     arr_cols => [],
		     cases_evaluated => 0,
		 });
	    $index_found = $index; #! ie, the last index after row-insertion.
	}
	$row = $self->{arr_rows}->[$index_found];
    }
    # -------------
    #!
    #! Step: Idneityf the column to update:
    #! Sub-step: sperate the column-index from its attribute:
    my ($col_name, $metric) = split("=", $str_group);
    if(!defined($metric) || !defined($col_name) ) {
	croak("!!\t Group-name=\"$str_group\" did not match the expected pattern; please investigate.");
    }
    #! Sub-step: Identify the local column-index to update (ie, if the score is of interest):
    my $column = undef;
    my $index_found = undef;
    { #! Apply logics:
	my $index = 0;
	foreach my $obj (@{$row->{arr_cols}}) {
	    if(!defined($index_found) && ($obj->{name} eq $col_name) ) {
		$index_found = $index;
	    } else {
		; #printf("\t\t [$row_header][$index_found] \t %s \t not-equal \t $col_name \t \n", $obj->{name});
	    }
	    $index += 1;
	}
	if(!defined($index_found)) { #! then we intaite it:
	    $index_found = $self->{hash_colNameToIndex}->{$col_name};
	    if(defined($index_found)) {
		my $size_current = scalar(@{$row->{arr_cols}});
		if($index_found >= $size_current) { #! then we need to assign emtpy values
		    for(my $i = $size_current; $i < ($index_found + 1); $i++) {
			my $col_name = $self->{arr_colName}->[$i];
			push(@{$row->{arr_cols}}, _init_column_self($col_name));
		    }
		}
	    } else { #! then the column-name is NOT seen before:
		#printf("\t\t [$row_header][$index_found] \t addColName \t $col_name \t \n");
		push(@{$row->{arr_cols}}, _init_column_self($col_name));
		$index_found = $index; #! ie, the last index after row-inseertion.
		#!
		#! Update the global name-mapping-strucutrer ... ie, for this mapping to be acceible+known for others:
		$self->{hash_colNameToIndex}->{$col_name} = $index_found;
		push(@{$self->{arr_colName}}, $col_name);
	    }
	}
	$column = $row->{arr_cols}->[$index_found];
    }
    # -------------
    #!
    { #! Step: Update the column in question:
	$metric =~ s/[_\(\)& ]/-/g; #! ie, to avoid getting problems with Latex
	#if($score != 0) { 	printf("\t\t [$row_header][$index_found] .= \"$str_group\":$score\n");    }
	if(!defined($column->{min_score}) || ($column->{min_score} > $score) ) {
	    $column->{min_score} = $score;
	    $column->{min_name}  = $metric;
	}
	if(!defined($column->{max_score}) || ($column->{max_score} < $score) ) {
	    $column->{max_score} = $score;
	    $column->{max_name}  = $metric;
	}
	#!
	#! Use a hash to ensure that scores found betwen two pairs are unqiue ... of improtance when generating scatter-plots.
	push(@{$column->{arr_scores}}, {
	    metric => $metric, #! which is sued to improve specifyt of scatter-plots ... To exemplify, constructing a scatter-plot with "(x=ncluster)=CCM, (y=dimension)=CCM" would be the same as construct a simple 1d-line-plot with "(x=ncluster=dimension)=score(CCM)" ... hence, by instead constructing a plto with "x(ncluster=2)=CCM, y(dimension=750)=CCM", then the results  is a ... % FIXME: to evlauate the relationships between mulipel preorpteis (eg, ncluster=2) ... sufficent to compare/align differnet scatter-plos? .... if we compare/interest the scatter-plots with the proerpty-table \ref{}, then we observe how ...??...
	    score => $score, global_row_id => $self->{global_row_id}}); #! which is useful when generating scatter-plots (eg, to simplify the task of assessing density in/of dfiferent expalnation-axis).
	#!
	#!
	$row->{cases_evaluated} += 1;
	$column->{cases_evaluated} += 1;
	# printf("\t\t [$row_header][$index_found] .= \"$str_group\":$score w/cases=%u\n", $column->{cases_evaluated});  	
    }
    #!
    { #! Step: keep a globally cosnsitent list of unqieu metric-names:	
	my $index_found = undef;
	my $index = 0;
	#! Search:
	foreach my $obj (@{$self->{arr_uniqueGroupMetricNames}}) {
	    if(!defined($index_found) && ($obj->{name} eq $str_group) ) {
		$index_found = $index;
	    }
	    $index += 1;
	}
	if(!defined($index_found)) { #! then we intaite it:
	    push(@{$self->{arr_uniqueGroupMetricNames}}, 
		 {
		     name => $str_group,
		     #arr_cols => [],
		     cases_evaluated => 0,
		 });
	    $index_found = $index; #! ie, the last index after row-insertion.
	}
	#! Then icnrement the count of cases which are evaluated:
	$self->{arr_uniqueGroupMetricNames}->[$index_found]->{cases_evaluated} += 1;
    }
}


#!
#! Update the parent-object with the scores:
sub updateParentObject_withScore {
    my ($self, $x, $s1_div, $y, $z) = @_;
    if(!defined($self->{self_globalSpec})) {return;}
    #!
    # FIXME: is below mapping correct <-- investiggate the result-files, and see if this make sense.
    #! Case=[x, y] = z:
    _assign_score_toGroup($self->{self_globalSpec}, $x, $y . "=" . $z, $s1_div);
    #! Case=[z, y] = x:
    _assign_score_toGroup($self->{self_globalSpec}, $z, $y . "=" . $x, $s1_div);
    #!
    #! Case=[z, x] = y:
    _assign_score_toGroup($self->{self_globalSpec}, $z, $x . "=" . $y, $s1_div);
}

sub _score_isOf_interest{
    my ($line, $col, $cutoff_min, $cutoff_max) = @_;
    my $isTo_drop = 0;
    if ($col =~ /^-?\d+\.?\d*$/) { if($col > 10000000) { $isTo_drop = 1; } }
    if( ($col eq "inf") || ($col eq "4294967296.000000") || ($col eq "nan") || ($col eq "-nan") || ($col eq "-")) {$isTo_drop = 1;}
    if($isTo_drop == 0) {
	if ($col !~ /^-?\d+\.?\d*$/) { 
	    croak("!!\t : val=\"$col\" is NOT a number, given line=\"$line\".");
	} else {
	    if($col < $cutoff_min) { $isTo_drop = 1; }
	    if($col > $cutoff_max) { $isTo_drop = 1; }
	    if( ($col > -0.001) &&  ($col < 0.001) ) {
		# print("Drop\t score:$col\n");
		$isTo_drop = 1; } #! ie, as we assuemt eh valeu is set to '0' ... ie, to get emanignful differences in repdcitions.
	}
    }
    return ($isTo_drop == 0);
}
    

#! Step: Map the variance to row-IDs (eg,  for all cases where STD != 0)
sub _readFile_getScore_1d {
    my ($self, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row, $result_filesAreInGroup) = @_;
    my $arr = [];
    my $file_index = 0;
    # my %hashOf_evaluatedSymmetricPairs; #! which is used to avoid generating charts for both case1xcase2 and case2xcase1, ie, as latter is overlapping <-- TODO: consider dropping this ... as each of the fgre-cases are alinged to each oher, ehnce it makes a sense having both 'versions of the figures'.
    foreach my $o_file_name (@{$arr_file_name}) {
	my $file_name = $o_file_name->{name};
	open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
	my $row_prefix = "file-" . $file_index;
	my $cnt_lines = 0;
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.
	    if($cnt_lines == 0) {
		$cnt_lines += 1;
		next; #! ie, as we asusmet the file-row-header si NOT to be used in the heatmpa-generaiotn (we in this script cconstruct).
	    }
	    if($line =~/\#/) {$cnt_lines += 1; next;} #! ie, as we assume taht the row-ehader is then NOT of interest
	    if($cnt_lines > $max_lineCount) { next;}
	    {
		if(!defined($regex_row) || ( $line =~ /$regex_row/) ) { #|| (scalar(@{$arr}) == 0) ) {
		    #if(!defined($regex_row) || ( $line =~ /$regex_row/) || (scalar(@{$arr}) == 0) ) {
		    #if($row_prefix ne "") { #! then increase the acucryacy of the row-name
		    #$line = $row_prefix . $line;
		    #}
		    #!
		    #!

		    #! Fetch data from the ccorrect columns:
		    my @arrOf_cols = split("\t", $line);
		    my $row_header = $arrOf_cols[0];
		    #printf("\t\t %s\n", $row_header);
		    my $s      = sprintf("%.1f", $arrOf_cols[2]); #! ie, the expeced index of the "variance" attribute.
		    my @arr_score = ($s);
		    if($self->{isTo_computeCCM_gold} == 1) { #! then we use all the scores.
			for(my $i = 1; $i < scalar(@arrOf_cols); $i++) {
			    my $s = $arrOf_cols[$i];
			    if($s =~ /[a-z]+/i) {
				croak("!!\t : [$cnt_lines].val[$i]=\"$s\" is NOT a number, given line=\"$line\"\t \t row_header=\"$row_header\", and file=\"$file_name\".");
			    }
			    push(@arr_score, $s);
			}
		    }
		    #printf("\t\t %s\n", $row_header);
		    my $row_header_raw = $row_header;
		    foreach my $score (@arr_score) { #! then seperately insert for each of he scores:
			#!
			#! Test if the sccore is of interest, ie, that it is inside the range viwed as of interest.
			if(_score_isOf_interest($line, $score, $cutoff_min, $cutoff_max)) { #! Then split the row-header, ie, to get the important parts of the data.
			    my $regex = $self->{regex_group_row};
			    if($row_header_raw =~ /$regex/) {
				my $val_meta = $2;
				$row_header = $1;
				if(defined($self->{inRowNameRegex_useGroupAtIndex} )) {
				    if($self->{inRowNameRegex_useGroupAtIndex} == 1) {
					$val_meta = $2; # TODO: is this always correct?
					$row_header = $1;
				    } elsif($self->{inRowNameRegex_useGroupAtIndex} == 2) {
					$val_meta = $1; # TODO: is this always correct?
					$row_header = $2;
				    } elsif($self->{inRowNameRegex_useGroupAtIndex} == 3) {
					$val_meta = $1; # TODO: is this always correct?
					$row_header = $3;
				    } else {
					croak("!!(update-script)\t add support for self->{inRowNameRegex_useGroupAtIndex}='$self->{inRowNameRegex_useGroupAtIndex}'");
				    }
				}
				#! Note: handle for case="row-Relative-Avg-k_rank"
				# # column-Avg-disjoint_ccm_dbSCAN
				if($row_header =~/(column|row)(\-.+?)\-([a-z_]+)$/i) {
				    my $old_name = $row_header;
				    $row_header = $3;
				    $row_header =~ s/[_\(\)& ]/-/g;
				    _assign_score_toGroup($self, $row_header, "normalization=" . $1 . $2, $score); #! eg, "distribution=Fisher at split=3000 score=1.5"				
				    #_assign_score_toGroup($self, $row_header, "normalization=" . $1 . "-" . $2, $score); #! eg, "distribution=Fisher at split=3000 score=1.5"				
				    #printf("\t\t(row-name-Changed)\t old\t $old_name \t new \t $row_header\n");
				} else {
				    #printf("\t\t(row-name-unchanged)\t $row_header\n");
				    $row_header =~ s/[_\(\)& ]/-/g;
				}
				#! Note: for each of the inserted column-attributes we compute properties={[total], min, max}.
				my $name_subGroup = $self->{name_subGroup} . "="; #! eg, "splitPoint="
				_assign_score_toGroup($self, $row_header,  $name_subGroup. $val_meta, $score); #! eg, "distribution=Fisher at split=3000 score=1.5"
				#! Then assign scores to the group-IDs in question (fethced from the ddirecitory-name):
				foreach my $group (@{$result_filesAreInGroup}) {
				    #! Note: correct to send the 'score-attribute' multipel times? <-- yes: the same score refers to different parameters ... each parameter reflects a diemsion (in data analtyics), hence this approach focus on fidnign the maximum construbitioni of each .... this is useful when comapring the effects of diemsrent dimensions 
				    _assign_score_toGroup($self, $row_header, $group, $score); #! eg, "distribution=Fisher at split=3000 score=1.5"
				}
				#!
				#! Add proepteis extracted from the file-name:
				my @arr_prop_fileName = @{$o_file_name->{groups}};
				foreach my $group (@arr_prop_fileName) {
				    _assign_score_toGroup($self, $row_header, $group, $score); #! eg, "distribution=Fisher at split=3000 score=1.5"
				}
			    } else {
				croak("!!\t row-header=\"$row_header_raw\" did NOT have a match for regex='$regex', given line: $line");
			    }
			}
		    }
		    #!
		    #!
		    $cnt_lines += 1;
		    #!
		    #! Increment the global row-id-prop ... of importance when merging mulitple scores (eg, STD-scores from differnet measuremetns) ... the stnrehgt of this appraoch relates t the fleciblity, ie, that we do not need to hardcode cases ... hence, reduces the analytical bias in this approach.
		    $self->{global_row_id} += 1;
		}
	    }
	}
	close(FILE_IN);
	$file_index += 1;
    }
    # #! Costruct the Latex-table:
    #__toHeatMap($keyword, $title, $cutoff_min, $cutoff_max, $arr);
}


#! Call the hpLsysis bash-script:
sub _exec_hpLysis_CCM_gold {
    my ($metric_ccm_gold, $file_holding_cluster, $result_file) = @_;
    #! Note: examples of $metric_ccm_gold: ("Rands-index", "Rands-index (alternative-implementation)", "Adjusted Rand Index (ARI)", "Chi-squared")
    #! Step(): Compute the clusters:
    #my $result_file = $tmpFile_ccm;
    #my $cmd = "./x_hp -ops=$ccm_gold -file2=$tmpFile_rCluster -file1=$tmpFile_clusters -out=$result_file";
    #! Note: an example-call: ../x_hp -ops=Rands-index -file2= f-result-tut-bias-2/result-2-postCCM-randomDistributions-features_img.tsv -file1=f-result-tut-bias-2/result-2-postCCM-randomDistributions-features_img.tsv -out=tmp-clusterSim.tsv
    my $cmd = $path_hpLysis . " -ops=\"$metric_ccm_gold\" -file1=$file_holding_cluster -file2=$file_holding_cluster -out=$result_file";
    print("(CCM-gold)\t" . $cmd . "\n");
    my $result_str = `$cmd`;
    #if(1 == 2)
    {
	print("\t(result-CCM-gold)\t" . $result_str);
    }
}    

#! Step: use a regex to filter for files, and then fetch the score for each file:
sub readDirectory_mapFile_toScore {
    my ($self, $regex_files, $cutoff_min, $cutoff_max, $max_lineCount, $max_colCount, $regex_row) = @_;
    #! 
    my $file_index = 0;
    foreach my $obj (
	#! Note: for detals of below (re-name folders) see the content in our "readme.md":
	#! Note: the "=" is used to splti the attribute from the row.
	# FIXME: validate that dir => "f-result-tut-bias-2" follows the udpated naming-conventions ... then re-run the measuremtns.
	{dir => "f-result-tut-bias-2",                                 arr_group=> ["ncluster=\$|distributions|\$", "normalization=none"] },
	{dir => "f-result-tut-bias-2-ncluster2-Minkowski-metrics",      arr_group=> ["ncluster=2"], "normalization=none" },
	{dir => "f-result-tut-bias-2-ncluster2-Euclid-normalized-data", arr_group=> ["ncluster=2"] },
	{dir => "f-result-tut-bias-2-ncluster2-normAvgRelative", arr_group=> ["ncluster=2", "normalization=avgRelative"] },
	{dir => "f-result-tut-bias-2-ncluster2-normRelativeStdAbs", arr_group=> ["ncluster=2", "normalization=relativeSTDabs"] },
	{dir => "f-result-tut-bias-2-ncluster2-normRelativeStd", arr_group=> ["ncluster=2", "normalization=relativeSTD"] },	
	{dir => "f-result-tut-bias-2-normRelativeStd", arr_group=> ["ncluster=\$|distributions|\$", "normalization=relativeSTD"] },
	{dir => "f-result-tut-bias-2-normAvgRelative", arr_group=> ["ncluster=\$|distributions|\$", "normalization=normAvgRelative"] },
	# ---
	{dir => "f-result-tut-bias-2-normRankRelative", arr_group=> ["ncluster=\$|distributions|\$", "normalization=normRankRelative"] },
	{dir => "f-result-tut-bias-2-ncluster2-normRankRelative", arr_group=> ["ncluster=2", "normalization=normRankRelative"] },
	# ---
	# TODO: complete: 
	# {dir => "", arr_group=> [] },
	# {dir => "", arr_group=> [] },
	) {
	my $dir = $obj->{dir};
	my @files;
	opendir(DIR, $dir) or die "Unalbe to open dir=\"$dir\": " . $!;
	@files = sort {(stat $a)[1] <=> (stat $b)[1]} readdir(DIR);
	my @arr_file_name = ();
	foreach my $file_name (@files) {
	    if($file_name =~ /$regex_files/) {
		#!
		my @arr_prop_fileName = ();
		{ if($file_name =~ /\-n(\d+)-/) {
		    #! Note: the "=" is used to splti the attribute from the row.
		    push(@arr_prop_fileName, "dimension=" . $1);
		    #$row_prefix = "dim=" . $1 . ",";
		  } #elsif($file_name =~/postCCM-randomDistributions-features_img/) {
		  #    $row_prefix = "clusterIDs" . ",";
		  #}
		  if($file_name =~ /\-n\d+\-\-([a-zA-Z]+)-corrDeviation/) { 
		      #! Note: the "=" is used to splti the attribute from the row.
		      push(@arr_prop_fileName, "metric=" . $1);
		      #$row_prefix .=  "" . $1 . ", "; #! eg, PearsonProductMoment
		  }
		  #printf("file_name=$file_name\n");
		  if($file_name =~/(Euclid)\-(column|row)\-(.+)\-corrDeviation/i) {
		      #! Then we assume a data-nrozation is applied:
		      my $str = "normalization=" . $2 . "-" . $3;
		      #printf("\t(file-prop::norm)\t$file_name\t $str\n");
		      push(@arr_prop_fileName, $str);
		  }
		}
		#!
		#!
		my $file_longPath = $dir . "/" . $file_name;
		if($self->{isTo_computeCCM_gold} == 0) {
		    #!
		    push(@arr_file_name, {name => $file_longPath, groups => \@arr_prop_fileName} );
		} else {
		    #!
		    #! Add the CCM-prop which is used:
		    push(@arr_prop_fileName, "CCM=" . $self->{metric_ccm_gold});
		    #!
		    #! Call the hpLysis-bash-eval-script:
		    my $result_path   = "tmp-cluster" . "/" . "input" . $file_index . ".tsv";
		    _exec_hpLysis_CCM_gold($self->{metric_ccm_gold}, $file_longPath, $result_path);
		    #!
		    #!
		    push(@arr_file_name, {name => $result_path, groups => \@arr_prop_fileName} );
		}
		$file_index += 1;
	    }
	}
	#! Read the files, and store summary-props into the $self data-structure:
	_readFile_getScore_1d($self, $cutoff_min, $cutoff_max, \@arr_file_name, $max_lineCount, $max_colCount, $regex_row, $obj->{arr_group});
    }
    close(DIR);
}


#! Exprot the table to a PDF-file, and then include the table into the auto-generatedd result-report.
sub export_tableToFile_self {     
    my ($self, $str_table_innerBody, $result_file_prefix, $title, $yLabel) = @_;
    my $result_file = $result_file_prefix;
    $result_file =~ s/\.tex/-exported\.tex/;
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
    #!
    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");
    #!    
    print(FILE_OUT qq(
%! Title: $title 
\\documentclass[border=9]{standalone}
\\usepackage[table,x11names,usenames, dvipsnames]{xcolor}
\\definecolor{lightgrey}{rgb}{0.9,0.9,0.9}
\\begin{document}
$str_table_innerBody
\\end{document}
));
    close(FILE_OUT);
    #!
    {
	my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	printf("\t(scatter-compile)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $file_fig = $result_file;
	$file_fig =~ s/\.tex/\.pdf/;
	if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$file_fig \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $o_specSummary = {path => $file_fig, title => $title,  yLabel => $yLabel, 
			     class => $title, #! where "class => $title" is sued to ensure that only one table is part of each image ... ie, given the (expected) alrge size of each table.
			     #metricGroup => $str_case,
			     dataContext => $self->{dataContext},
	};
	push(@{$self->{arr_exportedLatex_scatter}}, $o_specSummary);
	if(defined($self->{self_globalSpec})) { #! then add this summary-tabel to the summary-report
	    push(@{$self->{self_globalSpec}->{arr_exportedLatex_scatter}}, $o_specSummary);
	}
	{ #! Add exproted file to meta-file, ie, to Simplify understanding of data-files
	    my $cnt_rows = scalar(split("\n", $str_table_innerBody));
	    update_globalMetaFile($file_fig, $cnt_rows);
	}
	#!
	return $file_fig;
    }
}

#! Step: split the row-names (ie, file-name + row-name) into parts. Use this to idnietyf the min--max attribute for each case.
#! Note(example): ["result-2-randomDistributions-n3000--MINE-corrDeviation.tsv","student-t-3000"] => student-t & (MINE & STD:0) & (n3000 & STD:0)
#! Note(example):
#! Note(example): 
sub infoVis_constructTable_textual {
    my ($self, $result_file, $title, $isTo_plotFor_relative, $yLabel) = @_;
    my $figLabel = $result_file;
    #!
    #!
    my $sep_col = " & ";
    my $sep_row = ' \\\\ ';    
    my $sep_row_block = "\\rowcolor{lightgrey}";
    #!
    $figLabel =~ s/\.tex//;
    #!
    #!
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
    #! ---------------------------------------------------
    #!
    #! Store extmree valeus asoteid to the table:
    my $s_global_min     = $extreme_max;
    my $s_local_diff_min = $extreme_max;
    #! 
    my $s_global_max     = $extreme_min;
    my $s_local_diff_max = $extreme_min;    
    #! ---------------------------------------------------
    #!
    my @arr_str_header = ("Case", "Observations"); # getStrHeader_self($self); #! "metric
    my $str_header_isSet = 0;
    my $col_placement = "lc";
    # 
    my $str_header = "";
    my $str_rows = "";
    #!
    #! Iterate throguh the rows:
    my $index = 0;
    my $index_rowColor = 0;
    my $cnt_cols_added = 0; #! which is used to avoid printing empty tables.
    # printf("(info)\t iterates aross |rows|=%u\n", scalar(@{$self->{arr_rows}}));
    foreach my $row (@{$self->{arr_rows}}) {
	if($str_header_isSet == 0) { #! then construct he row-header:
	    foreach my $obj (@{$row->{arr_cols}}) {
		my $prefix = $obj->{name};
		if($isTo_plotFor_relative == 0) { #! then genreate a detialed view:
		    foreach my $case ("min-score","min-name", "max-score", "max-name") {
			my $str = $prefix . "-" . $case;
			push(@arr_str_header, $str);
			$col_placement .= "c";
		    }
		} else { #! then print out the extreme variations:
		    my $str = $prefix;
		    push(@arr_str_header, $str);
		    $col_placement .= "c";
		}
	    }
	    $str_header_isSet = 1;
	}
	{ #! then add the row-value:
	    my @result_prop = ($row->{name}, $row->{"cases_evaluated"});
	    #print("\t Str(rows|$index)=" . $row->{name} . " w/|cols|=" . scalar(@{$row->{arr_cols}}) . "\n");
	    foreach my $obj (@{$row->{arr_cols}}) {
		#!
		if($isTo_plotFor_relative == 0) { #! then genreate a detialed view:
		    my @arr_keys = ("min_score", "min_name", "max_score", "max_name");
		    foreach my $key (@arr_keys) {
			push(@result_prop, $obj->{$key});
		    }
		} else { #! then print out the extreme variations:
		    my $score_min = abs($obj->{"min_score"});
		    my $score_max = abs($obj->{"max_score"});
		    my $diff = "inf";
		    if( ($score_min != 0) && ($score_max != 0) ) {
			$score_min = abs($score_min);
			$score_max = abs($score_max);
			#!
			$diff = $score_max / $score_min;
			#!
			#!
			{ #! then we Global summary-rpops for this file, as stored in "table_tableFiles_extremeProps".
			    if($s_global_min > $score_min) {
				$s_global_min = $score_min;
			    }
			    if($s_global_max < $score_max) {
				$s_global_max = $score_max;
			    }		
			    #! Note: as we in the below lgocis makes use of differert refernece-atitrubtes when comoputing 'diff' rather than min--max, these valeus are Not drieclty interoperable.
			    if($s_local_diff_min > $diff) {
				$s_local_diff_min = $diff;
			    }
			    if($s_local_diff_max < $diff) {
				$s_local_diff_max = $diff;
			    }
			}

		    }
		    push(@result_prop, sprintf("%.1f", $diff));
		    $cnt_cols_added += 1;
		}		    
	    }
	    $str_rows .= join($sep_col, @result_prop) . $sep_row . "\n";
	}
	#print("\t Str(rows)=" . $str_rows);
	$index += 1;
	$index_rowColor += 1;
	if($index_rowColor == 2) {
	    $str_rows .= $sep_row_block . "\n"; #! ie, to rease readability.
	    $index_rowColor = 0;
	}
	#print("\t Str(rows)=" . $str_rows);	
    }
    #!
    if($cnt_cols_added > 0) { #! which is used to avoid printing empty tables.
	# print("\t Str(rows)=" . $str_rows);
	#!    
	my $str_header = join($sep_col, @arr_str_header) . $sep_row;
	my $str_table_innerBody = qq( \\begin{tabular}{$col_placement}
  $str_header
      \\hline
 % ----------------------------------------      
$str_rows
 \\end{tabular});
	my $dataContext = $self->{dataContext};
	my $str_table = sprintf( qq(\\begin{table*}[t!]
\\caption{ 
$dataContext 
$title
} \\label{$figLabel}
{ %\\footnotesize
$str_table_innerBody
}
\\end{table*}
				 ));
    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");
    print(FILE_OUT $str_table);
    close(FILE_OUT);
    #! Exprot the table to a PDF-file, and then include the table into the auto-generatedd result-report.
    my $title_prefix = "";
    if($isTo_plotFor_relative == 1) {
      $title_prefix = "For scores = relative: ";
    }
    my $path_pdfFile = export_tableToFile_self($self, $str_table_innerBody, $result_file, $title_prefix . $title, $yLabel);
#! ------------------------------------------------------------------------------------------------------------
#!
  { #! then we Global summary-props for this file, as stored in "table_tableFiles_extremeProps".
    if( ($s_global_min != $extreme_max) && ($s_global_max     != $extreme_min) ) { #! then we add the file to the global summary-spec:
       my $s_diff = $s_global_max / $s_global_min;
       #! Note: the 'url-attribue' is sued to simplify pintinf of file-names ... and may simplfiy publishing of the resuls ... ie, as the resuls may be uplaoded to a data-server.
       push(@{$self->{table_tableFiles_extremeProps}}, ["\\url{" . $path_pdfFile . "\}", $s_global_min, $s_global_max, $s_diff, $s_local_diff_min, $s_local_diff_max]);
    }
  }
#! ------------------------------------------------------------------------------------------------------------
#!
#!
}
}

sub __point_is_between_extreme_ranges {
    my ($score, $min, $max) = @_;
    if( ($score > $min) && ($score < $max) ) {
	return 1;
    } else {return 0;}
}

sub _exportHistoCountBucket_BarChart {
    my ($self, $result_file, $matrix, $arr_rowNames, $title, $xlabel, $yLabel) = @_;
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;

    #! Get exrreme props:
    my $score_min = 1000000000.0; my $score_max = -100000000000.0;
    foreach my $row (@{$matrix}) {
	foreach my $score (@{$row}) {
	    if($score_min > $score) {
		$score_min = $score;
	    }
	    if($score_max < $score) {
		$score_max = $score;
	    }	    
	}
    }
    if($score_min == $score_max) {
	return; #! ie, as there is not any sicant data-items.
    }
    $score_min = abs($score_min);
    $score_max = abs($score_max);    
    #!
    #! Step: cosntruct bucket-range:
    my @arr_bucket = ();
    my $size_each = ($score_max - $score_min)/ (1.0 * $self->{config_cntBuckets}); #! where '1.0' is sued to convert the config_cntBuckets attribute to a floating-point number.   
    my $curr_min = $score_min;
    for(my $i = 0; $i < $self->{config_cntBuckets}; $i++) {
	my $curr_max = $curr_min + $size_each;
	push(@arr_bucket, {min => $curr_min, max => $curr_max
	     });
	$curr_min += $size_each;
    }


    #!
    #! Fill the buckets:
    my @mat_result = ();
    my $row_index  = 0;
    #my @arr_rowNames = ();
    my @arr_str_legend = ();
    foreach my $row (@{$matrix}) {
	{ #! Intiate the measurrement---count(parameter) bucket:
	    my @r_row = ();
	    my $add_legend = (scalar(@arr_str_legend) == 0);
	    for(my $i = 0; $i < $self->{config_cntBuckets}; $i++) {
		my $b = $arr_bucket[$i];
		my $str = sprintf("%.1f--%.1f", $b->{min}, $b->{max});
		push(@r_row, {count => 0, name => $str});
		if($add_legend) {
		    push(@arr_str_legend, $str);
		}
	    }
	    push(@mat_result, \@r_row);
	}
	#!
	#! Idnitfy the number of apraemters asostied toe ach extrmee preiciton:
	# my $row_header = shift(@{$row});
	foreach my $score (@{$row}) {
	    #!
	    #! Find the bucket-id:
	    my $bucket_id = $self->{config_cntBuckets}; # ie, the extreme case.
	    {
		my $bucket_index = 0;
		foreach my $bucket (@arr_bucket) {
		    if( ($score >= $bucket->{min})  &&  ($score <= $bucket->{max}) ) {
			$bucket_id = $bucket_index;
		    }
		    $bucket_index += 1;
		}
	    }
	    #!
	    #! Increment the count assotied to this score
	    # Note: why is this count of interest?? <-- number of different parameters assitedd to it ... Hence, the number of freedoms.
	    $mat_result[$row_index]->[$bucket_id]->{count} += 1;
	}
	$row_index += 1;
    }
    my $str_data = "";
    #! Note: examples are given in "tut-barChar.tex",  http://pgfplots.net/tikz/examples/bar-plot/ , https://tex.stackexchange.com/questions/101320/grouped-bar-chart , http://pgfplots.net/tikz/examples/stacked-bar-plot/ , 
    
    { #! Construct the specification
	foreach my $row (@mat_result) {
	    #! Note: consturct a emasurement-line such as: \addplot coordinates {(tool8,7) (tool9,9) (tool10,4)}; ... or: ... \addplot[style={bblue,fill=bblue,mark=none}]    coordinates {(EgyptHD, 1.0) (Hover,1.0) (Navi,1.0)}
	    my @arr_str_coord = ();
	    foreach my $b (@{$row}) {
		push(@arr_str_coord, sprintf("(%s, %u)", $b->{name}, $b->{count}));
	    }
	    $str_data .= "\\addplot coordinates {" . join(" ", @arr_str_coord) . "};\n";
	}
    }
    #!
    #!
    my $str_legend_block = join(", ", @arr_str_legend);
    #! Note: the str_legend_rows .... refers to the name of each row ... 
    my $str_legend_rows = join(", ", @{$arr_rowNames});
    #!
    #!
    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");        
    #!    
    if(1) { #! then we sue the udpated style:
	print(FILE_OUT qq(
\\documentclass[border=10pt]{standalone}
\\usepackage{pgfplots}
\\pgfplotsset{width=7cm,compat=1.8}
\\begin{document}
\\begin{tikzpicture}
\\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    ylabel={\\#Freedoms},
    symbolic x coords={16881.0--16903.5, 16903.5--16926.0, 16926.0--16948.5, 16948.5--16971.0, 16971.0--16993.5, 16993.5--17016.0, 17016.0--17038.5, 17038.5--17061.0, 17061.0--17083.5, 17083.5--17106.0},
      nodes near coords,
      nodes near coords align={vertical},
      x tick label style={rotate=45,anchor=east},
    %xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    % height=8cm, width=15.5cm,
    bar width=0.4cm,
    ymajorgrids, tick align=inside,
    major grid style={draw=white},
    enlarge y limits={value=.1,upper},
    % ymin=0, ymax=100,
    % axis x line*=bottom,
    % axis y line*=right,
    %y axis line style={opacity=0},
    %tickwidth=0pt,
    enlarge x limits=true,
    legend style={
      at={(0.5,-0.2)},
      anchor=north,
      legend columns=-1,
                  /tikz/every even column/.append style={column
                    sep=0.5cm}
                          },
    ]
));
    } else {
    print(FILE_OUT qq(\\documentclass[border=10pt]{standalone}
\\usepackage{pgfplots}
\\pgfplotsset{width=7cm,compat=1.8}
\\begin{document}
\\begin{tikzpicture}
\\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    ylabel={\\#Freedoms},
    symbolic x coords={$str_legend_block},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    ]
));
    }
    print(FILE_OUT qq(
$str_data
\\legend{$str_legend_rows}
\\end{axis}
\\end{tikzpicture}
\\end{document}
));
    close(FILE_OUT);
    #!
    #! 
    {
	my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	printf("\t(scatter-compile)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $file_fig = $result_file;
	$file_fig =~ s/\.tex/\.pdf/;
	if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	push(@{$self->{arr_exportedLatex_scatter}}, {path => $file_fig, title => $title, 
						     class => $xlabel,
						     ylabel => $yLabel,
						     dataContext => $self->{dataContext},
						     #metricGroup => $str_case
	     });
	{ #! Add exproted file to meta-file, ie, to Simplify understanding of data-files
	    my $cnt_rows = scalar(split("&", $str_data));
	    update_globalMetaFile($file_fig, $cnt_rows);
	}
    }
}

#! Construct a Scatterplot, where variable=1 is along x-axis, while variable=" is a long the y-axis; the rows are (depicted through) the symbols used in scatter-plot.
sub _infoVis_constructTable_scatterPlot_2d_pair {
    my ($self, $result_file, $title, $colIndex_1, $colIndex_2, $colIndex_1_metric, $colIndex_2_metric) = @_;
    if(!defined($colIndex_1_metric)) {croak("!!\t Why is colIndex_1_metric NOT defined?");}
    if(!defined($colIndex_2_metric)) {croak("!!\t Why is colIndex_2_metric NOT defined?");}
    #!
    my $xlabel   =   $self->{arr_rows}->[0]->{arr_cols}->[$colIndex_1]->{name} . ":$colIndex_1_metric";
    my $ylabel   =   $self->{arr_rows}->[0]->{arr_cols}->[$colIndex_2]->{name} . ":$colIndex_2_metric";
    my $str_case = sprintf("case=\"%s\" \t x \t \"%s\"\n",   $ylabel,    $xlabel);    
    #!
    #! Intiae, if noit alreayd intated, the summary of extreme min--max preorpteis for each proerpty.
    if(!defined($self->{hash_xy_score}->{$str_case})) {
	$self->{hash_xy_score}->{$str_case} = {min_score => $extreme_max, max_score => $extreme_min, 
					       #! FIXME: ivneistgate, based on the result-data, if below maks sense ... used in/from our "updateParentObject_withScore(..)"
					       x => $self->{arr_rows}->[0]->{arr_cols}->[$colIndex_1]->{name},
					       y => $self->{arr_rows}->[0]->{arr_cols}->[$colIndex_1]->{name},
					       z => $colIndex_1_metric,					       					       
};
    }
    #!
    my @arr_str_header_internal   = ();
    my @arr_str_header_seenByUser = ();   
    my $str_header_isSet = 0;
    #!
    #!
    my $sep_col = "  ";
    my $sep_row = ' ';        
    #my $col_placement = "lc";
    # 
    #my $str_header = "";
    my $str_rows = ""; # ie,: "x" . $sep_col . "y" . $sep_col . "label" . $sep_row . "\n";


    #!
    #! Remember the parsed scores --- eg, to be used when cosntructing bar-chart in "sub _exportHistoCountBucket_BarChart(...)"
    my @matrix_resultScores = ();
    my @arr_rowNames        = ();
    my $points_are_symmetric = 1;
    #! 
    #! Iterate throguh the rows:
    my $index = 0;
    my $index_rowColor = 0;
    #printf("(info)\t iterates aross |rows|=%u\n", scalar(@{$self->{arr_rows}}));    
    foreach my $row (@{$self->{arr_rows}}) {
	if(!defined($row)) {next;}
	#!
	#! Fetcch the points:
	my @arr_points = ();
	my $localProp_count = {count_between_s1 => 0, count_between_s2 => 0};
	{ 
	    if( !defined($row->{arr_cols}->[$colIndex_1]) || !defined($row->{arr_cols}->[$colIndex_2]) ) {
		next; #! ie, as the pair is NOT known.
	    }
	    if( !defined($row->{arr_cols}->[$colIndex_1]->{arr_scores}) || !defined($row->{arr_cols}->[$colIndex_2]->{arr_scores}) ) {
		next; #! ie, as the pair is NOT known.
	    }	    
	    my $cnt_points_1   = scalar(@{$row->{arr_cols}->[$colIndex_1]->{arr_scores}});
	    my $cnt_points_2 = scalar(@{$row->{arr_cols}->[$colIndex_2]->{arr_scores}});
	    if( ($colIndex_1 >= scalar(@{$row->{arr_cols}})) || ($colIndex_2 >= scalar(@{$row->{arr_cols}})) ) {
		next; #! ie, as no items are then inserrted for this case.
	    }

	    #! Identify files which are shared by both x-axis and y-axis:
	    #! Note: movition of below "hashOf_fileIds" ... answers the following quesiton:  .... question:: is below srategy correct (for unifying the two soccres)? ... ie, to assume taht both proeprtes are simolutinsoly set? ... how may we address isncucries (Adosted to this strategy)?
	    my %hashOf_fileIds;
	    for(my $i = 0; $i < $cnt_points_1; $i++) {
		my $id = $row->{arr_cols}->[$colIndex_1]->{arr_scores}->[$i]->{global_row_id};
		if(!defined($hashOf_fileIds{$id})) {
		    $hashOf_fileIds{$id} = 1; #! ie, from x-axis.
		}
	    }
	    for(my $i = 0; $i < $cnt_points_2; $i++) {
		my $id = $row->{arr_cols}->[$colIndex_2]->{arr_scores}->[$i]->{global_row_id};
		if(defined($hashOf_fileIds{$id})) {
		    # FIXME: use below logics in cosntruction of "tut-triangle-axis.tex", where difference regards to use '3' instead of '2' <-- % FIXME: what are the use-cass here 'vvislaisitng three diemsions' is of interest/usefulness?
		    $hashOf_fileIds{$id} += 1; #! ie, where '2' indciates that the structure is used in both files.
		}
	    }	    
	    my $cnt_points = $cnt_points_1;
	    if($cnt_points > $cnt_points_2) {
		$cnt_points = $cnt_points_2; #! ie, use the minim number of points
	    }
	    #!
	    #printf("\t\t(infVis:Row: $id_row , cnt_points:$cnt_points \n");
	    for(my $i = 0; $i < $cnt_points; $i++) {
		my $id = $row->{arr_cols}->[$colIndex_1]->{arr_scores}->[$i]->{global_row_id};
		if($hashOf_fileIds{$id} == 2) { #! then the measurement is shared by both, ie, a cosnsitent meausremnet:
		    my $o1 = $row->{arr_cols}->[$colIndex_1]->{arr_scores}->[$i];
		    my $o2 = $row->{arr_cols}->[$colIndex_2]->{arr_scores}->[$i];
		    #
		    my $s1 = $o1->{score};
		    my $s2 = $o2->{score};		
		    if( ($o1->{metric} eq $colIndex_1_metric) && ($o2->{metric} eq $colIndex_2_metric) ) { #! then we assume that the metric-combination is acurate/useful.
			#! The merge the points:
			push(@arr_points, {s1 => $s1, s2 => $s2 } );
			{ #! Update the count-proerpty:
			    $localProp_count->{count_between_s1} +=  __point_is_between_extreme_ranges($s1, $row->{arr_cols}->[$colIndex_1]->{min_score}, $row->{arr_cols}->[$colIndex_1]->{max_score});
			    $localProp_count->{count_between_s2} +=  __point_is_between_extreme_ranges($s2, $row->{arr_cols}->[$colIndex_2]->{min_score}, $row->{arr_cols}->[$colIndex_2]->{max_score});
			}
			{ #! Update min--max prop:
			    #! Min:
			    if($self->{hash_xy_score}->{$str_case}->{min_score} > $s1) {
				$self->{hash_xy_score}->{$str_case}->{min_score} = $s1;
			    }
			    if($self->{hash_xy_score}->{$str_case}->{min_score} > $s2) {
				$self->{hash_xy_score}->{$str_case}->{min_score} = $s2;
			    }
			    #! Max:
			    if($self->{hash_xy_score}->{$str_case}->{max_score} < $s1) {
				$self->{hash_xy_score}->{$str_case}->{max_score} = $s1;
			    }
			    if($self->{hash_xy_score}->{$str_case}->{max_score} < $s2) {
				$self->{hash_xy_score}->{$str_case}->{max_score} = $s2;
			    }			    
			}
		    }
		}
	    }
	}			    
	#!
	#!
	my $isTo_ignore = 0;
	{#! Remember the set of unique poitns: used to skip printing cases where there is not any well-defined seperation ... . <-- how??: filter a) if min==max, then drop printing row; filter b) by counting the numbe of points which are found between two extrmeeies
	    if($row->{arr_cols}->[$colIndex_1]->{min_score} == $row->{arr_cols}->[$colIndex_1]->{max_score}) {
		if($row->{arr_cols}->[$colIndex_2]->{min_score} == $row->{arr_cols}->[$colIndex_2]->{max_score}) {
		    # printf("skip-row\t %s\n", $row->{name});
		    $isTo_ignore = 1; #! ie, as the rows are euqal.	     
		}
	    }
	    if($isTo_ignore == 0) { # then explore the number of poins found between the extremeties:
		#my $threshold_cnt_min = 10;
		my $threshold_cnt_min = 5;
		#my $threshold_cnt_min = 2;
		if( 
		    # TODO: cosnider suing a different min-point-hresold than '$threshold_cnt_min' ... the threshold is sued to get some visible/ifnormative plots
		    ($localProp_count->{count_between_s1} < $threshold_cnt_min) &&
		    ($localProp_count->{count_between_s2} < $threshold_cnt_min)
		    ) {
		    #! Note: mtpoviaiton for this 'skip-case' is to avoid generaitng scatter-plot for cases which as efifcelty may be viwed/interpreted from data
		    $isTo_ignore = 1;
		    #printf("skip-row\t %s\n", $row->{name});
		}
	    }
	} #! end-of-$isTo_ignore-test.
	#!
	#! 
	my $id_row = undef;
	if($isTo_ignore == 0) { #! then construct he row-header:
	    #printf("\t\t(infVis:Row: $id_row \n");
	    #! Idea: isnert for only a specifc prop ... iterat ethrough lsit of scores ... 
	    $id_row = "case" . $index ;
	    if($index >= scalar(@arr_props_color)) {
		croak("!!\t Not enough colors; figure out how handling thsi issue, eg, by adding enwolor-legends.");
	    }
	    push(@arr_str_header_internal, $id_row . "={" . $arr_props_color[$index] . "}");
	    push(@arr_str_header_seenByUser, $row->{name});
	    $str_header_isSet = 1;
	}
	if($isTo_ignore == 0) { #! then add the row-value:
	    my @row_resultScores = ();
	    foreach my $p_obj (@arr_points) {
		my $s1 = $p_obj->{s1};
		my $s2 = $p_obj->{s2};
		$str_rows .=  $s1 . $sep_col . $s2 . $sep_col . $id_row . $sep_row . "\n";
		if($s1 != $s2) {
		    printf("\t\t(not-symmetric)\t %.1f VS %.1f\t given \t %s\n" , $s1, $s2, $str_case); # FIXME: figure out wfor which cases this sitaution arise.
		    #! FIXME: valdiae below assumption of 'then isnerting both sccores' for this case.
		    push(@row_resultScores, $s1); #! ie, as we asume they are identifal % TODO: update this based on aboe "$s1 != $s2" test.
		    push(@row_resultScores, $s2); #! ie, as we asume they are identifal % TODO: update this based on aboe "$s1 != $s2" test.
		    #!
		    $points_are_symmetric = 0; #! ie, as this proerpty does NOT hold
		} else {
		    #!
		    push(@row_resultScores, $s1); #! ie, as we asume they are identifal % TODO: update this based on aboe "$s1 != $s2" test.
		}
	    }
	    #! 
	    #! Add data to the generic structure:
	    push(@arr_rowNames, $row->{name});
	    push(@matrix_resultScores, \@row_resultScores);
	    #!
	    #! Increment:
	    #print("\t Str(rows)=" . $str_rows);
	    $index += 1;
	    #print("\t Str(rows)=" . $str_rows);	
	}
    }
    { #! Apply filter-threshold(s):
	my $threshold_cnt_minCasesInFig = 2; # FIXME: idnetify the minimum number of legends to bbe in a fiureu  ... ie, to reducce the InfoVis-clustter ... as it is otherwise all to many figures.
	if(scalar(@arr_rowNames) < $threshold_cnt_minCasesInFig) {
	    return;
	}
    }
    
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
    if(scalar(@arr_str_header_internal) == 0) { 
	#! then we do NOT print the data, ie, to avoid itnroduccing noise in a user's configuraiton-files.
	    # printf("(skip-case)\t Did not find any interesting cominations for $str_case");
        return 0;
     } else {
	 printf("(export-scatterplot)\t Case\t $str_case \t file:\t $result_file\n");
     }
    if($self->{config_infoVisType_useBarChartNotScatterplot} == 1) {
	my $result_file_barChart = $result_file;
	$result_file_barChart =~ s/\.tex/-barChart\.tex/; #! ie, to have a unqieuly-named-file.
	_exportHistoCountBucket_BarChart($self, $result_file_barChart, \@matrix_resultScores, \@arr_rowNames, $title, $str_case, $ylabel);
	#!
	if($points_are_symmetric == 1) { #! ie, as here is then not a need for generating a 2d-scatter-plot.
	    return; #! ie, as we are then NOT interested in also constructing ScatteerPlots
	}
    }
    #!
    #!
    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");    
    #!
    # print("\t Str(rows)=" . $str_rows);
    my $str_header_internal = join(",\n", @arr_str_header_internal);
    my $str_legend          = join(",",  @arr_str_header_seenByUser);
    #!    
    print(FILE_OUT qq(
%! Title: $title 
\\documentclass[border=9]{standalone}
\\usepackage[table,x11names,usenames, dvipsnames]{xcolor}
\\usepackage{pgfplots}
\\begin{document}
\\begin{tikzpicture}
  \\begin{axis}[legend pos=south east,
      title={ $title },
      ylabel={ $ylabel },
      xlabel={ $xlabel }, 
    ]
    \\addplot[
      scatter,only marks,scatter src=explicit symbolic,
      scatter/classes={
         $str_header_internal
      }
    ]
    table[x=x,y=y,meta=label]{
      x    y    label
      $str_rows};
    \\legend{$str_legend}
  \\end{axis}
\\end{tikzpicture}
\\end{document}
	  ));
    close(FILE_OUT);
    {
        $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
	my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	printf("\t(scatter-compile)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $file_fig = $result_file;
	$file_fig =~ s/\.tex/\.pdf/;
	if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	push(@{$self->{arr_exportedLatex_scatter}}, {path => $file_fig, title => $title, 
						     class => $xlabel,
						     ylabel => $ylabel,
						     dataContext => $self->{dataContext},
						     #metricGroup => $str_case
	     });
	{ #! Add exproted file to meta-file, ie, to Simplify understanding of data-files
	    my $cnt_rows = scalar(@arr_str_header_seenByUser);
	    #my $cnt_rows = scalar(split("&", $str_data));
	    update_globalMetaFile($file_fig, $cnt_rows);
	}
    }
    
}
    
sub infoVis_constructTable_scatterPlot_2d {
    my ($self, $result_file_prefix, $title) = @_;
    #!
    #! Iterate through col-names:
    #! Note: to improve speciifty of the figures, the "arr_uniqueGroupMetricNames" is used to increase specifity of data, eg, for: "ncluster=2" 
    foreach my $o_str_group_1 (@{$self->{arr_uniqueGroupMetricNames}}) 
    # FIXME: cosnider actiing this loop ... de-acitves as there were no avialbel points ... ie, a nessity to get graphs.
    {
	my $str_group_1 = $o_str_group_1->{name};
	my ($col_name_1, $metric_1) = split("=", $str_group_1);
	# FIXME: is it correctly to only test the col-name-proeprty for column(0)??
	my $row_0 = $self->{arr_rows}->[0];
	my $obj_x_index = 0;	
	foreach my $obj_x (@{$row_0->{arr_cols}}) {
	    # printf("\t(scatter:explores::x)\t $str_group_1 : ($col_name_1 : $metric_1) \t VS %s\n", $obj_x->{name});
	    #if($obj_x->{name} eq $col_name_1) 
	    {
		foreach my $o_str_group_2 (@{$self->{arr_uniqueGroupMetricNames}}) {
		    my $str_group_2 = $o_str_group_2->{name};
		    my ($col_name_2, $metric_2) = split("=", $str_group_2);
		    #
		    my $obj_y_index = 0;
		    foreach my $obj_y (@{$row_0->{arr_cols}}) {	    
			#printf("\t(scatter:explores::x)\t $str_group_2 : ($col_name_2 : $metric_2) \t VS %s\n", $obj_y->{name});
			if($obj_y->{name} eq $col_name_2) {
			    #printf("\t(scatter:explores)\t $str_group_1 \t $str_group_2 \n");
			    if($str_group_1 ne $str_group_2) { #! ie, to NOT print out for metir-ocbmiantiosn which are equal ... as this otehrwise could be plotted as a 1d-plot <-- TODO: consider cosntruct a 1d-plot for thits case <-- fpor what sue-cases would this be suefl/interesting?
				#if($obj_x->{name} ne $obj_y->{name}) {
				#! Then compute: 
				my $result_file = $result_file_prefix . $str_group_1 . "-" . $str_group_2 . ".tex";
				#printf("\tInfoVis\t Start: scatter-plot\t $result_file\n");
				$result_file =~ s/[_: \=\(\)& ]/-/g; #! ie, remove difficult chars.
				#! Note: the col-index, instead of the row-objects, arepovdied. This to simplify fetching irrespeive of the rows:
				# printf("\t(scatter-between)\t %s\t%s\n", $str_group_1, $str_group_2);
				_infoVis_constructTable_scatterPlot_2d_pair($self, $result_file, $title, $obj_x_index, $obj_y_index, 
									    $metric_1, $metric_2
									    # $obj_x->{metric}, $obj_y->{metric}
				    );
				#_infoVis_constructTable_scatterPlot_2d_pair($self, $result_file, $obj_x->{name}, $obj_y->{name});
			    }
			}
			$obj_y_index += 1;
		    }
		}
	    }
	    $obj_x_index += 1;
	}
    }
}


# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
sub generate_report {
    my ($self, $result_file) = @_;
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
    my $latexString_includeIntoReport = $self->{latexString_includeIntoReport};
    #if{ #! then add summary-proerpties
    { #! Print: extreme properties for the constructed tables.
	my $tab = $self->{table_tableFiles_extremeProps};
	if(scalar(@{$tab}) > 0) { #! then we generate for this table
	    #!
	    #my @arr_str_header = ("File-name", "Global Min", "Global Max", "Global Diff.", "Row: min", "Row: max", "Row: diff");
	    #my $col_placement = "l cc cc cc";
	    my @arr_str_header = ("File-name", "Global Min", "Global Max", "Global Diff.", "Row: min-diff", "Row: max-diff");
	    my $col_placement = "l ccc cc";	    
	    #!
	    my $sep_col = " & ";
	    my $sep_row = ' \\\\ ';    
	    my $sep_row_block = "\\rowcolor{lightgrey}";
	    my $index_rowColor = 0;
	    #!
	    my $str_rows = "";
	    my $cnt_cols_added = 0;
	    foreach my $row (@{$tab}) {
		my @result_prop = ();
		my $col_ind = 0;
		foreach my $score (@{$row}) {
		    my $score_is_ok = 0;
		    if($col_ind > 0) {
			if( ($score ne "" ) && ($score ne "inf" ) ) {
			    if( ($score != 0) && ($score != 0) ) {
				$score_is_ok = 1;
				$cnt_cols_added += 1;
			    }
			}
		    } else {$col_ind = 1;} #! ie, as we then asusme that the first column descibes he row-name, ie, no need for filtering of valus.
		    if($score_is_ok == 1) {
			push(@result_prop, sprintf("%.1f", $score));
		    } else {
			push(@result_prop, "");
		    }
		    $col_ind += 1;
		}
		$str_rows .= join($sep_col, @result_prop) . $sep_row . "\n";		
		$index_rowColor += 1;
		if($index_rowColor == 2) {
		    $str_rows .= $sep_row_block . "\n"; #! ie, to rease readability.
		    $index_rowColor = 0;
		}		
	    }
	    #!
	    #!
	    if($cnt_cols_added > 0) { #! which is used to avoid printing empty tables.
		#my $title_prefix = "";
		my $str_header = join($sep_col, @arr_str_header) . $sep_row;
		my $title = "";
		my $title_prefix = 
		my $figLabel = "figure:";
		my $str_table_innerBody = qq( \\begin{tabular}{$col_placement}
  $str_header
      \\hline
 % ----------------------------------------      
$str_rows
 \\end{tabular});
		my $dataContext = $self->{dataContext};
		my $str_table = sprintf( qq(\\begin{table*}[t!]
\\caption{ 
$dataContext 
$title
} \\label{$figLabel}
{ %\\footnotesize
      $str_table_innerBody
}
\\end{table*}
				     ));	    
        my $local_result_file = $result_file;
        $local_result_file =~ s/[\$\(\)& ]//g;
	$local_result_file =~ s/\.tex/file-mapping\.tex/;
my $yLabel = "summary-props";
	    export_tableToFile_self($self, $str_table_innerBody, $local_result_file, $title_prefix . $title, $yLabel);	    
	}
    }

    { #! Test if we have data:
	if ((scalar(@{$self->{arr_exportedLatex_scatter}} == 0))
	    && (scalar keys(%{$self->{hash_xy_score}}) == 0)
	    && ($latexString_includeIntoReport eq "") )
	{
	    return; #! ie, as we then asusme that we do NOT have any data to generate
	}
    }    
    { #! then we export the results:

    if(scalar(@{$self->{arr_exportedLatex_scatter}} > 0)) { #! then we export the results:
	# my $result_file = "tmp-summary-report.tex";
	open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");    
	{ #! Set header:
	    print(FILE_OUT qq(
\\documentclass[conference]{IEEEtran}
\\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
% ---------------
\\usepackage{cite}
\\usepackage{amsmath,amssymb,amsfonts}
% ---------
\\usepackage{graphicx}
\\usepackage{textcomp}
\\usepackage[table,x11names]{xcolor}

\\definecolor{lightgrey}{rgb}{0.9,0.9,0.9}
\\usepackage{wrapfig}
\\usepackage[breaklinks=true]{hyperref}

\\def\\kd{\\textit{K-D tree}\\ }
\\def\\kds{\\textit{K-D trees}\\ }
\\def\\kdE{\\textit{K-D tree}} %! "E" for 'end'
\\def\\kdsE{\\textit{K-D trees}} %! "E" for 'end'
\\newcommand{\\citep}[1]{(\\protect\\cite{#1})}
\\newcommand{\\citet}[1]{\\protect\\cite{#1}} %! \\cite{\\url{#1}}} %! ie, do

\\graphicspath{{figures/}}

\\usepackage{longtable}
% \\usepackage[table]{xcolor}
\\usepackage{ctable} % for \\specialrule command
\\usepackage{multirow}
\\usepackage{multicol}   
\\maxdeadcycles=200 %! solves eror: "! Output loop---100 consecutive dead cycles" [https://tex.stackexchange.com/questions/409796/todonotes-output-loop-100-consecutive-dead-cycles-after-50-todo-on-a-page]

% --------
\\begin{document}));
	    my $latexString_includeIntoReport = $self->{latexString_includeIntoReport};
	    if($latexString_includeIntoReport ne "") { #! then add summary-proerpties
	    print(FILE_OUT qq(
\\section{Introduction}
$latexString_includeIntoReport
\\section{Details}
));
	    }		
	}
	#!
	#! Add the figures:
	my @arr_figsByGroup = ();
	my $prev_group = {title => undef, include => undef, class => "", cnt => 0, arr_yLabel => [], dataContext => $self->{dataContext}, };
	foreach my $o_file (@{$self->{arr_exportedLatex_scatter}}) {
	    my $metric_group = $o_file->{class}; #! which is used to order/aling subfigures into the same cpation ... theby (hopefully) iomrpvoing readaility
            if( ($metric_group ne $prev_group->{class} ) || ($o_file->{dataContext} ne $prev_group->{dataContext}) ){ #! then we add a new group
		if(defined($prev_group->{title})) {
		    push(@arr_figsByGroup, $prev_group); #! then add the prev-gorup to the list.
		    # $prev_group = {title => undef, include => undef, class => $metric_group};
		} #! else : assuems pre-goup si empty, ie, do nothing xmt updating class-name
		#!
		#! Then intaite:
		my $title     = $o_file->{title};
		$prev_group = {title => $title, include => "", class => $metric_group, cnt => 0, arr_yLabel => [], dataContext => $self->{dataContext},};
	    } #! else: then we cocnaten on the current/prev group, ie, do nothing.
	    #!
	    #! Append the string:
	    my $file_name = $o_file->{path};
	    print("generate-report\tAdd-fig\t" . $file_name . "\n");
	    $prev_group->{include} .= "\\includegraphics[trim = 0mm 0mm 0mm 0mm, clip,keepaspectratio=true,width=widthVariable]{$file_name}\n";
	    $prev_group->{cnt} += 1.0;
	    push(@{$prev_group->{arr_yLabel}}, $o_file->{ylabel});
            
	}
	if(defined($prev_group->{title})) {
	    push(@arr_figsByGroup, $prev_group);
	}
	foreach my $obj (@arr_figsByGroup) {
	    my $str_include = $obj->{include};
	    my $title       = $obj->{title};
	    #!
	    #! Adjsut the relativve figure-with:
	    my $width = 0.9 * (1.0/$obj->{cnt});
	    $width .= '\\linewidth';
	    $str_include =~ s/widthVariable/$width/g;  #! ie, make use of the above "widthVariable" as an insertion-point
	    #!
	    my $title_local = "Maps " .  $prev_group->{class} . " to the perspectives of " . join(", ", @{$obj->{arr_yLabel}});
	    $title_local .= "(" . $obj->{dataContext} . ")";
	    #! Write out:
	    print(FILE_OUT qq(
\\begin{figure*}[htbp]
  \\centering
  $str_include
  \\caption{ $title_local
... % FIXME: add something
   } \\label{figure:}
\\end{figure*}
\\clearpage %! ie, to avoid the fllowing error: ! LaTeX Error: Too many unprocessed floats.
		  ));
       }
}
}
# -------------------
{ #! Construct a table of extrmee min-max valeus
    #!
    my $sep_col = " & ";
    my $sep_row = ' \\\\ ';    
    my $sep_row_block = "\\rowcolor{lightgrey}";
    #!
    #! Iterate throguh the rows:
    my $str_rows = "";
    my $index = 0;
    my $index_rowColor = 0;
    while (my ($key, $o) = each (%{$self->{hash_xy_score}})) {
	my ($score_min, $score_max) = ($o->{min_score}, $o->{max_score});
	#! Do not rpitn thsi case if the score is set to empty.
	if($score_max == $extreme_min) {next;}
	if($score_min == $extreme_max) {next;}
	#!
	if($score_max == $score_min) {next;} #! ie, to abbeivate the table.
	#!
	#!
	my $diff = "inf";
	if( ($score_min != 0) && ($score_max != 0) ) {
	    $score_min = abs($score_min);
	    $score_max = abs($score_max);
	    #!
	    $diff = $score_max / $score_min;
	    #!
	    #! Update the parent-object with the scores:	    
	    updateParentObject_withScore($self, $o->{x}, $diff, $o->{y}, $o->{z});
	}
	#my @result_prop = (	    #push(@result_prop, );
	$str_rows .= sprintf("%s $sep_col %.1f $sep_col %.1f $sep_col %.1f $sep_row \n", $key , $diff, $score_min, $score_max);
	#!
	$index_rowColor += 1;
	if($index_rowColor == 2) {
	    $str_rows .= $sep_row_block . "\n"; #! ie, to rease readability.
	    $index_rowColor = 0;
	}	    
    }
    my $str_header = "Fixed Property $sep_col Diff. $sep_col Min. $sep_col Max $sep_row \n";
    #$str_header =~ s/[\$\(\)& ]//g;
    my $col_placement = "lccc";
    my $dataContext = $self->{dataContext};
    my $str_table = sprintf( qq(\\begin{table*}[t!]
\\caption{ $dataContext ---  Summary of how extreme outcomes in predictions
} \\label{figure:}
{ %\\footnotesize
 \\begin{tabular}{$col_placement}
  $str_header
      \\hline
 % ----------------------------------------      
$str_rows
 \\end{tabular}
}
\\end{table*}
	  ));
    print(FILE_OUT $str_table);    
#! 
if(defined($self->{self_globalSpec})) {
$self->{self_globalSpec}->{latexString_includeIntoReport} .= $str_table; #! ie, then incluyde this summary-table into the detailed report.
}
}
# -------------------
   #!
    #! Print the tail:
    print(FILE_OUT qq(\\end{document}));
    close(FILE_OUT);
#if(1 == 2) #! then we skip the pritng ... ie, to avoid issues in the latex-fiel fom corrupignt the buildd-process
    { #! Then compiel the report:
        my $local_result_file = $result_file;
        $local_result_file =~ s/[\$\(\)& ]//g;
	#$local_result_file =~ s/\.tex/file-mapping\.tex/;
        my $cmd = "pdflatex -halt-on-error $local_result_file 1>out_latex.txt";
	printf("\t(report-compile)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $local_result_file)) {	    printf(STDERR "!!\t For file=\t$local_result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $file_fig = $local_result_file;
	$file_fig =~ s/\.tex/\.pdf/;
	if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
    }    
}
}

my $self_globalSpec = init_self(undef, undef, undef);

# ************************************************************
sub main {
    my ($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix) = @_;
 #! File:
    my $cutoff_min = -10000.0;  my $cutoff_max = 40000000.0; my $max_lineCount = 100000;   my $max_colCount = 100000; 
    my $regex_filter_row = undef;
    #!
    #!
    #! Load the results into $self
    readDirectory_mapFile_toScore($self, $regex_files, $cutoff_min, $cutoff_max, $max_lineCount, $max_colCount, $regex_filter_row);
    #!
    { #! Generate a table:
	my $isTo_plotFor_relative = 0;
	my $yLabel = $title_table;
	infoVis_constructTable_textual($self, $table_result_file, $title_table, $isTo_plotFor_relative, $yLabel);
    }
    { #! Generate a table:
	my $isTo_plotFor_relative = 1;
	my $yLabel = $title_table . " (relative)";
	infoVis_constructTable_textual($self, $table_result_file_summary, $title_table, $isTo_plotFor_relative, $yLabel);
    }    
    { #! Generate scatter-plots:
	printf("InfoVis\t Start: scatter-plot\n");
	#! Note: we plot for all valeus ... remember all scores ... skip value=0.0
	#! Idea: ... density of points ... 
	#! Evlauation Space: align the different axis of interpreatiaility towardsd each each other; z=label=row-name. In the scatter-pltos the observaiotns from  Table \ref{} is summairsed, where difference is that all observiaotns (rather thant he extrmee scores) are used.
	#! Note: Figures are gneraed for each of the different/possible column--column pariwse combinations.
	infoVis_constructTable_scatterPlot_2d($self, $scatterplot_result_file_prefix, $title_scatterPlot);
    }
}

# ************************************************************
# ************************************************************
# ------------------------------
#if(1 == 2)
{
    #! Idea: ... idetnify the main axis of variability, and the influence of variability. ... evaluate the sigifnaicne of each contribution ... explroe the error boundary of similarty-metrics ... which provides an estiamtion for the sgicaince of benchmark-findings ... % FIXME: construct a scatter-plot relating the STD of sim-metrics to different CCMs ... are there any relationship? ... for what cases is it acepteblae that the simalrity-emtircs have a low STD (ie, without prodcuign poor clsuter-rpedicitosn)? % FIXME: why is this emausrmeent useful/informative? <-- alorithms are constructed toewards training-data. Training-data are oftne classifed based on dsitrubitons, as seen in \cite{rodriguez2019clustering}. The resutls preseted in TAble \re{} exemplife how the information of data-dsitrubitons is not suffient to preict the eprfmrancce of clsutering-algortihms. This indicates that there is a hidden bias in the applicaiton, and perofmrance, of data-analatyics.
    # % FIXME: seems like the difference in STD-score is small .... only at 2x ... waht does this idnicate? is it the ssentivyt of the STD-metirc? ... how does the STD-scores correlate to other attributes (eg, mean-score of ...??...) ... 
    my $title_table       = "Why knowledge of data-distributions are not suffient to predict the performance of clustering algorithms.";
    my $regex_group_row  = '^(.+)\-(\d+)$'; #! where only the first group is used.
    my $name_subGroup = "splitPoint"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    my $regex_files       = 'result-2-randomDistributions.+corrDeviation.tsv'; #! ie, avoid evlauating for other file-content-types.
    my $title_scatterPlot = "The relationship between factors influencing factors in cluster analysis.";
    my $table_result_file = "table-corrDeviation-distributions.tex";
    my $table_result_file_summary = "table-corrDeviation-distributions-relative.tex";
    my $scatterplot_result_file_prefix = "fig-scatter-corrDeviation-distributions-";
    #!
    #! Initate the object holding (a summary of) the parsed data:
    my $self = init_self($regex_group_row, $self_globalSpec, $name_subGroup);
    #!
    #! Apply:
    main($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix);
}
# --------------------------
#if(1 == 2)
{
    # FIXME: evaluate for file-type:
    # FIXME: seems like the STD-deviaiton in scores is not any larger than those obsered in "corrDeviation": why??
    my $title_table       = "Factors impacting the spread in feature-scores influences the topological constraints in clustering (eg, the cutoff threshodls used in DBSCAN).";
    # FIXME: evaluate for file-type: 	"f-result-tut-bias-2/result-2-randomDistributions-n3000-featureDeviation.tsv",
    my $regex_group_row  = '^(.+)\-(\d+)$'; #! where only the first group is used.
    my $name_subGroup = "splitPoint"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    my $regex_files       = 'result-2-randomDistributions.+featureDeviation.tsv'; #! ie, avoid evlauating for other file-content-types.
    #my $regex_files       = 'result-2-randomDistributions.+corrDeviation.tsv'; #! ie, avoid evlauating for other file-content-types.
    my $title_scatterPlot = "The relationship between factors influencing factors when assessing the representaivenss of training-data.";
    my $table_result_file = "table-featureDeviation-distributions.tex";
    my $table_result_file_summary = "table-featureDeviation-distributions-relative.tex";
    my $scatterplot_result_file_prefix = "fig-scatter-featureDeviation-distributions-";
    #!
    #! Initate the object holding (a summary of) the parsed data:
    #my $self_globalSpec = init_self(undef, undef);
    my $self = init_self($regex_group_row, $self_globalSpec, $name_subGroup);
    #!
    #! Apply:
    main($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix);
    #!
    #! Generate a report-summary:
    my $name_reportFile = $scatterplot_result_file_prefix . "-report.tex";
    generate_report($self, $name_reportFile);
}
# --------------------------
#if(1 == 2)
{
# FIXME: evaluate for file-type: ... STD between cluster-IDs ... the spread of clsuter-IDs ... an idnication of density in ... 
    # FIXME: evaluate for file-type:
    # FIXME: given the STD-resutls (which do not have any well-defined differnece ... at least we are not able to pin-pint case where distrubion is large ...) ... culd these obserivoinats be used to argue for the following assertion: "it is not the clustering-algorithm, nor the distrubions, nor the high-diemisionality, etc., which causses problems in data-anlaytis. Instead, it is the choise of alggorithmci bulidng-blocks (during the applciaiotn of data-analytics). The implciation is that ...??..." % FIXME: if latter assertion holds, then waht may the users learn/derive?
    #! Example-file. f-result-tut-bias-2/result-2-postCCM-randomDistributions-featureDeviation.tsv
    # FIXME: seems like below aspects of cluster-IDs does NOT result in any clear seperation between the sim-metrics ... ie, that the choise of sim-metrics is of lesser impoitance (than other strategies) ... however, neitehr does any other paramter ... ie, as STD-metric (for analsysign clsuter-rpedictosn) is all too crude ... % FIXME: update the paper wr.t this obserivoant ... an observiaont which is of improtance in unrelling/udnerstainding ....??...
    my $title_table       = "Explores the agreement, and disaggrement, in grouping of distriubiotns, when seen thoguh the lense of ... cluster-algorithms";
    my $regex_group_row  = '^([a-zA-Z]+)\-(.+)$'; #! where only the first group is used.
    my $name_subGroup = "algorithm"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    my $regex_files       = 'result-2-postCCM.+featureDeviation.tsv'; #! ie, avoid evlauating for other file-content-types.
    my $title_scatterPlot = "The spread in predicted cluster-memberships";
    my $table_result_file = "table-clusterIds-featureDeviation-distributions.tex";
    my $table_result_file_summary = "table-clusterIds-featureDeviation-distributions-relative.tex";
    my $scatterplot_result_file_prefix = "fig-clusterIds-scatter-featureDeviation-distributions-";
    #!
    #! Initate the object holding (a summary of) the parsed data:
    #my $self_globalSpec = init_self(undef, undef);
    my $self = init_self($regex_group_row, $self_globalSpec, $name_subGroup);
    #!
    #! Apply:
    main($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix);
    #!
    #! Generate a report-summary:
    my $name_reportFile = $scatterplot_result_file_prefix . "-report.tex";
    generate_report($self, $name_reportFile);
}

# --------------------------
#if(1 == 2)
{ #! then we compare the clustering-algoritihms, rather than the simliarty-metircs
# FIXME: 
    # FIXME: evaluate for file-type:
    # FIXME: given the STD-resutls (which do not have any well-defined differnece ... at least we are not able to pin-pint case where distrubion is large ...) ... culd these obserivoinats be used to argue for the following assertion: "it is not the clustering-algorithm, nor the distrubions, nor the high-diemisionality, etc., which causses problems in data-anlaytis. Instead, it is the choise of alggorithmci bulidng-blocks (during the applciaiotn of data-analytics). The implciation is that ...??..." % FIXME: if latter assertion holds, then waht may the users learn/derive? .... % FIXME: how does this 'no-STD-change' attriubtte reflect to observaitons deduced from the other figures?
    #! Example-file. f-result-tut-bias-2/result-2-postCCM-randomDistributions-featureDeviation.tsv
    # FIXME: seems like below aspects of cluster-IDs does NOT result in any clear seperation between the sim-metrics ... ie, that the choise of sim-metrics is of lesser impoitance (than other strategies) ... however, neitehr does any other paramter ... ie, as STD-metric (for analsysign clsuter-rpedictosn) is all too crude ... % FIXME: update the paper wr.t this obserivoant ... an observiaont which is of improtance in unrelling/udnerstainding ....??...
    my $title_table       = "Explores the agreement, and disaggrement, in grouping of distriubiotns, when seen thoguh the lense of ... cluster-algorithms";
    my $regex_group_row  = '^([a-zA-Z]+)\-(.+)$'; #! where only the first group is used.
    my $name_subGroup = "metric"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    my $regex_files       = 'result-2-postCCM.+featureDeviation.tsv'; #! ie, avoid evlauating for other file-content-types.
    my $title_scatterPlot = "The spread in predicted cluster-memberships";
    my $table_result_file = "table-clusterIds-scopeAlg-featureDeviation-distributions.tex";
    my $table_result_file_summary = "table-clusterIds-scopeAlg-featureDeviation-distributions-relative.tex";
    my $scatterplot_result_file_prefix = "fig-clusterIds-scopeAlg-scatter-featureDeviation-distributions-";
    #!
    #! Initate the object holding (a summary of) the parsed data:
    #my $self_globalSpec = init_self(undef, undef);
    my $self = init_self($regex_group_row, $self_globalSpec, $name_subGroup);
    $self->{inRowNameRegex_useGroupAtIndex} = 2; #! which is used with $regex_group_row to idneitfy the part to be used to organise entites.
    #!
    #! Apply:
    main($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix);
    #!
    #! Generate a report-summary:
    my $name_reportFile = $scatterplot_result_file_prefix . "-report.tex";
    generate_report($self, $name_reportFile);
}



# FIXME: add for cluster-id-CCM-gold-cases.
=head
-------

# FIXME: conceptual:
-- why is it correcct to use CCM-score to depict/capture proerprties of multiple ncluster poerpties/attributes (eg, in combiantion with split-point)? 
.... does it help to 'fixate' the ncluster,splitPoint,.... .... constructing differnet figrues for each? .... to add this attribute into teh score-hash ... ??
.... FIXME: for what cases do we NOT need to take this metric-prop into account?
.... FIXME: in the building of figures ... correct to 'include all subfigures which are part of the same 'metric' group (eg, at the same 'row' in the pdf-result-report)?
.... FIXME: ... in the buldign of the figures ... include the tables ... first generate stand-alone tables ... then use these stand-alone tables in the ...
.... FIXME: 
.... FIXME: 



.... 
--- answer: ... 
--- answer: ... 
-- ... 
--- answer: ... 
--- answer: ... 
-- ... 
--- answer: ... 
--- answer: ... 
-- ... 
--- answer: ... 
--- answer: ... 
-- ... 
--- answer: ... 
--- answer: ... 


-----
# FIXME: storyline??
-- ... 
-- ... 
-- ... 
-- ... 
-- ... 


# FIXME: manage to get a bacch-call working for: "x_hp"
# FIXME: find exiging pl/pm usages of "x_hp" ... 

# -----------------
=cut
    
# --------------------------
if(1 == 2)
{ #! then we evlaute the relatedness between the apramters seprately through each of the CCMs:
=head
  Results:
    %
    % FIXME: scope. .. may these resuls be used to argue fo ... whcih CCMs ae best sutied for fidnign the opinal ncluster-paremer?
    % FIXME: table: summary. cosntrut a table: [CCM-name] x [simMetric-scopeAlg, algMetric-scopeAlg, simMertric-scopeAlg, simMetric-dimension-scopeAlg, simMetric-scopeAlg] = score(max)/score(min) <--- take result forrm aummised tables.
    % FIXME: table: consutrt a table for below ... eg, by attriutes such as: ... difference fom 2x--125x+ ... diference strongly depdent on pariwise silmairyt-metrics.
    %
    ## FIXME: ... update perl-script ... each expoted table .... for each table-file: [global-min, global-max, global-diff, isolted-for-row: diff(min), diff(max) ... % ehnce, sumamrising impact of the paremters, and the min--max influence of difenet configuraiton-paramters. 
    # class: diff-1: unaffeced/insensite to [alg, ncluster,normsation,CCM] --- sensitvive to "case"  ... % FIXME: may the below ... examplify cases where the CCMs becomes higluy sensitve when ...++...

    -- "": 
    -- "": 
    -- "": 
% -----------------------------------------------------------------------
% FIXME: seems like below files falls into the "diff-1" category
% -----
table-CCM-algMetric-scopeAlg-Mirkin-relative-exported.pdf
table-CCM-algMetric-scopeAlg-Rands-indexalternative-implementation-relative-exported.pdf
table-CCM-algMetric-scopeAlg-Rands-index-relative-exported.pdf
table-CCM-algMetric-scopeAlg-StrehlGosh-relative-exported.pdf
table-CCM-algMetric-scopeAlg-Variation-of-information-relative-exported.pdf
table-CCM-algMetric-scopeAlg-Wallace-relative-exported.pdf
% -----------------------------------------------------------------------



table-CCM-simMetric-scopeAlg-AdjustedRandIndexARI-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Chi-squared-relative-exported.pdf
table-CCM-simMetric-scopeAlg-fMeasure-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Fowles-Mallows-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Jaccard-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Mirkin-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Rands-indexalternative-implementation-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Rands-index-relative-exported.pdf
table-CCM-simMetric-scopeAlg-StrehlGosh-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Variation-of-information-relative-exported.pdf
table-CCM-simMetric-scopeAlg-Wallace-relative-exported.pdf
table-clusterIds-featureDeviation-distributions-relative-exported.pdf
table-clusterIds-scopeAlg-featureDeviation-distributions-relative-exported.pdf
table-corrDeviation-distributions-relative-exported.pdf
table-featureDeviation-distributions-relative-exported.pdf



    -- "": 
    -- "": 
    -- "": 
=cut    
    # FIXME: 
    # FIXME: evaluate for file-type:
    # FIXME: ... 
    #! Example-file: 
    # FIXME[result]: ...
    my $title_table       = "Explores the agreement, and disaggrement, in grouping of distriubiotns, when seen thoguh the lense of gold-CCMs";
    my $regex_group_row  = '^([a-zA-Z]+)\-(.+)$'; #! where only the first group is used.
    #! Note: an example-file is: f-result-tut-bias-2/result-2-postCCM-randomDistributions-features_img.tsv
    #! Idea ... for rows=[simMetric-Alg]: evlauate similartyies in cluster-rpedictions ... 
    #! Idea ... for rows=[simMetric-Alg]: ... why is the folloing figure NOT symmetric?? <-- fig-CCM-algMetric-Rands-index--alternative-implementation--scatter-ncluster-2-normalization-.pdf
    #! Idea ... for rows=[simMetric-Alg]: ...
    #! Idea ... for rows=[simMetric-Alg]: ...    
    #! Idea ... for rows=[distribution-centerPoint]: ...
#! Idea ... for rows=[distribution-centerPoint]: ...    
    #! Idea ... 
    #! Idea ...     
    # FIXME: new for-loop .. for file: f-result-tut-bias-2/result-2-postCCM-trans-randomDistributions-n3-features_img.tsv
    my $regex_files       = 'result-2-postCCM.+randomDistributions-features_img.tsv'; #! ie, avoid evlauating for other file-content-types.
    my $title_scatterPlot = "The spread in predicted cluster-memberships";
    # --------------------------------------
    # FIXME: reduce-number-of-figurers ... equire at least one legend to be found for data to be generaed?


#my $self_globalSpec = init_self(undef, undef); #! which hold extreme plto-cases.
for(my $trans = 0; $trans < 2; $trans++) {
    #!
    #!
    # FIXME: also 
    my $name_subGroup = "algorithm"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    if($trans == 1) { #! ie, then evaluate for the case where rowID=<distribution>--<splitPoint>.
	#! Then: compute for the transpsoed matrix
	$regex_files       = 'result-2-postCCM.+trans.+randomDistributions-features_img.tsv'; #! ie, avoid evlauating for other file-content-types.
	$name_subGroup = "splitpoint"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    } #! else: then evaluate for the case where rowID=<alg>--<simMetric>.
    for(my $row_t = 1; $row_t <= 2; $row_t++) {
	# FIXME: test for both groups in $regex_group_row, as speifed throguh the $self->{inRowNameRegex_useGroupAtIndex} attribute.
	# FIXME: explroe for all the gold-CCMs
	# & 
	foreach my $metric_CCM ("Rands-index", "Rands-index (alternative-implementation)", "Adjusted Rand Index (ARI)", "Chi-squared", "Fowles-Mallows", "Mirkin", "Wallace", "Fred & Jain", "Variation-of-information",  "Strehl & Gosh", "fMeasure", "Jaccard") {
	    #!
	    #!
	    #!
	    #! Initate the object holding (a summary of) the parsed data:
	    #! Note: the self-object is moved inside the for-loop. This to ensrue that reportrs are genearted contiusly, ie, givne the long time it may take for analysing the complete set of predictions.
	    my $self = init_self($regex_group_row, $self_globalSpec, $name_subGroup);
	    #!
	    $self->{isTo_computeCCM_gold} = 1;
	    $self->{metric_ccm_gold} = $metric_CCM;
	    my $rowCase = "simMetric";
	    $self->{inRowNameRegex_useGroupAtIndex} = 1; #! which is used with $regex_group_row to idneitfy the part to be used to organise entites.
	    if($row_t == 2) {
		$self->{inRowNameRegex_useGroupAtIndex} = 2; #! which is used with $regex_group_row to idneitfy the part to be used to organise entites.
		$rowCase = "algMetric";
	    }
	    if($trans != 0) {
		$rowCase .= "-dimension";
	    }

	    # FIXME: ... add a new table-export ... in the infoVis-scatter-export ... store the min--max ... then export these results into a table ... and into a bar-chart-plot <-- would such a bar-chart-plot be useful/ifnromative??
	    # FIXME: ... 
	    # FIXME: ... 	    

	    #!
	    #!
	    #my $metric_CCM = "Rands-index";
	    my $table_result_file = "table-CCM-$rowCase-scopeAlg-$metric_CCM.tex";
	    my $table_result_file_summary = "table-CCM-$rowCase-scopeAlg-$metric_CCM-relative.tex";
	    my $scatterplot_result_file_prefix = "fig-CCM-$rowCase-$metric_CCM-scatter-";
	    # --------------------------------------
	    # FIXME: ... first valiate that all scaer-pltos are symmetirc ... ... add permtaution .... the idea is to fixate two paraemer-varialbes, and then investigate its number of freedoms ... hence, to quantiyu the improtance of factos not plotted/attribued ... to plot the values using a histogram instead ... eg, where template="tut-barChar.tex" ... complete: "sub _exportHistoCountBucket_BarChart".
	    # FIXME: make use of this function ... updae "_infoVis_constructTable_scatterPlot_2d_pair"  with a permtaution ...     # FIXME: compelte this ... ensure the input-data (eg, matirx) is as epxecte.d
	    # FIXME: generate a "tut-barChar.tex" permtuation of this ... allocate points to each of the 'buckets' ... eg, ... ... as scaffolded in "sub _exportHistoCountBucket_BarChart"
	    # $self->{config_infoVisType_useBarChartNotScatterplot} = 0; #! which if set to '1' implies that a BarChart is gnerrated (instead of a Scatterplot).
	    $self->{dataContext} = "Context: $rowCase-$metric_CCM";
	    # --------------------------------------	    
	    #!
	    #! Apply:
	    main($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix);
	    #!
	    #! Generate a report-summary:
	    my $name_reportFile = $scatterplot_result_file_prefix . "report-CCMs.tex";
	    generate_report($self, $name_reportFile);    
	}
    }
}

}



# FIXME: when working for CCM-gold ... anlayssi completed ... seperately evlvaued for all/important CCMs-gold ... then add for cluster-id-CCM-matrix-cases.

# --------------------------
if(1 == 2)
{
    # FIXME: evaluate for file-type:
    my $title_table       = "";
    my $regex_files       = 'result-2-randomDistributions.+corrDeviation.tsv'; #! ie, avoid evlauating for other file-content-types.
    my $regex_group_row  = '^(.+)\-(\d+)$'; #! where only the first group is used.
    my $name_subGroup = "??"; #! ie, which desribes how the sub-group (of the row-name) si to be itnerpted/handled.
    my $title_scatterPlot = "The relationship between factors influencing factors in cluster analysis.";
    my $table_result_file = "table-corrDeviation-distributions.tex";
    my $table_result_file_summary = "table-corrDeviation-distributions-relative.tex";
    my $scatterplot_result_file_prefix = "fig-scatter-corrDeviation-distributions-";
    #!
    #! Initate the object holding (a summary of) the parsed data:
    #my $self_globalSpec = init_self(undef, undef); #! which hold extreme plto-cases.
    my $self = init_self($regex_group_row, $self_globalSpec, $name_subGroup);
    #$self->{inRowNameRegex_useGroupAtIndex} = 2; #! which is used with $regex_group_row to idneitfy the part to be used to organise entites.
    #!
    #! Apply:
    main($self, $regex_files, $title_table, $title_scatterPlot, $table_result_file, $table_result_file_summary, $scatterplot_result_file_prefix);
    #!
    #! Generate a report-summary:
    my $name_reportFile = $scatterplot_result_file_prefix . "-report.tex";
    generate_report($self, $name_reportFile);
}
    
=head

=cut

# OK: FIXME: eror in table-CCM-algMetric-scopeAlg-Jaccard-relative.tex
# OK: FIXME: table-genreraiton (eg, "table-CCM-algMetric-scopeAlg-Jaccard-exported.pdf") ... no tables are genrated
# OK: FIXME: new summary-table-bug: \\ betwene row-header and line ... 
# ------------------------
# OK: FIXME: why does the following case arise: "splitPoint:MINE" (eg, in the summary-table)? <-- print out all combinations? <-- throguw an error?
# OK: FIXME: why is the x-axis-bar-chart unreadable? <-- too many deails .... has now roteted the x-labels 45 degrees (which seems to help).


# ******************************************************************************************
# ************************************* End Of Analysis: *****************************************************
# ******************************************************************************************
#!
{ #! Print forextrme plto-cases
    #my $self_globalSpec = init_self(undef, undef); #! which hold extreme plto-cases.
    my $title_table       = "Summary of variance in cluster-predictions";
    { #! Generate a table:
	my $isTo_plotFor_relative = 0;
	my $table_result_file = "table-summary-distributions.tex";
	my $yLabel = $title_table;
	infoVis_constructTable_textual($self_globalSpec, $table_result_file, $title_table, $isTo_plotFor_relative, $yLabel);
    }
    { #! Generate a table:
	my $isTo_plotFor_relative = 1;
	my $yLabel = $title_table . " (relative)";
	my $table_result_file = "table-summary-distributions-relative.tex";
	infoVis_constructTable_textual($self_globalSpec, $table_result_file, $title_table, $isTo_plotFor_relative, $yLabel);
    }    
    { #! Generate scatter-plots:
	printf("InfoVis\t Start: scatter-plot\n");
	#! Note: we plot for all valeus ... remember all scores ... skip value=0.0
	#! Idea: ... density of points ... 
	#! Evlauation Space: align the different axis of interpreatiaility towardsd each each other; z=label=row-name. In the scatter-pltos the observaiotns from  Table \ref{} is summairsed, where difference is that all observiaotns (rather thant he extrmee scores) are used.
	#! Note: Figures are gneraed for each of the different/possible column--column pariwse combinations.
	my $scatterplot_result_file_prefix = "fig-scatter-summary-distributions-";
	my $title_scatterPlot = "summary of extreme variance";
	infoVis_constructTable_scatterPlot_2d($self_globalSpec, $scatterplot_result_file_prefix, $title_scatterPlot);
    }    
    my $name_reportFile = "summary-extreme-cases" . "report-CCMs.tex";
    generate_report($self_globalSpec, $name_reportFile);    
}


#!
#!
close(FILE_IN_GLOBAL_META);


# mv *.out *.log *.aux *.cls *.tex *.pdf
