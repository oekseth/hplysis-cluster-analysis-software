=head
@brief provides logiccs for construiton of line-plots
=cut
package module_linePlot;
use strict;
use Carp;
#! 
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
#use lib '.'; #! which is used for transposing data, and loading strings of data into an array.
#require aux_matrix; #! which is used for transposing data, and loading strings of data into an array.


# -------------------------------------------

my @arr_props_color = (
    #! Note: for more systmeobls, see: https://no.overleaf.com/learn/latex/Pgfplots_package
    'color=purple,mark=triangle*',
    'color=MidnightBlue,mark=square',
    # ---
    'color=YellowOrange,mark=oplus',        
    # ---
    'color=black,mark=x,solid,dashed',
    #
    'color=pink,mark=-,solid',
    #
    'color=MidnightBlue,mark=-,solid,dashed',
    # ---
    'color=ForestGreen,mark=square',
    # ---
    'color=purple,mark=oplus',        
    'color=black,mark=oplus, thick, densely dashed',        
    'color=blue,mark=x,dotted', 
    #
    'color=MidnightBlue,mark=halfcircle',    
    'color=ForestGreen,mark=pentagon',
    #
    'color=YellowOrange,mark=oplus*',
    'color=black,mark=*,solid',
    'color=pink,mark=x,solid',
    'color=MidnightBlue,mark=oplus,solid,dashed',
    'color=ForestGreen,mark=-',        
    #
    'mark=o,draw=black,fill=black',
    'color=ForestGreen,mark=x',
    'color=YellowOrange,mark=triangle',
    'color=purple,mark=diamond,solid',           
    'color=YellowOrange,mark=triangle*',    
    #
    'color=purple,mark=x,solid',           
    'color=ForestGreen,mark=*',
    );

sub init_self {
    return {
	file_result => undef,
	xlabel => "Feature-index \$i\$",
	ylabel => "Feature-score \$f_x(i)\$",
    };
}

sub construct_basicLinePlot {
    my ($self, $title, $cutoff_min, $cutoff_max, $arr) = @_;
    my $val_min = 10000.0;
    my $val_max = -10000.0;
    
    my $str_result = "";
    my $index_line = 0;
    foreach my $line (@{$arr}) {
	chomp($line); #! ie, remove the trail-newline.
	my @arrOf_cols = split("\t", $line);
	my $row_header = shift(@arrOf_cols);
	$row_header =~ s/[_\(\)& ]/ /g; #! ie, to avoid getting problems with Latex
	# $row_header =~ s/_/ /g; #! ie, to avoid Latex from complaining.
	# 
	my $attr_line = "color=ForestGreen,mark=square";
	if($index_line < scalar(@arr_props_color)) {
	    $attr_line = $arr_props_color[$index_line];
	}
	$str_result .= '\addplot[' . $attr_line .'] coordinates {';
	my $index = 0;
	foreach my $col (@arrOf_cols) {
	    if( ($col ne "inf") && ($col ne "4294967296.000000") && ($col ne "nan") && ($col ne "-nan") && ($col ne "-")) {
		#! Avodi too large vlaues from cluttering the results:
		if($col > $cutoff_max) {
		    $col = $cutoff_max;
		} elsif($col < $cutoff_min) {
		    $col = $cutoff_min;
		} 
		$str_result .= sprintf("(%u, %.1f) ", $index, $col);
		#!
		#! Remember the extreme values (sued to adjsut the plot):
		if($col > $val_max) {
		    $val_max = $col;
		} elsif($col < $val_min) {
		    $val_min = $col;
		} 
	    }
	    $index += 1;
	}
	$str_result .= '} node [left] {};         \addlegendentry{' . $row_header . '}' . "\n" . '%---------------------' . "\n";
	#$str_result .= '';
	$index_line += 1;
    }
    print("\n\n\n*'''''''''''''''''''''''''''''**********************'\n#\n# $title \n # Min=$val_min, Max=$val_max, cutoff=($cutoff_min, $cutoff_max):");
    my $xlabel = $self->{xlabel};
    my $ylabel = $self->{ylabel};
    #!
    #!
    my $str_file = qq(
%! Title: $title 
\\documentclass[border=9]{standalone}
\\usepackage[table,x11names,usenames, dvipsnames]{xcolor}
\\definecolor{lightgrey}{rgb}{0.9,0.9,0.9}
\\usepackage{pgfplots}
\\pgfplotsset{width=7cm,compat=1.8}
%\\documentclass[pdftex,10pt,a4paper]{article}
%\\usepackage[lmargin=5mm,rmargin=5mm,tmargin=5mm,bmargin=5mm,paperwidth=8.85cm   ,paperheight=14cm] {geometry}
%\\include{libs}
%\\newcommand*{\\ie}{\\textit{i.e.\\@\\xspace}}

\\begin{document}
   \\begin{tikzpicture}[scale=0.60] %[domain=0:100, scale=0.9]  %[x=0.150cm,y=0.000160cm]    
     \\begin{axis}[
         scale only axis,
         legend style={at={(0.03,0.97)},
           anchor=north west, legend columns=1,
           font=\\footnotesize
         },
         height=8cm,
         width=11.25cm,
         %     width=\\textwidth,
%         ymin=0, ymax=1, 
         % xmin=0, xmax=4,% range for the x axis
         ymin=$val_min, ymax=$val_max,% range for the x axis
%         xmin=60000000,         xmax=100,
         bar width=0.3cm,
         scaled y ticks=base 10:0,
         %% axis y line*=left,% ’*’ avoids arrow heads
         title={$title}, 
         %xlabel={List length (n)}, 
         ylabel={$ylabel}, 
         %% title={Comparison of arithmetic-only with the most optimal memory access pattern:}, 
         xlabel={$xlabel}, 
         %xlabel={Numer of Entity-comparisons=[1000, 10,000, 1,000,000, 100,000,000]}, 
         %% ylxoabel={Relative Difference in Time.}, 
         yticklabel style={align=right,inner sep=0pt,   xshift=-0.3em         },
         xticklabel style={align=right,inner sep=1pt,   yshift=-0.2em   },
         %% enlargelimits=false,
         grid=major,
       ]
);
    $str_file .= "$str_result";
    $str_file .= sprintf(qq(\\end{axis}
   \\end{tikzpicture}
\\end{document}));
    $str_file .= sprintf("\n");

    if(defined($self->{file_result})) {
	my $result_file = $self->{file_result};
	$result_file =~ s/[$:\$|\| \t\(\)& ]//g;
	if($result_file !~ /\.tex$/) {$result_file .= ".tex";} #! ie, then add the file-suffix.
	open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");
	print(FILE_OUT $str_file);
	close(FILE_OUT);
	if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); }
	{
	    my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	    if($result_file =~/^(.+)\/(.+?)$/) {
		#! Then we need to exiplciuty speciy the result-direcotry [https://tex.stackexchange.com/questions/268997/pdflatex-seems-to-ignore-output-directory]
		my $dir_result = $1;
		my $f_name = $2;
		$cmd = "pdflatex -output-directory $dir_result  $result_file 1>out_latex.txt";
	    }
	    printf("\t(linePlot-compile)\t %s\n", $cmd);
	    system($cmd); #! ie, to simplfiy viwing of the data
	    my $file_fig = $result_file;
	    $file_fig =~ s/\.tex/\.pdf/;
	    if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a pdf-files; please investigate\n"); }
	    printf("\t(module-linePlot::ok) Export\t $file_fig\n");
	}    
    } else { #! then we pritn the countent (of the file) to STDOUT:
	print($str_file);
    }
}



#!  ---------------------------------- EOF:
1;
