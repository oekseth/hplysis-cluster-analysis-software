=head
Brief: 
=cut
use strict;

sub extreme_deviations {
    my ($keyword, $title, $cutoff_min, $cutoff_max, $arr) = @_;
    my $val_min = 10000.0;
    my $val_max = -10000.0;

    # FIXME: compelte writing of this function ... the idea is to fetch Boudnary-codntions from the data ... 
    # FIXME: latex: summarize: ... a table capturing the core axis of varialibyt ... use results to sumamrise all of the different aspects ... map the extreme deviations to what we observe in the figures .... Examplfies the improtance of taking the apsects of ... into account (Seciton \ref{}). 

    my $str_columns_wNumbers  = "";    
    my $str_result = "";
    my $index_line = 0;

    my @arr_rows_prop = ();
    # FIXME: fetch meta-prop: ... scaffold table-rows based on the observaitons in this figure .... each row: max, min, min-max, max/min ...  and then sumamrise the global-props for the compelte input-data.
    # FIXME: fetch meta-prop: ... table-scaffold ... use abovve strategy ... sumamrize for groups ... such as rowName=[student-t-2100 , student-t-700] into "student" ... 
    # FIXME: fetch meta-prop: ... apply for both AVG and STD
    # FIXME: fetch meta-prop: ...    
    # ------------------------
    # FIXME: Perl: new: geenralise above steps .. update our std-histo-code ... variance in predittions ... hence, a table captruing the error-boudnaryies.
    # ------------------------
    foreach my $line (@{$arr}) {
	chomp($line); #! ie, remove the trail-newline.
	my @arrOf_cols = split("\t", $line);
	my $row_header = shift(@arrOf_cols);
	$row_header =~ s/_/ /g; #! ie, to avoid Latex from complaining.	
	# 
	my @cols_updated = (); #! which is to hold the formatted row-names.
	my $index = 0;
	#! -----------------
	my $p_min = 10000.0; my $p_max = -10000.0;
	#! -----------------
	foreach my $col (@arrOf_cols) {
	    my $score_updated = "";
	    if($index_line == 0) {
		if($index > 0) { #! ie, do NOT color the table-row-name:
		    $str_columns_wNumbers  .= 'columns/' . $index . '/.style={color cells},' . "\n";
		}
	    }
	    if ($col =~ /^-?\d+\.?\d*$/) { 
		if($col > 10000000) {
		    $col = "inf"; #! ie, then asusem it is an infite number, hence ignoring it.
		}
	    }
	    if( ($col ne "inf") && ($col ne "4294967296.000000") && ($col ne "nan") && ($col ne "-nan") && ($col ne "-")) {
		#! Avodi too large vlaues from cluttering the results:
		if($col > $cutoff_max) {
		    $col = $cutoff_max;
		} elsif($col < $cutoff_min) {
		    $col = $cutoff_min;
		} 
		$score_updated = sprintf("%.1f", $col);
		#! Fetch sumamry of the rows, which is of initerest when generating summaries of core-features in 
		if($col > $p_max) {
		    $p_max = $col;
		}
		if($col < $p_min) {
		    $p_min = $col;
		}
	$str_result .= join(", ", @cols_updated) . "\n";
	$index_line += 1;
    }




    print("\n\n\n*'''''''''''''''''''''''''''''********$keyword**************'\n#\n# $title \n # Min=$val_min, Max=$val_max, cutoff=($cutoff_min, $cutoff_max):");
    print(qq( ));
printf("\n");		
}


#my $globalSpecificDataConfig__idOf_row = 'tagExperiment_(.+?)\.';
my $globalSpecificDataConfig__idOf_row = 'tagExperiment_(.+?)\..*(dataXmetrics|metricXdata)';
#my $globalSpecificDataConfig__idOf_row = 'tagExperiment_(.+?)\..+subCategory_(.+)';
my $globalSpecificDataConfig__idOf_col = 'tagSample_cluster[A-zA-Z]+.[a-zA-Z]+_.(.+)';
#! ---
#! -----
my $hashOf_globalSpec__rows = {}; my $hashOf_globalSpec__rows_count = 0;
my $hashOf_globalSpec__cols = {}; my $hashOf_globalSpec__cols_count = 0;


sub __get_groupId_row {
    # FIXME: rewrite below ... then make use of below code-chunk for data-filtering
    my $string = shift;
    if(!defined($globalSpecificDataConfig__idOf_row)) {	
	my ($global_index) = ($string =~ /^groupId_(\d+)\./);
	return $global_index;
    } else {	
	my ($global_index, $alt_2) = ($string =~ /$globalSpecificDataConfig__idOf_row/);	
	if(defined($global_index)) {
	    if($global_index =~ /^\s*[0]+(\d+)\s*$/) {$global_index = $1;} #! ie, remove leading '0''s (oekseth, 06. feb. 2017).
	    if(defined($alt_2)) {
		$global_index .= "_" . $alt_2;
		printf("global_index:\t $global_index\t given alt_2\t $alt_2\n");
	    }
	    if($globalSpecificDataConfig__idOf_row__useIndexDirectly) {return $global_index;}
	    my $number = $hashOf_globalSpec__rows->{$global_index};
	    if(defined($number)) {return $number;}
	    else {
		#! Get the 'next count' and then udpate the 'glboal counter':
		$number = $hashOf_globalSpec__rows_count++;
		$hashOf_globalSpec__rows->{$global_index} = $number;
		return $number;
	    }
	    #     printf("global_index=\"$global_index\" given string: $string\n");
	}
	return $global_index;
    }
}

sub summary_of_devationRows {
    my ($dir, $result_file, $result_file_transp, $in_file_pattern) = @_;    
    my @files;
    opendir(DIR, $dir) or die "Unalbe to open dir=\"$dir\": " . $!;
    my $globalSpecificDataConfig__idOf_row__useIndexDirectly = 0; #! ie, as we expect the 'input to NOT be a number'.
    if($globalSpecificDataConfig__idOf_row__useIndexDirectly ==0) {
	@files = sort {(stat $a)[1] <=> (stat $b)[1]} readdir(DIR);
    } else {
	@files = sort {
	    __get_groupId_row($a) <=> __get_groupId_row($b)
	    # ($b =~ /$globalSpecificDataConfig__idOf_row/)[0]
	    # $number_1 <=> ($a =~ /$globalSpecificDataConfig__idOf_row/)[0]	    
	    # ||
	    # fc($a)  cmp  fc($b)
	} readdir(DIR);    
    }
    #!
    #! Summary-prop of the transposed facts:
    #! Note: Idea: ... identify patterns in when seperation occurs ... are there any algorithms which consistently results in low degree of variance? ... when is it proper to assert 'a low degree of variance' (even for cases we would asusme have a high degree of variance)? ... Could this be a useful example of cases where Euclidean distance has isnfuficent prediciton-accurayc (for detecting csuch cases)?
    #! Note: Idea: ...     
    my %hashOf_eachTrans;
    
    open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");	    
    #!
    #! Iterate through the files:
    foreach my $input_file (@files) {
	if($input_file =~ /$in_file_pattern/) {
	    open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
	    #! Order the indiex in two different cateogries:
	    #! Note: idea: idneitfy the metric-combaitniosn which trindocues the largest variance in data. We aim at understanding reasons for when, and when, this seperation is idneitfied, throuygh oru knwoledge of the algorithms, metircs, and data. To exemplify, ... % FIXME: udpate this when we ahve ivnestiated the result-dat.
	    #! Note: idea: ... 
	    #! Note: idea: ... 	
	    my @arr_low = ();
	    my @arr_high = ();
	    #! 
	    #! Iterate through the rows:
	    my $cnt_lines = 0;
	    while (my $line = <FILE_IN>) {
		chomp($line); #! ie, remvoe the trail-newline.
		if($cnt_lines > 0) {
		    # FIXME: how may we use ifnormation, such as " skewness        kurtosis        quartile-1      quartile-2      quartile-3      valueExtreme-min        valueExtreme-max        quartile-2-variance", to imrpvoe acucrayc of ouur analsysi-appraoch (eg, of the sigiance of deviaotisnh)? 
		    my @arrOf_cols = split("\t", $line);
		    if(scalar(@arrOf_cols) > 1) {
			if(!defined($hashOf_eachTrans{$arrOf_cols[0]})) {
			    $hashOf_eachTrans{$arrOf_cols[0]} = {arr_low = [], arr_high = []};
			}
			if($arrOf_cols[2] < 0.1) {
			    push(@arr_low, $arrOf_cols[0]);
			    #push(@arr_low, $arrOf_cols[0] . ":" . $arrOf_cols[2]);

			    #! Add to transposed hash if Not already there:
			    my $found = 0;
			    # FIXME: validate below logic:
			    foreach my $e @{$hashOf_eachTrans{$arrOf_cols[0]}->{arr_low}} {
				printf("\t cmp-in-set:low\t %s \tVS \t%s\n", $e, $input_file);
				if($e eq $input_file) {
				    $found = 1;
				}
				if($found == 0) { push(@{$hashOf_eachTrans{$arrOf_cols[0]}->{arr_low}}, $input_file); }
			    }
			} else {
			    push(@arr_high, $arrOf_cols[0]);
			    #push(@arr_high, $arrOf_cols[0] . ":" . $arrOf_cols[2]);			    
			    #! Add to transposed hash if Not already there:
			    my $found = 0;
			    # FIXME: validate below logic:
			    foreach my $e @{$hashOf_eachTrans{$arrOf_cols[0]}->{arr_high}} {
				printf("\t cmp-in-set:high\t %s \tVS \t%s\n", $e, $input_file);
				if($e eq $input_file) {
				    $found = 1;
				}
				if($found == 0) { push(@{$hashOf_eachTrans{$arrOf_cols[0]}->{arr_high}}, $input_file); }
			    }
			}
		    }
		}
		$cnt_lines += 1;
	    }
	    # FIXME: ... 
	    #!
	    #! Write out the cases:
	    printf(FILE_OUT "%s\tlow\t%s\n", $input_file, join("\t", @arr_low));
	    printf(FILE_OUT "%s\thigh\t%s\n", $input_file, join("\t", @arr_high));
	}
	#!
	#!  
	close(FILE_IN);
    }
    close(FILE_OUT);
    { #! Export: the transposed mappings: 
	open(FILE_OUT, ">$result_file_transp") or die("!!\t input(file): An error in opning file=\"$result_file_transp\"");	    
	while (($key, $self) = each (%hashOf_eachTrans))
	#foreach my $head (keys %hashOf_eachTrans ) {	    
	    printf(FILE_OUT "%s\tlow\t%s\n",  $key, join("\t", @{$self->{arr_low}}));
	    printf(FILE_OUT "%s\thigh\t%s\n", $key, join("\t", @{$self->{arr_high}));
	}
	#!
	#!  
	close(FILE_IN);
    }
}


# FIXME: f-result-tut-bias-2/*dev*
# FIXME: f-result-tut-bias-2/*img.tsv # FIXME: udpat eour soruce-code ... comptuing a 'dev' for this.
