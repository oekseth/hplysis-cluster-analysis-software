=head
@brief provides logiccs for construiton of scatter-maps.
=cut
package module_barChart;
use strict;
use Carp;
# -------------------------------------------

sub init_self {
    my ($result_file) = @_;
    return {
	result_file => $result_file,
	config_cntBuckets => 10,
	#! where 'this' si a temrpaory-strut, to be nsued for future code-modivcaitons.
    };
}

sub _score_isOf_interest{
    #my ($line, $col) = @_; #, $cutoff_min, $cutoff_max) = @_;
    my ($line, $col, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) = @_;
    my $isTo_drop = 0;
    if ($col =~ /^-?\d+\.?\d*$/) { if($col > 10000000) { $isTo_drop = 1; } }
    if( ($col eq "inf") || ($col eq "4294967296.000000") || ($col eq "nan") || ($col eq "-nan") || ($col eq "-")) {$isTo_drop = 1;}
    if($isTo_drop == 0) {
	if ($col !~ /^-?\d+\.?\d*$/) { 
	    croak("!!\t : val=\"$col\" is NOT a number, given line=\"$line\".");
	} else {
	    if($col < $cutoff_min) { $isTo_drop = 1; }
	    if($col > $cutoff_max) { $isTo_drop = 1; }
	    if( ($col > -0.001) &&  ($col < 0.001) ) {
		# print("Drop\t score:$col\n");
		$isTo_drop = 1;  #! ie, as we assuemt eh valeu is set to '0' ... ie, to get emanignful differences in repdcitions.
	    }
	}
    }
    #if(1 == 2) { # FIXME: remove.
    if($isTo_drop == 0) {
	if(defined($arr_scoreIndex_toUse) && scalar(@{$arr_scoreIndex_toUse}) ) { #! then we investigate if this column is to be used in the result-filter.
	    my $is_found = 0;
	    foreach my $ind (@{$arr_scoreIndex_toUse}) {
		if($ind == $score_index) {$is_found = 1;}
	    }
	    if($is_found == 0) {$isTo_drop = 1;}
	}
    }
    return ($isTo_drop == 0);
}

sub _pair_isOf_interest {
    my ($line, $col_1, $col_2, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) = @_;
    return (
	_score_isOf_interest($line, $col_1, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) &&
	_score_isOf_interest($line, $col_2, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) );
}


sub barChart_typeOfStype_histogram {
    my ($self,  $matrix, $arr_rowNames, $title, $xlabel, $ylabel) = @_;
    #!
    #!
    my $result_file = $self->{result_file};
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
    if($result_file !~ /\.tex$/) {
	$result_file .= ".tex"; #! ie, then add a file-ending
    }
    $title =~ s/[_$:\$|\| \t\(\)& ]/ /g;
    $xlabel =~ s/[_$:\$|\| \t\(\)& ]/ /g;
    $ylabel =~ s/[_$:\$|\| \t\(\)& ]/ /g;
    #!

    #! Get exrreme props:
    my $score_min = 1000000000.0; my $score_max = -100000000000.0;
    foreach my $row (@{$matrix}) {
	foreach my $score (@{$row}) {
	    if($score_min > $score) {
		$score_min = $score;
	    }
	    if($score_max < $score) {
		$score_max = $score;
	    }	    
	}
    }
    if($score_min == $score_max) {
	return; #! ie, as there is not any sicant data-items.
    }
    $score_min = abs($score_min);
    $score_max = abs($score_max);    
    #!
    #! Step: cosntruct bucket-range:
    my @arr_bucket = ();
    my $size_each = ($score_max - $score_min)/ (1.0 * $self->{config_cntBuckets}); #! where '1.0' is sued to convert the config_cntBuckets attribute to a floating-point number.   
    my $curr_min = $score_min;
    for(my $i = 0; $i < $self->{config_cntBuckets}; $i++) {
	my $curr_max = $curr_min + $size_each;
	push(@arr_bucket, {min => $curr_min, max => $curr_max
	     });
	$curr_min += $size_each;
    }


    #!
    #! Fill the buckets:
    my @mat_result = ();
    my $row_index  = 0;
    #my @arr_rowNames = ();
    my @arr_str_legend = ();
    foreach my $row (@{$matrix}) {
	{ #! Intiate the measurrement---count(parameter) bucket:
	    my @r_row = ();
	    my $add_legend = (scalar(@arr_str_legend) == 0);
	    for(my $i = 0; $i < $self->{config_cntBuckets}; $i++) {
		my $b = $arr_bucket[$i];
		if( ($b->{min} ne "") && ($b->{max} ne "") ) {
		    my $str = sprintf("\"%.1f--%.1f\"", $b->{min}, $b->{max});
		    # $str =~ s/[_$:\$|\| \t\(\)& ]/ /g;
		    push(@r_row, {count => 0, name => $str});
		    if($add_legend) {
			push(@arr_str_legend, $str);
		    }
		}
	    }
	    push(@mat_result, \@r_row);
	}
	#!
	#! Idnitfy the number of apraemters asostied toe ach extrmee preiciton:
	# my $row_header = shift(@{$row});
	foreach my $score (@{$row}) {
	    #!
	    #! Find the bucket-id:
	    my $bucket_id = $self->{config_cntBuckets}; # ie, the extreme case.
	    {
		my $bucket_index = 0;
		foreach my $bucket (@arr_bucket) {
		    if( ($score >= $bucket->{min})  &&  ($score <= $bucket->{max}) ) {
			$bucket_id = $bucket_index;
		    }
		    $bucket_index += 1;
		}
	    }
	    #!
	    #! Increment the count assotied to this score
	    # Note: why is this count of interest?? <-- number of different parameters assitedd to it ... Hence, the number of freedoms.
	    $mat_result[$row_index]->[$bucket_id]->{count} += 1;
	}
	$row_index += 1;
    }
    my $str_data = "";
    #! Note: examples are given in "tut-barChar.tex",  http://pgfplots.net/tikz/examples/bar-plot/ , https://tex.stackexchange.com/questions/101320/grouped-bar-chart , http://pgfplots.net/tikz/examples/stacked-bar-plot/ , 
    
    { #! Construct the specification
	foreach my $row (@mat_result) {
	    #! Note: consturct a emasurement-line such as: \addplot coordinates {(tool8,7) (tool9,9) (tool10,4)}; ... or: ... \addplot[style={bblue,fill=bblue,mark=none}]    coordinates {(EgyptHD, 1.0) (Hover,1.0) (Navi,1.0)}
	    my @arr_str_coord = ();
	    foreach my $b (@{$row}) {
		if($b->{name} ne "") {
		    push(@arr_str_coord, sprintf("(%s, %u)", $b->{name}, $b->{count}));
		}
	    }
	    $str_data .= "\\addplot coordinates {" . join(" ", @arr_str_coord) . "};\n";
	}
    }
    #!
    #!
    my $str_legend_block = join(", ", @arr_str_legend);
    #! Note: the str_legend_rows .... refers to the name of each row ... 
    my $str_legend_rows = join(", ", @{$arr_rowNames});
    $str_legend_rows =~ s/[_$:\$|\| \t\(\)& ]/ /g;
    #!
    #!
    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");        
    #!    
    if(1) { #! then we sue the udpated style:
	print(FILE_OUT qq(
\\documentclass[border=10pt]{standalone}
\\usepackage{pgfplots}
\\pgfplotsset{width=7cm,compat=1.8}
\\begin{document}
\\begin{tikzpicture}
\\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    ylabel={\\#Freedoms},
    symbolic x coords={$str_legend_block},
    %% symbolic x coords={16881.0--16903.5, 16903.5--16926.0, 16926.0--16948.5, 16948.5--16971.0, 16971.0--16993.5, 16993.5--17016.0, 17016.0--17038.5, 17038.5--17061.0, 17061.0--17083.5, 17083.5--17106.0},
      nodes near coords,
      nodes near coords align={vertical},
      x tick label style={rotate=45,anchor=east},
      title={ $title },
    %xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    % height=8cm, width=15.5cm,
    bar width=0.4cm,
    ymajorgrids, tick align=inside,
    major grid style={draw=white},
    enlarge y limits={value=.1,upper},
    % ymin=0, ymax=100,
    % axis x line*=bottom,
    % axis y line*=right,
    %y axis line style={opacity=0},
    %tickwidth=0pt,
    enlarge x limits=true,
    legend style={
      at={(0.5,-0.2)},
      anchor=north,
      legend columns=-1,
                  /tikz/every even column/.append style={column
                    sep=0.5cm}
                          },
    ]
));
    } else {
    print(FILE_OUT qq(\\documentclass[border=10pt]{standalone}
\\usepackage{pgfplots}
\\pgfplotsset{width=7cm,compat=1.8}
\\begin{document}
\\begin{tikzpicture}
\\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    ylabel={\\#Freedoms},
    symbolic x coords={$str_legend_block},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    ]
));
    }
    print(FILE_OUT qq(
$str_data
\\legend{$str_legend_rows}
\\end{axis}
\\end{tikzpicture}
\\end{document}
));
    close(FILE_OUT);
    #!
    #! 
    {
	my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	if($result_file =~/^(.+)\/(.+?)$/) {
	    #! Then we need to exiplciuty speciy the result-direcotry [https://tex.stackexchange.com/questions/268997/pdflatex-seems-to-ignore-output-directory]
	    my $dir_result = $1;
	    my $f_name = $2;
	    $cmd = "pdflatex -output-directory $dir_result  $result_file 1>out_latex.txt";
	}
	printf("\t(scatter-compile)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $file_fig = $result_file;
	$file_fig =~ s/\.tex/\.pdf/;
	if(!(-f $file_fig)) {
	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n");
	    croak("todo-remove");
	}
    }
}

#!  ---------------------------------- EOF:
1;
