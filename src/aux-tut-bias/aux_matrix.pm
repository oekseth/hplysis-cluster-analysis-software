package aux_matrix;
use strict;
use Carp;
# -------------------------------------------
{ #! Init-prcoedure:
    my $found = 0;
    my $path_conversionScript = "liblib_x_hpLysis.so";
    foreach my $prefix ("../", "../../", "../../../") {
	my $p = $prefix . $path_conversionScript;
	if( ($found == 0) && (-f $p) 
	    #&& (-x $p) 
	    ) {
	    $path_conversionScript = $p; #! ie, the complete path to the script.
	    $found = 1;
	}
    }
    if($found == 0) {
	croak("!!\t Was NOT able to find the covnersion-scrtip \"$path_conversionScript\". Did you  remember compiling it? ... If not, then try: g++ -O2 -g tut-aux-matrixToPPM.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_ppm.exe (found in the hpLysis root/src folder).");
    } else {
	system("cp $path_conversionScript .");
    }
}


my $extreme_min = -1000000.0;
my $extreme_max = 1000000.0;



sub _score_isOf_interest{
    #my ($line, $col) = @_; #, $cutoff_min, $cutoff_max) = @_;
    my ($line, $col, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) = @_;
    my $isTo_drop = 0;
    if ($col =~ /^-?\d+\.?\d*$/) { if($col > 10000000) { $isTo_drop = 1; } }
    if( ($col eq "inf") || ($col eq "4294967296.000000") || ($col eq "nan") || ($col eq "-nan") || ($col eq "-")) {$isTo_drop = 1;}
    if($isTo_drop == 0) {
	if ($col !~ /^-?\d+\.?\d*$/) { 
	    croak("!!\t : val=\"$col\" is NOT a number, given line=\"$line\".");
	} else {
	    if($col < $cutoff_min) { $isTo_drop = 1; }
	    if($col > $cutoff_max) { $isTo_drop = 1; }
	    if( ($col > -0.001) &&  ($col < 0.001) ) {
		# print("Drop\t score:$col\n");
		$isTo_drop = 1; } #! ie, as we assuemt eh valeu is set to '0' ... ie, to get emanignful differences in repdcitions.
	}
    }
    if($isTo_drop == 0) {
	if(defined($arr_scoreIndex_toUse) && scalar(@{$arr_scoreIndex_toUse}) ) { #! then we investigate if this column is to be used in the result-filter.
	    my $is_found = 0;
	    foreach my $ind (@{$arr_scoreIndex_toUse}) {
		if($ind == $score_index) {$is_found = 1;}
	    }
	    if($is_found == 0) {$isTo_drop = 1;}
	}
    }
    return ($isTo_drop == 0);
}


#! Tranpose an input-matrix
sub transpose_matrix {
    my ($input_file, $result_file) = @_;    
    open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
#! 
#! Parse the data-set: 
    my $line_count = 0;
    my @arrOf_x_value; my @arrOf_rowHead;
    my @matrix;
    my @mat_col_row;
    my $nrows = 0; 
    while (my $line = <FILE_IN>) {
	chomp($line); #! ie, remvoe the trail-newline.    while (my $line = <FILE_IN>) {
	my @arrOf_cols = split("\t", $line);
	if(scalar(@arrOf_cols) > 1) {
	    if($line_count == 0) {
		my $head = shift(@arrOf_cols);
		@arrOf_rowHead = @arrOf_cols;		
		#!
		#! Write otu the file-anems to the meta-file, eg, to simplify docuemtantion-efforts:
		printf(FILE_OUT_global_meta "\n\n The file=\hpFile{%s} has meta-headers: \n", $input_file);
		my $index = 0;
		foreach my $id (@arrOf_cols) {
		    #printf(FILE_OUT_global_meta "index[%u]=\"%s\", \n", $index, $id);
		    $index++;
		}
	    } else { #! then it is a data-row:
		my $head = shift(@arrOf_cols); 
		#push(@arrOf_rowHead, $head); #! ie, as the column is the row-header.
		push(@arrOf_x_value, $head);
		#printf("for head=\"$head\" set-cols: %s\n", join(",", @arrOf_cols));
		#push(@matrix, \@arrOf_cols);
		push(@mat_col_row, \@arrOf_cols);
		if($nrows < scalar(@arrOf_cols)) {
		    $nrows = scalar(@arrOf_cols);
		}
	    }
	}
	$line_count++;
    }
    close(FILE_IN);
#my $ncols = scalar(@matrix);
    my $ncols = scalar(@mat_col_row);
    printf("ncols=$ncols\n"); 
    { #! Initate matrix:
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my @row = ();
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		#! Insert into the matrix:
		push(@row, $mat_col_row[$col_id][$row_id]);
	    }
	    push(@matrix, \@row);
	}
    }
    if(defined($result_file) && ($result_file  ne "") ) { #! then we wrie out the matrix.
	printf("\tExport\t%s\t at %s:%d\n", $result_file, __FILE__, __LINE__);
	open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");
	printf(FILE_OUT "#! %s\n", join("\t", @arrOf_x_value));
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    printf(FILE_OUT "%s\t%s\n", $arrOf_rowHead[$row_id], join("\t", @{$matrix[$row_id]}));
	}
	close(FILE_OUT);
    }
    return (\@matrix, \@arrOf_rowHead, \@arrOf_x_value); #! where the row-heads are incpoorapted into @matrix
}


sub getFiles_fromDir {
    my ($dir, $regex_files, $regex_files_cntGroups, $hash_remapProp) = @_;
    if($dir !~ /\/$/) {
	$dir .= "/";
    }
    #my ($dir, $regex_files) = @_;
    #my ($dir, $regex_files, $arr_nameGroups) = @_;
    my @files;
    opendir(DIR, $dir) or die "Unalbe to open dir=\"$dir\": " . $!;
    @files = sort {(stat $a)[1] <=> (stat $b)[1]} readdir(DIR);
    my @arr_file_name = ();
    my @res_files = ();
    foreach my $file_name (@files) {
	if($file_name eq ".") {next;}
	if($file_name eq "..") {next;}
	if($file_name =~ /$regex_files/) {
	    my $cnt_g =  $regex_files_cntGroups;
	    #my $cnt_g = scalar(@{$arr_nameGroups});
	    my @arr_prop_fileName = ();
	    if($cnt_g <= 1) {
		my $v = $1;
		if(defined($hash_remapProp) && defined($hash_remapProp->{$v})) {$v = $hash_remapProp->{$v};} #! ie, then imrpvoe readiblity, or merge gorups, through this prop.
		push(@arr_prop_fileName, $v);
	    }
	    if($cnt_g <= 2) {
		my $v = $2;
		if(defined($hash_remapProp) && defined($hash_remapProp->{$v})) {$v = $hash_remapProp->{$v};} #! ie, then imrpvoe readiblity, or merge gorups, through this prop.
		push(@arr_prop_fileName, $v);
	    }
	    if($cnt_g <= 3) {
		my $v = $3;
		if(defined($hash_remapProp) && defined($hash_remapProp->{$v})) {$v = $hash_remapProp->{$v};} #! ie, then imrpvoe readiblity, or merge gorups, through this prop.
		push(@arr_prop_fileName, $v);
	    }
	    if($cnt_g <= 4) {
		my $v = $4;
		if(defined($hash_remapProp) && defined($hash_remapProp->{$v})) {$v = $hash_remapProp->{$v};} #! ie, then imrpvoe readiblity, or merge gorups, through this prop.
		push(@arr_prop_fileName, $v);
	    }
	    if($cnt_g <= 5) {
		my $v = $5;
		if(defined($hash_remapProp) && defined($hash_remapProp->{$v})) {$v = $hash_remapProp->{$v};} #! ie, then imrpvoe readiblity, or merge gorups, through this prop.
		push(@arr_prop_fileName, $v);
	    }
	    if($cnt_g > 5) {croak("!!\t Add support for thsi group-count.");}
	    #!
	    #!
	    my $file_longPath = $dir .  $file_name;
	    #my $file_longPath = $dir . "/" . $file_name;
	    #!
	    #! Udpate the result-struct:
	    push(@res_files, {path => $file_longPath, local_name => $file_name, arr_groups => \@arr_prop_fileName });
	}
    }
    return \@res_files;		
}

sub getMatrix_representedAsLines {
    my ($arr_file_name, $isTo_transpose, $regex_group_row,  $inRowNameRegex_useGroupAtIndex, $max_lineCount, $isTo_returnLines_notColumns, $emptyColumnsAreToBeAdded) = @_;
#    my ($cutoff_min, $cutoff_max, $arr_file_name, $isTo_transpose, $regex_group_row,  $regex_group_row_index, $max_lineCount, $max_colCount, $isTo_returnLines_notColumns) = @_;
    my @arr_lines = (); #! the reutnr-object.
    # print("regex:\t $regex_group_row \n");
    # ------------------------------
    my $arr = [];
    my $file_index = 0;
    my $arr_colHeader = undef;
    foreach my $o_file_name (@{$arr_file_name}) {
	my $file_name = $o_file_name->{name};
	if($isTo_transpose) {	 
	    my $result_file = "tmp-transp.tsv"; #! where where a n-uqnie file si asued to avoid filling up teh file-system with tempraory files.
	    transpose_matrix($file_name, $result_file);
	    $file_name = $result_file;
	}
	open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
	my $row_prefix = "file-" . $file_index;
	my $cnt_lines = 0;
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.
	    if($cnt_lines == 0) {
		my @r = split("\t", $line);
		$arr_colHeader = \@r;
		$cnt_lines += 1;
		next; #! ie, as we asusmet the file-row-header si NOT to be used in the heatmpa-generaiotn (we in this script cconstruct).
	    }
	    if($line =~/\#/) {$cnt_lines += 1; next;} #! ie, as we assume taht the row-ehader is then NOT of interest
	    if($cnt_lines > $max_lineCount) { next;}
	    {
		if(!defined($regex_group_row) || ( $line =~ /$regex_group_row/) ) { #|| (scalar(@{$arr}) == 0) ) {
		    #if(!defined($regex_row) || ( $line =~ /$regex_row/) || (scalar(@{$arr}) == 0) ) {
		    #if($row_prefix ne "") { #! then increase the acucryacy of the row-name
		    #$line = $row_prefix . $line;
		    #}
		    #!
		    #!

		    #! Fetch data from the ccorrect columns:
		    my @arrOf_cols = ();
		    { #! Filter: do NOT include valeus which are NOT set:
			my @r = split("\t", $line);
			my $score_index = 0;
			foreach my $s (@r) {
			    my $cutoff_min = -100000;
			    my $cutoff_max =  100000;
			    my $arr_scoreIndex_toUse = undef;
			    if( ($score_index == 0) || _score_isOf_interest($line, $s, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index)) {
				push(@arrOf_cols, $s);
			    } else {
				if(defined($emptyColumnsAreToBeAdded) && ($emptyColumnsAreToBeAdded == 1) ) {
				    push(@arrOf_cols, "-");
				}
			    }
			    $score_index += 1;
			}
		    }
		    my $row_header = $arrOf_cols[0];
		    #printf("\t\t %s\n", $row_header);
		    my $row_header_raw = $row_header;
		    my $regex = $regex_group_row;
		    if($row_header_raw =~ /$regex/) {
			my $val_meta = $2;
			$row_header = $1;
			#printf("\t\t\t %s\n", $row_header);
			if($row_header eq "") {
			    croak("!!\t Row-header was not set (given regex='$regex'): did you forget the '(' group ')' paranthesis?");
			}
			if(defined($inRowNameRegex_useGroupAtIndex )) {
			    if($inRowNameRegex_useGroupAtIndex == 0) { #! then all row-headers are part of the same group.
				$val_meta = ""; 
				$row_header = $1;
			    } elsif($inRowNameRegex_useGroupAtIndex == 1) {
				$val_meta = $2; # TODO: is this always correct?
				$row_header = $1;
			    } elsif($inRowNameRegex_useGroupAtIndex == 2) {
				$val_meta = $1; # TODO: is this always correct?
				$row_header = $2;
			    } elsif($inRowNameRegex_useGroupAtIndex == 3) {
				$val_meta = $1; # TODO: is this always correct?
				$row_header = $3;
			    } else {
				croak("!!(update-script)\t add support for self->{inRowNameRegex_useGroupAtIndex}='inRowNameRegex_useGroupAtIndex'");
			    }
			}
		    } else {
			croak("!!\t row-header=\"$row_header_raw\" did NOT have a match for regex='$regex', given line: $line");
		    }
		    
		    #printf("\t\t %s\n", $row_header);
		    #my $s      = sprintf("%.1f", $arrOf_cols[2]); #! ie, the expeced index of the "variance" attribute.
		    my @arr_score = ();
		    if($isTo_returnLines_notColumns == 0) {
			@arr_score = ($row_header);
		    }
		    for(my $i = 1; $i < scalar(@arrOf_cols); $i++) {
			my $s = $arrOf_cols[$i];
			if($s =~ /[a-z]+/i) {
			    croak("!!\t : [$cnt_lines].val[$i]=\"$s\" is NOT a number, given line=\"$line\"\t \t row_header=\"$row_header\", and file=\"$file_name\".");
			}
			push(@arr_score, $s);
		    }
		    #! -------------
		    if(defined($emptyColumnsAreToBeAdded) && ($emptyColumnsAreToBeAdded == 1) ) { #! Task: ensure that the row-columns are cosnistent:
			{
			    my $ncols = scalar(@arr_score);
			    $ncols += 1; #! ie, to 'add' the row-heaer-name
			    my $ncols_header = scalar(@{$arr_colHeader});
			    if($ncols != $ncols_header) {
				my $key = $row_header;
				#my $key = $arr_score[0];
				croak("!!\t(aux-matrix::file-read)\t for key:\"$key\"\t incosnsiten ncols: row:$ncols while header:$ncols_header\t given file:\"$file_name\" \t  line:\t$line");
			    }
			}
		    }
		    #! -------------
		    if($isTo_returnLines_notColumns) {
			my $str = $row_header .  "\t" . join("\t", @arr_score);
			#printf("\t[$cnt_lines][$row_header]\t" . $str . "\n");
			push(@arr_lines, $str);
		    } else {
			#printf("row_header\t $row_header \n");			
			#push(@arr_score, $row_header);
			push(@arr_lines, \@arr_score);
		    }
		    #!
		    #!
		    $cnt_lines += 1;
		    #!
		    #! Increment the global row-id-prop ... of importance when merging mulitple scores (eg, STD-scores from differnet measuremetns) ... the stnrehgt of this appraoch relates t the fleciblity, ie, that we do not need to hardcode cases ... hence, reduces the analytical bias in this approach.
		    # $self->{global_row_id} += 1;
		}
	    }
	}
	close(FILE_IN);
	$file_index += 1;
    }    
    # ------------------------------
    #! return: 
    return (\@arr_lines, $arr_colHeader);
}

#! Note: idea is to make pltos mroe readble, ie, by splitting data into mulitple gorups ... henc,e addreisisng issues in image-clutter (weg, for cases where there are too many lagends in a line-plot-iamge).
sub splitLinesArr_intoGroup {
    my ($arr_lines,  $cnt_groups, $title, $file_prefix, $optional_writeToTemporaryFile) = @_;
    my @o_result = ();
    my $cnt_lines = 0;
    my $cnt_bucket = 0;
    my $obj_curr = {title => "", arr_lines => [], file_name => $file_prefix . "bucket0.tex"};
    my $prev_count = 0;
    my $bucket_total = 0;
    foreach my $line (@{$arr_lines}) {
	if($cnt_bucket >= $cnt_groups) {
	    $obj_curr->{title} = "Range($prev_count, $cnt_lines): " . $title;
	    push(@o_result, $obj_curr);
	    #! 
	    #! Update counters.
	    $prev_count = $cnt_lines;
	    $cnt_bucket = 0;
	    $bucket_total += 1;
	    #! Construct new object:
	    $obj_curr = {title => "", arr_lines => [], file_name => $file_prefix . "bucket" . $bucket_total . ".tex"};
	}
	push(@{$obj_curr->{arr_lines}}, $line);
	$cnt_bucket += 1;
	$cnt_lines += 1;
    }
    #! Add last object, and then return:
    $obj_curr->{title} = "Range($prev_count, $cnt_lines): " . $title;
    push(@o_result, $obj_curr);
    #!
    if(defined($optional_writeToTemporaryFile) && ($optional_writeToTemporaryFile == 1) ) {
	foreach my $o (@o_result) {
	    my $result_file = $o->{file_name};
	    $result_file =~ s/\.tex/\.tsv/;
	    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");
	    my $arr = $o->{arr_lines};
	    if(ref($arr->[0]) eq ref("")) {
		print(FILE_OUT join("\n", @{$arr}));
	    } else { #! then we asume it is an array:
		foreach my $row (@{$arr}) {
		    print(FILE_OUT join("\t", @{$row}) . "\n");
		}
	    }
	    close(FILE_OUT);
	    $o->{result_file} = $result_file;
	}
    }

    return \@o_result;
}


sub matrixFile_to_ppm {
    my ($input_file, $result_file, $isTo_exportToPDF) = @_;
    #!
    my $path_conversionScript = "tut_ppm.exe"; #! which we expeect is onstructed by: g++ -O2 -g tut-aux-matrixToPPM.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_ppm.exe
    my $found = 0;
    foreach my $prefix ("./", "../", "../../", "../../../") {
	my $p = $prefix . $path_conversionScript;
	if( ($found == 0) && (-f $p) && (-x $p) ) {
	    $path_conversionScript = $p; #! ie, the complete path to the script.
	    $found = 1;
	}
    }
    if($found == 0) {
	croak("!!\t Was NOT able to find the covnersion-scrtip \"$path_conversionScript\". Did you  remember compiling it? ... If not, then try: g++ -O2 -g tut-aux-matrixToPPM.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_ppm.exe (found in the hpLysis root/src folder).");
	return -1;
    }
    if(!(-f $input_file)) {	    printf(STDERR "!!\t Did NOT find input-file=\t$input_file ; please investigate\n");  return -1;	}
    my $file_fig = $result_file;
    { #! export to ppm
	my $cmd = $path_conversionScript . " $input_file $file_fig";
	printf("\t(tsv-to-ppm)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$file_fig \t Was Not able to generate a ppm-file; please investigate\n"); 	}
    }
    if($isTo_exportToPDF) { #! export to pdf
	my $file_pdf = $result_file;
	$file_pdf =~ s/\.ppm/\.pdf/;
	my $cmd = "convert $file_fig $file_pdf";
	printf("\t(ppm-to-pdf)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $file_pdf)) {	    printf(STDERR "!!\t For file=\t$file_pdf \t Was Not able to generate a pdf-file; please investigate\n"); 	}
    }
}

# ============================================================
# ============================================================
# ============================================================
# ============================================================

sub __get_formattedCell_func_extract_minMax_fromLines {
    my ($obj, $col_key) = @_;
    my $score = $obj->{$col_key};
    if(!defined($score)) {
	if($col_key eq "diff_max_min") {
	    my $score_min = $obj->{min};
	    my $score_max = $obj->{max};			    
	    if( ($score_min != 0) && ($score_max != 0) ) {
		$score_min = abs($score_min);
		$score_max = abs($score_max);
		#!
		$score = $score_max / $score_min;
	    } else {$score = "";} #! ie, then ignore it.
	    #printf("\t(aux-matrix::filterr-min-max)\t score\t'$score' given \t ('$score_max' - '$score_min')\n");
	} elsif($col_key eq "avg") {
	    if( ($obj->{cnt_cells} != 0) && ($obj->{sum} != 0) ) {
		#!
		$score = $obj->{sum} / $obj->{cnt_cells};
	    } else {$score = "";} #! ie, then ignore it.
	} elsif($col_key eq "min") {
	    #!
	    $score = $obj->{min};
	} elsif($col_key eq "max") {
	    #!
	    $score = $obj->{max};	    
	} else {
	    #! Note: to get a stack trace when 'croak(..)' is ccalled, write: perl -MCarp=verbose ... #! details: [https://perldoc.perl.org/Carp.html]
	    croak("!!\t In result-genraiton, add support for colKey='$col_key'");
	}
    }  else {
	#print("\t\t(aux-matrix)\t score{$col_key} = '' given score='$score'\n");
	#$score = "";
    } #! ie, then ignore it.
    return $score;
}

#! A funciton which generates the "self" object for the "extract_minMax_fromLines(..)" function.
sub init_extract_minMax_fromLines {
    return {
	rowPattern_isA_regex => 1, #! ie, where we assuemt aht the $regex_group_row attribute is to be treated as a regular aepxresion (and Not as a string)
	#! Case: "merge_eachRowAndNotColumns = 1": useful for summary-generation, eg, in the cosntruction of scatter-plots, bar-charts, line-plots, tables, etc.
	#! Case: "merge_eachRowAndNotColumns = 0": provides a detailed insight into the impact of different column-specific attributes
	merge_eachRowAndNotColumns              => 1,
	format_isTo_returnAsMatrix => 1,
        format_isTo_returnAsMatrix_rowsAsString => 1, #! which if set to '0' impleis that we instead exprots each cell sperationesly.	
        arr_scoreIndex_toUse => [],  #! whcih if empty impleist that we sue all scores.
        #! Specify: the proeprties to be exported when the "format_isTo_returnAsMatrix = 1" attribute is used:
        inExpert_keysToUse   => ["diff_max_min", "min", "max"],
        #inExpert_keysToUse   => ["diff_max_min", "min", "max", "cnt_rows", "cnt_cells", "sum", "arr_scores"],
        config_mergeRowsWithSameName    => 0, #! which is used as a snaity-check when the "my $arr_nameGroups  = undef" option is applied+used.
    };
}




=head
@brief split lines into groups, and then find their min--max property
@remarks 
    -- param="res_object": eiterh an empty hash, or the result of the previous ccall to the "extract_minMax_fromLines(..)" funciton-call (which is sueful for cases where muliple files needs to be analysed sperately)
    -- param="arr_lines":  we expecct the inptu to be a set of lines, eg, cosntructed through "sub getMatrix_representedAsLines(..)" in combination with "sub splitLinesArr_intoGroup(...)"

@remarks 
    -- this funciton summarizes, and joins, attributes across different measurements. An example relates to ffile=... where ... 
=cut
sub extract_minMax_fromLines {
    my ($self, $res_object, $arr_colHeader, $arr_lines, $regex_group_row, $arr_nameGroups, $cutoff_min, $cutoff_max) = @_;
    if(ref($res_object) != ref({})) {
	my $str = "result-object of type=" . ref($res_object) . " was not as expected: please udpate your funciotn-call";
	croak($str);
    }
    # my $res_object = {};
    foreach my $line (@{$arr_lines}) {
	my @arr_score = split("\t", $line);
	#! -------------
	{ #! Task: ensure that the row-columns are cosnistent:
	    if( $self->{config_mergeRowsWithSameName} == 1) {
		my $ncols = scalar(@arr_score);
		# $ncols += 1; #! ie, to 'add' the row-heaer-name
		my $ncols_header = scalar(@{$arr_colHeader});
		if($ncols != $ncols_header) {
		    my $key = $arr_score[0];
		    printf(STDERR "!!\t(aux-matrix::filter::pre)\t for key:\"$key\"\t incosnsiten ncols: row:$ncols while header:$ncols_header\n");
		    {
			if($ncols < $ncols_header) {
			# FIXME: is below correct <-- asusmtpion si that this ould arise from 'blank spaces', ie, q auqick-fix.
			    for(my $k = $ncols; $k < $ncols_header; $k++) {
				push(@arr_score, "-");
			    }
			} else {
			    # FIXME: is below correct <-- asusmtpion si that this ould arise from 'blank spaces', ie, q auqick-fix.
			    for(my $k = $ncols_header; $k < $ncols; $k++) {
				pop(@arr_score);
			    }		
			}
			my $ncols = scalar(@arr_score);
			printf(STDERR "!!\t\t(aux-matrix::filter::pre..Affter-udpate)\t for key:\"$key\"\t incosnsiten ncols: row:$ncols while header:$ncols_header\n");
		    }
		}
	    }
	}
	#! -------------
	my $row_name = shift(@arr_score);
	my @groups = ($row_name);
	my $isA_match = 1;
	if($self->{rowPattern_isA_regex}) {
	    @groups = ($row_name =~ m/$regex_group_row/g);	    
	    if(scalar(@groups) == 0) {$isA_match = 0;}
	    if(!defined($arr_nameGroups)) {  #! then we assuem that we are intersted in merging lines with the same row-header (an option sueful when the lien-aray ocnissts of lines from mulitple files)
		#! Note: to aovid this option from consuign the users7callers (of this API), we have added/used a "self" hash-config-otpion to this , ie, to validate tha below/this is the correct intpreation of hte users' indent.
		if( $self->{config_mergeRowsWithSameName} == 0) {
		    croak("!!\t Configuraiton NOT as expected: please either update the 'config_mergeRowsWithSameName' paraemter, OR set the arr_nameGroups attribute");
		}
		@groups = ($row_name);
	    } elsif(scalar(@groups) != scalar(@{$arr_nameGroups})) {
		my $str = sprintf("!!\t groups=\"%s\" was not split into the expected number of groups, give row_name=\"$row_name\" \t , and \t line=$line", join("\t", @groups));
		croak($str); # TODO: consider removing this.
		print($str . "\n");
	    }
	} else {
	    if($row_name ne $regex_group_row) {$isA_match = 0;}
	}
	if($isA_match == 0) {next;} #! ie, as we then assuemt atht this row should NOT be evalauted.

	for(my $i = 0; $i < scalar(@groups); $i++) {
	    my $g = $groups[$i];
	    if(defined($arr_nameGroups)) {
		$g = $arr_nameGroups->[$i];  #! ie, use an initutive name.
	    }
	    #printf("\t(aux-matrix)\t\t key:'$g'\n");
	    #foreach my $g (@groups) {
	    my $o_g = $res_object->{$g};
	    if($self->{merge_eachRowAndNotColumns} == 1) {
		if(!defined($o_g)) {
		    my $empty_object = {diff_max_min => undef, min => $extreme_max, max => $extreme_min, cnt_rows => 0, cnt_cells => 0, sum => 0, arr_scores => [], avg => undef};
		    $o_g = $empty_object;
		    $res_object->{$g} = $o_g;
		}
	    } else {
		if(!defined($o_g)) {
		    $o_g = {cnt_rows => 0, arr => []};
		    foreach my $score (@arr_score) { #! then seperately insert for each of he scores:
			my $empty_object = {diff_max_min => undef, min => $extreme_max, max => $extreme_min, cnt_rows => 0, cnt_cells => 0, sum => 0, arr_scores => [], avg => undef};
			push(@{$o_g->{arr}}, $empty_object);
		    }
		    $res_object->{$g} = $o_g;
		}
	    }
	    my $cnt_cells_added = 0;
	    my $score_index = 0;
	    foreach my $score (@arr_score) { #! then seperately insert for each of he scores:
		if(_score_isOf_interest($line, $score, $cutoff_min, $cutoff_max, $self->{arr_scoreIndex_toUse}, $score_index)) {
		    my $obj = $o_g;
		    #my $obj = $o_g->{arr};
		    if($self->{merge_eachRowAndNotColumns} == 0) { #! then we fetch an object speicif to colum=$score_index
			my $cnt_now = scalar(@{$o_g->{arr}});
			if($score_index >= $cnt_now) {
			    for(my $k = $cnt_now; $k < $score_index + 1; $k += 1) {
				my $empty_object = {diff_max_min => undef, min => $extreme_max, max => $extreme_min, cnt_rows => 0, cnt_cells => 0, sum => 0, arr_scores => [], avg => undef};
				push(@{$o_g->{arr}}, $empty_object);
			    }
			}
			#printf("\t(aux-matrix.col[$score_index])\t\t key:'$g'\n");
			$obj = $o_g->{arr}->[$score_index];
		    }
		    #! Update:
		    push(@{$obj->{arr_scores}}, $score);
		    $obj->{sum} += abs($score);
		    $cnt_cells_added += 1;
		    #! Update min--max prop:
		    if($obj->{min} > $score) {
			$obj->{min} = $score;
		    }
		    if($obj->{max} < $score) {
			$obj->{max} = $score;
		    }
		    #printf("\t(aux-matrix.col[$score_index])\t\t key:'$g'\t range[%.1f --- %.2f]\n", $obj->{min}, $obj->{max});
		}
		$score_index += 1;
	    }
	    if($cnt_cells_added > 0) {
		$o_g->{cnt_rows} += 1;		    
	    }
	}
    }
    #! Constct the result-object:
    if($self->{format_isTo_returnAsMatrix}) {
	if($self->{merge_eachRowAndNotColumns} == 1) {
	    my $matrix_return = [];
	    #! Construct row-header: 
	    if($self->{format_isTo_returnAsMatrix_rowsAsString}) { #! which if set to '0' impleis that we instead exprots each cell sperationesly.
		my $r = "#! Case\t" . join("\t", @{$self->{inExpert_keysToUse}}) . "\n";
		push(@{$matrix_return}, $r);
	    } else {
		my $r = ["#! Case"];
		foreach my $c (@{$self->{inExpert_keysToUse}}) {
		    push(@{$r}, $c);
		}
		push(@{$matrix_return}, $r);
	    }
	    #! 
	    #! Add data:
	    while (my ($key, $o) = each (%{$res_object})) {
		$key =~ s/[_\(\)& ]/ /g; #! ie, to avoid getting problems with Latex
		# printf("\t(aux-matrix.col[$score_index])\t\t key:'$key'\t range[%.1f --- %.2f]\n", $o->{min}, $o->{max});
		my @r = ($key);
		my $score_index = 0;
		foreach my $c (@{$self->{inExpert_keysToUse}}) {
		    #printf("\t(aux-matrix.col[$score_index])\t\t key:'$key'\t range[%.1f --- %.2f]\n", $o->{min}, $o->{max});
		    my $score = __get_formattedCell_func_extract_minMax_fromLines($o, $c);
		    push(@r, $score);
		    $score_index += 1;
		}
		#! Add row to result:
		if($self->{format_isTo_returnAsMatrix_rowsAsString}) { #! which if set to '0' impleis that we instead exprots each cell sperationesly.
		    my $r_s = join("\t", @r) . "\n";
		    push(@{$matrix_return}, $r_s);
		} else {
		    push(@{$matrix_return}, \@r);
		}
	    }

	    #! @return the new data-structure:
	    if(0 == scalar(@{$matrix_return})) {
		printf("\tNote\t(aux-matrix::filter(min--max))\t result array is empty\n");
		return;
	    }
	    printf("(aux-matrix::filter-lines-min-max)\t return(type)=matrix(merge_eachRowAndNotColumns=0)\n");
	    return ($matrix_return);
	} else { #! then we assume that $res_object is of type hash->column, and is to be returned as a matrix of scores.
	    #! Note: this code-block provides logics to handle the case of ... min--max for each cell ... ... how?? <-- only store for "diff_max_min"?
	    {
		if(scalar(@{$self->{inExpert_keysToUse}}) != 1) {
		    croak("!!\t To simplfy cosntruciton, we expected that only one co-attribute was to be expored: please udpate your 'inExpert_keysToUse' specificaiton.");
		}
	    }
	    my $isAdded_colHeader = 0;
	    my $matrix_return = [];
	    #!
	    #!		
	    while (my ($key, $obj_row) = each (%{$res_object})) {
		#! -----------
		if($isAdded_colHeader == 0) { #! Add column-headrs:
		    $isAdded_colHeader = 1;
		    my @row = ($arr_colHeader->[0]);
		    if(scalar(@{$self->{arr_scoreIndex_toUse}} > 0) ) {
			foreach my $ind (@{$self->{arr_scoreIndex_toUse}}) {
			    my $str = $arr_colHeader->[$ind];
			    $str =~ s/[_\(\)& ]/ /g; #! ie, to avoid getting problems with Latex
			    push(@row, $str);
			}
		    } else {
			@row = (); #! ie, to simplify below logic.
			foreach my $str (@{$arr_colHeader}) {
			    $str =~ s/[_\(\)& ]/ /g; #! ie, to avoid getting problems with Latex
			    push(@row, $str);
			}
		    }
		    #! Add:
		    push(@{$matrix_return}, \@row);		    
		}
		#! -----------
		$key =~ s/[_\(\)& ]/ /g; #! ie, to avoid getting problems with Latex
		my @r = ($key);
		# {
		#     my @k = keys(%{$obj_row});
		#     printf("(axu-matrix--summary)\t \t\ttype(obj_row)=%s\t keys:%s\n", ref($obj_row), join(",", @k));
		# }
		
		if($obj_row->{cnt_rows} > 0) {
		    my $ncols = scalar(@{$obj_row->{arr}});
		    $ncols += 1; #! ie, to 'add' the row-heaer-name
		    my $ncols_header = scalar(@{$arr_colHeader});
		    if($ncols != $ncols_header) {
			croak("!!\t(aux-matrix::filter)\t for key:\"$key\"\t incosnsiten ncols: row:$ncols while header:$ncols_header");
		    }
		    my $score_index = 0;
		    for(my $k = $0; $k < $ncols; $k += 1) {
			my $col_obj = $obj_row->{arr}->[$k];
			my $col_name = $self->{inExpert_keysToUse}->[0];
			my $score = __get_formattedCell_func_extract_minMax_fromLines($col_obj, $col_name);
			#printf("\t(aux-matrix.col[$score_index])\t\t key:'$key'\t range[%.1f --- %.2f] => score:'$score'\n", $col_obj->{min}, $col_obj->{max});
			if($self->{format_isTo_returnAsMatrix_rowsAsString}) { #! which if set to '0' impleis that we instead exprots each cell sperationesly.
			    if($score eq "") {
				$score = "-"; #! ie, to esnure concistnecy in the column-names.
			    }
			}
			push(@r, $score);
			$score_index++;
		    }
		    #! Add row to result:
		    if($self->{format_isTo_returnAsMatrix_rowsAsString}) { #! which if set to '0' impleis that we instead exprots each cell sperationesly.
			my $r_s = join("\t", @r) . "\n";
			push(@{$matrix_return}, $r_s);
		    } else {
			push(@{$matrix_return}, \@r);
		    }
		}
	    }
	    printf("(aux-matrix::filter-lines-min-max)\t return(type)=matrix-filtered-data-structure\n");
	    #!
	    #! @return the new data-structure:
	    if(0 == scalar(@{$matrix_return})) {
		printf("\tNote\t(aux-matrix::filter(min--max))\t result array is empty\n");
		return;
	    }
	    return ($matrix_return);
	}
    } else {
	#! then return the internal data-objecct.
	    printf("(aux-matrix::filter-lines-min-max)\t return(type)=internla-object\n");
	return ($res_object);
    }

    # drop? # FIXME: infoVis: ... load all files into one array, then construct for ... heatmap: "$self->{merge_eachRowAndNotColumns} = 0, $self->{arr_scoreIndex_toUse = [... sepeately for each case ...]" .... idea: ... describe min---max difference for the different CCMs ...
    # -----




    # -----    
    # FIXME: infoVis[heatMap]: ... load all files into one array, then construct for ... heatmap: "$self->{merge_eachRowAndNotColumns} = 1, $self->{arr_scoreIndex_toUse = []" .... .... idea: ... describe min---max difference for the different CCMs ... how large is the overlap between the different groups (eg, True Postive versus False Psotives)?
    # ------ permtuation of above ... 
    # FIXME: infoVis[1d-group-joinInto-2d]: ... input=[CCM-->case] visu=table     ... scoreArr=<relative|min|max|aveage ... construct seperate tables for these > ... 
    # FIXME: infoVis[1d-group-joinInto-2d]: ... input=[case-->CCM] visu=bar-chart ... 
    # FIXME: infoVis[1d-group-joinInto-2d]: ... input=transposed ... scatter ... apply regex-row-filter for ['nEqual / Equal', 'Equal: Worst/Best', 'Different: Worst/Best', 'Different: Score(best)', 'Different: Score(worst)', '', '', ] ...  in the parsing make use of $self->{rowPattern_isA_regex} = 0 ... in the aprsing construct a "arr[case] = arrScore" data-structure (for column(result-attribute)=relative) ....  then iterate through this array, where result are scatter-pltos serpately for $[x:case-i]x[y:case-j] for j \neq i$
    # FIXME: infoVis[1d-group-joinInto-2d]: ... input=[CCM-->case] visu=bar-chart ... all elements in "arr[case]" (... as sued as input to scatterplot) ... sperate scatter-plots for 'min, max, aveage, diff, ...'

    
}

sub construct2d_matrix_from1dSummary {
    my ($dir, $arr_regex_fileAttrInEachGroup, $arr_groupNames, $rowPattern_isA_regex) = @_;
    # FIXME: call: getFiles_fromDir
    foreach my $regex_files (@{$arr_regex_fileAttrInEachGroup}) {
	# FIXME: new wrapper-function: ... read_dir(folder, file-pattern) ... add groups(file_names) + lines(file) to an array ... call 'this' function ... udpate this function with an arr-group prop ... if specfied, then insert data into a {$group_1}->{$group_to} matrix ... at case=format_isTo_returnAsMatrix export the data as a matrix (where diff-prop is used).  
	#! Fetch the files:
	my @arr_groups = split(/\|/, $regex_files);
	my $res_files = getFiles_fromDir($dir, $regex_files, scalar(@arr_groups));

	# FIXME: ??
	# FIXME: ??
	# FIXME: ??

    }	

    # -----------------    
    # FIXME: infoVis[1d-group-joinInto-2d]: ... pre: construct for ... heatmap: "$self->{merge_eachRowAndNotColumns} = 0, $self->{arr_scoreIndex_toUse = []", ... construct seprate matrices for the different 'True--False cases' ... seperate matries for [CCM-name] [ [nrows=100,200, ...] , [scores=...] , [ncluster=...]  ....
    # FIXME: infoVis[1d-group-joinInto-2d]: ... heatmaps ... a lienary in changes between the groups? ... 
    # FIXME: infoVis[1d-group-joinInto-2d]: ... scatter (different pair-combinations) <-- Perl::pre: what code-udpdates do we need to "generate-smmary.pl"?
    # FIXME: infoVis[1d-group-joinInto-2d]: ... bar-chart ...
    # FIXME: infoVis[1d-group-joinInto-2d]: ... scatter ...     

    # -----------------
    
    # FIXME: infoVis: ... construct for ... heatmap: "$self->{merge_eachRowAndNotColumns} = 0, $self->{arr_scoreIndex_toUse = [... sepeately for each case ...]" ... genearte result-objets for diffrenet DIR-file-regexes ... then construct a matrix of [...CCMs ...][file-prop]
    # FIXME: infoVis[1d-group-joinInto-2d]: ... heatmaps ...
    # FIXME: infoVis[1d-group-joinInto-2d]: ... bar-chart ...
    # FIXME: infoVis[1d-group-joinInto-2d]: ... scatter ...     
    # FIXME: infoVis: ... construct for ... 
    # FIXME: infoVis: ... construct for ...         
}




# --------------------------------------
1;
