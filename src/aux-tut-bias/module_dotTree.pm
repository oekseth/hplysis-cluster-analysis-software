=head
@brief provides logiccs for construiton of construction of Dot-InfoVis-trees (oekseth, 06.10.2020)
=cut
package module_dotTree;
use strict;
use Carp;
#! 
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
#use lib '.'; #! which is used for transposing data, and loading strings of data into an array.
require aux_matrix; #! which is used for transposing data, and loading strings of data into an array.
# -------------------------------------------

sub init_self {
    my ($result_file) = @_;
    return {
	result_file => $result_file,
	regex_group_vertex => '^(.+)\-', #! ie, a genralised/tempalteable strategy for goruping. 
    };
}

# --------------------
#! Global data-structures, which are sued to ensure cosntecy in coloring across different data-seets.
my %hashOf_group; #! which is used to color vertices based on their group


my $color_node_img = '[ shape="box",style="filled", fillcolor="#FCD975" ];';
my $color_node_root = '[shape=circle; fixedsize=true; width=1.2; fillcolor="#ff4f69"; fontcolor="white"; style="filled"];';



my @arr_colors = (
    '[sides=9, distortion="0.936354", orientation=28, skew="-0.126818", color=salmon2];',
    '[sides=5, distortion="0.238792", orientation=11, skew="0.995935", color=deepskyblue];',
    '[sides=9, distortion="-0.698271", orientation=22, skew="-0.195492", color=burlywood2];',
    '[sides=7, distortion="0.265084", orientation=26, skew="0.403659", color=gold1];',
    '[sides=8, distortion="0.019636", orientation=79, skew="-0.440424", color=goldenrod2];',
    '[distortion="0.039386", orientation=2, skew="-0.461120", color=greenyellow];',
    '[sides=5, distortion="0.228564", orientation=63, skew="-0.062846", color=darkseagreen];',
    '[distortion="0.624013", orientation=56, skew="0.101396", color=dodgerblue1];',
    '[sides=8, distortion="0.731383", orientation=43, skew="-0.824612", color=thistle2];',
    '[sides=6, distortion="0.592100", orientation=34, skew="-0.719269", color=darkolivegreen3];',
    '[sides=10, distortion="0.298417", orientation=65, skew="0.310367", color=chocolate];',
    '[distortion="-0.997093", orientation=50, skew="-0.061117", color=turquoise3];',
    '[sides=7, distortion="0.878516", orientation=19, skew="0.592905", color=steelblue3];',
    '[sides=10, distortion="-0.960249", orientation=32, skew="0.460424", color=navy];',
    '[sides=10, distortion="-0.633186", orientation=10, skew="0.333125", color=darkseagreen4];',
    '[sides=8, distortion="-0.337997", orientation=52, skew="-0.760726", color=coral];',
    '[sides=7, distortion="0.788483", orientation=39, skew="-0.526284", color=darkolivegreen3];',
    '[sides=7, distortion="0.138690", orientation=55, skew="0.554049", color=coral3];',
    '[sides=7, distortion="-0.010661", orientation=84, skew="0.179249", color=blanchedalmond];',
    '[distortion="-0.239422", orientation=44, skew="0.053841", color=lightskyblue1];',
    '[distortion="-0.843381", orientation=70, skew="-0.601395", color=aquamarine2];',
    '[sides=10, distortion="0.251820", orientation=18, skew="-0.530618", color=lemonchiffon];',
    '[sides=5, distortion="-0.772300", orientation=24, skew="-0.028475", color=darkorange1];',
    '[distortion="-0.226170", orientation=38, skew="0.504053", color=lightyellow1];',
    '[sides=10, distortion="-0.807349", orientation=50, skew="-0.908842", color=darkorchid4];',
    '[sides=10, distortion="-0.030619", orientation=76, skew="0.985021", color=lemonchiffon2];',
    '[distortion="-0.644209", orientation=21, skew="0.307836", color=goldenrod3];',
    '[sides=7, distortion="0.640971", orientation=84, skew="-0.768455", color=cyan];',
    '[distortion="0.758942", orientation=42, skew="0.039886", color=blue];',
    '[sides=9, distortion="-0.348692", orientation=42, skew="0.767058", color=firebrick];',
    '[distortion="0.748625", orientation=74, skew="-0.647656", color=chartreuse4];',
    '[sides=10, distortion="0.851818", orientation=32, skew="-0.020120", color=greenyellow];',
    '[sides=10, distortion="0.992237", orientation=29, skew="0.256102", color=bisque4];',    
    '[sides=6, distortion="0.545461", orientation=16, skew="0.313589", color=mistyrose2];',
    '[sides=9, distortion="-0.267769", orientation=40, skew="0.271226", color=cadetblue1];',
    '[distortion="-0.848455", orientation=44, skew="0.267152", color=bisque2];',
    '[distortion="0.305594", orientation=75, skew="0.070516", color=orangered];',
    '[sides=10, distortion="-0.641701", orientation=50, skew="-0.952502", color=crimson];',
    '[sides=9, distortion="0.021556", orientation=26, skew="-0.729938", color=darkorange1];',        
    '[sides=6, distortion="0.985153", orientation=33, skew="-0.399752", color=darkolivegreen4];',
    '[sides=7, distortion="-0.687574", orientation=58, skew="-0.180116", color=lightsteelblue1];',
    # '',
    # --------- incremnt: prop="sides" += 1 or 10 --- and if NOT set, then incmrenet prop="distortion"
    '[sides=19, distortion="0.936354", orientation=28, skew="-0.126818", color=salmon2];',
    '[sides=15, distortion="0.238792", orientation=11, skew="0.995935", color=deepskyblue];',
    '[sides=19, distortion="-0.698271", orientation=22, skew="-0.195492", color=burlywood2];',
    '[sides=17, distortion="0.265084", orientation=26, skew="0.403659", color=gold1];',
    '[sides=18, distortion="0.019636", orientation=79, skew="-0.440424", color=goldenrod2];',
    '[distortion="1.039386", orientation=2, skew="-0.461120", color=greenyellow];',
    '[sides=15, distortion="0.228564", orientation=63, skew="-0.062846", color=darkseagreen];',
    '[distortion="1.624013", orientation=56, skew="0.101396", color=dodgerblue1];',
    '[sides=18, distortion="0.731383", orientation=43, skew="-0.824612", color=thistle2];',
    '[sides=16, distortion="0.592100", orientation=34, skew="-0.719269", color=darkolivegreen3];',
    '[sides=110, distortion="0.298417", orientation=65, skew="0.310367", color=chocolate];',
    '[distortion="-1.997093", orientation=50, skew="-0.061117", color=turquoise3];',
    '[sides=17, distortion="0.878516", orientation=19, skew="0.592905", color=steelblue3];',
    '[sides=20, distortion="-0.960249", orientation=32, skew="0.460424", color=navy];',
    '[sides=20, distortion="-0.633186", orientation=10, skew="0.333125", color=darkseagreen4];',
    '[sides=18, distortion="-0.337997", orientation=52, skew="-0.760726", color=coral];',
    '[sides=17, distortion="0.788483", orientation=39, skew="-0.526284", color=darkolivegreen3];',
    '[sides=17, distortion="0.138690", orientation=55, skew="0.554049", color=coral3];',
    '[sides=17, distortion="-0.010661", orientation=84, skew="0.179249", color=blanchedalmond];',
    '[distortion="-1.239422", orientation=44, skew="0.053841", color=lightskyblue1];',
    '[distortion="-1.843381", orientation=70, skew="-0.601395", color=aquamarine2];',
    '[sides=20, distortion="0.251820", orientation=18, skew="-0.530618", color=lemonchiffon];',
    '[sides=15, distortion="-0.772300", orientation=24, skew="-0.028475", color=darkorange1];',
    '[distortion="-1.226170", orientation=38, skew="0.504053", color=lightyellow1];',
    '[sides=20, distortion="-0.807349", orientation=50, skew="-0.908842", color=darkorchid4];',
    '[sides=20, distortion="-0.030619", orientation=76, skew="0.985021", color=lemonchiffon2];',
    '[distortion="-1.644209", orientation=21, skew="0.307836", color=goldenrod3];',
    '[sides=17, distortion="0.640971", orientation=84, skew="-0.768455", color=cyan];',
    '[distortion="1.758942", orientation=42, skew="0.039886", color=blue];',
    '[sides=19, distortion="-0.348692", orientation=42, skew="0.767058", color=firebrick];',
    '[distortion="1.748625", orientation=74, skew="-0.647656", color=chartreuse4];',
    '[sides=20, distortion="0.851818", orientation=32, skew="-0.020120", color=greenyellow];',
    '[sides=20, distortion="0.992237", orientation=29, skew="0.256102", color=bisque4];',    
    '[sides=16, distortion="0.545461", orientation=16, skew="0.313589", color=mistyrose2];',
    '[sides=19, distortion="-0.267769", orientation=40, skew="0.271226", color=cadetblue1];',
    '[distortion="-1.848455", orientation=44, skew="0.267152", color=bisque2];',
    '[distortion="1.305594", orientation=75, skew="0.070516", color=orangered];',
    '[sides=20, distortion="-0.641701", orientation=50, skew="-0.952502", color=crimson];',
    '[sides=19, distortion="0.021556", orientation=26, skew="-0.729938", color=darkorange1];',        
    '[sides=16, distortion="0.985153", orientation=33, skew="-0.399752", color=darkolivegreen4];',
    '[sides=17, distortion="-0.687574", orientation=58, skew="-0.180116", color=lightsteelblue1];',
    # --------- incremnt: prop="sides" += 1 or 10 --- and if NOT set, then incmrenet prop="distortion"
    '[sides=29, distortion="0.936354", orientation=28, skew="-0.126818", color=salmon2];',
    '[sides=25, distortion="0.238792", orientation=11, skew="0.995935", color=deepskyblue];',
    '[sides=29, distortion="-0.698271", orientation=22, skew="-0.195492", color=burlywood2];',
    '[sides=27, distortion="0.265084", orientation=26, skew="0.403659", color=gold1];',
    '[sides=28, distortion="0.019636", orientation=79, skew="-0.440424", color=goldenrod2];',
    '[distortion="2.039386", orientation=2, skew="-0.461120", color=greenyellow];',
    '[sides=25, distortion="0.228564", orientation=63, skew="-0.062846", color=darkseagreen];',
    '[distortion="2.624013", orientation=56, skew="0.101396", color=dodgerblue1];',
    '[sides=28, distortion="0.731383", orientation=43, skew="-0.824612", color=thistle2];',
    '[sides=26, distortion="0.592100", orientation=34, skew="-0.719269", color=darkolivegreen3];',
    '[sides=210, distortion="0.298417", orientation=65, skew="0.310367", color=chocolate];',
    '[distortion="-2.997093", orientation=50, skew="-0.061117", color=turquoise3];',
    '[sides=27, distortion="0.878516", orientation=19, skew="0.592905", color=steelblue3];',
    '[sides=30, distortion="-0.960249", orientation=32, skew="0.460424", color=navy];',
    '[sides=30, distortion="-0.633186", orientation=10, skew="0.333125", color=darkseagreen4];',
    '[sides=28, distortion="-0.337997", orientation=52, skew="-0.760726", color=coral];',
    '[sides=27, distortion="0.788483", orientation=39, skew="-0.526284", color=darkolivegreen3];',
    '[sides=27, distortion="0.138690", orientation=55, skew="0.554049", color=coral3];',
    '[sides=27, distortion="-0.010661", orientation=84, skew="0.179249", color=blanchedalmond];',
    '[distortion="-2.239422", orientation=44, skew="0.053841", color=lightskyblue1];',
    '[distortion="-2.843381", orientation=70, skew="-0.601395", color=aquamarine2];',
    '[sides=30, distortion="0.251820", orientation=18, skew="-0.530618", color=lemonchiffon];',
    '[sides=25, distortion="-0.772300", orientation=24, skew="-0.028475", color=darkorange1];',
    '[distortion="-2.226170", orientation=38, skew="0.504053", color=lightyellow1];',
    '[sides=30, distortion="-0.807349", orientation=50, skew="-0.908842", color=darkorchid4];',
    '[sides=30, distortion="-0.030619", orientation=76, skew="0.985021", color=lemonchiffon2];',
    '[distortion="-2.644209", orientation=21, skew="0.307836", color=goldenrod3];',
    '[sides=27, distortion="0.640971", orientation=84, skew="-0.768455", color=cyan];',
    '[distortion="2.758942", orientation=42, skew="0.039886", color=blue];',
    '[sides=29, distortion="-0.348692", orientation=42, skew="0.767058", color=firebrick];',
    '[distortion="2.748625", orientation=74, skew="-0.647656", color=chartreuse4];',
    '[sides=30, distortion="0.851818", orientation=32, skew="-0.020120", color=greenyellow];',
    '[sides=30, distortion="0.992237", orientation=29, skew="0.256102", color=bisque4];',    
    '[sides=26, distortion="0.545461", orientation=16, skew="0.313589", color=mistyrose2];',
    '[sides=29, distortion="-0.267769", orientation=40, skew="0.271226", color=cadetblue1];',
    '[distortion="-2.848455", orientation=44, skew="0.267152", color=bisque2];',
    '[distortion="2.305594", orientation=75, skew="0.070516", color=orangered];',
    '[sides=30, distortion="-0.641701", orientation=50, skew="-0.952502", color=crimson];',
    '[sides=29, distortion="0.021556", orientation=26, skew="-0.729938", color=darkorange1];',        
    '[sides=26, distortion="0.985153", orientation=33, skew="-0.399752", color=darkolivegreen4];',
    '[sides=27, distortion="-0.687574", orientation=58, skew="-0.180116", color=lightsteelblue1];',        
    );

sub __toGraph {
    my ($self, $title, $cutoff_min, $cutoff_max, $arr) = @_;
    #!
    my $result_file = $self->{result_file};
    #!
    my @arr_relations;
    my %hashOf_nodes;
    #! Note: remove/de-activve the "$hashOf_nodes{"root"}" to avodi the data from beign rooted
    $hashOf_nodes{"root"} = 1; #! ie, add a sytneti root.
    # 
    #! ----------------------------
    #!
    my %hashOf_img_relationships; #! which is used to merge differnet 'img' relationshsip for cases where there a 'real-life-node' is mapped to onlyu one img-node, ie, to reduce data-clutter.
    my %hashOf_relationEnd_isHead;
    my %hashOf_relationEnd_isTail;
    #!
    foreach my $line (@{$arr}) {
	chomp($line); #! ie, remove the trail-newline.
	$line =~ s/_/ /g; #! ie, to avoid Latex from complaining.	
	my @arrOf_cols = split("\t", $line);
	#my $l_row_header = shift(@arrOf_cols);
	if($line !~ /left\-child/) { #! then we assume the relatipnship does NOT describe a HCA-relationship
	    next;
	}
	if(scalar(@arrOf_cols) < 2) {
	    croak("!!\t Numer ofColumsn Not as epcted: line: $line");
	}	
	#! -----------------
	#! Input: # node-count	0.000	node-distance	0.000	left-child	Euclid-hca_average	right-child	PearsonProductMoment-k_medoid
	my $prev_col = "";
	my ($head, $tail, $weight) = ("", "", 1.0);
	foreach my $col (@arrOf_cols) {
	    if($prev_col eq "node-distance") {
		$weight = $col;
		if($weight =~ /0\.00/) {
		    $weight = 1.0;
		}
	    } elsif($prev_col eq "left-child") {
		$head = $col;
		$hashOf_nodes{$head} = 1;
		$hashOf_relationEnd_isHead{$head} = 1;
	    } elsif($prev_col eq "right-child") {
		$tail = $col;
		$hashOf_nodes{$tail} = 1;
		$hashOf_relationEnd_isTail{$tail} = 1;
	    }
	    $prev_col = $col;
	}
	if( ($head eq "") || ($tail eq "") ) {
	    croak("!!\t Investigate this case, where head='$head', tail='$tail', given line:\t$line");
	}
	push(@arr_relations, [$head, $tail, $weight]); #"\"$head\" -> \"$tail\" [weight=$weight];");
	#push(@arr_relations, "\"$head\" -> \"$tail\" [weight=$weight];");
	#!
	#!
	{ #! Update number ofm relationshsips assotied to the img-vertices.
	    for my $v ($head, $tail) {
		if($v =~ /img/i) {
		    if(!defined($hashOf_img_relationships{$v})) {
			$hashOf_img_relationships{$v} = 0;
		    }
		    $hashOf_img_relationships{$v}++;
		}
	    }
	}
    }
    

    #my $font_size = 36;
    my $font_size = 66;	    
    #! ------------------------------------------------------------------------
    my $result_file_tmp = "temp.dot"; #! ie, an itnermediary dot-file
    open(FILE_OUT, ">$result_file_tmp") or die("!!\t input(file): An error in opning file=\"$result_file_tmp\"");	    
    #! Write out header:
    #label = "\n\n\n\n$title",
    print( FILE_OUT qq(digraph "unix" {
    graph [fontname = "Helvetica-Oblique",
	   fontsize = $font_size,
	   label = "\n\n$title",
	   size = "12,12" ];
    node [shape = polygon,
	  sides = 4,
	  distortion = "0.0",
	  orientation = "0.0",
	  skew = "0.0",
	  color = white,
	  style = filled,
	  fontname = "Helvetica-Outline" ];
   ));
    #!
    #! Specify the layout of the ndoes:
    my $regex_group_vertex = $self->{regex_group_vertex};
    { my $key; my $value;
      my $index = 0;
      while (($key, $value) = each (%hashOf_nodes)) {
	  #!
	  my $group_color = undef;
	  if($key eq "root") {
	      $group_color = $color_node_root;
	  } elsif($key =~ /img/) {
	      if($hashOf_img_relationships{$key} == 1)  { #! then the img-relationship is used by only a singular relationship
		  $key = "img"; #! ie, remo the specity of the mapping
	      } 
	      $group_color = $color_node_img;
	  } else {
	      my ($group_id) = ($key =~ /$regex_group_vertex/);
	      if(!defined($group_id)) {
		  #if($key =~ /img/) {
		  #$group_color = $color_node_img;
		  #$group_color = '[color=lightsteelblue1,fontsize=16,style=filled];';
		  #} else {
		  croak("!!\t Add supprot for case where there are Not any well-defined group for key=\"$key\", given regex=\"$regex_group_vertex\".");
		  #}
	      } else { #! then a string-case:
		  $group_color = $hashOf_group{$group_id};
	      }
	      if(!defined($group_color)) { #! then we update the color:
		  my $arr_colors_size = scalar(@arr_colors);
		  if($index > $arr_colors_size) {
		      croak("!!\t Add supprot for case where there are index=(cntIndex.$index > max:$arr_colors_size), given group=\"$group_id\"");
		  }
		  $hashOf_group{$group_id} = $group_color = $arr_colors[$index];	      
		  $index += 1;
	      }
	  }
	  #!
	  print( FILE_OUT '"' . $key . '"' . $group_color . "\n");
	  #!
	  { #! Add relationshisp to 'root' for cases where a 'tail' is not a 'head' of any other relationships:
	      if(defined($hashOf_nodes{"root"}) && !defined($hashOf_relationEnd_isHead{$key})) {
		  my $head = $key;
		  my $tail = "root";
		  my $weight = 1.0;
		  push(@arr_relations, [$head, $tail, $weight]); #"\"$head\" -> \"$tail\" [weight=$weight];");
	      }
	  }
      }
    }
    
    #!
    #! Write out the relationships:
    foreach my $row (@arr_relations) {
	#!
	#!
	my ($head, $tail, $weight) = @{$row}; #! ie, fethc the values.
	#!
	#! Rduece clutter from the visu:
	if($head =~ /img/) {
	    if($hashOf_img_relationships{$head} == 1)  { #! then the img-relationship is used by only a singular relationship
		$head = "img"; #! ie, remo the specity of the mapping
	    }
	}
	if($tail =~ /img/) {
	    if($hashOf_img_relationships{$tail} == 1)  { #! then the img-relationship is used by only a singular relationship
		$tail = "img"; #! ie, remo the specity of the mapping
	    }
	}	
	#!
	#!
	my $value =  "\"$head\" -> \"$tail\" [weight=$weight];";
	print(FILE_OUT $value . "\n");
    }
    print(FILE_OUT "}\n");
    #!
    close(FILE_OUT);
    #!
    #! Convert to a graph:
    my $cmd = "dot -Tpdf $result_file_tmp -o $result_file > tmp-dot-result.txt";
    #printf("\tapply\t $cmd");
    system($cmd);
    { #! then write out Latex-help-information:
	print(STDOUT '\includegraphics[trim = 10mm 10mm 10mm 10mm, clip,keepaspectratio=true,width=\linewidth]{../hpLysis-handling-of-high-diemensionality/dot-figs/' . $result_file . '}' . "\n");
    }
}



sub  module_dotTree_resultFileToTree {
    my ($self, $title, $cutoff_min, $cutoff_max, $arr_file_name, $max_lineCount, $max_colCount, $regex_row) = @_;

    my $arr = [];
    my $file_index = 0;
    foreach my $file_name (@{$arr_file_name}) {
	open(FILE_IN, "<$file_name") or die("Unable to open the input-file $file_name\n");
	my $row_prefix = "file-" . $file_index;
	if($file_name =~ /\-n(\d+)-/) {
	    $row_prefix = "dim=" . $1 . ",";
	} elsif($file_name =~/postCCM-randomDistributions-features_img/) {
	    $row_prefix = "clusterIDs" . ",";
	}
	if($file_name =~ /\-n\d+\-\-([a-zA-Z]+)-corrDeviation/) { 
	    $row_prefix .=  "" . $1 . ", "; #! eg, PearsonProductMoment
	}	
	my $cnt_lines = 0;
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.
	    if($cnt_lines == 0) {
		$cnt_lines += 1;
		next; #! ie, as we asusmet the file-row-header si NOT to be used in the heatmpa-generaiotn (we in this script cconstruct).
	    }
	    if($cnt_lines < $max_lineCount) {
		if(!defined($regex_row) || ( $line =~ /$regex_row/) || (scalar(@{$arr}) == 0) ) {
		    if($row_prefix ne "") { #! then increase the acucryacy of the row-name
			$line = $row_prefix . $line;
		    }
		    #!
		    #!
		    my @arrOf_cols = split("\t", $line);
		    my @arrOf_cols_to_use = ();
		    for(my $i = 0; $i < scalar(@arrOf_cols); $i++) {
			if( !defined($max_colCount) || ($i < $max_colCount) ) {
			    push(@arrOf_cols_to_use, $arrOf_cols[$i]);
			}
		    }
		    #!
		    #!
		    # print "add-line\t" . $line . "\n";
		    push(@{$arr}, join("\t", @arrOf_cols_to_use));
		    $cnt_lines += 1;
		}
	    }
	}
	close(FILE_IN);
	$file_index += 1;
    }
    #! Costruct the Latex-table:
    __toGraph($self, $title, $cutoff_min, $cutoff_max, $arr);
}

sub module_dotTree_exportAllFiles {
    my ($self, $arr_file_name, $regex_row, $folder_path) = @_;
    my $cutoff_min = -1.0;  my $cutoff_max = 400.0;
    my $max_lineCount = 10000;    my $max_rowCount = 50; my $max_colCount = 1000000;
    system("mkdir -p $folder_path"); #! ie, to ensure that this path acutally exists. 
    if($folder_path =~ /"(.+)\/$/) {
	$folder_path =  $1; #! ie, then remove the '/' suffix.
    }
    #!
    #!
    foreach my $o (@{$arr_file_name}) {
	my $title = $o->{title};
	my $file_name = $o->{path};
	#my $dim = ""; my $metric = ""; my $alg = "";
	#($dim, $metric, $alg) = ($file_name_xmtPath =~ /\-n(\d+)\-\-([a-z]+)\-\-([a-z]+)_hca/i);
	#my $title = "Dimension=$dim, metric=$metric, algorithm=$alg";
	my @arr_comp = split(/\//, $file_name);
	my $result_file = $file_name;
	if(scalar(@arr_comp)) {$result_file = $arr_comp[scalar(@arr_comp)-1];} #! ie, the last component in the file-path.
	$result_file =~ s/\.tsv/\.pdf/;
	$result_file  = $folder_path . "/" . $result_file;
	#my $file_name = $folder_path . "/" . $file_name_xmtPath;	
	#!
	#!
	$self->{result_file} = $result_file;
	# printf("\t (module-dotTree)\t 
	module_dotTree_resultFileToTree($self, $title, $cutoff_min, $cutoff_max, [$file_name], $max_lineCount, $max_colCount, $regex_row);
    }
}


sub module_dotTree_exportAllFiles_splitOnRegex {
    my ($arr_file_name, $regex_row, $folder_path, $regex) = @_;
    my $cutoff_min = -1.0;  my $cutoff_max = 400.0;
    my $max_lineCount = 10000;    my $max_rowCount = 50; my $max_colCount = 1000000;
    system("mkdir -p $folder_path"); #! ie, to ensure that this path acutally exists. 
    if($folder_path =~ /"(.+)\/$/) {
	$folder_path =  $1; #! ie, then remove the '/' suffix.
    }
    #!
    #!
    foreach my $file_name_xmtPath (@{$arr_file_name}) {
	my $dim = ""; my $metric = ""; my $alg = "";
	($dim, $metric, $alg) = ($file_name_xmtPath =~ /$regex/i);
	my $title = "Dimension=$dim, metric=$metric, algorithm=$alg";
	my $result_file = $file_name_xmtPath;	
	$result_file =~ s/\.tsv/\.pdf/;
	$result_file  = $folder_path . "-" . $result_file;
	my $file_name = $folder_path . "/" . $file_name_xmtPath;	
	#!
	#!
	my $self = module_dotTree::init_self($result_file);
	# printf("\t (module-dotTree)\t 
	module_dotTree_resultFileToTree($self, $title, $cutoff_min, $cutoff_max, [$file_name], $max_lineCount, $max_colCount, $regex_row);
    }
}


# --------------------------------
1; 
