=head
@brief provides logiccs for construiton of scatter-maps.
=cut
package module_scatter;
use strict;
use Carp;
# -------------------------------------------

sub init_self {
    my ($result_file) = @_;
    return {
	result_file => $result_file,
	#! where 'this' si a temrpaory-strut, to be nsued for future code-modivcaitons.
    };
}

sub _score_isOf_interest{
    #my ($line, $col) = @_; #, $cutoff_min, $cutoff_max) = @_;
    my ($line, $col, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) = @_;
    my $isTo_drop = 0;
    if ($col =~ /^-?\d+\.?\d*$/) { if($col > 10000000) { $isTo_drop = 1; } }
    if( ($col eq "inf") || ($col eq "4294967296.000000") || ($col eq "nan") || ($col eq "-nan") || ($col eq "-")) {$isTo_drop = 1;}
    if($isTo_drop == 0) {
	if ($col !~ /^-?\d+\.?\d*$/) { 
	    croak("!!\t : val=\"$col\" is NOT a number, given line=\"$line\".");
	} else {
	    if($col < $cutoff_min) { $isTo_drop = 1; }
	    if($col > $cutoff_max) { $isTo_drop = 1; }
	    if( ($col > -0.001) &&  ($col < 0.001) ) {
		# print("Drop\t score:$col\n");
		$isTo_drop = 1;  #! ie, as we assuemt eh valeu is set to '0' ... ie, to get emanignful differences in repdcitions.
	    }
	}
    }
    #if(1 == 2) { # FIXME: remove.
    if($isTo_drop == 0) {
	if(defined($arr_scoreIndex_toUse) && scalar(@{$arr_scoreIndex_toUse}) ) { #! then we investigate if this column is to be used in the result-filter.
	    my $is_found = 0;
	    foreach my $ind (@{$arr_scoreIndex_toUse}) {
		if($ind == $score_index) {$is_found = 1;}
	    }
	    if($is_found == 0) {$isTo_drop = 1;}
	}
    }
    return ($isTo_drop == 0);
}

sub _pair_isOf_interest {
    my ($line, $col_1, $col_2, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) = @_;
    return (
	_score_isOf_interest($line, $col_1, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) &&
	_score_isOf_interest($line, $col_2, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $score_index) );
}

#! Construct a Scatterplot, where variable=1 is along x-axis, while variable=" is a long the y-axis; the rows are (depicted through) the symbols used in scatter-plot.
sub scatterPlot_typeOfStyle_heatMap {
    my ($self, $title, $ylabel, $xlabel, $arr_1, $arr_2, $arr_z, $cutoff_min,  $cutoff_max, $arr_scoreIndex_toUse) = @_;
    #!
    #!
    my $result_file = $self->{result_file};
    $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
    if($result_file !~ /\.tex$/) {
	$result_file .= ".tex"; #! ie, then add a file-ending
    }
    $xlabel =~ s/[_$:\$|\| \t\(\)& ]/ /g;
    $ylabel =~ s/[_$:\$|\| \t\(\)& ]/ /g;
    #!
    my $str_data = "";
    #!
    {#! Remember the parsed scores --- eg, to be used when cosntructing bar-chart in "sub _exportHistoCountBucket_BarChart(...)"    
	my @row_resultScores = ();
	for(my $i = 0; $i < scalar(@{$arr_1}); $i++)  {
	#foreach my $p_obj (@{$arr_1}) {
	    my $s1 = $arr_1->[$i];
	    my $s2 = $arr_2->[$i];
	    my $z  = $arr_z->[$i];		
	    my $line = "<tmp>";
	    if(_pair_isOf_interest($line, $s1, $s2, $cutoff_min, $cutoff_max, $arr_scoreIndex_toUse, $i)) {
		# $str_rows .=  $s1 . $sep_col . $s2 . $sep_col . $id_row . $sep_row . "\n";
		#!
		push(@row_resultScores, join(",", ($s1, $s2, $z))); #! ie, as we asume they are identifal % TODO: update this based on aboe "$s1 != $s2" test.
	    } else {
		; # printf("\t\t drop(score)\t %.2f\t %.2f\n", $s1, $s2);
	    }
	}
	if(scalar(@row_resultScores) == 0) {
	    printf("\t(drop\tscatter-module)\t %s\n", $result_file);
	    return;} #! ie, as no itneresting valeues were idneitfed.
	#!
	#! Compute the data-string:
	$str_data = join("\n", @row_resultScores);
    }
    

    #!
    #!
    open(FILE_OUT, ">$result_file") or die("Unable to open the result-file $result_file\n");    
    printf("\t(scatter-module)\t %s\n", $result_file);
    #!
    # print("\t Str(rows)=" . $str_rows);
    #my $str_header_internal = join(",\n", @arr_str_header_internal);
    #my $str_legend          = join(",",  @arr_str_header_seenByUser);
    #!    
    print(FILE_OUT qq(
%! Title: $title 
\\documentclass[border=9]{standalone}
\\usepackage[table,x11names,usenames, dvipsnames]{xcolor}
\\usepackage{pgfplots,filecontents}
\\begin{filecontents*}{data.dat}
  % -------------------------
  % example-data:
  % 0.09298303885566861,1976.677756339018,1.360252917788105
  % 0.05630059591218889,3608.903320218602,-12.0558030777737
  % -------------------------
  $str_data
\\end{filecontents*}
\\begin{document}
\\begin{tikzpicture}
  \\begin{axis}[colorbar,
      ylabel={ $ylabel },
      xlabel={ $xlabel },
]
        \\addplot[scatter, only marks] table [x index=0, y index=1,
          scatter src=\\thisrowno{2}, col sep=comma] {data.dat};
  \\end{axis}
\\end{tikzpicture}
\\end{document}
	  ));
    close(FILE_OUT);
    {
        $result_file =~ s/[$:\$|\| \t\(\)& ]//g;
	my $cmd = "pdflatex -halt-on-error $result_file 1>out_latex.txt";
	printf("\t(scatter-compile)\t %s\n", $cmd);
	system($cmd); #! ie, to simplfiy viwing of the data
	if(!(-f $result_file)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	my $file_fig = $result_file;
	$file_fig =~ s/\.tex/\.pdf/;
	if(!(-f $file_fig)) {	    printf(STDERR "!!\t For file=\t$result_file \t Was Not able to generate a latex-files; please investigate\n"); 	}
	else {
	    return $file_fig;
	}
    }
    
}

#! ****************************************************** EOF:
1;
