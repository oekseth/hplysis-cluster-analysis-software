/* #ifndef metricMacro_fidelity_h */
/* #define metricMacro_fidelity_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_fidelity
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "fidelity".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics

//! @return the correlations-core for: fidelity and "Bhattacharyya" and "hellinger and "Matusita"
//      printf("mult=%f given term1=%f and term2=%f, where T_FLOAT_MAX=%f, at %s:%d\n", mult, term1, term2, T_FLOAT_MAX, __FILE__, __LINE__); 
//      assert_possibleOverhead_testisNot_inf(input_result); 
#define correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord(term1, term2) ({ \
  const t_float mult = term1 * term2;					\
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term1 != T_FLOAT_MAX); \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term2 != T_FLOAT_MAX); \
  const t_float input_result = mathLib_float_sqrt_abs(mult);		\
  input_result; }) //! ie, return.

#define correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE(term1, term2) ({ \
    const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(term1, term2);	\
    const VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_SQRT_abs(mult);		\
    input_result; }) //! ie, return.

#define correlation_macros__distanceMeasures__fidelity__Bhattacharyya__postProcess(inp_value) ({ \
  assert_possibleOverhead(inp_value != T_FLOAT_MAX); \
  const t_float input_result = (inp_value > 0) ? mathLib_float_log(inp_value) : metricMacro__constants__defaultValue__postProcess__allMatches; \
  input_result; })
#define correlation_macros__distanceMeasures__fidelity__Hellinger__postProcess(inp_value) ({ \
      const t_float input_result = (inp_value > 0) ? 2*mathLib_float_sqrt_abs(1 - inp_value) : metricMacro__constants__defaultValue__postProcess__allMatches; \
      input_result; })
//      const t_float input_result = (inp_value != 0) ? mathLib_float_sqrt(2 - 2*mathLib_float_sqrt(inp_value)) : metricMacro__constants__defaultValue__postProcess__allMatches; 
#define correlation_macros__distanceMeasures__fidelity__Matusita__postProcess(inp_value) ({ \
      assert_possibleOverhead(inp_value != T_FLOAT_MAX);		\
      const t_float input_result = (inp_value != 0) ? mathLib_float_sqrt_abs(2 - 2*inp_value) : metricMacro__constants__defaultValue__postProcess__allMatches; \
  input_result; })

//! @return the correlations-core for: "squared chord"
#define correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord(term1, term2) ({ \
      const t_float sq_1 = mathLib_float_sqrt_abs(term1);			\
      const t_float sq_2 = mathLib_float_sqrt_abs(term2);			\
      const t_float diff = sq_1 - sq_2; \
      const t_float input_result = diff * diff; \
      input_result; }) //! ie, return.
#define correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE(term1, term2) ({ \
    const VECTOR_FLOAT_TYPE sq_1 = VECTOR_FLOAT_SQRT(term1);	\
    const VECTOR_FLOAT_TYPE sq_2 = VECTOR_FLOAT_SQRT(term2);	\
    const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(sq_1, sq_2);	\
    const VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_MUL(diff, diff);	\
      input_result; }) //! ie, return.

#define correlation_macros__distanceMeasures__fidelity__Hellinger_altDef__postProcess(value) ({ \
      const t_float input_result = metric_macro_defaultFunctions__postProcess__validateEmptyCase(mathLib_float_sqrt(2*value)); \
  input_result; })
#define correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess(value) ({ metric_macro_defaultFunctions__postProcess__none(value);})
// 


//! @return the correlations-core for: "squared chord (inversed)"
#define correlation_macros__distanceMeasures__fidelity__squaredChord_altDef__postProcess(inp_value) ({ \
      assert_possibleOverhead(inp_value != T_FLOAT_MAX);		\
      t_float input_result = 2*inp_value - 1;					\
      metric_macro_defaultFunctions__postProcess__validateEmptyCase(input_result); }) //! ie, return
/* #define correlation_macros__distanceMeasures__fidelity__squaredChord_altDef__postProcess(value) ({ \ */
  
/* }) */


// #endif //! EOF
