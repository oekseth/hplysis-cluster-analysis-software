#include "measure_kt_clusterAlg_fixed.h"
#include "measure_base.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure.h"
#include "maskAllocate.h"
#include "kt_clusterAlg_fixed.h"
#include "config_nonRank_each.h"
#include "kt_distance.h"

/* static const uint kilo = 1000; */
/* static clock_t clock_time_start; static clock_t clock_time_end; */
/* static struct tms tms_start; */
/* static  struct tms tms_end; */
/**
   @brief Start the measurement with regard to time.
   @remarks 
   - Uses the global vairalbe in this file for this prupose.
   - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
**/
/* static  void start_time_measurement() { */
/* #ifndef NDEBUG */
/*   //! Clears the cache by allocating 100MB of memory- and the de-allocating it: */
/*   // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses. */
/*   uint size = 1024*1024*100; */
/*   char *tmp = new char[size]; */
/*   assert(tmp); */
/*   memset(tmp, 7, size); */
/*   delete [] tmp; tmp = NULL; size = 0; */
/* #endif */
/*   tms_start = tms(); */
/*   clock_time_start = times(&tms_start); */
/*   // if(() == -1) // starting values */
/*   //   err_sys("times error"); */
/* } */
/* /\** */
/*    @brief Ends the measurement with regard to time. */
/*    @param <msg> If string is given, print the status information to stdout. */
/*    @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called. */
/*    @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called. */
/*    @return the clock tics on the system since the 'start_time_measurement()' method was called. */
/* **\/ */
/* static float end_time_measurement(const char *msg, float &user_time, float &system_time, const float prev_time_inSeconds) { */
/*   clock_time_end = times(&tms_end);  */
/*   long clktck = 0; clktck =  sysconf(_SC_CLK_TCK); */
/*   const clock_t t_real = clock_time_end - clock_time_start; */
/*   const float time_in_seconds = t_real/(float)clktck; */
/*   const float diff_user = tms_end.tms_utime - tms_start.tms_utime; */
/*   if(diff_user) user_time   = (diff_user)/((float)clktck); */
/*   const float diff_system = tms_end.tms_stime - tms_start.tms_stime; */
/*   system_time = diff_system/((float)clktck); */
/*   if(msg) { */
/*     // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__); */
/*     if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) { */
/*       printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg); */
/*     } else { */
/*       //float diff = prev_time_inSeconds / time_in_seconds; */
/*       float diff = time_in_seconds / prev_time_inSeconds; */
/*       // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__); */
/*       printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg); */
/*     } */
/*   } */
/*   //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg); */
/*   return (float)time_in_seconds; */
/* } */


/* /\** */
/*    @brief Ends the measurement with regard to time. */
/*    @return the clock tics on the system since the 'start_time_measurement()' method was called. */
/* **\/ */
/* static float end_time_measurement(const char *msg, const float prev_time_inSeconds) { */
/*   float user_time = 0, system_time=0; */
/*   return end_time_measurement(msg, user_time, system_time, prev_time_inSeconds); */
/* } */



/* //! Ends the time measurement for the array: */
/* static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) { */
/*   assert(stringOf_measureText); */
/*   assert(strlen(stringOf_measureText)); */
/*   //! Get knowledge of the memory-intiaition/allocation pattern: */
/*   { */
/*     //! Provides an identficator for the string: */
/*     char *string = new char[1000]; assert(string); memset(string, '\0', 1000); */
/*     sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);   */
/*     //! Prints the data: */
/*     //! Complte the measurement: */
/*     const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds); */
/*     //time.print_formatted_result(string);  */
/*     delete [] string; string = NULL;  */
    
/*     return time_in_seconds; */
/*   }  */
/* } */


static void __allcoate_clusterData(const uint ncols, const uint nrows, const uint nclusters, const uint transpose, t_float ***cdata, char ***cmask, const bool isTo_use_continousSTripsOf_memory) {
  
  /* const uint size_x = /\*nrows=*\/(transpose) ? max(ncols, nrows) : nclusters; */
  /* const uint size_y = /\*ncols=*\/(transpose) ? nclusters : max(ncols, nrows); */
  /* const uint size_x = /\*nrows=*\/(transpose) ? nrows : nclusters; */
  /* const uint size_y = /\*ncols=*\/(transpose) ? nclusters : nrows; //max(ncols, nrows); */
  uint size_x = nrows; uint size_y = nclusters;
  //  uint size_x = nrows; uint size_y = ncols;
  if(transpose == 0) {
    size_x = nclusters;   size_y = ncols;
    //size_x = ncols;   size_y = nrows;
  }
  /* const uint size_x = /\*nrows=*\/(transpose) ? nrows : ncols; */
  /* const uint size_y = /\*ncols=*\/(transpose) ? ncols : nrows; //max(ncols, nrows); */
  //printf("transpose=%u, size_x=%u, size_y=%u, (given nrows=%u, ncols=%u, nclusters=%u), given transpose='%s', at %s:%d\n", transpose, size_x, size_y, nrows, ncols, nclusters, (transpose == 1) ? "yes" : "no", __FILE__, __LINE__);
  maskAllocate__makedatamask(size_x, size_y, cdata, cmask, isTo_use_continousSTripsOf_memory);
}

//! Test singficnace of using iterative memory-access VS partial-random memory-access: compare "getclustermedians(...)" procedure with "getclustermeans(..)"
static void measure_finding_median() {

  const bool print_info = true;

  // FIXME: updarte doccuemtnation wrt. resutls of this evaluation.

  // FIXME: write a 'dynamic test' were we evlauate different size-combinaitons of "nrows" and "ncols"
  const uint size_of_array = 20; const uint nrows = size_of_array; const uint ncols = 20; 
  //const uint size_of_array = 200; const uint nrows = size_of_array; const uint ncols = 200; 
  //  const uint size_of_array = 1000; const uint nrows = size_of_array; const uint ncols = 100; 
  

  t_float **distmatrix = NULL; char **mask = NULL;
  //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
  const bool isTo_use_continousSTripsOf_memory = true;
  maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
    
  printf("mask=%p, nrows=%u, ncols=%u, at %s:%d\n", mask, nrows, ncols, __FILE__, __LINE__);

  { //! a dummy approach to test for erorrs.
    char sum = 0;
    for(uint i = 0; i < nrows; i++) {     for(uint j = 0; j < ncols; j++) { sum += mask[i][j];}}
  }

  // FIXME: in [”elow] conisder to test also for clsuters of size_of_array/2
  const uint arrOf_cluster_counts_size = 4; const uint arrOf_cluster_counts[arrOf_cluster_counts_size] = {10, 50, 100, 200};
  for(uint cluster_count_index = 0; cluster_count_index < arrOf_cluster_counts_size; cluster_count_index++) {
    const uint nclusters = arrOf_cluster_counts[cluster_count_index];    
    uint *mapOf_clusterId = new uint[nrows];

    { //! Test the effect of random/non-ordered clsuter-memberships:
      { //! Specify the cluster-ids:      
	for(uint i = 0; i < nrows; i++) {
	  mapOf_clusterId[i] = (uint)(rand() % nclusters); //! a random assignment.
	}
      }
      { //! Method using "means": two for-loops (to 'inspect' all of the cells), and one 'random' access to the mapOf_clusterId array. 
	for(uint transpose = 0; transpose < 2; transpose++) {
	  if(transpose && (nclusters >= ncols)) {continue;} //! ie, to avoid a pointless call.
	  if(!transpose && (nclusters >= nrows)) {continue;} //! ie, to avoid a pointless call.

	  //const uint size_x = (transpose==0) ? nrows : ncols; 	  const uint size_y = (transpose==0) ? ncols : nrows;
	  //const uint size_x = (transpose==0) ? nrows : ncols; 	  const uint size_y = (transpose==0) ? ncols : nrows;
	  uint size_x = nclusters; uint size_y = ncols;
	  if(transpose == 1) {
	    size_x = nrows; size_y = nclusters;
	  }
	  const t_float empty_value = 0;
	  t_float **cmask_tmp = allocate_2d_list_float(size_x, size_y, empty_value);
	  t_float **cdata = NULL; char **cmask = NULL;
	  printf("\n\nallocates, at %s:%d\n", __FILE__, __LINE__);
	  __allcoate_clusterData(ncols, nrows, nclusters, transpose, &cdata, &cmask, isTo_use_continousSTripsOf_memory);

  /* printf("mask=%p, nrows=%u, ncols=%u, at %s:%d\n", mask, nrows, ncols, __FILE__, __LINE__); */

  /* { //! a dummy approach to test for erorrs. */
  /*   char sum = 0; */
  /*   for(uint i = 0; i < nrows; i++) {     for(uint j = 0; j < ncols; j++) { sum += mask[i][j];}} */
  /* } */

	  

	  //! -------------------------------
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'


	  //class measure time = measure(); time.start_time_measurement();	    
	  for(uint i = 0; i < nrows; i++) {
	    for(uint k = 0; k < ncols; k++) {
	      const bool isTo_copmuteResults_optimal = false; const bool mayUse_zeroAs_emptySign_inComputations = false; const bool needTo_useMask_evaluation = false;
	      getclustermeans__extensiveInputParams(nclusters, nrows, ncols, distmatrix, mask, mapOf_clusterId, cdata, cmask, cmask_tmp, /*transpose=*/transpose, isTo_copmuteResults_optimal, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	    }
	  }
	  __assertClass_generateResultsOf_timeMeasurements("centroid-computation::init-random::mean, isTo_copmuteResults_optimal = false; const bool mayUse_zeroAs_emptySign_inComputations = false; const bool needTo_useMask_evaluation = false;", nrows, ncols); //, transpose, time, NULL);
	  free_2d_list_float(&cmask_tmp, size_x);
	  //! De-allocate locally reseerved memory:
	  maskAllocate__freedatamask(/*nelements=*/size_of_array, cdata, cmask, isTo_use_continousSTripsOf_memory);    
	}
      }
      { //! Evalaute the different method used for "medians": two for-loops (to 'inspect' all of the cells) and one for-loop o 'investigate' memberships in clusters.
	// FIXME: fi 'this' is consdierably slower, then consider to 'drop' the intermediate "arrOf_counts" array ... update the code ... and re-test ... usign the 'then old code' as en example-code' of a 'fialed' optimization.
	for(uint transpose = 0; transpose < 2; transpose++) {
	  if(transpose && (nclusters >= ncols)) {continue;} //! ie, to avoid a pointless call.
	  if(!transpose && (nclusters >= nrows)) {continue;} //! ie, to avoid a pointless call.
	  uint size_x = nclusters; uint size_y = ncols;   if(transpose == 1) { size_x = nrows; size_y = nclusters;  }
	  /* const uint size_x = (transpose==0) ? nrows : ncols; */
	  /* const uint size_y = (transpose==0) ? ncols : nrows; */
	  const t_float empty_value = 0;
	  t_float **cmask_tmp = allocate_2d_list_float(size_x, size_y, empty_value);
	  t_float **cdata = NULL; char **cmask = NULL;
	  __allcoate_clusterData(ncols, nrows, nclusters, transpose, &cdata, &cmask, isTo_use_continousSTripsOf_memory);

	  //t_float **cmask_tmp = allocate_2d_list_float((transpose==0) ? nrows : ncols, (transpose==0) ? ncols : nrows, /*empty-value=*/0);
	  //! -------------------------------
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'

	  //class measure time = measure(); time.start_time_measurement();	    
	  for(uint i = 0; i < nrows; i++) {
	    for(uint k = 0; k < ncols; k++) {
	      const bool isTo_copmuteResults_optimal = false; const bool mayUse_zeroAs_emptySign_inComputations = false; const bool needTo_useMask_evaluation = false; t_float **arrOf_valuesFor_clusterId_global = NULL;
	      getclustermedians__extensiveInputParams(nclusters, nrows, ncols, distmatrix, mask, mapOf_clusterId, cdata, cmask, cmask_tmp, /*transpose=*/transpose, isTo_copmuteResults_optimal, arrOf_valuesFor_clusterId_global, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	    }
	  }
	  __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration("centroid-computation::init-random::median, const bool isTo_copmuteResults_optimal = false; const bool mayUse_zeroAs_emptySign_inComputations = false; const bool needTo_useMask_evaluation = false; t_float **arrOf_valuesFor_clusterId_global = NULL;", nrows, ncols, nclusters, transpose); //, time, NULL);
	  free_2d_list_float(&cmask_tmp, size_x);
	  //! De-allocate locally reseerved memory:
	  maskAllocate__freedatamask(/*nelements=*/size_of_array, cdata, cmask, isTo_use_continousSTripsOf_memory);    
	}
      }
    }
    { //! Test the effect of 'seuental' clsuter-memberships:
      { //! Idnetify the clsuter-memberships:
	uint cluster_id = 0; uint cnt_inClusters = 0;
	//assert(nrows >= nclusters);
	uint cluster_threshold = nrows / nclusters;	  
	for(uint i = 0; i < nrows; i++) {
	  mapOf_clusterId[i] = cluster_id; cnt_inClusters++;
	  if(cnt_inClusters >= cluster_threshold) { //! then we 'go to the next cluster':
	    cnt_inClusters = 0; cluster_id++;
	  }	    
	}
      }
      { //! Method using "means": two for-loops (to 'inspect' all of the cells), and one 'random' access to the mapOf_clusterId array. 

	//class measure time = measure(); time.start_time_measurement();	    
	for(uint transpose = 0; transpose < 2; transpose++) {
	  if(transpose && (nclusters >= ncols)) {continue;} //! ie, to avoid a pointless call.
	  if(!transpose && (nclusters >= nrows)) {continue;} //! ie, to avoid a pointless call.
	  //! -------------------------------	 
	  uint size_x = nclusters; uint size_y = ncols;   if(transpose == 1) { size_x = nrows; size_y = nclusters;  }
	  const t_float empty_value = 0;
	  t_float **cmask_tmp = allocate_2d_list_float(size_x, size_y, empty_value);
	  t_float **cdata = NULL; char **cmask = NULL;
	  printf("\n\nallocates, at %s:%d\n", __FILE__, __LINE__);
	  __allcoate_clusterData(ncols, nrows, nclusters, transpose, &cdata, &cmask, isTo_use_continousSTripsOf_memory);


	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  

	  for(uint i = 0; i < nrows; i++) {
	    for(uint k = 0; k < ncols; k++) {
	      // assert(false); // FIXME: the perofmrance-test ... evlauating signciance of [”elow] arpaemters	      
	      //t_float **cmask_tmp = NULL;
	      const bool isTo_copmuteResults_optimal = true; const bool mayUse_zeroAs_emptySign_inComputations = true; const bool needTo_useMask_evaluation = false;
	      assert(cmask_tmp);
	      //printf("start, at %s:%d\n", __FILE__, __LINE__);
	      getclustermeans__extensiveInputParams(nclusters, nrows, nclusters, distmatrix, mask, mapOf_clusterId, cdata, cmask, cmask_tmp, /*transpose=*/transpose, isTo_copmuteResults_optimal, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	      //printf("compl, at %s:%d\n", __FILE__, __LINE__);
	    }
	  }
	  __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration("centroid-computation::init-fixed::mean", nrows, ncols, nclusters, transpose); //, time, NULL);
	  //! De-allocate locally reseerved memory:
	  maskAllocate__freedatamask(/*nelements=*/size_of_array, cdata, cmask, isTo_use_continousSTripsOf_memory);    
	  free_2d_list_float(&cmask_tmp, size_x); cmask_tmp = NULL;
	}
      }
      { //! Evalaute the different method used for "medians": two for-loops (to 'inspect' all of the cells) and one for-loop o 'investigate' memberships in clusters.
	// FIXME: fi 'this' is consdierably slower, then consider to 'drop' the intermediate "arrOf_counts" array ... update the code ... and re-test ... usign the 'then old code' as en example-code' of a 'fialed' optimization.

	//class measure time = measure(); time.start_time_measurement();	    
	for(uint transpose = 0; transpose < 2; transpose++) {
	  if(transpose && (nclusters >= ncols)) {continue;} //! ie, to avoid a pointless call.
	  if(!transpose && (nclusters >= nrows)) {continue;} //! ie, to avoid a pointless call.
	  //! -------------------------------	 
	  uint size_x = nclusters; uint size_y = ncols;   if(transpose == 1) { size_x = nrows; size_y = nclusters;  }
	  const t_float empty_value = 0;
	  t_float **cmask_tmp = allocate_2d_list_float(size_x, size_y, empty_value);
	  t_float **cdata = NULL; char **cmask = NULL;
	  printf("\n\nallocates, at %s:%d\n", __FILE__, __LINE__);
	  __allcoate_clusterData(ncols, nrows, nclusters, transpose, &cdata, &cmask, isTo_use_continousSTripsOf_memory);

	  //! -------------------------------
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  
	  

	  for(uint i = 0; i < nrows; i++) {
	    for(uint k = 0; k < ncols; k++) {
	      // assert(false); // FIXME: the perofmrance-test ... evlauating signciance of [”elow] arpaemters
	      //t_float **cmask_tmp = NULL;
	      assert(cmask_tmp);

	      t_float **arrOf_valuesFor_clusterId_global = NULL;
	      const bool isTo_copmuteResults_optimal = true; const bool mayUse_zeroAs_emptySign_inComputations = true; const bool needTo_useMask_evaluation = false;
	      getclustermedians__extensiveInputParams(nclusters, nrows, nclusters, distmatrix, mask, mapOf_clusterId, cdata, cmask, cmask_tmp, /*transpose=*/transpose, isTo_copmuteResults_optimal, arrOf_valuesFor_clusterId_global, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	    }
	  }	  
	  __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration("centroid-computation::init-fixed::median", nrows, ncols, nclusters, transpose); //size_of_array, nclusters, transpose, time, NULL);
	  //! De-allocate locally reseerved memory:
	  maskAllocate__freedatamask(/*nelements=*/size_of_array, cdata, cmask, isTo_use_continousSTripsOf_memory);    
	  free_2d_list_float(&cmask_tmp, size_x); cmask_tmp = NULL;
	}
      }
    }
    delete [] mapOf_clusterId; mapOf_clusterId = NULL;
  }
  //! De-allocate locally reseerved memory:
  maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
}

/**
   @brief the system test for class rule_set.
   @param <print_info> if set a limited status information is printed to STDOUT.
   @return true if test completed.
**/
bool assert_class__kt__clusterAlg_fixed() {
  
  


  
  // FIXME: include [below] when we have updated our "graphAlgorithms_distance" wrt. 'tranpose' and 'weight-pre-comptution for all vertices'.
  if(true) { //! Test signficance of 'matrix-pre-transpostion' and 'pre-computation of distance-matrix', ie, to evalaute/test the effect of pre-computing the distance-matrix wrt. exeuction-time:
    // FIXME: what is the average number of iterations in k-means clustering? ... eg, wrt. the cost of distance-matrix-look-ups ....  <-- as an polonymoial expectaiton ... ie, as all combinations/dimensions of |clsuters| needs to be investgiated for each vertex .... implies that for large/importnat cases pre-processing of the distance-matrx is benefitial ... A different strenghts/'ability' of the pre-processing is the simplity in 'supporting' parallel operations
    //const uint size_of_array = 20; const uint nrows = size_of_array; const uint ncols = size_of_array; 
    const uint size_of_array = 1000; const uint nrows = size_of_array; const uint ncols = size_of_array; 
    assert(nrows == ncols); //! ie, what we expect for medoid-clustering (oekseth, 06. nov. 2016).
    uint clusterid[size_of_array]; uint tclusterid[size_of_array]; uint counts[size_of_array]; uint mapping[size_of_array];
    t_float **distmatrix = NULL; char **mask = NULL;
    //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
    const bool isTo_use_continousSTripsOf_memory = true;
    maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
    
    // FIXME: in [”elow] conisder to test also for clsuters of size_of_array/2
    const uint arrOf_cluster_counts_size = 4; const uint arrOf_cluster_counts[arrOf_cluster_counts_size] = {10, 50, 100, 500};
    for(uint cluster_count_index = 0; cluster_count_index < arrOf_cluster_counts_size; cluster_count_index++) {
      //! Investigate the effects of optimization of different number-thresholds.
      const uint arrOf_numberOf_interations_size = 6; const uint arrOf_numberOf_interations[arrOf_numberOf_interations_size] = {10, 20, nrows / 4, 
																nrows/2, nrows, nrows*2};
      
      for(uint numberOf_iterations_index = 0; numberOf_iterations_index < arrOf_numberOf_interations_size; numberOf_iterations_index++) {
	const uint nclusters = arrOf_cluster_counts[cluster_count_index];
	uint *mapOf_clusterId = new uint[nrows];


	
	/* float **cdata = NULL; char **cmask = NULL; */
	/* maskAllocate__makedatamask(/\*nrows=*\/nclusters, /\*ncols=*\/max(ncols, nrows), &cdata, &cmask, isTo_use_continousSTripsOf_memory);	   */
	{
	  //! The operation:
	  t_float error = 0;
	  for(uint isTo_preComputeDistanceMatrix_ifSpeedIncreases = 0; isTo_preComputeDistanceMatrix_ifSpeedIncreases < 2; isTo_preComputeDistanceMatrix_ifSpeedIncreases++) {
	    for(uint transpose = 0; transpose < 2; transpose++) {

	      if(transpose && (nclusters >= ncols)) {continue;} //! ie, to avoid a pointless call.
	      if(!transpose && (nclusters >= nrows)) {continue;} //! ie, to avoid a pointless call.

	      t_float **cdata = NULL; char **cmask = NULL;
	      //printf("\n\nallocates, at %s:%d\n", __FILE__, __LINE__);
	      __allcoate_clusterData(ncols, nrows, nclusters, transpose, &cdata, &cmask, isTo_use_continousSTripsOf_memory);



	      for(uint isTo_use_mean = 0; isTo_use_mean < 3; isTo_use_mean++) {
		//! Initiate:
		for(uint m = 0; m < size_of_array; m++) {
		  clusterid[m] = 0; tclusterid[m] = 0; counts[m] = 0; mapping[m] = 0;
		}
		{
		  //! -------------------------------
		  //! Start the clock:
		  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
		  
		  //class measure time = measure(); time.start_time_measurement();	    
		  const e_kt_correlationFunction_t metric_id  = e_kt_correlationFunction_groupOf_minkowski_euclid;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;
		  //!
		  //! Comptue the clusters using oen fo the 'algoritms for k-clsuter-idneitficiaotn':
		  const uint cnt_iter = arrOf_numberOf_interations[numberOf_iterations_index];
		  uint iffound = UINT_MAX;
		  if(isTo_use_mean == 2) {
		    assert(nrows == ncols);
		    kmedoids__extensiveInputParams(nclusters, nrows, distmatrix, cnt_iter, clusterid, NULL, &error, &iffound, NULL, /*obj_rand__init=*/NULL);
		  } else {
		    if(isTo_use_mean == 0) {
		      const char cluster_method = 'm';
		      kcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncols, distmatrix, mask, /*weight=*/NULL, transpose, /*npass=*/cnt_iter, cluster_method, clusterid, &error, &iffound, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/true, NULL, NULL);
		    } else {
		      const char cluster_method = 'a';
		      kcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncols, distmatrix, mask, /*weight=*/NULL, transpose, /*npass=*/cnt_iter, cluster_method, clusterid, &error, &iffound, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/true, NULL, NULL);
		    }
		    //kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncols, distmatrix, mask, /*weight=*/NULL, transpose, /*npass=*/arrOf_numberOf_interations[numberOf_iterations_index], cdata, cmask, clusterid, &error, tclusterid, counts, mapping, isTo_use_mean, isTo_preComputeDistanceMatrix_ifSpeedIncreases);
		  }
		  // FIXME: update [”elow]
		  if(isTo_use_mean == 1) {
		    __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration("kClustering::mean", nrows, ncols, nclusters, transpose); //, time, NULL, isTo_use_mean, isTo_preComputeDistanceMatrix_ifSpeedIncreases);
		  } else if(isTo_use_mean == 0) {
		    __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration("kClustering::median", nrows, ncols, nclusters, transpose); //, time, NULL, isTo_use_mean, isTo_preComputeDistanceMatrix_ifSpeedIncreases);
		  } else {
		    __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration("kClustering::medioid", nrows, ncols, nclusters, transpose); //, time, NULL, isTo_use_mean, isTo_preComputeDistanceMatrix_ifSpeedIncreases);
		  }
		}
	      }
	      maskAllocate__freedatamask(/*nelements=*/size_of_array, cdata, cmask, isTo_use_continousSTripsOf_memory);    
	    }
	  }
	}
      //! De-allocate locally reseerved memory:
	delete [] mapOf_clusterId; mapOf_clusterId = NULL;
      }      
    }
    //! De-allocate locally reseerved memory:
    maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
  
  }


  return 1;
}

//! Then eprform a simpel test to evlauate the 'passing of itnernal assers' wrt. the k-means-clsuter-comparison.
static void simple_test(const bool kmean_isToUse_kMedian, const bool kmean_kmean_isToUse_medoidInComputation, const bool isTo_alsoCompute_corrMetric) {


  const e_kt_correlationFunction_t metric_id  = e_kt_correlationFunction_groupOf_minkowski_euclid;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;
  
  const uint nclusters = 4*8*4;
  //const uint nclusters = 4*8; //*2;
  //  const uint nclusters = 4*8;
  //const uint nclusters = 4*4;
  //  const uint nclusters = 4*2;
  //const uint nrows = nclusters*40; const uint ncols = nrows; 
  const uint nrows = 150*6; const uint ncols = nrows; //3000; 
  //  const uint nrows = 150; const uint ncols = 1500; 
  //  const uint nrows = 50; const uint ncols = 50; 
  //  const uint nrows = 20; const uint ncols = 50; 
  const uint transpose = 0;
  const bool isTo_use_continousSTripsOf_memory = true;
  const t_float empty_value = 0;
  //uint cnt_iter = 4000; //! where 'find-center' consumes 17%
  //uint cnt_iter = 1000; //! where 'find-center' consumes 23%
  uint cnt_iter = 200; //! where 'find-center' consumes 18%
  uint size_x = nclusters; uint size_y = ncols;
  if(transpose == 1) {
    size_x = nrows; size_y = nclusters;
  }
  //const uint size_x = (transpose==0) ? nrows : ncols; 	  const uint size_y = (transpose==0) ? ncols : nrows;
  t_float **distmatrix = allocate_2d_list_float(nrows, ncols, empty_value);
  for(uint i = 0; i < nrows; i++) {
    for(uint k = 0; k < ncols; k++) {
      distmatrix[i][k] = k;
    }
  }

  char **mask = NULL;
  t_float **cmask_tmp = allocate_2d_list_float(size_x, size_y, empty_value);
  t_float **cdata = NULL; char **cmask = NULL;
  __allcoate_clusterData(ncols, nrows, nclusters, transpose, &cdata, &cmask, isTo_use_continousSTripsOf_memory);


  if(isTo_alsoCompute_corrMetric) { //! sperately evalaute the time-completiscy of the distance-matrix in quesiton, ie, to have a 'point of comparison:
    const bool isTo_copmuteResults_optimal = true; const bool needTo_useMask_evaluation = false;
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'

    t_float (*metric_each) (config_nonRank_each_t) = setmetric__correlationComparison__each(metric_id, /*weight=*/NULL, /*mask=*/NULL, /*mask=*/NULL, /*needTo_useMask_evaluation=*/needTo_useMask_evaluation);

    struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
    setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);
    
    const bool isTo_useOptimizedMetricFor_kendall = isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id);
    if(isTo_useOptimizedMetricFor_kendall) { //(dist == 'k') ) {
      assert(transpose == 0); //! ie, to simplify our code.
      
      //! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure:
      //! Note In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
      init__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute, nrows, ncols, distmatrix, /*mask=*/NULL, needTo_useMask_evaluation);
    }
    

    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	if(row_id != col_id) {
	  const e_cmp_masksAre_used_t masksAre_used = e_cmp_masksAre_used_false;
	  //const e_cmp_masksAre_used_t masksAre_used = (needTo_useMask_evaluation_inDistanceComptuation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
	  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, /*mask=*/NULL, /*cmask=*/NULL, /*weight=*/NULL, transpose, masksAre_used, nrows, ncols);
	  
	  t_float distance =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncols, distmatrix, distmatrix, /*index1=*/row_id, /*index2=*/col_id, config_metric, metric_each,
						(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
	  assert(distance != T_FLOAT_MAX);
	}
      }
    }

    if(isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id)) { //(dist == 'k') ) {
      //if(isTo_copmuteResults_optimal && (dist == 'k') ) {
      free__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);
    } //! else we assuemt he "s_correlationType_kendall_partialPreCompute_kendall" data-structure is empty.
    __assertClass_generateResultsOf_timeMeasurements("exec-time of the exected corr-analsys to be 'called' prior to a clsutering-algortihm-call", nrows, ncols); //, transpose, time, NULL);
  }


  //uint *mapOf_clusterId = allocate_1d_list_uint(nrows, empty_value);
  uint *clusterid = allocate_1d_list_uint(nrows, empty_value);
  uint *tclusterid = allocate_1d_list_uint(nrows, empty_value);
  //! ---- 
  uint *counts = allocate_1d_list_uint(nclusters, empty_value);
  uint *mapping = allocate_1d_list_uint(nclusters, empty_value);
  //uint clusterid[size_of_array]; uint tclusterid[size_of_array]; uint counts[size_of_array]; uint mapping[size_of_array];
  //! Initiate:
  for(uint m = 0; m < nrows; m++) {
    clusterid[m] = 0; tclusterid[m] = 0; 
  }
  for(uint m = 0; m < nclusters; m++) {
    counts[m] = 0; mapping[m] = 0;
  }

  bool isTo_preComputeDistanceMatrix_ifSpeedIncreases = true;
  t_float error = 0;

  //
  uint iffound = UINT_MAX;
  
  assert(nrows == ncols); //! ie, waht we expect
  if(kmean_isToUse_kMedian) {
    bool isTo_use_mean = false;
    const char cluster_method = 'a';
    printf("\n\n# computes for kMeans::median, at %s:%d\n", __FILE__, __LINE__);
    kcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncols, distmatrix, mask, /*weight=*/NULL, transpose, /*npass=*/cnt_iter, cluster_method, clusterid, &error, &iffound, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/true, NULL, NULL);
  } else if(kmean_kmean_isToUse_medoidInComputation) {
    printf("\n\n# computes for kMeans::medoid, at %s:%d\n", __FILE__, __LINE__);
    kmedoids__extensiveInputParams(nclusters, nrows, distmatrix, cnt_iter, clusterid, NULL, &error, &iffound, NULL, NULL);
  } else {
    const char cluster_method = 'm';
    bool isTo_use_mean = true;
    printf("\n\n# computes for kMeans::mean, at %s:%d\n", __FILE__, __LINE__);
    kcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncols, distmatrix, mask, /*weight=*/NULL, transpose, /*npass=*/cnt_iter, cluster_method, clusterid, &error, &iffound, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/true, NULL, NULL);
  }
  // 
  //kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncols, distmatrix, mask, /*weight=*/NULL, transpose, /*npass=*/cnt_iter, cdata, cmask, clusterid, &error, tclusterid, counts, mapping, isTo_use_mean, isTo_preComputeDistanceMatrix_ifSpeedIncreases);
  		// FIXME: update [”elow]


  // FIXME: what is in the restul of: ... "tclusterid" VS "clusterid" ? <-- "clusterid[j] = centroids[tclusterid[j]];", ie, where "clusterid" is the vertex-id of the centroid in each cluster-id.
  // FIXME: what is in the restul of: ... "counts"? <-- the 'counts of vertices' assicated to each particular cluster
  // FIXME: what is in the restul of: ... "cdata"? <-- hold teh 'distance from each 'clsuter-id-center' to vertex'
  // FIXME: what is in the restul of: ... "cmask"? <-- itneranlly used to indciate the 'interest' in/for a given cluster, ie, used in cobmiatnion with "cdata"

  //!
  //! De.-allcoate the results, ie, as we in oru test is primarily interested in 'getting the correlationa-nalsyss throguh our itnernal tests':
  assert(mask == NULL);
  free_2d_list_float(&distmatrix, nrows);
  free_2d_list_float(&cdata, ncols);
  free_2d_list_float(&cmask_tmp, size_x);
  assert(cmask);
  free_2d_list_char(&cmask, ncols);
  free_1d_list_uint(&clusterid);
  free_1d_list_uint(&tclusterid);
  free_1d_list_uint(&counts);
  free_1d_list_uint(&mapping);
}



//! The main-function to test our "measure_codeStyle_functionRef_api"
void main__kt_clusterAlg_fixed(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  if(isTo_processAll || (0 == strncmp("simple_test", array[2], strlen("simple_test"))) ) {
    //! Givne teh 'random-access' we perform muliple sample-runs:
    for(uint sample_runs = 0; sample_runs < 4; sample_runs++) {
      printf("\n----------------\n # Sample-run %u:\n-----\n\n", sample_runs);
      //! Then ivnestgiate/comptue for the three dfiferent k-means algortims, and for the first corr-emtric also evlauate wrt. the correlation-metic.
      simple_test(/*k-median=*/false, /*use-medoid=*/false, /*compute-corrMetric=*/true);
      simple_test(/*k-median=*/true, /*use-medoid=*/false, /*compute-corrMetric=*/false);
      simple_test(/*k-median=*/false, /*use-medoid=*/true, /*compute-corrMetric=*/false);
      cnt_Tests_evalauted++;
    }
  }


  if(isTo_processAll || (0 == strncmp("measure_finding_median", array[2], strlen("measure_finding_median"))) ) {
    measure_finding_median();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("assert_class__kt__clusterAlg_fixed", array[2], strlen("assert_class__kt__clusterAlg_fixed"))) ) {
    assert_class__kt__clusterAlg_fixed();
    cnt_Tests_evalauted++;
  }

  /* if(isTo_processAll || (0 == strncmp("subCase-isolatedFor--kendall", array[2], strlen("subCase-isolatedFor--kendall"))) ) { */
  /*   call_subCase_timeCostMetricsIsolated__kendall(); */
  /*   cnt_Tests_evalauted++; */
  /* } */



  // printf("-----, at %s:%d\n", __FILE__, __LINE__);
  //! Appply the performance-tests:
  //apply_tests();

  /* assert(false); // FIXME: add soemthing ... use the 'sytntiec' asa tmplate. */


  /* assert(false); // FIXME: cosnider comparing our implemetnaiton ot he "OpenCV" impelmtantion ("https://github.com/opencv/opencv/blob/master/modules/core/src/kmeans.cpp") */


  // FIXME: try to gernealise [below]
  //const bool print_info = true;
  //assert_class__kt__clusterAlg_fixed();



  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
