//#define mapOf_vertexMembershipId self->mapOf_vertexMembershipId
//#define listOf_membership_vertexCollection self->listOf_membership_vertexCollection
//! ----------------
  { //! Merge the forest-id of the parent with 'this':

    //printf("(compare-forest)\t this[%u]='%u', parent[%u]='%u', at %s:%d\n", this_key, self->mapOf_vertexMembershipId[this_key], parent_key, self->mapOf_vertexMembershipId[parent_key], __FILE__, __LINE__);
    if(mapOf_vertexMembershipId[this_key] == UINT_MAX) {
      uint forest_id = UINT_MAX;
      if(mapOf_vertexMembershipId[parent_key] == UINT_MAX) {
	//assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
	forest_id = listOf_membership_vertexCollection.biggestInserted_row_id + 1; //! ie, the next id.
	mapOf_vertexMembershipId[parent_key] = forest_id;
	//forest_id = self->listOf_membership_vertexCollection.get_current_pos();
	//! Set the id of each verex:
	//! update the forest with its new-identified members:
	// printf("(push)\t (forest=%u)\t vertex=%u, at %s:%d\n", forest_id, parent_key, __FILE__, __LINE__); 
	assert(forest_id != UINT_MAX);
	assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
	push__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id, parent_key);	
	assert(listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
	//self->listOf_membership_vertexCollection.get_unsafe_list_object_and_insertNew_ifNotSet(forest_id)->push(parent_key);
      } else { //! then we use the forest-id of the aprent.
	forest_id = mapOf_vertexMembershipId[parent_key]; 
      }
      mapOf_vertexMembershipId[this_key] = forest_id;
      //! Make the child/this vertex/key part of the same 'disjoint forest' as the parent-id/key:
      
      { //! __make_vertexPartOf_forestWhichAGivenVertex_isPartOf(self, /*new-vertex*/this_key, /*vertex-knowing-disjointForestId=*/parent_key);
	const uint new_vertex = this_key; const uint vertexKnowing_disjointForestId = parent_key;
	//! Note: Insert a vertex int a forest known by a different vertex:
#ifndef NDEBUG
	{
	  const uint forest_id = mapOf_vertexMembershipId[vertexKnowing_disjointForestId]; 
	  assert(forest_id != UINT_MAX);
	  assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
	}
#endif
#include "__make_vertexPartOf_forestWhichAGivenVertex_isPartOf.c"
      }
      assert(forest_id == mapOf_vertexMembershipId[this_key]); // ie, that the [below] update word as we expected.
      assert(mapOf_vertexMembershipId[this_key] == mapOf_vertexMembershipId[parent_key]); // ie, what we expect.
      assert(mapOf_vertexMembershipId[this_key] != UINT_MAX);      
    } else if(mapOf_vertexMembershipId[parent_key] == UINT_MAX) {
      //! Make the parent-vertex part of the same 'disjoint forest' as the child-id/key:
      { //!__make_vertexPartOf_forestWhichAGivenVertex_isPartOf(self, /*new-vertex*/parent_key, /*vertex-knowing-disjointForestId=*/this_key);
	//! Note: Insert a vertex int a forest known by a different vertex:
	const uint new_vertex = parent_key; const uint vertexKnowing_disjointForestId = this_key;
#ifndef NDEBUG
	{
	  const uint forest_id = mapOf_vertexMembershipId[vertexKnowing_disjointForestId]; 
	  assert(forest_id != UINT_MAX);
	  assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
	}
#endif
#include "__make_vertexPartOf_forestWhichAGivenVertex_isPartOf.c"
      }
      assert(mapOf_vertexMembershipId[this_key] == mapOf_vertexMembershipId[parent_key]); // ie, what we expect.
    } else { //! Then we have to merge the set of identites, ie, the most complex part of both this implementaiotn/algorithm, and the most complex part wrt. marotized exeuction-time/complexity.
      //! Validate that the operation is to be regarded as 'complex': otehrwise we expect the caller to have hadneld the situation/case.
      const uint child_forest_id  = mapOf_vertexMembershipId[this_key];
      const uint parent_forest_id = mapOf_vertexMembershipId[parent_key];
      assert(child_forest_id != UINT_MAX);
      assert(parent_forest_id != UINT_MAX);
      
      //printf("\t(consider-merge)\t given child_forest_id=%u, parent_forest_id=%u, at %s:%d\n", child_forest_id, parent_forest_id, __FILE__, __LINE__);
      if(child_forest_id != parent_forest_id) {
	{ //! update_disjointForest_membership_id(this_key, parent_key);
	  //printf("\t\t(consider-merge::merges)\t given child_forest_id=%u, parent_forest_id=%u, at %s:%d\n", child_forest_id, parent_forest_id, __FILE__, __LINE__);
#include "__update_disjointForest_membership_id.c"
	}
      } // otherwise 'tehy are already merged', ie, the case is already hanlded (a case which we expect to be the most common case, at least for DAGs).
      // printf("mapOf_vertexMembershipId[%u VS %u] = (%u VS %u), at %s:%d\n", this_key, parent_key, mapOf_vertexMembershipId[this_key], mapOf_vertexMembershipId[parent_key], __FILE__, __LINE__);
      assert(mapOf_vertexMembershipId[this_key] == mapOf_vertexMembershipId[parent_key]); // ie, what we expect.
    }
  }
  
assert(mapOf_vertexMembershipId[this_key] == mapOf_vertexMembershipId[parent_key]); // ie, what we expect.
