#ifndef kt_assessPredictions_h
#define kt_assessPredictions_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_assesspredictions
   @brief logics lgoics for assessing different user-predictions (eg, of cluster and correlation-metices   (oekseth, 06. des. 2016). 
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016). 
 **/

#include "hpLysis_api.h"
#include "matrix_deviation.h" //! eg, used to comptue STD and Kurtosis

/**
   @struct s_kt_assessPredictions__clusterProp__dataAmbiguity
   @brief specifies the expected data-ambugity in a given gold-stadnard, ie, to 'adjsut' the ranks based on a 'deviation to this' (eosekth, 06. des. 2016).
 **/
typedef struct s_kt_assessPredictions__clusterProp__dataAmbiguity {
  t_float expected_STD;
  t_float expected_kurtosis;
  t_float expected_skewness;
  t_float expected_studentT__Pearson; 
  t_float expected_studentT__Spearman;
} s_kt_assessPredictions__clusterProp__dataAmbiguity_t;

//! @return an intiated version of the "s_kt_assessPredictions__clusterProp__dataAmbiguity_t" object.
s_kt_assessPredictions__clusterProp__dataAmbiguity_t setToEmpty__s_kt_assessPredictions__clusterProp__dataAmbiguity_t();

/**
   @struct s_kt_assessPredictions
   @brief configures the s_kt_assesspredictions_t object (oekseth, 06. des. 2016).
 **/
typedef struct s_kt_assessPredictions {
  s_kt_assessPredictions__clusterProp__dataAmbiguity_t clusterProp__dataAmbiguity;
  s_kt_correlationMetric_t corr_metricObj__ideal; //! which identifes/describes the correlation-metric which produce the 'ideal' cluster-result
} s_kt_assessPredictions_t;


//! @return an inlitazed object of type s_kt_assessPredictions_t using the default intalizaitons.
s_kt_assessPredictions_t setToEmpty__s_kt_assessPredictions_t();

/**
   @enum e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput
   @brief simplfies the build-process of different CCMs
 **/
typedef enum e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput {
  e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__all,
  e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__setBased,
  e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased,
  e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__subset_extremeCases,
  e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__all,
  //! -----
  e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef
} e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t;


/**
   @struct s_kt_assessPredictions_differentMetricSets
   @brief a data-itnerface to hold result-structures for different clsuter-cmp-matric-evlauatiosn (eosketh, 06. des. 2016).
   @remarks provide logics for accessing the cluster-results assicated to the use of the "e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t" enum
   @remarks used to 'support' the different 'categories' in our "e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef". Use an 'input-object with matirces, where each matrix-rows-index' 'describes' the internla/current row 'to inser thte ranks at' 
 **/
typedef struct s_kt_assessPredictions_differentMetricSets {
  s_kt_matrix_t m_ranks_inCatFor__metrics[e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef];
  //s_kt_matrix_t m_ranks_inCatFor__clusters[e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef];
  s_kt_longFilesHandler fileHandler; 
  uint global_cntMeasurements_currentPos;
  uint global_cntMeasurements_size;
} s_kt_assessPredictions_differentMetricSets_t;

/**
   @brief Itnaites and return the setTo_empty__s_kt_matrix_t object
   @param <global_cntMeasurements_size>
   @param <fileNameMeta> which if set to NULL impleis taht the 'mapping' betweent eh generated files and the 'meta-proerpteis desciribnt the fiels' is written to "stdout"
   @param <result_directory> which if Not set to NULL represents a writable directyr, a writable directry 'used' to store the results generated by this frunction.
   @return an intiated object.
**/
static s_kt_assessPredictions_differentMetricSets_t setToEmptyAndReturn__s_kt_assessPredictions_differentMetricSets_t(const uint global_cntMeasurements, const char *fileNameMeta, const char *result_directory) {
  s_kt_assessPredictions_differentMetricSets_t self;
  self.global_cntMeasurements_currentPos = 0;
  self.global_cntMeasurements_size = global_cntMeasurements;
  for(uint i = 0; i < (uint)e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef; i++) {
    setTo_empty__s_kt_matrix_t(&(self.m_ranks_inCatFor__metrics[i]));
    //setTo_empty__s_kt_matrix_t(&(self.m_ranks_inCatFor__clusters[i]));
  }
  self.fileHandler = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/result_directory, /*file_nameMeta=*/fileNameMeta, /*stringOf_header=*/NULL, /*stream_out=*/NULL);
  //! @return 
  return self;
}


/**
   @brief merge resutls of diffenret cluster-metric-comaprsions and cluster-result-comaprsions (oekseth, 06. des. 2016).
   @param <self> is the object we export, ie, the object which should cotnain 'data' from earlier calls
   @param <objResult__STD> which if set: is udpated with the STD of the correlation-score, a correlation-score which 'uses' the correlations-core comptued in heat-map-genration-step (in thsi funciton)
   @param <objResult__Skewness> similar to objResult__STD with difference that we comptue and udpate for Skewness
   @param <objResult__Kurtosis> similar to objResult__STD with difference that we comptue and udpate for Kurtosis
   @param <global_row_id> is the row-id-tag to 'annoate' the result-file (or the meta-ampping-fiele) with.
   @param <stringOf_measurementSubset> which may be sued to 'provide' an explcit-descirption of the string.
   @param <column_start_pos> is the 'index-offset' to be used when 'generating't eh file-name.
   @param <stringOf_specificDescription> which is used to 'annotate' teh result-file (or the meta-ampping-fiele) with.
   @return the number of columsn inserted

   @remarks an example-budiling-usage of thsi object cosnerns a call to our "correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(..)", ie, for whcih 'this object' then (is expected to) hold the prediction-ransk fo the cluster-comparsion-metics.
   @remarks an example-usage of the "column_start_pos" and 'return-value' is to use this as input to the column_start_pos for 'the next call', eg, when 'merging objects'. A use-case-example fo teh latter conserns the 'case' when we are interested in evalauting the cluster-comperisaon-metric-agreeemtne wrt. different 'perspectives' seprately for each data-set. Through the latter approach 'vertical alginment' (of heatmap-matrices with the same cluster-comparison-inference-type) simplifeis the idnetificaiton of 'cross-data-agreement'. Fromt he latter we observe how our appraoch facitlates the uidnetificaiotn of pattersn between complex relationships.
   @remarks for each 'inserted' rank-row-list we compute [AVG, STD, Skewness], and insert the resutl seperately into the differnet result-objects.
   
   @remarks goal is to idnetify/evlauate differences wrt. 'patterns/distrubiotns of cluster-memberhsips between/across/for differnet input-data-sets': to use autoatmed metrics in cobmaition with humasn 'ability' to find/dtect correlation-pattersn to infer resutls. For this task/goal we use a visaul WWW-appraoch to desicrbe the relatneesnss betwene the differnet data-sets. From the WWW-appraoch manages to (use this 'relatedness' to) explain/hypothese difference wrt. the 'main results' (eg, wrt. differences in similarity-agreements of cluster-comparison-emtics for/between different data-sets). The idea (in this function) is to first generate correlation-matrices for the dffierent cluster-memberships, and thereafter use (eg, in/through our WWW-approach) the 'generated' result to evlaute/descirbe the 'differnet number of combiatniosn which our complete/global comaprsion-appraoch covers'. For the WWW-appraoch we expect 'this fucntion' to be caleld for each data-set, for which the result becomes a colelciton of heatmap-matrices. The methdo (which we apply) is to perform an all-against-all-correlation-analsyssi using MINE of the cluster-memberships (seprately for each of our 'cases'). An example-use-case (of this function) is to call this funciton for a colleciton of data-sets and thereafre explroe how (eg, to what degree) the simlairty-pattesn (expresesed/idneitfied in this fucntion) is attributed to proeprties of the data-collection-set (which is explored). To simplify the latter task hpLyssi provides/includes a Perl-based heat-map-generator: to build an overview of matrices, ordering the matrices/iamges using core-proerpteis of (of the fiwels, expressed through defined/speciifed meta-attributes), visualzing the 'colleciotn' through a web-browser.
**/
static uint correlateAndExport__s_kt_assessPredictions_differentMetricSets_t(s_kt_assessPredictions_differentMetricSets_t *self, s_kt_assessPredictions_differentMetricSets_t *objResult__STD, s_kt_assessPredictions_differentMetricSets_t *objResult__Skewness, s_kt_assessPredictions_differentMetricSets_t *objResult__Kurtosis, uint global_row_id, const char *stringOf_measurementSubset, const uint column_start_pos, const char *stringOf_specificDescription) {
  assert(self); 
  if(global_row_id != UINT_MAX) {
    assert(global_row_id == self->global_cntMeasurements_currentPos); // TODO: validate this assumption ... which is used to simplify the correctness-elvauuiton of our approach.
  }
  global_row_id = self->global_cntMeasurements_currentPos;
  uint cnt_cols_inserted = 0;
  for(uint enum_clusterResultAsInput_index = 0; enum_clusterResultAsInput_index < (uint)e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef; enum_clusterResultAsInput_index++) {
    const e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t enum_clusterResultAsInput = (e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t)enum_clusterResultAsInput_index;
    const char *stringOf_enum = NULL;
    if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__all) {stringOf_enum = "all";}
    else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__setBased) {stringOf_enum = "setBased";}
    else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased) {stringOf_enum = "matrixBased";}
    else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__subset_extremeCases) {stringOf_enum = "matrixBased_andHpLysisCorrMetrics_extreme";}
    else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__all) {stringOf_enum = "matrixBased_andHpLysisCorrMetrics_all";}
    else {assert(false);} //! ie, as we then need to add support 'for this'.

    //! ---
    const s_kt_matrix_t *matrix_input = &(self->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index]);
    assert(matrix_input);
    //! ---
    
    assert(stringOf_enum);
    char stringOf_resultPrefix__sub[2000] = {'\0'}; 
    const uint cnt_vertices = matrix_input->ncols;
    assert(cnt_vertices > 0);
    if(stringOf_specificDescription && strlen(stringOf_specificDescription)) {
      sprintf(stringOf_resultPrefix__sub, "groupId_%u.tagLocalIndex_%u.tagNrows_%u_%u.tagExperiment_%s.tagSample_%s_%s_", global_row_id, 
	      enum_clusterResultAsInput_index + column_start_pos,
	      matrix_input->nrows, 
	      matrix_input->ncols,
	      stringOf_enum, stringOf_measurementSubset,
	      stringOf_specificDescription
	      );
    } else {
      sprintf(stringOf_resultPrefix__sub, "groupId_%u.tagLocalIndex_%u.tagNrows_%u_%u.tagExperiment_%s.tagSample_%s_", global_row_id, 
	      enum_clusterResultAsInput_index + column_start_pos,
	      matrix_input->nrows, 
	      matrix_input->ncols,
	      stringOf_enum, stringOf_measurementSubset);
    }


 
    //! -----------------------------------------------------
    //! -----------------------------------------------------
    //! 
    //! Apply the logics:
    //!
    //! Correlate and write out:
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, &(self->fileHandler)); //&fileHandler__localCorrMetrics);
    {
      //!
      //! Correlate:
      hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
      hp_config.stringOfResultPrefix__exportCorr__prior = stringOf_resultPrefix__sub;
      // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //!
      //! Correlate and export:
      const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, matrix_input, NULL, /*isTo_applyPreFilters=*/false);
      assert(is_ok);
      //! ------------------
      //! Update:
      cnt_cols_inserted++;
    }
    //! What we expect:
    assert(hp_config.matrixResult__inputToClustering.nrows > 0);     assert(hp_config.matrixResult__inputToClustering.ncols > 0);
    assert(hp_config.matrixResult__inputToClustering.nrows == matrix_input->nrows);     assert(hp_config.matrixResult__inputToClustering.ncols == matrix_input->nrows);
    //! 
    { //! Write out a 'transosed version':
      //!
      assert(strlen(stringOf_resultPrefix__sub)> 0);
      const uint cnt_chars = (uint)strlen(stringOf_resultPrefix__sub) + 20;
      const char empty_0 = 0;
      char *stringOf_resultPrefix__sub_trans = allocate_1d_list_char(cnt_chars, empty_0);
      assert(stringOf_resultPrefix__sub_trans);
      sprintf(stringOf_resultPrefix__sub_trans, "%s.tranpose=true", stringOf_resultPrefix__sub);
      //!
      //! Correlate:
      s_hpLysis_api_t hp_configTransposed = setToEmpty__s_hpLysis_api_t(NULL, &(self->fileHandler)); //&fileHandler__localCorrMetrics);
      hp_configTransposed.config.clusterConfig.isTo_transposeMatrix = true;
      hp_configTransposed.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
      hp_configTransposed.stringOfResultPrefix__exportCorr__prior = stringOf_resultPrefix__sub_trans;
      // hp_configTransposed.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //!
      //! Correlate and export:
      const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_configTransposed, matrix_input, NULL, /*isTo_applyPreFilters=*/false);
      assert(is_ok);
      //! De-allocates the "s_hpLysis_api_t" object.
      free__s_hpLysis_api_t(&hp_configTransposed);
      free_1d_list_char(&stringOf_resultPrefix__sub_trans);
      //! ------------------
      //! Update:
      cnt_cols_inserted++;
    }
    //! What we expect:
    assert(hp_config.matrixResult__inputToClustering.nrows > 0);     assert(hp_config.matrixResult__inputToClustering.ncols > 0);
    assert(hp_config.matrixResult__inputToClustering.nrows == matrix_input->nrows);     assert(hp_config.matrixResult__inputToClustering.ncols == matrix_input->nrows);
    const s_kt_matrix_t *matrix = &(hp_config.matrixResult__inputToClustering);

    //!
    //! --------------------------------------------------------- 
    if(objResult__STD || objResult__Kurtosis || objResult__Skewness) {
      assert(matrix); assert(matrix->nrows > 0);
      //!
      //! 
      //! Identify the 'degree of simlarity' for 'each case' and then updte each row:      
      //! Note: in [below] we use the logics defined in our "s_matrix_deviation_std_row_t(..)", a set of logics which is Not optimizwed wrt. memory-access-perofmrance though provides (ion the other hand) a simple impltation which is (realtivly speaking) straight-forward to validate (correctness of).
      { //! First itnaite:
	//! NotE: in [below] we use macors iot. simplify future- aminaince, ie, a code-readability-stratety.
#define __obj__ objResult__STD
	if(__obj__ && (__obj__->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].nrows == 0) ) {
	  //! Tehn we need to itnaite 'this':
	  assert(__obj__->global_cntMeasurements_size > 0);
	  assert(__obj__->global_cntMeasurements_currentPos == 0);
	  //! Intiate:
	  init__s_kt_matrix(&(__obj__->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index]), /*nrows=*/__obj__->global_cntMeasurements_size, /*ncols=*/matrix->nrows, /*isTo_allocateWeightColumns=*/false);
	}
#undef __obj__
#define __obj__ objResult__Kurtosis
	if(__obj__ && (__obj__->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].nrows == 0) ) {
	  //! Tehn we need to itnaite 'this':
	  assert(__obj__->global_cntMeasurements_size > 0);
	  assert(__obj__->global_cntMeasurements_currentPos == 0);
	  //! Intiate:
	  init__s_kt_matrix(&(__obj__->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index]), /*nrows=*/__obj__->global_cntMeasurements_size, /*ncols=*/matrix->nrows, /*isTo_allocateWeightColumns=*/false);
	}
#undef __obj__
#define __obj__ objResult__Skewness
	if(__obj__ && (__obj__->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].nrows == 0) ) {
	  //! Tehn we need to itnaite 'this':
	  assert(__obj__->global_cntMeasurements_size > 0);
	  assert(__obj__->global_cntMeasurements_currentPos == 0);
	  //! Intiate:
	  init__s_kt_matrix(&(__obj__->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index]), /*nrows=*/__obj__->global_cntMeasurements_size, /*ncols=*/matrix->nrows, /*isTo_allocateWeightColumns=*/false);
	}
#undef __obj__
      }
    }	
    //! 
    //! Update the result:
    for(uint row_id = 0; row_id < matrix->nrows; row_id++) {
      const t_float *row = matrix->matrix[row_id];
      assert(row);
      //! Copmute:
      s_matrix_deviation_std_row_t obj_deviation; setTo_empty__s_matrix_deviation_std_row(&obj_deviation);
      get_forRow_sampleDeviation__matrix_deviation_implicitMask(row, matrix->ncols, &obj_deviation);      
      const uint rowToUpdate = self->global_cntMeasurements_currentPos; // TODO: validate correctness of 'this' ... eg, consider using a different 'row-specifier'.
      //! --- 
      if(objResult__STD) {	
	assert(rowToUpdate < objResult__STD->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].nrows);
	assert(row_id < objResult__STD->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].ncols);
	objResult__STD->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].matrix[rowToUpdate][row_id] = obj_deviation.variance;	
      }
      if(objResult__Kurtosis) {	
	assert(rowToUpdate < objResult__Kurtosis->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].nrows);
	assert(row_id < objResult__Kurtosis->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].ncols);
	objResult__Kurtosis->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].matrix[rowToUpdate][row_id] = obj_deviation.kurtosis;	
      }
      if(objResult__Skewness) {	
	assert(rowToUpdate < objResult__Skewness->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].nrows);
	assert(row_id < objResult__Skewness->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].ncols);
	objResult__Skewness->m_ranks_inCatFor__metrics[enum_clusterResultAsInput_index].matrix[rowToUpdate][row_id] = obj_deviation.skewness;	
      }
    }
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);
  }

  //! then icnrmenet the 'row-counter':
  self->global_cntMeasurements_currentPos++;
  //!
  //! @return
  return cnt_cols_inserted;
}


/* /\** */

/*  **\/ */
/* static void correlateAndExportDirectly__s_kt_assessPredictions_differentMetricSets_t(s_kt_assessPredictions_differentMetricSets_t *self, const char *stringOf_measurementSubset, const char *stringOf_specificDescription, const uint global_row_id) { */
/*   char stringOf_resultPrefix__sub[2000] = {'\0'};  */
/*   sprintf(stringOf_resultPrefix__sub, "%s_%s */
/*   assert(false); // FIXME: call [ªbove]. */

/* } */

/* //! Allocates the internal matrices to the given diemsions: */
/* static void allocateInternal__matrices__s_kt_assessPredictions_differentMetricSets_t(s_kt_assessPredictions_differentMetricSets_t *self) { */

/* } */

//! De-allcoates the setTo_empty__s_kt_matrix_t object
static void free__setTo_empty__s_kt_assessPredictions_differentMetricSets_t(s_kt_assessPredictions_differentMetricSets_t *self) {
  for(uint i = 0; i < (uint)e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef; i++) {
    free__s_kt_matrix(&(self->m_ranks_inCatFor__metrics[i]));
    //free__s_kt_matrix(&(self->m_ranks_inCatFor__clusters[i]));
  }

  free__s_kt_longFilesHandler_t(&(self->fileHandler));  
}



/**
   @param <cnt_vertices> is the number of [n, n] vertices to evaluate the clsutering for
   @param <cnt_clusters> is a fixed max-count of clusters to evaluate.
   @param <stringOf_resultPrefix> is used to generate a set of unique result-files
   @param <clusterPredictionDataSets__set1> is the first data-set of cluster-prediction 
   @param <clusterPredictionDataSets__set1_size> is the number of rows (ie, cluster-samples), where each row is expected to hold cnt_vertices number of columns (where each column describe/reflect the cluster-membership of each vertex).
   @param <clusterPredictionDataSets__set2> is the first data-set of cluster-prediction 
   @param <clusterPredictionDataSets__set2_size> is the number of rows (ie, cluster-samples), where each row is expected to hold cnt_vertices number of columns (where each column describe/reflect the cluster-membership of each vertex).
   @param <arrOf_1d_dataXdata_goldStandard> is expected to hold the 'ranks' of the simliarity-signficance between [clusterPredictionDataSets__set1]x[clusterPredictionDataSets__set2].
   @param <squaredDistanceMatrix__1> is a distance-matrix which we compare the clsuter-distrubiotns agaisnt
   @param <squaredDistanceMatrix__2> is a distance-matrix which we compare the clsuter-distrubiotns agaisnt
   @param <squaredDistanceMatrix__3> is a distance-matrix which we compare the clsuter-distrubiotns agaisnt
   @param <config_min_rankThreshold> which is used to idnetify the most disjoint-signicant-sets of cluster-predictions: used when computing the data-set denoted as "dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv"
   @param <obj_result> which hold the result-object, eg, used to 'remember' teh 'ranks' of the metrics and cluster-sets.
   @remarks the "clusterPredictionDataSets__set*" is expected to each describe a mtrxi in formats [|cnt-cluster-sets|, cnt_vertices]. In brief the latter 'input-data-sets' are each expected to be/describe a matrix of cluster-results, where each row 'denotes' a cluster-result while each column maps each vertex to a uniue cluster-id (or cetnrality-id).
   @param <squaredDistanceMatrix__" where the set of the latter (eg "squaredDistanceMatrix__1", "squaredDistanceMatrix__2", ...) is used to compare muliple correlation-metric (ie, where the squared-distance-matrices may be the result of applying a corrleaiton-metic) and/or dilftering-appraoches (such as the 'effects' of applying PCA). Note in this cotnext that the "squaredDistanceMatrix__" are used for only a subset of the cluster-comparsion-metics: for detials please see our "kt_matrix_cmpCluster.h" (or contact the hpLysis develoeprs such as "Jan Christian Meyer <janchris@idi.ntnu.no>"). 
   @remarks the "e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t" is implcitly used in oru function, ie, to evlauate for 'differetn cases'. The motivation (for the latter) is to reduce the 'noise' which may arise from the different 'focus' of set-based cluster-comparison-metrics (eg, Fowles-Mallows) VS matrix-based (eg, Silhouette). An example-use-case (wrt. the latter result-set-generiaotn) conserns the case where we are interested in testing/evlauating the following null-hypothesis: "are there any distinct data-permtuations which may be used to a-prioery idneitfy the type of possible algorithm-qualtiy-assertsions (based on a-prior assumptions/knowledge of a given data-set)?".  For the latter question we observe how a comparison (betweend iffenret data-sets) of 'seperate cases' may simplify the eulicdiation of knowledge: to recue the 'space of compiarson' to entites which use the same 'type of data' to infer/idneitfy cluster-simlairty (ie, to use approximatiosn to provide a sca ffolded descirption of network isomorphy). 
   @remarks the "arrOf_1d_dataXdata_goldStandard" is expected to (in parcial tersm) be a 2d-matrix. To exmaplify the latter we for |clusterPredictionDataSets__set2|=4 and |clusterPredictionDataSets__set1|=4 may 'get' an input-matrix such as:
   ----
   t_float mapOf_scoreDistributions_case1_2xgoldStandard[16] = {					   
   6, 0, 3, 3,   //! ie, Row 1
   0, 6, 4, 4,   //! ie, Row 2 
   3, 4, 6, 6,   //! ie, Row 3 
   3, 4, 6, 6,   //! ie, Row 4
   };
   ---
   @remarks Focus on agreement between differnet cluster-comparison-metrics wrt. a given set of clsuter-predicitosn, where a gold-standard is used to 'rank' the different predicitons
   @remarks this funciton generates the following result-data-sets:
   -- "metricXdata": the simliarty between clsuter-comparison-metrics and 
   -- "metricXdata.ranks": where "metricXdata" is adjusteted wrt. the ranks, ie, "relative descriptiveness";
   -- "metricXdata.ranks_adjustedWrtGoldStandard.tsv": the result of computing "[gold]*metricXdata.ranks = [gold]*ranks(metricXdata)": describes how the 'user-defined ideal rank' is reflected in each of our evlauated cluster-comparison-metrics, ie, where we comute "arrOf_1d_dataXdata_goldStandard"x["metricXdata.ranks"];
   -- "dataXmetrics.ranks_adjustedWrtGoldStandard.tsv": the result of computing "rank(transpose([gold]*metricXdata.ranks)) = rank(transpose([gold]*ranks(metricXdata)))": provides clues (and/or indications) for how close a 'given cluster-prediction' is to the 'idealness' of a givne cluster-comparison-metric;
   -- "dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv": the result of inferring the disjoint-regions of resultMatrix=("config_min_rankThreshold" > ["dataXmetrics.ranks_adjustedWrtGoldStandard.tsv"]). Hoep is that 'this result-set' will simplify the visual indietifciaotn/understanding of how different cluster-predictions are captured in the complete range of cluster-comparison-metrics which are in use, eg, wrt. 'overlaps' in cluster-comparison-metrics correctness-predicitons 
   @remarks the "dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv" result-file:
   -- Idea[Apply the disjont-filter-clustering-procedure]: apply a 'filter' to identify the 'ranks' for each data-set: step(a) compute a "clusterDistrubiont_1[][]xclusterDsitribution_2[][] => clusterCmp[data-set][metric]" (where each 'row' represents a data-set-id and each column a cluster-compairson-metric); step(b) transpose teh matrix from step(a) and compute the ranks (seperately for/in each cluster-compiarson-metric); step(c) transpose the matrix from step(b) and identify the ranks of each cluster-comparison-metric; 	  
   -- In this evlauation we idneitfy the best-descptive clsuter-meitrcs for each of the data-sets. For this task we 'rank the metric-ranks for each data-set'. The result is 'a description of how (ie, to what extendt) each data-set-combination is regarded as close-to-ideal: as each metric have different quality-criteraias for gold-data-simliarty mutiple (and different) data-sets may be evalauted/described as a best-mathc-data-set. Therefore we assert that this comparison-appraoch may give insight into the use-case-applicaiblity (of cluster-comparison-metrics), ie, as distinct data-set-clusters indicates a clear/sigifcant correlation between cluster-prediction-resutls and cluster-comparsion-metrics' <-- TODO[JC]: may you try to improve the latter stences/asertion, and then use the latter in a to-be-written aritlce?
   -- build a transposed version of the 'rank-adjusted matrix', and 'in this' apply ranking', ie, iot. simplify the idneitficaiotn of well-described data-sets. In the result we find/describe the data-sets which are best described by the given cluster-comparison-metics
   
 **/
void standAlone__describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__s_kt_assessPredictions_t(const uint cnt_vertices, const uint cnt_clusters, const char *stringOf_resultPrefix, uint **clusterPredictionDataSets__set1, const uint clusterPredictionDataSets__set1_size, uint **clusterPredictionDataSets_set2, const uint clusterPredictionDataSets_set2_size, t_float *arrOf_1d_dataXdata_goldStandard, t_float **squaredDistanceMatrix__1, t_float **squaredDistanceMatrix__2, t_float **squaredDistanceMatrix__3, const t_float config_min_rankThreshold, s_kt_assessPredictions_differentMetricSets_t *obj_result);

/**
   @brief Evaluate the accuracy of the new-constructed distance-matrix: compare the distance-matrix using different matrix-comparison-functions specified in our "s_kt_matrix_cmpCluster_clusterDistance_t"
   @remarks an example-applicaiton (of this funciton) is to use a new-inferred correlation-matrix in combination with a gold-stadnard-clster-membership: to assess/elvuate the simliary with the gold-standard: complexity conserns the approx. 70,000 differnt clsuter-comparison-emtrics supported by hpLyssis, ie, which we evlauate in 'this function'.
   @remarks  describes a new method to compare cluster-comparision-metrics, specifically for a partiucalr data-set without being constrianed by cluster-algorithm-results:
   -- Use-case-result-evaluation: idneityf how clsuter-comparison-metrics overlaps wrt. a givne data-set, for which the result of 'mulipel data-sets' may indciate a g'enralized agreement'.
   -- method: the result of the metric-comparsion is used to generate clusters. In the cluster-generation we apply a disjoint-clustering on the 'results from the comarpsion': goal is to describe the relationship between the differnet cluster-exactness-measures: if we compare the heat-maps (prodcued in below) between/for diffneret function-calls (eg, for different inptu-data-sets) we expect that a simliarty (between the different metrics) will indicate that the cluster-comparison-metrics 'exibihbits' a 'stable proeprty', a 'stable proeprty which we assume will be related to the degree of data-ambugities, ie, the 'easiness in clsutering a particular data-set'.
   -- idea: in [below] we use disjoint-forest-clsutering combined with rank-trehshodls to idnetify groups of metrics. Thre strenght of this appraoch coserns both itds low exeuction-tiem-cost and the no-need to specirfy a predefined k-count of clusters. In brief as we are interested in a 'ptuative scaffodl of clsuter-emmberships' we hypotehse/assert that the disjotin-forest-based clsutering (combined with the rank-threshold) provide 'sufficent accracueis wrt. clsuter-accuracies
   -- result: in this comparison-evlauation we focus on idneitfying the 'agreement' between differnet cluster-evalaution-measures. For this task we compare [cluster-metrics=[permtautiosn-of{Silhouette, Dunn, Davils-Boulding}]]x[corr-metrics], and thereafter infer the ranks of the latter. The 'ranks' are used to idnicate the importance of the latter, ie, iot. hypothese/evlaute 'the ability of each metric to describe/enirch a g given cluster-comparison-emtric'.
**/
void standAlone__describeClusterCmpSimilarities_forDataSet__s_kt_assessPredictions_t(const uint *mapOf_nullHypothesis_vertexTo_centroidId, const s_kt_matrix_t *matrix_afterCorrelation, 
										     //s_kt_matrix_t *matrixTo_evaluate,
										     const uint ideal_clusterCnt, bool inputMatrix__isAnAdjecencyMatrix, const char *stringOf_resultPrefix, const t_float config_min_rankThreshold);

/**
   @brief identifies the simliartiy--divergence between a null-hyptohesis and a data-set (oekseth, 06. des. 2016).
   @param <self> is the internal object which hold the 'gnerlaized' configurations
   @param <mapOf_nullHypothesis_vertexTo_centroidId> is the 'mapping' between each vertex and teh 'centorid-vertex expected to describe the ogld-standard in a k-means clsutering'.
   @param <matrixTo_evaluate> is the data-set which we are to compare the 'gold-standard' with
   @param <ideal_clusterCnt> is the number of clusters 'in an ideal clsuter-result' (ie, wrt. mapOf_nullHypothesis_vertexTo_centroidId and matrixTo_evaluate)
   @param <stringOf_resultPrefix> is the string-rpefix to be used when gnereating adn exporting the result (of this evlauation).
   @param <inputMatrix__isAnAdjecencyMatrix> which if set to false imples that wwe assume the 'rows' to be in a different naem-space than teh 'columns': otherwise "nrows == ncols" is what we expect wrt. the matrixTo_evaluate object.
   @remarks provide lgoics to:
   -- idneitfy and evlauate simliarty betwen a 'precie correlation-metic' and a null-hypothesis (wrt. a given gold-stadnard)
 **/
void divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t(const s_kt_assessPredictions_t *self, uint *mapOf_nullHypothesis_vertexTo_centroidId, s_kt_matrix_t *matrixTo_evaluate, const uint ideal_clusterCnt, const char *stringOf_resultPrefix,  bool inputMatrix__isAnAdjecencyMatrix);

/**
   @brief identifies the simliartiy--divergence between a null-hyptohesis and a data-set (oekseth, 06. des. 2016).
   @param <matrixOf_stringsInEachCluster> hold a matrix of string-ids, where each row is epxected the hold the strings to be part of a given cluster: used to infer the "mapOf_nullHypothesis_vertexTo_centroidId"
   @param <cnt_clusters> is the number of clusters, ie, the number of rows in matrixOf_stringsInEachCluster
   @param <max_cnt_vertices> is the max-number of vertices in each cluster, ie, the 'fixed' column-size in matrixOf_stringsInEachCluster
   @param <nameOf_file> used to load the data-set, ie, to 'build' the "matrixTo_evaluate" object
   @param <matrixTo_evaluate> is the data-set which we are to compare the 'gold-standard' with
   @param <ideal_clusterCnt> is the number of clusters 'in an ideal clsuter-result' (ie, wrt. mapOf_nullHypothesis_vertexTo_centroidId and matrixTo_evaluate)
   @param <stringOf_resultPrefix> is the string-rpefix to be used when gnereating adn exporting the result (of this evlauation).
   @param <inputMatrix__isAnAdjecencyMatrix> which if set to false imples that wwe assume the 'rows' to be in a different naem-space than teh 'columns': otherwise "nrows == ncols" is what we expect wrt. the matrixTo_evaluate object.
   @remarks 
   -- a wrapper-function to "divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t(...)"
   -- "nameOf_file": if the files 'ends' with a ".tsv" suffix then we assume the input-fiel is a "TAbular Seperated Value" (TSV) matrix-file.
 **/
bool standAlone__divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t(const char ***matrixOf_stringsInEachCluster, const uint cnt_clusters, const uint max_cnt_vertices, const char *nameOf_file, const uint ideal_clusterCnt, const char *stringOf_resultPrefix,  bool inputMatrix__isAnAdjecencyMatrix);

#endif //! EOF
