/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @brief a wrapper-template to build centrality-functions for different configurations
   @remarks last generated at Sat Oct 22 11:19:35 2016 from "build_codeFor_tiling.pl"
 **/

if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__0(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__1(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__2(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__3(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__4(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__5(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__6(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__7(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__8(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__9(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__10(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__11(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__12(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__13(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__14(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__15(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__16(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__17(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__18(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__19(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__20(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__21(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__22(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__23(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__24(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__25(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__26(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__27(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__28(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__29(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__30(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__31(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__32(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__33(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__34(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__35(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__36(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__37(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__38(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__39(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__40(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__41(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__42(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__43(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__44(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__45(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__46(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__47(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__48(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__49(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__50(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__51(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__52(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__53(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__54(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__55(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__56(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__57(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__58(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__59(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__60(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__61(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__62(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__63(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__64(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__65(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__66(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__67(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__68(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__69(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__70(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__71(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__72(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__73(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__74(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__75(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__76(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__77(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__78(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__79(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__80(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__81(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__82(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__83(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__84(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__85(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__86(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__87(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__88(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__89(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__90(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__91(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__92(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__93(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__94(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__95(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__96(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__97(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__98(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__99(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__100(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__101(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__102(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__103(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__104(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__105(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__106(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__107(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__108(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__109(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__110(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__111(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__112(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__113(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__114(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__115(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__116(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__117(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__118(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__119(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__120(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__121(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__122(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__123(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__124(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__125(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__126(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__127(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__128(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__129(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__130(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__131(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__132(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__133(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__134(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__135(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__136(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__137(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__138(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__139(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__140(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__141(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__142(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__143(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__144(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__145(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__146(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__147(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__148(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__149(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__150(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__151(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__152(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__153(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__154(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__155(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__156(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__157(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__158(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__159(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__160(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__161(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__162(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__163(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__164(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__165(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__166(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__167(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__168(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__169(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__170(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__171(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__172(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__173(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__174(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__175(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__176(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__177(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__178(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__179(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__180(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__181(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__182(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__183(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__184(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__185(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__186(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__187(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__188(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__189(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__190(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__191(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__192(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__193(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__194(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__195(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__196(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__197(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__198(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__199(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__200(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__201(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__202(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__203(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__204(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__205(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__206(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__207(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__208(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__209(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__210(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__211(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__212(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__213(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__214(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__215(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__216(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__217(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__218(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__219(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__220(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__221(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__222(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__223(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__224(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__225(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__226(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__227(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__228(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__229(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__230(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__231(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__232(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__233(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__234(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__235(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__236(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__237(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__238(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__239(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__240(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__241(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__242(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__243(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__244(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__245(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__246(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__247(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__248(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__249(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__250(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__251(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__252(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__253(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__254(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__255(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__256(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__257(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__258(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__259(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__260(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__261(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__262(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__263(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__264(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__265(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__266(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__267(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__268(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__269(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__270(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__271(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__272(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__273(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__274(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__275(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__276(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__277(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__278(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__279(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__280(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__281(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__282(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__283(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__284(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__285(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__286(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__287(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__288(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__289(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__290(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__291(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__292(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__293(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__294(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__295(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__296(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__297(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__298(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__299(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__300(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__301(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__302(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__303(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__304(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__305(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__306(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__307(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__308(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__309(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__310(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__311(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__312(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__313(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__314(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__315(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__316(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__317(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__318(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__319(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__320(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__321(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__322(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__323(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__324(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__325(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__326(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__327(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__328(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__329(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__330(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__331(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__332(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__333(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__334(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__335(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__336(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__337(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__338(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__339(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__340(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__341(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__342(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__343(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__344(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__345(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__346(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__347(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__348(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__349(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__350(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__351(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__352(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__353(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__354(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__355(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__356(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__357(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__358(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__359(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__360(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__361(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__362(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__363(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__364(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__365(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__366(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__367(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__368(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__369(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__370(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__371(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__372(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__373(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__374(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__375(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__376(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__377(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__378(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__379(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__380(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__381(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__382(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__383(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__384(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__385(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__386(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__387(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__388(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__389(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__390(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__391(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__392(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__393(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__394(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__395(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__396(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__397(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__398(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__399(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__400(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__401(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__402(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__403(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__404(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__405(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__406(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__407(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__408(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__409(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__410(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__411(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__412(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__413(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__414(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__415(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__416(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__417(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__418(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__419(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__420(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__421(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__422(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__423(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__424(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__425(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__426(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__427(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__428(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__429(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__430(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__431(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__432(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__433(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__434(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__435(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__436(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__437(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__438(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__439(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__440(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__441(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__442(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__443(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__444(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__445(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__446(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__447(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__448(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__449(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__450(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__451(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__452(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__453(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__454(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__455(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__456(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__457(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__458(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__459(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__460(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__461(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__462(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__463(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__464(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__465(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__466(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__467(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__468(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__469(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__470(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__471(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__472(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__473(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__474(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__475(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__476(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__477(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__478(matrix, nrows, ncols, arrOf_result, self); return true; }
if((self.isTo_useSSE == 1) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_none) 
#endif
) { __kt_centrality__compute_for_internalType__479(matrix, nrows, ncols, arrOf_result, self); return true; }

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__480(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__481(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__482(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__483(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__484(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__485(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__486(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__487(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__488(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__489(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__490(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__491(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__492(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__493(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__494(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__495(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__496(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__497(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__498(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__499(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__500(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__501(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__502(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__503(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__504(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__505(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__506(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__507(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__508(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__509(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__510(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__511(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__512(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__513(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__514(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__515(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__516(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__517(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__518(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__519(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__520(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__521(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__522(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__523(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__524(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__525(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__526(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__527(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__528(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__529(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__530(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__531(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__532(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__533(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__534(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__535(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__536(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__537(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__538(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__539(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__540(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__541(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__542(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__543(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__544(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__545(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__546(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__547(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__548(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__549(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__550(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__551(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__552(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__553(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__554(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__555(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__556(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__557(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__558(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__559(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__560(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__561(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__562(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__563(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__564(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__565(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__566(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__567(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__568(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__569(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__570(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__571(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__572(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__573(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__574(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__575(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__576(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__577(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__578(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__579(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__580(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__581(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__582(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__583(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__584(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__585(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__586(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__587(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__588(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__589(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__590(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__591(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__592(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__593(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__594(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__595(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__596(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__597(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__598(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__599(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__600(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__601(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__602(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__603(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__604(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__605(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__606(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__607(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__608(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__609(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__610(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__611(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__612(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__613(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__614(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__615(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__616(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__617(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__618(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__619(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__620(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__621(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__622(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__623(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__624(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__625(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__626(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__627(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__628(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__629(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__630(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__631(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__632(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__633(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__634(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__635(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__636(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__637(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__638(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__639(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__640(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__641(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__642(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__643(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__644(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__645(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__646(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__647(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__648(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__649(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__650(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__651(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__652(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__653(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__654(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__655(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__656(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__657(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__658(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__659(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__660(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__661(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__662(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__663(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__664(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__665(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__666(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__667(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__668(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__669(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__670(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__671(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__672(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__673(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__674(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__675(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__676(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__677(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__678(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__679(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__680(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__681(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__682(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__683(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__684(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__685(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__686(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__687(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__688(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__689(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__690(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__691(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__692(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__693(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__694(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__695(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__696(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__697(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__698(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__699(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__700(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__701(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__702(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__703(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__704(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__705(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__706(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__707(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__708(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__709(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__710(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__711(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__712(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__713(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__714(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__715(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__716(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__717(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__718(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric) 
#endif
) { __kt_centrality__compute_for_internalType__719(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__720(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__721(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__722(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__723(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__724(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__725(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__726(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__727(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__728(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__729(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__730(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__731(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__732(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__733(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__734(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__735(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__736(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__737(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__738(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__739(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__740(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__741(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__742(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__743(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__744(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__745(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__746(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__747(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__748(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__749(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__750(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__751(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__752(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__753(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__754(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__755(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__756(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__757(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__758(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__759(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__760(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__761(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__762(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__763(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__764(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__765(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__766(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__767(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__768(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__769(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__770(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__771(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__772(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__773(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__774(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__775(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__776(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__777(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__778(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__779(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__780(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__781(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__782(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__783(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__784(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__785(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__786(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__787(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__788(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__789(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__790(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__791(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__792(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__793(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__794(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__795(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__796(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__797(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__798(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__799(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__800(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__801(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__802(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__803(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__804(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__805(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__806(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__807(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__808(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__809(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__810(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__811(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__812(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__813(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__814(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__815(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__816(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__817(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__818(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__819(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__820(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__821(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__822(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__823(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__824(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__825(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__826(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__827(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__828(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__829(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__830(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__831(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__832(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__833(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__834(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__835(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__836(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__837(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__838(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 0) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__839(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__840(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__841(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__842(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__843(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__844(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__845(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__846(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__847(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__848(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__849(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__850(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__851(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__852(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__853(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__854(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__855(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__856(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__857(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__858(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__859(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__860(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__861(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__862(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__863(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__864(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__865(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__866(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__867(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__868(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__869(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__870(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__871(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__872(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__873(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__874(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__875(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__876(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__877(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__878(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__879(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__880(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__881(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__882(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__883(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__884(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__885(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__886(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_log) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__887(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__888(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__889(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__890(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__891(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__892(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__893(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__894(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__895(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__896(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__897(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__898(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__899(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__900(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__901(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__902(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__903(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__904(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__905(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__906(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__907(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__908(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__909(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__910(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__911(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__912(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__913(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__914(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__915(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__916(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__917(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__918(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__919(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__920(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__921(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__922(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__923(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__924(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__925(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__926(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__927(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__928(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__929(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__930(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__931(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__932(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__933(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__934(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__935(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__936(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__937(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__938(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__939(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__940(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__941(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__942(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__943(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__944(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__945(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__946(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__947(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__948(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__949(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__950(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_log) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__951(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__952(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__953(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__954(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_logPlussOne) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__955(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__956(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__957(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__958(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"

#if(configure_performance__kt_centrality__buildForSlowCases == 1)
if((self.isTo_useSSE == 0) && (self.isTo_incrementallyUpdateResultVector == 1) && (self.typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_none) && (self.typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_none) && (self.typeOf_eigenVectorType_use == e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#if(configure_performance__kt_centrality__buildForSlowCases == 1)
 && (self.typeOf_performanceTuning == e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
#endif
) { __kt_centrality__compute_for_internalType__959(matrix, nrows, ncols, arrOf_result, self); return true; }

#endif //! for "e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed"
