{
  { //const char *stringOf_measureText = "transpose:naive for type=float and update-sequence = input-->result";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    for(uint i = 0; i < size_of_array; i++) {
      for(uint out = 0; out < size_of_array; out++) {
	matrix_result[out][i] = matrix_input[i][out];
      }
    }    
    const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText_case1, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
    mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#undef __use_Result_to_updateTableComparisonResults
#define __use_Result_to_updateTableComparisonResults 0 //! ie, as we then assume that 'thsio case' hs been udpated for the bucke -in-quesiton.
#endif
  }
  //! --------- new measurement:
  { // const char *stringOf_measureText = "transpose:naive for type=float and update-sequence = result-->input";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    for(uint i = 0; i < size_of_array; i++) {
      for(uint out = 0; out < size_of_array; out++) {
	matrix_input[i][out] = matrix_result[out][i];
      }
    }    
    const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText_case2, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
    mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif
#undef __use_Result_to_updateTableComparisonResults
  }
}
