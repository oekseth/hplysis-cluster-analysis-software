{
  { const char *stringOf_measureText = "dense row-iteration: traversal-time for a 3d-matrix-iteration: test for naive-contius-summation using"; //! For a sparse set:
#define __use_Result_to_updateTableComparisonResults 1
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  sumOf_values += matrix[row_id_out][col_id];
	}
      }
    }
	
    const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
    mapOf_timeCmp_forEachBucket[chunk_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif
#undef __use_Result_to_updateTableComparisonResults
  }	
#define __use_Result_to_updateTableComparisonResults 0
  //! -------------------------------
  { const char *stringOf_measureText = "dense row-iteration(transposed): traversal-time for a 3d-matrix-iteration: test for naive-contius-summation using"; //! For a sparse set:
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  //for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) 
	  {
	    sumOf_values += matrix_transposed[col_id][row_id];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
}
