#include "dense_closestPair_base.h"
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


/* #ifdef NDEBUG */
/* static  */
/* #endif */
//! Update the glboally tiled minbucket-tiles for each of the rows:
void  __forAll_update_globalMin_atBucketIndex(float *mapOf_minTiles_value, float *mapOf_minTiles_value_atRowIndex, float **matrixOf_locallyTiled_minPairs, const uint bucket_id, const uint row_endPosPlussOne, const bool config_useIntrisintitcs, s_rowChunk_t *obj_rowChunk) {

  // FIXME: write correctness-tests 'for this' ... where we validate that each row 'is has a known min-distance'.

  //! Note: in order to get correct valeus for the complete set, we first need to itnalize teh vlaues:
  mapOf_minTiles_value[bucket_id] = FLT_MAX;   mapOf_minTiles_value_atRowIndex[bucket_id] = FLT_MAX;

  //! Note: in [below] we choose to start iteration seperately for each bucket, ie, to test the effect of 'this' optmizaiton.
  uint row_updateIndex = 0;
  // const uint row_endPosPlussOne_intri = macro__get_maxIntriLength_float(row_endPosPlussOne);
  // if(config_useIntrisintitcs) {
  //   for(; row_updateIndex < row_endPosPlussOne_intri; row_updateIndex += VECTOR_FLOAT_ITER_SIZE) {
  //     assert(false); // FIXME: icnldue [below] funciton wehn we have validated correcntess of our "VECTOR_FLOAT_minDistance_eachTile(..)"
  //     __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[row_updateIndex], bucket_id, /*row-id=*/row_updateIndex);
  //   }
  // } 
  { //! Then hadnle the 'rmaineder':
#if(config_useRowChunks_update_globalMin_atBucketIndex == 1)     
    assert(obj_rowChunk);     assert(row_endPosPlussOne > 0);
    uint rowChunk_lastPosPlussOne = 1 + (row_endPosPlussOne/s_rowChunk_getSize_rowBucketEach(obj_rowChunk));
    if(rowChunk_lastPosPlussOne > s_rowChunk_getCnt_rowBuckets(obj_rowChunk)) {
      rowChunk_lastPosPlussOne = s_rowChunk_getCnt_rowBuckets(obj_rowChunk);
    }
    assert(rowChunk_lastPosPlussOne != 0); //! ie, what we expect.
    assert(obj_rowChunk); assert(rowChunk_lastPosPlussOne <= s_rowChunk_getCnt_rowBuckets(obj_rowChunk));
    uint rowChunk_id = 0;
    uint rowChunk_iterate_startPos = 0; uint rowChunk_iterate_endPos = 0;
    uint row_id = 0;

    // assert(false); // FXIEM: validate [”elow] and try to irmpveo correctness of this.

    if(false) { //! First initialize:
      // TODO: cosndier moving [”elow] insdie the fi-calsue.
      rowChunk_iterate_startPos = rowChunk_iterate_endPos;
      rowChunk_iterate_endPos = rowChunk_iterate_startPos + s_rowChunk_getSize_rowBucketEach(obj_rowChunk);
      if(rowChunk_iterate_endPos > row_endPosPlussOne) { rowChunk_iterate_endPos = row_endPosPlussOne; }

      assert(false == 0); //! ie, waht we expect wrt. [”elow]
      if(s_rowChunk_hasNotChanged(obj_rowChunk, bucket_id, rowChunk_id) == false) {
	//! Then iterate through the rows:
	for(row_id = rowChunk_iterate_startPos; row_id < rowChunk_iterate_endPos; row_id++) {
	  const t_float min_distance = matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 
	  //! Update the min-pos:
	  s_rowChunk_setMinPos_ifImproved(obj_rowChunk, bucket_id, rowChunk_id, row_id, min_distance);
	}
	// s_rowChunk_setToChanged(obj_rowChunk, bucket_id, rowChunk_id); //! ie, the result of [above] 'operation':
	//! Update the global-min-structure:
	const t_float min_distance = obj_rowChunk->matrixOf_globalColumnTiles[bucket_id][rowChunk_id];
	if(
	   // FIXME: after we have implemented a to-be-supported 'row-cunk-min' dat-astrucutre ... then update [”elow] ... ie, consider dorpppign [below] 'compelxity'
#ifdef NDEBUG
	   min_distance < mapOf_minTiles_value[bucket_id] //! which is used to increse the performance
#else
	   min_distance <= mapOf_minTiles_value[bucket_id]  //! ie, to reflect our code-tests
#endif
	   )
	  {		
	    mapOf_minTiles_value[bucket_id] = min_distance;
	    mapOf_minTiles_value_atRowIndex[bucket_id] = obj_rowChunk->matrixOf_globalColumnTiles_rowId[bucket_id][rowChunk_id];
	  }
      }
      //! Increment:
      rowChunk_id++;
    }
    assert(obj_rowChunk); assert(rowChunk_lastPosPlussOne <= s_rowChunk_getCnt_rowBuckets(obj_rowChunk));

    for(; rowChunk_id < rowChunk_lastPosPlussOne; rowChunk_id++) {
      // TODO: cosndier moving [”elow] insdie the fi-calsue.
      rowChunk_iterate_startPos = rowChunk_iterate_endPos;
      rowChunk_iterate_endPos = rowChunk_iterate_startPos + s_rowChunk_getSize_rowBucketEach(obj_rowChunk);
      if(rowChunk_iterate_endPos > row_endPosPlussOne) { rowChunk_iterate_endPos = row_endPosPlussOne; }
      //printf("rows[%u ... %u], at %s:%d\n", rowChunk_iterate_startPos, rowChunk_iterate_endPos, __FILE__, __LINE__);

      assert(false == 0); //! ie, waht we expect wrt. [”elow]
      if(s_rowChunk_hasNotChanged(obj_rowChunk, bucket_id, rowChunk_id) == false) {
	//! Then iterate through the rows:
	for(row_id = rowChunk_iterate_startPos; row_id < rowChunk_iterate_endPos; row_id++) {
	  const t_float min_distance = matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 
	  //! Update the min-pos:
	  s_rowChunk_setMinPos_ifImproved(obj_rowChunk, bucket_id, rowChunk_id, row_id, min_distance);
	}
	s_rowChunk_setToChanged(obj_rowChunk, bucket_id, rowChunk_id); //! ie, the result of [above] 'operation':
	//! Update the global-min-structure:
	const t_float min_distance = obj_rowChunk->matrixOf_globalColumnTiles[bucket_id][rowChunk_id];
	if(
	   // FIXME: after we have implemented a to-be-supported 'row-cunk-min' dat-astrucutre ... then update [”elow] ... ie, consider dorpppign [below] 'compelxity'
#ifdef NDEBUG
	   min_distance < mapOf_minTiles_value[bucket_id] //! which is used to increse the performance
#else
	   min_distance <= mapOf_minTiles_value[bucket_id]  //! ie, to reflect our code-tests
#endif
	   ) 
	  {		
	    mapOf_minTiles_value[bucket_id] = min_distance;
	    mapOf_minTiles_value_atRowIndex[bucket_id] = obj_rowChunk->matrixOf_globalColumnTiles_rowId[bucket_id][rowChunk_id];
	  }
      }
    }
    if(false) {printf("\t rowChunk_id=%u/%u, rowChunk_iterate_endPos=%u, row_endPosPlussOne=%u, at %s:%d\n", rowChunk_id, rowChunk_lastPosPlussOne, rowChunk_iterate_endPos, row_endPosPlussOne, __FILE__, __LINE__);}
    assert(rowChunk_lastPosPlussOne == rowChunk_id);
    assert(rowChunk_iterate_endPos == row_endPosPlussOne);
    // assert(row_id == row_endPosPlussOne);
#else //! then we tierate through the complete chunk of rows:
    for(; row_updateIndex < row_endPosPlussOne; row_updateIndex++) {
      //! Note: in [”elow] we update "mapOf_minTiles_value" and "mapOf_minTiles_value_atRowIndex" if "matrixOf_locallyTiled_minPairs[bucket_id]" has a 'better' distance.
      const uint row_id = row_updateIndex;
      const t_float min_distance = matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 


      if(
	 // FIXME: after we have implemented a to-be-supported 'row-cunk-min' dat-astrucutre ... then update [”elow] ... ie, consider dorpppign [below] 'compelxity'
#ifdef NDEBUG
	 min_distance < mapOf_minTiles_value[bucket_id] //! which is used to increse the performance
#else
	 min_distance <= mapOf_minTiles_value[bucket_id]  //! ie, to reflect our code-tests
#endif
	 ) {		
	mapOf_minTiles_value[bucket_id] = min_distance;		
	mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;		
      }
      //__slow_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[row_updateIndex], bucket_id, /*row-id=*/row_updateIndex);
    }
#endif
  }
}


/* #ifdef NDEBUG */
/* static  */
/* #endif */
//! Identify teh shortest paths for each bucket
void __init_minPairs_eachColumnBucket(t_float **distmatrix, const uint nrows, const uint ncols, const uint _cnt_bucketsEachRows, t_float **matrixOf_locallyTiled_minPairs, uint **matrixOf_locallyTiled_minPairs_columnPos, const uint bucket_size) {
   assert(nrows > 0); assert(ncols > 0);
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert(matrixOf_locallyTiled_minPairs_columnPos);
#else
  assert(matrixOf_locallyTiled_minPairs_columnPos == NULL);
#endif


#if(config_FixedNumberOf_bucketsFor_columns != 0)  //! then we simplify the work of the compiler wrt. the 'loop-expansions':
  assert(_cnt_bucketsEachRows == config_FixedNumberOf_bucketsFor_columns);
  const uint cnt_bucketsEachRows = config_FixedNumberOf_bucketsFor_columns;
#else
  const uint cnt_bucketsEachRows = _cnt_bucketsEachRows;
#endif
  assert(cnt_bucketsEachRows > 0);

  //! For each row find the min-valeu in each 'row-tile-chunk':
  for(uint row_id = 0; row_id < nrows; row_id++) {
    uint bucket_id = 0; uint column_pos = 0;
    // if(config_useIntrisintitcs) {
    // 	const uint cntBuckets_intri = macro__get_maxIntriLength_float(cnt_bucketsEachRows-1);
    // 	if(cntBuckets_intri > 0) {
    // 	  for(; bucket_id < cntBuckets_intri; bucket_id += VECTOR_FLOAT_ITER_SIZE) {
    // 	    //! Identify the min-value for each column-range:
    // 	    VECTOR_FLOAT_minDistance_eachTile(matrixOf_locallyTiled_minPairs[i], bucket_id, distmatrix[i], &column_pos, bucket_size);
    // 	  }
    // 	}
    // }
#if(1 == 1)
    //    if(true) 
    { //! then we make use of an non-SSE-version:



#if(config_reWrite_initFunction_identifyBcuekt_notUsingExtraForLoop == 1) //! then we 'redcue' the number of aritmetic operaitons, ie, wrt. idnetificiaotn of min-valeus for bucket-ids.

      uint column_pos = 0;       uint bucket_id = 0; // uint bucket_startPos = 0;
      uint bucket_endPos = bucket_size;
      {
	  matrixOf_locallyTiled_minPairs[row_id][bucket_id] = distmatrix[row_id][column_pos];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = column_pos;
#endif
	  column_pos++;
      }
      

      // FIXME: [below]
      /* const uint __bucket_id_last =  */
      /* const uint lastPos = (ncols / (cnt_bucketsEachRows-1))*bucket_size; */
      
      //#define __get_bucket_id() ({column_pos / bucket_size; })
	
      for(; column_pos < bucket_size; column_pos++) {      
	if(
	   // FIXME: after we have implemented a to-be-supported 'row-cunk-min' dat-astrucutre ... then update [”elow] ... ie, consider dorpppign [below] 'compelxity'
#ifdef NDEBUG
	   distmatrix[row_id][column_pos] < matrixOf_locallyTiled_minPairs[row_id][bucket_id]
#else
	   distmatrix[row_id][column_pos] <= matrixOf_locallyTiled_minPairs[row_id][bucket_id]
#endif
	   ) {
	  matrixOf_locallyTiled_minPairs[row_id][bucket_id] = distmatrix[row_id][column_pos];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = column_pos;
#endif	
	}
      }

      const uint column_pos_lastBucket_minusOne = (bucket_size * (cnt_bucketsEachRows-1));
      for(; column_pos < column_pos_lastBucket_minusOne; column_pos++) {      

	if(column_pos == bucket_endPos) {
	  bucket_id++;
	  //bucket_startPos = bucket_endPos; 
	  bucket_endPos += bucket_size;
	  matrixOf_locallyTiled_minPairs[row_id][bucket_id] = distmatrix[row_id][column_pos];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = column_pos;
#endif
	} else if(
		  // FIXME: after we have implemented a to-be-supported 'row-cunk-min' dat-astrucutre ... then update [”elow] ... ie, consider dorpppign [below] 'compelxity'
#ifdef NDEBUG
		  distmatrix[row_id][column_pos] < matrixOf_locallyTiled_minPairs[row_id][bucket_id]
#else
		  distmatrix[row_id][column_pos] <= matrixOf_locallyTiled_minPairs[row_id][bucket_id]
#endif
		  ) {
	  matrixOf_locallyTiled_minPairs[row_id][bucket_id] = distmatrix[row_id][column_pos];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = column_pos;
#endif	
	}
	//bucket_id = __get_bucket_id();
      }
      
      assert(column_pos == bucket_endPos); //! ei, what we expect.
      //	if(column_pos == bucket_endPos) 
      {
	bucket_id++;
	//bucket_startPos = bucket_endPos; 
	bucket_endPos += bucket_size;
	matrixOf_locallyTiled_minPairs[row_id][bucket_id] = distmatrix[row_id][column_pos];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = column_pos;
#endif
      }



      for(; column_pos < ncols; column_pos++) {
	if(
#ifdef NDEBUG
	   distmatrix[row_id][column_pos] < matrixOf_locallyTiled_minPairs[row_id][bucket_id]
#else
	   distmatrix[row_id][column_pos] <= matrixOf_locallyTiled_minPairs[row_id][bucket_id]
#endif
	   ) {
	//if(distmatrix[row_id][column_pos] < matrixOf_locallyTiled_minPairs[row_id][bucket_id]) {
	  matrixOf_locallyTiled_minPairs[row_id][bucket_id] = distmatrix[row_id][column_pos];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = column_pos;
#endif
	}
      }
      // printf("column_pos=%u/%u, column-lastPos=%u, ... bucket_id=%u/%u, bucket_endPos=%u, bucket_size=%u, at %s:%d\n", column_pos, ncols, lastPos, bucket_id, cnt_bucketsEachRows, bucket_endPos, bucket_size, __FILE__, __LINE__);
      assert(column_pos == ncols);
      assert(bucket_id == (cnt_bucketsEachRows-1));
# else

      for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {
	uint endPosPlussOne = column_pos + bucket_size;
	if(endPosPlussOne > ncols) {endPosPlussOne = ncols;}
	uint m = column_pos;
	assert(column_pos < ncols);
	t_float min_value = distmatrix[row_id][m]; uint foundAt_index = m; m++; //! ie, an 'absoltue max' value
	for(; m < endPosPlussOne; m++) {
	  if(distmatrix[row_id][m] < min_value) {
	    min_value = distmatrix[row_id][m];
	    foundAt_index = m;
	  }
	}
	assert(foundAt_index < ncols);
	matrixOf_locallyTiled_minPairs[row_id][bucket_id] = min_value; column_pos += bucket_size;
	if(false) {printf("[%u][bucket=%u] \t min=%f at column=%u, at %s:%d\n", row_id, bucket_id, min_value, foundAt_index, __FILE__, __LINE__);}
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = foundAt_index;
#endif
      }
#endif //! wrt. (config_reWrite_initFunction_identifyBcuekt_notUsingExtraForLoop == 1)
    }
#else
    {
      assert((bucket_size % VECTOR_FLOAT_ITER_SIZE) == 0); // ie, as [”elow] will otherwise resutl in an error.
      for(; bucket_id < cnt_bucketsEachRows-1; bucket_id++) {	
	matrixOf_locallyTiled_minPairs[row_id][bucket_id] = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(distmatrix[row_id], column_pos + 0, column_pos + bucket_size); column_pos += bucket_size;
      }
      //! Then the last bucket:
      assert(bucket_id == (cnt_bucketsEachRows-1));
      assert((column_pos + bucket_size) == cnt_bucketsEachRows); //! ie, otherwise write a new funciton to handle the 'un-covered tail'.
      assert((bucket_size % VECTOR_FLOAT_ITER_SIZE) == 0); // ie, as [”elow] will otherwise resutl in an error.
      matrixOf_locallyTiled_minPairs[row_id][bucket_id] = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(distmatrix[row_id], column_pos + 0, column_pos + bucket_size);
    }
#endif
  }
}


/* #ifdef NDEBUG */
/* static  */
/* #endif */
//! Identify the min-values for the 'global' column:
void __init_minColumns_eachBucket(const uint nrows, const uint _cnt_bucketsEachRows, t_float *mapOf_minTiles_value, t_float *mapOf_minTiles_value_atRowIndex, t_float **matrixOf_locallyTiled_minPairs, s_rowChunk_t *obj_rowChunk) {
/* #if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1) */
/*   assert(matrixOf_locallyTiled_minPairs_columnPos); */
/* #else */
/*   assert(matrixOf_locallyTiled_minPairs_columnPos == NULL); */
/* #endif */

#if(config_FixedNumberOf_bucketsFor_columns != 0)  //! then we simplify the work of the compiler wrt. the 'loop-expansions':
  assert(_cnt_bucketsEachRows == config_FixedNumberOf_bucketsFor_columns);
  const uint cnt_bucketsEachRows = config_FixedNumberOf_bucketsFor_columns;
#else
  const uint cnt_bucketsEachRows = _cnt_bucketsEachRows;
#endif


  uint row_id = 0; uint rowBucket_id = 0;
  { //! Initiate:
    for(uint bucket_id = 0; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
      const t_float score = matrixOf_locallyTiled_minPairs[row_id][bucket_id];	  
      mapOf_minTiles_value[bucket_id] = score;
      mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;		
# if(config_useRowChunks==1) //! then we update the global-min-row-chunk object:
      assert(obj_rowChunk);
      s_rowChunk_setMinPos(obj_rowChunk, bucket_id, rowBucket_id, row_id, score);
#else
    assert(obj_rowChunk == NULL);
#endif
    }
    row_id++;
  }

  //! Traverse through each of the rows:
  uint rowBucket_endPos = (obj_rowChunk) ? s_rowChunk_getCnt_rowBuckets(obj_rowChunk) : nrows;
  for(; row_id < nrows; row_id++) {
    uint bucket_id = 0; 
    // if(config_useIntrisintitcs) {
    // 	const uint cntBuckets_intri = macro__get_maxIntriLength_float(cnt_bucketsEachRows-1);
    // 	if(cntBuckets_intri > 0) {
    // 	  for(; bucket_id < cntBuckets_intri; bucket_id += VECTOR_FLOAT_ITER_SIZE) {	   
    // 	    //! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics:
    // 	    __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[i], bucket_id, /*row-id=*/i);
    // 	  }
    // 	}
    // }
    //! Then a non-intrisnictic code:
    for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
      //const uint row_id = i;
      const t_float min_distance = matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 

      // FIXME[time] .... consider to compare the exeuciton-time of "<=" and "<" ... and if we 'udpate [”elow] '' then aslo update our correctness-tests at/in our "assert_dense_closestPair.cxx"
      if(
	 // FIXME: after we have implemented a to-be-supported 'row-cunk-min' dat-astrucutre ... then update [”elow] ... ie, consider dorpppign [below] 'compelxity'
#ifdef NDEBUG
	 min_distance < mapOf_minTiles_value[bucket_id] //! which is used to increse the performance
#else
	 min_distance <= mapOf_minTiles_value[bucket_id]  //! ie, to reflect our code-tests
#endif
	 //min_distance <= mapOf_minTiles_value[bucket_id]
	 ) {		
	mapOf_minTiles_value[bucket_id] = min_distance;		
	mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;		
      }
# if(config_useRowChunks == 1) //! then we update the global-min-row-chunk object:
      // printf("\trowBucket_id=%u/%u, at %s:%d\n", rowBucket_id, s_rowChunk_getCnt_rowBuckets(obj_rowChunk), __FILE__, __LINE__);
     s_rowChunk_setMinPos_ifImproved(obj_rowChunk, bucket_id, rowBucket_id, row_id, min_distance);
#endif
      if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u/%u, at %s:%d\n", bucket_id, mapOf_minTiles_value[bucket_id], (uint)mapOf_minTiles_value_atRowIndex[bucket_id], row_id, __FILE__, __LINE__);}
      //__slow_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[i], bucket_id, /*row-id=*/i);
    }
# if(config_useRowChunks == 1) //! then we update the global-min-row-chunk object:
      assert_local(obj_rowChunk);
      if(row_id == rowBucket_endPos) {
	//bucket_startPos = bucket_endPos; 
	rowBucket_endPos += s_rowChunk_getCnt_rowBuckets(obj_rowChunk);
	//! Update the min-pos:
	// printf("rowBucket_id=%u/%u, at %s:%d\n", rowBucket_id, s_rowChunk_getCnt_rowBuckets(obj_rowChunk), __FILE__, __LINE__);
	//s_rowChunk_setMinPos_ifImproved(obj_rowChunk, bucket_id, rowBucket_id, row_id, min_distance);
	// FIXM E: try to describe a better approach than [below]
	rowBucket_id++;
	if(rowBucket_id == s_rowChunk_getCnt_rowBuckets(obj_rowChunk)) {
	  rowBucket_id--;
	}
      }
#endif
/* # if(config_useRowChunks==1) //! then we update the global-min-row-chunk object: */
/*     assert_local(obj_rowChunk); */
/*     if(rowBucket_endPos < s_rowChunk_getCnt_rowBuckets(obj_rowChunk)) { */
/*       assert(bucket_id == cnt_bucketsEachRows); */
/*       assert(bucket_id > 0); */
/*       const uint bucket_id_local = bucket_id-1; */
/*       //! Then a 'safe-guard' update wrt. the min-pos: */
/*       s_rowChunk_setMinPos(obj_rowChunk, bucket_id_local, rowBucket_id, row_id, matrixOf_locallyTiled_minPairs[row_id][bucket_id_local]); */
/*     } */
/* #endif     */
  }
}

