#include "s_allAgainstAll_config.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "mask_api.h"

//! Set to empty the s_allAgainstAll_config structure-object.
void setTo_empty__s_allAgainstAll_config(struct s_allAgainstAll_config *self) {
  assert(self);

  //! 
  //! Configurations wrt. the distance-metric:
  self->configMetric_power  = 3; //! whihc is the default power-valeu to be sued, ie, if any.
  self->weight = NULL;
  self->mask1 = NULL; 
  self->mask2 = NULL;
  self->transposed = false;


  //! -----------------------------
  //!
  //! Heurtitics which are safe to apply wrt. given 'rpcoessed' input-data-set(s):
  self->masksAre_used = e_cmp_masksAre_used_undef;   

  //! -----------------------------
  //!
  //! Advanced configurations wrt. application of a given distance-metric:
  self->typeOf_optimization = e_typeOf_optimization_distance_undef;
  self->isTo_invertMatrix_transposed = true;
  self->CLS = 64;
  self->isTo_use_continousSTripsOf_memory = true;;
  self->typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef;
  self->forNonTransposed_useFastImplementaiton = true;
  self->forNonTransposed_useFastImplementaiton_useSSE = true;

  //! -----------------------------
  //!
  //! Computation of sub-sets:
  self->iterationIndex_2 = UINT_MAX;
  self->s_inlinePostProcess = NULL; //! eg, wrt. the "s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic" struct.
  self->sparseDataResult = NULL;

  //! -----------------------------
  //!
  //! Parameters to seperate communicaiton-steps, eg, wwrt. MPI communication:
  self->inAllAgainstAll__inputIsAlreadySorted_forSpearman = false;


  //! -----------------------------
  //!
  //! Knolwedge the type of memory which has been allcoated in this object:
  self->__internalMemoryAllocation__hasAllocated__masks = false;

  //! -----------------------------
  //! -----------------------------
  //!
  //! Paralle shcuedling-sets.
  self->para_isTo_usePara_ifAboveThreshold = true;
  self->para_cntThreshold_SM_squares = 100; //! ie, the number of "(nrows_1/SM) * (nrows_2/SM) > threshold" blocks, eg for a symemtirc matrix with SM=64 
}

//! De-allcoates an object of type "s_allAgainstAll_config_t"
//s_allAgainstAll_config_t 
void free_memory__s_allAgainstAll_config(s_allAgainstAll_config_t *self) {
  if(self->__internalMemoryAllocation__hasAllocated__masks) {
    assert(self->mask1);
    assert(self->mask2);
    if(self->transposed  == false) {
      free_2d_list_char(&self->mask1, self->nrows); self->mask1 = NULL;
      free_2d_list_char(&self->mask2, self->nrows); self->mask2 = NULL;
    } else {
      free_2d_list_char(&self->mask1, self->ncols); self->mask1 = NULL;
      free_2d_list_char(&self->mask2, self->ncols); self->mask2 = NULL;
    }
    self->__internalMemoryAllocation__hasAllocated__masks = false; //! ei, givne [ªbove] allocation
  }
}

//! Use the proeprties in the s_allAgainstAll_config_t to 'idenitfy and set the correct empty values' in our resultMatrix (oekseth, 06. des. 2016)
//! @remarks resultMatrix is expected to be in diemsions [nrows_1, nrows_2].
void setMatrixTo_defaultEmptyValue__s_allAgainstAll_config_t(const s_allAgainstAll_config_t *self, t_float **resultMatrix, const uint nrows_1, const uint nrows_2) {
  assert(self); assert(resultMatrix); 
  assert(nrows_1 != 0);   assert(nrows_1 != UINT_MAX);
  assert(nrows_2 != 0);   assert(nrows_2 != UINT_MAX);
  //! Get the 'defualt value' to inite a list with:
  const t_float empty_value = get_defaultValue__e_allAgainstAll_SIMD_inlinePostProcess_t(self->typeOf_postProcessing_afterCorrelation);
  //! Set the default value in the given result-matrix:
  for(uint row_id = 0; row_id < nrows_1; row_id++) {
    for(uint col_id = 0; col_id < nrows_2; col_id++) {
      resultMatrix[row_id][col_id] = empty_value;
    }
  }
}
/**
   @brief set the emta-pproeprties to be used in the 'buidling' of the distance-matrix
   @param<self> is the object to update
   @param<isTo_init>  which if set to true intiate the object: should be set to false if proeprteis (not incldued in 'this' function-call) have explcitly (ie, intaitonlally) been set/intiated at an earlier exeuction-point
   @param<mask1> the masks to be used for "data1" in the "compute_allAgainstAll_distanceMetric(..)" function-call
   @param<mask2> the masks to be used for "data2" in the "compute_allAgainstAll_distanceMetric(..)" function-call
   @param<weight> which if set is used to mulipl the column-value with the score in the "weight" table, ie, if not set we assume the value of 'each' is set to "1".
   @param<isTo_transpose>  set to true if the transposed of the correlation-matrix is to be computed
   @param <masksAre_used> described the types of makss to be applied: to be consistent with "mask1" and "mask2" paraemters.
   @remarks 
   -- if "mask1" or "mask2" is intiated (ie, Not set to NULL), then we expect both to be set (eg, 'set' to '1' for both cases);
   -- the "masksAre_used" parameter is used to ncrease the perofrmance of our correlation-comptuation: if the 'maks' paremters are not set while the masksAre_used parameter is set to "e_cmp_masksAre_used_true" or "e_cmp_masksAre_used_undef" we assume implicit masks are to be used whiel if "e_cmp_masksAre_used_false" is set we assuem there is no need for usign/applying masks.
   -- the use of the "weight" parameter results in a slight increase in the xeuction-time, ie, if all columsn have the same weight we reccoment setting the "weight" parameter to "NULL"
 **/
void set_metaMatrix(s_allAgainstAll_config_t *self, const bool isTo_init, char **mask1, char **mask2, float weight[], const bool isTo_transpose, const e_cmp_masksAre_used_t  masksAre_used, const uint nrows, const uint ncols) {
  assert(self);
  if(isTo_init) { setTo_empty__s_allAgainstAll_config(self); }  
  self->nrows = nrows; self->ncols = ncols;
  self->mask1 = mask1;  self->mask2 = mask2;
  self->weight = weight;
  self->transposed = isTo_transpose;
  self->masksAre_used = masksAre_used;
}

//! Set masks of type uint, a data-type which we in this function converts to elements of 1-byte "char", ie, as an optmizaiton heuristic (oekseth, 06. setp. 2016).
void set_masks_typeOfMask_uint__s_allAgainstAll_config(s_allAgainstAll_config_t *self, const bool isTo_init, uint **mask1_uint, uint **mask2_uint, const bool isTo_translateMask_intoImplictDistanceMatrix, const uint nrows, const uint ncols, t_float **data1, t_float **data2, const bool isTo_use_continousSTripsOf_memory) {
  assert(self);
  self->nrows = nrows;   self->ncols = ncols;
  if(isTo_init) { setTo_empty__s_allAgainstAll_config(self); }
  if(isTo_translateMask_intoImplictDistanceMatrix) {
    assert(data1);     assert(data2);
    //! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then update "data" with the "T_FLOAT_MAX" for cases where "mask[row_id][column_id] == 0":
    assert(mask1_uint != NULL);
    s_mask_api_config_t config_mask; setTo_empty__s_mask_api_config(&config_mask);
    applyImplicitMask_fromUintMask__updateData__mask_api(data1, mask1_uint, nrows, ncols, config_mask);
    if(data1 != data2) {
      assert(mask2_uint != NULL);
      s_mask_api_config_t config_mask; setTo_empty__s_mask_api_config(&config_mask);
      applyImplicitMask_fromUintMask__updateData__mask_api(data2, mask2_uint, nrows, ncols, config_mask);
    }
    //graphAlgorithms_distance::update_distanceMatrix_withImplicitMask_fromMaskMatrix(nrows, ncolumns, data, mask_uint);
  } else {
    self->mask1 = convert_booleanMatrix_toCharMatrix__mask_api(nrows, ncols, mask1_uint, isTo_use_continousSTripsOf_memory);
    self->mask2 = convert_booleanMatrix_toCharMatrix__mask_api(nrows, ncols, mask2_uint, isTo_use_continousSTripsOf_memory);
    self->__internalMemoryAllocation__hasAllocated__masks = true;
  }
}

//! Initaites the s_allAgainstAll_config structure-object.
void init__s_allAgainstAll_config(struct s_allAgainstAll_config *self, const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2/* = UINT_MAX*/, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation/* = e_allAgainstAll_SIMD_inlinePostProcess_undef*/, void *s_inlinePostProcess/* = NULL*/, e_cmp_masksAre_used_t  masksAre_used/* = e_cmp_masksAre_used_undef*/, const bool isTo_use_SIMD/* = true*/, const enum e_typeOf_optimization_distance typeOf_optimization /* = e_allAgainstAll_SIMD_inlinePostProcess_undef*/) {
  assert(self);  setTo_empty__s_allAgainstAll_config(self); //! ie, a default set of calls to safe-guard agaisnt futrue non-cosnstent code-udpate-strategies, ie, to (hopefuilly) provkoek' perors which'occure' in most test-cases.
  //! SEt the values:
  self->CLS = CLS; //! ie, default assumption.
  self->isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory; //! ie, as we asusme  Knitting-Toosl 'contuin-memory-allcoation-fucntiosn' are used for memory-allcoation.
  // self->isTo_use_SIMD = isTo_use_SIMD;
  self->iterationIndex_2 = iterationIndex_2;
  self->typeOf_postProcessing_afterCorrelation = typeOf_postProcessing_afterCorrelation;
  self->s_inlinePostProcess = s_inlinePostProcess;
  // self->masksAre_used = masksAre_used;
  self->typeOf_optimization = typeOf_optimization;

  init_useFast__s_allAgainstAll_config(self, masksAre_used);
  self->forNonTransposed_useFastImplementaiton_useSSE = isTo_use_SIMD;

  if( 
     (isTo_use_SIMD) ||
     (typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible_subOptimal_wrt_instructionCache_useFunctionInsteadOfMacro) || 
     (typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible_subOptimal_wrt_instructionCache_seperateMaskAlternatives) || 
     (typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible_subOptimal_targetSpecific_code_instructionRewriting) 
      ) {
    self->forNonTransposed_useFastImplementaiton = true;
  } else {
    self->forNonTransposed_useFastImplementaiton = true;
  }  
}

//! Itnaites the object storing the result as a sparse data-et (oekseth, 06. aug. 2017).
void init__s_allAgainstAll_config__storeSubset(struct s_allAgainstAll_config *self, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult, const uint iterationIndex_2, const e_cmp_masksAre_used_t masksAre_used) {
  assert(sparseDataResult); //! ie, as the call is otherwise (most likely) wrong (oekseth, 06. okt. 2017).
    assert(self);  setTo_empty__s_allAgainstAll_config(self); //! ie, a default set of calls to safe-guard agaisnt futrue non-cosnstent code-udpate-strategies, ie, to (hopefuilly) provkoek' perors which'occure' in most test-cases.
  //! SEt the values:
    self->CLS = 64; //CLS; //! ie, default assumption.
  self->isTo_use_continousSTripsOf_memory = true; //isTo_use_continousSTripsOf_memory; //! ie, as we asusme  Knitting-Toosl 'contuin-memory-allcoation-fucntiosn' are used for memory-allcoation.
  // self->isTo_use_SIMD = isTo_use_SIMD;
  self->iterationIndex_2 = iterationIndex_2;
  self->typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum;
  self->s_inlinePostProcess = NULL; //s_inlinePostProcess;
  self->sparseDataResult = sparseDataResult;
  // self->masksAre_used = masksAre_used;
  self->typeOf_optimization = /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible;

  init_useFast__s_allAgainstAll_config(self, masksAre_used);
  self->forNonTransposed_useFastImplementaiton_useSSE = /*isTo_use_SIMD=*/true;
  self->forNonTransposed_useFastImplementaiton = true;
}

//! @return false if we do Not need to use masks (oekseth, 06. sept. 2016).
bool s_allAgainstAll_config__needTo_useMask_evaluation(const s_allAgainstAll_config_t *self) {
  assert(self);
  return (self->masksAre_used != e_cmp_masksAre_used_false); //! ie, if the 'no-mask' proeprty is epxlcitly set, then we assume that masks are Not needed to be seud (oekseth, 06. sept. 2016).
}


//! An ANSI C constructor to itnaite the object:
s_allAgainstAll_config_t get_init_struct_s_allAgainstAll_config() {
  s_allAgainstAll_config_t obj;
  setTo_empty__s_allAgainstAll_config(&obj);


  //! @return the result:
  return obj;
}

