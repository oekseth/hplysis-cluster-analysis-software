{
  assert(self);
  assert(thread_id < self->cnt_cores);
  assert(block_id < self->mapOf_blockId_rows_innerSize);
  assert(self->mapOf_blockId_startEachBlock_rows);
  assert(self->mapOf_blockId_startEachBlock_rows[thread_id]);
  assert(self->mapOf_blockId_startEachBlock_rows[thread_id][0] <= self->mapOf_blockId_rows_innerSize); //! ie, wrt. the max-number-of-elements.
  assert(scalarStart_row_id != NULL);   assert(scalarStart_col_id != NULL);
  assert(scalarEnd_row_id != NULL);   assert(scalarEnd_col_id != NULL);
  //! -----------
  *scalarStart_row_id = self->mapOf_blockId_startEachBlock_rows[thread_id][1+block_id];
  *scalarStart_col_id = self->mapOf_blockId_startEachBlock_cols[thread_id][1+block_id];
  //! -------
  *scalarEnd_row_id = *scalarStart_row_id + self->mapOf_blockId_startEachBlock_rows_size[thread_id][1+block_id];
  *scalarEnd_col_id = *scalarStart_col_id + self->mapOf_blockId_startEachBlock_cols_size[thread_id][1+block_id];
  //! @return the number of chunks in the given block-id assicated to thread_id:
  //return mapOf_blockId_rows_innerSize[thread_id][1+ block_id]; //! ie, whwhere the first 'psotion' is belived to hold the max-number of insrted elements  
}
