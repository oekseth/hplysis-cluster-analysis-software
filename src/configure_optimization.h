#ifndef configure_optimization_h
#define configure_optimization_h

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */

//! Simplify the configuration of our optmized "mine" software-implementaiton.
#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <math.h>
#include <float.h>
#include <assert.h>  

/**
   @file configure_optimization
   @brief provide compiler-logics and 'live-lgoics' to configure/change the flow of exeuctions in the software-process.
   @author Ole Kristian Ekseth (oekseth, 06. july 2016).
   @remarks the 'configuraiton-swithces' whitch we provide is sued to both chagne the type of lgocis wrt. inferences (ie, the degree of correctenss) and wrt. exeuction-time-optmilaity, ie, may be used to introspect upon the feasiblity of 'such software' wrt. computer-hardware-logics.
 **/

typedef unsigned int uint;

#ifndef MATLAB_MEX_FILE
#ifndef __cplusplus
typedef char bool;
#define true 1
#define false 0

#endif
#endif

//! @reoudn a float to 3-digit accuracy
//! @remakrs used to assess accurayc and ocrrectness of different impelmetantion-strategies wrt. optmizaiotn.
//#define debug_get_floatValue_approx(val1) (floorf(val1 * 1000) / 1000)
#define debug_get_floatValue_approx(val1) (floorf(val1 * 10.0) / 10) //! where "floorf(..)" rounds downwards



#define default_value 1

//#define function_prefix //! which si used in the MINE cofnigruations. 


#ifndef default_value_avoidErrors_nan
#define default_value_avoidErrors_nan 1 //! whre result is that we 'avoid' divide-by-zero' 'causing' "INFINITY" and "NAN" <-- note in thsi context that if this parameter is set to '0' while "default_value == 0", the implicaiton is errnrous resutls, errors arising from division by and on "0", ie, the 'naive' Mine-implement not only has a slwoewr performance, through is also incorrect.
#endif

//! ************************************************************************************
#ifndef VECTOR_CONFIG_USE_FLOAT
#define VECTOR_CONFIG_USE_FLOAT 1 //! which is used in our "def_intri.h": alterantive the "double" 'atomic' data-type is used <-- seems like 'this' reduces the comptuation-time with a factor of "2x" <-- FIXME[jc]: any expalnaitons for 'why such' wrt. non-SSE implementaiton?
#endif


#ifndef macroConfig_use_posixMemAlign
#define macroConfig_use_posixMemAlign 1 //! which is used in our "def_memAlloc.h" <-- seems not to 'affect' the overall exeuction-time.
#endif
//! -----------
#ifndef optmize_usePReComptued_floats
#define optmize_usePReComptued_floats 1  //! where the latter is used in our "fast_log.h"
#endif

#ifndef optmize_usePReComptued_floats_accessMemory
#define optmize_usePReComptued_floats_accessMemory 1 //! which is used to 'dsintinguish' between memory-accesses and a mathical approach for appxomiation. <-- if "0" is used, then accuracy increased with two extra deciamsl, though the perofrmance is reduced by a factor of 2.1x
#endif

#ifndef optmize_use_logValues_fromLocalTable
#define optmize_use_logValues_fromLocalTable 1 //! for which we comptue ... "log(m*n) = log(m) + log(n)" and "log(m*/n) = log(m) - log(n)" ... store the results in a new list "c_log[i] = log(c[i])" and "c_log_inv[i] = 1/c_log[i];" and "cumhist_log[i][s] = log(cumhist[i][s]);" <-- tids-kostnad vil kunne redusess fra 'random access' til 'linear access', dvs. ....??...
#endif

#ifndef optmize_use_logValues_sizeOf_logTable_zeroOnes
#define optmize_use_logValues_sizeOf_logTable_zeroOnes 1000*10 //! for which we comptue ... "log(m*n) = log(m) + log(n)" and "log(m*/n) = log(m) - log(n)" ... store the results in a new list "c_log[i] = log(c[i])" and "c_log_inv[i] = 1/c_log[i];" and "cumhist_log[i][s] = log(cumhist[i][s]);" <-- tids-kostnad vil kunne redusess fra 'random access' til 'linear access', dvs. ....??...
#endif


#ifndef optmize_use_multInsteadOf_div
#define optmize_use_multInsteadOf_div 1 //! then we use 'muliplicaiton' instead of divsion where feasible: in comptuer-hardware muliplicaiotn goes approx. 2x faster than muliplicaiton. <-- wrt. exeuction-time this paramter manages to explain a signfinat protion of the exeuction-time difference 'which we have achived'. <-- from "https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=10,13,3385,3278,3278,176,3574,3666,3690,3620,4571,4761,3819,4768,5520,3780,2056,3638,2056&techs=MMX,SSE,SSE2,SSE3,SSSE3&text=_div" we observe that the latency of "division" on Intel's "Sany Bridge" compute-hardware-architecture is "14" while "5" for "muliplication", ie, an approx. tiem-difference of "3x".  ... <-- observe that the 'replacement' of "div" with "mult" expalins a dsignficant portion of the non-logairtm-related otpmization.
#endif


#ifndef optmize_2d_loop_makeUseOf_symmetricProperty 
#define optmize_2d_loop_makeUseOf_symmetricProperty 1 //! for which we then manage a 2x-reduciton in exeuction-time.
#endif

#ifndef optmize_parallel_2dLoop_useFast
#define optmize_parallel_2dLoop_useFast 1 //! a macro-parameter which is used if 'rows' are to be compared in parallel
#endif

#ifndef optmize_parallel_2dLoop_useFast_slowScheduling
#define optmize_parallel_2dLoop_useFast_slowScheduling 0 //! which is a permtuation of "optmize_parallel_2dLoop_useFast" demonstrating the effects of naive non-otpmial scheudling of parallel threads.
#endif

#ifndef configure_parallel_2dLoop_symmetric_useFastScheduling
#define configure_parallel_2dLoop_symmetric_useFastScheduling 1
#endif

#ifndef optmize_parallel_1dLoop_useFast
#define optmize_parallel_1dLoop_useFast default_value //! a macro-parameter which is used if 'features' are to be compared in parallel
#endif
#ifndef optmize_parallel_1dLoop_useFast_scheduleFast
#define optmize_parallel_1dLoop_useFast_scheduleFast default_value //! which if set impleis that we make use of 'fast scheduling' in our computations.
#endif


#ifndef SPARSEVEC_CONFIG_DATATYPE
//! Then we use a default configuration wrt. the float-data-type to use (oekseth, 06. june 2016)
// FIXME: if we choose to use oterh 'valeus' than 0|1 for "VECTOR_CONFIG_USE_FLOAT" ... then update the macros which use the "VECTOR_CONFIG_USE_FLOAT" .. to an if-else macro-clause
#define SPARSEVEC_CONFIG_DATATYPE 1 //! then use 'float' instead of "int" <-- which if set to "0"  results in the SSE-intrinistics to 'be a bottlneck rather than an improvemtn'.
#endif


/* #if(VECTOR_CONFIG_USE_FLOAT == 1) */
/* #define __lcoalConfig__useSparseVecInCmp 0 */
/* #else */
/* #define __lcoalConfig__useSparseVecInCmp 1 */
/* #endif */
#if(SPARSEVEC_CONFIG_DATATYPE == 1)
#define __lcoalConfig__useSparseVecInCmp 0
#else
#define __lcoalConfig__useSparseVecInCmp 1
#endif


#ifndef SPARSEVEC_CONFIG_DATATYPE_counts_useInt
#define SPARSEVEC_CONFIG_DATATYPE_counts_useInt 1 //! which when compared to using "floatws" is observed to result in a perofmrance-difference of 1.03x (ie, where the use of 'itns' reduce the exeuction-time).
#define t_sparseVec_counts int
#define allocate_1d_list_sparseVec_counts allocate_1d_list_int 
#else //! then we use 'floats' wrt. teh counters: from copmuter-hardware-arhcietecutre we knwo that the use of flaots will go faster if the numbers are 'mainly used' wrt. muliplcaiton and divison (ie, as the copmtuer-ahrdware is optmized wrt. the latter).
#define t_sparseVec_counts t_sparseVec
#define allocate_1d_list_sparseVec_counts allocate_1d_list_sparseVec
#endif

#ifndef optmize_use_SSE_memoryAccess_aligned
#define optmize_use_SSE_memoryAccess_aligned 1 //! then we make usie of macros to specify aligned memory-access for SSE intrinistics, an option which results in a perormance-difference of !1.03x".
#endif

#if optmize_use_SSE_memoryAccess_aligned == 1
#define local_VECTOR_FLOAT_LOAD VECTOR_FLOAT_LOAD
#define local_SPARSEVEC_LOAD SPARSEVEC_LOAD
#else
#define local_VECTOR_FLOAT_LOAD VECTOR_FLOAT_LOADU
#define local_SPARSEVEC_LOAD SPARSEVEC_LOADU
#endif


#ifndef defaultCompilation_alwaysUse_SSE
#define defaultCompilation_alwaysUse_SSE 1 //! ie, we we then 'include this' as a terminal-input-parameter
#endif

#ifndef defaultCompilation_alwaysUse_SSE_slow
#define defaultCompilation_alwaysUse_SSE_slow 0 //! ie, we we then 'include this' as a terminal-input-parameter
#endif

#if (defaultCompilation_alwaysUse_SSE == 0) && (defaultCompilation_alwaysUse_SSE_slow == 1)
#warning "!!\t seems like an inocnsistnecy in the cofniguration, ie, please make the macro-variables in question cosnsitent: for questions please contact the devleoper at [oekseth@gmail.com]. (oekseth, 06. july 2016)."
#endif
//! Validate that the memory-allcoation-rpocedre is set correctly:
#if (optmize_use_SSE_memoryAccess_aligned == 0) //! then we expect SSE not to be used.
#if (defaultCompilation_alwaysUse_SSE == 0) //! ie, for which we have a 'reason' for a seg-fault, ie, due to out expectiaotn of 'choosing' to use memory-access-routines such as the "_mm_load_ps(..)" Invel-SSE calls (eosketh, 06. july 2016).
#warning "!!\t seems like an inocnsistnecy in the configuration, ie, please make the macro-variables in question cosnsitent: for questions please contact the devleoper at [oekseth@gmail.com]. (oekseth, 06. july 2016)."
#endif
#endif

#ifndef mineAccuracy_threshold_minValue_float_useAbs
//#define mineAccuracy_threshold_minValue_float_useAbs 0
#define mineAccuracy_threshold_minValue_float_useAbs 1
#endif
//! Then define the data-tyep based on [ªbove]
#if (mineAccuracy_threshold_minValue_float_useAbs == 1) //! ie, to use a cosnistent approach for 'setting' the min-data-value.
#define mineAccuracy_threshold_minValue_float T_FLOAT_MIN_ABS
#else
#define mineAccuracy_threshold_minValue_float T_FLOAT_MIN
#endif

#ifndef mineAccuracy_computeLog_useAbs
#define mineAccuracy_computeLog_useAbs 1
#endif


#ifndef typeOf_implementation_rapidMine //! where the latter 'option' is used to 'enalbe' the use of the software described in "http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3921152/"
#define typeOf_implementation_rapidMine 0 //! ie,a s we assuem that it would be wrong ti use a differnet article than the article pbulisehd in the 'orignal' Mine Science article, ie, for the results to always reflect 'the undelrying truth', ie, for which the Rapuid-Mine artile is not prooved to 'cover the case'.
#endif


//! ************************************************************************************

#ifndef __lcoalConfig__useSparseVecInCmp
#define __lcoalConfig__useSparseVecInCmp 0
#endif


//! ------------------------------------------------------
//! start: functions which are assicted to these caribalbes are explcitly changed through optimizations introduced/described by Ole Kristian Ekseth (oekseth)

//! -----
//! Note: Through [below] approach is nto expected to imrpvoe the fpromrance of the 'major code-parts', there are two otehr reason for why we prefer 'the memory-continous approach': (a) cosniderably shorter time to allcoate memory (ie, diminishing a 'fixed' time-complexity-code-part) and (b) performance is less dpednent on topology of the input-matrix, ie, an overall better performance on average (an optimization which is small, though will otpimize every part of the code).
static int use_continousSTripsOf_memory = true;
static int isTo_use_continousSTripsOf_memory(void) {
  return use_continousSTripsOf_memory;
}
static void set_to_use_continousSTripsOf_memory(const uint value) {
  assert(value <= 1);
  use_continousSTripsOf_memory = value;
}
//! -----
//! Note: when investigng the memory-access patterns of the "mine" software-tool we observe that a "Fortran" matrix-access-ptattern is used, ie, in cotnrast to the "row-wise" matrix-allocations 'perofmred' in this program-library. The latter observation aruges for a 'tranpostion' of the authors approach: through the use of a 'tranposed approach' (if 'activated' through [below] configurations) we expect the perofmance-difference/time-differnece/speed-up-factor to be in/of range ["2x"...">9x"].

/**
   @remarks time-difference for two vectors of size:
   [400]: "1.3"x from "0m0.106s" VS "0m0.106s" ... and "0m0.111s" for 'all-out optmization'
   [800]: ""x from "0m0.352s" VS "" ... and "0m0.287s" for 'all-out optmization'
   [1200]: ""x from "0m0.622s" VS "" ... and "" for 'all-out optmization'
   [1600]: ""x from "0m0.891s" VS "" ... and "" for 'all-out optmization'
   [3200]: ""x from "0m2.412s" VS "" ... and "" for 'all-out optmization'
   [10,000]: "1.25"x from "0m11.003s" VS "0m8.819s" ... and "0m9.390s" for 'all-out optmization'
   [40,000]: "1.96"x from "1m10.776s" VS "" ... and "0m56.742s" for 'all-out optmization'
   [80,000]: ""x from "2m55.527s" VS "" ... and "" for 'all-out optmization'
   [120,000]: "1.23"x from "5m0.340s" VS "4m3.122s" ... and "" for 'all-out optmization'
   []: ""x from "" VS "" ... and "" for 'all-out optmization'
   []: ""x from "" VS "" ... and "" for 'all-out optmization'
 **/
static int use_oldDataScheme = true;
static int isTo_use_oldDataScheme(void) {
  return use_oldDataScheme;
}
static void set_to_use_oldDataScheme(const uint value) {
  assert(value <= 1);
  use_oldDataScheme = (int)value;
}
//! A 'general' parameter which tests the effect of using 'generalisedd' code-re-writinig to reduce the exeuciton-time
static int use_codeRewritingTo_reduceExeuctionTime = true;
static int isTo_use_codeRewritingTo_reduceExeuctionTime(void) { 
  return use_codeRewritingTo_reduceExeuctionTime;
}
static void set_use_codeRewritingTo_reduceExeuctionTime(const uint value) {
  assert(value <= 1);
  use_codeRewritingTo_reduceExeuctionTime = (int)value;
}
#if (defaultCompilation_alwaysUse_SSE == 0)
//! A 'general' parameter which tests the effect of using intrinistics to reduce teh code-exeuction-time: for details, see our "def_intri.h"
static int use_codeRewritingTo_vectorOptimization_1dLoops = true;
static int isTo_use_codeRewritingTo_vectorOptimization_1dLoops(void) {
  return use_codeRewritingTo_vectorOptimization_1dLoops;
}
static void set_use_codeRewritingTo_vectorOptimization_1dLoops(const uint value) {
  assert(value <= 1);
  use_codeRewritingTo_vectorOptimization_1dLoops = (int)value;
}
#else //! then we expect h SSE to always 'be in use'.
#define isTo_use_codeRewritingTo_vectorOptimization_1dLoops(void) ({1;}) //! ie, then always return true: we expect 'this' to simplify the compilaers optmizaiton-efforts
#endif
//! A 'general' parameter which tests the effect of using intrinistics to reduce teh code-exeuction-time: for details, see our "def_intri.h"
/* static int use_codeRewritingTo_vectorOptimization_2dLoops = true; */
/* static int isTo_use_codeRewritingTo_vectorOptimization_2dLoops() { */
/*   return use_codeRewritingTo_vectorOptimization_2dLoops; */
/* } */
/* static void set_use_codeRewritingTo_vectorOptimization_2dLoops(const uint value) { */
/*   assert(value <= 1);   */
/*   use_codeRewritingTo_vectorOptimization_2dLoops = (int)value; */
/* } */

/* //! A simple function to identify the chunks of equal size in array. */
/* static void getChunksOf_sortedArray_equalValueChunks(const t_float *dx, const uint n, t_float *arrOf_clusterFirstIndex, uint *arrOf_clusterFirstIndex_size) { */
/*   assert(arrOf_clusterFirstIndex); */
/*   memset(arrOf_clusterFirstIndex, 0, sizeof(t_float) * n); */
/*   t_float prev_value = dx[0]; */
/*   uint counter = 0; */
/*   for(uint i = 0; i < n; i++) { */
/*     if(dx[i] != prev_value) { */
/*       arrOf_clusterFirstIndex[*arrOf_clusterFirstIndex_size] = i; counter++; */
/*     } */
/*     prev_value = prev_value; //! ie, update. */
/*   } */
/*   *arrOf_clusterFirstIndex_size = counter; */
/* } */
//! Used to test/evaluate the time-efficency of using 'memory-tiling' in matrix-operations.
static int use_memoryCacheEfficentTiling = false;
static int isTo_use_memoryCacheEfficentTiling(void) {
  return use_memoryCacheEfficentTiling;
}
static void setTo_use_memoryCacheEfficentTiling(const uint value) {
  assert(value >= 0);  assert(value <= 1);
  use_memoryCacheEfficentTiling = (int)value;
}
//! Test the time-signficance of avoiding muliple memory-allcoations (ie, given the time-cost of the latter): use a preAllocated_memory to simplify the 'transfer' of previously used memory-chunks.
static int use_preAllocatedMemory = true;
static int isTo_use_preAllocatedMemory(void) {
  return use_preAllocatedMemory;
}
static void setTo_use_preAllocatedMemory(const int value) {
  assert(value >= 0);  assert(value <= 1);
  use_preAllocatedMemory = value;
}


//! Load the files:
#include "def_intri.h"
#include "fast_log.h" //! which provide access to a 'fast-log' macro


#endif



/* #ifndef configure_optimization_h */
/* #define configure_optimization_h */

/* #include <stdlib.h> */
/* #include <stdio.h>  */
/* #include <string.h> */
/* #include <math.h> */
/* #include <float.h> */
/* #include <assert.h> */


/* //! ************************************************************************************ */
/* #ifndef VECTOR_CONFIG_USE_FLOAT */
/* #define VECTOR_CONFIG_USE_FLOAT 1 //! which is used in our "def_intri.h": alterantive the "double" 'atomic' data-type is used <-- seems like 'this' reduces the comptuation-time with a factor of "2x" <-- FIXME[jc]: any expalnaitons for 'why such' wrt. non-SSE implementaiton? */
/* #endif */
/* #ifndef macroConfig_use_posixMemAlign */
/* #define macroConfig_use_posixMemAlign 1 //! which is used in our "def_memAlloc.h" <-- seems not to 'affect' the overall exeuction-time. */
/* #endif */
/* //! ----------- */
/* #ifndef optmize_usePReComptued_floats */
/* #define optmize_usePReComptued_floats 1  //! where the latter is used in our "fast_log.h" */
/* #endif */

/* #ifndef optmize_usePReComptued_floats_accessMemory */
/* #define optmize_usePReComptued_floats_accessMemory 1 //! which is used to 'dsintinguish' between memory-accesses and a mathical approach for appxomiation. <-- if "0" is used, then accuracy increased with two extra deciamsl, though the perofrmance is reduced by a factor of 2.1x */
/* #endif */

/* #ifndef optmize_use_logValues_fromLocalTable */
/* #define optmize_use_logValues_fromLocalTable 1 //! for which we comptue ... "log(m*n) = log(m) + log(n)" and "log(m*\/n) = log(m) - log(n)" ... store the results in a new list "c_log[i] = log(c[i])" and "c_log_inv[i] = 1/c_log[i];" and "cumhist_log[i][s] = log(cumhist[i][s]);" <-- tids-kostnad vil kunne redusess fra 'random access' til 'linear access', dvs. ....??...    */
/* #endif */

/* #ifndef optmize_use_multInsteadOf_div */
/* #define optmize_use_multInsteadOf_div 1 //! then we use 'muliplicaiton' instead of divsion where feasible: in comptuer-hardware muliplicaiotn goes approx. 2x faster than muliplicaiton. <-- wrt. exeuction-time this paramter manages to explain a signfinat protion of the exeuction-time difference 'which we have achived'. <-- from "https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=10,13,3385,3278,3278,176,3574,3666,3690,3620,4571,4761,3819,4768,5520,3780,2056,3638,2056&techs=MMX,SSE,SSE2,SSE3,SSSE3&text=_div" we observe that the latency of "division" on Intel's "Sany Bridge" compute-hardware-architecture is "14" while "5" for "muliplication", ie, an approx. tiem-difference of "3x".  ... <-- observe that the 'replacement' of "div" with "mult" expalins a dsignficant portion of the non-logairtm-related otpmization. */
/* #endif */

/* #ifndef SPARSEVEC_CONFIG_DATATYPE */
/* //! Then we use a default configuration wrt. the float-data-type to use (oekseth, 06. june 2016) */
/* // FIXME: if we choose to use oterh 'valeus' than 0|1 for "VECTOR_CONFIG_USE_FLOAT" ... then update the macros which use the "VECTOR_CONFIG_USE_FLOAT" .. to an if-else macro-clause */
/* #define SPARSEVEC_CONFIG_DATATYPE 1 //! then use 'int' instead of "float" <-- which if set to "0"  results in the SSE-intrinistics to 'be a bottlneck rather than an improvemtn'. */
/* #endif */

/* #ifndef optmize_use_SSE_memoryAccess_aligned */
/* #define optmize_use_SSE_memoryAccess_aligned 1 //! then we make usie of macros to specify aligned memory-access for SSE intrinistics, an option which results in a perormance-difference of !1.03x". */
/* #endif */

/* #if optmize_use_SSE_memoryAccess_aligned == 1 */
/* #define local_VECTOR_FLOAT_LOAD VECTOR_FLOAT_LOAD */
/* #define local_SPARSEVEC_LOAD SPARSEVEC_LOAD */
/* #else */
/* #define local_VECTOR_FLOAT_LOAD VECTOR_FLOAT_LOADU */
/* #define local_SPARSEVEC_LOAD SPARSEVEC_LOADU */
/* #endif */

/* //! ************************************************************************************ */

/* typedef unsigned int uint; */
/* #define true 1 */
/* #define false 0 */




/* //! ------------------------------------------------------ */
/* //! start: functions which are assicted to these caribalbes are explcitly changed through optimizations introduced/described by Ole Kristian Ekseth (oekseth) */

/* //! ----- */
/* //! Note: Through [below] approach is nto expected to imrpvoe the fpromrance of the 'major code-parts', there are two otehr reason for why we prefer 'the memory-continous approach': (a) cosniderably shorter time to allcoate memory (ie, diminishing a 'fixed' time-complexity-code-part) and (b) performance is less dpednent on topology of the input-matrix, ie, an overall better performance on average (an optimization which is small, though will otpimize every part of the code). */
/* static int use_continousSTripsOf_memory = true; */
/* #define isTo_use_continousSTripsOf_memory() (use_continousSTripsOf_memory) */
/* #define set_to_use_continousSTripsOf_memory(value) ({ assert(value <= 1);   use_continousSTripsOf_memory = value; }) */
/* //! ----- */
/* //! Note: when investigng the memory-access patterns of the "mine" software-tool we observe that a "Fortran" matrix-access-ptattern is used, ie, in cotnrast to the "row-wise" matrix-allocations 'perofmred' in this program-library. The latter observation aruges for a 'tranpostion' of the authors approach: through the use of a 'tranposed approach' (if 'activated' through [below] configurations) we expect the perofmance-difference/time-differnece/speed-up-factor to be in/of range ["2x"...">9x"].  */

/* /\** */
/*    @remarks time-difference for two vectors of size: */
/*    [400]: "1.3"x from "0m0.106s" VS "0m0.106s" ... and "0m0.111s" for 'all-out optmization' */
/*    [800]: ""x from "0m0.352s" VS "" ... and "0m0.287s" for 'all-out optmization' */
/*    [1200]: ""x from "0m0.622s" VS "" ... and "" for 'all-out optmization' */
/*    [1600]: ""x from "0m0.891s" VS "" ... and "" for 'all-out optmization' */
/*    [3200]: ""x from "0m2.412s" VS "" ... and "" for 'all-out optmization' */
/*    [10,000]: "1.25"x from "0m11.003s" VS "0m8.819s" ... and "0m9.390s" for 'all-out optmization' */
/*    [40,000]: "1.96"x from "1m10.776s" VS "" ... and "0m56.742s" for 'all-out optmization' */
/*    [80,000]: ""x from "2m55.527s" VS "" ... and "" for 'all-out optmization' */
/*    [120,000]: "1.23"x from "5m0.340s" VS "4m3.122s" ... and "" for 'all-out optmization' */
/*    []: ""x from "" VS "" ... and "" for 'all-out optmization' */
/*    []: ""x from "" VS "" ... and "" for 'all-out optmization' */
/*  **\/ */
/* static int use_oldDataScheme = true; */
/* #define isTo_use_oldDataScheme() (use_oldDataScheme) */

/* #define set_to_use_oldDataScheme(value) ({assert(value <= 1); use_oldDataScheme = (int)value;}) */

/* //! A 'general' parameter which tests the effect of using 'generalisedd' code-re-writinig to reduce the exeuciton-time */
/* static int use_codeRewritingTo_reduceExeuctionTime = true; */
/* #define isTo_use_codeRewritingTo_reduceExeuctionTime() (use_codeRewritingTo_reduceExeuctionTime) */
/* #define set_use_codeRewritingTo_reduceExeuctionTime(value) ({assert(value <= 1); use_codeRewritingTo_reduceExeuctionTime = (int)value;}) */
/* //! A 'general' parameter which tests the effect of using intrinistics to reduce teh code-exeuction-time: for details, see our "def_intri.h" */
/* static int use_codeRewritingTo_vectorOptimization_1dLoops = true; */
/* #define isTo_use_codeRewritingTo_vectorOptimization_1dLoops() (use_codeRewritingTo_vectorOptimization_1dLoops) */
/* #define set_use_codeRewritingTo_vectorOptimization_1dLoops(value) ({assert(value <= 1); use_codeRewritingTo_vectorOptimization_1dLoops = (int)value;}) */

/* //! A 'general' parameter which tests the effect of using intrinistics to reduce teh code-exeuction-time: for details, see our "def_intri.h" */
/* /\* static int use_codeRewritingTo_vectorOptimization_2dLoops = true; *\/ */
/* /\* static int isTo_use_codeRewritingTo_vectorOptimization_2dLoops() { *\/ */
/* /\*   return use_codeRewritingTo_vectorOptimization_2dLoops; *\/ */
/* /\* } *\/ */
/* /\* static void set_use_codeRewritingTo_vectorOptimization_2dLoops(const uint value) { *\/ */
/* /\*   assert(value <= 1);   *\/ */
/* /\*   use_codeRewritingTo_vectorOptimization_2dLoops = (int)value; *\/ */
/* /\* } *\/ */

/* /\* //! A simple function to identify the chunks of equal size in array. *\/ */
/* /\* static void getChunksOf_sortedArray_equalValueChunks(const t_float *dx, const uint n, t_float *arrOf_clusterFirstIndex, uint *arrOf_clusterFirstIndex_size) { *\/ */
/* /\*   assert(arrOf_clusterFirstIndex); *\/ */
/* /\*   memset(arrOf_clusterFirstIndex, 0, sizeof(t_float) * n); *\/ */
/* /\*   t_float prev_value = dx[0]; *\/ */
/* /\*   uint counter = 0; *\/ */
/* /\*   for(uint i = 0; i < n; i++) { *\/ */
/* /\*     if(dx[i] != prev_value) { *\/ */
/* /\*       arrOf_clusterFirstIndex[*arrOf_clusterFirstIndex_size] = i; counter++; *\/ */
/* /\*     } *\/ */
/* /\*     prev_value = prev_value; //! ie, update. *\/ */
/* /\*   } *\/ */
/* /\*   *arrOf_clusterFirstIndex_size = counter; *\/ */
/* /\* } *\/ */
/* //! Used to test/evaluate the time-efficency of using 'memory-tiling' in matrix-operations. */
/* static int use_memoryCacheEfficentTiling = false; */
/* #define isTo_use_memoryCacheEfficentTiling() (use_memoryCacheEfficentTiling) */
/* #define setTo_use_memoryCacheEfficentTiling(value) ({assert(value >= 0);  assert(value <= 1); use_memoryCacheEfficentTiling = (int)value;}) */

/* //! Test the time-signficance of avoiding muliple memory-allcoations (ie, given the time-cost of the latter): use a preAllocated_memory to simplify the 'transfer' of previously used memory-chunks. */
/* static int use_preAllocatedMemory = true; */
/* #define isTo_use_preAllocatedMemory() (use_preAllocatedMemory) */
/* #define setTo_use_preAllocatedMemory(value) ({assert(value >= 0);  assert(value <= 1);use_preAllocatedMemory = value;}) */


/* //! Load the files: */
/* #include "def_intri.h" */
/* #include "fast_log.h" //! which provide access to a 'fast-log' macro */


/* #endif //! EOF */
