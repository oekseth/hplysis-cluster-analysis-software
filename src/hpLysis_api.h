#ifndef hpLysis_api_h
#define hpLysis_api_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hpLysis_api
   @brief provide a simplfied set of functiosn to access Knitting-Tools hpLysis software   (oekseth, 06. des. 2016). 
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016). 
 **/
#include "kt_metric_aux.h"
#include "kt_api.h" //! which (among others) is used for comptatuion of corrleation-atmrix in oru [”elow] exampels.
#include "kt_distance_cluster.h"
#include "kt_matrix_cmpCluster.h" //! which is used for evlauating and applicaiton fo clsuter-comparing in our "scalar_CCMclusterSimilarity__s_hpLysis_api_t(..)" (oekseth, 06. feb. 2017)
#include "kt_randomGenerator_vector.h" //! which is used to facitlate the integrationi and specificaton of start-ndoes and differnet ranodmziaotn-strategies wrt. iteraitve algorithms (oekseth, 06. feb. 2017).
#include "kt_clusterResult_condensed.h"
#include "kt_clusterAlg_dbScan.h"
#include "kt_clusterAlg_mcl.h"
#include "kd_tree.h" //! ie, to supprot "kd-tree" (eg, in DB-SCAN) (oekseth, 06. jul. 2017).

#ifdef __cplusplus
extern "C" 
#endif

//! Global variables:
//extern int hpLysis_api_version; //!< which is used to simplify the detection of future sigicant API-changes.  


// #include "e_kt_correlationFunction.h"

/* typedef enum test_enum { */
/*   test_enum_a, */
/*     test_enum_b, */
/*     test_enum_c */
/* } test_enum_t; */

/* static void test_func(test_enum_t test) { */
/*   printf("test=%d\n", test); */
/* } */
/* void test_func_2(test_enum_t test); */

/**
   @enum e_hpLysis_clusterAlg
   @brief enumerate differnet cluster-types which we support (oesketh, 06. de3s. 2016).
   @remarks for simplcity we only enuermate a subset of the cluster-tyeps supported in KnittingTools--hpLysis clsuter-api kt_api cluster-analsys-API, ie, for details please see the diffenret functions, and assicated coucmentiaotn, in our kt_api.h and assicated/related examples.
 **/
typedef enum e_hpLysis_clusterAlg {
  //! ------------------------------------------------------------
  //! Note: to 'run' the "kmeans++" algorithm a random-enum such as "e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss" sshoudl be used: the latter is defined in our "math_generateDistribution.h" and (among others) used in our "s_hpLysis_api::randomConfig::config_init" object (of type "s_kt_randomGenerator_vector__config_t" defined in our "kt_randomGenerator_vector.h" (oekseth, 06. feb. 2017).
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, //! an implemtnation which is inpsired by the poor implementaions found in the work of ("https://algorithmicthoughts.wordpress.com/2013/07/26/machine-learning-mini-batch-k-means/" and "https://github.com/siddharth-agrawal/Mini-Batch-K-Means")
  //! ------------------------------------------------------------
    e_hpLysis_clusterAlg_disjoint, //! ie, 'use disjoitnness of regions to idneitfy clusters': a fast appraoch which depends on masking of corrleation-valeus to be accurate.
    e_hpLysis_clusterAlg_disjoint_DBSCAN, //! where altter uses the defualt dBSCAN merging strategy.
    e_hpLysis_clusterAlg_disjoint_mclPostStrategy, //! which si a pemrtuation of "e_hpLysis_clusterAlg_disjoint_mclPostStrategy" where the MCL-strategy of merging 'non-core-vertices' (in a seprate post-sep after disjtoint-fores-tdineticiaotns) is applied/used (oekseth, 06. jul. 2017).
    e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, //! which is a permtuation of our "e_hpLysis_clusterAlg_disjoint" where 
    e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, //! which is a permtuation of our "e_hpLysis_clusterAlg_disjoint" where 
    e_hpLysis_clusterAlg_disjoint_MCL, //! ie, the CML-algorithm (oesketh, 06. jul. 2017).
  //! --------------------
    e_hpLysis_clusterAlg_disjoint_kdTree,
    e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN,
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG, //! use disjoint-forest to find the k-clsuters and therafter apply AVG-cluster-appraoch.
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank, //! use disjoint-forest to find the k-clsuters and therafter apply rank-cluster-appraoch.
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid, //! use disjoint-forest to find the k-clsuters and therafter apply medoid-cluster-appraoch.
  //! --------------------
  e_hpLysis_clusterAlg_kCluster__SOM, //! ie, use Self-Organsiing Maps (SOMs) for clustering
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_kruskal_hca, //! where we construct a "Minimum Spanning Tree" (MST), and then 'assumes' that sorted order assiated to the constructed tree' describes a hioerarhccial cluster (ie, an itnerpetaiton often seen in research). 
  e_hpLysis_clusterAlg_kruskal_fixed, //! where we set a 'max-count' wrt. the clusters
  e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, //! wherew we use a user-specifeid (or defualt) Cluster Comparison Metric (CCM) to idneityf the min-max-clusters thresholds.
  //! ------------------------------------------------------------
  //! Note: [below] is sued as reference-frames wrt. clustering-acccuracy, eg, to comapre/elvuate the correctness/prediocnt-acucryac of clsutierng (oesketh, 06. jul. 2017).
    e_hpLysis_clusterAlg_random_best,
    e_hpLysis_clusterAlg_random_worst,
    e_hpLysis_clusterAlg_random_average,
  //! ------------------------------------------------------------
  //e_hpLysis_clusterAlg_,
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_undef, //! ie, the number of elemnets 'in this'.
} e_hpLysis_clusterAlg_t;



static void get_enumBasedOn_configuration_scalar__tmp(const char *stringOf_enum, uint *scalar_result) {
  assert(stringOf_enum); assert(scalar_result);
  *scalar_result = (uint)get_enumBasedOn_configuration(stringOf_enum);
}


//! @return true if the algorithm in quesiton 'produce' a HCA-tree (either 'compelte' or broekena/parital, where an exampkle of the latter is "e_hpLysis_clusterAlg_kruskal_fixed_and_CCM")(oekseth, 06. feb. 2017)
static const bool isOf_type_HCA__e_hpLysis_clusterAlg_t(const e_hpLysis_clusterAlg_t alg_type) {
  if(
     (alg_type == e_hpLysis_clusterAlg_HCA_single)
     || (alg_type == e_hpLysis_clusterAlg_HCA_max)
     || (alg_type == e_hpLysis_clusterAlg_HCA_average)
     || (alg_type == e_hpLysis_clusterAlg_HCA_centroid)
     || (alg_type == e_hpLysis_clusterAlg_kruskal_hca)
     || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed)
     || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM)
     ) {return true;}
  else {return false;}
}
//! @return true if the algorithm in quesiton is senstivie/dependenct on an iteriatve-radnom step (when dieniefyign cluster-results) (oekseth, 06. feb. 2017)
static const bool isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(const e_hpLysis_clusterAlg_t alg_type) {
  if(
     (alg_type == e_hpLysis_clusterAlg_kCluster__AVG)
     || (alg_type == e_hpLysis_clusterAlg_kCluster__rank)
     || (alg_type == e_hpLysis_clusterAlg_kCluster__medoid)
     || (alg_type == e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch)
     || (alg_type == e_hpLysis_clusterAlg_kCluster__SOM)
     ) {return true;}
  else {return false;}
}

//! @return the string-represntiaotn of the "e_hpLysis_clusterAlg_t" enum (oesketh, 06. jan. 2017).
static const char *get_stringOf__e_hpLysis_clusterAlg_t(const e_hpLysis_clusterAlg_t enum_id) {
  //! Note[depend]: if [”elow] is udpated, then remember to update our: "hpLysisVisu_server.js" and our "hpLysis_metrics.pm" (oekseth, 06. apr. 2017).
  if(enum_id == e_hpLysis_clusterAlg_kCluster__AVG) {return "algorithm::k-means::avg";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__rank) {return "algorithm::k-means::rank";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__medoid) {return "algorithm::k-means::medoid";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_DBSCAN) {return "algorithm::DBSCAN";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint) {return "algorithm::hpCluster";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_mclPostStrategy) {return "algorithm::hpCluster::postMCL";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds) {return "algorithm::hpCluster::CCM::hpCluster";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds) {return "algorithm::hpCluster::CCM::dbScan";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_MCL) {return "algorithm::hpCluster::MCL";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_kdTree) {return "algorithm::hpCluster::kdTree";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN) {return "algorithm::hpCluster::kdTree::CCM";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch) {return "algorithm::k-means::altAlg::miniBatch";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG) {return    "algorithm::hpCluster::k-means::avg";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank) {return   "algorithm::hpCluster::k-means::rank";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid) {return "algorithm::hpCluster::k-means::medoid";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__SOM) {return "algorithm::k-means::SOM";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_HCA_single) {return "algorithm::HCA::single";}
  else if(enum_id == e_hpLysis_clusterAlg_HCA_max) {return "algorithm::HCA::max";}
  else if(enum_id == e_hpLysis_clusterAlg_HCA_average) {return "algorithm::HCA::average";}
  else if(enum_id == e_hpLysis_clusterAlg_HCA_centroid) {return "algorithm::HCA::centroid";}
  //! --------------------
  //! Note: An example-case wrt. our "Kruskal" MST implemntaiton conserns how we use MINE-based rank-trhesholds 'as defualt', and thereafter apply (if requested/set by a user) a mask-threshold to 'icnrease sigicnace' of higly simlair vertices, ie, to provide 'as arugment' (to our Kruskal) to different matrices: (a) a matrix used for building an MST and (b) a second amtrix used to ivnestgiate clsuter-correctness (eg, a non-masked MINE-simlairty-comptued matrix).
  else if(enum_id == e_hpLysis_clusterAlg_kruskal_hca) {return "algorithm::Kruskal::HCA";}
  else if(enum_id == e_hpLysis_clusterAlg_kruskal_fixed) {return "algorithm::Kruskal::fixed";}
  else if(enum_id == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) {return "algorithm::Kruskal::fixed::CCM";}
  //! ------------------------------------------------------------
  //! Note: [below] is sued as reference-frames wrt. clustering-acccuracy, eg, to comapre/elvuate the correctness/prediocnt-acucryac of clsutierng
  else if(enum_id == e_hpLysis_clusterAlg_random_best) {return "algorithm::random::best";}
  else if(enum_id == e_hpLysis_clusterAlg_random_worst) {return "algorithm::random::worst";}
  else if(enum_id == e_hpLysis_clusterAlg_random_average) {return "algorithm::random::average";}
  //! ------------------------------------------------------------
  //! --------------------
  //else if(enum_id == e_hpLysis_clusterAlg_) {return "algorithm::";}
  //! --------------------
  else {return "algorithm::<undef>";}

  //!
  //! An erorr:
  fprintf(stderr, "!!\t At this exec-point no naems were found for enum_id=%u, ie, we need add support for this: please forward this emssage to the sneior delveoper at [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
  assert(false);
  return NULL;
}
//! @return the string-represntiaotn of the "e_hpLysis_clusterAlg_t" enum (oesketh, 06. jan. 2017).
static const char *get_stringOf__short__e_hpLysis_clusterAlg_t(const e_hpLysis_clusterAlg_t enum_id) {
  if(enum_id == e_hpLysis_clusterAlg_kCluster__AVG) {return "k_avg";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__rank) {return "k_rank";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__medoid) {return "k_medoid";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_DBSCAN) {return "DBSCAN";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint) {return "hpCluster";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_mclPostStrategy) {return "hpCluster_MCL_post";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds) {return "hpCluster_ccm_dbSCAN";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds) {return "hpCluster_ccm_hpCluster";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_MCL) {return "hpCluster__MCL";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_kdTree) {return "hpCluster__kdTree";}
  else if(enum_id == e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN) {return "hpCluster__kdTree__CCM";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch) {return "miniBatch";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG) {return    "hpCluster_k_avg";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank) {return   "hpCluster_k_rank";}
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid) {return "hpCluster_k_medoid";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_kCluster__SOM) {return "som";}
  //! --------------------
  else if(enum_id == e_hpLysis_clusterAlg_HCA_single) {return "hca_single";}
  else if(enum_id == e_hpLysis_clusterAlg_HCA_max) {return "hca_max";}
  else if(enum_id == e_hpLysis_clusterAlg_HCA_average) {return "hca_average";}
  else if(enum_id == e_hpLysis_clusterAlg_HCA_centroid) {return "hca_centroid";}
  //! --------------------
  //! Note: An example-case wrt. our "Kruskal" MST implemntaiton conserns how we use MINE-based rank-trhesholds 'as defualt', and thereafter apply (if requested/set by a user) a mask-threshold to 'icnrease sigicnace' of higly simlair vertices, ie, to provide 'as arugment' (to our Kruskal) to different matrices: (a) a matrix used for building an MST and (b) a second amtrix used to ivnestgiate clsuter-correctness (eg, a non-masked MINE-simlairty-comptued matrix).
  else if(enum_id == e_hpLysis_clusterAlg_kruskal_hca) {return "Kruskal_hca";}
  else if(enum_id == e_hpLysis_clusterAlg_kruskal_fixed) {return "Kruskal_k";}
  else if(enum_id == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) {return "Kruskal_CCM";}
  //! ------------------------------------------------------------
  //! Note: [below] is sued as reference-frames wrt. clustering-acccuracy, eg, to comapre/elvuate the correctness/prediocnt-acucryac of clsutierng (oesketh, 06. jul. 2017).
  else if(enum_id == e_hpLysis_clusterAlg_random_best) {return "random_best";}
  else if(enum_id == e_hpLysis_clusterAlg_random_worst) {return "random_worst";}
  else if(enum_id == e_hpLysis_clusterAlg_random_average) {return "random_average";}
  //! --------------------
  //else if(enum_id == e_hpLysis_clusterAlg_) {return "algorithm::";}
  //! --------------------
  else {return "undef";}

  //!
  //! An erorr:
  fprintf(stderr, "!!\t At this exec-point no naems were found for enum_id=%u, ie, we need add support for this: please forward this emssage to the sneior delveoper at [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
  assert(false);
  return NULL;
}


//! @return the enum-represntiaotn of the input-string (oekseth, 06. mar. 2017)
//! @remarks use our get_stringOf__e_hpLysis_clusterAlg_t(..)" in order to be 'back-compatible' with the 'expected string-input'.
static const e_hpLysis_clusterAlg_t get_enumOf__e_hpLysis_clusterAlg_t(const char *cmp) {
  assert(cmp); assert(strlen(cmp));
  for(uint i = 0; i < e_hpLysis_clusterAlg_undef; i++) {
    const char *str = get_stringOf__e_hpLysis_clusterAlg_t((e_hpLysis_clusterAlg_t)i);
    //printf("\t\t(cmp::long::metric=%u) \"%s\" VS \"%s\", at %s:%d\n", i, cmp, str, __FILE__, __LINE__);
    if(str && strlen(str)) {
      if(strlen(str) == strlen(cmp)) {
	if(0 == strncasecmp(str, cmp, strlen(cmp))) {
	  return (e_hpLysis_clusterAlg_t)i; //! ie, as hte string is then found
	}
      }
    }
    //! ----- 
    str = get_stringOf__short__e_hpLysis_clusterAlg_t((e_hpLysis_clusterAlg_t)i);
    // printf("\t\t(cmp::short::metric=%u) \"%s\" VS \"%s\", at %s:%d\n", i, cmp, str, __FILE__, __LINE__);
    if(str && strlen(str)) {
      if(strlen(str) == strlen(cmp)) {
	if(0 == strncasecmp(str, cmp, strlen(cmp))) {
	  return (e_hpLysis_clusterAlg_t)i; //! ie, as hte string is then found
	}
      }
    }
  }
  //! A fall-back:
  return e_hpLysis_clusterAlg_undef;
}
//! Update the scalar_Result with the cluster-enum-id in queisotn (oekseth, 06. arp. 2017).
static void get_enumOf__e_hpLysis_clusterAlg_t__retVal(const char *cmp,  uint *scalar_result) {
  assert(cmp); assert(strlen(cmp)); assert(scalar_result);
  *scalar_result = (uint)get_enumOf__e_hpLysis_clusterAlg_t(cmp);
}

/* static void test_valyueByRef_int(int *scalar_result) { */
/*   *scalar_result = 1; */
/* } */
/* static void test_valyueByRef_t_float(t_float *scalar_result) { */
/*   *scalar_result = 0.1; */
/* } */
/* static void test_valyueByRef_double(double *scalar_result) { */
/*   *scalar_result = 0.1; */
/* } */
/* static void test_valyueByRef_uint(uint *scalar_result) { */
/*   *scalar_result = 1; */
/* } */

/* static void test_add(uint x, uint y, uint *result) { */
/*   *result = x + y; */
/* } */
/* static void test_add_int(int x, int y, int *result) { */
/*   *result = x + y; */
/* } */
/* static int test_sub(int *x, int *y) { */
/*   return *x+*y; */
/* } */

/**
   @struct s_hpLysis_api__config__valueMask
   @brief idneitfies the value-masks to be applied (ie, if any) (oekseth, 06. feb. 2017).
 **/
typedef struct s_hpLysis_api__config__valueMask {
  t_float threshold_min;   
  t_float threshold_max;
  bool isTo_applyValueThresholds; 
  bool isTo_adjustToRanks_beforeTrehsholds_forRows; //! used in combination with "isTo_applyValueThresholds": if set to true implies that we 'replace' the valeus (in each row) by the rows rank, ie, before we 'apply' the filter.  
} s_hpLysis_api__config__valueMask_t;

//! @return an intiated s_hpLysis_api__config__valueMask_t
static s_hpLysis_api__config__valueMask_t init__s_hpLysis_api__config__valueMask_t() {
  s_hpLysis_api__config__valueMask_t self;
  self.threshold_min = T_FLOAT_MAX;   self.threshold_max = T_FLOAT_MAX;
  self.isTo_applyValueThresholds = false;
  self.isTo_adjustToRanks_beforeTrehsholds_forRows = false;
  //self.threshold_min = 0.7;   self.threshold_max = T_FLOAT_MAX;
  //! @return
  return self;
}

/**
   @struct s_hpLysis_api__config
   @brief provide configuraiotn-options to our "s_hpLysis_api" hpLysis algorithm-exeucitons-interface (oekseht, 06. feb. 2017).
 **/
typedef struct s_hpLysis_api__config {
  //! Gernalizeed cofnigurations:
  s_kt_correlationMetric_t corrMetric_prior;
  bool corrMetric_prior_use; //! which if set to false implies that we do Not apply/use a 'prior' ocrrelaiton-metric, ie, for which we 'then' applty/use the data-set 'as-is'.
  //! ----
  s_kt_correlationMetric_t corrMetric_insideClustering;
  s_hp_distanceCluster_t corrMetric_insideClustering__mergeConf;
  s_kt_clusterAlg_mcl_t clusterConfig_mcl; //! which is used in MCL-clustering
  s_kt_api_config_t clusterConfig;  
  //! ----
  //!
  //! Configurations Specific for 'masking'
  s_hpLysis_api__config__valueMask_t config__valueMask;
  s_hpLysis_api__config__valueMask_t config__valueMask__afterSimMatrix; //! which is explcitly needed/used in: (a) "kruskal" to 'enalbe' the evlauation of CCM-metrics on a non-sparse-data-set; (b) "*disjoint*" 'cases' to evlauate/handle/idneitfy 'filtered cases'.
  //! ----
  //!
  //! Configurations when "nclsuters" is not specified though 'kmeans' is requested', ie, where we 'are then intersted in inferrign the k-count of clsuters based on a valeu-trehshold'.
  //t_float opt_kCount__disjointThresholdFilter__scoreRelative_min;   t_float opt_kCount__disjointThresholdFilter__scoreRelative_max;
  uint opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__min; //! ie, the max-count in a clsuter which we 'evaluate'
  uint opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__max; //! ie, the max-count in a clsuter which we 'evaluate'
  //! ----
  //!
  //! Algorithm-cofngiruation: "Self Orgnaising Maps" (SOM)
  uint algConfig_SOM__nxgrid;
  uint algConfig_SOM__nygrid;
  uint algConfig_SOM__initTau;
  //! ----
  //!
  s_kt_clusterAlg_config__ccm_t config__CCM; //! which is used for metircs which 'makes use of' CCM-based and dynamic k-means covnergence-criteira, eg, wrt our "kruskal" MST implementaiton (oekseth, 06. fe.b 2017)
  //! !Note: use the "isTo_pply__CCM" param in "config__CCM__dynamicKMeans" to 'switch on or Off' the default-CCM-appliciton: the latter param may be set to "false" to avoid the peformance-penalty assicated to CCM-calculation (oekseht, 06. mar. 2017).
  s_kt_clusterAlg_config__ccm_t config__CCM__dynamicKMeans; //! which for {HCA} and {random} is used as a 'post-step to idneitfy the 'best-fitting' Kmneas algoritm, while for {kmeans} is used to 're-start' each cofnigruaiton-option (oekseth, 06. mar. 2017).  
  // TODO[article::performance]: investgaite the accurayc-effect of uisng our "config__CCM__dynamicKMeans__useHCAPre_step_forKMeans_ncluster" (oesketh, 06. mar. 2017).
  bool config__CCM__dynamicKMeans__useHCAPre_step_forKMeans_ncluster; //! which 'descides' if we are to use a 'HCA-pre-step' to idneitfy the optinmal number fo clsuters (for the case where "nclsuters" [<=1, == UINT_MAX]. The latter option is motivated by the 'extremly long' exeuciton-time assicated to 'investigating all possible cndatne-n-cluster-options
  //bool config__CCM__dynamicKMeans__isToUse; 
  //! ------------------------
  //! 
  s_kd_searchConfig_config_t kdConfig;
  //! ------------------------
  //! 
  //! Configurations of random-cluster-computaitons (oesketh, 06. jul. 2017):
  uint config__randomClusterIndetificaiton__cntRandomIterations; //! which is the number of random-iteraiotns to be used in the clsutering (oekseth, 06. jul. 2017).
} s_hpLysis_api__config_t;

//! @return an itnalized verison of our "s_hpLysis_api__config_t" object (oekseth, 06. feb. 2017)
s_hpLysis_api__config_t init__s_hpLysis_api__config_t(const char *stringOf_exportResult__heatMapOf_clusters, s_kt_longFilesHandler_t *fileHandler);
/* //! De-allcoates the s_hpLysis_api__config_t object (oekseth, 06. feb. 2017). */
/* void free__s_hpLysis_api__config_t(s_hpLysis_api__config_t *self); */

/**
   @struct s_hpLysis_api
   @brief provide a genralizzed cofniguraitons wrt. comptuation of clsuters (oekseth, 06. des. 2016).
 **/
typedef struct s_hpLysis_api {
  s_hpLysis_api__config_t config;
  //! ----
  s_kt_randomGenerator_vector_t randomConfig; //! which is used to facitlate the integrationi and specificaton of start-ndoes and differnet ranodmziaotn-strategies wrt. iteraitve algorithms (oekseth, 06. feb. 2017).
  //!
  const char *stringOfResultPrefix__exportCorr__prior; //! ie, for 'export' for the 'matris used as input to the cluster-algorithms
  //! ----
  //!
  //! Result-objects: the type of result-object dpeends uppn the "e_hpLysis_clusterAlg_t" aprameter which is used.
  s_kt_matrix_t matrixResult__inputToClustering; //! which hold teh matrix which was used as input to clustering.
  s_kt_clusterAlg_fixed_resultObject_t obj_result_kMean;
  s_kt_matrix_t obj_result_kMean_clusterId_columnValue;
  s_kt_clusterAlg_SOM_resultObject_t obj_result_SOM;
  s_kt_clusterAlg_hca_resultObject_t obj_result_HCA;
  s_kt_matrix_t obj_result_clusterId_columnValue;
  //! ---------
  //! Specify the type of previosu configureations used, eg, where the "objectHistory__prev__alg_type" is utsed/tilized in our "export__hpLysis_api(..)" export-function.
  e_hpLysis_clusterAlg_t objectHistory__prev__alg_type;
  //!
  //! Result of 'dynamic k-means':
  uint dynamicKMeans__best__nClusters;
  t_float dynamicKMeans__best__score;
  
  // s_kt_matrix_t *objectHistory__prev__inputMatrix;
} s_hpLysis_api_t;

//! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
void hpLysis__globalInit__kt_api();
//! A 'glboal memory-de-allcoation-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
void hpLysis__globalFree__kt_api();


/**
   @brief Intalize the s_hpLysis_api_t to default values:
   @param <stringOf_exportResult__heatMapOf_clusters> which if Not set to NULL implies that we use the stirng to genrate export-files.
   @param <fileHandler> which if Not set to NULL is used to 'generate' abbbreviated file-name
   @return an intilized object.
**/
s_hpLysis_api_t setToEmpty__s_hpLysis_api_t(const char *stringOf_exportResult__heatMapOf_clusters, s_kt_longFilesHandler_t *fileHandler);
//! De-allocates the "s_hpLysis_api_t" object.
void free__s_hpLysis_api_t(s_hpLysis_api_t *self);


/**
   @brief apply filtering and comptue a correlation-matrix using Knitting-Tools approx. 1,000,000 different correlationi-metrics (oekseth, 06. des. 2016).
   @param <self> hold the configuraitons and result-data, eg, wrt. post-filters, correlation-metrics, export-proeprties, and result-structure: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_1> hold the first input-matrix
   @param <obj_2> hold the second input-matrix: if "obj_2 == NULL" then we compute a [obj_1]x[obj_2] matrix, ie, an 'result-request-appraoch' which is often of interest in research.
   @param <isTo_applyPreFilters> which if set to false implies that we 'ignore' the filter-property-cofngiruations in the "self" object.
   @return true upon success.
   @remakrs the "1,000,000" count 'comes' from the fact that (a) Knitting-Tools--hpLysis supports approax. 1,000 uniue correlation-emtics and (b) hpLysis facitlates the support for suign correlaiton-emtics 'both before and inside a given clustering' (an applciaiton-case which is not specifically supported by otehrs software-tools).
   @remarks is used to consturct a correlation-matrix, a a matrix which may be used tor purposes such as heatmap-visualziaiton, clustering, as 'nptu to your own algorithm', etc. In this function the result is stored in the 'itnernal' "self->matrixResult__inputToClustering" "s_hpLysis_api_t" variable (ie, from which the latter result may be accessed).
**/
bool filterAmdCorrelate__hpLysis_api(s_hpLysis_api_t *self, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2, const bool isTo_applyPreFilters);

/* typedef enum e_hpLysis_matrixCmp__scalar { */
/*   e_hpLysis_matrixCmp__scalar_ */
/*   e_hpLysis_matrixCmp__scalar_ */
/* } e_hpLysis_matrixCmp__scalar_t; */

/**
   @brief apply filtering and comptue a scalar (based on a spcifid correlation-matrix) using Knitting-Tools approx. 1,000,000 different correlationi-metrics (oekseth, 06. jan. 2017).
   @param <self> hold the configuraitons and result-data, eg, wrt. post-filters, correlation-metrics, export-proeprties, and result-structure: expected Not to be intaliseed (though Not set to NULL);
   @param <clusterCmp_method_id> descirbes the type of cluster-comparison to be used/applied
   @param <obj_1> hold the first input-matrix
   @param <obj_2> hold the second input-matrix: if "obj_2 == NULL" then we compute a [obj_1]x[obj_2] matrix, ie, an 'result-request-appraoch' which is often of interest in research.
   @param <isTo_applyPreFilters> which if set to false implies that we 'ignore' the filter-property-cofngiruations in the "self" object.
   @return true upon success.
   @remakrs a permtaution of "filterAmdCorrelate__hpLysis_api(..)" where we use the "scalar__getSimliarty__betwenMatrices__kt_distance_cluster(..)" funciton (in our "kt_distance_cluster.c") to 'idneitfy' a scalar valeu descrinb ghe simlairy between two matrices. 
   @remarks is used as an interface  to generate/construct a scalar to describe simlairty between two matrices
**/
bool scalar__filterAmdCorrelate__hpLysis_api(s_hpLysis_api_t *self, const e_kt_clusterComparison_t clusterCmp_method_id, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2, const bool isTo_applyPreFilters, t_float *scalar_result);



/**
   @brief provide an interface to different permtuatiosn of k-means clustering (oekseth, 06. des. 2016).
   @param <self> hold the configuraitons and result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <alg_type> is the algorithm which we are to use for clustering
   @param <obj_1> hold the input-matrix
   @param <nclusters> is the number of clusters to sub-divide the data-set into.   
   @param <npass> is the number of 'passes' to be sued in the k-means algorithm
   @return true upon success.
   @remarks if "e_hpLysis_clusterAlg_kCluster__SOM" is used then we assume "nygrid=nclusters and nxgrid=min(10*nclusters, 0.2*obj_1->nrows)"
**/
bool cluster__hpLysis_api(s_hpLysis_api_t *self, const e_hpLysis_clusterAlg_t alg_type, const s_kt_matrix_t *obj_1, const uint nclusters, const uint npass);
/**
   @brief improve k-means-clsutering through idneitficiaotn of intial centroids (oekseth, 06. feb. 2017).
   @param <self> hold the configuraitons and result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <alg_type> is the algorithm which we are to use for clustering
   @param <obj_1> hold the input-matrix
   @param <nclusters> is the number of clusters to sub-divide the data-set into.   
   @param <npass> is the number of 'passes' to be sued in the k-means algorithm
   @param <config__firstPass> is the 
   @param <alg_type_firstPass> is 'intial-pass-algorithm', ie, used to dienitfy the centers to be used 'in the first pass' wrt. randomness-investgiation'
   @return true upon success.
   @remarks extends "cluster__hpLysis_api(..)", ie, for specific detials see latter.
   @remarks idea: an appraoch to idenfiy start-ndoes in k-menas clustering through applicioant of an 'intial cluster-pass'. An example fo the latter is to idneitfy start-ndoes usign an intal HCA-algorithm (defined through the "alg_type_firstPass" input-parameter) and therafter comptue the clsuter 'though the latter'. 
   @remarks pointless: if you choose a clsuterinig-algorithm in "alg_type" which is Not a of an "e_hpLysis_clusterAlg_kCluster_*" algorithm-type, then this call is pointless
**/
bool cluster__kMeansTwoPass__hpLysis_api(s_hpLysis_api_t *self, const e_hpLysis_clusterAlg_t alg_type, const s_kt_matrix_t *obj_1, const uint nclusters, const uint npass, const s_hpLysis_api__config_t config__firstPass, const e_hpLysis_clusterAlg_t alg_type_firstPass);

/**
   @brief 'extracts' a 'k-means-result-set' using the "ccm_enum" CCM to idneitfy the ideal 'nclsuters' cluster-count (oekseth, 06. mar. 2017).
   @return true upon success.
**/
bool hcaPostStep__dynamicK____hpLysis_api(s_hpLysis_api_t *self, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum);

/**
   @enum e_hpLysis_export_formatOf_t
   @brief defines the different ytpes of data-exports in hpLysis (oekseth, 06. jan. 2017).
   @remarks proivde coencptual framewrok/itnroduction for the different export-rotuiens we provide in our gneeralized "hplysis_api" API-front-end: to condense the large numbe rof differetn exprot-routiens into a subset which may eaisliy be understood by most users. Differently told, the below list (of exprot-ofrmat-types) may esaily be extended usign the rich export-rotuiens found in both the hpLysis software and Knitting-Tools software.
   @remarks for detials wrt. the differente enum-attributes please see the comments 'inside' the enum-deifntions (in below).
 **/
typedef enum e_hpLysis_export_formatOf {
  //! ------------------------------------------------------ 
  //! Export: the simliarity/correlation matrix (used as input to the clustering).
  e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, //! ie, export usign text: use Tabular Seperatedd Valeus (TSV) as a syntax
  e_hpLysis_export_formatOf_similarity_matrix__syntax_JS, //! ie, export using JavaScript;
  e_hpLysis_export_formatOf_similarity_matrix__syntax_JSON, //! ie, export using JavaScript--JSON;
  //! ------------------------------------------------------ 
  //! Export: deivaiotn-attributes assiotaed tot he 'simlairty-matrix used as input to the clsutering', ie, 'condensed proerpties' instead of 'the complete list of cell-data'.
  //! Note: for details wrt. teh 'deviaiton-exprot' see our "s_matrix_deviation_skewnessDescription" object defined in our "matrix_deviation.h" (oekseth, 06. jan. 2016).
  e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_TSV, //! ie, use format=TSV.
  e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_Tex, //! ie, use format=latex
  //! ------------------------------------------------------ 
  //! Export: the cluster-results.
  //! Note: givne differences in the cluster-algorithms the result-set (adn result-format) will depend upon the type of clsutering-algorithm used, ie, wrt. the "e_hpLysis_clusterAlg_t alg_type" used in the export-routine.
  e_hpLysis_export_formatOf_clusterResults_vertex_toClusterIds,   //! which 'maps' each vertex to the cluster-id (ie, k-cluster-id=5)
  e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, //! which 'maps' each vertex to the vertex-centroid-id (eg, "gene(a) in-clsuter pathway(b)").
  e_hpLysis_export_formatOf_clusterResults_distanceToCentroids, //! the distance from each vertex to the centdis in each candiate-vertex-set.
  //! Export: a 'filtered verison' of the inptu-mtrex used to clsutering: in the exprot-result we omit (ie, mask) the relationshisp/edges between vertices which are Not in the same cluster. The result-generated-matrix may before be compared with alternative filtering-tehenisues to visually assess the accuraycy of the cluster-approach (eg, jk-means-clsuter-appraoch) to assess/elvuate correctness of the k-means-clsuter-appraoch.
  e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_TSV,
  e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS,
  e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JSON,
  /* //! ------------------------------------------------------  */
  /* //! Export: the consistency in simliarty: */
  /* //! Note: */
  /* e_hpLysis_export_formatOf_clusterConsistency */
  //e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix, 
  /* e_hpLysis_export_formatOf_similarity_, */
  /* e_hpLysis_export_formatOf_ */
} e_hpLysis_export_formatOf_t; 

/**
   @brief export a resutl-set stored in teh s_hpLysis_api_t object  (oekseth, 06. jan. 2016).
   @param <self> the object to hold the results of a "cluster__hpLysis_api(..)" call.
   @param <stringOf_file> which if Not set to NULL is as the file-naem to store the results: if set to NULL then we expect the "file_out" to be used.
   @param <file_out> which if Not set to NULL is the fiel-descirptor to use (eg, an ealready opend file-desicprtor or a stnadarized field-esciprtor such as ST
DOUT): if "stringOf_file" and "file_out" is set to NULL then the result is written to "STDOUT".
   @param <exportFormat> is the export-format to use: should be consistent with the previous call to the "cluster__hpLysis_api(..)" funciton.
   @param <alternativeMatrixToUse> which should be set if a pre-silmiarity-step is Not used
   @return true upon success.
   @remarks the cofniguraitons to this fucntion should be cosnistent with the previous call to the "cluster__hpLysis_api(..)" function. To examplify the latter if a cluster-export-enum is used then we expoect a cluster-algorithm to have been 'defined' wrt. the "const e_hpLysis_clusterAlg_t alg_type" parameter in the latter function.
 **/
bool export__hpLysis_api(s_hpLysis_api_t *self, const char *stringOf_file, FILE *file_out, const e_hpLysis_export_formatOf_t exportFormat, s_kt_matrix_t *alternativeMatrixToUse);

/**
   @return the set of clusters, ie, if any
   @date 05.01.2019
   @author Ole Kristian Ekseth (oekseth).
   @remarks if the HCA-tree is used (and the HCA has not yet been partioned into a tree), then the "self" object is ubdated with a pariting of HCA tree.
**/
s_kt_list_1d_uint_t getClusters__hpLysis_api(s_hpLysis_api_t *self);

/**
   @brief export the cluster-membershisp assicated to the 'last' comptaution (oesketh, 06. mar. 2017).
   @param <self> is the objec twhich hold the clster-results.
   @return a matrix whihc hold the clsuter-emberships.
   @remarks the contents of teh result will denepdn upon the following cocniditons:
   -- Case(k-clusters-known): export "[vertex] = clusterId"
   -- else:Case(hca-clusters-known): export triplet=(child, parent, score)
   -- else:Case(SOM-clusters-known): export triplet=(vertex, som_x, som_y).
 **/
s_kt_matrix_t export_clusterMemberships__hpLysis_api(s_hpLysis_api_t *self);
/**
   @brief generates a summarty-object to catprue core-proeprties of the clsuter-reuslts (oekseth, 06. apr. 2017)
   @param <self> is the objec twhich hold the clster-results.
   @return the new-constructed object.
   @remarks the object is (among others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
 **/
s_kt_clusterResult_condensed_t export__clusterAttributes__s_kt_clusterResult_condensed_t(s_hpLysis_api_t *self);
/**
   @brief identifies a scalar describing the cosnsitency in clusters versus an input-matrix (oekseth, 06. feb. 2017).
   @param <self> the object to hold the results of a "cluster__hpLysis_api(..)" call.
   @param <config__ccmMatrixBased__toUse> is the CCM-metric to be used.
   @param <alt_mapOf_clusterIds> which if Not set to NULL is sued as the mapping we compare the simlairty-amtrix to: otehrwise we expect the k-permtuations of clsutering-algorithm to have been explcitly constructed.
   @param <alternativeMatrixToUse> which should be set if a pre-silmiarity-step is Not used
   @return the scalar score desciribng the simliarty.
   @remarks CCM is a short-hand for "cluster-comparison-metircs". In this fucntion we 'enable' a CCM-based appraoch to 'get a measure of density' (of a simalrity-amtrix VS a cluster-result), eg, when comparedx to a gold-standard "alt_mapOf_clusterIds" setting.
 **/
t_float scalar_CCMclusterSimilarity__s_hpLysis_api_t(s_hpLysis_api_t *self, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse, const uint *alt_mapOf_clusterIds, s_kt_matrix_t *alternativeMatrixToUse);

/**
   @brief identify the optmal number of clsuters (and the 'optmality-factor') (oekseth, 06. mar. 2017).
   @param <matrix>  is the input-data to evaluate.
   @param <isAn__adjcencyMatrix> which is used to 'descieds' how to apply clsutering.
   @param <config__ccmMatrixBased__toUse> is the CCM used for convergence-criteria.
   @param <clusterAlg> is the clustering-alogirhtm to apply.
   @param <scalarResult_ccmScore> is the CCM-score idneitfed for the "nclsuters" case.
   @param <scalarResult_ncluster> is the number fo clusters to use, given a cluster-algorithm and a CCM   
   @return true upon success.
   @remarks this fucntion may be seen as an ntroduction to how 'combine' different API-supproting funcitoanlity' in oru "hplysis_api.h".
**/
bool standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(const s_kt_matrix_t *obj_1, const bool isAn__adjcencyMatrix, e_kt_correlationFunction_t metric_id, e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse, e_hpLysis_clusterAlg_t clusterAlg, t_float *scalarResult_ccmScore, uint *scalarResult_ncluster);

// TODO: consider adding lgoics wrt. 'random permtautison ... eg, to use lgocis in: #include "math_generateDistribution.h" //! which is used iot. 'gave' radnom permtuations.

#endif //! EOF
