/**
   @brief a stand-alone hello-world-APP which prints out a text-string.
   @remarks inispriation: "https://docs.oracle.com/javase/tutorial/getStarted/cupojava/unix.html". 
   @remarks build-process:
   -- compile: javac tut__altLAng_simMetric.java
   -- run: java -classpath . altLAng_simMetric
 */
class altLAng_simMetric {
    public static float value_result = 0;
    //! Idea: comptue Euclid xmt if-calsue-testign and xmt memory-fetching.:
    public static float get_timeOf__MatrixComp_xmtMem(int nrows_1, int ncols) {
      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = i;
	  }
      }
      //! Allcoate resutl-atmrix and compute:
      float matrix_result[][] = new float[nrows_1][nrows_1];
      long startTime = System.currentTimeMillis();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	      for(int i = 0; i < ncols; i++) {
		  sum += (row_id * row_id_out);
	      }
	      matrix_result[row_id][row_id_out] = sum;
	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
	  }
      }      
      long estimatedTime = System.currentTimeMillis() - startTime;      
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }
    //! Idea: comptue Euclid xmt if-calsue-testign:
    public static float get_timeOf__MatrixComp(int nrows_1, int ncols) {
      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = i;
	  }
      }
      //! Allcoate resutl-atmrix and compute:
      float matrix_result[][] = new float[nrows_1][nrows_1];
      long startTime = System.currentTimeMillis();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	      for(int i = 0; i < ncols; i++) {
		  sum += matrix[row_id][i] * matrix[row_id_out][i];
	      }
	      matrix_result[row_id][row_id_out] = sum;
	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
	  }
      }      
      long estimatedTime = System.currentTimeMillis() - startTime;
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }
    //! Idea: comptue Euclid 
    public static float get_timeOf__MatrixComp__useMask(int nrows_1, int ncols) {
      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = i;
	  }
      }
      //! Allcoate resutl-atmrix and compute:
      float matrix_result[][] = new float[nrows_1][nrows_1];
      long startTime = System.currentTimeMillis();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	      for(int i = 0; i < ncols; i++) {
		  float s_1 = matrix[row_id][i];
		  float s_2 = matrix[row_id_out][i];
		  if( (s_1 != Float.MAX_VALUE) && (s_2 != Float.MAX_VALUE) ) {
		      sum += s_1 * s_2;
		  }
	      }
	      matrix_result[row_id][row_id_out] = sum;
	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
	  }
      }      
      long estimatedTime = System.currentTimeMillis() - startTime;
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }
    public static float __getForCol(int ncols, float matrix[][], int row_id, int row_id_out) {
	float sum = 0F;
	for(int i = 0; i < ncols; i++) {
	    float s_1 = matrix[row_id][i];
	    float s_2 = matrix[row_id_out][i];
	    if( (s_1 != Float.MAX_VALUE) && (s_2 != Float.MAX_VALUE) ) {
		sum += s_1 * s_2;
	    }
	}
	return sum;
    }
    //! Idea: comptue Euclid 
    public static float get_timeOf__MatrixComp__useMask_funCall(int nrows_1, int ncols) {
      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = i;
	  }
      }
      //! Allcoate resutl-atmrix and compute:
      float matrix_result[][] = new float[nrows_1][nrows_1];
      long startTime = System.currentTimeMillis();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	      sum += __getForCol(ncols, matrix, row_id, row_id_out);
	      matrix_result[row_id][row_id_out] = sum;
	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
	  }
      }      
      long estimatedTime = System.currentTimeMillis() - startTime;
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }
    public static float __getForPair(float s_1, float s_2) {
	if( (s_1 != Float.MAX_VALUE) && (s_2 != Float.MAX_VALUE) ) {
	    return s_1 * s_2;
	}
	return 0;
    }

    public static float __getForCol_eachPair(int ncols, float matrix[][], int row_id, int row_id_out) {
	float sum = 0F;
	for(int i = 0; i < ncols; i++) {
	    float s_1 = matrix[row_id][i];
	    float s_2 = matrix[row_id_out][i];
	    sum += __getForPair(s_1, s_2);
	}
	return sum;
    }
    //! Idea: comptue Euclid 
    public static float get_timeOf__MatrixComp__useMask_funCall_andEachPair(int nrows_1, int ncols) {
      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = i;
	  }
      }
      //! Allcoate resutl-atmrix and compute:
      float matrix_result[][] = new float[nrows_1][nrows_1];
      long startTime = System.currentTimeMillis();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	      sum += __getForCol_eachPair(ncols, matrix, row_id, row_id_out);
	      matrix_result[row_id][row_id_out] = sum;
	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
	  }
      }      
      long estimatedTime = System.currentTimeMillis() - startTime;
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }
    
    public static void main(String[] args) {
	System.out.println("row_name\txmt_Mem_and_mask\txmt_mask\tmem_and_mask\tmem_and_mask_func\tmem_and_mask_eachPair"); //! ie, write out a header
	System.err.println("row_name\txmt_Mem_and_mask\txmt_mask\tmem_and_mask\tmem_and_mask_func\tmem_and_mask_eachPair"); //! ie, write out a header
	//for(int case_id = 0; case_id < 200; case_id++) {
	for(int case_id = 0; case_id < 20; case_id++) {
	    int nrows = 128*(1 + case_id);
	    int ncols = 128*(1 + case_id);      
	    // int nrows = 10; int ncols = 20;
	    //float time = 
	    System.out.println("matrix[" + nrows + "," + ncols + "]\t" 
			       + get_timeOf__MatrixComp_xmtMem(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp__useMask(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp__useMask_funCall(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp__useMask_funCall_andEachPair(nrows, ncols)
			       );
	    System.err.println("matrix[" + nrows + "," + ncols + "]\t" 
			       + get_timeOf__MatrixComp_xmtMem(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp__useMask(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp__useMask_funCall(nrows, ncols)
			       + "\t"
			       + get_timeOf__MatrixComp__useMask_funCall_andEachPair(nrows, ncols)
			       );
	    //+ "\n"); // Display the string.
	}
	System.err.println("sum of score=" + value_result);
    }
}
