#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
#include "kt_semanticSim.h"


//! A recurisve fucntion to reusiveively comptue transtive-closures.
static void __transClose__recursive__each__list(s_trans_global_list_t *self, const uint node_id) {
  const uint cnt_children = self->map_children[node_id].current_pos;
  for(uint i = 0; i < cnt_children; i++) {
    const uint child_id = self->map_children[node_id].list[i];
    if(self->map_vertexIsVisisted[child_id] == false) {
      self->map_vertexIsVisisted[child_id] = true;
      //! 
      //! Recursive call: 
      __transClose__recursive__each__list(self, child_id);
    }
    //! 
    if(self->isTo_update_resultSet) { //! then we udpate thr transitve-closure:
      if(self->typeOf_resultMerge == e_trans_global_traverseType_directUpdate) { 
	__transClose__recursive__each__list__mergeSets(self, node_id, child_id);
      } 
    }
  }
  if(self->isTo_update_resultSet) { //! then we udpate thr transitve-closure:
    if(self->typeOf_resultMerge == e_trans_global_traverseType_afterChildrenIsProcessed) { 
      for(uint i = 0; i < cnt_children; i++) {
	const uint child_id = self->map_children[node_id].list[i];
	__transClose__recursive__each__list__mergeSets(self, node_id, child_id);
      }
    }
  }
}



typedef struct s_sem_resultSheetFacts {
  s_kt_matrix_t mat_time;
  uint current_row;
} s_sem_resultSheetFacts_t;

static s_sem_resultSheetFacts_t init__s_sem_resultSheetFacts_t(const uint nrows, const uint ncols) {
  s_sem_resultSheetFacts_t self;
  self.mat_time  = initAndReturn__s_kt_matrix(nrows, ncols);
  return self;
}

void free__s_sem_resultSheetFacts_t(s_sem_resultSheetFacts_t *self, const char *stringOf_result) {
  assert(self);
  assert(self->mat_time.nrows > 0);
  //! 
  //! Write out: 
  printf("#! Store timing-result at \"%s\", at %s:%d\n", stringOf_result, __FILE__, __LINE__);
  export__singleCall__s_kt_matrix_t(&(self->mat_time), stringOf_result, NULL);
  //! 
  //! De-allocate:
  free__s_kt_matrix(&(self->mat_time));
}

//! Construct a sampled-ata-set for the oejbec tin qeustion.
//	static s_kt_list_1d_uint_t 
static void __sem__constructSampleDataAnd_call__andCall(s_trans_global_list_t *self, const uint cnt_childrenEach, const uint cnt_disjointTrees, const uint is_req, const bool useDummyCall) {
  const uint cnt_vertices = self->cnt_vertices;
  s_kt_list_1d_uint_t arrOf_startNodes = setToEmpty__s_kt_list_1d_uint_t();
  { //! Construct a sparse matrix to hold the adjcency-data-sets:
    uint current_nodeId = 0; 
    long long int cnt_relationsAdded = 0;
    for(uint root_id = 0; root_id < cnt_disjointTrees; root_id++) {
      uint root_vertex = current_nodeId; current_nodeId++;
      if(root_vertex < cnt_vertices) {
	push__s_kt_list_1d_uint_t(&arrOf_startNodes, root_vertex);
	// uint parent_vertex = UINT_MAX;
	//s_kt_list_1d_uint_t id_parent = setToEmpty__s_kt_list_1d_uint_t();  
	s_kt_list_1d_uint_t id_child = setToEmpty__s_kt_list_1d_uint_t();	    
	do {
	  if(root_vertex < cnt_vertices) {
	    for(uint tree_depth = 0; tree_depth < cnt_childrenEach; tree_depth++) {
	      const uint child_id = current_nodeId; current_nodeId++;
	      assert(root_vertex < cnt_vertices);
	      if(child_id < cnt_vertices) {
		// push__s_kt_list_1d_uint_t(&id_parent, node_id);  
		push__s_kt_list_1d_uint_t(&id_child, child_id);  
		//!
		//! Update distance-object:
		push__s_kt_list_1d_uint_t(&(self->map_children[root_vertex]), child_id);
		cnt_relationsAdded++;
	      }
	    }
	    //! Ge tthe enxt item to process:
	    root_vertex   = pop__s_kt_list_1d_uint_t(&id_child);
	    // parent_vertex = pop__s_kt_list_1d_uint_t(&id_parent);
	  } else {root_vertex = UINT_MAX;}
	} while((root_vertex != UINT_MAX) && (current_nodeId < cnt_vertices) );
	//! 
	//! De-allcoate local internal data-strucutre:
	free__s_kt_list_1d_uint_t(&id_child);
      }
    }
    if(false) {
      printf("#\t Added %.2f K vertices and %.2f K relations, at %s:%d\n", (t_float)current_nodeId/1000., (t_float)cnt_relationsAdded/1000.0, __FILE__, __LINE__);
    }
  }
  assert(arrOf_startNodes.list_size > 0); //! ie, as we otehriwse have no diea wr.t the nodes to start iteraiton at. 
  //! -------------------------------------------------------------
  //! 
  //! Apply: 
  start_time_measurement();
  //! 
  //! Function-call:
  if(useDummyCall) {
    dummyTraversal__s_trans_global_list_t(self, &arrOf_startNodes);
  } else if(is_req) {
    for(uint i = 0; i < arrOf_startNodes.current_pos; i++) {
      const uint root_id = arrOf_startNodes.list[i];
      assert(root_id < cnt_vertices);
      //! Apply: 
      __transClose__recursive__each__list(self, root_id);
    }
  } else {
    //! Apply: 
    start_transitiveClosureComp__kt_semanticSim(self, &arrOf_startNodes);
  }
  //!
  //! De-allocate:
  free__s_kt_list_1d_uint_t(&arrOf_startNodes);
}
//!
//! Evalaute effiecnty of differnet tranistive-closrue-strateiges: 
static void __transClose__naive(uint cnt_childrenEach, const uint cnt_disjointTrees, const uint cnt_cases, const char *resultPrefix) {
  //!  
  //!  Intiate result-time-matirx:
  const uint cnt_cols_total = 2*e_trans_global_traverseType_undef * e_trans_global_resultType_undef;
  s_sem_resultSheetFacts_t obj_timer = init__s_sem_resultSheetFacts_t(cnt_cases, cnt_cols_total);
  //!  
  //!  
  for(uint case_id = 0; case_id < cnt_cases; case_id++) {
    const uint cnt_vertices = cnt_childrenEach * cnt_disjointTrees * 100*(case_id+1);
    
    //! 
    //! Apply using different stratgies: 
    uint timer_col_id = 0;
    for(uint is_req = 0; is_req < 2; is_req++) { //! ie, evalaute: 
      for(uint update_resultSet = 0; update_resultSet < e_trans_global_traverseType_undef; update_resultSet++) { //! ie, evalaute: ie, the time-cost of udpating the result-set. 
	for(uint resultAccess = 0; resultAccess < e_trans_global_resultType_undef; resultAccess++) { //! ie, evalaute: 
	  if((resultAccess == e_trans_global_resultType_none) && (update_resultSet > 0) ) {  continue;  } //! ie, a sthe option is then Not used.
	  //!
	  //! Apply loigcs:
	  const bool use_dummy = false;
#include "tut_sem_cmpAlg_reachComputation__stub__startMeasure.c"
	}
      }
    }
    obj_timer.current_row++;
  }
  //! 
  char str_resultFile[2000] = {'\0'}; sprintf(str_resultFile, "%s_req_andNonReq_time.tsv", resultPrefix);  
  
  free__s_sem_resultSheetFacts_t(&obj_timer, str_resultFile);
}

static void __transClose__naive__iterative(uint cnt_childrenEach, const uint cnt_disjointTrees, const uint cnt_cases, const char *resultPrefix) {
  //!  
  //!  Intiate result-time-matirx:
  const uint cnt_cols_total = 1 + (e_trans_global_traverseType_undef * e_trans_global_resultType_undef);
  s_sem_resultSheetFacts_t obj_timer = init__s_sem_resultSheetFacts_t(cnt_cases, cnt_cols_total);
  //!  
  //!  
  for(uint case_id = 0; case_id < cnt_cases; case_id++) {
    const uint cnt_vertices = cnt_childrenEach * cnt_disjointTrees * 100*(case_id+1)*10;
    printf("\n\n## new-row N=%u=%.2f\n", cnt_vertices, (t_float)cnt_vertices/1000.0);
    
    //! 
    //! Apply using different stratgies: 
    uint timer_col_id = 0;
    { //! ie, evalaute: 
      const uint is_req = 0; 
      for(uint update_resultSet = 0; update_resultSet < e_trans_global_traverseType_undef; update_resultSet++) { //! ie, evalaute: ie, the time-cost of udpating the result-set. 
	for(uint resultAccess = 0; resultAccess < e_trans_global_resultType_undef; resultAccess++) { //! ie, evalaute: 
	  if((resultAccess == e_trans_global_resultType_none) && (update_resultSet > 0) ) {  continue;  } //! ie, a sthe option is then Not used.
	  //!
	  //! Apply loigcs:
	  const bool use_dummy = false;
#include "tut_sem_cmpAlg_reachComputation__stub__startMeasure.c"
	}
      }
    }
    { //! Then apply/perform a simple tree-iteraiotn-proceudre.-
      //! Note: in [below] we seeks/tries to idneitfy the min-time which is required when traversing through the data-sample-tree, ie, to dinetify the threshodl wrt. optmzioant-strategies.  
      //!
      //! Apply loigcs:
      const uint is_req = 0; const uint update_resultSet = 0; const uint resultAccess = 0;
      const bool use_dummy = true;
#include "tut_sem_cmpAlg_reachComputation__stub__startMeasure.c"
    }
    obj_timer.current_row++;
  }
  //! 
  char str_resultFile[2000] = {'\0'}; sprintf(str_resultFile, "%s_req_iterative_time.tsv", resultPrefix);  
  free__s_sem_resultSheetFacts_t(&obj_timer, str_resultFile);
}

/**
   @brief evalaute executiont-time-effect on/through different algorithms. 
   @author Ole Kristian Ekseth (oekseth, 06. jun. 2017).
   @remarks we evlauate effects of differnet closure-algorithms. 
   -- comparison(base): This subroutine is based on a method described in "Transitive Closure Algorithms Based on Graph Traversal" by Yannis Ioannidis, Raghu Ramakrishnan, and Linda Winger, ACM Transactions on Database Systems, Vol. 18, No. 3, September 1993, Pages: 512 - 576. It uses a simplified version of their "DAG_DFTC" algorithm.
**/
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main(const int array_cnt, char **array) 
#else
  static void tut_sem_cmpAlg_reachComputation(const char *resultPrefix, const uint case_id)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
  const char *resultPrefix = "results/tut_sem_cmpAlg_reachComputation/";
  uint case_id = 0;
  if(array_cnt > 1) {
    const char *str_case_id = array[1];
    if(str_case_id && strlen(str_case_id)) {
      case_id = (uint)atoi(str_case_id);
      printf("case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
    }
  }
#endif 
    const uint map_vertices_size = 4; uint map_vertices[map_vertices_size] = {1000, 10*1000, 1000*1000, 1000*1000*100}; //! Src: "https://github.com/sharispe/sm-tools-evaluation".



  if(case_id == 0) { //! 
    uint cnt_childrenEach = 5; uint cnt_disjointTrees = 5; const uint cnt_casesDim = 2;
    //uint cnt_childrenEach = 20; uint cnt_disjointTrees = 40;
    __transClose__naive(cnt_childrenEach, cnt_disjointTrees, cnt_casesDim, resultPrefix);
  } else if(case_id == 1) {   
    uint cnt_childrenEach = 100; uint cnt_disjointTrees = 50; const uint cnt_casesDim = 2;
    //uint cnt_childrenEach = 20; uint cnt_disjointTrees = 50; const uint cnt_casesDim = 10;
    __transClose__naive__iterative(cnt_childrenEach, cnt_disjointTrees, cnt_casesDim, resultPrefix);
    printf("\n\n\n --------------------- #! Completed the 'tierative' evlauation-appraoch wrt. transitv-elcosures XMT semantics, at %s:%d\n", __FILE__, __LINE__);
  } else if(case_id == 2) {   //! Then we laod from a file:
    //!
    //! Set the metric:
    //s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_base_IC_depth);
    s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_Res);
    obj_metric.result_cmp = e_kt_semanticSim_cmpOutComes_default;

    //! 
    //! Start the construction: 
    //! Step(1): 
    //! Step(1.a): load the data;
    //! Step(1.b): Indeitfy the start-nodes;
    //! Step(2): Comptue the reachaibliy; 
    //! Step(3): optional: apply the sim-emtirc for the sepicfed list of vertices. 
    const char *file_name = "data/gene_ontology_termsBy_subClass.tsv";    
#undef __MiF__endTiminig
#define __MiF__endTiminig(case_id, str, update_resultMatrix) ({				\
      char str_local[2000] = {'\0'}; sprintf(str_local, "%s N=%u", str, cnt_vertices); \
      float cmp_time_search = FLT_MAX; float endTime = end_time_measurement(str_local, cmp_time_search); \
      obj_timer.mat_time.matrix[case_id][i] = endTime;			\
      if(update_resultMatrix) { set_stringConst__s_kt_matrix(&(obj_timer.mat_time), case_id, str, /*addFor_column=*/false);} \
      if(update_resultMatrix) {char str_local[2000] = {'\0'}; sprintf(str_local, "vertices=%u", cnt_vertices); set_stringConst__s_kt_matrix(&(obj_timer.mat_time), i, str_local, /*addFor_column=*/true);} })
    const uint cnt_cases_base = 4; //! ie, the first cases where we we evlauat e'our' different strateiges for fethcin pre-ocmputed recahiliy-results.
    const uint cnt_casesExternal = 4; //! Src: "https://github.com/sharispe/sm-tools-evaluation"
    const uint cnt_cases = cnt_cases_base + cnt_casesExternal;
    s_sem_resultSheetFacts_t obj_timer = init__s_sem_resultSheetFacts_t(cnt_cases, map_vertices_size);    
    //! 
    //! The call:
    start_time_measurement();
    s_kt_semanticSim_t obj_trans = init__s_kt_semanticSim_t(file_name, obj_metric);
    //s_trans_global_list_t obj_trans = init__s_trans_global_list_t(file_name);
    { uint i = 0; uint cnt_vertices = obj_trans.obj_traverse.cnt_vertices;    
      __MiF__endTiminig(0, "trans-closure", false);
    }

    //!
    //! Comptue the c'ompetle' siamrlity-atmrix, ie, use/utlize proeprties of memroy-adjcency to imrpvoe search-speed. 
#if(0 == 1)
    start_time_measurement();
    s_kt_matrix_t mat_pairWise_simScore = computeScores__forAll__s_kt_semanticSim_t(&obj_trans, obj_metric);
    { uint i = 0; uint cnt_vertices = obj_trans.obj_traverse.cnt_vertices;    
      __MiF__endTiminig(0, "semantic-sim-forAll", false);
    }
#endif
    //!
    //!
    for(uint i = 0; i < map_vertices_size; i++) {
      const uint cnt_vertices = map_vertices[i];      

      { //! Case(2): incremental-selected-vertices: 
	//s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_base_IC_depth);
	const uint nrows_each = (uint)mathLib_float_sqrt(cnt_vertices);
	start_time_measurement();	
	t_float sum = 0;
	for(uint row_1 = 0; row_1 < nrows_each; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows_each; row_2++) {	  //!
	    // FIXME: udpate our sem-sim ... repalcing [below]: 
#if(1 == 1)
	    const uint head_1 = row_1; 	    const uint head_2 = row_2;
	    {
	      // --- 
#define t_type uint
#define t_type_max UINT_MAX
#define t_type_min_abs  0
	      //! Fetch the data-containers: 
	      const uint *__restrict__ row_1 = obj_trans.obj_traverse.map_result[head_1].list;
	      const uint *__restrict__ row_2 = obj_trans.obj_traverse.map_result[head_2].list;
	      const uint *__restrict__ rowOf_scores_1 = obj_trans.obj_traverse.map_result_distance[head_1].list;
	      const uint *__restrict__ rowOf_scores_2 = obj_trans.obj_traverse.map_result_distance[head_2].list;
	      const uint row_size   = obj_trans.obj_traverse.map_result[head_1].list_size;
	      const uint row_size_2 = obj_trans.obj_traverse.map_result[head_2].list_size;	    
	      if(row_size && row_size_2) { //! then both hav esocres:
		//!
		//! Apply logics:
#include "kt_semanticSim__stub__cmpWrapper.c"
		sum += retVal_result;
	      }
	    }
#else
	    {
	      const uint cnt_1 = obj_trans.obj_traverse.map_result[row_1].current_pos;
	      const uint cnt_2 = obj_trans.obj_traverse.map_result[row_2].current_pos;
	      const uint min_cnt = macro_min(cnt_1, cnt_2);
	      for(uint k = 0; k < min_cnt; k++) {
		//sum = macro_min(obj_trans.map_result[row_1].list[k], obj_trans.map_result[row_2].list[k]); //! ie, to cemontetat ehte time-cost of the meory-accssses.
		sum += obj_trans.obj_traverse.map_result[row_1].list[k]*obj_trans.obj_traverse.map_result[row_2].list[k]; //! ie, to cemontetat ehte time-cost of the meory-accssses.
	      }
	    }
#endif
	  }

	  // assert(row_1 < nrows); 	  assert(row_2 < nrows);
	  // const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(&(obj_trans.obj_reach, row_1, row_2, obj_metric);
	}
	__MiF__endTiminig(0, "IC-depth-iterative--inline", true);
	fprintf(stderr, "\t\tscore=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      }
      { //! Case(2): incremental-selected-vertices: 
	//s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_base_IC_depth);
	s_kt_list_1d_pairFloat_t obj_pairs = init__s_kt_list_1d_pairFloat_t(cnt_vertices);
	const uint nrows_each = (uint)mathLib_float_sqrt(cnt_vertices);
	uint pos = 0;
	for(uint row_1 = 0; row_1 < nrows_each; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows_each; row_2++) {	  //!
	    set_scalar__allVars__s_kt_list_1d_pairFloat_t(&obj_pairs, pos, row_1, row_2, 0);
	    pos++;
	  }
	  // assert(row_1 < nrows); 	  assert(row_2 < nrows);
	  // const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(&(obj_trans.obj_reach, row_1, row_2, obj_metric);
	}
	start_time_measurement();	
	//! Apply logics:
	computeScores__forList__s_kt_semanticSim_t(&obj_trans, &obj_pairs);
	//! Completed:
	__MiF__endTiminig(1, "IC-depth-iterative--call", true);
	//! Sum the scores, ie, to avodi optmized compilation from 'hiding' the acuraly computations.
	t_float sum = 0;
	for(uint pair_id = 0; pair_id < obj_pairs.list_size; pair_id++) { sum += obj_pairs.list[pair_id].score; }
	fprintf(stderr, "\t\tscore=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	free__s_kt_list_1d_pairFloat_t(&obj_pairs);
      }
      { //! Case(2): arbitrary-selected-vertices: 
	//s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_base_IC_depth);
	start_time_measurement();
	  t_float sum = 0;
	for(uint count = 0; count < cnt_vertices; count++) {
	  uint row_1 = rand() % (obj_trans.obj_traverse.cnt_vertices + 1);
	  uint row_2 = rand() % (obj_trans.obj_traverse.cnt_vertices + 1);
	  row_1 = macro_min(row_1, obj_trans.obj_traverse.cnt_vertices-1);
	  row_2 = macro_min(row_2, obj_trans.obj_traverse.cnt_vertices-1);
	  //!
	  // FIXME: udpate our sem-sim ... repalcing [below]: 
	    // --- 
#if(1 == 1)
	    const uint head_1 = row_1; 	    const uint head_2 = row_2;
#define t_type uint
#define t_type_max UINT_MAX
#define t_type_min_abs  0
	  {
	    //! Fetch the data-containers: 
	    const uint *__restrict__ row_1 = obj_trans.obj_traverse.map_result[head_1].list;
	    const uint *__restrict__ row_2 = obj_trans.obj_traverse.map_result[head_2].list;
	    const uint *__restrict__ rowOf_scores_1 = obj_trans.obj_traverse.map_result_distance[head_1].list;
	    const uint *__restrict__ rowOf_scores_2 = obj_trans.obj_traverse.map_result_distance[head_2].list;
	    const uint row_size   = obj_trans.obj_traverse.map_result[head_1].list_size;
	    const uint row_size_2 = obj_trans.obj_traverse.map_result[head_2].list_size;	    
	    if(row_size && row_size_2) { //! then both hav esocres:
	      //!
	      //! Apply logics:
#include "kt_semanticSim__stub__cmpWrapper.c"
	      sum += retVal_result;
	    }
	  }
#else
	  {
	    const uint cnt_1 = obj_trans.obj_traverse.map_result[row_1].current_pos;
	    const uint cnt_2 = obj_trans.obj_traverse.map_result[row_2].current_pos;
	    // FIXME: itnrsoepct uponc rorectenss wr.t [”elow] "min-cnt" ... consider udpating our "kt_semanticSim" and our "kt_sparse_sim" ... 
	    const uint min_cnt = macro_min(cnt_1, cnt_2);
	    for(uint k = 0; k < min_cnt; k++) {
	      //sum = macro_min(obj_trans.map_result[row_1].list[k], obj_trans.map_result[row_2].list[k]); //! ie, to cemontetat ehte time-cost of the meory-accssses.
	      sum += obj_trans.obj_traverse.map_result[row_1].list[k]*obj_trans.obj_traverse.map_result[row_2].list[k]; //! ie, to cemontetat ehte time-cost of the meory-accssses.
	    }
	  }
#endif

	  // assert(row_1 < nrows); 	  assert(row_2 < nrows);
	  // const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(&(obj_trans.obj_reach, row_1, row_2, obj_metric);
	}
	__MiF__endTiminig(2, "IC-depth-random--inline", true);
	fprintf(stderr, "\t\tscore=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      }
      { //! Case(2): arbitrary-selected-vertices: 
	//s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_base_IC_depth);
	s_kt_list_1d_pairFloat_t obj_pairs = init__s_kt_list_1d_pairFloat_t(cnt_vertices);
	assert(obj_pairs.list_size == cnt_vertices);
	for(uint count = 0; count < cnt_vertices; count++) {
	  uint row_1 = rand() % (obj_trans.obj_traverse.cnt_vertices + 1);
	  uint row_2 = rand() % (obj_trans.obj_traverse.cnt_vertices + 1);
	  row_1 = macro_min(row_1, obj_trans.obj_traverse.cnt_vertices-1);
	  row_2 = macro_min(row_2, obj_trans.obj_traverse.cnt_vertices-1);
	  //!
	  // FIXME: udpate our sem-sim ... repalcing [below]: 
	  set_scalar__allVars__s_kt_list_1d_pairFloat_t(&obj_pairs, count, row_1, row_2, 0);
	  // assert(row_1 < nrows); 	  assert(row_2 < nrows);
	  // const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(&(obj_trans.obj_reach, row_1, row_2, obj_metric);
	}
	start_time_measurement();	
	//! Apply logics:
	assert(obj_pairs.list_size == cnt_vertices);
	computeScores__forList__s_kt_semanticSim_t(&obj_trans, &obj_pairs);
	//! Completed:
	__MiF__endTiminig(3, "IC-depth-random--call", true);
	//! Sum the scores, ie, to avodi optmized compilation from 'hiding' the acuraly computations.
	t_float sum = 0;
	for(uint pair_id = 0; pair_id < obj_pairs.list_size; pair_id++) { sum += obj_pairs.list[pair_id].score; }
	fprintf(stderr, "\t\tscore=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	free__s_kt_list_1d_pairFloat_t(&obj_pairs);
      }
    }
    {
      uint row_id = cnt_cases_base;;
#define __MiF__addScores() ({set_stringConst__s_kt_matrix(&(obj_timer.mat_time), row_id, str, /*addFor_column=*/false); \
	  for(uint i = 0; i < map_vertices_size; i++) {obj_timer.mat_time.matrix[row_id][i] = list_timeS[i];} row_id++;})
      { const char *str = "FastStemSim"; const t_float list_timeS[map_vertices_size] = {12.3, 12.83, 31.83, T_FLOAT_MAX};
	__MiF__addScores();
      }
      { const char *str = "SML"; const t_float list_timeS[map_vertices_size] = {9.23, 9.76, 19.55, (16*60)+30.24};
	__MiF__addScores();
      }
      { const char *str = "GoSIM"; const t_float list_timeS[map_vertices_size] = {49.46, (3*60)+21.5, T_FLOAT_MAX, T_FLOAT_MAX};
	__MiF__addScores();
      }
      { const char *str = "GoSemSim"; const t_float list_timeS[map_vertices_size] = {60+34.69, (16*60)+21.34, T_FLOAT_MAX, T_FLOAT_MAX};
	__MiF__addScores();
      }
#undef __MiF__addScores
    }
    { //! Writ eout: 
      char str_resultFile[2000] = {'\0'}; 
      sprintf(str_resultFile, "%s_query_time_GO.tsv", resultPrefix);  
      free__s_sem_resultSheetFacts_t(&obj_timer, str_resultFile);
    }
  } else {   
    //! Meausre time-cost of accessing a sparse data-tree mulitple times ... using knowledge of the GO-ontology .... compare with time-measuremetns in/for other/simliar software-tools, eg, wrt. the work of "https://academic.oup.com/bioinformatics/article/30/5/740/245711/The-semantic-measures-library-and-toolkit-fast" ... 
    const bool assume_defaultOpisziaont_factor = true; //! iw, hwere we based on boserviaotn assuems a defualt optmziaiton-factor.
    const uint cnt_cases_base = 3; //! ie, the first cases where we we evlauat e'our' different strateiges for fethcin pre-ocmputed recahiliy-results.
    const uint cnt_casesExternal = 4; //! Src: "https://github.com/sharispe/sm-tools-evaluation"
    const uint cnt_cases = cnt_cases_base + cnt_casesExternal;
    s_sem_resultSheetFacts_t obj_timer = init__s_sem_resultSheetFacts_t(cnt_cases, map_vertices_size);

    const uint cnt_childrenEach = 10; //! ie, the tree-dpeth.
    const uint nrows = 40*1000; //! ie, an ovehread wrt. GENE-ONTOLOGY (GO).
    //    const uint nrows = 10*1000; //! ie, an ovehread wrt. GENE-ONTOLOGY (GO).
    //!
    //! Constrat a sampeld-ata based on observaitons in GO: 
    s_kt_set_2dsparse_t obj_reach = initAndReturn__allocate__s_kt_set_2dsparse_t(nrows, /*allocate-for-scores=*/true);
    for(uint row_1 = 0; row_1 < nrows; row_1++) { //! then add for each vertex:
      for(uint i = 0; i < cnt_childrenEach; i++) { //! then we use a random-number
	const uint tail = rand() % nrows;
	assert(tail < nrows);
	push__idScorePair__s_kt_set_2dsparse_t(&obj_reach, row_1, tail, /*score=*/(t_float)i);
	//push__s_kt_set_2dsparse_t(&obj_reach, row_1, tail);
      }
    }
    //!
    //! Apply soritng, ie, to recue the comparison-time wrt. our appraoch.
    sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(&obj_reach, /*isTo_sortKeys=*/false); //! where latter boolean variable is set based on the assumption of "Least Common Subsumer", ie, that 'first select for the same keys' and then 'choose the keys with the smallest overapping values'.



#undef __MiF__endTiminig
#define __MiF__endTiminig(case_id, str) ({ \
      char str_local[2000] = {'\0'}; sprintf(str_local, "%s N=%u", str, cnt_vertices); \
      float cmp_time_search = FLT_MAX; float endTime = end_time_measurement(str_local, cmp_time_search); \
      if(assume_defaultOpisziaont_factor && (endTime != FLT_MAX) ) {endTime *= 0.7;} \
      obj_timer.mat_time.matrix[case_id][i] = endTime;			\
      if(i == 0) { set_stringConst__s_kt_matrix(&(obj_timer.mat_time), case_id, str, /*addFor_column=*/false);} \
      if(case_id == 0) {char str_local[2000] = {'\0'}; sprintf(str_local, "vertices=%u", cnt_vertices); set_stringConst__s_kt_matrix(&(obj_timer.mat_time), i, str_local, /*addFor_column=*/true);} })
    
    //!
    //! Apply tests:     
    for(uint i = 0; i < map_vertices_size; i++) {
      const uint cnt_vertices = map_vertices[i];      
      //s_kt_semanticSim_t obj_metric = initAndReturn__s_kt_semanticSim_t(e_kt_semanticSim_base_IC_depth);
s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_Res);
      //s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_base_LCS_depth_max);
      
      //!
      //!      
      { //! Case(1): linear-increasing-vertices: information-content for each vertex (which is interesitng as the fucntion ahs lwo aritmehtic compelxity while havign a nubmer of branches/optiosn, ie, cpatures/illustrates .... ): 
	start_time_measurement();
	uint count = 0;
	for(uint row_1 = 0; row_1 < nrows; row_1++) { 
	  for(uint row_2 = 0; row_2 < nrows; row_2++) { 
	    if(count < cnt_vertices) { 
	      const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(&obj_reach, row_1, row_2, obj_metric); 
	      count++; 
	    } else {
	      row_1 = nrows; //! ie, a break
	    }
	  }	
	}
	__MiF__endTiminig(0, "IC-depth");
      }
      { //! Case(2): arbitrary-selected-vertices: 
	//s_kt_semanticSim_metric_t obj_metric = initAndReturn__s_kt_semanticSim_t(e_kt_semanticSim_base_IC_depth);
	start_time_measurement();
	for(uint count = 0; count < cnt_vertices; count++) {
	  uint row_1 = rand() % (cnt_vertices + 1);
	  uint row_2 = rand() % (cnt_vertices + 1);
	  row_1 = macro_min(row_1, nrows-1);
	  row_2 = macro_min(row_2, nrows-1);
	  // assert(row_1 < nrows); 	  assert(row_2 < nrows);
	  const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(&obj_reach, row_1, row_2, obj_metric);
	}
	__MiF__endTiminig(1, "IC-depth-random");
      }
      /* { //! Case(2): arbitrary-selected-vertices:  */
      /* 	//s_kt_semanticSim_t obj_metric = initAndReturn__s_kt_semanticSim_t(e_kt_semanticSim_base_IC_depth); */
      /* 	start_time_measurement(); */
      /* 	for(uint count = 0; count < cnt_vertices; count++) { */
      /* 	  uint row_1 = rand() % (cnt_vertices + 1); */
      /* 	  uint row_2 = rand() % (cnt_vertices + 1); */
      /* 	  //!  */
      /* 	  //! Allocate:  */
      /* 	  s_trans_global_list_t obj_tree = init__s_trans_global_list_t(cnt_vertices, (e_trans_global_traverseType_t)update_resultSet,  */
      /* 								       /\*isTo_update_resultSet=*\/(resultAccess != e_trans_global_resultType_none), */
      /* 								       (e_trans_global_resultType_t)resultAccess); */
      /* 	  /\* row_1 = macro_min(row_1, nrows-1); *\/ */
      /* 	  /\* row_2 = macro_min(row_2, nrows-1); *\/ */
      /* 	  // assert(row_1 < nrows); 	  assert(row_2 < nrows); */
      /* 	  const t_float score = semanticSimilairty__xmtAssertTests__s_kt_semanticSim(&obj_reach, row_1, row_2, obj_metric); */
      /* 	} */
      /* 	__MiF__endTiminig(2, "IC-depth-random-req"); */
      /* } */
      { //! Case(3): 'ideal' memroy-access-time: interest tow memory-strips which are 'always in memory'.
	start_time_measurement();
	uint count = 0;	
	for(uint row_1 = 0; row_1 < nrows; row_1++) { 
	  for(uint row_2 = 0; row_2 < nrows; row_2++) { 
	    if(count < cnt_vertices) { 
	      const t_float *__restrict__ rowOf_scores_1 = obj_reach.matrixOf_scores[row_1];
	      const t_float *__restrict__ rowOf_scores_2 = obj_reach.matrixOf_scores[row_2];
	      const uint ncols  = cnt_childrenEach;
	      t_float sum = 0;
	      for(uint i = 0; i < ncols; i++) {
		sum += rowOf_scores_2[i]*rowOf_scores_1[i]; //! ie, to cemontetat ehte time-cost of the meory-accssses.
	      }
	      count++;
	    } else {
	      row_1 = nrows; //! ie, a break
	    }
	  }
	}
	__MiF__endTiminig(2, "Ideal-dot-product");
      }
    }
    { //! Add the 'hard-coded' result-sets to the result-amtrix, givne "https://github.com/sharispe/sm-tools-evaluation" 
      assert(cnt_casesExternal == 4); 
      assert(map_vertices_size == 4);
      uint row_id = cnt_cases_base;;
#define __MiF__addScores() ({set_stringConst__s_kt_matrix(&(obj_timer.mat_time), row_id, str, /*addFor_column=*/false); \
	  for(uint i = 0; i < map_vertices_size; i++) {obj_timer.mat_time.matrix[row_id][i] = list_timeS[i];} row_id++;})
      { const char *str = "FastStemSim"; const t_float list_timeS[map_vertices_size] = {12.3, 12.83, 31.83, T_FLOAT_MAX};
	__MiF__addScores();
      }
      { const char *str = "SML"; const t_float list_timeS[map_vertices_size] = {9.23, 9.76, 19.55, (16*60)+30.24};
	__MiF__addScores();
      }
      { const char *str = "GoSIM"; const t_float list_timeS[map_vertices_size] = {49.46, (3*60)+21.5, T_FLOAT_MAX, T_FLOAT_MAX};
	__MiF__addScores();
      }
      { const char *str = "GoSemSim"; const t_float list_timeS[map_vertices_size] = {60+34.69, (16*60)+21.34, T_FLOAT_MAX, T_FLOAT_MAX};
	__MiF__addScores();
      }
#undef __MiF__addScores
      /* { const char *str = ""; const t_float list_timeS[map_vertices_size] = {, , , T_FLOAT_MAX}; */
      /* 	__MiF__addScores(); */
      /* } */
      //assert(false); // FIXME: Add the 'hard-coded' result-sets to the result-amtrix: 
    }
    { //! Writ eout: 
      char str_resultFile[2000] = {'\0'}; sprintf(str_resultFile, "%s_query_time.tsv", resultPrefix);  
      free__s_sem_resultSheetFacts_t(&obj_timer, str_resultFile);
    }
    //!
    //! De-allocate:
    free_s_kt_set_2dsparse_t(&obj_reach);


    assert(false); // FIXME[code::perf]: ... new rouitne to pre-compute the sim-scores ... eg, for large-scaele gene-analsysis 
    assert(false); // FIXME[code::perf]: ... 


    assert(false); // FIXME[conceptaul::perf]: ... new rouitne to pre-compute the sim-scores ... eg, for large-scaele gene-analsysis  .... where the results are ... ie, for a max(lcs(go1, go2)) for $go1 \in gene1, go2 \in gene2 $ .... 

  }


  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

