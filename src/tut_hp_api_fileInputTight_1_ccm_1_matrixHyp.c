#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis

/**
   @brief examplify how our "hp_api_fileInputTight.h" may be sued for: matrix-based CCM-evaluation
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- input: in this example we (a) load an 'arbirary' nput-file, and then (b) use 'randomness' to 'select at radnom' 'an hypothesis to investigate'. 
   -- logic: use matrix-based "cluster-comparison-metric" (CCM) to evalaute agreement in predictions
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette; //! ie, the CCM to use.
  //!
  //! Define an input-file to use in analysis:
  const char *file_name = "data/local_downloaded/11_milk.csv.hpLysis.tsv";
  //const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 

  //! Load the TSV-file:
  //! Note: advanced options in data-loading conserns noise-injection, both wrt. 'noise-functions' and 'merging of different data-sets'.
  s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(file_name, initAndReturn__s_kt_matrix_fileReadTuning_t()); //! where latter funciton is deifned in our "kt_matrix.h"
  //!
  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(1): Comptue simlairty-metric.
  //! Note: 'this operaiton' is required as the "file_name" is Not expected to describe/represent an adjcency-matrix
  s_kt_matrix_t mat_computedSim = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
  bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(/*metric=*/e_kt_correlationFunction_groupOf_MINE_mic, &mat_input, /*mat_input_2=*/NULL, &mat_computedSim);
  assert(is_ok); //! ie, what is expected.
  //!
  //! Export simlairty-metric:
  //allocOnStack__char__sprintf(2000, resultPath, "%s_%s", , get_stringOf__short__e_hpLysis_clusterAlg_t(clustAlg));
  const char *result_file = "data/fromTutExamples/tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.tsv";
  is_ok = export__singleCall__s_kt_matrix_t(&mat_computedSim, result_file, NULL);
  assert(is_ok); //! ie, what is expected.
  //!
  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(2): use CCM to evlauate cluster: 
  //! Pre-step: construct a 'random' hypothesis:
  s_kt_matrix_t mat_random = initAndReturn__s_kt_matrix(/*nrows=*/1, /*ncols=*/mat_computedSim.nrows);
  uint ncluster_hyp = mat_random.ncols / 3; //! ie, a 'wild-card' value.
  assert(ncluster_hyp > 0);
  { //! Set random values:
    for(uint row_id = 0; row_id < mat_random.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_random.ncols; col_id++) {
	if(true) {
	  mat_random.matrix[row_id][col_id] = rand() % ncluster_hyp; //! an appraoch which only returns psaudo-random-numbers: used to simplify 'udnerstaind' of this tutrocial-example.
	} else {
	  mat_random.matrix[row_id][col_id] = (t_float)col_id; //! ie, a 'structed' 'definiton' of the 'hypothesis'.
	}
      }
    }
  }
  //! Expoert [ªbove], ie, to 'allow' for a string-based input in [”elow] case(b) and case(c):
  const char *random_file = "data/fromTutExamples/tut_hp_api_fileInputTight_1_ccm_1_matrixHyp_random.tsv";
  is_ok = export__singleCall__s_kt_matrix_t(&mat_random, random_file, NULL);
  assert(is_ok); //! ie, what is expected.
  //! 
  { //! Case(a): API-logics:    
    s_kt_list_1d_uint_t list_hypo    = init__s_kt_list_1d_uint_t(mat_computedSim.nrows);
    assert(list_hypo.list_size == mat_random.ncols);
    //! Insert the valeus gnerated in oru 'random call':
    MF__floatToUint__matrix__atRow(/*row_id=*/0, /*mat_input=*/mat_random, /*list_result=*/list_hypo);
    // printf("dim[random]=[%u], vs matrix=[%u, %u], at %s:%d\n", mat_random.ncols, mat_computedSim.nrows, mat_computedSim.ncols, __FILE__, __LINE__);
    //! Apply logics:
    t_float ccm_result = 0;
    bool is_ok = apply__ccm__matrixBased__hp_api_fileInputTight(enum_id, &mat_computedSim, &list_hypo, &ccm_result);
    assert(is_ok);
    printf("ccm_result='%f', for a matrix[%u, %u], at %s:%d\n", ccm_result, mat_computedSim.nrows, mat_computedSim.ncols, __FILE__, __LINE__);
    //assert(false); // FIXME: remove
    //! De-allcoate:
    free__s_kt_list_1d_uint_t(&list_hypo);
  }
  const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
  const char *stringOf_inputFile_1 = result_file;
  const char *stringOf_inputFile_2 = random_file;
  const char *stringOf_exportFile = NULL; //! ie, to "stdout".
  const char *stringOf_exportFile__format = NULL;
  const uint ncluster = UINT_MAX; const char *stringOf_ncluster = NULL;
  const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
  //printf("------\n\n result_file=\"%s\", at %s:%d\n", result_file, __FILE__, __LINE__);
  //! 
  { //! Case(b): API-logis using strings:
    //! 
    //! Apply logics:
    uint arg_size = 0;
    char *listOf_args__2d[10];
    char *listOf_args = construct__terminalArgString__fromInput__hp_api_fileInputTight(/*debug_config__isToCall=*/true,  
										       stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse, &arg_size, listOf_args__2d);
    //const bool is_ok = readFromFile__exportResult__hp_api_fileInputTight(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse);
    assert(listOf_args); assert(strlen(listOf_args));
    MF__expand__bashInputArg__hp_api_fileInputTight("", "Use-case: CCM:matrix-based", listOf_args__2d, arg_size);
    //printf("%s\t #! %s\n", listOf_args, "Use-case: CCM:matrix-based");
    //! ----------------------------------
    //! 
    //! Case(c): Bash-API using strings:
    //! 
    //! Apply logics:
    const bool is_ok = fromTerminal__hp_api_fileInputTight(listOf_args__2d, arg_size);
    assert(is_ok);
    //! 
   //! De-allocate:
    if(listOf_args) {free_1d_list_char(&listOf_args); listOf_args = NULL;}
  }

  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  free__s_kt_matrix(&mat_random);
  free__s_kt_matrix(&mat_computedSim);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

