{ //! Only the pre-sorting:
  const char *stringOf_measureText = "sort: cost-sort-sparse-matrix: the realtive time-overhead wrt. a pre-step where we sort seeprately each row in a dense matrix both wrt. indxes and thereafter seperately for teh weights (where we for the latter us teh ranks identifed for the indexes)"; //! Evaluate for a dense matrix of entites:	  
  //! -------------------------------
  //! Start the clock:
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  //!
  //!
  //! The experiemnt:
  sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(&obj_2, /*isTo_sortKeys=*/true);
  //! --------------
  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
}
{ const char *stringOf_measureText = "correlation: cost-compare-sparse-matrices: the relative importance of intersecting two row-sets (from the same matrix)"; //! Only the corrleaitoin-metrics, ie, xmt the pre-sorting

  //! -------------------------------
  //! Start the clock:
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  //!
  { //! The experiemnt:
	    
    t_float sumOf_values = 0;
    /* t_float *arrOf_tmp_intersect = allocate_1d_list_float(size_of_array, default_value_float); */
    /* t_float *arrOf_tmp_onlyIn_row1 = allocate_1d_list_float(size_of_array, default_value_float); */
    /* t_float *arrOf_tmp_onlyIn_row2 = allocate_1d_list_float(size_of_array, default_value_float); */

#undef __localMacro_intersectingWeights
#undef __localMacro_intersectingWeights_match_row1
#undef __localMacro_intersectingWeights_match_row2
#define __localMacro_intersectingWeights(val1, val2) ({val1 * val2;}) //! ie, a simple correlation-metric where we return the sum of elmenets,
#define __localMacro_intersectingWeights_match_row1(val) ({0;}) //! ie, a simple case where we ingore the no-matches
#define __localMacro_intersectingWeights_match_row2(val) ({0;}) //! ie, a simple case where we ingore the no-matches


    for(uint row_id = 0; row_id < nrows; row_id++) {	  
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! Get seprate rows wrt. the keys and the scores:
	uint row_size   = 0; const uint *row_1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&obj_2, row_id, &row_size);
	uint row_size_2 = 0; const uint *row_2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&obj_2, row_id_out, &row_size_2);
	const t_float *rowOf_scores_1 = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&obj_2, row_id, &row_size);
	const t_float *rowOf_scores_2 = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&obj_2, row_id_out, &row_size_2);
	// printf("[row=%u]\trow_size=%u, size_of_array=%u, at %s:%d\n", row_id, row_size, size_of_array, __FILE__, __LINE__);
	assert(row_size == size_of_array); //! ie, what we expect
	assert(row_size_2 == size_of_array); //! ie, what we expect
	assert(row_1); assert(row_size > 0);
	assert(row_2); assert(row_size_2 > 0);
	//! --------
	uint cntLocal_intersect = 0; uint cntLocal_1 = 0; uint cntLocal_2 = 0;
	uint cntLocal_onlyIn_1_added = 0; uint cntLocal_onlyIn_2_added = 0;
	//! --------
	const uint cnt_iterateBoth = (row_size <= row_size_2) ? row_size : row_size_2; // = row_size + row_size_2;
	uint col_id = 0;
	for(; col_id < cnt_iterateBoth; col_id++) {
	  // FIXME: write a pertmaution of [below] where we assuem that tehre is a higher likleyhood of vertices 'having the same id', ie, a different order wrt. the if-branch-clauses.
	  if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
	    sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
	    cntLocal_onlyIn_1_added++;
	  } else if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
	    sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
	    cntLocal_onlyIn_2_added++;
	    //arrOf_tmp_onlyIn_row2[cntLocal_onlyIn_2_added++] = row_2[cntLocal_2++];
	  } else { //! then the elemnts are equal:
	    sumOf_values += __localMacro_intersectingWeights(rowOf_scores_1[cntLocal_1], rowOf_scores_2[cntLocal_2]);
	    //arrOf_tmp_intersect[cntLocal_intersect++] = row_1[cntLocal_1];
	    cntLocal_1++;    cntLocal_2++;
	    cntLocal_intersect++;
	  }
	}

	// FIXME: introspect upon the correctness of [below] asusmption ... ie, taht the 'interseciton' case si covered in [ªbove] for-loop-iteriaotn (oekseth, 06. otk. 2016).
	if(col_id < row_size) {
	  assert(col_id >= row_size_2); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_2
	  for(uint col_id = cntLocal_1; col_id < row_size; col_id++) {
	    sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
	  }
	} else if(col_id < row_size_2) {
	  assert(col_id >= row_size); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_1
	  for(uint col_id = cntLocal_2; col_id < row_size_2; col_id++) {
	    sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
	  }
	} 
	//! We expect the sets to be completely overalpping, ie, givne our test-case:
	assert(cntLocal_1 == size_of_array);
	assert(cntLocal_2 == size_of_array);
	assert(cntLocal_intersect == size_of_array);
      }
    }
    /* //! ------------------------------- */
    /* //! De-allcoate: */
    /* free_1d_list_float(&arrOf_tmp_intersect); */
    /* free_1d_list_float(&arrOf_tmp_onlyIn_row1); */
    /* free_1d_list_float(&arrOf_tmp_onlyIn_row2); */
  }
  //! --------------
  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
}
{ const char *stringOf_measureText = "correlation: cost-compare-sparse-matrices(revert-if-luases-to(row1==row2): the relative importance of intersecting two row-sets (from the same matrix): a pertmaution of where we assuem that there is a higher likleyhood of vertices 'having the same id', ie, a different order wrt. the if-branch-clauses."; //! Only the corrleaitoin-metrics, ie, xmt the pre-sorting

  //! -------------------------------
  //! Start the clock:
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  //!
  { //! The experiemnt:
	    
    t_float sumOf_values = 0;
    /* t_float *arrOf_tmp_intersect = allocate_1d_list_float(size_of_array, default_value_float); */
    /* t_float *arrOf_tmp_onlyIn_row1 = allocate_1d_list_float(size_of_array, default_value_float); */
    /* t_float *arrOf_tmp_onlyIn_row2 = allocate_1d_list_float(size_of_array, default_value_float); */

#undef __localMacro_intersectingWeights
#undef __localMacro_intersectingWeights_match_row1
#undef __localMacro_intersectingWeights_match_row2
#define __localMacro_intersectingWeights(val1, val2) ({val1 * val2;}) //! ie, a simple correlation-metric where we return the sum of elmenets,
#define __localMacro_intersectingWeights_match_row1(val) ({0;}) //! ie, a simple case where we ingore the no-matches
#define __localMacro_intersectingWeights_match_row2(val) ({0;}) //! ie, a simple case where we ingore the no-matches

    // FIXME[jc]: may you validate correctness of [below] appraoch wrt. the correcness of sparse-amtrix-computation?

    for(uint row_id = 0; row_id < nrows; row_id++) {	  
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! Get seprate rows wrt. the keys and the scores:
	uint row_size   = 0; const uint *row_1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&obj_2, row_id, &row_size);
	uint row_size_2 = 0; const uint *row_2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&obj_2, row_id_out, &row_size_2);
	const t_float *rowOf_scores_1 = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&obj_2, row_id, &row_size);
	const t_float *rowOf_scores_2 = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&obj_2, row_id_out, &row_size_2);
	// printf("[row=%u]\trow_size=%u, size_of_array=%u, at %s:%d\n", row_id, row_size, size_of_array, __FILE__, __LINE__);
	assert(row_size == size_of_array); //! ie, what we expect
	assert(row_size_2 == size_of_array); //! ie, what we expect
	assert(row_1); assert(row_size > 0);
	assert(row_2); assert(row_size_2 > 0);
	//! --------
	uint cntLocal_intersect = 0; uint cntLocal_1 = 0; uint cntLocal_2 = 0;
	uint cntLocal_onlyIn_1_added = 0; uint cntLocal_onlyIn_2_added = 0;
	//! --------
	const uint cnt_iterateBoth = (row_size <= row_size_2) ? row_size : row_size_2; // = row_size + row_size_2;
	uint col_id = 0;
	for(; col_id < cnt_iterateBoth; col_id++) {
	  // FIXME: write a pertmaution of [below] where we assuem that tehre is a higher likleyhood of vertices 'having the same id', ie, a different order wrt. the if-branch-clauses.
	  if(row_1[cntLocal_1] == row_2[cntLocal_2]) {
	    sumOf_values += __localMacro_intersectingWeights(rowOf_scores_1[cntLocal_1], rowOf_scores_2[cntLocal_2]);
	    //arrOf_tmp_intersect[cntLocal_intersect++] = row_1[cntLocal_1];
	    cntLocal_1++;    cntLocal_2++; 	    cntLocal_intersect++;
	  } else if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
	    sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
	    cntLocal_onlyIn_1_added++;
	  } else { //if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
	    sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
	    cntLocal_onlyIn_2_added++;
	    //arrOf_tmp_onlyIn_row2[cntLocal_onlyIn_2_added++] = row_2[cntLocal_2++];
	  }
	}

	// FIXME: introspect upon the correctness of [below] asusmption ... ie, taht the 'interseciton' case si covered in [ªbove] for-loop-iteriaotn (oekseth, 06. otk. 2016).
	if(col_id < row_size) {
	  assert(col_id >= row_size_2); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_2
	  for(uint col_id = cntLocal_1; col_id < row_size; col_id++) {
	    sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
	  }
	} else if(col_id < row_size_2) {
	  assert(col_id >= row_size); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_1
	  for(uint col_id = cntLocal_2; col_id < row_size_2; col_id++) {
	    sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
	  }
	} 
	//! We expect the sets to be completely overalpping, ie, givne our test-case:
	assert(cntLocal_1 == size_of_array);
	assert(cntLocal_2 == size_of_array);
	assert(cntLocal_intersect == size_of_array);
      }
    }
    /* //! ------------------------------- */
    /* //! De-allcoate: */
    /* free_1d_list_float(&arrOf_tmp_intersect); */
    /* free_1d_list_float(&arrOf_tmp_onlyIn_row1); */
    /* free_1d_list_float(&arrOf_tmp_onlyIn_row2); */
  }
  //! --------------
  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
}
