#include "kt_clusterAlg_mcl.h" 




//static void matrix_square(s_kt_matrix_t *mat, s_kt_list_1d_float *map_feature) {
static void matrix_square(s_kt_matrix_t *mat, const s_kt_correlationMetric_t obj_metric) {
  assert(mat);
  assert(mat->nrows > 0);
  assert(mat->ncols > 0);
  //! Reduce exeuciton-tiem through compaution of a transpsoed matrix:
  s_kt_matrix_base_t obj_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(mat);
  s_kt_matrix_t mat_trans = initAndReturn_transpos__s_kt_matrix(mat);
  s_kt_matrix_base_t mat_trans_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_trans);
  // FIXME[concpetual]: validate correcntess of our itnertpation .... ie, that the matrix_square is consered with the comtpatuion of dot-product .... ie, as resultMatrix[row] = matrix[row]*matrix[][row] .... ... "matrix[v][e] * matrix[e][*]".
  // FIXME[tut+eval]: evlauat ehte implciaotn/effect of different sialmrity-emtric s .... and set the dot-rpdocut as the defualt simarlity-emtric .... 

  s_kt_matrix_base_t obj_result_shallow = initAndReturn__empty__s_kt_matrix_base_t();
  const bool is_ok = apply__hp_distance(obj_metric, &obj_shallow, &mat_trans_shallow, &obj_result_shallow, init__s_hp_distance__config_t());
  assert(is_ok);
  s_kt_matrix_t obj_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&obj_result_shallow);
  /* assert(map_feature); */
  /* assert(map_feature->list_size == mat->ncols); */
  /* //! Initialize: */
  /* for(uint col_id = 0; col_id < map_feature->list_size; col_id++) { */
  /*   map_feature->list[col_id] = 0; */
  /* } */
  /*  my ($mx) = @_; */
  /*  my $sq = {}; */
  /*  my @nodes = keys %$mx; */
  /*  for my $n (@nodes) { */
  /*     $sq->{$n} = matrix_multiply_vector($mx, $mx->{$n}); */
  /*  } */
  /*  return $sq; */
  free__s_kt_matrix(mat);
  *mat = obj_result; //! ie, 'grab' the data-set.
  free__s_kt_matrix(&mat_trans);
}



//! Influate and prune.
static t_float matrix_inflate(s_kt_matrix_t *mat, const t_float conf_cellThreshold, const t_float conf_power, const bool hasEmptyValues) {
   /* my ($mx, $I) = @_; */
   /* my @nodes = keys %$mx; */
  t_float chaos = 0;
  // FIXME[code]: ... wrtie support for both sparse and dense matrix-input-sets   
  // FIXME[code]: ... SSE .... describe/generliase support ... 
  // FIXME[code]: ... write a generlaied wrapper .... for conf_power=[1, 2, ....]

  
  //! Note: as comtpaution of simarlity-emtrics sometimes reuslts in empty values a 'glboal-empty-value-evlauitons' may be in-cracucryte.
  // FIXME[cocneptaul]: cosnider to hance form a dense-evlauation toa  sparse elvaution during the comtpatuions .... ie, as it seems like enneites are grually 'fiutlered awya'.
  //  if(hasEmptyValues) 
{ //! ********************************************************************
#define __MiF__isOf_interest(score) ({isOf_interest(score);})
    if(conf_power == 1) {
#define __MiF__pow(score) ({score;})
      // ************************************************************************************************************************************************************************************************************
      // ************************************************************************************************************************************************************************************************************
      #include "kt_clusterAlg_mcl__stub__inflate.c"
      // ************************************************************************************************************************************************************************************************************
/* #define __score mat->matrix[row_id][col_id] */
/* #if(1 == 1) */
/* #define __MiF__printScore(tag) ({;}) //! ie, a 'empty' wrapper. */
/* #else */
/* #define __MiF__printScore(tag) ({fprintf(stderr, "(change::%s)\t[%u][%u]=%f, at %s:%d\n", tag, row_id, col_id, __score, __FILE__, __LINE__);}) */
/* #endif */

/*   for(uint row_id = 0; row_id < mat->nrows; row_id++) { */
/*     //   for my $n (@nodes) { #! ie, for each row:  */
/*     t_float sum = 0; */
/*     t_float sumsq = 0; */
/*     t_float max = T_FLOAT_MIN_ABS; */
/*     /\* my $sumsq = 0; *\/ */
/*     /\* my $max = 0; *\/ */
/*     for(uint col_id = 0; col_id < mat->ncols; col_id++) { */
/*       //      for my $nb (keys %{$mx->{$n}}) { */
/*       //# FIXME: move [below] to inseriton-rpcoedure. */
/*       const t_float score = __score; */
/*       if( */
/* 	 !__MiF__isOf_interest(score) ||  */
/* 	 (score < conf_cellThreshold) ) { //! remove/prune elemnts below a certain threshold. */
/* 	//         if ($mx->{$n}{$nb} < conf_cellThreshold) { # */
/* 	__MiF__printScore("empty"); */
/* 	__score = 0; //T_FLOAT_MAX; //! ie,     delete($mx->{$n}{$nb}); */
/*       } else { */
/* 	//! Comptue "pow(...)":  */
/* 	__MiF__printScore("power"); */
/* 	assert(isOf_interest(__score));  */
/* 	__score = __MiF__pow(__score); //$mx->{$n}{$nb} **= $I; */
/* 	assert(isOf_interest(__score)); */
/* 	sum += __score; //! ie, the sum-of-features. */
/*       } */
/*     } */
/*     if(sum != 0) { //! then we adjsut by the sum and copmtue the global-squared-sum. */
/*       const t_float sum_inv = 1.0/sum; */
/*       for(uint col_id = 0; col_id < mat->ncols; col_id++) { */
/* 	//      for my $nb (keys %{$mx->{$n}}) { */
/* 	//# FIXME: move [below] to inseriton-rpcoedure. */
/* 	const t_float score = __score; */
/* 	if(__MiF__isOf_interest(score) ) { */
/* 	  __score = macro_mul(__score, sum_inv); //! ie, $mx->{$n}{$nb} /= $sum; */
/* 	  assert(isOf_interest(__score)); */
/* 	  __MiF__printScore("average"); */
/* 	  sumsq = macro_pluss(sumsq, __score * __score);  //! sum x_i^2 over stochastic vector x */
/* 	  max = macro_max(max, __score); */
/* 	} */
/* 	//$max = $mx->{$n}{$nb} if $max < $mx->{$n}{$nb}; */
/*       } */
/*     } */
/*     const t_float diff_local = max - sumsq; */
/*     chaos = macro_max(chaos, diff_local); */
/*   } */
/* #undef __score */
/* #undef __MiF__printScore */

      // ************************************************************************************************************************************************************************************************************
      // ************************************************************************************************************************************************************************************************************
#undef __MiF__pow
    } else if(conf_power == 2) {
#define __MiF__pow(score) ({score*score;})
#include "kt_clusterAlg_mcl__stub__inflate.c"
#undef __MiF__pow
    } else {
#define __MiF__pow(score) ({mathLib_float_pow(score, conf_power);})
#include "kt_clusterAlg_mcl__stub__inflate.c"
#undef __MiF__pow
    }
#undef __MiF__isOf_interest
  } /* else { //! ******************************************************************** */
/* #define __MiF__isOf_interest(score) ({true;}) */
/*     if(conf_power == 1) { */
/* #define __MiF__pow(score) ({score;}) */
/* #include "kt_clusterAlg_mcl__stub__inflate.c" */
/* #undef __MiF__pow */
/*     } else if(conf_power == 2) { */
/* #define __MiF__pow(score) ({score*score;}) */
/* #include "kt_clusterAlg_mcl__stub__inflate.c" */
/* #undef __MiF__pow */
/*     } else { */
/* #define __MiF__pow(score) ({mathLib_float_pow(score, conf_power);}) */
/* #include "kt_clusterAlg_mcl__stub__inflate.c" */
/* #undef __MiF__pow */
/*     } */
/* #undef __MiF__isOf_interest */
/*   } //! ******************************************************************** */
  //!
  //! @return
  return chaos;       //! only meaningful if input is stochastic
}


/* # assumes but does not check doubly idempotent matrix. */
/* # can handle attractor systems of size < 10. */
static void matrix_interpret(s_kt_matrix_t *mat, t_float conf_postEval_pruning, s_kt_clusterAlg_fixed_resultObject_t *obj_result) {  /* # recognizes/preserves overlap. */
  //, s_kt_clusterAlg_fixed_resultObject_t *obj_result

  assert(false); // FIXME: ... seems like trhe transpsoed-amtrix-ops ... requrei/epxects an adjcnecy-amtrix as input ... vlaidte latter .... 
  assert(false); // FIXME: ... 
  assert(false); // FIXME: ... 



  assert(false); // FIXME[code]: insert a eprtmatuioon of our "__ccmDynamic_disjointForest_dbScan__hpLysis_api(..)" ("hpLysis_api.c")

  assert(false); // FIXME: correcntess .... symemtric poerpty wrt. the matrix ... while the MCL-proceudre discards both for the head-and-tail-vertex .... the DB-SCAN-proceudre only discards for the ehad-vertex .... ie, for which the MCL distjoint-forest-amtrix is guirnatted to be symemtric while hte DB-SCAN-dcisjtoint-input-amtrix may (for some caseS) be a-symmetirc ... hence the errnorus apoplciaotn/use of the current DB-SCAN impemtatnion-strateiges ... 
  assert(false); // FIXME[]: figure out how to ahndle the cases where "mapOf_intersting[row_id] == false", ie, where the altter impleis that a vertex is part of muliple clsuters .... an appraoch is to compute cluster-centers ... then assign veritces to the clsoest clusters ...  ie, simliar/overallping to our mini-batch-proceudre .... 
  assert(false); // FIXME[]: add storage-rotuiens to store the result in a HCA-tree ..... ie, call our udpated disjtoint-forest-proceudre ....??... 

  assert(false); // FIXME: consider using direclty the [below] proerpty wrt. data-merging ... ie, instead of explcitly calling our distjoitn-froest-impeltmatnion-cod ... 

  // FIXME[pre]: write a C/C++ funciton of [below] .... 
  // FIXME[pre]: seems like [”elow] is exaclty simlair to DB-scan .... ie, wrt. the use of " < 0.1" for cell-pair-filtering ... and the use of [][] for row-based filters ... and the sue of tree-rversla-procuedre ... 
  // FIXME: seems like [below] is a db-scan-permtaution ... ie, where one searhces for rechabilty of veritces .... 

   /* my ($limit) =  @_; */
   /* my $clusters=  {};   # hash of arrayrefs. */
   /* my $attrid  =  {}; */
   /* my $clid    =  0; */

   /* for my $n (keys %$limit) {           # crude removal of small elements. */
   /*    for my $nb (keys %{$limit->{$n}}) { */
   /*       delete $limit->{$n}{$nb} if $limit->{$n}{$nb} < conf_postEval_pruning; */
   /*    } */
   /* } */



   /* # FIXME: understanding the [”elow] "grep(..)" calll ...   */
   /* my $attr    =  { map { ($_, 1) } grep { $limit->{$_}{$_} } keys %$limit }; */
   /*                      # _ contract 'connected attractors', assign cluster id. */
   /* for my $a (keys %$attr) { */
   /*    next if defined($attrid->{$a}); */
   /*    my @aa = ($a); */
   /*    while (@aa) { */
   /*       my @bb = (); */
   /*       for my $aa (@aa) { */
   /*          $attrid->{$aa} = $clid; #! ie, the cluster-id. */
   /* 	    # FIXME: understanding the [”elow] "grep(..)" calll ....  */
   /* 	    #! collect vertices which are marked as ...  */
   /*          push @bb, grep { defined($attr->{$_}) } keys %{$limit->{$aa}};  */
   /*       } */
   /*       @aa = grep { !defined($attrid->{$_}) } @bb; */
   /*    } */
   /*    $clid++; */
   /* } */

   /* for my $n (keys %$limit) { */
   /*    if (!defined($attr->{$n})) {     # look at attractors */
   /* 	  #! Then make $n$ part of all of the clusters which the 'root-nodes' are member/part of .... where a root-node is (in this contenct/genre/interpreation) understood/viewed as ( (pair[i][i] >= threshold) && (...) )  */
   /*       for my $a (grep { defined($attr->{$_}) } keys %{$limit->{$n}}) { */
   /*          $clusters->{$attrid->{$a}}{$n}++; */
   /*       } */
   /*    } else { #! then we 'add' "$n" to cluster-id-"$attrid->{$n}"  */
   /*       $clusters->{$attrid->{$n}}{$n}++;  */
   /*    } */
   /* } */
   /* return $clusters; */
}




bool compute__kt_clusterAlg_mcl(s_kt_clusterAlg_mcl_t *self, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_kt_api_config_t clusterConfig) {
  if(!obj_1 || (obj_1->nrows == 0) ) {
    fprintf(stderr, "!!\t(input-erorr)\t Input does Not seem like an adjcnecy-amtrix, ie, pelase udpate your input-call, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  } else if(obj_1->nrows != obj_1->ncols)  {
    fprintf(stderr, "!!\t(input-erorr)\t Input does Not seem like an adjcnecy-amtrix, ie, pelase udpate your input-call, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  bool hasEmptyValues = false;
  { //! Investiate if there are empty valeus in the data-set, ie, an appraoch to reduce the exueciont-tiem-costs.
    const s_kt_matrix_t *mat = obj_1;
#define __score mat->matrix[row_id][col_id]
    for(uint row_id = 0; row_id < mat->nrows; row_id++) {
      uint col_id = 0;
      t_float score_max = __score; col_id++;
      for(; col_id < mat->ncols; col_id++) { score_max = macro_max(score_max, __score);}
      if( (score_max == T_FLOAT_MAX) || (score_max == T_FLOAT_MIN_ABS)) {hasEmptyValues = true;}
    }
#undef __score
  }
/*
    The MCL algorithm cosnsits of the following steps:
    (1) pre: max-score: identify the max-socre for each vertex scoring the socre in a max-score-list: if max-socre is not set then defualt value is '0';
    (2) pre: average: adjsut each score by the sum of scores, ie, $d(i, k) / \sum d(i, *)$;
    (3) MCL-while: iterate until the 'chaos'-score is less than a pre-deifned threshold (eg, threshold=0.001): 
    (3.a) simliarty-metirc: comptuet eh simlarity between the matrix and its transposed atrmxi, eg, a co-veraiance-amtrix;
    (3.b) contriubtion: adjust the values in/through the pwoer of 'p' and then average the valeus (ie, apply/compute $d(i, k) / \sum d(i, *)$);
    (3.c) chaos: computer/understand chaos as $ max_i\{ max(d(i, *)) - \sum(d(i,k)*d(i,k))\} $
    (4) post: merge the forests for all matrix[i][i] which are of itnerest. Thereafter assign the veritces "i" were "ofInterst(matrix[i][i] == false)" to the cluster-identites cluster(k) in "matrix[i][k with cluster(k) where ofInterst(k)]" (to muliepl clsuters). .... Note: based on the sample-Perl-implemtatnion provided by ....??.. assumes tha matrix is symmetric ... an assumption which is ......??.... 

// FIXME[new-artilce]: ... evaluate different interptaiton-appraoches for both 'chaos' and 'contribution' .... using latter reuslts to ......??.... where we from related-work observes how the latter .....??.... 
// FIXME[new-artilce]: ... 
// FIXME[new-artilce]: ... 
  // FIXMe[conpetual]: add support for ... sparse-data-sets .... where challenge consersn ....??.... 
  // FIXMe[conpetual]: add support for ... 
*/


  s_kt_matrix_t obj_local = setToEmptyAndReturn__s_kt_matrix_t(); 
  const bool is_ok = init__copy__s_kt_matrix(&obj_local, obj_1, false);
  assert(is_ok);
  if(false) { //! then we write otu the generated matrix:
    {
      const char *exportFile = "tmp.mcl_input.tsv";
      fprintf(stderr, "(info::debug)\t Exports file=\"%s\" before comptuign disjtoint-forests, at %s:%d\n", exportFile, __FILE__, __LINE__);
      const bool is_ok = export__singleCall__s_kt_matrix_t(&obj_local, exportFile, NULL);
      assert(is_ok);
    }
  }
  {
    //! Idnetify the max-score for each ... set "matrix[i][i]=max-score": 
    //! Note: [below] correpsodons to the "matrix_add_loops($mx);" in the "simplied_algo_minimcl.pl" MLC-algortihm-impemtantio.
    s_kt_matrix_t *mat = &obj_local;
#define __score mat->matrix[row_id][col_id]
    for(uint row_id = 0; row_id < mat->nrows; row_id++) {
      uint col_id = 0;
      t_float score_max = T_FLOAT_MIN_ABS; //__score; col_id++;
      //      t_float score_max = __score; col_id++;
      for(; col_id < mat->ncols; col_id++) { if(isOf_interest(__score)) {score_max = macro_max(score_max, __score);}}
      if(score_max == 0) {score_max = 1;}
      assert(isOf_interest(score_max));
      //fprintf(stderr, "(set-score)\t[%u][%u]=%f, at %s:%d\n", row_id, row_id, __score, __FILE__, __LINE__);
      mat->matrix[row_id][row_id] = score_max;
    }
#undef __score
  }
  if(false) { //! then we write otu the generated matrix:
    const char *exportFile = "tmp.mcl_afterAddLoops.tsv";
    fprintf(stderr, "(info::debug)\t Exports file=\"%s\" before comptuign disjtoint-forests, at %s:%d\n", exportFile, __FILE__, __LINE__);
    const bool is_ok = export__singleCall__s_kt_matrix_t(&obj_local, exportFile, NULL);
    assert(is_ok);
  }
  //! 
  //! Call matrix-"matrix_inflate(..)" with "$I=1", ie, where the [”elow] call is sued to ajdust each valeu by the average-score.
  // FIXME[tut+eval]: evaluate a nraomziaotn-aprpaoch wehre we adjsut by the average score, ie, as used/seen oin [”elow].
  matrix_inflate(&obj_local, self->conf_cellThreshold, /*self->conf_pow=*/1, hasEmptyValues);
  if(false) { //! then we write otu the generated matrix:
    const char *exportFile = "tmp.mcl_afterInflate.tsv";
    fprintf(stderr, "(info::debug)\t Exports file=\"%s\" before comptuign disjtoint-forests, at %s:%d\n", exportFile, __FILE__, __LINE__);
    const bool is_ok = export__singleCall__s_kt_matrix_t(&obj_local, exportFile, NULL);
    assert(is_ok);
  }
  //  s_kt_matrix_t obj_local = matrix_make_stochastic(obj_1);
  assert(obj_local.nrows > 0);
  assert(obj_local.ncols > 0);
  // matrix_dump($mx, 3, "start") if $::verbose;
  t_float ite = 0;
  t_float chaos = 1;
  loint count_index = 1;
  // FIXME: add max-iter
  // FIXME[cocneptaul]: seems like MCL is a cobmaiton of iterative-iegen-vector and DB-SCAN ... where a meausre of convergence is used to dientify the stop-ciritera  for the iteraiotn-phase .... and where point/motviaotn fot eh iteraiotn-pahse is to relax the vertices ..... 
  // FIXME[new-aritlce]: .... to explreo the implciaotn-prediicont-effects of different centrlaity-based methods on DB-scan-algorithm-permtauiotns .... hweren novelty/motivaiton cosners how we manages to dietfy/elvuaate .......??.... 
  loint cnt_sinzeLastMerge = 0;
  
  //!
  //! Start the iteration:
  // FIXME[cocneptual]: evlauat ethe imapct of [”elow] new-added "count_index >= obj_local.nrows" calsue ... which is used to avodi ernorsu cofniguraiton wrt. diffnere simarlity-etmrics  and data-sets .... where we from an epxiermental observiaotn ahve observed how CML rpdouce pointless/errnorus results for .... 
  // FIXMe [tut+perf]: evlauate different combiantsoiin wrt. [below] .... for differnet sysntic and real-flie data-sets .... then udpate our "tut_clust_mcl.c"
  while( (chaos > self->conf_minError) 
	 || (count_index < (loint)obj_local.nrows) 
	 ) {
    if(true) {
      if(self->convergence_minClusters > 1) { //! Compute a temporary result of MCL, ie, to resovle case where the MCL algorithm resutls in clsuters to converges towards zero .... 
	// FIXME[related-work]: try idnetyign simalri proceudres/aprpaoches .... ie, to avoid the MCL-covnerngece-prcoeudre from wrongly merging/remivng any siamrlityes wrt. accuracy .... 
	// FIXME[article]: update our aritle wrt. this rpcoeudre/strategy .... ie, hwo we address issues/coenrsn wirt. overfitting of relationshisp 
	// FIXME[concpetaul]: consider to evalaute/use CCMs to ivnestiate/assert clsuter-threshold-convergence. 
	//fprintf(stderr, "disjoint-cnt-calls, at %s:%d\n", __FILE__, __LINE__);
#include "kt_clusterAlg_mcl__stub__disjoint.c" //! ie, apply logics	
      }
      cnt_sinzeLastMerge = 0; //! ie, reset.      
    } else {cnt_sinzeLastMerge++;;} //! ie, increment.

    //! For each vertex/row call the "matrix_multiply_vector(..)" function: in the latter funciton we compute "sum[k] += (row[i][i] * row[i][k])". 
    //    my $sq = 
    matrix_square(&obj_local, self->obj_metric);
    //      my $progress = sprintf "chaos %.5f ite %d", $chaos, $ite;
    //    matrix_dump($sq, 3, "X $progress") if $::verbose;
    //! Call 'pow(..)' on each cell ... noramlize/adjust each score-value .... 
    chaos = matrix_inflate(&obj_local, self->conf_cellThreshold, self->conf_pow, hasEmptyValues);
    
    //    if(cnt_sinzeLastMerge > self->convergence_minClusters__cntIteraitonsBetweenEach) 

    /* matrix_dump($sq, 3, sprintf "I $progress") if $::verbose; */
    /* print STDERR "$progress\n" if !$::verbose; */
    /* $mx = $sq; */
    count_index++;
    // TODO[eval]: evlauate correntess of [below]
    if(count_index > (loint)(obj_local.nrows * obj_local.nrows)) {chaos = self->conf_minError;} //! ie, then break the exeuciotn.
    // fprintf(stderr, "iter[%u] given nrows=%u, at %s:%d\n", (uint)count_index, obj_local.nrows, __FILE__, __LINE__);
  }
  if(false) { //! then we write otu the generated matrix:
    /* { */
    /*   const char *exportFile = "tmp.mcl_input.tsv"; */
    /*   fprintf(stderr, "(info::debug)\t Exports file=\"%s\" before comptuign disjtoint-forests, at %s:%d\n", exportFile, __FILE__, __LINE__); */
    /*   const bool is_ok = export__singleCall__s_kt_matrix_t(obj_1, exportFile, NULL); */
    /*   assert(is_ok); */
    /* } */
    {
      const char *exportFile = "tmp.mcl_afterMCLFilter.tsv";
      fprintf(stderr, "(info::debug)\t Exports file=\"%s\" before comptuign disjtoint-forests, at %s:%d\n", exportFile, __FILE__, __LINE__);
      const bool is_ok = export__singleCall__s_kt_matrix_t(&obj_local, exportFile, NULL);
      assert(is_ok);
    }
  }
  //! 
  //! Apply the post-idneitficoant-step:
  { //! Comptue disjoint regions:
#include "kt_clusterAlg_mcl__stub__disjoint.c" //! ie, apply logics
  }
  
 free__s_kt_matrix(&obj_local);
 //!
 //! @return
 return true;
}
