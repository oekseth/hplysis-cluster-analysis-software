#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "db_ds_directMapping.h"
//#include "db_ds_bTree_keyValue.h"
//#include "db_ds_bTree_rel.h"
#include "kt_matrix.h"
#include "measure_base.h"
/**
   @brief demonstrates how a to build and access a data-structre using KNittingTools optmized data-access-pattersk
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- API: in this example we make use of logics defined in our "db_ds_directMapping.h"   
   -- Idea: examplify differnet use-case-patterns when using KnttingTools optmized data-access-pattern (where we use our "db_ds_directMapping.h"). A use-case conserns the case where we for each head 'store' 20 differnet relations: our 'simplicaiton' is that we 'only need' to access the 20 relations (in each of our iteraitons); 
   -- Simplication(implication): we 'insert cases' using a [rows/(cntKeys(tailObject)*|tails|*|synonyns|)][tails] appraoch;
   @remarks simplificaiotn: 
   -- overall: we do Not explictly insert synonyms. Instead we 'adjust' the number of insert-opreations and search-operations. 
   -- soundness: the correcdtness of our 'simplied apporach/assumpition' is based on the observaiton that (a) a 'direct normlized data-set with "nrows" relations and with on average "cnt_synsEach" for (head, rt, tail)', will corrspond to a graph with on average "nrows * cnt_synsEach" number of 'non-normalized' relationships.
   -- concrete: for data-access-cases Not desingd to handle synonyms we evlauate "nrows" cases, while 'for data-access-chems designed wrt. synonym-noramlizaiotn-handling' we evaluate only "nrows/(cnt_synsEach*cnt_synsEach*cnt_synsEach)" evalaution-cases.
   @remarks from the teresults we observe that:
   -- result(a): VS "key-->value" B-trees: 
   -- result(b): VS 'direct-list-access': 
   -- result(c): VS 'non-randomized access': 
   @todo try to 'apply' above observations ... ie, to 'make use of these to suggest novel finins in researhc'.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const uint nrows_base = 1000*100; 
  //const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
  const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
  const bool isTo_useRandomAccess = true;
  const bool isTo_useKeyBasedSearchPattern = true; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
  //! -------------------------------------------------
  const uint cnt_synsEach = 5;
  const uint cnt_predicatesForTails = 20;
  const uint config__cntColsEach = 20;
  //! -------------------------------------------------
  uint current_search = 0;
#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} else {if(current_search++ > nrows) {current_search = 0;} val_ret = current_search;} val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = search_id; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} val_ret;})
#endif
  //!
  //! Iterate through the differnet 'row-size-cases':

  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id < nrows) {val_ret = (nrows-1) % search_id;} else {val_ret = search_id % (nrows-1);} val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id > 0) {if(search_id < nrows) {val_ret = nrows % search_id;} else {val_ret = search_id % (nrows-1);}}  val_ret;})
  //long long initseed = (configure_function_uniform_dataType)time(0);
  long long initseed = 100; //! ie, to ensure that the randomoized funciton r'eturns the same result for all our cese', ie, in cotnrast to a 'nromaø' "(configure_function_uniform_dataType)time(0);" call.
  srand(initseed);  
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id > 0) {val_ret = nrows % search_id;}  val_ret;})  
  const uint config__keysToUse = 4; //! ie, ie, (head, rt, tail, relation_id)
  //printf("------------- at %s:%d\n", __FILE__, __LINE__);
  t_float result = 0;
  {
    const uint nrows =  nrows_base;
    //! ------------------------------------------------------------
    const uint nrows__adjustedTo_db_rel =  (nrows / config__keysToUse);
    const uint nrows__adjustedTo_db_rel_tails =  (nrows / (/*keyImpact=*/config__keysToUse*/*tailImpact=*/config__cntColsEach));
    const uint cnt_syns = (cnt_synsEach*cnt_synsEach*cnt_synsEach);
    const uint nrows__adjustedTo_db_rel_tails__syns =  (nrows / (/*keyImpact=*/config__keysToUse*/*synonmImpact=*/cnt_syns*/*tailImpact=*/config__cntColsEach));
    const uint cnt_searches_adjusted_db_rel = (cnt_searches / config__keysToUse);
    const uint cnt_searches_adjusted_db_rel__syn = (cnt_searches / (config__keysToUse*cnt_syns));
    //! ------------------------------------------------------------
    assert(nrows__adjustedTo_db_rel_tails__syns > 0);
    assert(nrows > 0);     assert(nrows__adjustedTo_db_rel > 0);
    float cmp_time_insert = FLT_MAX; float cmp_time_search = FLT_MAX;
    uint *arrOf_uints;
    uint col_cnt_insert = 0;     uint col_cnt_search = 0;
    { //! Construct a 'reference-set' where we use a 1d-list:
      start_time_measurement();
      arrOf_uints = (uint*)malloc(sizeof(uint)*nrows);
      assert(arrOf_uints);
      for(uint row_id = 0; row_id < nrows; row_id++) {
	arrOf_uints[row_id] = row_id;
      }      
      cmp_time_insert = end_time_measurement("Simple list-access(insert)", 0);
      //! 
      //! Search:
      start_time_measurement();
      for(uint search_id = 0; search_id < cnt_searches; search_id++) {
	const uint row_id = __Mi__getRandVal(search_id);
	//const uint row_id = search_id % nrows;
	result +=  (t_float)arrOf_uints[row_id];
      }
      cmp_time_search = end_time_measurement("Simple list-access(search)", 0);
    }

    //!
    //! Insert: 
    { //! Case: B-tree w/"s_db_rel_t" mapping: 
      for(uint blockCase = 0; blockCase < e_db_ds_directMapping__initConfig_undef; blockCase++) {
	start_time_measurement();
	uint __nrowsLocal = nrows__adjustedTo_db_rel_tails;
	uint __local__cntSyns = cnt_searches_adjusted_db_rel;
	if(useOptimizedSynMappings__e_db_ds_directMapping__initConfig((e_db_ds_directMapping__initConfig_t)blockCase)) {
	  __nrowsLocal = nrows__adjustedTo_db_rel_tails__syns;
	  __local__cntSyns = cnt_searches_adjusted_db_rel__syn;
	}
	s_db_ds_directMapping_t obj = init__enumConfig__s_db_ds_directMapping_t(/*cnt_head=*/nrows, /*cnt_relations=*/nrows, /*enum_id=*/(e_db_ds_directMapping__initConfig_t)blockCase);
	/* { //! Add synonyms */

	/*   assert(false); // FIXME[conceptual] describe how we are to 'isnert synonyms wrt. name-sapce' in our "tut_db_bTree_simpleExample_3__2dList.c". */

	/*   //! Add synonyms for: head, tail: */
	/*   assert(false); // FIXME: add seomthign. */
	/*   //! Add synonyms for: relation-type (rt): */
	/*   for(uint i = 0; i < cnt_predicatesForTails; i++) { */
	/*   assert(false); // FIXME: add seomthign.	     */
	/*   } */
	/* } */
	uint row_id_local = 0;
	uint current_pred = 0;
	for(uint row_id = 0; row_id < __nrowsLocal; row_id++) {
	  for(uint tail_id = 0; tail_id < config__cntColsEach; tail_id++) {
	    s_db_rel_t key = __M__init__s_db_rel_t();
	    key.head = row_id_local;
	    if(current_pred >= cnt_predicatesForTails) {current_pred = 0;} else {current_pred++;}
	    key.tailRel.rt = current_pred;
	    //key.tailRel.rt = row_id_local + 1;
	    key.tailRel.tail = row_id_local + 1;
	    key.tailRel.relation_id = row_id_local + 2;
	    //! Appply lgoics:
	    insertRelation__s_db_ds_directMapping_t(&obj, key);
	    row_id_local += config__keysToUse;
	  }
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "directMapping(%s)::insert", getString__e_db_ds_directMapping__initConfig((e_db_ds_directMapping__initConfig_t)blockCase));
	  const float endTime = end_time_measurement(str_local, cmp_time_insert);
	}
	//! 
	//! Search:
	start_time_measurement(); //! ie, start the time-emasurement:
	uint max_rowId = nrows - config__keysToUse - 1;
	current_pred = 0;
	for(uint search_id = 0; search_id < __local__cntSyns; search_id++) {
	  uint row_id = __Mi__getRandVal(search_id);
	  row_id = macro_max(max_rowId, row_id);
	  s_db_rel_t key = __M__init__s_db_rel_t();
	  key.head = row_id;
	  if(current_pred >= cnt_predicatesForTails) {current_pred = 0;} else {current_pred++;}
	  key.tailRel.rt = current_pred;
	  key.tailRel.tail = row_id + 1;
	  key.tailRel.relation_id = row_id + 2;
	  //! Appply lgoics:
	  //s_db_rel_t result_obj = __M__init__s_db_rel_t();
	  //result += 
	  //printf("key = %u, at %s:%d\n", (uint)key.head, __FILE__, __LINE__);
	  uint cnt_interesting = 0;
	  (t_float)findRelation__cntInteresting__s_db_ds_directMapping_t(&obj, key, &cnt_interesting, /*namesAlreadyMappedIntoSynonyms=*/false, NULL, true); //, (isTo_useKeyBasedSearchPattern == false));
	  result += (t_float)cnt_interesting;
	}
	{ //! Complete time-measurement:
	  char str_local[2000] = {'\0'}; sprintf(str_local, "directMapping(%s)::search", getString__e_db_ds_directMapping__initConfig((e_db_ds_directMapping__initConfig_t)blockCase));
	  const float endTime = end_time_measurement(str_local, cmp_time_search);
	  //! Update oru result-set:
	}
	//!
	//! De-allocate:
	free__s_db_ds_directMapping_t(&obj);
      }
    }
    //!
    //! De-allocate:
    assert(arrOf_uints);  free(arrOf_uints); arrOf_uints = NULL; //! ie, de-allocate.
  }
  //!
  //! De-allocate:


  //!
  //! @return
#ifndef __M__calledInsideFunction
#undef __Mi__getRandVal
  return true;
#endif
}
