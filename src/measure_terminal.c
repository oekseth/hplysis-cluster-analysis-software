#include "measure_terminal.h"
#include "kt_terminal.h"
#include "measure_base.h"
#include "kt_api.h" //! which (among others) is used for comptatuion of corrleation-atmrix in oru [”elow] exampels.
#include "correlation_base.h"
//! ------------------------
//! Libraries for file-copying:
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
//! ------------------------


//! @remarks a function which examplfieis the use fo the "kt_matrix" code.
static void export_matrix() {

  { //! Case(1): export a matrix using differnet result-syntexesi: do Not use any string-labels:
  //! Validate the correctness of our 'setters'.

    //! ----------------------
    //!
    //! Intiate:
    const uint nrows = 10; const uint ncols = 20;
    s_kt_matrix_t obj_matrix; init__s_kt_matrix(&obj_matrix, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
    printf("nrows=%u, ncols=%u, at %s:%d\n", obj_matrix.nrows, obj_matrix.ncols, __FILE__, __LINE__);
    assert(obj_matrix.nrows == nrows);
    assert(obj_matrix.ncols == ncols);

    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	const t_float value = (t_float)col_id;
	set_cell__s_kt_matrix(&obj_matrix, row_id, col_id, value); //! ie, set the value
	//! Validate:
	t_float result_value = T_FLOAT_MAX;
	get_cell__s_kt_matrix(&obj_matrix, row_id, col_id, &result_value);
	assert(result_value == value); //! ie, what we expect for cosnstency.
      }
    }


    //! ----------------------
    //!
    { //! Export, first a generic appraoch:
      const char *stringOf_resultFile = "ex_tsv_2.tsv";
      //const char *stringOf_resultFile = "ex_tsv.tsv";
      //const char *stringOf_resultFile = "tmp/ex_tsv.tsv";
      printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
      //! First convifugre the result-process:
      export_config__s_kt_matrix_t(&obj_matrix, stringOf_resultFile);
      export__s_kt_matrix_t(&obj_matrix);
      closeFileHandler__s_kt_matrix_t(&obj_matrix);

      //!
      { //! Load the matrix 'gneerated to teh file' into memory:
	//!
	//! Standard 'calls' for data-loading: 
	//! A simplicit approach to overeocme the issue wrt. 'read-rigths after we have engerated a result-file:

	s_kt_matrix_t obj_new;  setTo_empty__s_kt_matrix_t(&obj_new);
	const bool is_ok = readFrom__file__s_kt_matrix_t(&obj_new, stringOf_resultFile);
	assert(is_ok); //! ie, as we expec thte operation was a sucesses.

	//! 
	//! Validate that the inptu-valeus correpsodns to our exepctaiotns:
	assert(obj_new.ncols >= obj_matrix.ncols);
	assert(obj_new.nrows >= obj_matrix.nrows);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint col_id = 0; col_id < ncols; col_id++) {
	    //! Validate:
	    t_float result_value = T_FLOAT_MAX;
	    get_cell__s_kt_matrix(&obj_matrix, row_id, col_id, &result_value);
	    if(result_value != 0) {
	      //! Validate:
	      t_float result_value_2 = T_FLOAT_MAX;
	      get_cell__s_kt_matrix(&obj_new, row_id, col_id, &result_value_2);
	      if(result_value_2 != result_value) {
		fprintf(stderr, "!!\t Different-results-at[%u][%u]: %f VS %f, at %s:%d\n", row_id, col_id, result_value, result_value_2, __FILE__, __LINE__);
	      }
	      assert(result_value == result_value_2); //! ie, what we expect for cosnstency.
	    }
	  }
	}
	assert(obj_new.ncols == obj_matrix.ncols);
	assert(obj_new.nrows == obj_matrix.nrows);
	
	//!
	//! Apply a correlation-metric (uinsg our "kt_api" interface) to the input-matrix:
	s_kt_matrix_t obj_result; setTo_empty__s_kt_matrix_t(&obj_result);
	s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none);


	printf("\n\n # Compute distance-matrix, at %s:%d\n", __FILE__, __LINE__);

	assert(false); // FIXME: consider to udpat ehte "config_corr" ... hwo do we Know if thye matrix is an 'ajdcnecy-matrix'?
	s_kt_api_config_t config__corr = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, NULL);
	const bool is_ok_corr = kt_matrix__distancematrix(obj_metric, /*matrix-1=*/&obj_new, /*matrix-2=*/&obj_new, /*result=*/&obj_result, config__corr);
	assert(is_ok_corr); //! ie, as we expec thte operation was a sucesses.


	//!
	//! Re-export the results (and mnaully validate the results):
	const char *stringOf_resultFile_corrMatrix = "tmp/ex_corrMatrix.tsv";
	export_config__s_kt_matrix_t(&obj_result, stringOf_resultFile_corrMatrix);
	export__s_kt_matrix_t(&obj_result);


	//! ----------------------------------------------------------------
	//!
	//! De-allocate:
	free__s_kt_matrix(&obj_new);
	free__s_kt_matrix(&obj_result);
      }
    }
    //! ----------------------
    //!
    { //! Export, for mulitple TSV-files:
      const char *stringOf_resultFile = "tmp/ex_tsv_mult.tsv";
      printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
      //! First convifugre the result-process:
      export_config_extensive_s_kt_matrix_t(&obj_matrix, stringOf_resultFile, /*isTo_useMatrixFormat=*/true, /*isTo_inResult_isTo_useJavaScript_syntax=*/false, /*isTo_inResult_isTo_useJSON_syntax=*/false, /*stringOf_header=*/NULL, /*include_stringIdentifers_in_matrix=*/false, /*cnt_dataObjects_toGenerate=*/2);
      //! Note: to sinmplify our code we (though meaningless wrt. real-flie-applciatiosn) exprot the same data-set muliple times:
      export__s_kt_matrix_t(&obj_matrix);
      export__s_kt_matrix_t(&obj_matrix);
    }
    // export_dataSet_s_dataStruct_matrix_dense_matrix_tab__s_kt_matrix_t(
    

    //! ----------------------
    //!
    //! De-allocate:
    free__s_kt_matrix(&obj_matrix);
  }
  //! ------------------------------------------------------------------------------------------------------------
  { //! Case(2): export a matrix using differnet result-syntexesi: use string-labels AND export the/a weight-table
    //! ----------------------
    //!
    //!
    //! ----------------------
    //!
    //! Intiate:
    const uint nrows = 10; const uint ncols = 10;
    s_kt_matrix_t obj_matrix; init__s_kt_matrix(&obj_matrix, nrows, ncols, /*isTo_allocateWeightColumns=*/true);
    assert(obj_matrix.nrows == nrows);
    assert(obj_matrix.ncols == ncols);


    //!
    //! Set the weights::
    for(uint col_id = 0; col_id < ncols; col_id++) {
      const t_float value = (t_float)col_id;
      set_weight__s_kt_matrix(&obj_matrix, col_id, value);
      //!
      //! What we expect:
      t_float value_cmp = T_FLOAT_MAX;
      get_weight__s_kt_matrix(&obj_matrix, col_id, &value_cmp);
      assert(value_cmp == value);
    }


    //!
    //! Set the labels:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      char stringTo_add[1000] = {'\0'};
      sprintf(stringTo_add, "row-%u", row_id);
      //! The call:
      set_string__s_kt_matrix(&obj_matrix, /*index_pos=*/row_id, stringTo_add, /*addFor_column=*/false);
      //!
      //! What we expect:
      char *stringOf_cmp = NULL;
      // printf("---------------, at %s:%d\n", __FILE__, __LINE__);
      get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/row_id, /*addFor_column=*/false, &stringOf_cmp);
      // printf("\t\trow[%u]=\"%s\", at %s:%d\n", row_id, stringOf_cmp, __FILE__, __LINE__);
      assert(stringTo_add); assert(strlen(stringTo_add));
      assert(stringOf_cmp); assert(strlen(stringOf_cmp));
      const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
      if(is_equal == false) {
	fprintf(stderr, "!!\t The comapred strings were different at row[%u] = \"%s\" VS \"%s\", at %s:%d\n", row_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
      }
      assert(is_equal);
    }
    for(uint col_id = 0; col_id < ncols; col_id++) {
      char stringTo_add[1000] = {'\0'};
      sprintf(stringTo_add, "col-%u", col_id);
      //! The call:
      set_string__s_kt_matrix(&obj_matrix, /*index_pos=*/col_id, stringTo_add, /*addFor_column=*/true);
      //!
      //! What we expect:
      char *stringOf_cmp = NULL;
      get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/col_id, /*addFor_column=*/true, &stringOf_cmp);
      const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
      if(is_equal == false) {
	fprintf(stderr, "!!\t The comapred strings were different at col[%u] = \"%s\" VS \"%s\", at %s:%d\n", col_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
      }
      assert(is_equal);
    }

    //!
    //! Set the strings:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	const t_float value = (t_float)col_id;
	set_cell__s_kt_matrix(&obj_matrix, row_id, col_id, value); //! ie, set the value
	//! Validate:
	t_float result_value = T_FLOAT_MAX;
	get_cell__s_kt_matrix(&obj_matrix, row_id, col_id, &result_value);
	assert(result_value == value); //! ie, what we expect for cosnstency.
      }
    }


    //! ----------------------
    //!
    { //! Export, first a generic appraoch:
      const char *stringOf_resultFile = "tmp/ex_matrixWithLabels.tsv";
      printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
      //! First convifugre the result-process:
      export_config__s_kt_matrix_t(&obj_matrix, stringOf_resultFile);
      export__s_kt_matrix_t(&obj_matrix);
      closeFileHandler__s_kt_matrix_t(&obj_matrix);
      
      //! Note: we 'this appraoch' we also export the weight-table:
      const char *stringOf_resultFile_weightTAble = "tmp/ex_matrixWithLabels_weightTable.tsv";
      export_1dWeightTable__formatOf_tsv__s_kt_matrix_t(&obj_matrix, stringOf_resultFile_weightTAble);
      closeFileHandler__s_kt_matrix_t(&obj_matrix);

      { //! Load the results and validate that we 'get what we expect':
	//!
	//! Standard 'calls' for data-loading: 
	s_kt_matrix_t obj_new; const bool is_ok = readFrom__file__s_kt_matrix_t(&obj_new, stringOf_resultFile);
	assert(is_ok); //! ie, as we expec thte operation was a sucesses.
	
	//! 
	//! Validate the weights:
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  t_float value = T_FLOAT_MAX; 	  get_weight__s_kt_matrix(&obj_matrix, col_id, &value);	  
	  //!
	  //! What we expect:
	  t_float value_cmp = T_FLOAT_MAX;
	  get_weight__s_kt_matrix(&obj_new, col_id, &value_cmp);
	  if(value_cmp != 0) {
	    assert(value_cmp == value);
	  }
	}


	//! 
	//! Validate the row-names:
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  //!
	  //! What we expect:
	  char *stringTo_add = NULL;
	  get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/row_id, /*addFor_column=*/false, &stringTo_add);
	  //! ---
	  char *stringOf_cmp = NULL;
	  get_string__s_kt_matrix(&obj_new, /*index_pos=*/row_id, /*addFor_column=*/false, &stringOf_cmp);
	  const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
	  if(is_equal == false) {
	    fprintf(stderr, "!!\t The comapred strings were different at row[%u] = \"%s\" VS \"%s\", at %s:%d\n", row_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
	  }
	  assert(is_equal);
	}

	//! 
	//! Validate the column-names:
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  //!
	  //! What we expect:
	  char *stringTo_add = NULL;
	  get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/col_id, /*addFor_column=*/true, &stringTo_add);
	  //! ---
	  char *stringOf_cmp = NULL;
	  get_string__s_kt_matrix(&obj_new, /*index_pos=*/col_id, /*addFor_column=*/true, &stringOf_cmp);
	  const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
	  if(is_equal == false) {
	    fprintf(stderr, "!!\t The comapred strings were different at col[%u] = \"%s\" VS \"%s\", at %s:%d\n", col_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
	  }
	  assert(is_equal);
	}


	//! 
	//! Validate that the inptu-valeus correpsodns to our exepctaiotns:
	assert(obj_new.ncols == obj_matrix.ncols);
	assert(obj_new.nrows == obj_matrix.nrows);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint col_id = 0; col_id < ncols; col_id++) {
	    //! Validate:
	    t_float result_value = T_FLOAT_MAX;
	    get_cell__s_kt_matrix(&obj_matrix, row_id, col_id, &result_value);
	    if(result_value != 0) {
	      //! Validate:
	      t_float result_value_2 = T_FLOAT_MAX;
	      get_cell__s_kt_matrix(&obj_new, row_id, col_id, &result_value_2);
	      if(result_value_2 != result_value) {
		fprintf(stderr, "!!\t Different-results-at[%u][%u]: %f VS %f, at %s:%d\n", row_id, col_id, result_value, result_value_2, __FILE__, __LINE__);
	      }
	      assert(result_value == result_value_2); //! ie, what we expect for cosnstency.
	    }
	  }
	}
	
	//!
	//! Apply a correlation-metric (uinsg our "kt_api" interface) to the input-matrix:
	s_kt_matrix_t obj_result; setTo_empty__s_kt_matrix_t(&obj_result);
	s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none);
	assert(false); // FIXME: consider to udpat ehte "config_corr" ... hwo do we Know if thye matrix is an 'ajdcnecy-matrix'?
	s_kt_api_config_t config__corr = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, NULL);
	const bool is_ok_corr = kt_matrix__distancematrix(obj_metric, /*matrix-1=*/&obj_new, /*matrix-2=*/&obj_new, /*result=*/&obj_result, config__corr); ///*isTo_transposeMatrix=*/false);
	assert(is_ok_corr); //! ie, as we expec thte operation was a sucesses.


	//!
	//! Re-export the results (and mnaully validate the results):
	const char *stringOf_resultFile_corrMatrix = "tmp/ex_corrMatrix.tsv";
	export_config__s_kt_matrix_t(&obj_result, stringOf_resultFile_corrMatrix);
	export__s_kt_matrix_t(&obj_result);


	//! ----------------------------------------------------------------
	//!
	//! De-allocate:
	free__s_kt_matrix(&obj_new);
	free__s_kt_matrix(&obj_result);
	// readFrom_1dWeightTable__s_kt_matrix_t(
      }
    }
    //! ----------------------------------------------------------------
    //!
    //! De-allocate:
    free__s_kt_matrix(&obj_matrix);
  }
}


//! Test the argument-parsing
static void parse_args() {
  { //! A simple case 'cotnaing' a number of differnet tyeps of arguments:
    const uint array_cnt = 6+1;
     char *stringOf_arg[array_cnt] =
      {
	"measure_terminal", //! ie, the first param which shoudl reflect the name of the software.
	//! -------------------------------------
	"-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1",
	"-isTo_transposeMatrix=1",
	"-before-filter-correlation-type-id=e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya",
	"-hca_method=single",
	"-result-file=sample_result.txt",
	"-ifTwoMatrix-whenToMerge=afterFilter"
      };
    
    bool mandaroryInputParams_are_set;
    s_kt_terminal_t self = parse_cmdTerminalInputAgs__kt_terminal(stringOf_arg, array_cnt, &mandaroryInputParams_are_set);
    {
      uint actual_value = self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[/*isFor_max=*/0];
      //printf("actual_value=%u, at %s:%d\n", actual_value, __FILE__, __LINE__);
      /* actual_value = self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[/\*isFor_max=*\/1]; */
      /* printf("actual_value=%u, at %s:%d\n", actual_value, __FILE__, __LINE__); */
      assert(actual_value == 1);

      assert(self.isTo_transposeMatrix == 1);
      assert(self.metric_beforeFilter.metric_id == e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya);
      assert(self.clusterConfig.hca_method == get_charConfigurationEnum_fromEnum__kt_clusterAlg_hca(e_kt_clusterAlg_hca_type_single));
      assert(0 == strcmp(self.stringOf_resultFile, "sample_result.txt"));
      assert(self.whenToMerge_differentMatrices == e_kt_terminal_whenToMerge_afterFilter);
    }
    assert(mandaroryInputParams_are_set == true); //! ie, as correlation-metric-spec is set.
    if(mandaroryInputParams_are_set) {
      free__s_kt_terminal_t(&self);
    }    
  }
}

//! Test the correctenss of the "kt_terminal__stub__parse__input.c", using/evlautidng difnferet cases.
static void parse_matrix() {

  { //! A third 'use-case' where we load two 'in-funciton-file-generated input-matrices', and validate that the 'rows and columns' are correctly set
    //! -------------------------------
    //!
    //! Initiate:
    const uint nrows = 3;     const uint ncols = 2;
    char *arrOf_rows_1[nrows] = {"Jesus", "Thomas", "Holy Spirit (aka: \"Holy Ghost\")"};
    char *arrOf_rows_2[nrows] = {"Sakarja", "Joel", "Job"};
    char *arrOf_cols[ncols] = {"close-to-dad", "preachers"};
    //! ---
    const char *name_sample_1 = "tmp/sample_1.tsv";
    const char *name_sample_2 = "tmp/sample_2.tsv";
    //! --
    {
      //! Build to sample-matrices:
      s_kt_matrix_t obj_matrix_1; init__s_kt_matrix(&obj_matrix_1, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
      s_kt_matrix_t obj_matrix_2; init__s_kt_matrix(&obj_matrix_2, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
      obj_matrix_1.nameOf_rows = arrOf_rows_1;       obj_matrix_1.nameOf_columns = arrOf_cols;
      obj_matrix_2.nameOf_rows = arrOf_rows_2;       obj_matrix_2.nameOf_columns = arrOf_cols;
      //! -----------------------------------------------------------------
      //!
      //! Set the cell-values:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  set_cell__s_kt_matrix(&obj_matrix_1, row_id, col_id, (t_float)row_id);
	  set_cell__s_kt_matrix(&obj_matrix_2, row_id, col_id, (t_float)col_id+1);
	}
      }
      //! -----------------------------------------------------------------
      //!
      {//! Write out the data-sets:
	{
	  const char *stringOf_resultFile = name_sample_1; 
	  //const char *stringOf_resultFile = "tmp/sample_1.tsv";
	  printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
	  //! First convifugre the result-process:
	  export_config__s_kt_matrix_t(&obj_matrix_1, stringOf_resultFile);
	  export__s_kt_matrix_t(&obj_matrix_1);
	  closeFileHandler__s_kt_matrix_t(&obj_matrix_1);
	}
	{
	  //const char *name_sample_2 = "tmp/sample_2.tsv";
	  const char *stringOf_resultFile = name_sample_2; 
	  printf("# Export: to the result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
	  //! First convifugre the result-process:
	  export_config__s_kt_matrix_t(&obj_matrix_2, stringOf_resultFile);
	  export__s_kt_matrix_t(&obj_matrix_2);
	  closeFileHandler__s_kt_matrix_t(&obj_matrix_2);
	}
      }
      //! ----------------------------------------------------------------
      //!
      //! De-allocate, first 'celaring' the staitcally allcoated data-sets:
      obj_matrix_1.nameOf_rows = NULL;       obj_matrix_1.nameOf_columns = NULL;
      obj_matrix_2.nameOf_rows = NULL;       obj_matrix_2.nameOf_columns = NULL;
      free__s_kt_matrix(&obj_matrix_1);
      free__s_kt_matrix(&obj_matrix_2);
    }



    const uint array_cnt = 13+1;
     char *stringOf_arg[array_cnt] =
      {
	"measure_terminal", //! ie, the first param which shoudl reflect the name of the software.
	//! -------------------------------------
	"-result-isTo-exportInputFile=1",
	"-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1",
	"-isTo_transposeMatrix=0",
	"-before-filter-correlation-type-id=e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya",
	"-before-filter-correlation-type-isTo-export=1", //! which we 'set' in order to 
	//! Note: [”elow] calls 'ensures' that the 'correlation-metric is exported even though the cluster-anlayssi is Not perfomred':
	"-after-filter-correlation-type-id=e_kt_correlationFunction_groupOf_minkowski_cityblock",
	"-after-filter-correlation-type-isTo-export=1", //! which we 'set' in order to 
	//! ---
	"-hca_method=single",
	"-result-file=sample_result.txt",
	"-ifTwoMatrix-whenToMerge=afterFilter",
	/* "-sample-data-distribution=uniform", */
	/* "-sample-data-distribution-secondMatrix=lines-sinsoid", */
	"-result-format=matrix",
	//! Spedify the inptu-file:
	"-input-file-1=tmp/sample_1.tsv",
	"-input-file-2=tmp/sample_2.tsv",
	/* "-cnt-rows=10", */
	/* "-cnt-cols=20", */
      };
    
    bool mandaroryInputParams_are_set;
    s_kt_terminal_t data_obj = parse_cmdTerminalInputAgs__kt_terminal(stringOf_arg, array_cnt, &mandaroryInputParams_are_set);
    assert(data_obj.isTo_transposeMatrix == 0);    
    /* assert(0 == strcmp("uniform", data_obj.stringOf_sampleData_type)); */
    /* assert(data_obj.stringOf_sampleData_type_secondMatrix != NULL); */
    /* assert(0 == strcmp("lines-sinsoid", data_obj.stringOf_sampleData_type_secondMatrix)); */
    assert(mandaroryInputParams_are_set == true); //! ie, as correlation-metric-spec and HCA-spec is set.
    //! Validate the result-specs:
    assert(data_obj.isTo_applyDataFilter == true); //! ie, what we expect given [above] "-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1"
    assert(data_obj.isToApply_correlation_beforeFilter_includeInResult == true);
    assert(data_obj.isTo_inResult_isTo_exportInputData == true); //! ie, given teh [ªbove] "result-isTo-exportInputFile" parameter.    
    //! Valdiate that the diemsions are correlctly set:
    assert(data_obj.stringOf_input_file_1);
    assert(data_obj.stringOf_input_file_2);
    assert(strlen(data_obj.stringOf_input_file_1));
    assert(strlen(data_obj.stringOf_input_file_2));
    assert(0 == strcmp(data_obj.stringOf_input_file_1, name_sample_1));
    assert(0 == strcmp(data_obj.stringOf_input_file_2, name_sample_2));
    //assert(data_obj.nrows == 10);     assert(data_obj.ncols == 20);


    // assert(data_obj.isToApply_correlation_beforeFilter == false);
    
    //! -------------------------------
    //!
    //! Initiate local varialbes for the "kt_terminal__stub__parse__input.c":
    s_kt_terminal_t *self = &data_obj;
    assert(self);        
    const bool needToMerge_results = (self->stringOf_input_file_2 != NULL);
    bool needToMerge_results_areMerged = (needToMerge_results == false);    
    uint cnt_dataObjects_toGenerate = 0; 
    bool isTo_useMatrixFormat = true;

    printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);

  //! -----------------------------------------------------
  //!
  //! Apply logics to parse both the arguments and the data:
#define __is_called_fromTestFunction
#include "kt_terminal__stub__parse__input.c"
#undef __is_called_fromTestFunction


    printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);
    
    


    //! -------------------------------
    //!
    //! Validate the results of [above]:
    assert(data_obj.inputMatrix_1.nrows == nrows);     assert(data_obj.inputMatrix_1.ncols == ncols);
    assert(data_obj.inputMatrix_2.nrows == nrows);     assert(data_obj.inputMatrix_2.ncols == ncols);
    assert(data_obj.inputMatrix_result.nrows == 0); assert(data_obj.inputMatrix_result.ncols == 0);    


    //! --------------------------
    //!
    //! We expect the first matrix to at least have oen cell with T_FLOAT_MAX: validate that our 'funciton'æ ifnds this case.
    data_obj.inputMatrix_1.matrix[0][0] = T_FLOAT_MAX;
    const bool use_masks = matrixMakesUSeOf_mask(nrows, ncols, data_obj.inputMatrix_1.matrix, NULL, NULL, NULL, /*transpose=*/false, /*iterationIndex_2=*/UINT_MAX);
    assert(use_masks == true);


    //! -------------------------------
    //!
    //! Validate the 'parsed names':
    { //! matrix-1:
      s_kt_matrix_t obj_matrix = data_obj.inputMatrix_1;       char **arrOf_rows = arrOf_rows_1; //!ie, what we compare with 
      //! Iterate:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	//!
	//! What we expect:
	char *stringTo_add = NULL;
	get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/row_id, /*addFor_column=*/false, &stringTo_add);
	//! ---
	char *stringOf_cmp = arrOf_rows[row_id];
	const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
	if(is_equal == false) {
	  fprintf(stderr, "!!\t The comapred strings were different at row[%u] = \"%s\" VS \"%s\", at %s:%d\n", row_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
	}
	assert(is_equal);
      }
      //!
      //! Then compare wrt. the columns:
      for(uint col_id = 0; col_id < ncols; col_id++) {
	//!
	//! What we expect:
	char *stringTo_add = NULL;
	get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/col_id, /*addFor_column=*/true, &stringTo_add);
	//! ---
	char *stringOf_cmp = arrOf_cols[col_id];
	const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
	if(is_equal == false) {
	  fprintf(stderr, "!!\t The comapred strings were different at col[%u] = \"%s\" VS \"%s\", at %s:%d\n", col_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
	}
	assert(is_equal);
      }
    }
    //! ----
    { //! matrix-2:
      s_kt_matrix_t obj_matrix = data_obj.inputMatrix_2;       char **arrOf_rows = arrOf_rows_2; //!ie, what we compare with 
      //! Iterate:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	//!
	//! What we expect:
	char *stringTo_add = NULL;
	get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/row_id, /*addFor_column=*/false, &stringTo_add);
	//! ---
	char *stringOf_cmp = arrOf_rows[row_id];
	const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
	if(is_equal == false) {
	  fprintf(stderr, "!!\t The comapred strings were different at row[%u] = \"%s\" VS \"%s\", at %s:%d\n", row_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
	}
	assert(is_equal);
      }
      //!
      //! Then compare wrt. the columns:
      for(uint col_id = 0; col_id < ncols; col_id++) {
	//!
	//! What we expect:
	char *stringTo_add = NULL;
	get_string__s_kt_matrix(&obj_matrix, /*index_pos=*/col_id, /*addFor_column=*/true, &stringTo_add);
	//! ---
	char *stringOf_cmp = arrOf_cols[col_id];
	const int is_equal = (0 == strcmp(stringTo_add, stringOf_cmp));
	if(is_equal == false) {
	  fprintf(stderr, "!!\t The comapred strings were different at col[%u] = \"%s\" VS \"%s\", at %s:%d\n", col_id, stringTo_add, stringOf_cmp, __FILE__, __LINE__);
	}
	assert(is_equal);
      }
    }

    printf("cnt_dataObjects_toGenerate=%u isTo_useMatrixFormat=%u, at %s:%d\n", cnt_dataObjects_toGenerate, isTo_useMatrixFormat, __FILE__, __LINE__);

    printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);
    
    //! -------------------------------
    //!
    //! Then a simple correlation-test: combine matrix-fitlers and correlation-analsyssi for the data-set(s):
#define __is_called_fromTestFunction
#include "kt_terminal__stub__parse__correlationAndFilter.c"
#undef __is_called_fromTestFunction

    //! -------------------------------
    //!
    //! Generate result-sets for both Java-script (JS) and JSON:
    { //! A seperate/overehad-call to manully validate that the exprot wrosk for: java-script (JS):
      data_obj.stringOf_resultFile = "resultFile_js";
      data_obj.isTo_inResult_isTo_useJavaScript_syntax = true;
      data_obj.isTo_inResult_isTo_useJSON_syntax = false;
      const char *label_1 = "js_result";       
      const bool is_ok = exportDataSets__toACombinedFile__kt_terminal(self, label_1, /*isTo_useMatrixFormat=*/true);      
      assert(is_ok);
    }
    { //! A seperate/overehad-call to manully validate that the exprot wrosk for: JSON:
      data_obj.stringOf_resultFile = "resultFile_json";
      data_obj.isTo_inResult_isTo_useJavaScript_syntax = false;
      data_obj.isTo_inResult_isTo_useJSON_syntax = true;
      const char *label_1 = "json_result";       
      const bool is_ok = exportDataSets__toACombinedFile__kt_terminal(self, label_1, /*isTo_useMatrixFormat=*/true);
      assert(is_ok);
    }

    //! -------------------------------
    //!
    //!
    free__s_kt_terminal_t(&data_obj);
  }
  //! *********************************************************
  //! *********************************************************
  //! *********************************************************
  

  { //! A second 'case' where we evlauate/compare two input-matrices
    //! -------------------------------
    //!
    //! Initiate:
    const uint array_cnt = 13+1;
     char *stringOf_arg[array_cnt] =
      {
	"measure_terminal", //! ie, the first param which shoudl reflect the name of the software.
	//! -------------------------------------
	"-result-isTo-exportInputFile=1",
	"-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1",
	"-isTo_transposeMatrix=0",
	"-before-filter-correlation-type-id=e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya",
	"-before-filter-correlation-type-isTo-export=1", //! which we 'set' in order to 
	"-hca_method=single",
	"-result-file=sample_result.txt",
	"-ifTwoMatrix-whenToMerge=afterFilter",
	"-sample-data-distribution=uniform",
	"-sample-data-distribution-secondMatrix=lines-sinsoid",
	"-result-format=matrix",
	//! Spedify the dimesions of the sample-data-sets:
	"-cnt-rows=10",
	"-cnt-cols=20",
      };
    
    bool mandaroryInputParams_are_set;
    s_kt_terminal_t data_obj = parse_cmdTerminalInputAgs__kt_terminal(stringOf_arg, array_cnt, &mandaroryInputParams_are_set);
    assert(data_obj.isTo_transposeMatrix == 0);    
    assert(0 == strcmp("uniform", data_obj.stringOf_sampleData_type));
    assert(data_obj.stringOf_sampleData_type_secondMatrix != NULL);
    assert(0 == strcmp("lines-sinsoid", data_obj.stringOf_sampleData_type_secondMatrix));
    assert(mandaroryInputParams_are_set == true); //! ie, as correlation-metric-spec and HCA-spec is set.
    //! Validate the result-specs:
    assert(data_obj.isTo_applyDataFilter == true); //! ie, what we expect given [above] "-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1"
    assert(data_obj.isToApply_correlation_beforeFilter_includeInResult == true);
    assert(data_obj.isTo_inResult_isTo_exportInputData == true); //! ie, given teh [ªbove] "result-isTo-exportInputFile" parameter.    
    //! Valdiate that the diemsions are correlctly set:
    assert(data_obj.nrows == 10);     assert(data_obj.ncols == 20);



    // assert(data_obj.isToApply_correlation_beforeFilter == false);
    
    //! -------------------------------
    //!
    //! Initiate local varialbes for the "kt_terminal__stub__parse__input.c":
    s_kt_terminal_t *self = &data_obj;
    assert(self);        
    const bool needToMerge_results = (self->stringOf_input_file_2 != NULL);
    bool needToMerge_results_areMerged = (needToMerge_results == false);    
    uint cnt_dataObjects_toGenerate = 0; 
    bool isTo_useMatrixFormat = true;

    printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);

  //! -----------------------------------------------------
  //!
  //! Apply logics to parse both the arguments and the data:
#define __is_called_fromTestFunction
#include "kt_terminal__stub__parse__input.c"
#undef __is_called_fromTestFunction


  printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);


    //! -------------------------------
    //!
    //! Validate the results of [above]:
    assert(data_obj.inputMatrix_1.nrows == 10);     assert(data_obj.inputMatrix_1.ncols == 20);
    assert(data_obj.inputMatrix_2.nrows == 10);     assert(data_obj.inputMatrix_2.ncols == 20);
    assert(data_obj.inputMatrix_result.nrows == 0); assert(data_obj.inputMatrix_result.ncols == 0);    


    /* assert(false); // FIXME: update [ªbove] with 'explcit' sizes of the rnows and cnols. */
    /* assert(false); // FIXME: vlaidte taht 'all the input-amtrices are gnerated wrt. the resutls'. */

  /*   printf("cnt_dataObjects_toGenerate=%u isTo_useMatrixFormat=%u, at %s:%d\n", cnt_dataObjects_toGenerate, isTo_useMatrixFormat, __FILE__, __LINE__); */

  /* printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__); */

    //! -------------------------------
    //!
    //! Then a simple correlation-test: combine matrix-fitlers and correlation-analsyssi for the data-set(s):
#define __is_called_fromTestFunction
#include "kt_terminal__stub__parse__correlationAndFilter.c"
#undef __is_called_fromTestFunction

    //! -------------------------------
    //!
    //!
    free__s_kt_terminal_t(&data_obj);
  }
  //! *********************************************************
  //! *********************************************************
  //! *********************************************************
  


  
  {
    //! -------------------------------
    //!
    //! Initiate:
    const uint array_cnt = 10+1;
     char *stringOf_arg[array_cnt] =
      {
	"measure_terminal", //! ie, the first param which shoudl reflect the name of the software.
	//! -------------------------------------
	"-result-isTo-exportInputFile=1",
	"-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1",
	"-isTo_transposeMatrix=0",
	"-before-filter-correlation-type-id=e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya",
	"-before-filter-correlation-type-isTo-export=1", //! which we 'set' in order to 
	"-hca_method=single",
	"-result-file=sample_result.txt",
	"-ifTwoMatrix-whenToMerge=afterFilter",
	"-sample-data-distribution=uniform",
	"-result-format=matrix",
      };
    
    bool mandaroryInputParams_are_set;
    s_kt_terminal_t data_obj = parse_cmdTerminalInputAgs__kt_terminal(stringOf_arg, array_cnt, &mandaroryInputParams_are_set);
    assert(data_obj.isTo_transposeMatrix == 0);    
    assert(0 == strcmp("uniform", data_obj.stringOf_sampleData_type));
    assert(mandaroryInputParams_are_set == true); //! ie, as correlation-metric-spec and HCA-spec is set.

    assert(data_obj.isTo_applyDataFilter == true); //! ie, what we expect given [above] "-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=1"
    assert(data_obj.isToApply_correlation_beforeFilter_includeInResult == true);
    assert(data_obj.isTo_inResult_isTo_exportInputData == true); //! ie, given teh [ªbove] "result-isTo-exportInputFile" parameter.

    // assert(data_obj.isToApply_correlation_beforeFilter == false);
    
    //! -------------------------------
    //!
    //! Initiate local varialbes for the "kt_terminal__stub__parse__input.c":
    s_kt_terminal_t *self = &data_obj;
    assert(self);        
    const bool needToMerge_results = (self->stringOf_input_file_2 != NULL);
    bool needToMerge_results_areMerged = (needToMerge_results == false);    
    uint cnt_dataObjects_toGenerate = 0; 
    bool isTo_useMatrixFormat = true;

  //! -----------------------------------------------------
  //!
  //! Apply logics to parse both the arguments and the data:
#define __is_called_fromTestFunction
#include "kt_terminal__stub__parse__input.c"
#undef __is_called_fromTestFunction

    // printf("cnt_dataObjects_toGenerate=%u isTo_useMatrixFormat=%u, at %s:%d\n", cnt_dataObjects_toGenerate, isTo_useMatrixFormat, __FILE__, __LINE__);


    //! -------------------------------
    //!
    //! Validate the results of [above]:
    assert(data_obj.inputMatrix_1.nrows != 0); assert(data_obj.inputMatrix_1.ncols != 0);
    assert(data_obj.inputMatrix_2.nrows == 0); assert(data_obj.inputMatrix_2.ncols == 0);
    assert(data_obj.inputMatrix_result.nrows == 0); assert(data_obj.inputMatrix_result.ncols == 0);    
    //assert(
    



    //! -------------------------------
    //!
    //! Then a simple correlation-test: combine matrix-fitlers and correlation-analsyssi for the data-set(s):
#define __is_called_fromTestFunction
#include "kt_terminal__stub__parse__correlationAndFilter.c"
#undef __is_called_fromTestFunction

    //! -------------------------------
    //!
    //!
    free__s_kt_terminal_t(&data_obj);
  }
  //! *********************************************************
  //! *********************************************************
  //! ********************************************************* 
}


//! Test the correctness of our cluster-export-results:
static void export_cluster_results() {
  { //! Write a set of sytneitc data-result-test-cseses:

    {
      //! ----------------------------------
      //!
      //! Initatie our object:
      const uint nrows = 10; const uint ncols = nrows;
      s_kt_matrix_t matrix_local; init__s_kt_matrix(&matrix_local, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  const t_float score = col_id;
	  set_cell__s_kt_matrix(&matrix_local, row_id, col_id, score);
	}
      }
      s_kt_matrix_t *matrix = &matrix_local;
      s_kt_clusterResult_data_t obj_export;   init_exportForAllFormats_s_kt_clusterResult_data(&obj_export, nrows);
      { //! Specify data for the different data-sets:
	assert(obj_export.map_1dOf_vertexTypes_partOfCluster__som__centroidId);
	assert(obj_export.map_1dOf_vertexTypes_partOfCluster__som_x);
	assert(obj_export.map_1dOf_vertexTypes_partOfCluster__som_y);
	assert(obj_export.map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId);
	assert(obj_export.map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount);
	assert(obj_export.map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  obj_export.map_1dOf_vertexTypes_partOfCluster__som__centroidId[row_id] = (t_float)row_id;
	  obj_export.map_1dOf_vertexTypes_partOfCluster__som_x[row_id] = (t_float)row_id;
	  obj_export.map_1dOf_vertexTypes_partOfCluster__som_y[row_id] = (t_float)row_id;
	  obj_export.map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId[row_id] = (t_float)row_id;
	  obj_export.map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount [row_id] = (t_float)row_id;
	  obj_export.map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum [row_id] = (t_float)row_id;

	  //! Then intaite the matrices to valeus:
	  for(uint col_id = 0; col_id < ncols; col_id++) {
	    assert(obj_export.distancesIn_2d_correlationMatrix);
	    assert(obj_export.distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix);
	    assert(obj_export.distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid);
	    assert(obj_export.distancesIn_2d_clusterFrom__hcaToplogy__all_topology);
	    assert(obj_export.distancesIn_2d_pca_appliedToCorrelationMatrix);
	    //! Update:
	    obj_export.distancesIn_2d_correlationMatrix[row_id][col_id] = (t_float)row_id;
	    obj_export.distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix[row_id][col_id] = (t_float)row_id;
	    obj_export.distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid[row_id][col_id] = (t_float)row_id;
	    obj_export.distancesIn_2d_clusterFrom__hcaToplogy__all_topology[row_id][col_id] = (t_float)row_id;
	    obj_export.distancesIn_2d_pca_appliedToCorrelationMatrix[row_id][col_id] = (t_float)row_id;
	  }
	}	

      }

      //! ------------------------------
      //!
      //! Then export the result:
      { //! Ntoe: we 'here force the case where the software needs to use the internla idnexes when writing out the result':
	const e_kt_terminal_clusterResult__format_t resultFormat = e_kt_terminal_clusterResult__format_JSON;
	const char *resultPrefix = "clusterExport_xmtNames_json";
	const bool is_ok = writeOut__s_kt_clusterResult_data_t(&obj_export, matrix, resultFormat, resultPrefix, NULL);
	assert(is_ok);
      }
      //! ----------------------- 
      //!
      //! Explictly set the different row-names:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	char stringTo_add[1000] = {'\0'};
	sprintf(stringTo_add, "Salme-%u", row_id);
	set_string__s_kt_matrix(matrix, /*index_pos=*/row_id, stringTo_add, /*addFor_column=*/false);
      }
      for(uint col_id = 0; col_id < ncols; col_id++) {
	char stringTo_add[1000] = {'\0'};
	sprintf(stringTo_add, "Jesu-goodness-example-%u", col_id);
	set_string__s_kt_matrix(matrix, /*index_pos=*/col_id, stringTo_add, /*addFor_column=*/true);
      }

      //! ----------------------- 
      //!
      //! Write out 'for the otehr foramts.
      {
	const e_kt_terminal_clusterResult__format_t resultFormat = e_kt_terminal_clusterResult__format_JSON;
	const char *resultPrefix = "clusterExport_json";
	const bool is_ok = writeOut__s_kt_clusterResult_data_t(&obj_export, matrix, resultFormat, resultPrefix, NULL);
	assert(is_ok);
      }
      {
	const e_kt_terminal_clusterResult__format_t resultFormat = e_kt_terminal_clusterResult__format_javaScript;
	const char *resultPrefix = "clusterExport_javaScript";
	const bool is_ok = writeOut__s_kt_clusterResult_data_t(&obj_export, matrix, resultFormat, resultPrefix, NULL);
	assert(is_ok);
      }
      {
	const e_kt_terminal_clusterResult__format_t resultFormat = e_kt_terminal_clusterResult__format_tsv_matrix;
	const char *resultPrefix = "clusterExport_tsv_matrix";
	const bool is_ok = writeOut__s_kt_clusterResult_data_t(&obj_export, matrix, resultFormat, resultPrefix, NULL);
	assert(is_ok);
      }
      { //! Export the result to the stream
	const e_kt_terminal_clusterResult__format_t resultFormat = e_kt_terminal_clusterResult__format_JSON;
	const char *resultPrefix = NULL; 
	const bool is_ok = writeOut__s_kt_clusterResult_data_t(&obj_export, matrix, resultFormat, resultPrefix, stdout);
	assert(is_ok);
      }
      //!
      //! De-allcoat ethe internal objects:
      free__s_kt_clusterResult_data(&obj_export); //! ie, de-allcoate the exprot-object, as we ssume the 'result-object is no longer needed'.
      free__s_kt_matrix(&matrix_local);
    }
  }


  //! ***************************************************************************
  //!           Cluster results and write out the result: no-disjoint
  //! ***************************************************************************
  { 
    { //! Test the 'case' where: we 'provide correlation-matrix as-is':
      //! ----------------------------------
      //!
      //! Initatie our object:
      const uint nrows = 10; const uint ncols = nrows;
      s_kt_matrix_t matrix_local; init__s_kt_matrix(&matrix_local, nrows, ncols, /*isTo_allocateWeightColumns=*/false);      
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  const t_float score = col_id;
	  set_cell__s_kt_matrix(&matrix_local, row_id, col_id, score);
	}
      }
      //! ------------------------------------
      //!
      //! Configure the clsutering-objects:


      assert(false); // FIXME: validate correctness of [ªbove].

      //! ------------------------------------
      //!
      //! Appply the clsutering:

      assert(false); // FIXME: validate correctness of [ªbove].

      //! ------------------------------------
      //!
      //! Write otu the results:


      assert(false); // FIXME: validate correctness of [ªbove].

      assert(false); // FIXME: add internal tets.
      assert(false); // FIXME: add something

      assert(false); // FIXME: coosnider to test for tidsjoitness after applciaotni fo [ªbove] corrleaiotn-matrix ... eg, wrt. 'no-correlation' cases (wehre valeus are set to "1" for non-MINE metrics) 

      //!
      //! De-allcoat ethe internal objects:
      // free__s_kt_clusterResult_data(&obj_export); //! ie, de-allcoate the exprot-object, as we ssume the 'result-object is no longer needed'.
      free__s_kt_matrix(&matrix_local);
    }
    { //! Test the 'case' where: we use the "kt_terminal" API:

      assert(false); // FIXME: add something


      //! ------------------------------------
      //!
      //! Configure the clsutering-objects:


      assert(false); // FIXME: validate correctness of [ªbove].

      //! ------------------------------------
      //!
      //! Appply the clsutering:

      assert(false); // FIXME: validate correctness of [ªbove].

      //! ------------------------------------
      //!
      //! Write otu the results:


      assert(false); // FIXME: validate correctness of [ªbove].

    }
    //! --------------------------------------------------------------
  }




  // --------------------------------------------------
  assert(false); // FIXME[disjoint]: ... update out "kt_matrix" to support/faicliate the inclsuion/integration of 'different mappigns for rows and columns' ... and udpate our caller-rotuenisn wrt. the latter: code udpated and compiles;  <-- how do we update the "mapOf_globalTo_local" in our "kt_matrix__stub__buildSubset.c" ... ie, woudl it be correct to assume that 'for the latter case' we only update "mapOf_globalTo_local" for the 'row' case (ie, as we asusem only the 'rows' are used in the result-table, while ie, as the columsn are expected to describe/present the feautres)? <-- valdiate thsio assumption and therafter update our "kt_matrix__stub__buildSubset.c"
  // --------------------------------------------------
  assert(false); // FIXME: copmtue disjoint-forest and then iteraitly 'iterate through this' ... coneuqaitvly applying the clsutering-algorithms <-- tehreafter 'scaffold' a gneirc stratweg wrt updating our "kt_api" 'for such'. <-- udpat eour aritcle-scaccold
  assert(false); // FIXME[disjoint]: ... for "nrows !=ncols" do we need to to call the "get_datasetIdOf_vertex__s_kt_forest_findDisjoint_t(...)" function (eg, when 'fethcinig' the name-mappings of a given vertex)?

  assert(false); // FIXME[disjoint]: ... 
  assert(false); // FIXME[disjoint]: ... 
  assert(false); // FIXME: update our "kt_disjoint__stub__applyLogics_insdieEach.c" to facialtie/support ... ncols != nrows ... where the 'mapping-tables' are udpated wrt. .....??.... 
  assert(false); // FIXME: update our "kt_disjoint__stub__applyLogics_insdieEach.c" to facialtie/support ... the different 'diosjoint-combination-cases'
  assert(false); // FIXME: 'genralise' [ªbove] ... 'moving this' intop a seprate file "kt_disjoint__stub__applyLogics_insdieEach.c" (using macros) ... and then 'use this' wrt. updaitng our "kt_api.c" to support 'disjotin set-comptautions'.
  assert(false); // FIXME: update our "kt_disjoint__stub__applyLogics_insdieEach.c" to facialtie/support ... 'removal' of itnernalction/cells between 'a disjoint region adn the otehr regions' ... and tehn update our "ex_kMeans.c" ... an appraoch used to evlauate the 'implicaiton' wrt. disjotindata-fitlering-through-rank-trhesholds ... where we assuem teh input-macrsi has diemsions=[n, n]
  // --------------------------------------------------
  // --------------------------------------------------
  assert(false); // FIXME: ... update out tmerina-api-call: add differnet 'lvels' of help-message ... where the 'first' only 'lists' the 'input' option (and an example using 'this option') .... where we 'provice' a new option named '--help' to list 'the compelte set of options' <-- consider updating our temrinal-bash-synteax to 'conform' to "mcl -h"
  // --------------------------------------------------
  


  //! ***************************************************************************
  //!           Cluster results and write out the result: disjoint
  //! ***************************************************************************
  { 
    { //! Test the 'case' where: we 'provide correlation-matrix as-is':

      assert(false); // FIXME: complete teh implememtnaiton.
      assert(false); // FIXME: add something

    }
    { //! Test the 'case' where: we use the "kt_terminal" API:

      assert(false); // FIXME: add something

    }
    //! --------------------------------------------------------------
  }

  assert(false); // FIXME: for each 'cluster-specific result strucutre/object' ... 

  assert(false); // FIXME: enusre that the JSON-resutls which we exprot are compatilbe wrt. our exeictiosn in our "g_tree_ds.js" ... eg, wrt. the exmple-set in our "init_radial.js"
  assert(false); // FIXME: ensure that our 'updated appraoch' is integrated into the "kt_terminal.c"
  assert(false);

  
    
}

//! the main funciton for logic-texting
void measure_terminal_main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  
  //printf("cnt-args=%u, at %s:%d\n", array_cnt, __FILE__, __LINE__);

  if(isTo_processAll || (0 == strncmp("export-matrix", array[2], strlen("export-matrix"))) ) {
    export_matrix();
    cnt_Tests_evalauted++;
  }


  if(isTo_processAll || (0 == strncmp("parse-args", array[2], strlen("parse-args"))) ) {
    parse_args();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("parse-matrix", array[2], strlen("parse-matrix"))) ) {
    parse_matrix();
    cnt_Tests_evalauted++;
  }

 
  if(isTo_processAll || (0 == strncmp("apply-clustering--kt_api", array[2], strlen("apply-clustering--kt_api"))) ) {
    //! Test the correctness of our cluster-export-results:
    export_cluster_results();    
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("apply-clustering--kt_api--disjoint", array[2], strlen("apply-clustering--kt_api--disjoint"))) ) {

    assert(false); // FIXME: write a measure-disjotin-forest-case to 'handle' the differnet disjotint-forest-cases
    assert(false); // FIXME: update the following algorithm/implementiaont with 'dsijoint-froest-uspport', and then 'validate seprately the result for: ... many:many corr-metric
    assert(false); // FIXME: update the following algorithm/implementiaont with 'dsijoint-froest-uspport', and then 'validate seprately the result for: k-means
    assert(false); // FIXME: update the following algorithm/implementiaont with 'dsijoint-froest-uspport', and then 'validate seprately the result for: SOM
    assert(false); // FIXME: update the following algorithm/implementiaont with 'dsijoint-froest-uspport', and then 'validate seprately the result for: HCA
    assert(false); // FIXME: scaffold a procedure ... where we 'call the main-file' for each 'disjoitn set' ... and then 'merge in/after the compelte set has been parsed' ... updating a 'global result-object'.
    assert(false); // FIXME: write code (and validate the results) wrt. 'use' of a 'disjoint wrapper' in consturction/evlauation of data-sets.

    cnt_Tests_evalauted++;
  }


  if(isTo_processAll || (0 == strncmp("apply-clustering--kt_api--disjoint--para", array[2], strlen("apply-clustering--kt_api--disjoint--para"))) ) {

    
    assert(false); // FIXME: write code (and validate the results) wrt. 'parallel wrappers' ... sperately for many:many correlation-metric-comptautions
    assert(false); // FIXME: write code (and validate the results) wrt. 'parallel wrappers' ... into 'chunks' based on disjoitn-forests
    assert(false); // FIXME: write code (and validate the results) wrt. 'parallel wrappers' ... 'splitting' SOM-evauation and k-means clsuteirng 'into seperate thredads

    cnt_Tests_evalauted++;
  }



  if(isTo_processAll || (0 == strncmp("apply-clustering", array[2], strlen("apply-clustering"))) ) {

    assert(false); // FIXME: seperately call our "kt_terminal" for the diffenret exampel-cases (listed when a suer does nto specify/incldue any termianl-inptu-args) ... and cosnider adding enw/otehr test-cases
    assert(false);

    cnt_Tests_evalauted++;
  }


  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
