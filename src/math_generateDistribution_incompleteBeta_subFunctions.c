
#if(configure_performance__alglibCmp__useMacrosinsteadOfFunctions == 1)
#define ae_assert(cond, msg) ({ ;}) //! ie, then a 'dummy call'.
#else
/************************************************************************
Assertion

For  non-NULL  state  it  allows  to  gracefully  leave  ALGLIB  session,
removing all frames and deallocating registered dynamic data structure.

For NULL state it just abort()'s program.
************************************************************************/
void ae_assert(ae_bool cond, const char *msg)
{
  if( !cond ) {
    fprintf(stderr, "!!\t result not as expected: \"%s\", at [%s]:%s:%d\n", msg, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
  }
}
#endif

t_float gammafunction(t_float x);
t_float lngamma(t_float x, t_float* sgngam);
t_float errorfunction(t_float x);
t_float errorfunctionc(t_float x);
t_float normaldistribution(t_float x);
t_float inverf(t_float e);
t_float invnormaldistribution(t_float y0);
t_float incompletegamma(t_float a, t_float x);
t_float incompletegammac(t_float a, t_float x);
t_float invincompletegammac(t_float a, t_float y0);
void airy(t_float x,
     t_float* ai,
     t_float* aip,
     t_float* bi,
     t_float* bip);
t_float besselj0(t_float x);
t_float besselj1(t_float x);
t_float besseljn(ae_int_t n, t_float x);
t_float bessely0(t_float x);
t_float bessely1(t_float x);
t_float besselyn(ae_int_t n, t_float x);
t_float besseli0(t_float x);
t_float besseli1(t_float x);
t_float besselk0(t_float x);
t_float besselk1(t_float x);
t_float besselkn(ae_int_t nn, t_float x);
t_float beta(t_float a, t_float b);
t_float incompletebeta(t_float a, t_float b, t_float x);
t_float invincompletebeta(t_float a, t_float b, t_float y);
t_float binomialdistribution(ae_int_t k,
     ae_int_t n,
     t_float p);
t_float binomialcdistribution(ae_int_t k,
     ae_int_t n,
     t_float p);
t_float invbinomialdistribution(ae_int_t k,
     ae_int_t n,
     t_float y);
t_float chebyshevcalculate(ae_int_t r,
     ae_int_t n,
     t_float x);
/* t_float chebyshevsum(/\* Real    *\/ ae_vector* c, */
/*      ae_int_t r, */
/*      ae_int_t n, */
/*      t_float x); */
/* void chebyshevcoefficients(ae_int_t n, */
/*      /\* Real    *\/ ae_vector* c); */
/* void fromchebyshev(/\* Real    *\/ ae_vector* a, */
/*      ae_int_t n, */
/*      /\* Real    *\/ ae_vector* b); */
t_float chisquaredistribution(t_float v, t_float x);
t_float chisquarecdistribution(t_float v, t_float x);
t_float invchisquaredistribution(t_float v, t_float y);
t_float dawsonintegral(t_float x);
t_float ellipticintegralk(t_float m);
t_float ellipticintegralkhighprecision(t_float m1);
t_float incompleteellipticintegralk(t_float phi, t_float m);
t_float ellipticintegrale(t_float m);
t_float incompleteellipticintegrale(t_float phi, t_float m);
t_float exponentialintegralei(t_float x);
t_float exponentialintegralen(t_float x, ae_int_t n);
t_float fdistribution(ae_int_t a, ae_int_t b, t_float x);
t_float fcdistribution(ae_int_t a, ae_int_t b, t_float x);
t_float invfdistribution(ae_int_t a,
     ae_int_t b,
     t_float y);
void fresnelintegral(t_float x, t_float* c, t_float* s);
t_float hermitecalculate(ae_int_t n, t_float x);
/* t_float hermitesum(/\* Real    *\/ ae_vector* c, */
/*      ae_int_t n, */
/*      t_float x); */
/* void hermitecoefficients(ae_int_t n, */
/*      /\* Real    *\/ ae_vector* c); */
void jacobianellipticfunctions(t_float u,
     t_float m,
     t_float* sn,
     t_float* cn,
     t_float* dn,
     t_float* ph);
t_float laguerrecalculate(ae_int_t n, t_float x);
/* t_float laguerresum(/\* Real    *\/ ae_vector* c, */
/*      ae_int_t n, */
/*      t_float x); */
/* void laguerrecoefficients(ae_int_t n, */
/*      /\* Real    *\/ ae_vector* c); */
/* t_float legendrecalculate(ae_int_t n, t_float x); */
/* t_float legendresum(/\* Real    *\/ ae_vector* c, */
/*      ae_int_t n, */
/*      t_float x); */
/* void legendrecoefficients(ae_int_t n, */
/*      /\* Real    *\/ ae_vector* c); */
t_float poissondistribution(ae_int_t k, t_float m);
t_float poissoncdistribution(ae_int_t k, t_float m);
t_float invpoissondistribution(ae_int_t k, t_float y);
t_float psi(t_float x);
//t_float studenttdistribution(ae_int_t k, t_float t);
//t_float invstudenttdistribution(ae_int_t k, t_float p);
void sinecosineintegrals(t_float x,
     t_float* si,
     t_float* ci);
void hyperbolicsinecosineintegrals(t_float x,
     t_float* shi,
     t_float* chi);

/* t_float incompletegammac(t_float a, t_float x); */
/* t_float lngamma(t_float x, t_float* sgngam); */


/*************************************************************************
Incomplete gamma integral

The function is defined by

                          x
                           -
                  1       | |  -t  a-1
 igam(a,x)  =   -----     |   e   t   dt.
                 -      | |
                | (a)    -
                          0


In this implementation both arguments must be positive.
The integral is evaluated by either a power series or
continued fraction expansion, depending on the relative
values of a and x.

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE      0,30       200000       3.6e-14     2.9e-15
   IEEE      0,100      300000       9.9e-14     1.5e-14

Cephes Math Library Release 2.8:  June, 2000
Copyright 1985, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float incompletegamma(t_float a, t_float x)
{
    t_float igammaepsilon;
    t_float ans;
    t_float ax;
    t_float c;
    t_float r;
    t_float tmp;
    t_float result;


    igammaepsilon = 0.000000000000001;
    if( ae_fp_less_eq(x,(t_float)(0))||ae_fp_less_eq(a,(t_float)(0)) )
    {
        result = (t_float)(0);
        return result;
    }
    if( ae_fp_greater(x,(t_float)(1))&&ae_fp_greater(x,a) )
    {
        result = 1-incompletegammac(a, x);
        return result;
    }
    ax = a*ae_log(x)-x-lngamma(a, &tmp);
    if( ae_fp_less(ax,-709.78271289338399) )
    {
        result = (t_float)(0);
        return result;
    }
    ax = ae_exp(ax);
    r = a;
    c = (t_float)(1);
    ans = (t_float)(1);
    do
    {
        r = r+1;
        c = c*x/r;
        ans = ans+c;
    }
    while(ae_fp_greater(c/ans,igammaepsilon));
    result = ans*ax/a;
    return result;
}


/*************************************************************************
Complemented incomplete gamma integral

The function is defined by


 igamc(a,x)   =   1 - igam(a,x)

                           inf.
                             -
                    1       | |  -t  a-1
              =   -----     |   e   t   dt.
                   -      | |
                  | (a)    -
                            x


In this implementation both arguments must be positive.
The integral is evaluated by either a power series or
continued fraction expansion, depending on the relative
values of a and x.

ACCURACY:

Tested at random a, x.
               a         x                      Relative error:
arithmetic   domain   domain     # trials      peak         rms
   IEEE     0.5,100   0,100      200000       1.9e-14     1.7e-15
   IEEE     0.01,0.5  0,100      200000       1.4e-13     1.6e-15

Cephes Math Library Release 2.8:  June, 2000
Copyright 1985, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float incompletegammac(t_float a, t_float x)
{
    t_float igammaepsilon;
    t_float igammabignumber;
    t_float igammabignumberinv;
    t_float ans;
    t_float ax;
    t_float c;
    t_float yc;
    t_float r;
    t_float t;
    t_float y;
    t_float z;
    t_float pk;
    t_float pkm1;
    t_float pkm2;
    t_float qk;
    t_float qkm1;
    t_float qkm2;
    t_float tmp;
    t_float result;


    igammaepsilon = 0.000000000000001;
    igammabignumber = 4503599627370496.0;
    igammabignumberinv = 2.22044604925031308085*0.0000000000000001;
    if( ae_fp_less_eq(x,(t_float)(0))||ae_fp_less_eq(a,(t_float)(0)) )
    {
        result = (t_float)(1);
        return result;
    }
    if( ae_fp_less(x,(t_float)(1))||ae_fp_less(x,a) )
    {
        result = 1-incompletegamma(a, x);
        return result;
    }
    ax = a*ae_log(x)-x-lngamma(a, &tmp);
    if( ae_fp_less(ax,-709.78271289338399) )
    {
        result = (t_float)(0);
        return result;
    }
    ax = ae_exp(ax);
    y = 1-a;
    z = x+y+1;
    c = (t_float)(0);
    pkm2 = (t_float)(1);
    qkm2 = x;
    pkm1 = x+1;
    qkm1 = z*x;
    ans = pkm1/qkm1;
    do
    {
        c = c+1;
        y = y+1;
        z = z+2;
        yc = y*c;
        pk = pkm1*z-pkm2*yc;
        qk = qkm1*z-qkm2*yc;
        if( (qk != (t_float)(0)) )
        {
            r = pk/qk;
            t = mathLib_float_abs((ans-r)/r);
            ans = r;
        }
        else
        {
            t = (t_float)(1);
        }
        pkm2 = pkm1;
        pkm1 = pk;
        qkm2 = qkm1;
        qkm1 = qk;
        if( ae_fp_greater(mathLib_float_abs(pk),igammabignumber) )
        {
            pkm2 = pkm2*igammabignumberinv;
            pkm1 = pkm1*igammabignumberinv;
            qkm2 = qkm2*igammabignumberinv;
            qkm1 = qkm1*igammabignumberinv;
        }
    }
    while(ae_fp_greater(t,igammaepsilon));
    result = ans*ax;
    return result;
}


/*************************************************************************
Inverse of complemented imcomplete gamma integral

Given p, the function finds x such that

 igamc( a, x ) = p.

Starting with the approximate value

        3
 x = a t

 where

 t = 1 - d - ndtri(p) sqrt(d)

and

 d = 1/9a,

the routine performs up to 10 Newton iterations to find the
root of igamc(a,x) - p = 0.

ACCURACY:

Tested at random a, p in the intervals indicated.

               a        p                      Relative error:
arithmetic   domain   domain     # trials      peak         rms
   IEEE     0.5,100   0,0.5       100000       1.0e-14     1.7e-15
   IEEE     0.01,0.5  0,0.5       100000       9.0e-14     3.4e-15
   IEEE    0.5,10000  0,0.5        20000       2.3e-13     3.8e-14

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invincompletegammac(t_float a, t_float y0)
{
    t_float igammaepsilon;
    t_float iinvgammabignumber;
    t_float x0;
    t_float x1;
    t_float x;
    t_float yl;
    t_float yh;
    t_float y;
    t_float d;
    t_float lgm;
    t_float dithresh;
    ae_int_t i;
    ae_int_t dir;
    t_float tmp;
    t_float result;


    igammaepsilon = 0.000000000000001;
    iinvgammabignumber = 4503599627370496.0;
    x0 = iinvgammabignumber;
    yl = (t_float)(0);
    x1 = (t_float)(0);
    yh = (t_float)(1);
    dithresh = 5*igammaepsilon;
    d = 1/(9*a);
    y = 1-d-invnormaldistribution(y0)*mathLib_float_sqrt(d);
    x = a*y*y*y;
    lgm = lngamma(a, &tmp);
    i = 0;
    while(i<10)
    {
        if( ae_fp_greater(x,x0)||ae_fp_less(x,x1) )
        {
            d = 0.0625;
            break;
        }
        y = incompletegammac(a, x);
        if( ae_fp_less(y,yl)||ae_fp_greater(y,yh) )
        {
            d = 0.0625;
            break;
        }
        if( ae_fp_less(y,y0) )
        {
            x0 = x;
            yl = y;
        }
        else
        {
            x1 = x;
            yh = y;
        }
        d = (a-1)*ae_log(x)-x-lgm;
        if( ae_fp_less(d,-709.78271289338399) )
        {
            d = 0.0625;
            break;
        }
        d = -ae_exp(d);
        d = (y-y0)/d;
        if( ae_fp_less(mathLib_float_abs(d/x),igammaepsilon) )
        {
            result = x;
            return result;
        }
        x = x-d;
        i = i+1;
    }
    if( ae_fp_eq(x0,iinvgammabignumber) )
    {
        if( ae_fp_less_eq(x,(t_float)(0)) )
        {
            x = (t_float)(1);
        }
        while(ae_fp_eq(x0,iinvgammabignumber))
        {
            x = (1+d)*x;
            y = incompletegammac(a, x);
            if( ae_fp_less(y,y0) )
            {
                x0 = x;
                yl = y;
                break;
            }
            d = d+d;
        }
    }
    d = 0.5;
    dir = 0;
    i = 0;
    while(i<400)
    {
        x = x1+d*(x0-x1);
        y = incompletegammac(a, x);
        lgm = (x0-x1)/(x1+x0);
        if( ae_fp_less(mathLib_float_abs(lgm),dithresh) )
        {
            break;
        }
        lgm = (y-y0)/y0;
        if( ae_fp_less(mathLib_float_abs(lgm),dithresh) )
        {
            break;
        }
        if( ae_fp_less_eq(x,0.0) )
        {
            break;
        }
        if( ae_fp_greater_eq(y,y0) )
        {
            x1 = x;
            yh = y;
            if( dir<0 )
            {
                dir = 0;
                d = 0.5;
            }
            else
            {
                if( dir>1 )
                {
                    d = 0.5*d+0.5;
                }
                else
                {
                    d = (y0-yl)/(yh-yl);
                }
            }
            dir = dir+1;
        }
        else
        {
            x0 = x;
            yl = y;
            if( dir>0 )
            {
                dir = 0;
                d = 0.5;
            }
            else
            {
                if( dir<-1 )
                {
                    d = 0.5*d;
                }
                else
                {
                    d = (y0-yl)/(yh-yl);
                }
            }
            dir = dir-1;
        }
        i = i+1;
    }
    result = x;
    return result;
}




/*************************************************************************
Airy function

Solution of the differential equation

y"(x) = xy.

The function returns the two independent solutions Ai, Bi
and their first derivatives Ai'(x), Bi'(x).

Evaluation is by power series summation for small x,
by rational minimax approximations for large x.



ACCURACY:
Error criterion is absolute when function <= 1, relative
when function > 1, except * denotes relative error criterion.
For large negative x, the absolute error increases as x^1.5.
For large positive x, the relative error increases as x^1.5.

Arithmetic  domain   function  # trials      peak         rms
IEEE        -10, 0     Ai        10000       1.6e-15     2.7e-16
IEEE          0, 10    Ai        10000       2.3e-14*    1.8e-15*
IEEE        -10, 0     Ai'       10000       4.6e-15     7.6e-16
IEEE          0, 10    Ai'       10000       1.8e-14*    1.5e-15*
IEEE        -10, 10    Bi        30000       4.2e-15     5.3e-16
IEEE        -10, 10    Bi'       30000       4.9e-15     7.3e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 2000 by Stephen L. Moshier
*************************************************************************/
void airy(t_float x,
     t_float* ai,
     t_float* aip,
     t_float* bi,
     t_float* bip)
{
    t_float z;
    t_float zz;
    t_float t;
    t_float f;
    t_float g;
    t_float uf;
    t_float ug;
    t_float k;
    t_float zeta;
    t_float theta;
    ae_int_t domflg;
    t_float c1;
    t_float c2;
    t_float sqrt3;
    t_float sqpii;
    t_float afn;
    t_float afd;
    t_float agn;
    t_float agd;
    t_float apfn;
    t_float apfd;
    t_float apgn;
    t_float apgd;
    t_float an;
    t_float ad;
    t_float apn;
    t_float apd;
    t_float bn16;
    t_float bd16;
    t_float bppn;
    t_float bppd;

    *ai = 0;
    *aip = 0;
    *bi = 0;
    *bip = 0;

    sqpii = 5.64189583547756286948E-1;
    c1 = 0.35502805388781723926;
    c2 = 0.258819403792806798405;
    sqrt3 = 1.732050807568877293527;
    domflg = 0;
    if( ae_fp_greater(x,25.77) )
    {
        *ai = (t_float)(0);
        *aip = (t_float)(0);
        *bi = ae_maxrealnumber;
        *bip = ae_maxrealnumber;
        return;
    }
    if( ae_fp_less(x,-2.09) )
    {
        domflg = 15;
        t = mathLib_float_sqrt(-x);
        zeta = -2.0*x*t/3.0;
        t = mathLib_float_sqrt(t);
        k = sqpii/t;
        z = 1.0/zeta;
        zz = z*z;
        afn = -1.31696323418331795333E-1;
        afn = afn*zz-6.26456544431912369773E-1;
        afn = afn*zz-6.93158036036933542233E-1;
        afn = afn*zz-2.79779981545119124951E-1;
        afn = afn*zz-4.91900132609500318020E-2;
        afn = afn*zz-4.06265923594885404393E-3;
        afn = afn*zz-1.59276496239262096340E-4;
        afn = afn*zz-2.77649108155232920844E-6;
        afn = afn*zz-1.67787698489114633780E-8;
        afd = 1.00000000000000000000E0;
        afd = afd*zz+1.33560420706553243746E1;
        afd = afd*zz+3.26825032795224613948E1;
        afd = afd*zz+2.67367040941499554804E1;
        afd = afd*zz+9.18707402907259625840E0;
        afd = afd*zz+1.47529146771666414581E0;
        afd = afd*zz+1.15687173795188044134E-1;
        afd = afd*zz+4.40291641615211203805E-3;
        afd = afd*zz+7.54720348287414296618E-5;
        afd = afd*zz+4.51850092970580378464E-7;
        uf = 1.0+zz*afn/afd;
        agn = 1.97339932091685679179E-2;
        agn = agn*zz+3.91103029615688277255E-1;
        agn = agn*zz+1.06579897599595591108E0;
        agn = agn*zz+9.39169229816650230044E-1;
        agn = agn*zz+3.51465656105547619242E-1;
        agn = agn*zz+6.33888919628925490927E-2;
        agn = agn*zz+5.85804113048388458567E-3;
        agn = agn*zz+2.82851600836737019778E-4;
        agn = agn*zz+6.98793669997260967291E-6;
        agn = agn*zz+8.11789239554389293311E-8;
        agn = agn*zz+3.41551784765923618484E-10;
        agd = 1.00000000000000000000E0;
        agd = agd*zz+9.30892908077441974853E0;
        agd = agd*zz+1.98352928718312140417E1;
        agd = agd*zz+1.55646628932864612953E1;
        agd = agd*zz+5.47686069422975497931E0;
        agd = agd*zz+9.54293611618961883998E-1;
        agd = agd*zz+8.64580826352392193095E-2;
        agd = agd*zz+4.12656523824222607191E-3;
        agd = agd*zz+1.01259085116509135510E-4;
        agd = agd*zz+1.17166733214413521882E-6;
        agd = agd*zz+4.91834570062930015649E-9;
        ug = z*agn/agd;
        theta = zeta+0.25*ae_pi;
        f = ae_sin(theta);
        g = ae_cos(theta);
        *ai = k*(f*uf-g*ug);
        *bi = k*(g*uf+f*ug);
        apfn = 1.85365624022535566142E-1;
        apfn = apfn*zz+8.86712188052584095637E-1;
        apfn = apfn*zz+9.87391981747398547272E-1;
        apfn = apfn*zz+4.01241082318003734092E-1;
        apfn = apfn*zz+7.10304926289631174579E-2;
        apfn = apfn*zz+5.90618657995661810071E-3;
        apfn = apfn*zz+2.33051409401776799569E-4;
        apfn = apfn*zz+4.08718778289035454598E-6;
        apfn = apfn*zz+2.48379932900442457853E-8;
        apfd = 1.00000000000000000000E0;
        apfd = apfd*zz+1.47345854687502542552E1;
        apfd = apfd*zz+3.75423933435489594466E1;
        apfd = apfd*zz+3.14657751203046424330E1;
        apfd = apfd*zz+1.09969125207298778536E1;
        apfd = apfd*zz+1.78885054766999417817E0;
        apfd = apfd*zz+1.41733275753662636873E-1;
        apfd = apfd*zz+5.44066067017226003627E-3;
        apfd = apfd*zz+9.39421290654511171663E-5;
        apfd = apfd*zz+5.65978713036027009243E-7;
        uf = 1.0+zz*apfn/apfd;
        apgn = -3.55615429033082288335E-2;
        apgn = apgn*zz-6.37311518129435504426E-1;
        apgn = apgn*zz-1.70856738884312371053E0;
        apgn = apgn*zz-1.50221872117316635393E0;
        apgn = apgn*zz-5.63606665822102676611E-1;
        apgn = apgn*zz-1.02101031120216891789E-1;
        apgn = apgn*zz-9.48396695961445269093E-3;
        apgn = apgn*zz-4.60325307486780994357E-4;
        apgn = apgn*zz-1.14300836484517375919E-5;
        apgn = apgn*zz-1.33415518685547420648E-7;
        apgn = apgn*zz-5.63803833958893494476E-10;
        apgd = 1.00000000000000000000E0;
        apgd = apgd*zz+9.85865801696130355144E0;
        apgd = apgd*zz+2.16401867356585941885E1;
        apgd = apgd*zz+1.73130776389749389525E1;
        apgd = apgd*zz+6.17872175280828766327E0;
        apgd = apgd*zz+1.08848694396321495475E0;
        apgd = apgd*zz+9.95005543440888479402E-2;
        apgd = apgd*zz+4.78468199683886610842E-3;
        apgd = apgd*zz+1.18159633322838625562E-4;
        apgd = apgd*zz+1.37480673554219441465E-6;
        apgd = apgd*zz+5.79912514929147598821E-9;
        ug = z*apgn/apgd;
        k = sqpii*t;
        *aip = -k*(g*uf+f*ug);
        *bip = k*(f*uf-g*ug);
        return;
    }
    if( ae_fp_greater_eq(x,2.09) )
    {
        domflg = 5;
        t = mathLib_float_sqrt(x);
        zeta = 2.0*x*t/3.0;
        g = ae_exp(zeta);
        t = mathLib_float_sqrt(t);
        k = 2.0*t*g;
        z = 1.0/zeta;
        an = 3.46538101525629032477E-1;
        an = an*z+1.20075952739645805542E1;
        an = an*z+7.62796053615234516538E1;
        an = an*z+1.68089224934630576269E2;
        an = an*z+1.59756391350164413639E2;
        an = an*z+7.05360906840444183113E1;
        an = an*z+1.40264691163389668864E1;
        an = an*z+9.99999999999999995305E-1;
        ad = 5.67594532638770212846E-1;
        ad = ad*z+1.47562562584847203173E1;
        ad = ad*z+8.45138970141474626562E1;
        ad = ad*z+1.77318088145400459522E2;
        ad = ad*z+1.64234692871529701831E2;
        ad = ad*z+7.14778400825575695274E1;
        ad = ad*z+1.40959135607834029598E1;
        ad = ad*z+1.00000000000000000470E0;
        f = an/ad;
        *ai = sqpii*f/k;
        k = -0.5*sqpii*t/g;
        apn = 6.13759184814035759225E-1;
        apn = apn*z+1.47454670787755323881E1;
        apn = apn*z+8.20584123476060982430E1;
        apn = apn*z+1.71184781360976385540E2;
        apn = apn*z+1.59317847137141783523E2;
        apn = apn*z+6.99778599330103016170E1;
        apn = apn*z+1.39470856980481566958E1;
        apn = apn*z+1.00000000000000000550E0;
        apd = 3.34203677749736953049E-1;
        apd = apd*z+1.11810297306158156705E1;
        apd = apd*z+7.11727352147859965283E1;
        apd = apd*z+1.58778084372838313640E2;
        apd = apd*z+1.53206427475809220834E2;
        apd = apd*z+6.86752304592780337944E1;
        apd = apd*z+1.38498634758259442477E1;
        apd = apd*z+9.99999999999999994502E-1;
        f = apn/apd;
        *aip = f*k;
        if( ae_fp_greater(x,8.3203353) )
        {
            bn16 = -2.53240795869364152689E-1;
            bn16 = bn16*z+5.75285167332467384228E-1;
            bn16 = bn16*z-3.29907036873225371650E-1;
            bn16 = bn16*z+6.44404068948199951727E-2;
            bn16 = bn16*z-3.82519546641336734394E-3;
            bd16 = 1.00000000000000000000E0;
            bd16 = bd16*z-7.15685095054035237902E0;
            bd16 = bd16*z+1.06039580715664694291E1;
            bd16 = bd16*z-5.23246636471251500874E0;
            bd16 = bd16*z+9.57395864378383833152E-1;
            bd16 = bd16*z-5.50828147163549611107E-2;
            f = z*bn16/bd16;
            k = sqpii*g;
            *bi = k*(1.0+f)/t;
            bppn = 4.65461162774651610328E-1;
            bppn = bppn*z-1.08992173800493920734E0;
            bppn = bppn*z+6.38800117371827987759E-1;
            bppn = bppn*z-1.26844349553102907034E-1;
            bppn = bppn*z+7.62487844342109852105E-3;
            bppd = 1.00000000000000000000E0;
            bppd = bppd*z-8.70622787633159124240E0;
            bppd = bppd*z+1.38993162704553213172E1;
            bppd = bppd*z-7.14116144616431159572E0;
            bppd = bppd*z+1.34008595960680518666E0;
            bppd = bppd*z-7.84273211323341930448E-2;
            f = z*bppn/bppd;
            *bip = k*t*(1.0+f);
            return;
        }
    }
    f = 1.0;
    g = x;
    t = 1.0;
    uf = 1.0;
    ug = x;
    k = 1.0;
    z = x*x*x;
    while(ae_fp_greater(t,T_FLOAT_MIN))
    {
        uf = uf*z;
        k = k+1.0;
        uf = uf/k;
        ug = ug*z;
        k = k+1.0;
        ug = ug/k;
        uf = uf/k;
        f = f+uf;
        k = k+1.0;
        ug = ug/k;
        g = g+ug;
        t = mathLib_float_abs(uf/f);
    }
    uf = c1*f;
    ug = c2*g;
    if( domflg%2==0 )
    {
        *ai = uf-ug;
    }
    if( domflg/2%2==0 )
    {
        *bi = sqrt3*(uf+ug);
    }
    k = 4.0;
    uf = x*x/2.0;
    ug = z/3.0;
    f = uf;
    g = 1.0+ug;
    uf = uf/3.0;
    t = 1.0;
    while(ae_fp_greater(t,T_FLOAT_MIN))
    {
        uf = uf*z;
        ug = ug/k;
        k = k+1.0;
        ug = ug*z;
        uf = uf/k;
        f = f+uf;
        k = k+1.0;
        ug = ug/k;
        uf = uf/k;
        g = g+ug;
        k = k+1.0;
        t = mathLib_float_abs(ug/g);
    }
    uf = c1*f;
    ug = c2*g;
    if( domflg/4%2==0 )
    {
        *aip = uf-ug;
    }
    if( domflg/8%2==0 )
    {
        *bip = sqrt3*(uf+ug);
    }
}






/*************************************************************************
Beta function


                  -     -
                 | (a) | (b)
beta( a, b )  =  -----------.
                    -
                   | (a+b)

For large arguments the logarithm of the function is
evaluated using lgam(), then exponentiated.

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE       0,30       30000       8.1e-14     1.1e-14

Cephes Math Library Release 2.0:  April, 1987
Copyright 1984, 1987 by Stephen L. Moshier
*************************************************************************/
t_float beta(t_float a, t_float b)
{
    t_float y;
    t_float sg;
    t_float s;
    t_float result;


    sg = (t_float)(1);
    /* ae_assert(ae_fp_greater(a,(t_float)(0))||(a != (t_float)(ae_ifloor(a))), "Overflow in Beta"); */
    /* ae_assert(ae_fp_greater(b,(t_float)(0))||(b != (t_float)(ae_ifloor(b))), "Overflow in Beta"); */
    y = a+b;
    if( ae_fp_greater(mathLib_float_abs(y),171.624376956302725) )
    {
        y = lngamma(y, &s);
        sg = sg*s;
        y = lngamma(b, &s)-y;
        sg = sg*s;
        y = lngamma(a, &s)+y;
        sg = sg*s;
        // ae_assert(ae_fp_less_eq(y,ae_log(ae_maxrealnumber)), "Overflow in Beta");
        result = sg*ae_exp(y);
        return result;
    }
    y = gammafunction(y);
    ae_assert((y != (t_float)(0)), "Overflow in Beta");
    if( ae_fp_greater(a,b) )
    {
        y = gammafunction(a)/y;
        y = y*gammafunction(b);
    }
    else
    {
        y = gammafunction(b)/y;
        y = y*gammafunction(a);
    }
    result = y;
    return result;
}







/*************************************************************************
Binomial distribution

Returns the sum of the terms 0 through k of the Binomial
probability density:

  k
  --  ( n )   j      n-j
  >   (   )  p  (1-p)
  --  ( j )
 j=0

The terms are not summed directly; instead the incomplete
beta integral is employed, according to the formula

y = bdtr( k, n, p ) = incbet( n-k, k+1, 1-p ).

The arguments must be positive, with p ranging from 0 to 1.

ACCURACY:

Tested at random points (a,b,p), with p between 0 and 1.

              a,b                     Relative error:
arithmetic  domain     # trials      peak         rms
 For p between 0.001 and 1:
   IEEE     0,100       100000      4.3e-15     2.6e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float binomialdistribution(ae_int_t k,
     ae_int_t n,
     t_float p)
{
    t_float dk;
    t_float dn;
    t_float result;


    ae_assert(ae_fp_greater_eq(p, (t_float)(0)) && 
	      ae_fp_less_eq(p , (t_float)(1)), "Domain error in BinomialDistribution");
    ae_assert(k>=-1&&k<=n, "Domain error in BinomialDistribution");
    if( k==-1 )
    {
        result = (t_float)(0);
        return result;
    }
    if( k==n )
    {
        result = (t_float)(1);
        return result;
    }
    dn = (t_float)(n-k);
    if( k==0 )
    {
        dk = ae_pow(1.0-p, dn);
    }
    else
    {
        dk = (t_float)(k+1);
        dk = incompletebeta(dn, dk, 1.0-p);
    }
    result = dk;
    return result;
}


/*************************************************************************
Complemented binomial distribution

Returns the sum of the terms k+1 through n of the Binomial
probability density:

  n
  --  ( n )   j      n-j
  >   (   )  p  (1-p)
  --  ( j )
 j=k+1

The terms are not summed directly; instead the incomplete
beta integral is employed, according to the formula

y = bdtrc( k, n, p ) = incbet( k+1, n-k, p ).

The arguments must be positive, with p ranging from 0 to 1.

ACCURACY:

Tested at random points (a,b,p).

              a,b                     Relative error:
arithmetic  domain     # trials      peak         rms
 For p between 0.001 and 1:
   IEEE     0,100       100000      6.7e-15     8.2e-16
 For p between 0 and .001:
   IEEE     0,100       100000      1.5e-13     2.7e-15

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float binomialcdistribution(ae_int_t k,
     ae_int_t n,
     t_float p)
{
    t_float dk;
    t_float dn;
    t_float result;


    ae_assert(ae_fp_greater_eq(p,(t_float)(0))&&
	      ae_fp_less_eq(p , (t_float)(1)), "Domain error in BinomialDistributionC");
    ae_assert(k>=-1&&k<=n, "Domain error in BinomialDistributionC");
    if( k==-1 )
    {
        result = (t_float)(1);
        return result;
    }
    if( k==n )
    {
        result = (t_float)(0);
        return result;
    }
    dn = (t_float)(n-k);
    if( k==0 )
    {
      if( ae_fp_less(p , 0.01) )
        {
            dk = -nuexpm1(dn*nulog1p(-p));
        }
        else
        {
            dk = 1.0-ae_pow(1.0-p, dn);
        }
    }
    else
    {
        dk = (t_float)(k+1);
        dk = incompletebeta(dk, dn, p);
    }
    result = dk;
    return result;
}


/*************************************************************************
Inverse binomial distribution

Finds the event probability p such that the sum of the
terms 0 through k of the Binomial probability density
is equal to the given cumulative probability y.

This is accomplished using the inverse beta integral
function and the relation

1 - p = incbi( n-k, k+1, y ).

ACCURACY:

Tested at random points (a,b,p).

              a,b                     Relative error:
arithmetic  domain     # trials      peak         rms
 For p between 0.001 and 1:
   IEEE     0,100       100000      2.3e-14     6.4e-16
   IEEE     0,10000     100000      6.6e-12     1.2e-13
 For p between 10^-6 and 0.001:
   IEEE     0,100       100000      2.0e-12     1.3e-14
   IEEE     0,10000     100000      1.5e-12     3.2e-14

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invbinomialdistribution(ae_int_t k,
     ae_int_t n,
     t_float y)
{
    t_float dk;
    t_float dn;
    t_float p;
    t_float result;


    ae_assert(k>=0&&k<n, "Domain error in InvBinomialDistribution");
    dn = (t_float)(n-k);
    if( k==0 )
    {
        if( ae_fp_greater(y,0.8) )
        {
            p = -nuexpm1(nulog1p(y-1.0)/dn);
        }
        else
        {
            p = 1.0-ae_pow(y, 1.0/dn);
        }
    }
    else
    {
        dk = (t_float)(k+1);
        p = incompletebeta(dn, dk, 0.5);
        if( ae_fp_greater(p,0.5) )
        {
            p = invincompletebeta(dk, dn, 1.0-y);
        }
        else
        {
            p = 1.0-invincompletebeta(dn, dk, y);
        }
    }
    result = p;
    return result;
}




/*************************************************************************
Calculation of the value of the Chebyshev polynomials of the
first and second kinds.

Parameters:
    r   -   polynomial kind, either 1 or 2.
    n   -   degree, n>=0
    x   -   argument, -1 <= x <= 1

Result:
    the value of the Chebyshev polynomial at x
*************************************************************************/
t_float chebyshevcalculate(ae_int_t r,
     ae_int_t n,
     t_float x)
{
    ae_int_t i;
    t_float a;
    t_float b;
    t_float result;


    result = (t_float)(0);
    
    /*
     * Prepare A and B
     */
    if( r==1 )
    {
        a = (t_float)(1);
        b = x;
    }
    else
    {
        a = (t_float)(1);
        b = 2*x;
    }
    
    /*
     * Special cases: N=0 or N=1
     */
    if( n==0 )
    {
        result = a;
        return result;
    }
    if( n==1 )
    {
        result = b;
        return result;
    }
    
    /*
     * General case: N>=2
     */
    for(i=2; i<=n; i++)
    {
        result = 2*x*b-a;
        a = b;
        b = result;
    }
    return result;
}


/*************************************************************************
Summation of Chebyshev polynomials using Clenshaws recurrence formula.

This routine calculates
    c[0]*T0(x) + c[1]*T1(x) + ... + c[N]*TN(x)
or
    c[0]*U0(x) + c[1]*U1(x) + ... + c[N]*UN(x)
depending on the R.

Parameters:
    r   -   polynomial kind, either 1 or 2.
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Chebyshev polynomial at x
*************************************************************************/
t_float chebyshevsum(/* Real    */ ae_vector* c,
     ae_int_t r,
     ae_int_t n,
     t_float x)
{
    t_float b1;
    t_float b2;
    ae_int_t i;
    t_float result;


    b1 = (t_float)(0);
    b2 = (t_float)(0);
    for(i=n; i>=1; i--)
    {
        result = 2*x*b1-b2+c->ptr.p_t_float[i];
        b2 = b1;
        b1 = result;
    }
    if( r==1 )
    {
        result = -b2+x*b1+c->ptr.p_t_float[0];
    }
    else
    {
        result = -b2+2*x*b1+c->ptr.p_t_float[0];
    }
    return result;
}


/*************************************************************************
Representation of Tn as C[0] + C[1]*X + ... + C[N]*X^N

Input parameters:
    N   -   polynomial degree, n>=0

Output parameters:
    C   -   coefficients
*************************************************************************/
void chebyshevcoefficients(ae_int_t n,
     /* Real    */ ae_vector* c)
{
    ae_int_t i;

    ae_vector_clear(c);

    ae_vector_set_length(c, n+1);
    for(i=0; i<=n; i++)
    {
        c->ptr.p_t_float[i] = (t_float)(0);
    }
    if( n==0||n==1 )
    {
        c->ptr.p_t_float[n] = (t_float)(1);
    }
    else
    {
        c->ptr.p_t_float[n] = ae_exp((n-1)*ae_log((t_float)(2)));
        for(i=0; i<=n/2-1; i++)
        {
            c->ptr.p_t_float[n-2*(i+1)] = -c->ptr.p_t_float[n-2*i]*(n-2*i)*(n-2*i-1)/4/(i+1)/(n-i-1);
        }
    }
}


/*************************************************************************
Conversion of a series of Chebyshev polynomials to a power series.

Represents A[0]*T0(x) + A[1]*T1(x) + ... + A[N]*Tn(x) as
B[0] + B[1]*X + ... + B[N]*X^N.

Input parameters:
    A   -   Chebyshev series coefficients
    N   -   degree, N>=0
    
Output parameters
    B   -   power series coefficients
*************************************************************************/
void fromchebyshev(/* Real    */ ae_vector* a,
     ae_int_t n,
     /* Real    */ ae_vector* b) {
    ae_int_t i;
    ae_int_t k;
    t_float e;
    t_float d;

    ae_vector_clear(b);

    ae_vector_set_length(b, n+1);
    for(i=0; i<=n; i++)
    {
        b->ptr.p_t_float[i] = (t_float)(0);
    }
    d = (t_float)(0);
    i = 0;
    do
    {
        k = i;
        do
        {
            e = b->ptr.p_t_float[k];
            b->ptr.p_t_float[k] = (t_float)(0);
            if( i<=1&&k==i )
            {
                b->ptr.p_t_float[k] = (t_float)(1);
            }
            else
            {
                if( i!=0 )
                {
                    b->ptr.p_t_float[k] = 2*d;
                }
                if( k>i+1 )
                {
                    b->ptr.p_t_float[k] = b->ptr.p_t_float[k]-b->ptr.p_t_float[k-2];
                }
            }
            d = e;
            k = k+1;
        }
        while(k<=n);
        d = b->ptr.p_t_float[i];
        e = (t_float)(0);
        k = i;
        while(k<=n)
        {
            e = e+b->ptr.p_t_float[k]*a->ptr.p_t_float[k];
            k = k+2;
        }
        b->ptr.p_t_float[i] = e;
        i = i+1;
    }
    while(i<=n);
}




/*************************************************************************
Chi-square distribution

Returns the area under the left hand tail (from 0 to x)
of the Chi square probability density function with
v degrees of freedom.


                                  x
                                   -
                       1          | |  v/2-1  -t/2
 P( x | v )   =   -----------     |   t      e     dt
                   v/2  -       | |
                  2    | (v/2)   -
                                  0

where x is the Chi-square variable.

The incomplete gamma integral is used, according to the
formula

y = chdtr( v, x ) = igam( v/2.0, x/2.0 ).

The arguments must both be positive.

ACCURACY:

See incomplete gamma function


Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float chisquaredistribution(t_float v, t_float x)
{
    t_float result;


    ae_assert(ae_fp_greater_eq(x,(t_float)(0))&&ae_fp_greater_eq(v,(t_float)(1)), "Domain error in ChiSquareDistribution");
    result = incompletegamma(v/2.0, x/2.0);
    return result;
}


/*************************************************************************
Complemented Chi-square distribution

Returns the area under the right hand tail (from x to
infinity) of the Chi square probability density function
with v degrees of freedom:

                                 inf.
                                   -
                       1          | |  v/2-1  -t/2
 P( x | v )   =   -----------     |   t      e     dt
                   v/2  -       | |
                  2    | (v/2)   -
                                  x

where x is the Chi-square variable.

The incomplete gamma integral is used, according to the
formula

y = chdtr( v, x ) = igamc( v/2.0, x/2.0 ).

The arguments must both be positive.

ACCURACY:

See incomplete gamma function

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float chisquarecdistribution(t_float v, t_float x)
{
    t_float result;


    ae_assert(ae_fp_greater_eq(x,(t_float)(0))&&ae_fp_greater_eq(v,(t_float)(1)), "Domain error in ChiSquareDistributionC");
    result = incompletegammac(v/2.0, x/2.0);
    return result;
}


/*************************************************************************
Inverse of complemented Chi-square distribution

Finds the Chi-square argument x such that the integral
from x to infinity of the Chi-square density is equal
to the given cumulative probability y.

This is accomplished using the inverse gamma integral
function and the relation

   x/2 = igami( df/2, y );

ACCURACY:

See inverse incomplete gamma function


Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invchisquaredistribution(t_float v, t_float y)
{
    t_float result;


    ae_assert((ae_fp_greater_eq(y,(t_float)(0))&&
	       ae_fp_less_eq(y , (t_float)(1)))&&ae_fp_greater_eq(v,(t_float)(1)), "Domain error in InvChiSquareDistribution");
    result = 2*invincompletegammac(0.5*v, y);
    return result;
}




/*************************************************************************
Dawson's Integral

Approximates the integral

                            x
                            -
                     2     | |        2
 dawsn(x)  =  exp( -x  )   |    exp( t  ) dt
                         | |
                          -
                          0

Three different rational approximations are employed, for
the intervals 0 to 3.25; 3.25 to 6.25; and 6.25 up.

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE      0,10        10000       6.9e-16     1.0e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 2000 by Stephen L. Moshier
*************************************************************************/
t_float dawsonintegral(t_float x)
{
    t_float x2;
    t_float y;
    ae_int_t sg;
    t_float an;
    t_float ad;
    t_float bn;
    t_float bd;
    t_float cn;
    t_float cd;
    t_float result;


    sg = 1;
    if( ae_fp_less(x , (t_float)(0)) )
    {
        sg = -1;
        x = -x;
    }
    if( ae_fp_less(x , 3.25) )
    {
        x2 = x*x;
        an = 1.13681498971755972054E-11;
        an = an*x2+8.49262267667473811108E-10;
        an = an*x2+1.94434204175553054283E-8;
        an = an*x2+9.53151741254484363489E-7;
        an = an*x2+3.07828309874913200438E-6;
        an = an*x2+3.52513368520288738649E-4;
        an = an*x2+(-8.50149846724410912031E-4);
        an = an*x2+4.22618223005546594270E-2;
        an = an*x2+(-9.17480371773452345351E-2);
        an = an*x2+9.99999999999999994612E-1;
        ad = 2.40372073066762605484E-11;
        ad = ad*x2+1.48864681368493396752E-9;
        ad = ad*x2+5.21265281010541664570E-8;
        ad = ad*x2+1.27258478273186970203E-6;
        ad = ad*x2+2.32490249820789513991E-5;
        ad = ad*x2+3.25524741826057911661E-4;
        ad = ad*x2+3.48805814657162590916E-3;
        ad = ad*x2+2.79448531198828973716E-2;
        ad = ad*x2+1.58874241960120565368E-1;
        ad = ad*x2+5.74918629489320327824E-1;
        ad = ad*x2+1.00000000000000000539E0;
        y = x*an/ad;
        result = sg*y;
        return result;
    }
    x2 = 1.0/(x*x);
    if( ae_fp_less(x , 6.25) )
    {
        bn = 5.08955156417900903354E-1;
        bn = bn*x2-2.44754418142697847934E-1;
        bn = bn*x2+9.41512335303534411857E-2;
        bn = bn*x2-2.18711255142039025206E-2;
        bn = bn*x2+3.66207612329569181322E-3;
        bn = bn*x2-4.23209114460388756528E-4;
        bn = bn*x2+3.59641304793896631888E-5;
        bn = bn*x2-2.14640351719968974225E-6;
        bn = bn*x2+9.10010780076391431042E-8;
        bn = bn*x2-2.40274520828250956942E-9;
        bn = bn*x2+3.59233385440928410398E-11;
        bd = 1.00000000000000000000E0;
        bd = bd*x2-6.31839869873368190192E-1;
        bd = bd*x2+2.36706788228248691528E-1;
        bd = bd*x2-5.31806367003223277662E-2;
        bd = bd*x2+8.48041718586295374409E-3;
        bd = bd*x2-9.47996768486665330168E-4;
        bd = bd*x2+7.81025592944552338085E-5;
        bd = bd*x2-4.55875153252442634831E-6;
        bd = bd*x2+1.89100358111421846170E-7;
        bd = bd*x2-4.91324691331920606875E-9;
        bd = bd*x2+7.18466403235734541950E-11;
        y = 1.0/x+x2*bn/(bd*x);
        result = sg*0.5*y;
        return result;
    }
    if( ae_fp_greater(x,1.0E9) )
    {
        result = sg*0.5/x;
        return result;
    }
    cn = -5.90592860534773254987E-1;
    cn = cn*x2+6.29235242724368800674E-1;
    cn = cn*x2-1.72858975380388136411E-1;
    cn = cn*x2+1.64837047825189632310E-2;
    cn = cn*x2-4.86827613020462700845E-4;
    cd = 1.00000000000000000000E0;
    cd = cd*x2-2.69820057197544900361E0;
    cd = cd*x2+1.73270799045947845857E0;
    cd = cd*x2-3.93708582281939493482E-1;
    cd = cd*x2+3.44278924041233391079E-2;
    cd = cd*x2-9.73655226040941223894E-4;
    y = 1.0/x+x2*cn/(cd*x);
    result = sg*0.5*y;
    return result;
}




/*************************************************************************
Complete elliptic integral of the first kind

Approximates the integral



           pi/2
            -
           | |
           |           dt
K(m)  =    |    ------------------
           |                   2
         | |    sqrt( 1 - m sin t )
          -
           0

using the approximation

    P(x)  -  log x Q(x).

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE       0,1        30000       2.5e-16     6.8e-17

Cephes Math Library, Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float ellipticintegralk(t_float m)
{
    t_float result;


    result = ellipticintegralkhighprecision(1.0-m);
    return result;
}


/*************************************************************************
Complete elliptic integral of the first kind

Approximates the integral



           pi/2
            -
           | |
           |           dt
K(m)  =    |    ------------------
           |                   2
         | |    sqrt( 1 - m sin t )
          -
           0

where m = 1 - m1, using the approximation

    P(x)  -  log x Q(x).

The argument m1 is used rather than m so that the logarithmic
singularity at m = 1 will be shifted to the origin; this
preserves maximum accuracy.

K(0) = pi/2.

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE       0,1        30000       2.5e-16     6.8e-17

Cephes Math Library, Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float ellipticintegralkhighprecision(t_float m1)
{
    t_float p;
    t_float q;
    t_float result;


    if( ae_fp_less_eq(m1 , T_FLOAT_MIN) )
    {
        result = 1.3862943611198906188E0-0.5*ae_log(m1);
    }
    else
    {
        p = 1.37982864606273237150E-4;
        p = p*m1+2.28025724005875567385E-3;
        p = p*m1+7.97404013220415179367E-3;
        p = p*m1+9.85821379021226008714E-3;
        p = p*m1+6.87489687449949877925E-3;
        p = p*m1+6.18901033637687613229E-3;
        p = p*m1+8.79078273952743772254E-3;
        p = p*m1+1.49380448916805252718E-2;
        p = p*m1+3.08851465246711995998E-2;
        p = p*m1+9.65735902811690126535E-2;
        p = p*m1+1.38629436111989062502E0;
        q = 2.94078955048598507511E-5;
        q = q*m1+9.14184723865917226571E-4;
        q = q*m1+5.94058303753167793257E-3;
        q = q*m1+1.54850516649762399335E-2;
        q = q*m1+2.39089602715924892727E-2;
        q = q*m1+3.01204715227604046988E-2;
        q = q*m1+3.73774314173823228969E-2;
        q = q*m1+4.88280347570998239232E-2;
        q = q*m1+7.03124996963957469739E-2;
        q = q*m1+1.24999999999870820058E-1;
        q = q*m1+4.99999999999999999821E-1;
        result = p-q*ae_log(m1);
    }
    return result;
}


/*************************************************************************
Incomplete elliptic integral of the first kind F(phi|m)

Approximates the integral



               phi
                -
               | |
               |           dt
F(phi_\m)  =    |    ------------------
               |                   2
             | |    sqrt( 1 - m sin t )
              -
               0

of amplitude phi and modulus m, using the arithmetic -
geometric mean algorithm.




ACCURACY:

Tested at random points with m in [0, 1] and phi as indicated.

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE     -10,10       200000      7.4e-16     1.0e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
t_float incompleteellipticintegralk(t_float phi, t_float m)
{
    t_float a;
    t_float b;
    t_float c;
    t_float e;
    t_float temp;
    t_float pio2;
    t_float t;
    t_float k;
    ae_int_t d;
    ae_int_t md;
    ae_int_t s;
    ae_int_t npio2;
    t_float result;


    pio2 = 1.57079632679489661923;
    if( ae_fp_eq(m,(t_float)(0)) )
    {
        result = phi;
        return result;
    }
    a = 1-m;
    if( ae_fp_eq(a,(t_float)(0)) )
    {
        result = ae_log(ae_tan(0.5*(pio2+phi)));
        return result;
    }
    npio2 = ae_ifloor(phi/pio2);
    if( npio2%2!=0 )
    {
        npio2 = npio2+1;
    }
    if( npio2!=0 )
    {
        k = ellipticintegralk(1-a);
        phi = phi-npio2*pio2;
    }
    else
    {
        k = (t_float)(0);
    }
    if( ae_fp_less(phi , (t_float)(0)) )
    {
        phi = -phi;
        s = -1;
    }
    else
    {
        s = 0;
    }
    b = mathLib_float_sqrt(a);
    t = ae_tan(phi);
    if( ae_fp_greater(mathLib_float_abs(t),(t_float)(10)) )
    {
        e = 1.0/(b*t);
        if( ae_fp_less(mathLib_float_abs(e) , (t_float)(10)) )
        {
            e = ae_atan(e);
            if( npio2==0 )
            {
                k = ellipticintegralk(1-a);
            }
            temp = k-incompleteellipticintegralk(e, m);
            if( s<0 )
            {
                temp = -temp;
            }
            result = temp+npio2*k;
            return result;
        }
    }
    a = 1.0;
    c = mathLib_float_sqrt(m);
    d = 1;
    md = 0;
    while(ae_fp_greater(mathLib_float_abs(c/a),T_FLOAT_MIN))
    {
        temp = b/a;
        phi = phi+ae_atan(t*temp)+md*ae_pi;
        md = ae_trunc((phi+pio2)/ae_pi);
        t = t*(1.0+temp)/(1.0-temp*t*t);
        c = 0.5*(a-b);
        temp = mathLib_float_sqrt(a*b);
        a = 0.5*(a+b);
        b = temp;
        d = d+d;
    }
    temp = (ae_atan(t)+md*ae_pi)/(d*a);
    if( s<0 )
    {
        temp = -temp;
    }
    result = temp+npio2*k;
    return result;
}


/*************************************************************************
Complete elliptic integral of the second kind

Approximates the integral


           pi/2
            -
           | |                 2
E(m)  =    |    sqrt( 1 - m sin t ) dt
         | |
          -
           0

using the approximation

     P(x)  -  x log x Q(x).

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE       0, 1       10000       2.1e-16     7.3e-17

Cephes Math Library, Release 2.8: June, 2000
Copyright 1984, 1987, 1989, 2000 by Stephen L. Moshier
*************************************************************************/
t_float ellipticintegrale(t_float m)
{
    t_float p;
    t_float q;
    t_float result;


    ae_assert(ae_fp_greater_eq(m,(t_float)(0))&&
	      ae_fp_less_eq(m , (t_float)(1)), "Domain error in EllipticIntegralE: m<0 or m>1");
    m = 1-m;
    if( ae_fp_eq(m,(t_float)(0)) )
    {
        result = (t_float)(1);
        return result;
    }
    p = 1.53552577301013293365E-4;
    p = p*m+2.50888492163602060990E-3;
    p = p*m+8.68786816565889628429E-3;
    p = p*m+1.07350949056076193403E-2;
    p = p*m+7.77395492516787092951E-3;
    p = p*m+7.58395289413514708519E-3;
    p = p*m+1.15688436810574127319E-2;
    p = p*m+2.18317996015557253103E-2;
    p = p*m+5.68051945617860553470E-2;
    p = p*m+4.43147180560990850618E-1;
    p = p*m+1.00000000000000000299E0;
    q = 3.27954898576485872656E-5;
    q = q*m+1.00962792679356715133E-3;
    q = q*m+6.50609489976927491433E-3;
    q = q*m+1.68862163993311317300E-2;
    q = q*m+2.61769742454493659583E-2;
    q = q*m+3.34833904888224918614E-2;
    q = q*m+4.27180926518931511717E-2;
    q = q*m+5.85936634471101055642E-2;
    q = q*m+9.37499997197644278445E-2;
    q = q*m+2.49999999999888314361E-1;
    result = p-q*m*ae_log(m);
    return result;
}


/*************************************************************************
Incomplete elliptic integral of the second kind

Approximates the integral


               phi
                -
               | |
               |                   2
E(phi_\m)  =    |    sqrt( 1 - m sin t ) dt
               |
             | |
              -
               0

of amplitude phi and modulus m, using the arithmetic -
geometric mean algorithm.

ACCURACY:

Tested at random arguments with phi in [-10, 10] and m in
[0, 1].
                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE     -10,10      150000       3.3e-15     1.4e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1993, 2000 by Stephen L. Moshier
*************************************************************************/
t_float incompleteellipticintegrale(t_float phi, t_float m)
{
    t_float pio2;
    t_float a;
    t_float b;
    t_float c;
    t_float e;
    t_float temp;
    t_float lphi;
    t_float t;
    t_float ebig;
    ae_int_t d;
    ae_int_t md;
    ae_int_t npio2;
    ae_int_t s;
    t_float result;


    pio2 = 1.57079632679489661923;
    if( ae_fp_eq(m,(t_float)(0)) )
    {
        result = phi;
        return result;
    }
    lphi = phi;
    npio2 = ae_ifloor(lphi/pio2);
    if( npio2%2!=0 )
    {
        npio2 = npio2+1;
    }
    lphi = lphi-npio2*pio2;
    if( ae_fp_less(lphi , (t_float)(0)) )
    {
        lphi = -lphi;
        s = -1;
    }
    else
    {
        s = 1;
    }
    a = 1.0-m;
    ebig = ellipticintegrale(m);
    if( ae_fp_eq(a,(t_float)(0)) )
    {
        temp = ae_sin(lphi);
        if( s<0 )
        {
            temp = -temp;
        }
        result = temp+npio2*ebig;
        return result;
    }
    t = ae_tan(lphi);
    b = mathLib_float_sqrt(a);
    
    /*
     * Thanks to Brian Fitzgerald <fitzgb@mml0.meche.rpi.edu>
     * for pointing out an instability near odd multiples of pi/2
     */
    if( ae_fp_greater(mathLib_float_abs(t),(t_float)(10)) )
    {
        
        /*
         * Transform the amplitude
         */
        e = 1.0/(b*t);
        
        /*
         * ... but avoid multiple recursions.
         */
        if( ae_fp_less(mathLib_float_abs(e) , (t_float)(10)) )
        {
            e = ae_atan(e);
            temp = ebig+m*ae_sin(lphi)*ae_sin(e)-incompleteellipticintegrale(e, m);
            if( s<0 )
            {
                temp = -temp;
            }
            result = temp+npio2*ebig;
            return result;
        }
    }
    c = mathLib_float_sqrt(m);
    a = 1.0;
    d = 1;
    e = 0.0;
    md = 0;
    while(ae_fp_greater(mathLib_float_abs(c/a),T_FLOAT_MIN))
    {
        temp = b/a;
        lphi = lphi+ae_atan(t*temp)+md*ae_pi;
        md = ae_trunc((lphi+pio2)/ae_pi);
        t = t*(1.0+temp)/(1.0-temp*t*t);
        c = 0.5*(a-b);
        temp = mathLib_float_sqrt(a*b);
        a = 0.5*(a+b);
        b = temp;
        d = d+d;
        e = e+c*ae_sin(lphi);
    }
    temp = ebig/ellipticintegralk(m);
    temp = temp*((ae_atan(t)+md*ae_pi)/(d*a));
    temp = temp+e;
    if( s<0 )
    {
        temp = -temp;
    }
    result = temp+npio2*ebig;
    return result;
}




/*************************************************************************
Exponential integral Ei(x)

              x
               -     t
              | |   e
   Ei(x) =   -|-   ---  dt .
            | |     t
             -
            -inf

Not defined for x <= 0.
See also expn.c.



ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE       0,100       50000      8.6e-16     1.3e-16

Cephes Math Library Release 2.8:  May, 1999
Copyright 1999 by Stephen L. Moshier
*************************************************************************/
t_float exponentialintegralei(t_float x)
{
    t_float eul;
    t_float f;
    t_float f1;
    t_float f2;
    t_float w;
    t_float result;


    eul = 0.5772156649015328606065;
    if( ae_fp_less_eq(x , (t_float)(0)) )
    {
        result = (t_float)(0);
        return result;
    }
    if( ae_fp_less(x , (t_float)(2)) )
    {
        f1 = -5.350447357812542947283;
        f1 = f1*x+218.5049168816613393830;
        f1 = f1*x-4176.572384826693777058;
        f1 = f1*x+55411.76756393557601232;
        f1 = f1*x-331338.1331178144034309;
        f1 = f1*x+1592627.163384945414220;
        f2 = 1.000000000000000000000;
        f2 = f2*x-52.50547959112862969197;
        f2 = f2*x+1259.616186786790571525;
        f2 = f2*x-17565.49581973534652631;
        f2 = f2*x+149306.2117002725991967;
        f2 = f2*x-729494.9239640527645655;
        f2 = f2*x+1592627.163384945429726;
        f = f1/f2;
        result = eul+ae_log(x)+x*f;
        return result;
    }
    if( ae_fp_less(x , (t_float)(4)) )
    {
        w = 1/x;
        f1 = 1.981808503259689673238E-2;
        f1 = f1*w-1.271645625984917501326;
        f1 = f1*w-2.088160335681228318920;
        f1 = f1*w+2.755544509187936721172;
        f1 = f1*w-4.409507048701600257171E-1;
        f1 = f1*w+4.665623805935891391017E-2;
        f1 = f1*w-1.545042679673485262580E-3;
        f1 = f1*w+7.059980605299617478514E-5;
        f2 = 1.000000000000000000000;
        f2 = f2*w+1.476498670914921440652;
        f2 = f2*w+5.629177174822436244827E-1;
        f2 = f2*w+1.699017897879307263248E-1;
        f2 = f2*w+2.291647179034212017463E-2;
        f2 = f2*w+4.450150439728752875043E-3;
        f2 = f2*w+1.727439612206521482874E-4;
        f2 = f2*w+3.953167195549672482304E-5;
        f = f1/f2;
        result = ae_exp(x)*w*(1+w*f);
        return result;
    }
    if( ae_fp_less(x , (t_float)(8)) )
    {
        w = 1/x;
        f1 = -1.373215375871208729803;
        f1 = f1*w-7.084559133740838761406E-1;
        f1 = f1*w+1.580806855547941010501;
        f1 = f1*w-2.601500427425622944234E-1;
        f1 = f1*w+2.994674694113713763365E-2;
        f1 = f1*w-1.038086040188744005513E-3;
        f1 = f1*w+4.371064420753005429514E-5;
        f1 = f1*w+2.141783679522602903795E-6;
        f2 = 1.000000000000000000000;
        f2 = f2*w+8.585231423622028380768E-1;
        f2 = f2*w+4.483285822873995129957E-1;
        f2 = f2*w+7.687932158124475434091E-2;
        f2 = f2*w+2.449868241021887685904E-2;
        f2 = f2*w+8.832165941927796567926E-4;
        f2 = f2*w+4.590952299511353531215E-4;
        f2 = f2*w+(-4.729848351866523044863E-6);
        f2 = f2*w+2.665195537390710170105E-6;
        f = f1/f2;
        result = ae_exp(x)*w*(1+w*f);
        return result;
    }
    if( ae_fp_less(x , (t_float)(16)) )
    {
        w = 1/x;
        f1 = -2.106934601691916512584;
        f1 = f1*w+1.732733869664688041885;
        f1 = f1*w-2.423619178935841904839E-1;
        f1 = f1*w+2.322724180937565842585E-2;
        f1 = f1*w+2.372880440493179832059E-4;
        f1 = f1*w-8.343219561192552752335E-5;
        f1 = f1*w+1.363408795605250394881E-5;
        f1 = f1*w-3.655412321999253963714E-7;
        f1 = f1*w+1.464941733975961318456E-8;
        f1 = f1*w+6.176407863710360207074E-10;
        f2 = 1.000000000000000000000;
        f2 = f2*w-2.298062239901678075778E-1;
        f2 = f2*w+1.105077041474037862347E-1;
        f2 = f2*w-1.566542966630792353556E-2;
        f2 = f2*w+2.761106850817352773874E-3;
        f2 = f2*w-2.089148012284048449115E-4;
        f2 = f2*w+1.708528938807675304186E-5;
        f2 = f2*w-4.459311796356686423199E-7;
        f2 = f2*w+1.394634930353847498145E-8;
        f2 = f2*w+6.150865933977338354138E-10;
        f = f1/f2;
        result = ae_exp(x)*w*(1+w*f);
        return result;
    }
    if( ae_fp_less(x , (t_float)(32)) )
    {
        w = 1/x;
        f1 = -2.458119367674020323359E-1;
        f1 = f1*w-1.483382253322077687183E-1;
        f1 = f1*w+7.248291795735551591813E-2;
        f1 = f1*w-1.348315687380940523823E-2;
        f1 = f1*w+1.342775069788636972294E-3;
        f1 = f1*w-7.942465637159712264564E-5;
        f1 = f1*w+2.644179518984235952241E-6;
        f1 = f1*w-4.239473659313765177195E-8;
        f2 = 1.000000000000000000000;
        f2 = f2*w-1.044225908443871106315E-1;
        f2 = f2*w-2.676453128101402655055E-1;
        f2 = f2*w+9.695000254621984627876E-2;
        f2 = f2*w-1.601745692712991078208E-2;
        f2 = f2*w+1.496414899205908021882E-3;
        f2 = f2*w-8.462452563778485013756E-5;
        f2 = f2*w+2.728938403476726394024E-6;
        f2 = f2*w-4.239462431819542051337E-8;
        f = f1/f2;
        result = ae_exp(x)*w*(1+w*f);
        return result;
    }
    if( ae_fp_less(x , (t_float)(64)) )
    {
        w = 1/x;
        f1 = 1.212561118105456670844E-1;
        f1 = f1*w-5.823133179043894485122E-1;
        f1 = f1*w+2.348887314557016779211E-1;
        f1 = f1*w-3.040034318113248237280E-2;
        f1 = f1*w+1.510082146865190661777E-3;
        f1 = f1*w-2.523137095499571377122E-5;
        f2 = 1.000000000000000000000;
        f2 = f2*w-1.002252150365854016662;
        f2 = f2*w+2.928709694872224144953E-1;
        f2 = f2*w-3.337004338674007801307E-2;
        f2 = f2*w+1.560544881127388842819E-3;
        f2 = f2*w-2.523137093603234562648E-5;
        f = f1/f2;
        result = ae_exp(x)*w*(1+w*f);
        return result;
    }
    w = 1/x;
    f1 = -7.657847078286127362028E-1;
    f1 = f1*w+6.886192415566705051750E-1;
    f1 = f1*w-2.132598113545206124553E-1;
    f1 = f1*w+3.346107552384193813594E-2;
    f1 = f1*w-3.076541477344756050249E-3;
    f1 = f1*w+1.747119316454907477380E-4;
    f1 = f1*w-6.103711682274170530369E-6;
    f1 = f1*w+1.218032765428652199087E-7;
    f1 = f1*w-1.086076102793290233007E-9;
    f2 = 1.000000000000000000000;
    f2 = f2*w-1.888802868662308731041;
    f2 = f2*w+1.066691687211408896850;
    f2 = f2*w-2.751915982306380647738E-1;
    f2 = f2*w+3.930852688233823569726E-2;
    f2 = f2*w-3.414684558602365085394E-3;
    f2 = f2*w+1.866844370703555398195E-4;
    f2 = f2*w-6.345146083130515357861E-6;
    f2 = f2*w+1.239754287483206878024E-7;
    f2 = f2*w-1.086076102793126632978E-9;
    f = f1/f2;
    result = ae_exp(x)*w*(1+w*f);
    return result;
}


/*************************************************************************
Exponential integral En(x)

Evaluates the exponential integral

                inf.
                  -
                 | |   -xt
                 |    e
     E (x)  =    |    ----  dt.
      n          |      n
               | |     t
                -
                 1


Both n and x must be nonnegative.

The routine employs either a power series, a continued
fraction, or an asymptotic formula depending on the
relative values of n and x.

ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE      0, 30       10000       1.7e-15     3.6e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1985, 2000 by Stephen L. Moshier
*************************************************************************/
t_float exponentialintegralen(t_float x, ae_int_t n)
{
    t_float r;
    t_float t;
    t_float yk;
    t_float xk;
    t_float pk;
    t_float pkm1;
    t_float pkm2;
    t_float qk;
    t_float qkm1;
    t_float qkm2;
    t_float psi;
    t_float z;
    ae_int_t i;
    ae_int_t k;
    t_float big;
    t_float eul;
    t_float result;


    eul = 0.57721566490153286060;
    big = 1.44115188075855872*ae_pow((t_float)(10), (t_float)(17));
    if( ((n<0||ae_fp_less(x , (t_float)(0)))||ae_fp_greater(x,(t_float)(170)))||(ae_fp_eq(x,(t_float)(0))&&n<2) )
    {
        result = (t_float)(-1);
        return result;
    }
    if( ae_fp_eq(x,(t_float)(0)) )
    {
        result = (t_float)1/(t_float)(n-1);
        return result;
    }
    if( n==0 )
    {
        result = ae_exp(-x)/x;
        return result;
    }
    if( n>5000 )
    {
        xk = x+n;
        yk = 1/(xk*xk);
        t = (t_float)(n);
        result = yk*t*(6*x*x-8*t*x+t*t);
        result = yk*(result+t*(t-2.0*x));
        result = yk*(result+t);
        result = (result+1)*ae_exp(-x)/xk;
        return result;
    }
    if( ae_fp_less_eq(x , (t_float)(1)) )
    {
        psi = -eul-ae_log(x);
        for(i=1; i<=n-1; i++)
        {
            psi = psi+(t_float)1/(t_float)i;
        }
        z = -x;
        xk = (t_float)(0);
        yk = (t_float)(1);
        pk = (t_float)(1-n);
        if( n==1 )
        {
            result = 0.0;
        }
        else
        {
            result = 1.0/pk;
        }
        do
        {
            xk = xk+1;
            yk = yk*z/xk;
            pk = pk+1;
            if( (pk != (t_float)(0)) )
            {
                result = result+yk/pk;
            }
            if( (result != (t_float)(0)) )
            {
                t = mathLib_float_abs(yk/result);
            }
            else
            {
                t = (t_float)(1);
            }
        }
        while(ae_fp_greater_eq(t,T_FLOAT_MIN));
        t = (t_float)(1);
        for(i=1; i<=n-1; i++)
        {
            t = t*z/i;
        }
        result = psi*t-result;
        return result;
    }
    else
    {
        k = 1;
        pkm2 = (t_float)(1);
        qkm2 = x;
        pkm1 = 1.0;
        qkm1 = x+n;
        result = pkm1/qkm1;
        do
        {
            k = k+1;
            if( k%2==1 )
            {
                yk = (t_float)(1);
                xk = n+(t_float)(k-1)/(t_float)2;
            }
            else
            {
                yk = x;
                xk = (t_float)k/(t_float)2;
            }
            pk = pkm1*yk+pkm2*xk;
            qk = qkm1*yk+qkm2*xk;
            if( (qk != (t_float)(0)) )
            {
                r = pk/qk;
                t = mathLib_float_abs((result-r)/r);
                result = r;
            }
            else
            {
                t = (t_float)(1);
            }
            pkm2 = pkm1;
            pkm1 = pk;
            qkm2 = qkm1;
            qkm1 = qk;
            if( ae_fp_greater(mathLib_float_abs(pk),big) )
            {
                pkm2 = pkm2/big;
                pkm1 = pkm1/big;
                qkm2 = qkm2/big;
                qkm1 = qkm1/big;
            }
        }
        while(ae_fp_greater_eq(t,T_FLOAT_MIN));
        result = result*ae_exp(-x);
    }
    return result;
}




/*************************************************************************
F distribution

Returns the area from zero to x under the F density
function (also known as Snedcor's density or the
variance ratio density).  This is the density
of x = (u1/df1)/(u2/df2), where u1 and u2 are random
variables having Chi square distributions with df1
and df2 degrees of freedom, respectively.
The incomplete beta integral is used, according to the
formula

P(x) = incbet( df1/2, df2/2, (df1*x/(df2 + df1*x) ).


The arguments a and b are greater than zero, and x is
nonnegative.

ACCURACY:

Tested at random points (a,b,x).

               x     a,b                     Relative error:
arithmetic  domain  domain     # trials      peak         rms
   IEEE      0,1    0,100       100000      9.8e-15     1.7e-15
   IEEE      1,5    0,100       100000      6.5e-15     3.5e-16
   IEEE      0,1    1,10000     100000      2.2e-11     3.3e-12
   IEEE      1,5    1,10000     100000      1.1e-11     1.7e-13

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float fdistribution(ae_int_t a, ae_int_t b, t_float x)
{
    t_float w;
    t_float result;


    ae_assert((a>=1&&b>=1)&&ae_fp_greater_eq(x,(t_float)(0)), "Domain error in FDistribution");
    w = a*x;
    w = w/(b+w);
    result = incompletebeta(0.5*a, 0.5*b, w);
    return result;
}


/*************************************************************************
Complemented F distribution

Returns the area from x to infinity under the F density
function (also known as Snedcor's density or the
variance ratio density).


                     inf.
                      -
             1       | |  a-1      b-1
1-P(x)  =  ------    |   t    (1-t)    dt
           B(a,b)  | |
                    -
                     x


The incomplete beta integral is used, according to the
formula

P(x) = incbet( df2/2, df1/2, (df2/(df2 + df1*x) ).


ACCURACY:

Tested at random points (a,b,x) in the indicated intervals.
               x     a,b                     Relative error:
arithmetic  domain  domain     # trials      peak         rms
   IEEE      0,1    1,100       100000      3.7e-14     5.9e-16
   IEEE      1,5    1,100       100000      8.0e-15     1.6e-15
   IEEE      0,1    1,10000     100000      1.8e-11     3.5e-13
   IEEE      1,5    1,10000     100000      2.0e-11     3.0e-12

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float fcdistribution(ae_int_t a, ae_int_t b, t_float x)
{
    t_float w;
    t_float result;


    ae_assert((a>=1&&b>=1)&&ae_fp_greater_eq(x,(t_float)(0)), "Domain error in FCDistribution");
    w = b/(b+a*x);
    result = incompletebeta(0.5*b, 0.5*a, w);
    return result;
}


/*************************************************************************
Inverse of complemented F distribution

Finds the F density argument x such that the integral
from x to infinity of the F density is equal to the
given probability p.

This is accomplished using the inverse beta integral
function and the relations

     z = incbi( df2/2, df1/2, p )
     x = df2 (1-z) / (df1 z).

Note: the following relations hold for the inverse of
the uncomplemented F distribution:

     z = incbi( df1/2, df2/2, p )
     x = df2 z / (df1 (1-z)).

ACCURACY:

Tested at random points (a,b,p).

             a,b                     Relative error:
arithmetic  domain     # trials      peak         rms
 For p between .001 and 1:
   IEEE     1,100       100000      8.3e-15     4.7e-16
   IEEE     1,10000     100000      2.1e-11     1.4e-13
 For p between 10^-6 and 10^-3:
   IEEE     1,100        50000      1.3e-12     8.4e-15
   IEEE     1,10000      50000      3.0e-12     4.8e-14

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invfdistribution(ae_int_t a,
     ae_int_t b,
     t_float y)
{
    t_float w;
    t_float result;


    ae_assert(((a>=1&&b>=1)&&ae_fp_greater(y,(t_float)(0)))&&ae_fp_less_eq(y , (t_float)(1)), "Domain error in InvFDistribution");
    
    /*
     * Compute probability for x = 0.5
     */
    w = incompletebeta(0.5*b, 0.5*a, 0.5);
    
    /*
     * If that is greater than y, then the solution w < .5
     * Otherwise, solve at 1-y to remove cancellation in (b - b*w)
     */
    if( ae_fp_greater(w,y)||ae_fp_less(y , 0.001) )
    {
        w = invincompletebeta(0.5*b, 0.5*a, y);
        result = (b-b*w)/(a*w);
    }
    else
    {
        w = invincompletebeta(0.5*a, 0.5*b, 1.0-y);
        result = b*w/(a*(1.0-w));
    }
    return result;
}




/*************************************************************************
Fresnel integral

Evaluates the Fresnel integrals

          x
          -
         | |
C(x) =   |   cos(pi/2 t**2) dt,
       | |
        -
         0

          x
          -
         | |
S(x) =   |   sin(pi/2 t**2) dt.
       | |
        -
         0


The integrals are evaluated by a power series for x < 1.
For x >= 1 auxiliary functions f(x) and g(x) are employed
such that

C(x) = 0.5 + f(x) sin( pi/2 x**2 ) - g(x) cos( pi/2 x**2 )
S(x) = 0.5 - f(x) cos( pi/2 x**2 ) - g(x) sin( pi/2 x**2 )



ACCURACY:

 Relative error.

Arithmetic  function   domain     # trials      peak         rms
  IEEE       S(x)      0, 10       10000       2.0e-15     3.2e-16
  IEEE       C(x)      0, 10       10000       1.8e-15     3.3e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 2000 by Stephen L. Moshier
*************************************************************************/
void fresnelintegral(t_float x, t_float* c, t_float* s)
{
    t_float xxa;
    t_float f;
    t_float g;
    t_float cc;
    t_float ss;
    t_float t;
    t_float u;
    t_float x2;
    t_float sn;
    t_float sd;
    t_float cn;
    t_float cd;
    t_float fn;
    t_float fd;
    t_float gn;
    t_float gd;
    t_float mpi;
    t_float mpio2;


    mpi = 3.14159265358979323846;
    mpio2 = 1.57079632679489661923;
    xxa = x;
    x = mathLib_float_abs(xxa);
    x2 = x*x;
    if( ae_fp_less(x2 , 2.5625) )
    {
        t = x2*x2;
        sn = -2.99181919401019853726E3;
        sn = sn*t+7.08840045257738576863E5;
        sn = sn*t-6.29741486205862506537E7;
        sn = sn*t+2.54890880573376359104E9;
        sn = sn*t-4.42979518059697779103E10;
        sn = sn*t+3.18016297876567817986E11;
        sd = 1.00000000000000000000E0;
        sd = sd*t+2.81376268889994315696E2;
        sd = sd*t+4.55847810806532581675E4;
        sd = sd*t+5.17343888770096400730E6;
        sd = sd*t+4.19320245898111231129E8;
        sd = sd*t+2.24411795645340920940E10;
        sd = sd*t+6.07366389490084639049E11;
        cn = -4.98843114573573548651E-8;
        cn = cn*t+9.50428062829859605134E-6;
        cn = cn*t-6.45191435683965050962E-4;
        cn = cn*t+1.88843319396703850064E-2;
        cn = cn*t-2.05525900955013891793E-1;
        cn = cn*t+9.99999999999999998822E-1;
        cd = 3.99982968972495980367E-12;
        cd = cd*t+9.15439215774657478799E-10;
        cd = cd*t+1.25001862479598821474E-7;
        cd = cd*t+1.22262789024179030997E-5;
        cd = cd*t+8.68029542941784300606E-4;
        cd = cd*t+4.12142090722199792936E-2;
        cd = cd*t+1.00000000000000000118E0;
        *s = ae_sign(xxa)*x*x2*sn/sd;
        *c = ae_sign(xxa)*x*cn/cd;
        return;
    }
    if( ae_fp_greater(x,36974.0) )
    {
        *c = ae_sign(xxa)*0.5;
        *s = ae_sign(xxa)*0.5;
        return;
    }
    x2 = x*x;
    t = mpi*x2;
    u = 1/(t*t);
    t = 1/t;
    fn = 4.21543555043677546506E-1;
    fn = fn*u+1.43407919780758885261E-1;
    fn = fn*u+1.15220955073585758835E-2;
    fn = fn*u+3.45017939782574027900E-4;
    fn = fn*u+4.63613749287867322088E-6;
    fn = fn*u+3.05568983790257605827E-8;
    fn = fn*u+1.02304514164907233465E-10;
    fn = fn*u+1.72010743268161828879E-13;
    fn = fn*u+1.34283276233062758925E-16;
    fn = fn*u+3.76329711269987889006E-20;
    fd = 1.00000000000000000000E0;
    fd = fd*u+7.51586398353378947175E-1;
    fd = fd*u+1.16888925859191382142E-1;
    fd = fd*u+6.44051526508858611005E-3;
    fd = fd*u+1.55934409164153020873E-4;
    fd = fd*u+1.84627567348930545870E-6;
    fd = fd*u+1.12699224763999035261E-8;
    fd = fd*u+3.60140029589371370404E-11;
    fd = fd*u+5.88754533621578410010E-14;
    fd = fd*u+4.52001434074129701496E-17;
    fd = fd*u+1.25443237090011264384E-20;
    gn = 5.04442073643383265887E-1;
    gn = gn*u+1.97102833525523411709E-1;
    gn = gn*u+1.87648584092575249293E-2;
    gn = gn*u+6.84079380915393090172E-4;
    gn = gn*u+1.15138826111884280931E-5;
    gn = gn*u+9.82852443688422223854E-8;
    gn = gn*u+4.45344415861750144738E-10;
    gn = gn*u+1.08268041139020870318E-12;
    gn = gn*u+1.37555460633261799868E-15;
    gn = gn*u+8.36354435630677421531E-19;
    gn = gn*u+1.86958710162783235106E-22;
    gd = 1.00000000000000000000E0;
    gd = gd*u+1.47495759925128324529E0;
    gd = gd*u+3.37748989120019970451E-1;
    gd = gd*u+2.53603741420338795122E-2;
    gd = gd*u+8.14679107184306179049E-4;
    gd = gd*u+1.27545075667729118702E-5;
    gd = gd*u+1.04314589657571990585E-7;
    gd = gd*u+4.60680728146520428211E-10;
    gd = gd*u+1.10273215066240270757E-12;
    gd = gd*u+1.38796531259578871258E-15;
    gd = gd*u+8.39158816283118707363E-19;
    gd = gd*u+1.86958710162783236342E-22;
    f = 1-u*fn/fd;
    g = t*gn/gd;
    t = mpio2*x2;
    cc = ae_cos(t);
    ss = ae_sin(t);
    t = mpi*x;
    *c = 0.5+(f*ss-g*cc)/t;
    *s = 0.5-(f*cc+g*ss)/t;
    *c = *c*ae_sign(xxa);
    *s = *s*ae_sign(xxa);
}




/*************************************************************************
Calculation of the value of the Hermite polynomial.

Parameters:
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Hermite polynomial Hn at x
*************************************************************************/
t_float hermitecalculate(ae_int_t n, t_float x)
{
    ae_int_t i;
    t_float a;
    t_float b;
    t_float result;


    result = (t_float)(0);
    
    /*
     * Prepare A and B
     */
    a = (t_float)(1);
    b = 2*x;
    
    /*
     * Special cases: N=0 or N=1
     */
    if( n==0 )
    {
        result = a;
        return result;
    }
    if( n==1 )
    {
        result = b;
        return result;
    }
    
    /*
     * General case: N>=2
     */
    for(i=2; i<=n; i++)
    {
        result = 2*x*b-2*(i-1)*a;
        a = b;
        b = result;
    }
    return result;
}


/*************************************************************************
Summation of Hermite polynomials using Clenshaws recurrence formula.

This routine calculates
    c[0]*H0(x) + c[1]*H1(x) + ... + c[N]*HN(x)

Parameters:
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Hermite polynomial at x
*************************************************************************/
t_float hermitesum(/* Real    */ ae_vector* c,
     ae_int_t n,
     t_float x)
{
    t_float b1;
    t_float b2;
    ae_int_t i;
    t_float result;


    b1 = (t_float)(0);
    b2 = (t_float)(0);
    result = (t_float)(0);
    for(i=n; i>=0; i--)
    {
        result = 2*(x*b1-(i+1)*b2)+c->ptr.p_t_float[i];
        b2 = b1;
        b1 = result;
    }
    return result;
}


/*************************************************************************
Representation of Hn as C[0] + C[1]*X + ... + C[N]*X^N

Input parameters:
    N   -   polynomial degree, n>=0

Output parameters:
    C   -   coefficients
*************************************************************************/
void hermitecoefficients(ae_int_t n,
     /* Real    */ ae_vector* c) {
    ae_int_t i;

    ae_vector_clear(c);

    ae_vector_set_length(c, n+1);
    for(i=0; i<=n; i++)
    {
        c->ptr.p_t_float[i] = (t_float)(0);
    }
    c->ptr.p_t_float[n] = ae_exp(n*ae_log((t_float)(2)));
    for(i=0; i<=n/2-1; i++)
    {
        c->ptr.p_t_float[n-2*(i+1)] = -c->ptr.p_t_float[n-2*i]*(n-2*i)*(n-2*i-1)/4/(i+1);
    }
}




/*************************************************************************
Jacobian Elliptic Functions

Evaluates the Jacobian elliptic functions sn(u|m), cn(u|m),
and dn(u|m) of parameter m between 0 and 1, and real
argument u.

These functions are periodic, with quarter-period on the
real axis equal to the complete elliptic integral
ellpk(1.0-m).

Relation to incomplete elliptic integral:
If u = ellik(phi,m), then sn(u|m) = sin(phi),
and cn(u|m) = cos(phi).  Phi is called the amplitude of u.

Computation is by means of the arithmetic-geometric mean
algorithm, except when m is within 1e-9 of 0 or 1.  In the
latter case with m close to 1, the approximation applies
only for phi < pi/2.

ACCURACY:

Tested at random points with u between 0 and 10, m between
0 and 1.

           Absolute error (* = relative error):
arithmetic   function   # trials      peak         rms
   IEEE      phi         10000       9.2e-16*    1.4e-16*
   IEEE      sn          50000       4.1e-15     4.6e-16
   IEEE      cn          40000       3.6e-15     4.4e-16
   IEEE      dn          10000       1.3e-12     1.8e-14

 Peak error observed in consistency check using addition
theorem for sn(u+v) was 4e-16 (absolute).  Also tested by
the above relation to the incomplete elliptic integral.
Accuracy deteriorates when u is large.

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
void jacobianellipticfunctions(t_float u,
     t_float m,
     t_float* sn,
     t_float* cn,
     t_float* dn,
     t_float* ph)
{
    ae_frame _frame_block;
    t_float ai;
    t_float b;
    t_float phi;
    t_float t;
    t_float twon;
    ae_vector a;
    ae_vector c;
    ae_int_t i;

    assert(false); // FIXME: incldue permttuation of [”elow]
    // ae_frame_make(&_frame_block);
    *sn = 0;
    *cn = 0;
    *dn = 0;
    *ph = 0;
    assert(false); // FIXME: incldue permttuation of [”elow]
    /* ae_vector_init(&a, 0, DT_REAL); */
    /* ae_vector_init(&c, 0, DT_REAL); */

    ae_assert(ae_fp_greater_eq(m,(t_float)(0))&&ae_fp_less_eq(m , (t_float)(1)), "Domain error in JacobianEllipticFunctions: m<0 or m>1");
    ae_vector_set_length(&a, 8+1);
    ae_vector_set_length(&c, 8+1);
    if( ae_fp_less(m , 1.0e-9) )
    {
        t = ae_sin(u);
        b = ae_cos(u);
        ai = 0.25*m*(u-t*b);
        *sn = t-ai*b;
        *cn = b+ai*t;
        *ph = u-ai;
        *dn = 1.0-0.5*m*t*t;
	assert(false); // FIXME: incldue permttuation of [”elow]
        // ae_frame_leave();
        return;
    }
    if( ae_fp_greater_eq(m,0.9999999999) )
    {
        ai = 0.25*(1.0-m);
        b = ae_cosh(u);
        t = ae_tanh(u);
        phi = 1.0/b;
        twon = b*ae_sinh(u);
        *sn = t+ai*(twon-u)/(b*b);
        *ph = 2.0*ae_atan(ae_exp(u))-1.57079632679489661923+ai*(twon-u)/b;
        ai = ai*t*phi;
        *cn = phi-ai*(twon-u);
        *dn = phi+ai*(twon+u);
	assert(false); // FIXME: incldue permttuation of [”elow]
        // ae_frame_leave();
        return;
    }
    a.ptr.p_t_float[0] = 1.0;
    b = mathLib_float_sqrt(1.0-m);
    c.ptr.p_t_float[0] = mathLib_float_sqrt(m);
    twon = 1.0;
    i = 0;
    while(ae_fp_greater(mathLib_float_abs(c.ptr.p_t_float[i]/a.ptr.p_t_float[i]),T_FLOAT_MIN))
    {
        if( i>7 )
        {
            ae_assert(ae_false, "Overflow in JacobianEllipticFunctions");
            break;
        }
        ai = a.ptr.p_t_float[i];
        i = i+1;
        c.ptr.p_t_float[i] = 0.5*(ai-b);
        t = mathLib_float_sqrt(ai*b);
        a.ptr.p_t_float[i] = 0.5*(ai+b);
        b = t;
        twon = twon*2.0;
    }
    phi = twon*a.ptr.p_t_float[i]*u;
    do
    {
        t = c.ptr.p_t_float[i]*ae_sin(phi)/a.ptr.p_t_float[i];
        b = phi;
        phi = (ae_asin(t)+phi)/2.0;
        i = i-1;
    }
    while(i!=0);
    *sn = ae_sin(phi);
    t = ae_cos(phi);
    *cn = t;
    *dn = t/ae_cos(phi-b);
    *ph = phi;
    assert(false); // FIXME: incldue permttuation of [”elow]
	//ae_frame_leave();
}




/*************************************************************************
Calculation of the value of the Laguerre polynomial.

Parameters:
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Laguerre polynomial Ln at x
*************************************************************************/
t_float laguerrecalculate(ae_int_t n, t_float x)
{
    t_float a;
    t_float b;
    t_float i;
    t_float result;


    result = (t_float)(1);
    a = (t_float)(1);
    b = 1-x;
    if( n==1 )
    {
        result = b;
    }
    i = (t_float)(2);
    while(ae_fp_less_eq(i , (t_float)(n)))
    {
        result = ((2*i-1-x)*b-(i-1)*a)/i;
        a = b;
        b = result;
        i = i+1;
    }
    return result;
}


/*************************************************************************
Summation of Laguerre polynomials using Clenshaws recurrence formula.

This routine calculates c[0]*L0(x) + c[1]*L1(x) + ... + c[N]*LN(x)

Parameters:
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Laguerre polynomial at x
*************************************************************************/
t_float laguerresum(/* Real    */ ae_vector* c,
     ae_int_t n,
     t_float x)
{
    t_float b1;
    t_float b2;
    ae_int_t i;
    t_float result;


    b1 = (t_float)(0);
    b2 = (t_float)(0);
    result = (t_float)(0);
    for(i=n; i>=0; i--)
    {
        result = (2*i+1-x)*b1/(i+1)-(i+1)*b2/(i+2)+c->ptr.p_t_float[i];
        b2 = b1;
        b1 = result;
    }
    return result;
}


/*************************************************************************
Representation of Ln as C[0] + C[1]*X + ... + C[N]*X^N

Input parameters:
    N   -   polynomial degree, n>=0

Output parameters:
    C   -   coefficients
*************************************************************************/
void laguerrecoefficients(ae_int_t n,
     /* Real    */ ae_vector* c)
{
    ae_int_t i;

    ae_vector_clear(c);

    ae_vector_set_length(c, n+1);
    c->ptr.p_t_float[0] = (t_float)(1);
    for(i=0; i<=n-1; i++)
    {
        c->ptr.p_t_float[i+1] = -c->ptr.p_t_float[i]*(n-i)/(i+1)/(i+1);
    }
}




/*************************************************************************
Calculation of the value of the Legendre polynomial Pn.

Parameters:
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Legendre polynomial Pn at x
*************************************************************************/
t_float legendrecalculate(ae_int_t n, t_float x)
{
    t_float a;
    t_float b;
    ae_int_t i;
    t_float result;


    result = (t_float)(1);
    a = (t_float)(1);
    b = x;
    if( n==0 )
    {
        result = a;
        return result;
    }
    if( n==1 )
    {
        result = b;
        return result;
    }
    for(i=2; i<=n; i++)
    {
        result = ((2*i-1)*x*b-(i-1)*a)/i;
        a = b;
        b = result;
    }
    return result;
}


/*************************************************************************
Summation of Legendre polynomials using Clenshaws recurrence formula.

This routine calculates
    c[0]*P0(x) + c[1]*P1(x) + ... + c[N]*PN(x)

Parameters:
    n   -   degree, n>=0
    x   -   argument

Result:
    the value of the Legendre polynomial at x
*************************************************************************/
t_float legendresum(/* Real    */ ae_vector* c,
     ae_int_t n,
     t_float x)
{
    t_float b1;
    t_float b2;
    ae_int_t i;
    t_float result;


    b1 = (t_float)(0);
    b2 = (t_float)(0);
    result = (t_float)(0);
    for(i=n; i>=0; i--)
    {
        result = (2*i+1)*x*b1/(i+1)-(i+1)*b2/(i+2)+c->ptr.p_t_float[i];
        b2 = b1;
        b1 = result;
    }
    return result;
}


/*************************************************************************
Representation of Pn as C[0] + C[1]*X + ... + C[N]*X^N

Input parameters:
    N   -   polynomial degree, n>=0

Output parameters:
    C   -   coefficients
*************************************************************************/
void legendrecoefficients(ae_int_t n,
     /* Real    */ ae_vector* c)
{
    ae_int_t i;

    ae_vector_clear(c);

    ae_vector_set_length(c, n+1);
    for(i=0; i<=n; i++)
    {
        c->ptr.p_t_float[i] = (t_float)(0);
    }
    c->ptr.p_t_float[n] = (t_float)(1);
    for(i=1; i<=n; i++)
    {
        c->ptr.p_t_float[n] = c->ptr.p_t_float[n]*(n+i)/2/i;
    }
    for(i=0; i<=n/2-1; i++)
    {
        c->ptr.p_t_float[n-2*(i+1)] = -c->ptr.p_t_float[n-2*i]*(n-2*i)*(n-2*i-1)/2/(i+1)/(2*(n-i)-1);
    }
}




/*************************************************************************
Poisson distribution

Returns the sum of the first k+1 terms of the Poisson
distribution:

  k         j
  --   -m  m
  >   e    --
  --       j!
 j=0

The terms are not summed directly; instead the incomplete
gamma integral is employed, according to the relation

y = pdtr( k, m ) = igamc( k+1, m ).

The arguments must both be positive.
ACCURACY:

See incomplete gamma function

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float poissondistribution(ae_int_t k, t_float m)
{
    t_float result;


    ae_assert(k>=0&&ae_fp_greater(m,(t_float)(0)), "Domain error in PoissonDistribution");
    result = incompletegammac((t_float)(k+1), m);
    return result;
}


/*************************************************************************
Complemented Poisson distribution

Returns the sum of the terms k+1 to infinity of the Poisson
distribution:

 inf.       j
  --   -m  m
  >   e    --
  --       j!
 j=k+1

The terms are not summed directly; instead the incomplete
gamma integral is employed, according to the formula

y = pdtrc( k, m ) = igam( k+1, m ).

The arguments must both be positive.

ACCURACY:

See incomplete gamma function

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float poissoncdistribution(ae_int_t k, t_float m)
{
    t_float result;


    ae_assert(k>=0&&ae_fp_greater(m,(t_float)(0)), "Domain error in PoissonDistributionC");
    result = incompletegamma((t_float)(k+1), m);
    return result;
}


/*************************************************************************
Inverse Poisson distribution

Finds the Poisson variable x such that the integral
from 0 to x of the Poisson density is equal to the
given probability y.

This is accomplished using the inverse gamma integral
function and the relation

   m = igami( k+1, y ).

ACCURACY:

See inverse incomplete gamma function

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invpoissondistribution(ae_int_t k, t_float y)
{
    t_float result;


    ae_assert((k>=0&&ae_fp_greater_eq(y,(t_float)(0)))&&ae_fp_less(y , (t_float)(1)), "Domain error in InvPoissonDistribution");
    result = invincompletegammac((t_float)(k+1), y);
    return result;
}




/*************************************************************************
Psi (digamma) function

             d      -
  psi(x)  =  -- ln | (x)
             dx

is the logarithmic derivative of the gamma function.
For integer x,
                  n-1
                   -
psi(n) = -EUL  +   >  1/k.
                   -
                  k=1

This formula is used for 0 < n <= 10.  If x is negative, it
is transformed to a positive argument by the reflection
formula  psi(1-x) = psi(x) + pi cot(pi x).
For general positive x, the argument is made greater than 10
using the recurrence  psi(x+1) = psi(x) + 1/x.
Then the following asymptotic expansion is applied:

                          inf.   B
                           -      2k
psi(x) = log(x) - 1/2x -   >   -------
                           -        2k
                          k=1   2k x

where the B2k are Bernoulli numbers.

ACCURACY:
   Relative error (except absolute when |psi| < 1):
arithmetic   domain     # trials      peak         rms
   IEEE      0,30        30000       1.3e-15     1.4e-16
   IEEE      -30,0       40000       1.5e-15     2.2e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1992, 2000 by Stephen L. Moshier
*************************************************************************/
t_float psi(t_float x)
{
    t_float p;
    t_float q;
    t_float nz;
    t_float s;
    t_float w;
    t_float y;
    t_float z;
    t_float polv;
    ae_int_t i;
    ae_int_t n;
    ae_int_t negative;
    t_float result;


    negative = 0;
    nz = 0.0;
    if( ae_fp_less_eq(x , (t_float)(0)) )
    {
        negative = 1;
        q = x;
        p = (t_float)(ae_ifloor(q));
        if( ae_fp_eq(p,q) )
        {
            ae_assert(ae_false, "Singularity in Psi(x)");
            result = ae_maxrealnumber;
            return result;
        }
        nz = q-p;
        if( (nz != 0.5) )
        {
            if( ae_fp_greater(nz,0.5) )
            {
                p = p+1.0;
                nz = q-p;
            }
            nz = ae_pi/ae_tan(ae_pi*nz);
        }
        else
        {
            nz = 0.0;
        }
        x = 1.0-x;
    }
    if( ae_fp_less_eq(x , 10.0) && ae_fp_eq(x,(t_float)(ae_ifloor(x))) )
    {
        y = 0.0;
        n = ae_ifloor(x);
        for(i=1; i<=n-1; i++)
        {
            w = (t_float)(i);
            y = y+1.0/w;
        }
        y = y-0.57721566490153286061;
    }
    else
    {
        s = x;
        w = 0.0;
        while(ae_fp_less(s , 10.0))
        {
            w = w+1.0/s;
            s = s+1.0;
        }
        if( ae_fp_less(s , 1.0E17) )
        {
            z = 1.0/(s*s);
            polv = 8.33333333333333333333E-2;
            polv = polv*z-2.10927960927960927961E-2;
            polv = polv*z+7.57575757575757575758E-3;
            polv = polv*z-4.16666666666666666667E-3;
            polv = polv*z+3.96825396825396825397E-3;
            polv = polv*z-8.33333333333333333333E-3;
            polv = polv*z+8.33333333333333333333E-2;
            y = z*polv;
        }
        else
        {
            y = 0.0;
        }
        y = ae_log(s)-0.5/s-y-w;
    }
    if( negative!=0 )
    {
        y = y-nz;
    }
    result = y;
    return result;
}






/* /\************************************************************************* */
/* Functional inverse of Student's t distribution */

/* Given probability p, finds the argument t such that stdtr(k,t) */
/* is equal to p. */

/* ACCURACY: */

/* Tested at random 1 <= k <= 100.  The "domain" refers to p: */
/*                      Relative error: */
/* arithmetic   domain     # trials      peak         rms */
/*    IEEE    .001,.999     25000       5.7e-15     8.0e-16 */
/*    IEEE    10^-6,.001    25000       2.0e-12     2.9e-14 */

/* Cephes Math Library Release 2.8:  June, 2000 */
/* Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier */
/* *************************************************************************\/ */
/* t_float invstudenttdistribution(ae_int_t k, t_float p) */
/* { */
/*     t_float t; */
/*     t_float rk; */
/*     t_float z; */
/*     ae_int_t rflg; */
/*     t_float result; */


/*     ae_assert((k>0&&ae_fp_greater(p,(t_float)(0)))&&ae_fp_less(p , (t_float)(1)), "Domain error in InvStudentTDistribution"); */
/*     rk = (t_float)(k); */
/*     if( ae_fp_greater(p,0.25) && ae_fp_less(p , 0.75) ) */
/*     { */
/*         if( ae_fp_eq(p,0.5) ) */
/*         { */
/*             result = (t_float)(0); */
/*             return result; */
/*         } */
/*         z = 1.0-2.0*p; */
/*         z = invincompletebeta(0.5, 0.5*rk, mathLib_float_abs(z)); */
/*         t = mathLib_float_sqrt(rk*z/(1.0-z)); */
/*         if( ae_fp_less(p , 0.5) ) */
/*         { */
/*             t = -t; */
/*         } */
/*         result = t; */
/*         return result; */
/*     } */
/*     rflg = -1; */
/*     if( ae_fp_greater_eq(p,0.5) ) */
/*     { */
/*         p = 1.0-p; */
/*         rflg = 1; */
/*     } */
/*     z = invincompletebeta(0.5*rk, 0.5, 2.0*p); */
/*     if( ae_fp_less(ae_maxrealnumber*z , rk) ) */
/*     { */
/*         result = rflg*ae_maxrealnumber; */
/*         return result; */
/*     } */
/*     t = mathLib_float_sqrt(rk/z-rk); */
/*     result = rflg*t; */
/*     return result; */
/* } */




/*************************************************************************
Sine and cosine integrals

Evaluates the integrals

                         x
                         -
                        |  cos t - 1
  Ci(x) = eul + ln x +  |  --------- dt,
                        |      t
                       -
                        0
            x
            -
           |  sin t
  Si(x) =  |  ----- dt
           |    t
          -
           0

where eul = 0.57721566490153286061 is Euler's constant.
The integrals are approximated by rational functions.
For x > 8 auxiliary functions f(x) and g(x) are employed
such that

Ci(x) = f(x) sin(x) - g(x) cos(x)
Si(x) = pi/2 - f(x) cos(x) - g(x) sin(x)


ACCURACY:
   Test interval = [0,50].
Absolute error, except relative when > 1:
arithmetic   function   # trials      peak         rms
   IEEE        Si        30000       4.4e-16     7.3e-17
   IEEE        Ci        30000       6.9e-16     5.1e-17

Cephes Math Library Release 2.1:  January, 1989
Copyright 1984, 1987, 1989 by Stephen L. Moshier
*************************************************************************/
void sinecosineintegrals(t_float x,
     t_float* si,
     t_float* ci)
{
    t_float z;
    t_float c;
    t_float s;
    t_float f;
    t_float g;
    ae_int_t sg;
    t_float sn;
    t_float sd;
    t_float cn;
    t_float cd;
    t_float fn;
    t_float fd;
    t_float gn;
    t_float gd;

    *si = 0;
    *ci = 0;

    if( ae_fp_less(x , (t_float)(0)) )
    {
        sg = -1;
        x = -x;
    }
    else
    {
        sg = 0;
    }
    if( ae_fp_eq(x,(t_float)(0)) )
    {
        *si = (t_float)(0);
        *ci = -ae_maxrealnumber;
        return;
    }
    if( ae_fp_greater(x,1.0E9) )
    {
        *si = 1.570796326794896619-ae_cos(x)/x;
        *ci = ae_sin(x)/x;
        return;
    }
    if( ae_fp_less_eq(x , (t_float)(4)) )
    {
        z = x*x;
        sn = -8.39167827910303881427E-11;
        sn = sn*z+4.62591714427012837309E-8;
        sn = sn*z-9.75759303843632795789E-6;
        sn = sn*z+9.76945438170435310816E-4;
        sn = sn*z-4.13470316229406538752E-2;
        sn = sn*z+1.00000000000000000302E0;
        sd = 2.03269266195951942049E-12;
        sd = sd*z+1.27997891179943299903E-9;
        sd = sd*z+4.41827842801218905784E-7;
        sd = sd*z+9.96412122043875552487E-5;
        sd = sd*z+1.42085239326149893930E-2;
        sd = sd*z+9.99999999999999996984E-1;
        s = x*sn/sd;
        cn = 2.02524002389102268789E-11;
        cn = cn*z-1.35249504915790756375E-8;
        cn = cn*z+3.59325051419993077021E-6;
        cn = cn*z-4.74007206873407909465E-4;
        cn = cn*z+2.89159652607555242092E-2;
        cn = cn*z-1.00000000000000000080E0;
        cd = 4.07746040061880559506E-12;
        cd = cd*z+3.06780997581887812692E-9;
        cd = cd*z+1.23210355685883423679E-6;
        cd = cd*z+3.17442024775032769882E-4;
        cd = cd*z+5.10028056236446052392E-2;
        cd = cd*z+4.00000000000000000080E0;
        c = z*cn/cd;
        if( sg!=0 )
        {
            s = -s;
        }
        *si = s;
        *ci = 0.57721566490153286061+ae_log(x)+c;
        return;
    }
    s = ae_sin(x);
    c = ae_cos(x);
    z = 1.0/(x*x);
    if( ae_fp_less(x , (t_float)(8)) )
    {
        fn = 4.23612862892216586994E0;
        fn = fn*z+5.45937717161812843388E0;
        fn = fn*z+1.62083287701538329132E0;
        fn = fn*z+1.67006611831323023771E-1;
        fn = fn*z+6.81020132472518137426E-3;
        fn = fn*z+1.08936580650328664411E-4;
        fn = fn*z+5.48900223421373614008E-7;
        fd = 1.00000000000000000000E0;
        fd = fd*z+8.16496634205391016773E0;
        fd = fd*z+7.30828822505564552187E0;
        fd = fd*z+1.86792257950184183883E0;
        fd = fd*z+1.78792052963149907262E-1;
        fd = fd*z+7.01710668322789753610E-3;
        fd = fd*z+1.10034357153915731354E-4;
        fd = fd*z+5.48900252756255700982E-7;
        f = fn/(x*fd);
        gn = 8.71001698973114191777E-2;
        gn = gn*z+6.11379109952219284151E-1;
        gn = gn*z+3.97180296392337498885E-1;
        gn = gn*z+7.48527737628469092119E-2;
        gn = gn*z+5.38868681462177273157E-3;
        gn = gn*z+1.61999794598934024525E-4;
        gn = gn*z+1.97963874140963632189E-6;
        gn = gn*z+7.82579040744090311069E-9;
        gd = 1.00000000000000000000E0;
        gd = gd*z+1.64402202413355338886E0;
        gd = gd*z+6.66296701268987968381E-1;
        gd = gd*z+9.88771761277688796203E-2;
        gd = gd*z+6.22396345441768420760E-3;
        gd = gd*z+1.73221081474177119497E-4;
        gd = gd*z+2.02659182086343991969E-6;
        gd = gd*z+7.82579218933534490868E-9;
        g = z*gn/gd;
    }
    else
    {
        fn = 4.55880873470465315206E-1;
        fn = fn*z+7.13715274100146711374E-1;
        fn = fn*z+1.60300158222319456320E-1;
        fn = fn*z+1.16064229408124407915E-2;
        fn = fn*z+3.49556442447859055605E-4;
        fn = fn*z+4.86215430826454749482E-6;
        fn = fn*z+3.20092790091004902806E-8;
        fn = fn*z+9.41779576128512936592E-11;
        fn = fn*z+9.70507110881952024631E-14;
        fd = 1.00000000000000000000E0;
        fd = fd*z+9.17463611873684053703E-1;
        fd = fd*z+1.78685545332074536321E-1;
        fd = fd*z+1.22253594771971293032E-2;
        fd = fd*z+3.58696481881851580297E-4;
        fd = fd*z+4.92435064317881464393E-6;
        fd = fd*z+3.21956939101046018377E-8;
        fd = fd*z+9.43720590350276732376E-11;
        fd = fd*z+9.70507110881952025725E-14;
        f = fn/(x*fd);
        gn = 6.97359953443276214934E-1;
        gn = gn*z+3.30410979305632063225E-1;
        gn = gn*z+3.84878767649974295920E-2;
        gn = gn*z+1.71718239052347903558E-3;
        gn = gn*z+3.48941165502279436777E-5;
        gn = gn*z+3.47131167084116673800E-7;
        gn = gn*z+1.70404452782044526189E-9;
        gn = gn*z+3.85945925430276600453E-12;
        gn = gn*z+3.14040098946363334640E-15;
        gd = 1.00000000000000000000E0;
        gd = gd*z+1.68548898811011640017E0;
        gd = gd*z+4.87852258695304967486E-1;
        gd = gd*z+4.67913194259625806320E-2;
        gd = gd*z+1.90284426674399523638E-3;
        gd = gd*z+3.68475504442561108162E-5;
        gd = gd*z+3.57043223443740838771E-7;
        gd = gd*z+1.72693748966316146736E-9;
        gd = gd*z+3.87830166023954706752E-12;
        gd = gd*z+3.14040098946363335242E-15;
        g = z*gn/gd;
    }
    *si = 1.570796326794896619-f*c-g*s;
    if( sg!=0 )
    {
        *si = -*si;
    }
    *ci = f*s-g*c;
}


static void trigintegrals_chebiterationshichi(t_float x,
     t_float c,
     t_float* b0,
     t_float* b1,
     t_float* b2)
{


    *b2 = *b1;
    *b1 = *b0;
    *b0 = x*(*b1)-(*b2)+c;
}


/*************************************************************************
Hyperbolic sine and cosine integrals

Approximates the integrals

                           x
                           -
                          | |   cosh t - 1
  Chi(x) = eul + ln x +   |    -----------  dt,
                        | |          t
                         -
                         0

              x
              -
             | |  sinh t
  Shi(x) =   |    ------  dt
           | |       t
            -
            0

where eul = 0.57721566490153286061 is Euler's constant.
The integrals are evaluated by power series for x , 8
and by Chebyshev expansions for x between 8 and 88.
For large x, both functions approach exp(x)/2x.
Arguments greater than 88 in magnitude return MAXNUM.


ACCURACY:

Test interval 0 to 88.
                     Relative error:
arithmetic   function  # trials      peak         rms
   IEEE         Shi      30000       6.9e-16     1.6e-16
       Absolute error, except relative when |Chi| > 1:
   IEEE         Chi      30000       8.4e-16     1.4e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 2000 by Stephen L. Moshier
*************************************************************************/
void hyperbolicsinecosineintegrals(t_float x,
     t_float* shi,
     t_float* chi)
{
    t_float k;
    t_float z;
    t_float c;
    t_float s;
    t_float a;
    ae_int_t sg;
    t_float b0;
    t_float b1;
    t_float b2;

    *shi = 0;
    *chi = 0;

    if( ae_fp_less(x , (t_float)(0)) )
    {
        sg = -1;
        x = -x;
    }
    else
    {
        sg = 0;
    }
    if( ae_fp_eq(x,(t_float)(0)) )
    {
        *shi = (t_float)(0);
        *chi = -ae_maxrealnumber;
        return;
    }
    if( ae_fp_less(x , 8.0) )
    {
        z = x*x;
        a = 1.0;
        s = 1.0;
        c = 0.0;
        k = 2.0;
        do
        {
            a = a*z/k;
            c = c+a/k;
            k = k+1.0;
            a = a/k;
            s = s+a/k;
            k = k+1.0;
        }
        while(ae_fp_greater_eq(mathLib_float_abs(a/s),T_FLOAT_MIN));
        s = s*x;
    }
    else
    {
      if( ae_fp_less(x , 18.0) )
        {
            a = (576.0/x-52.0)/10.0;
            k = ae_exp(x)/x;
            b0 = 1.83889230173399459482E-17;
            b1 = 0.0;
            trigintegrals_chebiterationshichi(a, -9.55485532279655569575E-17, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 2.04326105980879882648E-16, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.09896949074905343022E-15, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.31313534344092599234E-14, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 5.93976226264314278932E-14, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -3.47197010497749154755E-14, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.40059764613117131000E-12, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 9.49044626224223543299E-12, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.61596181145435454033E-11, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.77899784436430310321E-10, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.35455469767246947469E-9, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.03257121792819495123E-9, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -3.56699611114982536845E-8, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.44818877384267342057E-7, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 7.82018215184051295296E-7, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -5.39919118403805073710E-6, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -3.12458202168959833422E-5, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 8.90136741950727517826E-5, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 2.02558474743846862168E-3, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 2.96064440855633256972E-2, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.11847751047257036625E0, &b0, &b1, &b2);
            s = k*0.5*(b0-b2);
            b0 = -8.12435385225864036372E-18;
            b1 = 0.0;
            trigintegrals_chebiterationshichi(a, 2.17586413290339214377E-17, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 5.22624394924072204667E-17, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -9.48812110591690559363E-16, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 5.35546311647465209166E-15, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.21009970113732918701E-14, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -6.00865178553447437951E-14, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 7.16339649156028587775E-13, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -2.93496072607599856104E-12, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.40359438136491256904E-12, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 8.76302288609054966081E-11, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -4.40092476213282340617E-10, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -1.87992075640569295479E-10, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.31458150989474594064E-8, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -4.75513930924765465590E-8, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -2.21775018801848880741E-7, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.94635531373272490962E-6, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 4.33505889257316408893E-6, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -6.13387001076494349496E-5, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, -3.13085477492997465138E-4, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 4.97164789823116062801E-4, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 2.64347496031374526641E-2, &b0, &b1, &b2);
            trigintegrals_chebiterationshichi(a, 1.11446150876699213025E0, &b0, &b1, &b2);
            c = k*0.5*(b0-b2);
        }
        else
        {
	  if( ae_fp_less_eq(x , 88.0) )
            {
                a = (6336.0/x-212.0)/70.0;
                k = ae_exp(x)/x;
                b0 = -1.05311574154850938805E-17;
                b1 = 0.0;
                trigintegrals_chebiterationshichi(a, 2.62446095596355225821E-17, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 8.82090135625368160657E-17, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -3.38459811878103047136E-16, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -8.30608026366935789136E-16, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 3.93397875437050071776E-15, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.01765565969729044505E-14, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -4.21128170307640802703E-14, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -1.60818204519802480035E-13, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 3.34714954175994481761E-13, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 2.72600352129153073807E-12, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.66894954752839083608E-12, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -3.49278141024730899554E-11, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -1.58580661666482709598E-10, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -1.79289437183355633342E-10, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.76281629144264523277E-9, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.69050228879421288846E-8, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.25391771228487041649E-7, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.16229947068677338732E-6, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.61038260117376323993E-5, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 3.49810375601053973070E-4, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.28478065259647610779E-2, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.03665722588798326712E0, &b0, &b1, &b2);
                s = k*0.5*(b0-b2);
                b0 = 8.06913408255155572081E-18;
                b1 = 0.0;
                trigintegrals_chebiterationshichi(a, -2.08074168180148170312E-17, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -5.98111329658272336816E-17, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 2.68533951085945765591E-16, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 4.52313941698904694774E-16, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -3.10734917335299464535E-15, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -4.42823207332531972288E-15, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 3.49639695410806959872E-14, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 6.63406731718911586609E-14, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -3.71902448093119218395E-13, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -1.27135418132338309016E-12, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 2.74851141935315395333E-12, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 2.33781843985453438400E-11, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 2.71436006377612442764E-11, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -2.56600180000355990529E-10, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -1.61021375163803438552E-9, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -4.72543064876271773512E-9, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, -3.00095178028681682282E-9, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 7.79387474390914922337E-8, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.06942765566401507066E-6, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.59503164802313196374E-5, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 3.49592575153777996871E-4, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.28475387530065247392E-2, &b0, &b1, &b2);
                trigintegrals_chebiterationshichi(a, 1.03665693917934275131E0, &b0, &b1, &b2);
                c = k*0.5*(b0-b2);
            }
            else
            {
                if( sg!=0 )
                {
                    *shi = -ae_maxrealnumber;
                }
                else
                {
                    *shi = ae_maxrealnumber;
                }
                *chi = ae_maxrealnumber;
                return;
            }
        }
    }
    if( sg!=0 )
    {
        s = -s;
    }
    *shi = s;
    *chi = 0.57721566490153286061+ae_log(x)+c;
}



























/*************************************************************************
ALGLIB 3.10.0 (source code generated 2015-08-19)
Copyright (c) Sergey Bochkanov (ALGLIB project).

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the hpLysis documentation 
the Free Software Foundation (www.fsf.org); either version 2 of the 
License, or 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
hpLysis documentation for more details.

A copy of the hpLysis documentation is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/


/*************************************************************************
Continued fraction expansion #1 for incomplete beta integral







Cephes Math Library, Release 2.8:  June, 2000
Copyright 1984, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
static t_float ibetaf_incompletebetafe(t_float a, t_float b, t_float x, t_float big, t_float biginv) {

  // assert(false); 
  // FIXME (a) udpate the java-doc [above] and (b) make 'this' accessible from the heade-rfile ... and (c) update our "matrix_deviation.c" wrt. "t-test"

    t_float xk;
    t_float pk;
    t_float pkm1;
    t_float pkm2;
    t_float qk;
    t_float qkm1;
    t_float qkm2;
    t_float k1;
    t_float k2;
    t_float k3;
    t_float k4;
    t_float k5;
    t_float k6;
    t_float k7;
    t_float k8;
    t_float r;
    t_float t;
    t_float ans;
    t_float thresh;
    int n;
    t_float result;

    //! Note[time]: while therea re max 300 iterations, each iteraiton results in approx. 10 add-operations, 10 mulitplcaiton-operaitons and 4 division-oeprations, ie, a max-cost of at least 3000x floating-poitn oeprations: if we assume that the perfomrance-difference between flaoting-poitn oepraitons and on-cached memory-access is 64x, then we (for the latter) may have a perofmrance-icnrease of approx. 50x.
    // FIXME: investigate [ªbove] wrt. our to-be-written perfomranc-test.

    k1 = a;
    k2 = a+b;
    k3 = a;
    k4 = a+1.0;
    k5 = 1.0;
    k6 = b-1.0;
    k7 = k4;
    k8 = a+2.0;
    pkm2 = 0.0;
    qkm2 = 1.0;
    pkm1 = 1.0;
    qkm1 = 1.0;
    ans = 1.0;
    r = 1.0;
    n = 0;
    thresh = 3.0*T_FLOAT_MIN;
    do
      {
        xk = -x*k1*k2/(k3*k4);
        pk = pkm1+pkm2*xk;
        qk = qkm1+qkm2*xk;
        pkm2 = pkm1;
        pkm1 = pk;
        qkm2 = qkm1;
        qkm1 = qk;
        xk = x*k5*k6/(k7*k8);
        pk = pkm1+pkm2*xk;
        qk = qkm1+qkm2*xk;
        pkm2 = pkm1;
        pkm1 = pk;
        qkm2 = qkm1;
        qkm1 = qk;
        if( (qk != (t_float)(0)) )    {
	  r = pk/qk;
        }
        if( (r != (t_float)(0)) ) {
	  t = mathLib_float_abs((ans-r)/r);
	  ans = r;
        } else {
	  t = 1.0;
        }
        if( t < thresh) {
	  break;
        }
        k1 = k1+1.0;
        k2 = k2+1.0;
        k3 = k3+2.0;
        k4 = k4+2.0;
        k5 = k5+1.0;
        k6 = k6-1.0;
        k7 = k7+2.0;
        k8 = k8+2.0;
        if( (mathLib_float_abs(qk)+mathLib_float_abs(pk)  > big) ) {
	  pkm2 = pkm2*biginv;
	  pkm1 = pkm1*biginv;
	  qkm2 = qkm2*biginv;
	  qkm1 = qkm1*biginv;
        }
        if( (mathLib_float_abs(qk),biginv) || (mathLib_float_abs(pk) < biginv) )       {
	  pkm2 = pkm2*big;
	  pkm1 = pkm1*big;
	  qkm2 = qkm2*big;
	  qkm1 = qkm1*big;
        }
        n = n+1;
      }
    while(n != 300);
    result = ans;
    return result;
}

/*************************************************************************
Continued fraction expansion #2
for incomplete beta integral

Cephes Math Library, Release 2.8:  June, 2000
Copyright 1984, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
static double ibetaf_incompletebetafe2(double a,
     double b,
     double x,
     double big,
     double biginv)
{
    double xk;
    double pk;
    double pkm1;
    double pkm2;
    double qk;
    double qkm1;
    double qkm2;
    double k1;
    double k2;
    double k3;
    double k4;
    double k5;
    double k6;
    double k7;
    double k8;
    double r;
    double t;
    double ans;
    double z;
    double thresh;
    ae_int_t n;
    double result;


  assert(false); // FIXME (a) udpate the java-doc [above] and (b) make 'this' accessible from the heade-rfile ... and (c) update our "matrix_deviation.c" wrt. "t-test"

    k1 = a;
    k2 = b-1.0;
    k3 = a;
    k4 = a+1.0;
    k5 = 1.0;
    k6 = a+b;
    k7 = a+1.0;
    k8 = a+2.0;
    pkm2 = 0.0;
    qkm2 = 1.0;
    pkm1 = 1.0;
    qkm1 = 1.0;
    z = x/(1.0-x);
    ans = 1.0;
    r = 1.0;
    n = 0;
    thresh = 3.0*ae_machineepsilon;
    do {
        xk = -z*k1*k2/(k3*k4);
        pk = pkm1+pkm2*xk;
        qk = qkm1+qkm2*xk;
        pkm2 = pkm1;
        pkm1 = pk;
        qkm2 = qkm1;
        qkm1 = qk;
        xk = z*k5*k6/(k7*k8);
        pk = pkm1+pkm2*xk;
        qk = qkm1+qkm2*xk;
        pkm2 = pkm1;
        pkm1 = pk;
        qkm2 = qkm1;
        qkm1 = qk;
        if( ae_fp_neq(qk,(double)(0)) ) {
            r = pk/qk;
        }
        if( ae_fp_neq(r,(double)(0)) )  {
            t = ae_fabs((ans-r)/r);
            ans = r;
        } else {
            t = 1.0;
        }
        if( ae_fp_less(t,thresh) ) {
            break;
        }
        k1 = k1+1.0;
        k2 = k2-1.0;
        k3 = k3+2.0;
        k4 = k4+2.0;
        k5 = k5+1.0;
        k6 = k6+1.0;
        k7 = k7+2.0;
        k8 = k8+2.0;
        if( ae_fp_greater(ae_fabs(qk)+ae_fabs(pk),big) )  {
            pkm2 = pkm2*biginv;
            pkm1 = pkm1*biginv;
            qkm2 = qkm2*biginv;
            qkm1 = qkm1*biginv;
        }
        if( ae_fp_less(ae_fabs(qk),biginv)||ae_fp_less(ae_fabs(pk),biginv) )
        {
            pkm2 = pkm2*big;
            pkm1 = pkm1*big;
            qkm2 = qkm2*big;
            qkm1 = qkm1*big;
        }
        n = n+1;
    }
    while(n!=300);
    result = ans;
    return result;
}


static t_float gammafunc_gammastirf(t_float x) {
    t_float y;
    t_float w;
    t_float v;
    t_float stir;
    t_float result;

    w = 1/x;
    stir = 7.87311395793093628397E-4;
    stir = -2.29549961613378126380E-4+w*stir;
    stir = -2.68132617805781232825E-3+w*stir;
    stir = 3.47222221605458667310E-3+w*stir;
    stir = 8.33333333333482257126E-2+w*stir;
    w = 1+w*stir;
    y = ae_exp(x);
    if( ae_fp_greater(x,143.01608) )    {
        v = ae_pow(x, 0.5*x-0.25);
        y = v*(v/y);
    }     else     {
        y = ae_pow(x, x-0.5)/y;
    }
    result = 2.50662827463100050242*y*w;
    return result;
}













/*************************************************************************
Natural logarithm of gamma function

Input parameters:
    X       -   argument

Result:
    logarithm of the absolute value of the Gamma(X).

Output parameters:
    SgnGam  -   sign(Gamma(X))

Domain:
    0 < X < 2.55e305
    -2.55e305 < X < 0, X is not an integer.

ACCURACY:
arithmetic      domain        # trials     peak         rms
   IEEE    0, 3                 28000     5.4e-16     1.1e-16
   IEEE    2.718, 2.556e305     40000     3.5e-16     8.3e-17
The error criterion was relative when the function magnitude
was greater than one but absolute when it was less than one.

The following test used the relative error criterion, though
at certain points the relative error could be much higher than
indicated.
   IEEE    -200, -4             10000     4.8e-16     1.3e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
Translated to AlgoPascal by Bochkanov Sergey (2005, 2006, 2007).
*************************************************************************/
t_float lngamma(t_float x, t_float* sgngam) {
    t_float a;
    t_float b;
    t_float c;
    t_float p;
    t_float q;
    t_float u;
    t_float w;
    t_float z;
    ae_int_t i;
    t_float logpi;
    t_float ls2pi;
    t_float tmp;
    t_float result;

    *sgngam = 0;

    *sgngam = (t_float)(1);
    logpi = 1.14472988584940017414;
    ls2pi = 0.91893853320467274178;
    if( ae_fp_less(x , -34.0) )
    {
        q = -x;
        w = lngamma(q, &tmp);
        p = (t_float)(ae_ifloor(q));
        i = ae_round(p);
        if( i%2==0 )
        {
            *sgngam = (t_float)(-1);
        }
        else
        {
            *sgngam = (t_float)(1);
        }
        z = q-p;
        if( ae_fp_greater(z,0.5) )
        {
            p = p+1;
            z = p-q;
        }
        z = q*ae_sin(ae_pi*z);
        result = logpi - ae_log(z)-w;
        return result;
    }
    if( ae_fp_less(x, (t_float)(13)) )
    {
        z = (t_float)(1);
        p = (t_float)(0);
        u = x;
        while(ae_fp_greater_eq(u,(t_float)(3)))
        {
            p = p-1;
            u = x+p;
            z = z*u;
        }
        while(ae_fp_less(u,(t_float)(2)))
        {
            z = z/u;
            p = p+1;
            u = x+p;
        }
        if( ae_fp_less(z,(t_float)(0)) )
        {
            *sgngam = (t_float)(-1);
            z = -z;
        }
        else
        {
            *sgngam = (t_float)(1);
        }
        if( ae_fp_eq(u,(t_float)(2)) )
        {
            result = ae_log(z);
            return result;
        }
        p = p-2;
        x = x+p;
        b = -1378.25152569120859100;
        b = -38801.6315134637840924+x*b;
        b = -331612.992738871184744+x*b;
        b = -1162370.97492762307383+x*b;
        b = -1721737.00820839662146+x*b;
        b = -853555.664245765465627+x*b;
        c = (t_float)(1);
        c = -351.815701436523470549+x*c;
        c = -17064.2106651881159223+x*c;
        c = -220528.590553854454839+x*c;
        c = -1139334.44367982507207+x*c;
        c = -2532523.07177582951285+x*c;
        c = -2018891.41433532773231+x*c;
        p = x*b/c;
        result = ae_log(z)+p;
        return result;
    }
    q = (x-0.5)*ae_log(x)-x+ls2pi;
    if( ae_fp_greater(x,(t_float)(100000000)) )
    {
        result = q;
        return result;
    }
    p = 1/(x*x);
    if( ae_fp_greater_eq(x,1000.0) )
    {
        q = q+((7.9365079365079365079365*0.0001*p-2.7777777777777777777778*0.001)*p+0.0833333333333333333333)/x;
    }
    else
    {
        a = 8.11614167470508450300*0.0001;
        a = -5.95061904284301438324*0.0001+p*a;
        a = 7.93650340457716943945*0.0001+p*a;
        a = -2.77777777730099687205*0.001+p*a;
        a = 8.33333333333331927722*0.01+p*a;
        q = q+a/x;
    }
    result = q;
    return result;
}





/*************************************************************************
Gamma function

Input parameters:
    X   -   argument

Domain:
    0 < X < 171.6
    -170 < X < 0, X is not an integer.

Relative error:
 arithmetic   domain     # trials      peak         rms
    IEEE    -170,-33      20000       2.3e-15     3.3e-16
    IEEE     -33,  33     20000       9.4e-16     2.2e-16
    IEEE      33, 171.6   20000       2.3e-15     3.2e-16

Cephes Math Library Release 2.8:  June, 2000
Original copyright 1984, 1987, 1989, 1992, 2000 by Stephen L. Moshier
Translated to AlgoPascal by Bochkanov Sergey (2005, 2006, 2007).
*************************************************************************/
t_float gammafunction(t_float x)
{
    t_float p;
    t_float pp;
    t_float q;
    t_float qq;
    t_float z;
    ae_int_t i;
    t_float sgngam;
    t_float result;

    //! Note[time]: while teh "gammafunc_gammastirf(..)


    sgngam = (t_float)(1);
    q = mathLib_float_abs(x);
    if( ae_fp_greater(q,33.0) )
    {
      if( ae_fp_less(x , 0.0) )
        {
            p = (t_float)(ae_ifloor(q));
            i = ae_round(p);
            if( i%2==0 )
            {
                sgngam = (t_float)(-1);
            }
            z = q-p;
            if( ae_fp_greater(z,0.5) )
            {
                p = p+1;
                z = q-p;
            }
            z = q*ae_sin(ae_pi*z);
            z = mathLib_float_abs(z);
	    
            z = ae_pi/(z*gammafunc_gammastirf(q));
        }
        else
        {
            z = gammafunc_gammastirf(x);
        }
        result = sgngam*z;
        return result;
    }
    z = (t_float)(1);
    while(ae_fp_greater_eq(x,(t_float)(3)))
    {
        x = x-1;
        z = z*x;
    }
    while(ae_fp_less(x , (t_float)(0)))
    {
        if( ae_fp_greater(x,-0.000000001) )
        {
            result = z/((1+0.5772156649015329*x)*x);
            return result;
        }
        z = z/x;
        x = x+1;
    }
    while(ae_fp_less(x , (t_float)(2)))
    {
      if( ae_fp_less(x , 0.000000001) )
        {
            result = z/((1+0.5772156649015329*x)*x);
            return result;
        }
        z = z/x;
        x = x+1.0;
    }
    if( ae_fp_eq(x,(t_float)(2)) )
    {
        result = z;
        return result;
    }
    x = x-2.0;
    pp = 1.60119522476751861407E-4;
    pp = 1.19135147006586384913E-3+x*pp;
    pp = 1.04213797561761569935E-2+x*pp;
    pp = 4.76367800457137231464E-2+x*pp;
    pp = 2.07448227648435975150E-1+x*pp;
    pp = 4.94214826801497100753E-1+x*pp;
    pp = 9.99999999999999996796E-1+x*pp;
    qq = -2.31581873324120129819E-5;
    qq = 5.39605580493303397842E-4+x*qq;
    qq = -4.45641913851797240494E-3+x*qq;
    qq = 1.18139785222060435552E-2+x*qq;
    qq = 3.58236398605498653373E-2+x*qq;
    qq = -2.34591795718243348568E-1+x*qq;
    qq = 7.14304917030273074085E-2+x*qq;
    qq = 1.00000000000000000320+x*qq;
    result = z*pp/qq;
    return result;

}

/*************************************************************************
Power series for incomplete beta integral.
Use when b*x is small and x not too close to 1.

Cephes Math Library, Release 2.8:  June, 2000
Copyright 1984, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
static t_float ibetaf_incompletebetaps(t_float a,
     t_float b,
     t_float x,
     t_float maxgam)
{
    t_float s;
    t_float t;
    t_float u;
    t_float v;
    t_float n;
    t_float t1;
    t_float z;
    t_float ai;
    t_float sg;
    t_float result;


    ai = 1.0/a;
    u = (1.0-b)*x;
    v = u/(a+1.0);
    t1 = v;
    t = u;
    n = 2.0;
    s = 0.0;
    z = T_FLOAT_MIN *ai;
    while((mathLib_float_abs(v) > z))     {
        u = (n-b)*x/n;
        t = t*u;
        v = t/(a+n);
        s = s+v;
        n = n+1.0;
    }
    s = s+t1;
    s = s+ai;
    u = a*mathLib_float_log(x);
    if( (a+b < maxgam) && (mathLib_float_abs(u) <  mathLib_float_log(ae_maxrealnumber)) )  {
        t = gammafunction(a+b)/(gammafunction(a)*gammafunction(b));
        s = s*t*mathLib_float_pow(x, a);
    } else {
      t = lngamma(a+b, &sg)-lngamma(a, &sg) - lngamma(b, &sg) +u + mathLib_float_log(s);
      if( ae_fp_less(t , ae_log(ae_minrealnumber)) ) {
	s = 0.0;
      } else {
	s = mathLib_float_exp(t);
      }
    }
    result = s;
    return result;
}



/*************************************************************************
Incomplete beta integral

Returns incomplete beta integral of the arguments, evaluated
from zero to x.  The function is defined as

                 x
    -            -
   | (a+b)      | |  a-1     b-1
 -----------    |   t   (1-t)   dt.
  -     -     | |
 | (a) | (b)   -
                0

The domain of definition is 0 <= x <= 1.  In this
implementation a and b are restricted to positive values.
The integral from x to 1 may be obtained by the symmetry
relation

   1 - incbet( a, b, x )  =  incbet( b, a, 1-x ).

The integral is evaluated by a continued fraction expansion
or, when b*x is small, by a power series.

ACCURACY:

Tested at uniformly distributed random points (a,b,x) with a and b
in "domain" and x between 0 and 1.
                                       Relative error
arithmetic   domain     # trials      peak         rms
   IEEE      0,5         10000       6.9e-15     4.5e-16
   IEEE      0,85       250000       2.2e-13     1.7e-14
   IEEE      0,1000      30000       5.3e-12     6.3e-13
   IEEE      0,10000    250000       9.3e-11     7.1e-12
   IEEE      0,100000    10000       8.7e-10     4.8e-11
Outputs smaller than the IEEE gradual underflow threshold
were excluded from these statistics.

Cephes Math Library, Release 2.8:  June, 2000
Copyright 1984, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float incompletebeta(t_float a, t_float b, t_float x) {
  t_float t;
  t_float xc;
  t_float w;
  t_float y;
  ae_int_t flag;
  t_float sg;
  t_float big;
  t_float biginv;
  t_float maxgam;
  t_float minlog;
  t_float maxlog;
  t_float result;


  big = T_FLOAT_MAX; //4.503599627370496e15;
  biginv = T_FLOAT_MIN; //2.22044604925031308085e-16;
  maxgam = 171.624376956302725;
  minlog = mathLib_float_log(T_FLOAT_MIN); //ae_log(ae_minrealnumber);
  maxlog = mathLib_float_log(T_FLOAT_MAX); //ae_log(ae_maxrealnumber);
  /* ae_assert(ae_fp_greater(a,(t_float)(0))&&ae_fp_greater(b,(t_float)(0)), "Domain error in IncompleteBeta"); */
  /* ae_assert(ae_fp_greater_eq(x,(t_float)(0))&&ae_fp_less_eq(x,(t_float)(1)), "Domain error in IncompleteBeta"); */


  if(x == 0) {
    result = (t_float)(0);
    return result;
  }
  if(x == 1) {
    result = (t_float)(1);
    return result;
  }

  flag = 0;
  if( (b*x <= 1.0) && (x <= 0.95) ) {
    result = ibetaf_incompletebetaps(a, b, x, maxgam);
    return result;
  }
  w = 1.0-x;
  if(x > a/(a+b) ) {
    flag = 1;
    t = a;
    a = b;
    b = t;
    xc = x;
    x = w;
  } else {
    xc = w;
  }
  if( (flag==1 && (b*x <= 1.0)) && (x <= 0.95) ) {
    t = ibetaf_incompletebetaps(a, b, x, maxgam);
    if( (t <= T_FLOAT_MIN) ) {
      result = 1.0- T_FLOAT_MIN;
    } else {
      result = 1.0-t;
    }
    return result;
  }

  y = x*(a+b-2.0)-(a-1.0);
  if( ae_fp_less(y,0.0) ) {
    w = ibetaf_incompletebetafe(a, b, x, big, biginv);
  } else {
    w = ibetaf_incompletebetafe2(a, b, x, big, biginv)/xc;
  }
  y = a*ae_log(x);
  t = b*ae_log(xc);
  if( ((a+b < maxgam) && (mathLib_float_abs(y) < maxlog)) && (mathLib_float_abs(t) < maxlog) ) {
    t = mathLib_float_pow(xc, b);
    t = t*mathLib_float_pow(x, a);
    t = t/a;
    t = t*w;
    t = t*(gammafunction(a+b)/(gammafunction(a)*gammafunction(b)));
    if( flag==1 ) {
      if( (t < T_FLOAT_MIN) ) {
	result = 1.0 - T_FLOAT_MIN; 
      } else {
	result = 1.0-t;
      }
    }
    else {
      result = t;
    }
    return result;
  }
  y = y+t+lngamma(a+b, &sg)-lngamma(a, &sg)-lngamma(b, &sg);
  y = y + mathLib_float_log(w/a);
  if( (y < minlog) ) {
    t = 0.0;
  } else {
    t = mathLib_float_exp(y);
  }
  if( flag==1 ) {
    if( (t <= T_FLOAT_MIN) ) {
      t = 1.0 - T_FLOAT_MIN;
    } else {
      t = 1.0-t;
    }
  }
  result = t;
  return result;
}








//! ---------------------------------------------

/*************************************************************************
Inverse of Normal distribution function

Returns the argument, x, for which the area under the
Gaussian probability density function (integrated from
minus infinity to x) is equal to y.


For small arguments 0 < y < exp(-2), the program computes
z = sqrt( -2.0 * log(y) );  then the approximation is
x = z - log(z)/z  - (1/z) P(1/z) / Q(1/z).
There are two rational functions P/Q, one for 0 < y < exp(-32)
and the other for y up to exp(-2).  For larger arguments,
w = y - 0.5, and  x/sqrt(2pi) = w + w**3 R(w**2)/S(w**2)).

ACCURACY:

                     Relative error:
arithmetic   domain        # trials      peak         rms
   IEEE     0.125, 1        20000       7.2e-16     1.3e-16
   IEEE     3e-308, 0.135   50000       4.6e-16     9.8e-17

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1988, 1992, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invnormaldistribution(t_float y0) {
    t_float expm2;
    t_float s2pi;
    t_float x;
    t_float y;
    t_float z;
    t_float y2;
    t_float x0;
    t_float x1;
    ae_int_t code;
    t_float p0;
    t_float q0;
    t_float p1;
    t_float q1;
    t_float p2;
    t_float q2;
    t_float result;


    expm2 = 0.13533528323661269189;
    s2pi = 2.50662827463100050242;
    if( ae_fp_less_eq(y0,(t_float)(0)) )
    {
        result = -ae_maxrealnumber;
        return result;
    }
    if( ae_fp_greater_eq(y0,(t_float)(1)) )
    {
        result = ae_maxrealnumber;
        return result;
    }
    code = 1;
    y = y0;
    if( ae_fp_greater(y,1.0-expm2) )
    {
        y = 1.0-y;
        code = 0;
    }
    if( ae_fp_greater(y,expm2) )
    {
        y = y-0.5;
        y2 = y*y;
        p0 = -59.9633501014107895267;
        p0 = 98.0010754185999661536+y2*p0;
        p0 = -56.6762857469070293439+y2*p0;
        p0 = 13.9312609387279679503+y2*p0;
        p0 = -1.23916583867381258016+y2*p0;
        q0 = (t_float)(1);
        q0 = 1.95448858338141759834+y2*q0;
        q0 = 4.67627912898881538453+y2*q0;
        q0 = 86.3602421390890590575+y2*q0;
        q0 = -225.462687854119370527+y2*q0;
        q0 = 200.260212380060660359+y2*q0;
        q0 = -82.0372256168333339912+y2*q0;
        q0 = 15.9056225126211695515+y2*q0;
        q0 = -1.18331621121330003142+y2*q0;
        x = y+y*y2*p0/q0;
        x = x*s2pi;
        result = x;
        return result;
    }
    x = mathLib_float_sqrt(-2.0*ae_log(y));
    x0 = x-ae_log(x)/x;
    z = 1.0/x;
    if( ae_fp_less(x,8.0) )
    {
        p1 = 4.05544892305962419923;
        p1 = 31.5251094599893866154+z*p1;
        p1 = 57.1628192246421288162+z*p1;
        p1 = 44.0805073893200834700+z*p1;
        p1 = 14.6849561928858024014+z*p1;
        p1 = 2.18663306850790267539+z*p1;
        p1 = -1.40256079171354495875*0.1+z*p1;
        p1 = -3.50424626827848203418*0.01+z*p1;
        p1 = -8.57456785154685413611*0.0001+z*p1;
        q1 = (t_float)(1);
        q1 = 15.7799883256466749731+z*q1;
        q1 = 45.3907635128879210584+z*q1;
        q1 = 41.3172038254672030440+z*q1;
        q1 = 15.0425385692907503408+z*q1;
        q1 = 2.50464946208309415979+z*q1;
        q1 = -1.42182922854787788574*0.1+z*q1;
        q1 = -3.80806407691578277194*0.01+z*q1;
        q1 = -9.33259480895457427372*0.0001+z*q1;
        x1 = z*p1/q1;
    }
    else
    {
        p2 = 3.23774891776946035970;
        p2 = 6.91522889068984211695+z*p2;
        p2 = 3.93881025292474443415+z*p2;
        p2 = 1.33303460815807542389+z*p2;
        p2 = 2.01485389549179081538*0.1+z*p2;
        p2 = 1.23716634817820021358*0.01+z*p2;
        p2 = 3.01581553508235416007*0.0001+z*p2;
        p2 = 2.65806974686737550832*0.000001+z*p2;
        p2 = 6.23974539184983293730*0.000000001+z*p2;
        q2 = (t_float)(1);
        q2 = 6.02427039364742014255+z*q2;
        q2 = 3.67983563856160859403+z*q2;
        q2 = 1.37702099489081330271+z*q2;
        q2 = 2.16236993594496635890*0.1+z*q2;
        q2 = 1.34204006088543189037*0.01+z*q2;
        q2 = 3.28014464682127739104*0.0001+z*q2;
        q2 = 2.89247864745380683936*0.000001+z*q2;
        q2 = 6.79019408009981274425*0.000000001+z*q2;
        x1 = z*p2/q2;
    }
    x = x0-x1;
    if( code!=0 )
    {
        x = -x;
    }
    result = x;
    return result;
}







/*************************************************************************
Inverse of imcomplete beta integral

Given y, the function finds x such that

 incbet( a, b, x ) = y .

The routine performs interval halving or Newton iterations to find the
root of incbet(a,b,x) - y = 0.


ACCURACY:

                     Relative error:
               x     a,b
arithmetic   domain  domain  # trials    peak       rms
   IEEE      0,1    .5,10000   50000    5.8e-12   1.3e-13
   IEEE      0,1   .25,100    100000    1.8e-13   3.9e-15
   IEEE      0,1     0,5       50000    1.1e-12   5.5e-15
With a and b constrained to half-integer or integer values:
   IEEE      0,1    .5,10000   50000    5.8e-12   1.1e-13
   IEEE      0,1    .5,100    100000    1.7e-14   7.9e-16
With a = .5, b constrained to half-integer or integer values:
   IEEE      0,1    .5,10000   10000    8.3e-11   1.0e-11

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1996, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invincompletebeta(t_float a, t_float b, t_float y)
{
    t_float aaa;
    t_float bbb;
    t_float y0;
    t_float d;
    t_float yyy;
    t_float x;
    t_float x0;
    t_float x1;
    t_float lgm;
    t_float yp;
    t_float di;
    t_float dithresh;
    t_float yl;
    t_float yh;
    t_float xt;
    ae_int_t i;
    ae_int_t rflg;
    ae_int_t dir;
    ae_int_t nflg;
    t_float s;
    ae_int_t mainlooppos;
    ae_int_t ihalve;
    ae_int_t ihalvecycle;
    ae_int_t newt;
    ae_int_t newtcycle;
    ae_int_t breaknewtcycle;
    ae_int_t breakihalvecycle;
    t_float result;


    i = 0;
    // ae_assert(ae_fp_greater_eq(y,(t_float)(0))&&ae_fp_less_eq(y,(t_float)(1)), "Domain error in InvIncompleteBeta");
    
    /*
     * special cases
     */
    if( ae_fp_eq(y,(t_float)(0)) )
    {
        result = (t_float)(0);
        return result;
    }
    if( ae_fp_eq(y,1.0) )
    {
        result = (t_float)(1);
        return result;
    }
    
    /*
     * these initializations are not really necessary,
     * but without them compiler complains about 'possibly uninitialized variables'.
     */
    dithresh = (t_float)(0);
    rflg = 0;
    aaa = (t_float)(0);
    bbb = (t_float)(0);
    y0 = (t_float)(0);
    x = (t_float)(0);
    yyy = (t_float)(0);
    lgm = (t_float)(0);
    dir = 0;
    di = (t_float)(0);
    
    /*
     * normal initializations
     */
    x0 = 0.0;
    yl = 0.0;
    x1 = 1.0;
    yh = 1.0;
    nflg = 0;
    mainlooppos = 0;
    ihalve = 1;
    ihalvecycle = 2;
    newt = 3;
    newtcycle = 4;
    breaknewtcycle = 5;
    breakihalvecycle = 6;
    
    /*
     * main loop
     */
    for(;;)
    {
        
        /*
         * start
         */
        if( mainlooppos==0 )
        {
            if( ae_fp_less_eq(a,1.0)||ae_fp_less_eq(b,1.0) )
            {
                dithresh = 1.0e-6;
                rflg = 0;
                aaa = a;
                bbb = b;
                y0 = y;
                x = aaa/(aaa+bbb);
                yyy = incompletebeta(aaa, bbb, x);
                mainlooppos = ihalve;
                continue;
            }
            else
            {
                dithresh = 1.0e-4;
            }
            yp = -invnormaldistribution(y);
            if( ae_fp_greater(y,0.5) )
            {
                rflg = 1;
                aaa = b;
                bbb = a;
                y0 = 1.0-y;
                yp = -yp;
            }
            else
            {
                rflg = 0;
                aaa = a;
                bbb = b;
                y0 = y;
            }
            lgm = (yp*yp-3.0)/6.0;
            x = 2.0/(1.0/(2.0*aaa-1.0)+1.0/(2.0*bbb-1.0));
            d = yp*mathLib_float_sqrt(x+lgm)/x-(1.0/(2.0*bbb-1.0)-1.0/(2.0*aaa-1.0))*(lgm+5.0/6.0-2.0/(3.0*x));
            d = 2.0*d;
            if( ae_fp_less(d,ae_log(ae_minrealnumber)) )
            {
                x = (t_float)(0);
                break;
            }
            x = aaa/(aaa+bbb*ae_exp(d));
            yyy = incompletebeta(aaa, bbb, x);
            yp = (yyy-y0)/y0;
            if( ae_fp_less(mathLib_float_abs(yp),0.2) )
            {
                mainlooppos = newt;
                continue;
            }
            mainlooppos = ihalve;
            continue;
        }
        
        /*
         * ihalve
         */
        if( mainlooppos==ihalve )
        {
            dir = 0;
            di = 0.5;
            i = 0;
            mainlooppos = ihalvecycle;
            continue;
        }
        
        /*
         * ihalvecycle
         */
        if( mainlooppos==ihalvecycle )
        {
            if( i<=99 )
            {
                if( i!=0 )
                {
                    x = x0+di*(x1-x0);
                    if( ae_fp_eq(x,1.0) )
                    {
                        x = 1.0-T_FLOAT_MIN;
                    }
                    if( ae_fp_eq(x,0.0) )
                    {
                        di = 0.5;
                        x = x0+di*(x1-x0);
                        if( ae_fp_eq(x,0.0) )
                        {
                            break;
                        }
                    }
                    yyy = incompletebeta(aaa, bbb, x);
                    yp = (x1-x0)/(x1+x0);
                    if( ae_fp_less(mathLib_float_abs(yp),dithresh) )
                    {
                        mainlooppos = newt;
                        continue;
                    }
                    yp = (yyy-y0)/y0;
                    if( ae_fp_less(mathLib_float_abs(yp),dithresh) )
                    {
                        mainlooppos = newt;
                        continue;
                    }
                }
                if( ae_fp_less(yyy,y0) )
                {
                    x0 = x;
                    yl = yyy;
                    if( dir<0 )
                    {
                        dir = 0;
                        di = 0.5;
                    }
                    else
                    {
                        if( dir>3 )
                        {
                            di = 1.0-(1.0-di)*(1.0-di);
                        }
                        else
                        {
                            if( dir>1 )
                            {
                                di = 0.5*di+0.5;
                            }
                            else
                            {
                                di = (y0-yyy)/(yh-yl);
                            }
                        }
                    }
                    dir = dir+1;
                    if( ae_fp_greater(x0,0.75) )
                    {
                        if( rflg==1 )
                        {
                            rflg = 0;
                            aaa = a;
                            bbb = b;
                            y0 = y;
                        }
                        else
                        {
                            rflg = 1;
                            aaa = b;
                            bbb = a;
                            y0 = 1.0-y;
                        }
                        x = 1.0-x;
                        yyy = incompletebeta(aaa, bbb, x);
                        x0 = 0.0;
                        yl = 0.0;
                        x1 = 1.0;
                        yh = 1.0;
                        mainlooppos = ihalve;
                        continue;
                    }
                }
                else
                {
                    x1 = x;
                    if( rflg==1&&ae_fp_less(x1,T_FLOAT_MIN) )
                    {
                        x = 0.0;
                        break;
                    }
                    yh = yyy;
                    if( dir>0 )
                    {
                        dir = 0;
                        di = 0.5;
                    }
                    else
                    {
                        if( dir<-3 )
                        {
                            di = di*di;
                        }
                        else
                        {
                            if( dir<-1 )
                            {
                                di = 0.5*di;
                            }
                            else
                            {
                                di = (yyy-y0)/(yh-yl);
                            }
                        }
                    }
                    dir = dir-1;
                }
                i = i+1;
                mainlooppos = ihalvecycle;
                continue;
            }
            else
            {
                mainlooppos = breakihalvecycle;
                continue;
            }
        }
        
        /*
         * breakihalvecycle
         */
        if( mainlooppos==breakihalvecycle )
        {
            if( ae_fp_greater_eq(x0,1.0) )
            {
                x = 1.0-T_FLOAT_MIN;
                break;
            }
            if( ae_fp_less_eq(x,0.0) )
            {
                x = 0.0;
                break;
            }
            mainlooppos = newt;
            continue;
        }
        
        /*
         * newt
         */
        if( mainlooppos==newt )
        {
            if( nflg!=0 )
            {
                break;
            }
            nflg = 1;
            lgm = lngamma(aaa+bbb, &s)-lngamma(aaa, &s)-lngamma(bbb, &s);
            i = 0;
            mainlooppos = newtcycle;
            continue;
        }
        
        /*
         * newtcycle
         */
        if( mainlooppos==newtcycle )
        {
            if( i<=7 )
            {
                if( i!=0 )
                {
                    yyy = incompletebeta(aaa, bbb, x);
                }
                if( ae_fp_less(yyy,yl) )
                {
                    x = x0;
                    yyy = yl;
                }
                else
                {
                    if( ae_fp_greater(yyy,yh) )
                    {
                        x = x1;
                        yyy = yh;
                    }
                    else
                    {
                        if( ae_fp_less(yyy,y0) )
                        {
                            x0 = x;
                            yl = yyy;
                        }
                        else
                        {
                            x1 = x;
                            yh = yyy;
                        }
                    }
                }
                if( ae_fp_eq(x,1.0)||ae_fp_eq(x,0.0) )
                {
                    mainlooppos = breaknewtcycle;
                    continue;
                }
                d = (aaa-1.0)*ae_log(x)+(bbb-1.0)*ae_log(1.0-x)+lgm;
                if( ae_fp_less(d,ae_log(ae_minrealnumber)) )
                {
                    break;
                }
                if( ae_fp_greater(d,ae_log(ae_maxrealnumber)) )
                {
                    mainlooppos = breaknewtcycle;
                    continue;
                }
                d = ae_exp(d);
                d = (yyy-y0)/d;
                xt = x-d;
                if( ae_fp_less_eq(xt,x0) )
                {
                    yyy = (x-x0)/(x1-x0);
                    xt = x0+0.5*yyy*(x-x0);
                    if( ae_fp_less_eq(xt,0.0) )
                    {
                        mainlooppos = breaknewtcycle;
                        continue;
                    }
                }
                if( ae_fp_greater_eq(xt,x1) )
                {
                    yyy = (x1-x)/(x1-x0);
                    xt = x1-0.5*yyy*(x1-x);
                    if( ae_fp_greater_eq(xt,1.0) )
                    {
                        mainlooppos = breaknewtcycle;
                        continue;
                    }
                }
                x = xt;
                if( ae_fp_less(mathLib_float_abs(d/x),128.0*T_FLOAT_MIN) )
                {
                    break;
                }
                i = i+1;
                mainlooppos = newtcycle;
                continue;
            }
            else
            {
                mainlooppos = breaknewtcycle;
                continue;
            }
        }
        
        /*
         * breaknewtcycle
         */
        if( mainlooppos==breaknewtcycle )
        {
            dithresh = 256.0*T_FLOAT_MIN;
            mainlooppos = ihalve;
            continue;
        }
    }
    
    /*
     * done
     */
    if( rflg!=0 )
    {
        if( ae_fp_less_eq(x,T_FLOAT_MIN) )
        {
            x = 1.0-T_FLOAT_MIN;
        }
        else
        {
            x = 1.0-x;
        }
    }
    result = x;
    return result;
}


