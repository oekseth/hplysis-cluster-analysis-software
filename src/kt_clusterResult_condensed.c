#include "kt_clusterResult_condensed.h"
/**
   @brief intiates an object to empty, and object used to provide an overview of clsuteirng-results (oekseth, 06. arp. 2017).
   @remarks the object is (moang others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
 **/
void initSetToEmpty__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self) {
  assert(self);
  self->arrOf_clusterRelationships = setToEmpty__s_kt_list_1d_pairFloat_t();
  self->arr_names_clustering = setToEmpty_andReturn__s_kt_list_1d_string_t();
  self->arr_vertexTo_centroid = init__s_kt_list_1d_uint_t(0);
  self->arr_hcaResult_distanceFromRoot = init__s_kt_list_1d_float_t(0);
  self->arr_clusterSets_child_countOfScoreSum = init__s_kt_list_1d_float_t(0);
  self->arr_clusterSets_parent_countOfScoreSum = init__s_kt_list_1d_float_t(0);
  //self. = init__s_kt_list_1d_float_t();
  //! @return 
}

/**
   @brief intiates an object to empty, and object used to provide an overview of clsuteirng-results (oekseth, 06. arp. 2017).
   @return an itnalized object of the s_kt_clusterResult_condensed_t type
   @remarks the object is (moang others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
 **/
s_kt_clusterResult_condensed_t initAndReturn__s_kt_clusterResult_condensed_t() {
  s_kt_clusterResult_condensed_t self;
  initSetToEmpty__s_kt_clusterResult_condensed_t(&self);
  //! @return 
  return self;
}

//! Construct a new-allocates s_kt_clusterResult_condensed_t cluster-object (oekseth, 06. apr. 2017).
void init__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self, const uint nrows, const uint max_cntClusters) {
  assert(self);
  assert(nrows > 0); assert(max_cntClusters); //! as the call is otheriwse pointelss.
  assert(nrows != UINT_MAX); assert(max_cntClusters != UINT_MAX);
  const uint cnt_total = nrows + max_cntClusters;
  self->arrOf_clusterRelationships = init__s_kt_list_1d_pairFloat_t(cnt_total);
  self->arr_names_clustering = init_andReturn__s_kt_list_1d_string_t(cnt_total);
  self->arr_vertexTo_centroid = init__s_kt_list_1d_uint_t(cnt_total);
  self->arr_hcaResult_distanceFromRoot = init__s_kt_list_1d_float_t(cnt_total);
  self->arr_clusterSets_child_countOfScoreSum = init__s_kt_list_1d_float_t(cnt_total);
  self->arr_clusterSets_parent_countOfScoreSum = init__s_kt_list_1d_float_t(cnt_total);
  //!
  //! Set 'start-scores' for each vertex:
  for(uint i = 0; i < cnt_total; i++) {
    //const t_float score = 1; //! ie, as we assume the 'count' starts with the vertex itself.
    //const uint index = 0;
    //set_scalar__s_kt_list_1d_pairFloat_t(&(self->arr_hcaResult_distanceFromRoot), index, score);
    const uint index  = i;
    set_scalar__s_kt_list_1d_float_t(&(self->arr_clusterSets_child_countOfScoreSum), index, /*score=*/0);
    set_scalar__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), index, /*score=*/0);
  }
}
//! @return a new-allocates s_kt_clusterResult_condensed_t cluster-object (oekseth, 06. apr. 2017).
s_kt_clusterResult_condensed_t init__s_kt_clusterResult_condensed_t(const uint nrows, const uint max_cntClusters) {
  s_kt_clusterResult_condensed_t self;
  assert(nrows > 0); assert(max_cntClusters); //! as the call is otheriwse pointelss.
  init__s_kt_clusterResult_condensed_t(&self, nrows, max_cntClusters);
  return self;
}

//! De-allcoates memory in an tianlseid s_kt_clusterResult_condensed_t object
void free__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self) {
  assert(self);
  free__s_kt_list_1d_pairFloat_t(&(self->arrOf_clusterRelationships));
  free__s_kt_list_1d_uint_t(&(self->arr_vertexTo_centroid));
  free__s_kt_list_1d_float_t(&(self->arr_hcaResult_distanceFromRoot));
  free__s_kt_list_1d_float_t(&(self->arr_clusterSets_child_countOfScoreSum));
  free__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum));
  // free__s_kt_list_1d_float_t(&());
}

//! Relate the head to the tail, and udpat ehte inverse relationship-mapping (oekseth, 06. apr. 2017).
void relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self, const uint head, const uint tail, const t_float score) {
  assert(self);
  assert(score != T_FLOAT_MAX);
  //! Add the relationship: 
  add_scalar__allVars__s_kt_list_1d_pairFloat_t(&(self->arrOf_clusterRelationships), head, tail, /*score=*/score);
  //! Set the 'direct mapping', ie, similar to [ªbove], where lgocis is inlcuded iot. 'simplify a merge with the HCA-logics'.
  assert(tail != UINT_MAX);
  printf("(arr_vertexTo_centroid)\t %u-->%u, at %s:%d\n", head, tail, __FILE__, __LINE__);
  set_scalar__s_kt_list_1d_uint_t(&(self->arr_vertexTo_centroid), head, /*value=*/tail);
  //! Increment the parent-count:
  increment_scalar__s_kt_list_1d_float_t(&(self->arr_clusterSets_child_countOfScoreSum), head, /*score=*/score);
  increment_scalar__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), tail, /*score=*/score);
}

#define __MiF__getString(index, str_fallBack) ({ const char *str = getAndReturn_string__s_kt_list_1d_string(&(self->arr_names_clustering), index); if(!str || !strlen(str)) {sprintf(str_fallBack, "vertex(%u)", index); str = str_fallBack;} str;})

bool export__toFormat__tsv__s_kt_clusterResult_condensed_t(const s_kt_clusterResult_condensed_t *self, const char *fileName_result, const char *seperator, const char *seperator_newLine) {
  assert(self);
  FILE *file_out = stdout;
  if(fileName_result && strlen(fileName_result)) {
    file_out = fopen(fileName_result, "wb");
    if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\" for writign, ie, pelase investigate both path and permtussions. OBservioant at [%s]:%s:%d\n", fileName_result, __FUNCTION__, __FILE__, __LINE__); return false;} //! ie, then an error in file-reading, ie, abort.
  }
  assert(file_out);
  if(!seperator || !strlen(seperator)) {seperator = "\t";} //! ie, our deuflat assumption.
  if(!seperator_newLine || !strlen(seperator_newLine)) {seperator_newLine = "\n";} //! ie, our deuflat assumption.
  //  #define __MiF__triplet

  //! Simplify fetching of strings:
#define __MiF__writeOut_varName(index) ({if(index == 0) { fprintf(file_out, "\n#! Data: %s\n", str_data);       }})
  //! Write out the 'terms' for a givne case:
#define __MiF__writeTerm__string(index, sep) ({ \
    if(index != UINT_MAX) {char str_fallBack[10000]; const char *str = __MiF__getString(index, str_fallBack); \
      fprintf(file_out, "%s%s", str, sep);				\
    } else {fprintf(file_out, "%s%s", "-", sep);} })
  //! Write out a float-value:
#define __MiF__writeTerm__index(value, sep) ({ \
    if(value != UINT_MAX) { fprintf(file_out, "%u%s", value, sep); } \
    else {fprintf(file_out, "%s%s", "-", sep);} })
  //! Write out a float-value:
#define __MiF__writeTerm__float(value, sep) ({ \
    if(value != T_FLOAT_MAX) { fprintf(file_out, "%f%s", value, sep); } \
    else {fprintf(file_out, "%s%s", "-", sep);} })
  //!
  //! Start the export-routine: 
  const char *str_data = "arrOf_clusterRelationships";
    uint cnt_ele = 0; get_count__s_kt_list_1d_pairFloat_t(&(self->arrOf_clusterRelationships), &cnt_ele);
  for(uint i = 0; i < cnt_ele; i++) {
    __MiF__writeOut_varName(i);
    //! Write out the triplet:
    //const uint head = self->arrOf_clusterRelationships.list[i].head;
    __MiF__writeTerm__string(self->arrOf_clusterRelationships.list[i].head, seperator);
    __MiF__writeTerm__string(self->arrOf_clusterRelationships.list[i].tail, seperator);
    __MiF__writeTerm__float(self->arrOf_clusterRelationships.list[i].score, seperator_newLine);
  }
  //! -----------------------
  { const char *str_data = "arr_vertexTo_centroid";  
    uint cnt_ele = 0; get_count__s_kt_list_1d_uint_t(&(self->arr_vertexTo_centroid), &cnt_ele);
    //const uint cnt_ele = self->arr_vertexTo_centroid.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_varName(i); //! ie, wirte out hhe java-script-var-naem for the first entry:
      //! Wirte out the index--valeu pair: 
      __MiF__writeTerm__string(i, seperator);
      __MiF__writeTerm__index(self->arr_vertexTo_centroid.list[i], seperator_newLine);
    }
  }
  //! -----------------------
  { const char *str_data = "arr_hcaResult_distanceFromRoot";  
    uint cnt_ele = 0; get_count__s_kt_list_1d_float_t(&(self->arr_hcaResult_distanceFromRoot), &cnt_ele);
    //const uint cnt_ele = self->arr_hcaResult_distanceFromRoot.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_varName(i); //! ie, wirte out hhe java-script-var-naem for the first entry:
      //! Wirte out the index--valeu pair: 
      __MiF__writeTerm__string(i, seperator);
      __MiF__writeTerm__float(self->arr_hcaResult_distanceFromRoot.list[i], seperator_newLine);
    }
  }
  //! -----------------------
  { const char *str_data = "arr_clusterSets_child_countOfScoreSum";  
    //! Note: in [below] we asusme taht 'only vlaues above 0' are of itnerest, ie, hence our 'none-use' of the default curreT_pos-apprapoch.
    uint cnt_ele = 0; //get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), &cnt_ele);    
    for(uint i = 0; i < self->arr_clusterSets_child_countOfScoreSum.list_size; i++) {
      const uint val = self->arr_clusterSets_child_countOfScoreSum.list[i];
      if( (val != UINT_MAX) && (val != 0) ) {cnt_ele = i + 1;} //! ie, the biggest index:
    }
    //uint cnt_ele = 0; get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_child_countOfScoreSum), &cnt_ele);
    //const uint cnt_ele = self->arr_clusterSets_child_countOfScoreSum.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_varName(i); //! ie, wirte out hhe java-script-var-naem for the first entry:
      //! Wirte out the index--valeu pair: 
      __MiF__writeTerm__string(i, seperator);
      __MiF__writeTerm__float(self->arr_clusterSets_child_countOfScoreSum.list[i], seperator_newLine);
    }
  }
  //! -----------------------
  { const char *str_data = "arr_clusterSets_parent_countOfScoreSum";  
    //const uint cnt_ele = self->arr_clusterSets_parent_countOfScoreSum.list_size;
    //! Note: in [below] we asusme taht 'only vlaues above 0' are of itnerest, ie, hence our 'none-use' of the default curreT_pos-apprapoch.
    uint cnt_ele = 0; //get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), &cnt_ele);    
    for(uint i = 0; i < self->arr_clusterSets_parent_countOfScoreSum.list_size; i++) {
      const uint val = self->arr_clusterSets_parent_countOfScoreSum.list[i];
      if( (val != UINT_MAX) && (val != 0) ) {cnt_ele = i + 1;} //! ie, the biggest index:
    }
    //uint cnt_ele = 0; get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), &cnt_ele);
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_varName(i); //! ie, wirte out hhe java-script-var-naem for the first entry:
      //! Wirte out the index--valeu pair: 
      __MiF__writeTerm__string(i, seperator);
      __MiF__writeTerm__float(self->arr_clusterSets_parent_countOfScoreSum.list[i], seperator_newLine);
    }
  }

  
  //! 
  //! Close file-handler, and return:
  if(file_out != stdout) { fclose(file_out);  }   
  return true;
}

bool export__toFormat__javaScript__s_kt_clusterResult_condensed_t(const s_kt_clusterResult_condensed_t *self, const char *fileName_result) {
  assert(self);
  FILE *file_out = stdout;
  if(fileName_result && strlen(fileName_result)) {
    file_out = fopen(fileName_result, "wb");
    if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\" for writign, ie, pelase investigate both path and permtussions. OBservioant at [%s]:%s:%d\n", fileName_result, __FUNCTION__, __FILE__, __LINE__); return false;} //! ie, then an error in file-reading, ie, abort.
  }
  assert(file_out);  
  const char *seperator = ","; const char *seperator_newLine = "\n";
#define __MiF__writeOut_jsVarName(index, sep) ({if(index == 0) { fprintf(file_out, "var %s = [%s", str_data, sep);       }})
  //! Write out a float-value:
#define __MiF__writeOut__js_index(index, sep) ({ \
    if(index != UINT_MAX) { fprintf(file_out, "%u%s", index, sep); } \
    else {fprintf(file_out, "%s%s", "null", sep);} })
  //! Write out a float-value:
#define __MiF__writeOut__js_float(value, sep) ({ \
    if(value != T_FLOAT_MAX) { fprintf(file_out, "%f%s", value, sep); } \
    else {fprintf(file_out, "%s%s", "null", sep);} })

  //!
  //! Start the export-routine: 
  //const bool isTo_useStrings = (self->arr_names_clustering.nrows > 0);
  { const char *str_data = "arrOf_names"; //! ie, write out for the latter.    
    uint cnt_ele = 0; get_count__s_kt_list_1d_string_t(&(self->arr_names_clustering), &cnt_ele);
    //const uint cnt_ele = self->arr_names_clustering.nrows;
    printf("cnt_naems=%u, at %s:%d\n", cnt_ele, __FILE__, __LINE__);
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_jsVarName(i, ""); //! ie, wirte out hhe java-script-var-naem for the first entry:
      //! Write out the string: 
      char str_fallBack_1[10000];        const char *str = __MiF__getString(i, str_fallBack_1);
      //const char *str = getAndReturn_string__s_kt_list_1d_string(&(self->arr_names_clustering), i);      
      printf("[%u]=\"%s\", at %s:%d\n", i, str, __FILE__, __LINE__);
      if((i +1) != cnt_ele) {
	fprintf(file_out, "\"%s\", ", str);
      } else {
	fprintf(file_out, "\"%s\"];\n", str);
      }
    }
  }
  //! -----------------------
  { const char *str_data = "arrOf_clusterRelationships";
    uint cnt_ele = 0; get_count__s_kt_list_1d_pairFloat_t(&(self->arrOf_clusterRelationships), &cnt_ele);
    //const uint cnt_ele = self->arrOf_clusterRelationships.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_jsVarName(i, "\n"); //! ie, wirte out hhe java-script-var-naem for the first entry:
      //! Write out the triplet:
      fprintf(file_out, "["); //! ie, 'start a new row-block'.
      //! Add the relationships, 'inserting' intoa (head, tial, score) feature-amtrix-format, a format among others supportef by our "g_tree_formatConversion__constructFrom__matrix__list__headTailScore(..)" ("g_tree_formatConversion.js"):
      __MiF__writeOut__js_index(self->arrOf_clusterRelationships.list[i].head, seperator);
      __MiF__writeOut__js_index(self->arrOf_clusterRelationships.list[i].tail, seperator);
      //const uint head = self->arrOf_clusterRelationships.list[i].head;
      if((i +1) != cnt_ele) {
	__MiF__writeOut__js_float(self->arrOf_clusterRelationships.list[i].score, "],\n");
	//fprintf(file_out, "],"); //! ie, 'complete the row-block'.
      } else {
	__MiF__writeOut__js_float(self->arrOf_clusterRelationships.list[i].score, "]\n];\n");
	/* fprintf(file_out, "]"); //! ie, 'complete the row-block'. */
	/* fprintf(file_out, "];\n"); */
      }
    }
  }
  //! -----------------------
  { const char *str_data = "arr_vertexTo_centroid";  
    uint cnt_ele = 0; get_count__s_kt_list_1d_uint_t(&(self->arr_vertexTo_centroid), &cnt_ele);
    //const uint cnt_ele = self->arr_vertexTo_centroid.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_jsVarName(i, ""); //! ie, wirte out hhe java-script-var-naem for the first entry:
      if((i +1) != cnt_ele) {
	__MiF__writeOut__js_index(self->arr_vertexTo_centroid.list[i], seperator);
	//fprintf(file_out, "],"); //! ie, 'complete the row-block'.
      } else {
	__MiF__writeOut__js_index(self->arr_vertexTo_centroid.list[i], "];\n");
	//fprintf(file_out, "]"); //! ie, 'complete the row-block'.
      }    
    }
  }
  //! -----------------------
  { const char *str_data = "arr_hcaResult_distanceFromRoot";  
    uint cnt_ele = 0; get_count__s_kt_list_1d_float_t(&(self->arr_hcaResult_distanceFromRoot), &cnt_ele);
    //const uint cnt_ele = self->arr_hcaResult_distanceFromRoot.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_jsVarName(i, ""); //! ie, wirte out hhe java-script-var-naem for the first entry:
      if((i +1) != cnt_ele) {
	__MiF__writeOut__js_float(self->arr_hcaResult_distanceFromRoot.list[i], seperator);
      } else {
	__MiF__writeOut__js_float(self->arr_hcaResult_distanceFromRoot.list[i], "];\n");
      }    
    }
  }
  //! -----------------------
  { const char *str_data = "arr_clusterSets_child_countOfScoreSum";  
    //! Note: in [below] we asusme taht 'only vlaues above 0' are of itnerest, ie, hence our 'none-use' of the default curreT_pos-apprapoch.
    uint cnt_ele = 0; //get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), &cnt_ele);    
    for(uint i = 0; i < self->arr_clusterSets_child_countOfScoreSum.list_size; i++) {
      const uint val = self->arr_clusterSets_child_countOfScoreSum.list[i];
      if( (val != UINT_MAX) && (val != 0) ) {cnt_ele = i + 1;} //! ie, the biggest index:
    }
    //uint cnt_ele = 0; get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_child_countOfScoreSum), &cnt_ele);
    //const uint cnt_ele = self->arr_clusterSets_child_countOfScoreSum.list_size;
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_jsVarName(i, ""); //! ie, wirte out hhe java-script-var-naem for the first entry:
      if((i +1) != cnt_ele) {
	__MiF__writeOut__js_float(self->arr_clusterSets_child_countOfScoreSum.list[i], seperator);
      } else {
	__MiF__writeOut__js_float(self->arr_clusterSets_child_countOfScoreSum.list[i], "];\n");
      }    
    }
  }
  //! -----------------------
  { const char *str_data = "arr_clusterSets_parent_countOfScoreSum";  
    //const uint cnt_ele = self->arr_clusterSets_parent_countOfScoreSum.list_size;
    //! Note: in [below] we asusme taht 'only vlaues above 0' are of itnerest, ie, hence our 'none-use' of the default curreT_pos-apprapoch.
    uint cnt_ele = 0; //get_count__s_kt_list_1d_float_t(&(self->arr_clusterSets_parent_countOfScoreSum), &cnt_ele);    
    for(uint i = 0; i < self->arr_clusterSets_parent_countOfScoreSum.list_size; i++) {
      const uint val = self->arr_clusterSets_parent_countOfScoreSum.list[i];
      if( (val != UINT_MAX) && (val != 0) ) {cnt_ele = i + 1;} //! ie, the biggest index:
    }
    for(uint i = 0; i < cnt_ele; i++) {
      __MiF__writeOut_jsVarName(i, ""); //! ie, wirte out hhe java-script-var-naem for the first entry:
      if((i +1) != cnt_ele) {
	__MiF__writeOut__js_float(self->arr_clusterSets_parent_countOfScoreSum.list[i], seperator);
	//fprintf(file_out, "],"); //! ie, 'complete the row-block'.
      } else {
	//fprintf(file_out, "]"); //! ie, 'complete the row-block'.
	__MiF__writeOut__js_float(self->arr_clusterSets_parent_countOfScoreSum.list[i], "];\n");
	//fprintf(file_out, "];\n");
      }    
    }
  }
  
  /* //! ----------------------- */
  /* { const char *str_data = "";   */
  /*   const uint cnt_ele = ; */
  /*   for(uint i = 0; i < cnt_ele; i++) { */
  /*     __MiF__writeOut_jsVarName(i); //! ie, wirte out hhe java-script-var-naem for the first entry: */
  /*     if((i +1) != cnt_ele) { */
  /* 	//fprintf(file_out, "],"); //! ie, 'complete the row-block'. */
  /*     } else { */
  /* 	//fprintf(file_out, "]"); //! ie, 'complete the row-block'. */
  /* 	fprintf(file_out, "];\n",); */
  /*     }     */
  /*   } */
  /* } */
			    /* /\*arrOf_names=*\/[/\*0=*\/"markus", /\*1=*\/"johannes", /\*2=*\/"jesus", /\*3=*\/"judas", /\*4=*\/"lukas"], */
			    /* /\*arr_vertexTo_centroid=*\/[/\*marksu:index=*\/1,/\*johannes, index==*\/1,/\*Jesus, index=*\/2,/\*Judas, index=*\/2, /\*Lukas, index=*\/2], */
			    /* /\*arr_hcaResult_distanceFromRoot=*\/[/\*marksu:dist=*\/4,/\*johannes, dist=*\/4,/\*Jesus, dist=*\/1,/\*Judas, dist=*\/6, /\*Lucas, index=*\/2] */
  //! 
  //! Close file-handler, and return:
  if(file_out != stdout) { fclose(file_out);  }   
  return true;
}
