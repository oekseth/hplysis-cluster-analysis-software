#include "kt_clusterAlg_prim.h"

// A C / C++ program for Prim's Minimum Spanning Tree (MST) algorithm. 
// The program is for adjacency matrix representation of the graph
 
#include <stdio.h>
#include <limits.h>
 
// Number of vertices in the graph
//#define V 5
 
// A utility function to find the vertex with minimum key value, from
// the set of vertices not yet included in MST
static int minKey(const t_float *key, const char *mstSet, const uint nrows) {
  // Initialize min value
  t_float min = key[0]; //T_FLOAT_MAX;
  uint min_index = 0; 
  for (uint v = 1; v < nrows; v++) {
    if( (mstSet[v] == false) && (key[v] < min) ) {
      min = key[v], min_index = v;
    }
  }
 
  return min_index;
}
 
// A utility function to print the constructed MST stored in parent[]
static int printMST(const uint *parent, const uint nrows, t_float **graph) {
  printf("Edge   Weight\n");
  for (int i = 1; i < nrows; i++) {
    printf("%d - %d    %f \n", parent[i], i, graph[i][parent[i]]);
  }
}
 
// Function to construct and print MST for a graph represented using adjacency
// matrix representation
void matrix__kt_clusterAlg_prim(const t_float **graph, const uint nrows) {
  assert(graph);
  assert(nrows > 1);


  assert(false); // FIXME[conceptaul]: trey desciribng didffneret suec-ases wrt. 'this' .... eg, when comapred to sim-metrics ... and how 'to set masks' ... 
  assert(false); // FIXME[code]: udpate docuemtntaiotn .... adn try suggesitng/(imrpving/using ad fiferent API ... eg, by 'replacing' "0" with "T_FLOAT_MAX" 


  const uint empty_0 = 0;
  uint *parent = allocate_1d_list_uint(nrows, empty_0); // Array to store constructed MST
  //! Note: our "key" hold the 'shrotest possilbe diredt scores' between vertices
  t_float *key = allocate_1d_list_float(nrows, empty_0);   // Key values used to pick minimum weight edge in cut
  char *mstSet = allocate_1d_list_char(nrows, empty_0); // To represent set of vertices not yet included in MST
 
  // Initialize all keys as INFINITE
  for (uint i = 0; i < nrows; i++) {
    key[i] = T_FLOAT_MAX, mstSet[i] = false;
  }
 
  // Always include first 1st vertex in MST.
  key[0] = 0;     // Make key 0 so that this vertex is picked as first vertex
  parent[0] = UINT_MAX; // First node is always root of MST 
 
  // The MST will have V vertices
  for (uint count = 0; count < nrows-1; count++) {
    //! Select/pick the vertex with the min-score 'among those which have not yet been evaluated' (ie, select vertices Not (yet) inclcued in the/our MST):
    int u = 0;
    if(true) { //! then 'inline' our fucntion used to find the 'min':
      //static int minKey(const t_float *key, const char *mstSet, const uint nrows) {
      // Initialize min value
      t_float min = key[0]; //T_FLOAT_MAX;
      uint min_index = 0; 
      for (uint v = 1; v < nrows; v++) {
	if( (mstSet[v] == false) && (key[v] < min) ) {
	  min = key[v], min_index = v;
	}
      }    
      u = min_index;
    } else {u = minKey(key, mstSet, nrows);}
    
    // Add the picked vertex to the MST Set
    mstSet[u] = true;
    
    // Update key value and parent index of the adjacent vertices of
    // the picked vertex. Consider only those vertices which are not yet
    // included in MST
    for (uint v = 0; v < nrows; v++) {      
      // graph[u][v] is non zero only for adjacent vertices of m
      // mstSet[v] is false for vertices not yet included in MST
      // Update the key only if graph[u][v] is smaller than key[v]
      if(isOf_interest(graph[u][v]) && (mstSet[v] == false) && (graph[u][v] <  key[v]) ) {
	parent[v]  = u, key[v] = graph[u][v];
      }
    }
  }

  assert(false); // FIXME: write a permtaution of [”elow] -... simlair to our *kruskal* impelemtaintions-trategy. 
  /* // print the constructed MST */
  /* printMST(parent, nrows, graph); */
  
  //! De-allcoate:
  free_1d_list_uint(&parent);
  free_1d_list_float(&key);
  free_1d_list_char(&mstSet);
}
 
 
// driver program to test above function
int test__matrix__kt_clusterAlg_prim() {
  /* Let us create the following graph
          2    3
      (0)--(1)--(2)
       |   / \   |
      6| 8/   \5 |7
       | /     \ |
      (3)-------(4)
      9          */
  const uint V = 5;
  t_float graph[V][V] = {{0, 2, 0, 6, 0},
		     {2, 0, 3, 8, 5},
		     {0, 3, 0, 0, 7},
		     {6, 8, 0, 0, 9},
		     {0, 5, 7, 9, 0},
  };
 
  // Print the solution
  matrix__kt_clusterAlg_prim(graph, V);
 
  return 0;
}
