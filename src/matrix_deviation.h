#ifndef matrix_deviation_h
#define matrix_deviation_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file matrix_deviation
   @brief provide lgoics for applying differnet algoirhtms for 'deviation'
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"


/**
   @struct s_matrix_deviation_std_row
   @brief the structure which is used to store the resutls of copmtuation the deviations for a gvien row (eosekth, 06. setp. 2016)
 **/
typedef struct s_matrix_deviation_std_row {
  t_float mean; 
  t_float variance;  //! ie, the sample-variance.
  t_float skewness;
  t_float kurtosis; //! ie, the sample kurosis
} s_matrix_deviation_std_row_t;


//! Initiate the s_matrix_deviation_std_row_t object, ie, set the latter object to empy
static void setTo_empty__s_matrix_deviation_std_row(s_matrix_deviation_std_row_t *self) {
  assert(self);
  self->mean = 0;
  self->variance = 0;
  self->skewness = 0;
  self->kurtosis = 0;
}
//! Initiate the s_matrix_deviation_std_row_t object, ie, set the latter object to empy
  //! @return an itnated matrix
static s_matrix_deviation_std_row_t setToEmptyAndReturn__s_matrix_deviation_std_row() {
  s_matrix_deviation_std_row_t self; setTo_empty__s_matrix_deviation_std_row(&self);
  //! @return an itnated amtrix:
  return self;
}


/**
   @struct s_matrix_deviation_skewnessDescription_t
   @brief intiates an object which is used to cpature data-deivaiton in a data-set (oekseth, 06. jan. 2016).
   @remarks extends the "Quaritle evlauation" ("https://en.wikipedia.org/wiki/Quartile")  with data-attributes used to evaluate variance of the ranks, an appraoched suggested by  (oekseth, 06. jan. 2016).
   @remarks cotnenxt: in "Whiskers Plot" (or: "Box Plot") the "Inter Quaritle Range (IQR)" is used to descitbe/depcit data-variace: the IQR is computed through/by "quartile(3) - quartile(1)", from which we obseve that the itnegration of quaritile(1) and quaritile(3) in a result-table simplifes evlauation of prediciton-simliarity (eg, wrt. the ifnerences which may be drawn/deluted).
**/
typedef struct s_matrix_deviation_skewnessDescription {
  s_matrix_deviation_std_row_t obj_deviation;
  t_float quartile_1; //! ie, the median ocomptued for the 'lower sorted half' of numbers.
  t_float quartile_2; //! ie, the median ocomptued for the 'complete sorted vecotr' of numbers: cuts the vector/number-set in half.
  t_float quartile_3; //! ie, the median ocomptued for the 'upper sorted half' of numbers.
  t_float quartile_2__andVar; //! the "(1/cntInteresting)*sum(\{rank[i], cntInteresting++\}) for \{abs(AVG - score[i]) < variance\}, ie, teh sum-of-ranks which are 'inside' the medan-threshold. Used as an alternative approach to describe the 'skweness' of vertex-ranks: different told the altter desibes the average of the rank-scores which are 'inside' an "AVG += STD" score
  t_float valueExtreme_min;   t_float valueExtreme_max;
} s_matrix_deviation_skewnessDescription_t;


//! Initiate the "s_matrix_deviation_skewnessDescription_t" and return (oekseth, 06. jan. 2016).
static s_matrix_deviation_skewnessDescription_t setToEmptyAndReturns_matrix_deviation_skewnessDescription_t() {
  s_matrix_deviation_skewnessDescription_t self; setTo_empty__s_matrix_deviation_std_row(&(self.obj_deviation));
  self.quartile_1 = T_FLOAT_MAX;
  self.quartile_2 = T_FLOAT_MAX;
  self.quartile_3 = T_FLOAT_MAX;
  self.quartile_2__andVar = T_FLOAT_MAX;
  self.valueExtreme_min = T_FLOAT_MAX; 
  self.valueExtreme_max = T_FLOAT_MAX;
  //! @return
  return self;
}



/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation_maskUint(const t_float *row, const uint *mask, const uint ncols, s_matrix_deviation_std_row_t *self);
/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation_maskChar(const t_float *row, const char *mask, const uint ncols, s_matrix_deviation_std_row_t *self);
/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation_implicitMask(const t_float *row, const uint ncols, s_matrix_deviation_std_row_t *self);
//! 'Cubild' and returnt he sampel STD non-squared varaine for the row in quesiton (oekseth, 06. feb. 2017).
//! @remarks we assume that any 'maksed empty values' in row are 'described by the T_FLOAT_MAX value, ie, wher eth latter vlaue is omtited from comptautions 
static t_float STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(const t_float *row, const uint ncols) {
  assert(row); assert(ncols != UINT_MAX); assert(ncols > 0);
  s_matrix_deviation_std_row_t self; setTo_empty__s_matrix_deviation_std_row(&self);
  get_forRow_sampleDeviation__matrix_deviation_implicitMask(row, ncols, &self);
  //! @return
  return self.variance; //! ie, the sample-variance.  
}
/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. sept. 2016)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forRow_sampleDeviation__matrix_deviation(const t_float *row, const uint ncols, s_matrix_deviation_std_row_t *self);

/**
   @brief idnetifies for a row the sample-deviation (oekseth, 06. jan. 2021)
   @param <row> is the input-data
   @param <mask> which iof set impleis that we ivnestgiate wrt. a mask stored for the "unsigned int" (uint) data-type
   @param <mask_char> which iof set impleis that we ivnestgiate wrt. a mask stored for the "char" data-type
   @param <ncols> is the number of columsn (ei, elemnets) in "row": expected to be greater tahn "1" (ie, as the call will otherwise be pointless).
   @param <self> is the object which we udpate wr.t the result
   @remarks similar to the "ALGLIB" library (which our is signivcantly faster than) we comptue teh vearicne using "nols-1" instead of "ncols"   
 **/
void get_forMatrix_sampleDeviation__matrix_deviation(t_float **matrix, const uint nrows, const uint ncols, s_matrix_deviation_std_row_t *self);

//! @return the signficance-score for: "pearson" and "student-t-distribution":
void get_signficanceFromCorrelation__Pearson__matrix_deviation(const t_float r, const uint n, t_float* bothtails, t_float* lefttail, t_float* righttail);
//! @returnt the signficance-score for: "Spearman" and "student-t-distribution":
void get_signficanceFromCorrelation__Spearman__matrix_deviation(const t_float r, const uint n, t_float* bothtails, t_float* lefttail, t_float* righttail);

//! @return the jarquebera given skewness and kurtosis
void jarqueberatest_ifSkewnessAndKurtosis_isKnown__matrix_deviation(const uint n, const t_float skewness, const t_float kurtosis, t_float *result) ;

/**
   @brief comptue deviation for the s_matrix_deviation_skewnessDescription_t object (oekseth, 06. des. 2016).
   @param <self>  si the object which is pdated
   @param <row> is the data-vector to comptute for
   @param <row_size> is the number of elements to evlauate
   @param <row_ranks> if set is used to comptue for the row-ranks
   @return true upon scucess, ie, "true" if the inptu-arpameters are correctly set. 
 **/
bool compute__s_matrix_deviation_skewnessDescription_t(s_matrix_deviation_skewnessDescription_t *self, const t_float *row, const uint row_size, const t_float *row_ranks);

/**
   @enum e_kt_matrix__exportFormats___extremeToPrint
   @brief provide a generic strategy to specify the type of 'extrme cases' to printw rt. deviaiotn-evlauation (eosekth, 06. jna. 2017).
   @remakrs 'costnructed' iot. simplify future mdoificoants/needs.
 **/
typedef enum e_kt_matrix__exportFormats___extremeToPrint {
  e_kt_matrix__exportFormats___extremeToPrint_STD_max,
  //e_kt_matrix__exportFormats___extremeToPrint_,
  e_kt_matrix__exportFormats___extremeToPrint_undef
} e_kt_matrix__exportFormats___extremeToPrint_t;


/**
   @brief export result to a file-desriptor (oekseth, 06. jan. 2017).
   @param <matrix> is the amtrix to export
   @param <nrows> is the number of rows in "matrix"
   @param <ncols> is the number of columns in "matrix"
   @param <file_out> is teh file-descriptor to use when exproting the result
   @param <column_sep>  which for latex/Tex formats should be ' & '
   @param <row_sep>  which for latex/Tex formats should be ' \\ '
   @param <config_isToTranspose__whenGeneratingOutput> which if set to true implies that we write out "[deviaiton-attributes][data-row-id]", an option which is of pseific usefullness if the number of rows is larger 10 (ie, to get a better 'fit for ordinary file-layouts')
   @param <aux__matrixRanks> which if Not set to NULL is used to write out the average of the rank-scores which are 'inside' an "AVG += STD" score
   @param <typeOf_extractToPrint> which if Not set to e_kt_matrix__exportFormats___extremeToPrint_undef is used to print an 'extreme case' wrt. the data-input
   @return true upon success
 **/
bool exportToFormat__matrix__matrix_deviation(t_float **matrix, const uint nrows, const uint ncols, FILE *file_out, const char *column_sep, const char *row_sep, const bool config_isToTranspose__whenGeneratingOutput, t_float **aux__matrixRanks, const e_kt_matrix__exportFormats___extremeToPrint_t typeOf_extractToPrint, char **arrOf_names_head);



#endif //! EOF
