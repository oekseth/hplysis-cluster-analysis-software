#include "kt_terminal.h"
//! ------------------
#include "kt_api.h" //! which is sued to access the input-matrix.
#include "hpLysis_api.h"
#include "e_kt_correlationFunction.h"
#include "kt_metric_aux.h"
#include "hp_ccm.h"
#include "hp_distance.h"
#include "kt_sparse_sim.h"
#include "hp_api_fileInputTight.h"
#include "kt_terminal.h"


#include "kt_matrix.h"
#include "kt_matrix_cmpCluster.h"
#include "kt_list_1d.h"
#include "hpLysis_api.h"


/*    //#include "def_typeOf_float.h" */
/* #include "kt_api.h" */
/* // FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty' */
#include "e_kt_eigenVectorCentrality.h"
#include "kt_centrality.h"
#include "kt_clusterAlg_hca__node.h"
#include "parse_main.h"
#include "kt_clusterAlg_fixed_resultObject.h" //! which hold 'access' to the k-means-clsuter-object (oekseth, 06. otk. 2016)
#include "kt_clusterAlg_hca_resultObject.h" //! (oekseth, 06. nov. 2016).
#include "kt_clusterAlg_SOM_resultObject.h" //! (oekseth, 06. nov. 2016).
#include "kt_clusterAlg_hca__node.h" //! (oekseth, 06. nov. 2016).
#include "e_kt_clusterAlg_hca_type.h"


#include "hp_api_fileInputTight.h"
//! ------------------

//!
//! A main-front-end to our software:
int main(int array_cnt, char **array) {
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************

  //!
  //! The call:
  const int ret_val = add_params__kt_terminal(array_cnt, array);

  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  //! @return:
  return ret_val;
}
