#ifndef hp_api_fileInputTight_h
#define hp_api_fileInputTight_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_api_fileInputTight
   @brief a simplifed API to our hpLysis clsuter-analysis-software  (oekseth, 06. mar. 2017). 
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017). 
   @remarks in our "readFromFile__exportResult__hp_api_fileInputTight(..)" we provide the 'most simplfied access-poitn' for a C/C++ based data-access to our hpLysis software: the function does not requre any knowledge of programming nor an understanding of teh basic data-structures used in oru hpLysis clsuter-analysis-software. 

   @remarks in this API we provide/enable a simplfied appraoch to use generalised logics wrt. our:
   * basic usage: "hp_API_fileInputTight.h": a simplified API to our hpLysis cluster-analysis-software;
   * "hpLysis_API.h": for cluster-analysis: supports the combination of algorithms, such as HCA in combination with the "Silhouette" CCM to compute the best-scoring k-means clsuter;
   * "hp_distance.h": for computation of similarity-metrics using 'dense' input-matrices, eg, for "Euclid", "MINE", "Kendall, "Shannon", etc.;
   * "kt_sparse_sim.h": similarity-computation using sparse data-sets (eg, 2d-lists of triplets);
   * "hp_ccm.h": evaluation of hypothesis and accuracy of cluster-predictions, where "CCM" is an acronym for "Cluster Comparison Metrics" (CCMs), such as "Silhouette", "Rands Index", and "Davis-Bouldins Index (DBI)". 


   Details of different pemtuation wrt. abopve algorithms and metrics are found for:
   -- "simliarty-metrics": the differetn 'base-case' simlairty-emtircs supported by hpLysis is seen in our "e_kt_correlationFunction.h" wrt. the "e_kt_categoryOf_correaltionPreStep_t" enum and "e_kt_correlationFunction_t" enum. In our implementaiotn of simlairty-emtrics a large number of permtuations are supported: to examplfy the latter, we in our "kt_sparse_sim.h" enables permtautions of the "Euclid" simlairty-metric wrt.: (a)  "use-weight", (b) handle-noMatches=[ignore/none, *max(|row1|, |row2|), *|row1|*|row2|, *(|row1|+|row2|)], (c) adjustBy-cnt-elements). (For extensive descriptsion of the latter, please see our "kt_sparse_sim_config.h").
   -- "CCMs": our "kt_matrix_cmpCluster.h" provide a large number of different options/enums for both established CCMs, and the consturciton of permtuations of estlaibsheid CCMs (ie, as each CCM is fomruelized by the tuhros in a genrlaiseed 'form', agneelriaziaotn which 'allows' a large number of different interpreationts);
   -- "cluster-algorithms": our "e_hpLysis_clusterAlg_t" (in our "hpLysis_api.h") provide a number of cluster-algorithsm, clsuter-algorithms which may be furter mdofied using different configruation-options (in our "s_hpLysis_api_t" object). 
   -- data-input: both dense matrices (of type "s_kt_matrix_t" and "s_kt_matrix_base_t") and sparse data-sets (eg, "s_kt_list_1d_pairFloat_t" and "s_kt_set_2dsparse_t").
 **/

#include "hpLysis_api.h"
#include "kt_list_1d.h"
#include "hp_ccm.h"
#include "hp_distance.h"
#include "kt_sparse_sim.h"
#include "e_kt_correlationFunction.h"


#define MF__expand__bashInputArg__hp_api_fileInputTight(intro_head, intro_tail, arg, arg_cnt) ({ \
    if(intro_head && strlen(intro_head) ) {printf("%s: ", intro_head);} else {printf("\t");}  \
    for(uint i = 0; i < arg_cnt; i++) {printf("%s ", arg[i]);} printf("%s\n", intro_tail);})


//! Print the list of matrix-based CCMs (oekseth, 06. mar. 2017)
//! @remarks Print our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__ccm_matrixBased__hp_api_fileInputTight();
//! Print the list of "gold"x"gold" CCMs (oekseth, 06. mar. 2017)
//! @remarks pint our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__ccm_twoClusterResults__hp_api_fileInputTight();
//! Print the list of simliarty-metrics (oekseth, 06. mar. 2017)
//! @remarks pint our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__simMetric__hp_api_fileInputTight();
//! Print the list of cluster-algorithms 'directly' supported through enums in our "hpLysis_api.h" (oekseth, 06. mar. 2017)
//! @remarks pint our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__clustAlg__hp_api_fileInputTight();
//! Print the comprensive list of enum-options supported by our "hp_api_fileInputTight.h" API (oekseth, 06. mar. 2017)
void printOptions__hp_api_fileInputTight();

/**
   @enum e_hp_API_fileInputTight__logicType
   @brief identifes the different funciont-categroies expected to be used as input for our "hp_API_fileInputTight.h" funciton-API (oekseth, 06. mar. 2017).
 **/
typedef enum e_hp_API_fileInputTight__logicType {
  e_hp_API_fileInputTight__logicType_ccm_matrix,
  e_hp_API_fileInputTight__logicType_ccm_goldXgold,
  e_hp_API_fileInputTight__logicType_sim,
  e_hp_API_fileInputTight__logicType_clustAlg,
  e_hp_API_fileInputTight__logicType_undef
} e_hp_API_fileInputTight__logicType_t;




/**
   @brief idnetifes the enum-id assicated to a given stringOf_enum (oekseth, 06. mar. 2017)
   @param <config__printAlternativesIfNotFound> which if set to true implies that we print out the 'complete set of options' if the stringOf_enum is Not found in the e_hp_API_fileInputTight__logicType_t catory.
   @param <operation_type> idnetifes the subset of enums to investgiate: if set to "e_hp_API_fileInputTight__logicType_undef" then we investrgiate the complete set of 'possible input-enums'.
   @param <stringOf_enum> idnetifes the type of oepration to use (eg, wrt. CCM of Silhouette).
   @param <scalar_enum> is the return-value, descriibng the enum to use.
   @param <scalarBool_isFound> is set to '0'n(or:"false") if the enum was Not found.
   @return true upon success.
**/
bool get_enumId_assicatedTo_string__hp_api_fileinputtight(const bool config__printAlternativesIfNotFound, const  e_hp_API_fileInputTight__logicType_t operation_type, const char *stringOf_enum, int *scalar_enum, bool *scalarBool_isFound);


/**
   @brief computes a "matrix x gold" CCM based on data-input, and store result in "scalar_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <mat_1> is the input-matrix to use
   @param <list_1> is the hypothesis to evlauate
   @param <scalar_result> is updated with the result of the computation
   @return true upon scucess.
 **/
bool apply__ccm__matrixBased__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id, s_kt_matrix_t *mat_1, s_kt_list_1d_uint_t *list_1, t_float *scalar_result);
/**
   @brief computes a "matrix x gold" CCM based on data-input, and store result in "scalar_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <mat_1> is the input-matrix to use
   @param <mat_2> is the list/row to comapre with.
   @param <mat_2_rowId> is the row-id in the matr_2 to use.
   @param <scalar_result> is updated with the result of the computation
   @return true upon scucess.
 **/
bool apply__ccm__matrixBased__matrixInput__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_2, const uint mat_2_rowId, t_float *scalar_result);
/**
   @brief computes a "gold1 x gold2" CCM based on data-input, and store result in "scalar_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <mat_1> is the first hypothesis to evlauate;
   @param <mat_1_rowId> idenitfes the row in "mat_1" to use.
   @param <mat_2> is the second hypothesis to evlauate (ie, where "list_1" is comapred to "list_2" using enum_id).
   @param <mat_2_rowId> idenitfes the row in "mat_2" to use.
   @param <scalar_result> is updated with the result of the computation
   @return true upon scucess.
 **/
bool apply__ccm__twoClusters__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_metric_t enum_id, s_kt_list_1d_uint_t *list_1, s_kt_list_1d_uint_t *list_2, t_float *scalar_result);
/**
   @brief computes a "gold1 x gold2" CCM based on data-input, and store result in "scalar_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <list_1> is the first hypothesis to evlauate;
   @param <list_2> is the second hypothesis to evlauate (ie, where "list_1" is comapred to "list_2" using enum_id).
   @param <scalar_result> is updated with the result of the computation
   @return true upon scucess.
 **/
bool apply__ccm__twoClusters__matrixInput__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_metric_t enum_id, s_kt_matrix_t *mat_1, const uint mat_1_rowId, s_kt_matrix_t *mat_2, const uint mat_2_rowId,  t_float *scalar_result);
/**
   @brief applies a dense matrix-based simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
   @param <obj_metric> is the metric to apply.
   @param <mat_1> is the input-matrix to use
   @param <mat_2> is the second input-matrix: set to NULL then we perform a 'self-comparison' for "mat_1"
   @param <mat_result> is updated with the result of the computation: will be set to a [mat_1.nrows][mat_2.nrows || mat_1.nrows] dimension.
   @return true upon scucess.
 **/
bool apply__simMetric_dense__extraArgs__hp_api_fileInputTight(const s_kt_correlationMetric_t obj_metric, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_2, s_kt_matrix_t *mat_result);
/**
   @brief applies a dense matrix-based simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <mat_1> is the input-matrix to use
   @param <mat_2> is the second input-matrix: set to NULL then we perform a 'self-comparison' for "mat_1"
   @param <mat_result> is updated with the result of the computation: will be set to a [mat_1.nrows][mat_2.nrows || mat_1.nrows] dimension.
   @return true upon scucess.
 **/
bool apply__simMetric_dense__hp_api_fileInputTight(const e_kt_correlationFunction_t enum_id, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_2, s_kt_matrix_t *mat_result);
/**
   @brief applies a sparse 2d-list simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
   @param <obj_metric> is the metric to apply.
   @param <list_1> the list of (head, tail, score) triplets to use
   @param <list_2> the second list of (head, tail, score) triplets to use: set to NULL then we perform a 'self-comparison' for "list_1"
   @param <mat_result> is updated with the result of the computation: will be set to a [max(obj.head(list_1))][max(obj.head(list_2)) || max(obj.head(list_2))] dimension.
   @return true upon scucess.
 **/
bool apply__simMetric_sparse__extraArgs__hp_api_fileInputTight(const s_kt_correlationMetric_t obj_metric, s_kt_list_1d_pairFloat_t *list_1,  s_kt_list_1d_pairFloat_t *list_2, s_kt_matrix_t *mat_result);
/* static bool apply__simMetric_sparse__extraArgs__hp_api_fileInputTight__wrapper(s_kt_correlationMetric_t obj_metric, s_kt_list_1d_pairFloat_t *list_1) { */
/*   assert(false); // FIXME: add soemthing */
/* } */
/**
   @brief applies a sparse 2d-list simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <list_1> the list of (head, tail, score) triplets to use
   @param <list_2> the second list of (head, tail, score) triplets to use: set to NULL then we perform a 'self-comparison' for "list_1"
   @param <mat_result> is updated with the result of the computation: will be set to a [max(obj.head(list_1))][max(obj.head(list_2)) || max(obj.head(list_2))] dimension.
   @return true upon scucess.
 **/
bool apply__simMetric_sparse__hp_api_fileInputTight(const e_kt_correlationFunction_t enum_id, s_kt_list_1d_pairFloat_t *list_1,  s_kt_list_1d_pairFloat_t *list_2, s_kt_matrix_t *mat_result);

/**
   @brief compute clusters based on data-input, and store result in "mat_result" (oekseth, 06. apr. 2017).
 **/
//bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const s_kt_correlationMetric_t obj_metric, const uint nclusters, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep);
bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const s_kt_correlationMetric_t obj_metric_pre, const s_kt_correlationMetric_t obj_metric_inside, const uint cnt_clusters_x, const uint cnt_clusters_y, t_float iterative_SOM_tauInit, uint iterative_maxIterCount, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep);
//bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const s_kt_correlationMetric_t obj_metric_pre, const s_kt_correlationMetric_t obj_metric_inside, const uint cnt_clusters_x, const uint cnt_clusters_y, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep);

/**
   @brief compute clusters based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
   @param <enum_id> is the logic-type to apply
   @param <mat_1> is the input-matrix to use
   @param <list_1> is the hypothesis to evlauate
   @param <mat_result> is updated with the result of the computation
   @return true upon scucess.
 **/
bool apply__clustAlg__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const uint nclusters, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result);

/* typedef enum e_hp_api_fileInputTight__typeOf__logic { */
/*   e_hp_api_fileInputTight__typeOf__logic_ccm, */
/*   e_hp_api_fileInputTight__typeOf__logic_, */
/*   e_hp_api_fileInputTight__typeOf__logic_undef */
/* } e_hp_api_fileInputTight__typeOf__logic_t; */
/* typedef enum e_hp_api_fileInputTight__formatOf__export { */
/*   e_hp_api_fileInputTight__formatOf__export__matrix_tsv, */
/*   e_hp_api_fileInputTight__formatOf__export__matrix_js, */
/*   e_hp_api_fileInputTight__formatOf__export__matrix_json, */
/*   e_hp_api_fileInputTight__formatOf__export__triplets_head_tail_score, */
/* } e_hp_api_fileInputTight__formatOf__export_t; */

/**
   @brief read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
   @param <stringOf_enum> is used to dienitfy teh tyep of lgoics to be used (eg, wrt. CCM, sim-metircs and clust-alg).
   @param <mat_1> is the input-data-set to use: expected to be a tab-seperated (tsv) file
   @param <mat_2> optional: if set represents the input-data-set to use: expected to be a tab-seperated (tsv) file
   @param <mat_result> is the result-structure.
   @param <stringOf_exportFile__format> is the export-file-format to use. Supported formats are ["tsv", "js", "json"].
   @param <nclusters> which if set spcified the nubmer of clsuters to comptue for.
   @param <inputFile_isSparse> which if set to trie impleis taht we asusemt he inptu-fiel describes triplets (head, tail, socre), ie, in contrast to assumign "matrix[head][tail] = score".
   @param <> 
   @return true upon scucess.
   @remarks the possible number of enum-strings may be seen by calling our "printOptions__hp_api_fileInputTight()".
 **/
bool readFromFile__stroreInStruct__exportResult__hp_api_fileInputTight(const char *stringOf_enum, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_2, s_kt_matrix_t *mat_result, const char *stringOf_exportFile, const char *stringOf_exportFile__format, uint nclusters, const bool inputFile_isSparse);

/**
   @brief read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
   @param <stringOf_enum> is used to dienitfy teh tyep of lgoics to be used (eg, wrt. CCM, sim-metircs and clust-alg).
   @param <stringOf_inputFile_1> is the input-data-set to use: expected to be a tab-seperated (tsv) file
   @param <stringOf_inputFile_2> optional: if set represents the input-data-set to use: expected to be a tab-seperated (tsv) file
   @param <stringOf_exportFile> is the export-file to export to: if set to "" or NULL then we exprot result to the "stdout" file-descriptor.
   @param <stringOf_exportFile__format> is the export-file-format to use. Supported formats are ["tsv", "js", "json"].
   @return true upon scucess.
   @remarks the possible number of enum-strings may be seen by calling our "printOptions__hp_api_fileInputTight()".
 **/
bool readFromFile__exportResult__hp_api_fileInputTight(const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse);
/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_fileInputTight(char **arg, const uint arg_size);
//static void printOptions____hp_api_fileInputTight();

//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_fileInputTight(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse, uint *scalar_cntArgs, char **listOf_args__2d);



#endif //! EOF
