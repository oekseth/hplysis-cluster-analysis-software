#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"

/**
   @brief examplifies percularities wrt. sparse data-evalaution (oekseth, 06. jan. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. jan. 2017).
   @remarks A permutation of "tut_buildSimilarityMatrix.c" where we evlauate different low-computation-cost approaches for simliarty-metric-comptaution. Demonstrates features wrt.:
   -- input(a): the use of a 'sparse' data-set-matrix as input
   -- input(b) the use of apre-specified clsuter-input as an alternaitve to a 'radnom start-point': for details see the "init__setDefaultClusterVector__s_kt_clusterAlg_fixed_resultObject_t(..)" fucntion-call.
   -- computation(a): calls: extend "tut_buildSimilarityMatrix.c") wrt. a 'special' call to the "kt_Api.h" wrt. the mean-cluster-cnter-comptuation, where the latter examplifeis the 'impliciaotn' of using 'median' to copmtue clsuters.
   -- computation(b): extends and "tut_kCluster.c" wrt. the iteraiton through muliple clsuter-algorithm-types for k-means coptaution
   -- result: the 'centrliaty-mean-approach' examplifes the use of 'specific' functiosn in our "kt_api" Knitting-Tools API: in this exqmple we make use of the "kt_matrix__getclustermeans(..)" funciton to find the centroids; the "kt_matrix__getclustermeans(..)" function tries/seeks to answer/idneitfy/describe "how close a vertex(A) neighbourhood is to clusterId(vertex(A)): to compute the 'mean distance' from each cluster-id to the 'potential' cluster-vertex-members. When the result of diffenret 'clsuter-closeness' is combined, the result is a vector describing the 'overall' closeness to each cluster (which may be described through the metaphore of 'gravItational forces'). In k-means-clustering the (result of the) "kt_matrix__getclustermeans(..)" function is used 'as a vector' to figuoure out how simliar each vertex is 'to each cluster-center'. In contrast the k-medoid-appraoch 'only infer for a signle point' (ie, where hte latter does Not make use of a vecotr for simlairty-comaprsioon).
**/

int main() 
#endif
{
  //! Test the MINE-correlaiton-emtric for a sytnetic data-set:
  const bool dataIsBetweenOneAndZero = false; //! ie, our defualt assumption.
  const bool inputMatrix__isAnAdjecencyMatrix = true;
  const uint cnt_Calls_max = 3; const uint arg_npass = 1000;
  const uint nrows = 5;     const uint ncols = 5;  const uint nclusters = 2;
  const char *result_file = "test_tut_buildSimilarityMatrix.tsv";


  uint mapOf_scoreDistributions_case1[nrows][nrows] = {
    //!
    //! Cluster(1): 
    {0, 1, 0, 0, 0}, //! 0-->1
    {1, 0, 4, 0, 0}, //! 1-->[0, 3]
    {1, 3, 0, 0, 10}, //! 2-->[0, 2, 4]
    //!
    //! Cluster(2):
    {0, 12, 0, 0, 1}, //! 3-->[1, 4]
    {0, 0, 0, 1, 0}, //! 4-->3
  };
  const uint hypotehtical__clusterMemberships__startPoint[nrows] = { //! ie, where we specify a set of start-clsuter-psotions to use:
    0, 0, 0, //! ie, first clsuter,
    1, 1 //! ie, second cluster.
  }; 
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);  
  init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  assert(obj_matrixInput.nrows == nrows);
  assert(obj_matrixInput.ncols == ncols);
  //! Copy the [ªbove] data-set into the 'new data-set':
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      obj_matrixInput.matrix[row_id][col_id] = mapOf_scoreDistributions_case1[row_id][col_id];
    }
  }
  //! Open teh file :
  FILE *file_out = stdout; //fopen(result_file, "wb");
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}
  //!
  //! Correlate:
  /* for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) { */
  /*   for(uint post_index = 0; post_index <= e_kt_categoryOf_correaltionPreStep_none; post_index++) { */
  s_kt_correlationMetric_t corrMetric_insideClustering;   init__s_kt_correlationMetric_t(&corrMetric_insideClustering,
											 e_kt_correlationFunction_groupOf_minkowski_euclid,
											 //e_kt_correlationFunction_groupOf_directScore_direct_min,
											 //e_kt_correlationFunction_groupOf_directScore_direct_max,
											 e_kt_categoryOf_correaltionPreStep_none);
  //! Set the corrleation-metric:
  s_kt_correlationMetric_t corrMetric_prior = corrMetric_insideClustering; /* ;   init__s_kt_correlationMetric_t(&corrMetric_prior, (e_kt_correlationFunction_t)metric_index, (e_kt_categoryOf_correaltionPreStep_t)post_index); */
  /* if(!dataIsBetweenOneAndZero && describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(corrMetric_prior.metric_id)) {continue;}  */
  /* if(!inputMatrix__isAnAdjecencyMatrix && isTo_use_directScore__e_kt_correlationFunction(corrMetric_prior.metric_id)) {continue;}  */
  //! Ntoe: for dfiferent corr-emtrics see our "e_kt_correlationFunction.h"
  /* s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid); */


  //!
  { //! Apply cluster-analsysis:
    //!
    //! COfnigure the api-object:
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
    hp_config.stringOfResultPrefix__exportCorr__prior = NULL; 
    hp_config.config.corrMetric_prior = corrMetric_insideClustering; //! ie, update the metric.
    //hp_config.corrMetric_prior = corrMetric_prior; //! ie, update the metric.
    //if(true) { //! then ovverride the default correlation-metric:
    
    //} //! else we use the 'default metic' specified in our "hpLysis_api.c"
    // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Correlate and export:
    const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
    assert(is_ok);
    //! 
    //! Export results to "file_out":
    assert(file_out);
    // fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    // printf("\t stream_out=%p, at %s:%d\n", file_out, __FILE__, __LINE__);
    const bool is_ok_e = export__hpLysis_api(&hp_config, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, NULL);
    assert(is_ok_e);


    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_config);	    
  }
  //! -------------------------------------------------------------
  { //! Identify the cluster-medians for the current clustering-task:
    //! Note: examplifes the use of 'specific' functiosn in our "kt_api" Knitting-Tools API: in this exqmple we make use of the "kt_matrix__getclustermeans(..)" funciton to find the centroids
    //! Note: the "kt_matrix__getclustermeans(..)" function tries/seeks to answer/idneitfy/describe "how close a vertex(A) neighbourhood is to clusterId(vertex(A)): to compute the 'mean distance' from each cluster-id to the 'potential' cluster-vertex-members. When the result of diffenret 'clsuter-closeness' is combined, the result is a vector describing the 'overall' closeness to each cluster (which may be described through the metaphore of 'gravItational forces'). In k-means-clustering the (result of the) "kt_matrix__getclustermeans(..)" function is used to ....??... 

    //!
    //! Specify cluster-ids
    s_kt_matrix_t obj_result_clusterId; init__rowMappingTable__s_kt_matrix(&obj_result_clusterId, /*isTo_initCompelte_object=*/true, nrows);
    assert(nrows == 5); //! ie, what we expect in [below].
    assert(obj_result_clusterId.mapOf_rowIds); assert(nclusters == 2);
    //! Cluster(1): 
    obj_result_clusterId.mapOf_rowIds[/*vertex=*/0] = /*cluster-id=*/0;
    obj_result_clusterId.mapOf_rowIds[/*vertex=*/1] = /*cluster-id=*/0;
    obj_result_clusterId.mapOf_rowIds[/*vertex=*/2] = /*cluster-id=*/0;
    //! Cluster(2): 
    obj_result_clusterId.mapOf_rowIds[/*vertex=*/3] = /*cluster-id=*/1;
    obj_result_clusterId.mapOf_rowIds[/*vertex=*/4] = /*cluster-id=*/1;
    
    //!
    //! Compute:       
    s_kt_matrix_t obj_result_clusterId_columnValue; setTo_empty__s_kt_matrix_t(&obj_result_clusterId_columnValue); 
    const bool is_ok = kt_matrix__getclustermeans(&obj_matrixInput, &obj_result_clusterId, &obj_result_clusterId_columnValue, /*nclusters=*/nclusters, /*isTo_transposeMatrix=*/false);
    assert(is_ok);

    //!
    //! Write out the result to stdout:    
    fprintf(file_out, "\n\n#! The cluster-ids specified for the vertices are: \n");
    assert(obj_result_clusterId.nrows > 0);
    assert(obj_result_clusterId.mapOf_rowIds != NULL);
    for(uint i = 0; i < obj_result_clusterId.nrows; i++) {
      fprintf(file_out, "vertex[%u]\t cluster(%u)\n", i, obj_result_clusterId.mapOf_rowIds[i]);
    }
    fprintf(file_out, "#! (result generated at %s:%d)\n", __FILE__, __LINE__);
    fprintf(file_out, "\n\n#! The distance to each centroid: \n");
    export__singleCall__s_kt_matrix_t(&obj_result_clusterId_columnValue, NULL, NULL); //! ie, export to stdout.
    
    //!
    //! De-allocate:
    free__s_kt_matrix(&obj_result_clusterId);
    free__s_kt_matrix(&obj_result_clusterId_columnValue);
  }
  //! ********************************************************************************************************
  //! 
  { //! Compute k-means-clusters, where we use our 'initial assumption' as a starting-point for the iteration.    
    //!
    //! Seperately comptue for [mean, rank, medoid]:
    for(uint clusterAlg_index = e_hpLysis_clusterAlg_kCluster__AVG; clusterAlg_index <= (uint)e_hpLysis_clusterAlg_kCluster__medoid; clusterAlg_index++) {
      //  { const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__medoid; 
      const e_hpLysis_clusterAlg clusterAlg = (e_hpLysis_clusterAlg_t)clusterAlg_index;
      assert(clusterAlg != e_hpLysis_clusterAlg_undef);
      fprintf(file_out, "#! ****************************************************************\n#! ****************************************************************\n#! ----------------------------------\n\n#! New algorithm: \"%s\"\n", get_stringOf__e_hpLysis_clusterAlg_t(clusterAlg));
      //!
      //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:          
      for(uint cnt_Calls = 0; cnt_Calls < cnt_Calls_max; cnt_Calls++) {
	s_hpLysis_api_t hp_cluster_config = setToEmpty__s_hpLysis_api_t(NULL, NULL);
      
	// (Symmetric) adjacency matrix                                                                                                                            
	hp_cluster_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
	hp_cluster_config.config.corrMetric_prior_use = false; //! ie, use data-as-is.

	//!
	//! Spefiy our intial cluster-assumpiton:
	assert(hypotehtical__clusterMemberships__startPoint);
	//assert(hp_cluster_config.obj_result_kMean.
	init__setDefaultClusterVector__s_kt_clusterAlg_fixed_resultObject_t(&hp_cluster_config.obj_result_kMean, nclusters, nrows, hypotehtical__clusterMemberships__startPoint);
	
	/* Expect 2 clear clusters */
	printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
	const bool is_also_ok = cluster__hpLysis_api (
						      &hp_cluster_config, clusterAlg, &obj_matrixInput,
						      /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						      );
	assert(is_also_ok);

	//! 
	//! Export results to "file_out":
	assert(file_out);
	const s_kt_correlationMetric_t corrMetric_prior = hp_cluster_config.config.corrMetric_prior;
	fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
	/* bool is_ok_e = export__hpLysis_api(&hp_cluster_config, /\*stringOf_file=*\/NULL, /\*file_out=*\/file_out, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV); */
	/* assert(is_ok_e); */
	//! Export the vertex-clsuterId-memberships:
	bool is_ok_e = export__hpLysis_api(&hp_cluster_config, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &obj_matrixInput);
	assert(is_ok_e);
	//! De-allocates the "s_hpLysis_api_t" object.
	free__s_hpLysis_api_t(&hp_cluster_config);
      }
    }    
  }  

  
  //!
  //! Close the file and the amtrix:
  assert(file_out); if(file_out != stdout) {fclose(file_out);} file_out = NULL;
  free__s_kt_matrix(&obj_matrixInput);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

