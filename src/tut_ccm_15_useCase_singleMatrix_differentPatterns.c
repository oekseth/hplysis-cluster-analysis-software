#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hpLysis_api.h" //! which is used for intiating and de-allcoating the MINE-optmized-dat-astructures.
#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.

#define __MiF__isTo_evaluate(sim_id) (!isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id))

//! A lcoal funciton to apply/consturct wrt. our expeirement:
static void __tut_ccm_15_useCase_singleMatrix_differentPatterns__compute(s_kt_matrix_t mat_data, const bool use_specificMetirc_inside, const char *result_filePrefix,  uint *mapOf_hypothesisClusterMemberships) {
  //! 
  //! Construct result-matrix: 
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  {
    const uint max_nrows = (uint)e_kt_correlationFunction_undef; //! ie, the amx-number of siamrlity-emtrics to evlauate/apply.
    const uint max_ncols = mat_data.nrows * mat_data.nrows;
    mat_result = initAndReturn__s_kt_matrix(max_nrows, max_ncols);
    //!
    //! Set the row-and-column-headers:
    for(uint  row_id = 0; row_id < max_nrows; row_id++) {
      const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)row_id);
      //      const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t((e_kt_correlationFunction_t)row_id);
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/row_id, str_local, /*addFor_column=*/false);
    }
    //! Set the column-headers:
    uint index_local = 0;
    for(uint  row_id_1 = 0; row_id_1 < mat_data.nrows; row_id_1++) {
      for(uint  row_id_2 = 0; row_id_2 < mat_data.nrows; row_id_2++) {
	char str_local[1000]; memset(str_local, '\0', 1000);
	sprintf(str_local, "c(%u) x c(%u)", row_id_1+1, row_id_2+1);
	set_stringConst__s_kt_matrix(&mat_result, /*index=*/index_local, str_local, /*addFor_column=*/true);
	index_local++;
      }
    }
  }
  //!
  //! Comptue simliarty-scores:
  for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
    uint index_local = 0;
    //    if(!isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id)) {
    if(__MiF__isTo_evaluate(sim_id)) {
      for(uint  row_id_1 = 0; row_id_1 < mat_data.nrows; row_id_1++) {
	for(uint  row_id_2 = 0; row_id_2 < mat_data.nrows; row_id_2++) {
	  t_float distance = 0;
	  //! Note: the [”elow] call is used to simplify our loigics wrt. thei tut: for large data-set we recconmend using the "apply__hp_distance(..)" funciotn (in our "hp_distance.h").
	  const bool is_ok = apply__rows_hp_distance(initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none), mat_data.matrix[row_id_1], mat_data.matrix[row_id_2], mat_data.ncols, &distance);
	  assert(index_local < mat_result.ncols);
	  mat_result.matrix[sim_id][index_local] = distance;
	  index_local++;
	}
      }
    }
  }
  { //! Export the results (and-de-allocates):
    {
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_featureScores_euclidInside_%u.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      //fprintf(stdout, "(info)\t Export results to file=\"%s\", at %s:%d\n", stringOf_result, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
    }
    {
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_featureScores_euclidInside_%u_nonTranspRank.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      s_kt_matrix_t mat_rank = initAndReturn_ranks__s_kt_matrix(&mat_result);
      //fprintf(stdout, "(info)\t Export results to file=\"%s\", at %s:%d\n", stringOf_result, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_rank, stringOf_result, NULL);
      free__s_kt_matrix(&mat_rank);
    }
    { //! Rank results and thereafter exprot:
      s_kt_matrix_t mat_rank = setToEmptyAndReturn__s_kt_matrix_t(); init__copy_transposed__thereafterRank__s_kt_matrix(&mat_rank, &mat_result, true);
      //      s_kt_matrix_t mat_rank = initAndReturn_transpos__s_kt_matrix(&mat_result);
      //const bool is_ok = init__copy_transposed__thereafterRank__s_kt_matrix(&mat_transp, &mat_result, true);
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_featureScores_euclidInside_%u_rank.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      //fprintf(stdout, "(info)\t Export results to file=\"%s\", at %s:%d\n", stringOf_result, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_rank, stringOf_result, NULL);
      free__s_kt_matrix(&mat_rank);
    }
      //    export__singleCall_firstTranspose__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
      //! De-allcote:
    free__s_kt_matrix(&mat_result);
  }
  //! ---------------------------------------------------------------------------- 
  //! 
  //! 
  { //! Compute CCM-scores for a 'fixed' clsuter-algorithm:
    const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
    //!
    //!
    const uint start_ccmId = (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD;
    const uint size_ccmId =(uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
    const uint cnt_ccm = size_ccmId - start_ccmId;
    const uint max_nrows = (uint)e_kt_correlationFunction_undef; //! ie, the amx-number of siamrlity-emtrics to evlauate/apply.
    mat_result = initAndReturn__s_kt_matrix(max_nrows, cnt_ccm);

    //!
    //! Set the row-and-column-headers:
    for(uint  row_id = 0; row_id < max_nrows; row_id++) {
      const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)row_id);
      //const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t((e_kt_correlationFunction_t)row_id);
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/row_id, str_local, /*addFor_column=*/false);
    }
    //! Set the column-headers:    
    uint index_local = 0;
    for(uint ccm_id = start_ccmId; ccm_id < size_ccmId; ccm_id++) {
      const char *str_local = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id);
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/index_local, str_local, /*addFor_column=*/true);
      index_local++;
    }
    //! 
    //! Compute CCM-score for each simlairty-metric:
    for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
      uint index_local = 0;    
      if(__MiF__isTo_evaluate(sim_id)) {
	for(uint ccm_id = start_ccmId; ccm_id < size_ccmId; ccm_id++) {
	  t_float scalarResult_ccmScore = 0; 	uint scalarResult_ncluster = 0;
	  if(use_specificMetirc_inside == false) {
	    const bool is_ok = standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(&mat_data, /*isAn__adjcencyMatrix=*/false, (e_kt_correlationFunction_t)sim_id, (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id, clusterAlg, &scalarResult_ccmScore, &scalarResult_ncluster);
	    assert(is_ok);
	  } else {
	    //!
	    //! Handle 'non-set-valeus':
	    //!
	    //! Allocate object:
	    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	    obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = /*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
	    obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
	    obj_hp.config.corrMetric_prior.metric_id = (e_kt_correlationFunction_t)sim_id;
	    obj_hp.config.corrMetric_prior_use = true;
	    obj_hp.config.corrMetric_insideClustering = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none); //(e_kt_correlationFunction_t)sim_id;
	    
	    //! 
	    //! Apply logics:
	    const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
	    assert(is_ok);
	    //!
	    //! Get the result-aprameters 'of our itnerest':
	    scalarResult_ncluster = obj_hp.dynamicKMeans__best__nClusters;
	    scalarResult_ccmScore = obj_hp.dynamicKMeans__best__score;
	    //! 
	    //! De-allcoate object, and return:
	    free__s_hpLysis_api_t(&obj_hp);
	  }


	  //!
	  //! Set the score:
	  mat_result.matrix[sim_id][index_local] = scalarResult_ccmScore;
	  index_local++;
	}
      }
    }
  }
  { //! Export the results (and-de-allocates):
    {
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_ccm_fixedClusterAlg_euclidInside_%u.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
    }
    {
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_ccm_fixedClusterAlg_euclidInside_%u_nonTranspRank.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      s_kt_matrix_t mat_rank = initAndReturn_ranks__s_kt_matrix(&mat_result);
      //fprintf(stdout, "(info)\t Export results to file=\"%s\", at %s:%d\n", stringOf_result, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_rank, stringOf_result, NULL);
      free__s_kt_matrix(&mat_rank);
    }
    { //! Then a transposed call:
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_ccm_fixedClusterAlg_euclidInside_%u.transp.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      export__singleCall_firstTranspose__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
    }
    //    export__singleCall_firstTranspose__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
    //! De-allcote:
    free__s_kt_matrix(&mat_result);
  }
  //! ---------------------------------------------------------------------------- 
  //! 
  //! 
  { //! Compute a fixed CCM-score for all of the clsuter-algorithms:
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
    //!
    //!
    const uint cnt_alg = (uint)e_hpLysis_clusterAlg_undef;
    const uint max_nrows = (uint)e_kt_correlationFunction_undef; //! ie, the amx-number of siamrlity-emtrics to evlauate/apply.
    mat_result = initAndReturn__s_kt_matrix(max_nrows, cnt_alg);
    s_kt_matrix_t mat_result_gold = setToEmptyAndReturn__s_kt_matrix_t();
    if(mapOf_hypothesisClusterMemberships) {
      mat_result_gold = initAndReturn__s_kt_matrix(max_nrows, cnt_alg);
    }
    //!
    //! Set the row-and-column-headers:
    for(uint  row_id = 0; row_id < max_nrows; row_id++) {
      const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)row_id);
      //const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t((e_kt_correlationFunction_t)row_id);
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/row_id, str_local, /*addFor_column=*/false);
      if(mapOf_hypothesisClusterMemberships) {
	set_stringConst__s_kt_matrix(&mat_result_gold, /*index=*/row_id, str_local, /*addFor_column=*/false);
      }
    }
    //! Set the column-headers:    
    uint index_local = 0;
    for(uint alg_id = 0; alg_id < cnt_alg; alg_id++) {
      const char *str_local = get_stringOf__short__e_hpLysis_clusterAlg_t((e_hpLysis_clusterAlg_t)alg_id);
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/index_local, str_local, /*addFor_column=*/true);
      if(mapOf_hypothesisClusterMemberships) {
	set_stringConst__s_kt_matrix(&mat_result_gold, /*index=*/index_local, str_local, /*addFor_column=*/true);
      }
      index_local++;
    }
    //! 
    //! Compute CCM-score for each simlairty-metric:
    for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
      if(__MiF__isTo_evaluate(sim_id)) {
	uint index_local = 0;    
	for(uint alg_id = 0; alg_id < cnt_alg; alg_id++) {
	  const e_hpLysis_clusterAlg clusterAlg = (e_hpLysis_clusterAlg_t)alg_id;
	  //for(uint ccm_id = start_ccmId; ccm_id < cnt_ccm; ccm_id++) {
	  t_float scalarResult_ccmScore = 0; 	uint scalarResult_ncluster = 0;
	  if(use_specificMetirc_inside == false) {
	    const bool is_ok = standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(&mat_data, /*isAn__adjcencyMatrix=*/false, (e_kt_correlationFunction_t)sim_id, enum_ccm, clusterAlg, &scalarResult_ccmScore, &scalarResult_ncluster);
	    assert(is_ok);
	  } else {
	    //!
	    //! Handle 'non-set-valeus':
	    //!
	    //! Allocate object:
	    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	    obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm;
	    obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
	    obj_hp.config.corrMetric_prior.metric_id = (e_kt_correlationFunction_t)sim_id;
	    obj_hp.config.corrMetric_prior_use = true;
	    obj_hp.config.corrMetric_insideClustering = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none); //(e_kt_correlationFunction_t)sim_id;
	    
	    //! 
	    //! Apply logics:
	    const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
	    assert(is_ok);
	    //!
	    //! Get the result-aprameters 'of our itnerest':
	    scalarResult_ncluster = obj_hp.dynamicKMeans__best__nClusters;
	    scalarResult_ccmScore = obj_hp.dynamicKMeans__best__score;
	    if(mapOf_hypothesisClusterMemberships) {
	      if(obj_hp.obj_result_kMean.vertex_clusterId != NULL) {
		if(obj_hp.obj_result_kMean.clusterId_size > 0) {
		  //! Then we comptue the score:
		  const uint cnt_vertices = macro_min(obj_hp.obj_result_kMean.cnt_vertex, mat_data.nrows);
		  t_float result_score = 0;
		  const bool is_ok = ccm__twoHhypoThesis__xmtMatrixBased__hp_ccm(
										 ///*enum=*/e_kt_matrix_cmpCluster_metric_randsIndex,
										 /*enum=*/e_kt_matrix_cmpCluster_metric_randsAdjustedIndex,
										 //	  /*enum=*/e_kt_matrix_cmpCluster_metric_,
										 obj_hp.obj_result_kMean.vertex_clusterId, mapOf_hypothesisClusterMemberships, cnt_vertices, &result_score);
		  if(is_ok) {
		    mat_result_gold.matrix[sim_id][index_local] = result_score;
		  }

		}
	      }
	    }
	    //! 
	    //! De-allcoate object, and return:
	    free__s_hpLysis_api_t(&obj_hp);
	  }
	  //!
	  //! Set the score:
	  mat_result.matrix[sim_id][index_local] = scalarResult_ccmScore;
	  index_local++;
	}
      }
    }
    if(mapOf_hypothesisClusterMemberships) { //! Export the results (and-de-allocates):
      {
	char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
	sprintf(stringOf_result, "%ssim_clusterAlg_fixedCCM_euclidInside_%u.gold.tsv", result_filePrefix, !use_specificMetirc_inside);
	fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result_gold.nrows, mat_result_gold.ncols, __FILE__, __LINE__);
	export__singleCall__s_kt_matrix_t(&mat_result_gold, stringOf_result, NULL);
      }
      //! 
      { //! Then a transposed call:
	char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
	sprintf(stringOf_result, "%ssim_clusterAlg_fixedCCM_euclidInside_%u.transp.gold.tsv", result_filePrefix, !use_specificMetirc_inside);
	fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result_gold.nrows, mat_result_gold.ncols, __FILE__, __LINE__);
	export__singleCall_firstTranspose__s_kt_matrix_t(&mat_result_gold, stringOf_result, NULL);
      }
      //! De-allcote:
      free__s_kt_matrix(&mat_result_gold);
    }
  }
  { //! Export the results (and-de-allocates):
    {
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_clusterAlg_fixedCCM_euclidInside_%u.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
    }
    //! 
    { //! Then a transposed call:
      char stringOf_result[1000]; memset(stringOf_result, '\0', 1000); 
      sprintf(stringOf_result, "%ssim_clusterAlg_fixedCCM_euclidInside_%u.transp.tsv", result_filePrefix, !use_specificMetirc_inside);
      fprintf(stdout, "(info)\t Export results to file=\"%s\", given result-matrix w/dims=[%u, %u], at %s:%d\n", stringOf_result, mat_result.nrows, mat_result.ncols, __FILE__, __LINE__);
      export__singleCall_firstTranspose__s_kt_matrix_t(&mat_result, stringOf_result, NULL);
    }
    //! De-allcote:
    free__s_kt_matrix(&mat_result);
  }
}

/**
   @brief demonstrates how to compute CCM-scores and simliarty-metic-scores for a signle input-inputs.
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks if a second input-paramter is given then we use the latter as a file-name, eg, "x_tut input_file.tsv"
   @remarks general: used to evlauate the simlairty-metic-score in a data-set.
**/
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main(const int array_cnt, char **array) 
#else
int tut_ccm_15_useCase_singleMatrix_differentPatterns(const char *strOf_fileName) 
#endif
{
  //! Intlaise the input-matrix:
  s_kt_matrix_t mat_data = setToEmptyAndReturn__s_kt_matrix_t();
  uint *mapOf_hypothesisClusterMemberships = NULL;
  //!
  //! Handle inptu-speifiv settings:
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
  //! 
  //! Load the data-set:  
  if((array_cnt >= 1) && array[1] && strlen(array[1])) { //! then we laod the data-set:
    fprintf(stdout, "(info)\t Loads data from file=\"%s\", at %s:%d\n", array[1], __FILE__, __LINE__);
    import__s_kt_matrix_t(&mat_data, array[1]);
  }
  const char *result_filePrefix = "";
#else
  if(strOf_fileName && strlen(strOf_fileName)) { //! eg, the file-name "./data/local_downloaded/iris.data.hpLysis.tsv"
    fprintf(stdout, "(info)\t Loads data from file=\"%s\", at %s:%d\n", strOf_fileName, __FILE__, __LINE__);
    import__s_kt_matrix_t(&mat_data, strOf_fileName);
  }
#endif
  const char *result_filePrefix = "";
  uint mapOf_hypothesisClusterMemberships_syntCase[7] = { //! ie, the clsuter-mberships-hyptoesis for [”elow], where we assuem that 
    //! Ntoe: [”elwo] represents the vertex[i] = cluster_id
    0, 1, 0, 1, 
    2, 2, 2
  };
  if(mat_data.nrows == 0) { //! then we use the data-set used in our reseahrc-ariltces to demonstre/examplify cases where all lines by deifniont are simliar. 
    //! Note: [”elow] data-set is taken from our "/home/klatremus/poset_src/articles/lib_metrics_sim/fig_similarity_metric_dataCases_eval.tex":
    mapOf_hypothesisClusterMemberships = mapOf_hypothesisClusterMemberships_syntCase;
    const uint nrows = 7; const uint ncols = 8; 
    t_float matrix[nrows][ncols] = {
      {5, 5, 5, /*3=*/5, 5, 5, 5, 5},
      {0, 1, 2, /*3=*/3, 4, 5, 6, 7},
      {6, 6.1, 6.2, /*3=*/6.3, 6.4, 6.5, 6.6, 6.7},
      {7, 6, 5, /*3=*/4, 3, 2, 1, 0},
      {2, 7, 3, /*3=*/4, 3, 2, 1, 0},
      {0, 5, 1, /*3=*/4, -2, 1, 0, 10},
      {2/4.0, 7/4.0, 3/4.0, /*3=*/6/4.0, 0, 3/4.0, 2/4.0, 12/4.0},
      /* {, , , /\*3=*\/, , , , ,}, */
      /* {}, */
      /* {}, */
      /* {}, */
    };
    //!
    //! Intiate:
    mat_data = initAndReturn__s_kt_matrix(nrows, ncols);
    //!
    //! Insert the elements into the matrix:
    for(uint  row_id = 0; row_id < nrows; row_id++) {
      for(uint  col_id = 0; col_id < ncols; col_id++) {
	mat_data.matrix[row_id][col_id] = matrix[row_id][col_id];
      }
    }
  }
  assert(mat_data.nrows > 0); //! ie, what we expect.
  __tut_ccm_15_useCase_singleMatrix_differentPatterns__compute(mat_data, /*use_specificMetirc_inside=*/false, result_filePrefix, mapOf_hypothesisClusterMemberships);
  __tut_ccm_15_useCase_singleMatrix_differentPatterns__compute(mat_data, /*use_specificMetirc_inside=*/true, result_filePrefix, mapOf_hypothesisClusterMemberships);
  //!
  //! De-allcoate
  free__s_kt_matrix(&mat_data);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}
