#include "fast_log.h"
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


#if optmize_usePReComptued_floats == 1 
t_float *static_listOf_values_forIndex = NULL; t_float *static_listOf_values_forIndex_midPoint = NULL; 
#endif

#ifndef NDEBUG

/* #if optmize_usePReComptued_floats == 1 */


/* //extern t_float *static_listOf_values_forIndex; extern t_float *static_listOf_values_forIndex_midPoint; extern static const int static_listOf_values_forIndex_size = 1000*500; //! ie, we expect less than 1,000,000 unique indexes. */

/* static void allocate_optmize_usePReComptued_floats() { */
/*   // FIXME: if ["elow] results in a performance-icnrease then remember to (a) de-allcoate ["elow] and (b) do not allcoat ethis 'ofr eahc' during the parallel copmtuation of columns */

/* #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values: */

/*   { //! Update: */
/*     static_listOf_values_forIndex = allocate_1d_list_float(/\*size=*\/static_listOf_values_forIndex_size*2, /\*default-value=*\/0);  */
/*     static_listOf_values_forIndex_midPoint = static_listOf_values_forIndex + static_listOf_values_forIndex_size; //! ei, to 'handle' zero-valeus. */

/*     // printf("allcoate for size=%d and arr=%p, add-midpoint=%p, at [%s]:%s:%d\n", (int)static_listOf_values_forIndex_size, static_listOf_values_forIndex, static_listOf_values_forIndex_midPoint, __FUNCTION__, __FILE__, __LINE__); */

/*     for(int i = 1; i < static_listOf_values_forIndex_size; i++) { */
/*       // printf("\t update-midPoint-index-log[%u], at %s:%d\n", (uint)i, __FILE__, __LINE__); */
/*       static_listOf_values_forIndex_midPoint[i] = (t_float)log((float)i); */
/*       assert(static_listOf_values_forIndex_midPoint[i] == static_listOf_values_forIndex[i + static_listOf_values_forIndex_size]); */
/*     } */
/*   } */

/* #if optmize_use_logValues_fromLocalTable != 1 //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]: */

/*   const uint cnt_allocated = 2*(uint)(static_listOf_values_size+1); //! ie, to handle the value-range [-1, 1]. */
/*   vec_fast_log_defaultValue = VECTOR_FLOAT_SET1(/\*static_listOf_values_size=*\/static_listOf_values_size); */
/*   static_listOf_values = allocate_1d_list_float(/\*size=*\/cnt_allocated, /\*default-value=*\/0);  */
/*   static_listOf_values_shallowCopy_aboveZero =  static_listOf_values + static_listOf_values_size; //! ie, to reduce the number of artimethic operations to identify the index for valeus in range [0, 1] */
/*   assert(static_listOf_values); */

/*   assert(VECTOR_FLOAT_getAtIndex(vec_fast_log_defaultValue, /\*index=*\/0) == static_listOf_values_size); */

/*   if(false) {printf("\t(info)\t allcoated \"static_listOf_values\" w/cnt_allocated=%u, at %s:%d\n", cnt_allocated, __FILE__, __LINE__);} */



/*   for(int i = 1; i <= (int)static_listOf_values_size; i++) { */
/*     // FIXME: make use of the log-valeus. */
/*     const uint local_index = static_listOf_values_size - i;   */
/* #if 1 == 2 */
/*     assert(i< cnt_allocated); */
/*     const float scalar_value = (-1*(float)i)*((float)1/(float)(static_listOf_values_size)); //! ie, the negative value-range [0, -1]  */
/*     assert(isnan(log(scalar_value))); //! ie, as we expect that negative nubmers will alwasy be compelx, ie, NAN */
/*     static_listOf_values[local_index] = log(scalar_value); */
/*     const float expected_index = __get_internalIndexOf_float(scalar_value); */
/*     // printf("[(%u=%f) <- %f] = %f, at %s:%d\n", local_index, expected_index, scalar_value, static_listOf_values[local_index], __FILE__, __LINE__); */
/*     assert(static_listOf_values[local_index] != NAN); */
/*     assert(expected_index == local_index); //! ie, what for for cosnsitency expect. */
/* #endif */
/*     static_listOf_values[local_index] = 0; //! ie, as we expect that negative nubmers will alwasy be compelx, ie, NAN */
/*   } */

/*   for(uint i = 1; i < static_listOf_values_size; i++) { */
/*     // FIXME: make use of the log-valeus.     */
/*     const uint local_index = static_listOf_values_size + i; */
/*     assert(local_index < cnt_allocated); */
/*     const float scalar_value = (float)i/(float)(static_listOf_values_size);     */
/*     static_listOf_values[local_index] = log(scalar_value); */
/*     // printf("[%u=%f] = %f , at %s:%d\n", local_index, scalar_value, static_listOf_values[local_index], __FILE__, __LINE__); */
/*     assert(static_listOf_values[local_index] != NAN);  */
/*     assert(__get_internalIndexOf_float(scalar_value) == local_index); //! ie, what for for cosnsitency expect.     */
/*     assert(static_listOf_values_shallowCopy_aboveZero[i] == static_listOf_values[local_index]); //! ie, givne the expectiaotn of a 'shallow copy'. */
/*   } */
/* #endif */
/* #endif //! whre 'thsi endif' refers to: #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values: */
/* } */
/* //! De-allcoat ehte reserved memory: */
/* static void free_optmize_usePReComptued_floats() { */
/*   // printf("(info)\t de-allocates the set of allcoated floats, at %s:%d\n", __FILE__, __LINE__); */
/* #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values: */
/*   free(static_listOf_values_forIndex);  */
/* #if optmize_use_logValues_fromLocalTable != 1 //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]: */
/*   assert(static_listOf_values); assert(static_listOf_values_size); */
/*   free(static_listOf_values);     */
/* #endif //! whre 'thsi endif' refers to: #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values: */
/* #endif */
/* } */
/* #else  */
/*  void allocate_optmize_usePReComptued_floats() { */
/*   // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */

/* } */
/* //! De-allcoat ehte reserved memory: */
/*  void free_optmize_usePReComptued_floats() {} */
/* #endif */







//! Main-function for correctness-evlauation:
void fast_log_validateCorrectness() {
  allocate_optmize_usePReComptued_floats();

  /* static const int arrOf_values_size = 4;   */
  /* float arrOf_values[arrOf_values_size] = {-0.9, 0.5, -0.2, 0.9}; */
  /* for(uint i = 0; i < arrOf_values_size; i++) { */
  /*   float test_value = arrOf_values[i]; */
  //printf("Cmp %.6f VS %.6f, at %s:%d\n", log(test_value), log_range_0_one(test_value), __FILE__, __LINE__);

#if optmize_use_logValues_fromLocalTable != 1 //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]:
  float test_value = -0.9; assert((int)log(test_value) == (int)log_range_0_one_abs(test_value));
  test_value = -0.5; assert((int)log(test_value) == (int)log_range_0_one_abs(test_value));
  test_value = -0.2; assert((int)log(test_value) == (int)log_range_0_one_abs(test_value));
  test_value = 0.2; assert((int)log(test_value) == (int)log_range_0_one_abs(test_value));
  test_value = 0.5; assert((int)log(test_value) == (int)log_range_0_one_abs(test_value));
  
#endif
  //assert(false); // FIXME: add soemthing

  free_optmize_usePReComptued_floats();
}

#else //! then we asusme the code is in an 'otpmzied compialtion-mode':
//! Main-function for correctness-evlauation:
void fast_log_validateCorrectness() {
  fprintf(stderr, "!!\t Called the test-function in optmized mode, ie, pointless as the asserts are anyhow de-activated. For questions please cotnact the developer at [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
}
#endif 
