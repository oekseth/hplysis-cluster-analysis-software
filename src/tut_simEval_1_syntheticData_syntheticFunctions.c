#include "ktSim_setOf_synData_spec.h"
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
#include "hp_clusterShapes.h"
#include "hp_ccm.h"
#include "hp_exportMatrix.h"

static const uint arrOf_sim_postProcess_size = 14;
static s_kt_correlationMetric_t arrOf_sim_postProcess[arrOf_sim_postProcess_size] = {
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none), //! ie, Pearson
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute, e_kt_categoryOf_correaltionPreStep_none), //! ie, Perason w/eslibhed permtaution
      // Absolute difference::Canberra 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_absoluteDifference_Canberra, e_kt_categoryOf_correaltionPreStep_none),
      // Fidelity::Hellinger:
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_categoryOf_correaltionPreStep_none),
      // Downward mutaiblity::symmetric-max
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax, e_kt_categoryOf_correaltionPreStep_none),
      // Inner product
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none),
      // Squared distance for Euclid
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Euclid, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_rank), //! ie, spearman.
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      //initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
    };

/**
   @enum
   @brief (oesketh, 06. arp. 2017)
 **/
typedef enum e_ktSim_join {
  e_ktSim_join_minus, //! ie, an aritmetchi oeprator and Not a comaprison-operator
  //e_ktSim_join_minus_abs,
  e_ktSim_join_pluss,
  e_ktSim_join_mult,
  e_ktSim_join_div,
  e_ktSim_join_min,
  e_ktSim_join_max,
  e_ktSim_join_undef,
} e_ktSim_join_t;

/**
   @enum
   @brief (oesketh, 06. arp. 2017)
 **/
typedef enum e_ktSim_adjust {
  e_ktSim_adjust_none,
  /* e_ktSim_adjust_max,  */
  /* e_ktSim_adjust_min,  */
  e_ktSim_adjust_abs,
  e_ktSim_adjust_log_10, //! large differences are reduced, eg, wrt. Shannon-Jensen-difference
  e_ktSim_adjust_log_2,
  e_ktSim_adjust_undef,
} e_ktSim_adjust_t;


/**
   @enum
   @brief (oesketh, 06. arp. 2017)
 **/
typedef enum e_ktSim_vector_summation {
  e_ktSim_vector_summation_min,
  e_ktSim_vector_summation_max,
  e_ktSim_vector_summation_sum,
  /* e_ktSim_vector_summation_div, */
  /* e_ktSim_vector_summation_div_minMax, */
  /* e_ktSim_vector_summation_mult_both, */
  /* e_ktSim_vector_summation_mult_log_result2, */
  e_ktSim_vector_summation_undef,
} e_ktSim_vector_summation_t;


/**
   @struct
   @brief (oesketh, 06. arp. 2017)
 **/
typedef struct s_ktSim {
  t_float each_pow; //! ie, the power to 'raise' eahc number in.
  e_ktSim_join_t merge_1;
  e_ktSim_adjust_t merge_2;
  t_float merge_3_pow; //! eg, to be set to '2' to 'emulate' Euclid.
  t_float merge_4_mult; //! eg, =0.5" for "sum(0.5*(v1+v2))".
  //t_float adjust_4_shift_scalar; //! eg, '+1' to handle '0' cases.  
  e_ktSim_vector_summation_t each_updateResult;
  t_float postStep_1_pow; //! which is used to 'handle' the post-stpe (eg, "sqrt(...)=()^(postStep_1_pow=0.5)").
  bool postStep_adjsutBy_vertexCount;
} s_ktSim_t;

//! @return an tinalised version.
s_ktSim_t init__s_ktSim_t() {
  s_ktSim_t self;
  self.each_pow = 1;
  self.merge_1 = e_ktSim_join_minus;
  self.merge_2 = e_ktSim_adjust_abs;
  self.merge_3_pow = 2; //! ie, 'simluarte' Euclid.
  //self.adjust_4_shift_scalar = 0;
  self.each_updateResult = e_ktSim_vector_summation_sum;
  self.postStep_1_pow = 2; //! ie, Euclid
  self.postStep_adjsutBy_vertexCount = false;
  //!
  //! @return
  return self;
}

#define MF__s_ktSim__set(pow_postStep, forAll, mult_eachResult, pow_resultEach, adjust_resultEach, operator, pow_each) ({obj.postStep_1_pow = pow_postStep; obj.each_updateResult = forAll; obj.merge_4_mult = mult_eachResult; obj.merge_3_pow = pow_resultEach; obj.merge_2 = adjust_resultEach; obj.merge_1 = operator; obj.each_pow = pow_each;})
/* #define MF__s_ktSim__() ({;}) */
/* #define MF__s_ktSim__() ({;}) */

/**
   @enum
   @brief (oesketh, 06. arp. 2017)
 **/
typedef enum e_ktSim_setOf_typeOfComb {
  e_ktSim_setOf_typeOfComb_numerator,
  e_ktSim_setOf_typeOfComb_numerDenum_max,
  e_ktSim_setOf_typeOfComb_numerDenum_min,
  e_ktSim_setOf_typeOfComb_numerDenum_pluss,
  e_ktSim_setOf_typeOfComb_numerDenum_minus,
  e_ktSim_setOf_typeOfComb_numerDenum_div,
  e_ktSim_setOf_typeOfComb_numerDenum_div_minMax, //! eg, wrt. "Wave-Hedges" which normalize wrt. "min(..)/max(..)", ie, where latter results/implies that ....??... 
  e_ktSim_setOf_typeOfComb_numerDenum_mult_both,
  e_ktSim_setOf_typeOfComb_numerDenum_mult_log_denum,
  //e_ktSim_setOf_typeOfComb_,
  e_ktSim_setOf_typeOfComb_undef,
} e_ktSim_setOf_typeOfComb_t;

/**
   @enum e_ktSim_typesOf_classes
   @brief provide a bunch of 'ready-made' synthetic simlairty-metrics (oesketh, 06. arp. 2017)
   @remarks idea is to permtuate estalibhsed simlairty-emtircs, thereby idenitfy 'reference-poitns' wrt. metric-accuracy.
 **/
typedef enum e_ktSim_typesOf_classes {
  e_ktSim_typesOf_classes_abs, //! ie, Minkowski::Cityblock
  e_ktSim_typesOf_classes_abs_pow, //! ie, Minkowski, eg, "Euclid"="sqrt(sum(v1-v2)^2)"
  e_ktSim_typesOf_classes_abs_pow_max, //! ie, Minkowski, eg, "Euclid"="max(v1-v2)^2", where latter 'represent' the de-numerator in the "Vicis-symmetric-max" simlairty-metric
  e_ktSim_typesOf_classes_abs_pow_normalized, //! eg, "Euclid(weighted)"="sqrt(sum(v1-v2)^2)/(sum(0.5*(v1+v2)))"
  e_ktSim_typesOf_classes_abs_relative, //! eg, for the metirc-class of "absolute-difference", where 'abs' is used in the numeator, while a 'non-abs-adjsuted operator is used as the de-numerator. ... eg, "Sorensen" = "sum(abs(v1, v2))/sum((v1 + v2))"
  e_ktSim_typesOf_classes_intersection, //! eg, "Wave-Hedges"="sum(1 - (min(v1, v2)/max(v1, v2)))"
  // FIXME[aritlce]: investigate wrt. [below] enum ... trying to 'capture' pattersn to be used in idneitficioant of new/future/improved silmiaryt-metrics.
  e_ktSim_typesOf_classes_intersection_euclid, //! an experimental modifcation by oekseth: "Wave-Hedges+Euclid"="sqrt(sum(sum(v1-v2)^2*(1 - (min(v1, v2)/max(v1, v2)))))" .... where a use-case is to investiagte/evlaute 'how close' the latter is to "Wawe-Hedges" and "Euclid" .... where our assumption is that the latter 'appraoch' may be used as an interpolation/approximaionat wrt. accurate simliarty-metrics, an hypotehsis which we investgiaotn wrt. our .....??....
  e_ktSim_typesOf_classes_squared, //! eg, "divergence"="sum((v1-v2)/((v1+v2)^2))"
  e_ktSim_typesOf_classes_fidelity, //! eg, "Hellinger"="sqrt(sum((sqrt(v1)-sqrt(v2))^2)"
  e_ktSim_typesOf_classes_fidelity_squared, //! an oekseth-metric, where "Hellinger" is combined with "divergence", ie, "sqrt(sum((sqrt(v1)-sqrt(v2))/(sqrt(v1)+sqrt(v2))^2))"
  e_ktSim_typesOf_classes_shannon, //! ie, .... eg, "Shannon"="sum((v1-v2)*log_abs(v1/v2) )"
  e_ktSim_typesOf_classes_shannon_minkowski, //! an oekseth-permtaution where Shannon is combined with Minkowski/Euclid: score="sum((v1-v2)^2*log_abs((v1/v2)^2 )"
  e_ktSim_typesOf_classes_dotProd, //! eg, "inner-product"="v1*v2" metrics.
  e_ktSim_typesOf_classes_downwardMutability, //! eg, "Vicis-symmetric-max"="sum(v1-v2)^2/max(v1, v2)", where we oberve that the latter is a pmeraaution of "minkowski", ie, where "min-max Minksowki" is combined with "sum-minkowski
  e_ktSim_typesOf_classes_undef,
} e_ktSim_typesOf_classes_t;

//! @return a stirng-represnetion of the "e_ktSim_typesOf_classes_t" enum
static const char *getString__e_ktSim_typesOf_classes(e_ktSim_typesOf_classes_t enum_id) {
  if(enum_id == e_ktSim_typesOf_classes_abs) {return "abs";}
  if(enum_id == e_ktSim_typesOf_classes_abs_pow) {return "abs-pow";}
  if(enum_id == e_ktSim_typesOf_classes_abs_pow_max) {return "abs-pow-max";}
  if(enum_id == e_ktSim_typesOf_classes_abs_pow_normalized) {return "abs-pow-normalized";}
  if(enum_id == e_ktSim_typesOf_classes_abs_relative) {return "abs-relative";}
  else if(enum_id == e_ktSim_typesOf_classes_intersection) {return "intersect";}
  else if(enum_id == e_ktSim_typesOf_classes_intersection_euclid) {return "intersect-Euclid";}
  else if(enum_id == e_ktSim_typesOf_classes_squared) {return "squared";}
  else if(enum_id == e_ktSim_typesOf_classes_fidelity) {return "fidelity";}
  else if(enum_id == e_ktSim_typesOf_classes_fidelity_squared) {return "fidelity-squared";}
  else if(enum_id == e_ktSim_typesOf_classes_shannon) {return "shannon";}
  else if(enum_id == e_ktSim_typesOf_classes_shannon_minkowski) {return "shannon-minkowski";}
  else if(enum_id == e_ktSim_typesOf_classes_dotProd) {return "dot-product";}
  else if(enum_id == e_ktSim_typesOf_classes_downwardMutability) {return "downward-mutabiliy";}  
  else if(enum_id == e_ktSim_typesOf_classes_undef) {return "undef";}
  /* else if(enum_id == e_ktSim_typesOf_classes_) {return "";} */
  /* else if(enum_id == e_ktSim_typesOf_classes_) {return "";} */
  /* else if(enum_id == ) {return "";} */
  /* else if(enum_id == ) {return "";} */
  fprintf(stderr, "!!\1T Not supproted for enum=%u, ie, investigate, %s:%d\n", enum_id, __FILE__, __LINE__);
  assert(false);
  return "";
}
/**
   @struct s_ktSim_setOf
   @brief desicrbes a sytnitec simlairty-metric (oekseth, 06. apr. 2017).
   @remarks provide lgoics to classify/combine diffenret metric-spaces wrt. 
 **/
typedef struct s_ktSim_setOf {
  s_ktSim_t numerator;
  s_ktSim_t deNumerator;
  e_ktSim_setOf_typeOfComb_t typeOf_merge;
  bool isTo_forMerge__insideEq; //! where alternative is to 'perofmr' the latter in the post-processing-step
  e_ktSim_vector_summation_t isTo_forMerge__insideEq_typeOf; //! where alternative is to 'perofmr' the latter in the post-processing-step
  t_float after_numEnum_isMerged_1_mul;
  t_float scalar_2_offset; //! eg, "+1" for "Wave-Hedges".
  //!
  //! Reference-object to be used
  struct s_ktSim_setOf *parentToUse;
  //bool parentToUse__isTo_forMerge__insideEq; 
  e_ktSim_setOf_typeOfComb_t parentToUse__after_numEnum_isMerged_typeOf; 
  t_float parentToUse__after_numEnum_isMerged_1_mul; 
  //!
  //! Optiona post-step-operaitons to use:
  t_float postStep_pow;
} s_ktSim_setOf_t;


/* #define MF__s_ktSim__() ({;}) */
/* #define MF__s_ktSim__() ({;}) */




/**
   @param <enum_id>  is the tsytnteci enum-di to cosntruct for
   @param <preAllocated_objectInRequsiveSpec> a stack/pre-allcoated "s_ktSim_setOf_t" object whcih we (for the mreo compelx sim-syn-sim-emtricas) update with a 'recuive-step'
   @return an intlaised speficiciaon-root-object.
 **/
s_ktSim_setOf_t init__s_ktSim_setOf_t(const e_ktSim_typesOf_classes_t enum_id, s_ktSim_setOf_t *preAllocated_objectInRequsiveSpec) {
//s_ktSim_setOf_t init__s_ktSim_setOf_t(const e_ktSim_typesOf_classes_t enum_id, const bool isTo_useMax, s_ktSim_setOf_t preAllocated_objectInRequsiveSpec) {
  s_ktSim_setOf_t self;
  //! Defualt intalization:
  self.numerator = init__s_ktSim_t();
  self.deNumerator = init__s_ktSim_t();
  self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerator;
  self.isTo_forMerge__insideEq = false;
  //  assert(false); // fXIEM: ensure athat we use 8below] and [ªbove] parameters correctly
  self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_undef;
  self.after_numEnum_isMerged_1_mul = 0; //! where latter idnidates tha tthe 'score' is Not to be appleid
  self.scalar_2_offset = 0;
  //!
  //! Enable the use of 'reqcursive objects':
  self.parentToUse = NULL;

  // assert(false); // FIXME: validte that [”elow] is correclty 'set' for all of our evlauaiton-cases.
  self.parentToUse__after_numEnum_isMerged_typeOf = e_ktSim_setOf_typeOfComb_numerDenum_mult_both;
  self.parentToUse__after_numEnum_isMerged_1_mul = 0;
  //self.parentToUse__isTo_forMerge__insideEq = false;
  //!
  //! Optiona post-step-operaitons to use:
  self.postStep_pow = 0;
  //! --------------------------------------------------------------------------------------
  //!
  //! Support lgoicsal operaiotns: 
  if(enum_id == e_ktSim_typesOf_classes_abs) {
    s_ktSim_t obj = self.numerator;
    MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1,  /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_abs, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    /* self.numerator.merge_3_pow = 1; */
    /* self.numerator.postStep_1_pow = 1; */
  } else if(enum_id == e_ktSim_typesOf_classes_abs_pow_max) { //! ie, Minkowski, eg, "Euclid"="sqrt(max(v1-v2)^2)", where latter 'represent' the de-numerator in the "Vicis-symmetric-max" simlairty-metric
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/0.5, /*forAll=*/e_ktSim_vector_summation_max, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_abs, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    //self.numerator.each_updateResult = e_ktSim_vector_summation_max;
  } else if(enum_id == e_ktSim_typesOf_classes_abs_pow_normalized) { //! eg, "Euclid(weighted)"="sqrt(sum(v1-v2)^2)/(sum(0.5*(v1+v2)))"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/0.5, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_abs, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    //! 
    //! Then updat ethe 'second object':
    obj = self.deNumerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/0.5, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_pluss, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_div;
    //self.numerator.each_updateResult = e_ktSim_vector_summation_max;
  } else if(enum_id == e_ktSim_typesOf_classes_abs_relative) { //! eg, for the metirc-class of "absolute-difference", where 'abs' is used in the numeator, while a 'non-abs-adjsuted operator is used as the de-numerator. ... eg, "Sorensen" = "sum(abs(v1 - v2))/sum((v1 + v2))"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_abs, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    //! 
    //! Then updat ethe 'second object':
    obj = self.deNumerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_pluss, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_div;
  } else if(enum_id ==   e_ktSim_typesOf_classes_intersection) { //! eg, "Wave-Hedges"="sum(1 - (min(v1, v2)/max(v1, v2)))"
  // FIXME[aritlce]: investigate wrt. [below] enum ... trying to 'capture' pattersn to be used in idneitficioant of new/future/improved silmiaryt-metrics.
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_min, /*pow-each=*/1);
    obj           = self.deNumerator;   MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_max, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_div;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_sum;
    self.after_numEnum_isMerged_1_mul = -1;
    self.scalar_2_offset = 1;
  } else if(enum_id ==   e_ktSim_typesOf_classes_intersection_euclid) { //! an experimental modifcation by oekseth: "Wave-Hedges+Euclid"="sqrt(sum((v1-v2)^2*(1 - (min(v1, v2)/max(v1, v2)))))" .... where a use-case is to investiagte/evlaute 'how close' the latter is to "Wawe-Hedges" and "Euclid" .... where our assumption is that the latter 'appraoch' may be used as an interpolation/approximaionat wrt. accurate simliarty-metrics, an hypotehsis which we investgiaotn wrt. our .....??....
    assert(preAllocated_objectInRequsiveSpec);
    //!
    //! First a recuivsive call:
    // FIXME: below sep seems wrong ... as tjhe oejbect is arleady allcoed ... trye removing below:
    *preAllocated_objectInRequsiveSpec = init__s_ktSim_setOf_t(e_ktSim_typesOf_classes_intersection, NULL); //! wher elatter 'null-variable' is used to give a 'ahrd-break' for inficnte/muliple requcives calls.
    //! Update the resuvie object-spec wrt. [ªbove]:
    self.parentToUse = preAllocated_objectInRequsiveSpec;
    self.parentToUse->isTo_forMerge__insideEq = true; //! ie, update the 'internal equations.
    //self.parentToUse__isTo_forMerge__insideEq = true; 
    self.parentToUse__after_numEnum_isMerged_1_mul = -1;
    //!
    //! Set the non-resuive 'spec':
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    //obj           = self.deNumerator;   MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_max, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_undef;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_sum;
    self.after_numEnum_isMerged_1_mul = 0;
    self.scalar_2_offset = 0;
    self.postStep_pow = 0.5; //! ie, comptue the squart(...) for the result.
  } else if(enum_id ==   e_ktSim_typesOf_classes_squared) { //! eg, "divergence"="sum((v1-v2)/((v1+v2)^2))"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    obj           = self.deNumerator;   MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_pluss, /*pow-each=*/2);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_div;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_sum;
  } else if(enum_id ==   e_ktSim_typesOf_classes_fidelity) { //! eg, "Hellinger"="sqrt(sum((sqrt(v1)-sqrt(v2))^2)"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/0.5);
  } else if(enum_id ==   e_ktSim_typesOf_classes_fidelity_squared) { //! an oekseth-metric, where "Hellinger" is combined with "divergence", ie, "sqrt(sum((sqrt(v1)-sqrt(v2))/(sqrt(v1)+sqrt(v2))^2))"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/0.5);
    obj         = self.deNumerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_pluss, /*pow-each=*/0.5);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_div;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_sum;
  } else if(enum_id ==   e_ktSim_typesOf_classes_shannon) { //! ie, .... eg, "Shannon"="sum((v1-v2)*log_abs(v1/v2) )"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    obj         = self.deNumerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_log_10, /*operator=*/e_ktSim_join_div, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_mult_both;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_sum;
  } else if(enum_id ==   e_ktSim_typesOf_classes_shannon_minkowski) { //! an oekseth-permtaution where Shannon is combined with Minkowski/Euclid: score="sum((v1-v2)^2*log_abs((v1/v2)^2 )"
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    obj         = self.deNumerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_log_10, /*operator=*/e_ktSim_join_div, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_mult_both;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_sum;
  } else if(enum_id ==   e_ktSim_typesOf_classes_dotProd) { //! eg, "inner-product"="v1*v2" metrics.
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_undef, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_mult, /*pow-each=*/1);
  } else if(enum_id ==   e_ktSim_typesOf_classes_downwardMutability) { //! eg, "Vicis-symmetric-max"="sum(v1-v2)^2/max(v1, v2)", where we oberve that the latter is a pmeraaution of "minkowski", ie, where "min-max Minksowki" is combined with "sum-minkowski
    s_ktSim_t obj = self.numerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_sum, /*mult-reachResult=*/1, /*pow-resultEach=*/2, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    obj         = self.deNumerator;     MF__s_ktSim__set(/*pow-post=*/1, /*forAll=*/e_ktSim_vector_summation_max, /*mult-reachResult=*/1, /*pow-resultEach=*/1, /*adjust-result-each=*/e_ktSim_adjust_none, /*operator=*/e_ktSim_join_minus, /*pow-each=*/1);
    //! 
    //! Specify how to 'merge':
    self.typeOf_merge = e_ktSim_setOf_typeOfComb_numerDenum_div;
    self.isTo_forMerge__insideEq_typeOf = e_ktSim_vector_summation_undef;
  } else {
    fprintf(stderr, "!!\t Add support for enum=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false); //! ie, as we then need adding support for this
  }

  // assert(false); // FIXME: validate corrnectess of [ªbove] ... manualily inspecting the configuriaotns ... 

  //!
  //! @return
  return self;
}

static t_float __initDefualtValue_e_ktSim_vector_summation_t(e_ktSim_vector_summation_t enum_id) {
  t_float sum_result = 0;
  //!
  //! Set the 'sum-varable' to a coirrect inti-value:
  if(enum_id      == e_ktSim_vector_summation_min) {sum_result = T_FLOAT_MAX;}
  else if(enum_id == e_ktSim_vector_summation_max) {sum_result = T_FLOAT_MIN_ABS;}
  else if( (enum_id != e_ktSim_vector_summation_sum) && (enum_id != e_ktSim_vector_summation_undef)) {
    assert(false); //! ie, as weh then need adding supoport 'for this'.
  }
  return sum_result;
}

static t_float computeMetric_local_arrOf__s_ktSim_t(const s_ktSim_t self, const t_float *term_1, const t_float *term_2, const uint ncols, t_float *arr_result, const bool isTo_applyPostStep, uint *scalar_cntInteresting) {
  assert(term_1);
  assert(term_2);
  assert(ncols > 0);
  assert(arr_result);
#define __MiF__useScalar(value) ({ (value != 0) && (value != 1);})
  //!
  //! Apply lgoics: 
  uint cnt_evalauted = 0; 
  //! Itnalie the vlaue 'to handle cases of extremenes'.
  t_float sum_result = __initDefualtValue_e_ktSim_vector_summation_t(self.each_updateResult);
  //!
  //!
  for(uint i = 0; i < ncols; i++) {
    t_float score_1 = term_1[i]; t_float score_2 = term_2[i];
    if(isOf_interest(score_1) && isOf_interest(score_2)) {
      //!      
      if(__MiF__useScalar(self.each_pow)) { //! then adjsut sepeatley each of the 'input-values':
	score_1 = mathLib_float_pow(score_1, self.each_pow);
	score_2 = mathLib_float_pow(score_2, self.each_pow);
      }
      t_float result = 0;
      //!
      //! Unify the two scalars:
      assert(self.merge_1 != e_ktSim_join_undef);
      if(     self.merge_1 == e_ktSim_join_minus) {result = score_1 - score_2;}
      else if(self.merge_1 == e_ktSim_join_pluss) {result = score_1 + score_2;}
      else if(self.merge_1 == e_ktSim_join_mult) {result = score_1 * score_2;}
      else if(self.merge_1 == e_ktSim_join_div) {result = ( (score_1 != 0) && (score_2 != 0)) ? score_1 / score_2 : T_FLOAT_MAX;}
      else if(self.merge_1 == e_ktSim_join_min) {result = macro_min(score_1 , score_2);}
      else if(self.merge_1 == e_ktSim_join_max) {result = macro_max(score_1 , score_2);}
      else {assert(false);} //! ie, as we then need adding support for this enum.
      //!
      //! Handle the case/situation where the [ªbov€] result was 'meanbingfuless':
      if(isOf_interest(result) == false) {continue;} //! ie, as we have then 'ifnerred' tha tthe rsult is no longer of interest.
      //!
      //! Adjsut the values:
      if(self.merge_2 != e_ktSim_adjust_none) {
	assert(self.merge_2 != e_ktSim_adjust_undef);
	if(self.merge_2 == e_ktSim_adjust_abs) { result = mathLib_float_abs(result);
	} else if(self.merge_2 == e_ktSim_adjust_log_10) {result = (result != 0) ? mathLib_float_log_abs(result) : T_FLOAT_MAX;
	} else if(self.merge_2 == e_ktSim_adjust_log_2) {result = (result != 0) ? mathLib_float_log_abs(result) / mathLib_float_log(2): T_FLOAT_MAX;
	} else {assert(false);} //! ie, as we then need adding support for this enum.	
      }
      //!
      //! Handle the case/situation where the [ªbov€] result was 'meanbingfuless':
      if(isOf_interest(result) == false) {continue;} //! ie, as we have then 'ifnerred' tha tthe rsult is no longer of interest.
      //!
      //! Change the relative weihts of the reuslt, ie, use "exp(..)":
      if(__MiF__useScalar(self.merge_3_pow) && __MiF__useScalar(result)) { //! then adjsut the rsult-scalar.
	result = mathLib_float_pow(result, self.merge_3_pow);
      }
      //!
      //! Update:
      if(arr_result) {arr_result[i] = result;}
      if(isOf_interest(result) && (self.each_updateResult != e_ktSim_vector_summation_undef)) {
	if(self.each_updateResult      == e_ktSim_vector_summation_min) {sum_result = macro_min(sum_result, result);}
	else if(self.each_updateResult == e_ktSim_vector_summation_max) {sum_result = macro_max(sum_result, result);}
	else if( (self.each_updateResult == e_ktSim_vector_summation_sum) ) {sum_result += result;} 
	/* else if(self.each_updateResult == e_ktSim_vector_summation_div) { assert(false); /\*todo: cosnider adding support*\/;} */
	/* else if(self.each_updateResult == e_ktSim_vector_summation_div_minMax) { ;} */
	/* else if(self.each_updateResult == e_ktSim_vector_summation_mult_both) { ;} */
	/* else if(self.each_updateResult == e_ktSim_vector_summation_mult_log_result2) { ;} */
	//else if(self.each_updateResult == ) { ;}
	else {
	  assert(false); //! ie, as weh then need adding supoport 'for this'.
	}	
      }
      cnt_evalauted++;
    }
  }
  //! -------------------------------------------
  //!
  //! Update caller-containers, and apply (if reuqested) a psot-.processing-step:
  *scalar_cntInteresting = cnt_evalauted;
  if(isOf_interest(sum_result) && (sum_result != 0) && (sum_result != T_FLOAT_MIN_ABS) && isTo_applyPostStep) { //! then we apply  psot-step-adjustment
    if(__MiF__useScalar(self.postStep_1_pow)) {
      sum_result = mathLib_float_pow(sum_result, self.postStep_1_pow);
    }
    if(isOf_interest(sum_result) && (sum_result != T_FLOAT_MIN_ABS)) {
      if(self.postStep_adjsutBy_vertexCount) {
	assert(cnt_evalauted != 0); //! ie, as latter woudl idnicated an inc-osnstecy in [ªbove].
	sum_result = sum_result / (t_float)cnt_evalauted;
      }
    }
  }
  //!
  //!
  //! @return
  return sum_result;
}


//!
//! Merge the results by 'either' using a 'serpate eahc-stpe' OR merge 'direclty' using the result-varialbe.
//! Note: the "arr_result_2" is optional: if Not set, then we use the result_1 variable.
static t_float __getScore__postProcessMerge(e_ktSim_setOf_typeOfComb_t typeOf_merge, const bool isTo_forMerge__insideEq, t_float *arr_result_1, t_float *arr_result_2, const t_float result_1, const t_float result_2, const uint ncols) {
  assert(arr_result_1); //! where "arr_result_2" is optioanl.
  //! Note: Itnalie the vlaue 'to handle cases of extremenes'.
  t_float sum_result = 0; //__initDefualtValue_e_ktSim_vector_summation_t(isTo_forMerge__insideEq_typeOf);
  if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_max) {
    sum_result = T_FLOAT_MIN_ABS;
  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_minus) {
    sum_result = T_FLOAT_MAX;
  }
  if(typeOf_merge != e_ktSim_setOf_typeOfComb_numerator) {
    if(isTo_forMerge__insideEq == true) {
      for(uint i = 0; i < ncols; i++) {
	t_float score_1 = arr_result_1[i]; t_float score_2 = (arr_result_2) ? arr_result_2[i] : result_2;
	if(isOf_interest(score_1) && isOf_interest(score_2)) {
	  if( (score_1 != 0) && (score_2 != 0) ) {
	    assert(score_1 != T_FLOAT_MIN_ABS);
	    assert(score_2 != T_FLOAT_MIN_ABS);
	    //! Apply logics:
	    if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_div) {
	      arr_result_1[i] = score_1 / score_2;
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_div_minMax) {
	      arr_result_1[i] = macro_min(score_1, score_2) / macro_max(score_1, score_2);
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_mult_both) {
	      arr_result_1[i] = score_1 * score_2;
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_mult_log_denum) {
	      arr_result_1[i] = score_1 * mathLib_float_log_abs(score_2);
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_max) {
	      arr_result_1[i] = macro_max(score_1, score_2);
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_min) {
	      arr_result_1[i] = macro_min(score_1, score_2);
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_pluss) {
	      arr_result_1[i] = macro_pluss(score_1, score_2);
	    } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_minus) {
	      arr_result_1[i] = macro_minus(score_1, score_2);
	    } else {
	      assert(false); //! ie, as weh then need aeddding support for this case.
	    }
	  }
	}
      }
    } else {
      if(isOf_interest(result_1) && isOf_interest(result_2)) {
	if((result_1 != 0) && (result_2 != 0)) {
	  if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_div) {
	    sum_result = result_1 / result_2;
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_div_minMax) {
	    sum_result = macro_min(result_1 , result_2) / macro_max(result_1 , result_2);;
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_mult_both) {
	    sum_result = result_1 * result_2;
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_mult_log_denum) {
	    sum_result = result_1 * mathLib_float_log_abs(result_2);
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_max) {
	    sum_result = macro_max(result_1, result_2);
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_min) {
	    sum_result = macro_min(result_1, result_2);
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_pluss) {
	    sum_result = macro_pluss(result_1, result_2);
	  } else if(typeOf_merge == e_ktSim_setOf_typeOfComb_numerDenum_minus) {
	    sum_result = macro_minus(result_1, result_2);
	  } else {
	    assert(false); //! ie, as weh then need aeddding support for this case.
	  }
	}
      }
    }    
  }
  if(sum_result == T_FLOAT_MIN_ABS) {sum_result = T_FLOAT_MAX;} //! ie, to simplfiy our value-cheinign-proceud.re
  return sum_result;
}

/* static t_float computeMetric_local(const t_float term_1, const t_float term_2) { */
  
/* } */
/**
   @brief comptue for a simalirty-metirc using a non-optmzied (through geneirc) simalirty-emtric-proceudre (oekseh, 06. apr. 2017).
 **/
static t_float computeMetric_local_arrOf__s_ktSim_setOf_t(const s_ktSim_setOf_t self, const t_float *term_1, const t_float *term_2, const uint ncols, t_float *arr_result_1, t_float *arr_result_2) {
  assert(term_1); assert(term_2); assert(ncols > 0);
  //!
  //! Allocate tmeporayr cotnainers: 
  const t_float empty_0 = 0;
  bool arrs_allcotedByCaller = (arr_result_1 != NULL);
  if(arrs_allcotedByCaller == false) {assert(arr_result_2 == NULL);} //! ei, as we expec tcontaintsy beteeen the latter
  else {assert(arr_result_2 != NULL);} //! ei, as we expec tcontaintsy beteeen the latter
  if(arrs_allcotedByCaller == true) {
    arr_result_1 = allocate_1d_list_float(empty_0, ncols); //for(uint i = 0; i < ncols; i++) {arr_local[i] = 
    assert(arr_result_1);
    arr_result_2 = allocate_1d_list_float(empty_0, ncols); //for(uint i = 0; i < ncols; i++) {arr_local[i] = 
    assert(arr_result_2);
  }
  t_float result_1 = 0;   t_float result_2 = 0;
  //! 
  {//! Copmtue for 'this':
    uint result_1_cntInteresting = 0; uint result_2_cntInteresting = 0;
    //const bool isTo_applyPostStep = self.parentToUse__isTo_forMerge__insideEq;
    const bool isTo_applyPostStep = self.isTo_forMerge__insideEq;
    result_1 = computeMetric_local_arrOf__s_ktSim_t(self.numerator, term_1, term_2, ncols, arr_result_1, isTo_applyPostStep, &result_1_cntInteresting);
    if( (self.typeOf_merge != e_ktSim_setOf_typeOfComb_numerator) && (self.typeOf_merge != e_ktSim_setOf_typeOfComb_undef) ) {
      result_2 = computeMetric_local_arrOf__s_ktSim_t(self.deNumerator, term_1, term_2, ncols, arr_result_2, isTo_applyPostStep, &result_2_cntInteresting);
    }
  }

  //!
  //! Merge the results by 'either' using a 'serpate eahc-stpe' OR merge 'direclty' using the result-varialbe.
  result_1 = __getScore__postProcessMerge(self.typeOf_merge, self.isTo_forMerge__insideEq, arr_result_1, arr_result_2, result_1, result_2, ncols);
  
  t_float *arr_result_1_rec = NULL; t_float *arr_result_2_rec = NULL; t_float result_rec = 0;
  if(self.parentToUse != NULL) {
    arr_result_1_rec = allocate_1d_list_float(empty_0, ncols); //for(uint i = 0; i < ncols; i++) {arr_local[i] = 
    arr_result_2_rec = allocate_1d_list_float(empty_0, ncols); //for(uint i = 0; i < ncols; i++) {arr_local[i] = 
    // FIXME: for what cases are the below recusive step used?
    result_rec = computeMetric_local_arrOf__s_ktSim_setOf_t(*(self.parentToUse), term_1, term_2, ncols, arr_result_1_rec, arr_result_2_rec);
  }

  //!
  //! Merge the results:
  if(self.parentToUse != NULL) {

    assert(false); // FIXMe[cocneptaul]: descfribe a 'cpocentaul' use-case wrt. [below] ... ie, to 'gain' from oru cocnetual work wrt. the compelxiteis which introduce wrt. 'reucisrive bocmination-step'.

    if(self.parentToUse->isTo_forMerge__insideEq == true) {
      //!
      //! Merge the results by 'either' using a 'serpate eahc-stpe' OR merge 'direclty' using the result-varialbe.
      if(self.isTo_forMerge__insideEq) { //! then 'both are lists':
	result_1 = __getScore__postProcessMerge(self.parentToUse__after_numEnum_isMerged_typeOf, /*isTo_forMerge__insideEq=*/true, arr_result_1, arr_result_1_rec, result_1, result_rec, ncols);
      } else { //! then 'second' is Not a list:
	result_1 = __getScore__postProcessMerge(self.parentToUse__after_numEnum_isMerged_typeOf, /*isTo_forMerge__insideEq=*/true, arr_result_1_rec, NULL, result_1, result_rec, ncols);
      }      
    } else {
      if(self.isTo_forMerge__insideEq) { //! then 'second' is Not a list:
	result_1 = __getScore__postProcessMerge(self.parentToUse__after_numEnum_isMerged_typeOf, /*isTo_forMerge__insideEq=*/true, arr_result_1, /*arr_result_1_rec=*/NULL, result_1, result_rec, ncols);
      } else { //! then 'none' are lists:
	result_1 = __getScore__postProcessMerge(self.parentToUse__after_numEnum_isMerged_typeOf, /*isTo_forMerge__insideEq=*/false, NULL, /*arr_result_1_rec=*/NULL, result_1, result_rec, ncols);
      }
    }
  } //! else we asusme 'this' has been addressed/handled in [ªbove]

  //!
  //!
  //! Handle cases where we searhc for 'extrme' cases (ie, as we 'oterhiwse' could have performed the summaiton in [abov€]):
  t_float sum_result = 0;
  if(self.isTo_forMerge__insideEq_typeOf != e_ktSim_vector_summation_undef) {
    //! Then we 'leap through' the list, finding the interestin value
    //! Note: Itnalie the vlaue 'to handle cases of extremenes'.
    sum_result = __initDefualtValue_e_ktSim_vector_summation_t(self.isTo_forMerge__insideEq_typeOf);
    
    for(uint i = 0; i < ncols; i++) {
      const t_float result = arr_result_1[i]; 
      if(isOf_interest(result)) {
	if(self.isTo_forMerge__insideEq_typeOf        == e_ktSim_vector_summation_min) {sum_result = macro_min(sum_result, result);}
	else if(self.isTo_forMerge__insideEq_typeOf   == e_ktSim_vector_summation_max) {sum_result = macro_max(sum_result, result);}
	else if( (self.isTo_forMerge__insideEq_typeOf == e_ktSim_vector_summation_sum) ) {sum_result += result;} 
	/* else if(self.each_updateResult == e_ktSim_vector_summation_div) { assert(false); /\*todo: cosnider adding support*\/;} */
	/* else if(self.each_updateResult == e_ktSim_vector_summation_div_minMax) { ;} */
	/* else if(self.each_updateResult == e_ktSim_vector_summation_mult_both) { ;} */
	/* else if(self.each_updateResult == e_ktSim_vector_summation_mult_log_result2) { ;} */
	//else if(self.each_updateResult == ) { ;}
	else {
	  assert(false); //! ie, as weh then need adding supoport 'for this'.
	}	
      }
    }
    if(sum_result == T_FLOAT_MIN_ABS) {sum_result = T_FLOAT_MAX;} //! ie, to simplify/reudce complexity wrt. reuslt-valeus/scores.
  } else {
    sum_result = result_1;
  }
  //! 
  //!  Investigate if we are to ajdust the reuslt-score.
  if(isOf_interest(sum_result) && (sum_result != 0)) {
    if(__MiF__useScalar(self.postStep_pow)) {
      sum_result = mathLib_float_pow(sum_result, self.postStep_pow);
    }
  }
  //!
  if(arrs_allcotedByCaller == true) {  //! De-allocae and return:
    free_1d_list_float(&arr_result_1); arr_result_1 = NULL;
    free_1d_list_float(&arr_result_2); arr_result_2 = NULL;
  }
  if(self.parentToUse != NULL) { //! De-allocae and return:
    assert(arr_result_1_rec);
    free_1d_list_float(&arr_result_1_rec); arr_result_1_rec = NULL;
    free_1d_list_float(&arr_result_2_rec); arr_result_2_rec = NULL;
  }
  return sum_result;
}


//! Apply the lcoal-cosntucted feautre-amtrix to eihter a 'real-world' simalrity-emtirc or a sytnitec simlairty-emtirc:
static void computeForSynmetric__synData__s_ktSim_setOf_synData_spec_t(s_ktSim_setOf_synData_spec_t *self, const e_ktSim_typesOf_classes_t syn_metric, e_kt_correlationFunction_t metric_id, e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {
  assert(self);

  s_ktSim_setOf_t obj_recurisve = init__s_ktSim_setOf_t(e_ktSim_typesOf_classes_abs, NULL);
  s_ktSim_setOf_t obj_main;
  if(metric_id == e_kt_correlationFunction_undef) { //! then we itnalise our synetci metric:
    obj_main = init__s_ktSim_setOf_t(syn_metric, &obj_recurisve);
  }
  //!
  //! Iterat through the diffneret funciton-cases:
  const uint cases1_cnt = self->local_conf.cases1_cnt;
  uint currentPos__col = 0;
  for(uint i = 0; i < cases1_cnt; i++) {
    //! 
    //! Note: to ensure a 'none-overlappign' wrt. our emasurement-evlauations (ie,  consistent increase in value-difference) we start the "k" iteraiton on "i+1": latter implies that the proerpty of "score_1 <= score_2" will always hold, ie, for which a high-numbered/indexed feature-positon/score  is associated to an increasing dot-product-value. Simliarty we for increasing 'i' 
    // TDOO[eval]: evlauate correnctess of [ªbov€] and [”elow] 'assumpiton' wrt. "i+1" ... and then update both oru docuemtatnion and our aritlce.
    for(uint k = i+1; k < cases1_cnt; k++) {
      //! 
      //! Fetch the scores:
      const uint ncols_local = self->local_conf.ncols_local; const t_float empty_0 = 0;
      t_float *row_1 = allocate_1d_list_float(ncols_local, empty_0); t_float *row_2 = allocate_1d_list_float(ncols_local, empty_0);
      //! Construct a feature-vector for the givne row-spec ("ktSim_setOf_synData_spec.c").
      // FIXME: article: represetnvieness: why is below cosntructed aritla-daa-sets representive? ... due to the different seperations we exploe? ... 
      // FIXME: new-tut: ... InfoVis: useful to print out below data-sets, and then use the datasets as an exmapel of Artifical Data? ... eg, to aim at idnetifying for each sim-metric-combination the data-id which mazimies the sepration to the other sim-metrics ... ie, [data-id][sim-metric] = score ... tranpse into [sim-metric][data-id] ... apply aveage rmsopation (to avodi issues in scales), and then compute [sim-metric][data-id] = max(data-id, sim-metirc(k)/sim-meric(this)) ... where result is matrix(relatvie)[sim-metric][data-id] ... construct a heatmap with [sim-metric][data-id] = sim-metric-ID (max-seperation) ... then construct an SLINK-gaph for [data-id][data-id] relationships ... possible to relatve preopteis of the data to their realtionships? ... if there is no patterns (or: agreement) in data, then have an example of a case where ...??... .... % FIXME(assumption): there are data-id which are strongly dfiferent from each other ... 
      constructFeatureVectors__s_ktSim_setOf_synData_spec_t(self, row_1, row_2, i, k);

      /* if(self->isTo_storeFeatures) { */
      /* 	if(self->cntSimMetricsEvaluated == 0) { //! Store the rows into the local data-set */
      /* 	  if(i == 0) { */
      /* 	    assert(row_1); */
      /* 	    assert(false); // FIXME: add somethign	       */
      /* 	  } */
      /* 	  if(k == (cases1_cnt-1)) { */
      /* 	    assert(row_1); */
      /* 	    assert(false); // FIXME: add somethign	       */
      /* 	  } */
      /* 	} */
      /* } */
      //!
      //! Get a 'shrot-hand' tot hte global index-pos to access/update:
      const uint row_id = self->cntSimMetricsEvaluated;       assert(row_id < self->matrixOf_features.nrows); 
      const uint col_id = currentPos__col;       assert(col_id < self->matrixOf_features.ncols);
      
      //! 
      //! Comptue the simlairty-metirc:
      t_float simMetric_score = T_FLOAT_MAX;
      if(metric_id != e_kt_correlationFunction_undef) {
	const bool is_ok = apply__rows_hp_distance(initAndReturn__s_kt_correlationMetric_t(metric_id, typeOf_correlationPreStep), row_1, row_2, ncols_local, &simMetric_score);
	assert(is_ok);
	if(col_id == 0) { //! then we set the row-string:
	  allocOnStack__char__sprintf(8000, string, "%s %s", get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(typeOf_correlationPreStep));
	  //const char *string = get_stringOf_enum__e_kt_correlationFunction_t(metric_id);
	  set_stringConst__s_kt_matrix(&(self->matrixOf_features), row_id, string,  /*addFor_column=*/false);
	}
      } else {
	simMetric_score = computeMetric_local_arrOf__s_ktSim_setOf_t(obj_main, row_1, row_2, ncols_local, NULL, NULL);
	if(col_id == 0) { //! then we set the row-string:
	  const char *string = getString__e_ktSim_typesOf_classes(syn_metric);
	  set_stringConst__s_kt_matrix(&(self->matrixOf_features), row_id, string, /*addFor_column=*/false);	  
	}
      }

      //! 
      {//! Set string-dienitfers, ie, if latter has Not ealreyd been set:
	/* if(row_id == 0) { //! then we set the column-names */
	// TODO: cosndier adding something.
	/* } */
	//assert(false); // FIXME: add somethign	
      }
      if(simMetric_score != T_FLOAT_MAX) {	
	//! 
	//! Update the result-matrix:
	self->matrixOf_features.matrix[row_id][col_id] = simMetric_score;	       
      }
      //! Icnrement column-pos:
      currentPos__col++;
      //!
      //! De-allcoate the local rows:
      assert(row_1); assert(row_2);
      free_1d_list_float(&row_1); row_1 = NULL;
      free_1d_list_float(&row_2); row_2 = NULL;
    }
  }		
  self->cntSimMetricsEvaluated++;
}



//! Consturct matrices desicrinb the feautre-vectors we evlauate, and write out facts/knowledge of the latter:
static void __writeOut__featureMatrix(const uint cnt_splits, const uint factorMul_cnt, const t_float factorMul_stepSize, const uint cases_factor_increase_cnt, const t_float cases_factor_increase_mult, const uint multFactor, const uint cases_factor_increase_typeOf_multInner, const uint cases1_cnt, const uint ncols_local, const char *resultPrefix, const bool isTo_writeOutTransposed) {
  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_postProcess, arrOf_sim_postProcess_size);
  //! ----------
  //! Allocate a matrix for 'ufniciaotn' fo the elvauaiton-results:
  s_kt_matrix_t matrix_merged = setToEmptyAndReturn__s_kt_matrix_t();  
  //!
  //! Iterate:
  for(uint split_id = 0; split_id < cnt_splits; split_id++) {
    //! Itnaie a splitting-array:
    uint indexPos_posToInvert_size = 0; uint *indexPos_posToInvert = get_splitArray_fromScalar__s_ktSim_setOf_synData_spec__localConf_t(split_id, &indexPos_posToInvert_size, ncols_local);
    //!  Apply lgocis:
    for(uint factorMul = 0; factorMul <= factorMul_cnt; factorMul++) {
      const t_float mulFactor_m = (factorMul < 2) ? ((t_float)factorMul*(t_float)factorMul_stepSize) : (t_float)factorMul; //! wher elatter a'ppraoch' is used to 'ensure' that we invesitgate for a 'straight line'.
      printf("mulFactor_m=%f, at %s:%d\n", mulFactor_m, __FILE__, __LINE__);
      for(uint factor = 0; factor < cases_factor_increase_cnt; factor++) {
	const t_float factor_mult = (factor != 0) ? (t_float)(factor * cases_factor_increase_mult) : 1.0;
	//! 
	//! Itnalise the cofnig-object:
	s_ktSim_setOf_synData_spec_t synData_spec = MF__fromConfig__s_ktSim_setOf_synData_spec_t();
	//!
	//! Compute the feuatre-matrix
	//! Note: the below fucniton calls "constructFeatureVectors__s_ktSim_setOf_synData_spec_t(self, row_1, row_2, i, k)", which is a supersuet of the call found in "computeForSynmetric__synData__s_ktSim_setOf_synData_spec_t(...)".
	s_kt_matrix_t mat_result = constructMAtrixOf_featureScores__s_ktSim_setOf_synData_spec_t(&synData_spec);
	//!
	//! Wirte out proepteis wrt. latter:
	//! Buidl a file-path: 
	allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_featureMatrix_split%u_factorMul_%u_factor_%u", resultPrefix, split_id, factorMul, factor);
	//! The data-consturciton-call:
	writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_result, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
	{ //! Insert our 'new-idienfied matrix' into the result-matrix, where row-naems are udpated with the "prefixInput_rows" string.
	  allocOnStack__char__sprintf(2000, prefixInput_rows, "split:%u, mul:%u", split_id, factorMul);
	  insertMatrix_intoMatrix__s_kt_matrix_t(&matrix_merged, &mat_result, prefixInput_rows);
	}
	//!
	//! De-allocate
	free__s_kt_matrix(&mat_result);
	free__s_ktSim_setOf_synData_spec_t(&synData_spec);
      }
    }
    if(indexPos_posToInvert != NULL) {free_1d_list_uint(&indexPos_posToInvert); indexPos_posToInvert = NULL; indexPos_posToInvert_size = 0;}
  }
  //!
  {//! Wirte out proepteis wrt. latter:
    //! Buidl a file-path: 
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_featureMatrix_merged", resultPrefix);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, matrix_merged, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
    //!
    //! De-allocate
    free__s_kt_matrix(&matrix_merged);
  }
}
//! Describes/idneifies the type of evlauation-aprpaoch to be appleid wrt. the CCMs
typedef enum e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval {
  e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedMatrix__splitClusterSetDataSet_eachRow_increaseRowSize,
  e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit,
} e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t;
//! The type of ocnfiguraiotns in oru evlaiaton-aprproach.
typedef struct s_eval__CCM__hardCodedCases__constructMatrix {
  const char *resultPrefix; s_hp_exportMatrix_t obj_exportMatrix;
  uint cnt_vertices_total;
  uint cnt_metrics; const uint *arrOf_enums;
  t_float score_weak; t_float score_strong;
  e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t typeOf_ccmTest;
} s_eval__CCM__hardCodedCases__constructMatrix_t;

static uint __eval__CCM__hardCodedCases__constructMatrix(s_eval__CCM__hardCodedCases__constructMatrix_t *self,
							 const bool isTo_use_matrixBased, const bool isTo_writeOutTransposed,
							 //const uint cnt_metrics, 
							 const uint cntVertices_cntBlocks,
							 const uint cntVertices_incrementMult_firstVal, const uint cntVertices_incrementMult_each,
							 const uint pos_relativeSplit, const uint v_order, 
							 const uint cnt_clusters_incrementMult, 
							 const uint fraction_verticesInSecondBlock_size,
							 const s_kt_correlationMetric_t metric_rankComparison, 
							 // ---
							 const uint cnt_clustersTotal, 
							 uint cnt_clusters_currentCount
							 ) {
  
#define __MiF__updateClusterCount() ({obj_2_cnt_clusters += 2;})

  //! 
  //! Intalize the result-set:
  s_kt_matrix_t mat_result_matrixBased[e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef];
  s_kt_matrix_t mat_result_setBased[e_kt_matrix_cmpCluster_metric_undef];
  s_kt_matrix_t *mat_result = (isTo_use_matrixBased) ? mat_result_matrixBased : mat_result_setBased;
  for(uint cmpMetric_id = 0; cmpMetric_id < self->cnt_metrics; cmpMetric_id++) { //! where "cnt_metrics" eather refers to $|CCM-gold|$ or $|CCM-matrix|$.
    const uint enum_id = self->arrOf_enums[cmpMetric_id];
    const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)enum_id;
    if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
    const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)enum_id;
    /* const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
    /* if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index) */
    /* const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)cmpMetric_id; */
    mat_result[cmpMetric_id] = initAndReturn__s_kt_matrix(/*nrows=*/cntVertices_cntBlocks, /*ncols=*/fraction_verticesInSecondBlock_size);
    //!
    { //! Set headers:
      //! Buidl a file-path: 
      uint cnt_vertices_total = self->cnt_vertices_total;
      uint index = 0;
      /* for(uint v_count = 1; v_count <= cntVertices_cntBlocks; v_count++) { //! which is the number of blocks to evaluate. */
      /* 	//for(uint i = 0; i < mat_result[cmpMetric_id].nrows; i++) { */
      /* 	allocOnStack__char__sprintf(2000, str, "|vertices|=%u", cnt_vertices_total); */
      /* 	assert(index < mat_result[cmpMetric_id].nrows); */
      /* 	set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), index, str, /\*addFor_column=*\/false); */
      /* 	cnt_vertices_total += cntVertices_incrementMult_each*v_count; */
      /* 	index++; */
      /* } */
      index = 0;
      uint obj_2_cnt_clusters = cnt_clustersTotal;
      for(uint fraction_verticesInSecondBlock = 1; fraction_verticesInSecondBlock < fraction_verticesInSecondBlock_size; fraction_verticesInSecondBlock++) { //! where we in 'this' assume that there should idally be a lienar decrease in closenss between gold-standard VS 'sample-cluster-prediciton-set', ie, where latter assumption/assertion/axiom is an improtnt cosntineutn/part in our CCM-evalaution
	//for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) {
	/* 	const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
	/* //for(uint i = 0; i < mat_result[cmpMetric_id].ncols; i++) { */
	/* 	const char *str = getStringOf__e_kt_matrix_cmpCluster_metric_t__short(metric_clusterCmp); */
	//	  const uint cnt_vertices_second = self->cnt_vertices_total/fraction_verticesInSecondBlock;

	//!
	//! Set colunmn-name depnding on the two different cases which the Artifical Data is construced upon/configured.
	if(self->typeOf_ccmTest == e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit) {
	  __MiF__updateClusterCount(); //! ie, oduling the count.
	  allocOnStack__char__sprintf(2000, str, "|nClusterDiff|=%.2f", (t_float)cnt_clustersTotal/(t_float)obj_2_cnt_clusters);
	  assert(str);
	  assert(index < mat_result[cmpMetric_id].ncols);
	  set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), index, str, /*addFor_column=*/true);
	} else {
	  allocOnStack__char__sprintf(2000, str, "|fraction|=%u", fraction_verticesInSecondBlock);
	  assert(str);
	  assert(index < mat_result[cmpMetric_id].ncols);
	  set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), index, str, /*addFor_column=*/true);
	}
	index++;
      }
    }
  }

  assert(isTo_use_matrixBased); // FIXME: remove

  t_float score_weak = self->score_weak;   t_float score_strong = self->score_strong; 

  for(uint v_count = 1; v_count <= cntVertices_cntBlocks; v_count++) { //! which is the number of blocks to evaluate.	  
    //! Construct the gold-standard:
    printf("v_count=%u, nclusterS=%u, score-range=[%f, %f], at %s:%d\n", v_count, cnt_clustersTotal, score_weak, score_strong, __FILE__, __LINE__);
    //! Note: the [”elow] score and scoreAssignmetn-paramters are ingored wr.t this use-case, ie, as we (in thsi use-case) cosnider/elvuate set-base assuinigments instead of a vertex-based assignemtn.
    s_hp_clusterShapes_t obj_1 = init_andReturn__s_hp_clusterShapes_t(self->cnt_vertices_total, score_weak, score_strong,  (e_hp_clusterShapes_vertexOrder_t)v_order, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    assert(cnt_clustersTotal <= self->cnt_vertices_total);
    const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_1, self->cnt_vertices_total, /*cnt_clusters=*/cnt_clustersTotal, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd=*/0);
    assert(is_ok);
    if(v_count == 1) { //! then we write out the inptu-matrix:
      //!
      //! Buidl a file-path: 
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_goldStandard_ccm_vert_%u_clust_%u_vOrder_%u.tsv", self->resultPrefix, self->cnt_vertices_total, cnt_clustersTotal, v_order);
      //! 
      //! The data-consturciton-call:
      export__singleCall__s_kt_matrix_t(&(obj_1.matrix), resultPrefix_local, NULL); 
    }
    if(false) { //! then we writ eotu the assigned clsuter-memberships:
      for(uint i = 0; i < self->cnt_vertices_total; i++) {
	printf("vertex[%u] = %u, at %s:%d\n", i, obj_1.map_clusterMembers[i], __FILE__, __LINE__);
      }
      assert(false); // TODO: remove when [ªbove] is validated:
    }
    //!
    //! Evaluate for the different CCMs:
    //! ----------------------------------------
    //!
    //! Iterate through the different non-matrix-based measures:
    for(uint cmpMetric_id = 0; cmpMetric_id < self->cnt_metrics; cmpMetric_id++) {
      const uint enum_id = self->arrOf_enums[cmpMetric_id];
      const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)enum_id;
      if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
      //if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(enum_id)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
      const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)enum_id;
      {
	const uint index = v_count - 1;
	if(self->typeOf_ccmTest == e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit) {
	  allocOnStack__char__sprintf(2000, str, "scoreMaxDiff=%.2f", (t_float)score_strong/(t_float)score_weak);	  	  
	  set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), index, str, /*addFor_column=*/false);
	} else {
	  allocOnStack__char__sprintf(2000, str, "|vertices|=%u", self->cnt_vertices_total);
	  set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), index, str, /*addFor_column=*/false);	  
	}
      }

      /* const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
      /* if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)	     */
      /* const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)cmpMetric_id; */
      //mat_result[cmpMetric_id] = initAndReturn__s_kt_matrix(/*nrows=*/cntVertices_cntBlocks, /*ncols=*/fraction_verticesInSecondBlock_size);
      //if(isTo_use_matrixBased && (metric_clusterCmp_matrixBased != e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin)) {continue;} // FIXME: remove
      // if(isTo_use_matrixBased && (metric_clusterCmp_matrixBased != e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE)) {continue;} // FIXME: remove
      //if(isTo_use_matrixBased && (metric_clusterCmp_matrixBased != e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette)) {continue;} // FIXME: remove
      //if(isTo_use_matrixBased && (metric_clusterCmp_matrixBased != e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns)) {continue;} // FIXME: remove
      //	    if(isTo_use_matrixBased && (metric_clusterCmp_matrixBased != e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin)) {continue;} // FIXME: remove
      //	    if(isTo_use_matrixBased && (metric_clusterCmp_matrixBased != e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC)) {continue;} // FIXME: remove
      /* const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
      /* if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index) */
      //! 
      uint obj_2_cnt_clusters = cnt_clustersTotal;
      //! Evaluate different Pertmautiosn to the vertex-set:
      for(uint fraction_verticesInSecondBlock = 1; fraction_verticesInSecondBlock < fraction_verticesInSecondBlock_size; fraction_verticesInSecondBlock++) { //! where we in 'this' assume that there should idally be a lienar decrease in closenss between gold-standard VS 'sample-cluster-prediciton-set', ie, where latter assumption/assertion/axiom is an improtnt cosntineutn/part in our CCM-evalaution
	//! 
	//! Construct the cluster-permtuation:
	//! Note: the [”elow] score and scoreAssignmetn-paramters are ingored wr.t this use-case, ie, as we (in thsi use-case) cosnider/elvuate set-base assuinigments instead of a vertex-based assignemtn.
	s_hp_clusterShapes_t obj_2;
	assert(fraction_verticesInSecondBlock > 0);
	if(self->typeOf_ccmTest == e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit) {
	  obj_2 = init_andReturn__s_hp_clusterShapes_t(self->cnt_vertices_total, /*score-weak=*/score_weak, /*score-strong=*/score_strong, e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	  bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_2, self->cnt_vertices_total, /*cnt_clusters=*/obj_2_cnt_clusters, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd=*/0);
	  assert(is_ok);
	  __MiF__updateClusterCount(); //! ie, oduling the count.
	} else {
	  obj_2 = init_andReturn__s_hp_clusterShapes_t(self->cnt_vertices_total, /*score-weak=*/10, /*score-strong=*/1, e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_sameScore);
	  if(fraction_verticesInSecondBlock != 1) {//! Update the assignments for the last cluster-chunk:
	    const uint cnt_vertices_second = self->cnt_vertices_total/fraction_verticesInSecondBlock;
	    assert(cnt_vertices_second > 1);
	    uint cnt_clusters_second = 0;
	    if(isTo_use_matrixBased) {
	      cnt_clusters_second = cnt_clustersTotal*(1+pos_relativeSplit);	  
	    } else { //! then we ened a 'cosnsitny' between the two clsuter-objects.
	      cnt_clusters_second = cnt_clustersTotal/(1+pos_relativeSplit);
	      assert(cnt_clusters_second < cnt_clustersTotal);
	    }
	    // printf("cnt_clusters_second=%u, cnt_vertices_second=%u, pos_relativeSplit=%u at %s:%d\n", cnt_clusters_second, cnt_vertices_second, pos_relativeSplit, __FILE__, __LINE__);
	    cnt_clusters_second = macro_min(cnt_clusters_second, cnt_vertices_second);
	    assert(cnt_clusters_second > 1);
	    const uint cnt_vertices_first = self->cnt_vertices_total - cnt_vertices_second;
	    uint cnt_clusters_first = cnt_clustersTotal - cnt_clusters_second;
	    cnt_clusters_first = macro_min(cnt_clusters_first, cnt_vertices_first);
	    assert(cnt_vertices_first <= self->cnt_vertices_total);
	    if(isTo_use_matrixBased == false) {
	      assert(cnt_clusters_first <= cnt_clustersTotal);
	    }
	    assert(cnt_clusters_first <= cnt_vertices_first);
	    bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_2, cnt_vertices_first, /*cnt_clusters=*/cnt_clusters_first, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd=*/0);
	    assert(is_ok);
	    assert(cnt_clusters_second <= cnt_vertices_second);
	    is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_2, cnt_vertices_second, /*cnt_clusters=*/cnt_clusters_second, /*cnt_clusterStartOffset=*/cnt_clusters_first, /*vertexCount__constantToAdd=*/0);
	    assert(is_ok);
	    if(false) { //! then write out the input-matrix: 
	      printf("gold=[");		    
	      for(uint i = 0; i < self->cnt_vertices_total; i++) {
		printf("%u,", obj_2.map_clusterMembers[i]);
		//printf("vertex[%u] = %u, at %s:%d\n", i, obj_2.map_clusterMembers[i], __FILE__, __LINE__);
	      }
	      printf("], given part1=[N=%u, C=%u], part2=[N=%u, C=%u], at %s:%d\n", cnt_vertices_first, cnt_clusters_first, cnt_vertices_second, cnt_clusters_second, __FILE__, __LINE__);
	      //export__singleCall__s_kt_matrix_t(&(obj_1.matrix), NULL, NULL);
	      //		    export__singleCall__s_kt_matrix_t(&obj_1, "tmp_inError.tsv", NULL);
	    }
	  } else {//! else we asusme 'it' is a self-comaprigon, ie, for which the latter is sued to 'valdiate ones of tail', ie, 'controlling' tha the CCM-emtric correctly assigns a 'top-score' when sets are equal.
	    if(cnt_clustersTotal <= self->cnt_vertices_total) {
	      bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_2, self->cnt_vertices_total, /*cnt_clusters=*/2, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd=*/0);
	      assert(is_ok);
	    } else {
	      bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_2, self->cnt_vertices_total, /*cnt_clusters=*/self->cnt_vertices_total, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd=*/0);
	      assert(is_ok);
	    }
	    assert(cnt_clustersTotal >= 1);	    
	  }
	  /* { //! Validate that the correctne number of is set: */
	  /*   uint max_clusterId = 0; */
	  /*   for(uint i = 0; i < self->cnt_vertices_total; i++) { */
	  /*     //printf("%u,", obj_2.map_clusterMembers[1]); */
	  /*     max_clusterId = macro_max(max_clusterId, obj_2.map_clusterMembers[i]); */
	  /*   } */
	  /*   // fprintf(stderr, "!!\tmax_clusterId=%u, given part1=[N=%u, C=%u], part2=[N=%u, C=%u], at %s:%d\n", max_clusterId, cnt_vertices_first, cnt_clusters_first, cnt_vertices_second, cnt_clusters_second, __FILE__, __LINE__); */
	  /*   //assert((max_clusterId+1) >= cnt_clusters_second); */
	  /*   assert(max_clusterId >= 2); */
	  /* } */
	  /* if(false) { //! then we writ eotu the assigned clsuter-memberships: */
	  /*   printf("blocks=[%u, %u] and clusters=[%u, %u], at %s:%d\n", cnt_vertices_first, cnt_vertices_second, cnt_clusters_first, cnt_clusters_second, __FILE__, __LINE__); */
	  /*   for(uint i = 0; i < self->cnt_vertices_total; i++) { */
	  /*     printf("vertex[%u] = %u, at %s:%d\n", i, obj_2.map_clusterMembers[i], __FILE__, __LINE__); */
	  /*   } */
	  /*   assert(false); // TODO: remove when [ªbove] is validated ... valdiating [ªbove] assingment-appraoch */
	  /* }	 */
	  //printf("cnt_clustersTotal=%u, at %s:%d\n", cnt_clustersTotal, __FILE__, __LINE__);
	  //printf("cnt_clustersTotal=%u, at %s:%d\n", cnt_clustersTotal, __FILE__, __LINE__);
	}
	//printf("..., isTo_use_matrixBased=%u, at %s:%d\n",isTo_use_matrixBased,  __FILE__, __LINE__);
	//!
	//! Compute the CCM: 
	t_float ccm_score = T_FLOAT_MAX;
	if(isTo_use_matrixBased) {
	  if(self->typeOf_ccmTest == e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit) {
	    s_kt_matrix_base_t mat_base = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&(obj_2.matrix));
	    /* { //! Valdiate input:  */
	    /*   uint k_clusterCount = 0; */
	    /*   for(uint i = 0; i < mat_base.nrows; i++) { */
	    /* 	if(obj_1.map_clusterMembers[i] != UINT_MAX) { */
	    /* 	  k_clusterCount = macro_max(k_clusterCount, obj_2.map_clusterMembers[i]); */
	    /* 	} */
	    /*   } */
	    /*   //printf("in a matrix wiht dims=[%u, %u] k_clusterCount=%u, at %s:%d\n", mat_base.nrows, mat_base.ncols, k_clusterCount, __FILE__, __LINE__); */
	    /*   assert(k_clusterCount >= 1); //! ie, at least tow clusters. */
	    /* } */	    
	    //! Then we 'invert' the object-spec:
	    const bool is_ok = ccm__singleMatrix__hp_ccm(metric_clusterCmp_matrixBased, &mat_base, /*case_1=*/obj_1.map_clusterMembers, &ccm_score);
	    assert(is_ok);
	  } else {
	    s_kt_matrix_base_t mat_base = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&(obj_1.matrix));
	    { //! Valdiate input: 
	      uint k_clusterCount = 0;
	      for(uint i = 0; i < mat_base.nrows; i++) {
		if(obj_2.map_clusterMembers[i] != UINT_MAX) {
		  k_clusterCount = macro_max(k_clusterCount, obj_2.map_clusterMembers[i]);
		}
	      }
	      //printf("in a matrix wiht dims=[%u, %u] k_clusterCount=%u, at %s:%d\n", mat_base.nrows, mat_base.ncols, k_clusterCount, __FILE__, __LINE__);
	      assert(k_clusterCount >= 1); //! ie, at least tow clusters.
	    }

	    const bool is_ok = ccm__singleMatrix__hp_ccm(metric_clusterCmp_matrixBased, &mat_base, /*case_1=*/obj_2.map_clusterMembers, &ccm_score);
	    assert(is_ok);
	  }
	  //printf("ccm_score=%f, at %s:%d\n", ccm_score, __FILE__, __LINE__);
	} else {
	  bool is_ok = ccm__twoHhypoThesis__hp_ccm(metric_clusterCmp, 
						   /*case_1=*/obj_1.map_clusterMembers,
						   /*case_2=*/obj_2.map_clusterMembers, 
						   ///*cnt-case_1*/cnt_vertices_total, 
						   /*cnt-case_2*/self->cnt_vertices_total, 
						   (obj_1.matrix.matrix),
						   (obj_2.matrix.matrix),
						   //NULL, 
						   //NULL, 
						   &ccm_score);
	  assert(is_ok);
	}
	
	if(ccm_score != T_FLOAT_MAX) { //! then update the CCM-score:
	  if(isTo_use_matrixBased) {
	    const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(metric_clusterCmp_matrixBased);
	    if(false) {fprintf(stderr, "[%u][%u]\t\t \"%s\": '%f'at %s:%d\n", v_count-1, fraction_verticesInSecondBlock-1, str, ccm_score, __FILE__, __LINE__); 		  }
	    if(false) {fprintf(stderr, "(found)\t\t \"%s\": '%f', given v_count=%u, fraction_verticesInSecondBlock=%u, at %s:%d\n", str, ccm_score,  v_count, fraction_verticesInSecondBlock, __FILE__, __LINE__);}
	  }
	  mat_result[cmpMetric_id].matrix[v_count-1][fraction_verticesInSecondBlock-1] = ccm_score;
	} else { //! then the CCM-sscore was Not dienfed
	  if(isTo_use_matrixBased) {
	    // const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(metric_clusterCmp_matrixBased);
	    // fprintf(stderr, "(not-found)\t\t \"%s\": '%f', given v_count=%u, fraction_verticesInSecondBlock=%u, at %s:%d\n", str, ccm_score,  v_count, fraction_verticesInSecondBlock, __FILE__, __LINE__); 		  
	    
	  } else {
	    fprintf(stderr, "cmpMetric_id=%u, v_count=%u, fraction_verticesInSecondBlock=%u, at %s:%d\n", cmpMetric_id, v_count, fraction_verticesInSecondBlock, __FILE__, __LINE__); 
	  }
	  //assert(false); // FIXME: remove.
	  if(false) { //! then write out the input-matrix: 
	    printf("gold=[");		    
	    for(uint i = 0; i < self->cnt_vertices_total; i++) {
	      printf("%u,", obj_2.map_clusterMembers[i]);
	      //printf("vertex[%u] = %u, at %s:%d\n", i, obj_2.map_clusterMembers[i], __FILE__, __LINE__);
	    }
	    printf("], at %s:%d\n", __FILE__, __LINE__);
	    //printf("], given part1=[N=%u, C=%u], part2=[N=%u, C=%u], at %s:%d\n", cnt_vertices_first, cnt_clusters_first, cnt_vertices_second, cnt_clusters_second, __FILE__, __LINE__);
	    //export__singleCall__s_kt_matrix_t(&(obj_1.matrix), NULL, NULL);
	    //		    export__singleCall__s_kt_matrix_t(&obj_1, "tmp_inError.tsv", NULL);
	  }
	}
	//!
	//! De-allocate: 
	free__s_hp_clusterShapes_t(&obj_2);
      }
    }
    //!
    //!
    //! ------------------- Increment:
    self->cnt_vertices_total += cntVertices_incrementMult_each*v_count;
    //!
    //! De-allocate: 
    free__s_hp_clusterShapes_t(&obj_1);
    if(self->typeOf_ccmTest == e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit) {
      score_strong *= 2; //! ie, double the score-size
    }
  }
  printf("\t\t # evaluates CCM-hypothesis, at %s:%d\n", __FILE__, __LINE__);
  //! 
  //! Write out the relationship between base/zero hyptoehssi VS the different results/accuracies:
  assert(mat_result[0].nrows > 0); //! ie, as we otherwise need suing a differnet indces than [0].
  assert(mat_result[0].ncols > 0); //! ie, as we otherwise need suing a differnet indces than [0].
  s_kt_matrix_t mat_scoresVS_ranks = initAndReturn__s_kt_matrix(/*nrows=*/self->cnt_metrics, /*ncols=*/mat_result[0].nrows);
  //! 
  //! Set gold-hypotehsis:
  const t_float empty_0 = 0;
  t_float *list_gold = allocate_1d_list_float(mat_scoresVS_ranks.ncols, empty_0);
  for(uint i = 0; i < mat_scoresVS_ranks.ncols; i++) {list_gold[i] = (t_float)i;} //! ie, as we assuem hte altter corresponds to an 'ideal' ranking of the data-set
  { //! Set the headers of the rows and columns:
    for(uint cmpMetric_id = 0; cmpMetric_id < self->cnt_metrics; cmpMetric_id++) {
    const uint enum_id = self->arrOf_enums[cmpMetric_id];
    const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)enum_id;
    if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
    //if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(enum_id)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
    const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)enum_id;
      /* const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
      /* if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index) */
      /* const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)cmpMetric_id; */
      //mat_result[cmpMetric_id] = initAndReturn__s_kt_matrix(/*nrows=*/cntVertices_cntBlocks, /*ncols=*/fraction_verticesInSecondBlock_size);
      //const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id;
      const char *str = NULL;
      if(isTo_use_matrixBased) {
	str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(metric_clusterCmp_matrixBased);
	//assert(cmpMetric_id < mat_result[cmpMetric_id].nrows);
	//set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), cmpMetric_id, str, /*addFor_column=*/false);	      
	//set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), cmpMetric_id, str, /*addFor_column=*/false);	      
      } else {
	str = getStringOf__e_kt_matrix_cmpCluster_metric_t__short(metric_clusterCmp); 
	//assert(cmpMetric_id < mat_result[cmpMetric_id].nrows);
	//set_stringConst__s_kt_matrix(&(mat_result[cmpMetric_id]), cmpMetric_id, str, /*addFor_column=*/false);	      
      }
      set_stringConst__s_kt_matrix(&mat_scoresVS_ranks, cmpMetric_id, str, /*addFor_column=*/false);	      
    }
  }

  // assert(isTo_use_matrixBased); // FIXME: remove

  //!
  //! Write out the result, and theb de-allocate: 
  for(uint cmpMetric_id = 0; cmpMetric_id < self->cnt_metrics; cmpMetric_id++) {
    /* const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id; */
    /* if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index) */
    const uint enum_id = self->arrOf_enums[cmpMetric_id];
    const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)enum_id;
    if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
    //if(!isTo_use_matrixBased && isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(enum_id)) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
    const  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_clusterCmp_matrixBased = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)enum_id;
    //mat_result[cmpMetric_id] = initAndReturn__s_kt_matrix(/*nrows=*/cntVertices_cntBlocks, /*ncols=*/fraction_verticesInSecondBlock_size);
    //!
    { //! Comptue the ranks:
      for(uint m = 0; m < mat_result[0].nrows; m++) {
	t_float scalar_result = 0;
	const bool is_ok = apply__rows_hp_distance(metric_rankComparison, list_gold, mat_result[cmpMetric_id].matrix[m], mat_scoresVS_ranks.ncols, &scalar_result);
	assert(is_ok);
	mat_scoresVS_ranks.matrix[cmpMetric_id][m] = scalar_result;
      }
    }
    //!
    //! Buidl a file-path: 
    const char *str = getStringOf__e_kt_matrix_cmpCluster_metric_t__short(metric_clusterCmp);
    if(isTo_use_matrixBased) {
      str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(metric_clusterCmp_matrixBased);
    } /* else { */
    /*   assert(false); // FIXME: remove */
    /* } */
    printf("\twrite-out: %s\t at %s:%d\n", str, __FILE__, __LINE__);
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_split_%u_vOrder_%u_nClusters_%u_ccm%s%u_%s", self->resultPrefix, pos_relativeSplit, v_order, cnt_clustersTotal, (isTo_use_matrixBased) ? "Matrix" : "Set", cmpMetric_id, str);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&(self->obj_exportMatrix), mat_result[cmpMetric_id], resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
    //! De-allocate: 
    free__s_kt_matrix(&(mat_result[cmpMetric_id]));
  }
  //  assert(false); // FIXME: remove
  { //! Write out the comaprsion betwene the gold-standard VS the metrics: 
    //!
    //! Buidl a file-path: 
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_goldStandard_ccm_setBased_split_%u_vOrder_%u_clust_%u", self->resultPrefix, pos_relativeSplit, v_order, cnt_clustersTotal);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&(self->obj_exportMatrix), mat_scoresVS_ranks, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
    //! De-allocate: 
    free__s_kt_matrix(&mat_scoresVS_ranks);
    assert(list_gold); free_1d_list_float(&list_gold); list_gold = NULL;
  }
  cnt_clusters_currentCount *= cnt_clusters_incrementMult;
  
  //! ---- 
  return cnt_clusters_currentCount;
}


static void __eval__CCM__hardCodedCases(const char *resultPrefix__, const bool isTo_use_matrixBased) {
  allocOnStack__char__sprintf(2000, resultPrefix, "%s_%s", resultPrefix__, (isTo_use_matrixBased) ? "ccmMatrix_" : "ccmSetBased"); //! ie, to sserpate/dsintiiuch between the cases
  printf("\n\n--------------\n# Case: \"%s\", at %s:%d\n", (isTo_use_matrixBased) ? "matrix-based" : "set-based", __FILE__, __LINE__);
  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_postProcess, arrOf_sim_postProcess_size);

  //! ----------
  const bool isTo_writeOutTransposed = false;  //  TODO: consider setting this to true.
  //!
  //! Configuraitons:
  const uint cntVertices_cntBlocks = 15;
  uint cntVertices_incrementMult_firstVal = 200; //! where the latter is used to ivnestigate difference between 'linar increase' (wrt. the "v\_count >= 1") vs 'non-linear increase' (wrt. the "v\_count == 1"). 
  const uint cntVertices_incrementMult_each = 100;
  //! ------------------
  const uint relative_positionOf_splits_count = 4; //! where latter is used to evaluate the effects/implicatiosn of 'spreads' between a gold-standard VS altnertive cluster-ensamble-prediciotns.
  //! ------------------
  const uint cnt_clusters_cntIterations = 3;
  const uint cnt_clusters_incrementMult = 10; 
  uint cnt_clusters_currentCount = 6; //! ie, the 'start' number of clusters
  //! ------------------
  const uint fraction_verticesInSecondBlock_size = 10; //! where describes the f'raciton of mis-matches' between a gold-standard vs a hyptoetical/sample clsuter-result-set/ensamble: is deinged to ideally have/represent a linear decrease in clsuter-accurayc (when comapred to our gold-stdnard-base-assumption).
  //! ------------------
  e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t typeOf_ccmTest = e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedMatrix__splitClusterSetDataSet_eachRow_increaseRowSize;
  s_kt_correlationMetric_t metric_rankComparison = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none);


  //! ----------------------------------------------------------------------------
  const uint cnt_metrics = (isTo_use_matrixBased) ? (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef : (uint)e_kt_matrix_cmpCluster_metric_undef; //! ie, to 'handle' sperately for matrix-based CCMS and/or set-based CCMs.


  //!
  //! Allcoat eh sapce of the metics:
  const uint empty_0 = 0;
  uint *arrOf_enums = allocate_1d_list_uint(cnt_metrics, empty_0);
  for(uint i = 0; i < cnt_metrics; i++) {arrOf_enums[i] = i;} //! ie, as the enusm maps directly to numbers.

  //! ---------------------------------------------------------------------
  //! 
  //! Comapre the gold-standard to different vertex-orders
  for(uint pos_relativeSplit = 1; pos_relativeSplit <= relative_positionOf_splits_count; pos_relativeSplit++) {      
    for(uint v_order = 0; v_order < (uint)e_hp_clusterShapes_vertexOrder_undef; v_order++) { //! which is used to investigate the CCM-socre-effect on different orders of cluster-predictions, ie, an 'appraoch' which ideally shoudl not affect/influce CCM-scores:
      for(uint clustCount_i = 1; clustCount_i <= relative_positionOf_splits_count; clustCount_i++) { //! ie, evlauate diffenret number of clusters: 
	printf("clustCount_i=%u, at %s:%d\n", clustCount_i, __FILE__, __LINE__);
	//uint cnt_vertices_total = cntVertices_incrementMult_firstVal;	  
	s_eval__CCM__hardCodedCases__constructMatrix_t obj_eval = {resultPrefix, obj_exportMatrix, cntVertices_incrementMult_firstVal, cnt_metrics, arrOf_enums, /*weak=*/1, /*strong=*/10, typeOf_ccmTest};
	const uint cnt_clustersTotal = macro_min(cnt_clusters_currentCount, obj_eval.cnt_vertices_total);
	assert(cnt_clustersTotal <= obj_eval.cnt_vertices_total);
	//!
	//! Start/apply the evlaution: 
	cnt_clusters_currentCount += __eval__CCM__hardCodedCases__constructMatrix(&obj_eval,
										  //resultPrefix, &obj_exportMatrix, 
										  isTo_use_matrixBased, isTo_writeOutTransposed,
										  cntVertices_cntBlocks,
										  cntVertices_incrementMult_firstVal, cntVertices_incrementMult_each,
										  pos_relativeSplit, v_order, 
										  cnt_clusters_incrementMult, 
										  fraction_verticesInSecondBlock_size,
										  metric_rankComparison, 
										  //cnt_vertices_total, 
										  cnt_clustersTotal,
										  cnt_clusters_currentCount
										  );
      }
    }
  }
  assert(arrOf_enums); free_1d_list_uint(&arrOf_enums); arrOf_enums = NULL;
}

/**
   @brief an exhuathvie appraoch to seek captuign core-charartistics of CCMs
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks 
   -- call from main: "./x_measures tut_simEval_1_syntheticData_syntheticFunctions 1>out.txt"
**/
static int mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(const char *resultPrefix, const uint case_id, e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t typeOf_ccmTest) {
  {  //! Explctily evlauate differnet 'hard-coded' test-cases wrt. CCMs:
    //! Note: a simplfied test-proceudre toe vlauate baisc/core cases
    {//! Matrix-based CCMs:
      const bool isTo_use_matrixBased = true;
      //const uint cnt_metrics = 1;
      const uint cnt_metrics = 10;
      // FIXME: extend [”elow] when we have solved issues/quesitons cosernsin the other matrix-based cCMs whcih we (paln to add) supprot for.
      const uint arrOf_enums[cnt_metrics] = {
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD,
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared,
      };
      {      
	s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_postProcess, arrOf_sim_postProcess_size);
	
	//! ----------
	const bool isTo_writeOutTransposed = false;  //  TODO: consider setting this to true.
	//!
	//! Configuraitons:
	//const uint cntVertices_cntBlocks = 4;
	const uint cntVertices_cntBlocks = 12;
	uint cntVertices_incrementMult_firstVal = 200; //! where the latter is used to ivnestigate difference between 'linar increase' (wrt. the "v\_count >= 1") vs 'non-linear increase' (wrt. the "v\_count == 1"). 
	//uint cntVertices_incrementMult_firstVal = 20; //! where the latter is used to ivnestigate difference between 'linar increase' (wrt. the "v\_count >= 1") vs 'non-linear increase' (wrt. the "v\_count == 1"). 
	const uint cntVertices_incrementMult_each = 100;
	//! ------------------
	const uint relative_positionOf_splits_count = 4; //! where latter is used to evaluate the effects/implicatiosn of 'spreads' between a gold-standard VS altnertive cluster-ensamble-prediciotns.
	//! ------------------
	const uint cnt_clusters_cntIterations = 3;
	const uint cnt_clusters_incrementMult = 10; 
	uint cnt_clusters_currentCount = 6; //! ie, the 'start' number of clusters
	//! ------------------
	const uint fraction_verticesInSecondBlock_size = 10; //! where describes the f'raciton of mis-matches' between a gold-standard vs a hyptoetical/sample clsuter-result-set/ensamble: is deinged to ideally have/represent a linear decrease in clsuter-accurayc (when comapred to our gold-stdnard-base-assumption).
	//! ------------------
	s_kt_correlationMetric_t metric_rankComparison = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none);

	if(case_id >= 3) 
	{ //! Case(3):  larger score-difference
	  //! ----------------
	  const uint pos_relativeSplit = 1; 
	  const uint v_order = e_hp_clusterShapes_vertexOrder_linear; //! which is used to investigate the CCM-socre-effect on different orders of cluster-predictions, ie, an 'appraoch' which ideally shoudl not affect/influce CCM-scores:
	  const t_float score_weak = 1; const t_float score_strong = 100;
	  for(uint case_id = 0; case_id < 3; case_id++) {
	    //uint clustCount_i = 1; //! ie, evlauate different numbers/orders of clusters:
	    uint cnt_clustersTotal = 1;
	    if(case_id == 0) {cnt_clustersTotal = 1;}
	    else if(case_id == 1) {cnt_clustersTotal = 4;}
	    else if(case_id == 2) {case_id = 16;}
	    else {assert(false);} //! ie, as we then need adding support for this cas
	    //! ----------------
	    printf("clustCount_i=%u, at %s:%d\n", cnt_clustersTotal, __FILE__, __LINE__);
	    //uint cnt_vertices_total = cntVertices_incrementMult_firstVal;	  

	    s_eval__CCM__hardCodedCases__constructMatrix obj_eval = {resultPrefix, obj_exportMatrix, cntVertices_incrementMult_firstVal, cnt_metrics, arrOf_enums, score_weak, score_strong, typeOf_ccmTest};
	    //const uint cnt_clustersTotal = macro_min(cnt_clusters_currentCount, obj_eval.cnt_vertices_total);
	    assert(cnt_clustersTotal <= obj_eval.cnt_vertices_total);
	    //!
	    //! Start/apply the evlaution: 
	    cnt_clusters_currentCount += __eval__CCM__hardCodedCases__constructMatrix(&obj_eval,
										      //resultPrefix, &obj_exportMatrix, 
										      isTo_use_matrixBased, isTo_writeOutTransposed,
										      cntVertices_cntBlocks,
										      cntVertices_incrementMult_firstVal, cntVertices_incrementMult_each,
										      pos_relativeSplit, v_order, 
										      cnt_clusters_incrementMult, 
										      fraction_verticesInSecondBlock_size,
										      metric_rankComparison, 
										      //cnt_vertices_total, 
										      cnt_clustersTotal,
										      cnt_clusters_currentCount
										      );
	  }
	}
	//assert(false); // FIXME: remove.
	if(case_id == 2) 
	{ //! Case(2):  no score-difference.
	  //! ----------------
	  const uint pos_relativeSplit = 1; 
	  const uint v_order = e_hp_clusterShapes_vertexOrder_linear; //! which is used to investigate the CCM-socre-effect on different orders of cluster-predictions, ie, an 'appraoch' which ideally shoudl not affect/influce CCM-scores:
	  const t_float score_weak = 1; const t_float score_strong = 1.5;
	  for(uint case_id = 0; case_id < 3; case_id++) {
	    //uint clustCount_i = 1; //! ie, evlauate different numbers/orders of clusters:
	    uint cnt_clustersTotal = 1;
	    if(case_id == 0) {cnt_clustersTotal = 1;}
	    else if(case_id == 1) {cnt_clustersTotal = 4;}
	    else if(case_id == 2) {case_id = 16;}
	    else {assert(false);} //! ie, as we then need adding support for this cas
	    //! ----------------
	    printf("clustCount_i=%u, at %s:%d\n", cnt_clustersTotal, __FILE__, __LINE__);
	    //uint cnt_vertices_total = cntVertices_incrementMult_firstVal;	  

	    s_eval__CCM__hardCodedCases__constructMatrix obj_eval = {resultPrefix, obj_exportMatrix, cntVertices_incrementMult_firstVal, cnt_metrics, arrOf_enums, score_weak, score_strong, typeOf_ccmTest};
	    //const uint cnt_clustersTotal = macro_min(cnt_clusters_currentCount, obj_eval.cnt_vertices_total);
	    assert(cnt_clustersTotal <= obj_eval.cnt_vertices_total);
	    //!
	    //! Start/apply the evlaution: 
	    cnt_clusters_currentCount += __eval__CCM__hardCodedCases__constructMatrix(&obj_eval,
										      //resultPrefix, &obj_exportMatrix, 
										      isTo_use_matrixBased, isTo_writeOutTransposed,
										      cntVertices_cntBlocks,
										      cntVertices_incrementMult_firstVal, cntVertices_incrementMult_each,
										      pos_relativeSplit, v_order, 
										      cnt_clusters_incrementMult, 
										      fraction_verticesInSecondBlock_size,
										      metric_rankComparison, 
										      //cnt_vertices_total, 
										      cnt_clustersTotal,
										      cnt_clusters_currentCount
										      );
	  }
	}
	//assert(false); // FIXME: remove.
	if(case_id <= 1) 
	{ //! Case(1):  evaluate influence of increase in cluster-count, where we assume: vertex-roder to lienar wrt. teh clsuter-partions, and score-wihin = 1 wile score-strong = 10: 
	  //! ----------------
	  const uint pos_relativeSplit = 1; 
	  const uint v_order = e_hp_clusterShapes_vertexOrder_linear; //! which is used to investigate the CCM-socre-effect on different orders of cluster-predictions, ie, an 'appraoch' which ideally shoudl not affect/influce CCM-scores:
	  const t_float score_weak = 1; const t_float score_strong = 10;
	  for(uint case_id = 0; case_id < 3; case_id++) {
	    //uint clustCount_i = 1; //! ie, evlauate different numbers/orders of clusters:
	    uint cnt_clustersTotal = 1;
	    if(case_id == 0) {cnt_clustersTotal = 1;}
	    else if(case_id == 1) {cnt_clustersTotal = 4;}
	    else if(case_id == 2) {cnt_clustersTotal = 16;}
	    else {assert(false);} //! ie, as we then need adding support for this cas
	    //! ----------------
	    printf("clustCount_i=%u, at %s:%d\n", cnt_clustersTotal, __FILE__, __LINE__);
	    //uint cnt_vertices_total = cntVertices_incrementMult_firstVal;	  

	    s_eval__CCM__hardCodedCases__constructMatrix obj_eval = {resultPrefix, obj_exportMatrix, cntVertices_incrementMult_firstVal, cnt_metrics, arrOf_enums, score_weak, score_strong, typeOf_ccmTest};
	    //const uint cnt_clustersTotal = macro_min(cnt_clusters_currentCount, obj_eval.cnt_vertices_total);
	    assert(cnt_clustersTotal <= obj_eval.cnt_vertices_total);
	    //!
	    //! Start/apply the evlaution: 
	    cnt_clusters_currentCount += __eval__CCM__hardCodedCases__constructMatrix(&obj_eval,
										      //resultPrefix, &obj_exportMatrix, 
										      isTo_use_matrixBased, isTo_writeOutTransposed,
										      cntVertices_cntBlocks,
										      cntVertices_incrementMult_firstVal, cntVertices_incrementMult_each,
										      pos_relativeSplit, v_order, 
										      cnt_clusters_incrementMult, 
										      fraction_verticesInSecondBlock_size,
										      metric_rankComparison, 
										      //cnt_vertices_total, 
										      cnt_clustersTotal,
										      cnt_clusters_currentCount
										      );
	  }
	}
      }
    }
    /* assert(false); // FIXME: add for ... set-based */
    /* assert(false); // FIXME: add for ... extend test-cases ...  */
    /* assert(false); // FIXME: add for ...  */
    printf("------------------\n# Completed, at %s:%d\n----------------------------------\n\n", __FILE__, __LINE__);
    if(true) {return 1;} //! ie, as we then assume that [belo]ł is Not of itnerest.
  }
  //! ----------------------------------------------------------------
  { //! A simplfied CCM-based evaluation (of extrene hard-coded cluster-resutl-cases):
    //! Note: a simplfied veriosn used to demontrrate the seperability wrt. differente CCM-metrics.
    //! Note: in below the 'dfault configuraiton' is to evalaute a clsuter-rpedion-set cosnsiteng of two uniform cluster distributiosn $C = C_1 + D_2 = \{ v_a \in C_1 + v_b \in C_2 \} = V$, ie, where cluster $C$ hold the compelte set of vertices, and where the differnet clsuter-distributions are used to caputre/describe the msimatch between a gold-standard VS an acutal clsuter-repdicont-result. In this/our CCM-evlauation we seek to desirbe the choise of evlulauted topolgoy of gold-standards and hypothetical clsuter-prediction-results. Example-applciaitonsi (wrt. latter) is to evaluate the meaning/implciaiton of differences in ARI-scores when new algorithms are rpopseod, eg, wrt. the new-prosped algorithm seen in \cite{hahsler2016clustering}. 
    //! ---------------------------------------------------------------------
    { allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_%s", resultPrefix, "ccmMatrixBased");
      __eval__CCM__hardCodedCases(resultPrefix_local, /*isTo_use_matrixBased=*/true);
    }
    // assert(false); // FIXME: remove.
    { allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_%s", resultPrefix, "ccmSetBased");
      __eval__CCM__hardCodedCases(resultPrefix, /*isTo_use_matrixBased=*/false);
    }

    // --- 
    assert(false); // FIXME[conceptaul] ... new tut ... update our real-life data-anslysis ... constuct a matrix[data-set][cluster-algorithm] = [select=[best, worse]]x[select(ccm-score), select(ccm-score/ccm-score(Euclid)), select(sim-metric), select(sim-metric-category)], ie, where we compute a "ccm(k) x matrix[data-set][cluster-algorithm] x [sim-metric]" evaluat-case ... and matrix[data-set][sim-metric] = [sim-metric-score, sim-metric-score/Euclid, ranked(sim-metric)]
    assert(false); // FIXME[conceptual]: \cite{wiwie2015comparing} ~/Skrivebord/clusterShapes_nmeth.3583.png ....  try to scaffold an appraoch to 'clasisfy'  the diffent 'shapes' of eahc vector ... by using MINE ... insering the max-scores for a matrix ... where we comapre each rows (for a given data-set-matrix) to/with [shape=[spearhical, convex], seperation=[seperate, higly overlapping, nested]] ... and then use our .... library to 'classify' each vertex. 

    assert(false); // FIXME[code]: ... write a generic prodcue ... call our tut-examples ... seperately for each 'data-ensamble' generate a matrix ... merge latter in our "hpLysisVisu.js"
    assert(false); // FIXME[code]: ... new function to idneitfy/evaluate the simlairty between a vertex and a data-set ... to be included/written in our "kt_api__centerPoints.h"
    assert(false); // FIXME[code::new::tut]: ... examplify [ªbove] sim-metric-condence-funciton ... 
    assert(false); // FIXME[code]: ... compute 'medoid' ... call our "kt_matrix__centroids(..)" ... compare similarity-metrics ... between 'center' VS each vertex ... post-process 'adjustments': for case where "Euclid==0" set to global-max-score('max-score') .... 
    assert(false); // FIXME[code]: ... write out ... the ranks assicated to metric ... into a text-file ... 'grep' latter (for MINE) .... use results to consutrct a test-data-set for the data-sets hwere MINE out-perofmrs the other emtics) ... 
    assert(false); // FIXME[article]: construct heat-maps ... investigate differnecet hyptoehsis ... eg, wrt. ....??.... .... export data-sets
    assert(false); // FIXME[article]: ... provides(a)  .... classification of data-sets based on smiliarity-metrics ... of interest when (ie, in the context of evaluation-cases where) .... 
    assert(false); // FIXME[article]: ... provides(b)  .... clustering vs Euclid: importance/impact/implication of using Euclid for different simliarty-metrics; 
    assert(false); // FIXME[article]: ... provides(c)  .... simliarty vs Euclid: how simlairty-scrores varies when .... caputre a 'pattern of variance' between different data-sets .... 
    // ------------------
    assert(false); // FIXME[conceptaul] ... new tut ... accuracy of simliarty-metircs .... first for a sytneitc data-set ... therafter for real-life data-sets ... limit our coparison to the real-life-data-sets of ......??... 
    assert(false); // FIXME[code]: ...
    assert(false); // FIXME: ... 
    assert(false); // FIXME: ... 
    assert(false); // FIXME[article]: ... provides(a)  .... data-set-topologies: differnet 'optmal predictiosn' for different data-sets;
    assert(false); // FIXME[article]: ... provides(b)  .... simliarty-metrics: 
    assert(false); // FIXME[article]: ... provides(c)  .... cluster-algorithms: CCM-score-difference between 
    // ------------------
    assert(false); // FIXME[conceptaul] ... new tut ... PCA-evaluation ... use the cluster-C-appraoch ... compute PCA ... compare with ...??.... 
    assert(false); // FIXME[code::new-tut]: ... new tut ... examplifyin ghte use of a 'slow' PCA-based pre-filter-step
    assert(false); // FIXME[code]: ... update our "hpLysis_api" ... with PCA 'as a slow-optioanl step' ... (ie, a boolen which may be 'activated' after sim-metric-comptuations) ... 
    assert(false); // FIXME[code]: ... measurements ... new 'for-loop' ... to evalaute the 'effects' of PCA ... generate a seperate/new matrix-set/collection
    assert(false); // FIXME[code]: ... measurements ... 
    assert(false); // FIXME[code]: ... 
    assert(false); // FIXME: ... 
    assert(false); // FIXME: ... 
    assert(false); // FIXME[article]: ... provides(a)  .... accuracy-degredation: use the CPA in our data-set-evlauation 
    assert(false); // FIXME[article]: ... provides(b)  .... 
    assert(false); // FIXME[article]: ... provides(c)  .... 
    // ------------------
    assert(false); // FIXME[conceptaul] ... new tut ...
    assert(false); // FIXME[code]: ...
    assert(false); // FIXME: ... 
    assert(false); // FIXME: ... 
    assert(false); // FIXME[article]: ... provides(a)  .... 
    // ------------------
    assert(false); // FIXME: ... 
    assert(false); // FIXME: ... 
    assert(false); // FIXME: ... get 'toruhg' ... fixing code-issues .. and validate that oru 'itnal' hyptoesses are correct ... ie, inspect data-
    // ------------------
    // --
    // --
    assert(false); // FIXME[concpetual]: ... describe/related the 'k-means convergence-thresohld' to the other/estbliahsed simliarty-metrics
    assert(false); // FIXME: ... 
    assert(false); // FIXME: ... udpate our figure-caption wrt. [ªbove] appraoch/configuraiton ... cosnider moving latter to the imlemtnation-section .... incluiidng/consideriang all of the notes/comemtns in [above]
    assert(false); // FIXME[fig+aritcle]: ... 

    assert(false); // FIXME[new-fig]: ... 
    assert(false); // FIXME[fig-caption] ... update Fig. \ref{}
    // ---
    // --- new fig:
  }
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy


/**
   @brief 
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks 
   -- call from main: "./x_measures tut_simEval_1_syntheticData_syntheticFunctions 1>out.txt"
**/
int main() 
#else
  static void tut_simEval_1_syntheticData_syntheticFunctions(const char *resultPrefix)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
  const char *resultPrefix = "r-tut-simEval-1-";
#endif 
  const bool isTo_writeOutTransposed = false;  //  TODO: consider setting this to true.

  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_postProcess, arrOf_sim_postProcess_size);

  // FIXME: 
  //! Note: below enum is defined in 'this file'.
  e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t typeOf_ccmTest = e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedMatrix__splitClusterSetDataSet_eachRow_increaseRowSize;
  mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, 0, typeOf_ccmTest);

  /* //! ---------------------------------------------------------------- */
  /* { //! Load the [IRIS-data, gunien-pgi-MINE-data-set, "Sex ratio and litter size in the guinea-pig"], compare with a 2x different gold-standard-clsute-ensamlbe-set (ie, seperately for each inptu-data-set) ...  */
  /*   //! Note: the results of the latter may be re-produced using/throuhg oour \url{./x_measures tut_hypothesis_whyResultDiffers_1_all} ...  */

  /*   assert(false); // FIXME[artilces--new-iter:""]:  */
  /*   assert(false); // FIXME[artilces--new-iter:"survey_hpLysis_all/"]: ... PHD-thesis ... motvaiotn for our broad appraoch ... improve indoruction ...  */
  /*   assert(false); // FIXME[artilces--new-iter:"acm__clustMetricCmp__algCmp/"]: ... use of reuslts in/from our "hp_evalHypothesis_algOnData.c" ... scaffold requriemetns ... mtovaitons ... future-work ... new/nvoel figures ....  */
  /*   assert(false); // FIXME[artilces--new-iter:"ccm-eval-case-1"]: ... imrpvoe lange adn stuructre ... idneityf key-apsects/reuqirements ... scaffold result-figures ... relate to our "tut_simEval_1_syntheticData_syntheticFunctions.c" adn our "hp_evalHypothesis_algOnData.c" */
  /*   assert(false); // FIXME[artilces--new-iter:"ccm-eval-case-2"]: ... imrpvoe lange adn stuructre ... idneityf key-apsects/reuqirements ... scaffold result-figures ... relate to our "tut_simEval_1_syntheticData_syntheticFunctions.c" adn our "hp_evalHypothesis_algOnData.c" */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... move our "writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, ..)" to a new struct ...??...  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... get compling ... start exeuction ... for the 'iris-colleciton', and the 'MINE-sample-data' collection ...  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::article]: ... include our "hp_evalHypothesis_algOnData.c" dicssusiosn/methodlogy into our k-means-in-dpeth-artilce. */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::article]: ...  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"]: ...  */


  /*   assert(false); // FIXME[article::MINE]: .... udpate our MINE-artilce with the test-data-resutls 'found' in our "mine_cmp.html" ... including latter in an appendix ... suggesting to use latter as bassi in a new aritlce ...  */


  /*   //allocOnStack__char__sprintf(2000, resultPrefix_input, "%s_inputMatrix_%s", resultPrefix, "iris"); */

  /*   //!  */
  /*   //! Load data-set and gold-standard-cluster-permtuations */

  /*     //!  */
  /*     //! Investige seperately for difnferet CCM-based cluster-coparison-metircs:  */
  /*     //!  */
  /*     //! Evaluate for: cluster-algorithms: */
  /*     //!  */
  /*     //! Evaluate for: simliarty-metircs:  */
  /*     assert(false); // FIXME: ...  */
  /*     assert(false); // FIXME: ...  */
  /*     assert(false); // FIXME: ...  */
  /*     assert(false); // FIXME: ... result-eval ... seperately for different CCM-metrics  */
  /*     assert(false); // FIXME[fig+aritcle]: ... and evaluate for a subset of simlairty-meitrcs and the 'base' cluster-algorihtms ... use latter as a motivation for  */

  /*     assert(false); // FIXME[conceptaul] ...  */
  /*     assert(false); // FIXME[code]: ...  */
  /*     // -- */

  /* } */

  /* //! ---------------------------------------------------------------- */
  /* { //! Mask-based disjoint-forest using explict sytneitc/controlled feature-vectors. */
  /*   //! Note: a simplfied veriosn used to demontrrate the seperability wrt. differente metrics. */
  /*   assert(false); // FIXME: ... manage to construct 'such a set' usign our 'atuoamted' data-cnstruciton-approach */
  /*   assert(false); // % FIXME[code::CCM]: .... update our "kt_matrix_cmpClusters.h" .... support/inetrate measures 'which does not need an adjecney-atmrix as input' (where where it is sufficent to use/provide a feautre-matrix in combiantion with a clsuter-pratiion) ....  */
  /*   assert(false); // % FIXME[new-visu::code::tut+vsiu+eval]: .... a sperte visualiziaont-appraoch where we evlauate/consider above 'dsijtoint-fores-tclsuter-appraoch' ... add logings to our "kt_clusterAlg_dbScan.h" ... cosntruct/gneere new figure ... and udpate opur db-scan-aritlce-scaffold with latter  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME[fig+aritcle]: udapte our figure wrt. [ªbove] method-descirption. */
  /*   assert(false); // FIXME[fig+aritcle]: ...  */
  /*   assert(false); // FIXME[new-fig]: ...  */
  /*   assert(false); // FIXME[new-fig]: ...  */
  /*   // -- */
  /*   // --- new fig: differnet combinations of the matrix-based SSE CCM, eg, wrt. 'log' and simliarty-metircs */
  /*   assert(false); // FIXME[conceptaul] ... the evaluation of how simliarty-metrics affects the SSE-evalaution may provide novel/new insight into .....  */
  /*   assert(false); // FIXME[code]: ...  */
  /*   assert(false); // FIXME[fig-caption] ... update Fig. \ref{} */
  /*   // -- */
  /*   assert(false); // FIXME[conceptaul] ... support a re-cpomptuation' of centrodis ... ie, use the approach  (among others) seen in the "minibatch" clsuter-algorithm \cite{} ...  */
  /*   assert(false); // FIXME[code]: ...  */
  /*   assert(false); // FIXME[visu+article]: ... write a sperte evalaution-case where we comapre differnet permtautions of 'this' aprpaoch, eg, wrt. for mask-based clsuter-alg where we viausalize/comptue a heat-map matrix "[simliarty-metric][ccm-metric] = CCM-score".  */
  /*   // -- */
  /*   // -- */
  /*   assert(false); // FIXME[conceptaul] ...  */
  /*   assert(false); // FIXME[code]: ...  */
  /*   // -- */
  /* } */
  /* //! ---------------------------------------------------------------- */
  /* { //! Cluster-CCM-evaluation of our sytnetic data-sets */
  /*   assert(false); // FIXME: ... describe how ... to cosnmtruct meaniful feautre-vector-comaprison-appraoches between feature-vectors .... consider to use a 'value-threshold' when constructing syentitc data-cases ... use our 'explcit disjotinf-roest' evaluation wrt. the latter  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME[fig+aritcle]: ...  */
    
  /* } */
  /* //! ---------------------------------------------------------------- */
  /* { //! PCA through HCA: ....  */
  /*   assert(false); // FIXME: ... describe how ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME[fig+aritcle]: ...  */
  /* } */
  /* //! ---------------------------------------------------------------- */
  /* { //!  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME: ...  */
  /*   assert(false); // FIXME[fig+aritcle]: ...  */
  /* } */


  const uint factorMul_cnt = 2; const t_float factorMul_stepSize = 0.3;    //! where latter appraoch is used to 'enalbe' an ivneistigation/comparison of 'dfifenret distributiosn', eg.w rt 'straight line' VS 'currved line.
  const uint cases_factor_increase_cnt = 3; const t_float cases_factor_increase_mult = 5;
  const uint cases1_cnt = 4; const t_float multFactor = 2;
  const uint ncols_local = 10;
  const char cases_factor_increase_typeOf_multInner = 0;
  const uint cnt_splits = 3; //! where the later is optioanl and is used to 'revliey invert' teh data-sets. To exmaplify the latter we for $indexPos_posToInvert = [1, 5$ and input-scores=$[0, 1, 2, 3, 4, 5, 6]$ would get $[0, 1/1, 1/2, 1/3, 1/4, 5, 5]$.
  
  {//! Consturct matrices desicrinb the feautre-vectors we evlauate, and write out facts/knowledge of the latter:
    __writeOut__featureMatrix(cnt_splits, factorMul_cnt, factorMul_stepSize, cases_factor_increase_cnt, cases_factor_increase_mult, multFactor, cases_factor_increase_typeOf_multInner, cases1_cnt, ncols_local, resultPrefix, isTo_writeOutTransposed);
  }
  { //! Apply a simeitraty-emtric-evalaution, evaluating for a subset
    assert(cnt_splits > 0);
    const uint maxCnt__dataCases = cnt_splits*factorMul_cnt*cases_factor_increase_cnt;  uint maxCnt__dataCases__currentPos = 0;
    assert(maxCnt__dataCases > 0);
    uint maxCnt_metrics = arrOf_sim_postProcess_size;
    const bool isTo_evalauteHardCodedMetrics = false;
    if(isTo_evalauteHardCodedMetrics) {
      maxCnt_metrics += (uint)e_ktSim_typesOf_classes_undef;
    }
    const uint maxCnt_synData = cases1_cnt*cases1_cnt;
    //! 
    //! Allocate result-matrices:
    /* s_kt_matrix_t mat_result_metricTo_dataComb  = initAndReturn__s_kt_matrix(maxCnt__dataCases, maxCnt_metrics); */
    /* s_kt_matrix_t mat_result_synDataTo_dataComb = initAndReturn__s_kt_matrix(maxCnt__dataCases, maxCnt_synData); */
    /* //! Allocate a matrix for 'ufniciaotn' fo the elvauaiton-results: */
    /* s_kt_matrix_t matrix_merged = setToEmptyAndReturn__s_kt_matrix_t();   */
    //const bool matrix_merged__isToUse = false; //! where latter otpioin uis used to reduce the memory-signature.
    uint matrix_count = 0;
    fprintf(stderr, "(info)\t # Data-cases to evalaute are |matrices|=%u, permtations-each-row=%u, at %s:%d\n", cnt_splits*factorMul_cnt * cases_factor_increase_cnt, ncols_local, __FILE__, __LINE__);
    for(uint split_id = 0; split_id < cnt_splits; split_id++) {
      //! Itnaie a splitting-array:
      uint indexPos_posToInvert_size = 0; uint *indexPos_posToInvert = get_splitArray_fromScalar__s_ktSim_setOf_synData_spec__localConf_t(split_id, &indexPos_posToInvert_size, ncols_local);
      //!  Apply lgocis:
      for(uint factorMul = 1; factorMul <= factorMul_cnt; factorMul++) {
	const t_float mulFactor_m = (factorMul != 1) ? ((t_float)factorMul*factorMul_stepSize) : 1; //! wher elatter a'ppraoch' is used to 'ensure' that we invesitgate for a 'straight line'.
	for(uint factor = 0; factor < cases_factor_increase_cnt; factor++) {
	  const t_float factor_mult = (factor != 0) ? factor * cases_factor_increase_mult : 1;
	  //! 
	  //! Itnalise the cofnig-object:
	  s_ktSim_setOf_synData_spec_t synData_spec = MF__fromConfig__s_ktSim_setOf_synData_spec_t();	
	  //! Note: Allcoate the matrix: hold the 'result-feature-vecotrs':
	  synData_spec.matrixOf_features = initAndReturn__s_kt_matrix(maxCnt_metrics, maxCnt_synData);
	  for(uint sim_id = 0; sim_id < arrOf_sim_postProcess_size; sim_id++) {
	    //s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t();
	    //! Comptue the simlarity-metic.
	    /* s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t(); */
	    /* const bool is_ok = apply__simMetric_dense__extraArgs__hp_api_fileInputTight(arrOf_sim_postProcess[sim_id], &mat_result, NULL, &mat_result_simMetric); */
	    /* const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id; */
	    /* if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;} */
	    //!
	    //! Apply logics:
	    const e_ktSim_typesOf_classes_t syn_metric = e_ktSim_typesOf_classes_undef; //! ie, as we are interested in using a 'real-world' metric.
	    const s_kt_correlationMetric_t met = arrOf_sim_postProcess[sim_id];
	    computeForSynmetric__synData__s_ktSim_setOf_synData_spec_t(&synData_spec, syn_metric, met.metric_id, met.typeOf_correlationPreStep);
	  }
	  //! ---------------------------------------------------
	  //! 
	  //! Write out the result: write out both the ranks and the deviaitons wrt. results.
	  //!
	  //!
	  //! Wirte out proepteis wrt. latter:
	  //! Buidl a file-path: 
	  allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_subset_%u_simMetricToFeatures_split%u_%u_factor_%u", resultPrefix, matrix_count, split_id, factorMul, factor);
	  //! 
	  //! The data-consturciton-call:
	  writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, synData_spec.matrixOf_features, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);


	  //!
	  //! De-allocate
	  //free__s_kt_matrix(&mat_result);
	    //! De-allocate
	  free__s_ktSim_setOf_synData_spec_t(&synData_spec);
	  maxCnt__dataCases__currentPos++;
	  matrix_count++;
	}
      }
    }

    


    /* assert(false); // FIXME[code"tut_simEval_1_syntheticData_syntheticFunctions.c"::]: get ...  */
    /* assert(false); // FIXME[web+fig+aritcle]: ... MINE-article .... simlairty and difference between/for different smliarty-metrics VS systnetc data-sets .... find patterns ... argue for why the latter (ie, indicates the need for a future/in-depth research). */
    /* assert(false); // FIXME[fig+aritcle]: ... difference in ranks ... overlap betwene groups ... difference in priority/tanks ... inlfuecne on nroamlizaiton ... simliarty-post-step .. difference in 'unfied' rpedionction between simliarty-metrics ...  */

  }
  //! ----------------------------------------------------------------
  { //! 
    //! 
    //! 
    //! 

    //!
    //! Allocate a matrix: store the STD-valeus assicated to each "[factor x typeOf_multInner x factorMul_cnt ][sim_id]" and "[factor x data_id x factorMul_cnt][sim_id]".
    //! Note [ªbove] rsutls are used to investigate/evlauate/identify the synatcic cases which maximize seperation both between/wrt. data-sets and wrt. simlairty-metrics.
    //! Note: to simplify evlauation, we write/generate a sperate filw hcih consistns of the 'extreme cases' (ie, minSTD and max-STD), both for the data-set-combaitnos and wrt. simlarity-metrics.    
    // FIXME: try out diffneret combiantiosn fro [below] "cases_factor_increase_typeOf_multInner" ... seeking/tryign to maximize the STD-valeu wrt. scores 
    assert(cnt_splits > 0);
    const uint maxCnt__dataCases = cnt_splits*factorMul_cnt*cases_factor_increase_cnt;  uint maxCnt__dataCases__currentPos = 0;
    assert(maxCnt__dataCases > 0);
    uint maxCnt_metrics = (uint)e_kt_correlationFunction_undef;
    //uint maxCnt_metrics = (uint)e_kt_correlationFunction_undef + arrOf_sim_postProcess_size;
    const bool isTo_evalauteHardCodedMetrics = false;
    if(isTo_evalauteHardCodedMetrics) {
      maxCnt_metrics += (uint)e_ktSim_typesOf_classes_undef;
    }
    const uint maxCnt_synData = cases1_cnt*cases1_cnt;
    //! 
    //! Allocate result-matrices:
    s_kt_matrix_t mat_result_metricTo_dataComb  = initAndReturn__s_kt_matrix(maxCnt__dataCases, maxCnt_metrics);
    s_kt_matrix_t mat_result_synDataTo_dataComb = initAndReturn__s_kt_matrix(maxCnt__dataCases, maxCnt_synData);
    //! Allocate a matrix for 'ufniciaotn' fo the elvauaiton-results:
    s_kt_matrix_t matrix_merged = setToEmptyAndReturn__s_kt_matrix_t();  
    const bool matrix_merged__isToUse = false; //! where latter otpioin uis used to reduce the memory-signature.
    //!
    //! Note: moviation behicn [below] "factor" iteraiotn is to investigate wrt. the 'effect' of nroamzliaton-factors. 
    for(uint split_id = 0; split_id < cnt_splits; split_id++) {
      //! Itnaie a splitting-array:
      uint indexPos_posToInvert_size = 0; uint *indexPos_posToInvert = get_splitArray_fromScalar__s_ktSim_setOf_synData_spec__localConf_t(split_id, &indexPos_posToInvert_size, ncols_local);
      //!  Apply lgocis:
      for(uint factorMul = 1; factorMul <= factorMul_cnt; factorMul++) {
	const t_float mulFactor_m = (factorMul != 1) ? ((t_float)factorMul*factorMul_stepSize) : 1; //! wher elatter a'ppraoch' is used to 'ensure' that we invesitgate for a 'straight line'.
	for(uint factor = 0; factor < cases_factor_increase_cnt; factor++) {
	  const t_float factor_mult = (factor != 0) ? factor * cases_factor_increase_mult : 1;
	  //! 
	  //! Itnalise the cofnig-object:
	  s_ktSim_setOf_synData_spec_t synData_spec = MF__fromConfig__s_ktSim_setOf_synData_spec_t();	
	  //! Note: Allcoate the matrix: hold the 'result-feature-vecotrs':
	  synData_spec.matrixOf_features = initAndReturn__s_kt_matrix(maxCnt_metrics, maxCnt_synData);
	  /* for(uint sim_id = 0; sim_id < arrOf_sim_postProcess_size; sim_id++) { */
	  /*   //s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t(); */
	  /*   //! Comptue the simlarity-metic. */
	  /*   /\* s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t(); *\/ */
	  /*   /\* const bool is_ok = apply__simMetric_dense__extraArgs__hp_api_fileInputTight(arrOf_sim_postProcess[sim_id], &mat_result, NULL, &mat_result_simMetric); *\/ */
	  /*   /\* const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id; *\/ */
	  /*   /\* if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;} *\/ */
	  /*   //! */
	  /*   //! Apply logics: */
	  /*   const e_ktSim_typesOf_classes_t syn_metric = e_ktSim_typesOf_classes_undef; //! ie, as we are interested in using a 'real-world' metric. */
	  /*   const s_kt_correlationMetric_t met = arrOf_sim_postProcess[sim_id]; */
	  /*   computeForSynmetric__synData__s_ktSim_setOf_synData_spec_t(&synData_spec, syn_metric, met.metric_id, met.typeOf_correlationPreStep); */
	  /* } */
	  //! 
	  //! Merge the 'lcoal' and 'global' matrix: 
	  if(false)
	    { //! Insert our 'new-idienfied matrix' into the result-matrix, where row-naems are udpated with the "prefixInput_rows" string.
	      /* if(matrix_merged__isToUse) { */
	      /* 	allocOnStack__char__sprintf(2000, prefixInput_rows, "subset:split:%u, mul:%u", split_id, factorMul); */
	      /* 	insertMatrix_intoMatrix__s_kt_matrix_t(&matrix_merged, &(synData_spec.matrixOf_features), prefixInput_rows); */
	      /* } */
	      //! De-allocate
	      free__s_ktSim_setOf_synData_spec_t(&synData_spec);
	      //! Itnalise the cofnig-object:
	      s_ktSim_setOf_synData_spec_t synData_spec = MF__fromConfig__s_ktSim_setOf_synData_spec_t();	
	      //! Note: Allcoate the matrix: hold the 'result-feature-vecotrs':
	      synData_spec.matrixOf_features = initAndReturn__s_kt_matrix(maxCnt_metrics, maxCnt_synData);
	    }
	  //! 
	  //! Comptue for the simlairyt-score:
	  for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
	    const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
	    if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;}
	    if(isTo_use_directScore__e_kt_correlationFunction(metric_id)) {continue;}
	    //!
	    //! Apply logics:
	    const e_ktSim_typesOf_classes_t syn_metric = e_ktSim_typesOf_classes_undef; //! ie, as we are interested in using a 'real-world' metric.
	    computeForSynmetric__synData__s_ktSim_setOf_synData_spec_t(&synData_spec, syn_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
	  } //! and at this exec-point we have elvuated for all of the simliaryt-metircs.
	  //! 
	  //! Merge the 'lcoal' and 'global' matrix: 
	  /* if(matrix_merged__isToUse) { //! Insert our 'new-idienfied matrix' into the result-matrix, where row-naems are udpated with the "prefixInput_rows" string. */
	  /*   allocOnStack__char__sprintf(2000, prefixInput_rows, "all:split:%u, mul:%u", split_id, factorMul); */
	  /*   insertMatrix_intoMatrix__s_kt_matrix_t(&matrix_merged, &(synData_spec.matrixOf_features), prefixInput_rows); */
	  /* } */
	  if(isTo_evalauteHardCodedMetrics) {
	    //! Itnalise the cofnig-object:
	    s_ktSim_setOf_synData_spec_t synData_spec = MF__fromConfig__s_ktSim_setOf_synData_spec_t();	
	    //! Note: Allcoate the matrix: hold the 'result-feature-vecotrs':
	    synData_spec.matrixOf_features = initAndReturn__s_kt_matrix(maxCnt_metrics, maxCnt_synData);	    
	    //!
	    //! Copmtue using our 'sytnetic' sim-functions, an appraoch we 'include' in [”elow]:
	    assert(false); // TODO: consider adding supprot for this 'option'.
	    //! Note: Iterate through the 'pre-defined sytneitc sim-metircs':
	    for(uint sim_id = 0; sim_id < e_ktSim_typesOf_classes_undef; sim_id++) {
	      const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_undef;
	      //!
	      //! Apply logics:
	      const e_ktSim_typesOf_classes_t syn_metric = (e_ktSim_typesOf_classes_t)sim_id; //! ie, as we are interested in Not using a 'real-world' metric.
	      computeForSynmetric__synData__s_ktSim_setOf_synData_spec_t(&synData_spec, syn_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
	    }
	    //! 
	    //! Merge the 'lcoal' and 'global' matrix: 
	    if(matrix_merged__isToUse) { //! Insert our 'new-idienfied matrix' into the result-matrix, where row-naems are udpated with the "prefixInput_rows" string.
	      allocOnStack__char__sprintf(2000, prefixInput_rows, "hardCoded:split:%u, mul:%u", split_id, factorMul);
	      insertMatrix_intoMatrix__s_kt_matrix_t(&matrix_merged, &(synData_spec.matrixOf_features), prefixInput_rows);
	      //! De-allocate
	      free__s_ktSim_setOf_synData_spec_t(&synData_spec);
	    }
	  }
	  //! ---------------------------------------------------
	  //! 
	  //! Write out the result: write out both the ranks and the deviaitons wrt. results.
	  //!
	  //!
	  //! Wirte out proepteis wrt. latter:
	  //! Buidl a file-path: 
	  allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_simMetricToFeatures_split%u_%u_factor_%u", resultPrefix, split_id, factorMul, factor);
	  //! 
	  //! The data-consturciton-call:
	  writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, synData_spec.matrixOf_features, resultPrefix_local, mat_result_metricTo_dataComb.matrix[maxCnt__dataCases__currentPos], mat_result_synDataTo_dataComb.matrix[maxCnt__dataCases__currentPos], isTo_writeOutTransposed);
	  //! ---------------------------------------------------
	  //! 
	  //!
	  //! De-allocate
	  //free__s_kt_matrix(&mat_result);
	  //! De-allocate
	  free__s_ktSim_setOf_synData_spec_t(&synData_spec);
	  maxCnt__dataCases__currentPos++;
	}
      }
      if(indexPos_posToInvert != NULL) {free_1d_list_uint(&indexPos_posToInvert); indexPos_posToInvert = NULL; indexPos_posToInvert_size = 0;}
    }
    //!
    if(matrix_merged__isToUse) {//! Wirte out proepteis wrt. latter:
      //! Buidl a file-path: 
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_featureMatrix_merged", resultPrefix);
      //! The data-consturciton-call:
      writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, matrix_merged, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
      //!mat
      //! De-allocate
      free__s_kt_matrix(&matrix_merged);
    }
    //!
    //! Note: iot. enablle/fcialtie a simliarty-analysis of dependcies between data-formats, we insert the standard 
    { //! 
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_simMetricTo_dataCombinations", resultPrefix);
      writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_result_metricTo_dataComb, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
    }
    { //! 
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_synethticFeaturesTo_dataCombinations", resultPrefix);
      writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_result_synDataTo_dataComb, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
    }    
    free__s_kt_matrix(&mat_result_metricTo_dataComb);
    free__s_kt_matrix(&mat_result_synDataTo_dataComb);


    /* assert(false); // FIXME[conceptaul]: 'mannally' idneitfy ... a high seperation between different data-dstirbutions  */
    /* assert(false); // FIXME[conceptaul]: ....  */
    /* assert(false); // FIXME: add a post-rpocessing-stpe where [ªbove] is supported. */
    /* assert(false); // FIXME: descirbe an STD-eval-appraoch to fidn the 'best' option wrt. the "cases_factor_increase_typeOf_multInner" .... eg, to maximize the STD-valeu wrt. scores  */
  }

  /* { */

  /*   //! ------------------------------------------------------------ */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code::conceptual::article]: ... aruge for 'wehn our dsijtoitn-forest-appraoch' may outpefomr estalibhed appraoches (eg, for different classes/types of data, such as wrt. hetoerngous data with sigicnat/known/influencing hubs, eg, wtrt. higly central proeteins) ....  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... udpate oru web-visu wrt. ª[bov€] ... evluate results ... */
  /*   //! ------------------------------------------------------------ */
  /*   //! ------------------------------------------------------------ */
  /*   assert(false); //  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... prodcue a heatmap using 'only' syntetic sim-emtrics ... 'get through' ... 'manually' 'read-through 't eh scores. */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... integrate a lsit of 'simlairty-emtric-subsets' ... 'get through' ... 'manually' 'read-through 't eh scores. */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... add support for result-generation .... wrt. sim-metrics and HCA-dynamci-clsutering ...   */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... add support for result-generation .... wrt.  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... udpate oru web-visu wrt. ª[bov€] ... evluate results ... */
  /*   assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  improve our testual desicpriotn wrt. [ªbove] ... in oru aritlc-escafofld */
  /*   assert(false); // FIXME: cocnpetual: try to use the difnferet result-atmrices which we have cosnturcted ... udpateing our web-based visualiziaotns ...d sicuissing latter .... fidnign itnereisting patterns .... aguign for the 'need' to elvuate the differnet 'pserpekctives' we gnerate reuslt-data for.  */
  /* } */
  
  /* assert(false); // ok:FIXME: get our "tut_simEval_1_syntheticData_syntheticFunctions.c" to compile  */
  /* assert(false); // ok:FIXEM;: generate result-matrices for the STD-evluation-results     */
  /* assert(false); // ok:FIXMe[conceptaul]: 'mannally' idneitfy ... seperaiton in [ªbov€] ... visaullze the results ... udpate our docu ...  */
  /* assert(false); // ok:FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... add supprot for diffneret 'styentic' simalrity-emtrics/math-functions ... paper-scaffold 'generic' funciotns ... and then update our to-be-written "e_ktSim_typesOf_classes_t" enum */
  /* assert(false); // ok:FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... add supprot for diffneret 'styentic' simalrity-emtrics/math-functions ... write functiosn  */
  /* assert(false); // ok:FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... add supprot for diffneret 'styentic' simalrity-emtrics/math-functions ... use latter, ie, update our function-calls. */
  /* assert(false); // ok:FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... 'mvoe-out' the similairty-metircs into a new ehader-file/API named ....??...  */
  /* assert(false); // ok:FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... add/insert our 'syentitc-data-cosntruction' into a funciotn in [ªbove] ... which 'generates' an "s_kt_matrix_t" data based on a 'sprficioatn' of valeu-dsitributioins ...  */
  /* assert(false); // ok:FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... 'fork-out' the 'inner-evlauation-aprpaoch' to sa sperte funciton ... and add/write a seperate 'tut-genriec-fucniton' to call/use latter .... anemd ...??.. */
  /* //! ------------------------------------------------------------ */
  /* //! ------------------------------------------------------------ */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... generate matrices, and write out result */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... udpate oru web-visu wrt. ª[bov€] ... evluate results ... take screen-shot ... write an image-caption. */
  /* //! ------------------------------------------------------------ */
  /* //! ------------------------------------------------------------ */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code::conceptual]: ... contruct matrices of feuatres using [above] 'sytnetic data-generation-appraoch', and then include a permatuoin of our "tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering.c" .... investigate the simliarty between simlairty between clustering-algorithms and data-sets (eg, to descirtbe the combiend infleucne of data-sets and simlairty-emtrics on clsuter-algorithms), for whcih we .....??...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... udpate oru web-visu wrt. ª[bov€] ... evluate results ...  */
  /* //! ------------------------------------------------------------ */
  /* //! ------------------------------------------------------------ */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code::conceptual]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... udpate oru web-visu wrt. ª[bov€] ... evluate results ...  */
  /* //! ------------------------------------------------------------ */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... cosntruct reuslts foe [ªbove] ... add a new 'param'+fucntion-Call to our "kt_graphAlgorithms_main.cxx" ... manually ivneistgate wrt. results   */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ... udpate oru web-visu wrt. ª[bov€] ... evluate results ...  */
  /* assert(false); // FIXME["tut_simEval_1_syntheticData_syntheticFunctions.c"::code]: ...  */
  /* assert(false); // FIXME[conceptaul]: .... cocneptually describe how [ dbelowªbove] 'appraoch' may be used for 'arbitrary' data-sets .... and then udpate our aarticle-scaffold */


  /* assert(false); // FIXME[conceptaul]: .... update our [below] use-case-evaluiton wrt. the 'to-be-cosntructed' 'hp-eval-api' ... using the 'enw-fodun' requriemetns to udpate both our ª[boe] evla-appraoch and our aritcle-scaffold */
  /* assert(false); // FIXME[code]: .... 'fork out' [above] into a enw file .....??...  */
  /* assert(false); // FIXME[conceptaul]: .... update [ªbvoe] and [”elw] based ont eh new API ... and then update our aritle-scaffold. */
  /* assert(false); // FIXME[conceptaul]: ....  */


  { //! Idea. ... evluate the 'effects' of combining/merging diffenret dsitributioants  .. and/or a case/appraoch where 'ranking' becomes of importnace .... 
    //! Hypothesis. ....??.... Scores of simliarty-metrics are strongly boudn/dpedendt on the input-data. Therefore, we expect a seperaiton wrt. input-data to be distringushed/visible (ie, clearly seperable) wrt./between different data-distrubionts (ie, where the seperation/distinguishness between simlairty-emtgircs 'follow the lines' of the input-data-topology/attriubtes). Implicaiton (of latter) is classifciaotn of prediction-cabpilibitlies wrt. data-seperation. To examplfiy the latter, if we are interested in sperating wrt. data-variance and we clsuter wrt. the "Euclid" simliarty-metric, then a data-set with  .......??.... results in an errnous classifcation.  
    //! Weakness of our appraoch. ... ....??... 
    //! Result. ....??..
    
    assert(false); // FIXMe[conceptaul]: 'mannally' idneitfy ... 
    assert(false); // FIXMe[conceptaul]: 'mannally' idneitfy ... 

    //! 
    //! Allocate a matrix:
    assert(false); // FIXME: add somethign
    
    //! Comptue for the simlairyt-score:
    for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
      const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
      if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;}
      //if(metric_id != e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5) {continue;} //! fixme:reomve this ... inclued to 'figure out why' a isnan || inf-case occurs (oesketh, 06. arp. 2017).

      //!
      //! Iterat through the diffneret funciton-cases:
      assert(false); // FIXME: add somethign

      const uint ncols_local = 3;
      char row_1[ncols_local]; char row_2[ncols_local]; 
      //! 
      //! Fetch the scores:
      assert(false); // FIXME: add somethign
      
      //! 
      //! Comptue the simlairty-metirc:
      assert(false); // FIXME: add somethign

      //! 
      //! Update the result-matrix:
      assert(false); // FIXME: add somethign
    }
    //! 
    //! Write out the result: (a) the feature-scores, (b) the ranks, (c) the 'nroamlzied' scores (both non-transposed and transposed), (d) the deviation-scores (both non-transposed and transposed)
    //! Note: the dievioant-scores-result-file/matix is used to evlauate/investigate the seperability/classificaiton-ability of the synactic features: .....??...
    assert(false); // FIXME: add somethign

    //!
    //! Write out the syantic data-set (used to construct the input-data-evaluation)
    assert(false); // FIXME: add somethign


    //!
    //!
    assert(false); // FIXME: add somethign


    //!
    //! Copmtue using our 'sytnetic' sim-functions
    assert(false); // FIXME: move [ªbove] 'into' a new 'stub-file' ... update latter wrt. 'gneriicty' ... nad hten integrate our 'sytantic fucniton-call-setup'.
  }
  { //! Idea. ...  investigate feautre-simlairty 
    //! Hypothesis. ...??....
    //! Weakness of our appraoch. ... ....??... 
    //! Result. ...??....

    assert(false); // FIXME: use and update our "hp_categorizeMatrices_syn.h" .... and then re-start our s'epfvilized' tut-exmapel wrt. altter.
  }
  { //! Idea. ...  test correctness of our "hp_categorizeMatrices_syn" wrt. CCM-classificiaotns .... use known/well-deifned sepration-cases wrt. features ... comapre predicotns/classifciaotns ... suggest/evluatre the accuracy of our appraoch ....
    //! Hypothesis. ...??....
    //! Weakness of our appraoch. ... ....??... 
    //! Result. ...??....

  }
  { //! Idea. ...  
    //! Hypothesis. ...??....
    //! Weakness of our appraoch. ... ....??... 
    //! Result. ...??....

  }


  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

