#ifndef def_memAlloc_h
#define def_memAlloc_h

/**
   @file def_memAlloc
   @brief describes and generalises a set of memory-allocations.
   @remarks used to simplify siwthcing between different 'states'.

   @remarks easy-to-do-bugs which we have observed is/conersn:
   - using an aritmentic expression (when claling the macrocs), eg, "allocate_1d_list_float(x+1, 0" instead of "const uitn size  = x+1; allocate_1d_list_float(size, 0);". Result is a 'vilent' crash of the softare
 **/

#include <malloc.h>
#include <limits.h>

// FIXME[jc]: <-- we have itnroduced 'this' given in-correct answeres when suign "memset(..)" to integer-pointers' .. do you have a work-arodunw rt. 'this'?
// FIXME: write an itnrii-verison of 'this' ... and evaluate the eprformacne-ebenft
#define memset_local(arr, default_value, arr_size) ({for(uint i = 0; i < (uint)arr_size; i++) {arr[i] = default_value;}})


// FIXME[JC]: do you belive there is a differnece in exueciton-time (of memory-accesses) assicated to "new [] " and "malloc(..)" if we muse "memset(..)" (or for-loops) .... ie, wrt. 'locality in time of requested/required memory-access'? <-- if the latter is the case, would we then expect this 'issue'/'case' to be mroe signfinat if paralll/muliple thresas are allocating meory? <-- if so, are there anby known low-level approach to effciently allocate memory (eg, to avoid 'low-level mamory-locks', ie, if such exist)

//! A standarized approach to ensure that memory is correctly allocated into chunks:
// FIXME[jc]: what are the 'correct values' to be used in the "aligned(..)" call?
#define memAlign_preAllocated  __attribute__ ((aligned (16))) 

#ifndef macroConfig_use_posixMemAlign
//#define macroConfig_use_posixMemAlign 0 //! ie, our default assumption
#define macroConfig_use_posixMemAlign 1 //! ie, our default assumption
#endif

//! *********************************************************
//! Tailor the memory-allocaitons for the system and usage:
//! ********
#if macroConfig_use_posixMemAlign == 1 //! then we assume that Intel's intrisntics (eg, SSE code-standards) is ot be used/applied.
//! Start: "Posix memalgin" ----------------------------------------------------------

// FIXME[JC]: may you suggest different valeus/combinatiosn wrt. [”elow] "SIZE_INTRI_ALIGNMENT_CHUNKS" ... eg, using system-levle-macros to ''cofnigure' [”elow]? <-- should we use "64" for __m256 systems?
#define SIZE_INTRI_ALIGNMENT_CHUNKS 64

//! Allocate a 1d-list of data:
//#define alloc_generic_type_1d(type_t, ptr, size) (ptr = (type_t*)malloc(size * sizeof(type_t)))    
#define alloc_generic_type_1d(type_t, ptr, size) ({const int result_data = posix_memalign((void**)&ptr, SIZE_INTRI_ALIGNMENT_CHUNKS, (int)size * sizeof(type_t)); assert(result_data == 0); ptr;})
//! Allocate a 2d-list of data:
#define alloc_generic_type_2d(type_t, ptr, size) ({ptr = (type_t**)malloc(size * sizeof(type_t*)); ptr;})

//! De-allocate a 1d-list of data:
#define free_generic_type_1d(ptr) (free(ptr))    
//! De-allocate a 2d-list of data:
#define free_generic_type_2d(ptr) (free(ptr))    

//! complete: "Posix memalgin" ----------------------------------------------------------
#elif macroConfig_use_posixMemAlign == 0
//! Then we tailor the memory-allcoations for teither the stnadards of "ANSI C" or "C++":
#ifdef __cplusplus
//! Start: "C++" ----------------------------------------------------------

//! Allocate a 1d-list of data:
#define alloc_generic_type_1d(type_t, ptr, size) ({ptr = new type_t[size]; ptr;})    
//! Allocate a 2d-list of data:
#define alloc_generic_type_2d(type_t, ptr, size) ({ptr = new type_t*[size]; ptr;})    

//! De-allocate a 1d-list of data:
#define free_generic_type_1d(ptr) (delete [] ptr)    
//! De-allocate a 2d-list of data:
#define free_generic_type_2d(ptr) (delete [] ptr)    

//! complete: "C++" ----------------------------------------------------------
#else
//! Start: "ANSI C" ----------------------------------------------------------

//! Allocate a 1d-list of data:
				      // 
				      //#define alloc_generic_type_1d(type_t, ptr, size) ({printf("size=%d, at %s:%d\n", (int)size, __FILE__, __LINE__); ptr = (type_t*)malloc(size * sizeof(type_t)); ptr;})    
#define alloc_generic_type_1d(type_t, ptr, size) ({ptr = (type_t*)malloc(size * sizeof(type_t)); ptr;})    
//! Allocate a 2d-list of data:
#define alloc_generic_type_2d(type_t, ptr, size) ({ptr = (type_t**)malloc(size * sizeof(type_t*)); ptr;})    

//! De-allocate a 1d-list of data:
#define free_generic_type_1d(ptr) (free(ptr))    
//! De-allocate a 2d-list of data:
#define free_generic_type_2d(ptr) (free(ptr))    


//! complete: "ANSI C" ----------------------------------------------------------

#endif

//! Then we 'contniue' our elvuvation of the macroConfig_use_posixMemAlign parameer
#else 
#error "Add support for this case" //! ie, as we then need to add soemthing (oekseth, 06. june 2016).
assert(false) //! ie, to syntax-error to 'ensure' a compilation-abort-case.
#endif
//! Complete: *********************************************************



//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_float(size, default_value) ({ assert((char)default_value < CHAR_MAX); t_float *tmp; alloc_generic_type_1d(t_float, tmp, size); printf("sizeof=%d, size=%d, at %s:%d\n", (int)sizeof(t_float), (int)size, __FILE__, __LINE__); memset(tmp, (char)default_value, size*sizeof(t_float)); tmp; })

//#define allocate_1d_list_float(size, default_value) ({t_float *tmp = (t_float*)malloc(size*sizeof(t_float))})
//#define allocate_1d_list_float(size, default_value) ({ t_float *tmp = (t_float*)malloc(size*sizeof(t_float)); memset(tmp, (char)default_value, size*sizeof(t_float)); tmp; })
// FIXME: use [”elow] and remove [above]
#define allocate_1d_list_float(size, default_value) ({ assert((char)default_value < CHAR_MAX); t_float *tmp; alloc_generic_type_1d(t_float, tmp, size);  memset_local(tmp, (char)default_value, size); tmp; })
#define allocate_1d_list_sparseVec(size, default_value) ({ assert((char)default_value < CHAR_MAX); t_sparseVec *tmp; alloc_generic_type_1d(t_sparseVec, tmp, size);  memset_local(tmp, (char)default_value, size); tmp; })
// FIXME[article]: consider to include [”elow] code as an examplficiaiton of possible 'traps' when convering from functiosn to macros
//#define allocate_1d_list_float(size, default_value) ({ float *tmp = new float[size]; memset(tmp, default_value, size*sizeof(float)); return tmp; })


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_float(list) ({assert(list); t_float *tmp = *list; free_generic_type_1d(tmp);})
#define free_1d_list_sparseVec(list) ({assert(list); t_sparseVec *tmp = *list; free_generic_type_1d(tmp);})

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_uint(size, default_value) ({uint *tmp = (uint*)malloc(size * sizeof(uint));memset(tmp, default_value, size*sizeof(uint)); tmp;})
// FIXME: use [”elow] and remove [above]
#define allocate_1d_list_uint(size, default_value) ({uint *tmp; alloc_generic_type_1d(uint, tmp, size); memset_local(tmp, default_value, size); tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_uint(list) ({assert(list); uint *tmp = *list;  free_generic_type_1d(tmp); })

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_int(size, default_value) ({int *tmp = (int*)malloc(size*sizeof(int)); memset(tmp, default_value, size*sizeof(int)); tmp;})
// FIXME: use [”elow] and remove [above]
// printf("default-value=%d, at %s:%d\n", default_value, __FILE__, __LINE__);
#define allocate_1d_list_int(size, default_value) ({int *tmp;  alloc_generic_type_1d(int, tmp, size); memset_local(tmp, (int)default_value, size);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_int(list) ({assert(list); int *tmp = *list;  free_generic_type_1d(tmp); })
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_int(size, default_value) ({int *tmp = (int*)malloc(size*sizeof(int)); memset(tmp, default_value, size*sizeof(int)); tmp;})
// FIXME: use [”elow] and remove [above]
// printf("default-value=%d, at %s:%d\n", default_value, __FILE__, __LINE__); 
#define allocate_1d_list_int_short_unsigned(size, default_value) ({unsigned short int *tmp; alloc_generic_type_1d(unsigned short int, tmp, size); memset_local(tmp, default_value, size);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_int_short_unsigned(list) ({assert(list); unsigned short int *tmp = *list;  free_generic_type_1d(tmp); })

#define allocate_1d_list_int_short(size, default_value) ({short int *tmp; alloc_generic_type_1d(short int, tmp, size); memset_local(tmp, default_value, size);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_int_short(list) ({assert(list); short int *tmp = *list;  free_generic_type_1d(tmp); })


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_char(size, default_value) ({ char *tmp; alloc_generic_type_1d(char, tmp, size); memset_local(tmp, default_value, size); tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_char(list) ({assert(list); char *tmp = *list;  free_generic_type_1d(tmp); })

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_2d_list_uint(size_x, size_y, default_value) ({ \
      uint **tmp; alloc_generic_type_2d(uint, tmp, size_x); \
      tmp[0]; alloc_generic_type_1d(uint, tmp[0], size_x * size_y);	\
      assert((char)default_value < CHAR_MAX);				\
      memset_local(tmp[0], (char)default_value, /*size=*/size_x*size_y); \
      uint offset = size_y; uint current = size_y;			\
      for(uint i = 1; i < size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_uint(list) ({assert(list); uint **tmp = *list; free_generic_type_1d(tmp[0]);   free_generic_type_2d(tmp);})

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_2d_list_char(nrows, ncols, default_value) ({ \
      char **tmp = alloc_generic_type_2d(char, tmp, nrows);   \
      assert((char)default_value < CHAR_MAX);				\
      tmp[0]; alloc_generic_type_1d(char, tmp[0], ncols*nrows);		\
      memset_local(tmp[0], (char)default_value, /*size=*/nrows*ncols); \
      for(uint i = 0; i < nrows; i++) {					\
	tmp[i] = tmp[0] + i*ncols;					\
      }									\
      const uint cnt_cells = nrows * ncols;				\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_char(list) ({assert(list);  char **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_2d_list_float(size_x, size_y, default_value) ({ \
      t_float **tmp = alloc_generic_type_2d(t_float, tmp, size_x);		\
      tmp[0]; alloc_generic_type_1d(t_float, tmp[0], size_x * size_y);	\
      assert((char)default_value < CHAR_MAX);				\
      memset_local(tmp[0], (char)default_value, /*size=*/size_x*size_y); \
      uint offset = size_y; uint current = size_y;			\
      for(uint i = 1; i < size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_float(list) ({ assert(list);  t_float **tmp = *list; free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);  })


#define allocate_2d_list_sparseVec(size_x, size_y, default_value) ({ \
      t_sparseVec **tmp = alloc_generic_type_2d(t_sparseVec, tmp, size_x);		\
      tmp[0]; alloc_generic_type_1d(t_sparseVec, tmp[0], size_x * size_y);	\
      assert((char)default_value < CHAR_MAX);				\
      memset_local(tmp[0], (char)default_value, /*size=*/size_x*size_y); \
      /*uint offset = size_y;*/ uint current = size_y;			\
      for(uint i = 1; i < size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_sparseVec(list) ({ assert(list);  t_sparseVec **tmp = *list; free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);  })







//! complete: <file> wrt. "def_memAlloc_h" ----------------------------------------------------------
#endif //! ie, EOF
