#ifndef maskAllocate_h
#define maskAllocate_h

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"

//#include "mask_api.h"


  //! Allcoate a distance-matrix and an assicated mask-set.
  uint maskAllocate__makedatamask__uint(const uint nrows, const uint ncols, float*** pdata, uint*** pmask, const bool isTo_use_continousSTripsOf_memory);
  //! De-allocate the data allcoated in "makedatamask(..)"
  void maskAllocate__freedatamask__uint(const uint n, float** data, uint** mask, const bool isTo_use_continousSTripsOf_memory);
  //! Allcoate a distance-matrix and an assicated mask-set.
  uint maskAllocate__makedatamask(const uint nrows, const uint ncols, float*** pdata, char*** pmask, const bool isTo_use_continousSTripsOf_memory);
  //! De-allocate the data allcoated in "makedatamask(..)"
  void maskAllocate__freedatamask(const uint n, float** data, char** mask, const bool isTo_use_continousSTripsOf_memory);

#endif //! EOF
