#include "correlationType_delta.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @brief compute the weighted euclidian distance between two rows or columns in a matrix.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float __kt_euclid_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose)  {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  assert(data1);   assert(data2);
  t_float result = 0.;
  t_float tweight = 0;
  if(transpose == 0) { /* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.    
    for(uint i = 0; i < n; i++)       {
      if( ( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) ) {	
	//if(isOf_interest(data1[index1][i]) && isOf_interest(data2[index2][i]) ) 
	{
	  const t_float term = data1[index1][i] - data2[index2][i];
	  const t_float weight_local = (weight) ? weight[i] : 1;
	  result += weight_local*term*term;
	  tweight += weight_local;
	}
      }
    }
  } else {
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare the same column in mulitple row: if the column-size does not signficantly exceed the "L2" memory-cache-size (for a given computer-memory-hardware) then we expect an 'ideal' memory-allocation-scheme to provide/give a "linear" "n" speed-up (for every call to this function).
    for(uint i = 0; i < n; i++) {
      if( ( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) ) {
	//if(isOf_interest(data2[i][index1]) && isOf_interest(data2[i][index2]) )
	  {
	  t_float term = data1[i][index1] - data2[i][index2];
	  const t_float weight_local = (weight) ? weight[i] : 1;
	  result += weight_local *term*term;
	  tweight += weight_local;
	}
      }
    }
  }
  if(!tweight) return 0; /* usually due to empty clusters */
  result /= tweight;
  return result;
}

/* ********************************************************************* */

/**
   @brief compute the weighted "City Block" distance between two rows or columns in a matrix. 
   @remarks City Block distance is defined as the absolute value of X1-X2 plus the absolute value of Y1-Y2 plus..., which is equivalent to taking an "up and over" path.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float __kt_cityblock_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  t_float result = 0.;   t_float tweight = 0;
  if(transpose == 0) {/* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	const t_float term = data1[index1][i] - data2[index2][i];
	const t_float weight_local = (weight) ? weight[i] : 1;
        result = result + weight_local*mathLib_float_abs(term);
        tweight += weight_local;
      }
    }
  } else {
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	const t_float term = data1[i][index1] - data2[i][index2];
	const t_float weight_local = (weight) ? weight[i] : 1;
        result = result + weight_local*mathLib_float_abs(term);
        tweight += weight_local;
      }
    }
  }
  if(!tweight) return 0; /* usually due to empty clusters */
  result /= tweight;
  return result;
}


/**
   @brief compute the euclidian (or: geometric distance) between two rows or columns.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
**/
t_float kt_euclid(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const s_allAgainstAll_config_t self) {
  if( (transpose == 0) && self.forNonTransposed_useFastImplementaiton) {
    t_float result = 0.;   t_float tweight = 0;

    //! Note: [below] 'itnri-lgoics' is similar tot eh funciton found in our "correlationType_spearman.cxx"
    uint cnt_elements_intri = ncols - VECTOR_FLOAT_ITER_SIZE;
    if(((t_float)ncols/(t_float)VECTOR_FLOAT_ITER_SIZE) && ((t_float)VECTOR_FLOAT_ITER_SIZE)) {      
      cnt_elements_intri = ncols; //! ie, as we then assume that "cnt_elements" is divisable by "VECTOR_FLOAT_ITER_SIZE"
    }
    
    //! Handle seperately the different maks-cases:
    if(!mask1 && !mask2) {
      uint col_id = 0;
      if(!weight) {

	assert(false); // FIXME: handle the case where the masks as specified implcitly ... ie, write a seperate 'permtaution' of this case.

	if(self.forNonTransposed_useFastImplementaiton_useSSE) {
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET_zero();
	  
	  // FIXME: icnldue a macro where we drop the use of SSE ... and then test the importance of SSE wrt. the ecudlic distance-metric.
	  
	  // FIXME: cinldue a macro where we always use SSE ... ie, where we expec the data-set-length to be zero-paddded <-- woudl this work, ie, wrt. the correnctess of the 'adjustment-facotr'?
 
	  for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) { 
	    VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOAD(&data1[index1][col_id]);
	    VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOAD(&data2[index2][col_id]);
	    //! Apply the aritmetics:
	    vec_result = VECTOR_FLOAT_ADD(vec_result, correlation_macros__distanceMeasures__euclid__SSE(vec_1, vec_2));
	  }
	  //! Update the scalars:
	  result = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_result);
	}
	for(; col_id < ncols; col_id++) { //! where we in [”elow] use a geneirc macro-function define din our "correlation_macros__distanceMeasures.h":
	  t_float term = correlation_macros__distanceMeasures__euclid(data1[index1][col_id], data2[index2][col_id]);	  
	  result += term;
	}
	tweight = ncols; //! ie, as all cells/relations are then assume dto have the same weight.
      } else { //! then we also need to comptue for the weight-proeprty:

	// FIXME[performance]: for "weight" ... compare teh tiem-effect of 'setting all wegith-valeus to '1' ... ie, as used in the naive "cluster.c" implemtantion .... ie, to 'msiuse memroy and flaoting-poitnahrdware to comptue resutls which are not used' (ie, to alwys ask someone to bring you a pot of coffe, before mremebering every time that you have an odd coffe-pot-allgeric, ie, that you do not drink coffe).
	if(self.forNonTransposed_useFastImplementaiton_useSSE) {
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET_zero();
	  VECTOR_FLOAT_TYPE vec_tweight = VECTOR_FLOAT_SET_zero();
	  
	  for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) { 
	    VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOAD(&data1[index1][col_id]);
	    VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOAD(&data2[index2][col_id]);
	    VECTOR_FLOAT_TYPE vec_w = VECTOR_FLOAT_LOAD(&weight[col_id]);
	    //! Apply the aritmetics and make use of the weight:
	    VECTOR_FLOAT_TYPE vec_local = correlation_macros__distanceMeasures__euclid__SSE(vec_1, vec_2);
	    vec_local = VECTOR_FLOAT_MUL(vec_local, vec_w);
	    vec_result = VECTOR_FLOAT_ADD(vec_result, vec_local);
	    // correlation_macros__distanceMeasures__euclid_SSE_weight(vec_1, vec_2, vec_w, vec_result, vec_tweight);
	  }
	  //! Update the scalars:
	  result = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_result);
	  tweight = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_tweight);
	}

	for(; col_id < ncols; col_id++) { //! where we in [”elow] use a geneirc macro-function define din our "correlation_macros__distanceMeasures.h":
	  t_float term = correlation_macros__distanceMeasures__euclid(data1[index1][col_id], data2[index2][col_id]);	  
	  const t_float weight_local = weight[col_id];
	  result += (term * weight_local);
	  tweight += weight_local;
	}	
      }
    } else {


      if(self.forNonTransposed_useFastImplementaiton_useSSE) {
	assert(false); // add somethign 
      }
      
      assert(false); // FIXME: write a moficaiton ... using [ªbove].
      assert(false); // FIXME: update [below]

      for(uint i = 0; i < ncols; i++)       {
	if( ( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) ) {	
	  if(isOf_interest(data1[index1][i]) && isOf_interest(data2[index2][i]) ) {
	    const t_float term = data1[index1][i] - data2[index2][i];
	    const t_float weight_local = (weight) ? weight[i] : 1;
	    result += weight_local*term*term;
	    tweight += weight_local;
	  }
	}
      }
    }
    
    assert(false); // add somethign 
    
    if(!tweight) return 0; /* usually due to empty clusters */
    result /= tweight;
    return result;
  } else {
    return __kt_euclid_slow(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose);
  }
}

/**
   @brief compute the Cityblock (or: Manhatten distance) between two rows or columns.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
t_float kt_cityblock(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const s_allAgainstAll_config_t self) {

  if( (transpose == 0) && self.forNonTransposed_useFastImplementaiton) {

    if(self.forNonTransposed_useFastImplementaiton_useSSE) {
      assert(false); // add somethign 
    }
    
    assert(false); // add somethign 
    return T_FLOAT_MAX;
  } else {
    return __kt_cityblock_slow(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose);
  }

}
