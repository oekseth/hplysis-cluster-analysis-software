#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.

/**
   @brief examplfiy the writign and reading to/from a file using logics in our "kt_matrix.h" API.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- general: use logics in our "kt_matrix.h" 
   -- related: a permtuation of our "tut_matrix_1_toAndFromFile_values.c".
**/
int main() 
#else
  void tut_parse_5_sparseList_score1Score2()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 10; 
  //!
  //! Allocate:
  s_kt_list_1d_fPair_t obj_data = init__s_kt_list_1d_fPair_t(nrows);
  //!
  //! Set values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    set_scalar__s_kt_list_1d_fPair_t(&obj_data, row_id, MF__initVal__s_ktType_fPair((t_float)row_id, (t_float)row_id));
  }
  //! Export result:
  const char *fileName_matrix = "sampleMatrix.tsv";
  const bool is_ok = export__tsv__s_kt_list_1d_fPair_t(&obj_data, fileName_matrix);
  assert(is_ok);
  //!
  //! Read input-file:
  s_kt_list_1d_fPair_t obj_fromFile = initFromFile__s_kt_list_1d_fPair_t(fileName_matrix);    
  //! 
  //! Comapre results: we expec tboth matrices to be equal:
  assert(obj_fromFile.list_size >= nrows);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    assert(MF__isEqual__s_ktType_fPair((obj_fromFile.list[row_id]), (obj_data.list[row_id] )));
  }

  //!
  //! De-allocates:
free__s_kt_list_1d_fPair_t(&obj_fromFile);
free__s_kt_list_1d_fPair_t(&obj_data);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

