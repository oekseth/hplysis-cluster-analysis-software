#ifndef kt_terminal_clusterResult_h
#define kt_terminal_clusterResult_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_terminal_clusterResult
   @brief a lgoical wrapper to combine a number of cluster-algorithms inot one unifed appraoch and one unifed strucutre of result-files (oekseth, 06. nov. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for a terminal-API pelase see our "kt_terminal.h" header-file
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_kt_correlationFunction.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "kt_terminal_parser.h"
#include "kt_matrix.h"
#include "kt_matrix_filter.h"
#include "e_kt_correlationFunction.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_clusterAlg_SOM_resultObject.h"
#include "kt_clusterAlg_hca_resultObject.h"

/**
   @enum e_kt_terminal_clusterResult__format
   @brief describes the result-format to be sued when exproting the result-set (oekseth, 06. des. 2016)
 **/
typedef enum e_kt_terminal_clusterResult__format {
  e_kt_terminal_clusterResult__format_tsv_relation,
  e_kt_terminal_clusterResult__format_tsv_matrix,
  e_kt_terminal_clusterResult__format_javaScript,
  e_kt_terminal_clusterResult__format_JSON
} e_kt_terminal_clusterResult__format_t;

//! @return the enum-representaiotn descrinbg the result-format to use (oekseth, 06. ds. 2016)
e_kt_terminal_clusterResult__format_t get_enumType__e_kt_terminal_clusterResult__format_t(const bool isTo_inResult_isTo_useJavaScript_syntax, const bool isTo_inResult_isTo_useJSON_syntax, const bool isTo_useMatrixFormat);

/**
   @struct s_kt_clusterResult_config
   @brief provides a configruation of the cluster-strategies supported by the hpLysis software (oekseth, 06. des. 2016).
   @remarks among others used in the "kt_terminal" object to gneralise clsuter-algorithm cofngiruations.
 **/
typedef struct s_kt_clusterResult_config {
  bool isTo_subDivideMatrix_usingDisjointForest;

  //! ----------------------------------
  //! 
  //! HCA-clustering
  //s_kt_correlationMetric_t hca_method; 
  char hca_method; bool isToCompute_hca;
  //! ----------------------------------
  //! 
  //! k-means and k-medians clsutering
  //s_kt_correlationMetric_t kmeans_kmeans_metric;
  bool isToCompute_kmeans; bool kmean_isToUse_kMedian;
  bool kmean_kmean_isToUse_medoidInComputation;   
  uint kmean_npass; uint kmean_kmean_nclusters;
  uint *kmean_result_clusterId;
  t_float kmean_result_error; 
  uint *kmean_result_mapOf_iffound; //! descirption: If kmedoids is successful: the number of times the optimal clustering solution was found. 

  //! ----------------------------------
  //! 
  //! Self Oranising Maps (SOM)
  bool isToCompute_som;
  uint som_nxgrid; uint som_nygrid; t_float som_initTau; uint som_niter;

  //! ----------------------------------
  //! 
  //! Feature-vector-clsutering (eg, PCA):
  bool isToCompute_pca;
  

} s_kt_clusterResult_config_t;


//! Initaite the s_kt_clusterResult_config_t object to default values.
void init__s_kt_clusterResult_config_t(s_kt_clusterResult_config_t *self); //, s_kt_correlationMetric_t metric_insideClustering);


/**
   @struct s_kt_clusterResult_data
   @brief hold the reulst-data, a result-data compaticble with our "g_tree_ds.js" java-script-calss (oekseth, 06. des. 2016).
   @remarks among others used in the "kt_terminal" object to gneralise clsuter-algorithm cofngiruations.
 **/
typedef struct s_kt_clusterResult_data {
  //! --------------------
  uint nrows; //! where we expect the "nrows" to also describe the "ncols" proeprty.
  //!
  //! The configuraitons;
  bool for_som;
  bool for_kMeans;
  bool for_hca;
  bool for_pca;
  uint cnt_dataObjects_toExport;
  //! --------------------
  uint *map_1dOf_vertexTypes_partOfCluster__som__centroidId; //! which if set: idneities the central vertex-id best describing a given SOM-id
  uint *map_1dOf_vertexTypes_partOfCluster__som_x; //! which if set: idneities the SOM-id which a given vertex is part of
  uint *map_1dOf_vertexTypes_partOfCluster__som_y;
  uint cnt_SOM_x_added; //! which is used to 'seprate different inseritons wrt. data-sets'.
  //! ---
  // map_1dOf_vertexTypes_boolean_isOfType_imaginary_kMeans;, //! which if set: idnetiies the vertices classified as a kMeans-imgarinary-node
  uint *map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId; //! which if set: idneities the central vertex-is best describing the k-means-cluster which a given vertex is part of
  //! ---	
  t_float *map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount;  //! which if set: describes the node-distance to the root-vertex.
  t_float *map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum;
  // map_1dOf_vertexTypes_partOfCluster__hca_lowerPst_30;, //! which if set: identies the vertices which are part of the 'lower 30' per-cent-part of a dendodram/HCA which a vertex is part of.
  //1d_mapOf_vertexTypes_partOfCluster__;, //! which if set: 
  //! ------------------------------------------------
  //!
  //! 2d-score-matrices describing the cluster-results:
  //! Note: the [”elow] matrices are expected having the same diemsions as the matrixOf_linkScores 'main distance-matrix' (oekseth, 06. nov. 201&).
  t_float **distancesIn_2d_correlationMatrix; //! ie, the default correlation-matrix.
  t_float **distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix ; //! which if set: is expected to describe the internal distances between vertices in a SOM cluster (ie, where we have combiend the sub-mtracies from the SOM-calculuations into one 'unfied' data-matrix).
  t_float **distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid; //! which if set: is the distance from each vertex-cluster-centroid to each 'of the possible vertices in the clsuter': the distance from a cluster-center to all otehr veritces.
  //distancesIn_2d_clusterFrom_kMeans_to_kMeansId ;, //! which if set: describes the 
  t_float **distancesIn_2d_clusterFrom__hcaToplogy__all_topology ; //! which if set: describes/idneities the direct relationshisp in the comptued dednogram-tree' (ie, where a distance!= 0 indicates a relationship)
  t_float **distancesIn_2d_pca_appliedToCorrelationMatrix;  //! which if set: hold the corrleation-atmrix after the corrleation-matrix has been adjusted using the most isignfcnat PCA-eigen-vectors.

} s_kt_clusterResult_data_t;

/**
   @brief intiates the "s_kt_clusterResult_data_t" strucutre (oekseth, 06. des. 201&)
   @param <self> is the object to intaite.
   @param <nrows> is the diemsion of the amtrix: we expect the result-matrix to be in "[nrows, nrows]" diemsion.
   @param <for_som> which is to be set if this result-option is to be epxorted
   @param <for_kMeans> which is to be set if this result-option is to be epxorted
   @param <for_hca> which is to be set if this result-option is to be epxorted
   @param <for_pca> which is to be set if this result-option is to be epxorted
 **/
void init__s_kt_clusterResult_data(s_kt_clusterResult_data_t *self, const uint nrows, bool for_som, bool for_kMeans, bool for_hca, bool for_pca);
/**
   @brief exports the data-set for all formats
   @param <self> is the object to intaite.
   @param <nrows> is the diemsion of the amtrix: we expect the result-matrix to be in "[nrows, nrows]" diemsion.
   @remarks a generalizaiton of the "init__s_kt_clusterResult_data(..)" function.
**/
void init_exportForAllFormats_s_kt_clusterResult_data(s_kt_clusterResult_data_t *self, const uint nrows);

/**
   @brief de-allocates the s_kt_clusterResult_data_t object
 **/
void free__s_kt_clusterResult_data(s_kt_clusterResult_data_t *self);

/**
   @brief update the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void updateClusterResults__kMeans__s_kt_clusterResult_data_t(s_kt_clusterResult_data_t *self, const s_kt_clusterAlg_fixed_resultObject *clusterResults, const uint *keyMap_localToGlobal);
/**
   @brief update the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @param <nygrid> is the number of grid-postion in the x-drieicotn.
   @param <nxgrid> is the number of grid-postion in the y-drieicotn.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void updateClusterResults__SOM__s_kt_clusterResult_data_t(s_kt_clusterResult_data_t *self, const s_kt_clusterAlg_SOM_resultObject_t *clusterResults, const uint *keyMap_localToGlobal, const uint nxgrid, const uint nygrid);
/**
   @brief write out the cotnents of the s_kt_clusterResult_data_t object usign the format specifed in the input-argumetns
   @param <self> is the object to to use.
   @param <matrix> is the correlation-matrix which we export.
   @param <resultFormat> is the reuslt-format to exprot the clsuter-results into.
   @param <resultPrefix> is teh file-rpefix to be sued: if "resultPrefix == NULL" then we write out the reseult to the stream_out "stdout" stream
   @param <stream_out> if "stream_out " is used then we use the latter in the result-generation: an example of "stream_out" is if the latter is sset to stdout
   @return true upon success
 **/
bool writeOut__s_kt_clusterResult_data_t(const s_kt_clusterResult_data_t *self, s_kt_matrix_t *matrix, const e_kt_terminal_clusterResult__format_t resultFormat, const char *resultPrefix, FILE *stream_out);

//! @return an approxmiatuion of the number of clsuter-data-objects to genreate (oekseth, 06. des. 2016).
static uint get_cntDataObjects_toGenerate__s_kt_clusterResult_config_t(const s_kt_clusterResult_config_t *self) {
  uint cnt_dataObjects_toGenerate = 0;
  if(self->isToCompute_hca) {cnt_dataObjects_toGenerate++;}
  if(self->kmean_isToUse_kMedian) {cnt_dataObjects_toGenerate++;}
  if(self->isToCompute_som) {cnt_dataObjects_toGenerate++;}
  if(self->isToCompute_pca) {cnt_dataObjects_toGenerate++;}
  //! -----------
  return cnt_dataObjects_toGenerate;
}
/**
   @brief perform clsuter-anlasys and exports the result(s).
   @param <self> is the object to to use.
   @param <matrix> is the data-set to be clustered: may either be 'raw data' or a distance-matrix genrated from a call to our "kt_matrix__distancematrix(..)", where the latter function is defined in our "kt_api.h".
   @param <metric_insideClustering> is the metric to be used during the clsutering (ie, if the specified clsutering-algrotihms makes use of any distnace-metric inside the 'clsuter-alorithm itself').
   @param <resultFormat> is the reuslt-format to exprot the clsuter-results into.
   @param <resultPrefix> is teh file-rpefix to be sued: if "resultPrefix == NULL" then we write out the reseult to the "stdout" stream
   @return true upon success
   @remarks if configured collects cluster-rpedictison from a number of clsutering-algorithms, and thereafter collects the cluster-algorithm-results into a unfiied result-strucutre, a structure exported to the "resultPrefix" file-type using our "s_kt_clusterResult_data_t" data-structure
 **/
bool performClusterAnalysis__andGenerateResults__s_kt_clusterResult_config_t(s_kt_clusterResult_config_t *self, s_kt_matrix_t *matrix, s_kt_correlationMetric_t metric_insideClustering, const e_kt_terminal_clusterResult__format_t resultFormat, const char *resultPrefix);

#endif //! EOF
