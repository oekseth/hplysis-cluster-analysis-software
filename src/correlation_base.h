#ifndef correlation_base_h
#define correlation_base_h

#include "configure_cCluster.h"
/* #include "list_uint.h" */
#include "def_intri.h"
#include "log_clusterC.h"
#include "correlation_enums.h"

//! Note: this funciton is used to 'only' iterate throug half of the items in a [][] matrix.
void __get_updatedNumberOF_lastSizeChunk_entry(const uint nrows, const uint SM, uint *numberOf_chunks, uint *chunkSize_row_last);


//! Identify the last chunk-size for datasets where nrows and ncols are not divisible by SM.
void __get_updatedNumberOF_lastSizeChunk(const uint nrows, const uint ncols, const uint SM, uint *chunkSize_row_last, uint *chunkSize_col_last);

//! @return the sum of values of the vector
t_float get_sum_SIMD_vetor1d__float(const uint size, const t_float *list);

//! @reutrn the max value for a matrix.
  t_float get_maxValue__float(const uint nrows, const uint ncols, t_float **data, const bool isTo_use_fastFunction/* = true*/);
  //! @reutrn the max value for a matrix.
  t_float get_minValue__float(const uint nrows, const uint ncols, t_float **data, const bool isTo_use_fastFunction/* = true*/);
    //! @reutrn the min value for a matrix.
  char get_minValue__char(const uint nrows, const uint ncols, char **data, const bool isTo_use_fastFunction/* = true*/);
  //! @reutrn the max value for a matrix.
  char get_maxValue__char(const uint nrows, const uint ncols, char **data, const bool isTo_use_fastFunction/* = true*/);
    //! @reutrn the min value for a matrix.
  uint get_minValue__uint(const uint nrows, const uint ncols, uint **data, const bool isTo_use_fastFunction/* = true*/);
  //! @reutrn the max value for a matrix.
  uint get_maxValue__uint(const uint nrows, const uint ncols, uint **data, const bool isTo_use_fastFunction/* = true*/);

  //! @return true if we need to 'care' cabotu the mask-property when calculating matrices:
  bool matrixMakesUSeOf_mask(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const bool transpose, const uint iterationIndex_2);
//! A Nno-SSE permtuationi of the "matrixMakesUSeOf_mask(..)" function.
  bool matrixMakesUSeOf_mask__noSSE(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const bool transpose, const uint iterationIndex_2);
//! @return true if we need to 'care' cabotu the mask-property when calculating matrices:
bool matrixMakesUSeOf_mask__uint(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  uint** mask1, uint** mask2, const bool transpose, const uint iterationIndex_2);

#endif //! EOF
