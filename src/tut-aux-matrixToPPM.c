#include "kt_matrix.h"
#include "export_ppm.h" //! which is used for *.ppm file-export.

//! Expot the matrix as a PPM-file.
void _tut_aux_toPPM(s_kt_matrix_t *mat_export, const char *file_name_result) {
  { //! Apply a pre-nromaiaont-step:
    { //! Make the scores psoivie:
      //! Note: background: the ppm-exprt does not eseme to work when teh scores are negative
      // FIXME: is below pre-step correct?
      t_float score_min = T_FLOAT_MAX;
      for(uint row_id = 0; row_id < mat_export->nrows; row_id++) {
	for(uint col_id = 0; col_id < mat_export->ncols; col_id++) {
	  const t_float score = mat_export->matrix[row_id][col_id];
	  if(isOf_interest(score)) {
	    // score_max = macro_max(score_max, score);
	    score_min = macro_min(score_min, score);
	  }
	      }
      }
      if(score_min < 0) {
	score_min *= -1.0; //! ie, remove the sign.
	for(uint row_id = 0; row_id < mat_export->nrows; row_id++) {
	  for(uint col_id = 0; col_id < mat_export->ncols; col_id++) {
	    mat_export->matrix[row_id][col_id] += score_min; //! hence using the offset to assure that all values are >= 0
	  }
	}
      }
    } //! end of prestep:positive-property.
    //allocOnStack__char__sprintf(2000, str_fileName, "%s-artificalData-n%u-%s-f%ut%u-image.tsv", filePrefix, nrows, "minInsideMaxOutside", (uint)score_weak, (uint)score_strong);	  
    //kt_storage_resultOfClustering_exportTo_ppm(mat_export, /*file_base=*/"", str_fileName, /*isTo_alsoExportTo_tsv=*/true);
  }
  //	    const char *file_name_result_img = "tmp_img.ppm";
  //fprintf(stderr, "(info:export::ppm)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
  ppm_image_t *picture = exportTo_image ( mat_export->matrix, mat_export->nrows, mat_export->ncols, mat_export->nameOf_rows, mat_export->nameOf_columns);
  ppm_image_write ( file_name_result, picture );
  ppm_image_finalize ( picture );	
}

/**
   @brief provide lgois for converitng a hpLysis-matirx to a PPM image file
   @author Ole Kristian Ekseth (oekseth, 26. Aug. 2020).
   @remarks compile:
   -- g++ -O2 -g tut-aux-matrixToPPM.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_ppm.exe
**/
int main(const int argc, char * argv[])  {
  //! Print details of usage:
  if(argc < 3) {
    fprintf(stderr, "Usage\t %s\t%s\t%s\n", argv[0], "input-file", "result-file");
    return -1;
  } else if(argc > 3) {
    fprintf(stderr, "!! \t Error: \tExpeccted '3' arguments, while you provided '%d' arguments. Usage\t %s\t%s\t%s\n", argc, argv[0], "input-file", "result-file");
    return -1;
  }
  const char *input_file = argv[1];


  { //! Validate that the file exxists:
    FILE *file = fopen(input_file, "r");
    if (file == NULL) {
      fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", input_file, __FUNCTION__, __FILE__, __LINE__);
      return -1;
    }
  }
  
  //! Load data:
  s_kt_matrix_fileReadTuning_t read_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  s_kt_matrix_t mat_export = readFromAndReturn__file__advanced__s_kt_matrix_t(input_file, read_config);
  if(mat_export.nrows == 0) {
    fprintf(stderr, "!!\t Unable to open file=\"%s\", at %s:%d\n", input_file, __FILE__, __LINE__);
  }
  //! Export ddata:
  const char *file_name_result = argv[2];
  _tut_aux_toPPM(&mat_export, file_name_result);
  //! De-allocate:
  free__s_kt_matrix(&mat_export);

  
  //! ------------------------------------
  return 0;
}
