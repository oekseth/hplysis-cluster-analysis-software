#ifndef hp_frameWork_CCM_boundaries_h
#define hp_frameWork_CCM_boundaries_h
/**
   @file 
   @brief povides a frawmork for analsysing boundaries of Cluster Comparison / Covnegence Metrivcs (CCMs)

 **/

#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "kt_list_1d.h"
#include "hp_distance.h"
// ***********************************************************
#include "kt_storage_resultOfClustering.h" //!  lgocis for summarizing core-attributes of the data, explroed across different sim-metrics
//#include "kt_distributionTypesC.h" //! which simplfies access to distrubion-types.
// ***********************************************************


static void __unsafeWithSideEffects_export_matrixToPPM(s_kt_matrix_t *mat_export, const char *filePrefix) {
  allocOnStack__char__sprintf(2000, str, "%s.tsv", filePrefix);	  
  const bool is_ok = export__singleCall__s_kt_matrix_t(mat_export, str, NULL); assert(is_ok);
  { //! Export the result:
    { //! Make the scores psoivie:
      //! Note: background: the ppm-exprt does not eseme to work when teh scores are negative
      // FIXME: is below pre-step correct?
      t_float score_min = T_FLOAT_MAX;
      for(uint row_id = 0; row_id < mat_export->nrows; row_id++) {
  	for(uint col_id = 0; col_id < mat_export->ncols; col_id++) {
  	  const t_float score = mat_export->matrix[row_id][col_id];
  	  if(isOf_interest(score)) {
  	    // score_max = macro_max(score_max, score);
  	    score_min = macro_min(score_min, score);
  	  }
  	}
      }
      if(score_min < 0) {
  	score_min *= -1.0; //! ie, remove the sign.
  	for(uint row_id = 0; row_id < mat_export->nrows; row_id++) {
  	  for(uint col_id = 0; col_id < mat_export->ncols; col_id++) {
  	    mat_export->matrix[row_id][col_id] += score_min; //! hence using the offset to assure that all values are >= 0
  	  }
  	}
      }
    } //! end of prestep:positive-property.
    allocOnStack__char__sprintf(2000, str, "%s.ppm", filePrefix);
    //allocOnStack__char__sprintf(2000, str_fileName, "%s-artificalData-n%u-%s-f%ut%u-image.tsv", filePrefix, nrows, "minInsideMaxOutside", (uint)score_weak, (uint)score_strong);
    kt_storage_resultOfClustering_exportTo_ppm(*mat_export, /*file_base=*/"", str, /*isTo_alsoExportTo_tsv=*/true);
  }
  
}


typedef struct s_eval_ccm_defineSyntMatrix {
  /**
     @brief configure the generatiniong of the sytentic matrix.
   **/
  const char *filePrefix;
  uint nrows;
  t_float score_weak;
  t_float score_strong;
  uint cnt_clusters;
} s_eval_ccm_defineSyntMatrix_t;

s_eval_ccm_defineSyntMatrix_t init_s_eval_ccm_defineSyntMatrix_t(const char *filePrefix, const uint nrows, const t_float score_weak, const t_float score_strong, const uint cnt_clusters) {
  s_eval_ccm_defineSyntMatrix_t self;
  //!
  //!
  self.filePrefix   = filePrefix;
  self.nrows        = nrows;
  self.score_weak   = score_weak;
  self.score_strong   = score_strong;
  self.cnt_clusters = cnt_clusters;
  //!
  return self;
}



typedef struct s_eval_ccm_booleanSensitivity_countCase {
  /**
     @brief hold counters for True Postives (TPs), True Negaties (TNs), etc.
   **/
  long long int cnt_positive;
  long long int cnt_negative;
  //!
  //! Note: below proeprties are used to construct "cnt_negative, cnt_positive" scores for a new/next caculcaion-comptuation-run.
  long long int inputToSensitivty_observed_True_count;
  t_float inputToSensitivty_observed_True_min;
  t_float inputToSensitivty_observed_True_max;    
  /* long long int cnt_TP; */
  /* long long int cnt_FP; */
  /* //! */
  /* long long int cnt_TN; */
  /* long long int cnt_FN;   */
} s_eval_ccm_booleanSensitivity_countCase_t;

s_eval_ccm_booleanSensitivity_countCase_t init_s_eval_ccm_booleanSensitivity_countCase_t() {
  s_eval_ccm_booleanSensitivity_countCase_t self;
  self.cnt_positive = 0;
  self.cnt_negative = 0;
  //!
  self.inputToSensitivty_observed_True_count = 0;
  self.inputToSensitivty_observed_True_min = T_FLOAT_MAX;
  self.inputToSensitivty_observed_True_max = T_FLOAT_MIN_ABS;  
  //! ---
  //self.cnt_FP = 0;
  //self.cnt_FN = 0;    
  //!
  return self;
}

typedef enum e_eval_ccm_booleanSensitivity {
  /**
     @brief used to consturct a co-occurence matrix of [True, False]x[Positive,Negative]
   **/
  e_eval_ccm_booleanSensitivity_Posititve,
  e_eval_ccm_booleanSensitivity_Negative,  
  // ---------
  e_eval_ccm_booleanSensitivity_undef
} e_eval_ccm_booleanSensitivity_t;

typedef enum e_eval_ccm_groupOf_artificialDataClass {
  /**
     @brief ...
   **/
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit,
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutside,
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideDecreasedScoreForSmallerClusters,
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd1,
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100,
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear,
  e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideReverseChanging_constantToAdd100_twoNestedLinear,
  //e_eval_ccm_groupOf_artificialDataClass_,    
  //! -------------------
  e_eval_ccm_groupOf_artificialDataClass_undef
} e_eval_ccm_groupOf_artificialDataClass_t;

/**
   @brief generates data given enum_id
   @return a cluster-shape
 **/
s_hp_clusterShapes_t generateData_s_eval_ccm_defineSyntMatrix_t(const s_eval_ccm_defineSyntMatrix_t *self, const e_eval_ccm_groupOf_artificialDataClass_t enum_id, const uint size_left, const uint size_right) {
  //assert(size_left != size_right); // TODO: cosndier dropping thsi requriement.
  // ----
  // FIXME: future: how may the egneridc data-tyeps be extended to user-defined data-types?
  // ----
  // FIXME: artilce: update artilce witht eh equations which each data-set represent ... which si fo improtance/interest due to ...??...
  // ----
  

  assert(self->cnt_clusters < self->nrows);
  // FIXME: based on the results ... update the descritpion of this cluster-ensabmle-permtuation
  //! Note: idea: ... relvitly adjust the number of clusters between the two cluster-ensamble-partitions ... where the number of clusters in each partion scales linearly to the size of each parition ... while the numberr of clsuters (in the data-set) remains constant: $ ... frac = |clusters|/|vertices|, |clusters \in partition(1)| = frac * |vertices \in partition(1)|   ... |clusters \in partition(1)| .... k = vertices(cluster_i)/ ... /$ %!< FIXME: is latter correct?
  const uint nrows_local = self->nrows;
  t_float percentage_clusters =  (t_float)self->cnt_clusters / (t_float)self->nrows ;
  uint cnt_clusters_left  = (uint)(percentage_clusters * (t_float)size_left);
  if(cnt_clusters_left >= size_left) {
    cnt_clusters_left = size_left - 1;
  }
  //if(cnt_clusters_left == 0) {cnt_clusters_left = 1;}
  uint cnt_clusters_right = self->cnt_clusters - cnt_clusters_left;
  if(cnt_clusters_right > size_right) {
    cnt_clusters_right = size_right;
  }
  //if(cnt_clusters_right == 0) {cnt_clusters_right = 1;}


  if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit) {
    //! Note: data: only change the size of the tractiosn, and NOT the number of clsuters in each fraction. 
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    //! Construct for set=1:
    assert(size_left > 0); //! ie, what we expect in the below:
    //bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
    bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, /*cnt_clusters=*/1, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
    assert(is_ok);
    assert(self->cnt_clusters > 1);
    assert(self->cnt_clusters < self->nrows);
    uint cnt_clusters_local = self->cnt_clusters -1;
    if(cnt_clusters_local > size_right) {cnt_clusters_local = size_right;}

    //bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows_local, cnt_clusters_local, /*cnt_clusterStartOffset=*/0);

    if(cnt_clusters_local > 0) {is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, /*cnt_clusters=*/cnt_clusters_local, /*cnt_clusterStartOffset=*/1, /*vertexCount__constantToAdd0*/100);}
    //is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /*cnt_clusterStartOffset=*/self->nrows/2, /*vertexCount__constantToAdd0*/100);
    //! -------------------------
    assert(is_ok);
    return obj_shape;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutside) {
    //__generateArtificalData_export(filePrefix, str_topoType, nrows, score_weak, score_strong
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    //! Construct for set=1:
    //! ---
    // FIXME: is below permtaution aceptable ... ie, that there should be a lienarty in below?
    // FIXME: based on the results ... update the descritpion of this cluster-ensabmle-permtuation
    /* uint cnt_clusters_local = self->cnt_clusters + size_right; */
    /* const uint nrows_local = self->nrows; */
    /* if(cnt_clusters_local > nrows_local) {cnt_clusters_local = nrows_local;} */
    //! ---
    bool is_ok = true;
    if(cnt_clusters_left > 0) {buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0);}
    //bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows_local, cnt_clusters_local, /*cnt_clusterStartOffset=*/0);
    if(cnt_clusters_right > 0) {is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left);}
    //const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows_local, cnt_clusters_local, /*cnt_clusterStartOffset=*/0);
    assert(is_ok);
    return obj_shape;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideDecreasedScoreForSmallerClusters) {
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);
    //! Construct for set=1:
    //! ---
    // FIXME: is below permtaution aceptable ... ie, that there should be a lienarty in below?
    // FIXME: based on the results ... update the descritpion of this cluster-ensabmle-permtuation
    uint cnt_clusters_local = self->cnt_clusters + size_right;
    const uint nrows_local = self->nrows;
    if(cnt_clusters_local > nrows_local) {cnt_clusters_local = nrows_local;}
    //! ---
    bool is_ok = true;
    if(cnt_clusters_left > 0) {is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0);}
    //bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows_local, cnt_clusters_local, /*cnt_clusterStartOffset=*/0);
    if(cnt_clusters_right > 0) {is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left);}
    //bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/0);
    //bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows_local, cnt_clusters_local, /*cnt_clusterStartOffset=*/0);
    //is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left, /*vertexCount__constantToAdd0*/0);
    //const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, self->nrows, self->cnt_clusters, /*cnt_clusterStartOffset=*/0);
    assert(is_ok);	 
    //! -------------------------
    assert(is_ok);
    return obj_shape;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd1) {
    //__generateArtificalData_export(filePrefix, str_topoType, self->nrows, score_weak, score_strong
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside); //__decreasedScoreForSmallerClusters);
    //! ---
    // FIXME: is below permtaution aceptable ... ie, that there should be a lienarty in below?
    // FIXME: based on the results ... update the descritpion of this cluster-ensabmle-permtuation
    uint cnt_clusters_local = self->cnt_clusters + size_right;
    const uint nrows_local = self->nrows;
    if(cnt_clusters_local > nrows_local) {cnt_clusters_local = nrows_local;}
    //! ---
    //! Construct for set=1:
    if(cnt_clusters_left > 0) {
      const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0,  /*vertexCount__constantToAdd0*/0);
      //const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, self->nrows, self->cnt_clusters, /*cnt_clusterStartOffset=*/1);
      assert(is_ok);
    }
    if(cnt_clusters_right > 0) {
      const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left,  /*vertexCount__constantToAdd0*/0);
      assert(is_ok);
    }
    //! -------------------------
    return obj_shape;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100) {
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    //! Construct for set=1:
    if(cnt_clusters_left > 0) {
      const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0,  /*vertexCount__constantToAdd0*/1);
      //const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, self->nrows, self->cnt_clusters, /*cnt_clusterStartOffset=*/1);
      assert(is_ok);
    }
    if(cnt_clusters_right > 0) {
      const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left,  /*vertexCount__constantToAdd0*/1);
      assert(is_ok);
    }
    //const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, self->nrows, self->cnt_clusters, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/1);
    // --------------
    return obj_shape;
  } else if( (enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideReverseChanging_constantToAdd100_twoNestedLinear) || (enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear) ){
    //! ---
    // FIXME: is below permtaution aceptable ... ie, that there should be a lienarty in below?
    //const uint cnt_clusters_right = (uint)(percentage_clusters * (t_float)size_right);    
    //!
    //!
    if (enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear) {
      //! -------------------------
      s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      //! Construct for set=1:
      //! Construct for set=1:
      //self->cnt_clusters/2
      bool is_ok = true;
      if(cnt_clusters_left > 0) {is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);}
      //bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, self->nrows/2, self->cnt_clusters/2, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
      assert(is_ok);
      //! 
      if(cnt_clusters_right > 0) {is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left, /*vertexCount__constantToAdd0*/100);}
      //is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows_local, cnt_clusters_local, /*cnt_clusterStartOffset=*/self->cnt_clusters/2, /*vertexCount__constantToAdd0*/100);
      assert(is_ok);
      assert(is_ok);
      return obj_shape;
    } else { //if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideReverseChanging_constantToAdd100_twoNestedLinear) {
      s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      //! Construct for set=1:
      //bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, self->nrows/2, self->cnt_clusters/2, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
      bool is_ok = true;
      if(cnt_clusters_left > 0) {is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, cnt_clusters_left, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);}
      //bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, self->nrows/2, self->cnt_clusters/2, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
      assert(is_ok);
      //! 
      if(cnt_clusters_right > 0) {is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, cnt_clusters_right, /*cnt_clusterStartOffset=*/cnt_clusters_left, /*vertexCount__constantToAdd0*/100);}
      //! -------------------------
      assert(is_ok);
      return obj_shape;
    }
  } else {
    assert(false); //! ie, then add supprot for this.
    return setToEmptyAndReturn__s_hp_clusterShapes_t();
  }
  
}

//! @return a string-represetnation of the e_eval_ccm_groupOf_artificialDataClass_t
//! @remarks of improtance when lablling dta, eg, rows in a result-tale
const char *get_string_s_eval_ccm_defineSyntMatrix_t(const e_eval_ccm_groupOf_artificialDataClass_t enum_id) {
  if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit) {
    const char *str_topoType_base = "minInsideMaxOutsideLinearChanging-constantToAdd100-twoNestedLinear-dynamicSplit";
    return str_topoType_base;
    // --- 
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutside) {
    const char *str_topoType = "minInsideMaxOutside";
    // --- 
    return str_topoType;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideDecreasedScoreForSmallerClusters) {
    const char *str_topoType = "minInsideMaxOutsideDecreasedScoreForSmallerClusters";
    return str_topoType;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd1) {
    const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd1";
    return str_topoType;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100) {
    const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd100";
    return str_topoType;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear) {
    const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd100-twoNestedLinear";
    return str_topoType;
  } else if(enum_id == e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideReverseChanging_constantToAdd100_twoNestedLinear) {
    const char *str_topoType = "minInsideMaxOutsideReverseChanging-constantToAdd100-twoNestedLinear";
    return str_topoType;
    /*
  } else if(enum_id == ) {
    return str_topoType;
  } else if(enum_id == ) {
    return str_topoType;        
    */
  } else {
    fprintf(stderr, "!!\t(not-supported)\t Add support for enum=%u, at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, then add supprot for this.
    return NULL;
  }  
}

typedef struct s_eval_ccm_defineSyntMatrix_comparisonBasis {
  /**
     @brief holds a configuraiton-spec when comparing the perfomrance of complex Ariticcal Data-Shapes
     @remarks Useful results (generated from below collection):
     -- data: similar shapes: pin-point cases where CCMs are unable to seperate traits (eg, of improtance in Digtial Forenics, and its subcateorgies, such as clsuter-analsys, etc.)
     -- data: observed: True(min--max, average),  False(min--max, average)
     -- data: [True, False]x[Positive,Negative]: ...
     -- data: histogram: ...
     ... null-hyptoesis (Eq. \ref{}): ... a single benchmark of CCM-scores is suficcent/rerpresentive to understand other/related data-sets. ... the topolgocical impacct of data has an singicant constirubtion of the CCM-scores, hence ... 
     ....  histo-type is used/applied to estiamte the seperatbility across different dimensions ... apply a row-based data-nromsaiton before rpign the socreos out (to get an approxmiate of the seperaily, rhater thant he sim-scores themself) .... hyptoesis: if the sepraility decreases with icnreasing number of dimeisons, then ... 
     -- data: linear-order versus hyptossi: default spec. correlation: compare to hyptoesis = [enum=e_eval_ccm_groupOf_artificialDataClass_t][e_hypo_shape_size {increase, decrease, bell-shape} ]
     ... insert into a new s_kt_list_1d_float arr_scores[e_hypo_shape_size] ....
     ... provides insight into different classes of ...
     ... 
     -- data: 
     -- InfoVis: 
     -- InfoVis: 
     -- InfoVis: 
     -- InfoVis: 
     -- InfoVis: 
     -- InfoVis: 
     @remarks Complexity levels of usage:
     -- level(0): ... compute CCM-boundaries ....
     -- level(1): ... apply CCM-boundaries to basic data-topologies ... 
     -- level(2): ... 
     -- level(3): ... 
     -- level(): ... 
     -- level(): ... 
     -- level(): ... 
   **/
  const char *file_name_result;
  const char *dir_result; //! an optioanl direciotry-path, to where to store euaxialry-result-files.
  // FIXME: ok: ... a new enum reprsetning the different ARticcal ata traits we evaluate ... 
  // FIXME: ok: .... an enumerated shape_ref[] list ...
  // FIXME: ok: update our 'update' function with below:
  // FIXME: ok: ... if the shape-ref is empty, then set it in the first run .... based on the proper enum ...
  s_hp_clusterShapes_t shape_ref[e_eval_ccm_groupOf_artificialDataClass_undef];
  e_kt_matrix_cmpCluster_metric_t ccm_enum;
  //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum;
  // ok: FIXME: new triplet-struct in "kt_list_1d.h" list_range ... "s_kt_list_1d_tripleUint_t" ... then intaitve it ... 
  // ok: FIXME: new tail-enum ... two different objects ... casePositive , caseNegative .... descrie different TP--TF seperation-cases ... such as ...??... <-- concepetual: why would scuh 'overlapping features' result in ...??...
  // ok:FIXME: update code with the new 'range' attribute ... replace all ->range  calls.
  s_kt_list_1d_tripleT_Float_t list_range[e_eval_ccm_booleanSensitivity_undef]; //! where each tripelt cosnsits of {min, max, ...??..}.
  //t_float range[2];
  s_kt_list_1d_float_t list_histo[e_eval_ccm_booleanSensitivity_undef]; //! ie, an aray to be sued henc os truccting histograms; each elemetn refers to: e_eval_ccm_booleanSensitivity_t eval_case
  // ok: FIXME: make use of "e_eval_ccm_booleanSensitivity_t eval_case" when determining which "counter_result" prop to update; 
  e_eval_ccm_booleanSensitivity_t eval_case;
  FILE *file_out;
  // ok: FIXME: update our 'update' function with below:
  s_eval_ccm_booleanSensitivity_countCase_t counter_result[e_eval_ccm_booleanSensitivity_undef];
  //!
  // ok: FIXME: global_data_id ... init ... incmrenet ... include intot eh file-names we cosntruct/generate ... 
  // ok: FIXME: make use of below ... init ... hypo  ... then de-allocate.
  // ok:FIXME: make use of below ... init ...  metric
  //! Note: data-id ... use a global counter ... and then update the name of the generated PPM-files based on this?
  uli global_data_id; //! sued to isenify the uqnie idneitfy ot the data we ahve elvauted;  the dta-type is defined in "types_base.h"
  s_kt_matrix_t mat_linearOrderOfCCMScores_hypo; //! a collection of different ordeirngs of CCM-scores, eg, linear, reverse, bell-shaped
  //! Note: ... we use a "s_kt_list_1d_float_t" structre instead of amtrices (with fixed size-chunks). This in order to idneityf different clsuters of data-sets (whcich exhibits different infleucne on the CCM-stting in qeustion). Hence, to explroe depdenceies (eg, in benchamrks, ana danslsysi-pipeliens).
  s_kt_list_1d_float_t arr_linearOrderOfCCMScores_result_score[e_eval_ccm_groupOf_artificialDataClass_undef]; //! ie, constuct this sperately for different artifual data-set-combinations.
  s_kt_list_1d_pair_t arr_linearOrderOfCCMScores_result_id[e_eval_ccm_groupOf_artificialDataClass_undef]; //! which holds the (global_data_id, hypo-id) 
  //
  //s_kt_list_1d_pair_t arr_linearOrderOfCCMScores_result_id[e_eval_ccm_groupOf_artificialDataClass_undef]; //! which holds the (global_data_id, best=min=(hyptohesis=corr(matrix, mat_linearOrderOfCCMScores_hypo)));
  /* s_kt_matrix_t mat_linearOrderOfCCMScores_result_scoresMin_value; */
  /* s_kt_matrix_t mat_linearOrderOfCCMScores_result_scoresMin_dataID; */
  /* s_kt_matrix_t mat_linearOrderOfCCMScores_result_scoresMax_value; */
  /* s_kt_matrix_t mat_linearOrderOfCCMScores_result_scoresMax_dataID;     */
  s_kt_correlationMetric_t mat_linearOrderOfCCMScores_evalMetric; //! eg, initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)1
} s_eval_ccm_defineSyntMatrix_comparisonBasis_t;

//! Intiate the oebject:
s_eval_ccm_defineSyntMatrix_comparisonBasis_t init_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(const char *file_name_result) {
  s_eval_ccm_defineSyntMatrix_comparisonBasis_t self;
  //! ---------
  self.file_name_result = file_name_result;
  self.dir_result = NULL; //! an optioanl direciotry-path, to where to store euaxialry-result-files.
  //!
  //! 
  for(uint i = 0; i < e_eval_ccm_booleanSensitivity_undef; i++) {
    self.counter_result[i] = init_s_eval_ccm_booleanSensitivity_countCase_t();
    self.list_histo[i] = setToEmpty__s_kt_list_1d_float_t();
    self.list_range[i] = setToEmpty__s_kt_list_1d_tripleT_Float_t();
    assert(self.list_range[i].list_size == 0);
  }
  //! ---------
  self.file_out = NULL;
  if((file_name_result != NULL) && strlen(file_name_result)) {
    self.file_out = fopen(file_name_result, "wb");
    if (self.file_out == NULL) {
      fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", file_name_result, __FUNCTION__, __FILE__, __LINE__);
    }
  }
  self.ccm_enum = e_kt_matrix_cmpCluster_metric_randsIndex; //! ie, the default choise.
  //!
  //! Init:
  self.mat_linearOrderOfCCMScores_hypo = setToEmptyAndReturn__s_kt_matrix_t();
  for(uint i = 0; i < e_eval_ccm_groupOf_artificialDataClass_undef; i++) {
    self.shape_ref[i] = setToEmptyAndReturn__s_hp_clusterShapes_t();
    self.arr_linearOrderOfCCMScores_result_score[i] = setToEmpty__s_kt_list_1d_float_t();
    self.arr_linearOrderOfCCMScores_result_id[i]    = setToEmpty__s_kt_list_1d_pair_t();
  }
  //! Note: to dynamically get the name of the selected sim-metric, write: 	    const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(curr_metric.metric_id);
  self.mat_linearOrderOfCCMScores_evalMetric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank);
  self.global_data_id = 0; //! which is used to dineityf the quney idnityf of data.
  //self.shape_ref = setToEmptyAndReturn__s_hp_clusterShapes_t();  
  //!
  return self;
}

/**
   @brief Export result-set, and then de-allocte:
 **/
static void free_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(s_eval_ccm_defineSyntMatrix_comparisonBasis_t *self) {
  { //! Print our tresult of counters
    FILE *res = stdout;
    if(self->file_out !=  NULL) {
      res = self->file_out;
    }
    // TODO: cosnider rpitning to a different files.
    //! ---------
    assert(2 == e_eval_ccm_booleanSensitivity_undef); //! which is what we expect for "map_str".
    const char *map_str[2] = {"True", "False"};
    for(uint i = 0; i < e_eval_ccm_booleanSensitivity_undef; i++) {
      fprintf(res, "counter:%s-%s\toccurences\t%lld\n", map_str[i], "Positive", self->counter_result[i].cnt_positive);
      fprintf(res, "counter:%s-%s\toccurences\t%lld\n", map_str[i], "Negative", self->counter_result[i].cnt_negative);
      //! 
      fprintf(res, "counter:%s-%s\toccurences\t%lld\n", map_str[i], "Observed-Total", self->counter_result[i].inputToSensitivty_observed_True_count);
      if(self->counter_result[i].inputToSensitivty_observed_True_min != T_FLOAT_MAX) {
	fprintf(res, "counter:%s-%s\toccurences\t%.2f\n", map_str[i], "Observed-Score-Min", self->counter_result[i].inputToSensitivty_observed_True_min);
      } else {
	fprintf(res, "counter:%s-%s\toccurences\t%s\n", map_str[i], "Observed-Score-Min", "-");
      }
      if(self->counter_result[i].inputToSensitivty_observed_True_max != T_FLOAT_MIN_ABS) {
	fprintf(res, "counter:%s-%s\toccurences\t%.2f\n", map_str[i], "Observed-Score-Max", self->counter_result[i].inputToSensitivty_observed_True_max);
      } else {
	fprintf(res, "counter:%s-%s\toccurences\t%s\n", map_str[i], "Observed-Score-Max", "-");
      }	
    }
    
    //! -------------------------------------------------------
    //! 
    { //! Export: Calcuate sneitivty, specity, etc., and then export these results to a file
      //! Note: useful when analsying ... 
      //! Note: details of beloq euations are givne at: https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-negative-false-positive-and-fal  https://en.wikipedia.org/wiki/Sensitivity_and_specificity 
      //! -------------------------
      //!
      const t_float TP = self->counter_result[0].cnt_positive;
      const t_float TN = self->counter_result[0].cnt_negative;
      //! 
      const t_float FP = self->counter_result[1].cnt_positive;
      const t_float FN = self->counter_result[1].cnt_negative;            
      //!
      //! 
      //! -------------------------
      //!
      //! Write out the results:
      const char *row_label = "Sensitivity and specificity"; //! a label icnlued to simplify filtering of values in the result-file, eg, when cosntrucitng InfoVis-applications.
      //! -------------------------
      //! 
      //! Note: Overall accuracy: ACC = (TP+TN)/(TP+FP+FN+TN)
      const t_float conf_accuracy = (TP+TN) / (TP + TN + FP + FN);
      fprintf(res, "%s\t%s\t%f\n", row_label, "Accuracy (ACC)", conf_accuracy);      
      const t_float conf_misclassification = 1- conf_accuracy;
      fprintf(res, "%s\t%s\t%f\n", row_label, "Misclassification (1 - ACC)", conf_misclassification);      
      //! Note: aka TPR: Sensitivity, hit rate, recall, or true positive rate
      const t_float conf_sensitivity = (TP / (TP + FN));
      fprintf(res, "%s\t%s\t%f\n", row_label, "Sensitivity (TPR)", conf_sensitivity);            
      //! 
      //! Note: Specificity or true negative rate: TNR = TN / (TN+FP) 
      const t_float conf_specificity = (TN / (TN + FP));
      fprintf(res, "%s\t%s\t%f\n", row_label, "Specificity (TNR)", conf_specificity);            
      // FIXME: update below conf_specificity ... as it is examly equal to conf_specificity ... 
      const t_float conf_precision    = (TN / (TN + FP));
      fprintf(res, "%s\t%s\t%f\n", row_label, "Precision", conf_precision);
      const t_float conf_f1 = 2 * ((conf_precision * conf_sensitivity) / (conf_precision + conf_sensitivity));
      fprintf(res, "%s\t%s\t%f\n", row_label, "Confidence (F1)", conf_f1);
      //!
      //! Note: Precision or positive predictive value: PPV = TP/(TP+FP)
      const t_float conf_precision_PPV = TP / (TP+FP);
      fprintf(res, "%s\t%s\t%f\n", row_label, "Precision: Positive Predictive Value (PPV)", conf_precision_PPV);
      //!  Negative predictive value
      const t_float conf_NPV = TN/(TN+FN);
      fprintf(res, "%s\t%s\t%f\n", row_label, "Negative Predictive Value (NPV)", conf_NPV); 
      //! Fall out or false positive rate
      const t_float  conf_FPR = FP/(FP+TN);
      fprintf(res, "%s\t%s\t%f\n", row_label, "Fallout: False Positive Rate (FPR)", conf_FPR);
      //! False negative rate
      const t_float  conf_FNR = FN/(TP+FN);
      fprintf(res, "%s\t%s\t%f\n", row_label, "False Negative Rate (FNR)", conf_FNR);
      //! False discovery rate
      const t_float conf_FDR = FP/(TP+FP);
      fprintf(res, "%s\t%s\t%f\n", row_label, "False Discovery Rate (FDR)", conf_FDR);

      // FIXME: add metric: from \cite{parraga2019unsupervised} [https://www.researchgate.net/profile/Jorge_Parraga-Alava2/publication/338793297_An_Unsupervised_Learning_Approach_for_Automatically_to_Categorize_Potential_Suicide_Messages_in_Social_Media/links/5e2cdb8f299bf152167e1f35/An-Unsupervised-Learning-Approach-for-Automatically-to-Categorize-Potential-Suicide-Messages-in-Social-Media.pdf]: name="F-mesure", definition=$ Precision=TP / (TP+FP), Recall=TP/(TP+FN), \textit{F-Score}=2*(Recall*Precision)(Recall + Precision)$  <-- is the $F-meausre$ the same as the $F_1$ scocre in "https://pdfs.semanticscholar.org/0db9/50ab5898196a18ecac1273a8693ca277ba6e.pdf"? <-- yes!
      // FIXME: add metric: from \cite{} []:
      // FIXME: add metric: from \cite{} []:      
      // FIXME: add metric: 
    }    
  }
  //!
  char dir_result[2000];       memset(dir_result, '\0', 2000); 
  if(self->dir_result != NULL) {
    sprintf(dir_result, "%s", self->dir_result);
  }     
  { //! Score-histo: construt a histogram of the scores .... use the histogram to argue that differnet CCMs has different densityes, and areas of focal poitns
    const uint config_cnt_histogram_bins = 20; // TODO: make this option confiurable?
    //!
    //!
    //! init: mat_res
    const uint cnt_histoTypes = 3;
    s_kt_matrix_t mat_res = initAndReturn__s_kt_matrix(/*nrows=*/(e_eval_ccm_booleanSensitivity_undef*cnt_histoTypes), /*ncols=*/config_cnt_histogram_bins);
    //! 
    { //! Set column-label:
      for(uint i = 0; i < config_cnt_histogram_bins; i++) {
	char str_local[3000]; sprintf(str_local, "bin-%u", i);
	set_stringConst__s_kt_matrix(&mat_res, /*col_id=*/i, str_local, /*addFor_column=*/true);
      }
    }
    //!
    //! 
    //!        
    assert(2 == e_eval_ccm_booleanSensitivity_undef); //! which is what we expect for "map_str".
    const char *map_str[2] = {"True", "False"};
    //!
    for(uint i = 0, case_id = 0; i < e_eval_ccm_booleanSensitivity_undef; i++) {  
      //s_kt_matrix_t  mat_scores = initAndReturn__s_kt_matrix(/*nrows=*/1, /*ncols=*/self->list_histo[i].list_size);
      //! -----------------------------
      { //! Compute the historgram:
	{ //! Set row-label:
	  allocOnStack__char__sprintf(2000, str_local, "%s-%s", map_str[i], "histoType-count");
	  set_stringConst__s_kt_matrix(&mat_res, /*row_id=*/case_id, str_local, /*addFor_column=*/false);
	}
	const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
	//const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
	//const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;    
	const bool config_isTo_forMinMax_useGlobal = true;
	s_kt_list_1d_float_t list_histo = applyTo_list__kt_aux_matrix_binning(&(self->list_histo[i]), config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	//s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(&mat_scores, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	assert(list_histo.list_size <= mat_res.ncols);
	assert(case_id < mat_res.nrows);
	for(uint col_id = 0; col_id < list_histo.list_size; col_id++) {
	  mat_res.matrix[case_id][col_id] = list_histo.list[col_id];
	}
	//! De-allocate:
	free__s_kt_list_1d_float_t(&list_histo);
	//!
	case_id++;
      }
      //! -----------------------------
      { //! Compute the historgram:
	{ //! Set row-label:
	  allocOnStack__char__sprintf(2000, str_local, "%s-%s", map_str[i], "histoType-sum");
	  set_stringConst__s_kt_matrix(&mat_res, /*row_id=*/case_id, str_local, /*addFor_column=*/false);
	}
	//const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
	const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
	//const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;    
	const bool config_isTo_forMinMax_useGlobal = true;
	s_kt_list_1d_float_t list_histo = applyTo_list__kt_aux_matrix_binning(&(self->list_histo[i]), config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	//s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(&mat_scores, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	assert(list_histo.list_size <= mat_res.ncols);
	assert(case_id < mat_res.nrows);
	for(uint col_id = 0; col_id < list_histo.list_size; col_id++) {
	  mat_res.matrix[case_id][col_id] = list_histo.list[col_id];
	}
	//! De-allocate:
	free__s_kt_list_1d_float_t(&list_histo);
	//!
	case_id++;
      }
      //! -----------------------------
      { //! Compute the historgram:
	{ //! Set row-label:
	  allocOnStack__char__sprintf(2000, str_local, "%s-%s", map_str[i], "histoType-countAverageOnBins");
	  set_stringConst__s_kt_matrix(&mat_res, /*row_id=*/case_id, str_local, /*addFor_column=*/false);
	}
	//const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
	//const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
	const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;    
	const bool config_isTo_forMinMax_useGlobal = true;
	s_kt_list_1d_float_t list_histo = applyTo_list__kt_aux_matrix_binning(&(self->list_histo[i]), config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	//s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(&mat_scores, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
	assert(list_histo.list_size <= mat_res.ncols);
	assert(case_id < mat_res.nrows);
	for(uint col_id = 0; col_id < list_histo.list_size; col_id++) {
	  mat_res.matrix[case_id][col_id] = list_histo.list[col_id];
	}
	//! De-allocate:
	free__s_kt_list_1d_float_t(&list_histo);
	//!
	case_id++;
      }      
      //! -----------------------------
      //! De-allocate:
      //free__s_kt_matrix(&mat_scores);
    }
    { //! Export
      //!
      s_kt_matrix_t *matrix = &mat_res;
      { //! Write out the result-matrices, both 'as-is' and 'transposed':
	{
	  allocOnStack__char__sprintf(2000, file_name, "%s%s-%s", "histogram-raw-", dir_result, self->file_name_result);
	  bool is_ok = export__singleCall__s_kt_matrix_t(matrix, file_name, NULL); assert(is_ok); 
	}
	{  /*! Then export a tranpsoed 'verison': */			
	  allocOnStack__char__sprintf(2000, file_name, "%s%s-%s", "histogram-transposed-", dir_result, self->file_name_result);
	  bool is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, file_name, NULL); assert(is_ok); 
	}
      }
      { //! Investigate the impact ... adjsuted to the averaged score ... hencce, to see a scale-invariant change in prediciton.scores
	s_kt_matrix_t mat_cpy = initAndReturn__copy__normalized__s_kt_matrix_t(matrix);
	s_kt_matrix_t *matrix = &mat_cpy;
	{
	  allocOnStack__char__sprintf(2000, file_name, "%s%s-%s", "histogram-raw-normalizedAvg-", dir_result,self->file_name_result);
	  bool is_ok = export__singleCall__s_kt_matrix_t(matrix, file_name, NULL); assert(is_ok); 
	}
	{  /*! Then export a tranpsoed 'verison': */			
	  allocOnStack__char__sprintf(2000, file_name, "%s%s-%s", "histogram-transposed-normalizedAvg-", dir_result, self->file_name_result);
	  bool is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, file_name, NULL); assert(is_ok); 
	}
	//!
	free__s_kt_matrix(&mat_cpy);
      }      
    }
    //! De-allocate:
    free__s_kt_matrix(&mat_res);
  }

  { //! Explore accurayc of the different hytpeossi conceirng lineary in CCM score-change:
    //! Step(1): open file (to hold all results):
    allocOnStack__char__sprintf(2000, file_name_meta_all, "%s%s-%s-%s.tsv", dir_result, self->file_name_result, "forAll", "hypothesis-CcmVShypoScores-sorted");
    FILE *file_out_meta_all = fopen(file_name_meta_all, "wb");
    if(file_out_meta_all == NULL) {
      fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", file_name_meta_all, __FUNCTION__, __FILE__, __LINE__);
    }
    bool file_out_meta_all_headerISAdded = false;

    for(uint type_id = 0; type_id < e_eval_ccm_groupOf_artificialDataClass_undef; type_id++) {
      const e_eval_ccm_groupOf_artificialDataClass_t type_data = (e_eval_ccm_groupOf_artificialDataClass_t)type_id; // eg,: e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit;
      const char *str_topoType_base = get_string_s_eval_ccm_defineSyntMatrix_t(type_data);
      // allocOnStack__char__sprintf(2000, str_topoType, "%s-d%u", str_topoType_base, data_id);      
      s_kt_list_1d_float_t *list_scores = (&self->arr_linearOrderOfCCMScores_result_score[type_id]);
      if(list_scores->current_pos > 0) { //! then data is added:
	
	{ //! Print out a sorted version of each list ... 
	  //! Note: the meta-finraotion icnldued into the rsorted list is enabled through the copmtuation of a ranked list.
	  //! Note: below 'rank-code' either appleis merge-sorte, insertion-sot, or quicksort. An example of a qucisort-call is: quicksort(row_size, /\*input=*\/row, /\*arr_index=*\/arr_index, /\*arr_result=*\/row_tmp_float);
	  //! Note: in "list_rank" a a float-data-type (and not an index-mapptible data-type) is returned. This to icnrease the specify of the ranks (ie, as the ranks may share the same CCM-score)
	  // --- 
	  // TODO: an InfoVis-sue-case where the rnaks mbya be exproted direlcty?
	  // --- 
	  t_float *list_rank =get_rank__correlation_rank(list_scores->current_pos, list_scores->list);  //! a functione defined in "correlation_rank.h".
	  assert(list_rank);
	  if(list_rank != NULL) {
	    //! Insert the ranks into an inverted list:
	    s_kt_list_1d_uint_t  map_inverted = init__s_kt_list_1d_uint_t(list_scores->current_pos);
	    //! Init:
	    for(uint col_id = 0; col_id < list_scores->current_pos; col_id++) {
	      map_inverted.list[col_id] = UINT_MAX;
	    }
	    //! Set the scores:
	    //! FIXME: correct that below does not need apriroy knowledge of <-- otehrise, frist cosntruct a sroted lsit of data-IDs whicch overlap
	    s_kt_list_2d_uint_t map_overlappingRanks_centroid_toOthers = setToEmpty__s_kt_list_2d_uint_t();
	    //!
	    //! Cosntruct the ivnerse mapping-table:
	    for(uint col_id = 0; col_id < list_scores->current_pos; col_id++) {
	      const t_float new_index_ = list_rank[col_id];
	      if(new_index_ == T_FLOAT_MAX) {continue;} //! ie, as we then assume that the value is NOT set.
	      assert(new_index_ >= 0.0); //! ie, as we otehrwise have a bug.
	      const uint new_index = (uint)new_index_;
	      if(new_index < list_scores->current_pos) {
		if(map_inverted.list[new_index] == UINT_MAX) {
		  map_inverted.list[new_index] = col_id; //! ie, an inverted index, hence represtning a sorted map.
		} else { //! then the score has already been used. ... which impleis  the score is overalpping.
		  //! Note: below id sdiegned to handle above oveallpin-data-cases <-- how?
		  //! Note: the below-group may be rerived by suers by searching for all
		  // TODO: consider compressing the "new_index", ie, as this list may otehrwise beomc eall too large (ie, result in unfeible high memory-cosntumpion).
		  push__s_kt_list_2d_uint_t(&map_overlappingRanks_centroid_toOthers, /*global-reference=*/new_index, /*leaf-node=*/col_id);
		  // printf("current_pos:%u\n", map_overlappingRanks_centroid_toOthers.current_pos); // FIXME: remove.
		  // printf("\t(ignored)\t at sort:%u\t [%u] equals [%u] in its mapping, at %s:%d\n", new_index, col_id, map_inverted.list[new_index], __FILE__, __LINE__);
		  assert(/*new_index=*/new_index < map_overlappingRanks_centroid_toOthers.current_pos); //! ie, what we expect after above isnertion-step.
		}
	      }
	    }
	    assert(map_inverted.list_size <= self->arr_linearOrderOfCCMScores_result_id[type_id].list_size);
	    //!
	    //! Print the meta-data (assotied to this order):
	    //!
	    //! Step(1): open file:
	    allocOnStack__char__sprintf(2000, file_name_meta, "%s%s-%s-%s.tsv", dir_result, self->file_name_result, str_topoType_base, "hypothesis-CcmVShypoScores-sorted");
	    FILE *file_out_meta = fopen(file_name_meta, "wb");
	    if (file_out_meta == NULL) {
	      fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", file_name_meta, __FUNCTION__, __FILE__, __LINE__);
	    } else { //! then add conetn to it:
	      //!
	      //! Add header:
	      // TODO: based on the result-data ... how foten does overlas in ranks (ie, "Overlapping-IDs") occure?
	      fprintf(file_out_meta, "#! Row-name\t%s\t%s\t%s\t%s\n", "CCM-Score", "Data-ID", "Rank", "Overlapping-IDs");
	      if(file_out_meta_all_headerISAdded == false) {
		if(file_out_meta_all != NULL) {
		  fprintf(file_out_meta_all, "#! Row-name\t%s\t%s\t%s\t%s\t%s\n", "Data-type", "CCM-Score", "Data-ID", "Rank", "Overlapping-IDs");
		}
		file_out_meta_all_headerISAdded = true;
	      }
	      //!
	      //! Write out each attribute:
	      for(uint col_id = 0; col_id < map_inverted.list_size; col_id++) {
		//! Note: to print the sorted list w/meta-proerpteis, we need to invert the column-IDs to print (hence, the need for the "map_inverted" list):
		const uint row_id = map_inverted.list[col_id]; //! ie, 'col_id'; called to invert the index-proeprty.
		const uint new_index = row_id;
		if(row_id != UINT_MAX) { //! then it is NOT marked as overlapping:
		  assert(row_id < self->arr_linearOrderOfCCMScores_result_id[type_id].list_size);
		  // self->matrix[row_id][col_id] = new_index
		  const t_float score = list_scores->list[row_id]; //! ie, where "row_id" is sued as "list_scores" has the same insertion-order as "arr_linearOrderOfCCMScores_result_id"
		  const uint data_id = self->arr_linearOrderOfCCMScores_result_id[type_id].list[row_id].head;
		  const uint hypo_id = self->arr_linearOrderOfCCMScores_result_id[type_id].list[row_id].tail;
		  assert(hypo_id < self->mat_linearOrderOfCCMScores_hypo.nrows);
		  //! Get the string specifed for the hyptosis-in-question:
		  const char *str_hypo = getAndReturn_string__s_kt_matrix(&(self->mat_linearOrderOfCCMScores_hypo), /*index=*/hypo_id, /*addFor_column=*/false);
		  assert(str_hypo != NULL);
		  assert(strlen(str_hypo));
		  //!
		  //! Print the data:
		  //fprintf(file_out_meta, "%s\t%f\t%u\t%s\t", str_hypo, score, data_id, str_rank);
		  allocOnStack__char__sprintf(2000, str_rank, "%u/%u", col_id, map_inverted.list_size);
		  fprintf(file_out_meta, "%s\t%f\tid:%u\t%s\t", str_hypo, score, data_id, str_rank);
		  if(file_out_meta_all != NULL) {
		    fprintf(file_out_meta_all, "%s\t", str_topoType_base);
		    fprintf(file_out_meta_all, "%s\t%f\tid:%u\t%s\t", str_hypo, score, data_id, str_rank);
		    //fprintf(file_out_meta_all, "%s\t%f\t%u\t%s\t", str_hypo, score, data_id, str_rank);
		  }
		  //!
		  bool isAdded_overlappingID = false;
		  //printf("\touter-test\t(related)\t at sort:%u\t w/dataID:%u has |related|=%u, at %s:%d\n", new_index, data_id, map_overlappingRanks_centroid_toOthers.list[new_index].current_pos, __FILE__, __LINE__);
		  if(/*new_index=*/new_index < map_overlappingRanks_centroid_toOthers.current_pos) { //! then this index may have have inserted items
		    //printf("\ttest\t(related)\t at sort:%u\t w/dataID:%u has |related|=%u, at %s:%d\n", new_index, data_id, map_overlappingRanks_centroid_toOthers.list[new_index].current_pos, __FILE__, __LINE__);
		    if(map_overlappingRanks_centroid_toOthers.list[new_index].current_pos > 0) { //! then 'col_id' has assotied overlalping data-IDs:
		      //fprintf(file_out_meta, "%s\t%f\t%u\t%s\t", str_hypo, score, data_id, str_rank);
		      fprintf(file_out_meta, "related:");
		      if(file_out_meta_all != NULL) {    fprintf(file_out_meta_all, "related:"); }
		      //printf("\t(related)\t at sort:%u\t w/dataID:%u has |related|=%u, at %s:%d\n", new_index, data_id, map_overlappingRanks_centroid_toOthers.list[new_index].current_pos, __FILE__, __LINE__);
		      for(uint k = 0; k < map_overlappingRanks_centroid_toOthers.list[new_index].current_pos; k++) {
			//! Fetch the related data-id:
			const uint overlapping_id = map_overlappingRanks_centroid_toOthers.list[new_index].list[k];
			//! Valdiate docnsitency:
			assert(overlapping_id != UINT_MAX);
			assert(overlapping_id < list_scores->list_size); //! ie, as we expect cosnsitency.
			//! Construct the string
			allocOnStack__char__sprintf(2000, str, "%u", overlapping_id);
			//! Then write out the data:
			fputs(str, file_out_meta);
			if(file_out_meta_all != NULL) { fputs(str, file_out_meta_all); }
			if(k != (map_overlappingRanks_centroid_toOthers.list[new_index].current_pos - 1)) {
			  fputc(',', file_out_meta); //! ie, a number-list-seperator
			  if(file_out_meta_all != NULL) { fputc(',', file_out_meta_all); }
			}
			isAdded_overlappingID = true;
		      }
		    }
		  }
		  if(isAdded_overlappingID == false) {
		    fputc('-', file_out_meta); //! ie, add an empty-mark (as no related-ID-data were added/set)
		    if(file_out_meta_all != NULL) { fputc('-', file_out_meta_all); }
		  }
		} else { //! then row_id == UINT_MAX
		  //! TODO: should we add something? <-- howfully, the  "Overlapping-IDs" suffices.
		}
		//! Complete the writing of this:
		fputc('\n', file_out_meta);
		if(file_out_meta_all != NULL) { fputc('\n', file_out_meta_all); }
	      } //! and at this exec-poitns all columns are iterated.
	      //! Close file:
	      fclose(file_out_meta);
	    }

	    //! De-allcoat ethe locally reserved memory:
	    free__s_kt_list_1d_uint_t(&map_inverted);
	    free_1d_list_float(&list_rank); list_rank = NULL; //! ie, de-allcoate.
	  } //! else the 'rank' is empty, ie, no data.
	} //! and at this exec-point we asusemt aht tall the sroted lists are pritned out.
      } else { //! then an heads-up:
	printf("(info)\t No scores assoited to topo-type=\"%s\". OBservation at [%s]:%s:%d\n", str_topoType_base, __FUNCTION__, __FILE__, __LINE__);
      }
    }   
    if (file_out_meta_all == NULL) {
      fclose(file_out_meta_all);
      file_out_meta_all = NULL;      
    }
  }
  //!
  if(self->file_out != NULL) {
    fclose(self->file_out);
    self->file_out = NULL;
  }
  //! De-allocate:
  for(uint i = 0; i < e_eval_ccm_groupOf_artificialDataClass_undef; i++) {
    free__s_hp_clusterShapes_t(&(self->shape_ref[i]));
    free__s_kt_list_1d_pair_t(&(self->arr_linearOrderOfCCMScores_result_id[i]));
    free__s_kt_list_1d_float_t(&(self->arr_linearOrderOfCCMScores_result_score[i]));
  }
}

void clear_ArtifcicalData_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(s_eval_ccm_defineSyntMatrix_comparisonBasis_t *self) {  
  //! De-allocate, and then init:
  for(uint i = 0; i < e_eval_ccm_groupOf_artificialDataClass_undef; i++) {
    free__s_hp_clusterShapes_t(&(self->shape_ref[i]));
    self->shape_ref[i] = setToEmptyAndReturn__s_hp_clusterShapes_t();
  }
}

/**
   @brief compute CCM-similarty, and intersects this with completing hyptosis.
   @return the computed CCM-sim-score
 **/
static t_float apply_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(s_eval_ccm_defineSyntMatrix_comparisonBasis_t *self, s_hp_clusterShapes_t &obj_shape, const char *str_topoType, s_eval_ccm_defineSyntMatrix_t *prop, const e_eval_ccm_groupOf_artificialDataClass_t type_data)  {
  assert(type_data < e_eval_ccm_groupOf_artificialDataClass_undef);
  assert(self->eval_case != e_eval_ccm_booleanSensitivity_undef);
  //!
  if(isEmpty__s_hp_clusterShapes_t(&(self->shape_ref[type_data]))) {
    self->shape_ref[type_data] = obj_shape; //! ie, copy its content.
    assert(self->shape_ref[type_data].matrix.nrows > 0); //! ie, as we expect the matrix to be intiation
    return T_FLOAT_MAX;
  } else {
    assert(self->shape_ref[type_data].matrix.nrows > 0); //! ie, as we expect the matrix to be intiation
  }
  assert(self->shape_ref[type_data].matrix.nrows > 0); //! ie, as we expect the matrix to be intiation

  if(self->shape_ref[type_data].matrix.nrows != 0) {
    assert(self->shape_ref[type_data].matrix.nrows == prop->nrows); //! ie, what we expect for cosnistnecy.
    assert(self->shape_ref[type_data].matrix.ncols == prop->nrows); //! ie, what we expect for cosnistnecy.
  }
  t_float scalar_score = T_FLOAT_MAX;
  { //! Perform the comparison:			
    ccm__twoHhypoThesis__hp_ccm(self->ccm_enum,
				obj_shape.map_clusterMembers, self->shape_ref[type_data].map_clusterMembers,
				//!
				/*vertices=*/obj_shape.map_clusterMembers_size,
				//!
				obj_shape.matrix.matrix, self->shape_ref[type_data].matrix.matrix,
				&scalar_score);

    if(scalar_score != T_FLOAT_MAX) {
      //! Note: idea: idneitfyf different topolgoeis which rpcoeuses approxmaitely the same CCM-score.
      //! Note: Algortihm (applied): test/compae two competing hyptoesis (of 'Negative' versus 'Positive'), and then update for the hhyptoesis which fits:
      for(uint hypo_case = 0; hypo_case < e_eval_ccm_booleanSensitivity_undef; hypo_case++) {
	for(uint range_index = 0; range_index < self->list_range[hypo_case].list_size; range_index++) {	  
	  t_float range[2] = {
	    self->list_range[hypo_case].list[range_index].head,
	    self->list_range[hypo_case].list[range_index].tail, 	    
	  };
	  if( (scalar_score > range[0]) &&  (scalar_score < range[1]) ) {
	    if(true) { //! then 
	      allocOnStack__char__sprintf(2000, str, "%s-preStep-artificalData-dataID%lu-n%u-c%u-%s-f%ut%u", prop->filePrefix, self->global_data_id, prop->nrows, prop->cnt_clusters, str_topoType, (uint)prop->score_weak, (uint)prop->score_strong); 
	      s_kt_matrix_t *mat_export = &(obj_shape.matrix);			
	      assert(2 == e_eval_ccm_booleanSensitivity_undef); //! which is what we expect for "map_str".
	      const char *map_str[2] = {"True", "False"};
	      const bool debug = false;
	      // FIMXE: update our intit-routine with below:
	      const bool isTo_export = (self->list_range[hypo_case].list[range_index].score > 0);
	      // TODO: always correct to export below?
	      if(isTo_export) {
		__unsafeWithSideEffects_export_matrixToPPM(&(obj_shape.matrix), str);
	      }
	      //! -----------------------
	      //!
	      //! Case: update negative-and-positivve-counts
	      if(hypo_case == self->eval_case) { //! then the range matches the knowledge we ahve of the evaluated data:
		if(debug) {printf("%s--Positive\trange\t%s\t %s\t #! at %s:%d\n", map_str[hypo_case], str_topoType, str, __FILE__, __LINE__);}
		if(self->file_out != NULL) {
		  fprintf(self->file_out, "%s--Positive \trange\t%f--%f\tfile-prefix\t%s\n", map_str[hypo_case], range[0], range[1], str);
		}
		self->counter_result[self->eval_case].cnt_positive++; 
	      } else { //! then there is a mismatch in our expectaitions:
		if(debug) {printf("%s--Negative\trange\t%s\t  %s\t  #! at %s:%d\n", map_str[hypo_case], str_topoType, str, __FILE__, __LINE__);}
		if(self->file_out != NULL) {
		  fprintf(self->file_out, "%s--Negative \trange\t%f--%f\tfile-prefix\t%s\n", map_str[hypo_case], range[0], range[1], str);
		}
		self->counter_result[self->eval_case].cnt_negative++; 
	      }
	    }
	  }
	}
      }
      { //! Update: add, and make use, of coutners for TP,FP,... in combiantion with the range-prop (where the range-prop is sued as a config for detemrining this).
	//!
	//! Case: proerpties to e used for a next/future calculation-run:
	self->counter_result[self->eval_case].inputToSensitivty_observed_True_count++;
	//!
	if(self->counter_result[self->eval_case].inputToSensitivty_observed_True_min > scalar_score) {
	  self->counter_result[self->eval_case].inputToSensitivty_observed_True_min = scalar_score;
	}
	//!
	if(self->counter_result[self->eval_case].inputToSensitivty_observed_True_max < scalar_score) {
	  self->counter_result[self->eval_case].inputToSensitivty_observed_True_max = scalar_score;
	}
	//!	
      }
      //!
      //! Histogram-construciton: Udpate the score-distributions:
      push__s_kt_list_1d_float_t(&(self->list_histo[self->eval_case]), scalar_score);
    }    
  }


  
  //! De-allocate:
  free__s_hp_clusterShapes_t(&obj_shape);
  //!
  //!
  return scalar_score;
}


// *********************************************************' evalaute for a single gold-CCM
static void apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(s_eval_ccm_defineSyntMatrix_t *self,  const uint cnt_data_iterations,  const bool isTo_apply_postClustering, s_eval_ccm_defineSyntMatrix_comparisonBasis_t *obj_cmp, const bool isTo_exportDimMatrix) {
  //!
  const uli global_data_id_WhenThisFuncWasCalled = obj_cmp->global_data_id;
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  if(isTo_exportDimMatrix) {
    mat_result = initAndReturn__s_kt_matrix(e_eval_ccm_groupOf_artificialDataClass_undef, /*cols=*/cnt_data_iterations);
  }
  //! Note: idea is to dmeonstrate abmigyt of CCMs ... by prinitng PPM-images where CCM-score is inside a given frame  
  {      

    //!       
    //! Iterate through different data-caseS:
    for(uint type_id = 0; type_id < e_eval_ccm_groupOf_artificialDataClass_undef; type_id++) {
      //!
      //!
      //! Explore diffenret cluster-midpoints:
      //! Set the iteraiton-block-configuraiton:
      assert(cnt_data_iterations > 0);
      assert(cnt_data_iterations <= self->nrows);
      const uint block_increment_size = (uint)((t_float)self->nrows / (t_float)cnt_data_iterations); 
      assert(block_increment_size > 0);
      assert(block_increment_size < self->nrows);
      uint size_left = self->nrows - block_increment_size;
      uint size_right = self->nrows - size_left;
      //! Validate:
      assert(size_left > 0);
      assert(size_right > 0);
      assert(size_left <= self->nrows);
      assert(size_right  <= self->nrows);
      //!
      //! Init: the list to be used when comparing the results to altatnive/possible hyptoesis. 
      s_kt_list_1d_float_t list_hypoLocal = init__s_kt_list_1d_float_t(cnt_data_iterations);
      //!
      for(uint data_id = 0; data_id < cnt_data_iterations; data_id++) {
	const e_eval_ccm_groupOf_artificialDataClass_t type_data = (e_eval_ccm_groupOf_artificialDataClass_t)type_id; // eg,: e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit;
	//!
	const char *str_topoType_base = get_string_s_eval_ccm_defineSyntMatrix_t(type_data);
	allocOnStack__char__sprintf(2000, str_topoType, "%s-d%u", str_topoType_base, data_id);
	s_hp_clusterShapes_t obj_shape = generateData_s_eval_ccm_defineSyntMatrix_t(self, type_data, size_left, size_right);
	//! ----------------------------------------------------------------------------------------------
	//!
	//! Perform the comparison:
	const t_float ccm_score = apply_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(obj_cmp, obj_shape, str_topoType, self, type_data);
	if(obj_cmp->eval_case == e_eval_ccm_booleanSensitivity_Posititve) {
	  if( (ccm_score != T_FLOAT_MIN_ABS) && (ccm_score != T_FLOAT_MAX) && (ccm_score != T_FLOAT_MIN) && (ccm_score != 0) && (ccm_score != 1) ) {
	    //printf("\t\t sim_score=%f, at dataId:%lu, at %s:%d\n", ccm_score, obj_cmp->global_data_id, __FILE__, __LINE__);
	  }
	}
	if(isTo_exportDimMatrix) { //! then update the matrix:
	  //! Note: povide integated support for exporting feature-matrices seperately for each of these function-calls
	  //!
	  //! Set the name
	  if(data_id == 0) {
	    set_stringConst__s_kt_matrix(&mat_result, /*row_id=*/type_id, str_topoType_base, /*addFor_column=*/false);
	  }
	  if(type_id == 0) { //! then we set for all the columns;
	    //char str_local[3000]; sprintf(str, "block-%u--%u", size_left, size_right);
	    allocOnStack__char__sprintf(2000, str, "block-%u--%u", size_left, size_right);
	    //allocOnStack__char__sprintf(2000, str, "data-%u",data_id);	    
	    set_stringConst__s_kt_matrix(&mat_result, /*row_id=*/data_id, str, /*addFor_column=*/true);	      
	  }	  
	  //!
	  //! Set the CCM-score:
	  mat_result.matrix[type_id][data_id] = ccm_score;
	}
	list_hypoLocal.list[data_id] = ccm_score;
	// free__s_hp_clusterShapes_t(&obj_shape);
	//! ---------------------------------------------------------------
	//!
	//!
	//!
	//! Adjust the size of the data:
	size_left -= block_increment_size;
	if( (size_left <= 0) || (size_left >= self->nrows) ) {size_left = 1;} //! ie, boundary-condtion.
	size_right = self->nrows - size_left;
	//! Validate:
	assert(size_left > 0);
	assert(size_right > 0);
	assert(size_left <= self->nrows);
	assert(size_right  <= self->nrows);	
      }
      { //! then compute similarites to hypthesis: 
	//! Idea: Eval: compare the linear order of the scores to different outcomes.
	//! Note: motivation is to idneitfiy different categoeis fo Ariticla Shapes (eg, which affects the interpreability of CCM-scores). This is of improtance both in resul-evautlion \citep{}, use of multi-metric learning \citep{}, etc.
	if(list_hypoLocal.list_size > 0) {
	  assert(obj_cmp->mat_linearOrderOfCCMScores_hypo.nrows > 0);
	  t_float sim_score_min = T_FLOAT_MAX;
	  for(uint hypo_id = 0; hypo_id < obj_cmp->mat_linearOrderOfCCMScores_hypo.nrows; hypo_id++) {
	    assert(/*ncols=*/list_hypoLocal.list_size > 0);
	    //! Note: the default metric-chosei (for this comparison) is to use Kendall's Tau to compute distance of the features to different mid-points  (as seen in the cosntructor of this self-object)
	    t_float sim_score = T_FLOAT_MAX;
	    const bool is_ok = apply__rows_hp_distance(obj_cmp->mat_linearOrderOfCCMScores_evalMetric, list_hypoLocal.list, obj_cmp->mat_linearOrderOfCCMScores_hypo.matrix[hypo_id], list_hypoLocal.list_size, &sim_score);
	    //const bool is_ok = apply__rows_hp_distance(obj_cmp->mat_linearOrderOfCCMScores_evalMetric, list_hypoLocal.list, (t_float*)dimensions_curve, list_hypoLocal.list_size, &sim_score);
	    //const bool is_ok = apply__rows_hp_distance(curr_metric, row_1, row_2, ncols, &scalar_result);
	    assert(is_ok);
	    //! Update the result-strcuture:
	    if( (sim_score != T_FLOAT_MIN_ABS) && (sim_score != T_FLOAT_MAX) && (sim_score != T_FLOAT_MIN) ) {
	      //! Then add the data to the collection:
	      push__s_kt_list_1d_float_t(&obj_cmp->arr_linearOrderOfCCMScores_result_score[type_id], sim_score);
	      push__s_kt_list_1d_pair_t(&obj_cmp->arr_linearOrderOfCCMScores_result_id[type_id], /*data-id=*/obj_cmp->global_data_id, /*hypo=*/hypo_id);
	    }
	  }
	}
	//! De-allocate:
	free__s_kt_list_1d_float_t(&list_hypoLocal);
      }
      //!
      //! Increment the qunie-id-type:
      obj_cmp->global_data_id++; //! which is used to dineityf the quney idnityf of data.      
    }
  }
  
  //! Increment the qunie-id-type:
  // // obj_cmp->global_data_id++; //! which is used to dineityf the quney idnityf of data.

  if(isTo_exportDimMatrix) { //! then export the correlation-amtrix (wich we asusme exibhits a lienar proerpty):
    //!
    s_kt_matrix_t *matrix = &mat_result;
    /* char dir_result[2000];       memset(dir_result, '\0', 2000);  */
    /* if(obj_cmp->dir_result != NULL) { */
    /*   sprintf(dir_result, "%s", obj_cmp->dir_result); */
    /* }  */
    allocOnStack__char__sprintf(2000, file_prefix, "%s-preStep-artificalData-ccmDataId-dataInRange%lu-%lu-n%u-c%u-f%ut%u-%s", self->filePrefix, obj_cmp->global_data_id, global_data_id_WhenThisFuncWasCalled,
				self->nrows, self->cnt_clusters, (uint)self->score_weak, (uint)self->score_strong, "detailsOfScores"); 
    { //! Write out the result-matrices, both 'as-is' and 'transposed':
      {
	allocOnStack__char__sprintf(2000, str_prefix, "%s-raw.tsv", file_prefix);
	bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_prefix, NULL); assert(is_ok); 
      }
      {  /*! Then export a tranpsoed 'verison': */			
	allocOnStack__char__sprintf(2000, str_prefix, "%s-transposed.tsv", file_prefix);
	//allocOnStack__char__sprintf(2000, file_name, "%s%s-%s", "histogram-transposed-", dir_result, self->file_name_result);
	bool is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_prefix, NULL); assert(is_ok); 
      }
    }
    { //! Investigate the impact ... adjsuted to the averaged score ... hencce, to see a scale-invariant change in prediciton.scores
      s_kt_matrix_t mat_cpy = initAndReturn__copy__normalized__s_kt_matrix_t(matrix);
      s_kt_matrix_t *matrix = &mat_cpy;
      {
	allocOnStack__char__sprintf(2000, str_prefix, "%s-normalizedAvg-transposed.tsv", file_prefix);
	  bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_prefix, NULL); assert(is_ok); 
      }
      {  /*! Then export a tranpsoed 'verison': */			
	allocOnStack__char__sprintf(2000, str_prefix, "%s-normalizedAvg-transposed.tsv", file_prefix);
	bool is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_prefix, NULL); assert(is_ok); 
      }
      //!
      free__s_kt_matrix(&mat_cpy);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////7
/////////////////////////////////////////////////////////////////////////////////////////7
/////////////////////////////////////////////////////////////////////////////////////////7
/////////////////////////////////////////////////////////////////////////////////////////7
/////////////////////////////////////////////////////////////////////////////////////////7

/**
   @brief an Hellow World example, which exemplfies basic usage of this framework   
   @remarks exemplifes a 'main function', ie, a tempatle for constructing different tutorials (tut's).
 **/
void static_tut_helloWorld() {
  // FIXME: make use fo thsi funciton.
  /////////////////////////////////////////////////////////////////////////////////////////7

  assert(false); // FIXME: construct new ARitcila-data-set --- overallping clusters .... then evvlauate hos this affects t he degree of overlaps in predicont-accurayc ... <-- possible to use to hsi to elvuate differenyt/muliple voerlappiong hyptossi? <-- for this task, could we merge two different distrubionts .... eg, ncluster=2 with ncluster=200 <-- what should the coccret/acucrate outcome be? ... waht ould be validat data-epr,mtautiosn (where valid is udnerstood as \textit{data-per,mtuatiosn which does not alter/affect accuracy of the correct/expected data-seperation) .... ??

  // FIXME[user-api::config-file]: specify the data-dsitruion to evalute ... ie, combination of different aprramets ... <-- pre: cosntruct an optioanl data-strucutre to hold the 'currently' ahrd-odeded data-strucutres-cluster-hp_shapes-spec ...
  // FIXME[user-api::config-file]:
  // FIXME[user-api::config-file]:
  // FIXME[user-api::config-file]:
  // FIXME[user-api::config-file]:

  // ----------------------------- motiaiotn: identify cases where $(score(CCM_i) \Big| data_x) ==(score(CCM_i) \Big| data_y)$  ... andd the use thsi as a proof of coenpt example ... that two equal CCM-scores does NOT imply that the data-sets are dintifical; they may be dinticial, though dhis does NOT need to be the case; for detials, see the discussions oif \textit{isomporph topologies} in \cite{}.
  //
  // FIXME: add supprot for CCM-matrix 
  // # FIXME: "hp_frameWork_CCM_boundaries.c": apply simalrity-emtric as a rep-step before comptuging the CCM-matrix-scores ... the
  // FIXME: apply a2d-iterriaotn acorss diffenret synteitc clstuer-shapes ... 
  // FIXME: load sytentic data-sets, and then apply above/this proceudre ... do we dientify datasets with the same $score(CCM_i)$? <--  use both real-life, and sytentic, dataests ... for thsytnetic dqataests, use datasets in folder="??" ... which are simalri to those rpesetntyed in https://www.mdpi.com/2078-2489/11/1/16/htm
  // -----------------------------
  // FIXME: data-permtaution: a new cofnig
  // idea: gnerate the random point-clouds, as illustrated in Fig.7 in https://www.researchgate.net/profile/Jose_Maria_Luna-Romera/publication/320236075_An_approach_to_validity_indices_for_clustering_techniques_in_Big_Data/links/59e45eb2aca2724cbfe8fb89/An-approach-to-validity-indices-for-clustering-techniques-in-Big-Data.pdf % compare our updated data-generaiotn-rpoceudre to the latter research 
  // algorithm(suggestion): randly remove 'k < n' points insidde each cluster ... evlauation: is there a lienar realtionshiåp between 'removePoiints=k <n' versus the CCM-score (ie, as we would asserrt/asusme that the acurayc of each clsuter-repion is reduced)? <-- to handle issues in randomeness .... seperately select min--max CCM-scores ... use these results to idnicate the PAreto Boudnar (or: radius) for ... 
  //! evlauation: ... use these results to ...??...
    // -----------------------------
  // FIXME: explroe the \textit{monotonically proerpty} discussed in \cite{liu2010understanding} ... for this task, change the number of clusters ... both CCMS=(gold, matrix) ... then plot the CCM-scores for the different data-topologies ... 
  // FIXME: extension: add support ... evlauate for different math-functions
  // FIXME: extension: add support ... different data-distrubiotns ... for thwih our "" API is used ... 
  // FIXME: extension: add support ... configure the deifnion of "Postive" and "Negative" thresholds ... eg, to assert that "ncluster +- 10% of correct nclstuer" are "Postiv" ... which for clsuters(correct)=20 woudl imply that "nclsuters=[18, 19, 20, 21, 22] should we viwed as an accurate classifciaotn ... then comptue the TP-FP-... matrix ... generatig a table with columns=[CCM, data-id, sensitivty, acucrayc, ..., ...].
  // FIXME: extension: add support ...
  // FIXME: extension: add support ...   
  // FIXME: extension:
  // -----------------------------
  // FIXME: explroe the \textit{monotonically proerpty} discussed in \cite{liu2010understanding} ... for this task, "to evaluate the influence of noise on internalvalidation indice" \citep{liu2010understanding} ... both CCMS=(gold, matrix) ... then plot the CCM-scores for the different data-topologies ...  ... 
  // ----------------------------- FIXME: permtaution: explroe the effects differneces in densities .... an extension of \cite{liu2010understanding} ... (for which we ...??..) % FIXME: how may we use the distrubion-API to adjust the scores ot the clusters? .... by traversing through each data-point ... and then ...??....
  // FIXME: extension: add support ...
  // FIXME: extension: add support ...   
  // FIXME: extension:
  // -----------------------------
  
  //if(false)
  { //! Case-1: ....
    //! Note: this use-case is expected to be clalccuated (relviatyly speaking) fast. For this reason, both the CCM, and the data-ensabmel, stays fixed.
    // ---
    //! Validate input: the comparison-object which is used. 
    //const uint featureDims_stepSize = 100; //! ie, which is used to consturc the number of piitns in question
    //const uint featureDims_iterations = 10;
    //! ----
    //for(uint nrows_index = 0; nrows_index < featureDims_iterations; nrows_index++) {
    //! Method: Consturct feature-matrix of dimeions, and then analyse their itnernal simliarites.
    // ---
    const char *filePrefix_base = "r-hp_frameWork_Case1-CCM_boundaries_helloWorldCaseTest-";  //! which is sued if tempraoryb data are exproeted, eg, PPM-images.
    //const char *filePrefix = "r-hp_frameWork_CCM_boundaries_helloWorldCaseTest-images-";  //! which is sued if tempraoryb data are exproeted, eg, PPM-images.
    const char *file_name_result = "r-hp_frameWork_Case1-CCM_boundaries_helloWorldCaseTest.tsv";
    s_eval_ccm_defineSyntMatrix_comparisonBasis_t self_result = init_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(file_name_result);
    self_result.ccm_enum = e_kt_matrix_cmpCluster_metric_randsIndex; //! ie, the default choise.
    //
    const uint cnt_data_iterations = 20; //! where this attribute needs to be fixed when analsysing hytpsis, ie, to simplify the engerration of tailore-made hytpsos to compare with.
    /**
       // FIXME: infoVis: ... visually idnetify dim-curves which are extremely different ... then plto these dim-curves ... 
       // FIXME: infVis ... auaotmed ... based on "list_hypoLocal.list" ... select an ensamble-id of dim-scores ... then apply a new iteraiton: print out the curves only for these
       --- 
       // FIXME: sumamrize abbove applciations into a use-ccase-scenatro-list 
     **/
    const bool isTo_exportDimMatrix = true;  
    //! -----------------------------------------------------
    //! 
    { //! Define Hyptoisis-conditions:
      //! Note: the below boudnaries are the result of ane arlier copmtuation; to maise the sepration between the cases, we (in below/this configuraiton) makes sue of the three wrost scores, and trhee best scores, observed/gaind forr the emtric qin question:
      //! Note: reprsentivness of the results ... 
      const uint arr_sound_size = 2;
      t_float arr_sound[arr_sound_size][2] = {{-1.020300     ,  -0.906805},  {-0.906805       -0.793303}};
      //!
      //! Set the scores:
      for(uint k = 0; k < arr_sound_size; k++) {
	set_scalar__s_kt_list_1d_tripleT_Float_t(&(self_result.list_range[e_eval_ccm_booleanSensitivity_Posititve]), /*index=*/k, MF__initVal__s_ktType_tripleT_Float(arr_sound[k][0], arr_sound[k][1], /*isTo_print=*/0) );
      }
      const uint arr_poor_size = 2;
      t_float arr_poor[arr_poor_size][2] = {{0.795700      ,  0.909196}, {0.909196,        1.000000}}; 
      //!
      //! Set the scores:
      for(uint k = 0; k < arr_poor_size; k++) {
	set_scalar__s_kt_list_1d_tripleT_Float_t(&(self_result.list_range[e_eval_ccm_booleanSensitivity_Negative]), /*index=*/k, MF__initVal__s_ktType_tripleT_Float(arr_poor[k][0], arr_poor[k][1], /*isTo_print=*/0) );
      }      
    }
    //! -----------------------------------------------------
    //! 
    { //! Define different cruve-shaped hyptosis:
      //! Note: idea (of this evaluation-algorithm-pipeline): idneitfy cases where the CCM-growth-factor changes substaintlly (which if true would stronly inlfuence clsuter-repioncts (Eq. \ref{}))  .... code-permtautiopn. compute for a single CCM (eg, DBI)  <-- udpate framwork by ...??...  ... use Kendall's Tau to idneitfy the most dissibimilar metrics  ... plto the dissimilarity using a rank-base searhd (both for the  scores, and the invrerted scores) ... plot the different results  using Krusla'ls MST <-- % Note: insted we select the 't' best scores ... ie, to reduce the memory-footprint of this appraoch ... and to make the aprpaoch mroe dynamic ... eg, to allow/enalbe userrs to specy abirayt number of hytpeosis to explore ... hower, groups bettween the extrem edges might be as interesting (as they seperate from the others) ... this argues for includign all the ... and then onstruct/depcit/nalayse the clsters (of scores) which form ... 
      // FIXME: permtaution: "eval_ccm_data(..)": ... change data to iterate across .... use enum=e_eval_ccm_groupOf_artificialDataClass_t ... % FIXME: does this itnroduce enough/suffient viarnacce in the unldyering data? <-- try combine this with the extensive alg-oermtuatiosn we have used ...
// FIXME: code: update apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t with a new matrix mat_featrues(/*nrows=*/e_eval_ccm_groupOf_artificialDataClass_t][e_hypo_shape_size, /*ncols=*/dims) .... calcuate corr-scores after each 'run' ... 
      //! ------------------------------------
      // FIXME: futue: add supprot fors epitying different fucnctiosn, and data-sets to comapre with ... <-- should we support specying a hytpsis-matrix as an arguemtn?
      //! ------------------------------------
      if(true) { //! then we define a chunk of different hytpeossi
	assert(self_result.mat_linearOrderOfCCMScores_hypo.nrows == 0); //! as we expect this to be empty.
	//!
	//! Set the hypteosis:
	const uint hypo_cnt = 3;
	const char *str_hypo[hypo_cnt] = {"linear", "reverse", "bell-shaped"};
	self_result.mat_linearOrderOfCCMScores_hypo = initAndReturn__s_kt_matrix(/*nrows=*/hypo_cnt, /*ncols=*/cnt_data_iterations);
	//!
	//! Set the data:
	for(uint row_id = 0; row_id < self_result.mat_linearOrderOfCCMScores_hypo.nrows; row_id++) {
	  //! Set: row-name
	  set_stringConst__s_kt_matrix(&(self_result.mat_linearOrderOfCCMScores_hypo), /*row_id=*/row_id, str_hypo[row_id], /*addFor_column=*/false);
	  t_float score_inc = 0.0;
	  for(uint col_id = 0; col_id < self_result.mat_linearOrderOfCCMScores_hypo.ncols; col_id++) {
	    //! Set: column-name
	    if(row_id == 0) {
	      allocOnStack__char__sprintf(2000, str_local, "%s-%u", "index", col_id);
	      set_stringConst__s_kt_matrix(&(self_result.mat_linearOrderOfCCMScores_hypo), /*row_id=*/col_id, str_local, /*addFor_column=*/true);
	    }
	    //! Set: score:
	    if(row_id == 0) { self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = (t_float)col_id; }
	    else if(row_id == 1) { self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = (t_float)(cnt_data_iterations - col_id); }
	    else if(row_id == 2) { //! then a bell-shaped-curve is cosntruted
	      if(col_id < (uint)( (t_float) self_result.mat_linearOrderOfCCMScores_hypo.ncols / 2) ) {
		score_inc -= 1.0;
	      } else {
		score_inc += 1.0;
	      }
	      self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = score_inc;
	    } else { assert(false); } //! ie, then add supprot for this
	  }
	}
      }
    }
    //! -----------------------------------------------------
    //!     
    {
      //!      
      //! Note: based on our other/related meusrements, we have that below cofnigrusions should results in a high seperation between CCMs (hence, the repsrestniveness of belwo cofniuraiton):
      uint cnt_clusters = 2;
      const uint nrows = 100;
      //const t_float score_strong = 100;      const t_float score_weak = 1;
      const t_float score_strong = 1;      const t_float score_weak = 100;
      //! Apply logcs:
      const bool isTo_exportArtificalData = false;
      const bool apply_postClustering = true;
      //! ------------------------------
      //!
      //! Apply:
      { //! Compute for: 'positive-cases':
	//! 
	//! Configure:
	self_result.eval_case = e_eval_ccm_booleanSensitivity_Posititve;
	allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix_base, "case-Positive", "linearTestCase", nrows, cnt_clusters, (uint)score_weak, (uint)score_strong, cnt_data_iterations);
	s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters);
	//! ------------------------------
	clear_ArtifcicalData_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result); //! which is used as a safe-guard (both wrt. future code-changes, and if/when this code-chunk is copy-pasted).
	//! 
	//! Call:
	apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_artiFicialData, cnt_data_iterations, /*isTo_apply_postClustering=*/false, &self_result, isTo_exportDimMatrix);
      }
      // ---------
      { //! Compute for: 'negative-cases': doubles the number of clusters; motivaiton of this is to 
	// FIXME ... (conceptual) ... what is the vality of asserting that the hyyssis should be distnvtly different from case where $result_{true}(|clusters|) <<< result_{false}(|clusters|*2)$  ??
	// FIXME ... (conceptual) ... may this ambity be used to exemplify the ambity in result-evlauation ... and the ptoential + applcialibty of Approximatie Computing ... ie, given the Interpretation Boundary (and assitate \textit{result/prediciton epsilon})?
	// FIXME ... (conceptual) ...
	// FIXME ... (conceptual) ... 		
	//! 
	//! Configure:
	cnt_clusters *= 2;
	if(cnt_clusters > nrows) {cnt_clusters = nrows;}
	//! 
	self_result.eval_case = e_eval_ccm_booleanSensitivity_Negative;
	allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix_base, "case-Negative", "linearTestCase", nrows, cnt_clusters, (uint)score_weak, (uint)score_strong, cnt_data_iterations);
	s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters);
	//! ------------------------------
	// FIXME: ...
	// FIXME: ... what are the cmp-data to be used?
	// FIXME: ... change the default reference-data to be used ... 
	// FIXME: ...
	// FIXME: ... 
	// FIXME: udpate init-main-funciton with: ... default shape
	// FIXME: call: clear_ArtifcicalData_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result);
	apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_artiFicialData, cnt_data_iterations, /*isTo_apply_postClustering=*/false, &self_result, isTo_exportDimMatrix);
      }
    }
    //!
    //! Export result-set, and then de-allocte:
    free_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result);
  }
  /////////////////////////////////////////////////////////////////////////////////////////7
  /////////////////////////////////////////////////////////////////////////////////////////7
  /////////////////////////////////////////////////////////////////////////////////////////7
  const uint map_cluster_size = 15;
  const uint map_cluster[map_cluster_size] = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30};
  //if(false)  { const uint cluster_id = 0; // FIXME: remove, and inlcude bleow
  for(uint cluster_id = 0; cluster_id < map_cluster_size; cluster_id++) {
    //! Case-2: .... 
    // FIXME: are there any cases where "Rand's Index" idnietfies a CCM-score (for the tested topologies)?
    // FIXME: pre: code: new loop ... compare the different data-stributiosn to each other ... permtuation of "Case-2" ... which of the combinations results in different CCM-score (for "Rand's Index")?
    // ---
    // FIXME[conceptual]: ... when does "Rand's Index" idnetify different clusters? <-- its definiont (Eq. \ref{}) states that ...??...
    // FIXME[conceptual]: ... real-life examples of when different clsuters should be seen 'as the same'?
    // FIXME[conceptual]: ...     
    // FIXME[conceptual]: ...     
    // ---        
    const uint cnt_clusters = map_cluster[cluster_id];
    // ---
    const char *filePrefix_base = "r-hp_frameWork_Case2-CCM_boundaries_helloWorldCaseTest-";  //! which is sued if tempraoryb data are exproeted, eg, PPM-images.
    allocOnStack__char__sprintf(2000, filePrefix, "%s-c%u-", filePrefix_base, cnt_clusters);
    //yconst char *file_name_result = "r-hp_frameWork_CCM_boundaries_helloWorldCaseTest.tsv";
    allocOnStack__char__sprintf(2000, file_name_result, "%ssummary.tsv", filePrefix);
    s_eval_ccm_defineSyntMatrix_comparisonBasis_t self_result = init_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(file_name_result);
    self_result.ccm_enum = e_kt_matrix_cmpCluster_metric_randsIndex; //! ie, the default choise.
    //
    const uint cnt_data_iterations = 20; //! where this attribute needs to be fixed when analsysing hytpsis, ie, to simplify the engerration of tailore-made hytpsos to compare with.
    /**
       // FIXME: infoVis: ... visually idnetify dim-curves which are extremely different ... then plto these dim-curves ... 
       // FIXME: infVis ... auaotmed ... based on "list_hypoLocal.list" ... select an ensamble-id of dim-scores ... then apply a new iteraiton: print out the curves only for these
       --- 
       // FIXME: sumamrize abbove applciations into a use-ccase-scenatro-list 
     **/
    const bool isTo_exportDimMatrix = true;  
    //! -----------------------------------------------------
    //! 
    { //! Define Hyptoisis-conditions:
      //! Note: the below boudnaries are the result of ane arlier copmtuation; to maise the sepration between the cases, we (in below/this configuraiton) makes sue of the three wrost scores, and trhee best scores, observed/gaind forr the emtric qin question:
      //! Note: reprsentivness of the results ... 
      const uint arr_sound_size = 2;
      t_float arr_sound[arr_sound_size][2] = {{-1.020300     ,  -0.906805},  {-0.906805       -0.793303}};
      //!
      //! Set the scores:
      for(uint k = 0; k < arr_sound_size; k++) {
	set_scalar__s_kt_list_1d_tripleT_Float_t(&(self_result.list_range[e_eval_ccm_booleanSensitivity_Posititve]), /*index=*/k, MF__initVal__s_ktType_tripleT_Float(arr_sound[k][0], arr_sound[k][1], /*isTo_print=*/0) );
      }
      const uint arr_poor_size = 2;
      t_float arr_poor[arr_poor_size][2] = {{0.795700      ,  0.909196}, {0.909196,        1.000000}}; 
      //!
      //! Set the scores:
      for(uint k = 0; k < arr_poor_size; k++) {
	set_scalar__s_kt_list_1d_tripleT_Float_t(&(self_result.list_range[e_eval_ccm_booleanSensitivity_Negative]), /*index=*/k, MF__initVal__s_ktType_tripleT_Float(arr_poor[k][0], arr_poor[k][1], /*isTo_print=*/0) );
      }      
    }
    //! -----------------------------------------------------
    //! 
    { //! Define different cruve-shaped hyptosis:
      //! Note: idea (of this evaluation-algorithm-pipeline): idneitfy cases where the CCM-growth-factor changes substaintlly (which if true would stronly inlfuence clsuter-repioncts (Eq. \ref{}))  .... code-permtautiopn. compute for a single CCM (eg, DBI)  <-- udpate framwork by ...??...  ... use Kendall's Tau to idneitfy the most dissibimilar metrics  ... plto the dissimilarity using a rank-base searhd (both for the  scores, and the invrerted scores) ... plot the different results  using Krusla'ls MST <-- % Note: insted we select the 't' best scores ... ie, to reduce the memory-footprint of this appraoch ... and to make the aprpaoch mroe dynamic ... eg, to allow/enalbe userrs to specy abirayt number of hytpeosis to explore ... hower, groups bettween the extrem edges might be as interesting (as they seperate from the others) ... this argues for includign all the ... and then onstruct/depcit/nalayse the clsters (of scores) which form ... 
      // FIXME: permtaution: "eval_ccm_data(..)": ... change data to iterate across .... use enum=e_eval_ccm_groupOf_artificialDataClass_t ... % FIXME: does this itnroduce enough/suffient viarnacce in the unldyering data? <-- try combine this with the extensive alg-oermtuatiosn we have used ...
// FIXME: code: update apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t with a new matrix mat_featrues(/*nrows=*/e_eval_ccm_groupOf_artificialDataClass_t][e_hypo_shape_size, /*ncols=*/dims) .... calcuate corr-scores after each 'run' ... 
      //! ------------------------------------
      // FIXME: futue: add supprot fors epitying different fucnctiosn, and data-sets to comapre with ... <-- should we support specying a hytpsis-matrix as an arguemtn?
      //! ------------------------------------
      if(true) { //! then we define a chunk of different hytpeossi
	assert(self_result.mat_linearOrderOfCCMScores_hypo.nrows == 0); //! as we expect this to be empty.
	//!
	//! Set the hypteosis:
	const uint hypo_cnt = 3;
	const char *str_hypo[hypo_cnt] = {"linear", "reverse", "bell-shaped"};
	self_result.mat_linearOrderOfCCMScores_hypo = initAndReturn__s_kt_matrix(/*nrows=*/hypo_cnt, /*ncols=*/cnt_data_iterations);
	//!
	//! Set the data:
	for(uint row_id = 0; row_id < self_result.mat_linearOrderOfCCMScores_hypo.nrows; row_id++) {
	  //! Set: row-name
	  set_stringConst__s_kt_matrix(&(self_result.mat_linearOrderOfCCMScores_hypo), /*row_id=*/row_id, str_hypo[row_id], /*addFor_column=*/false);
	  t_float score_inc = 0.0;
	  for(uint col_id = 0; col_id < self_result.mat_linearOrderOfCCMScores_hypo.ncols; col_id++) {
	    //! Set: column-name
	    if(row_id == 0) {
	      allocOnStack__char__sprintf(2000, str_local, "%s-%u", "index", col_id);
	      set_stringConst__s_kt_matrix(&(self_result.mat_linearOrderOfCCMScores_hypo), /*row_id=*/col_id, str_local, /*addFor_column=*/true);
	    }
	    //! Set: score:
	    if(row_id == 0) { self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = (t_float)col_id; }
	    else if(row_id == 1) { self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = (t_float)(cnt_data_iterations - col_id); }
	    else if(row_id == 2) { //! then a bell-shaped-curve is cosntruted
	      if(col_id < (uint)( (t_float) self_result.mat_linearOrderOfCCMScores_hypo.ncols / 2) ) {
		score_inc -= 1.0;
	      } else {
		score_inc += 1.0;
	      }
	      self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = score_inc;
	    } else { assert(false); } //! ie, then add supprot for this
	  }
	}
      }
    }
    //! -----------------------------------------------------
    //!     
    { //! Explore data-shapes:
      //!      
      //! Note: based on our other/related meusrements, we have that below cofnigrusions should results in a high seperation between CCMs (hence, the repsrestniveness of belwo cofniuraiton):
      //uint cnt_clusters = 2;
      const uint nrows = 100;
      //const t_float score_strong = 100;      const t_float score_weak = 1;
      // const t_float score_strong = 1;      const t_float score_weak = 100;
      //! Apply logcs:
      const bool isTo_exportArtificalData = false;
      const bool apply_postClustering = false;
      //const uint weights_map_size = 1;
      const uint weights_map_size = 5;
      // FIXME: paper: is it true that 'strong scores' should be higher? ... how does this relationship/understanding relate to simialrty-meitrccs? ... 
      // FIXME: correlate below into a seperate matrix.
      // FIXME: upddate paper with below results.
      const uint weights_map[weights_map_size][2] = {
	{100,1},{100, 25},{100, 50}, 
	{100, 99}
	, {1000, 99}
      //{99, 100}
      //{95, 100}
      }; //! where the ",{100, 95},{95, 100}" is used to explreo the effects oc ases where there is hanrdly any sepration, as seen for ases where Eculdidena simlairty faind it challengign to handle otuliers (as exemplfied in resutls generate from our "tut-2" results).
    //!
      //! ------------------------------
      //!
      //! Apply:
      { //! Compute for: 'positive-cases':
	//for(uint cluster_id = 0; cluster_id < map_cluster_size; cluster_id++) {
	//const uint cnt_clusters = map_cluster[cluster_id];
	//! 
	for(uint weight_index = 0; weight_index < weights_map_size; weight_index++) {
	  const t_float score_weak   = weights_map[weight_index][0];
	  const t_float score_strong = weights_map[weight_index][1];
	  //!
	  assert(cnt_clusters < nrows);
	  //! 	  
	  //! Configure:
	  self_result.eval_case = e_eval_ccm_booleanSensitivity_Posititve;
	  allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix, "case-Positive", "linearTestCase", nrows, cnt_clusters, (uint)score_weak, (uint)score_strong, cnt_data_iterations);
	  s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters);
	  //! ------------------------------
	  if(false) {
	    clear_ArtifcicalData_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result);} //! which is used as a safe-guard (both wrt. future code-changes, and if/when this code-chunk is copy-pasted).
	  //! 
	  //! Call:
	  apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_artiFicialData, cnt_data_iterations, /*isTo_apply_postClustering=*/false, &self_result, isTo_exportDimMatrix);

	  // FIXME: remvoe below ... when we ahve figured out why the aobve cCMs prdocue idneitcial reslts ... 
	  // cnt_clusters += 2;   if(cnt_clusters*4 >= nrows) {cnt_clusters = (uint)((t_float)nrows * 0.25);} 
	  //cnt_clusters *= 2;   if(cnt_clusters*4 >= nrows) {cnt_clusters = (uint)((t_float)nrows * 0.25); }
	}
      }
      // ---------
      { //! Compute for: 'negative-cases': doubles the number of clusters; motivaiton of this is to 
	// FIXME ... (conceptual) ... we assuem that the weight-difference shoudl NOT inlfuence the overrall predicciton-acuracy ... ie, as the 'weights' ollowins the same distrubions. Hence, ... 
	// FIXME ... (conceptual) ... 		
	uint cnt_clusters_local = cnt_clusters * 2;
	if(cnt_clusters_local > nrows) {cnt_clusters_local = cnt_clusters;}
	//! 
	//! Configure:
	for(uint weight_index = 0; weight_index < weights_map_size; weight_index++) {
	  const t_float score_weak   = weights_map[weight_index][0];
	  const t_float score_strong = weights_map[weight_index][1];
	  //! 
	  self_result.eval_case = e_eval_ccm_booleanSensitivity_Negative;
	  allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix, "case-Negative", "linearTestCase", nrows, cnt_clusters_local, (uint)score_weak, (uint)score_strong, cnt_data_iterations);
	  s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters_local);
	  //! 
	  apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_artiFicialData, cnt_data_iterations, /*isTo_apply_postClustering=*/false, &self_result, isTo_exportDimMatrix);
	}
      }
    }
    //!
    //! Export result-set, and then de-allocte:
    free_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result);
  }
  // --------------------------------------------------------------------
  // FIXME: re-comptue the tut-16 articial data-set (Alrerayd included into the report) ... givne the bug of setitng the score_strong attribute ... then udpate the paper ... 
  fprintf(stderr, "(info) \tcompleted analsysis, at %s:%d\n", __FILE__, __LINE__); assert(false); // FIXME: remove.  
  /////////////////////////////////////////////////////////////////////////////////////////7
  /////////////////////////////////////////////////////////////////////////////////////////7
  /////////////////////////////////////////////////////////////////////////////////////////7
  {  //! Case-3: .... 
    // FIXME: ... keep the shape_ref object unchanged when weight-porps changes ... while update/reset/clear it when props=[clusters, nrows,...] changes.
    // FIXME: explroation-arpemter-sapce: grually increas the space to explroe ... where limtiaton is the exec-time ...
    // FIXME: explreo for all gold-ccms ... update ...       self_result.ccm_enum = e_kt_matrix_cmpCluster_metric_randsIndex; //! ie, the default choise.
    // FIXME: ... keep the shape_ref object unchanged when weight-porps changes ... while update/reset/clear it when props=[clusters, nrows,...] changes.
    const uint map_cluster_size = 5;
    const uint map_cluster[map_cluster_size] = {2, 4, 8, 16, 32};
    for(uint cluster_id = 0; cluster_id < map_cluster_size; cluster_id++) {
      // FIXME: compelte this.
    }
  }

  // FIXME: explroe the effets of different clustering-apretmers ... such as \textit{ncluster} in k-means

  // FIXME: integrate dteaisl/logics from: tut-1,tut-3
  // FIXME: integrate dteaisl/logics from: tut-2
  // FIXME: integrate dteaisl/logics from:
  // FIXME: integrate dteaisl/logics from:
  // FIXME: integrate dteaisl/logics from:
}


// *********************************************************************************************
// *********************************************************************************************
// *********************************************************************************************
// *********************************************************************************************

//! ---------------------------------------------
#endif //! EOF
