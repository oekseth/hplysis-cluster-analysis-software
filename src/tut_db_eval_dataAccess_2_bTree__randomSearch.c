#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "db_ds_directMapping.h"
#include "db_ds_bTree_keyValue.h"
#include "db_ds_bTree_rel.h"
#include "kt_matrix.h"
#include "measure_base.h"
#include "aux_sysCalls.h"
/**
   @brief demonstrates how a bo build and access a b-tree w/elements = ['key-->value', s_db_rel_t(...)].
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- api(files): for details see our "db_ds_bTree_keyValue.h", "db_ds_bTree_rel.h", our "db_rel.h", and our "db_ds_directMapping.h"
   -- setup(access): in our time-meausrements we use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
   -- setup(timing): 
   -- [mapping]: in order to 'map directly' our '1d-key-search' to our "s_db_rel_t" we 'consutrct' a '1d-key' based on the 'complete set of keys' used in our "s_db_rel_t". A cocnrete sue-case wr.t the latter is to use an 'index-noramlizioant-atrategy' (eg, a bitmap) to access/idnietyf the 'keys' in a 1d-be-tree.
   @remarks Search-scope: to evalaute the 'effect' of increased data-size for a fixed number of data-searches:
    -- [expectaiton]: 'smaller data-sets' are expected to 'give' a higher/improved performance, which is sue to: (a) reduced search-space, and (b) icnreased temproal and spaital emmory-lcoality. This search-differences is expcted to be 'of lesser sigicnace' for list-based accessed (eg, through our "db_ds_directMapping.h").
    -- [expectaiton::"b-tree"]: we expect an increased 'node-chunk-size' to result in increase performance, ie, givent eh 'reduced number or artibttryar searches' in the data-set.    
    @remarks 
    -- 
result: the reuslts may be used to describe/elvuate:
    -- the exeuciton-tiem-difference wrt. (a) 'list-access', (b) B-tree w/key-size=4 Bytes, (c) B-tree w/key-size=16 Bytes, (d) 2d-list-traversla ... 
    -- time-overhead of simplicitc queryies using a neaive implementaiton: investigate the optimzaiton-sigicvnace of inlcuding a 'complete' relationship into the "B-tree" search-field. In this evlauation-appraoch we compare/evaluate the difference between a 'multi-level' B-tree (where we access the "predicate-index" after a search for a given "head-vertex") and an 'epxlict relation-tree' (where a relationship is stored as  a 'single node'). What we obseve, is that a 'direct inlsuion' of the relation (as a key) in the B-treee sigicantly boosts the performance
    -- time(list-based VS B-tree): comapre the exueciotn-time between differetn B-tree-imlmenetiaonts and a list-based implmeentaiton. In this evlauatin ewe evlauate: (a) 'direct pointer-look-up', (b) accessing a b-tree to 'find' the index-value (usinig/through our "s_db_ds_bTree_keyInt_t") (and then 'fetch' the relation using our "db_ds_directMapping.h") VS (c) 'normalization and direct list-access' (in our "db_ds_directMapping.h"). The latter clearly indicates the importance of both oiur normalization-based optmziation-strategy and our 2d-list-based search-appraoch. In order to investigate the latter appraoches senstivity to database-size, we 'grudally' increase the data-size (while keepoping the 'search-space' fixeed), a search-space which is 'evaluated' both wrt. a key-searhc-order reflecting the inptu-order (or the relationships) and a random-access-search-order. Given the 'possiblity' of increased search-perofrmance for B-trees with higher/larger number of 'explcit' children, we (for the B-trees) evalate key-sizes of [4, 16, 32]. 
    @remarks different use-cases we cover (in this totital-example) are:    
    -- wildCard-searches:  describe cases where a 'sort-hueritc' (eg, for "b-trees") are unable to correctly directly/idneitfy results (ie, under the assumption that database need to have engelized lgocis to handle different case); examplifed through the "1d-list-iteraiton": for a 'pointer-based' B-tree the time-cost is sigicnatly higher (which expalins why we do Not investgiate the latter wrt. our B-tree-implementaitons)   
    -- normalizationSupport: the 'merging' of entites with the same meaning, a case explcitly 'pronoused' for data-relationships 'roinating' from data-ehcxange-formats' (eg, BioPax-formattet relationships). When our "normalizaiton-support" is combined with our "dild-card-searches" the result is a sigicnat perofmrance--boost: in our "x_measures db  synt__3" we observe a perofmrance-difference of mroe tahn 400,000x (when comparing a 'normalized-fast-access' strategy' with a 'list-iteration thuhg non-normalized relationships, a difference which is expected to be considerably larger if we had used our "B-tree" pointer-based resucive-search-implementaiton). 
    -- ...    
    @remarks our appraoch sigicnatly out-performs  MySQL (as seen throguh our "tut_external__mySQL.c"):
    -- brief: we have Not explcitly included calls to our MySQL script 'from this text-case' given the extreme perofmranc-edifferent (when comapred to our appraoch).
    -- param="isTo__useIndex__onTriplet = [0, 1]": based on observations we do Not observe any perofmrance-improvmeents when using our "index_triplet" in [bewlow], ie, where latter indicates that our "db_ds_directMapping.h" API results in a perofmrnace-boost (when comarped in disk-driven MySQL). (When evlauating the latter option we have also tried using "B-tree" as an 'idnes-sccture', an appraoch which did not result in a sigicnat/observable perofmrance-increase/change.)  const bool isTo_useKeyBasedSearchPattern = true; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const bool isTo_useKeyBasedSearchPattern = true; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
  const uint config__cntColsEach = 20;
  const uint cnt_predicatesForTails = 20;
    const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
  const uint cnt_synsEach = 5;
  const uint nrows_base = 1000*100; 
  //const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
  const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
  const bool isTo_useRandomAccess = true;
  uint current_search = 0;
#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} else {if(current_search++ >= nrows) {current_search = 0;} val_ret = current_search;} val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = search_id; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} val_ret;})
  const char *stringOf_resultDir = NULL;
  const bool isTo_evaluate_2dListAccess = false; //! which may be set to 'false' iot. reuce the sigicnat time-perofmrance-pentaly assicated to 2d-list-access: for many cases we observe a performacne-differecen fo 400,000x (for allist-based access and cosndierably higher for a pointer-based access, ie, for cases where 'the B-tree-sorted-property' may nto be sued (eg, due to wild-cards in searches). 
#endif
  long long initseed = 100; //! ie, to ensure that the randomoized funciton r'eturns the same result for all our cese', ie, in cotnrast to a 'nromaø' "(configure_function_uniform_dataType)time(0);" call.
  srand(initseed);  
  const uint config__keysToUse = 4; //! ie, ie, (head, rt, tail, relation_id)
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id > 0) {val_ret = nrows % search_id;}  val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = rand() % nrows; val_ret;})
  uint cnt_evaluationCases = 
    /*simpleList=*/1 + 
    /*B-tree==*/(2*e_db_ds_bTree_cntChildren_undef) + 
    /*s_db_ds_directMapping_t=*/e_db_ds_directMapping__initConfig_undef;
  if(isTo_evaluate_2dListAccess) {cnt_evaluationCases++;}
  //!
  //! Allocate space for the result-matrices:
  assert(cnt_nrowsCases > 0); assert(cnt_searches > 0); assert(cnt_evaluationCases > 0);
  s_kt_matrix_t mat_time_insert = initAndReturn__s_kt_matrix(cnt_nrowsCases, cnt_evaluationCases);
  s_kt_matrix_t mat_time_search = initAndReturn__s_kt_matrix(cnt_nrowsCases, cnt_evaluationCases);

#define __Mi__setColName(string) ({ if(case_id == 0) {set_stringConst__s_kt_matrix(&mat_time_insert, col_cnt_insert, string, /*addFor_column=*/true); set_stringConst__s_kt_matrix(&mat_time_search, col_cnt_search, string, /*addFor_column=*/true);}})


  //!
  //! Iterate through the differnet 'row-size-cases':
  t_float result = 0;
  for(uint case_id = 0; case_id < cnt_nrowsCases; case_id++) {
    const uint nrows = (1 + case_id) * nrows_base;
    { //! Set the row-header:
      char str_local[3000]; sprintf(str_local, "nrows(%u)", nrows);
      set_string__s_kt_matrix(&mat_time_insert, case_id, str_local, /*addFor_column=*/false);
      set_string__s_kt_matrix(&mat_time_search, case_id, str_local, /*addFor_column=*/false);
    }
    //! ------------------------------------------------------------
    const uint nrows__adjustedTo_db_rel =  (nrows / config__keysToUse);
    const uint nrows__adjustedTo_db_rel_tails =  (nrows / (/*keyImpact=*/config__keysToUse*/*tailImpact=*/config__cntColsEach));
    const uint cnt_syns = (cnt_synsEach*cnt_synsEach*cnt_synsEach);
    const uint nrows__adjustedTo_db_rel_tails__syns =  (nrows / (/*keyImpact=*/config__keysToUse*/*synonmImpact=*/cnt_syns*/*tailImpact=*/config__cntColsEach));
    const uint cnt_searches_adjusted_db_rel = (cnt_searches / config__keysToUse);
    const uint cnt_searches_adjusted_db_rel__syn = (cnt_searches / (config__keysToUse*cnt_syns));
    //const uint cnt_searches_adjusted_db_rel = (cnt_searches / config__keysToUse);
    //! ------------------------------------------------------------
    assert(cnt_searches_adjusted_db_rel > 0);
    assert(nrows > 0);     assert(nrows__adjustedTo_db_rel > 0);
    float cmp_time_insert = FLT_MAX; float cmp_time_search = FLT_MAX;
    uint *arrOf_uints;
    uint col_cnt_insert = 0;     uint col_cnt_search = 0;
    { //! Construct a 'reference-set' where we use a 1d-list:
      __Mi__setColName("directAccess"); //! ie, udpate the name.
      start_time_measurement();
      arrOf_uints = (uint*)malloc(sizeof(uint)*nrows);
      assert(arrOf_uints);
      for(uint row_id = 0; row_id < nrows; row_id++) {
	arrOf_uints[row_id] = row_id;
      }      
      cmp_time_insert = end_time_measurement("Simple list-access(insert)", 0);
      //! Update oru result-set:
      assert(col_cnt_insert < mat_time_insert.ncols);
      mat_time_insert.matrix[case_id][col_cnt_insert++] = cmp_time_insert;
      //! 
      //! Search:
      start_time_measurement();
      for(uint search_id = 0; search_id < cnt_searches; search_id++) {
	const uint row_id = __Mi__getRandVal(search_id);
	//const uint row_id = search_id % nrows;
	result += (t_float)arrOf_uints[row_id];
      }
      cmp_time_search = end_time_measurement("Simple list-access(search)", 0);
      //! Update oru result-set:
      assert(col_cnt_search < mat_time_search.ncols);
      mat_time_search.matrix[case_id][col_cnt_search++] = cmp_time_search;
    }

    //!
    //! Insert: 
    { //! Case: B-tree w/"key-->value" mapping: 
      for(uint blockCase = 0; blockCase < e_db_ds_bTree_cntChildren_undef; blockCase++) {
	start_time_measurement();
	s_db_ds_bTree_keyInt_t obj = init__s_db_ds_bTree_keyInt_t((e_db_ds_bTree_cntChildren_t)blockCase);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  s_db_baseNodeId_typeOfKey_int_t key; key.head = row_id;
	  insert__s_db_ds_bTree_keyInt_t(&obj, key);
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "B-tree(bCase:%u):insert", blockCase);
	  __Mi__setColName(str_local); //! ie, udpate the name.
	  const float endTime = end_time_measurement(str_local, cmp_time_insert);
	  //! Update oru result-set:
	  assert(col_cnt_insert < mat_time_insert.ncols);
	  mat_time_insert.matrix[case_id][col_cnt_insert++] = endTime;
	}
	//! 
	//! Search:
	start_time_measurement();
	for(uint search_id = 0; search_id < cnt_searches; search_id++) {
	  //const uint row_id = search_id % nrows;
	  const uint row_id = __Mi__getRandVal(search_id);
	  s_db_baseNodeId_typeOfKey_int_t key; key.head = row_id;
	  s_db_baseNodeId_typeOfKey_int_t result_obj;
	  result += (t_float)find__s_db_ds_bTree_keyInt_t(&obj, key, &result_obj, (isTo_useKeyBasedSearchPattern == false), NULL);
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "B-tree(bCase:%u):serach", blockCase);
	  const float endTime = end_time_measurement(str_local, cmp_time_search);
	  //! Update oru result-set:
	  assert(col_cnt_search < mat_time_search.ncols);
	  mat_time_search.matrix[case_id][col_cnt_search++] = endTime;
	}
      }
    }
    { //! Case: B-tree w/"s_db_rel_t" mapping: 
      for(uint blockCase = 0; blockCase < e_db_ds_bTree_cntChildren_undef; blockCase++) {
	start_time_measurement();
	s_db_ds_bTree_rel_t obj = init__s_db_ds_bTree_rel_t((e_db_ds_bTree_cntChildren_t)blockCase);
	uint row_id_local = 0;
	for(uint row_id = 0; row_id < nrows__adjustedTo_db_rel; row_id++) {
	  s_db_rel_t key = __M__init__s_db_rel_t();
	  key.head = row_id_local;
	  key.tailRel.rt = row_id_local + 1;
	  key.tailRel.tail = row_id_local + 2;
	  key.tailRel.relation_id = row_id_local + 3;
	  //! Appply lgoics:
	  insert__s_db_ds_bTree_rel_t(&obj, key);
	  row_id_local += config__keysToUse;
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "B-tree:rel(bCase:%u):insert", blockCase);
	  __Mi__setColName(str_local); //! ie, udpate the name.
	  const float endTime = end_time_measurement(str_local, cmp_time_insert);
	  //! Update oru result-set:
	  assert(col_cnt_insert < mat_time_insert.ncols);
	  mat_time_insert.matrix[case_id][col_cnt_insert++] = endTime;
	}
	//! 
	//! Search:
	start_time_measurement();
	uint max_rowId = nrows - config__keysToUse - 1;
	for(uint search_id = 0; search_id < cnt_searches_adjusted_db_rel; search_id++) {
	  uint row_id = __Mi__getRandVal(search_id);
	  row_id = macro_max(max_rowId, row_id);
	  s_db_rel_t key = __M__init__s_db_rel_t();
	  key.head = row_id;
	  key.tailRel.rt = row_id + 1;
	  key.tailRel.tail = row_id + 2;
	  key.tailRel.relation_id = row_id + 3;
	  //! Appply lgoics:
	  s_db_rel_t result_obj = __M__init__s_db_rel_t();
	  //result += 
	  //printf("key = %u, at %s:%d\n", (uint)key.head, __FILE__, __LINE__);
	  (t_float)find__s_db_ds_bTree_rel_t(&obj, key, &result_obj, (isTo_useKeyBasedSearchPattern == false), NULL);
	  result += (t_float)result_obj.head;
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "B-tree:rel(bCase:%u):serach", blockCase);
	  const float endTime = end_time_measurement(str_local, cmp_time_search);
	  //! Update oru result-set:
	  assert(col_cnt_search < mat_time_search.ncols);
	  mat_time_search.matrix[case_id][col_cnt_search++] = endTime;
	}
	//!
	//! De-allocate:
	free__s_db_ds_bTree_rel_t(&obj);
      }
    }
    { //! Case: B-tree w/"s_db_rel_t" mapping: 
      for(uint blockCase = 0; blockCase < e_db_ds_directMapping__initConfig_undef; blockCase++) {
	if(!isTo_evaluate_2dListAccess && (blockCase == e_db_ds_directMapping__initConfig__2dList)) {continue;} //! ie, as we then assume taht 'this case ius accited th all to long exeuciton-time.
	start_time_measurement();
	uint __nrowsLocal = nrows__adjustedTo_db_rel_tails;
	uint __local__cntSyns = cnt_searches_adjusted_db_rel;
	if(useOptimizedSynMappings__e_db_ds_directMapping__initConfig((e_db_ds_directMapping__initConfig_t)blockCase)) {
	  __nrowsLocal = nrows__adjustedTo_db_rel_tails__syns;
	  __local__cntSyns = cnt_searches_adjusted_db_rel__syn;
	}
	s_db_ds_directMapping_t obj = init__enumConfig__s_db_ds_directMapping_t(/*cnt_head=*/nrows, /*cnt_relations=*/nrows, /*enum_id=*/(e_db_ds_directMapping__initConfig_t)blockCase);
	/* { //! Add synonyms */

	/*   assert(false); // FIXME[conceptual] describe how we are to 'isnert synonyms wrt. name-sapce' in our "tut_db_bTree_simpleExample_3__2dList.c". */

	/*   //! Add synonyms for: head, tail: */
	/*   assert(false); // FIXME: add seomthign. */
	/*   //! Add synonyms for: relation-type (rt): */
	/*   for(uint i = 0; i < cnt_predicatesForTails; i++) { */
	/*   assert(false); // FIXME: add seomthign.	     */
	/*   } */
	/* } */
	uint row_id_local = 0;
	uint current_pred = 0;
	for(uint row_id = 0; row_id < __nrowsLocal; row_id++) {
	  for(uint tail_id = 0; tail_id < config__cntColsEach; tail_id++) {
	    s_db_rel_t key = __M__init__s_db_rel_t();
	    key.head = row_id_local;
	    if(current_pred >= cnt_predicatesForTails) {current_pred = 0;} else {current_pred++;}
	    key.tailRel.rt = current_pred;
	    //key.tailRel.rt = row_id_local + 1;
	    key.tailRel.tail = row_id_local + 1;
	    key.tailRel.relation_id = row_id_local + 2;
	    //! Appply lgoics:
	    insertRelation__s_db_ds_directMapping_t(&obj, key);
	    row_id_local += config__keysToUse;
	  }
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "directMapping(%s)::insert", getString__e_db_ds_directMapping__initConfig((e_db_ds_directMapping__initConfig_t)blockCase));
	  __Mi__setColName(str_local); //! ie, udpate the name.
	  const float endTime = end_time_measurement(str_local, cmp_time_insert);
	  //! Update oru result-set:
	  assert(col_cnt_insert < mat_time_insert.ncols);
	  mat_time_insert.matrix[case_id][col_cnt_insert++] = endTime;
	}
	//! 
	//! Search:
	start_time_measurement();
	uint max_rowId = nrows - config__keysToUse - 1;
	current_pred = 0;
	for(uint search_id = 0; search_id < __local__cntSyns; search_id++) {
	  uint row_id = __Mi__getRandVal(search_id);
	  row_id = macro_max(max_rowId, row_id);
	  s_db_rel_t key = __M__init__s_db_rel_t();
	  key.head = row_id;
	  if(current_pred >= cnt_predicatesForTails) {current_pred = 0;} else {current_pred++;}
	  key.tailRel.rt = current_pred;
	  key.tailRel.tail = row_id + 1;
	  key.tailRel.relation_id = row_id + 2;
	  //! Appply lgoics:
	  //s_db_rel_t result_obj = __M__init__s_db_rel_t();
	  //result += 
	  //printf("key = %u, at %s:%d\n", (uint)key.head, __FILE__, __LINE__);
	  uint cnt_interesting = 0;
	  (t_float)findRelation__cntInteresting__s_db_ds_directMapping_t(&obj, key, &cnt_interesting, /*namesAlreadyMappedIntoSynonyms=*/false, NULL, true); //, (isTo_useKeyBasedSearchPattern == false));
	  result += (t_float)cnt_interesting;
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "directMapping(%s)::search", getString__e_db_ds_directMapping__initConfig((e_db_ds_directMapping__initConfig_t)blockCase));
	  const float endTime = end_time_measurement(str_local, cmp_time_search);
	  //! Update oru result-set:
	  assert(col_cnt_search < mat_time_search.ncols);
	  mat_time_search.matrix[case_id][col_cnt_search++] = endTime;
	}
	//!
	//! De-allocate:
	free__s_db_ds_directMapping_t(&obj);
      }
    }
    //!
    //! De-allocate:
    assert(arrOf_uints);  free(arrOf_uints); arrOf_uints = NULL; //! ie, de-allocate.
  }

  //! -------------------------------------------------------------------------
  //!
  //! Prepare data-export:
  char str_filePath[2000] = {'\0'}; 
  if(stringOf_resultDir && strlen(stringOf_resultDir)) {
    if(true) {
      sprintf(str_filePath, "%s", stringOf_resultDir); //! hten we 'allow' the "stringOf_resultDir" to descirbe a file-preifx (and not only a directory-name).
    } else {
      if(stringOf_resultDir[strlen(stringOf_resultDir)-1] == '/') {
	sprintf(str_filePath, "%s", stringOf_resultDir);
      } else {sprintf(str_filePath, "%s/", stringOf_resultDir);}
    }
  }  
  //! 
  //! Specificy an 'itnernal' wrapper-matcro to export the reuslts:
#define __Mi__export(matrix, suffix) ({ \
      char str_conc[2000] = {'\0'}; sprintf(str_conc, "%s%s.tsv", str_filePath, suffix); \
      bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); \
      memset(str_conc, '\0', 2000); sprintf(str_conc, "%s%s.transposed.tsv", str_filePath, suffix); \
      /*! Then export a tranpsoed 'verison': */				\
      is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); })

  //!
  //! Export
  __Mi__export(&mat_time_insert, "data_insert");
  __Mi__export(&mat_time_search, "data_search");
  
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_time_insert);
  free__s_kt_matrix(&mat_time_search);
#undef __Mi__export

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
