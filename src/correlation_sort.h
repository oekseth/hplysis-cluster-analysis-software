#ifndef correlation_sort_h
#define correlation_sort_h

#include "correlation_base.h"

//! Infer the rank of elements in a non-sorted list of values, eg, a column in a matrix.
typedef enum e_distance_rank_typeOf_computation {
  e_distance_rank_typeOf_computation_ideal,
  e_distance_rank_typeOf_computation_quickSort,
  e_distance_rank_typeOf_computation_mergeSort,
  e_distance_rank_typeOf_computation_undef
} e_distance_rank_typeOf_computation_t;

/**
   @enum e_typeOf_sortFunction
   @brief describe different supported sort-functions.
 **/
typedef enum e_typeOf_sortFunction {
  e_typeOf_sortFunction_ideal, //! ie, then chooses the sort-fucniton with the highest perofrmance, ie, lwoest time-cost
  e_typeOf_sortFunction_quickSort_POSIX,
  e_typeOf_sortFunction_quickSort_localImplementaiton_indexOverhead,
  e_typeOf_sortFunction_quickSort_templateImplementation_default,
  e_typeOf_sortFunction_quickSort_templateImplementation_randomized,
  e_typeOf_sortFunction_mergeSort_internal,
  e_typeOf_sortFunction_mergeSort_internal_memoryAcceesOptimized,
  e_typeOf_sortFunction_mergeSort_list_generic, //! ie, teh merge-osrt routine used in our lsit-gneeric fucniton.
  e_typeOf_sortFunction_insertionSort_default,
  //e_typeOf_sortFunction_insertionSort_default_reverse,
  e_typeOf_sortFunction_insertionSort_bubbleSort,
  e_typeOf_sortFunction_undef
} e_typeOf_sortFunction_t;



//! @return the median 
//! @remarks is an atlernatve fucntion to comptue median.
t_float get_median_alt2 (int n, t_float x[]);
  //! @remarks time-complexity estimated to "2"
  //! @remarks if arr_tmpToBeUsedIn_sorting is set to NULL, then we assuem the inptu-list is sorted.
  t_float get_median(t_float *arrOf, t_float *arr_tmpToBeUsedIn_sorting, const uint arrOf_list_size);
//! Apply a  variaent of the quick-sort algorithm
void quicksort(const uint n, const t_float *arr_input, uint *arr_index, t_float *arr_result);
//! Apply a  variaent of the quick-sort algorithm
void quicksort_uint(const uint n, const uint *arr_input, uint *arr_index, uint *arr_result);

/**
   sort_array -- Sorts the array by moving elements forward
   until a a key that is greater than 'this' is found.
   @Changed: 22.06.2011 by oekseth
*/
long int sort_array_insertionSort_slow(t_float *array, const uint size);
/**
   @brief a modfied isnertion-sort with optimized memory-access-schemes
   @remarks expeted to perform 2.2x faster that the "sort_array_insertionSort_slow(..)" function: combines ordered lsit-traversal with optimized meomrya-cceess throguht ehte "memmove(..)" funciton.
*/
long int sort_array_insertionSort(t_float *list, const uint list_size);
//! Use merge-sort for in-place sorting of arr_input data.
//! @return the sort-distance, which is (for some cases) used tor computation of Kendalls Tau.
long int mergeSort(t_float* arr_input, t_float* arr_tmp, const size_t arr_size, uint *arr_index /* = NULL */);

  //! Sort a given array.
  void sort_array(const t_float *array_in, t_float *array_result, const uint array_size, const enum e_typeOf_sortFunction sort_type);



  //! @return the identifed distance for the pair in a symmetric matrix
  //! @remarks This function searches the distance matrix to find the pair with the shortest distance between them. 
  //!          The indices of the pair are returned in ip and jp; 
  //! @remarks time-complexity estimated to be "n*n", ie, quadratic.
  t_float find_closest_pair(const uint n, t_float** distmatrix, uint* ip, uint* jp);

#endif //! EOF
