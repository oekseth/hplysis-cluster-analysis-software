
//! *****************************************************************
//! *****************************************************************
//!               Define the temproary data-set, eg, used by our "func_template__postProcess__completeCorrelationComputation_squaredDistanceMatrix.c"
//! *****************************************************************

//printf("......... at %s:%d\n", __FILE__, __LINE__);

#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
if(listOf_inlineResults) {
    assert(SM > 0);
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
    KT__memset_2d_list(listOf_inlineResults, SM, SM, T_FLOAT_MIN);
    assert(listOf_inlineResults[0][0] = T_FLOAT_MIN); //! ie, to validte the memory isneriton funciton.
  //    const uint size = SM * SM; for(uint i = 0; i < size; i++) {listOf_inlineResults[0][i] = T_FLOAT_MIN;} //! ie, then intiate the values to 'nearly 0', though with difference tha tthe latter is used as a 'mark' to state that 'a value has Not been set';
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
    KT__memset_2d_list(listOf_inlineResults, SM, SM, T_FLOAT_MAX);
    assert(listOf_inlineResults[0][0] = T_FLOAT_MAX); //! ie, to validte the memory isneriton funciton.
    //const uint size = SM * SM; for(uint i = 0; i < size; i++) {listOf_inlineResults[0][i] = T_FLOAT_MAX;}
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
    KT__memset_2d_list(listOf_inlineResults, SM, SM, T_FLOAT_MIN_ABS);
    assert(listOf_inlineResults[0][0] = T_FLOAT_MIN_ABS); //! ie, to validte the memory isneriton funciton.    
    //    const uint size = SM * SM; for(uint i = 0; i < size; i++) {listOf_inlineResults[0][i] = T_FLOAT_MIN_ABS;}
#else //! tehnw e are itnerested in the max-value
#warning "Investigate why this case was 'entered'"
#endif //! otehrwise we assuemt that eh compelx object is udpated, a 'compelx object' which we anyhow choose to udpate (ie, aslos tof rthe 'only-compare-tow-rows' case).
  //memset(listOf_inlineResults, 0, sizeof(t_float)*SM); //! ie, clear the content:
  assert(resultMatrix == NULL);
 }
/*   t_float *__restrict__ rres_startPos = NULL;  */
/* if(listOf_inlineResults) { */
/*   rres_startPos = listOf_inlineResults[0]; */
/*  } else { */
/*   assert(resultMatrix != NULL); */
/*   rres_startPos = &resultMatrix[index1][index2]; //! ie, the 'upper square' of the matrix we update.		   		    */
/*  } */
#endif

/* #if(__localConfig__templateTile__rowSize_isKnown == 1) */
/* assert((index1 + chunkSize_index1) <= nrows); // FIXME: remove. */
/* assert((index2 + chunkSize_index2) <= nrows); // FIXME: remove. */
/* #endif */


{ //! Teh code-chunk to comptue for the tiling (oekseth, 06. otk. 2016).
#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /*ie, e_template_correlation_tile_maskType_mask_explicit */
  assert(mask1);   assert(mask2); //! ie, what we expect.
  const char *__restrict__ rmask1 = NULL; const char *__restrict__ rmask2 = NULL; 
#endif
#if(__templateInternalVariable__isTo_updateCountOf_vertices == __macro__e_template_correlation_tile_maskType_mask_explicit) //! then we are to update the count of itnereesting cells.
  assert(complexResult);
  assert(complexResult->matrixOf_result_cnt_interesting_cells); //! ie, as the latter shoudl then have been set.
#endif

  // assert(false); // FIXME: update the "s_kt_computeTile_subResults_t" to 'provide' an internal idnex-reference .... ie, to 'facitlaite' memory-reduction by 'only' allocating memory/'memory-space' for "[chunkSize_index1][chunkSize_index2]"


#include "funcRef_aux__set_minMax_variables.c"


  //  printf("......... numberOf_chunks_cols=%u, at %s:%d\n", numberOf_chunks_cols, __FILE__, __LINE__);
  assert_possibleOverhead(numberOf_chunks_cols >= 1);
  //__i__allAgainstAll_nonSIMD__subSquare_notTransposed_maskImplicit(index1, index2, 
  const t_float *__restrict__ rmul1; const t_float *__restrict__  rmul2; 
  uint i = 0;
  //printf("SM=%u, SM-last=%u, ncols=%u, cnt-chunkd=%u, at %s:%d\n", SM, chunkSize_col_last, ncols, numberOf_chunks_cols, __FILE__, __LINE__);
  assert_possibleOverhead( ( (numberOf_chunks_cols-1)*SM + chunkSize_col_last) == ncols); //! ie, what we expect wrt. cosnsitency (oekseth, 06. nvo. 2016).
  for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
  //for(uint cnt_i = 0; cnt_i < tile_col.cntBlocks; cnt_i++, i += SM)  {
    //uint chunkSize_cnt_i = get_blockSize__s_aux_findPartitionFor_tiles_t(&tile_col, cnt_i);
    uint chunkSize_cnt_i = ( (cnt_i + 1 ) < numberOf_chunks_cols) ? SM : chunkSize_col_last;
    uint chunkSize_cnt_i_intris = (chunkSize_cnt_i == SM) ? chunkSize_cnt_i : 
      //! Then we idneitfy the size of the inner intristinc code-block: 
      (chunkSize_cnt_i > VECTOR_FLOAT_ITER_SIZE) ? chunkSize_cnt_i - VECTOR_FLOAT_ITER_SIZE : 0; 


    //! --------------------------------------------------------------------------------------
    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
    uint i2 = 0; //const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; 

    //printf("......... at %s:%d\n", __FILE__, __LINE__);
 uint debug_local_cntChunks_evaluated = 0;

    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
    //assert(resultMatrix[index1]);
    //const uint debug_startPos_rres = (index1 * ncols) + index2;
    // assert(resultMatrix + debug_startPos_rres);
    for (i2 = 0 //! ie, for "index1" 
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
	   //rres  = rres_startPos, //&resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
#endif //! else we assuem 'this' stricture is Not to be udpated.
	   // FIXME: validate [below]
	   //rmul1 = &data1[index1][i]
/* #if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /\*ie, e_template_correlation_tile_maskType_mask_explicit *\/ */
/* 	   , rmask1 = &mask1[index1][i] */
/* #endif */
	   ;
	 i2 < chunkSize_index1;
	 i2++ 
	   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
	   //rres += ncols, 
	   //	   rres += ncols, 
/* 	   , */
/* 	   rmul1 += ncols */
/* #if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /\*ie, e_template_correlation_tile_maskType_mask_explicit *\/ */
/* 	   , rmask1 += ncols */
/* #endif */
	   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
	 ) {
      rmul1 = &data1[index1+i2][i]; //! ie, emulating "rmul1 += ncols" (oekseth, 06. jul. 2017).
#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /*ie, e_template_correlation_tile_maskType_mask_explicit */
      rmask1 = &mask1[index1+i2][i];
#endif
      //printf("......... at %s:%d\n", __FILE__, __LINE__);

#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
  t_float *__restrict__ rres = NULL; 
  //t_float *__restrict__ rres_startPos = NULL;
//t_float **local_resultMatrix = &listOf_inlineResults[;

//printf("......... at %s:%d\n", __FILE__, __LINE__);

if(listOf_inlineResults) {
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(resultMatrix == NULL); // TODO: vlaidate correcntes of this assumption (oekseth, 06. des. 2016).
  rres = listOf_inlineResults[i2];
  // printf("use-inline-result-block, at %s:%d\n", __FILE__, __LINE__);
  //rres = &listOf_inlineResults[i2][0];
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(listOf_inlineResults);
  //fprintf(stderr, "id=%u < %u, and access poitner=listOf_inlineResults=%p, listOf_inlineResults[..]=%p,  at %s:%d\n", i2, SM, listOf_inlineResults, listOf_inlineResults[i2], __FILE__, __LINE__);
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(i2 < SM); //! ie, as we expec tthe 'tempraorly chunk' to have been allocated to [SM, SM] in our "func_template__allAgainstAll.c"  (oekseth, 06. des. 2016).
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(listOf_inlineResults[i2]);
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(rres);
  //printf("rres\t\tfirst-val=%f given poiner=%p, i2=%u, at %s:%d\n", rres[0], rres, i2, __FILE__, __LINE__);
  //assert(rres != NULL); // FIXME: remove
 } else {
  //  if(self.sparseDataResult == NULL) 
{ 
    assert(resultMatrix != NULL);
  //rres_startPos = &resultMatrix[index1][index2]; //! ie, the 'upper square' of the matrix we update.		   		   
  /* rres = &rres_startPos[i2]; //! ie, the 'upper square' of the matrix we update.		   		    */
  /* assert(resultMatrix[index1+12] == (&resultMatrix[index1][index2]); //! ie, the 'upper square' of the matrix we update.		   		    */
  //  assert(
/* #if(__localConfig__templateTile__rowSize_isKnown == 1) */
/*   /\* assert( (index1+ i2) < nrows); // FIXME: remove *\/ */
/*   /\* assert( (index2+ chunkSize_index2 - 1) < nrows); // FIXME: remove *\/ */
/*   /\* printf("investigate matrix in range[..%u...%u../%u][..%u../%u], resultMatrix=%p, at %s:%d\n", index1, (uint)i2, nrows, index2, nrows, resultMatrix, __FILE__, __LINE__); *\/ */
/* #else */
/*   printf("(nrows not known), at %s:%d\n", __FILE__, __LINE__); */
/* #endif   */
  /* assert(&resultMatrix[index1][index2] != NULL); // FIXME: remove */
    rres = &(resultMatrix[index1+i2][index2]); //! ie, the 'upper square' of the matrix we update.		   		   
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(rres);
  }
  /* assert(&resultMatrix[index1+0][index2] != NULL); // FIXME: remove */
  /* assert(&resultMatrix[index1+1][index2] != NULL); // FIXME: remove */
  /* assert(&resultMatrix[index1+i2][index2] != NULL); // FIXME: remove */
  /* t_float *tmp = &resultMatrix[index1+i2][index2]; //! ie, the 'upper square' of the matrix we update.		   		    */
  /* t_float *__restrict__ tmp_2 = &resultMatrix[index1+i2][index2]; //! ie, the 'upper square' of the matrix we update.		   		    */
  /* printf("poiner: %p == %p (= %p = %p) , at %s:%d\n", rres, &resultMatrix[index1+i2][index2], tmp, tmp_2, __FILE__, __LINE__); */
  /* assert(rres != NULL); // FIXME: remove */
  /* printf("first-val=%f given poiner=%p, at %s:%d\n", rres[0], rres, __FILE__, __LINE__); */
  /* assert(&rres == &resultMatrix[index1+i2][index2+0]); // FIXME: remvoe */
  /* assert(&rres[0] == &resultMatrix[index1+i2][index2+0]); // FIXME: remvoe */
  /* assert(&rres[1] == &resultMatrix[index1+i2][index2+1]);// FIXME: remvoe */
    //); //! ie, the 'upper square' of the matrix we update.		   		   
 }
#endif

/* #if(__localConfig__templateTile__rowSize_isKnown == 1) */
/* #if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar) */
/*       if(listOf_inlineResults == NULL) {// FIXME: remove */
/* 	printf("validate thtat rres is set correctly, given index1=%u+%u, and poitner=%p, at %s:%d\n", index1, i2, rres, __FILE__, __LINE__); */
/* 	assert(rres <= &resultMatrix[nrows-1][nrows-1]); // FIXME: remvoe */
/* 	assert(&rres[chunkSize_index2-1] <= &resultMatrix[nrows-1][nrows-1]); // FIXME: remvoe */
/*       } else { */
/* 	printf("(use-internal-sub-sample-list, at %s:%d\n", __FILE__, __LINE__); */
/*       } */
/* #endif */
/* #endif */
      // assert( ((rres + ncols*chunkSize_index1) -1) <= pointer_resultLastPos1);

      //! Prefetch the data:
      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
      _mm_prefetch (&data1[8], _MM_HINT_NTA);
#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /*ie, e_template_correlation_tile_maskType_mask_explicit */
      _mm_prefetch (&mask1[8], _MM_HINT_NTA);
#endif
      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      
      uint k2 = 0; 



      for (k2 = 0 //! ie, for "index2" 
	     // FIXME: validate [below]
	     //rmul2 = &data2[index2][i]
#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /*ie, e_template_correlation_tile_maskType_mask_explicit */
	     //	     , rmask2 = &mask1[index2][i]
#endif
;  
	   k2 < chunkSize_index2; k2++ // , rmul2 += chunkSize_cnt_i
	     //! Move to the same psotion in the next row:
	     // FIXME: validte [below]
	     //rmul2 += ncols
/* #if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /\* ie, e_template_correlation_tile_maskType_mask_explicit *\/ */
/* 	     , rmask2 +=  ncols */
/* #endif */
	   ) {				
	rmul2 = &data2[index2+k2][i]; //! theryeby repalcing our "rmul2 += ncols" (oekseth, 06. jul. 2017).
#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /*ie, e_template_correlation_tile_maskType_mask_explicit */
	rmask2 = &mask1[index2+k2][i];
#endif
/* #if(__localConfig__templateTile__rowSize_isKnown == 1) */
/* 	assert(k2 < nrows); // FIXME: remove. */
/* 	printf("(i2, k2)=(%u, %u|nrows=%u & index2=%u), ie, the current 'cell' to correlate, at %s:%d\n", i2, k2, nrows, index2, __FILE__, __LINE__); */
/* 	assert((index2+k2) < nrows); // FIXME: remove. */
/* #else  */
/* 	printf("(i2, k2)=(%u, %u), ie, the current 'cell' to correlate, at %s:%d\n", i2, k2, __FILE__, __LINE__); */
/* #endif */

	//uint debug_local_cntChunks_evaluated = 0;
	//!
	//! Investigate if we are to set the min-valeu or '0', ie, if (a) teh count' or (b) teh min-value is of itnerest, ie, is to be used.
// #if(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.

// 	assert(false); // FIXME:w rt the "isTo_findMaxValue" ... inveistae if tehre are macro-cases where 'parts of teh complex object is to be intiated to 'min' OR 'max'.

	//#endif	  

	//! ----
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
	    //! Tehn we itnaite a temproary 'on-SSE' boject ot hold the results:
	s_template_correlation_tile_temporaryResult_SSE_t objLocalComplex_sse; setTo_empty__s_template_correlation_tile_temporaryResult_SSE(&objLocalComplex_sse, isTo_findMaxValue, isTo_findMinValue);	    
	s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, isTo_findMaxValue, isTo_findMinValue);
#elif(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
	s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, isTo_findMaxValue, isTo_findMinValue);
#endif

	//! The operation:		
	//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
	// _mm_prefetch (&data2[8], _MM_HINT_NTA);
		
	//! The operation:		
	//! Note: we expect every valeu 'ot fit onto' chunkSize_cnt_i, ie, as the latter is tob e divisible by "4", ie, which explains why we do not sue a ssperate for-loop 'after [”elow]'.


	//assert(false); // in the ”[elow] call ... udapte the 'called' amcor to use the real-life 'case'.

#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) )
	t_float result = T_FLOAT_MIN; //! ie, then intiate the values to 'nearly 0', though with difference tha tthe latter is used as a 'mark' to state that 'a value has Not been set';
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
	t_float result = T_FLOAT_MAX;
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
	t_float result = T_FLOAT_MIN_ABS;
#else
#warning "Investigate this issue, ie, are we to suem 'um of vlaues'?"
#endif
	//printf("default-value-default=%f, at %s:%d\n", result, __FILE__, __LINE__);
#endif
	{ //! Compute the results for the 'lcoal tile':
	  //
	  //! Note: the "configure_SSE_" vairalbes are expected to be defined in our "configure_cCluster.h" (and possilbe being updated by/in the buidlign of the Make-file and set during compilation)
	  //

	  // TODO: vlaidate correnctess wrt. [”elow] 'settting' of "global_index1" and "global_index2" <-- seems like [”elow] is wrong, ie, 'udpate this' wehn we start work on imrpvoing the 'mina tiling-function' (which 'this' is wrapped inside).
	  const uint global_index1 = getRowIndex__s_kt_computeTile_subResults_t(complexResult, i2);
	  const uint global_index2 = getColumnIndex__s_kt_computeTile_subResults_t(complexResult, k2);
	  //printf("\t[%u][%u], at %s:%d\n", global_index1, global_index2, __FILE__, __LINE__);

	  //! --------------------------------------
	  //! Comptue for each column-index

/* #if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar) */
/* 	  //printf("result=%f, at %s:%d\n", result, __FILE__, __LINE__); */
/* #if(__localConfig__templateTile__rowSize_isKnown == 1) */
/* 	assert(k2 < nrows); // FIXME: remove. */
/* 	printf("(i2, k2)=(%u, %u|nrows=%u & index2=%u), ie, the current 'cell' to correlate, at %s:%d\n", i2, k2, nrows, index2, __FILE__, __LINE__); */
/* 	assert((index2+k2) < nrows); // FIXME: remove. */
/* 	if(listOf_inlineResults != NULL) { */
/* 	  printf("\t(default-value:k2=%u)\t %f, at %s:%d\n", k2, rres[k2], __FILE__, __LINE__); */
/* 	} */
/* #endif  */
/* #endif */
	  
	  // assert(false); // FIXME: include [”elow]

#define __config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject 1
#include "template_correlation_tile_innerMostColumn.c"

	  //#if (TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_implicit) || (TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) 
	  //!
	  //! Then we are interested in valdaitng that the valeus has been set  (oekseth, 06. jan. 2017).
	  //! Note[correctness]; in this cotnext that we asusem that teh 'case' where 'sum of vaues in a factor'="T_FLOAT_MIN" has a simlair probiality (ie, will never occure for cases which inluence the results), ie, for whcih the latter is sued as an idnciator for 'emptyness' wrt. 'sums of vlaues' (oesketh, 06. jan. 2017);
	  //! Note[optimizaiton]: though we could have used a 'counter' to investiatgate/idneityf 'case where none of the cells have been ivnestigated' the latter would have ocurred a permance-overhead: as we in our optmizaiton would have 'nneded' to update an object sigicnatly larger than sizeof(t_float) we asser/asusem that 'this' would have incurred a noticable perofrmance-delya, ie, hence the improtance for our optmizaiton-appraoch (oekseth, 06. jan. 2017).
	  // FIXME[article]: udpate our artilce wrt. [abov€] assumption/observiaotn/heuystics ... 'using' the latter as an example fo an optmizaiotn-strategy.
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
	  const t_float result_tmp_2 = VECTOR_FLOAT_getAtIndex(objLocalComplex_sse.numerator, /*at-index=*/0);
	  const bool sse_isOf_interest = ((result_tmp_2 != T_FLOAT_MIN) & (result_tmp_2 != T_FLOAT_MIN_ABS) & (result_tmp_2 != T_FLOAT_MAX));
	  const t_float result_tmp = objLocalComplex_nonSSE.numerator; //, /*at-index=*/0);
#else
	  const t_float result_tmp = result;
	  const bool sse_isOf_interest = true;
#endif

	    if( sse_isOf_interest && (
				      (result_tmp != T_FLOAT_MIN) & (result_tmp != T_FLOAT_MIN_ABS) & (result_tmp != T_FLOAT_MAX) 
				      )
		)
	      //#endif
	    { //! Then we update the 'concatened results:

	      //! --------------------------------------
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)


#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)


	      /* if(listOf_inlineResults) { */
	      /*   rres = listOf_inlineResults[0]; */
	      /* } else { */
	      /*   assert(resultMatrix != NULL); */
	      /*   rres = &resultMatrix[index1][index2]; //! ie, the 'upper square' of the matrix we update.		   		    */
	      /* } */
#endif

	      //	printf("\t(update-result:k2=%u=[%u][%u]=[%u][%u]:global)\t input-value: %f, at %s:%d\n", k2, i2, k2, index1+i2, index2+k2, result, __FILE__, __LINE__);
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
	      //	printf("\t(update-result:k2=%u=[%u][%u]=[%u][%u]:global)\t += %f, given pointer=%p, at %s:%d\n", k2, i2, k2, index1+i2, index2+k2, result, rres, __FILE__, __LINE__);
	      // printf("\t(update-result::sum::k2=%u)\t %f + %f, at %s:%d\n", k2, rres[k2], result, __FILE__, __LINE__);

	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(rres);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(k2 < SM); //! ie, as we expec tthe 'tempraorly chunk' to have been allocated to [SM, SM] in our "func_template__allAgainstAll.c"  (oekseth, 06. des. 2016).
/* #if (TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_implicit) || (TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit)  //! a macro which is used to simplify debuggin (oekseth, 06. jan. 2017) */
/* 	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(result != T_FLOAT_MIN); */
/* #else */
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(result != T_FLOAT_MIN);
	      //#endif
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(result != T_FLOAT_MAX);
	      //assert_possibleOverhead(isinf(result)  == false); //! where latter is otu-commented to 'handle' the case/issue where 'hgih values results in 'inf' when beiing mulipels with each other', ie, where 'inf' is a correct answer, ie, does Not idnicate an error (oekseth, 06. arp. 2017)
	      // printf("increment-score[%u] = %f + %f, at %s:%d\n", k2, rres[k2], result, __FILE__, __LINE__);
	      rres[k2] += result;
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
	      //printf("\t(update-result)\t min(%f, %f), at %s:%d\n", rres[k2], result, __FILE__, __LINE__);
	      assert_possibleOverhead(isinf(result)  == false); 
	      rres[k2] = macro_min(rres[k2], result); //! ie, seleec the min
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
	      assert_possibleOverhead(isinf(result)  == false); 
	      //printf("\t(update-result)\t max(%f, %f), at %s:%d\n", rres[k2], result, __FILE__, __LINE__);
	      rres[k2] = macro_max(rres[k2], result); //! ie, seleec the min
#else //! tehnw e are itnerested in the max-value
	      //assert(false); // FIXME: ensure taht our caller has itnaited the result-valeus to T_FLOAT_MIN_ABS
#warning "Investigate why this case was 'entered'"
	      //#endif
#endif //! otehrwise we assuemt that eh compelx object is udpated, a 'compelx object' which we anyhow choose to udpate (ie, aslos tof rthe 'only-compare-tow-rows' case).
#endif 
	    }
	  debug_local_cntChunks_evaluated++;
	} //! and at this exeuction-point the lcoal tile is comptued.
      }
    }
    //! ----
#if(__localConfig__templateTile__rowSize_isKnown == 1)
    { // FIXME: remvoe thsi code-chunk
      //printf("## debug_local_cntChunks_evaluated=%u, debug_cnt_resultCells_updated=%u, at %s:%d\n", debug_local_cntChunks_evaluated, debug_cnt_resultCells_updated, __FILE__, __LINE__);
      if(debug_cnt_resultCells_updated != 0) {
	assert_possibleOverhead(debug_local_cntChunks_evaluated == debug_cnt_resultCells_updated);
      }
      debug_cnt_resultCells_updated = debug_local_cntChunks_evaluated;
    }
#endif
    //! ----
  }
}
