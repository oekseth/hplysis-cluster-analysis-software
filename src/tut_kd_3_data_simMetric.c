#include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hp_ccm.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "export_ppm.c" //! which is used for *.ppm file-export.
#endif
 

static void __tut_kd_3_manyMany_singleInput_exportTo_ppm(s_kt_matrix_t mat_result, const char *file_pref) {
  //! Copy:
  s_kt_matrix_t mat_cpy = initAndReturn__copy__s_kt_matrix(&mat_result, true);
  //! Adjust(1): get max.
  t_float score_max = T_FLOAT_MIN_ABS;
  for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
      const t_float score = mat_cpy.matrix[row_id][col_id];
      if(isOf_interest(score)) {
	score_max = macro_max(score_max, score);
      }
    }
  }
  if( (score_max != T_FLOAT_MIN_ABS) && (score_max != 0) ) {
    //! Adjust(1):
    t_float score_max_inv = 1/score_max;
    for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
	const t_float score = mat_cpy.matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  mat_cpy.matrix[row_id][col_id] *= score_max_inv;
	}
      }
    }
    //! Export:
    char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_img.ppm", file_pref);
    //	    const char *file_name_result_img = "tmp_img.ppm";
    //     fprintf(stderr, "(info:export)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
    ppm_image_t *picture = exportTo_image ( mat_cpy.matrix, mat_cpy.nrows, mat_cpy.ncols, mat_cpy.nameOf_rows, mat_cpy.nameOf_columns);
    ppm_image_write ( file_name_result, picture );
    ppm_image_finalize ( picture );
    /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL); */
    /* assert(is_ok); */
    //! De-allocate:
  }
  free__s_kt_matrix(&mat_cpy);
}


  static const uint arrOf_tut3_CCMs_size = 13;
  //static const uint arrOf_tut3_CCMs_size = 1;
static e_kt_matrix_cmpCluster_clusterDistance__cmpType_t arrOf_tut3_CCMs[arrOf_tut3_CCMs_size] = {
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared,
};


// static bool global_config__

#define __MiF__isTo_evaluate__local(sim_id) (!isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id))

//! DEscribe the type fo simalrity-emtric-applcaiotn ot be used during the comptautiosn
typedef enum e_tut_kd_3__simAdjustToChoose {
  e_tut_kd_3__simAdjustToChoose_before,
  e_tut_kd_3__simAdjustToChoose_during,
  e_tut_kd_3__simAdjustToChoose_undef
} e_tut_kd_3__simAdjustToChoose_t;

typedef struct s_tut_kd_3 {
  const char *tag;
  e_hpLysis_clusterAlg clusterAlg;
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccmPost_matrix;
  e_kt_matrix_cmpCluster_metric_t ccmPost_gold;
  e_kd_tree_searchStrategy_t enum_kdTree;
  e_tut_kd_3__simAdjustToChoose_t corrMetric_prior_use;    
} s_tut_kd_3_t;

//! @reutnra na object with defualt cofniguraitons.
s_tut_kd_3_t init__s_tut_kd_3_t() {
  s_tut_kd_3_t self;
  self.tag = NULL;
  self.clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
  self.ccmPost_matrix = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
  self.ccmPost_gold = e_kt_matrix_cmpCluster_metric_randsIndex;
  self.enum_kdTree = e_kd_tree_searchStrategy_n_nearest;
  self.corrMetric_prior_use = e_tut_kd_3__simAdjustToChoose_during;
  ///!
  ///! @return
  return self;
}
typedef enum e_tut_kd_3_mat__typeOf_adjustment_inputIndex {
  e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall,
  e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef
} e_tut_kd_3_mat__typeOf_adjustment_inputIndex_t;

//! A wrapper to write/hodl teh amtrix-results:
typedef struct s_tut_kd_3_mat {
  const char *tag;
  e_tut_kd_3_mat__typeOf_adjustment_inputIndex_t typeOfIndexAdjustment_row_id;
  e_tut_kd_3_mat__typeOf_adjustment_inputIndex_t typeOfIndexAdjustment_col_id;
  s_kt_matrix_t mat_result_ccmMatrix;  s_kt_matrix_t mat_result_ccmGold; //! ie, to identify the best-fit wrt. simliarty-metircs
  s_kt_matrix_t mat_result_simId;  
} s_tut_kd_3_mat_t;

//! Write out the result and then de-allcoate the object:
void s_tut_kd_3_mat_t__writeOut__free(s_tut_kd_3_mat_t *self, const char *resultPrefix_local, s_tut_kd_3_t objConf, const uint isTo_normalize) {
  assert(self);
  assert(self->tag); assert(strlen(self->tag)); //! where latter is used to give the object a unique idneitfy.
#define __writeOut_matrix__summarySets(obj_mat, obj_mat_tag) ({ char fileName_result[1000]; memset(fileName_result, '\0', 1000); sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, objConf.corrMetric_prior_use); export__singleCall__s_kt_matrix_t(&(obj_mat), fileName_result, NULL); free__s_kt_matrix(&(obj_mat)); })
  //! -------------------------  
  __writeOut_matrix__summarySets((self->mat_result_ccmMatrix), "ccmMatrix_ccm");
  __writeOut_matrix__summarySets((self->mat_result_ccmGold), "ccmGold_ccm");
  __writeOut_matrix__summarySets((self->mat_result_simId), "simId");
  //! -------------------------
#undef __writeOut_matrix__summarySets
}

static void s_tut_kd_3_mat_t__setValue__max(s_tut_kd_3_mat_t *self, uint row_id,  uint col_id, const t_float ccmScore_mat, const t_float ccmScore_gold, const uint aux_id) {
  assert(row_id != UINT_MAX);
  assert(col_id != UINT_MAX);
  // --------
  if(self->typeOfIndexAdjustment_row_id != e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef) { //! then we chang ethe index:
    if(self->typeOfIndexAdjustment_row_id == e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall) { //! then we chang ethe index:
      s_kt_correlationMetric_t obj_metric = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)row_id, e_kt_categoryOf_correaltionPreStep_none);
      row_id = getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(obj_metric);
    } else {assert(false);} //! ie, as we then nee supprot ting
  }
  if(self->typeOfIndexAdjustment_col_id != e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef) { //! then we chang ethe index:
    if(self->typeOfIndexAdjustment_col_id == e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall) { //! then we chang ethe index:
      s_kt_correlationMetric_t obj_metric = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)col_id, e_kt_categoryOf_correaltionPreStep_none);
      col_id = getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(obj_metric);
    } else {assert(false);} //! ie, as we then nee supprot ting
  }
  // --------
  assert(self->mat_result_ccmMatrix.ncols);
  assert(self->mat_result_ccmGold.ncols);
  assert(self->mat_result_simId.nrows);
  assert(row_id < self->mat_result_ccmMatrix.nrows);
  assert(row_id < self->mat_result_ccmGold.nrows);
  assert(row_id < self->mat_result_simId.nrows);
  assert(col_id < self->mat_result_ccmMatrix.ncols);
  assert(col_id < self->mat_result_ccmGold.ncols);
  assert(col_id < self->mat_result_simId.ncols);
  bool ccmScore_isImproved = false;
  {
    //! Get the CCM-score:
    //  printf("[simId=%u][ccm=\"%s\"], ccm_score=%f, at %s:%d\n", sim_id, getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm), ccm_score,  __FILE__, __LINE__);
    if(ccmScore_mat != T_FLOAT_MAX) {
      if( (ccmScore_mat > self->mat_result_ccmMatrix.matrix[row_id][col_id]) || (self->mat_result_ccmMatrix.matrix[row_id][col_id] == T_FLOAT_MAX) ) { //! then we udpate the CCM-score:
	assert(self->mat_result_ccmMatrix.nrows);
	assert(col_id < self->mat_result_ccmMatrix.ncols);
	assert(row_id < self->mat_result_ccmMatrix.nrows);
	//!
	//! Set the score:
	self->mat_result_ccmMatrix.matrix[row_id][col_id] = ccmScore_mat;
	self->mat_result_simId.matrix[row_id][col_id] = aux_id;
	// ---
	ccmScore_isImproved = true;
      }
    }
  }
  if(ccmScore_isImproved) {
    //!
    //! Set the score:
    self->mat_result_ccmGold.matrix[row_id][col_id] = ccmScore_gold;
  }
}

//! Intiaite [sim-id][ccm-id]
static s_tut_kd_3_mat_t init__s_tut_kd_3_mat_t__SimCCM(const char *tag, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames) {
  assert(tag); assert(strlen(tag)); //! where latter is used to give the object a unique idneitfy.
  s_tut_kd_3_mat_t self; self.tag = tag;
  self.typeOfIndexAdjustment_row_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall;
  self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //!
  //! Itniate result-matrices:
  uint cnt_simMetrics = 0; for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
  self.mat_result_ccmMatrix = initAndReturn__s_kt_matrix(cnt_simMetrics, arrOf_tut3_CCMs_size);
  self.mat_result_ccmGold   = initAndReturn__s_kt_matrix(cnt_simMetrics, arrOf_tut3_CCMs_size);
  self.mat_result_simId     = initAndReturn__s_kt_matrix(cnt_simMetrics, arrOf_tut3_CCMs_size);
  //! Set column-headers:
  for(uint ccm_id = 0; ccm_id < arrOf_tut3_CCMs_size; ccm_id++) {
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_tut3_CCMs[ccm_id];
    assert(ccm_id < self.mat_result_simId.ncols);
    const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm);
    const char *stringOf_tagSample = str;
  /* for(uint data_id = 0; data_id < mat_input_size; data_id++) {	 */
  /*   char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id); */
  /*   const char *stringOf_tagSample = stringOf_tagSample_local;  */
  /*   if(arrOf_stringNames) { */
  /*     const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id); */
  /*     if(str && strlen(str)) {stringOf_tagSample = str;} /\*! ie, then use the user-providced data-description to 'set' the string.*\/ */
  /*   } */
  /*   assert(stringOf_tagSample); assert(strlen(stringOf_tagSample)); */
    //! 
    //! Set the row-name of the summary-containers:
#define __setRowName(obj) ({set_stringConst__s_kt_matrix(&obj, ccm_id, stringOf_tagSample, /*addFor_column=*/true);})
    __setRowName((self.mat_result_ccmMatrix));
    __setRowName((self.mat_result_ccmGold));
    __setRowName((self.mat_result_simId));
#undef __setRowName
  }
  //! Set the strings: columns:
  uint index_local = 0;    
  for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
    if(__MiF__isTo_evaluate__local(sim_id)) {
      const char *str = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)sim_id);
      assert(str); assert(strlen(str));
      assert(index_local < self.mat_result_ccmGold.nrows);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmMatrix, index_local, str, /*addFor_column=*/false);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmGold, index_local, str, /*addFor_column=*/false);
      set_stringConst__s_kt_matrix(&self.mat_result_simId, index_local, str, /*addFor_column=*/false);
      //! Increment:
      index_local++;
    }
  }  
  //!
  //! @return
  return self;
}

//! Intiaite [data-id][ccm-id]
static s_tut_kd_3_mat_t init__s_tut_kd_3_mat_t__DataCCM(const char *tag, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames) {
  assert(tag); assert(strlen(tag)); //! where latter is used to give the object a unique idneitfy.
  s_tut_kd_3_mat_t self; self.tag = tag;
  self.typeOfIndexAdjustment_row_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall;
  self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //!
  //! Itniate result-matrices:
  uint cnt_simMetrics = arrOf_tut3_CCMs_size; //for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
  self.mat_result_ccmMatrix = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  self.mat_result_ccmGold   = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  self.mat_result_simId     = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  //! Set column-headers:
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
    //! 
    //! Set the row-name of the summary-containers:
#define __setRowName(obj) ({set_stringConst__s_kt_matrix(&obj, data_id, stringOf_tagSample, /*addFor_column=*/false);})
    __setRowName((self.mat_result_ccmMatrix));
    __setRowName((self.mat_result_ccmGold));
    __setRowName((self.mat_result_simId));
#undef __setRowName
  }
  //! Set the strings: columns:
  uint index_local = 0;    
  for(uint ccm_id = 0; ccm_id < arrOf_tut3_CCMs_size; ccm_id++) {
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_tut3_CCMs[ccm_id];
    
  /* for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {   */
  /*   if(__MiF__isTo_evaluate__local(sim_id))  */
    {
      const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm);
      //const char *str = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)sim_id);
      assert(str); assert(strlen(str));
      assert(index_local < self.mat_result_ccmGold.ncols);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmMatrix, index_local, str, /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmGold, index_local, str, /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&self.mat_result_simId, index_local, str, /*addFor_column=*/true);
      //! Increment:
      index_local++;
    }
  }  
  //!
  //! @return
  return self;
}

//! Intiaite [data-id][sim-id-category]
static s_tut_kd_3_mat_t init__s_tut_kd_3_mat_t__DataSimCategories(const char *tag, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames) {
  assert(tag); assert(strlen(tag)); //! where latter is used to give the object a unique idneitfy.
  s_tut_kd_3_mat_t self; self.tag = tag;
  self.typeOfIndexAdjustment_row_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall;
  //  self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //!
  //! Itniate result-matrices:
  const uint cnt_simMetrics = e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; //0; for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
  self.mat_result_ccmMatrix = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  self.mat_result_ccmGold   = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  self.mat_result_simId     = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  //! Set column-headers:
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
    //! 
    //! Set the row-name of the summary-containers:
#define __setRowName(obj) ({set_stringConst__s_kt_matrix(&obj, data_id, stringOf_tagSample, /*addFor_column=*/false);})
    __setRowName((self.mat_result_ccmMatrix));
    __setRowName((self.mat_result_ccmGold));
    __setRowName((self.mat_result_simId));
#undef __setRowName
  }
  //! Set the strings: columns:
  uint index_local = 0;    
  for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; sim_id++) {  
    if(__MiF__isTo_evaluate__local(sim_id)) {
      const char *str = getStringOf__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_id);
      assert(str); assert(strlen(str));
      assert(index_local < self.mat_result_ccmGold.ncols);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmMatrix, index_local, str, /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmGold, index_local, str, /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&self.mat_result_simId, index_local, str, /*addFor_column=*/true);
      //! Increment:
      index_local++;
    }
  }  
  //!
  //! @return
  return self;
}


//! Intiaite [data-id][sim-id]
static s_tut_kd_3_mat_t init__s_tut_kd_3_mat_t__DataSim(const char *tag, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames) {
  assert(tag); assert(strlen(tag)); //! where latter is used to give the object a unique idneitfy.
  s_tut_kd_3_mat_t self; self.tag = tag;
  self.typeOfIndexAdjustment_row_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_simMetric_categorySmall;
  self.typeOfIndexAdjustment_col_id = e_tut_kd_3_mat__typeOf_adjustment_inputIndex_undef;
  //!
  //! Itniate result-matrices:
  uint cnt_simMetrics = 0; for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
  self.mat_result_ccmMatrix = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  self.mat_result_ccmGold   = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  self.mat_result_simId     = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
  //! Set column-headers:
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
    //! 
    //! Set the row-name of the summary-containers:
#define __setRowName(obj) ({set_stringConst__s_kt_matrix(&obj, data_id, stringOf_tagSample, /*addFor_column=*/false);})
    __setRowName((self.mat_result_ccmMatrix));
    __setRowName((self.mat_result_ccmGold));
    __setRowName((self.mat_result_simId));
#undef __setRowName
  }
  //! Set the strings: columns:
  uint index_local = 0;    
  for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
    if(__MiF__isTo_evaluate__local(sim_id)) {
      const char *str = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)sim_id);
      assert(str); assert(strlen(str));
      assert(index_local < self.mat_result_ccmGold.ncols);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmMatrix, index_local, str, /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&self.mat_result_ccmGold, index_local, str, /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&self.mat_result_simId, index_local, str, /*addFor_column=*/true);
      //! Increment:
      index_local++;
    }
  }  
  //!
  //! @return
  return self;
}



//! Evaluate for a given data-set.
//! @return the results of the measurements wrt. the CCM-exec-time.
static void  __tut_kd_3__dataset(s_tut_kd_3_t self, s_kt_matrix_t mat_input, s_kt_list_1d_uint_t *obj_gold, 
				 s_tut_kd_3_mat_t *objData__DataSim,
				 s_tut_kd_3_mat_t *objData__DataSimCatSim, s_tut_kd_3_mat_t *objData__DataCCM, s_tut_kd_3_mat_t *objData__SimCCM,
				 //s_kt_matrix_t *mat_result_ccmMatrix, s_kt_matrix_t *mat_result_simId, s_kt_matrix_t *mat_result_ccmGold, 
				 const uint data_id) {
  //  assert(mat_input); 
  assert(mat_input.nrows > 0);
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree;
  const e_hpLysis_clusterAlg clusterAlg = self.clusterAlg; //e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;

  // IFXME[tut+eval]: write a permtatuion of [”elow] where we isntead iterate through a set of "e_kt_matrix_cmpCluster_metric_t" .... then select the clsuter with best CCM-score .... where mtoviaton/novelty conserns how we ......??...

  if(obj_gold) {
    assert(obj_gold->list_size == mat_input.nrows); //! ie, for cosnistency.
  }


  uint index_local = 0;    
  for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
    if(__MiF__isTo_evaluate__local(sim_id)) {
      //      if(sim_id < 6) {continue;} // FIXME: remove.

      //! 
      //! Idneityf the CCM-id which maizmie the CCM-score
      t_float CCM_scoreMax = T_FLOAT_MIN_ABS; //bool CCM_scoreMax_isSet = false;
      for(uint ccm_id = 0; ccm_id < arrOf_tut3_CCMs_size; ccm_id++) {
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_tut3_CCMs[ccm_id];
	//if(true) {continue;} // FIXME: remove
	  
	//!
	//! Handle 'non-set-valeus':
	//!
	//! Allocate object:
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
	obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
	obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
	obj_hp.config.corrMetric_prior_use = false; //! ie, a default configuraiton.
	if(self.corrMetric_prior_use != e_tut_kd_3__simAdjustToChoose_during) {
	  // printf("simId=%u, at %s:%d\n", sim_id, __FILE__, __LINE__);
	  obj_hp.config.corrMetric_prior.metric_id = (e_kt_correlationFunction_t)sim_id;
	  obj_hp.config.corrMetric_prior_use = true;
	}
	if(self.corrMetric_prior_use != e_tut_kd_3__simAdjustToChoose_before) {
	  // printf("simId=%u, at %s:%d\n", sim_id, __FILE__, __LINE__);
	  obj_hp.config.corrMetric_insideClustering = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none); //(e_kt_correlationFunction_t)sim_id;
	}
	//	if(true)  	  {

	  //! 
	  //! Apply logics:
	  // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
    if(false) {
      const s_kt_matrix_t *obj_1 = &mat_input;
      for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
	uint cnt_interesting = 0;
	for(uint col_id = 0; col_id < obj_1->ncols; col_id++) {      
	  if(isOf_interest(obj_1->matrix[row_id][col_id])) {
	    printf("%f\t", obj_1->matrix[row_id][col_id]); //cpy_ranksEachRow.matrix[row_id][col_id]);
	  }
	}
	printf("\n");
      }
    }
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_input, /*nclusters=*/UINT_MAX, /*npass=*/100);
	  assert(is_ok);
	  // }
	  //	if(false) 
	  
	  t_float ccm_score_matrix = T_FLOAT_MAX;
	  if(obj_hp.config.corrMetric_prior_use == true) { //! then a simarlity-metirc has been computed.
	    ccm_score_matrix = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, self.ccmPost_matrix, NULL, NULL);
	  } else {
	    if( (obj_hp.obj_result_kMean.cnt_vertex > 0) && (obj_hp.obj_result_kMean.cnt_vertex != UINT_MAX) ) {
	      uint *map_clusterMembers = obj_hp.obj_result_kMean.vertex_clusterId;
	      s_kt_matrix_base_t vec_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_input);
	      const bool is_ok = ccm__singleMatrix__applyDefSimMetricBefore__hp_ccm(/*ccm_enum*/self.ccmPost_matrix, &vec_2, map_clusterMembers, &ccm_score_matrix);
	      assert(is_ok);
	    }
	  }
	  t_float ccm_score_gold = T_FLOAT_MAX;
	  if(obj_gold && obj_gold->list_size && (ccm_score_matrix != T_FLOAT_MAX) ) { // && ccmScore_isImproved) {
	    //! Get the CCM-score:	  
	    uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
	    uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;	  
	    if(cnt_vertex > 0) {
	      assert(vertex_clusterId);
	      cnt_vertex = macro_min(cnt_vertex, obj_gold->list_size);
	      assert(cnt_vertex > 0);
	      t_float ccm_score = 0; //scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, self.ccmPost_gold, NULL, NULL);
	      const bool is_ok = ccm__twoHhypoThesis__xmtMatrixBased__hp_ccm(self.ccmPost_gold, vertex_clusterId, obj_gold->list, cnt_vertex, &ccm_score);
	      if(is_ok) {
		//assert(mat_result_ccmGold);
		/* assert(index_local < mat_result_ccmGold->ncols); */
		/* assert(data_id < mat_result_ccmGold->nrows); */
		ccm_score_gold = ccm_score;
	      }
	    }
	    //	  }
	  } 
	  //!
	  //! Add the scores:
	  if(ccm_score_matrix != T_FLOAT_MAX) {
	    printf("[simId=%u][ccm=\"%s\"], ccm_score=%f, at %s:%d\n", sim_id, getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm), ccm_score_matrix,  __FILE__, __LINE__);
	    s_tut_kd_3_mat_t__setValue__max(objData__DataSim, /*row_id=*/data_id, /*row_id=*/sim_id, ccm_score_matrix, ccm_score_gold, /*aux_id=*/ccm_id);
	  }
	  //assert(false);
	  if(objData__DataSimCatSim != NULL) {
	    s_tut_kd_3_mat_t__setValue__max(objData__DataSimCatSim, /*row_id=*/data_id, /*row_id=*/sim_id, ccm_score_matrix, ccm_score_gold, /*aux_id=*/ccm_id);
	  }
	  if(objData__DataCCM != NULL) {
	    s_tut_kd_3_mat_t__setValue__max(objData__DataCCM, /*row_id=*/data_id, /*row_id=*/ccm_id, ccm_score_matrix, ccm_score_gold, /*aux_id=*/sim_id);
	  }
	  if(objData__SimCCM != NULL) {
	    s_tut_kd_3_mat_t__setValue__max(objData__SimCCM, /*row_id=*/sim_id, /*row_id=*/ccm_id, ccm_score_matrix, ccm_score_gold, /*aux_id=*/data_id);
	  }
	/* //! */
	/* //! Get the result-aprameters 'of our itnerest': */
	/* scalarResult_ncluster = obj_hp.dynamicKMeans__best__nClusters; */
	/* scalarResult_ccmScore = obj_hp.dynamicKMeans__best__score; */
	//! 
	//! De-allcoate object, and return:
	free__s_hpLysis_api_t(&obj_hp);
	//!
	//! Increment:
      }      
      index_local++;
    } //! else the simlairty-metirc is Not of interest.
  }
}

/**
   @brief capture the STD of STDs in a givne data-set.
   @return a score cpaturing the variance in the data-set
 **/
static t_float __tut_kd_3__getSummaryOf_matrix__STDEachRow(const s_kt_matrix_t mat_input, const bool inRowMerge_useAVG_insteadOfSTD) {
  s_kt_list_1d_float_t obj_scores = init__s_kt_list_1d_float_t(mat_input.nrows);
  { //! Comptue the sum of scores sperately for each input-row:
    for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
      const t_float score = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(mat_input.matrix[row_id], mat_input.ncols);
      obj_scores.list[row_id] = score;
    }
  }


  {
    t_float sumOf_scores = 0;
    if(inRowMerge_useAVG_insteadOfSTD) {
      //! 
      //! @return sum
      uint cnt_eval = 0;
      for(uint i = 0; i < obj_scores.list_size; i++) {
	const t_float score = obj_scores.list[i];
	if(isOf_interest(score)) {cnt_eval++; sumOf_scores += mathLib_float_abs(score);}
      }
      if( (cnt_eval > 0) && (sumOf_scores != 0) ) {
	sumOf_scores = sumOf_scores / (t_float)cnt_eval;
      } else {sumOf_scores = T_FLOAT_MAX;}
      
    } else {
      sumOf_scores = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(obj_scores.list, obj_scores.list_size);
    }
    //! 
    //! De-allocate: 
    free__s_kt_list_1d_float_t(&obj_scores);

    //!
    //! @return
    return sumOf_scores;
  }

  return T_FLOAT_MAX;
}

/**
   @brief Computes result-predicitosn for a collection of data-sets
   @remarks 
   .. evalaute for both kd-tree and brute-appraoch to comapre implciaotn-effect of kd-tree-data-set
   .. 
   .. 
**/
static void tut_kd_3__fromMatrix__fixedCCM(const s_kt_matrix_t *mat_input, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix_local, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccmPost_matrix_duringIterative) {
  //! 
  { //! Write out generlid proeprties of the input-data-set: 
    //! Note: constrct new table ... [real-data-input][name, count(rows), count(column), avg(row(STD)), STD(row(STD)), avg(col(STD)), STD(col(STD))] .... 
  //!
  //! Open result-file-pointer: null-hypothesis: 
    char result_file[1000]; memset(result_file, '\0', 1000); sprintf(result_file, "%s_%s", resultPrefix_local, "inputData_summaryProp.tsv");
    fprintf(stderr, "(info)\t Generates result-file \"%s\", at %s:%d\n", result_file, __FILE__, __LINE__);
    FILE *fPointer = fopen(result_file, "wb");
    assert(fPointer);
    //! 
    //! ITerate through each data-chunk: 
    for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
      s_kt_matrix_t mat_data = mat_input[data_id]; //setToEmptyAndReturn__s_kt_matrix_t();
      if(mat_data.nrows > 0) {
	assert(mat_data.nrows > 0); 
	assert(mat_data.ncols > 0); 
	//! Get variance: 
	t_float STD_STD = __tut_kd_3__getSummaryOf_matrix__STDEachRow(mat_data, /*inRowMerge_useAVG_insteadOfSTD=*/false); 
	t_float STD_avg = __tut_kd_3__getSummaryOf_matrix__STDEachRow(mat_data, /*inRowMerge_useAVG_insteadOfSTD=*/true); 
	//! Transpose and get the proeprties
	s_kt_matrix_t mat_tran = initAndReturn_transpos__s_kt_matrix(&mat_data);
	t_float t_STD_STD = __tut_kd_3__getSummaryOf_matrix__STDEachRow(mat_tran, /*inRowMerge_useAVG_insteadOfSTD=*/false); 
	t_float t_STD_avg = __tut_kd_3__getSummaryOf_matrix__STDEachRow(mat_tran, /*inRowMerge_useAVG_insteadOfSTD=*/true); 
	free__s_kt_matrix(&mat_tran); 
	assert(mat_data.nrows > 0); //! ie as we expect latter to be unchanded.
	//! 
	if(data_id == 0) { //! Write out: row-header:
	  fprintf(fPointer, "#! Row-name "
		  "\t count(rows) \t count(cols)"
		  "\t avg(row(STD)) \t STD(row(STD)) "
		  "\t t:avg(row(STD)) \t t:STD(row(STD)) "
		  "\n"
		  );
	}

	//! 
	//! Write out: row-name
	const char *tag = (arrOf_stringNames && data_id < arrOf_stringNames->nrows) ? arrOf_stringNames->nameOf_rows[data_id] : NULL;
	if(tag && strlen(tag) ) {
	  fprintf(fPointer, "%s", tag);
	} else { fprintf(fPointer, "%u", data_id); }
	//! 
	//! Write out: valeus:
	fprintf(fPointer, 
		"\t %u\t %u" //! "\t count(rows) \t count(cols)"
		"\t %f\t %f" //avg(row(STD)) \t STD(row(STD)) "
		"\t %f\t %f" // t:avg(row(STD)) \t t:STD(row(STD)) "
		"\n",
		mat_data.nrows, mat_data.ncols, 
		STD_avg, STD_STD, 
		t_STD_avg, t_STD_STD
		);
      }
    }
    //! Then close the reuslt-file-pointer
    assert(fPointer); fclose(fPointer);
  }

  const uint arrOf_eval_size = 5; 
  const s_tut_kd_3_t arrOf_eval[arrOf_eval_size] = {
    {/*tag=*/"dbScanBruteCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    {/*tag=*/"dbScanBruteCCMsSilRandSimBefore", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_before},
    // --- 
    // --- ------------------------------
    {/*tag=*/"dbScanKdCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    {/*tag=*/"dbScanKdCCMsSilRandSimBefore", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_before},
    // ---
    {/*tag=*/"hcaCCMsDunnsRandDuring", /*alg=*/e_hpLysis_clusterAlg_HCA_single, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
  };

  //!
  //! Itniate result-matrices:
  uint cnt_simMetrics = 0; for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
  //! 
  //! Iterate through each of the cases:
  for(uint eval_id = 0; eval_id < arrOf_eval_size; eval_id++) {
    //! 
    s_kt_matrix_t objData__dataSim_min = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
    s_kt_matrix_t objData__dataSim_max = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
    s_kt_matrix_t objData__time_min = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
    s_kt_matrix_t objData__time_max = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
    //! 
    { //! 
      uint sim_id_local = 0;
      for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
	if(__MiF__isTo_evaluate__local(sim_id)) { //! the compute simlairty: 
	  const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
	  //const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm);
	  const char *stringOf_tagSample = get_stringOf_enum__e_kt_correlationFunction_t(metric_id);
	  set_stringConst__s_kt_matrix(&objData__dataSim_min, sim_id_local, stringOf_tagSample, /*addFor_column=*/true);
	  set_stringConst__s_kt_matrix(&objData__dataSim_max, sim_id_local, stringOf_tagSample, /*addFor_column=*/true);
	  set_stringConst__s_kt_matrix(&objData__time_min, sim_id_local, stringOf_tagSample, /*addFor_column=*/true);
	  set_stringConst__s_kt_matrix(&objData__time_max, sim_id_local, stringOf_tagSample, /*addFor_column=*/true);
	  sim_id_local++;
	}
      }
      if(arrOf_stringNames && arrOf_stringNames->nrows) {
	assert(arrOf_stringNames->nrows >= mat_input_size);
	for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
	  const char *stringOf_tagSample = arrOf_stringNames->nameOf_rows[data_id];
	  if(stringOf_tagSample && strlen(stringOf_tagSample)) {
	    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
	    set_stringConst__s_kt_matrix(&objData__dataSim_min, data_id, stringOf_tagSample, /*addFor_column=*/false);
	    set_stringConst__s_kt_matrix(&objData__dataSim_max, data_id, stringOf_tagSample, /*addFor_column=*/false);
	    set_stringConst__s_kt_matrix(&objData__time_min, data_id, stringOf_tagSample, /*addFor_column=*/false);
	    set_stringConst__s_kt_matrix(&objData__time_max, data_id, stringOf_tagSample, /*addFor_column=*/false);
	  }
	}
      }
    }
    s_tut_kd_3_t self = arrOf_eval[eval_id];
    //!
    //! Iterate: 
    for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
      // s_kt_list_1d_uint_t obj_gold = setToEmpty__s_kt_list_1d_uint_t();
      s_kt_matrix_t mat_data = mat_input[data_id]; //setToEmptyAndReturn__s_kt_matrix_t();
      if(mat_data.nrows == 0) {continue;}
      assert(mat_data.nrows > 0); 
      assert(mat_data.ncols > 0); 
      printf("[%u]\t start-clustering for matrix=[%u, %u]: at %s:%d\n", data_id, mat_data.nrows, mat_data.ncols, __FILE__, __LINE__);
      for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
	if(__MiF__isTo_evaluate__local(sim_id)) { //! the compute simlairty: 
	  const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
	  s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/metric_id, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"      
	  char fileName_result_prefix[1000];
	  { //! Set the file-name-preifx:
	    const char *tag = (arrOf_stringNames && data_id < arrOf_stringNames->nrows) ? arrOf_stringNames->nameOf_rows[data_id] : NULL;
	    if(tag && strlen(tag) ) {
	      sprintf(fileName_result_prefix, "%s_objTag_%s_dataIs_%s_simId_%s", resultPrefix_local, self.tag, tag, get_stringOf_enum__e_kt_correlationFunction_t(metric_id));
	    } else {
	      sprintf(fileName_result_prefix, "%s_objTag_%s_dataId_%u_simId_%s", resultPrefix_local, self.tag, data_id, get_stringOf_enum__e_kt_correlationFunction_t(metric_id));
	    }
	  }
	  { //! Apply simalirty-metric and then writ export into a *.ppm format: 
	    //	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	    s_hp_distance__config_t hp_config = init__s_hp_distance__config_t(); 
	    //!
	    //! Compute similairty: 
	    s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data);
	    s_kt_matrix_base_t vec_result = initAndReturn__empty__s_kt_matrix_base_t();
	    bool is_ok = apply__hp_distance(obj_metric, &vec_1, NULL,  &vec_result, /*config=*/hp_config);      
	    assert(is_ok);
	    //!
	    //! Export result: 
	    s_kt_matrix_t mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	    if(true) {
	      __tut_kd_3_manyMany_singleInput_exportTo_ppm(mat_result, /*file_pref=*/fileName_result_prefix);
	    } else {
	      fprintf(stderr, "(info)\t export to file=\"%s\", at %s:%d\n", fileName_result_prefix, __FILE__, __LINE__);
	      is_ok = export__singleCall__s_kt_matrix_t(&mat_result, fileName_result_prefix, NULL);
	      assert(is_ok);
	    }
	    //!
	    //! De-allocate:
	    free__s_kt_matrix_base_t(&vec_result);
	  } 
	  //!
	  //! Handle 'non-set-valeus':
	  //!
	  //! Allocate object:
	  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	  obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = ccmPost_matrix_duringIterative; //enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
	  obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
	  obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
	  obj_hp.config.corrMetric_prior_use = false; //! ie, a default configuraiton.
	  if(self.corrMetric_prior_use != e_tut_kd_3__simAdjustToChoose_during) {
	    //if(false) {
	    // printf("simId=%u, at %s:%d\n", sim_id, __FILE__, __LINE__);
	    obj_hp.config.corrMetric_prior.metric_id = (e_kt_correlationFunction_t)sim_id;
	    obj_hp.config.corrMetric_prior_use = true;
	  }
	  if(self.corrMetric_prior_use != e_tut_kd_3__simAdjustToChoose_before) {
	    //if(true) {
	    // printf("simId=%u, at %s:%d\n", sim_id, __FILE__, __LINE__);
	    obj_hp.config.corrMetric_insideClustering = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none); //(e_kt_correlationFunction_t)sim_id;
	  }
	  start_time_measurement();
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, self.clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
	  const char *str_local = "matrix-based"; 		const t_float time_s = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);		
	  assert(is_ok);
	  //! Compute CCM: 
	  t_float ccm_score_matrix = T_FLOAT_MAX;
	  if(obj_hp.config.corrMetric_prior_use == true) { //! then a simarlity-metirc has been computed.
	    ccm_score_matrix = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, self.ccmPost_matrix, NULL, NULL);
	  } else {
	    if( (obj_hp.obj_result_kMean.cnt_vertex > 0) && (obj_hp.obj_result_kMean.cnt_vertex != UINT_MAX) ) {
	      uint *map_clusterMembers = obj_hp.obj_result_kMean.vertex_clusterId;
	      s_kt_matrix_base_t vec_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data);
	      const bool is_ok = ccm__singleMatrix__applyDefSimMetricBefore__hp_ccm(/*ccm_enum*/self.ccmPost_matrix, &vec_2, map_clusterMembers, &ccm_score_matrix);
	      assert(is_ok);
	    }
	  }
	  //const t_float ccm_score_matrix = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, self.ccmPost_matrix, NULL, NULL);
	  if( isOf_interest(ccm_score_matrix) ) {	    
	    //! Update for: "min":
	    if(objData__dataSim_min.matrix[data_id][sim_id] != T_FLOAT_MAX) {
	      if(objData__dataSim_min.matrix[data_id][sim_id] > ccm_score_matrix) {
		objData__dataSim_min.matrix[data_id][sim_id] = ccm_score_matrix;
		objData__time_min.matrix[data_id][sim_id] = time_s;
	      }
	    } else {
	      objData__dataSim_min.matrix[data_id][sim_id] = ccm_score_matrix;
	      objData__time_min.matrix[data_id][sim_id] = time_s;
	    }	
	    //! Update for: "max":
	    if(objData__dataSim_max.matrix[data_id][sim_id] != T_FLOAT_MAX) {
	      if(objData__dataSim_max.matrix[data_id][sim_id] < ccm_score_matrix) {
		objData__dataSim_max.matrix[data_id][sim_id] = ccm_score_matrix;
		objData__time_max.matrix[data_id][sim_id] = time_s;
	      }
	    } else {
	      objData__dataSim_max.matrix[data_id][sim_id] = ccm_score_matrix;
	      objData__time_max.matrix[data_id][sim_id] = time_s;
	    }	
	  }
	}
      }
    }
    {
      //!
      //! Write out the result and then de-allcoate the object:
      char fileName_result[1000]; memset(fileName_result, '\0', 1000); 
      //    const bool isTo_normalize = false;
      sprintf(fileName_result, "%s_%s_dataToSim_min.tsv", resultPrefix_local, arrOf_eval[eval_id].tag);
      //sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, self.corrMetric_prior_use);
      //    sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, objConf.corrMetric_prior_use);
      export__singleCall__s_kt_matrix_t(&(objData__dataSim_min), fileName_result, NULL); 
      free__s_kt_matrix(&(objData__dataSim_min));
      //    s_tut_kd_3_mat_t__writeOut__free(&objData__dataSim, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
    }
    {
      //!
      //! Write out the result and then de-allcoate the object:
      char fileName_result[1000]; memset(fileName_result, '\0', 1000); 
      //    const bool isTo_normalize = false;
      sprintf(fileName_result, "%s_%s_dataToSim_max.tsv", resultPrefix_local, arrOf_eval[eval_id].tag);
      //sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, self.corrMetric_prior_use);
      //    sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, objConf.corrMetric_prior_use);
      export__singleCall__s_kt_matrix_t(&(objData__dataSim_max), fileName_result, NULL); 
      free__s_kt_matrix(&(objData__dataSim_max));
      //    s_tut_kd_3_mat_t__writeOut__free(&objData__dataSim, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
    }
    {
      //!
      //! Write out the result and then de-allcoate the object:
      char fileName_result[1000]; memset(fileName_result, '\0', 1000); 
      //    const bool isTo_normalize = false;
      sprintf(fileName_result, "%s_%s_time_min.tsv", resultPrefix_local, arrOf_eval[eval_id].tag);
      //sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, self.corrMetric_prior_use);
      //    sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, objConf.corrMetric_prior_use);
      export__singleCall__s_kt_matrix_t(&(objData__time_min), fileName_result, NULL); 
      free__s_kt_matrix(&(objData__time_min));
      //    s_tut_kd_3_mat_t__writeOut__free(&objData__dataSim, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
    }
    {
      //!
      //! Write out the result and then de-allcoate the object:
      char fileName_result[1000]; memset(fileName_result, '\0', 1000); 
      //    const bool isTo_normalize = false;
      sprintf(fileName_result, "%s_%s_time_max.tsv", resultPrefix_local, arrOf_eval[eval_id].tag);
      //sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, self.corrMetric_prior_use);
      //    sprintf(fileName_result, "%s_%s_%s_%s_norm_%u_simAppType%u.tsv", resultPrefix_local, self->tag, obj_mat_tag, objConf.tag, isTo_normalize, objConf.corrMetric_prior_use);
      export__singleCall__s_kt_matrix_t(&(objData__time_max), fileName_result, NULL); 
      free__s_kt_matrix(&(objData__time_max));
      //    s_tut_kd_3_mat_t__writeOut__free(&objData__dataSim, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
    }
  }
}

/**
   @brief Computes result-predicitosn for a collection of data-sets
   @remarks 
   .. evalaute for both kd-tree and brute-appraoch to comapre implciaotn-effect of kd-tree-data-set
   .. 
   .. 
**/
static void tut_kd_3__fromMatrix(const s_kt_matrix_t *mat_input, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix_local, const bool config_rowNameIdentifiesCluster) {
  const uint arrOf_eval_size = 2 + 3 + 3 + 4;
  const s_tut_kd_3_t arrOf_eval[arrOf_eval_size] = {
    {/*tag=*/"disjointCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_before},
    // {/*tag=*/"disjointCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    // ---------------- 
    {/*tag=*/"dbScanBruteCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    {/*tag=*/"dbScanBruteCCMsSilRandSimBefore", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_before},
    {/*tag=*/"dbScanBruteCCMsSilRandSimBoth", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_undef},
    // ---- 
    {/*tag=*/"dbScanKdCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    {/*tag=*/"dbScanKdCCMsSilRandSimBefore", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_before},
    {/*tag=*/"dbScanKdCCMsSilRandSimBoth", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_undef},
    /* {/\*tag=*\/"dbScanDirectCCMsSilRand", /\*alg=*\/e_hpLysis_clusterAlg_disjoint, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/false}, */
    /* // ---  */
    /* {/\*tag=*\/"dbScanKdCCMsSilRand2", /\*alg=*\/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_n_nearest, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    /* {/\*tag=*\/"dbScanBruteCCMsSilRand2", /\*alg=*\/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    /* {/\*tag=*\/"dbScanDirectCCMsSilRand2", /\*alg=*\/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/false}, */
    /* // ---  */
    /* {/\*tag=*\/"dbScanKdCCMsDunnsRand", /\*alg=*\/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_n_nearest, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    /* {/\*tag=*\/"dbScanBruteCCMsDunnsRand", /\*alg=*\/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    /* {/\*tag=*\/"dbScanDirectCCMsDunnsRand", /\*alg=*\/e_hpLysis_clusterAlg_disjoint, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/false}, */
    /* // ---  */
    {/*tag=*/"hcaCCMsSilRandSimDuring", /*alg=*/e_hpLysis_clusterAlg_HCA_single, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    {/*tag=*/"hcaCCMsSilsRandSimBoth", /*alg=*/e_hpLysis_clusterAlg_HCA_single, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_undef},
    // ---
    {/*tag=*/"hcaCCMsDunnsRandDuring", /*alg=*/e_hpLysis_clusterAlg_HCA_single, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_during},
    {/*tag=*/"hcaCCMsDunnsRand", /*alg=*/e_hpLysis_clusterAlg_HCA_single, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/e_tut_kd_3__simAdjustToChoose_undef},
    /* {/\*tag=*\/"mclCCMsDunnsRand", /\*alg=*\/e_hpLysis_clusterAlg_disjoint_mclPostStrategy, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    /* {/\*tag=*\/"randomBbest", /\*alg=*\/e_hpLysis_clusterAlg_random_best, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    /* {/\*tag=*\/"randomWorst", /\*alg=*\/e_hpLysis_clusterAlg_random_worst, /\*ccmPost_matrix=*\/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /\*ccmPost_gold=*\/e_kt_matrix_cmpCluster_metric_randsIndex, /\*enum_kdTree=*\/e_kd_tree_searchStrategy_brute_fast, /\*use-corrMetric-prior-clustering=*\/e_tut_kd_3__simAdjustToChoose_during}, */
    // --- 
  };  

  printf("at %s:%d\n", __FILE__, __LINE__);

  //! 
  //! Evalaute for different algorithm-cases wrt. KD-tree: 
  for(uint eval_id = 0; eval_id < arrOf_eval_size; eval_id++) {
    //    fprintf(stderr, "eval_id=%u, at %s:%d\n", eval_id, __FILE__, __LINE__); 
    //! Note: we expect .... 
    //! allocate the result-data-sets .... seperately for "[data-id][sim-metric]" and "[data-id][internal-matrix-ccm]"
    //!
    //! Seprately ivnestigate ofr diferent nroamlziaiton-appraoches:     
    for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {
      if( (isTo_normalize != e_kt_normType_avgRelative_abs) && (isTo_normalize != e_kt_normType_undef) ) {continue;} //! ie, to reduc ethe nubmer of evlauated combiantionis: in oru "tut_norm_3_cluster_kd_dbScan_80DataSets.c" we focus on dat-anroamzion-effects, ie, for detials please see the latter. 
      //const uint isTo_normalize = e_kt_normType_undef; //! ie, do Not normalize.
      //    {
      //!
      //! Itniate result-matrices:
      s_tut_kd_3_mat_t objData__DataSim = init__s_tut_kd_3_mat_t__DataSim("dataSim", mat_input_size, arrOf_stringNames);
      s_tut_kd_3_mat_t objData__DataSimCatSim = init__s_tut_kd_3_mat_t__DataSimCategories("dataSimCatSmall", mat_input_size, arrOf_stringNames);
      s_tut_kd_3_mat_t objData__DataCCM = init__s_tut_kd_3_mat_t__DataCCM("dataCCM", mat_input_size, arrOf_stringNames);
      s_tut_kd_3_mat_t objData__SimCCM = init__s_tut_kd_3_mat_t__SimCCM("simCCM", mat_input_size, arrOf_stringNames);

      //! Note: we expect .... 
      //!
      //! Iterate: 
      for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
	s_kt_list_1d_uint_t obj_gold = setToEmpty__s_kt_list_1d_uint_t();
/* 	char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id); */
/* 	const char *stringOf_tagSample = stringOf_tagSample_local;  */
/* 	if(arrOf_stringNames) { */
/* 	  const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id); */
/* 	  if(str && strlen(str)) {stringOf_tagSample = str;} /\*! ie, then use the user-providced data-description to 'set' the string.*\/ */
/* 	} */
/* 	assert(stringOf_tagSample); assert(strlen(stringOf_tagSample)); */
/* 	//!  */
/* 	//! Set the row-name of the summary-containers: */
/* #define __setRowName(obj) ({set_stringConst__s_kt_matrix(&obj, data_id, stringOf_tagSample, /\*addFor_column=*\/false);}) */
/* 	__setRowName(mat_result_ccmMatrix); */
/* 	__setRowName(mat_result_ccmGold); */
/* 	__setRowName(mat_result_simId); */
/* #undef __setRowName */
	//!
	//! Construct gold-data-mapping:
	if(config_rowNameIdentifiesCluster == false) { //! then Construct gold-data-mapping .... use a MINE+HCA-pre-step: 
	  //!
	  //! Handle 'non-set-valeus':
	  //!
	  //! Allocate object:
	  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	  /* obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///\*enum_ccm=*\/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id; */
	  /* obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true; */
	  //obj_hp.config.kdConfig = self.enum_kdTree; //! ie, the defualt enum-kd-type
	  if(true) {
	    obj_hp.config.corrMetric_prior.metric_id = e_kt_correlationFunction_groupOf_MINE_mic;
	    obj_hp.config.corrMetric_prior_use = true;
	  }
	  //! 
	  //! Apply logics:
	  printf("[%u]\t (gold-cluster)\t start-clustering for matrix=[%u, %u]: at %s:%d\n", data_id, mat_input[data_id].nrows, mat_input[data_id].ncols, __FILE__, __LINE__);
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, /*clusterAlg*/e_hpLysis_clusterAlg_HCA_max, &(mat_input[data_id]), /*nclusters=*/UINT_MAX, /*npass=*/100);
	  assert(is_ok);
	  printf("[%u]\t (gold-cluster)\t ok: post-process for matrix=[%u, %u]: at %s:%d\n", data_id, mat_input[data_id].nrows, mat_input[data_id].ncols, __FILE__, __LINE__);
	  const uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
	  uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;	  
	  if(cnt_vertex > 0) {
	    obj_gold = init__s_kt_list_1d_uint_t(cnt_vertex);
	    memcpy(obj_gold.list, vertex_clusterId, sizeof(uint)*cnt_vertex);
	  }
	  //! 
	  //! De-allcoate object, and return:
	  free__s_hpLysis_api_t(&obj_hp);
	} else { //! then Construct gold-data-mapping .... supprot idneitiocnat of clsuter-dis based on gold-name-comparison ... 
	  //! 
	  //! Find the local index of each data-set:
	  const uint cnt_vertex = mat_input[data_id].nrows;
	  obj_gold = init__s_kt_list_1d_uint_t(cnt_vertex);
	  s_kt_list_1d_string_t map_clusterIds = setToEmpty_andReturn__s_kt_list_1d_string_t();
	  //! 
	  //! Find:
	  assert(mat_input[data_id].nameOf_rows);
	  for(uint i = 0; i < cnt_vertex; i++) {
	    const char *str_search = mat_input[data_id].nameOf_rows[i];
	    if(str_search && strlen(str_search)) {
	      const uint clsuter_id = get_indexOf_string_slowSearch__s_kt_list_1d_string_t(&map_clusterIds, str_search, /*insertIf_notFound=*/true);
	      assert(clsuter_id != UINT_MAX);
	      obj_gold.list[i] = clsuter_id; //! ie, set the clsuter-id
	    }
	  }
	  //! De-allocate:
	  free__s_kt_list_1d_string(&map_clusterIds);
	}


	//!
	//! Apply noramizaiotn, ie, if requested:
	s_kt_matrix_t mat_data = mat_input[data_id]; //setToEmptyAndReturn__s_kt_matrix_t();
	if(isTo_normalize != e_kt_normType_undef) {
	  mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&(mat_input[data_id]), (e_kt_normType_t)isTo_normalize);
	  //free__s_kt_matrix(&obj_matrixInput); //! ie, as we assuem [below] has 'perofmred' a deed copy-oepration.
	  //obj_matrixInput = mat_copy; //! ie, a sahholw copy.
	}
	//!
	//! Comptue teh clsuters
	printf("[%u]\t start-clustering for matrix=[%u, %u]: at %s:%d\n", data_id, mat_data.nrows, mat_data.ncols, __FILE__, __LINE__);
	__tut_kd_3__dataset(arrOf_eval[eval_id], mat_data, 
			    (obj_gold.list_size > 0) ? &obj_gold : NULL, 
			    &objData__DataSim,
			    &objData__DataSimCatSim, &objData__DataCCM, &objData__SimCCM,
			    //&mat_result_ccmMatrix, &mat_result_ccmGold, &mat_result_simId, 
			    data_id);      
	/* //! */
	/* //! De-allocate: */
	/* free__s_kt_matrix(&mat_result_ccmMatrix); */
	/* free__s_kt_matrix(&mat_result_ccmGold); */
	free__s_kt_list_1d_uint_t(&obj_gold);
	if(isTo_normalize != e_kt_normType_undef) {
	  assert(mat_data.matrix != mat_input[data_id].matrix); //! ie, to avoid a 'clearing' of the input-data-set-id.
	  free__s_kt_matrix(&mat_data);
	}	
	//free__s_kt_matrix(&obj_matrixInput);
      }
      //!
      //! Write out the result and then de-allcoate the object:
      s_tut_kd_3_mat_t__writeOut__free(&objData__DataSim, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
      s_tut_kd_3_mat_t__writeOut__free(&objData__DataSimCatSim, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
      s_tut_kd_3_mat_t__writeOut__free(&objData__DataCCM, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
      s_tut_kd_3_mat_t__writeOut__free(&objData__SimCCM, resultPrefix_local, arrOf_eval[eval_id], isTo_normalize);
      //!
      //!
    }
  }
}
//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t __static__readDataFromObject__kd__tut_3(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation, const bool isTo_transposeMatrix) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  fileRead_config.isTo_transposeMatrix = isTo_transposeMatrix;
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}



static void __eval__dataCollection__tut_3(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix, const bool rowName_identifiesClusterId, const bool isTo_transposeMatrix) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = __static__readDataFromObject__kd__tut_3(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false, isTo_transposeMatrix); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    const char *tag_suffix = strrchr(tag, '/');
    if(tag_suffix && strlen(tag_suffix)) {
      tag = tag_suffix + 1; //! ie, to avodi the fodler-speerator from clsuttienrg the results
    }
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
  }
  //!
  //! Apply logics: 
  { //! Comptue where gold is usedinfed through MINE+HCA:
    char resultPrefix_local[10000]; memset(resultPrefix_local, '\0', 10000); sprintf(resultPrefix_local, "%s_isToUseHCAMINE", resultPrefix);
    tut_kd_3__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix_local, /*config_rowNameIdentifiesCluster=*/false); //! ie, use HCA+MINE.
  }
  if(rowName_identifiesClusterId) { //! Comptue where gold is usedinfed through overallpgin row-names:
    tut_kd_3__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix, /*config_rowNameIdentifiesCluster=*/true);
  } 
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}

//! A funciton which first merge two dat-asets, and thereafter call our "tut_kd_3__fromMatrix__fixedCCM(..)" 'simpled' object-ufnciotn.
static void __eval__dataCollection__tut_3__fixedCCM(const s_hp_clusterFileCollection_t *mapOf_realLife_1, const uint mapOf_realLife_size_1, 
						    const s_hp_clusterFileCollection_t *mapOf_realLife_2, const uint mapOf_realLife_size_2, 
						    const s_hp_clusterFileCollection_t *mapOf_realLife_3, const uint mapOf_realLife_size_3, 
						    const char *resultPrefix, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccmPost_matrix_duringIterative,
						    const bool isTo_includeTransposed
						    ) {
  //  assert(mapOf_realLife_1); assert(mapOf_realLife_size_1 > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  uint mapOf_realLife_size = mapOf_realLife_size_1 + mapOf_realLife_size_2 + mapOf_realLife_size_3;  
  //  if(isTo_includeTransposed) {mapOf_realLife_size *= 2;}
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  uint data_id_local = 0;
  const s_hp_clusterFileCollection_t *mapOf_realLife = mapOf_realLife_1; uint obj_currentSize = mapOf_realLife_size_1; uint dataSetEvalId = 0; 
  uint insert_index = 0;
  //! 
  //! Cosntruct a mappign-file to hold the feautre-count and row-coutn for each row, eg, to be used in psot-processing oto evlauat ehte sigicnace of feuatre-counts for KD-tree (K-D tree) clsuter-algorithm-optmzioant-strateiges
  FILE *fileP_meta = NULL;
  { //! (eosekth, 06. otk. 2017).
    assert(resultPrefix); assert(strlen(resultPrefix)); assert(strlen(resultPrefix) < 2000); 
    char file_name[2000]; memset(file_name, '\0', 2000);
    sprintf(file_name, "%s_metaProp.tsv", resultPrefix);
    fileP_meta = fopen(file_name, "wb");
    assert(fileP_meta);
    fprintf(fileP_meta, "#! File-tag\t cnt-rows\t cnt-rows\n");
  }

  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    if(data_id_local >= obj_currentSize) { //! htne we 'jump' to the next data-id.
      if(dataSetEvalId == 0) {
	mapOf_realLife = mapOf_realLife_2;
	data_id_local = 0;
	obj_currentSize = mapOf_realLife_size_2;
      } else if(dataSetEvalId == 1) {
	mapOf_realLife = mapOf_realLife_3;
	data_id_local = 0;
	obj_currentSize = mapOf_realLife_size_3;
      } else {assert(false);} //! ie, as we then nee dadding supprot for this
      //! Increrment
      dataSetEvalId++; 
    }
    if( (data_id_local > 0) && mapOf_realLife[data_id_local].tag && mapOf_realLife[data_id_local-1].tag && 
	(strlen(mapOf_realLife[data_id_local].tag) == strlen(mapOf_realLife[data_id_local-1].tag) ) &&
	(0 == strcmp(mapOf_realLife[data_id_local].tag, mapOf_realLife[data_id_local-1].tag) ) ) {
      //!
      data_id_local++;
      continue; //! ie, as we then assume that the therea re mulipel conceuaite coccurences of the same data-set, whioch fwe for this use-case ingores/omits (oekseth, 06. okt. 2017).
    }
    //! 
    //! 
    s_kt_matrix_t obj_matrixInput = __static__readDataFromObject__kd__tut_3(mapOf_realLife[data_id_local], data_id_local, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false, isTo_includeTransposed); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id_local].tag;
    assert(tag); assert(strlen(tag));
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
    if(fileP_meta) {
      fprintf(fileP_meta, "%s\t%u\t%u\n", tag, obj_matrixInput.nrows, obj_matrixInput.ncols);
    }
    //!
    insert_index++;
    //!
    data_id_local++;
  }
  assert(insert_index <= mapOf_realLife_size);
  {
    assert(fileP_meta);
    fclose(fileP_meta); fileP_meta = NULL;
  }
  //!
  //! Apply logics: 
  { //! Comptue where gold is usedinfed through MINE+HCA:
    char resultPrefix_local[10000]; memset(resultPrefix_local, '\0', 10000); sprintf(resultPrefix_local, "%s_isToUseHCAMINE", resultPrefix);
    tut_kd_3__fromMatrix__fixedCCM(mat_collection.list, /*mapOf_realLife_size=*/insert_index, &arrOf_stringNames, resultPrefix_local, ccmPost_matrix_duringIterative);
    //    tut_kd_3__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix_local, /*config_rowNameIdentifiesCluster=*/false); //! ie, use HCA+MINE.
  }
  /* if(rowName_identifiesClusterId) { //! Comptue where gold is usedinfed through overallpgin row-names: */
  /*   tut_kd_3__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix, /\*config_rowNameIdentifiesCluster=*\/true); */
  /* }  */
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}



#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief  compute [data-id][sim-id] = max(ccmScore(ccm-id, cluster-result)) wrt. sim-id, gold-ccm, matrix-ccm (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks we identify the best simalrity-emtrics to be sued for a given data-set.
   @remarks related-tut-examples:
   -- "tut_ccm_15_useCase_singleMatrix_differentPatterns.c": logics to evaluate implicaiton of different patterns. 
   -- "hp_evalHypothesis_algOnData.c": an API for data-anlaysis.
   -- "tut_kd_1_cluster_multiple_simMetrics.c": min-max-evaluation-accuracy wrt. CCMs and simalrityu-emtrics.
**/
int main(const int array_cnt, char **array) 
#else
  int tut_kd_3_data_simMetric(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  {    
    { //! Compute [data-id][sim-id] for a given colleciton of matrices.
      const bool isTo_transposeMatrix = true;
      //const char *resultPrefix = "tut_evalKdSim3_transp_";
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccmPost_matrix_duringIterative = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      const char *resultPrefix__dataSubset = "tut_evalKdSim3_combine3DataSets_fixedCCM_transp";
      // --------------
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
      // -------
#define __MiCo__useLocalVariablesIn__varName_useLocal 1
#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      // ------------------------
      const s_hp_clusterFileCollection_t *mapOf_realLife_1 = mapOf_realLife; uint mapOf_realLife_size_1 = mapOf_realLife_size;
#define __MiV__exper nameOf_experiment_FCPS
#define __MiV__reaLife_list mapOf_realLife_2 
#define __MiV__reaLife_list_size  mapOf_realLife_size_2
#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      //    const s_hp_clusterFileCollection_t *mapOf_realLife_2 = mapOf_realLife; uint mapOf_realLife_size_2 = mapOf_realLife_size;
      // --------------------------------------
#define __MiV__exper nameOf_experiment_realKT
#define __MiV__reaLife_list mapOf_realLife_3
#define __MiV__reaLife_list_size  mapOf_realLife_size_3
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      //    const s_hp_clusterFileCollection_t *mapOf_realLife_3 = mapOf_realLife; uint mapOf_realLife_size_3 = mapOf_realLife_size;
#undef __MiCo__useLocalVariablesIn__varName_useLocal
      //! 
      //! The call:
      __eval__dataCollection__tut_3__fixedCCM(
					      mapOf_realLife_1, mapOf_realLife_size_1, 
					      mapOf_realLife_2, mapOf_realLife_size_2, 
					      mapOf_realLife_3, mapOf_realLife_size_3, 
					      /*resultPrefix=*/resultPrefix__dataSubset, 
					      ccmPost_matrix_duringIterative,
					      isTo_transposeMatrix
					      );
    }
    { //! Compute [data-id][sim-id] for a given colleciton of matrices.
      const bool isTo_transposeMatrix = false;
      const char *resultPrefix = "tut_evalKdSim3_";
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccmPost_matrix_duringIterative = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      const char *resultPrefix__dataSubset = "tut_evalKdSim3_combine3DataSets_fixedCCM_";
      // --------------
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
      // -------
#define __MiCo__useLocalVariablesIn__varName_useLocal 1
#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      // ------------------------
      const s_hp_clusterFileCollection_t *mapOf_realLife_1 = mapOf_realLife; uint mapOf_realLife_size_1 = mapOf_realLife_size;
#define __MiV__exper nameOf_experiment_FCPS
#define __MiV__reaLife_list mapOf_realLife_2 
#define __MiV__reaLife_list_size  mapOf_realLife_size_2
#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      //    const s_hp_clusterFileCollection_t *mapOf_realLife_2 = mapOf_realLife; uint mapOf_realLife_size_2 = mapOf_realLife_size;
      // --------------------------------------
#define __MiV__exper nameOf_experiment_realKT
#define __MiV__reaLife_list mapOf_realLife_3
#define __MiV__reaLife_list_size  mapOf_realLife_size_3
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      //    const s_hp_clusterFileCollection_t *mapOf_realLife_3 = mapOf_realLife; uint mapOf_realLife_size_3 = mapOf_realLife_size;
#undef __MiCo__useLocalVariablesIn__varName_useLocal
      //! 
      //! The call:
      __eval__dataCollection__tut_3__fixedCCM(
					      mapOf_realLife_1, mapOf_realLife_size_1, 
					      mapOf_realLife_2, mapOf_realLife_size_2, 
					      mapOf_realLife_3, mapOf_realLife_size_3, 
					      /*resultPrefix=*/resultPrefix__dataSubset, 
					      ccmPost_matrix_duringIterative,
					      isTo_transposeMatrix
					      );
    }
  }

  { //! Fris seperatley for a gvien input-file:
    // const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
    const char *file_name = "tests/data/kt_mine/fish_growth.tsv"; //! ie, a samlmer rela-life dat-aset than the IRIS-data-set.
    const uint index_pos = 3;
    bool fileIs_speificed = false; //! which is used to simplify error-generiaotn.
    if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) {
      //! then set the file-name:
      file_name = array[index_pos];
      fileIs_speificed = true;
    }
    //!
    //! Load an arbitrary data-set:
    // const uint nrows = 1000; const uint ncols = 20;
    //const uint nrows = 1000; const uint ncols = 20;
    //  const uint nrows = 1000*10; const uint ncols = 5;
    //const uint nrows = 1000*1000; const uint ncols = 20;
    s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols);
    //! Then laod the file:
    assert(file_name); assert(strlen(file_name));
    import__s_kt_matrix_t(&mat_input, file_name);
    if(mat_input.nrows == 0) {
      if(fileIs_speificed) {
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      } else { //! then we asusmet eh file-name was spefieid by the user.
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      }
      return false;
    }
    //!
    //! Apply logics:
    tut_kd_3__fromMatrix(&mat_input, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"result_tutEval_3_single", /*config_rowNameIdentifiesCluster=*/false);    
    //!
    //! De-allocate:
    free__s_kt_matrix(&mat_input);
    assert(false); // FIXME: remvoe
  }
  if(false)
  { //! Evaluate large real-lfie GEO-data:sets
    //! Note: For this test-case to work you need to have downlaoded the GEO-data-sets, eg, the " ../../mine-data-analysis/src/sample_data/GSE19753_series_matrix.txt" which is pased through KnittingTools "../../mine-data-analysis/src/perl_buildSamples/parse_geo.pl" GEO-patser.
    const char *fileName = "../../mine-data-analysis/src/result_GSE19743.tsv";
    const int result = access(fileName, R_OK);
    if (result == 0) {  //! thewe we were able to open the file:
      //! PArse the data-file:
      s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(fileName, initAndReturn__s_kt_matrix_fileReadTuning_t());
      if(mat_input.nrows > 0) {
	//!
	//! Apply logics:
	tut_kd_3__fromMatrix(&mat_input, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"GEO_GSE19743", /*config_rowNameIdentifiesCluster=*/false);    	  
	//!
	//! A transposed verison:
	s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_input);
	tut_kd_3__fromMatrix(&mat_transp, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"GEO_GSE19743.transposed", /*config_rowNameIdentifiesCluster=*/false);    	  
	//!
	//! De-allocate: 
	free__s_kt_matrix(&mat_input);
	free__s_kt_matrix(&mat_transp);
      } else {
	fprintf(stderr, "!!\t Unable to read file \"%s\" at %s:%d\n", fileName, __FILE__, __LINE__);
      }
    }
  }
  { //! Load for different real-life data-sets:
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
  { //! Fris seperatley for a gvien input-file:
    const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
    const uint index_pos = 3;
    bool fileIs_speificed = false; //! which is used to simplify error-generiaotn.
    if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) {
      //! then set the file-name:
      file_name = array[index_pos];
      fileIs_speificed = true;
    }
    //!
    //! Load an arbitrary data-set:
    // const uint nrows = 1000; const uint ncols = 20;
    //const uint nrows = 1000; const uint ncols = 20;
    //  const uint nrows = 1000*10; const uint ncols = 5;
    //const uint nrows = 1000*1000; const uint ncols = 20;
    s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols);
    //! Then laod the file:
    assert(file_name); assert(strlen(file_name));
    import__s_kt_matrix_t(&mat_input, file_name);
    if(mat_input.nrows == 0) {
      if(fileIs_speificed) {
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      } else { //! then we asusmet eh file-name was spefieid by the user.
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      }
      return false;
    }
    //!
    //! Apply logics:
    tut_kd_3__fromMatrix(&mat_input, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"result_tutKd3", /*config_rowNameIdentifiesCluster=*/false);    
    //!
    //! De-allocate:
    free__s_kt_matrix(&mat_input);
  }
  //  assert(false); // IXME: remove.
  const bool isTo_transposeMatrix = false;
  const char *resultPrefix = "tut_evalKdSim3_eachEnsamble";
    {
#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_3(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);
    }
    { //const char *nameOf_experiment = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_3(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);
    }
    { 
#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_3(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/true, isTo_transposeMatrix);
    }
    {
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
    }
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
  }

  // ------------------- 
  assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... update our proceudre .... add support for a 'direct' DB-SCAN-evaluation .... where the all-agianst-all-sialrmity-metirc-evlauation udrig a dynamic-CCM-evlauation-aprpaoch is 'seen' as un-neccesary for use-cases where users have domain-knwoeldge of their data-sets (hence correctenss of the latter) .... 



  //! *************************************************************
  //! 
  //! Case: synetic data-sets where the cluster-sepraiotns are explcitly 'encoded' in the input-data-sets.
  //! 
  { //! Compute for synteitc data-sets where cluster-partiions is expllicty known.
    //! Note: we make use of logics in our \hpFile{hp_clusterShapes.h} for cnsturciotn of synteitc data-sets, ie, to eanble/allwo a controlled/careful comaprsin/cosniderioant of CCMs prediocnt-accuracy-difference. 
    const uint nrows = 20; 
    { //! A linear increase in in-correct cluster-predictions:
      //! Note: for simplcitiy we choose to gradulaly decrease teh score-difference between in-clsuter versus between-cluster-members, ie, where latter provides a cocneutally intutive relationship wrt. estlibhsihing cluster-rpediocnt-accuracy. 
      { //! A squared shape: 

      }
      { //! Linear-increase  in cluster-result-sets: 

      }
    }
    { //! Cases where all clusters are correctly idneitfied/set (ie, test in-accuracies/differences wrt. differnet strategies) .... examplfies prediocnt-in-accuraices/base-assumptiosn wrt. CCM-score-senstitiy .... 
      //! Note: to investitat the implication of predictoin-difference (for the same clsuters) we have desinged an appraoch to adjust the cluster-topologeis while keepign the correncess of the clsuter-seperaiton fixed. .... 
      { //! A squared shape:

      }
      { //! Linear-increase in cluster-result-sets.

      }
    }
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... concpetual .... representive data-sets to explore .... case(1) ... a linear increase in in-correct cluster-predioncts ... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... concpetual .... representive data-sets to explore .... case(2) ... cases where all clusters are correctly idneitfied/set (ie, test in-accuracies/differences wrt. differnet strategies) .... examplfies prediocnt-in-accuraices/base-assumptiosn wrt. CCM-score-senstitiy .... 
    // ---- 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... iteriaotn-funciton to consturct different sytneitc dat-distributions ... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... set the row-name 'as' the gold-standard-cluster-id
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... evaluate for differet matrix-dimesnions ..... testing the implcaiotn/effect of data-size-pertubrations .... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... evaluate CCMs directly .... compute [CCM-permtautnions for ccmType=[matrix, gold]][dataEnsamlbe('x' w/ 'syntetic-data' x 'gold-cluster'] = [``Kendall's Tau'' ] ... ie, (a) cosntruct systneitc data-sets; (b) apply CCM-measures, ie, cosntruct a vector of CCM-scores (seperately for each gold-ccm and matrix-ccm); (c) after a data-ensamble is comptued then (sperately for each CCM) compare with a vector holding the corretly rodered ranks (ie, for which we apply/use the pariwise simlairty-metric in quesiton); (d) when all data-ensambles and CCMs are evlauated then export the result. 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... new function .... syntethic data-sets with known cluster-distributions ... 
    // ------------------- 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... 
  }



  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}

#undef __MiF__isTo_evaluate__local

