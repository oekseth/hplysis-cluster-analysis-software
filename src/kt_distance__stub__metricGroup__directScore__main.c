/**
   @brief provide the main-method-logics for "kt_distance__stub__metricGroup__directScore.c" (oekseth, 06. des. 2016).
   @remarks
   - the "e_cmp_masksAre_used_t  masksAre_used" (defined in "correlation_enums.h") is used to idnetify the type of masks to use.
   - the "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" is used to 'find the inner macro-type', ie, the macro-distance-function specified in "metricMacro_directScore.h"
 **/


const e_kt_correlationFunction_groupClass__directScore_t group_id = getGroupOfMetric__directScore__e_kt_correlationFunction(metric_id);
assert(isTo_use_directScore__e_kt_correlationFunction(metric_id));
// printf("group_id=%u, e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus=%u, at %s:%d\n", group_id, e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus, __FILE__, __LINE__);
assert(group_id != e_kt_correlationFunction_groupClass__directScore_undef);

/* if(masksAre_used == e_cmp_masksAre_used_false) { */
/*   printf("\t Does Not use masks, at %s:%d\n", __FILE__, __LINE__); */
/*  } */

// 

//! ---------------------------------------------------------------------------
//!
//! Idneitfy the group-sub-cases:
if(group_id == e_kt_correlationFunction_groupClass__directScore_direct) { 
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_direct 
  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
 } else if(group_id == e_kt_correlationFunction_groupClass__directScore_mergeMatrices__sum) {
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_mergeMatrices__sum

  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
  //assert(false); // FIXME: .... 
 } else if(group_id == e_kt_correlationFunction_groupClass__directScore_mergeMatrices__mul) {
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_mergeMatrices__mul

  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
  //assert(false); // FIXME: .... 
 } else if(group_id == e_kt_correlationFunction_groupClass__directScore_mergeMatrices__avg) {
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_mergeMatrices__avg

  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
  //assert(false); // FIXME: .... 
 } else if(group_id == e_kt_correlationFunction_groupClass__directScore_symmetricProperty__sum) {
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_symmetricProperty__sum
  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
  //assert(false); // FIXME: .... 
 } else if(group_id == e_kt_correlationFunction_groupClass__directScore_symmetricProperty__mul) {
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_symmetricProperty__mul

  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
  //assert(false); // FIXME: .... 
 } else if(group_id == e_kt_correlationFunction_groupClass__directScore_symmetricProperty__avg) {
  //!
  //! Idneitfy the mask-type and weight-type to be used:
#define __stub_metricGroup__scoreFetchTypeIndex e_kt_correlationFunction_groupClass__directScore_symmetricProperty__avg

  //! start: --------------------------------- generic part: ---
  if(!mask1 && !mask2) {
    if(masksAre_used == e_cmp_masksAre_used_false) {
#define __stub_metricGroup__maskType 0
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    } else {
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
    }
  } else if(mask1 && mask2) {
#define __stub_metricGroup__maskType 2
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  } else {
    assert(false); // then we need to add support 'for this case'.
    //! Use a 'fall-back':
#define __stub_metricGroup__maskType 1
#include "kt_distance__stub__metricGroup__directScore__main_stub__specifyInnerMacro.c" //! ie, specify the 'inner macor-type' and apply the logics.
  }
  //! finish: --------------------------------- generic part: ---
 }

//! --------------------
//! Reset the macro-variables:
#undef __stub_metricGroup__allAgainstAll
#undef __stub_metricGroup__oneToMany
