
#include "kt_hash_string.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_sim_string.h"
#include "hp_exportMatrix.h"
#include "hp_entropy.h"

static void __eval__tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix(const char *str_resultPrefix, const e_kt_entropy_genericType_t enum_entropy, const uint configHistogram_cntBins, s_kt_hash_string_2dSparse_t obj_str_2d, const bool isTo_computeClusters) {

  //  for(uint metric_id = 0; metric_id < e_kt_sim_string_type_undef; metric_id++) {
  { uint metric_id = e_kt_sim_string_type_LevenshteinDistance;
    const e_kt_sim_string_type_t enum_id = (e_kt_sim_string_type_t)metric_id;
    { const uint isTo_normalize_result = 1;
      //    for(uint isTo_normalize_result = 0; isTo_normalize_result < 2; isTo_normalize_result++) {
      //	s_kt_sim_string_t obj_sim = init__s_kt_sim_string_t();
      s_kt_sim_string_t obj_sim = init__s_kt_sim_string_t();
      obj_sim.isTo_normalize_result = (bool)isTo_normalize_result;
      //! 
      //! Initate the post-simarlity-seleciton-fitler: 
      s_kt_list_2d_kvPair_filterConfig_t config_filter = init__s_kt_list_2d_kvPair_filterConfig_t(
												  /*config_cntMin_afterEach=*/1, 
												  /*config_cntMax_afterEach=*/UINT_MAX,
												  /*config_ballSize_max=*/T_FLOAT_MAX, 
												  /*isTo_ingore_zeroScores=*/false);
      FILE *fileP_resultHisto_symmetric = NULL;       FILE *fileP_resultHisto = NULL;
      {
	char stringOf_resultHistoFile[1000]; memset(stringOf_resultHistoFile, '\0', 1000);
	sprintf(stringOf_resultHistoFile, "%s_stat_histo_%s_norm_%u_symmetric.tsv", str_resultPrefix, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	printf("(info:export)\t \"%s\", at %s:%d\n", stringOf_resultHistoFile, __FILE__, __LINE__);
	fileP_resultHisto_symmetric = fopen(stringOf_resultHistoFile, "w");
	if (fileP_resultHisto_symmetric == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultHistoFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
      }
      {
	char stringOf_resultHistoFile[1000]; memset(stringOf_resultHistoFile, '\0', 1000);
	sprintf(stringOf_resultHistoFile, "%s_stat_histo_%s_norm_%u.tsv", str_resultPrefix, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	fileP_resultHisto = fopen(stringOf_resultHistoFile, "w");
	if (fileP_resultHisto == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultHistoFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
      }

      FILE *fileP_result_symmetric = NULL;       FILE *fileP_result = NULL;
      {
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s_stat_entropy_%s_norm_%u_symmetric.tsv", str_resultPrefix, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	printf("(info:export)\t \"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
	fileP_result_symmetric = fopen(stringOf_resultFile, "w");
	if (fileP_result_symmetric == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
      }
      {
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s_stat_entropy_%s_norm_%u.tsv", str_resultPrefix, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	printf("(info:export)\t \"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
	fileP_result = fopen(stringOf_resultFile, "w");
	if (fileP_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
      }
      

      //! 
      //! Comapre results: we expec tboth matrices to be equal:
      //  assert(obj_fromFile.list_size >= nrows);
      for(uint row_id = 0; row_id < obj_str_2d.obj_dense.obj_strings.nrows; row_id++) {
	s_kt_list_1d_string_t strObj_1 = setToEmpty_andReturn__s_kt_list_1d_string_t();
	s_kt_list_2d_kvPair_t obj_sparse = setOf__compute__vertex__kt_sim_string(&obj_sim, enum_id, &config_filter, &obj_str_2d, row_id, &strObj_1);
	//! 
	//! 
	if(strObj_1.nrows > 0) { //! Write out result:
	  //const s_kt_list_1d_string_t *strObj_1 = &(obj_str_2d.obj_sparse.obj_strings);
	  assert(strObj_1.nrows > 0); //! ie, as we exsect there is at least one stirng in the data-set.
	  assert(row_id < obj_str_2d.obj_dense.obj_strings.nrows);
	  const char *str_head = obj_str_2d.obj_dense.obj_strings.nameOf_rows[row_id];
	  assert(str_head);
	  assert(strlen(str_head));
	  //!
	  //! Write out: 
	  { //! The result 'as-is': 
	    if(isTo_computeClusters) {
	      char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	      sprintf(stringOf_resultFile, "%s_row.%u_%s_norm_%u", str_resultPrefix, row_id, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	      //! The call:
	      export__s_kt_list_2d_kvPair__hp_exportMatrix(stringOf_resultFile, obj_sparse, &strObj_1, /*isTo_storeInMatrix=*/true);
	    }
	    //! 
	    if(fileP_result) { //! Write out the entropy-score: 
	      assert(fileP_result);
	      t_float entropy_score = T_FLOAT_MAX; from2dSparseList__slow__hp_entropy(enum_entropy, /*isTo_applyPostScaling=*/false, /*data=*/&obj_sparse, &entropy_score);
	      if(entropy_score != T_FLOAT_MAX) {
		//! Write out:
		fprintf(fileP_result, "%s\t%f\n", str_head, entropy_score);
	      }
	    }
	  }
	  //if(false) 
	  { //! Generate a symmetrci-DB-SCAN-data-set-appraoch: we 'require': ([key1][key2] != null)--implies--([key2][key1] != null), ie, for the rank-filtered subset to be symemtrically cosnistent/agreeable.
	    s_kt_list_2d_kvPair_t obj_sparse_subsetSymmetric = get_symmetricSubset__onKeys__s_kt_list_2d_kvPair_filterConfig_t(&obj_sparse);
	    if(isTo_computeClusters) {
	      char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	      sprintf(stringOf_resultFile, "%s_row.%u_%s_norm_%u_subsetSymmetric", str_resultPrefix, row_id, getString__e_kt_sim_string_type_t(enum_id), /*isTo_storeInMatrix=*/true);
	      //! The call:
	      export__s_kt_list_2d_kvPair__hp_exportMatrix(stringOf_resultFile, obj_sparse_subsetSymmetric, &strObj_1,  /*isTo_storeInMatrix=*/true);
	    }
	    //! 
	    if(fileP_result_symmetric) { //! Write out the entropy-score: 
	      assert(fileP_result_symmetric);
	      t_float entropy_score = T_FLOAT_MAX; from2dSparseList__slow__hp_entropy(enum_entropy, /*isTo_applyPostScaling=*/false, /*data=*/&obj_sparse_subsetSymmetric, &entropy_score);
	      if(entropy_score != T_FLOAT_MAX) {
		//! Write out:
		fprintf(fileP_result_symmetric, "%s\t%f\n", str_head, entropy_score);
	      }
	    }
	    { //! Comptue and wirte out hsitograms:
	      if(fileP_resultHisto != NULL) {
		writeOut_histoGram1d_TSV__s_kt_list_2d_kvPair_t(&obj_sparse, fileP_resultHisto, str_head, /*isTo_generateHead=*/(row_id == 0), configHistogram_cntBins);
	      }
	      if(fileP_resultHisto_symmetric != NULL) {
		writeOut_histoGram1d_TSV__s_kt_list_2d_kvPair_t(&obj_sparse_subsetSymmetric, fileP_resultHisto_symmetric, str_head, /*isTo_generateHead=*/(row_id == 0), configHistogram_cntBins);
	      }
	    }
	    //!
	    //! De-allcoate: 
	    //free__s_kt_sparse_sim_t(&obj_sim);
	    free__s_kt_list_2d_kvPair_t(&obj_sparse_subsetSymmetric);
	  }
	}
	//!
	//! De-allcoate: 
	//free__s_kt_sparse_sim_t(&obj_sim);
	free__s_kt_list_2d_kvPair_t(&obj_sparse);
	free__s_kt_list_1d_string(&strObj_1);
	//assert(false); // FIXME: remove
	//assert(MF__isEqual__s_ktType_kvPair((obj_fromFile.list[row_id]), (obj_data.list[row_id] )));
      }
      //! 
      //! 
      if(fileP_result_symmetric != NULL) {
	fclose(fileP_result_symmetric); fileP_result_symmetric = NULL;
      }
      if(fileP_result != NULL) {
	fclose(fileP_result); fileP_result = NULL;
      }
      if(fileP_resultHisto_symmetric != NULL) {
	fclose(fileP_resultHisto_symmetric); fileP_resultHisto_symmetric = NULL;
      }
      if(fileP_resultHisto != NULL) {
	fclose(fileP_resultHisto); fileP_resultHisto = NULL;
      }
    }
  }
}

/**
   @brief demonstrates a parsing-task where we laod data-rows into a string-1d-list, and then use logics in our "kt_hash_string.h" to split a string-set into (head, tail) parts.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- general: use logics in our "kt_hash_string.h" and our "kt_list_1d_string.h"
**/
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strateyg
int main() 
#else
  void tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  { //! A simple test-case: 
    const uint nrows = 5; 
    const char *mapOf_rows[nrows] = {
      "eide harriet  eide karl\t   eide kåre\t   eide karl\t   eide kåre",
      "rem tore\t  bø tore ness tore\t   berg tore\t   pryser tore", 
      "kerbel lucy   ebel uwe\t\tleber huck\t  keel aldo\t   key ellen\t   keel aldo",
      "kovács ferenc \tkortner even\thorch franz\t kovacs david\t johns berit\t thiess frank",
      "prøysen alf   \tprøysen elin"
    };
    
    const char *str_resultPrefix = "tut_parse_9_stub_";
    const e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_ML_Shannon;
    const uint configHistogram_cntBins = 10; //! which is used hwen cosntrubing histograms.
    //!
    //! Allocate:
    s_kt_list_1d_string_t obj_data = setToEmpty_andReturn__s_kt_list_1d_string_t();
    //  s_kt_list_1d_kvPair_t obj_data = init__s_kt_list_1d_kvPair_t(nrows);
    //!
    //! Set values:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      assert(mapOf_rows[row_id]);
      set_stringConst__s_kt_list_1d_string(&obj_data, row_id, mapOf_rows[row_id]);
    }
    //! Load result:
    s_kt_hash_string_2dSparse_t obj_str_2d = init__fromHash__s_kt_hash_string_2dSparse_t(&obj_data, /*word_seperator_keyValues=*/'\t', /*word_seperator_valueValues=*/'\t', /*isTo_useDifferentMappingTables_forKeysIndexes=*/true);
    printf("current_pos=%u, and [%u][%u], at %s:%d\n", obj_str_2d.obj_2d.current_pos, obj_str_2d.obj_dense.obj_strings_cntInserted, obj_str_2d.obj_sparse.obj_strings_cntInserted, __FILE__, __LINE__);
    //  assert(obj_str_2d.obj_2d.current_pos == 2); //! ie, as we expect two unqiuely inserted rows.
    /* assert(obj_str_2d.obj_dense.obj_strings_cntInserted == 2); //! ie, should reflect [ªbvoe] */
    /* assert(obj_str_2d.obj_sparse.obj_strings_cntInserted == 6); //! ie,  */
    
    //!
    //! Apply logics:
    __eval__tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix(str_resultPrefix, enum_entropy, configHistogram_cntBins, obj_str_2d, /*isTo_computeClusters=*/true);
    //!
    //! De-allocates:
    free__s_kt_list_1d_string(&obj_data);
    free__s_kt_hash_string_2dSparse_t(&obj_str_2d);
  }
  { //! Laod data from a large file:
    // -----------
    const char *str_resultPrefix = "tut_parse_9_realWorld_";
    const e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_ML_Shannon;
    const uint configHistogram_cntBins = 10; //! which is used hwen cosntrubing histograms.
    // -----------
    const char *file_inp = "data/titles.sorted.txt";
    printf("#\t Parse-file: \"%s\", at %s:%d\n", file_inp, __FILE__, __LINE__);
    s_kt_list_1d_string_t obj_data = initFromFile__s_kt_list_1d_string_t(file_inp);
    printf("#\t Translate-file: \"%s\", at %s:%d\n", file_inp, __FILE__, __LINE__);
    s_kt_hash_string_2dSparse_t obj_str_2d = init__fromHash__s_kt_hash_string_2dSparse_t(&obj_data, /*word_seperator_keyValues=*/':', /*word_seperator_valueValues=*/'\0', /*isTo_useDifferentMappingTables_forKeysIndexes=*/true);
    printf("current_pos=%u, and [%u][%u], at %s:%d\n", obj_str_2d.obj_2d.current_pos, obj_str_2d.obj_dense.obj_strings_cntInserted, obj_str_2d.obj_sparse.obj_strings_cntInserted, __FILE__, __LINE__);
    //    if(false) 
    {
      printf("#\t Start-computing, at %s:%d\n", __FILE__, __LINE__);    
      //!
      //! Apply logics:
      __eval__tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix(str_resultPrefix, enum_entropy, configHistogram_cntBins, obj_str_2d, /*isTo_computeClusters=*/false);
    }
    //!
    //! De-allocates:
    free__s_kt_list_1d_string(&obj_data);
    free__s_kt_hash_string_2dSparse_t(&obj_str_2d);
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

