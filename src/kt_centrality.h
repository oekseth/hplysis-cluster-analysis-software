#ifndef kt_centrality_h
#define kt_centrality_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @file kt_centrality
   @brief provide logics for copmutation of centrality
   @remarks a generalised applciaotn of centrliaty involves the factors of:
   (1) construct an itnail weight-table for each relationship, eg, degree-centrlaity
   (2) use (if of itnerest) power-iteration (or: "Newton's iterative meothod");
   (3) post-process (eg, adjsut using Shannon's entropy);

   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
 **/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_kt_eigenVectorCentrality.h"

#if(configure_performance__kt_centrality__buildForSlowCases == 1) //! then we add support for building wrt. different test-cases.
/**
   @enum e_kt_centrality_config__testImpactOfNaiveImplemementation
   @brief specify diffenret perofmrance-tuning-parameters.
   @remarks is epxected to only be active if the "configure_performance__kt_centrality__buildForSlowCases" macro-apram is set to "1", ie, to avodi soruce-code bieng un-neccesary large for particular data-sets.
 **/
typedef enum e_kt_centrality_config__testImpactOfNaiveImplemementation {
  e_kt_centrality_config__testImpactOfNaiveImplemementation_none, //! ie, where the fact implementaiton is used:
  e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric,
  e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed, 
} e_kt_centrality_config__testImpactOfNaiveImplemementation_t;

#define __macro__e_kt_centrality_config__testImpactOfNaiveImplemementation_none                                                                1
#define __macro__e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric                            2
#define __macro__e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed  3

#endif //! else we assuemt hsi macrois Not of itnerest.

/**
   @struct s_kt_centrality_config
   @brief logics for configuring the copmtaution of the centrlaity
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
 **/
typedef struct s_kt_centrality_config {  
  uint powerIncrease_ofDistanceSum;
  uint max_cnt_iterations;
  float maxError_forConvergence;
  e_kt_eigenVectorCentrality_t typeOf_eigenVectorType_adjustment;
  e_kt_eigenVectorCentrality_postProcess_t typeOf_eigenVectorType_adjustment_postProcessTAble;
  e_kt_eigenVectorCentrality_typeOf_weightUse_t typeOf_eigenVectorType_use;
  e_kt_centrality_typeOf_postAdjustment_t typeOf_postAdjustment;
  bool isTo_useSSE;  bool isTo_useSSE__matrixIsAllocated_with_overfloatSSE_space;
  bool isTo_incrementallyUpdateResultVector;
#if(configure_performance__kt_centrality__buildForSlowCases == 1) //! then we add support for building wrt. different test-cases.
  e_kt_centrality_config__testImpactOfNaiveImplemementation_t typeOf_performanceTuning; //! ie, for which we need to access the 
#endif
} s_kt_centrality_config_t;

//! Initaites the s_kt_centrality_config_t object.
static void setTo_empty__s_kt_centrality_config(s_kt_centrality_config_t *self) {
  assert(self);
  //! Init:
  self->powerIncrease_ofDistanceSum = UINT_MAX;
  self->max_cnt_iterations = 1000;
  self->maxError_forConvergence = 0.01;
  self->typeOf_eigenVectorType_adjustment = e_kt_eigenVectorCentrality_none;
  self->typeOf_eigenVectorType_adjustment_postProcessTAble = e_kt_eigenVectorCentrality_postProcess_undef;
  self->typeOf_eigenVectorType_use = e_kt_eigenVectorCentrality_typeOf_weightUse_none;
  self->typeOf_postAdjustment = e_kt_centrality_typeOf_postAdjustment_none;
  self->isTo_useSSE = true; self->isTo_useSSE__matrixIsAllocated_with_overfloatSSE_space = false;
  self->isTo_incrementallyUpdateResultVector = false;
#if(configure_performance__kt_centrality__buildForSlowCases == 1) //! then we add support for building wrt. different test-cases.
  self->typeOf_performanceTuning = e_kt_centrality_config__testImpactOfNaiveImplemementation_none; //! ie, use the fast alternative
#endif
}

/* #define kt_mathMacros_SSE__powerIteration(matrix, _row_input, _row_tmp, nsteps) ({ \ */
/*   const t_float *__restrict__ row_input = _row_input;   const t_float *__restrict__ row_tmp = _row_tmp; \ */
/* for each(''simulation'') { */
/*   // calculate the matrix-by-vector product Ab */
/*   for(i=0; i<n; i++) { */
/*     tmp[i] = 0; */
/*     for (j=0; j<n; j++) */
/*       tmp[i] += A[i][j] * b[j];  */
/*     // dot product of i-th row in A with the column vector b */
/*   } */

/*   // calculate the length of the resultant vector */
/*   norm_sq=0; */
/*   for (k=0; k<n; k++) */
/*     norm_sq += tmp[k]*tmp[k];  */
/*   norm = sqrt(norm_sq); */

/*   // normalize b to unit vector for next iteration */
/*   b = tmp/norm; */
/* 	} */

/**
   @enum e_kt_centrality_config__simplfiedCategories
   @brief provide a genelairzed appraoch to simplify a batch-evlauation of differetn iterative centrality-evlauation cases (oekseth, 06. feb. 2017)
 **/
typedef enum e_kt_centrality_config__simplfiedCategories {
  e_kt_centrality_config__simplfiedCategories_inside_none__postProcess_result__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_none__postProcess_1_dividedBy_result__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_none__postProcess_log__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_none__postProcess_logPlussOne__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_none__postProcess_none__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_none__postProcess_undef__weight_none__postAdjustment_none,
  //! ---- 
  e_kt_centrality_config__simplfiedCategories_inside_sumScalarLog__postProcess_result__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_sumScalarLog__postProcess_1_dividedBy_result__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_sumScalarLog__postProcess_log__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_sumScalarLog__postProcess_logPlussOne__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_sumScalarLog__postProcess_none__weight_none__postAdjustment_none,
  e_kt_centrality_config__simplfiedCategories_inside_sumScalarLog__postProcess_undef__weight_none__postAdjustment_none,
  //! ---- 
  e_kt_centrality_config__simplfiedCategories_none__postProcess_result__weight_none__postAdjustment_none,  
  e_kt_centrality_config__simplfiedCategories_none__postProcess_1_dividedeBy_result__weight_none__postAdjustment_none,  
  e_kt_centrality_config__simplfiedCategories_none__postProcess_log__weight_none__postAdjustment_none,  
  e_kt_centrality_config__simplfiedCategories_none__postProcess_logPlussOne__weight_none__postAdjustment_none,  
  e_kt_centrality_config__simplfiedCategories_none__postProcess_none__weight_none__postAdjustment_none,  
  e_kt_centrality_config__simplfiedCategories_none__postProcess_undef__weight_none__postAdjustment_none,  
  //! -----------------------------------------  
  e_kt_centrality_config__simplfiedCategories_undef,
} e_kt_centrality_config__simplfiedCategories_t;

//! Initaites the s_kt_centrality_config_t object (oekseth, 06. feb. 2017)
//! @reutrn an itnated s_kt_centrality_config_t object
s_kt_centrality_config_t initAndReturn__e_kt_centrality_config__simplfiedCategories_t(const e_kt_centrality_config__simplfiedCategories_t enum_id);


/**
   @struct s_kt_centrality_config__wrapper
   @brief a wrapepr-structure to our "s_kt_centrality_config" to simplify ''itneraction' with our "e_kt_centrality_config__simplfiedCategories" enum (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
 **/
typedef struct s_kt_centrality_config__wrapper_ranks {  
  s_kt_centrality_config_t config;
  uint cnt_ranksToExtract_percent;
  //t_float STD_threshold
} s_kt_centrality_config__wrapper_ranks_t;

//! @retunr an itnated object of type "s_kt_centrality_config__wrapper_ranks_t" (oekseth, 06. feb. 2017).
static s_kt_centrality_config__wrapper_ranks_t initAndReturn__s_kt_centrality_config__wrappert(const e_kt_centrality_config__simplfiedCategories_t enum_id, const uint cnt_ranksToExtract_percent) {
//static s_kt_centrality_config__wrapper_ranks_t initAndReturn__s_kt_centrality_config__wrappert(const e_kt_centrality_config__simplfiedCategories_t enum_id, const uint cnt_ranksToExtract_percent) {
  s_kt_centrality_config__wrapper_ranks_t self;

# ifndef not_include_headerFiles
  self.config = initAndReturn__e_kt_centrality_config__simplfiedCategories_t(enum_id);
#else 
#pragma warning ("option not supported")
#define __Mi__postProcess() ({return;})
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).
  self.cnt_ranksToExtract_percent = cnt_ranksToExtract_percent;
  //!
  //!
  //! @return the wrapper-object:
  return self;
}


/**
   @brief copmtue centrality, centrlaity-measures speifeid in the self object.
   @param <matrix> is the input-data to compute for: an adjencey matrix hwere weights may be used to 'describe' distance: if a relationship is Not to be inlcucded, the set toe 'distance' to "0";
   @param <nrows> is the number of rows in the matrix
   @param <ncols> is the number of columns in the matrix: as we expect an ajdcency-matrix ncols==nrows is what we expect
   @param <arrOf_result> hold the set of centrliaty-scores identifed in this fucntion.
   @param <self> is the configuration- and sepficiaotn-object for copmtaution of centrlaity-scores
   @return true if operation was a success, ie, if configuraiton confired to our possible range of 'value-support'.
 **/
const bool kt_compute_eigenVector_centrality(t_float **matrix, const uint nrows, const uint ncols, t_float *arrOf_result, const s_kt_centrality_config_t self);


/**
   @brief copmtue centrality, centrlaity-measures speifeid in the self object, and return a ranked subset of the vertices (oesketh, 06. feb. 2017).
   @param <self> is the cofnigruation-object.
   @param <matrix> is the input-data to compute for: an adjencey matrix hwere weights may be used to 'describe' distance: if a relationship is Not to be inlcucded, the set toe 'distance' to "0";
   @param <nrows__ncols> is the number of rows and columns in the matrix, ie, where "nrows__ncols == nrows" and "nrows__ncols == ncols"
   @param <scalar_returnListSize> is the size of the return-list.
   @return  a list of high-ranked vertices.
 **/
uint *get_centralVertices__s_kt_centrality_config__wrappert_t(const s_kt_centrality_config__wrapper_ranks_t self, t_float **matrix, const uint nrows__ncols, uint *scalar_returnListSize);


#endif //! EOF
