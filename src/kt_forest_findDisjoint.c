#include "kt_forest_findDisjoint.h"
 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "correlation_sort.h"

#ifndef macro_max
#define macro_max(x, y) ((x) > (y) ? (x) : (y))
#endif
#ifndef macro_min
#define macro_min(x, y) ((x) < (y) ? (x) : (y))
#endif


//! Intiates an empty object
void setTo_Empty__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self) {
  assert(self);
  self->listOf_relations = NULL;
  //! Intiate the stacks:
  setTo_Empty__s_kt_set_1dsparse_t(&(self->vertexStack_child));
  setTo_Empty__s_kt_set_1dsparse_t(&(self->vertexStack_parent));
  setTo_Empty__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection));
  self->_nrows = 0; self->_ncols = 0;
  self->mapOf_vertexMembershipId = NULL;
  self->mapOf_disjointForest_vertexId = NULL;
  self->mapOf_vertexMembershipId_childsAreAdded = NULL;
  self->mask = NULL; //self->_maskIsAllocated = false;
  self->matrix = NULL; self->emptyValueIf_maskIsNotUsed = T_FLOAT_MAX;
  self->_inputType = e_kt_forest_findDisjoint_inputRelationType_sparseRelations;
  self->typeOf_startVertices = e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_increase;
  self->typeOf_graphTraversalOrder = e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping;
  //! ------------------------
  //!
  //! Status-code:
  self->statusCode = e_kt_forest_findDisjoint_statusCode_undef;
  //! ------------------------
  //!
  //! Counters for local-name-spaces:
  self->mapOf_vertexMembershipId_size = 0;
  self->globalIndexOfset_rowId = 0; 
  self->globalIndexOfset_columnId = 0;
  //! ------------------------
  //!
  //! Specify a 'supportive data-structure' to hold/remember the different data-set-ids for each namespace ... used in construction of the "uint **mapOf_setMember" return-data-structure-id:
  self->mapOf_nameSpaceDomain_offset_rows_cnt = 0; 
  self->mapOf_nameSpaceDomain_offset_columns_cnt = 0;
  self->mapOf_nameSpaceDomain_offsetStart_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt] = 0;  self->mapOf_nameSpaceDomain_offsetEnd_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt] = UINT_MAX;
  self->mapOf_nameSpaceDomain_offsetStart_columns[self->mapOf_nameSpaceDomain_offset_columns_cnt] = 0;  self->mapOf_nameSpaceDomain_offsetEnd_columns[self->mapOf_nameSpaceDomain_offset_columns_cnt] = UINT_MAX;
  //self->ncols_current = 0;
  //! ------------------------
  //! ------------------------
  //! ------------------------
  //!
  //! Mapping-support for DB-scan permtautions (oekseth, 06. jul. 2017):
  self->obj_mapOf_rootVertices_pointer = NULL;
  self->obj_mapOf_rootVertices_pointer__forBothHeadAndTail = false;
}
//! @return an empty object of this type
 s_kt_forest_findDisjoint_t get_empty__s_kt_forest_findDisjoint() {
  s_kt_forest_findDisjoint_t self;
  setTo_Empty__s_kt_forest_findDisjoint(&self);
  return self;
}

//! A gneralized intaiton:
static void __init_structure_wrt_previousElements(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, const bool isTo_init, const bool useSeperateNameSpace_rows, const bool useSeperateNameSpace_columns, const uint totalCntUniqueVerticesToEvalauteForAllIterations) {
  //! Frist inatie:
  if(isTo_init) {setTo_Empty__s_kt_forest_findDisjoint(self);}
  if(isTo_init == false) {assert(totalCntUniqueVerticesToEvalauteForAllIterations == self->mapOf_vertexMembershipId_size);} //! ie, as we otherwise maytt have an incocnctency.
  self->mapOf_vertexMembershipId_size = totalCntUniqueVerticesToEvalauteForAllIterations; //, __ncols);
  if(useSeperateNameSpace_rows) {  
    self->mapOf_nameSpaceDomain_offset_rows_cnt++;
    if(self->globalIndexOfset_columnId == 0) {      
      //const uint prev_pos = self->globalIndexOfset_rowId;
      self->globalIndexOfset_rowId = self->globalIndexOfset_rowId + self->_nrows;
      //printf("globalIndexOfset_rowId=%u, at %s:%d\n", self->globalIndexOfset_rowId, __FILE__, __LINE__);
      //! Update the glboal mapping-container:
      /* self->mapOf_nameSpaceDomain_offsetStart_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt] = prev_pos;  */ 
    } else {
      assert(self->_ncols > 0);
      assert(self->globalIndexOfset_rowId < self->globalIndexOfset_columnId);
      self->globalIndexOfset_rowId = self->globalIndexOfset_columnId + self->_ncols;
      //printf("globalIndexOfset_rowId=%u, at %s:%d\n", self->globalIndexOfset_rowId, __FILE__, __LINE__);
    }    
    //! Assicate the current row-mappings-scheme to the mapping-offset in question:
    self->mapOf_nameSpaceDomain_offsetEnd_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt] = self->globalIndexOfset_rowId;
  }
  if(useSeperateNameSpace_columns) {  
    self->mapOf_nameSpaceDomain_offset_columns_cnt++;
    if(useSeperateNameSpace_rows || !ncols) { //! where the "!ncols" case is inlcuded in order to 'faicliatet' the case where we are interested in 'enabling' a "first matrix-comptaution run where a specific/unique name-sapce is givne the the column-incexes".
      self->globalIndexOfset_columnId += self->globalIndexOfset_rowId + nrows; //! ie, 'the row-chunk-id' after all the 'pproerpteis of this' has been added.
      //printf("globalIndexOfset_columnId=%u, at %s:%d\n", self->globalIndexOfset_columnId, __FILE__, __LINE__);
    } else {
      self->globalIndexOfset_columnId += self->globalIndexOfset_columnId + self->_ncols; 
      //printf("globalIndexOfset_columnId=%u, given _ncols=%u, at %s:%d\n", self->globalIndexOfset_columnId, self->_ncols, __FILE__, __LINE__);
    }
    assert(self->globalIndexOfset_columnId < self->mapOf_vertexMembershipId_size);
    //! Assicate the current row-mappings-scheme to the mapping-offset in question:
    self->mapOf_nameSpaceDomain_offsetEnd_columns[self->mapOf_nameSpaceDomain_offset_columns_cnt] = self->globalIndexOfset_columnId;
  }
  self->_nrows = nrows;
  self->_ncols = ncols;


  // printf("row-offsets=%u and column-offset=%u at %s:%d\n", self->globalIndexOfset_rowId, self->globalIndexOfset_columnId, __FILE__, __LINE__);
}

/**
   @brief allocates an s_kt_forest_findDisjoint_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate 
   @param <ncols> which is used if the useSeperateNameSpace_columns parameter is used, ie, to properly set a 'namespace-offset'.
   @param <listOf_relations> is the forest/graph/network to evlauate.
   @param <isTo_init> which shoudl be set to false if this is Not the first intaition-call to this fucniton
   @param <useSeperateNameSpace_rows> which should be set tot rue fi mulipel matices are to bee valuated seprately (ie, for different nam-spaces) in order to identfy disjoitn sets, eg, wehre 'ermge-point' is the feature/columnd ids.
   @param <useSeperateNameSpace_columns> which is ismilar to "useSeperateNameSpace_rows" with difference that we assuem that the rows are to be the 'merging-points.
   @param <totalCntUniqueVerticesToEvalauteForAllIterations> which is the total number of unique vertices to add (ie, for all 'calls' to this object wrt the idfferent input-name-spaces): 
 **/
void allocate__extensive__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, const s_kt_set_2dsparse_t *listOf_relations, const bool isTo_init, const bool useSeperateNameSpace_rows, const bool useSeperateNameSpace_columns, uint _totalCntUniqueVerticesToEvalauteForAllIterations) {
  assert(self);
  //! A gneralized intaiton:
  uint __ncols = 0;
  if(listOf_relations && listOf_relations->_nrows) {
    //printf("start: identify the mas-outer-index given nrows=%u, at %s:%d\n", listOf_relations->_nrows, __FILE__, __LINE__);
    for(uint this_key = 0; this_key < listOf_relations->_nrows; this_key++) {
      //printf("\t\t\t identify the mas-outer-index given nrows=%u, at %s:%d\n", listOf_relations->_nrows, __FILE__, __LINE__);
      uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(listOf_relations, this_key, &list_size);
      for(uint child_index = 0; child_index < list_size; child_index++) {
	const uint child_key = list[child_index];
	__ncols = macro_max(__ncols, child_key);
      }
    }
  }
  //printf("completed: identify the mas-outer-index, at %s:%d\n", __FILE__, __LINE__);
  __ncols += 1; //! ie, as the latter descirbe sth emax-index while we nee dth emax-size
  if(_totalCntUniqueVerticesToEvalauteForAllIterations == UINT_MAX) {_totalCntUniqueVerticesToEvalauteForAllIterations = nrows;}
  assert(nrows != UINT_MAX);
  const uint totalCntUniqueVerticesToEvalauteForAllIterations = macro_max(_totalCntUniqueVerticesToEvalauteForAllIterations, macro_max(__ncols, nrows));
  __init_structure_wrt_previousElements(self, nrows, ncols, isTo_init, useSeperateNameSpace_rows, useSeperateNameSpace_columns, totalCntUniqueVerticesToEvalauteForAllIterations);
  //! Tehreafter set the relation-set:
  //! Note: we expec thte 'allocation itself' to be eprformed when teh 'main-funciton' is called.
  self->listOf_relations = listOf_relations;
  self->_inputType = e_kt_forest_findDisjoint_inputRelationType_sparseRelations;
}
/**
   @brief allocates an s_kt_forest_findDisjoint_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <ncols> is the number of rows to allocate
   @param <matrix> is the dense list of interesting relations.
   @param <mask> which if Not set to NULL implies taht all valeus set to "mask[i][j] == 1" is expected to be a relation: if "mask" is used then we expect "matrix == NULL", ie, for consistency.
   // @param <isTo_transpose> which if set to true implies that we tanspsoe teh matrix, ie, before computing the relationships.
   @param <emptyValueIf_maskIsNotUsed> is the valeu which 'sates' that a vertex is empty in "matrix" (eg, "0" or "T_FLOAT_MAX"): if "mask" is used then we expect "mask == NULL"
   @param <isTo_init> which shoudl be set to false if this is Not the first intaition-call to this fucniton
   @param <useSeperateNameSpace_rows> which should be set tot rue fi mulipel matices are to bee valuated seprately (ie, for different nam-spaces) in order to identfy disjoitn sets, eg, wehre 'ermge-point' is the feature/columnd ids.
   @param <useSeperateNameSpace_columns> which is ismilar to "useSeperateNameSpace_rows" with difference that we assuem that the rows are to be the 'merging-points.
   @param <totalCntUniqueVerticesToEvalauteForAllIterations> which is the total number of unique vertices to add (ie, for all 'calls' to this object wrt the idfferent input-name-spaces): 
 **/
void allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, t_float **matrix, char **mask, const t_float emptyValueIf_maskIsNotUsed, const bool isTo_init, const bool useSeperateNameSpace_rows, const bool useSeperateNameSpace_columns, uint _totalCntUniqueVerticesToEvalauteForAllIterations) {
  assert(self);
  assert(self);
  //! A gneralized intaiton:
  if(_totalCntUniqueVerticesToEvalauteForAllIterations == UINT_MAX) {_totalCntUniqueVerticesToEvalauteForAllIterations = nrows;}
  const uint totalCntUniqueVerticesToEvalauteForAllIterations = macro_max(_totalCntUniqueVerticesToEvalauteForAllIterations, macro_max(nrows, ncols));
  __init_structure_wrt_previousElements(self, nrows, ncols, isTo_init, useSeperateNameSpace_rows, useSeperateNameSpace_columns, totalCntUniqueVerticesToEvalauteForAllIterations);
  //! Tehreafter a 'target-specifit' itnaiton:
  if(matrix) {
    assert(mask == NULL);
    self->matrix = matrix;
    self->_inputType = e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float;
    self->emptyValueIf_maskIsNotUsed = emptyValueIf_maskIsNotUsed;
  } else {
    assert(mask);
    self->_inputType = e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char;
  }
}

/**
   @brief Allocate and a apply the disjoint-matrix-construction: in this function we support 'disjoint-case-three' (where the latter is described in the Knitting-Tools docuemtaniton and assicated codde-examples) (oekseth, 06. des. 2016).
   @param <self> is the object to boht intaite and apply lgocis for
   @param <nrows_1> is the number of rows in matrix_1
   @param <nrows_2> is the number of rows in matrix_2
   @param <ncols> is the number of columns in matrix_1 and matrix_2
   @param <default_value_float__> is the 'empty value' in your matirx, eg, "T_FLOAT_MAX" or "1": if therea re no 'empty valeus' then your call is pointless (ie, as all regions will tehn be realted to each other): if we your in need 'of fidnign the empty regions' tehn we suggest usign hpLyssis "kt_matrix_filter" ANSI-C class-logics.
   @remarks 
   -- the results may be 'fetched' using our "cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__(..)"
   -- is a permtuation of our "allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(..)" where we facilates the 'merging' of two matrcies using the 'columns'æ as a 'point of itnersection'.
**/
void allocateAndApply__denseMatrix___differentNameSpaceInRowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **matrix_1, t_float **matrix_2, const t_float default_value_float__) {
  //const t_float default_value_float__ = T_FLOAT_MAX; //! ie, the default 'topion'.  
  assert(self); 
  assert(matrix_1); assert(nrows_1 != 0); 
  assert(matrix_2); assert(nrows_2 != 0); 
  assert(ncols != 0);   
  // fprintf(stderr, "\n\n\n # evlauate matrix with dims=[%u, %u][%u], at %s:%d\n", nrows_1, nrows_2, ncols, __FILE__, __LINE__);
  //!
  //! Case: matrix_1:
  allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(self, nrows_1, ncols, matrix_1, /*mask=*/NULL, /*empty-value=*/default_value_float__, /*isTo_init=*/true, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/true, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/(nrows_1 + nrows_2 +ncols));
  const uint globalIndexOfset_rowId = self->globalIndexOfset_rowId;
  const uint globalIndexOfset_columnId = self->globalIndexOfset_columnId;
  //!
  //! Validate that the name-space is itnaited as expected:
  assert(globalIndexOfset_rowId == 0);
  assert(globalIndexOfset_columnId == nrows_1);

  //!
  //! Then comptue the data-set for matrix_1
  //fprintf(stderr, "\n\n\n # First disjoint-forest-ops, at %s:%d\n", __FILE__, __LINE__);
  graph_disjointForests__s_kt_forest_findDisjoint(self, /*isTo_identifyCentrlaityVertex=*/false);

  //!
  //! Case: matrix_2:
  const uint cnt_vertices = nrows_1 + nrows_2 + ncols;
  allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(self, nrows_2, ncols, matrix_2, /*mask=*/NULL, /*empty-value=*/default_value_float__, /*isTo_init=*/false, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/false, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/cnt_vertices);
  //!
  //! Validate that the name-space is itnaited as expected:
  assert(self->globalIndexOfset_rowId == (nrows_1 + ncols));
  assert(self->globalIndexOfset_columnId == nrows_1);
  assert(self->globalIndexOfset_columnId == globalIndexOfset_columnId);

  //!
  //! Then comptue the data-set for matrix_2
  //fprintf(stderr, "\n\n\n # Last disjoint-forest-ops, at %s:%d\n", __FILE__, __LINE__);
  graph_disjointForests__s_kt_forest_findDisjoint(self, /*isTo_identifyCentrlaityVertex=*/false);
}



//! De-allcoates an object of type s_kt_forest_findDisjoint
void free_s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self) {
  assert(self);
  //! ------------
  //! De-allocate the 'stacks':
  free_s_kt_set_1dsparse_t(&(self->vertexStack_child));
  free_s_kt_set_1dsparse_t(&(self->vertexStack_parent));
  free_s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection));

  //! De-allcoat ehte oridnary lists:
  //printf("de-allcoates memory, at %s:%d\n", __FILE__, __LINE__);
  if(self->mapOf_vertexMembershipId) {free_1d_list_uint(&(self->mapOf_vertexMembershipId)); self->mapOf_vertexMembershipId = NULL;}
  if(self->mapOf_disjointForest_vertexId) {free_1d_list_uint(&(self->mapOf_disjointForest_vertexId)); self->mapOf_disjointForest_vertexId = NULL;}
  if(self->mapOf_vertexMembershipId_childsAreAdded) {free_1d_list_char(&(self->mapOf_vertexMembershipId_childsAreAdded)); self->mapOf_vertexMembershipId_childsAreAdded = NULL;}
  //if(self->_maskIsAllocated && self->mask) {free_2d_char(&(self->mask)); self->mask = NULL;}
}

/**
   @brief idnetifies the forest-id assicated to a given vertex.
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <vertex_id> is the vertex to get the dsijoint-forest for
   @return the forest-id in qestion.
 **/
const uint get_foreestId_forVertex__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint vertex_id) {
  assert(self);
  assert(self->mapOf_nameSpaceDomain_offset_rows_cnt == 0); 
  assert(self->mapOf_nameSpaceDomain_offset_columns_cnt == 0); 
  if(vertex_id < self->mapOf_vertexMembershipId_size) {
    return self->mapOf_vertexMembershipId[vertex_id];
  } else {return UINT_MAX;}
}
/**
   @brief idnetifies the forest-id assicated to a given vertex.
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <vertex_id> is the vertex to get the dsijoint-forest for
   @param <datasetId> ist he isneriton-point of the data-set which is evaluated
   @param <vertex_describesRow> which fi set to false impleis taht we ivnestigate the name-space for the column
   @return the forest-id in qestion.
   @remarks in cotnrast to the "get_foreestId_forVertex__s_kt_forest_findDisjoint(..)" this funciton re-re-maps the vertex_id into a global namespcae-id, ie, to handle the case where muliple vertices are given as inpuut to the data-set.
 **/
const uint get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint vertex_id, const uint datasetId, const bool vertex_describesRow) {
  assert(self);
  uint global_vertex;
  if(vertex_describesRow) {global_vertex = self->mapOf_nameSpaceDomain_offsetEnd_rows[datasetId] + vertex_id;}
  else  {
    /* printf("offset=%u, self->mapOf_nameSpaceDomain_offset_columns_cnt=%u, at %s:%d\n",  */
    /* 	   self->mapOf_nameSpaceDomain_offsetEnd_columns[datasetId], */
    /* 	   self->mapOf_nameSpaceDomain_offset_columns_cnt, __FILE__, __LINE__); */
    global_vertex = self->mapOf_nameSpaceDomain_offsetEnd_columns[datasetId] + vertex_id;}
  if(global_vertex == UINT_MAX) {
    fprintf(stderr, "!!\t Seems like your requested vertex=%u was Not found in datasetId=%u given vertex_describesRow='%s', ie, pelase investgiation. Observation at [%s]:%s:%d\n", vertex_id, datasetId, (vertex_describesRow) ? "true" : "false", __FUNCTION__, __FILE__, __LINE__);
  }
  assert(global_vertex != UINT_MAX); //! ie, as we otherwise woudl have a bug.
  

  //! @return the result:
  /* printf("access-pointer=%p, at %s:%d\n", self->mapOf_vertexMembershipId, __FILE__, __LINE__); */
  /* printf("mapOf_vertexMembershipId=%u given global_vertex=%u<%u, vertex-input-id=%u,  and datasetId=%u, at %s:%d\n",  */
  /* 	 //(uint)INFINITY, */
  /* 	 self->mapOf_vertexMembershipId[global_vertex], */
  /* 	 global_vertex, self->mapOf_vertexMembershipId_size, vertex_id, datasetId, __FILE__, __LINE__); */
  assert_possibleOverhead(global_vertex < self->mapOf_vertexMembershipId_size); //! ie, as we otherwise expoect 'having a memroy-error'.
  return self->mapOf_vertexMembershipId[global_vertex];
}
/**
   @brief construct a list of vertex-members for the forest-id in qeustion.
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call
   @param <arrOf_result_vertexMembers> is the dense list of vertex-members in forest_id
   @param <scalar_arrOf_result_vertexMembers> is the number of vertices/elmenets in "arrOf_result_vertexMembers"
   @remarks the funciton may be used as part of a do...while(..) loop in order for seperate clsuter-analsysis of disfjoitn-forests (eg,w rt. alll-agianst-all corrleaiton-analsysis and/or k-means clsteruiign-optmizaiotn).
 **/
void cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint **arrOf_result_vertexMembers, uint *scalar_arrOf_result_vertexMembers) {
  assert(self);
  assert(*arrOf_result_vertexMembers == NULL); //! ie, to aovid voerwriting exisiting memory--allcoaitons.
  uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
  if(list_size) {
    const uint default_value_uint = 0;
    *arrOf_result_vertexMembers = allocate_1d_list_uint(list_size, default_value_uint);
    memcpy(*arrOf_result_vertexMembers, list, sizeof(uint)*list_size);
    *scalar_arrOf_result_vertexMembers = list_size;
    assert(*arrOf_result_vertexMembers != NULL);
  } else {
    *scalar_arrOf_result_vertexMembers = 0; arrOf_result_vertexMembers = NULL;
  }
}


/**
   @brief extends the "cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(..)" to infer for cases hwere the name-spacess differs (eosketh, 06. des. 2016).
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call
   @param <arrOf_result_vertexMembers_row> is the dense list of vertex-members in forest_id: for rows
   @param <scalar_arrOf_result_vertexMembers_row> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for rows
   @param <arrOf_result_vertexMembers_col> is the dense list of vertex-members in forest_id: for cols
   @param <scalar_arrOf_result_vertexMembers_col> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for cols
   @remarks to itnaite this object please call eitehr "allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(..)" or a prmatuion fo the latter.
 **/
void cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint **arrOf_result_vertexMembers_row, uint *scalar_arrOf_result_vertexMembers_row, uint **arrOf_result_vertexMembers_col, uint *scalar_arrOf_result_vertexMembers_col) {
  assert(self);
  assert(*arrOf_result_vertexMembers_row == NULL); //! ie, to aovid voerwriting exisiting memory--allcoaitons.
  assert(*arrOf_result_vertexMembers_col == NULL); //! ie, to aovid voerwriting exisiting memory--allcoaitons.
  const uint globalStartPos_offset1 = get_internalStartPos_of_verticesFrom_dataSetId__s_kt_forest_findDisjoint_t(self, /*datasetId=*/1, /*for_rows=*/false);
  if( (globalStartPos_offset1 == 0) || (globalStartPos_offset1 == UINT_MAX) ) {
    fprintf(stderr, "!!\t Seems like this fucntion is being used on a wrongly itnaited s_kt_forest_findDisjoint_t object: please reade the documetnaiton: if the leatter does not help then please cotnac the developer at [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
  if(list_size) {
    const uint default_value_uint = 0;
    //! Fidn the nubmer fo elments above-and-below the offset-threshold:
    uint cnt_beloveOffset = 0;     uint cnt_aboveOffset = 0;
    for(uint i = 0; i < list_size; i++) {
      assert(list[i] != UINT_MAX);
      if(list[i] < globalStartPos_offset1) {
	cnt_beloveOffset++;
      } else {
	cnt_aboveOffset++;
      }
    }
    assert( (cnt_aboveOffset + cnt_beloveOffset) == list_size);
    *arrOf_result_vertexMembers_row = allocate_1d_list_uint(cnt_beloveOffset, default_value_uint);
    *arrOf_result_vertexMembers_col = allocate_1d_list_uint(cnt_aboveOffset, default_value_uint);
    //! Updte the szies:
    *scalar_arrOf_result_vertexMembers_row = cnt_beloveOffset;
    *scalar_arrOf_result_vertexMembers_col = cnt_aboveOffset;
    { //! Add the values:
      uint *row = *arrOf_result_vertexMembers_row;
      uint *col = *arrOf_result_vertexMembers_col;
      cnt_beloveOffset = 0; cnt_aboveOffset = 0; //! ie, to simplfiy our [”elow] lgocis:
      for(uint i = 0; i < list_size; i++) {
	assert(list[i] != UINT_MAX);
	if(list[i] < globalStartPos_offset1) {
	  row[cnt_beloveOffset] = list[i];
	  cnt_beloveOffset++;
	} else {
	  col[cnt_aboveOffset] = list[i] - globalStartPos_offset1; //! ie, to 'ge't the input-data-value
	  cnt_aboveOffset++;
	}
      }
    }    
    assert(*arrOf_result_vertexMembers_row != NULL);
    assert(*arrOf_result_vertexMembers_col != NULL);
  } else {
    *scalar_arrOf_result_vertexMembers_row = 0; *arrOf_result_vertexMembers_row = NULL;
    *scalar_arrOf_result_vertexMembers_col = 0; *arrOf_result_vertexMembers_col = NULL;
  }
}

/**
   @brief extends the "cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(..)" to 'handle' the sue-case where we have two input-matrices and where the 'elements' are merged in the column-interseciton-point (oekseth, 06. des. 2016).
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call
   @param <arrOf_result_vertexMembers_row_1> is the dense list of vertex-members in forest_id: for rows in matrix_1
   @param <scalar_arrOf_result_vertexMembers_row_1> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for rows in matrix_1
   @param <arrOf_result_vertexMembers_row_2> is the dense list of vertex-members in forest_id: for rows in matrix_2
   @param <scalar_arrOf_result_vertexMembers_row_2> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for rows in matrix_2
   @param <arrOf_result_vertexMembers_col> is the dense list of vertex-members in forest_id: for cols
   @param <scalar_arrOf_result_vertexMembers_col> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for cols
   @remarks to itnaite this object please call eitehr "allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(..)" or a prmatuion fo the latter.
 **/
void cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint **arrOf_result_vertexMembers_row_1, uint *scalar_arrOf_result_vertexMembers_row_1, uint **arrOf_result_vertexMembers_row_2, uint *scalar_arrOf_result_vertexMembers_row_2, uint **arrOf_result_vertexMembers_col, uint *scalar_arrOf_result_vertexMembers_col) {
  assert(self);
  assert(*arrOf_result_vertexMembers_row_1 == NULL); //! ie, to aovid voerwriting exisiting memory--allcoaitons.
  assert(*arrOf_result_vertexMembers_row_2 == NULL); //! ie, to aovid voerwriting exisiting memory--allcoaitons.
  assert(*arrOf_result_vertexMembers_col == NULL); //! ie, to aovid voerwriting exisiting memory--allcoaitons.
  const uint globalStartPos_offset1 = get_internalStartPos_of_verticesFrom_dataSetId__s_kt_forest_findDisjoint_t(self, /*datasetId=*/1, /*for_rows=*/false);
  const uint globalStartPos_offset2 = get_internalStartPos_of_verticesFrom_dataSetId__s_kt_forest_findDisjoint_t(self, /*datasetId=*/2, /*for_rows=*/true);
  if( (globalStartPos_offset1 == 0) || (globalStartPos_offset1 == UINT_MAX) ) {
    fprintf(stderr, "!!\t Seems like this fucntion is being used on a wrongly itnaited s_kt_forest_findDisjoint_t object: please reade the documetnaiton: if the leatter does not help then please cotnac the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if( (globalStartPos_offset2 == 0) || (globalStartPos_offset2 == UINT_MAX) ) {
    fprintf(stderr, "!!\t Seems like this fucntion is being used on a wrongly itnaited s_kt_forest_findDisjoint_t object: please reade the documetnaiton: if the leatter does not help then please cotnac the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if(globalStartPos_offset1 >= globalStartPos_offset2) {
    fprintf(stderr, "!!\t(possible-error): seems like you intiated the column-intersection-poitn After your intlaised the row-interseiocnt-point (in your second inptu-matrix), ie, there might be a bug (thoguh not nesscrarly): in brief we woudl be thanfkul if you could follwo the adviced intiation-proceudre (when suing a strucutr-efunction of this type), ie, to increase the chance that yoru results are correct. To summarize please reade the documetnaiton: if the leatter does not help then please cotnac the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
  if(list_size) {
    const uint default_value_uint = 0;
    //! Fidn the nubmer fo elments above-and-below the offset-threshold:
    uint cnt_beloveOffset = 0;    
    uint cnt_aboveOffset_col = 0;
    uint cnt_aboveOffset_row2 = 0;
    for(uint i = 0; i < list_size; i++) {
      assert(list[i] != UINT_MAX);
      if(list[i] < globalStartPos_offset1) {
	cnt_beloveOffset++;
      } else if(list[i] < globalStartPos_offset2) {
	cnt_aboveOffset_col++;
      } else {
	cnt_aboveOffset_row2++;
      }
    }
    assert( (cnt_aboveOffset_col + cnt_beloveOffset + cnt_aboveOffset_row2) == list_size);
    *arrOf_result_vertexMembers_row_1 = allocate_1d_list_uint(cnt_beloveOffset, default_value_uint);
    *arrOf_result_vertexMembers_row_2 = allocate_1d_list_uint(cnt_aboveOffset_row2, default_value_uint);
    *arrOf_result_vertexMembers_col   = allocate_1d_list_uint(cnt_aboveOffset_col, default_value_uint);
    //! Updte the szies:
    *scalar_arrOf_result_vertexMembers_row_1 = cnt_beloveOffset;
    *scalar_arrOf_result_vertexMembers_row_2 = cnt_aboveOffset_row2;
    *scalar_arrOf_result_vertexMembers_col   = cnt_aboveOffset_col;
    { //! Add the values:
      uint *row   = *arrOf_result_vertexMembers_row_1;
      uint *row_2 = *arrOf_result_vertexMembers_row_2;
      uint *col = *arrOf_result_vertexMembers_col;
      cnt_beloveOffset = 0; cnt_aboveOffset_col = 0; cnt_aboveOffset_row2 = 0; //! ie, to simplfiy our [”elow] lgocis:
      for(uint i = 0; i < list_size; i++) {
	assert(list[i] != UINT_MAX);
	if(list[i] < globalStartPos_offset1) {
	  row[cnt_beloveOffset] = list[i];
	  cnt_beloveOffset++;
	} else if(list[i] < globalStartPos_offset2) {
	  col[cnt_aboveOffset_col] = list[i] - globalStartPos_offset1; //! ie, to 'ge't the input-data-value
	  cnt_aboveOffset_col++;
	} else {
	  row_2[cnt_aboveOffset_row2] = list[i] - globalStartPos_offset2; //! ie, to 'ge't the input-data-value
	  cnt_aboveOffset_row2++;
	}
      }
    }    
    assert(*arrOf_result_vertexMembers_row_1 != NULL);
    assert(*arrOf_result_vertexMembers_row_2 != NULL);
    assert(*arrOf_result_vertexMembers_col != NULL);
  } else {
    *scalar_arrOf_result_vertexMembers_row_1 = 0; *arrOf_result_vertexMembers_row_1 = NULL;
    *scalar_arrOf_result_vertexMembers_row_2 = 0; *arrOf_result_vertexMembers_row_2 = NULL;
    *scalar_arrOf_result_vertexMembers_col = 0; *arrOf_result_vertexMembers_col = NULL;
  }
}

//! @return the total number of vertices involved in disjoitn forests.
//! @remarks expect teh number of identified vertices to correspond to the number of unique vertices assoicated to realtions.
const uint get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self) {
  uint cnt = 0;
  //const list_2d_uint *listOf_disjointForests = &self->listOf_membership_vertexCollection;

  //printf("\t cnt-possible-disjtoint-sets=%u, at %s:%d\n", self->listOf_membership_vertexCollection.biggestInserted_row_id, __FILE__, __LINE__);
  if(self->listOf_membership_vertexCollection.biggestInserted_row_id == UINT_MAX) {
    //assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
    fprintf(stderr, "!!\t Seems like no vertices were evalauted, ie, consider ivneigiating this case, an observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return 0;
  }

  for(uint disjointForest_id = 0; disjointForest_id < (self->listOf_membership_vertexCollection.biggestInserted_row_id+1); disjointForest_id++) {
    const uint forest_id = disjointForest_id;
    uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
    //printf("forest_id=%u has cnt=%u, at %s:%d\n", forest_id, list_size, __FILE__, __LINE__);
    cnt += list_size;
  }
  return cnt;
}

/**
   @brief identify the number of distjoint clsuters based on the min-max-filter
   @param <countThreshold_min> which is the numumum number of vertices which are to be in a given "idsjoitn forest" (ie, for the forest to be 'counted'): ignroed if countThreshold_min == countThreshold_max
   @param <countThreshold_max> similar to "countThreshold_min" with difference that we evlauate for the max-count: ignroed if countThreshold_min == countThreshold_max
   @return the total number of vertices involved in disjoitn forests.
   @remarks expect teh number of identified vertices to correspond to the number of unique vertices assoicated to realtions.
**/
const uint get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint countThreshold_min, const uint countThreshold_max) {
  assert(self);
  uint cnt = 0;
  //const list_2d_uint *listOf_disjointForests = &self->listOf_membership_vertexCollection;
  if(self->listOf_membership_vertexCollection.biggestInserted_row_id == UINT_MAX) {
    //assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
    if(self->statusCode != e_kt_forest_findDisjoint_statusCode_undef) {
      // printf("at %s:%d\n", __FILE__, __LINE__);
      return 0; //! ie, as we then assume that all vertices were 'ifnerred to be in the same disjoitn-froest'.
    } else {
      fprintf(stderr, "!!\t Seems like no vertices were evalauted, ie, consider ivneigiating this case: to help you (in your investgiation) note taht the current status-code='%u',  an observation at [%s]:%s:%d\n", self->statusCode, __FUNCTION__, __FILE__, __LINE__);
      assert(false); // TODO: remove
    }
    return 0;
  }
  //assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
  const uint cnt_disjoint_max = self->listOf_membership_vertexCollection.biggestInserted_row_id+1; //! ie, a max-estiamte 'as vertices may have been removed in the process of emrging disjotinf-reosts' (oekseth, 06. des. 201&).

  //printf("cnt_disjoint_max=%u, at %s:%d\n", cnt_disjoint_max, __FILE__, __LINE__);
  if(countThreshold_max != countThreshold_min) {

    for(uint disjointForest_id = 0; disjointForest_id < cnt_disjoint_max; disjointForest_id++) {
      const uint forest_id = disjointForest_id;
      uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
      bool isOf_interest = true;
      
      // printf("forest[%u] has cnt-element=%u, at %s:%d\n", disjointForest_id, list_size, __FILE__, __LINE__);

      if( (countThreshold_min != UINT_MAX) && (list_size < countThreshold_min) ) {
	isOf_interest = false;
      }
      if( (countThreshold_max != UINT_MAX) && (list_size > countThreshold_max) ) {
	isOf_interest = false;
      }
      if(isOf_interest && list_size) {cnt++;}
    }
  } else { //! then we assuem that all the disjoitn-forests are of interest.
    uint cnt_actual = 0;
    { //! Valdiate that each set has elemetns:
      for(uint disjointForest_id = 0; disjointForest_id < cnt_disjoint_max; disjointForest_id++) {
	const uint forest_id = disjointForest_id;
	uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
	//printf("forest[%u] has cnt-element=%u, at %s:%d\n", disjointForest_id, list_size, __FILE__, __LINE__);
	if(list_size != 0) {cnt_actual++;}
      }
      //printf("cnt_actual=%u, cnt_disjoint_max=%u, at %s:%d\n", cnt_actual, cnt_disjoint_max, __FILE__, __LINE__);
      assert(cnt_actual <= cnt_disjoint_max);
    }
    cnt = cnt_actual; //cnt_disjoint;
  }
  return cnt;
}


//! @return if the vertices in question are part of the same disjointForest
const bool partOf_same_disjointForest__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint key_1, const uint key_2) {  
  const bool has_same_id = (self->mapOf_vertexMembershipId[key_1] == self->mapOf_vertexMembershipId[key_2]);
  if(has_same_id) {
    if(self->mapOf_vertexMembershipId[key_1] != UINT_MAX) { 
      //! Then both vertices are assigned to the same SC, which we now know is specified/set.
      return true;
    } 
  }
  return false; // ie, as the vertices are not explictly declreared in the same SC    
}

//! @return a vertex which 'mauy be used as unification for the vertices in a given disjoint forest' (oekseth, 06. feb. 2016).
const uint get_centralityVertex_inDisjointforest__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint vertex_id) {
  const uint forest_id = self->mapOf_vertexMembershipId[vertex_id]; //.get_key();
  if(forest_id != UINT_MAX) {
    //assert(self->listOf_membership_vertexCollection[forest_id]._nrows > 0); //! ie, as we expec the 'froests' to be a dence list.
    //assert(self->listOf_membership_vertexCollection[forest_id]->template_get_value(/*index=*/0).is_empty() == false); //! ie, as a forest need to have at leas tone vertex to be a forest.
    const uint centrlaity_id_arbitrary = self->mapOf_disjointForest_vertexId[forest_id]; //listOf_membership_vertexCollection[forest_id]->get_meta_property().get_key();
    // assert(centrlaity_id_arbitrary != UINT_MAX); //! ie, as we expect it ot be set.
    //! @return the identified centrliaty-value:
    return centrlaity_id_arbitrary;
  } else {return UINT_MAX;} //! ie, as no centrliaty-keys was identifed, eg, for the case where the vertex was not par tof the disjoint forest.
}
//! Print the disjointForest-mappings
void print_disjointForestsMappings__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, char **mapOf_words) {
  assert(self);
  //const list_2d_uint *listOf_disjointForests = &self->listOf_membership_vertexCollection;
  //if(listOf_disjointForests && listOf_disjointForests->get_current_pos() ) {
  {
  assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
    for(uint disjointForest_id = 0; disjointForest_id < (self->listOf_membership_vertexCollection.biggestInserted_row_id+1); disjointForest_id++) {
      const uint forest_id = disjointForest_id;
      uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
      if(list_size > 0) {      
	printf("(putative-disjointForest-id:%u)\t", disjointForest_id);
	for(uint local_index = 0; local_index < list_size; local_index++) {
	  const uint vertex_id = list[local_index]; //listOf_disjointForests->get_list_object(disjointForest_id)->template_get_value(local_index).get_key();
	  printf("\"%s\"\t", mapOf_words[vertex_id]); //, __FILE__, __LINE__);
	}
	printf("\t at %s:%d\n", __FILE__, __LINE__);
      }
    }
  }
}

//! Print the disjointForest-mappings for each vetex
void print_disjointForestMappings_forEachVertex__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, char **mapOf_words) {
  assert(mapOf_words);
  assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
  for(uint this_key = 0; this_key < (self->listOf_membership_vertexCollection.biggestInserted_row_id+1); this_key++) {
    const uint disjointForest_id = self->mapOf_vertexMembershipId[this_key];
    if(disjointForest_id != UINT_MAX) {
      printf("\t vertex[%u]=\"%s\" is-part-of-disjointForest-id='%u', at %s:%d\n", this_key, mapOf_words[this_key], disjointForest_id, __FILE__, __LINE__);
    }
  }
}

//! A proceudre to simplify generic tests for itnersitng proceudre,s eg, wrt. DB-scan-root-nodes (oekseht, 06.- jul . 2017).
#define MF__dForest__isRootNode(self, this_key) ({if(self->obj_mapOf_rootVertices_pointer) {assert(this_key != UINT_MAX); assert(this_key < self->obj_mapOf_rootVertices_pointer->list_size);} bool isOf_interest = !self->obj_mapOf_rootVertices_pointer; if(!isOf_interest && self->obj_mapOf_rootVertices_pointer->list[this_key]) {isOf_interest = true;} isOf_interest;})
#define MF__dForest__isRootNode__child(self, child_key) ({if(self->obj_mapOf_rootVertices_pointer && self->obj_mapOf_rootVertices_pointer__forBothHeadAndTail) {assert(child_key != UINT_MAX); assert(child_key < self->obj_mapOf_rootVertices_pointer->list_size);} bool isOf_interest = !self->obj_mapOf_rootVertices_pointer; if(!isOf_interest && self->obj_mapOf_rootVertices_pointer->list[child_key]) {isOf_interest = true;} isOf_interest;})

//! Inser the children of this_key into the stack, ie, if any.
static void  __add_children_toStack__sparse(s_kt_forest_findDisjoint_t *self, const uint this_key) {
  if(MF__dForest__isRootNode(self, this_key)) {
    assert(self);
    assert(this_key < self->mapOf_vertexMembershipId_size); //! ie, as we expet the size to 'be inside the allocated threshold of entits'.
    assert(this_key >= self->globalIndexOfset_rowId);
    const uint this_key_local = this_key - self->globalIndexOfset_rowId;
    assert(this_key_local < self->_nrows);
    uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(self->listOf_relations, this_key_local, &list_size);
    if(list_size == 0) {
      return; // ie, as this_key does not have any explictly asosicated/related vertices.
    }
    /* const uint *list = (uint*)self->listOf_relations->get_sparseRow_ofIds__s_kt_set_2dsparse_t(this_key)->get_safe_list(); */
    /* const uint list_size = self->listOf_relations->get_sparseRow_ofIds__s_kt_set_2dsparse_t(this_key)->get_current_pos(); */
    if(list_size) {assert(list);}
    for(uint child_index = 0; child_index < list_size; child_index++) {
      const uint child_key_local = list[child_index];
      //printf("globalIndexOfset_columnId=%u, at %s:%d\n", self->globalIndexOfset_columnId, __FILE__, __LINE__);;
      const uint child_key = child_key_local + self->globalIndexOfset_columnId;
      if( (child_key != this_key) && MF__dForest__isRootNode__child(self, child_key) ) {
	//{printf("(relate-vertices)\t %u\t%u\t # at %s:%d\n", child_key, this_key, __FILE__, __LINE__);}
	push__s_kt_set_1dsparse_t(&(self->vertexStack_child), child_key);
	push__s_kt_set_1dsparse_t(&(self->vertexStack_parent), this_key);
	assert(this_key_local == this_key); // FIXME: remove when we start testing wrt. differnet name-name-spaces.
	assert(child_key_local == child_key); // FIXME: remove when we start testing wrt. differnet name-name-spaces.
	// printf("(add-to-stack)\t relation(%u, %u), at %s:%d\n", this_key, child_key, __FILE__, __LINE__);
      } //! else we asusem it is a reflecsive relationship, ie, for which we ignore the relationship
    }
  }
}

//! Inser the children of this_key into the stack, ie, if any.
static void __add_children_toStack__dense__float(s_kt_forest_findDisjoint_t *self, const uint this_key) {
  assert(self);
  assert(this_key < self->mapOf_vertexMembershipId_size); //! ie, as we expet the size to 'be inside the allocated threshold of entits'.
  assert(this_key >= self->globalIndexOfset_rowId);
  if(MF__dForest__isRootNode(self, this_key)) {
    const uint this_key_local = this_key - self->globalIndexOfset_rowId;
    //printf("this_key=%u, this_key_local=%u, nrows=%u, ncols=%u, at %s:%d\n", this_key, this_key_local, self->_nrows, self->_ncols, __FILE__, __LINE__);
    assert(this_key_local < self->_nrows);
    assert(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float);
    //! Iterate:
    uint cnt_inserted = 0;
#ifndef NDEBUG
    uint list_size_prev = 0;
    const uint *list = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&(self->vertexStack_child), &list_size_prev);
#endif
    for(uint child_key_local = 0; child_key_local < self->_ncols; child_key_local++) {
      if(self->matrix[this_key_local][child_key_local] != self->emptyValueIf_maskIsNotUsed) {      
	//printf("globalIndexOfset_columnId=%u, at %s:%d\n", self->globalIndexOfset_columnId, __FILE__, __LINE__);;
	const uint child_key = child_key_local + self->globalIndexOfset_columnId;
	if( (child_key != this_key) && MF__dForest__isRootNode__child(self, child_key) ) {
	//	if(child_key != this_key) {
	  //printf("\t\t add-for-child:\t child_key=%u, given parent=%u, at %s:%d\n", child_key, this_key, __FILE__, __LINE__);
	  // if(false && hash) {printf("(relate-vertices)\t %s\t%s\t # at %s:%d\n", hash->get_word(child_key), hash->get_word(this_key), __FILE__, __LINE__);}
	  push__s_kt_set_1dsparse_t(&(self->vertexStack_child), child_key);
	  push__s_kt_set_1dsparse_t(&(self->vertexStack_parent), this_key);
	  assert_possibleOverhead(has_vertex__s_kt_set_1dsparse_t(&(self->vertexStack_child), child_key));
	  cnt_inserted++;
	  /* assert(this_key_local == this_key); // FIXME: remove when we start testing wrt. differnet name-name-spaces. */
	  /* assert(child_key_local == child_key); // FIXME: remove when we start testing wrt. differnet name-name-spaces. */
	}
      }
#ifndef NDEBUG
      const uint cnt_expected = cnt_inserted + list_size_prev;
      if(cnt_expected) { //! Valdiate that the child-lsit ahs the expected size:
	uint list_size = 0; const uint *list = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&(self->vertexStack_child), &list_size);
	// printf("list_size=%u, cnt_inserted=%u, at %s:%d\n", list_size, cnt_inserted, __FILE__, __LINE__);
	assert(list_size == cnt_expected);
      }
      if(cnt_expected) { //! Valdiate that the parent-lsit ahs the expected size:
	uint list_size = 0;
	const uint *list = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&(self->vertexStack_parent), &list_size);
	assert(list_size == cnt_expected);
      }
#endif
    }
  }
}
//! Inser the children of this_key into the stack, ie, if any.
static void __add_children_toStack__dense__char(s_kt_forest_findDisjoint_t *self, const uint this_key) {
  if(MF__dForest__isRootNode(self, this_key)) {
    assert(self);
    assert(this_key < self->mapOf_vertexMembershipId_size); //! ie, as we expet the size to 'be inside the allocated threshold of entits'.
    assert(this_key >= self->globalIndexOfset_rowId);
    const uint this_key_local = this_key - self->globalIndexOfset_rowId;
    assert(this_key_local < self->_nrows);
    assert(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char);
    uint *mapOf_vertexMembershipId = self->mapOf_vertexMembershipId;
    //! Iterate:
    for(uint child_key_local = 0; child_key_local < self->_ncols; child_key_local++) {
      // printf("globalIndexOfset_columnId=%u, at %s:%d\n", self->globalIndexOfset_columnId, __FILE__, __LINE__);;
      const uint child_key = child_key_local + self->globalIndexOfset_columnId;
      if(self->mask[this_key_local][child_key_local] && (child_key != this_key)  && MF__dForest__isRootNode__child(self, child_key) ) {
	// if(false && hash) {printf("(relate-vertices)\t %s\t%s\t # at %s:%d\n", hash->get_word(child_key), hash->get_word(this_key), __FILE__, __LINE__);}
	push__s_kt_set_1dsparse_t(&(self->vertexStack_child), child_key);
	push__s_kt_set_1dsparse_t(&(self->vertexStack_parent), this_key);
      }
    }
  }
}
//!
//! Merge two sets of vertices using a spasrse list (oesketh, 06. feb. 2017):
s_kt_set_2dsparse_t static__merge_sets__kt_forest_findDisjoint(const uint this_key, const uint parent_key, uint *mapOf_vertexMembershipId, s_kt_set_2dsparse_t *listOf_membership_vertexCollection_ref) {
  //! -----------------------------------------------------------------------
  //!
  s_kt_set_2dsparse_t listOf_membership_vertexCollection = *listOf_membership_vertexCollection_ref;
#include "__stub__aux__mergeSets.c"
  return listOf_membership_vertexCollection; //! ie, where the latter is an udpated version/permtuation of "listOf_membership_vertexCollection_ref"
}

//! Evaluate all the descendents of the vertex (in question).
static void __strongConnect_childCall(s_kt_forest_findDisjoint_t *self, const uint this_key, const uint parent_key) {
  //! What we expect:
  assert(this_key != UINT_MAX);
  assert(parent_key != UINT_MAX);
  assert(this_key != parent_key); // ie, as we do not expect a self-cyclic relation.
  // if(false && hash) {printf("(connect-forests)\t %s\t%s\t # at %s:%d\n", hash->get_word(this_key), hash->get_word(parent_key), __FILE__, __LINE__);}

  //! Investigate if the vertex has already been updated:
  if(this_key >= self->mapOf_vertexMembershipId_size) {
    fprintf(stderr, "!!\t Investigate correctness of your call: this_key=%u >= max=%u, given nrows=%u, at %s:%d\n", this_key, self->mapOf_vertexMembershipId_size, self->_nrows, __FILE__, __LINE__);
  }
  assert(this_key < self->mapOf_vertexMembershipId_size); //! ie, as we otheriwe will have a memopry-corrutpion.
  const bool is_already_updated = self->mapOf_vertexMembershipId_childsAreAdded[this_key];
  //const bool is_already_updated = (self->mapOf_vertexMembershipId[this_key] != UINT_MAX);
  //!
  //! -----------------------------------------------------------------------
  //! ---------------- note: the [”elow] short-cuts are used iot. simplify integration/merign with our "kt_clusterAlg_kruskal.c" (eosketh, 06. feb. 2017).
  uint *mapOf_vertexMembershipId = self->mapOf_vertexMembershipId;
  s_kt_set_2dsparse_t listOf_membership_vertexCollection = self->listOf_membership_vertexCollection;
  //!
  //! Apply logics:
  listOf_membership_vertexCollection = static__merge_sets__kt_forest_findDisjoint(this_key, parent_key, mapOf_vertexMembershipId, &listOf_membership_vertexCollection);
  //!
  //! -----------------------------------------------------------------------
  //! ---------------- note: the [”elow] short-cuts are used iot. simplify integration/merign with our "kt_clusterAlg_kruskal.c" (eosketh, 06. feb. 2017).
  self->mapOf_vertexMembershipId = mapOf_vertexMembershipId;
  self->listOf_membership_vertexCollection = listOf_membership_vertexCollection;
  //! -----------------------------------------------------------------------
  //!  


  if( (self->typeOf_graphTraversalOrder == e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology) && (is_already_updated == false) ) {
    assert(self->mapOf_vertexMembershipId_childsAreAdded != NULL);
    self->mapOf_vertexMembershipId_childsAreAdded[this_key] = true; //! ie, 'state' that the children is added.
    // printf("--------\n#[%u]\t add-children, at %s:%d\n", this_key, __FILE__, __LINE__);
    //! Then we assume the children of this has not earlier been inserted:
    //! Note: insert the children of this_key into the stack, ie, if any.
    if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_sparseRelations) {
      __add_children_toStack__sparse(self, this_key);
    } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float) {
      // printf("add-children, at %s:%d\n", __FILE__, __LINE__);
      __add_children_toStack__dense__float(self, this_key);
    } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char) {
      __add_children_toStack__dense__char(self, this_key);
    } 
  /* } else { */
  /*   if(is_already_updated == false) { */
  /*     printf("--------\n#[%u]\t Not-add-children (as we from the cofngigruation-of-this-object asusems suchis Not of interest), at %s:%d\n", this_key, __FILE__, __LINE__); */
  /*   } */
  }

  // printf("\t(compare-forest::result)\t this[%u]='%u', parent[%u]='%u', at %s:%d\n", this_key, self->mapOf_vertexMembershipId[this_key], parent_key, self->mapOf_vertexMembershipId[parent_key], __FILE__, __LINE__);

  assert(self->mapOf_vertexMembershipId[this_key] != UINT_MAX);
  assert(self->mapOf_vertexMembershipId[parent_key] != UINT_MAX);
}





//! Evaluate all the descendents of the vertex (in question).
//! @return the number of evaluated vertices
static uint __strongConnect(s_kt_forest_findDisjoint_t *self, const uint this_key, const bool isCalledFromRoot) {
  assert(self);
  //const uint this_key = _this_key + self->globalIndexOfset_rowId;
  assert(this_key < self->mapOf_vertexMembershipId_size); //! ie, as we expet the size to 'be inside the allocated threshold of entits'.
  //! Investigate if the vertex has already been updated:
  const bool is_already_updated = self->mapOf_vertexMembershipId_childsAreAdded[this_key];
  // if(is_already_updated) {assert(self->mapOf_vertexMembershipId[this_key] != UINT_MAX);}

  //printf("##\nstrongConnect\tvertex=%u, at %s:%d\n", this_key, __FILE__, __LINE__);
  //! Note: in contrast to the "strongConnect_childCall(...)" function we do not set the forest-id directly. This in order to let any 'previously inserted vertices' with a better/lower idnex to be used, ie, by letting the parent-vertex update the parent-verex.
  
  /* { */
  /*   t_float score = T_FLOAT_MAX; */
  /*   const uint prev_parent = pop__s_kt_set_1dsparse_t(&(self->vertexStack_parent), &score); */
  /*   assert(prev_parent == UINT_MAX); //! ie, as we expec tthat he stack is empty. */
  /* } */
  
  if( 
     /* ( (self->typeOf_graphTraversalOrder == e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology) || isCalledFromRoot) &&  */
     (is_already_updated == false) ) {
    //! Insert the children of this_key into the stack, ie, if any.
    //printf("--------\n#[%u]\t add-children, at %s:%d\n", this_key, __FILE__, __LINE__);
    assert(self->mapOf_vertexMembershipId_childsAreAdded != NULL);
    self->mapOf_vertexMembershipId_childsAreAdded[this_key] = true; //! ie, 'state' that the children is added.
    if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_sparseRelations) {
      __add_children_toStack__sparse(self, this_key);
    } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float) {
      //printf("add-children, isCalledFromRoot=%u, this_key=%u, at %s:%d\n", isCalledFromRoot, this_key, __FILE__, __LINE__);
      __add_children_toStack__dense__float(self, this_key);
    } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char) {
      __add_children_toStack__dense__char(self, this_key);
    } 
    //__add_children_toStack(self, this_key);
  } 
  
  uint cnt_evaluated = 0; // ie, to include the count of 'this-key'

  uint prev_parent = UINT_MAX;
  bool haveEvaluated_thisKey = false;
  /* { */
  /*   uint list_size = 0; const uint *list = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&(self->vertexStack_child), &list_size); */
  /*   printf("\nlist_size=%u,  at %s:%d\n", list_size, __FILE__, __LINE__); */
  /* } */
  do {
    //while(self->vertexStack.get_current_pos() > 0){
    //! Get the current relation from the stack:
    t_float score = T_FLOAT_MAX;
    prev_parent = pop__s_kt_set_1dsparse_t(&(self->vertexStack_parent), &score);
    const uint prev_child = pop__s_kt_set_1dsparse_t(&(self->vertexStack_child), &score);    
    /* printf("(pop-from-stack)\t relation(%u, %u), at %s:%d\n", prev_parent, prev_child, __FILE__, __LINE__); */
    /* printf("\t\t prev_parent=%u, at %s:%d\n", prev_parent, __FILE__, __LINE__); */
    if(prev_parent != UINT_MAX) {
      //printf("(pop-from-stack)\t relation(%u, %u), at %s:%d\n", prev_parent, prev_child, __FILE__, __LINE__);
      assert(prev_child != UINT_MAX);
      assert(prev_child != prev_parent); // ie, as we do not expect a self-cyclic relation.
	
      /* const uint prev_pos = self->vertexStack.get_current_pos(); */
      /* const graph_relation relation = self->vertexStack.pop(); */
      /* const uint new_pos = self->vertexStack.get_current_pos(); */
      /* assert(new_pos < prev_pos); // ie, as we expect the [ªbove] call to have resulted in one relation/vertex being remopved from the stack. */	
      if(prev_child >= self->mapOf_vertexMembershipId_size) {
	fprintf(stderr, "!!\t Investigate correctness of your call: this_key=%u >= max=%u, given nrows=%u, and listOf_relations=%p and typeOf_graphTraversalOrder=%u, at %s:%d\n", prev_child, self->mapOf_vertexMembershipId_size, self->_nrows, self->listOf_relations, (uint)self->typeOf_graphTraversalOrder, __FILE__, __LINE__);
      }
      assert(prev_child < self->mapOf_vertexMembershipId_size); //! ie, as we otheriwe will have a memopry-corrutpion.

      //! Update the 'new' this-key:      
      // printf("\t Evaluates for child: %u, at %s:%d\n", prev_child, __FILE__, __LINE__);
      __strongConnect_childCall(self, prev_child, prev_parent); //relation.get_key_this(), relation.get_key_parent());
      assert(self->mapOf_vertexMembershipId[prev_parent] != UINT_MAX);
      assert(self->mapOf_vertexMembershipId[prev_child] != UINT_MAX);
      if(prev_parent == this_key) {haveEvaluated_thisKey = true;}
      cnt_evaluated++;
    } else { //! then we asusemt ahte stack is emptuy
      assert(prev_child == UINT_MAX); //! ie, as we otehrwise will have an in-conssnteyc.
    }
    // printf("\t\t prev_parent=%u, at %s:%d\n", prev_parent, __FILE__, __LINE__);
  } while(prev_parent != UINT_MAX);
  assert(prev_parent == UINT_MAX);

  if( (cnt_evaluated > 0) && (haveEvaluated_thisKey) ) {
    //if(!haveEvaluated_thisKey) {printf("cnt_evaluated=%u, at %s:%d\n", cnt_evaluated, __FILE__, __LINE__);}
    assert(haveEvaluated_thisKey);
    assert(self->mapOf_vertexMembershipId[this_key] != UINT_MAX);
  }

  // printf("\n\n--------------- (continues-at-next-vertex-in-list, at %s:%d\n", __FILE__, __LINE__);

  //! @return the number of evaluated vertices
  if(haveEvaluated_thisKey) {return cnt_evaluated;}
  else {return 0;} //! ie, as we then asume that 'none of the evlauted relationships' are assicated to 'this'.
}


//! Executes the algorithm for identidcation of disjoint-forests (oekseth, 06. otk. 2016).
//! @remarks is the 'main funciton' in the disjoint-forest comptuation/idneitficiaotn.
void graph_disjointForests__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const bool isTo_identifyCentrlaityVertex) {
  if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_sparseRelations) {
    assert(self->listOf_relations);  assert(self->listOf_relations->_nrows);
    if(!self->listOf_relations || !self->listOf_relations->_nrows) {
      fprintf(stderr, "(no-input-given)\t Not able to compute the set of disjoint forests, as no relations were specified, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return;
    }
  } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float) {
    if(!self->matrix || !self->_nrows || !self->_ncols) {
      fprintf(stderr, "(no-input-given)\t Not able to compute the set of disjoint forests, as no relations were specified, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return;
    }
  } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char) {
    if(!self->mask || !self->_nrows || !self->_ncols) {
      fprintf(stderr, "(no-input-given)\t Not able to compute the set of disjoint forests, as no relations were specified, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return;
    }
  }

  // printf("# cnt_elements=%u, at %s:%d\n", self->mapOf_vertexMembershipId_size, __FILE__, __LINE__);

  if( (self->statusCode == e_kt_forest_findDisjoint_statusCode_undef) ) { //! then we investigate if the data-set has some 'mepty values':

    //!
    //! Idneityf the nummber of empty valeus:
    uint cnt_empty_values = 0;
    if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_sparseRelations) {
      assert(self->listOf_relations);
      for(uint this_key = 0; this_key < self->_nrows; this_key++) {
	uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(self->listOf_relations, this_key, &list_size);
	if(list_size == 0) {
	  continue; // ie, as this_key does not have any explictly asosicated/related vertices.
	}
	if(list_size < self->_ncols) {cnt_empty_values += (self->_ncols - list_size);} //! ie, the 'number of columsn Not described'.
      }
    } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float) {
      for(uint this_key = 0; this_key < self->_nrows; this_key++) {
	for(uint child_key = 0; child_key < self->_ncols; child_key++) {	  
	  if(self->matrix[this_key][child_key] == self->emptyValueIf_maskIsNotUsed) { cnt_empty_values++;}
	}
      }
    } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char) {
      for(uint this_key = 0; this_key < self->_nrows; this_key++) {
	for(uint child_key = 0; child_key < self->_ncols; child_key++) {
	  if(self->mask[this_key][child_key] == false) { cnt_empty_values++;}
	}
      }
    }
    //! -----------------------------
    //! 
    //! Update the status-code:
    const bool has_emptyValues = (cnt_empty_values > 0); 
    if(has_emptyValues == true) {
      // fprintf(stderr, "(has-empty)\t at %s:%d\n", __FILE__, __LINE__);
      self->statusCode = e_kt_forest_findDisjoint_statusCode_emptyValues_some;
    } else { //! then we assume 'all valeus are connected'.
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      self->statusCode = e_kt_forest_findDisjoint_statusCode_emptyValues_none;
    }
  } else if(self->statusCode == e_kt_forest_findDisjoint_statusCode_emptyValues_none) {
#ifndef NDEBUG
    fprintf(stderr, "(no-disjoint-regions)\t Brief: we drop evlauation as we expect all of your vertices to be connected. Details: the previous call resulted in all entites 'being in the same cluster'. Hence we infer that all entites 'by defintion' are connected, ie, no point in wasting your time (by valdiating the latter assumption). For questions please contact the senior developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
#endif
    return; //! ie, as we assume 'this oepration has been succesfully completed'.
  }


  const uint nrows = self->_nrows;
  const uint default_cnt_elements = nrows;
  const uint default_value_uint = 0;

  uint __ncols = self->_ncols;
  if(self->listOf_relations) {
    __ncols = 0;
    for(uint this_key = 0; this_key < nrows; this_key++) {
      uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(self->listOf_relations, this_key, &list_size);
      for(uint child_index = 0; child_index < list_size; child_index++) {
	const uint child_key = list[child_index];
	  __ncols = macro_max(__ncols, child_key);
      }
    }
    __ncols += 1; //! ie, as the latter descirbe sth emax-index while we nee dth emax-size
    assert(__ncols > 0); //! ie, as we epcte there at least to be one relationship, ie, for 'a users call to Not be point-less'.    
  }

  if(self->mapOf_vertexMembershipId == NULL) {
    assert(self->mapOf_vertexMembershipId == NULL);
    assert(self->mapOf_vertexMembershipId_size != 0);
    //printf("nrows: %u == %u, at %s:%d\n", self->listOf_relations->_nrows, nrows, __FILE__, __LINE__);
    // if(self->listOf_relations) {assert(self->listOf_relations->_nrows < nrows);} //! ie, as there may be 'otubound non-repcircpaol relationships' (oesketh, 06. 10, 2019).
    //if(self->listOf_relations) {assert(self->listOf_relations->_nrows == nrows);}
    const uint cnt_elements = self->mapOf_vertexMembershipId_size;
    assert(cnt_elements != UINT_MAX);
    // printf("(allocate-mem):\tcnt_elements=%u, at %s:%d\n", cnt_elements, __FILE__, __LINE__);
    self->mapOf_vertexMembershipId = allocate_1d_list_uint(cnt_elements, default_value_uint);
    for(uint i = 0; i < self->mapOf_vertexMembershipId_size; i++) {self->mapOf_vertexMembershipId[i] = UINT_MAX;}

    assert(self->mapOf_vertexMembershipId_childsAreAdded == NULL);
    self->mapOf_vertexMembershipId_childsAreAdded = allocate_1d_list_char(cnt_elements, default_value_uint);
    for(uint i = 0; i < self->mapOf_vertexMembershipId_size; i++) {self->mapOf_vertexMembershipId_childsAreAdded[i] = false;}

    //! ------------
    //! De-allocate the 'stacks':
    free_s_kt_set_1dsparse_t(&(self->vertexStack_child));
    free_s_kt_set_1dsparse_t(&(self->vertexStack_parent));
    free_s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection));

    //! Therafter allcoate the set of stack-elements:
    allocate__s_kt_set_1dsparse_t((&self->vertexStack_child), default_cnt_elements, /*row=*/NULL, /*mask=*/NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/false);
    allocate__s_kt_set_1dsparse_t((&self->vertexStack_parent), default_cnt_elements, /*row=*/NULL, /*mask=*/NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/false);
    allocate__s_kt_set_2dsparse_t((&self->listOf_membership_vertexCollection), /*max-cnt-disjoitn-forestS=*/self->mapOf_vertexMembershipId_size, /*default_cnt_elements=*/nrows/10, /*matrix=*/NULL, /*mask=*/NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/false);
  } //! else we assume the data-sets have alrady been allcoated 'in a previosu run', eg, for the case wehre we are itnerested in merging muliple matrices for the smae vertex--feature marging, eg, for the case where we are itnereted in teh disjoitn vertex-sets wrt. a set of 'featurs thye have in common'.
  
  //! First identify the vertices which 'does not have any parents:
  uint mapOf_incomingRelationsCnt_size = nrows;
  const bool isTo_reverseRelationiships = isTo_reverseRelationiships_func(self);/* ( */
					   /* (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes)  || */
					   /* (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_increase)  || */
					   /* (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_decrease)   */
					   /* ); */
  
  // printf("isTo_reverseRelationiships=%u, at %s:%d\n", isTo_reverseRelationiships, __FILE__, __LINE__);
  if(isTo_reverseRelationiships) {
    mapOf_incomingRelationsCnt_size = __ncols;
    // printf("isTo_reverseRelationiships=%u, __ncols=%u, nrows=%u, listOf_relations=%p, at %s:%d\n", isTo_reverseRelationiships, __ncols, nrows, self->listOf_relations,  __FILE__, __LINE__);
  }
  uint *mapOf_incomingRelationsCnt = allocate_1d_list_uint(mapOf_incomingRelationsCnt_size, default_value_uint);
  if(self->typeOf_startVertices != e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder) { //! The operation:    
    ; // printf("build list-of-init-counts; applies sorting, at %s:%d\n", __FILE__, __LINE__);
#include "kt_forest_findDisjoint__func__findStartNodes.c" //! which invovles a feature-const and (if neccesary) a sort-rnak-procedure.
  } //! and at this exeuciton-point we know which relationships 'are the veritces which are not reachable from any other realtionships.
  
  uint cnt_evaluated = 0;
  const bool is_root = true;
  { //! Iterate through the start-nodes:
    if(self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes) { //! The operation:    
      const uint nrows_min = macro_min(mapOf_incomingRelationsCnt_size, nrows);
      for(uint this_key = 0; this_key < nrows_min; this_key++) {
	//printf("(evlauate)\t[%u], at %s:%d\n", this_key,  __FILE__, __LINE__);
	if(mapOf_incomingRelationsCnt[this_key] == UINT_MAX) {
	  const uint this_key_global = this_key + self->mapOf_nameSpaceDomain_offset_rows_cnt;
	  //! Then the vertex is a start-node, ie, we add it to the set:
	  assert(self->mapOf_vertexMembershipId[this_key] == UINT_MAX); // ie, as we expect a 'start-node' to not have been reached form an earlier vertex.	  
	  
	  // printf("call: __strongConnect(..), at %s:%d\n", __FILE__, __LINE__);
	  const uint cnt_local = __strongConnect(self, this_key_global, is_root);
	  //printf("\t[%u]\tcnt_local=%u, at %s:%d\n", this_key_global, cnt_local, __FILE__, __LINE__);
	  cnt_evaluated += cnt_local;
	  if(cnt_local > 0) {
	    assert(self->mapOf_vertexMembershipId[this_key_global] != UINT_MAX);
	  }
	}
      }
    } else if(self->typeOf_startVertices != e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder) { //! The operation:    
      const uint nrows_min = macro_min(mapOf_incomingRelationsCnt_size, nrows);
      for(uint i = 0; i < nrows_min; i++) {
	const uint this_key = mapOf_incomingRelationsCnt[i];
	//printf("(evlauate)\t[%u], at %s:%d\n", this_key,  __FILE__, __LINE__);
	if(this_key != UINT_MAX) {
	  //! Then the vertex is a start-node, ie, we add it to the set:
	  if(mapOf_incomingRelationsCnt[this_key] == UINT_MAX) {
	    //assert(self->mapOf_vertexMembershipId[this_key] == UINT_MAX); // ie, as we expect a 'start-node' to not have been reached form an earlier vertex.
	    const uint this_key_global = this_key + self->mapOf_nameSpaceDomain_offset_rows_cnt;
	    if(this_key_global >= self->mapOf_vertexMembershipId_size) {
	      fprintf(stderr, "!!\t Investigate correctness of your call: this_key=%u >= max=%u, given nrows=%u, at %s:%d\n", this_key, self->mapOf_vertexMembershipId_size, self->_nrows, __FILE__, __LINE__);
	    }
	    assert(this_key_global < self->mapOf_vertexMembershipId_size); //! ie, as we otheriwe will have a memopry-corrutpion.
	    // printf("call: __strongConnect(..), at %s:%d\n", __FILE__, __LINE__);
	    const uint cnt_local = __strongConnect(self, this_key_global, is_root);
	    //printf("\t[%u]\tcnt_local=%u, at %s:%d\n", this_key_global, cnt_local, __FILE__, __LINE__);
	    cnt_evaluated += cnt_local;
	    if(cnt_local > 0) {assert(self->mapOf_vertexMembershipId[this_key_global] != UINT_MAX);	  }
	  }
	}
      }
    } else { //! Then we use the intiaion-order determined by the index-order:
      //printf("------------------ at %s:%d\n", __FILE__, __LINE__);
      for(uint this_key = 0; this_key < nrows; this_key++) {
	// printf("(evlauate)\t[%u], at %s:%d\n", this_key,  __FILE__, __LINE__);
	const uint this_key_global = this_key + self->mapOf_nameSpaceDomain_offset_rows_cnt;
	//printf("(is-added='%s')\t #[%u], at %s:%d\n", (self->mapOf_vertexMembershipId_childsAreAdded[this_key_global]) ? "true" : "false", this_key, __FILE__, __LINE__);
	if( (self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false) 
	    || (self->mapOf_vertexMembershipId[this_key_global] == UINT_MAX) ) {
	  //if(self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false) {
	  //if(self->mapOf_vertexMembershipId[this_key] == UINT_MAX) {
	  // printf("call: __strongConnect(..), at %s:%d\n", __FILE__, __LINE__);
	  const uint cnt_local = __strongConnect(self, this_key_global, is_root);
	  cnt_evaluated += cnt_local;
	  if(cnt_local > 0) {assert(self->mapOf_vertexMembershipId[this_key_global] != UINT_MAX);	  }
	  //assert(self->mapOf_vertexMembershipId[this_key_global] != UINT_MAX);
	} //! else we asusme 'this' has been evlauted through an implict topology-traversal-step.
      }
    }
    free_1d_list_uint(&mapOf_incomingRelationsCnt); // ie, as we no longer need this structure.
  }
  


  { //! Then inspect the cases where the vertices may not have been reached, ie, due to a cycle, where there are no 'start nodes' as all vertices are both the (implict) child and parent vertex of the other (oekseth, 04. july 2015).
    //if( (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes) || (cnt_evaluated != nrows) ) 
    { //! The operation:    
      // printf("------------------ nrows=%u,  at %s:%d\n", nrows, __FILE__, __LINE__);
      for(uint this_key = 0; this_key < nrows; this_key++) {
	uint global_offset = (self->mapOf_nameSpaceDomain_offsetEnd_rows != NULL) ? self->mapOf_nameSpaceDomain_offsetEnd_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt] : 0; if(global_offset == UINT_MAX) {global_offset = 0;}
	const uint this_key_global = this_key + global_offset;
	// printf("this_key=%u, this_key_global=%u, global_offset=%u, at %s:%d\n", this_key, this_key_global, global_offset, __FILE__, __LINE__);
	//printf("(is-added='%s')\t #[%u], at %s:%d\n", (self->mapOf_vertexMembershipId_childsAreAdded[this_key_global]) ? "true" : "false", this_key, __FILE__, __LINE__);
	const uint cnt_elements = self->mapOf_vertexMembershipId_size;	
	assert(this_key_global < cnt_elements);
	if( (self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false) 
	    || (self->mapOf_vertexMembershipId[this_key_global] == UINT_MAX) )
	  {
	    //if(self->mapOf_vertexMembershipId[this_key] == UINT_MAX) {
	    //! Then the vertex has not been earlier evaluated, ie, evaluate the vertex:
	    //printf("\n #[%u], at %s:%d\n", this_key, __FILE__, __LINE__);
	    //printf("this_key_global=%u, given this_key=%u and global_offset=%u, at %s:%d\n", this_key_global, this_key, global_offset, __FILE__, __LINE__);
	    //printf("call: __strongConnect(..), at %s:%d\n", __FILE__, __LINE__);
	    const uint cnt_local = __strongConnect(self, this_key_global, is_root);;
	    cnt_evaluated += cnt_local;
	    if(cnt_local > 0) {
	      assert(self->mapOf_vertexMembershipId[this_key_global] != UINT_MAX);
	      assert(self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == true);
	    }
	  }
      }
    } //! else we assume the complete set of vertices to have been evlauted wrt. the 'intaiteion-step'.
    //! Validate that all vertices has been assigned a forest-id:
    if(self->listOf_relations) {
      for(uint this_key = 0; this_key < nrows; this_key++) {
	const uint this_key_global = this_key + self->mapOf_nameSpaceDomain_offset_rows_cnt;
	//if(self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false) {
	if(self->mapOf_vertexMembershipId[this_key_global] == UINT_MAX) {
	  uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(self->listOf_relations, this_key, &list_size);
	  if(list_size > 0) {
	    assert(self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false);
	    assert(false); // ie, as this_key has not been assigned a disjoint-fores-ist.
	  } // else we assume the vertex is not assoicated with any synonyms, which may well be the case.
	}
      }
    } else {
      assert(self->matrix);
      assert(self->_ncols);
      for(uint this_key = 0; this_key < nrows; this_key++) {
	const uint this_key_global = this_key + self->mapOf_nameSpaceDomain_offset_rows_cnt;
	//if(self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false) {
	uint cnt_elements = 0;
	if(self->mapOf_vertexMembershipId[this_key_global] == UINT_MAX) {
	  uint list_size = 0; 
	  if(self->mask) {
	    const char *row = self->mask[this_key];
	    for(uint col_id = 0; col_id < self->_ncols; col_id++) {
	      if(row[col_id]) {cnt_elements++;}
	    }
	  } else {
	    const t_float *row = self->matrix[this_key];
	    for(uint col_id = 0; col_id < self->_ncols; col_id++) {
	      if(row[col_id] != self->emptyValueIf_maskIsNotUsed) {cnt_elements++;}
	    }
	  }
	  if(list_size != 0) {
	    assert(self->mapOf_vertexMembershipId_childsAreAdded[this_key_global] == false);
	  }
	  if(list_size > 0) {
	    assert(false); // ie, as this_key has not been assigned a disjoint-fores-ist.
	  } // else we assume the vertex is not assoicated with any synonyms, which may well be the case.
	} else {
	  // NotE: [”elow] test-code is currently omitted iot. simplify testign of mulipel names-acpes, where the latter may (among others) be 'activated' from "allocateAndApply__denseMatrix___differentNameSpaceInRowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(..)" (oesketh, 06.d es. 2016).
	  /* const bool child_is_added = self->mapOf_vertexMembershipId_childsAreAdded[this_key_global]; */
	  /* printf("evalautes for this_key_global=%u <? %u, at %s:%d\n", this_key_global, self->mapOf_nameSpaceDomain_offsetEnd_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt], __FILE__, __LINE__); */
	  /* if(this_key_global < self->mapOf_nameSpaceDomain_offsetEnd_rows[self->mapOf_nameSpaceDomain_offset_rows_cnt]) { */
	  /*   assert(child_is_added == true); */
	  /* } */
	}
      }
    }
  }

  /* if(true) { //! then we print ouyt the 'glboal mappings': */
  /*   const uint cnt_elements = self->mapOf_vertexMembershipId_size; */
  /*   printf("\n\n\n # Write otu the glboal dsijotin-emberships, at %s:%d\n", __FILE__, __LINE__); */
  /*   for(uint vertex_id = 0; vertex_id < cnt_elements; vertex_id++) { */
  /*     const uint forest_id = self->mapOf_vertexMembershipId[vertex_id]; */
  /*     if(forest_id != UINT_MAX) { */
  /* 	printf("vertex[%u].cluster=%u, at %s:%d\n", vertex_id, forest_id, __FILE__, __LINE__); */
  /*     } */
  /*   } */
  /* } */



  if(isTo_identifyCentrlaityVertex) { //! then 'specify' one centrlaity-vertex to each of the identified disjoint forests:
    //! Note: an applicaiton of the "isTo_identifyCentrlaityVertex" parameter conserns the merging/unifciaotn of complex networks, ie, where a 'centrliaty-node' may be used to replace a set of connected synonyms.
    //assert(self->listOf_membership_vertexCollection.get_current_pos());
    if(self->listOf_membership_vertexCollection.biggestInserted_row_id == UINT_MAX) {
      return; //!< casee added by (oekseth, 06. 01. 2019).
    }
    assert(self->listOf_membership_vertexCollection.biggestInserted_row_id != UINT_MAX);
    const uint cnt_forests = self->listOf_membership_vertexCollection.biggestInserted_row_id + 1;
    /* uint cnt_forests_actual = 0; */
    /* for(uint forest_id = 0; forest_id < cnt_forests; forest_id++) { */
    /*   uint list_size = 0;  const uint *list = self->listOf_membership_vertexCollection.get_sparseRow_ofIds__s_kt_set_2dsparse_t(this_key, &list_size); */
    /*   if(list_size > 0) {       */
    /* 	cnt_forests_actual++; */
    /*   } */
    /* } */
    assert(self->mapOf_disjointForest_vertexId == NULL); //! ie,as we expect 'this call' to have been made only once.
    assert(cnt_forests > 0);
    if(cnt_forests > 0) {
      assert(cnt_forests != UINT_MAX);
      assert(cnt_forests < 1000000); //! ie, as we otherwise might (not not necceary) have  abug.
      //printf("allocate for cnt_forests=%u, at %s:%d\n", cnt_forests, __FILE__, __LINE__);
      self->mapOf_disjointForest_vertexId = allocate_1d_list_uint(cnt_forests, default_value_uint);
      for(uint forest_id = 0; forest_id < cnt_forests; forest_id++) {
	uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->listOf_membership_vertexCollection), forest_id, &list_size);
	if(list_size > 0) {      
	  /* 	c */
	  /* assert(self->listOf_membership_vertexCollection[forest_id]); //! ie, as we expec the 'froests' to be a dence list. */
	  /* assert(self->listOf_membership_vertexCollection[forest_id]->template_get_value(/\*index=*\/0).is_empty() == false); //! ie, as a forest need to have at leas tone vertex to be a forest. */
	  const uint centrlaity_id_arbitrary = list[0]; //self->listOf_membership_vertexCollection[forest_id]->template_get_value(/*index=*/0).get_key();
	  //! Specify/set the 'arbitrary chosen forest-vertex-id':
	  self->mapOf_disjointForest_vertexId[forest_id] = centrlaity_id_arbitrary;
	  /* 	self->listOf_membership_vertexCollection.get_unsafe_list_object(forest_id)->get_unsafe_meta_property()->set_key(centrlaity_id_arbitrary); */
	  /* //!What we epxect from the [ªbove] insertion: */
	  /* assert(centrlaity_id_arbitrary == self->listOf_membership_vertexCollection[forest_id]->get_meta_property().get_key()); */
	}
      }
    } else {
      fprintf(stderr, "(info)\t No forests identiifed, ie, please investigate this issue (an issue which could be due to the fact that there were not any interesting valeus in your data-set): for questions please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
  }
  /* if(false) { */
  /*   printf("(info)\t Inspectee %u 'inner' vertices, given a total of %u vertices, resulting in %u dsijoint-forests, at %s:%d\n", */
  /* 	   cnt_evaluated, self->listOf_relations->get_current_pos(), self->listOf_membership_vertexCollection.get_current_pos(), __FILE__, __LINE__); */
  /* } */


  // assert(false); // TODO: consider inlcuding [”elow] 'cases'.

  // assert(cnt_evaluated >= self->listOf_relations->get_current_pos()); // ie, as we expect that at least the number of vertices with children have been evaluated.
  /* //! Finalize the operation: */
  /* const bool is_ok = self->listOf_membership_vertexCollection.apply_merge_sort(); */
  /* assert(is_ok); */
  /* assert(self->listOf_membership_vertexCollection.is_sorted()); // ie, our expectaiotn given the [ªbove] call. */
}



/**
   @brief identify the number of disjoitn clusters in a matrix (oekseth,. 06. okt. 2016)
   @param <nrows> is the number of row in the amtrix
   @param <ncols> is the number of columns in the matrix
   @param <matrix> is score-matrix of vertices: note that oru assumption of 'score matrix' is a generalizaiton of an adjcency-matrix, ie, where '0' is considered to reflect a relationship: in order to validate that thios interpretation is proerply understood we inlcud ehte "isTo_useImplictMask", where this proerpty si to be set to true if "mask == NULL", ie, to increase thte change that the funciton is properly used.
   @param <mask> if set then we combien the '0' filter with the "mask[i][j] == 1" test
   @param <isTo_useImplictMask> which if set to true impleis that we investigate for valeus which have "T_FLOAT_MAX" (and if a cell is set to the altter valeu then we asusme the lvaue is Not of interest);
   @param <countThreshold_min> which is the numumum number of vertices which are to be in a given "idsjoitn forest" (ie, for the forest to be 'counted'): ignroed if countThreshold_min == countThreshold_max
   @param <countThreshold_max> similar to "countThreshold_min" with difference that we evlauate for the max-count: ignroed if countThreshold_min == countThreshold_max
   @param <inputMatrix__isAnAdjecencyMatrix> which is used to investgate/'find' the 'interpretiaotn-case' wrt. the matrix-names-space to use
   @return the number of interesting disjoitn clsuters.
   @remarks 
   - if valeu-fitlering is of itnerest then we suggest using oru rich library of mask-filters, eg, wrt. functions found in our "mask_api.h";
   - no-match: use T_FLOAT_MAX if "mask == NULL", ie, a '0' is interpred as a relationship (ie, as stated in above paramter-description);
 **/
uint find_cntClusters_inMatrix__s_kt_forest_findDisjoint(const uint nrows, const uint ncols, t_float **matrix, char **mask, const bool isTo_useImplictMask, const uint countThreshold_min, const uint countThreshold_max, const bool inputMatrix__isAnAdjecencyMatrix) {
  assert(nrows); assert(ncols); assert(matrix); 
  if(mask == NULL) {
    if(isTo_useImplictMask == false) {
#ifndef __config_kt_forest_findDisjoint__hideErrorMEssage
      fprintf(stderr, "!!\t A pointless call: you have 'stated' that all valeus are of itnerest (ie, as the mask nor the isTo_useImplictMask is used). To understand the latter note that if all values are of interest, then there is no point in comptuing the number of disjoitn forests (ie, as we from Luvig Holberg and the \"Mor Lille is a stone\" story-logic infer that there will always be only one disjoitn cluster, ie, a pointless call: for quesitons please cotnact the debeloepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false);
#endif //! else we asusme a fucnion explcitly investigate this case.
      return 1;
    }
  } else {
    if(isTo_useImplictMask == true) {
      fprintf(stderr, "!!\t An ambigious call: you have 'stated' that aboth masks and isTo_useImplictMask is to be evaluated: to simplify our work we suggest you use only ofe of the laternatives, ie, as we now will choose one of them: for quesitons please cotnact the debeloepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
  }
  if(!matrix || !nrows || !ncols) {
    fprintf(stderr, "!!\t A pointless call: you have not specified both an inptu-matrix and a (nrows, ncols) distance-proeprties, ie, a pointless call: for quesitons please cotnact the debeloepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return UINT_MAX; //! ie, as we have no idea upon the number of clusters.
  }
  //! ----------------------------------------------------------------------------------------------------------------------------------------
  //s_kt_set_2dsparse_t listOf_relations; allocate__s_kt_set_2dsparse_t(&listOf_relations, nrows, ncols, matrix, mask, isTo_useImplictMask, /*isTo_allocateFor_scores=*/false);
  s_kt_forest_findDisjoint_t obj_disjoint; 
  //allocate__s_kt_forest_findDisjoint(&obj_disjoint, nrows, &listOf_relations);

  uint *map_vertexGroups_cols = NULL; uint map_vertexGroups_cols_size = 0;
  bool data_isUpdated = false;
  const t_float __localConfig__specificEmptyValue = (isTo_useImplictMask) ? T_FLOAT_MAX : 0;
  //  if(config.isTo_applyDisjointSeperation) 

  uint cnt_interesting_disjointForests = UINT_MAX;
  if(inputMatrix__isAnAdjecencyMatrix) {
    assert(nrows == ncols);
    //! ---
    //assert(matrix_disjointInput->nrows == matrix_disjointInput->ncols);     
    //const uint nrows = matrix_disjointInput->nrows;
    allocate__denseMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows, nrows, matrix, /*mask=*/mask, /*emptyValueIf_maskIsNotUsed=*/__localConfig__specificEmptyValue);
    //! Idneitfy the disjoint regiosn:
    graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
    //!
    //! Iterate through the set of disjotin sets
    cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, countThreshold_min, countThreshold_max);
  } else {
    assert(nrows > 0);     assert(ncols > 0);
    /* map_vertexGroups_cols_size = ncols; */
    /* const t_float val_0 = 0; map_vertexGroups_cols = allocate_1d_list_uint(ncols, val_0); */
    /* for(uint i = 0; i < ncols; i++) {map_vertexGroups_cols[i] = UINT_MAX;} //! ie, 'reset'. */
    allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix, /*mask=*/mask, /*empty-value=*/__localConfig__specificEmptyValue, /*isTo_init=*/true, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/true, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/(nrows+ncols));
    //! Idneitfy the disjoint regiosn:
    graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
    //!
    //! Iterate through the set of disjotin sets
    cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, countThreshold_min, countThreshold_max);
  }
  assert(cnt_interesting_disjointForests != UINT_MAX); //! ie, as we expect the latter to have been set



  //! -----------------------------
  //! De-allocate:
  free_s_kt_forest_findDisjoint(&obj_disjoint);
  //free_s_kt_set_2dsparse_t(&listOf_relations);
  //! -----------------------------
  //! @return the result:
  return cnt_interesting_disjointForests;
}
