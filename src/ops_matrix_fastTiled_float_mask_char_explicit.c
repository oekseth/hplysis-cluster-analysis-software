//!
//! 
#define __funcName__fast float_mask_char_explicit_case_1_andSum
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); VECTOR_FLOAT_TYPE vec_mask_1 = VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(row_1_mask, int_j); VECTOR_FLOAT_TYPE vec_mask_2 = VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(row_2_mask, int_j); 
#define __MiF__ops_each_preStep_2 VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_1, vec_2, vec_mask_1, vec_mask_2, vec_mask_combined);
#define __MiF__ops_each VECTOR_FLOAT_ADD(vec_1, vec_2)
#include "ops_matrix_fastTiled_mask.c"
//!
//! 
	  
	  

#define __funcName__fast float_mask_char_explicit_case_2_andSum
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2 VECTOR_FLOAT_TYPE vec_localSum = VECTOR_FLOAT_ADD(vec_1, vec_2); VECTOR_FLOAT_charMask_pair_old(vec_localSum, row_1_mask, row_2_mask, int_j);
#define __MiF__ops_each vec_localSum
#include "ops_matrix_fastTiled_mask.c"
#if(false)
//!
//! 
#define __funcName__fast 
#define __MiF__ops_each_preStep_1 
#define __MiF__ops_each_preStep_2 
#define __MiF__ops_each VECTOR_FLOAT_ADD(vec_1, vec_2)
#include "ops_matrix_fastTiled_mask.c"
#endif
