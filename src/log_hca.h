#ifndef log_hca_h
#define log_hca_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


//#ifndef configIsCalledFromExternal_software
#include "def_intri.h"
#include "types.h"
#include "configure_hcaMem.h"
#include <time.h>
#include <signal.h>
#include <sys/times.h>
#include <time.h>
#include <stdio.h>
#include <sys/times.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//#endif //! else we assuem the caller uses a 'linker' to include 'these' (oekseth, 06. july 2016).

/**
   @file log_hca.h
   @brief a structure and lgoics to capture the details of our HCA-mem implementation.
   @author Ole Kristian Ekseth (oekseth)
 **/
/**
   @enum e_log_hca_typeOf
   @brief a set of ids which is used in our log_hca to describe variaous performance-cases.
   @author Ole Kristian Ekseth (oekseth)
   @remarks 
   -- "e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate": indicates that for "sinus" data-distributions the tiem-cost of 'imrpviing buckets' is in-signficant.
   -- "e_log_hca_typeOf_all_hcaSpecific_cpy_partialBuckets" wrt. our "hca_cluster.c" for n=16,000: data-types["sin":0, "rand":0, ""linear-differentCoeff-a":0].
 **/
typedef enum e_log_hca_typeOf {
  e_log_hca_typeOf_all, //! ie, the compelte exeuciton-time of our HCA-mem algorithm,
  e_log_hca_typeOf_init,
  e_log_hca_typeOf_init_details_minPairsEachColumnBucket_local,
  e_log_hca_typeOf_init_details_minPairsEachColumnBucket_global,
  e_log_hca_typeOf_all_hcaSpecific,
  e_log_hca_typeOf_all_hcaSpecific_max,
  e_log_hca_typeOf_all_hcaSpecific_cpy,
  e_log_hca_typeOf_all_hcaSpecific_cpy_partialBuckets,
  e_log_hca_typeOf_all_hcaSpecific_copyToHcaMem,
  e_log_hca_typeOf_all_dataUpdateCalls,
  e_log_hca_typeOf_all_dataUpdateCalls_getMinPos,
  e_log_hca_typeOf_numberOf_updates_fromFunc_updateRow,
  e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn,
  e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop,
  e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_isBetter,
  e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate,
  e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath,
  e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateRow,
  e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateColumn,
  e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_findMinRow,
  e_log_hca_typeOf_numberOf_updates_globalColumnTile, //! which is used in conjuciton with call to our "__forAll_update_globalMin_atBucketIndex(..)" function.
  e_log_hca_typeOf_numberOf_updates_copyContinousSections, //! ie, the number of updates which are 'an imrpovement ofthe local-min-values'
  //! Note: if new eums are added, then remember to update our "writeOut_object(..)" in our "log_hca.c"
  e_log_hca_typeOf_undef  
} e_log_hca_typeOf_t;

//! The __cplusplus macro macro was included in this document by oekseth to make it possible to link to KnittingTools C++-library.
#ifdef __cplusplus
extern "C"{
#endif 


/**
   @struct s_log_hca
   @brief a structure and lgoics to capture the details of our HCA-mem implementation.
   @author Ole Kristian Ekseth (oekseth)
   @remarks among other an object of this struct is used to 'remember' the signficant of different 'operation-types', eg, wrt. the cost of 'value-udpate due to shrinking matrix-size in/from our "s_dense_closestPair_getClosestPair(..)"'.
**/
typedef struct s_log_hca {
  struct tms _mapOf_times_internal_tms[e_log_hca_typeOf_undef]; //! ie, the number of measurements.
  clock_t _mapOf_times_internal_clock[e_log_hca_typeOf_undef]; //! ie, the number of measurements.
  //struct tms _mapOf_times_internal[e_log_hca_typeOf_undef]; //! ie, the number of measurements.
  t_float mapOf_times[e_log_hca_typeOf_undef];
  uint mapOf_counter[e_log_hca_typeOf_undef]; //! ie, counters assicated to a given "e_log_hca_typeOf" attribute.
} s_log_hca_t;


  //! Intiaite the object.
  void s_log_hca_init(s_log_hca_t *obj);

/**
   @brief Start the measurement with regard to time.
   @param <obj> is the object to update
   @param <enum_id> is attribute to update in obj.
**/
  void start_time_measurement_log_hca(s_log_hca_t *obj, const e_log_hca_typeOf_t enum_id);
/**
   @brief Ends the measurement with regard to time.
   @param <obj> is the object to update
   @param <enum_id> is attribute to update in obj.
**/
  void end_time_measurement_log_hca(s_log_hca_t *obj, const e_log_hca_typeOf_t enum_id);
  /**
     @brief update the "s_log_hca_t" object with counter (oekseth, 06. july 2016).
     @param <self> the object to hold the s_log_hca_t object.
     @param <enum_id> the log-type to update
     @param <cnt> if not set to "0" nor "UINT_MAX" then wwe use this valeu to update the assciated s_log_hca_t structure     
  **/
  void log_updateCounter(s_log_hca_t *self, const e_log_hca_typeOf_t enum_id, const uint cnt);
  /**
     @brief write out the resutls of the log_hca object.
     @param <obj> is the object to write out the contents for
     @param <stream> is the stream to write the result to, eg, STDOUT
   **/
  void writeOut_object(const s_log_hca_t *obj, FILE *stream);

#ifdef __cplusplus
}
#endif
#endif //! EOF

