    //! ----------------------
    for(uint dist_enum_id = 0; dist_enum_id < e_kt_correlationFunction_undef; dist_enum_id++) {
      assert(e_kt_categoryOf_correaltionPreStep_none == 2); //! ie, waht we expect wrt. [below] "3" threshold
      for(uint dist_enum_preStep = 0; dist_enum_preStep < 3; dist_enum_preStep++) {
	for(char config_weight = 0; config_weight < 2; config_weight++) {
	  for(char config_mask = 0; config_mask < 3; config_mask++) {
	    for(char config_useTwoMatricesAsInput = 0; config_useTwoMatricesAsInput < 2; config_useTwoMatricesAsInput++) {
	      for(char config_transpose = 0; config_transpose < 2; config_transpose++) {
		const bool transpose = config_transpose;

		if(transpose) {continue;} // FIXME: remvoe when other emmory-errors are resolved


		e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)dist_enum_id; e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;
		if(true == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id))  {continue;} // TODO: cosndier dropping 'this ciertiera'.
		if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == true) {continue;} // TODO: cosndier dropping 'this ciertiera'.

		//! -------------------------------------------
		//! Generate the measurement-test:
		char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
		char stringOf_measureText[6000]; memset(stringOf_measureText, '\0', 6000);
		sprintf(stringOf_measureText, "%s: \"%s -- %s\"; transpose='%s',  use-weight='%s', mask-type='%s', use-two-differentMatrices-as-inpnut='%s', %s", 
			stringOf_measureText_base, 
			get_stringOf_enum__e_kt_correlationFunction_t((e_kt_correlationFunction_t)dist_enum_id),
			get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t((e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep),
			(config_transpose) ? "true" : "false",
			(config_weight) ? "true" : "false",
			(config_mask == 2) ? "maskExplicit" : (config_mask == 1) ? "maskImplicit" : "mask-none",
			//(config_mask == 1) ? "maskExplicit" :  "mask-none",
			(config_useTwoMatricesAsInput) ? "true" : "false",
			stringOf_data_toAdd
			);

		t_float **local_matrix_1 = matrix;   t_float **local_matrix_2 = matrix;
		if(transpose == true) {
		  local_matrix_1 = matrix_transposed; local_matrix_2 = matrix_transposed;
		}
		t_float *local_weight = NULL;
		if(config_weight) {local_weight = weight;}
	  
		if(config_useTwoMatricesAsInput) {
		  if(transpose == false) {
		    local_matrix_2 = allocate_2d_list_float(nrows, size_of_array, default_value_float);
		  } else {
		    local_matrix_2 = allocate_2d_list_float(size_of_array, nrows, default_value_float);
		  }
		}
		char **local_mask1 = NULL; 	      char **local_mask2 = NULL;
		bool masks_isAllocated = false;
		e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true;
		if(config_mask == 0) {
		  masksAre_used = e_cmp_masksAre_used_false;
		} else if(config_mask == 2) {
		  if(transpose == false) {
		    local_mask1 = mask1; local_mask2 = mask2;
		  } else {
		    masks_isAllocated = true;
		    local_mask1 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
		    local_mask2 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
		  }
		}
		//! 
		//! Start the clock:
		start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
		//!
		//!
		//! The experiemnt:
		t_float sumOf_values = 0;

		// printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
#if(__localConfig__measureDelta__type__manyToMany == 1)
		t_float **matrix_result = NULL;


		if(transpose == false) { matrix_result = allocate_2d_list_float(nrows, ncols, default_value_float);}
		else {                   matrix_result = allocate_2d_list_float(ncols, nrows, default_value_float);}
		s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
		set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
		  //!
		  //! The call:
		  kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
		  //free_1d_list_float(&arrOf_result);
		  free_memory__s_allAgainstAll_config(&self);		  
		  free_2d_list_float(&matrix_result, ncols);
#else //! Then we need to tierate:
		for(uint row_id = 0; row_id < nrows; row_id++) {
#if(__localConfig__measureDelta__type__each == 1)
		  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
		    if(row_id_out == row_id) {continue;}
		    //! --------------
		    if(local_mask1) {
		      assert(local_mask2);
		      assert(local_mask1[row_id]); 	      assert(local_mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
		    }
		    //! --------------
		    printf("[%u][%u]\tcompare for 'each' using a fast correlation-emtric-approach, at %s:%d\n", row_id, row_id_out, __FILE__, __LINE__);
		    sumOf_values += kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2, local_mask1, local_mask2, local_weight, /*index1=*/row_id, /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 1), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
		    printf("[%u][%u]\t(completed)\tcompare for 'each' using a fast correlation-emtric-approach, at %s:%d\n", row_id, row_id_out, __FILE__, __LINE__);
		  }
#else
		  t_float *arrOf_result = allocate_1d_list_float(ncols, default_value_float);
		  s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
		  set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
		  kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  /*index1=*/row_id, &self, /*optionalStore_kendall=*/NULL, arrOf_result, /*metric_oneToMany_nonRank=*/NULL); // /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 1), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
		  free_1d_list_float(&arrOf_result);
		  free_memory__s_allAgainstAll_config(&self);
		  // void kt_compare__oneToMany(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, const uint index1, s_allAgainstAll_config_t *self, s_correlationType_kendall_partialPreCompute_kendall_t *optionalStore_kendall, t_float *arrOf_result, void (*metric_oneToMany_nonRank) (config_nonRank_oneToMany_t) );
#endif
		}
#endif
		//! --------------
		// printf("update time-measurement, at %s:%d\n", __FILE__, __LINE__);
		__assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
		if(config_useTwoMatricesAsInput) {
		  if(transpose == false) {
		    free_2d_list_float(&local_matrix_2, nrows);
		  } else {
		    free_2d_list_float(&local_matrix_2, size_of_array);
		  }
		}
		if(masks_isAllocated) {
		  if(transpose == false) {
		    free_2d_list_char(&local_mask1, nrows);
		    free_2d_list_char(&local_mask2, nrows);
		  } else {
		    free_2d_list_char(&local_mask1, size_of_array);
		    free_2d_list_char(&local_mask2, size_of_array);
		  }
		}
	      }
	    }
	  }
	}
      }
    }

#undef __localConfig__measureDelta__type__manyToMany
#undef __localConfig__measureDelta__type__each
