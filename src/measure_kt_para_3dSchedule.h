#ifndef measure_kt_para_3dSchedule_h
#define measure_kt_para_3dSchedule_h
 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file measure_kt_para_3dSchedule
   @brief an access-point to test sytnetically our "kt_para_3dSchedule"
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
#include "types.h"
#include "def_intri.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
//#include "log_clusterC.h"

//! The main test-function.
void measure_kt_para_3dSchedule__main(const int array_cnt, char **array);


#endif //! EOF
