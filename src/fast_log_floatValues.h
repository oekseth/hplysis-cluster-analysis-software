#ifndef fast_log_floatValues_h
#define fast_log_floatValues_h

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


#include "configure_optimization.h"

#ifndef optmize_use_logValues_sizeOf_logTable_zeroOnes
#error "Investigate this issue, ie, why the vairable is not defined"
#endif

static VECTOR_FLOAT_TYPE vec_fast_log_defaultValue;

//! ------------------------------------------
//! Note: teh latter optmize_use_logValues_sizeOf_logTable_zeroOnes is expected to be decrelared in our "configure_optimization.h"
static const uint static_listOf_values_size = optmize_use_logValues_sizeOf_logTable_zeroOnes; // 1000*10; //*100;  
static t_float *static_listOf_values; static t_float *static_listOf_values_shallowCopy_aboveZero; //! which is a reference to "static_listOf_values"
//! Note: ["elow] funciton is used to handle the 'case' of [-1, 1] value-range
// printf("val=%f, index_beforeOffset=%f, after=%d, at %s:%d\n", val, index_beforeOffset, (int)(index_beforeOffset+static_listOf_values_size), __FILE__, __LINE__); 
#define __get_internalIndexOf_float(val) ({ const float index_beforeOffset = (float)(val * (float)optmize_use_logValues_sizeOf_logTable_zeroOnes); (int)(index_beforeOffset + optmize_use_logValues_sizeOf_logTable_zeroOnes);})
#define __get_internalIndexOf_float_0_one(val) ({(int)(val * (float)optmize_use_logValues_sizeOf_logTable_zeroOnes);})
//! ------------------------------------------

//! ---------------------------
#if 1 == 1
#define log_range_vector_get_memoryLocation(vec) (VECTOR_FLOAT_MUL(vec, vec_fast_log_defaultValue))
#else
#define log_range_vector_get_memoryLocation(vec) (VECTOR_FLOAT_SET1(1))
#endif
//! ------------------------
//! ---- 
#if 1 == 1 //! where we observe that 'this approach' has a "1.2x" performance-overhead 'caused' by memory-accesses, ie, a higher 'cost' of this than the memory-accesses themself.
#define log_range_vector_get_memoryLocation_abs(vec) (VECTOR_FLOAT_ADD(VECTOR_FLOAT_MUL(vec, vec_fast_log_defaultValue), vec_fast_log_defaultValue))
#else //! then we test the tiem-overhead of our approach wrt. artimetic operations
#define log_range_vector_get_memoryLocation_abs(vec) (VECTOR_FLOAT_SET1(1))
#endif


//#define log_range_vector_get_memoryLocation(vec) (VECTOR_FLOAT_ADD(VECTOR_FLOAT_MUL(vec, VECTOR_FLOAT_SET1(optmize_use_logValues_sizeOf_logTable_zeroOnes)), VECTOR_FLOAT_SET1(optmize_use_logValues_sizeOf_logTable_zeroOnes)))

// FIXME: write code to test/validate correctness of this approach.
    // FIXME[article]: validate correctness of ["elow] "fabs(..)" assumption ... and otehrwise update our "log_range_0_one(..)" macro-itnaition.
#define log_range_0_one(val) (static_listOf_values_shallowCopy_aboveZero[__get_internalIndexOf_float_0_one(val)]) 
#define log_range_0_one_abs(val) ({assert(static_listOf_values); static_listOf_values[__get_internalIndexOf_float(val)];})
//#define log_range_0_one_abs(val) (static_listOf_values[__get_internalIndexOf_float(val)])

//! ------------------------
#if 1 == 1 //! where we observe that 'this approach' has a "1.3x" performance-overhead 'caused' by memory-accesses
#define log_range_0_one_inputIsAdjusted(val) (static_listOf_values_shallowCopy_aboveZero[(uint)val]) //! ie, as we then assumet ha that a user ahs 'adjusted' the input, eg, by using SSE itnrisntict (oekseth, 06. june 2016).
#define log_range_0_one_inputIsAdjusted_abs(val) (static_listOf_values[(uint)val]) //! ie, as we then assumet ha that a user ahs 'adjusted' the input, eg, by using SSE itnrisntict (oekseth, 06. june 2016).
#else //! then we test the tiem-overhead of our approach wrt. memory-accesses
#define log_range_0_one_inputIsAdjusted(val) (0.1)
#define log_range_0_one_inputIsAdjusted_abs(val) (0.1)
#endif
//! ------------------------

#endif
