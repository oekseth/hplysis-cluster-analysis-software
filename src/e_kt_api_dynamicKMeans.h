#ifndef e_kt_api_dynamicKMeans_h
#define e_kt_api_dynamicKMeans_h

/**
   @enum e_kt_api_dynamicKMeans
   @brief provides a 'specifciaiton' wrt. the type of k-mean-permtuation-algorithm to use (oekseth, 06. des. 2016).
 **/
typedef enum e_kt_api_dynamicKMeans {
  e_kt_api_dynamicKMeans_AVG, //! ie, "mean"
  e_kt_api_dynamicKMeans_rank, //! ie, "median"
  e_kt_api_dynamicKMeans_medoid, 
  // --- 
  e_kt_api_dynamicKMeans_undef
} e_kt_api_dynamicKMeans_t;


#endif //! EOF
