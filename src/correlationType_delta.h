#ifndef correlationType_delta_h
#define correlationType_delta_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlationType_delta
   @brief provide lgois for copmtuation of the delta-logics, eg, Euclid and Spearman.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "s_allAgainstAll_config.h"
#include "correlation_macros__distanceMeasures.h"

/**
   @brief compute the weighted euclidian distance between two rows or columns in a matrix.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float __kt_euclid_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose);

/**
   @brief compute the weighted "City Block" distance between two rows or columns in a matrix. 
   @remarks City Block distance is defined as the absolute value of X1-X2 plus the absolute value of Y1-Y2 plus..., which is equivalent to taking an "up and over" path.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float __kt_cityblock_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose);
/**
   @brief compute the euclidian (or: geometric distance) between two rows or columns.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
**/
t_float kt_euclid(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const s_allAgainstAll_config_t config);

/**
   @brief compute the Cityblock (or: Manhatten distance) between two rows or columns.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
t_float kt_cityblock(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const s_allAgainstAll_config_t config);

#endif //! EOF
