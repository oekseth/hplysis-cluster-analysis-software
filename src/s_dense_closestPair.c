#include "s_dense_closestPair.h"
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */

#ifndef min
#define min(x, y)	((x) < (y) ? (x) : (y))
#endif
#ifndef max
#define	max(x, y)	((x) > (y) ? (x) : (y))
#endif


/**
   @brief re-intalize the time-emasurements for the enum_id in quesiton.
   @param <self> the object to hold the s_log_hca_t object.
     @param <enum_id> the log-type to update
**/
void update_log_startMeasurement(s_dense_closestPair_t *self, const e_log_hca_typeOf_t enum_id) {
  assert(self); assert(enum_id != e_log_hca_typeOf_undef);
# if(config_useLog_main==1)
  //! The call:
  start_time_measurement_log_hca(&self->obj_log, enum_id);
# endif
}

//! @return the column-bucket-id assicated to column_pos for the self object.
uint get_columnBucketId(const s_dense_closestPair_t *self, const uint column_pos) {
  assert(column_pos != UINT_MAX); assert(self);
  if(column_pos != 0) {
    const uint bucket_id = column_pos / self->_bucket_size; 
    return bucket_id;
  } else {return 0;}
}

//! @return the first-column-pos to investigate for bucket_id
uint get_columnBucket_startPosOf_bucket(const s_dense_closestPair_t *self, const uint bucket_id) {
  assert(self);   assert(bucket_id < self->cnt_bucketsEachRows);
  const uint column_pos_startOf_block = bucket_id * self->_bucket_size; //! ie, to 'adjust' the the beginng of the tile.
  assert(column_pos_startOf_block < self->ncols);
  //! @return the result:
  return column_pos_startOf_block;
}
//! @return the last-column-pos to investigate for bucket_id
uint get_columnBucket_endPlussOnePosOf_bucket(const s_dense_closestPair_t *self, const uint bucket_id) {
  assert(self);   assert(bucket_id < self->cnt_bucketsEachRows);
  const uint column_pos_startOf_block = bucket_id * self->_bucket_size; //! ie, to 'adjust' the the beginng of the tile.
  assert(column_pos_startOf_block < self->ncols);
  assert(column_pos_startOf_block == get_columnBucket_startPosOf_bucket(self, bucket_id)); //! ie, a cosnsitency-check.
  uint column_pos_endOf_block = column_pos_startOf_block + self->_bucket_size;
  if(column_pos_endOf_block > self->_ncols_threshold) {
    column_pos_endOf_block = self->_ncols_threshold;
  }
  //! @return the result:
  return column_pos_endOf_block;
}
/**
   @brief update the "s_log_hca_t" object with updated time-measurements (oekseth, 06. july 2016).
   @param <self> the object to hold the s_log_hca_t object.
   @param <enum_id> the log-type to update
   @param <cnt> if not set to "0" nor "UINT_MAX" then wwe use this valeu to update the assciated s_log_hca_t structure     
**/
void update_log_endMeasurement(s_dense_closestPair_t *self, const e_log_hca_typeOf_t enum_id, const uint cnt) {
  assert(self); assert(enum_id != e_log_hca_typeOf_undef);
# if(config_useLog_main==1)
  //! The call:
  end_time_measurement_log_hca(&self->obj_log, enum_id);
  //! Update the counter:
  log_updateCounter(&self->obj_log, enum_id, cnt);
# else 
  assert(cnt != UINT_MAX-20); //! ie, to avoid teh compiler complaingin about un.-used vlaues.
# endif  
}

//! @return the max-bucket-size.
//! @remarks hadnle the case where '+1' is to 'handle' the 'implcit floor-call, and where 'assicated lgocis' is 'rpvodied' in our below for-loop wrt. the "if(i_end > column_endPosPlussOne)" lgocial-clause.
static uint __get_local_bucket_size(const uint ncols, const uint bucket_column_size) {
  uint bucket_size = ncols / bucket_column_size;
  // FIXME: mvoe [below] into a new macor .. and make use of 'this' wrt. 'the otehr fucntiosn'.
  if( (bucket_size*bucket_column_size) < ncols) {
    bucket_size += 1;
    // printf("(set-bucket) size=%u given ncols=%u and bucket_column_size=%u, at %s:%d\n", bucket_size, ncols, bucket_column_size, __FILE__, __LINE__);
  }
  return bucket_size;
}


//! Evaluate if the object is conssitenten.
#if(config_debugSlowdown_useExtensive_correctnessTesting == 1)
bool classWideTests_runTime(s_dense_closestPair_t *self, const uint nrows_threshold, const uint ncols_threshold, const char *caller_file, const int caller_line, const bool test_distancematrix) {
  assert_local(self); assert_local(caller_file); assert_local(caller_line >= 0);  //! ie, what we expect.
  assert(nrows_threshold <= self->_nrows_threshold); assert(ncols_threshold <= self->_ncols_threshold); 
  
  //uint bucket_id = 0; //ncols_threshold / self->_bucket_size; 
  const uint bucket_end = __get_local_bucket_size(ncols_threshold, self->_bucket_size);
  
  
  if(false) {printf("\n#(test-correctness)\t called-from %s:%d, at %s:%d\n", caller_file, caller_line, __FILE__, __LINE__);}


  //! Frist valie date. the column buckets/chunks:
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
#endif  
  for(uint row_id = 0; row_id < nrows_threshold; row_id++) {
    //uint column_pos = 0;
    uint column_pos = 0;
    for(uint bucket_id = 0; bucket_id < bucket_end; bucket_id++) {
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
      if(false) {printf("[row=%u][bucket=%u]\t cmp(%d, %d), at %s:%d\n", row_id, bucket_id, 
		       (uint)self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id], (uint)ncols_threshold,
		       __FILE__, __LINE__);}
      assert_local(self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] < ncols_threshold);
#endif
      if(test_distancematrix) {
	//! Validate that (a) the distance is found in the set and (b) that the distance is the min-distance:
	uint end_pos = column_pos + self->_bucket_size; 
	if(end_pos > ncols_threshold) {end_pos = ncols_threshold;}
	if(end_pos > 0) {
	  uint foundAt_index = UINT_MAX; 
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
#ifndef NDEBUG	      
	  bool indexWasFound = false;
#endif
#endif
	  t_float better_score = T_FLOAT_MAX; uint errorAtIndex = UINT_MAX;
	  const uint start_pos = column_pos;
	  for(; column_pos < end_pos; column_pos++) {
	    if(false) {printf("[row=%u][bucket=%u][%u]\t cmp(%f, %f), at %s:%d\n", row_id, bucket_id, column_pos, self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], self->distmatrix[row_id][column_pos], __FILE__, __LINE__);}
	    const t_float best_score = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	    const t_float local_score = self->distmatrix[row_id][column_pos];
	    if(best_score == local_score) {

	      //assert_local(self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] == self->distmatrix[row_id][column_pos]);
	      foundAt_index = column_pos;
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
#ifndef NDEBUG	      
	      //if(best_score == foundAt_index) 
	      {indexWasFound = true;}
#endif
#endif
	    } else if(local_score < best_score) {
	      better_score = local_score; 	      errorAtIndex = column_pos;
	    }
	  }
	  if(false) {printf("\t\t\t(foudn-at-index) [%u]=%f, at %s:%d\n", foundAt_index, self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__);}
	  if(errorAtIndex != UINT_MAX) {
	    fprintf(stderr, "!!\t(seems like a bug)\t at row=%u, while we expected the best-score [%u]=%f, we observed best-score [%u]=%f, where delta=%f, ie, seems like an icnsonstecy, ie, pelase investgaite.  Observation at [%s]:%s:%d\n", 
		    row_id,
		    foundAt_index, self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], 
		    errorAtIndex, better_score, 
		    self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] - better_score, 
		    __FUNCTION__, __FILE__, __LINE__);
	    assert(false); //! ie, as we then have an inconcisstency.
	  }	  
	  if(foundAt_index == UINT_MAX) {
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	    const uint expected_col_pos = (uint)self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id];
  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; */
  /*   printf("test: %f, at %s:%d\n", self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__); // FIXME: remvoe */
  /*   //assert(false); */
  /* } */

	    fprintf(stderr, "!!\t(unable-to-fifnd-index)\t [row=%u/%u][bucket=%u][%u...%u]\t where expected-min-score=%f and expected-column-pos=[%u]=%f,  at %s:%d\n", row_id, nrows_threshold, bucket_id, start_pos, end_pos, self->matrixOf_locallyTiled_minPairs[row_id][bucket_id],  expected_col_pos, self->distmatrix[row_id][expected_col_pos], __FILE__, __LINE__);
	    assert(self->distmatrix[row_id][expected_col_pos] == self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]); //! ie, as otherwise will idnciate that the "matrixOf_locallyTiled_minPairs" has not been rppeorply updated.
#else
	    fprintf(stderr, "!!\t(unable-to-fifnd-index)\t [row=%u/%u][bucket=%u][%u...%u]\t where expected-min-score=%f, at %s:%d\n", row_id, nrows_threshold, bucket_id, start_pos, end_pos, self->matrixOf_locallyTiled_minPairs[row_id][bucket_id],  __FILE__, __LINE__);
#endif
	  }
	  assert_local(foundAt_index != UINT_MAX);
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  assert_local(indexWasFound);
#endif
	}
      }
    }
  }

  if(self->isTo_use_fast_minTileOptimization) { //! then we allcoate memory an intlaize:
    for(uint bucket_id = 0; bucket_id < bucket_end; bucket_id++) {
      const uint row_id = self->mapOf_minTiles_value_atRowIndex[bucket_id];
      assert_local(row_id < nrows_threshold);
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
      //! Validate the correctness wrt. the column:
      const uint column_id = self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id];
      assert_local(column_id < ncols_threshold);  
      //! Valdiate that the score is correctly set:
      if(test_distancematrix) {
	if(self->distmatrix[row_id][column_id] != self->mapOf_minTiles_value[bucket_id]) {
	  if(false) {printf("(cmp)\t bucket_id=%u w/min-row=%u and expected-column-pos=%u, and score(local, global)=(%f VS %f), at %s:%d\n", bucket_id, row_id, column_id, self->distmatrix[row_id][column_id], self->mapOf_minTiles_value[bucket_id], __FILE__, __LINE__);}
	}
	assert_local(self->distmatrix[row_id][column_id] == self->mapOf_minTiles_value[bucket_id]);
      }
#endif
    }
  }


  return 1; //! ie, as this exueciton-point we assume the object is correct.
}
#else
bool classWideTests_runTime(s_dense_closestPair_t *self, const uint nrows_threshold, const uint ncols_threshold,  const char *caller_file, const int caller_line, const bool test_distancematrix) {assert_local(self); assert_local(caller_file); assert((int)test_distancematrix < 2); assert_local(caller_line >= 0); assert(nrows_threshold != UINT_MAX); assert(ncols_threshold != UINT_MAX); return 1;} //! ie, then a 'dummy' object
#endif


/**
   @brief A function to intiate the s_dense_closestPair object.
**/
void s_dense_closestPair_init(s_dense_closestPair_t *self, const uint nrows, const uint ncols, uint _cnt_bucketsEachRows, t_float **distmatrix, const bool config_useIntrisintitcs, const bool isTo_use_fast_minTileOptimization) {
  //! What we expect:
  assert_local(self); assert_local(nrows);  assert_local(distmatrix);

#if(config_FixedNumberOf_bucketsFor_columns != 0)  //! then we simplify the work of the compiler wrt. the 'loop-expansions':
  const uint cnt_bucketsEachRows = config_FixedNumberOf_bucketsFor_columns;
  if(cnt_bucketsEachRows > ncols) {
    fprintf(stderr, "!!\t From your compiler-macro-paraemter we observe that %u number of buckets are to be used. This in cotnrast to the |%u| expected column in your input-matrix: we expec thte number fo column-beckets to be fwer than the matyrix-call, ie, please update your configuraiton. For quesitons, please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_bucketsEachRows, ncols, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return;
  }
#else
  uint cnt_bucketsEachRows = _cnt_bucketsEachRows;
  if(ncols < cnt_bucketsEachRows) {
    cnt_bucketsEachRows = ncols/2; //! then we adjust the intial score.
  }
#endif

  assert_local(cnt_bucketsEachRows); 

# if(config_useLog_main==1)
  //! Intiaite:
  s_log_hca_init(&self->obj_log);
  //! Start the time-measurement:
  start_time_measurement_log_hca(&self->obj_log, e_log_hca_typeOf_all);
  update_log_startMeasurement(self, e_log_hca_typeOf_init);
#endif

  //! Intialise the containers.
  self->nrows = nrows; self->ncols = ncols; self->cnt_bucketsEachRows = cnt_bucketsEachRows; self->distmatrix = distmatrix; self->config_useIntrisintitcs = config_useIntrisintitcs; self->isTo_use_fast_minTileOptimization = isTo_use_fast_minTileOptimization;
  self->_bucket_size = self->ncols / self->cnt_bucketsEachRows;
  const uint local_bucket_size = __get_local_bucket_size(ncols, self->_bucket_size);
  if(local_bucket_size != self->cnt_bucketsEachRows) {
    self->_bucket_size += 1;
    const uint cnt_expected = __get_local_bucket_size(ncols, self->_bucket_size);
    assert_local(self->cnt_bucketsEachRows >= cnt_expected);
    //! Then adjust the number of buckets to compute for:
    cnt_bucketsEachRows = self->cnt_bucketsEachRows = cnt_expected;
#if(config_FixedNumberOf_bucketsFor_columns != 0)  //! then we simplify the work of the compiler wrt. the 'loop-expansions':
    if(cnt_bucketsEachRows != cnt_expected) {
      fprintf(stderr, "!!\t From your compiler-macro-paraemter we observe that %u number of buckets are to be used: in contrast we derive '%u' column-buckets from the |%u| expected column in your input-matrix: we expect the number of column-buckets (inferred to be of size=|bucket|=%u) to be 'reflect' the bucket-size, ie, please update your configuration. For quesitons, please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_bucketsEachRows, cnt_expected, ncols, self->_bucket_size, __FUNCTION__, __FILE__, __LINE__);
      assert(false);
      return;
    }
#endif
  } else {
    assert_local(local_bucket_size == self->cnt_bucketsEachRows);
  }
  /* //  const uint local_bucket_size = __get_local_bucket_size(ncols, cnt_bucketsEachRows); */
  /* if(local_bucket_size != self->cnt_bucketsEachRows) { */
  /*   assert_local(local_bucket_size > self->cnt_bucketsEachRows); //! ie, as we othersise might have an icnosnstncy. */
  /*   self->cnt_bucketsEachRows = local_bucket_size; //! ie, to handle 'overlfows' wrt. bucket-size. */
  /*   cnt_bucketsEachRows = self->cnt_bucketsEachRows; //! ie, udpate */
  /*   printf("cnt_bucketsEachRows=%u, at %s:%d\n\n", cnt_bucketsEachRows, __FILE__, __LINE__); */
  /* } */
  //#ifndef NDEBUG
  self->_nrows_threshold = nrows;
  self->_ncols_threshold = ncols;
  //#endif
  //! Allcoate memory:
  // FIXME: try to identify the 'otpmial' number of buckets for each row.
  const t_float default_value_float = /*default-value=*/0;
  self->matrixOf_locallyTiled_minPairs = allocate_2d_list_float(nrows, cnt_bucketsEachRows, default_value_float);
  self->_arrOf_tmp_scalarRowValues = allocate_1d_list_float(nrows, default_value_float);
  assert_local(self->_arrOf_tmp_scalarRowValues != NULL);

#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
  self->mapOf_endPosOf_columnTiles_toUpdate = NULL;
#endif
  self->mapOf_minTiles_value = NULL; self->mapOf_minTiles_value_atRowIndex = NULL;
  if(self->isTo_use_fast_minTileOptimization) { //! then we allcoate memory an intlaize:
    self->mapOf_minTiles_value =allocate_1d_list_float(cnt_bucketsEachRows, default_value_float); //! ie, as we are interested in the min-value for every row wrt. a given tile-bucket.
    self->mapOf_minTiles_value_atRowIndex = allocate_1d_list_float(cnt_bucketsEachRows, default_value_float); //! ie, as we are interested in the min-value for every row wrt. a given tile-bucket.
    //! Intalise the arrays using a max-distance-value:
    // FIXME: write a macro wrt. [below] which sets teh valeus ... and compare the performance-difference.
    for(uint i = 0; i < cnt_bucketsEachRows; i++) {self->mapOf_minTiles_value[i] = T_FLOAT_MAX; self->mapOf_minTiles_value_atRowIndex[i] = T_FLOAT_MAX;}
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
    const uint default_value_uint = 0;
    self->mapOf_endPosOf_columnTiles_toUpdate = allocate_1d_list_uint(cnt_bucketsEachRows, default_value_uint);
# endif
  }
# if(config_useRowChunks == 1)
  init_s_rowChunk(&self->obj_rowChunk, self->cnt_bucketsEachRows, nrows);
# endif
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  const uint default_value_uint = 0;
  self->matrixOf_locallyTiled_minPairs_columnPos = allocate_2d_list_uint(nrows, cnt_bucketsEachRows, default_value_uint);
#else
  self->matrixOf_locallyTiled_minPairs_columnPos = NULL;
#endif

  
  { //! Identify teh shortest paths for each bucket
    update_log_startMeasurement(self, e_log_hca_typeOf_init_details_minPairsEachColumnBucket_local);

    //const uint bucket_size = self->_bucket_size;

    if(false) {printf("Given column-range=[%u, %u-1] and bucket_size=%u, we evalaute buckets in range=[%u, %u-1], at %s:%d\n", 0, ncols, self->_bucket_size, 0, self->_bucket_size-1, __FILE__, __LINE__);}
    
    if( config_useIntrisintitcs && (self->_bucket_size % VECTOR_FLOAT_ITER_SIZE) != 0) { //! ie, to simplify our code, ie, reuce the algorithmic overhead.
      fprintf(stderr, "!!\t The input was not specified as expected: the bucket-size=%u = %u/%u was not 'divisable' by '%u', ie, a (minor) slwodown may incure: if the latter is acceptable, then please request a code-update of the (below assicated \"VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(..)\" function) code, ie, for the latter please cotneact the develoepra t [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", self->_bucket_size, self->ncols, self->cnt_bucketsEachRows, (uint)VECTOR_FLOAT_ITER_SIZE, __FUNCTION__, __FILE__, __LINE__);
    }

    //! Identify teh shortest paths for each bucket
    __init_minPairs_eachColumnBucket(distmatrix, nrows, self->ncols, self->cnt_bucketsEachRows,    self->matrixOf_locallyTiled_minPairs, self->matrixOf_locallyTiled_minPairs_columnPos, self->_bucket_size);
    //! Complete the time-measurement:
    update_log_endMeasurement(self, e_log_hca_typeOf_init_details_minPairsEachColumnBucket_local, /*cnt=*/1);
  }

  // assert_local(false); // FIXME: remove

  if(self->isTo_use_fast_minTileOptimization) { 
    update_log_startMeasurement(self, e_log_hca_typeOf_init_details_minPairsEachColumnBucket_global);
    //! Then identify the min-values for the 'global' column:
    __init_minColumns_eachBucket(nrows, self->cnt_bucketsEachRows, self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, __get_obj_rowChunk(self)); //, self->matrixOf_locallyTiled_minPairs_columnPos);
    //! Complete the time-measurement:
    update_log_endMeasurement(self, e_log_hca_typeOf_init_details_minPairsEachColumnBucket_global, /*cnt=*/1);
  }

  // assert_local(false); // FIXME: remove

  //! What we expect:
  assert_local(self); assert_local(self->nrows); assert_local(self->cnt_bucketsEachRows); assert_local(self->matrixOf_locallyTiled_minPairs); assert_local(self->distmatrix);
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
#endif
  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/true)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
  //! Complete the time-measurement:
  update_log_endMeasurement(self, e_log_hca_typeOf_init, /*cnt=*/1);
}

//! De-allcoate the locally reserved memory.
void s_dense_closestPair_free(s_dense_closestPair_t *self) {
  free_2d_list_float(&self->matrixOf_locallyTiled_minPairs, self->_nrows_threshold);
  if(self->matrixOf_locallyTiled_minPairs) {
    free_1d_list_float(&self->mapOf_minTiles_value);
    free_1d_list_float(&self->mapOf_minTiles_value_atRowIndex);
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
    free_1d_list_uint(&self->mapOf_endPosOf_columnTiles_toUpdate);
# endif
  }
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  free_2d_list_uint(&self->matrixOf_locallyTiled_minPairs_columnPos, self->_nrows_threshold);
#endif
  assert_local(self->_arrOf_tmp_scalarRowValues); 
  // printf("Free-poitner, at %s:%d\n", __FILE__, __LINE__);
  free(self->_arrOf_tmp_scalarRowValues); 
  //  free(&(self->_arrOf_tmp_scalarRowValues)); 
  // printf("ok: Free-poitner, at %s:%d\n", __FILE__, __LINE__);
  //assert_local(self->_arrOf_tmp_scalarRowValues == NULL); //! ie, wtha we expect
  self->_arrOf_tmp_scalarRowValues = NULL;
# if(config_useRowChunks == 1)
  free_s_rowChunk(&self->obj_rowChunk);
# endif

  //! Then write out the result of the log-object:
# if(config_useLog_main==1) 
  //! Complet the time-measurement:
  end_time_measurement_log_hca(&self->obj_log, e_log_hca_typeOf_all);
  //! Write out:
  writeOut_object(&self->obj_log, stdout);
# endif
}

//! Update the shortest-paht-positions wrt. an updated row
void s_dense_closestPair_updateAt_row(s_dense_closestPair_t *self, const uint row_id, const uint column_startPos, const uint column_endPosPlussOne, const uint row_endPosPlussOne) {
   //! What we expect (generic):
  assert_local(self); assert_local(self->nrows); assert_local(self->cnt_bucketsEachRows); assert_local(self->matrixOf_locallyTiled_minPairs); assert_local(self->distmatrix);
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
#endif
  //! Specific:
  assert_local(row_id < self->nrows); assert_local(column_startPos <= column_endPosPlussOne); assert_local(row_endPosPlussOne <= self->nrows); assert_local(column_endPosPlussOne <= self->ncols);
  assert_local(self->_bucket_size > 0); assert_local(self->_bucket_size != UINT_MAX);
  assert_local(column_startPos != column_endPosPlussOne); //! ie, what we expect
  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
  //! Update the log-function, starting 'at a new time-point':
  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateRow);


  //! Identify the bucket-range:
  uint bucket_id = column_startPos / self->_bucket_size; 
  const uint bucket_end = __get_local_bucket_size(column_endPosPlussOne, self->_bucket_size);
  //const uint bucket_end = (column_endPosPlussOne / self->_bucket_size) + 1;  //! where '+1' is to 'handle' the 'implcit floor-call, and where 'assicated lgocis' is 'rpvodied' in our below for-loop wrt. the "if(i_end > column_endPosPlussOne)" lgocial-clause.
  
  if(false) {printf("(update-row[%u]\t for columns in range[%u...%u/%u] and bucket %u...%u/%u, at [%s]:%s:%d\n", row_id, column_startPos, column_endPosPlussOne, self->_ncols_threshold, bucket_id, bucket_end, self->cnt_bucketsEachRows, __FUNCTION__, __FILE__, __LINE__);}
  if(false) {printf("\n#\t Given column-range=[%u, %u-1] and bucket_size=%u, we evalaute buckets in range=[%u, %u/%u], at %s:%d\n", column_startPos, column_endPosPlussOne, self->_bucket_size, bucket_id, bucket_end, self->_bucket_size, __FILE__, __LINE__);}

  { //! What we expect:
    const uint last_pos_actual = bucket_end*self->_bucket_size;
    // printf("(cmp) %u VS %u, at %s:%d\n", last_pos_actual, column_endPosPlussOne, __FILE__, __LINE__);
    assert_local(last_pos_actual >= column_endPosPlussOne);
  }

  uint column_pos = bucket_id * self->_bucket_size; //! ie, the start-pos of the current row-chunk-id.
  assert_local(column_pos <= column_startPos); //! ie, as we otehriwse might have a bug.


  if(self->config_useIntrisintitcs) {
    assert_local(false); // FIXME: validate correcntess of this funciton ... ie, 'move' this funciton into sub-parts which may esily be verfied wrt. assert-tests.
    assert_local(false); // FIXME: write differnet systnetic perofrmance-tests wrt. this funciton ... eg, to dineitfy 'trheashodls' wrt. SSE-optmalizaitons.
    const uint cntBuckets_intri_size = macro__get_maxIntriLength_float((bucket_end - bucket_id) - 1);
    if(cntBuckets_intri_size > 0) {
      const uint cntBuckets_intri = bucket_id + cntBuckets_intri_size;

      assert_local(false); // FIXME: valdiate correctness of the vec_ref_mask values <-- try to set different values, and ivnesttigate the result.

      //! Identify the min-tiles seperately for each "row[ ... chunk-tile-0 ... chunk-tile-1 ... chunk-tile-(etc) ... ]".
      for(; bucket_id < cntBuckets_intri; bucket_id += VECTOR_FLOAT_ITER_SIZE) {
	//! Identify the min-value for each column-range:
	VECTOR_FLOAT_minDistance_eachTile(self->matrixOf_locallyTiled_minPairs[row_id], bucket_id, self->distmatrix[row_id], &column_pos, self->_bucket_size);


	if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:

	  assert_local(false); // FIXME: move this out into a seperate function, adn then validate correctness 'of this'.

	  //! Update the min-tile-values:
	  VECTOR_FLOAT_TYPE vec_needToIterate;
	  
	  const uint bucket_id_local_end = bucket_id + VECTOR_FLOAT_ITER_SIZE;
	  for(uint bucket_id_local = bucket_id; bucket_id_local < bucket_id_local_end; bucket_id_local++) {
/* #if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1) */
/* 	    uint *arrOf_xPos = self->matrixOf_locallyTiled_minPairs_columnPos[row_id]; */
/* #else */
/* 	    uint *arrOf_xPos = NULL; */
/* #endif */
	    const uint cnt_needTo_iterateThroughAllRows =  __slow_update_globalMin_scoreAndIndex_forBucket(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs[row_id], bucket_id_local, /*row-id=*/row_id);
	    if(cnt_needTo_iterateThroughAllRows > 0) { //! Then 'handle' the last bucket, suign non-itrnsitnictic iteration:
	      // FIXME: write a performnce-time-test wrt. the "_mm_castps_si128(..)" when compared to using "(uint)" casts ... ie, as an example of the 'explict optmizaitons' which may be performned .. and similar wr.t the use of "0xfff0" VS FLT_MAX
	      
	      //! Then we update all of the vlaues:
	      for(uint row_updateIndex = 0; row_updateIndex < row_endPosPlussOne; row_updateIndex++) {

		assert_local(false); // FIXME: make a modfied call wr.t [”elow]

		// __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs[row_updateIndex], bucket_id, /*row-id=*/row_updateIndex);
	      }
	    }
	  }
	}

	// FIXME: remove [”elow] when we know that [”elow] conditon always hold:
	assert_local(column_pos <= column_endPosPlussOne);
      }
    }    
    assert_local(false); // FIXME: validate taht all of the [ªbove] 'non-itnricticntict idnex-cases' ahs been covered/ahndled.
  }



  {
    const uint column_startPos = column_pos;
    // FIXME: why does [”elow] use "bucket_end + 1" ?? <-- udpated, though need to validdate correctness 'of not doing this'.
    for(; bucket_id < bucket_end; bucket_id++) {
      assert_local(column_pos <= column_endPosPlussOne); //! ie, as we otherwise might have a bug.     

      //! Idnetify the start- and stop-conditions:
      //! Note: the column-pos is iteratilvy updated in the for-loop, ie, the column-pos is always up-to-date.
      uint i_end = column_pos + self->_bucket_size;
      // FIXME: test the performance-effects of using a seperate for-loop for the [”elow] case <-- first move [”elow] into a new funciton ... and validate that 'the use of a fucntion does not increase the euxeicont-time'.
      if(i_end > self->_ncols_threshold) {i_end = self->_ncols_threshold;} //! ie, to avodi iteration 'past' the threshold.
      //if(i_end > column_endPosPlussOne) {i_end = column_endPosPlussOne;} //! ie, to avodi iteration 'past' the threshold.
      // FIXME: dopr [”elow] if-caluse when [ªbov€] FIXME is 'resovled'.
      const t_float old_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id];    
      if(false) {printf("[%u][%u...%u]|bucket=%u\t given column_endPosPlussOne=%u, at %s:%d\n", row_id, column_pos, i_end, bucket_id, column_endPosPlussOne, __FILE__, __LINE__);}
      if(column_pos < column_endPosPlussOne) {
	assert_local(i_end <= self->_ncols_threshold); //! ie, as we otehrwise will have an incosnsitency.  
	//assert_local(i_end <= column_endPosPlussOne); //! ie, as we otehrwise will have an incosnsitency.  
	//! Reset the vlaue, through remember the old/previous value:
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	uint indexFoundAt = column_pos;
#endif
	if(false) { printf("\t\t\t[%u][%u/%u] at bucket=%u\t score(actual, min)=(%f, %f), at %s:%d\n", row_id, column_pos, i_end, bucket_id, self->distmatrix[row_id][column_pos], self->distmatrix[row_id][column_pos], __FILE__, __LINE__ );}
	self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = self->distmatrix[row_id][column_pos]; column_pos++;
	//! Then find the min-values in the 'row-tile-chunk':
	for(; column_pos < i_end; column_pos++) {
	  const t_float currentValue = self->distmatrix[row_id][column_pos];
	  if(self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] > currentValue) {
	    self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue; //! ie, as 'currentValue' is then 'closer'.
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	    indexFoundAt = column_pos;
#endif
	    if(false) { printf("\t\t\t[%u][%u] at bucket=%u\t score(actual, min)=(%f, %f), at %s:%d\n", row_id, column_pos, bucket_id, self->distmatrix[row_id][column_pos], self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__ );}
	  }
	  assert_local(column_pos <= self->_ncols_threshold); //! ie, as we otehrwise will have an incosnsitency.  
	  //assert_local(column_pos <= column_endPosPlussOne); //! ie, as we otherwise might have a bug
	}
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
	self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = indexFoundAt;
#endif
      }
      assert_local(column_pos <= self->_ncols_threshold); //! ie, as we otehrwise will have an incosnsitency.  
      //assert_local(column_pos  column_endPosPlussOne); //! ie, as we otherwise might have a bug.     
      if(false) {printf("[%u][bucket=%u] \t min=%f, at %s:%d\n", row_id, bucket_id, /*min_value=*/self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__);}

      if( old_minValue != self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]) { //! then the min-value has changed:
	//! Then we udpate the min-row-index for each column-tile:  


	if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:
	  //! Update the global min wrt. score and index-mappings for a column-tile usign intrisinistics:
	  //! Note: in [”elow] we compare 'global optmium' VS 'local optmium': "matrixOf_locallyTiled_minPairs[bucket_id]" VS "mapOf_minTiles_value[bucket_id]". In this comparsin an 'optmizaiton' (which we makes use of) is that if 'thr row with improved global score' si the same as 'the earlier row witht he improved global score' then we do no perofmr [”elow] 'call wrt. glboal idnetificiaotn of min-values':
	  const bool needTo_iterateThroughAllRows = __slow_update_globalMin_scoreAndIndex_forBucket(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs[row_id], bucket_id, /*row-id=*/row_id);
	  if(needTo_iterateThroughAllRows) { //! Then 'handle' the last bucket, suign non-itrnsitnictic iteration:
	    //! Then we need to iterate through all of the 'tiled rows', ie, for the bucket in question:

	    //! Note: we do Not test/evaluate an optmizaiton-option wrt. the 'time-optimum-effect' of 'storing the bucket-ids-to-update in a dense list'. The latter due to (a) the 'already in-place order of the bucket_ids', (b) overhead wrt. memory-accesses and (c) the 'arelady approximate order' of the memory-accesses wrt. [”elow] 'option'.

	    // FIXME: add a new temproary "arrOf_rowsTo_update" table, ie, to avoid 'muliple calls from updating the same min-value wrt. the "isTo_use_fast_minTileOptimization" case ... for which we may reduce the time-complexity (of the row-udpate-funcion-call) by a factor of "|rows|*|chunks|".

#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
	    assert(self->mapOf_endPosOf_columnTiles_toUpdate);
	    self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id] = row_endPosPlussOne; //! ie, number of columns to iterate through.
#else	    
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile); 
	    //! Update the glboally tiled mincolumn-tile for each of the rows:
	    //! Note: in [below] we iterate compare each 'local best-fit': "mapOf_minTiles_value[bucket_id]" VS "matrixOf_locallyTiled_minPairs[...][bucket_id]":
	    __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, row_endPosPlussOne, self->config_useIntrisintitcs);
	    assert_local(self->mapOf_minTiles_value_atRowIndex[bucket_id] < self->_ncols_threshold); //! ie, givne our above update-procedure/calls.
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile, /*counut=*/1); 
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	    //! Validate the correctness wrt. the column:
	    const uint column_id = self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id];
	    assert_local(column_id < self->_ncols_threshold);  
	    const uint row_id_globalMin = self->mapOf_minTiles_value_atRowIndex[bucket_id];
	    if(false) {printf("min[bucket=%u] \t min(local, global)=(%f, %f), row_id(local, global)=(%u, %u), at %s:%d\n", bucket_id, self->distmatrix[row_id][column_id], self->mapOf_minTiles_value[bucket_id], row_id, (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
	    if(row_id_globalMin == row_id) {
	      //! Valdiate that the score is correctly set:
	      assert_local(self->distmatrix[row_id][column_id] == self->mapOf_minTiles_value[bucket_id]);
	    } else {
	      const t_float value_better = self->mapOf_minTiles_value[bucket_id];
	      assert(value_better <= self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]);
	    }
#endif
#endif //! ie, wrt. "config_reWrite_infer_onlyAtOnePoint"
	  }
	}
      }
    }
    //! Update the log-function:
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateRow, /*count=*/column_pos);
    if(false) {printf("column_pos=%u, column_endPosPlussOne=%u, given column_startPos=%u, at %s:%d\n", column_pos, column_endPosPlussOne, column_startPos, __FILE__, __LINE__);}
  }
  
  //! What we expect:
  if(false) {printf("- updated [%u][%u...%u], where column_pos=%u, at [%s]:%s:%d\n", row_id, column_startPos, column_endPosPlussOne, column_pos, __FUNCTION__, __FILE__, __LINE__);}
  assert_local(column_pos >= column_endPosPlussOne); //! ie, as we otherwise might have a bug.     
  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.


  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   const t_float val1 = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]; const t_float val2 = self->distmatrix[row_id][column_pos]; */
  /*   printf("test: %f VS %f, at %s:%d\n", val1, val2, __FILE__, __LINE__); // FIXME: remvoe */
  /*   assert(val1 == val2); */
  /* } */

}


//! If a column-tile has has an improved/shroter distance, then consider to update 'local-min(row)(column-tile)' and 'global-min(column-tile)':
//! @return true if the locla-column-min has changed, ie, for whcih we (at some exec-point) need to update the lcoal-min-value.
bool update_minValues_for_columnBucket_setOptimal_atColumnPos(s_dense_closestPair_t *self, const uint row_id, const uint bucket_id, const uint column_pos, const t_float currentValue) {
  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_isBetter);
  self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue;
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
  const uint indexFoundAt = column_pos;
  self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = indexFoundAt;
#endif
  bool localMinChanged = false; //! which is used to avoid unnceccesary updated wrt. the locla-min-proerpty.
  if(self->mapOf_minTiles_value && (self->mapOf_minTiles_value[bucket_id] > currentValue) ) {
    self->mapOf_minTiles_value[bucket_id] = currentValue;
    if(self->mapOf_minTiles_value_atRowIndex[bucket_id] != row_id) {
      self->mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;
      localMinChanged = true;
    }
  }
#if(config_useRowChunks == 1)
  { //! Then update the min-row-chunk:
    s_rowChunk_t *obj_rowChunk = &self->obj_rowChunk;
    const uint rowBucket_id = get_idOf_rowChunk(obj_rowChunk, row_id);
#ifndef NDEBUG
    if(rowBucket_id >= s_rowChunk_getCnt_rowBuckets(obj_rowChunk)) {
      fprintf(stderr, "!!\t rowBucket_id=%u, cnt-buckets=%u, at %s:%d\n", rowBucket_id, s_rowChunk_getCnt_rowBuckets(obj_rowChunk), __FILE__, __LINE__);
    }
#endif
    assert(rowBucket_id < s_rowChunk_getCnt_rowBuckets(obj_rowChunk));
    s_rowChunk_setMinPos_ifImproved(obj_rowChunk, bucket_id, rowBucket_id, row_id, currentValue);
  }
#endif
  //printf("row_id=%u for bucket=%u, at %s:%d\n", row_id, bucket_id, __FILE__, __LINE__);
  if(false) {printf("(column-update)\t\t[row=%u][bucket=%u] \t min=%f at column=%u, at %s:%d\n", row_id, bucket_id, /*min_value=*/self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], column_pos, __FILE__, __LINE__);}
  update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_isBetter, /*cnt=*/1);

  //! @return
  return localMinChanged;
}

//! if the previous min-value is no longer valid/descritpive, then identify a new min-value (for the given row-column-tile), ie, "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)";
//! Note(1): this funciton si 'investgiated' for the case of: "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)"
//! Note(2): this if-case-block examplifes an exeuciton-time-performance consideration/challenge, ie, which conserns the case where the updated value is 'along' the min-positon
static void __update_minValues_for_columnBucket_iterate(s_dense_closestPair_t *self, const uint row_id, const uint bucket_id, const uint column_pos, const t_float currentValue, const uint column_pos_startOf_block) {
  //! then we assume that the 'old min-value' no longer hodls, ie, we ened to tierate t'rhoguht eh compelte chunk':
  //! Update the log-function, starting 'at a new time-point':
  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate);
  // #if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  uint indexFoundAt = column_pos;
  // #endif
  self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue; //! ie, to reset.
  uint column_pos_tmp = column_pos_startOf_block; //! ie, teh first/start index in a given column-tile:
  uint i_end = column_pos_startOf_block + self->_bucket_size;
  // FIXME: cosndier to move [”elow] to a enw function ... and then 'avoid' the ned for a 'dcodnitonal if-branch-statment'.
  if(i_end > self->_ncols_threshold) {i_end = self->_ncols_threshold;;} //! ie, to avodi iteration 'past' the threshold.
  //if(i_end > column_endPosPlussOne) {i_end = column_endPosPlussOne;} //! ie, to avodi iteration 'past' the threshold.

  uint debug_cnt_minValueHasCahnged = 0;
  const uint i_start = column_pos_tmp;
  for(; column_pos_tmp < i_end; column_pos_tmp++) {
    const t_float currentValue = self->distmatrix[row_id][column_pos_tmp];
    if(self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] > currentValue) {
      self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue;
      debug_cnt_minValueHasCahnged++;
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
      indexFoundAt = column_pos_tmp;
#endif
    }
  }
  if(false) {printf("(column-update)[row=%u][bucket=%u] \t min=%f at index=%u, for index-range[%u...%u], at %s:%d\n", row_id, bucket_id, /*min_value=*/self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], indexFoundAt, i_start, i_end,  __FILE__, __LINE__);}
  // FIXME: consider to include a 'max-value-table' for each column .... ie, wrt. oru "assert_local(debug_cnt_minValueHasCahnged > 0);" in oru "s_dense_closestPair.c" ... ie, to 'avoid this loop from being entered if the udpated vlaeu is still a min-value <-- first update our log-structre wrt. the importance 'of this case'.
  // assert_local(debug_cnt_minValueHasCahnged > 0); //! ie, as the for-loop-iteratioopn otehrwise is pointless.
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
  self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = indexFoundAt;
  assert(self->distmatrix[row_id][indexFoundAt] == self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]); //! ie, as we otehrwise might have an in-cosnistnecy in [ªbove]
#endif

  //! Update the log-function, starting 'at a new time-point':
  update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate, /*cnt=*/1);

}

//! Update the row at the givne column-psotion.
//inline 
//static __forceinline
//static __alwaysinline 
/* static __always_inline uint __updateAt_column_at_row(s_dense_closestPair_t *self, const uint row_id, const uint column_pos, const uint column_pos_startOf_block, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues, const uint bucket_id, uint *cnt_minTiles_updated_inConsistent) { */
/*   uint cnt_updated_rows = 0; */
/*   //! ------- */

/*   return cnt_updated_rows; */
/*   // //! Update the value: */
/*   // self->distmatrix[row_id][column_pos] = currentValue; */
/* } */

#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
//! Update for row-column pairs where the 'earlier column-local-min' no lnger holds, ie, for whcih we need to idneityf a different/new lcola-column-min
void s_dense_closestPair_updateAt_column_notAt_columnSelfPos(s_dense_closestPair_t *self, const uint input_column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues, const uint *mapOf_columnPos_toUse, const uint *arrOf_condensed_rowId_toUse, const uint arrOf_condensed_rowId_toUse_currPos, const uint cnt_alreadyInserted_forColumnAtSelfPos)  {
  //! What we expect (generic):
  assert_local(self); assert_local(self->nrows); assert_local(self->cnt_bucketsEachRows); assert_local(self->matrixOf_locallyTiled_minPairs); assert_local(self->distmatrix);
  //! Specific:
  assert_local(input_column_pos < self->ncols); assert_local(row_startPos <= row_endPosPlussOne); assert_local(row_startPos < self->nrows); assert_local(column_endPosPlussOne <= self->ncols);
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 0)	
  assert_local(arrOf_previouslyUsedValues); // assert_local(matrixWith_newValues_fixedColumnPos > 0); assert_local(matrixWith_newValues_fixedColumnPos < self->ncols);
#endif
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
  assert(arrOf_condensed_rowId_toUse);
  if(arrOf_condensed_rowId_toUse_currPos == 0) {
    //printf("--------------------- at %s:%d\n", __FILE__, __LINE__);
    return;} //! ie, as the then expect that there is 'no itneresting column to udpate'.
#else
  assert(arrOf_condensed_rowId_toUse == NULL);
  assert( (arrOf_condensed_rowId_toUse_currPos == 0) || (arrOf_condensed_rowId_toUse_currPos == UINT_MAX) ); //! ei, we expec tthis to be explcitly set to '0'.
#endif
  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
  //! Update the log-function, starting 'at a new time-point':
  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn);


  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
  //! Identify the bucket-range:
  uint bucket_id = input_column_pos / self->_bucket_size; 
  assert(bucket_id == get_columnBucketId(self, input_column_pos)); //! ei, what we expect wrt. cosnsitency.
  const uint column_pos_startOf_block = bucket_id * self->_bucket_size; //! ie, to 'adjust' the the beginng of the tile.
  uint cnt_minTiles_updated_inConsistent = 0;
  if(false) {printf("(update-column[%u]\t for rows in range[%u...%u/%u] and bucket %u/%u, at [%s]:%s:%d\n", input_column_pos, row_startPos, row_endPosPlussOne, self->_nrows_threshold, bucket_id, self->cnt_bucketsEachRows, __FUNCTION__, __FILE__, __LINE__);}
  uint cnt_updated_rows = 0;  

  {
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop);
    for(uint curr_pos = 0; curr_pos < arrOf_condensed_rowId_toUse_currPos; curr_pos++) {
      const uint row_id = arrOf_condensed_rowId_toUse[curr_pos];
      assert(row_id != UINT_MAX);	    
      assert(mapOf_columnPos_toUse); //! ie, as we then assuem this case is aleays used
      // FIXME: instead of [”elow] cosnider to use a 'dense' lsit ... to avoid the overehad assicated with [”elow]
      const uint column_pos = mapOf_columnPos_toUse[row_id];
      assert(column_pos != UINT_MAX);
      const t_float currentValue = self->distmatrix[row_id][column_pos];
      //! Test if the inserted value may irmpvoe the score:
      const bool minValueIs_imrpoved = __macro__minValueIsImproved(self, row_id, bucket_id, currentValue);      
      assert(minValueIs_imrpoved == false); //! ie, as we expect that the caller has seperately ivnestigated wrt. this issue/case.
      
      //! Investigate if the 'old min-value' no longer hodls, ie, we ened to tierate t'rhoguht eh compelte chunk':
      const bool minValue_forColumnBucket_noLongerHolds = __macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id, column_pos);
assert(minValue_forColumnBucket_noLongerHolds == true); //! ie, what we expect . [rows][column] called wrt. this fucntion.
      { //! then we need to update the min-valeus for the 'tile':

	//! then we need to traverse through the min-tile-set, ie, as it they may be a change in the min-values:	  
	cnt_updated_rows++; //! ie, as we assume this operation to have a relative high cost wrt. exuection-time.
	//! Then iterate 'through the remainder':
	//! if the previous min-value is no longer valid/descritpive, then identify a new min-value (for the given row-column-tile), ie, "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)";
	__update_minValues_for_columnBucket_iterate(self, row_id, bucket_id, column_pos, currentValue, column_pos_startOf_block);  
	//! Then 'make it clear' in the post-row-processing that we need to update the global-min wrt. the tiles
	//! Note: in [”elow] there is no poin in first investigating the "self->mapOf_minTiles_value_atRowIndex[bucket_id]" value, ie, as we do not know the 'second-best' valeu.
	cnt_minTiles_updated_inConsistent++; //! which is used as a a 'boolean mark'.
      }
    }
    //! Update the log-function, starting 'at a new time-point':
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop, /*cnt=*/1);
  }

  //! Update the log-function:
  update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn, /*count-rows-updated=*/cnt_updated_rows);


  if((cnt_alreadyInserted_forColumnAtSelfPos > 0) || (cnt_minTiles_updated_inConsistent > 0) ) { //! Then we evaluate/investgiate wrt. the min-values for all of the rows, ie, a possible time-compelxity-increase of |nrows|.
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
    assert(self->mapOf_endPosOf_columnTiles_toUpdate);
    self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id] = row_endPosPlussOne; //! ie, number of columns to iterate through.
#else
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile); 
    //! Iterate through the best-performing tiles and update.
    //! Note: update the glboally tiled mincolumn-tile for each of the rows:
    __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, row_endPosPlussOne, self->config_useIntrisintitcs);
    if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
    //! Update the log-function, starting 'at a new time-point':
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile, /*counut=*/1);
#endif //! ie, wrt. "config_reWrite_infer_onlyAtOnePoint"
  } else { //! then we asusem that none of the valeus 'represent' a min-tile-case:
    if(false) {printf("- updated [%u...%u][%u] w/bucket_id=%u, at [%s]:%s:%d\n", row_startPos, row_endPosPlussOne, input_column_pos, bucket_id, __FUNCTION__, __FILE__, __LINE__);}
  }

  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
}
#endif //! wrt. the "config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 0" macro-config-option.
/**
   <@brief Update the shortest-paht-positions wrt. an updated column
   @remarks procedure:
   -- each row-tile: Update the min-column-tile for each row: we expect one signualr valeus at each row to be updated: if a 'row-assicated tile' 'is at the best row-tile-index and has changed', then we need to update all values 'for the given tile'.
   -- the global tile: iterate through the best-performing tiles and update.

**/
void s_dense_closestPair_updateAt_column(s_dense_closestPair_t *self, const uint input_column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues, const uint *mapOf_columnPos_toUse, const uint *arrOf_condensed_rowId_toUse, const uint arrOf_condensed_rowId_toUse_currPos)  {
  //! What we expect (generic):
  assert_local(self); assert_local(self->nrows); assert_local(self->cnt_bucketsEachRows); assert_local(self->matrixOf_locallyTiled_minPairs); assert_local(self->distmatrix);
  //! Specific:
  assert_local(input_column_pos < self->ncols); assert_local(row_startPos <= row_endPosPlussOne); assert_local(row_startPos < self->nrows); assert_local(column_endPosPlussOne <= self->ncols);
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 0)	
  assert_local(arrOf_previouslyUsedValues); // assert_local(matrixWith_newValues_fixedColumnPos > 0); assert_local(matrixWith_newValues_fixedColumnPos < self->ncols);
#endif
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
  assert(arrOf_condensed_rowId_toUse);
  if(arrOf_condensed_rowId_toUse_currPos == 0) {
    //printf("--------------------- at %s:%d\n", __FILE__, __LINE__);
    return;} //! ie, as the then expect that there is 'no itneresting column to udpate'.
#else
  assert(arrOf_condensed_rowId_toUse == NULL);
  assert( (arrOf_condensed_rowId_toUse_currPos == 0) || (arrOf_condensed_rowId_toUse_currPos == UINT_MAX) ); //! ei, we expec tthis to be explcitly set to '0'.
#endif
  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
  //! Update the log-function, starting 'at a new time-point':
  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn);


  
  //! Identify the bucket-range:
  uint bucket_id = input_column_pos / self->_bucket_size; 
  assert(bucket_id == get_columnBucketId(self, input_column_pos)); //! ei, what we expect wrt. cosnsitency.
  const uint column_pos_startOf_block = bucket_id * self->_bucket_size; //! ie, to 'adjust' the the beginng of the tile.
  assert(column_pos_startOf_block == get_columnBucket_startPosOf_bucket(self, bucket_id));
  uint cnt_minTiles_updated_inConsistent = 0;
  if(false) {printf("(update-column[%u]\t for rows in range[%u...%u/%u] and bucket %u/%u, at [%s]:%s:%d\n", input_column_pos, row_startPos, row_endPosPlussOne, self->_nrows_threshold, bucket_id, self->cnt_bucketsEachRows, __FUNCTION__, __FILE__, __LINE__);}
  uint cnt_updated_rows = 0;  


#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
  {
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop);
    for(uint curr_pos = 0; curr_pos < arrOf_condensed_rowId_toUse_currPos; curr_pos++) {
      const uint row_id = arrOf_condensed_rowId_toUse[curr_pos];
      assert(row_id != UINT_MAX);	    
      assert(mapOf_columnPos_toUse); //! ie, as we then assuem this case is aleays used
      // FIXME: instead of [”elow] cosnider to use a 'dense' lsit ... to avoid the overehad assicated with [”elow]
      const uint column_pos = mapOf_columnPos_toUse[row_id];
      assert(column_pos != UINT_MAX);
      const t_float currentValue = self->distmatrix[row_id][column_pos];
      //! Test if the inserted value may irmpvoe the score:
      const bool minValueIs_imrpoved = __macro__minValueIsImproved(self, row_id, bucket_id, currentValue);      
      //! Investigate if the 'old min-value' no longer hodls, ie, we ened to tierate t'rhoguht eh compelte chunk':
      const bool minValue_forColumnBucket_noLongerHolds = __macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id, column_pos);
      { //! then we need to update the min-valeus for the 'tile':
	if(minValueIs_imrpoved) {  //! then the case is simple, ie, we do not need to idnevistate/idneitfy any specific/new valeus:
	  //! If a column-tile has has an improved/shroter distance, then consider to update 'local-min(row)(column-tile)' and 'global-min(column-tile)':
	  update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id, column_pos, currentValue);
	} else { //! then we need to traverse through the min-tile-set, ie, as it they may be a change in the min-values:	  
	  if(minValue_forColumnBucket_noLongerHolds)  {
	    cnt_updated_rows++; //! ie, as we assume this operation to have a relative high cost wrt. exuection-time.
	    //! Then iterate 'through the remainder':
	    //! if the previous min-value is no longer valid/descritpive, then identify a new min-value (for the given row-column-tile), ie, "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)";
	    __update_minValues_for_columnBucket_iterate(self, row_id, bucket_id, column_pos, currentValue, column_pos_startOf_block);  
	    //! Then 'make it clear' in the post-row-processing that we need to update the global-min wrt. the tiles
	    //! Note: in [”elow] there is no poin in first investigating the "self->mapOf_minTiles_value_atRowIndex[bucket_id]" value, ie, as we do not know the 'second-best' valeu.
	    cnt_minTiles_updated_inConsistent++; //! which is used as a a 'boolean mark'.
	  }
	}
      }
    }
    //! Update the log-function, starting 'at a new time-point':
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop, /*cnt=*/1);
  }
#else //! then we assume that "config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 0"




  uint row_id = row_startPos;



  { //! Then a non-intrisinistic block:
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop);
    for(; row_id < row_endPosPlussOne; row_id++) {
      //update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_isBetter);
#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
      assert(mapOf_columnPos_toUse); //! ie, as we then assuem this case is aleays used
      // FIXME: instead of [”elow] cosnider to use a 'dense' lsit ... to avoid the overehad assicated with [”elow]
      const uint column_pos = mapOf_columnPos_toUse[row_id];
      if(column_pos == UINT_MAX) {continue;} //! ie, as we tehn assume this case is not of interest.
#else
      const uint column_pos = input_column_pos;
      assert(mapOf_columnPos_toUse == NULL); //! ie, our exepctation.
#endif //! ie, at 'end of' "config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns"


      //condensed_iterator++;
      // FIXME[aritcle]: ... we ahve observed that moving [below] into as speretae fucntion reduces the overall exeuction-time by a factor of 1.2x ... egen though we explcitly mark the funciton as "inline" (and use optional gcc-params wrt. this casE) <-- fxime[jc]: suggestison wrt. thsi case, ie, why 'it is not psosibile to combinfugre the gcc compilar wr.t this case?
      //! Then update the row at the givne column-psotion:
      const t_float currentValue = self->distmatrix[row_id][column_pos];
/* #if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1) */
/*       const bool minValueIs_imrpoved = true; */
/* #else */
      //! Test if the inserted value may irmpvoe the score:
      const bool minValueIs_imrpoved = __macro__minValueIsImproved(self, row_id, bucket_id, currentValue);      
      //! Investigate if the 'old min-value' no longer hodls, ie, we ened to tierate t'rhoguht eh compelte chunk':
      const bool minValue_forColumnBucket_noLongerHolds = __macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id, column_pos);
      
      //! Compare the old/previous min-value to the 'value before the distance-matrix was udpated': 
      //if(old_minValue == old_value) 
      { //! then we need to update the min-valeus for the 'tile':
	if(minValueIs_imrpoved) {  //! then the case is simple, ie, we do not need to idnevistate/idneitfy any specific/new valeus:
	  //! If a column-tile has has an improved/shroter distance, then consider to update 'local-min(row)(column-tile)' and 'global-min(column-tile)':
	  update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id, column_pos, currentValue);
	} else { //! then we need to traverse through the min-tile-set, ie, as it they may be a change in the min-values:	  
	  if(minValue_forColumnBucket_noLongerHolds)  {
	    cnt_updated_rows++; //! ie, as we assume this operation to have a relative high cost wrt. exuection-time.
	    //! Then iterate 'through the remainder':
	    //! if the previous min-value is no longer valid/descritpive, then identify a new min-value (for the given row-column-tile), ie, "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)";
	    __update_minValues_for_columnBucket_iterate(self, row_id, bucket_id, column_pos, currentValue, column_pos_startOf_block);  
	    //! Then 'make it clear' in the post-row-processing that we need to update the global-min wrt. the tiles
	    //! Note: in [”elow] there is no poin in first investigating the "self->mapOf_minTiles_value_atRowIndex[bucket_id]" value, ie, as we do not know the 'second-best' valeu.
	    cnt_minTiles_updated_inConsistent++; //! which is used as a a 'boolean mark'.
	  }
	}
      }
      //cnt_updated_rows += __updateAt_column_at_row(self, row_id, column_pos, column_pos_startOf_block, column_endPosPlussOne, arrOf_previouslyUsedValues, bucket_id, &cnt_minTiles_updated_inConsistent);

    }
    //! Update the log-function, starting 'at a new time-point':
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn_details_rowLoop, /*cnt=*/1);
  }
#endif //! wrt. the "config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 0" macro-config-option.

  //! Update the log-function:
  update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_updateColumn, /*count-rows-updated=*/cnt_updated_rows);

  if(cnt_minTiles_updated_inConsistent > 0) { //! Then we evaluate/investgiate wrt. the min-values for all of the rows, ie, a possible time-compelxity-increase of |nrows|.
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
    assert(self->mapOf_endPosOf_columnTiles_toUpdate);
    self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id] = row_endPosPlussOne; //! ie, number of columns to iterate through.
#else
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile); 
    //! Iterate through the best-performing tiles and update.
    //! Note: update the glboally tiled mincolumn-tile for each of the rows:
    __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, row_endPosPlussOne, self->config_useIntrisintitcs);
    if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
    //! Update the log-function, starting 'at a new time-point':
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile, /*counut=*/1);
#endif //! ie, wrt. "config_reWrite_infer_onlyAtOnePoint"
  } else { //! then we asusem that none of the valeus 'represent' a min-tile-case:
    if(false) {printf("- updated [%u...%u][%u] w/bucket_id=%u, at [%s]:%s:%d\n", row_startPos, row_endPosPlussOne, input_column_pos, bucket_id, __FUNCTION__, __FILE__, __LINE__);}
  }

  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.


  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   const t_float val1 = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]; const t_float val2 = self->distmatrix[row_id][column_pos]; */
  /*   //printf("test: %f VS %f, at %s:%d\n", val1, val2, __FILE__, __LINE__); // FIXME: remvoe */
  /*   assert(val1 == val2); */
  /* } */
}


//! @return the closest pair, ie, update scalar_index1, scalar_index2
t_float s_dense_closestPair_getClosestPair(s_dense_closestPair_t *self, uint *scalar_index1, uint *scalar_index2, const uint nrows_threshold, const uint ncols_threshold) {
  //! What we expect:
  assert_local(self); assert_local(self->nrows); assert_local(self->cnt_bucketsEachRows); assert_local(self->matrixOf_locallyTiled_minPairs); assert_local(self->distmatrix);
  //! Specific:
  assert_local(nrows_threshold <= self->nrows); assert_local(ncols_threshold <= self->ncols);
  //! Update the log-function, starting 'at a new time-point':
  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath);

  //! Set the 'return indexes' to empty:
  *scalar_index1 = UINT_MAX; *scalar_index2 = UINT_MAX;

  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.

  /* if(false) { */
  /*   *scalar_index2 = 1; *scalar_index1 = 1;     return 1; */
  /* } */


  if(false) {printf("\n#\t(find-min)\t for |matrix|=%u <= %u, at %s:%d\n", nrows_threshold, self->_nrows_threshold, __FILE__, __LINE__);}

  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   const t_float val1 = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]; const t_float val2 = self->distmatrix[row_id][column_pos]; */
  /*   printf("test: %f VS %f, at %s:%d\n", val1, val2, __FILE__, __LINE__); // FIXME: remvoe */
  /*   assert(val1 == val2); */
  /* } */
  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   printf("test: %f VS %f, at %s:%d\n", self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], self->distmatrix[row_id][column_pos], __FILE__, __LINE__); // FIXME: remvoe */

  /* } */


  //! 
  //! 'Handle' the case where the next matrix-boundary 'is inside an exsiting boundary': iterate through both through the boundayr-columns and boundary-row:
  //! Note: in [”elow] provide/apply logics to find the 'next-best min-value', ie, to (a) update the last column-tile for each row, (b) identify the min-value for each column-tile (ie, for the complete set of rows).
  if(nrows_threshold != self->_nrows_threshold) { //! then 'remove' max-scores assicated to the last row
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateRow);
    //if(false) {
     if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:
      //! Note[time]: for a data-set with 'only incrementing data' the relative time-cost of [”elow] is/seems marginal.
      // printf("..... at %s:%d\n", __FILE__, __LINE__);
      assert_local(self->_nrows_threshold <= self->nrows);
      assert_local(nrows_threshold < self->_nrows_threshold); //! ie, our gneeral expectaiotns wrt. use.
      assert_local(nrows_threshold != 0);
      const uint row_id = self->_nrows_threshold - 1; //! ie, the presumed last row-index.
      assert_local(row_id == nrows_threshold); //! whihch fi not hodl impleis that we need to use a for-loop in [”elow]
      
      //! Investigate if teh Iterate through the row and udpate:
      uint bucket_id = 0; //ncols_threshold / self->_bucket_size; 
      
      //printf("bucket-id=%u, at %s:%d\n", bucket_id);
      const uint bucket_end = __get_local_bucket_size(ncols_threshold, self->_bucket_size);
      //const uint bucket_end = (ncols_threshold / self->_bucket_size) + 1;  //! where '+1' is to 'handle' the 'implcit floor-call, and where 'assicated lgocis' is 'rpvodied' in our below for-loop wrt. the "if(i_end > column_endPosPlussOne)" lgocial-clause.
      for(; bucket_id < bucket_end; bucket_id++) {
	//! Then we need to idnify a new 'min-value' for the column-column-tile, ie, first investgiate the if the row 'is the best-fit row-index':
	if(false) {printf("(update)\tmin-bucket[%u] has row-id=%u, at %s:%d\n", bucket_id, (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
	if(row_id == (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id]) { //! then we need to dinetify a new min-value for the column-tile:
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
	  assert(self->mapOf_endPosOf_columnTiles_toUpdate);
	  self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id] = nrows_threshold; //! ie, number of columns to iterate through.
#else	    
	  //! Update the log-function, starting 'at a new time-point':
	  update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile); 
	    //! The operation:
	  __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, nrows_threshold, self->config_useIntrisintitcs);
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile, /*counut=*/1); 
	  if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
	  assert_local(row_id != (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id]); //! ie, as otehrwise would indciate an incosnsitnecy.
#endif //! ie, wrt. "config_reWrite_infer_onlyAtOnePoint"
	}          
      }
    } else {
      assert_local(self->mapOf_minTiles_value == NULL); //! ie, whta we expect.
    }
    //! Update ehte object 'for the next run':
    self->_nrows_threshold = nrows_threshold;
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateRow, /*cnt=*/1);
  }

  // assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/false)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.

  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   printf("test: %f VS %f, at %s:%d\n", self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], self->distmatrix[row_id][column_pos], __FILE__, __LINE__); // FIXME: remvoe */
  /* } */


  //if(false) {
  if(ncols_threshold != self->_ncols_threshold) { //! then 'remove' max-scores assicated to the last column:
    //! Note[time]: in [”elow] block if [abov€] data-set never changes the time-cost of below is 1.9x (when compare dot the overall exueciotn-time).
    assert_local(ncols_threshold < self->_ncols_threshold); //! ie, our gneeral expectaiotns wrt. use.
    assert_local(self->_ncols_threshold <= self->ncols);
    assert_local(ncols_threshold != 0);
    assert_local(self->_ncols_threshold == (1 + ncols_threshold)); //! ie, otherwise update [”elow] "prev_bucket_id".
    const uint column_pos = ncols_threshold;
    assert_local(column_pos < self->ncols); //! ie, givne [ªbove] set of epxtctiaotns/assertions.
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateColumn);

    //printf("..... at %s:%d\n", __FILE__, __LINE__);
    //! 
    //! Iterate through the last column of each row and udpate:
    const uint bucket_id = (ncols_threshold -1 ) / self->_bucket_size;  //! ie, as we update the last column
    //assert_local(bucket_id != self->_bucket_size);
    const uint prev_bucket_id = ncols_threshold / self->_bucket_size;  //! ie, as we update the last column
    
    if(bucket_id == prev_bucket_id) { //! then there might be errnous indexes in the buckets:
      assert_local(self->_arrOf_tmp_scalarRowValues);


      //! Then update the column-tiels based on [ªbove]:
      //! Note: in [”elow] we iterate through each bucket and test if a bucket 'has row-index less than the givne contraint'      
      //if(false) 
      {
	uint cnt_changed = 0;
	for(uint row_id = 0; row_id < self->_nrows_threshold; row_id++) {
	  
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  const bool isTo_iterateThroguAll = (self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] == ncols_threshold);
#else
	  const bool isTo_iterateThroguAll = (self->distmatrix[row_id][column_pos] == self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]);
#endif
	  //if(false) {
	  if(isTo_iterateThroguAll) { 
	    uint m  = bucket_id * self->_bucket_size; //! ie, to 'adjust' the the beginng of the tile.
	    assert_local(m < ncols_threshold); 	    assert_local(m < self->ncols);
	    t_float min_value = self->distmatrix[row_id][m]; uint foundAt_index = m; m++; //! ie, an 'absoltue max' value	
	    for(; m < ncols_threshold; m++) { 
	      if(self->distmatrix[row_id][m] < min_value) {
		min_value = self->distmatrix[row_id][m];
		foundAt_index = m;
	      }
	    }
	    assert_local(foundAt_index < self->ncols);
	    self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = min_value;// column_pos += bucket_size;
	    if(false) {printf("[%u][bucket=%u] \t min=%f at column=%u, at %s:%d\n", row_id, bucket_id, min_value, foundAt_index, __FILE__, __LINE__);}
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	    self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] = foundAt_index;
	    const uint column_id = self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id];
	    assert_local(column_id < self->_ncols_threshold);  
#endif
	    cnt_changed++;
	  }
	}
	//if(false) {
	if(self->isTo_use_fast_minTileOptimization) { //! then we allcoate memory an intlaize:
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  const uint row_id_local = self->mapOf_minTiles_value_atRowIndex[bucket_id];
	  const bool needTo_update = (ncols_threshold >= self->matrixOf_locallyTiled_minPairs_columnPos[row_id_local][bucket_id]);
#else 
	  const bool needTo_update = (cnt_changed > 0);
#endif
	  //if(row_id >= ncols_threshold) {
	  if(needTo_update) {
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
	    assert(self->mapOf_endPosOf_columnTiles_toUpdate);
	    self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id] = nrows_threshold; //! ie, number of columns to iterate through.
#else	    
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile); 
	    //! Iterate through the best-performing tiles and update.
	    //! Note: update the glboally tiled mincolumn-tile for each of the rows:
	    __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, nrows_threshold, self->config_useIntrisintitcs);
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile, /*counut=*/1); 
	    if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
#endif //! ie, wrt. "config_reWrite_infer_onlyAtOnePoint"
	  }
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 0)
#ifndef NDEBUG
	  const uint row_id = self->mapOf_minTiles_value_atRowIndex[bucket_id];
	  //printf("row_id=%u, at %s:%d\n", row_id, __FILE__, __LINE__);
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
	  //! Validate the correctness wrt. the column:
	  const uint column_id = self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id];
	  assert_local(column_id < self->_ncols_threshold);  
	  //! Valdiate that the score is correctly set:
	  assert_local(self->distmatrix[row_id][column_id] == self->mapOf_minTiles_value[bucket_id]);
#endif
#endif
#endif //! ie, wrt. "config_reWrite_infer_onlyAtOnePoint"
	}	
      } /* else { */
	/* //! Frist 'collect' the old values: */
	/* for(uint row_id = 0; row_id < self->_nrows_threshold; row_id++) { */
	/*   self->_arrOf_tmp_scalarRowValues[row_id] = self->distmatrix[row_id][column_pos]; */
	/* } */
      /* 	s_dense_closestPair_updateAt_column(self, column_pos, /\*row_startPos=*\/0, /\*row_endPosPlussOne=*\/nrows_threshold, /\*column_endPosPlussOne=*\/ncols_threshold, /\*arrOf_previouslyUsedValues=*\/self->_arrOf_tmp_scalarRowValues); */
      /* } */
    } //! else we assume the 'old' buckets will enver be accessed, ie, no need to update them.

    //! Update ehte object 'for the next run':
    self->_ncols_threshold = ncols_threshold;
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath_details_updateColumn, /*cnt=*/1);
  }

  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   printf("test: %f VS %f, at %s:%d\n", self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], self->distmatrix[row_id][column_pos], __FILE__, __LINE__); // FIXME: remvoe */
  /*   //assert(false); */
  /* } */

  uint bucket_size = __get_local_bucket_size(ncols_threshold, self->_bucket_size);


#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
  if(self->isTo_use_fast_minTileOptimization) { //! Then we update the global set of min-tiel-values:
    assert(self->mapOf_endPosOf_columnTiles_toUpdate);
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile); 
    //! Then iterate through the set of buckets, and update:
    uint cnt_updated = 0;
    for(uint bucket_id = 0; bucket_id < bucket_size; bucket_id++) {
      uint column_endPosPlussOne = self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id];
      if(column_endPosPlussOne > 0) {
	assert(column_endPosPlussOne <= ncols_threshold);
	column_endPosPlussOne = min(ncols_threshold, column_endPosPlussOne);
	//! The call:
	//! Iterate through the best-performing tiles and update.
	//! Note: update the glboally tiled mincolumn-tile for each of the rows:
	__forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, column_endPosPlussOne, self->config_useIntrisintitcs, __get_obj_rowChunk(self));
	assert_local(column_endPosPlussOne <= self->_ncols_threshold);
	const uint found_bucket_minValue_atRow = (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id];
	if(false) {printf("\t\t (updated-bucket)\t for columns[%u, %u/%u], where min-row[bucket=%u]=%u/%u, at %s:%d\n", 0, column_endPosPlussOne, self->_ncols_threshold, bucket_id, found_bucket_minValue_atRow, self->_ncols_threshold,  __FILE__, __LINE__);}
	assert_local(found_bucket_minValue_atRow < self->_ncols_threshold); //! ie, givne our above update-procedure/calls.
	//! Reset:
	self->mapOf_endPosOf_columnTiles_toUpdate[bucket_id] = 0; //! ie, an 'appraoch' which ensures cosnistency.
	cnt_updated++;
      }
    }
    //! Update the log-function, starting 'at a new time-point':
    update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_globalColumnTile, /*count=*/cnt_updated); 
  }
#endif


  /* { */
  /*   const uint row_id = 1; const uint bucket_id = 0; const uint column_pos = 0; */
  /*   printf("test: %f VS %f, at %s:%d\n", self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], self->distmatrix[row_id][column_pos], __FILE__, __LINE__); // FIXME: remvoe */
  /*   //assert_local(false); */
  /* } */

  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/true)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.

  t_float retVal_minValue = T_FLOAT_MAX; t_float bucket_id_foundAt = T_FLOAT_MAX;

  // assert_local(false); // FIXME: ... in [below] ... sperate between #(bucket-size) and |bucket-size| ... seems like the "__get_local_bucket_size(..)" returns "|bucket-size|" while "self->cnt_bucketsEachRows" rerpesents/describes "#(bucket-size)"
  

  //printf("cnt_bucketsEachRows=%u, bucket_size=%u, at %s:%d\n\n", self->cnt_bucketsEachRows, __FILE__, __LINE__);
  assert_local(bucket_size <= self->cnt_bucketsEachRows); //! ie, as otherwise we migth have a bug.

  /* if(false) { */
  /*   *scalar_index2 = 1; *scalar_index1 = 1;     return 1; */
  /* } */

  // FIXME: in our perofmrance-tests both test wrt. time-eprofrmance the two 'options' in [”elow] ... and simarly wr.t correctness.
  if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:
    //! Iterate through the buckets:
    uint bucket_id = 0; 
    if(false) {printf("bucket-range=[%u, %u/%u], and ncols_threshold=%u, at %s:%d\n", bucket_id, bucket_size, self->_bucket_size, ncols_threshold, __FILE__, __LINE__);}
    //! Then we use a 'naivistic' appraoch:      
    for(; bucket_id < bucket_size; bucket_id++) {
      if(retVal_minValue > self->mapOf_minTiles_value[bucket_id]) {
	retVal_minValue = self->mapOf_minTiles_value[bucket_id];
	//! Then update the index-refrences:
	*scalar_index1 = (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id]; 
	bucket_id_foundAt = bucket_id;
      }
      if(false) {printf("min-bucket[%u] has row-id=%u/%u, w/score=%f, at %s:%d\n", bucket_id, (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], ncols_threshold, self->mapOf_minTiles_value[bucket_id], __FILE__, __LINE__);}
      assert_local((uint)self->mapOf_minTiles_value_atRowIndex[bucket_id] < ncols_threshold); //! ie, givne our above update-procedure/calls.
    }
    assert_local(retVal_minValue != T_FLOAT_MAX); assert_local(bucket_id_foundAt != T_FLOAT_MAX); //! ie, as we otehrwise might have a bug.          
  } else { //! then we need to evlauate "|nrows|*|column-tile|":
    //const uint row_endPosPlussOne_intri = macro__get_maxIntriLength_float(nrows_threshold);
    //const uint cntBuckets_intri = (self->config_useIntrisintitcs) ? macro__get_maxIntriLength_float(self->cnt_bucketsEachRows) : 0;
    //! Iterate through the buckets:    
    for(uint row_id = 0; row_id < nrows_threshold; row_id++) {
      uint bucket_id = 0; 
      for(; bucket_id < bucket_size; bucket_id++) {
	if(retVal_minValue > self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]) {
	  retVal_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  //! Then update the index-refrences:
	  *scalar_index1 = row_id;    bucket_id_foundAt = bucket_id;	    
	}
      }
    }     
  }

  //! Update the log-function, starting 'at a new time-point':
  update_log_endMeasurement(self, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_fromFunc_getShortestPath, /*cnt=*/0);

  // FIXME[critical]:re vmoe [”elow] clause:

/* #ifdef NDEBUG */
/*   if(*scalar_index1 == UINT_MAX) { */
/*     const uint bucket_id = 0; */
/*     retVal_minValue = self->mapOf_minTiles_value[bucket_id]; */
/*     *scalar_index1 = (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id];  */
/*     bucket_id_foundAt = bucket_id; */
/*   }  */
/*   *scalar_index2 = self->matrixOf_locallyTiled_minPairs_columnPos[(uint)*scalar_index1][(uint)bucket_id_foundAt]; */
/*   return retVal_minValue; */
/* #else */
/*   fprintf(stderr, "!!\t seems like a critica error, ie, please investgiate. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
/*   assert(false); */
/* #endif */
  
  //!
  //! Our last step (in finding the min-values): identify the 'outer index':
  assert(*scalar_index1 != UINT_MAX); assert(retVal_minValue != T_FLOAT_MAX); assert(bucket_id_foundAt != T_FLOAT_MAX); //! ie, as we otehrwise might have a bug.
  assert(*scalar_index1 < self->nrows);     assert(*scalar_index1 < self->_nrows_threshold);

#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
  assert((uint)*scalar_index1 < self->nrows);
  assert((uint)bucket_id_foundAt < self->cnt_bucketsEachRows);
  *scalar_index2 = self->matrixOf_locallyTiled_minPairs_columnPos[(uint)*scalar_index1][(uint)bucket_id_foundAt];
  if(false) {printf("#\t (min-value)\t %f\t at (%u, %u|%u), for bucket=%u, at %s:%d\n", retVal_minValue, *scalar_index1, *scalar_index2, self->_ncols_threshold, (uint)bucket_id_foundAt, __FILE__, __LINE__);}
  assert_local(*scalar_index2 < self->ncols);   assert_local(*scalar_index2 < self->_ncols_threshold);
#else
  {//! Find the 'column-pos' with the value in question, ie, iterate through one signle tile assicated to one defined row:
    //! Note: below call is the 'key-point' in our optmizaiton, ie, to not store the column-pos in our index-update.
    uint column_pos = (uint)bucket_id_foundAt * (uint)self->_bucket_size;
    assert_local(column_pos < self->_ncols_threshold);
    uint end_pos = column_pos + self->_bucket_size;
    if(end_pos > self->_ncols_threshold) {end_pos = self->_ncols_threshold;}
    const uint row_id = *scalar_index1;
    assert_local(retVal_minValue == self->matrixOf_locallyTiled_minPairs[row_id][(uint)bucket_id_foundAt]); //! ie, as it otherwise would indciate an incosnsteincy int eh udpate-procedure
    if(false) {printf(" column_pos=[%u, %u/%u], given column-bucket=%u, and min-id[%u][%u]=%f, at %s:%d\n", column_pos, end_pos, self->_ncols_threshold, (uint)bucket_id_foundAt, row_id, (uint)bucket_id_foundAt, self->matrixOf_locallyTiled_minPairs[row_id][(uint)bucket_id_foundAt], __FILE__, __LINE__);}
    //    const uint end_pos = (end_pos <= ncols_threshold) ? column_pos + self->_bucket_size : ncols_threshold;
    for(; column_pos < end_pos; column_pos++) {
      if(false) {printf("\t\t\t[%u][%u]=%f, at %s:%d\n", *scalar_index1, column_pos, self->distmatrix[*scalar_index1][column_pos], __FILE__, __LINE__);}
      if(self->distmatrix[*scalar_index1][column_pos] == retVal_minValue) {
	// FIXME: test the perofrmance of 'instead strotign the min-valeu for "*scalar_index2" in a seperate table ... which we update when updating "matrixOf_locallyTiled_minPairs"
	*scalar_index2 = column_pos;
      }
    }
#ifndef NDEBUG
    if(*scalar_index2 == UINT_MAX) { //! then there might be an error in the udpate-procedure: we itterat ethrough the matrix to idnetify the 'actual' column-pos, ie, to simplify debugging, ie, if any:
      uint end_pos_test = column_pos + self->_bucket_size;
      if(end_pos_test > self->ncols) {end_pos_test = self->ncols;}
      for(; column_pos < end_pos_test; column_pos++) {
	if(self->distmatrix[*scalar_index1][column_pos] == retVal_minValue) {
	  *scalar_index2 = column_pos;
	}
      }
      printf("!!\t\t seems like an error in the update-lgoics for \"matrixOf_locallyTiled_minPairs\", ie, as the column-pos was found at '%u' > |%u|, at %s:%d\n", *scalar_index2, end_pos, __FILE__, __LINE__);
      assert_local(*scalar_index2 != UINT_MAX);
    }
#endif
  }
#endif
  //! we expect to have found both indexes:
  if(false) {printf("#\t (min-value)\t %f\t at (%u, %u), for bucket=%u, at %s:%d\n", retVal_minValue, *scalar_index1, *scalar_index2, (uint)bucket_id_foundAt, __FILE__, __LINE__);}
  // FIXME: include [”elow]:
  assert_local(*scalar_index1 != UINT_MAX);   assert_local(*scalar_index2 != UINT_MAX);
  assert_local(*scalar_index1 < self->nrows);     assert_local(*scalar_index1 < self->_nrows_threshold);
  assert_local(*scalar_index2 < self->ncols);   assert_local(*scalar_index2 < self->_ncols_threshold);
  assert_local(classWideTests_runTime(self, self->_nrows_threshold, self->_ncols_threshold,  __FILE__, __LINE__, /*test_distancematrix=*/true)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.
  const t_float expected_minValue = self->distmatrix[(uint)*scalar_index1][(uint)*scalar_index2];
  if(false) {printf("#\t (min-value)\t %f=%f\t at (%u, %u), for bucket=%u, at %s:%d\n", retVal_minValue, expected_minValue, *scalar_index1, *scalar_index2, (uint)bucket_id_foundAt, __FILE__, __LINE__);}
  assert_local(retVal_minValue == expected_minValue);//! ie, as we expec tthe "distmatrix" to always be cosnistnecly updated.


  //! @return the shortest distance:
  return retVal_minValue;
}



//! Write out the min-scores for the "self" object.
void writeOut_MinScores_humanized(FILE *stream, const s_dense_closestPair_t *self) {
  assert_local(stream); assert_local(self); assert(self->_ncols_threshold); assert(self->_nrows_threshold); assert(self->cnt_bucketsEachRows); //! ie, what we expect
  //! Write out meta-data:
  fprintf(stream, "\n# The min-scores for a matrix with |%u||%u| elements divided into %u column-buckets, a summary generated at %s:%d\n", self->_nrows_threshold, self->_ncols_threshold, self->cnt_bucketsEachRows, __FILE__, __LINE__);
  const uint bucket_end = __get_local_bucket_size(self->_ncols_threshold, self->_bucket_size);


  //! First write out the 'local min-scores':
  fprintf(stream, "-\t The local-min-scores are:\n");
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
  assert_local(self->matrixOf_locallyTiled_minPairs_columnPos);
#endif  
  for(uint row_id = 0; row_id < self->_nrows_threshold; row_id++) {
    //uint column_pos = 0;
    for(uint bucket_id = 0; bucket_id < bucket_end; bucket_id++) {
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
      fprintf(stream, "\t[%u][bucket=%u]\t score=%f at column=%u, at %s:%d\n", row_id, bucket_id, 
	     self->matrixOf_locallyTiled_minPairs[row_id][bucket_id],
	     (uint)self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id], 
	     __FILE__, __LINE__);
#else
      fprintf(stream, "\t[%u][bucket=%u]\t column=%u, at %s:%d\n", row_id, bucket_id, 
	     self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id],
	     __FILE__, __LINE__);
#endif
    }
  }

  if(self->isTo_use_fast_minTileOptimization) { //! then we allcoate memory an intlaize:  
    //! Write out the 
    fprintf(stream, "-\t The global-min-scores are:\n");
    for(uint bucket_id = 0; bucket_id < bucket_end; bucket_id++) {
      const uint row_id = (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id];
      const t_float score = self->mapOf_minTiles_value[bucket_id];
#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
      fprintf(stream, "\t[bucket=%u]\t score=%f, at (%u, %u), at %s:%d\n", bucket_id, 
	      score,
	      row_id,
	      (uint)self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id], 
	     __FILE__, __LINE__);
#else
      fprintf(stream, "\t[bucket=%u]\t score=%f, at (%u, ...), at %s:%d\n", bucket_id, 
	      score,
	      row_id,
	     __FILE__, __LINE__);
#endif
    }
  }

}
