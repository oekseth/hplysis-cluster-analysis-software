#include "alg_floydWarshall.h"
 
/* A utility function to print solution */
static void printSolution(t_float **matrix, const uint nrows) {
  printf ("Following matrix shows the shortest distances"
	  " between every pair of vertices \n");
  for (int i = 0; i < nrows; i++) {
    for (int j = 0; j < nrows; j++) {
      if(isOf_interest(dist[i][j])) {
	printf ("%7d", dist[i][j]);
      } else {
	printf("%7s", "INF");
      }
      printf("\n");
    }
  }
}
 
// Solves the all-pairs shortest path problem using Floyd Warshall algorithm
//! Srcs: "http://www.geeksforgeeks.org/dynamic-programming-set-16-floyd-warshall-algorithm/"
t_float **alg_floydWarshall (const t_float **matrix, const uint nrows) {

  /* dist[][] will be the output matrix that will finally have the shortest 
     distances between every pair of vertices */
  const t_float empty_0 = 0;
  t_float **dist = allocate_2d_list_float(nrows, nrows, empty_0);

 
  assert(false); // FIXME[conceptaul]: trey desciribng didffneret suec-ases wrt. 'this' .... eg, when comapred to sim-metrics ... and how 'to set masks' ... 
  assert(false); // FIXME[code]: udpate docuemtntaiotn .... adn try suggesitng/(imrpving/using ad fiferent API ... eg, by 'replacing' "0" with "T_FLOAT_MAX" 

  /* Initialize the solution matrix same as input graph matrix. Or 
       we can say the initial values of shortest distances are based
       on shortest paths considering no intermediate vertex. */
  for (uint i = 0; i < nrows; i++) {
    for (uint j = 0; j < nrows; j++) {
      dist[i][j] = matrix[i][j];
    }
  }
 
  /* Add all vertices one by one to the set of intermediate vertices.
      ---> Before start of a iteration, we have shortest distances between all
      pairs of vertices such that the shortest distances consider only the
      vertices in set {0, 1, 2, .. k-1} as intermediate vertices.
      ----> After the end of a iteration, vertex no. k is added to the set of
      intermediate vertices and the set becomes {0, 1, 2, .. k} */
  for (uint k = 0; k < nrows; k++) {
    // Pick all vertices as source one by one
    for (uint i = 0; i < nrows; i++) {
      // Pick all vertices as destination for the
      // above picked source
      for (uint j = 0; j < nrows; j++) {
	// If vertex k is on the shortest path from
	// i to j, then update the value of dist[i][j]
	if( (dist[i][k] + dist[k][j]) < dist[i][j])
	  dist[i][j] = dist[i][k] + dist[k][j];
      }
    }
  }
 
  // Print the shortest distance matrix
  if(false) {
    printSolution(dist, nrows);
  }
  
  //free_2d_list_float(&dist);
  return dist;
}

 
// driver program to test above function
int test_alg_floydWarshall() {
  /* Let us create the following weighted graph
            10
       (0)------->(3)
        |         /|\
      5 |          |
        |          | 1
       \|/         |
       (1)------->(2)
       3           */
  const uint V = 4;
  t_float graph[V][V] = { {0,   5,  T_FLOAT_MAX, 10},
			  {T_FLOAT_MAX, 0,   3, T_FLOAT_MAX},
			  {T_FLOAT_MAX, T_FLOAT_MAX, 0,   1},
			  {T_FLOAT_MAX, T_FLOAT_MAX, T_FLOAT_MAX, 0}
  };
 
  // Print the solution
  alg_floydWarshall(graph);
  return 0;
}
