#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.

/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a permtuation of "tut_inputFile_2.c".
   @remarks in this test-case we examplify how to evlauate/test different randomze-cases: we foucs on time-execution-perofmrnace. Of interest is ot idnetify the wrost-case exec-time and result-accuracy for challing data-serts. Therefre we (in this test-case) use/evlauate large/difficult data-sets (where size is descided using a manual/itnal investigaiton). To facilitate a simplfied evlauation of the clsuter-accuracy we 'merge'/concncatenate each of the well-defined data-sets with a data-set defined by a cofngiruation-variable named config__nameOfDefaultVariable. Our null-hypothesis is that our MINE-approach provide a lower/improved worst-case perofmrance (when compared to alternaitve appraoches). 
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;

  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;

  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "binomial_p005";
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //obj_config.globalConfig__isToStore__inputMatrix__inFormat__csv = isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.


  assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  /* //! -------------------------------------------- */
  /* //! */
  /* //! File-specific cofnigurations:  */
  /* s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t(); */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = false; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;   */
  /* fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file. */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config; */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = true; */
  /* //! */
  //printf("at %s:%d\n", __FILE__, __LINE__);
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  // printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! We are interested in a more performacne-demanind approach: 
  const uint sizeOf__nrows = 500;
  const uint sizeOf__ncols = 500;
  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat = fileRead_config__syn;
  s_kt_matrix_t matrix_concatToAll; setTo_empty__s_kt_matrix_t(&matrix_concatToAll);
  //printf("at %s:%d\n", __FILE__, __LINE__);
  if(config__nameOfDefaultVariable && strlen(config__nameOfDefaultVariable)) {
    //printf("at %s:%d\n", __FILE__, __LINE__);
    matrix_concatToAll = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable, fileRead_config__syn__concat);
    //printf("at %s:%d\n", __FILE__, __LINE__);
    //! 
    //! Update the configuraiton-object:
    assert(matrix_concatToAll.ncols > 0);
    fileRead_config__syn.mat_concat = &matrix_concatToAll;
  }
  //printf("at %s:%d\n", __FILE__, __LINE__);
  //! -------------------------------------------------------------------------
  //!
  //!
  //! Intiate the sytnetic data-sets:
  const uint mapOf_functionStrings_base_size = 17;
  const char *mapOf_functionStrings_base[mapOf_functionStrings_base_size] = {
    "lines-different-ax",
    "lines-curved",
    "lines-different-ax-and-axx",
    "lines-ax-inverse-x",
    //! --
    "lines-circle",
    "lines-sinsoid",
    "lines-sinsoid-curved",
    //! [ªbove]: cnt=(3+4)=7;
    //! ------------------------ 
    "random",
    "uniform",
    "binomial_p05",
    "binomial_p010",
    "binomial_p005",
    //! --
    "flat",
    "linear-equal",
    "linear-differentCoeff-b",
    "linear-differentCoeff-a",
    "sinus",
    //! [ªbove]: cnt=(5+5)=10 ... ie, 17 cases.
  };
  
  //printf("at %s:%d\n", __FILE__, __LINE__);
  //! ---            
  static const uint stringOf_noise_size = 3;
  //assert((uint)s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef == stringOf_noise_size); //! ie, what we expect for [”elow]
  const char *stringOf_noise[stringOf_noise_size] = {
    "",
    "-medium",
    "-large",
  };
  //! --------------------------
  //!
  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint mapOf_realLife_size = mapOf_functionStrings_base_size * stringOf_noise_size * __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  //printf("at %s:%d\n", __FILE__, __LINE__);
  for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {      
    for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
      assert(base_id < mapOf_functionStrings_base_size);       assert(noise_id < stringOf_noise_size);
      //! Budil the sample-string:
      //! Note: though [”elow] will result in a memory-leaksge we assume 'this leakge' will be insigidncat, ie, where 'this leakge' is allowed to reduce the change of the user (ie, you) to be cofnused wr.t how to apply the lgocis we exmaplify (in this code-chunk-example).
      const char empty_0 = 0; const uint char_Def_Size = 1000;
      //printf("at %s:%d\n", __FILE__, __LINE__);
      char *stringOf_sampleData_type = allocate_1d_list_char(char_Def_Size, empty_0); sprintf(stringOf_sampleData_type, "%s%s", mapOf_functionStrings_base[base_id], stringOf_noise[noise_id]);
      for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
	const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
	assert(current_pos < mapOf_realLife_size);
	//printf("at %s:%d\n", __FILE__, __LINE__);
	//!
	//! Add the object:
	mapOf_realLife[current_pos].tag = stringOf_sampleData_type;
	mapOf_realLife[current_pos].file_name = stringOf_sampleData_type;
	mapOf_realLife[current_pos].fileRead_config = fileRead_config__syn;
	mapOf_realLife[current_pos].inputData__isAnAdjcencyMatrix = false;
	mapOf_realLife[current_pos].k_clusterCount = k_clusterCount;
	mapOf_realLife[current_pos].mapOf_vertexClusterId = NULL;
	mapOf_realLife[current_pos].mapOf_vertexClusterId_size = 0;
	mapOf_realLife[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
	mapOf_realLife[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
	//mapOf_realLife[current_pos] = {/*tag=*/stringOf_sampleData_type, /*file_name=*/stringOf_sampleData_type, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/k_clusterCount, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans};
	//!
	current_pos++;
      }
    }
  }

  //! ----------------------------------
  //! 
  //! Apply Logics:
  printf("at %s:%d\n", __FILE__, __LINE__);
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_concatToAll);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
