#include "kt_longFilesHandler.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */



/**
   @brief itnaites an object to write out a 'mapping' between 'ifnroamtive files-names' and 'file-naems which are short enough to be acceptable on a gvine system' (oekseth, 06. des. 2016)
   @param <self> is the object to hold teh data
   @param <result_directory> which if Not set to NULL describes the directory 'to store the data in'
   @param <file_nameMeta> is the file-naem to exprot the 'file-name-mappigns' to.
   @param <stringOf_header> if Not set to NULL then we write this strin 'to the top' of the file
   @param <stream_out> which if Not set to NULL implies that we use a use-specified file-dseciprtior.
   @return an itnaited object
   @remarks a use-case for "stream_out" conserns:
   -- system-speicifc: eg, to use "stdout" or "stderr"
   -- result-concatnation: to 'merge' muliple header-files, eg, by suing the same "stream_out" in muliple objects
 **/
s_kt_longFilesHandler_t initAndReturn__s_kt_longFilesHandler_t(const char *result_directory, const char *file_nameMeta_input, const char *stringOf_header, FILE *stream_out) {
  s_kt_longFilesHandler_t self;
  //!
  //! Default settings:
  self.stream_out = stdout;
  self.currentCounter = 0;
  self.result_directory = result_directory;
  self.stream_out__isAllocated = false;
  //!
  //! Open teh file:
  if( (stream_out == NULL) && file_nameMeta_input && strlen(file_nameMeta_input) ) {
    self.stream_out__isAllocated = true;
    char str_tmp[1000] = {'\0'};
    //!
    //! Open:
    if(file_nameMeta_input && strlen(file_nameMeta_input)) {    
      //!
      //! Add lgocis whent eh user specifieds a 'difficutl-to-fiend' name for the meta-resutl-fiel, ie, to simplify 'manual inveisttaiton of atuoamtic-created file-names'.
      const uint file_nameMeta_size = strlen(file_nameMeta_input) + 200;
      const char empty_0 = 0;
      char *file_nameMeta = allocate_1d_list_char(file_nameMeta_size, empty_0);
      if(strlen(file_nameMeta_input) > 4) {
	const char *tail = file_nameMeta_input + (strlen(file_nameMeta_input) - 4);
	if(0 == strncmp(tail, ".tsv", 4)) {
	  sprintf(file_nameMeta, "%s", file_nameMeta_input);
	} else { //! then we include a meta-.suffix.
	  sprintf(file_nameMeta, "%s.meta.tsv", file_nameMeta_input);
	}
      } else {
	sprintf(file_nameMeta, "%s", file_nameMeta_input);
      }

      char file_nameMeta__local[1000] = {'\0'};
      if(result_directory && strlen(result_directory) ) {
	if(result_directory[strlen(result_directory)-1] != '/') {
	  sprintf(file_nameMeta__local, "%s/%s", result_directory, file_nameMeta); //! ie, build a unique string by combining the memory-address and the unix-time.
	} else {
	  sprintf(file_nameMeta__local, "%s%s", result_directory, file_nameMeta); //! ie, build a unique string by combining the memory-address and the unix-time.
	}
      } else {
	sprintf(file_nameMeta__local, "%s", file_nameMeta); //! ie, build a unique string by combining the memory-address and the unix-time.
      }      
      assert(strlen(file_nameMeta) > 1);
      self.stream_out = fopen(file_nameMeta__local, "w");
      // printf("given file_nameMeta=\"%s\" sets the stream-out variable, at %s:%d\n", file_nameMeta, __FILE__, __LINE__);
      if (self.stream_out == NULL) {
	struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start);
	if(result_directory && strlen(result_directory) ) {
	  if(result_directory[strlen(result_directory)-1] != '/') {
	    sprintf(str_tmp, "%s/%p_%lld.brief.tsv", result_directory, &self, (long long int)clock_time_start); //! ie, build a unique string by combining the memory-address and the unix-time.
	  } else {
	    sprintf(str_tmp, "%s%p_%lld.brief.tsv", result_directory, &self, (long long int)clock_time_start); //! ie, build a unique string by combining the memory-address and the unix-time.
	  }
	} else {
	  sprintf(str_tmp, "%p_%lld.brief.tsv", &self, (long long int)clock_time_start); //! ie, build a unique string by combining the memory-address and the unix-time.
	}
	self.stream_out = fopen(str_tmp, "w");
	if (stream_out != NULL) {      
	  fprintf(stderr, "!!\t Seems like your input-file-name \"%s\" was all to long: we have now shorted the file-name into \"%s\", and included the 'original-fiel-name' as a '#! fileName: ' tag in your result-file. For quesitons please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", file_nameMeta, str_tmp, __FUNCTION__, __FILE__, __LINE__);
	  //! Add the file-name as a 'tag':
	  fprintf(stream_out, "#! fileName: \t%s\n", str_tmp);
	}
	file_nameMeta = str_tmp;
      }
      if (self.stream_out == NULL) {      
	fprintf(stderr, "!!\t Unale to open the file \"%s\" for writing: please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", file_nameMeta, __FUNCTION__, __FILE__, __LINE__);
	return self;
      }
      assert(file_nameMeta); assert(file_nameMeta_input != file_nameMeta);
      free_1d_list_char(&file_nameMeta);
    } else {self.stream_out = stdout;}
    // printf("sets the stream-out variable, at %s:%d\n", __FILE__, __LINE__);
    //self.stream_out = stream_out;
  }
  //!
  //! Write out the result:
  assert(self.stream_out);
  if(stringOf_header && strlen(stringOf_header)) {
    fprintf(self.stream_out, "#! %s\n", stringOf_header);
  }
  //! @return
  return self;
}
//! De-allcoates the s_kt_longFilesHandler_t object
void free__s_kt_longFilesHandler_t(s_kt_longFilesHandler_t *self) {
  //! Clsoe the file, ie, if opened.
  //if( (self->stream_out != NULL) && (self->stream_out != stdout) && (self->stream_out != stderr) ) {
  if(self->stream_out__isAllocated) {
    fclose(self->stream_out); self->stream_out = NULL;
    self->stream_out__isAllocated = false;
  }
  //! Reset the variables:
  self->result_directory = NULL;
  self->currentCounter = 0;
  self->result_directory = NULL;
  self->stream_out__isAllocated = false;
}

/**
   @brief allcoates a file-naem which is accrtaapble 'on file-systems' and include 'into the result-file' a mapping between 'the enw-file-name' and 'internal-file-anems'.
   @param <self> is the object to hold teh data
   @param <long_file_name> is the file-name to 'store' in teh exprot-file
   @param <prefix> which if Not set to NULL is 'inlcuded' prior to the 'unqiue file-tag'
   @return a new-allcoated file-naem whcih 'is short enough to be used on all/most OS (oekseth, 06. des. 2016).
 **/
char *allocateDenseFileName__s_kt_longFilesHandler_t(s_kt_longFilesHandler_t *self, const char *long_file_name, const char *prefix) {
  assert(self); assert(long_file_name); assert(self->stream_out);
  if(prefix) {assert(strlen(prefix) < 100);} //! ie, as this object-suage may otherwise be poitnelss.
  //!
  //! 
  char str_tmp[1000] = {'\0'};
  struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start);
  const char *result_directory = self->result_directory;
  if(!prefix || !strlen(prefix)) {prefix = "";}
  if(result_directory && strlen(result_directory) ) {
    if(result_directory[strlen(result_directory)-1] != '/') {
      sprintf(str_tmp, "%s/%s%p_%lld.%llu.tsv",  result_directory, prefix, self, (long long int)clock_time_start, self->currentCounter++); //! ie, build a unique string by combining the memory-address and the unix-time.
    } else {
      sprintf(str_tmp, "%s%s%p_%lld.%llu.tsv", result_directory, prefix, self, (long long int)clock_time_start, self->currentCounter++); //! ie, build a unique string by combining the memory-address and the unix-time.
    }
  } else {
    sprintf(str_tmp, "%s%p_%lld.%llu.tsv", prefix, self, (long long int)clock_time_start, self->currentCounter++); //! ie, build a unique string by combining the memory-address and the unix-time.
  }  
  assert(strlen(str_tmp) > 1);
  //!
  //! Write otu the mappings:
  fprintf(self->stream_out, "%s\thas_long_file_name\t%s\n", str_tmp, long_file_name);
  //! 
  //! Cosntruct a copy, and return:
  const uint cnt_chars = strlen(str_tmp) + 1; const char def_value = '\0';
  char *ret_string = allocate_1d_list_char(cnt_chars, def_value);
  assert(ret_string);
  strncpy(ret_string, str_tmp, cnt_chars - 1); 
  //! @return
  return ret_string;
}
