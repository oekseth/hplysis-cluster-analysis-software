#ifndef kt_storage_resultOfClustering_h
#define kt_storage_resultOfClustering_h

/**
   @file 
   @brief lgocics for analsying different interptaitons of a covaraince-matrix, when seen through the lense of clsuteirng-algorithms
   @date 06.08.2020
   @author oekseth   
 **/


#include "hpLysis_api.h"
#include "alg_dbScan_brute.h"
#include "hp_ccm.h"
#include "hp_exportMatrix.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "kt_matrix.h"
#include "kt_list_1d_string.h"
#include "export_ppm.h" //! which is used for *.ppm file-export.
#include "kt_matrix_cmpCluster.h"
#include "aux_sysCalls.h" //! eg, to simplify usage of Linux-system-calls

//! Export data to ppm
//! Note: the default fucntion exports data using grayscales. In the function we tranlsate numbers inot a range '0 ... 1', hence the need for data copy
static void kt_storage_resultOfClustering_exportTo_ppm(s_kt_matrix_t &mat_result, const char *file_base, const char *file_pref, const bool isTo_alsoExportTo_tsv) {
  //! Copy:
  s_kt_matrix_t mat_cpy = initAndReturn__copy__s_kt_matrix(&mat_result, true);
  //! Adjust(1): get max.
  t_float score_max = T_FLOAT_MIN_ABS;
  t_float score_min = T_FLOAT_MAX;
  for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
      const t_float score = mat_cpy.matrix[row_id][col_id];
      if(isOf_interest(score)) {
	score_max = macro_max(score_max, score);
	score_min = macro_min(score_min, score);
      }
    }
  }
  printf("# score_max:%f, score_min=%f, given dims=%u,%u, at %s:%d\n", score_max, score_min,  mat_cpy.nrows, mat_cpy.ncols, __FILE__, __LINE__);
  if( (score_max != T_FLOAT_MIN_ABS) && (score_min = T_FLOAT_MAX) ) { // && (score_max != 0) ) {
    //! Adjust(1):
    //t_float score_max_inv = 1/(score_max - score_min); // FIXME: validate this.
    const t_float score_max_inv = 1/(score_max);
    for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
	const t_float score = mat_cpy.matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  mat_cpy.matrix[row_id][col_id] *= score_max_inv; 
	}
      }
    }
    if(isTo_alsoExportTo_tsv) {
      //if(self.isTo_exportResultFiles) {
      char file_name_result[1000]; memset(file_name_result, '\0', 1000); 
      if((file_base != NULL) && (strlen(file_base) > 0) ) {
	sprintf(file_name_result, "%s_%s_img.tsv", file_base, file_pref);
      } else {
	sprintf(file_name_result, "%s_img.tsv", file_pref);
      }
      fprintf(stderr, "(info:export::tsv)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(&mat_result, file_name_result, NULL);
      //}
      //export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL);
      //(&mat_cpy, file_name_result, NULL);
      //export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(&mat_cpy, file_name_result, NULL);
    }
    //! Export:
    char file_name_result[1000]; memset(file_name_result, '\0', 1000);
    char file_name_pdf[1000]; memset(file_name_pdf, '\0', 1000);
    if((file_base != NULL) && (strlen(file_base) > 0) ) {
      sprintf(file_name_result, "%s_%s_img.ppm", file_base, file_pref);
      sprintf(file_name_pdf, "%s_%s_img.pdf", file_base, file_pref);
    } else {
      sprintf(file_name_result, "%s_img.ppm", file_pref);
      sprintf(file_name_pdf, "%s_img.pdf", file_pref);
    }
    //	    const char *file_name_result_img = "tmp_img.ppm";
    fprintf(stderr, "(info:export::ppm)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
    ppm_image_t *picture = exportTo_image ( mat_cpy.matrix, mat_cpy.nrows, mat_cpy.ncols, mat_cpy.nameOf_rows, mat_cpy.nameOf_columns);
    ppm_image_write ( file_name_result, picture );
    ppm_image_finalize ( picture );
    if(true) { //! then we try cconvering the PPM-file to a PDF-file, ie, to simplify report-generaiton.
      //! Fix: handle case where '.' is found in the file-name (which for tools such as "pdflatex" caseus compilation-rpoblems).
      const uint cnt_chars = strlen(file_name_pdf);
      assert(cnt_chars > 0);
      const uint cnt_file_ending = 4; //! ie, strlen(".pdf") = 4; 
      assert(cnt_chars > cnt_file_ending);      
      for(uint i = 0; i < (cnt_chars - cnt_file_ending); i++) { //! where improtance of "(cnt_chars - cnt_file_ending)" is seen from the ened for avoid ".pdf" from becombing "-pdf"
	if(file_name_pdf[i] == '.') {
	  file_name_pdf[i] = '-';
	}
      }
      //! 
      allocOnStack__char__sprintf(2000, str_cmd, "convert %s %s", file_name_result, file_name_pdf); //! ie, creat eht direcotry (if directy does No0t relayd exists).
      MF__system(str_cmd); //! whcih is foudn in "aux_sysCalls.h"
    }
    /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL); */
    /* assert(is_ok); */
    //! De-allocate:
  }
  free__s_kt_matrix(&mat_cpy);
}



typedef struct s_kt_storage_resultOfClustering_data {
  /**
     @brief hold the cluster-IDs computed from a co-varaince matrix.
   **/
  s_kt_matrix_t mat_clusterIds;
  uint mat_prev_pos;
} s_kt_storage_resultOfClustering_data_t;
/**
   @brief a wrapper-fucniton for clustering.
 **/
typedef struct s_kt_storage_resultOfClustering {
  e_hpLysis_clusterAlg clusterAlg;
  uint nclusters;
  uint cnt_Calls_max;
  uint arg_npass;
  //!
  bool apply_for_many_algs;
  //!
  s_kt_storage_resultOfClustering_data_t data;
} s_kt_storage_resultOfClustering_t;
s_kt_storage_resultOfClustering_t initAndReturn_s_kt_storage_resultOfClustering_t(const uint nclusters) {
  s_kt_storage_resultOfClustering_t self;
  //! Note: in [below] we examplify different permtauaions wrt. HCA: algorithm-calls:
  self.clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //self.clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //self.clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //self.clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //self.clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //self.clusterAlg = e_hpLysis_clusterAlg_disjoint;
  self.nclusters = nclusters;
  //self.nclusters = 2;
  self.cnt_Calls_max = 1;
  self.arg_npass = 100;
  //self.arg_npass = 1000;
  //!
  //self.apply_for_many_algs = false;
  self.apply_for_many_algs = true;
  //!
  self.data.mat_prev_pos = 0;
  self.data.mat_clusterIds = setToEmptyAndReturn__s_kt_matrix_t();
  //!
  return self;
}

void free__s_kt_storage_resultOfClustering_t(s_kt_storage_resultOfClustering_t *self) {
  free__s_kt_matrix(&(self->data.mat_clusterIds));
}

//! The approach for correlation:
static void apply_s_kt_storage_resultOfClustering(const s_kt_storage_resultOfClustering_t *self, s_kt_matrix_t &obj_matrixInput, const char *result_file, const bool inputMatrix__isAnAdjecencyMatrix = true) { // , t_float *arrResult_clusterIds) {
  //! Open teh file :
  FILE *file_out = fopen(result_file, "wb");
  //FILE *file_out = stdout;
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}

  //!
  //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:
  for(uint cnt_Calls = 0; cnt_Calls < self->cnt_Calls_max; cnt_Calls++) {
    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
    //! Assumption: (Symmetric) adjacency matrix                                                                                                                            
    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
    obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.

    /* Expect 2 clear clusters */
     printf("## npass=%u, at %s:%d\n", self->arg_npass, __FILE__, __LINE__);
    const bool is_also_ok = cluster__hpLysis_api (
						  &obj_hp, self->clusterAlg, &obj_matrixInput,
						  /*nclusters=*/ self->nclusters, /*npass=*/ self->arg_npass
						  );
    assert(is_also_ok);

    //! 
    //! Export results to "file_out":
    assert(file_out);
    const s_kt_correlationMetric_t corrMetric_prior = obj_hp.config.corrMetric_prior;
    fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    /* bool is_ok_e = export__hpLysis_api(&obj_hp, /\*stringOf_file=*\/NULL, /\*file_out=*\/file_out, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV); */
    /* assert(is_ok_e); */
    //! Export the vertex-clsuterId-memberships:
    export__hpLysis_api(&obj_hp, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &obj_matrixInput);
    // assert(is_ok_e);

    // FIXME: why does all fo the veritces en up in the same clsuter
    
    //! 
    //! 
    const uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;
    if(cnt_vertex > 0) { //! Export: traverse teh generated result-matrix-object and write out the results:    
      //! Note: the [”elow] calls implictly 'test' the/our "cuttree(..)" proceudre (first described in the work of "clsuter.c"):
      const uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;      
      fprintf(file_out, "clusterMemberships=[");
      uint max_cnt = 0;
      uint i_cnt = obj_matrixInput.nrows;
      if(vertex_clusterId != NULL) {
	i_cnt = cnt_vertex;
      }
      for(uint i = 0; i < i_cnt; i++) {
	uint c = 0;
	if(vertex_clusterId != NULL) {
	  c = (uint)vertex_clusterId[i];
	  //! Note: idea is to store the cluster memberships. Thsi is useful both when comparing different clsutering algorithm-combinations. This is useful when distiuinguishing (and understanding) the effect of different approaches: the 'raw' cluster-memberships provides isnight into the entropy of different clsutering-algorithms; applying a correlation-emtric (to analyse the 'raw' data) provides a crude appraoch for capturing simlairties in different algorithms; the use of CCMs (to analyse the clustering meberships accross different input-data-matrices) gives the entropy alogn the coinccende-matrix (where a coinccende-matrix is udnerstood as ....). 
	  assert(self->data.mat_prev_pos < self->data.mat_clusterIds.nrows);
	  assert(i < self->data.mat_clusterIds.ncols);
	  self->data.mat_clusterIds.matrix[self->data.mat_prev_pos][i] = (t_float)c;  //! ie, then isnert the id into the simliarty-matrix 
	}
	if(c < 4294967296) {
	  if( (c < UINT_MAX) && (c < (uint)T_FLOAT_MAX) ) {
	    fprintf(file_out, "%u->%u, ", i, c);
	    max_cnt = macro_max(max_cnt, c);
	  } else {
	    fprintf(file_out, "%u->none, ", i);
	  }
	}
      }
      fprintf(file_out, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);
    }

    // FIXME: new export: to ".dot" format.
    
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&obj_hp);	    
  }


  //!
  //! Close the file and the amtrix:
  assert(file_out); 
  if(file_out != stdout) {fclose(file_out); file_out = NULL;}
}

static void kt_storage_resultOfClustering_correlate_then_exportTo_ppm(s_kt_matrix_t &mat_corrAll, const char *resultPrefix_local, s_kt_correlationMetric_t metric_local, const char *str_metric, s_kt_storage_resultOfClustering_t *obj_cluster) { //, s_kt_storage_resultOfClustering_t *t_obj_cluster) {
  //!
  s_kt_matrix_base_t mat_shallow         = initAndReturn__notAllocate__s_kt_matrix_base_t(mat_corrAll.nrows, mat_corrAll.ncols, mat_corrAll.matrix);
  s_kt_matrix_base_t mat_result        = initAndReturn__empty__s_kt_matrix_base_t();
  s_hp_distance__config_t local_config = init__s_hp_distance__config_t();
  const bool is_ok = apply__hp_distance(metric_local, &mat_shallow, &mat_shallow, &mat_result, local_config);
  // const bool is_ok = apply__rows_hp_distance(list_metrics[i], row_1, row_2, ncols, &scalar_result);
  assert(is_ok);
  printf("\t(correlate-and-export)\t ncols=%u, at %s:%d\n", mat_result.ncols, __FILE__, __LINE__);
  if(mat_result.ncols > 0) { //! then we assume data is allcoated:
    //! 'Steal' the data:
    mat_result.matrix__isAllocated = false; //! ie, to avodi a garbage-rotuein from wrongly clearing the data used in the below "mat_export" struct.
    s_kt_matrix_t mat_export = setToEmptyAndReturn__s_kt_matrix_t();
    mat_export.matrix = mat_result.matrix;
    mat_export.nrows = mat_result.nrows;
    mat_export.ncols = mat_result.ncols;
    //!	  
    { //! Set the row-headers and columns:
      assert(mat_export.ncols  == mat_export.nrows);
      assert(mat_corrAll.nrows == mat_export.nrows);	    
      for(uint i = 0; i < mat_export.nrows; i++) {
	const char *str = getAndReturn_string__s_kt_matrix(&mat_corrAll, i, /*addFor_column=*/false);
	if(str != NULL) {
	  set_stringConst__s_kt_matrix(&mat_export, /*index=*/i, str, /*addFor_column=*/false);
	  set_stringConst__s_kt_matrix(&mat_export, /*index=*/i, str, /*addFor_column=*/true);
	} else {
	  fprintf(stderr, "!!\t(str-not-set)\t Investiagte this case, at %s:%d\n", __FILE__, __LINE__);
	}
      }
    }
    //!
    { //! Export the result:
      allocOnStack__char__sprintf(2000, str_fileName, "%s--%s", resultPrefix_local, str_metric);
      kt_storage_resultOfClustering_exportTo_ppm(mat_export, /*file_base=*/"", str_fileName, /*isTo_alsoExportTo_tsv=*/true);
      { //! Export summary of core-props:
	allocOnStack__char__sprintf(2000, str_fileName, "%s--%s-corrDeviation.tsv", resultPrefix_local, str_metric);
	extractAndExport__deviations__singleCall__tsv__s_kt_matrix_t(&mat_export, str_fileName);
      }
    }
    //!
    //! 
    if(obj_cluster != NULL) { //! then a clustering-step is applied: 
      if(obj_cluster->apply_for_many_algs) { //! then iterate over all the clsutering-algorithms, using the base-params cofnuiguration:
	// FIXME: ... there were not any unique clusters when applying HCA::single to the radnomzied-data-sets ... due to ...??...
	// FIXME: ... when increasing the number of dimension (in the data), we observe a ... difference in clusters %< FIXME: when there are distinct clusters .... then inrease the "nrows" setting, for whcih we observe that .... 
	// FIXME: ... 	
	for(uint clust_id = 0; clust_id < e_hpLysis_clusterAlg_undef; clust_id++) {	  
	  assert(obj_cluster->data.mat_prev_pos < obj_cluster->data.mat_clusterIds.nrows); //! ie, as the itni-procedure otehrwise have a bug.
	  const e_hpLysis_clusterAlg_t enum_id = (e_hpLysis_clusterAlg_t)clust_id; 
	  //s_kt_storage_resultOfClustering_t obj_cluster_local = *obj_cluster;
	  obj_cluster->clusterAlg = enum_id;
	  //obj_cluster_local.clusterAlg = enum_id;
	  const char *str_clust = get_stringOf__short__e_hpLysis_clusterAlg_t(enum_id);
	  allocOnStack__char__sprintf(2000, str_fileName, "%s--%s--%s.tsv", resultPrefix_local, str_metric, str_clust);
	  const bool inputMatrix__isAnAdjecencyMatrix = true;
	  //! Note: 
	  apply_s_kt_storage_resultOfClustering(obj_cluster, mat_export, (char*)str_fileName, inputMatrix__isAnAdjecencyMatrix);
	  //apply_s_kt_storage_resultOfClustering(&obj_cluster_local, mat_export, (char*)str_fileName, inputMatrix__isAnAdjecencyMatrix);
	  //! ----------------
	  { //! Update hte result-cluster-id-matrix:
	    assert(obj_cluster->data.mat_clusterIds.nrows > 0); //! ie, as we expect thei to be itnated.
	    //! Seet the row-name:
	    allocOnStack__char__sprintf(2000, str, "%s-%s", str_metric, str_clust);
	    set_stringConst__s_kt_matrix(&(obj_cluster->data.mat_clusterIds), /*index=*/obj_cluster->data.mat_prev_pos, str, /*addFor_column=*/false);
	    //! Increment the counter:
	    obj_cluster->data.mat_prev_pos++;
	  } //! ----------------
	}
      } else { //! then apply for only the given cluster-alg:
	allocOnStack__char__sprintf(2000, str_fileName, "%s--%s--%s.tsv", resultPrefix_local, str_metric, get_stringOf__short__e_hpLysis_clusterAlg_t(obj_cluster->clusterAlg));
	const bool inputMatrix__isAnAdjecencyMatrix = true;
	apply_s_kt_storage_resultOfClustering(obj_cluster, mat_export, (char*)str_fileName, inputMatrix__isAnAdjecencyMatrix); //, /*arrResult_clusterIds=*/NULL);
      }
    }
    //! De-allocate:
    free__s_kt_matrix(&mat_export);      
  } //! else: then the matrix is empty.
}	

// s_hpLysis_api__config_t obj_hp_config = init__s_kt_api_config_t(/*isTo_transposeMatrix=*/false, /*isTo_applyDisjointSeperation=*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, /*fileHandler=*/NULL);


static void analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(s_kt_matrix_t &mat_corrAll, const char *resultPrefix_local, const bool isTo_transpose, s_kt_storage_resultOfClustering_t *obj_cluster, s_kt_storage_resultOfClustering_t *t_obj_cluster) {
  { //! Export summary of core-props:
    allocOnStack__char__sprintf(2000, str_fileName, "%s-featureDeviation.tsv", resultPrefix_local);
    // FIXME: belwo is useful for gaining singht into the variance of the predictions, such as the spread and density ... such as the relative frequency of the vertices being part of a cluster ... the use of STD, medidan, and aveage, are one of the compoens (used) in the desing of CCMs .... hence, it provides a ...??.. % FIXME: what arre the signitsh which we drive from these exprots, eg, of clsuter-ids?
    extractAndExport__deviations__singleCall__tsv__s_kt_matrix_t(&mat_corrAll, str_fileName);
  }
  const uint cnt_metrics_case_oneEachCategori = 4;
  const uint cnt_metrics_case_Minkowski = 4;
  //const uint metrics_total = (uint)e_kt_normType_undef; //! for case where Euclidan simliarty is intersected with diffnere tnromsaiton-strategies.
  const uint metrics_total = cnt_metrics_case_oneEachCategori;
  // const uint metrics_total = cnt_metrics_case_Minkowski; 
  uint metrics_total_curr = 0;
  const uint t_metrics_total = 2; uint t_metrics_total_curr = 0;
  if(obj_cluster) { //! Initate: the result-matrix holding the clsuter-IDs.
    //! Note: the itnaiton-stpe is moved to this code-chunk, as hope is that it becomes esier maitnianing the code ... as the numer of ros (in the end-cluster-matrix) is detmeriend by below cofnigruaiton.
    if(obj_cluster->data.mat_clusterIds.matrix) {
      assert(obj_cluster->data.mat_clusterIds.matrix != t_obj_cluster->data.mat_clusterIds.matrix); //! ie, that hte objects are different.
      assert(obj_cluster->data.mat_clusterIds.matrix != mat_corrAll.matrix); //! ie, to dectect the case where the same matrix is given as both input-arg, and as a result-arg.
    }
    //assert(obj_cluster->data.mat_clusterIds.matrix != t_obj_cluster->data.mat_clusterIds.matrix); //! ie, that hte objects are different.
    assert(obj_cluster->data.mat_clusterIds.nrows == 0);
    assert(obj_cluster->data.mat_clusterIds.ncols == 0);
    assert(t_obj_cluster->data.mat_clusterIds.nrows == 0);
    assert(t_obj_cluster->data.mat_clusterIds.ncols == 0);    
    //!------
    if(obj_cluster->apply_for_many_algs) {
      obj_cluster->data.mat_clusterIds = initAndReturn__s_kt_matrix(/*nrows=*/metrics_total *  e_hpLysis_clusterAlg_undef, /*ncols=*/mat_corrAll.nrows);
    } else {
      obj_cluster->data.mat_clusterIds = initAndReturn__s_kt_matrix(/*nrows=*/metrics_total *  1, /*ncols=*/mat_corrAll.nrows);      
    }
    //! Set the col-header:
    assert(mat_corrAll.nrows == obj_cluster->data.mat_clusterIds.ncols);
    for(uint i = 0; i < obj_cluster->data.mat_clusterIds.ncols; i++) {
      const char *str = getAndReturn_string__s_kt_matrix(&mat_corrAll, i, /*addFor_column=*/false);
      //allocOnStack__char__sprintf(2000, str, "ind:%u", k);
      if(str != NULL) {
	set_stringConst__s_kt_matrix(&(obj_cluster->data.mat_clusterIds), /*index=*/i, str, /*addFor_column=*/true);
      } else {
	fprintf(stderr, "!!\t(str-not-set)\t Investiagte this case, at %s:%d\n", __FILE__, __LINE__);
      }
    }
    //!------
    assert(t_obj_cluster);
    if(t_obj_cluster->apply_for_many_algs) {
      t_obj_cluster->data.mat_clusterIds = initAndReturn__s_kt_matrix(/*nrows=*/t_metrics_total *  e_hpLysis_clusterAlg_undef, /*ncols=*/mat_corrAll.ncols);
      //! Set the row-col-header:
    } else {
      t_obj_cluster->data.mat_clusterIds = initAndReturn__s_kt_matrix(/*nrows=*/t_metrics_total *  1, /*ncols=*/mat_corrAll.ncols);
      //! Set the row-col-header:
    }
    //! Set the col-header:
    assert(mat_corrAll.ncols == t_obj_cluster->data.mat_clusterIds.ncols);
    assert(t_obj_cluster->data.mat_clusterIds.matrix != obj_cluster->data.mat_clusterIds.matrix);
    for(uint i = 0; i < obj_cluster->data.mat_clusterIds.ncols; i++) {
      const char *str = getAndReturn_string__s_kt_matrix(&mat_corrAll, i, /*addFor_column=*/true);
      //allocOnStack__char__sprintf(2000, str, "ind:%u", k);
      set_stringConst__s_kt_matrix(&(t_obj_cluster->data.mat_clusterIds), /*index=*/i, str, /*addFor_column=*/true);
      if(str != NULL) {
      } else {
	fprintf(stderr, "!!\t(str-not-set)\t Investiagte this case, at %s:%d\n", __FILE__, __LINE__);
      }
    }
    //!------
  }
  { //! Export: raw data, both trannsposed and not-transposed.
    {
      // FIXME: ...
      //! Export to ppm:
      allocOnStack__char__sprintf(2000, str, "%s-features", resultPrefix_local);
      kt_storage_resultOfClustering_exportTo_ppm(mat_corrAll, /*file-base=*/"", str, /*isTo_alsoExportTo_tsv=*/true);
    }
    // *************************************************************
    // ********************* choise the sim-metric to compute for ****************************************
    // *************************************************************    

    // FIXME: write a permtaution of below ... apply different nroamsioant-rpestep before Euclidan distance ... 
    // FIXME: write a permtaution of below ... differnet prmatuison of Kendall's Tau ... where chosei of itnepreation is not provided in reseahrc-artilces ... issues in repdocuaiblity ... What could be aruged , is that the difference is inside the interpration-aspace (or: Pareto/Error Boundary) of algorithms .... Itnerstignly, the difference in rpedictiosn is at ... , which aruges that ... 
    // FIXME: write a permtaution of below ... differnet prmatuison of Pearosn ... 
    if(false) { //! then we explore for different rnaozation-metrics:
      assert(metrics_total == e_kt_normType_undef);
      const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none);
      for(uint i = 0; i < e_kt_normType_undef; i++) {
	// FIXME: udpate paper with desription of the nrosaiton-metircs ... as sene in const char *get_description__humanReadable__e_kt_normType_t(const e_kt_normType_t enum_id) in "kt_aux_matrix_norm.c"
	//! Apply a nroamsaiton-pre-step.
	s_kt_matrix_t mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_corrAll, (e_kt_normType_t)i);
	//! For: Euclid:
	allocOnStack__char__sprintf(2000, str_metric, "%s-%s", "Euclid", get_str__humanReadable__e_kt_normType_t((e_kt_normType_t)i)); 
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_norm, resultPrefix_local, metric_local, str_metric, obj_cluster); //, t_obj_cluster);
	//! De-allcoate:
	assert(mat_norm.matrix != mat_corrAll.matrix); //! ie, as we asusemt he poitnes are unique.
	free__s_kt_matrix(&mat_norm);
      }
    } else if(false) { //! idea: to the best of our knowledge, Minkoswki-based metrics (eg, Cityublock, and Eucldian distance) are used to derive conclsuions, eg, of how clsuteirng-algorithms perfoomr. The coise of simlairty-metric has a strong influence of the outcome of how clusteirng algortihms intrpet a dataset (Fig. \ref{}). Of inerest is therefore to isoalte the default set of simliarty-metircs, and identify their ability to sperate between different cases (eg, the handlign of high-diemsiuonal data).
      const uint cnt_metrics = 4;
      assert(cnt_metrics == cnt_metrics_case_Minkowski); //! ie, as we expect cosnsitency in intial setting.
      assert(metrics_total == cnt_metrics);
      const e_kt_correlationFunction_t arr_metrics[cnt_metrics] = {
  e_kt_correlationFunction_groupOf_minkowski_euclid,
  e_kt_correlationFunction_groupOf_minkowski_cityblock,
  e_kt_correlationFunction_groupOf_minkowski_minkowski,
  //  e_kt_correlationFunction_groupOf_minkowski_chebychev,
  e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max
  /* e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski, */
  /* e_kt_correlationFunction_groupOf_intersection_WaveHedges, */
  /*   e_kt_correlationFunction_groupOf_innerProduct_innerProduct, */
  /*   e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya, */
  /*   e_kt_correlationFunction_groupOf_squared_Euclid, */
  /* e_kt_correlationFunction_groupOf_shannon_KullbackLeibler, */
  /*   e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges, */
      };
      const char *arr_str[cnt_metrics] = {"Euclid", "Cityblock", "Minkowski", "Chebychev"}; //, "Chebychev-minInsteadOf-max", 
					  // "Kulczynski-absolute-difference", "WaveHedges", "InnerProduct", "Bhattacharyya", "Euclid (squared)", "Pearson (squared)", "Shannon-KullbackLeibler", "VicisWaveHedges-downwardMutability"};
      //!
      //!
      for(uint i = 0; i < cnt_metrics; i++) {
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(arr_metrics[i], e_kt_categoryOf_correaltionPreStep_none);
	const char *str_metric = arr_str[i];
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric, obj_cluster); //, t_obj_cluster);
      }
    } else { //! Compute simliarty, and then export.
      assert(cnt_metrics_case_oneEachCategori == 4); //! ie, the number of metrics comptued for below:
      assert(metrics_total == cnt_metrics_case_oneEachCategori);
      { //! For: Euclid:
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none);
	const char *str_metric = "Euclid";
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric, obj_cluster); //, t_obj_cluster);
      }
      assert(metrics_total_curr++ < metrics_total); //! ie, for validaitng the init-settings.
      { //! For: Pearson:
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic, e_kt_categoryOf_correaltionPreStep_none);
	// FIXME: paper: ... discuss the nubmer of different metrics for simliarty ... eg, different deisnions for Pearos ... sublte differences infleucnes the overall outcome ... % FIXME: exemplify with a cocnrete esult-figure, such as ...??..
	const char *str_metric = "PearsonProductMoment";
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric, obj_cluster); //, t_obj_cluster);
      }
      assert(metrics_total_curr++ < metrics_total); //! ie, for validaitng the init-settings.
      { //! For: Kendall:
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, e_kt_categoryOf_correaltionPreStep_rank);
	const char *str_metric = "Kendall";
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric, obj_cluster); //, t_obj_cluster);
      }
      assert(metrics_total_curr++ < metrics_total); //! ie, for validaitng the init-settings.
      { //! For: MINE:
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none);
	const char *str_metric = "MINE";
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_corrAll, resultPrefix_local, metric_local, str_metric, obj_cluster); //, t_obj_cluster);
      }
      assert(metrics_total_curr++ < metrics_total); //! ie, for validaitng the init-settings.
    }
    if(isTo_transpose) { //! Compute simliarty, and then export, for transposed matrix:
      //! Note[observation::result]:
      //! Note[observation::result]:	
      //! Note[observation]:
      //! Note[observation]: 	
      //!
      //! Tranpose:
      s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_corrAll);
      //!
      //!	
      { //! For: Kendall:
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, e_kt_categoryOf_correaltionPreStep_rank);
	const char *str_metric = "transp-Kendall";
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_transp, resultPrefix_local, metric_local, str_metric, t_obj_cluster);
      }
      assert(t_metrics_total_curr++ < t_metrics_total); //! ie, for validaitng the init-settings.
      { //! For: MINE:
	const s_kt_correlationMetric_t metric_local = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none);
	const char *str_metric = "transp-MINE";
	kt_storage_resultOfClustering_correlate_then_exportTo_ppm(mat_transp, resultPrefix_local, metric_local, str_metric, t_obj_cluster);
      }
      assert(t_metrics_total_curr++ < t_metrics_total); //! ie, for validaitng the init-settings.
      //!
      //! de-allocate:
      free__s_kt_matrix(&mat_transp);
    }
    // ----------
  }

  // FIXME: cosntruct entopry-hsitogram ... used to sumamrizee the connribution of ...??...
  
}


#endif //! kt_storage_resultOfClustering
