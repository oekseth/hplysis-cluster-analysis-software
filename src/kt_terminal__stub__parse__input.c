{
  { //! Idnetify the number of tiems we are to write out the result:
    if(self->isToApply_correlation_beforeFilter_includeInResult) {  
      cnt_dataObjects_toGenerate++;  
      if(self->whenToMerge_differentMatrices != e_kt_terminal_whenToMerge_beforeFilter) {cnt_dataObjects_toGenerate++;}
    }
       
    if(self->isToApply_correlation_afterFilter_includeInResult) {   cnt_dataObjects_toGenerate++;  
      if(self->whenToMerge_differentMatrices == e_kt_terminal_whenToMerge_inClustering) {cnt_dataObjects_toGenerate++;}
    }
    cnt_dataObjects_toGenerate += get_cntDataObjects_toGenerate__s_kt_clusterResult_config_t(&(self->clusterConfig));
  }
  if(cnt_dataObjects_toGenerate == 0) {
    if(self->object_is_usedInternally_resultdoesThereforeNotNeedBeExported == false) {
      fprintf(stderr, "!!\t You did not request any data-objects to be generated, ie, please validate that your call is correct (as we at this exeuction-point has the interpretaiton that you forgot specifiying a result-paramter): for quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#ifndef __is_called_fromTestFunction      
      return 0;
#endif
    }
  }
  

  //! IDentify the type of format:
  if(self->result_format && strlen(self->result_format) && (0 == strncmp("relation", self->result_format, strlen("relation"))) ) {
    isTo_useMatrixFormat = false;
  }

  // printf("----------------------, at %s:%d\n", __FILE__, __LINE__);
  
  

  




  //! Allocatye the object holding the input-data:
  // s_dataStruct_matrix_dense_t self; init_s_dataStruct_matrix_dense_t_setToEmpty(&self); //! ie, set the object-variables to empty.
  //! Load a data-set into the selfh, either using input-data or a value-range-distribution defined by our "stringOf_sampleData_type" param.
  //printf("at %s:%d\n", __FILE__, __LINE__);
  readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(&(self->inputMatrix_1), self->stringOf_input_file_1, 
							   /*cnt_rows=*/self->nrows, /*cnt_columns=*/self->ncols,
							   self->isTo_readData_fromStream, self->stringOf_sampleData_type, 
							   self->fractionOf_toAppendWith_sampleData_type_rows, self->fractionOf_toAppendWith_sampleData_type_columns, 
							   self->stringOf_sampleData_type_realLife, self->isTo_transposeMatrix);
  //printf("at %s:%d\n", __FILE__, __LINE__);
  //! Fro cosntient logics update the object wrt. the min-max-proerp
  self->nrows = self->inputMatrix_1.nrows;
  self->ncols = self->inputMatrix_1.ncols;
  //! ---
  if( (self->stringOf_input_file_1_mask != NULL) && strlen(self->stringOf_input_file_1_mask)) {
    //! Then we udpate the object with the enw-parsed data-structure-elements:
    readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(&(self->inputMatrix_1), self->stringOf_input_file_1_mask, 
							     /*cnt_rows=*/self->nrows, /*cnt_columns=*/self->ncols,
							     /*self->isTo_readData_fromStream=*/NULL, NULL, 
							     /*self->fractionOf_toAppendWith_sampleData_type_rows=*/0, /*self->fractionOf_toAppendWith_sampleData_type_columns=*/0, 
							     /*self->stringOf_sampleData_type_realLife=*/NULL, self->isTo_transposeMatrix);    
  }
  if( (self->stringOf_input_file_1_weightList != NULL) && strlen(self->stringOf_input_file_1_weightList)) {
    readFrom_1dWeightTable__s_kt_matrix_t(&(self->inputMatrix_1), self->stringOf_input_file_1_weightList); //! ioe, then read the weight-lsit for the 'columsn'.
  } else { //! Then we are itnerested in de-allcoating the weight-table (if allcoated), ie, to avodi ony 'comptuational overehad' wrt. data-anlayssis:
    free_1d_list_float(&(self->inputMatrix_1.weight)); self->inputMatrix_1.weight = NULL;
  }

  /* build_dataset_fromInputOrSample(&(self->inputMatrix_1.data_obj), self->stringOf_input_file_1,  */
  /* 				  /\*cnt_rows=*\/self->nrows, /\*cnt_columns=*\/self->ncols, */
  /* 				  self->isTo_readData_fromStream, self->stringOf_sampleData_type,  */
  /* 				  self->fractionOf_toAppendWith_sampleData_type_rows, self->fractionOf_toAppendWith_sampleData_type_columns,  */
  /* 				  self->stringOf_sampleData_type_realLife, self->isTo_transposeMatrix); */
  
  if(self->stringOf_input_file_2 || self->stringOf_sampleData_type_secondMatrix) {
    const char *stringOf_sampleDistribution = self->stringOf_sampleData_type_secondMatrix;
    if(!stringOf_sampleDistribution || !strlen(stringOf_sampleDistribution)) {
      stringOf_sampleDistribution = self->stringOf_sampleData_type; //! ie, then use the same dat-aditstrubitons for 'the first matrix' and 'the second matrix'.
    }
    readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(&(self->inputMatrix_2), self->stringOf_input_file_2, 
							     /*cnt_rows=*/self->nrows, /*cnt_columns=*/self->ncols,
							     false, stringOf_sampleDistribution, 
							     self->fractionOf_toAppendWith_sampleData_type_rows, self->fractionOf_toAppendWith_sampleData_type_columns, 
							     self->stringOf_sampleData_type_realLife, self->isTo_transposeMatrix);

    //    printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);
    if( (self->stringOf_input_file_2_mask != NULL) && strlen(self->stringOf_input_file_2_mask)) {
      readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(&(self->inputMatrix_2), self->stringOf_input_file_2_mask, 
							       /*cnt_rows=*/self->nrows, /*cnt_columns=*/self->ncols,
							       false, /*self->stringOf_sampleData_type=*/NULL, 
							       /*self->fractionOf_toAppendWith_sampleData_type_rows=*/0, /*self->fractionOf_toAppendWith_sampleData_type_columns=*/0, 
							       /*self->stringOf_sampleData_type_realLife=*/NULL, self->isTo_transposeMatrix);
    }
    if( (self->stringOf_input_file_2_weightList != NULL) && strlen(self->stringOf_input_file_2_weightList)) {
      readFrom_1dWeightTable__s_kt_matrix_t(&(self->inputMatrix_2), self->stringOf_input_file_2_weightList); //! ioe, then read the weight-lsit for the 'columsn'.
    } else { //! Then we are itnerested in de-allcoating the weight-table (if allcoated), ie, to avodi ony 'comptuational overehad' wrt. data-anlayssis:
      free_1d_list_float(&(self->inputMatrix_2.weight)); self->inputMatrix_2.weight = NULL;
    }
  }
  
  

  //! What we expect:
  assert(self->nrows > 0);  assert(self->ncols > 0);  


  if(self->isTo_inResult_isTo_exportInputData == true) { //! then w 'at the tal' exproit the input-matrix:   
    //! Then we export the data-sets to a singular file, ie, for each of the matrices 'which has data' (oekseth, 06. des. 2016).
    const bool is_ok = exportDataSets__toACombinedFile__kt_terminal(self, "inputData", isTo_useMatrixFormat);
    assert(is_ok);
    ///*isTo_writeOut_stringHeaders=*/(cnt_matrices_exported == 0), /*isLAst=*/((cnt_dataObjects_written + 1) == cnt_dataObjects_toGenerate));
  }

  //printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);
}
