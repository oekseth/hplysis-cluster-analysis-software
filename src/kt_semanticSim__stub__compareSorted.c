    //!
//const uint cnt_iterateBoth = (row_size <= row_size_2) ? row_size : row_size_2; // = row_size + row_size_2;
const uint cnt_iterateBoth = macro_max(row_size, row_size_2);
    //!
    //!
    //!
    t_type sumOf_1 = 0;       t_type sumOf_2 = 0; 
    uint cntLocal_intersect = 0; uint cntLocal_1 = 0; uint cntLocal_2 = 0;
    uint cntLocal_onlyIn_1_added = 0; uint cntLocal_onlyIn_2_added = 0;

    //!
    //! Iterate: 
    uint col_id = 0;
bool use_1 = true; bool use_2 = true; bool both_found = false;
    for(; col_id < cnt_iterateBoth; col_id++) {
      //! Gvient he 'sprasenes's of the input we expect both 'sets' to be of itnerest:
      //assert_possibleOverhead(isOf_interest(row_1[cntLocal_1]));
      //assert_possibleOverhead(isOf_interest(row_2[cntLocal_2]));
      //!
      //! Apply logics:
      if(use_1 && use_2 && (row_1[cntLocal_1] == row_2[cntLocal_2]) ) {
    	//const t_type score_1 = rowOf_scores_1[cntLocal_1];  const t_type score_2 = rowOf_scores_2[cntLocal_2];
	//!
	//! Apply logics:
	//ret_result += 1;
	__MiF__selectFunction(cntLocal_1, cntLocal_2);
    	//! -------------------------------------------------------------------------------------
    	//!
    	//! Compute the distance:
    	cntLocal_1++;    cntLocal_2++;
	use_1 = (cntLocal_1 < row_size); //! where htis synes is used to aovid branchign.
	use_2 = (cntLocal_2 < row_size_2); //! where htis synes is used to aovid branchign.
	/* if(cntLocal_1 >= row_size) {use_1 = false;} */
	/* if(cntLocal_2 >= row_size) {use_2 = false;} */
    	cntLocal_intersect++;
	both_found = true;
#if(__MiF__stopIterationWhen_verticesNoLongerMatches__breakAtFirst == 1)
	break;
#endif
      } else if(use_1 && (!use_2 || (row_1[cntLocal_1] < row_2[cntLocal_2])) ) {
#if(__MiC__onlyInterestingcCasesAreOfInterest == 0)
    	sumOf_1 += rowOf_scores_1[cntLocal_1];  // TODO[perofmrance]: cosndier to 'oinly activate this' for a specific input-macor-apram (oekseth, 06. mar. 2017).
#endif
    	cntLocal_onlyIn_1_added++;
    	cntLocal_1++;
	use_1 = (cntLocal_1 < row_size); //! where htis synes is used to aovid branchign.
#if(__MiF__stopIterationWhen_verticesNoLongerMatches == 1)
	if(both_found) {col_id = cnt_iterateBoth;} //! ie, then sotp the evalaution.
#endif
	//if(cntLocal_1 >= row_size) {use_1 = false;}
      } else if(use_2) { //if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
#if(__MiC__onlyInterestingcCasesAreOfInterest == 0)
    	sumOf_2 += rowOf_scores_2[cntLocal_2];  // TODO[perofmrance]: cosndier to 'oinly activate this' for a specific input-macor-apram (oekseth, 06. mar. 2017).
#endif
    	cntLocal_onlyIn_2_added++;
    	cntLocal_2++;
	use_2 = (cntLocal_2 < row_size_2); //! where htis synes is used to aovid branchign.
	//if(cntLocal_2 >= row_size) {use_2 = false;}
    	//arrOf_tmp_onlyIn_row2[cntLocal_onlyIn_2_added++] = row_2[cntLocal_2++];
#if(__MiF__stopIterationWhen_verticesNoLongerMatches == 1)
	if(both_found) {col_id = cnt_iterateBoth;} //! ie, then sotp the evalaution.
#endif
      } //else { //! then the elemnts are equal:
    	//}
#if(__MiC__onlyInterestingcCasesAreOfInterest == 1)
      if(!use_1 || !use_2) {col_id = cnt_iterateBoth;} //! ie, then stop the evlauaiton-aprpapcoh.
#endif
    }
#if(1 == 2)
    // FIXME: fidn/idneityf an applciaoitn wr.t [”elow].
    if(col_id < row_size) {
      assert(col_id >= row_size_2); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_2
      for(uint col_id = cntLocal_1; col_id < row_size; col_id++) {
	sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
      }
    } else if(col_id < row_size_2) {
      assert(col_id >= row_size); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_1
      for(uint col_id = cntLocal_2; col_id < row_size_2; col_id++) {
	sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
      }
    } 
#endif    

#undef __MiF__selectFunction
#undef __MiF__stopIterationWhen_verticesNoLongerMatches
#undef __MiF__stopIterationWhen_verticesNoLongerMatches__breakAtFirst
