#ifndef e_kt_clusterComparison_h
#define e_kt_clusterComparison_h
/**
   @file e_kt_clusterFunction
   @brief provide a formal defintion of the clsuter-comparions which we support (oekseth, 06. otk. 2016)
   @author Ole KRistina Ekseth (oekseth)
   @remarks used to idneity the type of supported distance-metric when comparing two distance-matrices (oekseth, 06. nov. 2016).   
 **/
typedef enum e_kt_clusterComparison {
  e_kt_clusterComparison_mean_artimethic, //! the distance between the arithmetic means of the two clusters
  e_kt_clusterComparison_median_artimethic, // the distance between the medians of the two clusters
  e_kt_clusterComparison_extreme_min, //! the smallest pairwise distance between members of the two clusters
  e_kt_clusterComparison_extreme_max, //! the largest pairwise distance between members of the two clusters
  e_kt_clusterComparison_avg, //! average of the pairwise distances between members of the clusters
  e_kt_clusterComparison_total, //! the total of the pairwise distances between members of the clusters, ie, the result beofre 'avg' is appleid/adjusted (oekseth, 06. jun. 2017)
  e_kt_clusterComparison_undef
} e_kt_clusterComparison_t;

//! Map a "cluster.c" char-meothd-id to Knitting-Tools set of enum-ids (oekseth, 06. otk. 2016).
static const e_kt_clusterComparison_t get_enumId_fromChar__e_kt_clusterComparison_t(const char method) {
  switch (method)
    { case 'a': {return e_kt_clusterComparison_mean_artimethic;}
    case 'm': {return e_kt_clusterComparison_median_artimethic;}
    case 's': {return e_kt_clusterComparison_extreme_min;}
    case 'x': {return e_kt_clusterComparison_extreme_max;}
    case 'v': {return e_kt_clusterComparison_avg;}
    case 't': {return e_kt_clusterComparison_total;}
    }
  //! ---------------------------
  //! At this exeuction-point we did not ahve any matches:
  fprintf(stderr, "!!\t None of the enums matched your char='%c', ie, pelase validate correctness of your call; will now return a default alterantive: for questions please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", method, __FUNCTION__, __FILE__, __LINE__);
  return e_kt_clusterComparison_avg;  
}

//! @retunr the char-value assicated to the enum (oesketh, 06. okt. 2016)
//! @remarks the fucntion is the 'inverse' of the "get_enumId_fromChar__e_kt_clusterComparison_t(..)" function
static char get_char_fromEnum(const e_kt_clusterComparison_t enum_id) {
  if(enum_id == e_kt_clusterComparison_mean_artimethic) {return 'a';}
  else if(enum_id == e_kt_clusterComparison_median_artimethic) {return 'm';}
  else if(enum_id == e_kt_clusterComparison_extreme_min) {return 's';}
  else if(enum_id == e_kt_clusterComparison_extreme_max) {return 'x';}
  else if(enum_id == e_kt_clusterComparison_avg) {return 'v';}
  else if(enum_id == e_kt_clusterComparison_total) {return 't';}
  /* else if(enum_id == ) {return ;} */
  /* else if(enum_id == ) {return ;}
   */

  fprintf(stderr, "!!\t Investige thsi case, ie, whye enum_id=%u is not handled: as a first step validate that your have not used the 'undef-option'=%u in your argumetn. For questions please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", enum_id, e_kt_clusterComparison_undef, __FUNCTION__, __FILE__, __LINE__);
  return 's'; //! i,e one of the options.

}

//! @return a string-representiaotn of the e_kt_clusterComparison_t enum_id (oekseth, 06. des. 2016).
static const char *__getStringOf__e_kt_clusterComparison_t(const e_kt_clusterComparison_t enum_id) {
  if(enum_id == e_kt_clusterComparison_mean_artimethic) {return "mean-aritmethic";}
  else if(enum_id == e_kt_clusterComparison_median_artimethic) {return "median-aritmethic";}
  else if(enum_id == e_kt_clusterComparison_extreme_min) {return "extreme-min";}
  else if(enum_id == e_kt_clusterComparison_extreme_max) {return "extreme-max";}
  else if(enum_id == e_kt_clusterComparison_avg) {return "AVG";}
  else if(enum_id == e_kt_clusterComparison_total) {return "SUM";}
  else {return "";}
}

//! @return a string-representiaotn of the e_kt_clusterComparison_t enum_id (oekseth, 06. des. 2016).
static const char *getStringOf__e_kt_clusterComparison_t(const e_kt_clusterComparison_t enum_id) {
  const char *str = __getStringOf__e_kt_clusterComparison_t(enum_id);
  if(str && strlen(str)) {return str;}
  /* else if(enum_id == ) {return ;} */
  /* else if(enum_id == ) {return ;}
   */

  fprintf(stderr, "!!\t Investige thsi case, ie, whye enum_id=%u is not handled: as a first step validate that your have not used the 'undef-option'=%u in your argumetn. For questions please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", enum_id, e_kt_clusterComparison_undef, __FUNCTION__, __FILE__, __LINE__);
  return "<undefined>"; //! i,e one of the options.
}

//! @return a correspoding enum (oesketh, 06. apr. 2018).
static const e_kt_clusterComparison_t getEnum__e_kt_clusterComparison_t(const char *cmp) {
  assert(cmp); assert(strlen(cmp));
  for(uint i = 0; i < e_kt_clusterComparison_undef; i++) {
    const e_kt_clusterComparison_t enum_id = (e_kt_clusterComparison_t)i;
    const char *str = __getStringOf__e_kt_clusterComparison_t(enum_id);
    if(cmp && strlen(cmp)) {
      if(strlen(cmp) == strlen(str)) {
	if(0 == strcasecmp(cmp, str)) {return enum_id;}
      }
    }
  }
  return e_kt_clusterComparison_undef;
}

/**
   @enum e_kt_clusterComparison_postScore_eval
   @brief defines post procesisng steps after comptautsion (ie, if any) (oekseth, 06. par. 2018).
 **/
typedef enum e_kt_clusterComparison_postScore_eval {
  e_kt_clusterComparison_postScore_eval_avg, //! eg, for the "Mean Average Error (MAE)" metric.
  e_kt_clusterComparison_postScore_eval_avg_sqrt, //! eg, for the "Root mean squared error (RMSE)" metric
  e_kt_clusterComparison_postScore_eval_undef
} e_kt_clusterComparison_postScore_eval_t;

//! @return a string-rperstiaont of the "e_kt_clusterComparison_postScore_eval_t" enum (oekseth, 06. apr. 2018).
static const char *getString__e_kt_clusterComparison_postScore_eval_t(const e_kt_clusterComparison_postScore_eval_t enum_id) {
  if(enum_id == e_kt_clusterComparison_postScore_eval_avg) {return "avg";}
  else if(enum_id == e_kt_clusterComparison_postScore_eval_avg_sqrt) {return "avg-sqrt";}
  else {return "";}
}

//! @return the correspoidning enum
static e_kt_clusterComparison_postScore_eval_t getEnum_e_kt_clusterComparison_postScore_eval_t(const char *str) {
  for(uint i = 0; i < e_kt_clusterComparison_postScore_eval_undef; i++) {
    const e_kt_clusterComparison_postScore_eval_t enum_id = (e_kt_clusterComparison_postScore_eval_t)i;
    assert(str); assert(strlen(str));
    const char *cmp = getString__e_kt_clusterComparison_postScore_eval_t(enum_id);
    if(cmp && strlen(cmp)) {
      if(strlen(cmp) == strlen(str)) {
	if(0 == strcasecmp(cmp, str)) {return enum_id;}
      }
    }
  }
  return e_kt_clusterComparison_postScore_eval_undef;
}

#endif //! EOF
