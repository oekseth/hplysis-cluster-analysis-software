#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
/**
   @brief demosntraes the max-heap-proerpty (oesketh, 06. jul. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
**/
int main() 
#else
  void tut_list_4_heap_keyValue()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //!
  //! Load an arbitrary data-set:
  const uint nrows = 1000;
  s_kt_list_1d_kvPair_t obj_list = init__s_kt_list_1d_kvPair_t(nrows);
  for(uint i = 0; i < nrows; i++) { //! then use an increasing sort-proeprty, ie, where the 'ivners'e is epxected to be the result of the max-ehap-proeprty.
    obj_list.list[i] = MF__initVal__s_ktType_kvPair(i, (t_float)i);
  }
  //!
  //! Construct the heap:
  buildHeap_max__s_kt_list_1d_kvPair_t(&obj_list);
  //!
  //! Validate correnctes sof the max-heap-proeprty:
  assert(obj_list.current_pos == nrows);
  for(uint i = 1; i < nrows; i++) { //! then use an increasing sort-proeprty, ie, where the 'ivners'e is epxected to be the result of the max-ehap-proeprty.
    //printf("compare: '%f' > '%f', at %s:%d\n", obj_list.list[i-1], obj_list.list[i], __FILE__, __LINE__);
    assert(obj_list.list[0].tail > obj_list.list[i].tail); //! ie, the max-heap-proeprty
  }
  if(true) { //! then a speerate sort-call:
    sort__s_kt_list_1d_kvPair_t(&obj_list);    
  }
  //!
  //! De-allcote:
  free__s_kt_list_1d_kvPair_t(&obj_list);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
