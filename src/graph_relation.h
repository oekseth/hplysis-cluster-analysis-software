#ifndef graph_relation_h
#define graph_relation_h
/**
   @file
   @brief Store the parent-child relationship in a stack of rleationships..
   @ingroup semantic
   @author Ole Kristian Ekseth (oekseth)
**/

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "type_uint_n.h"
//#include "n_logicalType.h"
//#include "reachable_item.h"
//#include "common_string_parser.h"

/**
   @class graph_relation
   @brief Store the parent-child relationship in a stack of rleationships..
   @ingroup graph
   @author Ole Kristian Ekseth (oekseth)
**/
class graph_relation : public type_uint_n<2> {
 public:
  //! @return the tail-id 
  const uint get_key_this() const {
    return get_key(0);
  }
  //! @return the inferred relation-type 
  const uint get_key_parent() const {
    return get_key(1);
  }

  /**
     @brief Compare two objects.
     @param <obj> the object to compare with.
     @return the enum describing the result of the comparison.
  **/
  const compare is_bigger_than(const class graph_relation obj) const {
    return is_bigger_than(&obj);
  }

  /**
     @brief Compare two objects.
     @param <obj> the object to compare with.
     @return the enum describing the result of the comparison.
     @remarks we use a reference to allow the usage of inheritance, to avoid inherited objects with a larger size (in Bytes) to produce erronous calls, ie, due to implict calls to this function due to implict inheritance.
  **/
  const compare is_bigger_than(const class graph_relation *obj) const {
    assert(obj); // as we expect it to be set.
    return type_uint_n<2>::is_bigger_than(obj);
  }

  //! The constructor
  graph_relation(const uint key_this = UINT_MAX, const uint key_parent = UINT_MAX) {
    {
      set_key(0, key_this);    assert(key_this == get_key_this()); // ie, first set the value, and then validate that the 'getter' is set as we expects.
      set_key(1, key_parent);  assert(key_parent == get_key_parent()); // ie, first set the value, and then validate that the 'getter' is set as we expects.
    }
  }
  //! The main test function for this class  
  static void assert_class(const bool print_info);
};

//typedef reachable_item<graph_relation > graph_relation_with_head;

#endif
