#include "measure_kt_para_3dSchedule.h"
  /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "kt_para_3dSchedule.h"

static void test_correctness() {
  /* { //! A simple test were we 'see' if teh internal "assert(false);" care called: if the program does Not abort then we asusem the test was succcessful (oekseth, 06. otk. 201&) */
  /*   const uint nrows = 95; const uint ncols = nrows*0.5; const uint SM = 60; const uint cnt_cores = 6; */
  /*   const e_kt_para_3dSchedule_typeOf_scheduleStrategy_t schedule_type = e_kt_para_3dSchedule_typeOf_scheduleStrategy_simpleDivision_unSymmstricOverhead; */
  /*   s_kt_para_3dSchedule self; */
  /*   init_s_kt_para_3dSchedule(&self, cnt_cores, nrows, ncols, SM, schedule_type); */
  /*   //! De-allocate: */
  /*   free__s_kt_para_3dSchedule(&self); */
  /* } */
  { //! A simple test were we 'see' if teh internal "assert(false);" care called: if the program does Not abort then we asusem the test was succcessful (oekseth, 06. otk. 201&)
    const uint nrows = 95; const uint ncols = nrows*0.5; const uint SM = 12; const uint cnt_cores = 6;
    const e_kt_para_3dSchedule_typeOf_scheduleStrategy_t schedule_type = e_kt_para_3dSchedule_typeOf_scheduleStrategy_simpleDivision_SymmstricOverhead;
    s_kt_para_3dSchedule self;
    init_s_kt_para_3dSchedule(&self, cnt_cores, nrows, ncols, SM, schedule_type);
    //! De-allocate:
    free__s_kt_para_3dSchedule(&self);
  }
}

//! Idnetify the perofmrance-lag assicated with using fucntion-calls as comapred to a simple 'aritmetic mulicllaiton-strategy' (ie, without any overhead assicate to meomory-look-ups and code-stack-overhead).
static void functionCallOverhead() {

  assert(false); // FIXME: add seomthing

}

//! Compare the performance-cost of different scheduling-policies: for 1d, 2d and 3d-lsit-iteraiton-case we evlauate/test both wrt. different scheudling-functiosn and wrt. different OpenMP 'internal' schemduling-functions.
static void schedule_policies() {

 assert(false); // FIXME: add seomthing

}

//! The main assert function.
//! @remarks we evalaute the performance-capbilitaies and diffneret disjtoint-forest-comtpaution-permtuations in our "measure_kt_matrix_filter.c" (oekseth, 06. otk. 2016).
void measure_kt_para_3dSchedule__main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  if(isTo_processAll || (0 == strncmp("test_correctness", array[2], strlen("test_correctness"))) ) {
    test_correctness();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("functionCallOverhead", array[2], strlen("functionCallOverhead"))) ) {
    functionCallOverhead();
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("schedule_policies", array[2], strlen("schedule_policies"))) ) {
    schedule_policies();
    cnt_Tests_evalauted++;
  }

  
  assert(false); // FXIME: explreo the 'parallel strategy' of [below]:
  /*  #pragma omp parallel */
  /* { */
  /*     #pragma omp single */
  /*   for (int i = 0; i < 10; i++) */
  /*     { */
  /*        #pragma omp task */
  /* 	callback(i); */
  /* 	printf("Task %d created\n", i); */
  /*     } */
  /* } */
  /* printf("Parallel region ended\n"); */


  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}


