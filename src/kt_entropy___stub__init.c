  { //! Update:
    const t_float empty_0 = /*default-value=*/0;
    // printf("(kt-entropy-stub-init)\t listof_values_forindex_size=%d\n", (int)listOf_values_forIndex_size);
    assert((int)listOf_values_forIndex_size > 0);
    assert(listOf_values_forIndex_size > 0);
    self.listOf_values_forIndex = allocate_1d_list_float(/*size=*/listOf_values_forIndex_size*2, empty_0); 
    self.listOf_values_forIndex_midPoint = self.listOf_values_forIndex + listOf_values_forIndex_size; //! ei, to 'handle' zero-valeus.

    // printf("allcoate for size=%d and arr=%p, add-midpoint=%p, at [%s]:%s:%d\n", (int)listOf_values_forIndex_size, listOf_values_forIndex, listOf_values_forIndex_midPoint, __FUNCTION__, __FILE__, __LINE__);

    for(int i = 1; i < listOf_values_forIndex_size; i++) {
      // printf("\t update-midPoint-index-log[%u], at %s:%d\n", (uint)i, __FILE__, __LINE__);
      self.listOf_values_forIndex_midPoint[i] = (t_float)__MiF__log((float)i+self.val_adjust);
      assert(self.listOf_values_forIndex_midPoint[i] == self.listOf_values_forIndex[i + listOf_values_forIndex_size]);
      if (mineAccuracy_computeLog_useAbs == true) {//! then we assume that 'taking' the "abs' is correct wrt. corrrectness of the resutls (ie, wrt. the applciaotn of the algorithm):
	self.listOf_values_forIndex_midPoint[-1*i] = self.listOf_values_forIndex_midPoint[i]; //! ie, implicty stored the 'abs' value.
      }
    }
  }

  if(optmize_use_logValues_fromLocalTable == false) { //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]:
    const uint cnt_allocated = 2*(uint)(self.listOf_values_size+1); //! ie, to handle the value-range [-1, 1].
    // FIXME: vlaidte correnctes sof [”elow] init-procudre (oekseth, 06. mar. 2018).
    self.vec_fast_log_defaultValue = VECTOR_FLOAT_SET1(/*listOf_values_size=*/(t_float)listOf_values_size);
    self.listOf_values = allocate_1d_list_float(/*size=*/cnt_allocated, /*default-value=*/0); 
    self.listOf_values_shallowCopy_aboveZero =  self.listOf_values + listOf_values_size; //! ie, to reduce the number of artimethic operations to identify the index for valeus in range [0, 1]
    assert(self.listOf_values);
    assert(VECTOR_FLOAT_getAtIndex(self.vec_fast_log_defaultValue, /*index=*/0) == listOf_values_size);

    if(false) {printf("\t(info)\t allcoated \"listOf_values\" w/cnt_allocated=%u, at %s:%d\n", cnt_allocated, __FILE__, __LINE__);}



    for(int i = 1; i <= (int)listOf_values_size; i++) {
      // FIXME: make use of the log-valeus.
      const uint local_index = listOf_values_size - i;  
#if 1 == 2
      assert(i< cnt_allocated);
      const float scalar_value = (-1*(float)i)*((float)1/(float)(listOf_values_size)); //! ie, the negative value-range [0, -1] 
      assert(isnan(__MiF__log(scalar_value))); //! ie, as we expect that negative nubmers will alwasy be compelx, ie, NAN
      self.listOf_values[local_index] = __MiF__log(scalar_value);
      const float expected_index = __get_internalIndexOf_float(scalar_value);
      // printf("[(%u=%f) <- %f] = %f, at %s:%d\n", local_index, expected_index, scalar_value, listOf_values[local_index], __FILE__, __LINE__);
      assert(listOf_values[local_index] != NAN);
      assert(expected_index == local_index); //! ie, what for for cosnsitency expect.
#endif
      self.listOf_values[local_index] = 0; //! ie, as we expect that negative nubmers will alwasy be compelx, ie, NAN
    }
    
    for(uint i = 1; i < listOf_values_size; i++) {
      // FIXME: make use of the log-valeus.    
      const uint local_index = listOf_values_size + i;
      assert(local_index < cnt_allocated);
      const float scalar_value = (float)i/(float)(listOf_values_size);    
      self.listOf_values[local_index] = __MiF__log(scalar_value);
      // printf("[%u=%f] = %f , at %s:%d\n", local_index, scalar_value, listOf_values[local_index], __FILE__, __LINE__);
      assert(self.listOf_values[local_index] != NAN); 
      //      assert(__get_internalIndexOf_float(scalar_value) == local_index); //! ie, what for for cosnsitency expect.    
      assert(self.listOf_values_shallowCopy_aboveZero[i] == self.listOf_values[local_index]); //! ie, givne the expectiaotn of a 'shallow copy'.
      if (mineAccuracy_computeLog_useAbs == true) {//! then we assume that 'taking' the "abs' is correct wrt. corrrectness of the resutls (ie, wrt. the applciaotn of the algorithm):
	assert(self.listOf_values_shallowCopy_aboveZero != NULL);
	self.listOf_values_shallowCopy_aboveZero[-1*(int)i] = self.listOf_values[local_index]; //! ie, implicty stored the 'abs' value.
      }
    }
  } //! whre 'thsi endif' refers to: #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values:

#undef __MiF__log
