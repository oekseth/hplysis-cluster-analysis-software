#include "alg_dynamicProgramming_base.h"



/* Returns length of LCS for X[0..m-1], Y[0..n-1] */
//! Src: "http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/"
static uint lcs__recursive( const char *X, const char *Y, const uint m, const uint n) {
  if (m == 0 || n == 0)
    return 0;
  if (X[m-1] == Y[n-1])
    return 1 + lcs(X, Y, m-1, n-1);
  else
    return macro_max(lcs(X, Y, m, n-1), lcs(X, Y, m-1, n));
}
 
/* Returns length of LCS for X[0..m-1], Y[0..n-1] */
//! Src: "http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/"
//! Note: of interest is to observe how "LCS(...)" is strongly simlairt to (a) insertion-sort (wrt. the '2d-iterioant') and wrt. prefix-sum (where 'only' the "+1" step is used, ie, in cosntrast tot he "LCS(..)" appraoch of using "max(..)"). 
static uint lcs__dp( const char *X, const char *Y, const uint m, const uint n) { 
  const uint nrows = m+1; const uint ncols = n+1;
  const uint empty_0 = 0;
  uint **L = allocate_2d_list_float(nrows, ncols, empty_0);

  assert(false); // FIXME: write a permtaution of "lcs__recursive(..)" where we use an "s_kt_list_1d_uint_t" data-structure as input ... and scaffold use-cases .... eg, to idneityf/descirbe ....??... <-- an example-input is to compare two feautre-vectors, where we ...??.... 
  
  /* Following steps build L[m+1][n+1] in bottom up fashion. Note 
     that L[i][j] contains length of LCS of X[0..i-1] and Y[0..j-1] */
  for (uint i=1; i<=m; i++) {
    for (uint j=1; j <=n; j++) {
      /* if( (i == 0) || (j == 0) ) { */
      /*   L[i][j] = 0; */
      /* } else  */
      if (X[i-1] == Y[j-1]) {
	L[i][j] = L[i-1][j-1] + 1;
      } else {
	L[i][j] = macro_max(L[i-1][j], L[i][j-1]);
      }
    }
  }
  
  /* L[m][n] contains length of LCS for X[0..n-1] and Y[0..m-1] */
  const uint result = L[m][n];
  free_2d_list_float(&L);
  return result;
}
  


// A naive C/C++ based recursive implementation of LIS problem
#include<stdio.h>
#include<stdlib.h>
 
// Recursive implementation for calculating the LIS
//! Src: "http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/"
static const uint _lis__rec(const uint arr[], const uint n, uint *max_lis_length) {
  // Base case
  if (n == 1)
    return 1;
 
  int current_lis_length = 1;
  for (uint i=0; i<n-1; i++) {
    // Recursively calculate the length of the LIS
    // ending at arr[i]
    int subproblem_lis_length = _lis__rec(arr, i, max_lis_length);
    
    // Check if appending arr[n-1] to the LIS
    // ending at arr[i] gives us an LIS ending at
    // arr[n-1] which is longer than the previously
    // calculated LIS ending at arr[n-1] 
    if( 
       (arr[i] < arr[n-1]) //! then 'value is less than parent' (ie, the 'approx-sorted-porperty' holds
       && (current_lis_length < (1+subproblem_lis_length)) //! then we 'have an improvement'.
	) {
      current_lis_length = 1+subproblem_lis_length;
    }
  }
 
  // Check if currently calculated LIS ending at
  // arr[n-1] is longer than the previously calculated
  // LIS and update max_lis_length accordingly 
  if (*max_lis_length < current_lis_length)
    *max_lis_length = current_lis_length;
 
  return current_lis_length;
}
 

/**
   @brief compute "Longest Increasing Substring" (LIS).
   @remarks The wrapper function for _lis__rec(). 
   @remarks Src: "http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/"
   @remarks idea: while 'this recusive procedure' back-track all 'cases where the previus-elements/child is less than this/parent', a 'dynamic-appraoch' instead 'mvoes forward' (ie, wher eht child ivnestiates if the poarent results ini a better solluiont'. 
   @remarks Inputs and outputs: 
   Input  : arr[] = {3, 10, 2, 1, 20}
   Output : Length of LIS = 3
   The longest increasing subsequence is 3, 10, 20
   
   Input  : arr[] = {3, 2}
   Output : Length of LIS = 1
   The longest increasing subsequences are {3} and {2}
   
   Input : arr[] = {50, 3, 10, 7, 40, 80}
   Output : Length of LIS = 4
   The longest increasing subsequence is {3, 7, 40, 80}
 **/
static uint lis__recursive(const uint arr[], const uint n) {
  uint max_lis_length = 1; // stores the final LIS
 
  // max_lis_length is passed as a reference below 
  // so that it can maintain its value
  // between the recursive calls 
  _lis__rec( arr, n, &max_lis_length );
 
  return max_lis_length;
}
 
/* lis() returns the length of the longest increasing
   subsequence in arr[] of size n */
static uint lis__dp(const uint arr[], const uint n) {
#if(1 == 0)
  uint *list = (uint*) malloc ( sizeof( uint ) * n );
  /* Initialize LIST values for all indexes */
  for (uint i = 0; i < n; i++ ) {
    list[i] = 1;
  }
#else
  const uint empty_0 = 0;
  uint *list = allocate_1d_listt_uint(n, empty_0);
#endif
 
  assert(false); // FIXME[conceutaliste]: try desciring/suggesting an 'applicoant' wrt. this alorithm/approach .,.. when evlauating simalrity-emtrics.

  /* Compute optimized LIST values in bottom up manner */
  uint  max = 0;
  for(uint i = 1; i < n; i++ ) {
    for(uint j = 0; j < i; j++ ) { 
      if( (arr[i] > arr[j]) //! ie, the 'sorted property' holds.
	  //! Note: to udnerstand the improtance/impact of the latter, try ansswering the quesiton: "does the path increase if we choose to inlcude Bergen in our path to London?".
	  && (list[i] < (list[j] + 1)) //! ie, "listt[j]" results in a na increased/longer path-length for "i"
	  ) {
	list[i] = list[j] + 1;
      }
    }
  }
 
  /* Pick maximum of all LIS values */
  for (uint i = 0; i < n; i++ ) {
    if (max < list[i]) {
      max = list[i];
    }
  }
 
  /* Free memory to avoid memory leak */
#if(1 == 0)
  free(list);
#else
  free_1d_list_uint(&list);
#endif
 
  return max;
}



// A Dynamic Programming based C++ program to find minimum
// number operations to convert str1 to str2
#include<bits/stdc++.h>
using namespace std;
 
// Utility function to find minimum of three numbers
#define min_3(x,  y, z) ({ macro_min(macro_min(x, y), z);})

 
static uint editDistDP(const char *str1, const char *str2, const uint m, const uint n) {
  // Create a table to store results of subproblems
  const uint nrows = m+1; const uint ncols = n+1;
  const uint empty_0 = 0;
  uint **dp = allocate_2d_list_float(nrows, ncols, empty_0);
  //uint dp[m+1][n+1];
 
  assert(false); // FIXME[conceptualse]: compare to "LCS(..)" and "LIS(..)"
  assert(false); // FIXME[conceptualse]: try descirinb how 'this proceudre' may be used to evaluate simlairty (or alt. to 'discuss strenghts of simalrity-emtrcs') .... ie, as 'this appraoch' use a 'binary count of max-matches'. <-- consider to 'support this' wrt. 'sparse data-sets', ie, where we ....??...

  // Fill d[][] in bottom up manner
  for (uint i=0; i<=m; i++) {
    for (uint j=0; j<=n; j++) {
      // If first string is empty, only option is to
      // isnert all characters of second string
      if (i==0) {
	dp[i][j] = j;  // intialise to worst-possible-score: Min. operations = j
      }
      
      // If second string is empty, only option is to
      // remove all characters of second string
      else if (j==0) {
	dp[i][j] = i; // itnlaise to worst-possilbe-score: Min. operations = i
      }
      
      // If last characters are same, ignore last char
      // and recur for remaining string
      else if (str1[i-1] == str2[j-1]) {
	dp[i][j] = dp[i-1][j-1];
      }
      
      // If last character are different, consider all
      // possibilities and find minimum
      else {
	dp[i][j] = 1 + min_3(dp[i][j-1],  // Insert
			     dp[i-1][j],  // Remove
			     dp[i-1][j-1]); // Replace
      }
    }
  }
  const uint score =  dp[m][n];
  free_2d_list_float(&dp);
  return score;
}
 

static void test__alg_dynamicProgramming_base() {
  {  //! Test for "longest Common Substring" (LCS):
    const char X[] = "AGGTAB";
    const char Y[] = "GXTXAYB";
    
    uint m = strlen(X);
    uint n = strlen(Y);

    
    printf("Length of LCS is %d\n", lcs__recursive( X, Y, m, n ) );
    printf("Length of LCS is %d\n", lcs__dp( X, Y, m, n ) );
  }
  { //! Test for "Longest Increasing Substring" (LIS):
    uint arr[] = {10, 22, 9, 33, 21, 50, 41, 60};
    uint n = sizeof(arr)/sizeof(arr[0]);
    printf("Length of LIS is %d\n", lis__recursive( arr, n ));
    printf("Length of LIS is %d\n", lis__dp( arr, n ));
  } 
  { //! Edit-dstiance: 
    const char *str1 = "sunday";
    const char *str2 = "saturday";
    printf("Length of edit-distance is %d\n", editDistDP(str1, str2, strlen(str1), strlen(str2));
  }
}
 
