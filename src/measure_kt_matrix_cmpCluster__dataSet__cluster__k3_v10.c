 const uint cnt_defaultScoreTypes_case2 = 4; const uint cnt_defaultScoreTypes_cntVertices_case2 = 10; const uint cnt_defaultScoreTypes_cntClusters2 = 3;
uint mapOf_scoreDistributions_case2[cnt_defaultScoreTypes_case2][cnt_defaultScoreTypes_cntVertices_case2] = {
  //! List the clusterId assicated to each vertex:
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //! Case1.1: ie, all in the same cluster, eg, where vertex[0] is in cluster='0'
  {0, 0, 0, 0, 1, 1, 1, 2, 2, 2}, //! partiionen approx. equally betweent eh clusters.
  {0, 0, 0, 0, 0, 0, 1, 1, 1, 1}, //! partiionto into two clusters, where we observe a 'slight skew' in one of the 'direciton'
  {1, 1, 1, 1, 1, 1, 2, 2, 2, 2}, //! [same as [above]], ie, a permtaution to investigate the 'relatedness wrt. simlmiarty in skew'
};
//! The 'gold standard' when [above] comparison-metric-results are combined: to assess the relative exactness of the cluster-comparison-metrics we 'write down' the expected 'best-scoring clsuter-comparison-squares'. Note: in [below] we 'asisgn a higher valeu to metrics with a higer score': while '6' is the  topmost metric, '0' is the worst metric
t_float mapOf_scoreDistributions_case2_2xgoldStandard[/*cnt_defaultScoreTypes_case2*cnt_defaultScoreTypes_case2=*/16] = {
  6, 1, 3, 3,   /*! ie, Row 1 */
  1, 6, 4, 4,   /*! ie, Row 2 */
  // FIXME[JC]: may you vlaidate correctness of [”elow]?
  3, 4, 6, 6,   /*! ie, Row 3 */
  3, 4, 6, 6,   /*! ie, Row 4 */
};
//! A Distance-matrix-set which is used to 'test different distance-baed-metrics ability to capture incosnistencies in data':
t_float distMatrix_case2_subCase_a[cnt_defaultScoreTypes_cntVertices_case2][cnt_defaultScoreTypes_cntVertices_case2] = {
  //! In this 'case' we have: all the vertices in the same cluster. 
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  //! 
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  //! 
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  //! 
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
};
t_float distMatrix_case2_subCase_b[cnt_defaultScoreTypes_cntVertices_case2][cnt_defaultScoreTypes_cntVertices_case2] = {
  //! In this 'case' we have: 'isolated ilsealnds', ie, no vertices are assicated to any other vertices.
  {10, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 10, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 10, 0, 0, 0, 0, 0, 0, 0},
  //! 
  {0, 0, 0, 10, 0, 0, 0, 0, 0, 1},
  {0, 0, 0, 0, 10, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 10, 0, 0, 0, 0},
  //! 
  {0, 0, 0, 0, 0, 0, 10, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 10, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 10, 0},
  //! 
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 10},
};
t_float distMatrix_case2_subCase_c[cnt_defaultScoreTypes_cntVertices_case2][cnt_defaultScoreTypes_cntVertices_case2] = {
  //! In this 'case' we have: a well-defined iseland wrt. the different clusters.
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  //! 
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  //! 
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
  //! 
  {10, 10, 10, 10, 10, 10, 1, 1, 1, 1},
};


