
//!
//! Store the gold-matrix:
s_kt_matrix_t matrixOf_gold__corr; setTo_empty__s_kt_matrix_t(&matrixOf_gold__corr);
const uint def_0 = 0;
const uint cnt_goldStandardRows = 0;
uint **clusterPredictionDataSets__set1 = allocate_2d_list_uint(cnt_goldStandardRows, nrows, def_0);
//s_kt_matrix_t matrixOf_gold__clustMemberships; setTo_empty__s_kt_matrix_t(&matrixOf_gold__clustMemberships);
//uint cnt_gold_added = 0;
s_kt_matrix_t obj_clusterResults__noiseFixedAndDynamic; init__s_kt_matrix(&obj_clusterResults__noiseFixedAndDynamic, /*nrows=*/stringOf_noise_size*fraction_colNoise_cnt, /*ncols=*/nrows, /*isTo_allocateWeightColumns=*/false); uint obj_clusterResults__noiseFixedAndDynamic__currentRowIndex = 0;

//! 
//! Application: 
s_kt_matrix_t obj_clusterResults__noiseFixed; init__s_kt_matrix(&obj_clusterResults__noiseFixed, /*nrows=*/stringOf_noise_size, /*ncols=*/nrows, /*isTo_allocateWeightColumns=*/false); uint obj_clusterResults__noiseFixed__currentRowIndex = 0;

//! 
//! Application: 
s_kt_matrix_t obj_clusterResults__noiseDynamic; init__s_kt_matrix(&obj_clusterResults__noiseDynamic, /*nrows=*/fraction_colNoise_cnt, /*ncols=*/nrows, /*isTo_allocateWeightColumns=*/false); uint obj_clusterResults__noiseDynamic__currentRowIndex = 0;


for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
  //! Budil the sample-string:
  char stringOf_sampleData_type[2000]; sprintf(stringOf_sampleData_type, "%s%s", mapOf_functionStrings_base[base_id], stringOf_noise[noise_id]);
  if( (noise_id > 0) && (NULL == strstr(stringOf_sampleData_type, "lines-"))) { continue;} //! ie, to avoid 'calls which produce the same result as "noise_id == 0".
  const bool isTo_transposeMatrix = mapOf_functionStrings_base__classificaiton[base_id].isTo_transpose;

  for(uint fraction_colNoise = 0; fraction_colNoise < fraction_colNoise_cnt; fraction_colNoise++) {
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    //! 
    { //! Load the sample-data:
      const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const uint fractionOf_toAppendWith_sampleData_typeFor_columns = 20*fraction_colNoise;
      const char *stringOf_sampleData_type_realLife = NULL;	      
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
      if(obj_baseToInclude.nrows) {
	for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
	  //assert(obj_baseToInclude.matrix[row_id]);
	  assert(row_id < obj_baseToInclude.nrows);
	  assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
	  const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
	  assert(row);
	}
      }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
      //! 
      //! What we expect:
      assert(obj_matrixInput.nrows > 0); 		assert(obj_matrixInput.ncols > 0);		  
    }
    //!
    //! Make the 'call':
    s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, fileHandler__global);
    hp_config.config.corrMetric_prior = corrMetric_prior;
    hp_config.config.corrMetric_insideClustering = corrMetric_insideClustering;
    hp_config.config.clusterConfig.isTo_transposeMatrix = false;
    hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    //hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
    //!
    //! Cluster:
    const bool is_ok = cluster__hpLysis_api(&hp_config, e_hpLysis_clusterAlg_kCluster__AVG, &obj_matrixInput, /*k-cluster=*/nclusters_ideal, /*npass=*/npass_default);
    assert(is_ok);
    if(isTo_use_MINE__e_kt_correlationFunction(corrMetric_insideClustering.metric_id)) {
      if(matrixOf_gold__corr.nrows == 0) { //! then remember the gold-matrix:
	//! We expec the result-matrix to 'hold' data:
	assert(hp_config.matrixResult__inputToClustering.nrows > 0);
	assert(hp_config.matrixResult__inputToClustering.ncols > 0);		   
	//! Copy the gold-data-set, a data-set used to assess the quality of the different clustering-metrics:
	const bool is_ok = init__copy__s_kt_matrix(&matrixOf_gold__corr, &hp_config.matrixResult__inputToClustering, /*isTo_updateNames=*/true);
	assert(is_ok);
	assert(matrixOf_gold__corr.nrows > 0); 
	assert(matrixOf_gold__corr.ncols > 0);
	//!
	//! Copy the clsuter-memberships:
	for(uint col_id = 0; col_id < hp_config.obj_result_kMean.cnt_vertex; col_id++) {
	  const uint cluster_id = hp_config.obj_result_kMean.vertex_clusterId[col_id];
	  clusterPredictionDataSets__set1[0][col_id] = cluster_id;
	}
      }
    }
    //!
    //! Update the result:
    assert(hp_config.obj_result_kMean.cnt_vertex > 0);
    assert(hp_config.obj_result_kMean.vertex_clusterId != NULL);
    assert(hp_config.obj_result_kMean.cnt_vertex <= obj_matrixInput.nrows);
    assert(hp_config.obj_result_kMean.cnt_vertex <= obj_matrixInput.nrows);
    //! Valdiate that the input-matrices are correctly intiated:
    //assert(obj_clusterResults__noise__currentRowIndex < obj_clusterResults__noise.nrows);
    //assert(obj_matrixInput.nrows == obj_clusterResults__noise.ncols);
    //! --
    /* assert(obj_clusterResults__allCombinations__currentrowIndex < obj_clusterResults__allCombinations.nrows); */
    /* assert(obj_clusterResults__allCombinations.ncols == obj_matrixInput.nrows); */
    //! --
    assert(obj_clusterResults__noiseFixedAndDynamic__currentRowIndex < obj_clusterResults__noiseFixedAndDynamic.nrows);
    assert(obj_clusterResults__noiseFixedAndDynamic.ncols == obj_matrixInput.nrows);
    if(fraction_colNoise == 0) {
      //! --
      assert(obj_clusterResults__noiseFixed__currentRowIndex < obj_clusterResults__noiseFixed.nrows);
      assert(obj_clusterResults__noiseFixed.ncols == obj_matrixInput.nrows);
    }
    if(noise_id == 0) {
      //! --
      assert(obj_clusterResults__noiseDynamic__currentRowIndex < obj_clusterResults__noiseDynamic.nrows);
      assert(obj_clusterResults__noiseDynamic.ncols == obj_matrixInput.nrows);
    }		
    //! ------
    //! Iterate:
    for(uint col_id = 0; col_id < hp_config.obj_result_kMean.cnt_vertex; col_id++) {
      const uint cluster_id = hp_config.obj_result_kMean.vertex_clusterId[col_id];
      //! Update:
      //obj_clusterResults__allCombinations.matrix[obj_clusterResults__allCombinations__currentrowIndex][col_id] = cluster_id;
      obj_clusterResults__noiseFixedAndDynamic.matrix[obj_clusterResults__noiseFixedAndDynamic__currentRowIndex][col_id] = cluster_id;		   
      if(fraction_colNoise == 0) {
	obj_clusterResults__noiseFixed.matrix[obj_clusterResults__noiseFixed__currentRowIndex][col_id] = cluster_id;
      }
      if(noise_id == 0) {
	obj_clusterResults__noiseDynamic.matrix[obj_clusterResults__noiseDynamic__currentRowIndex][col_id] = cluster_id;
      }
    }
    //! Increment row-counter:
    //obj_clusterResults__allCombinations__currentrowIndex++;
    obj_clusterResults__noiseFixedAndDynamic__currentRowIndex++;
    if(fraction_colNoise == 0) {
      obj_clusterResults__noiseFixed__currentRowIndex++;
    }
    if(noise_id == 0) {
      obj_clusterResults__noiseDynamic__currentRowIndex++;
    }

    //!
    //! De-allcoate:
    free__s_kt_matrix(&obj_matrixInput);
    free__s_hpLysis_api_t(&hp_config);		
  }
 }

//! ---------------------------------------
//! 
//! What we expect:
assert(matrixOf_gold__corr.nrows > 0); //! as it otherwise maight be thatthe 'gold-stadnard-metrics' has erronrosuly Not been used ... eg, where we then could 'udpate our call to use a specific/fucntion-define dmeitrc'.
assert(matrixOf_gold__corr.nrows == matrixOf_gold__corr.ncols); //! ie, as we expec the matrix to be 'squared'
assert(obj_clusterResults__noiseFixedAndDynamic.nrows > 0);
assert(obj_clusterResults__noiseFixed.nrows > 0);
assert(obj_clusterResults__noiseDynamic.nrows > 0);


//! ---------------------------------------
//! 
//! Apply logics:	  
{ //! ----------------
  const char *copmuteClusterSim__prefix = "noiseFixedAndDynamic";
  #define __computeclusterSim__inputObj obj_clusterResults__noiseFixedAndDynamic
  #define __computeclusterSim__resultObj &cmpResult_differentSizes__noiseFixedAndDynamic
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__step2__fixedDataAndDim__computeClusterSim.c"
}
{ //! ----------------
  const char *copmuteClusterSim__prefix = "dynamicNoise";
  #define __computeclusterSim__inputObj obj_clusterResults__noiseDynamic
  #define __computeclusterSim__resultObj &cmpResult_differentSizes__noiseDynamic
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__step2__fixedDataAndDim__computeClusterSim.c"
}
{ //! ----------------
  const char *copmuteClusterSim__prefix = "fixedNoise";
  #define __computeclusterSim__inputObj obj_clusterResults__noiseFixed
  #define __computeclusterSim__resultObj &cmpResult_differentSizes__noiseFixed
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__step2__fixedDataAndDim__computeClusterSim.c"
}



//! ---------------------------------------
//! 
//! De-allocate:
free__s_kt_matrix(&matrixOf_gold__corr);
assert(clusterPredictionDataSets__set1);  free_2d_list_uint(&clusterPredictionDataSets__set1, cnt_goldStandardRows); clusterPredictionDataSets__set1 = NULL;
free__s_kt_matrix(&obj_clusterResults__noiseFixedAndDynamic);
free__s_kt_matrix(&obj_clusterResults__noiseFixed);
free__s_kt_matrix(&obj_clusterResults__noiseDynamic);
