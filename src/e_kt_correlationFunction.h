#ifndef e_kt_correlationFunction_h
#define e_kt_correlationFunction_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @file e_kt_correlationFunction
   @brief the global defintions of the correlation-functions which is supported by Knitting-Tools correlation-software (oekseth, 06. setp. 2016)
   @author Ole Kristian Ekseth (oekseth)   
 **/
#ifndef SWIG
#include <assert.h>
#endif // #ifndef SWIG

/**
   @enum e_kt_categoryOf_correaltionPreStep
   @brief classifies the diffenret tyeps of rpe-steps which may be used before applicaiton of the non-rank correaltion-metrics
   @author Ole Kristian Ekseth (oekseth)   
   @remarks to examplify usage if the "e_kt_categoryOf_correaltionPreStep_rank" enum is used in combaiton with Pearsos's correlation-metric then the 'result' is comptuation of Spearman's correlation-coefficient. From the latter example we observe how the applicaiton of ranks as a pre-step is a commonly known/estasblisehd practice in interprtation of relationshisp/correlations (in the field of knowledge-anlayssi and knowledge-dsicovery). 
 **/
typedef enum e_kt_categoryOf_correaltionPreStep {
  e_kt_categoryOf_correaltionPreStep_rank,
  e_kt_categoryOf_correaltionPreStep_binary,
  e_kt_categoryOf_correaltionPreStep_none,
} e_kt_categoryOf_correaltionPreStep_t;

/* //! @return the string-rperesnetiont of the e_kt_categoryOf_correaltionPreStep_t enum (oekseth, 06. nov. 2016). */
/* static const char *get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) { */
/*   if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {return "rank";} */
/*   else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {return "binary";} */
/*   else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) {return "none";} */

/*   //assert(false); // TODO: consider adding support for this. */


/*   return ""; */
/* } */

/**
   @enum e_kt_correlationFunction
   @brief provide a standrized approach to specify the correlation-enum of choise (ie, to use).
   @author Ole Kristian Ekseth (oekseth)   
 **/
typedef enum e_kt_correlationFunction {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly,
#endif
  // FIXME: make use of [”elow] and then update our article witht he new-itnroduced correlation-emtric ...   and consider to include/inroroporate other correlation-metrics ("http://people.sc.fsu.edu/~jburkardt/c_src/correlation/correlation.html") ... writtten for an aritcle with title "A Review of Gaussian Random Fields and Correlation Functions"

  // TODO[article]: validate the 'exepctions' described for "Kendall" in our "correlationType_kendall.c" wrt. the "todo[article]" 'tag' (oekseth, 06. des. 2016).
  e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine,
  e_kt_correlationFunction_groupOf_rank_kendall_coVariance,
  e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock,
  e_kt_correlationFunction_groupOf_rank_kendall_Dice,
  e_kt_correlationFunction_groupOf_rank_kendall_Goodman, //! Note: equaiton is based on the work found in ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-15-S2-S2" w/title "On the selection of appropriate distances for gene expression data clustering"] (oekseth, 06. apr. 2017).
  e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized, //! Note: equaiton is based on the work found in ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-15-S2-S2" w/title "On the selection of appropriate distances for gene expression data clustering"] (oekseth, 06. apr. 2017).
  //e_kt_correlationFunction_groupOf_rank_kendall_Cosine,
  //  e_kt_correlationFunction_groupOf_rank_kendall_Hellinger,



  // FIXME: itneate [”elow] in oru enum-supports 

  //! A subset of the Minkowski group of metrics, wehre the oterhs are Euclid and CityBlock
  e_kt_correlationFunction_groupOf_minkowski_euclid,
  e_kt_correlationFunction_groupOf_minkowski_cityblock,
  e_kt_correlationFunction_groupOf_minkowski_minkowski,
  e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne,
  e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3,
  e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4,
  e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5,
  e_kt_correlationFunction_groupOf_minkowski_chebychev,
  e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max,

  //! A new group of metrics: "absolute difference"
  e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen,
  e_kt_correlationFunction_groupOf_absoluteDifference_Gower,
  e_kt_correlationFunction_groupOf_absoluteDifference_Soergel,
  e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski,
  e_kt_correlationFunction_groupOf_absoluteDifference_Canberra,
  e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian,
  // FIXME: write a senticvigty-and-correcntess-tests for [”elow] emtric with mid-fix "oekseth" ... metrics which are nto foudn nor described in the compreehsnive overview-wrok by ["http://users.uom.gr/~kouiruki/sung.pdf"] (oekseth, 06. sept. 2016)
  //! Note: for a reference wrt. the 1-pass algorithm for variance-comptaution see ["http://suave_skola.varak.net/proj2/stddev.pdf"]
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean,
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2,
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3,
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4,
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5,
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne,
  e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable,

  //! A new group of metrics: "intersection"  
  e_kt_correlationFunction_groupOf_intersection_intersection,
  e_kt_correlationFunction_groupOf_intersection_intersection_altDef, //! ie, a 'semi-inverted' metric to "e_kt_correlationFunction_groupOf_intersection_intersection,"
  e_kt_correlationFunction_groupOf_intersection_WaveHedges,
  e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt,
  e_kt_correlationFunction_groupOf_intersection_Czekanowski,
  e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef,
  e_kt_correlationFunction_groupOf_intersection_Motyka,
  e_kt_correlationFunction_groupOf_intersection_Motyka_altDef,
  e_kt_correlationFunction_groupOf_intersection_Kulczynski,
  e_kt_correlationFunction_groupOf_intersection_Ruzicka,
  e_kt_correlationFunction_groupOf_intersection_Tanimoto,
  e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef,



  //! A new group of metrics: "Inner Product"
  e_kt_correlationFunction_groupOf_innerProduct_innerProduct,
  e_kt_correlationFunction_groupOf_innerProduct_harmonicMean,
  e_kt_correlationFunction_groupOf_innerProduct_cosine,
  e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook,
  e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef,
  e_kt_correlationFunction_groupOf_innerProduct_Dice,
  e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef,
  e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance,
  e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian, //! "Petter Abrahamsen,     A Review of Gaussian Random Fields and Correlation Functions,     Norwegian Computing Center, 1997." and "https://en.wikipedia.org/wiki/Distance_correlation"  
  e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator,

  //! A new group of metrics: "Fidelity"
  e_kt_correlationFunction_groupOf_fidelity_fidelity,
  e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya,
  e_kt_correlationFunction_groupOf_fidelity_Hellinger,
  e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef,
  e_kt_correlationFunction_groupOf_fidelity_Matusita,
  e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef,
  e_kt_correlationFunction_groupOf_fidelity_squaredChord,
  e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef,


  //! A new group of metrics: "Squared"
  e_kt_correlationFunction_groupOf_squared_Euclid,
  e_kt_correlationFunction_groupOf_squared_Pearson,
  e_kt_correlationFunction_groupOf_squared_Neyman,
  e_kt_correlationFunction_groupOf_squared_squaredChi,
  e_kt_correlationFunction_groupOf_squared_probabilisticChi,
  e_kt_correlationFunction_groupOf_squared_divergence,
  e_kt_correlationFunction_groupOf_squared_Clark,
  e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi,
  //! Note: [below] 'perason-correlation-coeffcient' metircs are Not found nor described in the compreehsnive overview-wrok by ["http://users.uom.gr/~kouiruki/sung.pdf"], though extensivly described in other soruce (eg, ("https://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient")) (oekseth, 06. sept. 2016)
  e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic,
  e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute,
  e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered,
  e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute,
  // e_kt_correlationFunction_groupOf_squared_,

  //! A new group of metrics: "Shannon's"
  e_kt_correlationFunction_groupOf_shannon_KullbackLeibler,
  e_kt_correlationFunction_groupOf_shannon_Jeffreys,
  e_kt_correlationFunction_groupOf_shannon_kDivergence,
  e_kt_correlationFunction_groupOf_shannon_Topsoee,
  e_kt_correlationFunction_groupOf_shannon_JensenShannon,
  e_kt_correlationFunction_groupOf_shannon_JensenDifference,


  //! A new group of metrics: "Combinations"
  e_kt_correlationFunction_groupOf_combinations_Taneja,
  e_kt_correlationFunction_groupOf_combinations_KumarJohnson,
  e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock,



  //! A new group of metrics: "Downward mutaiblity" (a term coined by oekseth): "Vicissitude" (or: downward mutability metrics (where latter is conined/used by oekseth))
  e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges,
  e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax,
  e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin,
  e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric,
  e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax,
  e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax,
  e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin, //! which is expcted to be the laste entry before the "directScore" enum.

  //! A new group of metrics: "directScore" (a term conined by oekseth) where we 'choose' the 'direct interaction-brdige' between two vertices (ie, instead of using the featreu-neighbourhood-descritpions of a given vertex). An example-applicaiton of "directScore" correlation-metrics conserns the case where the matrxi (or matrices) which we use/evlauate 'hold' the shortest-paths between entites (eg, where the 'correlation' between two vertices 'provides' enough information in describing siliarty (eg, when compared to a more time-cost-heavry operation of comparing two rows (Rather than only two cells in the "directScore" correlation-metric-group))) (oekseth, 06. des. 2016).
  //! ---
  //! Note: the "_direct_" sub-type of the "directScore" metric implies that we for the case "row_1, row_2" compute 'sum' as "matrix_1[head][tail] + matrix_2[head][tail]".
  //!
  //!
  //! ---------------- a new semi-sub-grop: "_direct_":
  //! ---------  **** a new sub-group *** 
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  e_kt_correlationFunction_groupOf_directScore_direct_avg,
  e_kt_correlationFunction_groupOf_directScore_direct_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_minus,
  e_kt_correlationFunction_groupOf_directScore_direct_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_direct_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_min,
  e_kt_correlationFunction_groupOf_directScore_direct_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_direct_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_abs,
  e_kt_correlationFunction_groupOf_directScore_direct_log_abs,
  e_kt_correlationFunction_groupOf_directScore_direct_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus, //! which is expected ot be the laste enum in "_direct_" sub-group
  //!
  //!
  //! ---------------- a new semi-sub-grop: "mergeMatrices":
  //!
  //! 
  //! ---------  **** a new sub-group ***: _mergeMatrices__sum
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus, 
  //!
  //! 
  //! ---------  **** a new sub-group ***: _mergeMatrices__mul_
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus,
  //!
  //! 
  //! ---------  **** a new sub-group ***: _mergeMatrices__avg_
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus, //! which is expected ot be the laste enum in "_nergeMatrices_" sub-group
  //!
  //!
  //! ---------------- a new semi-sub-grop: "symmetricProperty":
  //!
  //! 
  //! ---------  **** a new sub-group ***: _mergeMatrices__sum
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus,
  //!
  //! 
  //! ---------  **** a new sub-group ***: _symmetricProperty__mul_
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus,
  //!
  //! 
  //! ---------  **** a new sub-group ***: _symmetricProperty__avg_
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1, //! ie, alwasy 'choose' matrix[head][tail]
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2, //! ie, alwasy 'choose' matrix[tail][head]
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs,
  //! ---
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs,
  e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus,
#endif //! (globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct=)
  /* //! A new group of metrics: "" */
  /* e_kt_correlationFunction_groupOf_, */

  //! ------------------------

  e_kt_correlationFunction_groupOf_MINE_mic,
  e_kt_correlationFunction_groupOf_MINE_mas,
  e_kt_correlationFunction_groupOf_MINE_mev,
  e_kt_correlationFunction_groupOf_MINE_mcn,
  e_kt_correlationFunction_groupOf_MINE_mcn_general,
  e_kt_correlationFunction_groupOf_MINE_gmic,
  e_kt_correlationFunction_groupOf_MINE_tic,
  //! ------------------------

  e_kt_correlationFunction_undef
} e_kt_correlationFunction_t;


//! @return true if "enum_id" describes a 'kendall' correlation-metric
static bool isTo_use_kendall__e_kt_correlationFunction(const enum e_kt_correlationFunction enum_id) {
  if(
     (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine)
     || (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_coVariance)
     || (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock)
     || (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Dice)
     || (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Goodman)
     || (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized)
     //|| (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Cosine)
     //|| (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Hellinger)
     //|| (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Brownian)
     ) {
    return true;
  } else {return false;}
}

//! @return true if "enum_id" describes a 'MINE' correlation-metric
static bool isTo_use_MINE__e_kt_correlationFunction(const enum e_kt_correlationFunction enum_id) {
  if(
     (enum_id == e_kt_correlationFunction_groupOf_MINE_mic) 
     || (enum_id == e_kt_correlationFunction_groupOf_MINE_mas)
     || (enum_id == e_kt_correlationFunction_groupOf_MINE_mev)
     || (enum_id == e_kt_correlationFunction_groupOf_MINE_mcn)
     || (enum_id == e_kt_correlationFunction_groupOf_MINE_mcn_general)
     || (enum_id == e_kt_correlationFunction_groupOf_MINE_gmic)
     || (enum_id == e_kt_correlationFunction_groupOf_MINE_tic)
     //|| (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Cosine)
     //|| (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Hellinger)
     //|| (enum_id == e_kt_correlationFunction_groupOf_rank_kendall_Brownian)
     ) {
    return true;
  } else {return false;}
}



//! @return true if "enum_id" describes a 'direct-score' correlation-metric-group (oekseth, 06. des. 2016).
static bool isTo_use_directScore__e_kt_correlationFunction(const enum e_kt_correlationFunction enum_id) {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  if(
     (enum_id > e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin)
     && (enum_id < e_kt_correlationFunction_groupOf_MINE_mic)
     ) {
    return true;
  } else {return false;}
#else
  return false;
#endif
}

/**
   @enum e_kt_correlationFunction_groupClass__directScore
   @brief provide specifciatons to idneitfy the sub-group-id of metrics in the "directScore" correlation-type (oekseth, 06. des. 2016).
 **/
typedef enum e_kt_correlationFunction_groupClass__directScore {
  e_kt_correlationFunction_groupClass__directScore_direct,
  //! ----
  e_kt_correlationFunction_groupClass__directScore_mergeMatrices__sum,
  e_kt_correlationFunction_groupClass__directScore_mergeMatrices__mul,
  e_kt_correlationFunction_groupClass__directScore_mergeMatrices__avg,
  //! ----
  e_kt_correlationFunction_groupClass__directScore_symmetricProperty__sum,
  e_kt_correlationFunction_groupClass__directScore_symmetricProperty__mul,
  e_kt_correlationFunction_groupClass__directScore_symmetricProperty__avg,
  //! ----
  e_kt_correlationFunction_groupClass__directScore_undef,
} e_kt_correlationFunction_groupClass__directScore_t;
//!
//! Specify macros which (among others) are expected to be used in our "kt_distance__stub__metricGroup__directScore.c" (oekseth, 06. des. 2016).
#define __macro__e_kt_correlationFunction_groupClass__directScore_direct 0
#define __macro__e_kt_correlationFunction_groupClass__directScore_mergeMatrices__sum 1
#define __macro__e_kt_correlationFunction_groupClass__directScore_mergeMatrices__mul 2
#define __macro__e_kt_correlationFunction_groupClass__directScore_mergeMatrices__avg 3
#define __macro__e_kt_correlationFunction_groupClass__directScore_symmetricProperty__sum 4
#define __macro__e_kt_correlationFunction_groupClass__directScore_symmetricProperty__mul 5
#define __macro__e_kt_correlationFunction_groupClass__directScore_symmetricProperty__avg 6


//! @return the e_kt_correlationFunction_groupClass__directScore_t id of a metric in the "directScore" corrleaiton-metric-gorup (oesketh, 06. des. 2016).
static e_kt_correlationFunction_groupClass__directScore_t getGroupOfMetric__directScore__e_kt_correlationFunction(const e_kt_correlationFunction_t enum_id) {
  if(isTo_use_directScore__e_kt_correlationFunction(enum_id) == true) {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
    if(enum_id <= e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_direct;
      //! ----------------------
    } else if(enum_id <= e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_mergeMatrices__sum;
    } else if(enum_id <= e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_mergeMatrices__mul;
    } else if(enum_id <= e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_mergeMatrices__avg;
      //! ----------------------
    } else if(enum_id <= e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_symmetricProperty__sum;
    } else if(enum_id <= e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_symmetricProperty__mul;
    } else if(enum_id <= e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus) {
      return e_kt_correlationFunction_groupClass__directScore_symmetricProperty__avg;
      //! ----------------------
    } else {

      // FIXME: add something.
      
      //! A fall-back-groutine:
      //fprintf(stderr, "!!\t Seems like you have specified a not-yet-supported metric=%u, ie, please request a code-update. For qeustions please cotnact the senior developer [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
      return e_kt_correlationFunction_groupClass__directScore_undef;
    }
    #else
    assert(false);
    return e_kt_correlationFunction_groupClass__directScore_undef;
#endif
  } else {
    //fprintf(stderr, "!!\t Seems like you have specified a non-DirectScore metric=%u, ie, please investgiate your call. For qeustions please cotnact the senior developer [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
    return e_kt_correlationFunction_groupClass__directScore_undef;
  }
}

/**
   @struct s_kt_correlationMetric
   @brief provide lgocis and parsing-functions for correlation-emtric data-inputs (oekseth, 06. otk. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/
typedef struct s_kt_correlationMetric {
  e_kt_correlationFunction_t metric_id;
  e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep;
} s_kt_correlationMetric_t;




//! Intaite the s_kt_correlationMetric_t object
static void init__s_kt_correlationMetric_t(s_kt_correlationMetric_t *self, const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {
  // assert(self);
  self->metric_id = metric_id;
  self->typeOf_correlationPreStep = typeOf_correlationPreStep;
}
//! Intaite the s_kt_correlationMetric_t object
static s_kt_correlationMetric_t initAndReturn__s_kt_correlationMetric_t(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {
  // assert(self);
  s_kt_correlationMetric_t self;
  self.metric_id = metric_id;
  self.typeOf_correlationPreStep = typeOf_correlationPreStep;
  //! @return
  return self;
}


//! Intaite the s_kt_correlationMetric_t object
static void setTo_empty__s_kt_correlationMetric_t(s_kt_correlationMetric_t *self) {
  //assert(self);
  self->metric_id = e_kt_correlationFunction_undef;
  self->typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_none;
}

//! Intaite the s_kt_correlationMetric_t object (oekseth, 06. des. 2016).
static s_kt_correlationMetric_t setTo_empty__andReturn__s_kt_correlationMetric_t() {
  s_kt_correlationMetric_t self;
  setTo_empty__s_kt_correlationMetric_t(&self);
  return self;
}
//! @return true if the "s_kt_correlationMetric_t" is set to empty (oekseth, 06. arp. 2017)
static bool isEmpty__s_kt_correlationMetric_t(const s_kt_correlationMetric_t *self) {
  return (!self || (self->metric_id == e_kt_correlationFunction_undef) );
}
//! Intaite the s_kt_correlationMetric_t object
static void setTo_default__s_kt_correlationMetric_t(s_kt_correlationMetric_t *self) {
  //assert(self);
  self->metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  self->typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_none;
}

//! Intaite the s_kt_correlationMetric_t object (oekseth, 06. des. 2016).
static s_kt_correlationMetric_t setTo_default__andReturn__s_kt_correlationMetric_t() {
  s_kt_correlationMetric_t self;
  setTo_default__s_kt_correlationMetric_t(&self);
  return self;
}

//! @breif ivnestigate if mask-valeus in/for a given 'metric'  may correctly be replaced by "0" (oekseth, 06. des. 2016).
//! @return true if T_FLOAT_MAX or mask-matrices may be 'replaced' by a more perofrmance-freienly "0" case.
//! @remarks idea is to use the "P * Q" mulitplcaiton-cases to replace T_FLOAT_MAX ans mask[][] matrices, ie, for which we reduce the number of ic-branches and/or flaoting-point-operaiton.s
//! @remarks in 'this funciton' we assume that 'in rank-based correlation-metrics it would be erronous to replace masks (eg, T_FLOAT_MAX) with '0'. in breif we assert that '0' as a 'fitler-value' in rank-based metrics would result in erronous results, ie, as Kendall would find the distance betwen 'emptyness' and a 'defined valeus (for "P AND !Q" and "!P AND Q").
static bool mayReplace_maskWith__zero__s_kt_correlationMetric_t(const s_kt_correlationMetric_t obj) {
  if(obj.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {return false;} //! ie, as 'itnalizing' to '0' woudl then be milseading.
  else {
    const e_kt_correlationFunction_t enum_id = obj.metric_id;
    if(
       (enum_id == e_kt_correlationFunction_groupOf_innerProduct_harmonicMean) 
       || (enum_id == e_kt_correlationFunction_groupOf_innerProduct_innerProduct) 
       || (enum_id == e_kt_correlationFunction_groupOf_innerProduct_cosine) 
       || (enum_id == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook) 
       || (enum_id == e_kt_correlationFunction_groupOf_innerProduct_Dice) 
       || (enum_id == e_kt_correlationFunction_groupOf_fidelity_fidelity) 
       || (enum_id == e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya) 
       || (enum_id == e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef) 
       || (enum_id == e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef) 
       || (enum_id == e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef) 
       || (enum_id == e_kt_correlationFunction_groupOf_shannon_KullbackLeibler) 
       || (enum_id == e_kt_correlationFunction_groupOf_shannon_Jeffreys) 
       //|| (enum_id == ) 
       ) {return true;}
    else {return false;}
  }
}

/**
   @enum e_kt_correlationMetric_initCategory
   @brief provide a categorization of the different correlation-metrics wrt. their exeuction-time (oekseth, 06. des. 2016).
 **/
typedef enum e_kt_correlationMetric_initCategory {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O0,
#endif
  e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1,
  e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O2,
  e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3,
} e_kt_correlationMetric_initCategory_t;

//! Intaite the s_kt_correlationMetric_t object (oekseth, 06. des. 2016).
static s_kt_correlationMetric_t setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(const e_kt_correlationMetric_initCategory_t enum_id) {
//static s_kt_correlationMetric_t setTo_empty__andReturn__s_kt_correlationMetric_t(const e_kt_correlationMetric_initCategory_t enum_id) {
  s_kt_correlationMetric_t self;  
  setTo_default__s_kt_correlationMetric_t(&self);
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  if(enum_id == e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O0) {
    self.metric_id = e_kt_correlationFunction_groupOf_directScore_direct_avg;
  } else 
#endif
if(enum_id == e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1) {
    self.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  } else if(enum_id == e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1) {
    self.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  } else if(enum_id == e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O2) {
    self.metric_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
  } else if(enum_id == e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3) {
    self.metric_id = e_kt_correlationFunction_groupOf_MINE_mic;
  }
  return self;
}


/**
   @enum e_kt_correlationMetric_initCategory_timeComplexitySet__small
   @brief provide a categorization of the different correlation-metrics wrt. their exeuction-time (oekseth, 06. des. 2016).
   @remarks exktends the "e_kt_correlationMetric_initCategory" to simplify testing and comparison of popular corrleation-metrics
 **/
typedef enum e_kt_correlationMetric_initCategory_timeComplexitySet__small {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_avg, //! "e_kt_correlationFunction_groupOf_directScore_direct_avg"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_mul, //! "e_kt_correlationFunction_groupOf_directScore_direct_mul"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_maxNumerator, //! "e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator"
#endif
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid, //! "e_kt_correlationFunction_groupOf_minkowski_euclid"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_citbyblock, //! "e_kt_correlationFunction_groupOf_minkowski_cityblock"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_absoluteDifference_Canberra, //! "e_kt_correlationFunction_groupOf_absoluteDifference_Canberra"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson, //! "e_kt_correlationFunction_groupOf_squared_Pearson"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman, //! 'rank' and "e_kt_correlationFunction_groupOf_squared_Pearson"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_kendall_coVariance,
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_O3_MINE_mic, //! "e_kt_correlationFunction_groupOf_MINE_mic"
  e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef, 
} e_kt_correlationMetric_initCategory_timeComplexitySet__small_t;

//! @return a string-represnetioant of the "e_kt_correlationMetric_initCategory_timeComplexitySet__small_t" enum
static const char *getStringOf__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(const e_kt_correlationMetric_initCategory_timeComplexitySet__small_t enum_id) {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_avg) {return "direct--avg";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_mul) {return "";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_maxNumerator) {return "O0--direct--maxNumerator";}
  else 
#endif
if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid) {return "O1--Minkowski--Euclid";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_citbyblock) {return "O1--Minkowski--Citbyblock";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_absoluteDifference_Canberra) {return "O1--absoluteDifference--Canberra";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson) {return "O1--Squared--Pearson";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman) {return "O2--Spearman";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_kendall_coVariance) {return "O2--Kendall";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O3_MINE_mic) {return "O3--MINE--Mic";}
  else {return "<undefined>";} // ie, as we then need to add support 'for this'.  
}

//! @return the gorup-id assciated to the verex:
static e_kt_correlationMetric_initCategory_timeComplexitySet__small_t getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(const s_kt_correlationMetric_t obj) {
  const e_kt_correlationFunction_t metric_id = obj.metric_id;
  //! ----------
  if((metric_id == e_kt_correlationFunction_groupOf_squared_Pearson) && (obj.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman;}
  else if((metric_id == e_kt_correlationFunction_groupOf_rank_kendall_coVariance) && (obj.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_kendall_coVariance;}
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  else if( (metric_id == e_kt_correlationFunction_groupOf_directScore_direct_avg) ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_avg;}
  /* if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_avg) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_avg;} */
  /* else if(metric_id == ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_mul;} */
  /* else if(metric_id == ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_maxNumerator;} */
  else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_mul) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_mul;}
  else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_maxNumerator;}
#endif
  else if(metric_id == e_kt_correlationFunction_groupOf_minkowski_euclid) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid;}
  else if(metric_id == e_kt_correlationFunction_groupOf_minkowski_cityblock) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_citbyblock;}
  else if(metric_id == e_kt_correlationFunction_groupOf_absoluteDifference_Canberra) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_absoluteDifference_Canberra;}
  else if(metric_id == e_kt_correlationFunction_groupOf_squared_Pearson) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}
  else if(metric_id == e_kt_correlationFunction_groupOf_MINE_mic) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O3_MINE_mic;}
  //else if(metric_id == ) {return ;}
  //! -------------------------------------------------------------------------------------------------------------------------
  else if(metric_id <= e_kt_correlationFunction_groupOf_rank_kendall_Dice) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_kendall_coVariance;}  
  else if(metric_id <= e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid;}
  // else if(metric_id <= e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable) {return ;}  
  else if(metric_id <= e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}
  //! -------------------------------------------------------------------------------------------------------------------------
  else if(metric_id <= e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}  
  else if(metric_id <= e_kt_correlationFunction_groupOf_shannon_JensenDifference) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin) {return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson;}  
  //! -------------------------------------------------------------------------------------------------------------------------
  //! -------------------------------------------------------------------------------------------------------------------------
  //else { //if(metric_id <= ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O3_MINE_mic;}
  else {
    return e_kt_correlationMetric_initCategory_timeComplexitySet__small_O3_MINE_mic;
    //return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef; //! ie, as the metic was Not defined
  }
  /* if(obj.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) { */
  /*   if(metric_id ==  */
  /* } */
}

//! @reutnr a correlation-object which describes differnet 'default test-metrics' in our "e_kt_correlationMetric_initCategory_timeComplexitySet__small_t"
static s_kt_correlationMetric_t getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_t enum_id) {
  s_kt_correlationMetric_t obj;setTo_default__s_kt_correlationMetric_t(&obj);
  //! -------------
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
  if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_avg) {obj.metric_id = e_kt_correlationFunction_groupOf_directScore_direct_avg;;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_mul) {obj.metric_id = e_kt_correlationFunction_groupOf_directScore_direct_mul;;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O0_maxNumerator) {obj.metric_id = e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator;;}
  else 
#endif
  //! ------------ 
   if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid) {obj.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_citbyblock) {obj.metric_id = e_kt_correlationFunction_groupOf_minkowski_cityblock;;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_absoluteDifference_Canberra) {obj.metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_Canberra;;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_squared_Pearson) {obj.metric_id = e_kt_correlationFunction_groupOf_squared_Pearson;;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman) {       obj.metric_id = e_kt_correlationFunction_groupOf_squared_Pearson;     /*Set the rank-attribute: */  obj.typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_rank;;}
  //! ------------ 
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_kendall_coVariance) {obj.metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; /*Set the rank-attribute: */  obj.typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_rank;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__small_O3_MINE_mic) {obj.metric_id = e_kt_correlationFunction_groupOf_MINE_mic;;}
  else {;} // ie, as we then need to add support 'for this'.  
  //!
  //! @return
  return obj;
}


/**
   @enum e_kt_correlationMetric_initCategory_timeComplexitySet__medium
   @brief provide a categorization of the different correlation-metrics wrt. their exeuction-time (oekseth, 06. feb. 2017).
   @remarks exktends the "e_kt_correlationMetric_initCategory" to simplify testing and comparison of popular corrleation-metrics
 **/
typedef enum e_kt_correlationMetric_initCategory_timeComplexitySet__medium {
  /* e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_avg, //! "e_kt_correlationFunction_groupOf_directScore_direct_avg" */
  /* e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_mul, //! "e_kt_correlationFunction_groupOf_directScore_direct_mul" */
  /* e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_maxNumerator, //! "e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator" */
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_euclid, //! "e_kt_correlationFunction_groupOf_minkowski_euclid"
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_citbyblock, //! "e_kt_correlationFunction_groupOf_minkowski_cityblock"
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference_Canberra, //! "e_kt_correlationFunction_groupOf_absoluteDifference_Canberra"
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared_Pearson, //! "e_kt_correlationFunction_groupOf_squared_Pearson"
  //! -------------------------------------------------------------------------
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_intersection,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_innerProduct,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_fidelity,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_shannon,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_combinations,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_downwardMutability,
  //! ---------------------------------------------------------------------------------
  /* //! ----------- */
  /* e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1, */
  //! -----------
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_spearman, //! 'rank' and "e_kt_correlationFunction_groupOf_squared_Pearson"
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall_coVariance,
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O3_MINE_mic, //! "e_kt_correlationFunction_groupOf_MINE_mic"
  e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef, 
} e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t;

//! @return a string-represnetioant of the "e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t" enum
static const char *getStringOf__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(const e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t enum_id) {
  /* if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_avg) {return "direct--avg";} */
  /* else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_mul) {return "";} */
  /* else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_maxNumerator) {return "O0--direct--maxNumerator";} */
  /* else  */
  if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski) {return "O1--Minkowski";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_euclid) {return "O1--Minkowski--Euclid";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_citbyblock) {return "O1--Minkowski--Citbyblock";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference) {return "O1--absoluteDifference";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference_Canberra) {return "O1--absoluteDifference--Canberra";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared) {return "O1--Squared";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared_Pearson) {return "O1--Squared--Pearson";}
  //! -------------------------------------------------------------------------------------------------------------------------
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_intersection) {return "O1--intersection";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_innerProduct) {return "O1--innerProduct";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_fidelity) {return "O1--fidelity";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_shannon) {return "O1--Shannon";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_combinations) {return "O1--combinations";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_downwardMutability) {return "O1--downwardMutability";}
  //! ---------------------------------------------------------------------------------------------------------------------------------------
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_spearman) {return "O2--Spearman";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall) {return "O2--Kendall";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall_coVariance) {return "O2--Kendall--coVariance";}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O3_MINE_mic) {return "O3--MINE--Mic";}
  else {return "<undefined>";} // ie, as we then need to add support 'for this'.  
}

//! @reutnr a correlation-object which describes differnet 'default test-metrics' in our "e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t" (eosketh, 06. feb. 2017)
static s_kt_correlationMetric_t getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(const e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t enum_id) {
  s_kt_correlationMetric_t obj;setTo_default__s_kt_correlationMetric_t(&obj);
  if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski) {obj.metric_id = e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_euclid) {obj.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_citbyblock) {obj.metric_id = e_kt_correlationFunction_groupOf_minkowski_cityblock;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference) {obj.metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference_Canberra) {obj.metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_Canberra;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared) {obj.metric_id = e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared_Pearson) {obj.metric_id = e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute;}
  //! -------------------------------------------------------------------------------------------------------------------------
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_intersection) {obj.metric_id = e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_innerProduct) {obj.metric_id = e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_fidelity) {obj.metric_id = e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_shannon) {obj.metric_id = e_kt_correlationFunction_groupOf_shannon_JensenDifference;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_combinations) {obj.metric_id = e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_downwardMutability) {obj.metric_id = e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin;}
  //! ---------------------------------------------------------------------------------------------------------------------------------------
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_spearman) {obj.metric_id = e_kt_correlationFunction_groupOf_squared_Pearson; obj.typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_rank;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall) {obj.metric_id = e_kt_correlationFunction_groupOf_rank_kendall_Dice;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall_coVariance) {obj.metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance;}
  else if(enum_id == e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O3_MINE_mic) {obj.metric_id = e_kt_correlationFunction_groupOf_MINE_mic;}
  //else {return "<undefined>";} // ie, as we then need to add support 'for this'.  

  //! @return the result:
  return obj;
}

/**
   @brief relates the correlation-metric to the 'medium-sized group' of simlairty-emtirc-cateogires (oekseth, 06. feb. 2017).
   @return the e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t 'classifiying' a given correlation/simlairty metric. 
 **/
static e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(const s_kt_correlationMetric_t obj) {
  const e_kt_correlationFunction_t metric_id = obj.metric_id;
  //! ----------
  if( (metric_id == e_kt_correlationFunction_groupOf_squared_Pearson) && (obj.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_spearman;}
  /* if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_avg) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_avg;} */
  /* else if(metric_id == ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_mul;} */
  /* else if(metric_id == ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O0_maxNumerator;} */
  else if(metric_id == e_kt_correlationFunction_groupOf_minkowski_euclid) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_euclid;}
  else if(metric_id == e_kt_correlationFunction_groupOf_minkowski_cityblock) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski_citbyblock;}
  else if(metric_id == e_kt_correlationFunction_groupOf_absoluteDifference_Canberra) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference_Canberra;}
  else if(metric_id == e_kt_correlationFunction_groupOf_squared_Pearson) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared_Pearson;}
  else if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_coVariance) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall_coVariance;}  
  //! -------------------------------------------------------------------------------------------------------------------------
  else if(metric_id <= e_kt_correlationFunction_groupOf_rank_kendall_Dice) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O2_kendall;}  
  else if(metric_id <= e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_minkowski;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_absoluteDifference;}  
  else if(metric_id <= e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_intersection;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_innerProduct;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_fidelity;}
  //! -------------------------------------------------------------------------------------------------------------------------
  else if(metric_id <= e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_squared;}  
  else if(metric_id <= e_kt_correlationFunction_groupOf_shannon_JensenDifference) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_shannon;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_combinations;}
  else if(metric_id <= e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O1_downwardMutability;}  
  //! -------------------------------------------------------------------------------------------------------------------------
  //! -------------------------------------------------------------------------------------------------------------------------
  //else { //if(metric_id <= ) {return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O3_MINE_mic;}
  else {
    return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_O3_MINE_mic;
    //return e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef; //! ie, as the metic was Not defined
  }
  /* if(obj.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) { */
  /*   if(metric_id ==  */
  /* } */
}

/* //! @reutnr a correlation-object which describes differnet 'default test-metrics' in our "e_kt_correlationMetric_initCategory_timeComplexitySet__small_t" */
/* static s_kt_correlationMetric_t getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_t enum_id) { */
/*   s_kt_correlationMetric_t obj;setTo_empty__s_kt_correlationMetric_t(&obj); */

/* //! Intaite the s_kt_correlationMetric_t object */
/* static void init__s_kt_correlationMetric_t(s_kt_correlationMetric_t *self, const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) { */
/*   // assert(self); */
/*   self->metric_id = metric_id; */
/*   self->typeOf_correlationPreStep = typeOf_correlationPreStep; */
/* } */

//! @return true if the metric is epxected to be sued for valeus in range [0...1], ie, for "gemoetric mean" (oekseth, 06. okt. 2016).
static bool describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(const e_kt_correlationFunction_t metric_id) {
  return (
	  (metric_id == e_kt_correlationFunction_groupOf_intersection_intersection_altDef)
	  || (metric_id == e_kt_correlationFunction_groupOf_fidelity_Hellinger) //! where we for the latter 'have' "2*sqrt(1 - sum(..))"
	  || (metric_id == e_kt_correlationFunction_groupOf_fidelity_Matusita) //! where we for the latter 'have' "sqrt(2 - 2*sum(..))"
	  //|| (metric_id == ) //! where we for the latter 'have'  ...
	  );
}



/* //! If a string-inptu maches the given  */
/* static void initFromString__s_kt_correlationMetric_t(s_kt_correlationMetric_t *self, const char *string) { */


/* } */



#endif //! EOF
