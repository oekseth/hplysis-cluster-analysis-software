#include "db_ds_directMapping.h"

//! Set the object to empty:
s_db_ds_directMapping_t setToEmpty__s_db_ds_directMapping_t() {
  s_db_ds_directMapping_t self;
  self.listOf_rel = NULL; 
  self.listOf_rel_size = 0;
  self.listOf_rel_size__currPos = 0;
  setTo_Empty__s_kt_set_2dsparse_t(&(self.mapOf_vertexTo_tailRel)); 
  setTo_Empty__s_kt_set_2dsparse_t(&(self.mapOf_vertexFrom_tail)); 
  //! ----------------------
  self.cnt_head = 0;
  self.map__size = 0;
  self.map_syn_head = NULL;
  self.map_syn_rt = NULL;
  self.map_syn_tail = NULL;
  //! ----------------------
  self.objInverse__isAllocated = false;
  //! @return
  return self;
}

//! Intiate the s_db_ds_directMapping_t object
s_db_ds_directMapping_t init__s_db_ds_directMapping_t(const uint cnt_head, const uint cnt_relations, const bool config__use2dList__toImproveAccessSpeed, const bool config__use2dList__toImproveAccessSpeed__alsoForTailToHead, const bool config__useExplcitSynbMappingTable, const bool config__allcoateInverse, const bool config__useBTree__forRelations) {
  assert(cnt_head > 0); assert(cnt_relations > 0);
  s_db_ds_directMapping_t self;
  self.listOf_rel_size = cnt_relations;
  self.listOf_rel_size__currPos = 0;
  self.treeOf_relations__isUsed = false;
  self.cnt_head = cnt_head;
  self.listOf_rel = NULL;
  if(config__useBTree__forRelations == false) {
    self.listOf_rel = alloc_generic_type_1d_xmtMemset(s_db_rel, self.listOf_rel, cnt_relations);
    for(uint rel_id = 0; rel_id < self.listOf_rel_size; rel_id++) {
      self.listOf_rel[rel_id] = __M__init__s_db_rel_t();
    }
  } else {
    self.treeOf_relations = init__s_db_ds_bTree_rel_t(/*typeOf_tree=*/e_db_ds_bTree_cntChildren_32);
    self.treeOf_relations__isUsed = true;
  }
  setTo_Empty__s_kt_set_2dsparse_t(&(self.mapOf_vertexTo_tailRel)); 
  setTo_Empty__s_kt_set_2dsparse_t(&(self.mapOf_vertexFrom_tail)); 
  if(config__use2dList__toImproveAccessSpeed) {
    allocate__s_kt_set_2dsparse_t(&(self.mapOf_vertexTo_tailRel), cnt_head, 100, NULL, NULL, false, /*isTo_allocateFor_scores=*/false);
  }
  if(config__use2dList__toImproveAccessSpeed__alsoForTailToHead) {
    allocate__s_kt_set_2dsparse_t(&(self.mapOf_vertexFrom_tail), cnt_head, 100, NULL, NULL, false, /*isTo_allocateFor_scores=*/false);
    assert(self.mapOf_vertexFrom_tail._nrows);
    // assert(false); // FIXME: remove.
  }
  self.map__size = 0;
  if(config__useExplcitSynbMappingTable) {
    const uint empty_0 = 0;
    self.map__size = cnt_head;
    self.map_syn_head = allocate_1d_list_uint(cnt_head, empty_0);
    self.map_syn_rt = allocate_1d_list_uint(cnt_head, empty_0);
    self.map_syn_tail = allocate_1d_list_uint(cnt_head, empty_0);
    for(uint i = 0; i < cnt_head; i++) {
      self.map_syn_tail[i] = self.map_syn_rt[i] = self.map_syn_head[i] = UINT_MAX;
    }
  } else {
    self.map_syn_head = NULL;
    self.map_syn_rt = NULL;
    self.map_syn_tail = NULL;
  }
  self.objInverse__isAllocated = false;
  if(config__allcoateInverse) {
    self.objInverse__isAllocated = true;
    const uint size_obj = 1;
    self.objInverse = alloc_generic_type_1d_xmtMemset(s_db_ds_directMapping_t, self.objInverse, size_obj);
    *self.objInverse = init__s_db_ds_directMapping_t(cnt_head, cnt_relations, config__use2dList__toImproveAccessSpeed, config__use2dList__toImproveAccessSpeed__alsoForTailToHead, /*config__useExplcitSynbMappingTable=*/false, /*config__allcoateInverse=*/false, config__useBTree__forRelations); //! where latter vairables is alwyas set to false as we expect 'this rotuine' to handle 'these cases'.

    // assert(false); // FIXME: remove
  }
  //! @return
  return self;
}


//! De-allcoate the object.
void free__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self) {
  assert(self);
  if(self->listOf_rel) {
    free_generic_type_1d((self->listOf_rel)); self->listOf_rel = NULL; 
    self->listOf_rel_size = 0;
  } else if(self->treeOf_relations__isUsed) {
    free__s_db_ds_bTree_rel_t(&(self->treeOf_relations));
    self->treeOf_relations__isUsed = false;
  }
  free_s_kt_set_2dsparse_t(&(self->mapOf_vertexTo_tailRel));
  free_s_kt_set_2dsparse_t(&(self->mapOf_vertexFrom_tail));
  if(self->map_syn_tail) {
    free_1d_list_uint(&(self->map_syn_head)); self->map_syn_head = NULL;
    free_1d_list_uint(&(self->map_syn_rt));   self->map_syn_rt = NULL;
    free_1d_list_uint(&(self->map_syn_tail)); self->map_syn_tail = NULL;
  }
  if(self->objInverse__isAllocated) {
    free__s_db_ds_directMapping_t((self->objInverse));
    free_generic_type_1d((self->objInverse));
    self->objInverse__isAllocated = false;
  }
}

//! Insert a relationship, using lgocis configured in the init-fucntion of our s_db_ds_directMapping_t self object (oekseth, 06. mar. 2017).
bool insertRelation__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const s_db_rel_t relation_inp) {
  assert(self);
  assert(relation_inp.head != UINT_MAX);
  assert(relation_inp.tailRel.tail != UINT_MAX);  
  //! 
  //! Note we do Not add explcit support for 'synonym-handling' as we assume taht our 'searhc-oepration' will -support this', ie, to reflect common data-base-appraoches.
  s_db_rel_t relation = relation_inp;
  if(self->map_syn_head) { //! then we udpate wrt. the synonyms 'before inserting':
    //!
    //! Then we use a 'direct mapping':
#include "db_ds_directMapping__get__synMappings_fast.c"
  }
  //!
  //! Insert the relaton:
  if(self->treeOf_relations__isUsed == true) { //! ie, then insert the relationship 'into'/using a B-tree:
    insert__s_db_ds_bTree_rel_t(&(self->treeOf_relations), relation);
    return true;
  } 
  const uint insertedAtPos = self->listOf_rel_size__currPos;
  assert(insertedAtPos < self->listOf_rel_size);
  self->listOf_rel[insertedAtPos] = relation;
  //!
  //! Update mapping-table:
  if(self->mapOf_vertexTo_tailRel._nrows > 0) { //! then we use a mapping-table to increase searh-perfomrance.
    assert(relation.head < __M__specialValue__predicate_synonym);
    assert(relation.head != UINT_MAX);
    assert(relation.head < self->mapOf_vertexTo_tailRel._nrows); //! ie, as the object-cofniguraiotn is otherwise errnous.
    push__s_kt_set_2dsparse_t(&(self->mapOf_vertexTo_tailRel), /*row=*/relation.head, /*internal-index=*/insertedAtPos);
  }
  if(self->mapOf_vertexFrom_tail._nrows > 0) { //! then we use a mapping-table to increase searh-perfomrance.
    //! Whichs is used to provide/add 'optinal support' for 'also' storing/usiong a 'tail-mapping' data-set:
    assert(relation.head < __M__specialValue__predicate_synonym);
    assert(relation.tailRel.tail < self->mapOf_vertexFrom_tail._nrows); //! ie, as the object-cofniguraiotn is otherwise errnous.
    push__s_kt_set_2dsparse_t(&(self->mapOf_vertexFrom_tail), /*row=*/relation.tailRel.tail, /*internal-index=*/insertedAtPos);
  }
  //!
  //! Finalize:
  self->listOf_rel_size__currPos++;
  if(self->objInverse__isAllocated) { //! Then we invert the relation, and insert:
    const s_db_rel_t relation_inv = __M__getInverse__s_db_rel_t(relation);
    const bool is_ok = insertRelation__s_db_ds_directMapping_t(self->objInverse, relation_inv);
    assert(is_ok);
  }
  //! @return ok
  return true;
}

//! identify a list of internal indxes to interesting relations.
bool findRelation__cntInteresting__s_db_ds_directMapping_t(const s_db_ds_directMapping_t *self, const s_db_rel_t relation_inp, uint *scalar_cntInteresting, const bool namesAlreadyMappedIntoSynonyms, s_kt_set_1dsparse_t *result_set, const bool isToStore__verticesInResultStack) {
  assert(self);
  //s_kt_set_1dsparse_t result_set; allocate__s_kt_set_1dsparse_t(&result_set, /*ncols=*/100, NULL, NULL, 
  //*scalar_cntInteresting = 0;

  assert(false == __M__allValuesInObj__hasArbitraryClauses__s_db_rel_t(relation_inp)); //! ie, as otehrise we woudl have a 'pointless' search-operation.
  //__MF__print__stdOut__s_db_rel(relation_inp);
  
  s_db_rel_t relation = relation_inp;
  if(namesAlreadyMappedIntoSynonyms == false) {
    assert(false); // FIXME: remove.
    if(self->map_syn_head) {
      //!
      //! Then we use a 'direct mapping':
#include "db_ds_directMapping__get__synMappings_fast.c"
      //! ---
    } else { //! then we need to 'idneityf lists of synonym-candidates' .... and then 'make seperate calls for the combiantions' using a 3d-for-loop

      // assert(false); // FIXME: remove.

      s_kt_set_1dsparse_t relId__head; allocate__s_kt_set_1dsparse_t(&relId__head, /*ncols=*/10, NULL, NULL, false, /*isto_allocatefor_scores=*/false);
      s_kt_set_1dsparse_t relId__rt; allocate__s_kt_set_1dsparse_t(&relId__rt, /*ncols=*/10, NULL, NULL, false, /*isto_allocatefor_scores=*/false);
      s_kt_set_1dsparse_t relId__tail; allocate__s_kt_set_1dsparse_t(&relId__tail, /*ncols=*/10, NULL, NULL, false, /*isto_allocatefor_scores=*/false);
      const bool locaSyn__isToStore__verticesInResultStack = true;
      { //! For: heads:
	push__s_kt_set_1dsparse_t(&relId__head, relation.head); //! ie, set 'default value'
	if(relation.head < (__M__wildCard__s_db_rel_t)) {
	  //!
	  if(relation.head != __M__wildCard__s_db_rel_t) 
	    {//! Updete the relation:
	    s_db_rel_t rel_syn = __M__initWildCard__s_db_rel_t(); 
	    rel_syn.head = relation.head;
	    rel_syn.tailRel.rt = __M__specialValue__predicate_synonym; //! ie, the 'key' in this search
	    rel_syn.tailRel.tail = __M__wildCard__s_db_rel_t; //! ie, to 'search for call cases where the ehad-vertex matches'.
	    //! Apply:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, rel_syn, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, &relId__head, locaSyn__isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);	  
	  }
	  if(relation.head != __M__wildCard__s_db_rel_t) 
	  {//! Then for the 'inverse':
	    s_db_rel_t rel_syn = __M__initWildCard__s_db_rel_t(); 
	    rel_syn.head = __M__wildCard__s_db_rel_t; //! ie, to 'search for call cases where the ehad-vertex matches'.
	    rel_syn.tailRel.rt = __M__specialValue__predicate_synonym; //! ie, the 'key' in this search
	    rel_syn.tailRel.tail = relation.head;
	    //! Apply:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, rel_syn, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, &relId__head, locaSyn__isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);	  
	  }	
	}
      }  
      //! ------------------------------------------------
      { //! For: rts:
	push__s_kt_set_1dsparse_t(&relId__rt, relation.tailRel.rt); //! ie, set 'default value'
	if(relation.tailRel.rt < (__M__wildCard__s_db_rel_t)) {
	  //!
	  {//! Updete the relation:
	    s_db_rel_t rel_syn = __M__initWildCard__s_db_rel_t(); 
	    rel_syn.head = relation.tailRel.rt;
	    rel_syn.tailRel.rt = __M__specialValue__predicate_synonym; //! ie, the 'key' in this search
	    rel_syn.tailRel.tail = __M__wildCard__s_db_rel_t; //! ie, to 'search for call cases where the ehad-vertex matches'.
	    //! Apply:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, rel_syn, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, &relId__rt, locaSyn__isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);	  
	  }
	  {//! Then for the 'inverse':
	    s_db_rel_t rel_syn = __M__initWildCard__s_db_rel_t(); 
	    rel_syn.head = __M__wildCard__s_db_rel_t; //! ie, to 'search for call cases where the ehad-vertex matches'.
	    rel_syn.tailRel.rt = __M__specialValue__predicate_synonym; //! ie, the 'key' in this search
	    rel_syn.tailRel.tail = relation.tailRel.rt;
	    //! Apply:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, rel_syn, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, &relId__rt, locaSyn__isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);	  
	  }	
	}
      }  
      //! ------------------------------------------------
      { //! For: tails:
	push__s_kt_set_1dsparse_t(&relId__tail, relation.tailRel.tail); //! ie, set 'default value'
	if(relation.tailRel.tail < (__M__wildCard__s_db_rel_t)) {
	  //!
	  {//! Updete the relation:
	    s_db_rel_t rel_syn = __M__initWildCard__s_db_rel_t(); 
	    rel_syn.head = relation.tailRel.tail;
	    rel_syn.tailRel.rt = __M__specialValue__predicate_synonym; //! ie, the 'key' in this search
	    rel_syn.tailRel.tail = __M__wildCard__s_db_rel_t; //! ie, to 'search for call cases where the ehad-vertex matches'.
	    //! Apply:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, rel_syn, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, &relId__tail, locaSyn__isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);	  
	  }
	  {//! Then for the 'inverse':
	    s_db_rel_t rel_syn = __M__initWildCard__s_db_rel_t(); 
	    rel_syn.head = __M__wildCard__s_db_rel_t; //! ie, to 'search for call cases where the ehad-vertex matches'.
	    rel_syn.tailRel.rt = __M__specialValue__predicate_synonym; //! ie, the 'key' in this search
	    rel_syn.tailRel.tail = relation.tailRel.tail;
	    //! Apply:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, rel_syn, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, &relId__tail, locaSyn__isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);	  
	  }	
	}
      }  
      //! ------------------------------------------------    
      //!
      //! Then we need to 'sort', ie, to 'avoid' any 'non-uqniue cases' from resulting in muliple search-calls for t he same synonym-combiantion:
      const uint empty_0 = 0;

      //!
      //! Then sort the result for head:
      uint arr_mappings_head_size = 0;
      const uint *arr_mappings_head = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&relId__head, &arr_mappings_head_size);
      uint *arr_sorted_head = allocate_1d_list_uint(arr_mappings_head_size, empty_0);
      uint *arr_tmp_head = allocate_1d_list_uint(arr_mappings_head_size, empty_0);
      quicksort_uint(arr_mappings_head_size, arr_mappings_head, arr_tmp_head, arr_sorted_head);
      //!
      //! Then sort the result for rt:
      uint arr_mappings_rt_size = 0;
      const uint *arr_mappings_rt = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&relId__rt, &arr_mappings_rt_size);
      uint *arr_sorted_rt = allocate_1d_list_uint(arr_mappings_rt_size, empty_0);
      uint *arr_tmp_rt = allocate_1d_list_uint(arr_mappings_rt_size, empty_0);
      quicksort_uint(arr_mappings_rt_size, arr_mappings_rt, arr_tmp_rt, arr_sorted_rt);
      //!
      //! Then sort the result for tail:
      uint arr_mappings_tail_size = 0;
      const uint *arr_mappings_tail = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&relId__tail, &arr_mappings_tail_size);
      uint *arr_sorted_tail = allocate_1d_list_uint(arr_mappings_tail_size, empty_0);
      uint *arr_tmp_tail = allocate_1d_list_uint(arr_mappings_tail_size, empty_0);
      quicksort_uint(arr_mappings_tail_size, arr_mappings_tail, arr_tmp_tail, arr_sorted_tail);

      //! ------------------------------------------------    
      //!
      //! Search for the 'complete set of candidates':      
      uint prev_key_h = UINT_MAX;
      for(uint index_h = 0; index_h < arr_mappings_head_size; index_h++) {
	if(prev_key_h == arr_sorted_head[index_h]) {continue;} //! ie, as we have løaredy 'evlauated this case'.
	uint prev_key_rt = UINT_MAX;
	for(uint index_rt = 0; index_rt < arr_mappings_rt_size; index_rt++) {
	  uint prev_key_tail = UINT_MAX;
	  if(prev_key_rt == arr_sorted_rt[index_rt]) {continue;} //! ie, as we have løaredy 'evlauated this case'.
	  for(uint index_tail = 0; index_tail < arr_mappings_tail_size; index_tail++) {
	    if(prev_key_tail == arr_sorted_tail[index_tail]) {continue;} //! ie, as we have løaredy 'evlauated this case'.
	    prev_key_tail = arr_sorted_tail[index_tail];
	    //!
	    //! Updete the relation:
	    s_db_rel_t relation = __M__initWildCard__s_db_rel_t(); 
	    relation.head = arr_sorted_head[index_h];
	    relation.tailRel.rt = arr_sorted_rt[index_rt];
	    relation.tailRel.tail = arr_sorted_tail[index_tail];
	    //!
	    //! The call:
	    const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self, relation, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, result_set, isToStore__verticesInResultStack); //! where latter boolean parameteers ensures that we 'avoid' an infitniente loop.
	    assert(is_ok);
	  }
	  prev_key_rt = arr_sorted_rt[index_rt];
	}
	prev_key_h = arr_sorted_head[index_h];
      }
      
      //!
      //! De-allocatE:
      free_1d_list_uint(&arr_sorted_head);
      free_1d_list_uint(&arr_sorted_rt);
      free_1d_list_uint(&arr_sorted_tail);
      free_1d_list_uint(&arr_tmp_head);
      free_1d_list_uint(&arr_tmp_rt);
      free_1d_list_uint(&arr_tmp_tail);
      free_s_kt_set_1dsparse_t(&relId__head);
      free_s_kt_set_1dsparse_t(&relId__rt);
      free_s_kt_set_1dsparse_t(&relId__tail);
      //! 
      //! Complete:
      return true;
    }
  }
  //s_kt_set_1dsparse_t result_set; allocate__s_kt_set_1dsparse_t(&result_set, /*ncols=*/100, NULL, NULL, 

  //!
  //! Insert the relaton:
  if(self->treeOf_relations__isUsed == true) { //! ie, then insert the relationship 'into'/using a B-tree:
    s_db_rel_t relation_result = __M__init__s_db_rel_t();
    const bool isTo_inpsectAll = __M__hasArbitraryClauses__s_db_rel_t(relation);
    const uint cnt_found = find__s_db_ds_bTree_rel_t(&(self->treeOf_relations), relation, &relation_result, isTo_inpsectAll, result_set);
    /* //! Note: [”elow] asusmption is based on our sytnetic data-set-evlauation-cases, ie, that we for B-trees does Not inveitgate overhead wrt. eueciotn-time, ie, to simplify our test-logics (and the scope of the latter). */
    /* assert(result_set == NULL); //! ie, as we 'oterhwise' need to idneityf an 'alternative wrok-around' wrt. 'how to handle non-index-based relations'.  */
    //! ----------------------- 
    *scalar_cntInteresting += cnt_found;
    return true;
  } 

  //!
  //! Identify the tgype of optimized index-aaccess (if any) which may be used to fetch/retrive the relations:
  if( (false == __M__isWildCart__s_db_rel_t(relation.head) && self->mapOf_vertexTo_tailRel._nrows > 0) ) { //! then we use a mapping-table to increase searh-perfomrance.
    // printf("\t (search-has-Not-head-wildCard)\t at %s:%d\n", __FILE__, __LINE__);
    assert(self->listOf_rel);
    uint arrOf_ids_size = 0;
    const uint *arrOf_ids = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mapOf_vertexTo_tailRel), relation.head, &arrOf_ids_size);
    if(arrOf_ids_size && arrOf_ids) {
      for(uint i = 0; i < arrOf_ids_size; i++) {
	const uint rel_id = arrOf_ids[i];
	const s_db_rel_t rel = self->listOf_rel[rel_id];
	if(__M__isEqual__s_db_rel_t(relation, rel)) {
	  //! Then we have 'found' a given relationship:
	  *scalar_cntInteresting++;
	  if(result_set) {
	    if(isToStore__verticesInResultStack) {
	      uint searchId_head = UINT_MAX; uint searchId_tail = UINT_MAX;
	      MF__resultsToUSe__afterSearch__s_db_searchNode_rel(rel);
	      if(searchId_head != UINT_MAX) {
		push__s_kt_set_1dsparse_t(result_set, searchId_head);
	      }
	      if( (searchId_tail != UINT_MAX) && (searchId_tail != searchId_head) ) {
		push__s_kt_set_1dsparse_t(result_set, searchId_tail);
	      }
	    } else {
	      push__s_kt_set_1dsparse_t(result_set, rel_id);
	    }
	  }
	}
      }
    }
  } else if( (false == __M__isWildCart__s_db_rel_t(relation.tailRel.tail)) && (self->mapOf_vertexFrom_tail._nrows  || self->objInverse__isAllocated) ) { //! then we use a mapping-table to increase searh-perfomrance.
    // printf("\t (search-has-Not-tail-wildCard)\t at %s:%d\n", __FILE__, __LINE__);
    assert(self->listOf_rel);
    if(self->objInverse__isAllocated) { //! Then we invert the relation, and insert:
      //! Then we optimze access usign 'cocnetquative emmroy-access, ie, by using the 'inverse-insterted mapping-table':
      const s_db_rel_t relation_inv = __M__getInverse__s_db_rel_t(relation); //! ie, constrct an ivnerse 'version' of the relation.
      const bool is_ok = findRelation__cntInteresting__s_db_ds_directMapping_t(self->objInverse, relation_inv, scalar_cntInteresting, /*namesAlreadyMappedIntoSynonyms=*/true, result_set, isToStore__verticesInResultStack);
      assert(is_ok);
    } else { //! then we 'collect' a framgented list of relation-ids, ie, an appraoch expected to result in a high number/degree of memory-ache-misses.
      uint arrOf_ids_size = 0;
      const uint *arrOf_ids = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mapOf_vertexFrom_tail), relation.tailRel.tail, &arrOf_ids_size);
      if(arrOf_ids_size && arrOf_ids) {
	for(uint i = 0; i < arrOf_ids_size; i++) {
	  const uint rel_id = arrOf_ids[i];
	  const s_db_rel_t rel = self->listOf_rel[rel_id];
	  if(__M__isEqual__s_db_rel_t(relation, rel)) {
	    //! Then we have 'found' a given relationship:
	    *scalar_cntInteresting++;
	    if(result_set) {
	      if(isToStore__verticesInResultStack) {
		uint searchId_head = UINT_MAX; uint searchId_tail = UINT_MAX;
		MF__resultsToUSe__afterSearch__s_db_searchNode_rel(rel);
		if(searchId_head != UINT_MAX) {
		  push__s_kt_set_1dsparse_t(result_set, searchId_head);
		}
		if( (searchId_tail != UINT_MAX) && (searchId_tail != searchId_head) ) {
		  push__s_kt_set_1dsparse_t(result_set, searchId_tail);
		}
	      } else {
		push__s_kt_set_1dsparse_t(result_set, rel_id);
	      }
	    }
	  }
	}
      }
    }
  } else { //! else the inseriton godes lsiglty faster, through at the cost of considerably increase in searhc-time
    assert(self->listOf_rel);
    // printf("\t (iterate-through-all)\t tailIsWildCard=%s, at %s:%d\n",  (__M__isWildCart__s_db_rel_t(relation.tailRel.tail)) ? "true" : "false", __FILE__, __LINE__);
    // assert(false); // FIXME: remove
    for(uint rel_id = 0; rel_id < self->listOf_rel_size__currPos; rel_id++) {
      const s_db_rel_t rel = self->listOf_rel[rel_id];
      if(__M__isEqual__s_db_rel_t(relation, rel)) {
	//! Then we have 'found' a given relationship:
	*scalar_cntInteresting++;
	if(result_set) {
	  if(isToStore__verticesInResultStack) {
	    uint searchId_head = UINT_MAX; uint searchId_tail = UINT_MAX;
	    MF__resultsToUSe__afterSearch__s_db_searchNode_rel(rel);
	    if(searchId_head != UINT_MAX) {
	      push__s_kt_set_1dsparse_t(result_set, searchId_head);
	    }
	    if( (searchId_tail != UINT_MAX) && (searchId_tail != searchId_head) ) {
	      push__s_kt_set_1dsparse_t(result_set, searchId_tail);
	    }
	  } else {
	    push__s_kt_set_1dsparse_t(result_set, rel_id);
	  }
	}
      }
    }
  }
  //! @return ok
  return true;
}

//! Update the amping between a 'genrlaized vertex-name' vertex_norm and the 'sepcific' vertex_syn 'searchalbeæ' vertex-name
bool setMappingFor_head__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const uint vertex_norm, const uint vertex_syn) {
  assert(self);
  if(self->map_syn_head) {
    //! Update the mapping:
    /* assert(vertex_norm < self->mapOf_vertexTo_tailRel._nrows); */
    /* assert(vertex_syn < self->mapOf_vertexTo_tailRel._nrows); */
    /* assert(self->map_syn_head[vertex_syn] == UINT_MAX); //! ie, as we expect latter to Not have been set. */
    self->map_syn_head[vertex_syn] = vertex_norm; //! ie, the mapping.
  } else { //! Then we 'insert the mapping into the relation-table:
    const bool is_ok = insertRelation__s_db_ds_directMapping_t(self, init_synonym(vertex_norm, vertex_syn)); assert(is_ok);
  }
  //! @return
  return true;
}

//! Update the amping between a 'genrlaized vertex-name' vertex_norm and the 'sepcific' vertex_syn 'searchalbeæ' vertex-name
bool setMappingFor_rt__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const uint vertex_norm, const uint vertex_syn) {
  assert(self);
  if(self->map_syn_head) {
    /* assert(vertex_norm < self->mapOf_vertexTo_tailRel._nrows); */
    /* assert(vertex_syn < self->mapOf_vertexTo_tailRel._nrows); */
    /* //! Update the mapping: */
    /* assert(self->map_syn_rt[vertex_syn] == UINT_MAX); //! ie, as we expect latter to Not have been set. */
    self->map_syn_rt[vertex_syn] = vertex_norm; //! ie, the mapping.
  } else { //! Then we 'insert the mapping into the relation-table:
    const bool is_ok = insertRelation__s_db_ds_directMapping_t(self, init_synonym(vertex_norm, vertex_syn)); assert(is_ok);
  }
  //! @return
  return true;
}

//! Update the amping between a 'genrlaized vertex-name' vertex_norm and the 'sepcific' vertex_syn 'searchalbeæ' vertex-name
bool setMappingFor_tail__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const uint vertex_norm, const uint vertex_syn) {  
  assert(self);
  if(self->map_syn_head) {
    assert(self);
    assert(self->map_syn_tail);
    /* assert(vertex_norm < self->mapOf_vertexTo_tailRel._nrows); */
    /* assert(vertex_syn < self->mapOf_vertexTo_tailRel._nrows); */
    //! Update the mapping:
    // assert(self->map_syn_tail[vertex_syn] == UINT_MAX); //! ie, as we expect latter to Not have been set.
    self->map_syn_tail[vertex_syn] = vertex_norm; //! ie, the mapping.
  } else { //! Then we 'insert the mapping into the relation-table:
    const bool is_ok = insertRelation__s_db_ds_directMapping_t(self, init_synonym(vertex_norm, vertex_syn)); assert(is_ok);
  }
  //! @return
  return true;
}

//! Apply a recusive search for the vertex:
static void  __rescursiveSearchFor__nestedSet__s_db_ds_directMapping_t(const s_db_ds_directMapping_t *self, s_db_searchResult_rel_t *searchResult, const bool isTo__applyPreStep, const uint vertex_id__input) {

  uint vertex_id = vertex_id__input;
  
  const bool config__isTo__performRecusiveSearch = true; //! whiuch is sued to ivnestigate the time-fporoamcne-benfit of using a 'stack' instead of a recusive funciton-call.
  const bool config__isTo__useSortedPopertyForInsertion = false;
  uint *stackIdOf__vertex = NULL;
  uint *stackIdOf__caller = NULL; uint stackIdOf__pos = 0; uint stackIdOf__size = 0;
  const uint empty_0 = 0;
  char *vertexIs_addedToStack = allocate_1d_list_char(self->cnt_head, empty_0);
  if(config__isTo__performRecusiveSearch) {
    assert(self->listOf_rel_size > 0);
    stackIdOf__size = self->listOf_rel_size;
    stackIdOf__vertex = allocate_1d_list_uint(self->listOf_rel_size, empty_0);
    stackIdOf__caller = allocate_1d_list_uint(self->listOf_rel_size, empty_0);
    for(uint i = 0; i < self->listOf_rel_size; i++) {
      stackIdOf__caller[i] = stackIdOf__vertex[i] = UINT_MAX;
    }
    //! 
    //! 'Set' the first vertex:
    stackIdOf__vertex[0] = vertex_id;
    stackIdOf__caller[0] = UINT_MAX;
    stackIdOf__pos++;
  }

  //!
  //! Iterate through the stack of vertices:
  do {
    const uint idOf_caller = (config__isTo__performRecusiveSearch) ? stackIdOf__caller[stackIdOf__pos] : UINT_MAX;
    if(config__isTo__performRecusiveSearch) {
      assert(stackIdOf__pos > 0);
      stackIdOf__pos--;
      vertex_id = stackIdOf__vertex[stackIdOf__pos];
    }
    assert(searchResult->mapOf_visistedVertices.list_size != 0);
    assert(searchResult->mapOf_visistedVertices.list_size >= self->cnt_head);
    if(vertex_id != UINT_MAX) {
      assert(vertex_id < searchResult->mapOf_visistedVertices.list_size);
      assert(vertex_id < self->cnt_head);
    }
    
    if(!config__isTo__performRecusiveSearch && (searchResult->result_cnt_nodesExpanded >= searchResult->config.threshold__scalar__cntVerticesToInvestigate__max)) {return;} //! ie, as we have then 'reached' the max-number-of-vertices' to investigate.
    else if(config__isTo__performRecusiveSearch && (searchResult->result_cnt_nodesExpanded >= searchResult->config.threshold__scalar__cntVerticesToInvestigate__max)) {continue;} //! ie, as we have then 'reached' the max-number-of-vertices' to investigate.
    
    if((vertex_id == UINT_MAX) || (false == MF__isVisited__s_db_searchResult_rel(searchResult, vertex_id))) {
      bool __startRecursion = true;
      if(vertex_id != UINT_MAX) {
	searchResult->result_cnt_nodesExpanded++; //! ie, increment the count 'of unqiue vertices which have been expanded'.
      }   
      if(isTo__applyPreStep) {
	assert(vertex_id != UINT_MAX);
	__startRecursion = false; 
	uint arrOf_search_size = 0;
	s_db_rel_t *arrOf_search = getSearchNodes__s_db_searchNode_rel_t(searchResult->config.search_preSelectionCriteria, /*vertex=*/vertex_id, &arrOf_search_size, /*isTo__applyRecursiveCall=*/false);
	printf("\t\t evalautes cntSearchTripletes=%u, at %s:%d\n", arrOf_search_size, __FILE__, __LINE__);
	if(arrOf_search_size > 0) {
	  assert(arrOf_search);
	  //if(vertex_id != UINT_MAX) 
	  assert(vertex_id != UINT_MAX);
	  { //! Then 'it is enough' to see if the vertex is of interest:
	    uint cnt_interesting = 0;
	    for(uint i = 0; (i < arrOf_search_size) && (cnt_interesting == 0); i++) {
	      s_db_rel_t relation_inp = arrOf_search[i];
	      findRelation__cntInteresting__s_db_ds_directMapping_t(self, relation_inp, &cnt_interesting, /*namesAlreadyMappedIntoSynonyms=*/(false == searchResult->config.configLogics__isToApply__synonymMappings__inFunction), NULL, /*isToStore__verticesInResultStack=*/true);
	    }
	    if(cnt_interesting > 0) {
	      const uint searchId_head = vertex_id;
	      const bool isTo_traverse = MF__notOfInterest__updateMarkIfNotAlready__s_db_searchResult_rel(searchResult, searchId_head);
	      __startRecursion = isTo_traverse;
	    }
	    /* } else { //! then we 'also need' to idneitfy the given vertex: */
	    /* } */
	  }
	  //!
	  //! De-allocate:
	  //free_s_kt_set_1dsparse_t(&result_set);
	  if(arrOf_search) {__MF__free__s_db_rel(arrOf_search);} //! ie, then de-allocate the latter.
	}
      }
      if(__startRecursion && searchResult->config.search_inRecursive) { //! then we 'apply' a recusive search:
	uint arrOf_search_size = 0;
	s_db_rel_t *arrOf_search = getSearchNodes__s_db_searchNode_rel_t(searchResult->config.search_inRecursive, /*vertex=*/vertex_id, &arrOf_search_size, /*isTo__applyRecursiveCall=*/true);
	//printf("\t\t [recusriveStep]\t evalautes cntSearchTripletes=%u, at %s:%d\n", arrOf_search_size, __FILE__, __LINE__);
	assert(arrOf_search_size > 0); // TODO: validate this assumption ... whci 'is expected to become wrong' when we increse the search-compelxity of ouyr "db_searchNode_rel.h" API.
	if(arrOf_search_size > 0) {
	  assert(arrOf_search);
	  s_kt_set_1dsparse_t result_set; allocate__simple__s_kt_set_1dsparse_t(&result_set, /*ncols=*/1000); 
	  for(uint i = 0; i < arrOf_search_size; i++) {
	    uint cnt_interesting = 0;
	    s_db_rel_t relation_inp = arrOf_search[i];
	    findRelation__cntInteresting__s_db_ds_directMapping_t(self, relation_inp, &cnt_interesting, /*namesAlreadyMappedIntoSynonyms=*/(false == searchResult->config.configLogics__isToApply__synonymMappings__inFunction), &result_set, /*isToStore__verticesInResultStack=*/true);
	  }
	  uint arrOf_relationIds_size = 0;
	  const uint *arrOf_relationIds  = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&result_set, &arrOf_relationIds_size);
	  //const uint *arrOf_relationIds = arrOf_relationIds__base;

	  uint debug_cntCalled = 0;
	  if(arrOf_relationIds_size > 0) {
	    uint *arrOf_relationIds__sorted = NULL;
	    if(config__isTo__useSortedPopertyForInsertion) {
	      const uint empty_0 = 0;
	      uint *tmp_arr = allocate_1d_list_uint(arrOf_relationIds_size, empty_0);
	      arrOf_relationIds__sorted = allocate_1d_list_uint(arrOf_relationIds_size, empty_0);
	      quicksort_uint(arrOf_relationIds_size, /*input=*/arrOf_relationIds, tmp_arr, arrOf_relationIds__sorted);
	      free_1d_list_uint(&tmp_arr);
	      arrOf_relationIds = arrOf_relationIds__sorted;
	    }

	    t_float dist_child_max = T_FLOAT_MIN_ABS;
	    uint prev_id = UINT_MAX;
	    if(config__isTo__performRecusiveSearch) {

	      for(uint i = 0; i < arrOf_relationIds_size; i++) {
		const uint searchId_head = arrOf_relationIds[i];
		if(searchId_head == prev_id) {continue;} //! ie, as we 'then' have alreayd evlauated this case.
		assert(searchId_head < self->cnt_head);
		if(vertexIs_addedToStack[searchId_head]) {continue;}
		vertexIs_addedToStack[searchId_head] = 1; //! ie, as we 'add this' in [”elow]
		prev_id = searchId_head;
		assert(searchId_head != UINT_MAX);
		assert(searchId_head != (__M__wildCard__s_db_rel_t));
		if(false == MF__isVisited__s_db_searchResult_rel(searchResult, searchId_head)) { //! then the 'child' have not been ealirer visited.
		  //!
		  //! Then we add the child at the current psotion, and icnrement coutner:
		  assert((stackIdOf__pos + 1) < stackIdOf__size);
		  stackIdOf__vertex[stackIdOf__pos] = searchId_head;
		  stackIdOf__caller[stackIdOf__pos] = vertex_id;
		  //! Increment:
		  stackIdOf__pos++;
		} 
		{ //! then update the distance:
		  t_float dist_child_local = 1;
		  if(vertex_id != UINT_MAX) {
		    dist_child_local = MF__getDistance__s_db_searchResult_rel(searchResult, vertex_id);
		    if(dist_child_local == T_FLOAT_MAX) {dist_child_local = 1;}
		    else {dist_child_local += 1;}  //! where '1' is expected tot be the 'defualt- relation-count between the vertices.
		  }
		  //if( (searchId_head != UINT_MAX)  && (vertex_id != UINT_MAX) ) {		  
		  // assert(false); 
		  // TODO[cocneptaul]: consider to vlaidate/describe why  [below] non-reusive proceudre produce the same result 'as our reusvie proceudre' .... and in this context ... update our non-recusive proceudr ein oru "db_ds_directMapping.c" ... figure out how ... to udpat ehte 'amx-idsiance if we do Not sue a reusviese prodcuere' ... (eosketh, 06. mar. 2017).
		  t_float dist_this = MF__getDistance__s_db_searchResult_rel(searchResult, searchId_head);
		  if(dist_this != T_FLOAT_MAX) {
		    if(dist_child_local > dist_this) { //! then we udpat 'this':		      
		      dist_this = dist_child_local;
		      MF__setDistance__s_db_searchResult_rel(searchResult, searchId_head, dist_this);		    
		    } //! else we 'assume the previous distance' describes/rpesentas a hibhger/better max-score.
		  } else {
		    MF__setDistance__s_db_searchResult_rel(searchResult, searchId_head, dist_this);
		  }		  
		}
	      }	      
	    } else {
	      for(uint i = 0; i < arrOf_relationIds_size; i++) {
		const uint searchId_head = arrOf_relationIds[i];
		if(searchId_head == prev_id) {continue;} //! ie, as we 'then' have alreayd evlauated this case.
		assert(searchId_head < self->cnt_head);
		if(vertexIs_addedToStack[searchId_head]) {continue;}
		vertexIs_addedToStack[searchId_head] = 1; //! ie, as we 'add this' in [”elow]
		prev_id = searchId_head;
		if(false == MF__isVisited__s_db_searchResult_rel(searchResult, searchId_head)) { //! then the 'child' have not been ealirer visited.
		  //! 'Apply' a recusive step/call:
		  __rescursiveSearchFor__nestedSet__s_db_ds_directMapping_t(self, searchResult, isTo__applyPreStep, searchId_head);
		  debug_cntCalled++;
		}
		const t_float dist_child_local = MF__getDistance__s_db_searchResult_rel(searchResult, searchId_head);
		if(dist_child_local != T_FLOAT_MAX) {dist_child_max = macro_max(dist_child_max, dist_child_local);}
	      }
	      //! Update the distance:
	      if(vertex_id != UINT_MAX) {
		if(dist_child_max != T_FLOAT_MIN_ABS) { //! then we 'udpat the dsitance of this':
		  t_float dist_this = MF__getDistance__s_db_searchResult_rel(searchResult, vertex_id);
		  if(dist_this != T_FLOAT_MAX) {dist_this += dist_child_max;}
		  MF__setDistance__s_db_searchResult_rel(searchResult, vertex_id, dist_this);
		}
	      }
	    }
	    if(arrOf_relationIds__sorted) {free_1d_list_uint(&arrOf_relationIds__sorted);}
	  }
	  // printf("\t\t [recusriveStep]\t cntMatches=%u, debug_cntCalled=%u, given our cntSearchTripletes=%u, at %s:%d\n", arrOf_relationIds_size, debug_cntCalled, arrOf_search_size, __FILE__, __LINE__);
	  //!
	  //! De-allocate:
	  free_s_kt_set_1dsparse_t(&result_set);
	  if(arrOf_search) {__MF__free__s_db_rel(arrOf_search);} //! ie, then de-allocate the latter.
	}
      }
    }  //! else then the ndoe has already been visisted, ie, to Not update the result    
  } while (stackIdOf__pos > 0);
  if(config__isTo__performRecusiveSearch) {
    assert(stackIdOf__caller);
    free_1d_list_uint(&stackIdOf__vertex);
    free_1d_list_uint(&stackIdOf__caller);
  }
  assert(vertexIs_addedToStack); free_1d_list_char(&vertexIs_addedToStack); vertexIs_addedToStack = NULL;
    //! @return
  //return MF__getDistance__s_db_searchResult_rel(searchResult, vertex_id);
}

//! Search for a set of nodes using a tree-traversal procedure (oekseth, 06. mar. 2017).
bool searchFor__nestedSet__s_db_ds_directMapping_t(const s_db_ds_directMapping_t *self, s_db_searchResult_rel_t *searchResult, const bool config__isTo_preCompute_preSearchConditions) {
  assert(self);
  assert(searchResult);
  assert(searchResult->mapOf_visistedVertices.list);
  assert(searchResult->mapOf_visistedVertices.list_size);
  //! ----------------------------------------------------------------------------------
  //! 
  if(searchResult->config.search_preSelectionCriteria) {
    if(config__isTo_preCompute_preSearchConditions && self->listOf_rel) { //! then we identify the 'complete set' of relationships
      uint arrOf_search_size = 0;
      s_db_rel_t *arrOf_search = getSearchNodes__s_db_searchNode_rel_t(searchResult->config.search_preSelectionCriteria, /*vertex=*/UINT_MAX, &arrOf_search_size, /*isTo__applyRecursiveCall=*/false);
      if(arrOf_search_size > 0) {
	assert(arrOf_search);
	s_kt_set_1dsparse_t result_set; allocate__simple__s_kt_set_1dsparse_t(&result_set, /*ncols=*/1000); 
	for(uint i = 0; i < arrOf_search_size; i++) {
	  uint cnt_interesting = 0;
	  s_db_rel_t relation_inp = arrOf_search[i];
	  findRelation__cntInteresting__s_db_ds_directMapping_t(self, relation_inp, &cnt_interesting, 
								/*namesAlreadyMappedIntoSynonyms=*/(false == searchResult->config.configLogics__isToApply__synonymMappings__inFunction), 
								&result_set, /*isToStore__verticesInResultStack=*/true);
	}
	uint arrOf_relationIds_size = 0;
	const uint *arrOf_relationIds = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&result_set, &arrOf_relationIds_size);
	if(arrOf_relationIds_size > 0) {
	  for(uint i = 0; i < arrOf_relationIds_size; i++) {
	    const uint searchId_head = arrOf_relationIds[i];
	    //!
	    //! Investigate the 'recusive search-ctieria':
	    if(searchId_head < searchResult->mapOf_visistedVertices.list_size)  {
	      const bool isTo_traverse = MF__notOfInterest__updateMarkIfNotAlready__s_db_searchResult_rel(searchResult, searchId_head);
	      if(isTo_traverse && searchResult->config.search_inRecursive) { //! then 'this call' has Not alreayd 'been made':
		assert(false); // FIXME: ... 
	      }
	    }	   
	  }
	}
	//!
	//! De-allocate:
	free_s_kt_set_1dsparse_t(&result_set);
	if(arrOf_search) {__MF__free__s_db_rel(arrOf_search);} //! ie, then de-allocate the latter.
	//!
	//! @return true, ie, as we have completed 'this search-oerpation at this exec-point':
      } else {
	fprintf(stderr, "!!\t (no-match)\t Seems like your pre-docntion-search did Not reuslt in any matches, ie, please ivnestgiate latter. Obseration at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, a compiler-heads-up.
	return false;
      }
    } else { //! then we 'star thte search' 'evlauating the complete set of vertices', ie, a 'naive appraoch' seen in ...??..
      // TODO[article]: try descriing 'database-cases' where [ªbove] 'optmisaiton-strategy' is Not used ... ie, where 'this code-seciton' is the 'nroaml result'.
      assert(self->cnt_head > 0);
      for(uint vertex_id = 0; vertex_id < self->cnt_head; vertex_id++) {
	__rescursiveSearchFor__nestedSet__s_db_ds_directMapping_t(self, searchResult, 
								  // /*namesAlreadyMappedIntoSynonyms=*/(false == searchResult->config.configLogics__isToApply__synonymMappings__inFunction), 
								  /*isTo__applyPreStep=*/true, 
								  /*vertex_id=*/vertex_id);
      }
    }
  } 
  //! ----------------------------------------------------------------------------------
  //! 
  { //! Then a call our 'vertex-based recursive search-procedure':
    const bool namesAlreadyMappedIntoSynonyms = (false == searchResult->config.configLogics__isToApply__synonymMappings__inFunction);
    assert(namesAlreadyMappedIntoSynonyms); // FIXME: remove this.
    __rescursiveSearchFor__nestedSet__s_db_ds_directMapping_t(self, searchResult, 
							      //namesAlreadyMappedIntoSynonyms, 
							      /*isTo__applyPreStep=*/false, 
							      /*vertex_id=*/UINT_MAX);
  } 

  //! ----------------------------------------------------------------------------------
  //! 
  //! ----------------------------------------------------------------------------------
  //! @return
  return true;
}
