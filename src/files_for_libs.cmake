
set(__suffix_hcaMemory_alg "${suffix}../hca-memory/src")

set(FILES_HCA_MEMORY  
  #"${__suffix_hcaMemory_alg}/hca_cluster.c" 
  # "${__suffix_hcaMemory_alg}/s_dense_closestPair.c" 
  # "${__suffix_hcaMemory_alg}/rowChunk.c" 
  # "${__suffix_hcaMemory_alg}/dense_closestPair_base.c"
  # "${__suffix_hcaMemory_alg}/log_hca.c"
  "s_dense_closestPair.c" 
  "rowChunk.c" 
   "dense_closestPair_base.c"
   "log_hca.c"
)
#! Itnerpret the "C" fiels as C++:
foreach(name ${FILES_HCA_MEMORY})
  set_source_files_properties(${name} PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
endforeach()
# include_directories ("${PROJECT_BINARY_DIR}/${suffix}../hca-memory/src/")  
#include_directories ("${PROJECT_BINARY_DIR}/${suffix}c_parse/")  
include_directories ("${PROJECT_BINARY_DIR}/${suffix}../tut_standAlone/")  

set_source_files_properties("maskAllocate.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiel
set_source_files_properties("math_generateDistribution.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_randomGenerator_vector.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_resultS_ccmTime.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_distance.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_base.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_list_1d_string.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_list_1d.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_extendedLogics.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("matrix_transpose.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("distance_2rows_slow.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_distance_slow.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_distance.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_sparse_sim.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_metric_aux.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_distance__groupOf_direct.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_distance__groupOf_direct__each.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_distance__groupOf_direct__1ToMany.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_clusterDistance.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_filter.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_matrix_filter.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("matrix_deviation.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("s_allAgainstAll_config.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_dbScan.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_api.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels

set_source_files_properties("kt_assessPredictions.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels

set_source_files_properties("kt_api_config.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_api__centerPoints.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hpLysis_api.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterResult_condensed.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("config_nonRank_each.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("config_nonRank_manyToMany.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("config_nonRank_oneToMany.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("matrix_cmpAlg_ROC.c"  PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_distance_cluster.c"  PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("math_generateDistribution_incompleteBeta.c"  PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels

set_source_files_properties("math_baseCmp_operations_alglib.c"  PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_distribution_studentT.c"  PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_fixed.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_kruskal.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_config__ccm.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_fixed__alg__miniBatch.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_SVD.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_SOM.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_clusterAlg_fixed.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_clusterAlg_svd.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("s_kt_computeTile_subResults.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_centrality.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_centrality.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_hca.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_clusterAlg_hca.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_math_matrix.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("assert_intri.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_intri.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("mask_base.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("log_clusterC.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_semanticSim.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_set_2dsparse.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("db_ds_directMapping.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_set_1dsparse.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_forest_findDisjoint.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_forest_findDisjoint.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_para_3dSchedule.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_para_3dSchedule.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_set_sparse.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_mask.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_sort.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("mask_api.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_inCategory_delta.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_base.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_sort.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_rank.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_rank_rowPair.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_s_allAgainstAll_SIMD_inlinePostProcess.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_correlation.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlationType_proximity_altAlgortihmComputation.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlationType_proximity.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlationType_delta.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlationType_kendall.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlationType_spearman.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlationType_kendall_partialPreCompute_kendall.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_api_rowCompare.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_inCategory_matrix_base.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_inCategory_rank_kendall.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_inCategory_rank_spearman.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("correlation_api.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
#set_source_files_properties("" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("parse_main.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("s_dataStruct_matrix_dense.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
#set_source_files_properties("" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_terminal.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_terminal_parser.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_terminal_clusterResult.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("ktSim_setOf_synData_spec.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_setOf.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_longFilesHandler.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hpLysis.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("e_template_correlation_tile.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_fixed_resultObject.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_hca_resultObject.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_SOM_resultObject.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("e_kt_clusterAlg_hca_type.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("aux_findPartitionFor_tiles.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_terminal.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
#! MINE conversion-proceudre to simplify building of C++ software-chain:
set_source_files_properties("mine.c"  PROPERTIES LANGUAGE CXX )
set_source_files_properties("rapidMic_core.c"  PROPERTIES LANGUAGE CXX )
set_source_files_properties("fast_log.c"  PROPERTIES LANGUAGE CXX )
#! ----
set_source_files_properties("ex_kMeans.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_kt_matrix_cmpCluster.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_cmpCluster.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_ccm.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels



#! ----
set_source_files_properties("hp_clusterShapes.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_clusterFileCollection.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_clusterFileCollection_simMetricSet.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_evalHypothesis_algOnData.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("db_ds_bTree_rel.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("db_ds_bTree_keyValue.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("measure_db.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels ...    #! (oekseth, 06. mar. 2017)
set_source_files_properties("db_inputData_matrix.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("db_searchNode_rel.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("db_searchResult.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_distance_wrapper.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_api_fileInputTight.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_categorizeMatrices_syn.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_exportMatrix.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_entropy.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_entropy.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_api_semanticSim.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_api_entropy.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_matrix_base_uint.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_distanceCluster.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_clusterAlg_mcl.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kd_tree.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_list_2d.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_sim_string.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiesl
set_source_files_properties("kt_hash_string.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_api_sim_dense.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_api_norm.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_measureStub_time.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_image_segment.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("tut_wrapper_image_segment.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("external_emd.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("hp_histogram.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("tut_wrapper_emd.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("tut_wrapper_histogram.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_aux_matrix_norm.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_aux_matrix_binning.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_aux_matrix_blur.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("kt_earth_moving_distance_emd.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("alg_dbScan_brute.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("alg_dbScan_brute_sciKitLearn.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
set_source_files_properties("alg_dbScan_brute_linkedListSet.c" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels
#set_source_files_properties("" PROPERTIES LANGUAGE CXX ) #! ie, to simplify the compilation/linking between c files and CXX fiels

  #! -------------------------------------
  #!
set(CXXFILES_KT_GRAPHALGORITHMS_tests
  #! -------------------------------------
  #!
  #! Collecitonw:
  tut_wrapper_image_segment.c
  tut_wrapper_histogram.c
  tut_wrapper_emd.c external_emd.c
  kt_measureStub_time.c
  #! -------------------------------------
  #!
  #! Use-cases:  
  ex_kMeans.c
  measure_kt_matrix_cmpCluster.c
  
  #! -------------------------------------
  #!
  #! Performance-evaluations

  # #graph_disjointForests.cxx 

  measure_externalCxx__nipals.cxx

  #! Test-files:
  measure_terminal.c
  graphAlgorithms_timeMeasurementsExternalTool_mine.cxx graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.cxx graphAlgorithms_timeMeasurements_syntetic.cxx
  measure_correlation.c
  measure_kt_centrality.c 
  measure_kt_clusterAlg_hca.c
  measure_kt_clusterAlg_fixed.c 
  measure_kt_clusterAlg_svd.c 
  measure_som.cxx measure_syntetic_tiling_fixedRowAppliedOn2dMatrix.cxx 
  measure_codeStyle_functionRef_api.cxx measure_codeStyle_functionRef.cxx
  measure_distribution_studentT.c
  measure_kt_matrix_filter.c
  measure_kt_para_3dSchedule.c
  measure_kt_forest_findDisjoint.c measure_kt_set_sparse.c
  measure_sort.c
  measure_mask.c

  #! 'Utility' classes (oekseth, 06. juni 2016):
  measure_intri.c  assert_intri.c 
  measure_db.c #! (oekseth, 06. mar. 2017)
)

set(CXXFILES_KT_GRAPHALGORITHMS
  kt_resultS_ccmTime.c
  kt_terminal.c
  kt_terminal_clusterResult.c
  kt_terminal_parser.c
  #! ... 
  hp_image_segment.c
  #! ... 
  hp_evalHypothesis_algOnData.c #! which among others use "hp_exportMatrix.h"
  hp_exportMatrix.c #! depedns (among others) on "hp_api_fileInputTight.h":  used to extenive exprot of data-sets, eg, wr.t sim-emtric-comptarsions (oekseth, 06. jun. 2017)
  hp_distanceCluster.c
  #!
  #! Wrapper-functions to falicte the evaluation of different caseS:
  hp_histogram.c #! eg, for iamge anlaysis of histograms.
  hp_api_fileInputTight.c #! which is 'enbapsultaes' [hpLysis_api. hp_ccm, hp_distance, kt_sparse_sim] (oekseht, 06. mar. 2017)
  hp_api_entropy.c
  hp_api_semanticSim.c
  hp_api_sim_dense.c
    hp_api_norm.c


  hp_clusterFileCollection.c
  hp_clusterFileCollection_simMetricSet.c
  hp_categorizeMatrices_syn.c #! which evlautes closenss to data-matrices using sytneitc funcitosn and dat-adistrubitosn as 'point-of-reference'.

  #!
  #! Main API access-point: provide a set of gernalizwed funcitons, eg, where "hpLysis_api.c" 'wraps' funcitoanltiy in "kt_api.c"
  kt_assessPredictions.c  
  hpLysis_api.c
  hp_ccm.c #! which is a simplifed interface to our "kt_matrix_cmpCluster.h" (oekseth, 06. mar. 2017)
  kt_clusterResult_condensed.c #! where latter 'at the time of wirting' does Not have any dpeenecneis, ie, 'order in the compiling-order' is without improtance (oekseth, 06. apr. 2017).
  #!
  #! Generators
  db_inputData_matrix.c #! (oesketh, 06. mar. 2017)

  #!
  #! Functions for computaitons of Entropy (oesketh, 06. jun. 2017).
  hp_entropy.c
  kt_entropy.c
  hp_distance_wrapper.c #! which is used to 'allow' the use (in our "hp_distance.h") of the  "s_kt_matrix_t" object when computing simlairty-scores
  hp_distance.c #! which is a wrapper-fucntion to "kt_dsitance.c" (eosekth, 06. feb. 2017).

  #!
  #! API-wrappers which provide a rich degree of speciufity:  
  kt_api.c 
  kt_api__centerPoints.c
  kt_matrix_cmpCluster.c
  hp_clusterShapes.c #! which 'provides' shapes of data-distribtuions, eg, used to validate results produced by "kt_matrix_cmpCluster.c", eg, wrt. Dunn and Rand.
  kt_matrix_setOf.c

  kt_matrix_extendedLogics.c #! which depends on "kt_matrix.c" and "kt_randomGenerator_vector.c" (oekseth, 06. feb. 2017)
  
  #!
  #! Fiels for clustering-analysis:
  kt_clusterAlg_SVD.c # graphAlgorithms_svd.cxx
  kt_clusterAlg_SOM.c kt_clusterAlg_SOM_resultObject.c #graphAlgorithms_som.cxx 
  kt_clusterAlg_fixed.c   
  kt_clusterAlg_fixed__alg__miniBatch.c  
  kt_clusterAlg_fixed_resultObject.c # graphAlgorithms_kCluster.cxx 
  #! Note: the "kt_randomGenerator_vector.c" is incuuded Before the "*hca*" and "kruskal" as we are interetsted in rpoviding support for 'supporting the use of a dynamic appraoch' to idneity candiate-clsuter-vertices in k-means-clstuering (oekseth, 06. feb. 2017).
  kt_clusterAlg_kruskal.c
  kt_clusterAlg_config__ccm.c
  kt_randomGenerator_vector.c
  kt_clusterAlg_hca.c kt_clusterAlg_hca_resultObject.c e_kt_clusterAlg_hca_type.c
  kt_centrality.c 
  kt_clusterAlg_mcl.c
  kt_clusterAlg_dbScan.c
  alg_dbScan_brute.c
  alg_dbScan_brute_sciKitLearn.c
  alg_dbScan_brute_linkedListSet.c
  kd_tree.c
  kt_api_config.c
  # graphAlgorithms_centrality.cxx 
  # # FIXME: write perofmrance-tests for "graphAlgorithms_distribution.cxx" ... and identify/suggest applicaitosn (oekseth, 06. otk. 2016).
  # graphAlgorithms_distribution.cxx
  # graphAlgorithms_distance.cxx 


  
  #!
  #! Fucntiosn for computation of distance-scores:
  kt_matrix_clusterDistance.c
  kt_semanticSim.c #! (oesketh, 06. jun. 2017).
  kt_sim_string.c
  kt_sparse_sim.c #! (oekseht, 06. mar. 2017)
  kt_earth_moving_distance_emd.c #! (oekseth, 06. feb. 2018)
  kt_distance_cluster.c kt_distance.c 
  kt_metric_aux.c #! used among others by our "kt_distance.c" and our "kt_sparse_sim.c" (oekseht, 06. mar. 2017)
  kt_matrix_filter.c matrix_deviation.c matrix_cmpAlg_ROC.c
  kt_distance_slow.c  distance_2rows_slow.c 
  kt_distance__groupOf_direct.c
  kt_distance__groupOf_direct__1ToMany.c
  kt_distance__groupOf_direct__each.c

  #!
  #! Fucntions for sytentic data-construciton
  ktSim_setOf_synData_spec.c

  #!
  #! Basis data-strucutres:
  kt_matrix.c
  kt_longFilesHandler.c #! used as an auziliary for "kt_matrix.c"
  

  #! Internal functiosn for comptaution fo distance-scores:
  correlation_api.c 
  
  #! Fucntions for: all-agaisnt-all correlation-comptuation:
  correlation_inCategory_rank_spearman.c correlation_inCategory_rank_kendall.c 
  correlation_inCategory_matrix_base.c


  #correlationFunc_optimal_compute_allAgainstAll_SIMD.cxx



  #! Fucntions for correlation-comptuation when using a feature-pair (ie, comparison of two rows):
  correlation_api_rowCompare.c 
  correlationType_kendall_partialPreCompute_kendall.c
  correlationType_spearman.c  correlationType_kendall.c 
  correlationType_delta.c 
  correlationType_proximity.c correlationType_proximity_altAlgortihmComputation.c


  
  #! Auziilary funcitons for ranks, sort and distance-comptuation:
  correlation_inCategory_delta.c 
  # s_kt_correlationConfig_allAgainstAll.cxx 
  # s_kt_correlationConfig.cxx 

  e_template_correlation_tile.c 

  correlation_rank_rowPair.c

  correlation_rank.c

  #!
  #! MINE linkage-files:
  #mine.c 
  rapidMic_core.c fast_log.c
  #mine/mine.c mine/rapidMic_core.c mine/fast_log.c

  #!
  #! C-parsers:
  parse_main.c s_dataStruct_matrix_dense.c
  #c_parse/parse_main.c c_parse/s_dataStruct_matrix_dense.c

  #!
  #! Fucntiosn used itnernaly to irmpvoing the performance of our appraoch, eg, wrt. optmaized parallel scheudliong and wrt. idnetificaiton of disjoitn-sets of vertices (oekseth, 06. okt. 2016):
  kt_para_3dSchedule.c 
  kt_forest_findDisjoint.c
  db_ds_directMapping.c

  correlation_s_allAgainstAll_SIMD_inlinePostProcess.c

  config_nonRank_manyToMany.c config_nonRank_oneToMany.c   config_nonRank_each.c
  s_kt_computeTile_subResults.c
  s_allAgainstAll_config.c

  kt_set_2dsparse.c
  kt_set_1dsparse.c


  correlation_sort.c correlation_base.c

  kt_math_matrix.c #! which is perofmrance-tested in our "measure_externalCxx__nipals.cxx" (oekseth, 06. otk. 2016).
  mask_api.c mask_base.c
  aux_findPartitionFor_tiles.c
  
  math_generateDistribution.c math_generateDistribution_incompleteBeta.c math_baseCmp_operations_alglib.c
  maskAllocate.c matrix_transpose.c

  # graphAlgorithms_aux.cxx
  db_ds_bTree_rel.c   db_ds_bTree_keyValue.c
  db_searchResult.c db_searchNode_rel.c #! (oekseth, 06. mar. 2017).
  # --- 
  kt_aux_matrix_blur.c
  kt_aux_matrix_norm.c
  kt_aux_matrix_binning.c
  # --- 
  kt_hash_string.c
  kt_list_2d.c
  kt_list_1d.c #! where the latter makes use of our "kt_matrix_base.c" for file-reading.
  kt_matrix_base.c #! which is a geneirc verison of our "kt_matrix.c" (oekseth, 06. feb. 2017).
  kt_matrix_base_uint.c
  # FIXME: write perofmrance-tests for our "graph_disjointForests.cxx" ... and identify/suggest applicaitosn (oekseth, 06. otk. 2016).
  kt_list_1d_string.c #! does not have any dpednecies of improtace; is designed to simplife initeraiton with SWIG (eg, for Perl nd JavaScript).


  ${FILES_HCA_MEMORY}

  log_clusterC.c
) 
