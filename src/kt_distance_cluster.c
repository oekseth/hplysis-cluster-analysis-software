#include "kt_distance_cluster.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "e_kt_clusterComparison.h"
#include "kt_distance.h"
#include "distance_2rows_slow.h"
#include "kt_set_2dsparse.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"

// #include <omp.h> // for parallisation using OpenMP
//#include "quick_sort.h"
#include <stdio.h>
//#include <gsl/gsl_multifit.h>

// #include "ex_intrisintics.h"

// //! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
// static t_float *allocate_1d_list_float(const uint size, const t_float default_value) {  
//   t_float *tmp = new t_float[size];
//   memset(tmp, default_value, size*sizeof(t_float));
//   return tmp;
// }







//! Use a slwo procedure to infer the weights assicted to the rows and columns.
t_float* __calculate_weights_slow(const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const char dist, const t_float cutoff, const t_float exponent) {

  const uint ndata = (transpose==0) ? ncolumns : nrows;
  const uint nelements = (transpose==0) ? nrows : ncolumns;
  //! Allocate memory:
  const t_float default_value = 0;
  t_float* result = allocate_1d_list_float(nelements, default_value); //list_generic<t_float>::allocate_list(/*size=*/nelements, /*empty-value=*/0, __FILE__, __LINE__);
  /* Set the metric function as indicated by dist */
  t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = setmetric_slow(dist);
  //! Note[memory-access-patterns]: (same result as in "graphAlgorithms_distance::distancematrix(..)");
  for(uint i = 0; i < nelements; i++) {
    result[i] += 1.0;
    for(uint j = 0; j < i; j++) {
      
      const t_float distance = metric(ndata, data, data, mask, mask, weights, i, j, transpose);
      //! Comptue the score for the weight:
      macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(distance, cutoff, exponent, i, j, result, /*isTo_updateAlsoFor_symmetric_elements=*/false);      
    }
  }
  //! "re-weight" the results:
  for(uint i = 0; i < nelements; i++) result[i] = 1.0/result[i];
  return result;
}

/**
   @brief ...
   @param <nrows> The number of rows in the gene expression data matrix, equal to the number of genes.
   @param <ncolumns> The number of columns in the gene expression data matrix, equal to the number of microarrays.
   @param <data> The array containing the gene expression data.
   @param <mask> This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
   @param <weight> The weights that are used to calculate the distance. The length of this vector is ncolumns if gene weights are being clustered, and nrows if microarrays weights are being clustered.
   @param <transpose> If transpose==0, the weights of the rows of the data matrix are calculated. Otherwise, the weights of the columns of the data matrix are calculated.
   @param <dist> Defines which distance measure is used, as given by the table:
   - dist=='e': Euclidean distance
   - dist=='b': City-block distance
   - dist=='c': correlation
   - dist=='a': absolute value of the correlation
   - dist=='u': uncentered correlation
   - dist=='x': absolute uncentered correlation
   - dist=='s': Spearman's rank correlation
   - dist=='k': Kendall's tau
   - For other values of dist, the default (Euclidean distance) is used.
   @param <cutoff> The cutoff to be used to calculate the weights.
   @param <exponent> The exponent to be used to calculate the weights. 
   @param <isTo_use_fastFunction> which if set to false imples that we use the 'legacy' slow function.
   @return a pointer to a newly allocated array containing the calculated weights for the rows (iftranspose==0) or columns (if transpose==1). 
   @remarks This function calculates the weights using the weighting scheme proposed by Michael Eisen: 
   - w[i] = 1.0 / sum_{j where d[i][j]<cutoff} (1 - d[i][j]/cutoff)^exponent
   - where the cutoff and the exponent are specified by the user.
   @remarks Memory-access-pattern of this function is:  (same result as in "graphAlgorithms_distance::distancematrix(..)");
**/
t_float* calculate_weights__extensiveParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const t_float cutoff, const t_float exponent,  const uint CLS, const uint iterationIndex_2) {

  //if(isTo_use_fastFunction == false) {assert(false);} //! ie, as a diffenret function should then have been called.
  // if(isTo_use_fastFunction == false) { //! then we use the 'naive' function shipped with the 'orignal' "cluster.c", a set of functions which are only sliglty modifeid/updated.
  //   return __calculate_weights_slow(nrows, ncolumns, data, mask, weights, transpose, dist, cutoff, exponent);
  // }

  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.

  // FIXME: cosndier suggesting similar weighrting-schemes based on proeprties 'which we are to identify' in our data ... and try to cpmare this appraoch to the "Degree centrlaity".

  //! Note: we have optmized this fucntion wrt. use of 'temproary internal matrices' in our "__correlationFunc_optimal_compute_allAgainstAll_SIMD(..)", ie, the optmization of 'this' is transfferred into the latter: our optmization-approach 'enables' a generalizaiton of tasks, ie, simplifes future/new 'logical requiremnts'.
  const uint ndata = (transpose==0) ? ncolumns : nrows;
  const uint nelements = (transpose==0) ? nrows : ncolumns;
  //! Allocate memory:
  t_float* result = allocate_1d_list_float(/*size=*/nelements, /*empty-value=*/0);  
  s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t obj_spec; 
  obj_spec.exponent = exponent; obj_spec.cutoff = cutoff; obj_spec.listOf_result_afterCorrelation = result;


  const bool isTo_invertMatrix_transposed = true;
  t_float **matrix_result = NULL;
  const t_float default_value_float = 0;
  if(transpose == 0) {matrix_result = allocate_2d_list_float(nrows, ncolumns, default_value_float);}
  else {matrix_result = allocate_2d_list_float(ncolumns, nrows, default_value_float);}
  s_allAgainstAll_config_t config = get_init_struct_s_allAgainstAll_config();
  const bool isTo_use_continousSTripsOf_memory = true;
  init__s_allAgainstAll_config(&config, CLS, isTo_use_continousSTripsOf_memory, /*iterationIndex_2=*/iterationIndex_2, /*typeOf_postProcessing_afterCorrelation=*/e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights, (void*)&obj_spec, /*masksAre_used=*/e_cmp_masksAre_used_undef, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
  set_metaMatrix(&config, /*isTo_init=*/false, /*mask=*/mask, /*mask=*/mask, weights, transpose, /*masksAre_used=*/e_cmp_masksAre_used_undef, nrows, ncolumns);
  //const bool isTo_use_fastFunction = true;
  const bool isTo_use_fastFunction = true;
  if(isTo_use_fastFunction == true) {init_useFast__s_allAgainstAll_config(&config, /*masksAre_used=*/e_cmp_masksAre_used_undef);}
  config.isTo_invertMatrix_transposed = isTo_invertMatrix_transposed;
  
  // if(mask_uint) {
  //   set_masks_typeOfMask_uint__s_allAgainstAll_config(&config, /*isTo_init=*/false, mask_uint, mask_uint, /*isTo_translateMask_intoImplictDistanceMatrix=*/true, nrows, ncolumns, data, data, /*isTo_use_continousSTripsOf_memory=*/true);
  // }

  kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, data, /*result-matrix=*/NULL, config, NULL);

  //assert(false); // FIXME: udpate [”elwo] call using our new-added "kt_distance" fucntioanitliy. (oekseth, 06. sept. 2016).
  //compute_allAgainstAll_distanceMetric_slow(dist, nrows, ncolumns, data, data, weights, /*resultMatrix=*/NULL, mask, mask, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, /*isTo_use_continousSTripsOf_memory=*/true, iterationIndex_2, e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights, (void*)&obj_spec);

  
  for(uint i = 0; i < nelements; i++) {result[i] = (result[i]) ? 1.0/result[i] : 0;}
  return result;
}

//! Copmute the assicated meidans for the two data-sets/cluster/point-cloud:
static void computeSumOf_squaredMatrix_updateTail_wrtMedians(const uint n1, const uint *map_index, const uint nrows, const uint ncolumns, t_float **data, char **mask, t_float *arr_cdata, char *arr_cmask, const bool isTo_useImplictMask) { 
  const t_float empty_0 = 0;
  assert(nrows != UINT_MAX);
  assert(n1 != UINT_MAX);
  const uint temp_size = macro_max(nrows, n1);
#ifndef NDEBUG
  if(map_index) { //! then validate that the map-idnexes are inside the 'expected range':
    for(uint k = 0; k < n1; k++) {
      const uint i = map_index[k];
      if(i != UINT_MAX) {
	assert(i < nrows);
      }
    }
  } else {
    assert(n1 <= nrows); //! ie, as we 'then' use the "i \in n1" to 'look-up' the "data" matrix.
  }
#endif
  assert(temp_size != 0);
  assert(temp_size >= n1); //! ie, to 'detect' any errnous future changes.
  t_float* temp = allocate_1d_list_float(temp_size, empty_0);  
  t_float* temp_sorted = allocate_1d_list_float(temp_size, empty_0);
  //! Note: in [”elow] we do not use itnristinctcs givne the exepcted high time-cost of the sorting-operation: a different cosndieration conserns teh mask-operaiton, ie, we a 'pre-test' to check i fmask-valeus are set may outweight the use of itrnisitnctic operimzation.

#ifdef __typeOf_maskTo_use
#error "Had expected this macor-varialbe to be uneiq for this case, ie, pelase ivnestigate"
#endif

  if(mask) {
#define __typeOf_maskTo_use 2 
#include "computeSumOf_squaredMatrix_updateTail_wrtMedians.c"
  } else if(isTo_useImplictMask == true) {
#define __typeOf_maskTo_use 1
#include "computeSumOf_squaredMatrix_updateTail_wrtMedians.c"
  } else {
#define __typeOf_maskTo_use 0
#include "computeSumOf_squaredMatrix_updateTail_wrtMedians.c"
  }
}


//! A slow pertmuation of the cluster-distance funciton: included for benchmarkin.
t_float clusterdistance_slow(uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const char dist, const char method, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed) {
#include "distance_cluster_slow.c" //! ie, the code-file ... seprated into its own chunk givne (a) its non-improtance and (b) o get a clearner code (oekseth, 06. setp. 2016).
}

//! Compute the sum of cells wrt. the tail-vetrtex. ie, updated for 'teh outer index'.
// FIXME[jc]: may you try to dientify a g'enrlaized applicaiton' of [”elow] function ... ie, to classify our 'opmmziation-fucntiosn' ... tehreby describing/providign a new library functiosn for matrix-operaitons in network-analsysis?
static void computeSumOf_squaredMatrix_updateTail(const uint n1, const uint *map_index, const uint ncolumns, t_float **data, char **mask, t_float *arr_cdata, t_float *arr_count, const bool isTo_useImplictMask) {
  // FIXME[jc]: may you itnrospect upon "the >= 16" threshold wrt. itrnsitnictics?
  // FIXM: benchmark/time-comparison this function ... usign diffenre thtreshodl wr.t the "16" threshodl ... updating the latter in our fucntion-caller.
  const uint ncolumns_intri = (ncolumns >= 256) ? ncolumns - VECTOR_FLOAT_ITER_SIZE : 0;
  

#ifdef __typeOf_maskTo_use
#error "Had expected this macor-varialbe to be uneiq for this case, ie, pelase ivnestigate"
#endif

  if(mask) {
#define __typeOf_maskTo_use 2 
#include "computeSumOf_squaredMatrix_updateTail.c"
  } else if(isTo_useImplictMask == true) {
#define __typeOf_maskTo_use 1
#include "computeSumOf_squaredMatrix_updateTail.c"
  } else {
#define __typeOf_maskTo_use 0
#include "computeSumOf_squaredMatrix_updateTail.c"
  }
}

//! Build a dense matric using the map_index as index-mapping-table.
static t_float **build_denseMatrixOf_sparse_floats(const uint n1, const uint ncolumns, t_float **data, const uint *map_index) {
  const t_float empty_0 = 0;
  t_float **data1_dense = allocate_2d_list_float(n1, ncolumns, empty_0);
  for(uint i = 0; i < n1; i++) {
    const uint k = (!map_index) ? i : map_index[i];
    memcpy(data1_dense[i], data[k], sizeof(t_float)*ncolumns);
  }
  return data1_dense;
}
//! Build a dense matric using the map_index as index-mapping-table.
static char **build_denseMatrixOf_sparse_chars(const uint n1, const uint ncolumns, char **data, const uint *map_index) {
  const t_float empty_0 = 0;
  char **data1_dense = allocate_2d_list_char(n1, ncolumns, empty_0);
  for(uint i = 0; i < n1; i++) {
    const uint k = (!map_index) ? i : map_index[i];
    memcpy(data1_dense[i], data[k], sizeof(char)*ncolumns);
  }
  return data1_dense;
}

// const s_kt_correlationMetric_t obj_metric
static t_float clusterdistance_fast(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows_1, const uint nrows_2, const uint ncolumns, t_float **data1, t_float **data2, char **mask1, char **mask2, t_float *weight, const uint n1, const uint n2, const uint *index1, const uint *index2, const  e_kt_clusterComparison_t  method, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_useImplictMask) {
//static t_float clusterdistance_fast(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows_1, const uint nrows_2, const uint ncolumns, t_float **data1, t_float **data2, char **mask1, char **mask2, t_float *weight, const uint n1, const uint n2, const uint *index1, const uint *index2, const char method, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_useImplictMask) {

  bool useMatrics_withoutIndexConversion = ( (index1 == NULL) && (index2 == NULL) ); //! ie, to 'allow' the case where there is no re-mapping.
  /* Set the metric function as indicated by dist */

  /* Set the metric function as indicated by dist */
  //float (*metric) (uint, float**, float**, char**, char**, const float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);
  t_float (*metric_each)(config_nonRank_each) = (isTo_use_kendall__e_kt_correlationFunction(metric_id) == false) ? setmetric__correlationComparison__each(metric_id, weight, mask1, mask2, isTo_useImplictMask) : NULL;
 //! else we assume 'the funciton-poiwner may be identified/foudn in the fucniton which we call' (oekseth, 06. des. 2016).
    //    = setmetric__correlationComparison__each(metric_id, weight, (mask1) ? mask1[0] : NULL, (mask2) ? mask2[0] : NULL, /*needTo_useMask_evaluation=*/(mask1) ? true : isTo_useImplictMask);



  //t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = setmetric_slow(dist);

  const uint transpose = false; //! ie, what we expect.

  t_float return_distance = T_FLOAT_MAX;
  const e_kt_clusterComparison_t method_id = method; // get_enumId_fromChar__e_kt_clusterComparison_t(method);

  //printf("method='%c' == method_id=%u, at %s:%d\n", method, method_id, __FILE__, __LINE__);
  assert(e_kt_clusterComparison_undef != /*enum_id_method=*/method_id);
  if(
     (method_id == e_kt_clusterComparison_extreme_min)
     || (method_id == e_kt_clusterComparison_extreme_max)
     || (method_id == e_kt_clusterComparison_avg)
     || (method_id == e_kt_clusterComparison_total)
    ){
    assert(useMatrics_withoutIndexConversion == true); //! meaning="use input-matrices as-is": ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
    assert(nrows_1 == n1); //! ie, what we expect.
    assert(nrows_2 == n2); //! ie, what we expect.
  }


  switch (method_id)
    { case e_kt_clusterComparison_mean_artimethic: //! the distance between the arithmetic means of the two clusters
    { /* Find the center */
      //! Note: use a 'two-step' distance-computation: step(a) idnetify the sum of distances assicated to each vertex/clsuter, and then in step(b) copmute the distance between 'the average/mean sum of dsitances' between the point-clouds in each of the two input-clsuters.

	//! Note[memory-access-patterns]: worst-case implies "n1" + "n2" + "ncolumns" cache-misses;
	// FIXME: for [”elow] write a 'geneirc code-test' to evlaaute/compare the time-cost of 'ordering' the memory-allcoations ... eg, to first allocate for "[0]" and thereafter for "[1]"
	//! Allcoate:
      //! Note: we use "t_float" for count iot. simplyf the logics of our macro-operations.
      // FIXME[docuemtantion]: update wrt. [”elow] consideration .. ie, that 'if the data-types has the same Byte-cosnumption then usign the samte data-type (eg,  '"t_float" and "t_float"' instead of '"t_float" and "uint"') improves the perofrmance of the comptuations <-- write a simple performance-comparison wrt. this case.
        t_float* count[2] = { 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0),
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0)};
        char* cmask[2] = { 
	  allocate_1d_list_char(ncolumns, 0),
	  allocate_1d_list_char(ncolumns, 0)
	};
        t_float* cdata[2] = { 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0), 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0)};

	//! Note[memory-access-patterns]: at worst-case each 'outer index' has a random/unpredictable location/value, for which "n2" cache-misses is expected. 	
	//! Idneitfy the counts/sinficance for the first clsuter:
	computeSumOf_squaredMatrix_updateTail(n1, index1, ncolumns, data1, mask1, cdata[0], count[0], isTo_useImplictMask);
	//! Idneitfy the counts/sinficance for the second cluster:
	computeSumOf_squaredMatrix_updateTail(n2, index2, ncolumns, data2, mask2, cdata[1], count[1], isTo_useImplictMask);

	//! Idnetify the average distance seperately for vertices in each 'point-cloud'/'cluster-set':
	//! Note[memory-access-patterns]: eitehr one or two cache-misses
        for(uint i = 0; i < 2; i++) {
          for(uint j = 0; j < ncolumns; j++) {
	      // TODO[jc]: wrt. intristintitcs ... suggestions for how we in [”elow] may combine the "t_float" and "char" data-types?
	    if(count[i][j] > 0) {
	      cdata[i][j] = cdata[i][j] / count[i][j]; //! ie, the average/'mean' distance between all of the points assicated to a given vertex/cluster.
              cmask[i][j] = 1; //! ie, as the cell is then of interest.
            } else {cmask[i][j] = 0;}
          }
	}
	//! Compute the distance between teh two 'point-clouds'
	// FIXME: complete [below]
	//! Note: the 'average distance' assicated to each of the 'clusters/vertices in each point-cloud' (as computed [ªbove]) is descriptive/correct based on the observation of ....??...
	//! Note[memory-access-patterns]: in worst-case "ncolumns" cache-misses
	const t_float distance = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows_1, ncolumns, cdata, cdata, cmask, cmask, weight, /*index1=*/0, /*index2=*/1, /*transpose=*/0, isTo_useImplictMask, metric_each, NULL);
        //const t_float distance = metric (ncolumns,cdata,cdata,cmask,cmask,weight,0,1,0);
	//! De-allocate the temporary containers:
        for(uint i = 0; i < 2; i++) {
	  free_generic_type_1d(cdata[i]);
	  free_generic_type_1d(cmask[i]);
          free_generic_type_1d(count[i]);
        }
	//! @return
        return_distance = distance;
      break;
    }
    case e_kt_clusterComparison_median_artimethic: // the distance between the medians of the two clusters
    { 

        //t_float* temp = allocate_1d_list_float(/*size=*/nrows, /*empty-value=*/0), 
        t_float* cdata[2] = { 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0), 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0)};
        char* cmask[2] = { 
	  allocate_1d_list_char(/*size=*/ncolumns, /*empty-value=*/0), 
	  allocate_1d_list_char(/*size=*/ncolumns, /*empty-value=*/0)};

	//! Copmute the assicated meidans for the two data-sets/cluster/point-cloud:
	assert(n1 <= nrows_1); // TODO: validate this assumption.
	computeSumOf_squaredMatrix_updateTail_wrtMedians(n1, index1, nrows_1, ncolumns, data1, mask1, cdata[0], cmask[0], isTo_useImplictMask);
	assert(n2 <= nrows_2); // TODO: validate this assumption.
	computeSumOf_squaredMatrix_updateTail_wrtMedians(n2, index2, nrows_2, ncolumns, data2, mask2, cdata[1], cmask[1], isTo_useImplictMask);


	// FIXME: udpate our distance-mtratrices tith an 'imrpvoed' version ... eg, wrt. "kendall(..)" and "spearman(..)" <--- currently omitted, ie, as the tiem-irmpvoement (when compared to the other functiosn, eg, the "sort-fucntion") is assumed to be neglectivblbe.

	//! Compute distance:
	//! Note[memory-access-patterns]: worst-case implies "ncolumns" cache-misses
	const t_float distance = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows_1, ncolumns, cdata, cdata, cmask, cmask, weight, /*index1=*/0, /*index2=*/1, /*transpose=*/0, isTo_useImplictMask, metric_each, NULL);
        //const t_float distance = metric (ncolumns,cdata,cdata,cmask,cmask,weight,0,1,0);

	//! De-allocate the temporary containers:
        for(uint i = 0; i < 2; i++) {
	  free_generic_type_1d(cdata[i]);
	  free_generic_type_1d(cmask[i]);
	}
	//! @return_distance = the result
        return_distance = distance;

      break;
    }
    case e_kt_clusterComparison_extreme_min: // the smallest pairwise distance between members of the two clusters
    { 
      assert(useMatrics_withoutIndexConversion == true); //! meaning="use-inptu-amtrices-as-is": ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
      assert(nrows_1 == n1); //! ie, what we expect.
      const uint iterationIndex_2 = /*iterationIndex_2=*/n2;
#ifndef NDEBUG
      // TODO: cosnider to use a differen macro than [”elow] "configureDebug_useExntensiveTestsIn__tiling" (oekseth, 06. de.s 0216)
#if(configureDebug_useExntensiveTestsIn__tiling)
      { //! Then we 'apply' a stress-test to try 'rpovking' meomry-bugs, eg, 'detected' using Valgirnd (oekseth, 06. des. 2016).
	t_float sum = 0;
	assert(transpose == false);
	if(data1) {
	  //! 
	  for(uint i = 0; i < nrows_1; i++) {
	    assert(data1[i]);
	    for(uint k = 0; k < ncolumns; k++) {
	      if(isOf_interest(data1[i][k])) {
		sum += data1[i][k];}}}
	}
	if(data2) {
	  //! 
	  const uint nrows_2_perm = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows_2;
	  for(uint i = 0; i < nrows_2_perm; i++) {
	    assert(data2[i]);
	    //const uint ncolumns_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : ncolumns;
	    for(uint k = 0; k < ncolumns; k++) {    if(isOf_interest(data2[i][k])) {sum += data2[i][k];}}}
	    //for(uint k = 0; k < ncolumns_2; k++) { sum += data2[i][k];}}
	}
      }
#endif
#endif
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; init_ToMin__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(&obj_spec);
      //s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; 
      //obj_spec.return_distance = T_FLOAT_MAX;


      //assert(false); // FIXME: udpate [”elwo] call using our new-added "kt_distance" fucntioanitliy. (oekseth, 06. sept. 2016).
      kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows_1, ncolumns, data1, data2, weight, /*resultMatrix=*/NULL, mask1, mask2, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, /*isTo_use_continousSTripsOf_memory=*/true, iterationIndex_2, e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min, (void*)&obj_spec, e_cmp_masksAre_used_undef, NULL,
								    // FIXME: add for [”elow].
								    NULL
								    );

      //! @return_distance = the result
      return_distance = obj_spec.return_distance;
      break;
    }
    case e_kt_clusterComparison_extreme_max: //! the largest pairwise distance between members of the two clusters
    { 
      assert(useMatrics_withoutIndexConversion == true); //! meaning="use-inptu-amtrices-as-is": ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
      //assert(useMatrics_withoutIndexConversion == false); //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
      assert(nrows_1 == n1); //! ie, what we expect.
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; init_ToMax__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(&obj_spec);
      //s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; obj_spec.return_distance = T_FLOAT_MIN_ABS;


      kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows_1, ncolumns, data1, data2, weight, /*resultMatrix=*/NULL, mask1, mask2, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/n2, e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max, (void*)&obj_spec, e_cmp_masksAre_used_undef, NULL,
								    // FIXME: add for [”elow].
								    NULL
								    );
      //! @return_distance = the result
      return_distance = obj_spec.return_distance;
      break;
    }
    case e_kt_clusterComparison_avg: //! average of the pairwise distances between members of the clusters
    { 
      assert(useMatrics_withoutIndexConversion == true); //! meaning="use-inptu-amtrices-as-is": ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
      /* assert(useMatrics_withoutIndexConversion == false); //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funcito */
      /* assert(useMatrics_withoutIndexConversion == false); //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton. */
      assert(nrows_1 == n1); //! ie, what we expect.
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; init_ToSum__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(&obj_spec);
      // s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; obj_spec.return_distance = 0;

      kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows_1, ncolumns, data1, data2, weight, /*resultMatrix=*/NULL, mask1, mask2, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/n2, e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum, (void*)&obj_spec, /*masksAre_used=*/e_cmp_masksAre_used_undef, NULL,
								    // FIXME: add for [”elow].
								    NULL
								    );
      //! @return_distance = the result
      return_distance = obj_spec.return_distance;
      if( (return_distance != 0) && (return_distance != T_FLOAT_MAX) ) {return_distance /= (n1*n2);} //! ie, adjsut (oekseth, 06. jun. 2017)
      break;
    }
    case e_kt_clusterComparison_total: //! average of the pairwise distances between members of the clusters
    { 
      assert(useMatrics_withoutIndexConversion == true); //! meaning="use-inptu-amtrices-as-is": ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
      /* assert(useMatrics_withoutIndexConversion == false); //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funcito */
      /* assert(useMatrics_withoutIndexConversion == false); //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton. */
      assert(nrows_1 == n1); //! ie, what we expect.
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; init_ToSum__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(&obj_spec);
      // s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; obj_spec.return_distance = 0;

      kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows_1, ncolumns, data1, data2, weight, /*resultMatrix=*/NULL, mask1, mask2, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/n2, e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum, (void*)&obj_spec, /*masksAre_used=*/e_cmp_masksAre_used_undef, NULL,
								    // FIXME: add for [”elow].
								    NULL
								    );
      //! @return_distance = the result
      return_distance = obj_spec.return_distance;
      //if( (return_distance != 0) && (return_distance != T_FLOAT_MAX) ) {return_distance /= (n1*n2);}
      break;
    }
    case e_kt_clusterComparison_undef: {
      fprintf(stderr, "!!\t Investigate this case: cluster-compiarson-type set to 'undef', at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false);
    }
  }


  /* if(return_distance != T_FLOAT_MAX) {return return_distance;} */
  /* else { */
  /*   //! @return the result */
  /*   assert(false); //! ie, then ivnestgiate this case. */
  /* /\* Never get here *\/ */
  /*   return -2.0; */
  /* }  */

  //! @return (oekseth, 06. des. 2016)
  return return_distance;
}

/**
   @brief infer thej distance between two clusters/point-clouds/cluster-clouds
   @remarks The clusterdistance routine calculates the distance between two clusters containing genes or microarrays using the measured gene expression vectors. The distance between clusters, given the genes/microarrays in each cluster, can be defined in several ways. Several distance measures can be used. 
   @remarks in below we describe a few number of the parameters:
   # "n1": The number of elements in the first cluster.
   # "n2": The number of elements in the second cluster.
   # "index1": Identifies which genes/microarrays belong to the first cluster.
   # "index2": Identifies which genes/microarrays belong to the second cluster.
   # "method" Defines how the distance between two clusters is defined, given which genes belong to which cluster:
   - method=='a': the distance between the arithmetic means of the two clusters
   - method=='m': the distance between the medians of the two clusters
   - method=='s': the smallest pairwise distance between members of the two clusters
   - method=='x': the largest pairwise distance between members of the two clusters
   - method=='v': average of the pairwise distances between members of the clusters
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
   @return The routine returns the distance in t_float precision. If the parameter transpose is set to a nonzero value, the clusters are interpreted as clusters of microarrays, otherwise as clusters of gene.
   @remarks Memory-access-pattern of this function is either: worst-case implies the following set of cahce-miss-descriptiosn: (a.1 and b.1): ("n1" + "n2" + "ncolumns"); (a.2) ( ("n1" * "nrows") + ("n2" * "nrows") + (2 * "nrows") ); (b.2): (("n1" * "nrows") + ("n2" * "nrows") + ("nrows"*2)*2 + "nrows"); (c): ("n1" * "n2" * ("ncolumns" | "nrows")). From the later (combined with [”elow] description included in the code itself) we observe that in ideal memory-allcoation-schemes the highest time-cost is assicated/described by the distance-metric, where the distance-metric has a linear time-complexity.
**/
t_float clusterdistance__extensiveParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t method, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_useImplictMask) {  
    assert(metric_id != e_kt_correlationFunction_undef);  
  // FIXME: perofrmancte-test .... compare 'transposed operations' where 'datsets are shuffled on random' for 'euler'.
  // FIXME: perofrmancte-test .... 
  // if(isTo_use_fastFunction == false) {
  //   return clusterdistance_slow(nrows, ncolumns, data, mask, weight, n1, n2, index1, index2, dist, method, transpose, isTo_use_continousSTripsOf_memory, isTo_invertMatrix_transposed);
  // }
  /* if one or both clusters are empty, return */
  if(n1 < 1 || n2 < 1) return -1.0;

  // FIXME: udpat ethe callers of this funciton with the isTo_use_continousSTripsOf_memory parameter ... adnt valdiate taht "delete" is used instead of "free"

  if( //isTo_invertMatrix_transposed && 
      (transpose == 1) ) {
    const t_float default_value_float = 0;
    t_float **data1_transposed = allocate_2d_list_float(ncolumns, nrows, /*default-value=*/default_value_float);
    char **mask1_transposed = (mask) ? allocate_2d_list_char(ncolumns, nrows, /*default-value=*/default_value_float) : NULL;
    matrix__transpose__compute_transposedMatrix(nrows, ncolumns, data, mask, data1_transposed, mask1_transposed, /*isTo_useIntrisinistic=*/true);


    //! Compute the result, calling this funciton once more:
    //! Note: the 'two-step' call is sued to simplify the logics fo this fucntion, ie, to ese maitnainance and verifciaiton of crrectness (wrt. the implemnetiaton).
    const t_float ret_Val = clusterdistance__extensiveParams(metric_id, typeOf_correlationPreStep, ncolumns, nrows, data1_transposed, mask1_transposed, weight, 
					  n2, n1, index2, index1, //! ie, invert the set of 'fitler-params'.
					    method, /*transpose=*/0, isTo_use_continousSTripsOf_memory, isTo_invertMatrix_transposed, CLS, isTo_useImplictMask);
    //! De-allcoat ehte locally stored matrices:
    free_2d_list_float(&data1_transposed, ncolumns); data1_transposed = NULL;
    if(mask1_transposed) {
      free_2d_list_char(&mask1_transposed, ncolumns); mask1_transposed = NULL;
    }
    //! @return the result:
    return ret_Val;
  }
  //! At this expectuion-poitn we expect only non-transpsoed oepraitons to be performed:
  assert(transpose == 0);
  


  t_float return_distance = T_FLOAT_MAX;

  // FIXME: update our fucntion wrt. [”elow] 'case'.
  bool useMatrics_withoutIndexConversion = ( (index1 == NULL) && (index2 == NULL) ); //! ie, to 'allow' the case where there is no re-mapping.3
  
  const e_kt_clusterComparison_t method_id = method; // get_enumId_fromChar__e_kt_clusterComparison_t(method);
  //printf("method='%c' == method_id=%u, at %s:%d\n", method, method_id, __FILE__, __LINE__);
  assert(e_kt_clusterComparison_undef != /*enum_id_method=*/method_id);
  if(
     (method_id == e_kt_clusterComparison_extreme_min)
     || (method_id == e_kt_clusterComparison_extreme_max)
     || (method_id == e_kt_clusterComparison_avg)
     || (method_id == e_kt_clusterComparison_total)
    ){
    //! Then we are interested in (for simplictly) to always use a 'dense matrix input' (oekseth, 06. des. 2016).
    useMatrics_withoutIndexConversion = false; //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
  }

  // FIXME: update our eprformance-evalauteins .... testing the importance of such a 're-mapping wrt. the matrix'.

  if(useMatrics_withoutIndexConversion == true) {

    // FIXME: figure out why [below] 'covernisons' may be omitted ... cosniddering udpating wrt. our approx. 80 unique correlation-metric-enums (oekseth, 06. setp. 2016)
    // if( (dist == 's') || (dist == 'x') || (dist == 'v') ) {      
    //   useMatrics_withoutIndexConversion = true;
    // } else
    {
      if( (n1 == nrows) && (n2 == nrows) ) {
	useMatrics_withoutIndexConversion = true;
	for(uint i = 0; i < n1; i++) {
	  const uint index = index1[i];
	  if(index != i) {useMatrics_withoutIndexConversion = false;}      
	}
	if( (useMatrics_withoutIndexConversion == true) && (index1 != index2) ) {
	  for(uint i = 0; i < n2; i++) {
	    const uint index = index2[i];
	    if(index != i) {useMatrics_withoutIndexConversion = false;}      
	  }
	}
      } else {
	// FIXME: use perofrmance-time-evalauteion to idnetify the best 'default threshodl' for "threshold_translateInto_dense" parameter.
	const t_float threshold_translateInto_dense = 0.5; //! ie, 
	if( (n1/nrows) > threshold_translateInto_dense) {
	  if( (n2/nrows) > threshold_translateInto_dense) {
	    useMatrics_withoutIndexConversion = true;
	  }
	}
      }
    }
  }

  if(
     (method_id == e_kt_clusterComparison_extreme_min)
     || (method_id == e_kt_clusterComparison_extreme_max)
     || (method_id == e_kt_clusterComparison_avg)
     || (method_id == e_kt_clusterComparison_total)
    ){
    assert(useMatrics_withoutIndexConversion == false); //! ie, as we expect this 'to have been handled' in the 'start-potion' of this funciton.
    //assert(nrows == n1); //! ie, what we expect.
  }


  if(useMatrics_withoutIndexConversion == true) { //! VAldiate that the indces are all of itnerest: <-- udpated to " == true" by (oekseth, 06. des. 2016).
  //if(useMatrics_withoutIndexConversion == false) { //! VAldiate that the indces are all of itnerest:
    // FIXME[jc]: ... seemslike the 'uint' max-value requires SIMD4 ... may you validate that assumtpion ... ie, that instrinsticts-operaitons will not reduce the exueciton-tiem for this oepraiton?
    // FIXME[jc] ... tag="part-traverse-1d-cmp-max" .. see "ex_intrisintics.h" for 'candidate-optimziaiton'.

    for(uint i = 0; i < n1; i++) {
      const uint index = index1[i];
      //assert(index >= 0); // FIXME: remove this wehen we have change dthe index-lists to INT_MAX
      if( (index < 0) || (index >= nrows) || (index == UINT_MAX) ) return_distance = -1.0;
    }
    for(uint i = 0; i < n2; i++) {
      const uint index = index2[i];
      //assert(index >= 0); // FIXME: remove this wehen we have change dthe index-lists to INT_MAX
      if( (index < 0) || (index >= nrows) || (index == UINT_MAX) ) return_distance = -1.0;
    }
  
    if(return_distance != T_FLOAT_MAX) {
      return return_distance; //! ie, as the value has then been set.
    }

    //fprintf(stderr, "at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    
    t_float **data1 = data;  t_float **data2 = data; char **mask1 = mask; char **mask2 = mask;
    const t_float return_value = clusterdistance_fast(metric_id, typeOf_correlationPreStep, nrows, nrows, ncolumns, data1, data2, mask1, mask2, weight, n1, n2, index1, index2, method, isTo_use_continousSTripsOf_memory, isTo_invertMatrix_transposed, CLS, isTo_useImplictMask);  

    //! @return the result:
    return return_value;
  } else {
    // FIXME:  compare the time-difference wrt. 'the naive' call/operation (where the worst-case is for vertices to have a random sequence-roder).. 
    //! Construct a new dense matrix:
    //      t_float *data1_dense = allocate_2d_list_float(n1, ncolumns);
    // FIXME[performance]: compare the signficance of our [below] appraoch ... ie, wrt. 'avoding the need ror randomzied data-access' ... cosntructin g the 'order to rpcoess' using differnet randomzeid 'generatoris' (eg, "uniform" and "bionmoial" (oekseth, 06. sept. 2016)
    assert(transpose == false); //! ie, what we expect.
    t_float **data1_dense = build_denseMatrixOf_sparse_floats(n1, ncolumns, data, index1);
    char **mask1_dense = (mask) ? build_denseMatrixOf_sparse_chars(n1, ncolumns, mask, index1) : NULL;
    t_float *weight_1 = (weight) ? allocate_1d_list_float(n1, 0) : NULL;
    //t_float *weight_1 = (weight) ? allocate_2d_list_uint(n2, 0);
    t_float **data2_dense = data1_dense; char **mask2_dense = mask1_dense;
    if(index1 != index2) { //! then build two seperate matrcies, ie, for the comrpessed indices.
      assert(n2 > 0); assert(ncolumns > 0);
      data2_dense = build_denseMatrixOf_sparse_floats(n2, ncolumns, data, index2);
      assert(data2_dense);
      assert(data2_dense != data); //! ie, as we otehrwise need to add an 'if-calsue' to our-deallcoation-calls.
      mask2_dense = (mask) ? build_denseMatrixOf_sparse_chars(n2, ncolumns, mask, index2) : NULL;
    }
    if(weight) {	
      for(uint i = 0; i < n1; i++) {
	const uint k = (!index1) ? i : index1[i];
	weight_1[i] = weight[k];
      }
      // for(uint i = 0; i < n2; i++) {
      //   const uint k = (!index2) ? i : index2[i];
      //   weight_1[i] = weight[k];
      // }
    }
    //! Compute the result:
    assert(transpose == 0);
    
    if(
       (method_id == e_kt_clusterComparison_extreme_min)
       || (method_id == e_kt_clusterComparison_extreme_max)
       || (method_id == e_kt_clusterComparison_avg)
       || (method_id == e_kt_clusterComparison_total)
       ){
      assert(useMatrics_withoutIndexConversion == false); //! ie, 'the task applied in [above].
    }
    
    // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    // fprintf(stderr, "at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(metric_id != e_kt_correlationFunction_undef);
    const t_float return_value = clusterdistance_fast(metric_id, typeOf_correlationPreStep, n1, n2, ncolumns, data1_dense, data2_dense, mask1_dense, mask2_dense, weight_1, n1, n2, NULL, NULL, method, isTo_use_continousSTripsOf_memory, isTo_invertMatrix_transposed, CLS, isTo_useImplictMask);

    //! De-allcoate the locally reserved data:
    //free_2d_list_float(&data2_dense);
    const bool data1_eq_data2 = (data1_dense == data2_dense);
    const bool mask1_eq_mask2 = (mask1_dense == mask2_dense);
    if(data1_dense != data) {
      if(data1_dense) {free_2d_list_float(&data1_dense, n1); data1_dense = NULL;} 
    }
    if(mask1_dense) {free_2d_list_char(&mask1_dense, n1); mask1_dense = NULL;} 
    if(index1 != index2) { //! then build two seperate matrcies, ie, for the comrpessed indices.
      if(!data1_eq_data2 && data2_dense) {
	assert(data2_dense != data);
	free_2d_list_float(&data2_dense, n2); data2_dense = NULL;
      }
      if(mask2_dense && !mask1_eq_mask2) {free_2d_list_char(&mask2_dense, n2); mask2_dense = NULL;}
    }

    if(weight_1) {free_1d_list_float(&weight_1);}
    
    //! @return:
    return return_value;
  }
}

//! A function-permuation which use an enum to specify the method (oekseht, 0+6. des. 2016)
t_float clusterdistance__extensiveParams__enumMethod(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t enum_id_method, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_useImplictMask) {

  e_kt_clusterComparison_t method = enum_id_method;
  //const char method = get_char_fromEnum(enum_id_method);
    return clusterdistance__extensiveParams(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, n1, n2, index1, index2, method, transpose, isTo_use_continousSTripsOf_memory, isTo_invertMatrix_transposed, CLS, isTo_useImplictMask);
}

//! A simplifed permtuation of "clusterdistance__extensiveParams__enumMethod(..)" (oekseth, 06. des. 2016)
t_float clusterdistance__extensiveParams__enumMethod__simplified(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t enum_id_method) {
    assert(metric_id != e_kt_correlationFunction_undef);
    e_kt_clusterComparison_t method = enum_id_method;
    //const char method = get_char_fromEnum(enum_id_method);
    assert(e_kt_clusterComparison_undef != /*enum_id_method=*/enum_id_method);
    assert(metric_id != e_kt_correlationFunction_undef);
    return clusterdistance__extensiveParams(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, n1, n2, index1, index2, method, 
					    /*transpose=*/false,
					    /*isTo_use_continousSTripsOf_memory=*/true,
					    /*isTo_invertMatrix_transposed=*/true,
					    /*CLS=*/64,
					    /*isTo_useImplictMask=*/true);
}



//! Comptue the scalar simlairty between two different matrices (oekseth, 06. jan. 2017)
bool scalar__getSimliarty__betwenMatrices__kt_distance_cluster(const e_kt_clusterComparison_t clusterCmp_method_id, const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *data1, const s_kt_matrix_t *data2, t_float *scalar_result) {
  if(!data1 || !data2 || !scalar_result) {
    fprintf(stderr, "!!\t Inptu-params not as expected: pelase investigate. OBservation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up
    return false;
  }
  if(data1->ncols != data2->ncols) {
    fprintf(stderr, "!!\t Inptu-params not as expected: ncols(data1)=%u while ncols(data2)=%u, ie, an incosnstnecy: pelase investigate. OBservation at [%s]:%s:%d\n", data1->ncols, data2->ncols, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up
    return false;
  }
  if(data1->weight || data2->weight) {
    fprintf(stderr, "Option Not supported: while you specified the weight-table we have not yet support for using the weight-table: if the latter is of prtaical applicability and/or importance then please cotnact the senior devleoper at [oekseth@gmail.com]. Whill now continue assuming the weight-tables were Not set. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  
  bool isTo_useImplictMask = false;
  {
    t_float value_max = T_FLOAT_MIN_ABS;
    for(uint i = 0; i < data1->nrows; i++) {
      for(uint k = 0; k < data1->ncols; k++) {value_max = macro_max(value_max, data1->matrix[i][k]);}}
    if(value_max != T_FLOAT_MAX) {
      for(uint i = 0; i < data2->nrows; i++) {
	for(uint k = 0; k < data2->ncols; k++) {value_max = macro_max(value_max, data2->matrix[i][k]);}}
    }
    assert(value_max != T_FLOAT_MIN_ABS);
    isTo_useImplictMask = (value_max == T_FLOAT_MAX); //! ie, where the latter ouwld incirate the 'occurence' of implcit masks.
  }

  const uint n1 = data1->nrows;   const uint n2 = data2->nrows;
  const t_float return_value = clusterdistance_fast(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, n1, n2, data1->ncols, data1->matrix, data2->matrix, /*mask1=*/NULL, /*mask2=*/NULL, /*weight=*/NULL, n1, n2, /*index1=*/NULL, /*index2=*/NULL, clusterCmp_method_id, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_invertMatrix_transposed=*/true, /*CLS=*/64, isTo_useImplictMask);  
  if(isOf_interest(return_value) && (return_value != 0) ) { //! then we provide support for different psot-prcessing routines.
    
  }
  *scalar_result = return_value;
  //! At this exeuction-point we asusme the oepraiton was a success:
  return true; //(return_value != T_FLOAT_MAX);
}
