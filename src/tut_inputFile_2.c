#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks examplify the use of a 'advanced inptu-file specificaiton' through the "s_kt_matrix_fileReadTuning_t" into hpLysis-clustering, ie, in combiantion with the "traverse__s_hp_clusterFileCollection_traverseSpec(..)"   funciton in our "hp_clusterFileCollection.h".
   @remarks Demonstrates features wrt.:
   -- input: use of logics in "s_kt_matrix_t" to load input-files: examplfies both a 'direct' speicficaiton of attributes, and a geneirc stragty using lgoics and data-strucutres deifned in our "hp_clusterFileCollection.h"
   -- computation: use of different correlation/simliarty metrics
   -- result(a): how results from mmuliple clusterings may be exported into one single-unified java-script file.
   -- result(b): an iterative appraoch to print out cluster-conistnecy-scores when compating an input-amtrix to a godl-standard-set of clusters:
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;

  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //! ----------------------------------- 
  //! 
  //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
  //! ----------------------------------- 
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.

  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  if(
     true
/* #ifndef NDEBUG */
/* true */
/* #else */
/* false //! ie, use the large datg-aset if we are Not in debug-modues */
/* #endif */
) {
    fileRead_config__syn.imaginaryFileProp__nrows = 10;
    fileRead_config__syn.imaginaryFileProp__ncols = 10;
  } else { //! then we are interested in a more performacne-demanind approach: 
    fileRead_config__syn.imaginaryFileProp__nrows = 400;
    fileRead_config__syn.imaginaryFileProp__ncols = 400;
  }
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.



  const uint mapOf_realLife_size = 6 + 11 + 3; //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    // FIXME[article]: update our aritlce wrt. [”elow] 'note' tags/descipritons.
    // FIXME[article]:  ... 
    // FIXME[article]:  ... 
    //! -------------------------------------------------- 
    //! Note[<syntetic> w/cnt=6]: .... 
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,     metric__beforeClust, metric__insideClust, clusterAlg
     },
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! -------------------------------------------------- 
    //! 
    //! 
    //! Note[<real-life-small-data-sets> w/cnt=11]: 
    //! ---- 
    //! cnt_clusters=2, size=15; seperation=ambiguous-overlapping; Trait="curved, intersecting"; Euclid oversimplifies/'extremifies' the speration, while canberra is uanble to capture the 'curved relationships': the other metrics are unable to capture the relationjships. 
    {/*tag=*/"birthWeight_chinese", /*file_name=*/"tests/data/kt_mine/birthWeight_chineseChildren.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ... cnt_clusters=1, size=; seperation=periodic; Trait="curved, intersecting"; Euclid 
    {/*tag=*/"butterFat_cows", /*file_name=*/"tests/data/kt_mine/butterFat_percentage_cows.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! cnt_clusters=1, size=11; seperation='well-defined'; Trait="curved, x-axis-start-point differs"; Euclid 
    {/*tag=*/"fishGrowth", /*file_name=*/"tests/data/kt_mine/fish_growth.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ... cnt_clusters=2, size=4; seperation=ambiguous; Trait="curved, fluctating, discrete-sinus"; Euclid 
    // FIXME: consider using [below] as an example of 'periodic clsuters which are diicuflt to clsuter'.
    // FIXME: consider to transpose [below] .... figure out how to 'hanlde' the case where 'a ranking' would result in isngificnat seperation 
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ------ 
    //! 
    //! "Spottinggrade": cnt_clusters=2, size=4; seperation='change VS not-change, ie, a non-linear comparison'; Trait="curved, intersecting"; ....??... <-- asusmes other metrics will find thios challenging
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
  //! ... cnt_clusters=2, size=3; seperation='ambigious: silamirlty in curve-increase'; Trait="curved, simliar-peaks"; 
    {/*tag=*/"guinePigs", /*file_name=*/"tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_housed_pregnancy_first.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! "limesOxygen": if transposed nearly-all (ie, xmt. "species 2, 75 seawater") seems to have a simlar flucation
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ------ 
    //! 
    //! "Weedlength": cnt_clusters=1, size=~20; seperation='clear: samve dsicrete sinus-vurve wrt. flcutaitons ... Metrics: none of the compared metric manages to idneitfy/describe this relationship
    {/*tag=*/"weedLength", /*file_name=*/"tests/data/kt_mine/weed_length.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //{/*tag=*/"", /*file_name=*/"", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! -------------------------------------------------- 
    //! Note[]: 
    //! -------------------------------------------------- 
    //! Note[iris]: the results produced by/through MINE results in a considerably better/higher CCM-score (when comapred to Euclid), an observation which is consernsent between/across k-means-pmeruatiosn such as [mean, rank, medoid]. However, a challenge/sekenss wrt. the evlauatin of the IRIS data-set conserns the apriory-interpetaiton/expectaiton (where the researhcers states/assuems tha tehre are three clsuters i IIRS, while currnet cluster-metrics/CCMs indicates that the latter is Not supported), an observaiotn which may imply that (when considering real-lfie dat-asets) we ....??... <--- try to use the altter observiaotn as a jumping-store to ....??... 
    //! Note[iris]: k-means: "rank" VS "avg": while the use of a simlairty-pre-step results in the "avg" and "rank" to 'produce' exaclty the same results, the results (converted to sclar-numbers through CCMs) indicates that the algorithsm 'produce' a 2x-difference (when comptuign sepreately for "rank" and "avg" while Not using/appliying an Eculidcian sim-metric-pre-step). In brief, when comparing/evaluating the dfiferences between k-means with--withouth the 'correlation-pre-step', we clearly observe that the clsuter-algrotihms are stronlgy influence by any 'data-normalizaiton-steps' (illustrated in the latter through our applicaiton/use of Euclid as a simalrity-pre-step appraoch). 
    //! Note[iris]: ..     
    //! Note[iris, cluster="kMeans--[AVG, rank, emdoid]"]: metric="Mine": clusters are in either group(0) OR group(1); metric="Euclid": result simialr to "scikit-learn" (ie, where group(0) in MINE is exaclty simliar, while group(1 =~ 2) (a result which is simlair to the 'defualt answer'); <-- FIXME: try expalinign/arguing for why the MINE-impelmetnation may be correct.
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/4, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //{/*tag=*/, /*file_name=*/, /*fileRead_config=*/fileRead_config,/*k_clusterCount=*/, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/, /*metric__insideClust=*/, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_},
    //! Note[iris, cluster="kMeans--medoid"]: metric="Mine": ... ; metric="Euclid": ... 
    //! Note[iris, cluster=""]: metric="Mine": ... ; metric="Euclid": ... 
    //! Note[iris, cluster=""]: metric="Mine": ... ; metric="Euclid": ... 
    //! -------------------------------------------------- 
  };


  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------


  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
