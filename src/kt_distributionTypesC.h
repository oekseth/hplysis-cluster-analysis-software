#ifndef kt_distributionTypesC_h
#define kt_distributionTypesC_h


#include "kt_matrix.h"
#include "kt_list_1d_string.h"

/**
   @file
   @brief provide logics for analsying the effect of different data-distrubtions; simplfies access to distrubion-types..
   @date 06.08.2020
   @author oekseth
 **/
// FIXME: when below code-chunk is used in more classes, then move it to a seperate class/header-file.
#include <random> //! used for the distributions. <-- TODO: if this example is to be supporteed by the C-language, then update below code-chunk

/**
   @enum e_distributionTypesC
   @brief lists the random-types included in C++' random.h header-file.
   @remarks 
   -- convention: the naming (of the enums) correpsonds bithto the hpLysis noimenckalture (of starting enums with an "e_" prefix, followed by the struct/class name (which it is stronlgy asosted to).
   -- specific: the remainder of the enum-attribute-names reflects the varialbe-names found in "s_distributionTypesC_globalSpec"; hence, simplfies searhces for uccrecnes, and usages, of these distributions.
 **/
typedef enum e_distributionTypesC {
  e_distributionTypesC_d_student_t,
  e_distributionTypesC_d_bernoulli_distribution, //!,
  e_distributionTypesC_d_binomial_distribution, //! disccreate,
  e_distributionTypesC_d_cauchy_distribution,
  e_distributionTypesC_d_chi_squared_distribution,
  e_distributionTypesC_d_discrete_distribution,
  e_distributionTypesC_d_exponential_distribution, //! exponentional, contiuous distribution,
  e_distributionTypesC_d_extreme_value_distribution, //! andom number-distribution,
  e_distributionTypesC_d_fisher_f_distribution, //! random number distribution,
  e_distributionTypesC_d_gamma_distribution, //! continuous distribution,
  e_distributionTypesC_d_geometric_distribution, //! discreate,
  e_distributionTypesC_d_lognormal_distribution, //! random number distribution,
  e_distributionTypesC_d_negative_binomial_distribution, //! random number distribution,
  e_distributionTypesC_d_normal_distribution, //! continuous distribution for random numbers,
  e_distributionTypesC_d_piecewise_constant_distribution, //! random number distribution,
  //  e_distributionTypesC_d_piecewise_linear_distribution, //! random number distribution, % FXIME: ... dropped, as it seems to be a emmorye-rror in oru aprpaoch ... suggestions for why?
  e_distributionTypesC_d_poisson_distribution, //! disceaete random numbers,
  e_distributionTypesC_d_uniform_int_distribution, //! randm numbes,
  e_distributionTypesC_d_uniform_real_distribution, //! Uniform continuous distribution for random numbers,
  e_distributionTypesC_d_weibull_distribution, //! adnom numbers,

  //! --------------
  e_distributionTypesC_undef
} e_distributionTypesC_t;

/**
   @brief a crude appraoch for templating different C++ distribution-types.
 **/
typedef class s_distributionTypesC_globalSpec {
public:
  //! ---------------
  e_distributionTypesC_t distribution_id;
  std::default_random_engine generator;
  //! ---------------
  std::student_t_distribution<t_float> d_student;
  std::bernoulli_distribution d_bernoulli_distribution; //!
  std::binomial_distribution<int> d_binomial_distribution; //! disccreate
  std::cauchy_distribution<t_float> d_cauchy_distribution;
  std::chi_squared_distribution<t_float> d_chi_squared_distribution;
  std::discrete_distribution<int> d_discrete_distribution;
  std::exponential_distribution<t_float> d_exponential_distribution; //! exponentional, contiuous distribution
  std::extreme_value_distribution<t_float> d_extreme_value_distribution; //! andom number-distribution
  std::fisher_f_distribution<t_float> d_fisher_f_distribution; //! random number distribution
  std::gamma_distribution<t_float> d_gamma_distribution; //! continuous distribution
  std::geometric_distribution<int> d_geometric_distribution; //! discreate
  std::lognormal_distribution<t_float> d_lognormal_distribution; //! random number distribution
  std::negative_binomial_distribution<int> d_negative_binomial_distribution; //! random number distribution
  std::normal_distribution<t_float> d_normal_distribution; //! continuous distribution for random numbers
  std::piecewise_constant_distribution<t_float> d_piecewise_constant_distribution; //! random number distribution
  //  std::piecewise_linear_distribution<t_float> d_piecewise_linear_distribution; //! random number distribution
  std::poisson_distribution<int> d_poisson_distribution; //! disceaete random numbers
  std::uniform_int_distribution<int> d_uniform_int_distribution; //! randm numbes
  std::uniform_real_distribution<t_float> d_uniform_real_distribution; //! Uniform continuous distribution for random numbers
  std::weibull_distribution<t_float> d_weibull_distribution; //! adnom numbers
} s_distributionTypesC_globalSpec_t;

s_distributionTypesC_globalSpec_t init_s_distributionTypesC_globalSpec_t(const e_distributionTypesC_t distribution_id, const uint center_index) {
  s_distributionTypesC_globalSpec_t self;
  self.distribution_id = distribution_id;
  // FIXME: paper: sould we incldue the equations defnin blow ... such as those listed at "http://www.cplusplus.com/reference/random/piecewise_constant_distribution/" ...??..
  
  //!
  //!
  switch(self.distribution_id) {
  case e_distributionTypesC_d_student_t : {self.d_student = std::student_t_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_bernoulli_distribution : {self.d_bernoulli_distribution = std::bernoulli_distribution(center_index);  break;  }
  case e_distributionTypesC_d_binomial_distribution : {self.d_binomial_distribution = std::binomial_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_cauchy_distribution: {self.d_cauchy_distribution = std::cauchy_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_chi_squared_distribution: {self.d_chi_squared_distribution = std::chi_squared_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_discrete_distribution: {self.d_discrete_distribution = std::discrete_distribution<int>(center_index, center_index * 100);  break;  } // FIXME: isntad of this ... should we add extre construaion-paratmers ... to cpture the broad range ... as seen at: "http://www.cplusplus.com/reference/random/discrete_distribution/discrete_distribution/" ??
  case e_distributionTypesC_d_exponential_distribution: {self.d_exponential_distribution = std::exponential_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_extreme_value_distribution: {self.d_extreme_value_distribution = std::extreme_value_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_fisher_f_distribution: {self.d_fisher_f_distribution = std::fisher_f_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_gamma_distribution: {self.d_gamma_distribution = std::gamma_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_geometric_distribution: {self.d_geometric_distribution = std::geometric_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_lognormal_distribution: {self.d_lognormal_distribution = std::lognormal_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_negative_binomial_distribution: {self.d_negative_binomial_distribution = std::negative_binomial_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_normal_distribution: {self.d_normal_distribution = std::normal_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_piecewise_constant_distribution: {
    // FIXME: function-input-args: any better strategy than usign below configuraiton as gneeric input?
    //! Src: http://www.cplusplus.com/reference/random/piecewise_constant_distribution/
    const t_float p = (t_float)center_index;
    std::array<t_float, 6> intervals {0.0, 2*p, 4*p, 6*p, 8*p, 10*p};
    std::array<t_float, 5> weights {2.0, 1.0, 2.0, 1.0, 2.0};
    //!
    self.d_piecewise_constant_distribution = std::piecewise_constant_distribution<t_float>(intervals.begin(),intervals.end(),weights.begin());
    break;  }
    /*
  case e_distributionTypesC_d_piecewise_linear_distribution:  {
    //! Src: http://www.cplusplus.com/reference/random/piecewise_linear_distribution/
    // FIXME: function-input-args: any better strategy than usign below configuraiton as gneeric input?
    const t_float p = (t_float)center_index;
    std::array<t_float, 6> intervals {0.0, 2*p, 4*p, 6*p, 8*p, 10*p};
    std::array<t_float, 5> weights {2.0, 1.0, 2.0, 1.0, 2.0};
    fprintf(stderr, "\t(info)\t init:\t self.d_piecewise_linear_distribution, at %s:%d\n", __FILE__, __LINE__); // FIXME: remove.
    self.d_piecewise_linear_distribution = std::piecewise_linear_distribution<t_float>(intervals.begin(),intervals.end(),weights.begin());  break;  }
    */
  case e_distributionTypesC_d_poisson_distribution: {self.d_poisson_distribution = std::poisson_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_uniform_int_distribution: {self.d_uniform_int_distribution = std::uniform_int_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_uniform_real_distribution : {self.d_uniform_real_distribution = std::uniform_real_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_weibull_distribution: {self.d_weibull_distribution = std::weibull_distribution<t_float>(center_index);  break;  }
    // case  : {self.d_ = std::<t_float>(center_index);  break;  }        
  default:
    fprintf(stderr, "!!\t Input NOT valid, given distribution_id=%u, at [%s]:%d", distribution_id, __FILE__, __LINE__);
    assert(false); //! as we did NOIT expect this case
    break;
  }
  //!
  //! @return
  return self;
}


//! Compute distributions:
s_kt_list_1d_float_t get_values_s_distributionTypesC_t(s_distributionTypesC_globalSpec_t *self, const uint nrows) {
  assert(nrows > 0);
  //! Note: see: http://www.cplusplus.com/reference/random/chi_squared_distribution/chi_squared_distribution/ .... then sumamrise the ideas in ... https://medium.com/@ciortanmadalina/overview-of-data-distributions-87d95a5cbf0a
  s_kt_list_1d_float_t vec_scores = init__s_kt_list_1d_float_t(nrows);

    //double number = (self.distribution)->(self.generator);
    //double number = 
#define get_DistScore(distribution) ({for(uint i = 0; i < nrows; i++) {const t_float score = distribution(self->generator); vec_scores.list[i] = score;}}) //! ie, return the score; motivation is to reduce the code-bloat (when using the tempatled random.h-C++-libarary.
  
  //!
  //!
  switch(self->distribution_id) {
  case e_distributionTypesC_d_student_t : {get_DistScore(self->d_student); break;} // = std::student_t_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_bernoulli_distribution : {get_DistScore(self->d_bernoulli_distribution); break;} // = std::bernoulli_distribution(center_index);  break;  }
  case e_distributionTypesC_d_binomial_distribution : {get_DistScore(self->d_binomial_distribution); break;} // = std::binomial_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_cauchy_distribution: {get_DistScore(self->d_cauchy_distribution); break;} // = std::cauchy_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_chi_squared_distribution: {get_DistScore(self->d_chi_squared_distribution); break;} // = std::chi_squared_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_discrete_distribution: {get_DistScore(self->d_discrete_distribution); break;} // = std::discrete_distribution<int>(center_index, center_index * 100);  break;  } // FIXME: isntad of this ... should we add extre construaion-paratmers ... to cpture the broad range ... as seen at: "http://www.cplusplus.com/reference/random/discrete_distribution/discrete_distribution/" ??
  case e_distributionTypesC_d_exponential_distribution: {get_DistScore(self->d_exponential_distribution); break;} // = std::exponential_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_extreme_value_distribution: {get_DistScore(self->d_extreme_value_distribution); break;} // = std::extreme_value_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_fisher_f_distribution: {get_DistScore(self->d_fisher_f_distribution); break;} // = std::fisher_f_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_gamma_distribution: {get_DistScore(self->d_gamma_distribution); break;} // = std::gamma_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_geometric_distribution: {get_DistScore(self->d_geometric_distribution); break;} // = std::geometric_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_lognormal_distribution: {get_DistScore(self->d_lognormal_distribution); break;} // = std::lognormal_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_negative_binomial_distribution: {get_DistScore(self->d_negative_binomial_distribution); break;} // = std::negative_binomial_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_normal_distribution: {get_DistScore(self->d_normal_distribution); break;} // = std::normal_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_piecewise_constant_distribution: {
    // FIXME: function-input-args: any better strategy than usign below configuraiton as gneeric input?
    //! Src: http://www.cplusplus.com/reference/random/piecewise_constant_distribution/
    /* const t_float p = (t_float)center_index; */
    /* std::array<t_float, 6> intervals {0.0, 2*p, 4*p, 6*p, 8*p, 10*p}; */
    /* std::array<t_float, 5> weights {2.0, 1.0, 2.0, 1.0, 2.0}; */
    //!
    get_DistScore(self->d_piecewise_constant_distribution); break;} // = std::piecewise_constant_distribution<t_float>(intervals.begin(),intervals.end(),weights.begin());
    //  case e_distributionTypesC_d_piecewise_linear_distribution:  {    get_DistScore(self->d_piecewise_linear_distribution); break;} // = std::piecewise_linear_distribution<t_float>(intervals.begin(),intervals.end(),weights.begin());  break;  }
  case e_distributionTypesC_d_poisson_distribution: {get_DistScore(self->d_poisson_distribution); break;} // = std::poisson_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_uniform_int_distribution: {get_DistScore(self->d_uniform_int_distribution); break;} // = std::uniform_int_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_uniform_real_distribution : {get_DistScore(self->d_uniform_real_distribution); break;} // = std::uniform_real_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_weibull_distribution: {get_DistScore(self->d_weibull_distribution); break;} // = std::weibull_distribution<t_float>(center_index);  break;  }
    // case  : {get_DistScore(self->d_ = std::<t_float>(center_index);  break;  }        
  default:
    fprintf(stderr, "!!\t Input NOT valid, given distribution_id=%u, at [%s]:%d", self->distribution_id, __FILE__, __LINE__);
    assert(false); //! as we did NOIT expect this case
    break;
  }

  //!
  //! 
  return vec_scores;
#undef get_DistScore //! ie, to avodi odd-behaing code (eg, as a result of a future copy-past-operaiton).
}

//! @reutrn the string-repreenation of the enum
static const char *getStringOfDistribution_s_distributionTypesC_globalSpec(const s_distributionTypesC_globalSpec_t *self) {
    //double number = (self.distribution)->(self.generator);
    //double number = 
  const char *ret_val =  "error";
#define get_DistScore(str) ({ret_val = str;})
  
  //!
  //!
  switch(self->distribution_id) {
  case e_distributionTypesC_d_student_t : {get_DistScore("student-t"); break;} // = std::student_t_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_bernoulli_distribution : {get_DistScore("bernoulli"); break;} // = std::bernoulli_distribution(center_index);  break;  }
  case e_distributionTypesC_d_binomial_distribution : {get_DistScore("binomial"); break;} // = std::binomial_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_cauchy_distribution: {get_DistScore("cauchy"); break;} // = std::cauchy_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_chi_squared_distribution: {get_DistScore("chi-squared"); break;} // = std::chi_squared_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_discrete_distribution: {get_DistScore("discrete"); break;} // = std::discrete_distribution<int>(center_index, center_index * 100);  break;  } // FIXME: isntad of this ... should we add extre construaion-paratmers ... to cpture the broad range ... as seen at: "http://www.cplusplus.com/reference/random/discrete_distribution/discrete_distribution/" ??
  case e_distributionTypesC_d_exponential_distribution: {get_DistScore("exponential"); break;} // = std::exponential_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_extreme_value_distribution: {get_DistScore("extreme-value"); break;} // = std::extreme_value_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_fisher_f_distribution: {get_DistScore("fisher-f"); break;} // = std::fisher_f_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_gamma_distribution: {get_DistScore("gamma"); break;} // = std::gamma_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_geometric_distribution: {get_DistScore("geometric"); break;} // = std::geometric_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_lognormal_distribution: {get_DistScore("lognormal"); break;} // = std::lognormal_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_negative_binomial_distribution: {get_DistScore("negative-binomial"); break;} // = std::negative_binomial_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_normal_distribution: {get_DistScore("normal"); break;} // = std::normal_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_piecewise_constant_distribution: {    get_DistScore("piecewise-constant"); break;} // = std::piecewise_constant_distribution<t_float>(intervals.begin(),intervals.end(),weights.begin());
    //  case e_distributionTypesC_d_piecewise_linear_distribution:  {    get_DistScore("piecewise-linear"); break;} // = std::piecewise_linear_distribution<t_float>(intervals.begin(),intervals.end(),weights.begin());  break;  }
  case e_distributionTypesC_d_poisson_distribution: {get_DistScore("poisson"); break;} // = std::poisson_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_uniform_int_distribution: {get_DistScore("uniform-int"); break;} // = std::uniform_int_distribution<int>(center_index);  break;  }
  case e_distributionTypesC_d_uniform_real_distribution : {get_DistScore("uniform-real"); break;} // = std::uniform_real_distribution<t_float>(center_index);  break;  }
  case e_distributionTypesC_d_weibull_distribution: {get_DistScore("weibull"); break;} // = std::weibull_distribution<t_float>(center_index);  break;  }
    // case  : {get_DistScore(" = std::<t_float>(center_index);  break;  }        
  default:
    fprintf(stderr, "!!\t Input NOT valid, given distribution_id=%u, at [%s]:%d", self->distribution_id, __FILE__, __LINE__);
    assert(false); //! as we did NOIT expect this case
    break;
  }
  //!
  //! 
  return ret_val;
#undef get_DistScore //! ie, to avodi odd-behaing code (eg, as a result of a future copy-past-operaiton).  
}

/**
   @brief a wrapper for easying an enumerated access to different data-distributions
 **/
typedef class s_distributionTypesC // : s_distributionTypesC_globalSpec_t 
{
public:
  e_distributionTypesC_t distribution_id;
  uint center_index;
  //void (*distribution)(std::default_random_engine);
  s_distributionTypesC_globalSpec_t  obj_distribution;
  //auto distribution;
  //void *distribution;
} s_distributionTypesC_t;


static s_distributionTypesC_t init_s_distributionTypesC(const e_distributionTypesC_t distribution_id, const uint center_index) {
  s_distributionTypesC_t self;
  self.distribution_id = distribution_id;
  self.center_index    = center_index;
  //! Init distributions:
  self.obj_distribution = init_s_distributionTypesC_globalSpec_t(distribution_id, center_index);
  //!
  return self;
}


//! @reutrn the string-repreenation of the enum
static const char *getStringOfDistribution_s_distributionTypesC(const s_distributionTypesC_t *self) {
  return getStringOfDistribution_s_distributionTypesC_globalSpec(&(self->obj_distribution));
}


//! @return a list of values in the given distribution.
static s_kt_list_1d_float_t get_values_s_distributionTypesC_t(s_distributionTypesC_t *self, const uint nrows) {
  assert(nrows > 0);
  return get_values_s_distributionTypesC_t(&(self->obj_distribution), nrows);
}




#endif //! #define kt_distributionTypesC_h
