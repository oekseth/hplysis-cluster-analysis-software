//! ****************************************************************************************
//! ******************************** Configure ********************************************************
//! -------------------------------------------------------------------------------------
const uint mapOf_slowMetricsDistance_size = 8;
const char mapOf_slowMetricsDistance[mapOf_slowMetricsDistance_size] = {'e', 'b', 'c', 
									'a', 'u', 'x', 
									's', 'k'};
const char *mapOf_slowMetricsDistance_stringOfDesc[mapOf_slowMetricsDistance_size] = {
  "Euclid", "cityBlock", "Pearson-productMomemt-generic",
  "Pearson-productMomemt-absolute", "Pearson-productMomemt-uncentered", "Pearson-productMomemt-uncentered-absolute",
  "Spearman", "Kendall"
};
//! -------------------------------------------------------------------------------------
const uint mapOf_clusterCmpMetrics_size = 5;
const char mapOf_clusterCmpMetrics[mapOf_clusterCmpMetrics_size] = {'a', 'm', 's', 
								    'x', 'v'};
const char *mapOf_clusterCmpMetrics_stringOfDesc[mapOf_clusterCmpMetrics_size] = {
  "mean-aritmetics", "median-aritmetics",
  "extreme-minimum",   "extreme-maximum", "average"
};
//! -------------------------------------------------------------------------------------
const uint mapOf_clusterFactions_size = 8;
/* const t_float mapOf_clusterFactions[mapOf_clusterFactions_size] = { */
/*   0.1, 0.2, 0.3, 0.4, 0.5, 0.6,  */
/* }; */



//! -------------------------------------------------------------------------------------
//! -------------------------------------------------------------------------------------
//! ********************************** Evaluate/apply for diffent 'spreads' of cluster-memberships (eg, to evaluate for differnet 'fractiosn' of memory-cahcem-misses, relating the latter to the exeuciton-time) ******************************************************
{ //! Comptue a scalar to rpesent/descirbe the similarity between two clsuters using a navie software-implementaiton:
  {const char *stringOf_measureText_base = "two-cluster-comparison(overlapping set of  entites)";
#define __localConfig__constructIndex1() ({ for(uint i = 0; i < n1; i++) {mapOf_index1[i] = i;}})
#define __localConfig__constructIndex2() ({ for(uint i = 0; i < n2; i++) {mapOf_index2[i] = i;}})
    //! Apply approx. 2^6=64 differnet tests:
#if(__localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics == 1)
#include "measure__stub__2d__naive_kt_distance_cluster__stub__slowNaiveComputation.c"
#else
#include "measure__stub__2d__naive_kt_distance_cluster__stub__fast.c"
#endif
  }
  {const char *stringOf_measureText_base = "two-cluster-comparison(disjoint set of  entites)";
#define __localConfig__constructIndex1() ({ for(uint i = 0; i < n1; i++) {mapOf_index1[i] = i;}})
#define __localConfig__constructIndex2() ({ for(uint i = 0; i < n2; i++) {mapOf_index2[i] = n1 + i;}})
    //! Apply approx. 2^6=64 differnet tests:
#if(__localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics == 1)
#include "measure__stub__2d__naive_kt_distance_cluster__stub__slowNaiveComputation.c"  
#else
#include "measure__stub__2d__naive_kt_distance_cluster__stub__fast.c"
#endif
  }
  {const char *stringOf_measureText_base = "two-cluster-comparison(disjoint set of  entites: reverse)";
#define __localConfig__constructIndex1() ({ for(uint i = 0; i < n1; i++) {mapOf_index1[i] = nrows - i;}})
#define __localConfig__constructIndex2() ({ for(uint i = 0; i < n2; i++) {const int new_index = (nrows - n1) + i; assert(new_index >= 0); mapOf_index2[i] = (uint)new_index;}})
    //! Apply approx. 2^6=64 differnet tests:
#if(__localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics == 1)
#include "measure__stub__2d__naive_kt_distance_cluster__stub__slowNaiveComputation.c"       
#else
#include "measure__stub__2d__naive_kt_distance_cluster__stub__fast.c"
#endif
  }
  {const char *stringOf_measureText_base = "two-cluster-comparison(disjoint set of  entites: every-second...and-possible-overlapping)";
#define __localConfig__constructIndex1() ({ int prev_index = 0; for(uint i = 0; i < n1; i++) {prev_index = prev_index+2; if(prev_index > nrows) {prev_index = 0;} mapOf_index1[i] = (uint)prev_index;}})
#define __localConfig__constructIndex2() ({ int prev_index = n1; for(uint i = 0; i < n2; i++) {prev_index = prev_index+2; if( (prev_index > nrows) || (prev_index < 0) ) {prev_index = 0;} mapOf_index1[i] = (uint)prev_index;}})
    //! Apply approx. 2^6=64 differnet tests:
#if(__localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics == 1)
#include "measure__stub__2d__naive_kt_distance_cluster__stub__slowNaiveComputation.c"  
#else
#include "measure__stub__2d__naive_kt_distance_cluster__stub__fast.c"
#endif
  }
}
//! ****************************************************************************************
#undef __localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics
