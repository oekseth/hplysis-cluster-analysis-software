#include "kt_hash_string.h"
/* #include "kt_sim_string.h" //! ie, the simplifed API for hpLysis */
/* #include "kt_sparse_sim.h" //! where the latter is used as an altneritve to the "kt_sim_string", ie, to afcilaitet/supprot comptaution of "Pairwise simalrity-meitrcs (PSMs)" */
#include <time.h>    // time()


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief a simple to valite logics in our "kt_hash_string.h"
   @author Ole Kristian Ekseth (oekseth, 06. aug. 2017).
**/
int main() 
#else
  void tut_hash_1_duplicateKeys()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  const uint list_str_size = 3+3+2;
  char *list_str[list_str_size] = {
    "hamsun", "hamsum", "gamsun",
    // ---
    "famsun", "hamsuz",
    //! Note[below]: examplify for use-cases where the strings are 'xtremely dis-simliar .... to goal/Motivaiotn is to vlaidte and exmply cases where score-differences is profunc/makred .... from the [”elow] results we observe how (among others) the "levenshtein-distance" manages to capture our itnal/prioer expeciotan/assumptiosnw rt. silairty.
    "lavransdatter", "lavran", "la",
  };
  //! 
  //! Intiate:
  s_kt_hash_string_t obj_hash = init__s_kt_hash_string_t();
  //! 
  //! 
  for(uint cnt_iter = 0; cnt_iter < 10; cnt_iter++) {
    for(uint i = 0; i < list_str_size; i++) {
      //    if(i == k) {continue;} // TODO: consider removing this.
      //! 
      //! Simply access to the strings:
      const char *str1 = list_str[i];       
      //! Validate that the strings are set:
      assert(str1); assert(strlen(str1));
      //!
      //! Apply logics: get string:
      uint key_str = get_key__kt_hash_string(&obj_hash, str1);
      if(cnt_iter > 0) {assert(key_str == i);}
      else {assert(key_str == UINT_MAX);} //! ie, as the key is then expected to Not ahve been inserted.
      
      //!
      //! Apply logics: add to string:
      key_str = add_string__kt_hash_string(&obj_hash, str1);
      assert(key_str == i);
      //!
      //! Apply logics: get string:
      key_str = get_key__kt_hash_string(&obj_hash, str1);
      assert(key_str == i);
    }
  }
  //!
  //! Write out keys:
  dump__s_kt_hash_string_t(&obj_hash, stdout);
  //!
  //! De-allocate: 
  free__s_kt_hash_string_t(&obj_hash);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

