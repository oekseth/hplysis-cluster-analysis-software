#include "kt_assessPredictions.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
//#include "kt_terminal.h"
//#include "measure_base.h"
#include "kt_api_config.h"
#include "kt_distance.h" //! which (among others) is used for comptatuion of corrleation-atmrix in oru [”elow] exampels.
#include "kt_api.h" //! which (among others) is used for comptatuion of corrleation-atmrix in oru [”elow] exampels.
#include "correlation_base.h"
#include "correlation_macros__distanceMeasures.h"
#include "kt_matrix_cmpCluster.h"
#include "kt_matrix_filter.h"


//! @return an intiated version of the "s_kt_assessPredictions__clusterProp__dataAmbiguity_t" object.
s_kt_assessPredictions__clusterProp__dataAmbiguity_t setToEmpty__s_kt_assessPredictions__clusterProp__dataAmbiguity_t() {
  s_kt_assessPredictions__clusterProp__dataAmbiguity_t self;
  self.expected_STD = T_FLOAT_MAX;
  self.expected_kurtosis = T_FLOAT_MAX;
  self.expected_skewness = T_FLOAT_MAX;
  self.expected_studentT__Pearson  = T_FLOAT_MAX;
  self.expected_studentT__Spearman = T_FLOAT_MAX;

  //! @return
  return self;
}

//! @return an inlitazed object of type s_kt_assessPredictions_t using the default intalizaitons.
s_kt_assessPredictions_t setToEmpty__s_kt_assessPredictions_t() {
  s_kt_assessPredictions_t self;
  self.clusterProp__dataAmbiguity = setToEmpty__s_kt_assessPredictions__clusterProp__dataAmbiguity_t();
  self.corr_metricObj__ideal.metric_id = e_kt_correlationFunction_groupOf_MINE_mic;
  self.corr_metricObj__ideal.typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_none;
  //! @return
  return self;
}


//! Focus on agreement between differnet cluster-comparison-metrics wrt. a given set of clsuter-predicitosn, where a gold-standard is used to 'rank' the different predicitons
void standAlone__describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__s_kt_assessPredictions_t(const uint cnt_vertices, const uint cnt_clusters, const char *stringOf_resultPrefix, uint **clusterPredictionDataSets__set1, const uint clusterPredictionDataSets__set1_size, uint **clusterPredictionDataSets_set2, const uint clusterPredictionDataSets_set2_size, t_float *arrOf_1d_dataXdata_goldStandard, t_float **squaredDistanceMatrix__1, t_float **squaredDistanceMatrix__2, t_float **squaredDistanceMatrix__3, const t_float config_min_rankThreshold, s_kt_assessPredictions_differentMetricSets_t *obj_result) {
  assert(obj_result);
  s_kt_longFilesHandler *fileHandler__global = &(obj_result->fileHandler);


  const uint cnt_data_nrows = clusterPredictionDataSets__set1_size;  const uint cnt_data_ncols = clusterPredictionDataSets_set2_size;
  const char *prefix__clusterSets = "data";
    //! -----------------------------------------------------------------
    //! Compare different partitions for the data:
#define arrOf_2d_data__set1 clusterPredictionDataSets__set1
#define arrOf_2d_data__set2 clusterPredictionDataSets_set2
#define arrOf_1d_dataXdata_goldStandard arrOf_1d_dataXdata_goldStandard
    //! --------------------
    // Define the number-and-value of the distance-correlation-metices in deimsnion [cnt_vertices, cnt_vertices] to use:
  uint cnt_matricesToEvaluate = 0;
  if(squaredDistanceMatrix__1) {cnt_matricesToEvaluate++;}
  if(squaredDistanceMatrix__2) {cnt_matricesToEvaluate++;}
  if(squaredDistanceMatrix__3) {cnt_matricesToEvaluate++;}
  assert(cnt_matricesToEvaluate > 0); //! ie, what we expect.
#define distMatrix_0 squaredDistanceMatrix__1
#define distMatrix_1 squaredDistanceMatrix__2
#define distMatrix_2 squaredDistanceMatrix__3
    //! -----------------------------------------------------
    //! -----------------------------------------------------
    //! 
    //! Apply the logics:
#include "kt_assessPredictions__stub__clusterMetricCmp__main__wrapper.c"
    //! ------------------------

  //! ----------------------------------------
  //! 
  //! De-allocate:
  //fileHandler__global__ = *fileHandler__global;
}


//! Evaluate the accuracy of the new-constructed distance-matrix: compare the distance-matrix using different matrix-comparison-functions specified in our "s_kt_matrix_cmpCluster_clusterDistance_t"
void standAlone__describeClusterCmpSimilarities_forDataSet__s_kt_assessPredictions_t(const uint *mapOf_nullHypothesis_vertexTo_centroidId, const s_kt_matrix_t *matrix_afterCorrelation,  
										     //s_kt_matrix_t *matrixTo_evaluate, 
										     const uint ideal_clusterCnt, bool inputMatrix__isAnAdjecencyMatrix, const char *stringOf_resultPrefix, const t_float config_min_rankThreshold) {
  s_kt_longFilesHandler fileHandler__global__ = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/NULL, /*file_nameMeta=*/"metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
  s_kt_longFilesHandler *fileHandler__global = &fileHandler__global__;


  //! ----
  assert(config_min_rankThreshold <= 1);
  assert(config_min_rankThreshold > 0);
  if( (config_min_rankThreshold <= 0) || (config_min_rankThreshold > 1) ) {
    fprintf(stderr, "!!\t Yoru rank-trheshold=%f was not as expected: we assumed tha tlter shoudl be in [0.0001, 1], which is Not the case, ie, please udpate your call. Observation at [%s]:%s:%d\n", config_min_rankThreshold, __FUNCTION__, __FILE__, __LINE__);
  }
  //! -----------------------------------------------------------
  //!

  char nameOf_fileToExport_clusterCmpAssessment[1000] = {'0'}; sprintf(nameOf_fileToExport_clusterCmpAssessment, "%s_kMeansClusterCmp", stringOf_resultPrefix);            

  //! 
  //! Initaite the cluster-matrix-result-object:
  uint cnt_cmpMetricsToEvaluate = 0; uint cnt_corrMetrics = 0;
  { //! Idneitfy the number of rows and columsn:
    //!
    //! Idneitfy the number of rows:
    for(uint cmpType_id = 0; cmpType_id <= (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns; cmpType_id++) {
      for(uint cmpType_subType_id = 0; cmpType_subType_id < (uint)e_kt_matrix_cmpCluster_clusterDistance__config_metric__default; cmpType_subType_id++) {
	cnt_cmpMetricsToEvaluate++;
      }
    }
    assert(cnt_cmpMetricsToEvaluate > 0);
    //!
    //! Idneitfy the number of columns:
    for(uint cluster_cmp_id = 0; cluster_cmp_id < e_kt_clusterComparison_undef; cluster_cmp_id++) {
      e_kt_clusterComparison_t metric_clusterCmp_between = (e_kt_clusterComparison_t)cluster_cmp_id;
      assert(metric_clusterCmp_between != e_kt_clusterComparison_undef);
      //! Iterate through the different cluster-comparison-metrics: the post-process-types:
      for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
	for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	  e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; 
	  if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	     (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	     || 
#endif
	     describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	     || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	     ) {continue;} //! ie, as we then assuem 'this is Not of interest'
	  //! Udpat ehte count 'for the itnersting corr-emtics':
	  cnt_corrMetrics++;
	}
      }
    }
    assert(cnt_corrMetrics > 0);
  }
  assert(cnt_cmpMetricsToEvaluate > 0); assert(cnt_corrMetrics > 0);
  s_kt_matrix_t matrix__findClusterCmpCategories; //setTo_empty__s_kt_matrix_t(&matrix__findClusterCmpCategories);       
  init__s_kt_matrix(&matrix__findClusterCmpCategories, cnt_cmpMetricsToEvaluate, cnt_corrMetrics, /*isTo_allocateWeightColumns=*/false);      
      
  //!
  //! Iterate through the different cases wrt.
  uint row_id_global = 0;
  for(uint cmpType_id = 0; cmpType_id <= (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns; cmpType_id++) {
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t cmpType = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)cmpType_id;
    for(uint cmpType_subType_id = 0; cmpType_subType_id < (uint)e_kt_matrix_cmpCluster_clusterDistance__config_metric__default; cmpType_subType_id++) {
      const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmpType_subType = (e_kt_matrix_cmpCluster_clusterDistance__config_metric__t)cmpType_subType_id;
      { //! Build a string-reprsentaiton and upate the matrix:
	char string_local[1000] = {'\0'};
	sprintf(string_local, "%s::%s", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(cmpType), getString__e_kt_matrix_cmpCluster_clusterDistance__config_metric__t(cmpType_subType));
	assert(row_id_global < matrix__findClusterCmpCategories.nrows);
	set_string__s_kt_matrix(&matrix__findClusterCmpCategories, row_id_global, string_local, /*addFor_column=*/false);	    
      }
      //!
      //! Iterate through the columns:
      //! -------------------------------------------------------------------
      //!
      //! Iterate through the corrleation-metircs:
      uint col_id_global = 0;
      for(uint cluster_cmp_id = 0; cluster_cmp_id < e_kt_clusterComparison_undef; cluster_cmp_id++) {
	e_kt_clusterComparison_t metric_clusterCmp_between = (e_kt_clusterComparison_t)cluster_cmp_id;
	assert(metric_clusterCmp_between != e_kt_clusterComparison_undef);
	//! Iterate through the different cluster-comparison-metrics: the post-process-types:
	for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
	  for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	    //! Iterate through the different cluster-comparison-metrics: the corrleation-emtrics themself:
	    e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;

	    if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	       (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	       || 
#endif
describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	       || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	       ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
	    //! -----------------------
	    if(row_id_global == 0) {//!
	      //! Genearte an ifnroamtive string: 
	      assert(row_id_global < matrix__findClusterCmpCategories.nrows);
	      char string_local[2000]; memset(string_local, '\0', 2000);
	      sprintf(string_local, "%s::%s::%s", 
		      getStringOf__e_kt_clusterComparison_t(metric_clusterCmp_between),
		      get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(dist_metricCorrType),
		      get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id));
		  
	      assert(strlen(string_local));
	      { //! Build a string-reprsentaiton and upate the matrix:
		assert(col_id_global < matrix__findClusterCmpCategories.ncols);
		set_string__s_kt_matrix(&matrix__findClusterCmpCategories, col_id_global, string_local, /*addFor_column=*/true);	    
	      }
	    }
	    //! ------------------------------------------------
	    //! 
	    //! Configure how we are to infer the metrics:
	    s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	    //! state that we are to use a complex comparsin-strategy:
	    config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true;
	    //! Set the tyep of clsuterign-comparison-strategy to use:
	    config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between;
	    //! Set the type of corrleaiton-metirc to be used:
	    config_intraCluster.metric_complexClusterCmp_obj.typeOf_correlationPreStep = dist_metricCorrType;
	    config_intraCluster.metric_complexClusterCmp_obj.metric_id = dist_metric_id;



	    //! 
	    //! Initaite the cluster-evluation-object:
	    s_kt_matrix_cmpCluster_clusterDistance_t obj_clusterDistance  = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
	    if(matrix_afterCorrelation->nrows == matrix_afterCorrelation->ncols) {
	      init__s_kt_matrix_cmpCluster_clusterDistance(&obj_clusterDistance, mapOf_nullHypothesis_vertexTo_centroidId, 
							   /*cnt-vertices=*/matrix_afterCorrelation->nrows, 
							   /*cnt-vertices=*/matrix_afterCorrelation->ncols, 
							   /*cnt_clusters=*/ideal_clusterCnt, 
							   /*matrix=*/matrix_afterCorrelation->matrix,
							   // NULL,
							   config_intraCluster);
	      //! --------------------------------------------------------------
	      //!
	      //! Compute, and Update the result-matrix:
	      const t_float score = compute__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_clusterDistance, cmpType, cmpType_subType);
	      assert(false); 	    // FIXME[result]: does [below] 'call' have a 'point'? <-- write a 'test' where we comapre the result of 'writing out the result using the disjoitn-rpocedure' VS 'writing out the matrix only after using rank-fitlering'.
	      matrix__findClusterCmpCategories.matrix[row_id_global][col_id_global] = score; 
	      //!
	      //! De-allocate:
	      free__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_clusterDistance);
	    } //! else we assuem teh matrix is Not an adjcency-matrix.
	    col_id_global++;
	  }
	}
      }
      //!
      //! Update the row-id:
      row_id_global++;
    }
  }
  //! -----------------------------------------------------------
  //!
  //! Compute ranks and thereafter use the rank_minTrheshold to 'decide' the threshold-filter:
  const t_float min_rank = (config_min_rankThreshold != T_FLOAT_MAX) ? matrix__findClusterCmpCategories.nrows * config_min_rankThreshold : T_FLOAT_MIN_ABS;
  s_kt_clusterAlg_fixed_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
  const t_float max_rank = T_FLOAT_MAX;
  s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, fileHandler__global); 
  char nameOf_fileToExport[1000] = {'0'}; sprintf(nameOf_fileToExport, "%s_disjointMasks.tsv", nameOf_fileToExport_clusterCmpAssessment);            
  ktApi__afterCorrelation.stringOf_exportResult__heatMapOf_clusters = nameOf_fileToExport;
  //! Make the call:
  const bool is_ok = kt_matrix__disjointClusterSeperation(&matrix__findClusterCmpCategories, &obj_result, min_rank, max_rank, false, /*isTo_adjustToRanks_beforeTrehsholds_forRows=*/true, NULL, ktApi__afterCorrelation);
  assert(is_ok);
      
  //! -----------------------------------------------
  //! De-allocate the locally allocated matrix:
  free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
  free__s_kt_matrix(&matrix__findClusterCmpCategories);
  //! ----------------------------------------
  //! 
  //! De-allocate:
  free__s_kt_longFilesHandler_t(fileHandler__global);  fileHandler__global__ = *fileHandler__global;
}



//! A funciton focused on evlauating a single data-set.
void divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t(const s_kt_assessPredictions_t *self, uint *mapOf_nullHypothesis_vertexTo_centroidId, s_kt_matrix_t *matrixTo_evaluate, const uint ideal_clusterCnt, const char *stringOf_resultPrefix,  bool inputMatrix__isAnAdjecencyMatrix) {
  //! What we expect:
  assert(self);
  assert(mapOf_nullHypothesis_vertexTo_centroidId);
  assert(matrixTo_evaluate);
  assert(matrixTo_evaluate->nrows != 0);
  assert(matrixTo_evaluate->nrows != UINT_MAX);
  assert(matrixTo_evaluate->ncols != 0);
  assert(matrixTo_evaluate->ncols != UINT_MAX);
  if(inputMatrix__isAnAdjecencyMatrix == true) {
    if(matrixTo_evaluate->nrows != matrixTo_evaluate->ncols) {
      fprintf(stderr, "!!\t Seems like your configuraiton is in-consistent: whiel you (in your function-call) stated that your matrix were an ajdcency-matrix, the matrixTo_evaluate=[%u, %u] wrt. the diemsions of the rows and columns, ie, pelase validate the correctness of your call. As a tmeproary fix we now asusme that your matrix is Not an ejcency matrix. For quesitons please cotnact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", matrixTo_evaluate->nrows, matrixTo_evaluate->ncols, __FUNCTION__, __FILE__, __LINE__);
      inputMatrix__isAnAdjecencyMatrix = false;
    }
  }
  s_kt_longFilesHandler fileHandler__global__ = initAndReturn__s_kt_longFilesHandler_t(/*result_directory=*/NULL, /*file_nameMeta=*/"metaFile.tsv", /*stringOf_header=*/NULL, /*stream_out=*/NULL);
  s_kt_longFilesHandler *fileHandler__global = &fileHandler__global__;

  //! Validate that all 'ideal vertex-amppigns' has a spceifd clsuter-id, and try to 'pårovke' emmory-errors (ie, to detect erronsou sage).
  //! Note: we asusme it is inttutive to assert that "|mapOf_nullHypothesis_vertexTo_centroidId| == matrixTo_evaluate->nrows", ie, as all vertices in the matrix are expected to be 'allocate'd to a given clsuter.
  for(uint row_id = 0; row_id < matrixTo_evaluate->nrows; row_id++) {
    assert(mapOf_nullHypothesis_vertexTo_centroidId[row_id] != T_FLOAT_MAX);
    assert(mapOf_nullHypothesis_vertexTo_centroidId[row_id] <= matrixTo_evaluate->nrows);
  }


  s_kt_api_config_t ktApi__input = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, inputMatrix__isAnAdjecencyMatrix, fileHandler__global);


  // ------------------------------
  assert(false); // FIXME[kt_api]: ... write a function/function-wrapper where we 'atuoamcially' idenitfy 'k clusters' from a [min-cnt-clusters, max-cnt-clusters, min-rank-thresohld, max-rnak-thresohld] configuration ... and write a 'sample call' to 'this' in [”elow]
  assert(false); // FIXME[kt_api]: ... write a function which 'autmically idneifty the ideal k-count-of-clsuters by 'testing a range of c-counts ... and using eg, "Silhouettes idnex" to detmerine the b'est-firt kk-count ... and 'tretunr' the count
  assert(false); // FIXME: ... figure out ... 
  assert(false); // FIXME: ...
  // ------------------------------
  assert(false); // FIXME: pre-condition: complete our "measure_kt_matrix_cmpCluster.c" ... and ensure that the "findExtremeCases-step0" use-case-logics is 'factored out' into a new/sperate function in "kt_assessPredictions.c"


  { //! Apply k-means clustering and compare result to the 'ideal set':
    //!
    //! Correlate the data-set before clustering:
    s_kt_matrix_t matrix_afterCorrelation; setTo_empty__s_kt_matrix_t(&matrix_afterCorrelation); 
    s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, fileHandler__global); //! ie, as the 'correlaiton-call' should result in an adjcency-matrix.
    {
      //! 
      //! Correlate the input-matrix:
      kt_matrix__distancematrix(self->corr_metricObj__ideal, matrixTo_evaluate, matrixTo_evaluate, &matrix_afterCorrelation, ktApi__input);
      assert(matrix_afterCorrelation.ncols == matrix_afterCorrelation.nrows); //! ie, as we expect to 'now have an adjcency-matrix'.

      //!
      //! Write out the "matrix_afterCorrelation" matrix:
      assert(matrix_afterCorrelation.ncols == matrixTo_evaluate->nrows); assert(matrix_afterCorrelation.nrows == matrixTo_evaluate->nrows);
      char nameOf_fileToExport[1000] = {'0'}; sprintf(nameOf_fileToExport, "%s_idealCorrelation.tsv", stringOf_resultPrefix);      
      export__singleCall__s_kt_matrix_t(&matrix_afterCorrelation, nameOf_fileToExport, fileHandler__global);
    }
    //! -------------------------------------------------------------------
    //!
    { //! Identify simliar cluster-comparison-metrics for the given clsuter-comparison-metric.
      const t_float config_min_rankThreshold = 0.8; //! ie, use the 20% most higly-rankged vertices.
      //! For the new-inferred correlation-matrix assess/elvuate the simliary with the gold-standard: complexity conserns the approx. 70,000 differnt clsuter-comparison-emtrics supported by hpLyssis, ie, which we evlauate in [”elow]:
      assert(matrix_afterCorrelation.nrows > 0);       assert(matrix_afterCorrelation.ncols > 0);
      standAlone__describeClusterCmpSimilarities_forDataSet__s_kt_assessPredictions_t(mapOf_nullHypothesis_vertexTo_centroidId, &matrix_afterCorrelation, 
										      //matrixTo_evaluate, 
										      ideal_clusterCnt, inputMatrix__isAnAdjecencyMatrix, stringOf_resultPrefix, config_min_rankThreshold);
    }    
    //! -------------------------------------------------------------------
    //!
    assert(false); // FIXME: when [”elow] rpoceudre is 'completed' then 'factor the proceudre out to a enw fucntion' ... and describe/suggest different sue-case-applicaitons.
    { //! Apply the clustering and assess the staiblity of the clsutering:
      const uint numberOf_kMeans__iterations = 50;   //! which is used to 'test the divergence betweene ach run'.
      const uint numberOf_kMeans__iterations__insideEach__cnt = 10; const uint numberOf_kMeans__iterations__insideEach__stepSize = 300;
      //const uint numberOf_kMeans__iterations__insideEach__cnt_kToTest = 4; 
      //! 
      //! Apply the clustering 
      //! Note: in this evlauation we seek/try to explroe both 'complex appraoches' (eg, MINE and differnt k-means-algorithm-permtautions) and simplcitic approaches (eg, dsijoint-forest-clustering and "directScore" distance-metrics). This in order to idneityf the 'boundary' for tailed/specific/otpmized appraoches/sollutions: if a 'high-time-cost appraoch appraoch' procue results which are in-signicant from the naive-low-cost appraoches (eg, a disjtoint-forest-cluster-appraoch) then we assert that the high-cost-appraoch is valueless. The applcialbitility (of the latter) is seen wrt. ...??... <-- try introspecing 'abotu this'
      //! Note: In order to assess/evlauate the clustering-accuarcy we compare [niter=[100, 200, ...] x centroid=[mean, median, medoid](k=fixed|disjotin-forest-inferred)  + disjotin-forest(rankTreshhold=[10, 40, 60, 80]...) 
      //! -- data-comparison: in this procedure we build cluster-result-data for: 
      //!    case(a) "Prediction-stability-categories: Signficant statbility cluster-predictions" (where a use-case is to describe the min-disfference between cluster-algorithms for a givne clsuter-algorithm to produce mroe signficant results), where we constuct a  matrix[algorithm-combination][rank-steadiness=[AVG, STD, Kurtosis, Skewness <-- try a 'sperate evlaution.-case' where we extend the latter with [Student-t, ...??..]]] (where the 'latter ranks' are inferred/idneitifed in the 'transposed matrix', and where the results are exported to a 'masked heat-map clustred both using a using disjoitn-forest-computation', and where the 'disjoint count' in the latter is used as 'input' to a k-means-based clustering-procedure');  <-- use-case: "to find/indietfy the algorithm-strategies which produce stable/agreeable/convergent resutls (for a parituclar data-set): in ideal cases we expect the stability to reflect the 'ambituity-factor' (in a given data-set)" ....  iot. evaluate different 'ambigutiy-factors' produce/gernate data-sets at different rank-factors: an example-usage-case conserns the case/situation where we expect the data-set have an 'ambiguous ideal clsuter-result', ie, for which we would 'rank'/regard clsuter-strageies with an 'expressed intenrla ambugity' as 'more accurate than algorithms which errnosuly/rongly produce/idneitfy stable clsuters', eg, wrt. answering the questioni "what are the clsuter-algorithm-cofnigruations which best capture the in-exatness/ambiguties in my data-set (eg, where ambigutiy in 'seeked' in order have a broad/gneralizzed clsuter-cagtegoies)?". The latter example-use-case is in cotnrast to what we often observe/read (in sceitnitifc litterature), ie, where we asert/assuemt aht 'the implicaiton of a low stability-degree-factor in cluster-predcitons should be comapred to the cluster-seperatbility of a given data-set' (ie, in cotrnast to the often-see-appraoch of saying/stating that 'cluster-algorithms whith high stability are mroe accruate than algorithms with low stability'). In order to address this issue we 'makes use of' the "self->clusterProp__dataAmbiguity" stability-accracuy-input-paremter (in this funciton), ie, where we 'seek' to rank the data-set based on the 'expected stability' (rather than to idnetify the algorithm-combinations which maximze teh stability). To summarize we assert that this/our appraoch manages to provide better/imrpvoed estimations of clsuter-result-exactness (eg, when compared to a naive "Silhouette index-comparison-appraoch"). <-- wrt. the 'procedure' we insert all 'evalautions' as 'rows' (where the columsn describe the different 'measurement-categoires')
      //!    case(b) "Cluster-cluster-comparison: identify the cluster-results which compares best/well to the gold-standard; apply cluster-prediction-simliartiy to gold-standard-algorithm-combinations: use our ...??.. procedure (eg, examplfied in our 'findExtremeCases-step0' use-case described in our 'measure_kt_matrix_cmpCluster.c')": in our "matrix__clusterMembeships" idneitfy/collect the cluster-memberships (produced in the latter/this evlauation) and thereafter assess/idneitfy the simlariy between 'the clsuter-sets' (ie, by calling the ....??... function). An example-use-case wrt. "case(b)" conserns the question: "what cluster-algoirthms results in the best simliarty-classifciaotn to the gold-standard?"
      //!    case(c) "identify overlaps in measures": compare two correlation-matrices (ie, matrices in the same diemsions which describe the same vertices), and from the 'comparsion of matrices' idneitfy an overlap in predictions. An example (of the latter) is to use the "dot-prodcut correlation-metric" to maximize 'internla agreements' in predictions, eg, to "identify the cases where clsuter-prediction-result-instaiblity relates/correlates to high exeuction-time" (an assumption which we expect to hold for many cases: if the cluster-prediction-result is Not well-defined then we expect an iterative-heuritcic-algorithm to first converge when 'the maximnum number of traversals' are 'reached').
      //!    case(c.1) "how prediction-stability relates to high-scoring clsuter-results": from the results of/from "case(a)" and "case(b)" derive/hypotehse an agrrement (between the prediction-results). Procedure: 'remember' the result-matrix and thereafter 'produce' a correlation-matrix+clustering where we comptue "case(a) x case(b)", ie, to idneityf/assert/describe the similairty between the 'different stability-indications of clsuter-stability' VS the 'maximized/ideal data-clusters assicated to the cluster-comparsion-metrics'. visually compare the 'predicted result' <-- procedure: 'take the two generated/produced [algorithm-clsuter-result][algorithm-clsuter-result] correlation-matrices as input', use an accurate clsuter-algorithms for sub-segments idnetified by using differnet thresholds' and therafter 'apply clustering' <-- todo: try to describe how we may 'infer' interesting/descirptive reesults fom the latter.
      //!    case(c.2) "how exeuction-time relates to stability of cluster-algorithm-results" ... where we 'rleate the euxeciton-time' to the cluster-exactness' through a comparison of ....??... <-- TODO: try to describe/compelete this use-case. <-- procedure: simliarly to "case(c)" we first 'produce' a [algorithm-clsuter-result][algorithm-cluster-result] for the two matrix-cases (ie, exeuction-time and cluster-staiblity) and thereafter apply clustering. A use-case (wrt. the latter) is to answer the quesiton: "what are the algorithms which mazimize result-variability and exeuction-time (eg, to idnetify the worst-performers both wrt. stability and exuection-time ofr a dats-et which is expected to 'provide' a non-ambigiuous cluster specificaiton/defintions).  ....??....
      //!    case(d) "the importance of cluster-algorithms VS correlation-metrics: build ..  ....??...., and then count the number ...??... " : build a matrix=[innerCorrMetric x cluster-alg-type][pre-processing-steps=[filter-procedure]x[corr-metrics]], compute ranks (for each row) and use a rank-min-threshod=70%, idnetify a k-hyptoetical-number-of-clusters, and then apply k-means-clustering sepraely to each 'dsijoitn sub-regions, before writing our a 'masked version' of the cluster-input-rank-matrix; results are expected/hope to provide leads/intutition into describing the signficance of clutster-algorithm-pertubations: in order to 'nuemrically assess the latter' we 'generate an algorithm-gold-standard' using/under the null-hypotehsis "the clsuter-results are clearly seperately wrt. each cluster-algorithm", ie, where we build a 'hyptheical' cluster-allocation where cluster1=[{clsuter-erresults gernated usign algorithm-permtaution-1}], cluster1=[{clsuter-erresults gernated usign algorithm-permtaution-2}]. In this/our work we investigate different nullh-hythessi, a set of null-hyptoess which are 'provided' as a cluster-class to our  ...??.. procedure (eg, examplfied in our 'findExtremeCases-step0' use-case described in our 'measure_kt_matrix_cmpCluster.c')". The result of the latter function-call is (among others) a heat-map of how 'well' each 'gold-standard fits to the predicted cluster' <-- consider updating the  ...??... function to return a ...??x.. object where teh ...??x.. ojbect is used to ....??... <-- try suggestion 'such an appraoch' .... to 'limit' this study to only use "default Silhouette" and where we perform a 2d-comparsiong of "[actual-cluster-result, groupsSelectionCtieria=[postProcessType, corrTypeInside, corrTypeBefore, algorithmType, number-of-iterations, k-count-of-clusters]]" cluster-meberships, and then idneityf the average-rank and STD of [...]["algorithmType"]. From the latter we observe that the result will provide clues/dscriptions of how the relationship between 'ideal cluster-appraoch' and 'naive cluster-comparison'. In the assessment our "null-hypotehsis" is that "there will be a weak relationship (wrt. Silhouette---clusterAlg", ie, where assert that the use of "Silhouette index" to assess/describe the accuracy of a given cluster-algorithm is pointless/meaningless (ie, due to the large nubmer of other 'parameters/factors invovled wrt. both cluster-genraiton and cluster-analsysis. <--- procedure: step(a) 'from each of the correlation-metrics in case(a--d) buidl a sample-gold-cluster-set where each cluster-category (ie, the permtautions of the given clsuter-category) are in a sepearte/disntinct cluster' (eg, 'all combinations/permtuations assiated to k=4-means clustering'), an store the latter 'in our matrix of gold-standards'; step(b) produce different clsuter-resutls from the result-correlation-amtrices generated in "case(a--c)" and thereafter call our "~findExtremeCases-step0~" (ie, to relate the deviation between teh 'hypotehetical-gold-cluster-set' and the 'actual data inferred from our comparison-appraoch'); step(c) result-assessment: .....??...
      assert(false); // FIXME: try describing use-case-applciatiosn 'wrt. this procedure'.
      
      //!
      //! Intiate a matrix to hold the different cluster-algorithms:
      uint sampleData__cntMetrics_total = 0; uint sampleData__cntClusters = 0; uint cnt_totalClustersProduced_xmtEachConvergenceTest = 0; uint cnt_kToTestAbove_idealCount = 4;
      const t_float config_min_rankThreshold = 0.8;
      { //! Idneitfy the different 'coutns' wrt. our evlauation:

	assert(false); // FIXME: incldue a set of pre-processing-strategies .... eg, [PCA, lgo-transform, rank-masking, ...] ... ie, in combination 'with [”below].
	assert(false); // FIXME[data-to-use]: case(a): [all-comparison-stratgiesies] ...  w/cols=: [AVG, STD, Kurtosis, Skewness, studentT-Person, studentT-Spearman], [studentT-Person, studentT-Spearman], [AVG, STD, Kurtosis, Skewness], [exec-time, ie, where we adjust by the 'globally max exec-time' iot ...??... <-- try suggestiona a 'normalizaiton-strategy' wrt. exeuction-time (or alternativly 'use the results as-is')]
	assert(false); // FIXME[data-to-use]: case(b): [all-comparison-stratgiesies] ...  w/cols=: 
	assert(false); // FIXME[data-to-use]: case(c): [all-comparison-stratgiesies] ...  w/cols=: 
	assert(false); // FIXME[data-to-use]: case(d): [innerCorrMetric x cluster-alg-type][pre-processing-steps=[filter-procedure]x[corr-metrics]] ...  w/cols=: ...??....
	assert(false); // FIXME: [ªbove]:

      }
      assert(false); // FIXME: conside rto update [ªbove] based on the 'number of combiatniosn we evlaute' ... ie, try to 'amize the possilbe number of combiatniosn wrt. our max-memnm-threshold ... eg, to use differnet permtuations of 'k' and extend the number of pre-filteringmetrics we evlauate/explore/include
      assert(sampleData__cntMetrics_total > 0); assert(sampleData__cntClusters > 0);
      assert(cnt_totalClustersProduced_xmtEachConvergenceTest > 0); //! where the "cnt_totalClustersProduced_xmtEachConvergenceTest" is used wrt. the "case(a)" we describe (ie, where the latter is outlined/described in this fucntiosn JavaDoc).
      s_kt_matrix_t matrix__clusterMembeships; init__s_kt_matrix(&matrix__clusterMembeships, sampleData__cntMetrics_total, sampleData__cntClusters, /*isTo_allocateWeightColumns=*/false);

      
      //! ------------------------------------
      //!
      //! Iteration: build the matrices of correlations and clusters:
      //! -- traversal-order: of interst is to use of 'prior' assumption to provide a 'gernalized clsuter of rows and columsn' (ie, to try 'visually merge' signficant cells/entries/obsevations). If not otherwise stated we (in this test/evlauation) investigate the null-hypotehsis that "the type of cluster-algorithm is profound/important in 'determining' the accraucy of/for the cluster-result" (an null-hypotehsis which we expect to be counter-pfooved/contradcited). In practise the latter implies that we 'use' the 'main hypotetical pirncipal component as the first key in buidling of the key-pairs descriing the rows', eg, "clusterAlg::corrBemetricPrior::corrMetricInClustering::priorFilterStep" if we assume that "cluster-algorithm" holds-over "correlation-metirc-before" holds-over "correlation-metric-applied-in-clusterin" holds-over "prioer filter-step (eg, PCA)". Visually the latter is 'translated' into regiosn where the first row are clustered based on th cluster-algorithm (eg, disjoitn-forest, k-means, k-median, k-medoid, SOM, etc.): if the 'assumption of the hypotetical principal axis actually hold' then we expect the 'color of the (rows assicated to the) different cluster-algorithms to be disitnct' (eg, 'intrisutive colors for k-means' while 'invisible colors for a disjtoint-forest-appraoch'). To summarize we in the latter haave outlined/described the reason for the 'traversal order', ie, how the 'ordering of the rows in a genrated ehat-map may provide signifnat clues/enahcnesments of visual human-rechonizable patterns in data' (ie, as 'regions whicht hte same colromerges into regions wr.t a humans visual percpection').
      // TODO[JC]: may yu consider to 'tralsnate' this (ie, the orderign/lcustienrg of row-order) into your PCA-article-evlaatuion ... eg, to ....??...
      //! -- filter-applications: in order to 'gernalize' our filter-adjustments we idneityf the 'global average' of each 'proerpty' and then apply a percent-threshold 'on this' <-- add support for this.
      //! -- ...??....
      //! -----
      //!
      //! Iterate through the different cluster-comparison-metrics: the post-process-types:
      //for(uint clusterAlg_id = 0; clusterAlg_id < (uint)e_hpLysis_clusterAlg_undef; clusterAlg_id++) {

      assert(false); // FIXME: udpate [”elow] ... insert row-names and column-naems for 'each of our matrices'.
      assert(false); // FIXME: udpate [”elow] ... 

      for(uint clusterAlg_id = 0; clusterAlg_id <= (uint)e_hpLysis_clusterAlg_disjoint; clusterAlg_id++) {  //! where we 'limit' the number of cases iot. reduce the memory-consumption wr.t the 'all-agaisnt-all-correlation-appraoch'.
	const e_hpLysis_clusterAlg_t clusterAlg = (e_hpLysis_clusterAlg_t)clusterAlg_id;
	assert(clusterAlg != e_hpLysis_clusterAlg_undef); //! ie, to 'detect' any future erorrnous changes.
	//! 
	//! Iterate through the different cluster-correlation-metrics: the corrleation-emtrics themself:
	//for(uint corrMetricBeforeClust_enum_preStep = 0; corrMetricBeforeClust_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; corrMetricBeforeClust_enum_preStep++) 
	{
	  for(uint clusterMetric_index = 0; clusterMetric_index <= (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	    e_kt_correlationFunction_t corrMetricBeforeClust_metric_id = (e_kt_correlationFunction_t)clusterMetric_index;
	    e_kt_categoryOf_correaltionPreStep_t corrMetricBeforeClust_preAdjust = e_kt_categoryOf_correaltionPreStep_none;
	    //e_kt_categoryOf_correaltionPreStep_t corrMetricBeforeClust_preAdjust = (e_kt_categoryOf_correaltionPreStep_t)corrMetricBeforeClust_enum_preStep;
	    if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	       (corrMetricBeforeClust_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	       || 
#endif
describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(corrMetricBeforeClust_metric_id)
	       || isTo_use_directScore__e_kt_correlationFunction(corrMetricBeforeClust_metric_id)
	       ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
	    //! ----------------
	    //!
	    //! Apply the correlation-pre-step:
	    const s_kt_matrix_t *matrix_afterCorrelation = matrixTo_evaluate;
	    s_kt_matrix_t matrix_afterCorrelation__locallyAllocated; setTo_empty__s_kt_matrix_t(&matrix_afterCorrelation__locallyAllocated);
	    if(corrMetricBeforeClust_metric_id != e_kt_correlationFunction_undef) {
	      //!
	      //! Start the time-emasurement:
	      struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start);
	      uint cnt_iterations = 1; //! ie, the default asusmption.
	      //!
	      //! Correlate:
	      s_kt_api_config_t config_ktApi = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/inputMatrix__isAnAdjecencyMatrix, fileHandler__global); 
	      s_kt_correlationMetric_t obj_metric_beforeClust = setTo_empty__andReturn__s_kt_correlationMetric_t();
	      obj_metric_beforeClust.metric_id = corrMetricBeforeClust_metric_id;
	      obj_metric_beforeClust.typeOf_correlationPreStep = corrMetricBeforeClust_preAdjust;
	      kt_matrix__distancematrix(obj_metric_beforeClust, matrixTo_evaluate, NULL, &matrix_afterCorrelation__locallyAllocated, config_ktApi);
	      matrix_afterCorrelation = &matrix_afterCorrelation__locallyAllocated; //! ie, update the reference

	      //!
	      //! Compare the matrix with the clusters in the input-matrix:
	      assert(false); // FIXME: describe a sue-case wrt. this
	      assert(false); // FIXME: add something ... eg [Silhouette, Dunn] ...
		  
	      //!
	      //! Stop the time-emasurement:		
	      struct tms tms_end;	      clock_t clock_time_end = times(&tms_end);
	      long clktck = sysconf(_SC_CLK_TCK);
	      const clock_t t_real = clock_time_end - clock_time_start;
	      float time_in_seconds = t_real/(float)clktck;

	      //! Update the matrix:
	      assert(false); // FIXME: describe a use-case wrt. this
	      assert(false); // FIXME: add something.
		
	    } //! then we do nothign, ie, for which 'we may use' the analaysis-results to evlauate/ivnestigate the 'effect' of Not applying a pre-correlation-step:
	      //! ----------------
	    

	      //!
	      //! Iterate through the (complete set of) corr-emtrics to be used 'inside' the clustering:
	      //for(uint corrMetricInsideClust_enum_preStep = 0; corrMetricInsideClust_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; corrMetricInsideClust_enum_preStep++) 
	    {	      
	      for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
		e_kt_correlationFunction_t corrMetricInsideClust_metric_id = (e_kt_correlationFunction_t)clusterMetric_index;
		e_kt_categoryOf_correaltionPreStep_t corrMetricInsideClust_preAdjust = e_kt_categoryOf_correaltionPreStep_none;
		//e_kt_categoryOf_correaltionPreStep_t corrMetricInsideClust_preAdjust = (e_kt_categoryOf_correaltionPreStep_t)corrMetricInsideClust_enum_preStep;
		if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
		   (corrMetricInsideClust_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
		   || 
#endif
describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(corrMetricInsideClust_metric_id)
		   //|| isTo_use_directScore__e_kt_correlationFunction(corrMetricInsideClust_metric_id)
		   ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
		s_kt_correlationMetric_t obj_metric_inClust = setTo_empty__andReturn__s_kt_correlationMetric_t();
		obj_metric_inClust.metric_id = corrMetricInsideClust_metric_id;
		obj_metric_inClust.typeOf_correlationPreStep = corrMetricInsideClust_preAdjust;

		//!
		//! Test diffenret cases wrt. 'the k-count of clusters being above the expected-ideal-case' (of cluster-count in k-means clustering):
		assert(cnt_kToTestAbove_idealCount >= 1);
		for(uint cnt_kAbove_ideal = 0; cnt_kAbove_ideal <= cnt_kToTestAbove_idealCount; cnt_kToTestAbove_idealCount++) {
		  if((cnt_kAbove_ideal > 0) && (clusterAlg == e_hpLysis_clusterAlg_disjoint)) {continue;} //! ie, as 'as the distjoint-forest-algortihm does Not use the k-number in its evaluation'.
		  //!
		  //! Start the time-emasurement:
		  struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start);
		  uint cnt_iterations = 1; //! ie, the default asusmption.

		
		  assert(numberOf_kMeans__iterations__insideEach__cnt >= 1);
		  for(uint cntIter_index = 1; cntIter_index <= numberOf_kMeans__iterations__insideEach__cnt; cntIter_index++) {
		    const uint niter = cntIter_index*numberOf_kMeans__iterations__insideEach__stepSize;
		    assert(niter > 0);
		  
		    //!
		    //! Apply the clustering:
		    if(
		       (clusterAlg == e_hpLysis_clusterAlg_kCluster__AVG)
		       || (clusterAlg == e_hpLysis_clusterAlg_kCluster__rank)
		       || (clusterAlg == e_hpLysis_clusterAlg_kCluster__medoid)
		       ) { //! then we compute for k-means-based-clustering-calls:
		      const t_float def_0 = 0;
		      t_float *arrOf_scores = allocate_1d_list_float(numberOf_kMeans__iterations, def_0);
		      for(uint cnt_tests = 0; cnt_tests < numberOf_kMeans__iterations; cnt_tests++) {
			cnt_iterations++;
			//! Comptue the k-means-cluster:
			s_kt_clusterAlg_fixed_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
			s_kt_api_config_t config_ktApi = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/inputMatrix__isAnAdjecencyMatrix, fileHandler__global); 
			//! ---------------
			//! start: cluster-compute ---------------
			const uint cnt_k = ideal_clusterCnt + cnt_kAbove_ideal;
			if(clusterAlg == e_hpLysis_clusterAlg_kCluster__AVG) {
			  const bool is_ok = kt_matrix__kluster(obj_metric_inClust, matrixTo_evaluate, &obj_result, cnt_k, /*npass=*/niter, /*isTo_useMean=*/true, config_ktApi, NULL);
			  assert(is_ok);
			} else if(clusterAlg == e_hpLysis_clusterAlg_kCluster__rank) {
			  s_kt_api_config_t config_ktApi = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/inputMatrix__isAnAdjecencyMatrix, fileHandler__global); 
			  const bool is_ok = kt_matrix__kluster(obj_metric_inClust, matrixTo_evaluate, &obj_result, cnt_k, /*npass=*/niter, /*isTo_useMean=*/false, config_ktApi, NULL);
			  assert(is_ok);
			} else if(clusterAlg == e_hpLysis_clusterAlg_disjoint) {
			  const bool is_ok = kt_matrix__kmedoids(obj_metric_inClust, matrixTo_evaluate, &obj_result, cnt_k, /*npass=*/niter, config_ktApi, NULL);
			  assert(is_ok);
			} else {
			  assert(false); //! ie, as we then need to add support 'for this alternative'
			}
			//! finish: cluster-compute ---------------
		      
			//! Update: silhoutette-score-matrix-per-niter:
			assert(false); // FIXME: ... 
		      
			//!
			//! Compare the matrix with the clusters in the input-matrix:
			assert(false); // FIXME: describe a sue-case wrt. this
			assert(false); // FIXME: add something ... eg [Silhouette, Dunn] ...

			//! Update: cluster-membership-matrix:
			assert(false); // FIXME: ... 

			//! Update: "arrOf_scores"
			assert(false); // FIXME: ... 

			//! -----------------------------------------------
			//! De-allocate the locally allocated matrix:
			free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
		      }
		      //!
		      //! Updat ehte result-matrix: insert values seperately using [][niter], ie, where 'number-of-max-iteraitons' is used as column-index.
		      assert(false); // FIXME: describe a use-case wrt. this
		      assert(false); // FIXME: ... 

		      //!
		      //! De-allocate:
		      free_1d_list_float(&arrOf_scores); arrOf_scores = NULL;
		      //!
		      //! De-allocate:
		      //free_1d_list_float(&arrOf_scores); arrOf_scores = NULL;
		    } else if(clusterAlg == e_hpLysis_clusterAlg_disjoint) {
		      //! Comptue the k-means-cluster:
		      s_kt_clusterAlg_fixed_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);		      
		      s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/inputMatrix__isAnAdjecencyMatrix, fileHandler__global);
 		      const t_float min_rank = (config_min_rankThreshold != T_FLOAT_MAX) ? matrixTo_evaluate->nrows * config_min_rankThreshold : T_FLOAT_MIN_ABS;
		      const t_float max_rank = T_FLOAT_MAX;
		      //!
		      //! Apply the clustering:
		      const bool is_ok = kt_matrix__disjointClusterSeperation(matrixTo_evaluate, &obj_result, min_rank, max_rank, false, /*isTo_adjustToRanks_beforeTrehsholds_forRows=*/true, NULL, ktApi__afterCorrelation);
		      assert(is_ok);

		      //! Update: silhoutette-score-matrix-per-niter:
		      assert(false); // FIXME: ... 
		      
		      //!
		      //! Compare the matrix with the clusters in the input-matrix:
		      assert(false); // FIXME: describe a sue-case wrt. this
		      assert(false); // FIXME: add something ... eg [Silhouette, Dunn] ...

		      //! Update: cluster-membership-matrix:
		      assert(false); // FIXME: ... 

		      //! Update: "arrOf_scores"
		      assert(false); // FIXME: ... 

		      //! -----------------------------------------------
		      //! De-allocate the locally allocated matrix:
		      free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
		    } else {
		      assert(false); //! ie, as we then need to add support 'for this alternative'
		    }
		  }

		  //!		  
		  { //! Stop the time-emasurement:
		    assert(cnt_iterations >= 1);
		    struct tms tms_end;	      clock_t clock_time_end = times(&tms_end);
		    long clktck = sysconf(_SC_CLK_TCK);
		    const clock_t t_real = clock_time_end - clock_time_start;
		    float time_in_seconds = t_real/(float)clktck;
		    if(time_in_seconds != 0) {
		      time_in_seconds = 1/(t_float)cnt_iterations;
		    }
		    //! Update the matrix:
		    assert(false); // FIXME: add something.
		  }
		} //! and at this exec-point the differnet 'k-number-of-clustsers' have been evlauated.
	      }
	    }
	    //!
	    //! De-allcoate locally reserved memory:
	    free__s_kt_matrix(&matrix_afterCorrelation__locallyAllocated);
	  }	 
	}
      }
    
      assert(false); // FIXME: ... 
      // --------------------------
      assert(false); // FXIME: consider to compare a number of different k-means-configuraiton-settings ... and then exprot ....??....
      assert(false); // FIXME: in thsi evlauation also call+invetigate/evlauate the accurayc of a 'rank-beased threshold using oru dsijtoin-forest-procedure' <-- write a 'loop' to ivnestigate 'wrt. this' 
      assert(false); // FXIME: add something.
      // const bool is_ok = kt_matrix__kluster(obj_metric_fast, &obj_cpy_ranksEachRow, &obj_result, cnt_clusters, /*npass=*/1000, /*isTo_useMean=*/true, ktApi__afterCorrelation);
      //assert(is_ok); //! ie, what we expect.
      assert(false); // FIXME: update our k-means-exprot-object .. to export a 'version' of the matrix where all 'interactiosn between non-clsuter-embers are remvoed'.
      

      //! ------------------------------------
      //!
      { //! case(a): Assess the staiblity of the clsutering:
	//! Use-case-result-evaluation(1): ...

	assert(false); // FIXME: update [ªbove] ... consider to store the exec-time in a 'simpel list' ...  .... rank the list .... divide th elsit into 'k' clusters ... and then compare the 'exec-time-clsuters' the the 'clusters produced for the complex results/clusters/matrices in this'.
	assert(false); // FIXME[code]: perform muliple runs, store the [AVG, STD, kurtosis, skewness], and comptue 'sperately' for the latter (ie, to 'cpature' differences in cluster-rpediction when the 'same setting is used').
	assert(false); // FXIME: update [ªbove] use-case-applicaiton-descritpion:
	assert(false); // FIXME: wrt. our k-means result ... generate k-means-results for differetn [min_rank, max_rank] thresholds: a use-case is to comapre the 'low-signficant metrics' for different data-sets (eg, through a visual comparison using heatmap-generated data-sets).
	assert(false); // FIXME: use the "self->clusterProp__dataAmbiguity" to assess ambigutiy, ie, where we ...??... <-- conser to update all 'of the respective values' wrt. "mathLib_float_abs(val[type] - expectedValue"


	assert(false); // FIXME: write out a 'seprate result-matrix' whcih re-orders the matrix ... where we investigate/evlauate teh null-hypotehsis that "the type of correlation-emtrics (applied before the clsutering) results in signficant/visible/enhanced clusters of low-scoring and high-scoring regions" (ie, where we 'choose' to have the 'pre-correltation-metric' in the innermost for-loop, an appraoch which will/may ....??..  <-- consider to 'also' ...??... )  <--- write a 'seperate permtuation' where we 'seprately investgiate' wrt. ["pre-filter-appraoches", "correlation-emtrics usided 'inside' the clsutering", "number of iterations in k-means-clsutering"]

	assert(false); // FIXME: export hte results of different 'traversal-matrices'.
      }

      //! ------------------------------------
      //!
      //! Compare the gold-standard to the 'ifnerred clustering'.
      //! Note: for an examplifciaiton of 'this', please see our "findExtremeCases-step0" use-case in our "measure_kt_matrix_cmpCluster.c"
      //! Use-case-result-evaluation: ...
      assert(false); // FXIME: update [ªbove] use-case-applicaiton-descritpion:
      assert(false); // FXIME: add something.

      //! ------------------------------------
      //!
      { //! Case(c): "identify overlaps in measures":
	//! Note: wehn we compare the different results (eg, "euxeciton-time" and "clsuter-stailbity" idneitfied in "case(a)") we use the 'resutl-correlation-matrix' (ie, we do Not compare the differetn correlation-matrices used internally in buidlign of each cluster-result). In brief we assert that 'this appraoch' cosndierably simplfies the comaprsion-appraoch (ie, in cotnrast to 'applying this proceudre' the complete set of correlation-atmrices wrt. the 'steps' we compare).
	assert(false); // FIXME: ... 
	assert(false); // FIXME: consider to udpate our clsutering-procedure ... to 'apply a recusrive STD-seperation for clusters which are all to alrge to 'handle' <-- todo: woudl 'such an appraoch' be correct?
	assert(false); // FIXME: ... 
      }
      //! ------------------------------------
      //!
      { //! Case(...): ... 

	assert(false); // FIXME: ... 
      }




      //!
      //!
      //! De-allocate:
      free__s_kt_matrix(&matrix__clusterMembeships);
    }

    //! -------------------------------------------------------------------
    //!
    { //! Extend [ªbove] to perfomr an extneisve evlaaution of combiantiosn, ie, without 'being limtied by the meoory-space'. In this apåpraoch we first comptue clsuter-reuslts (ie, simliar to [ªbove]) through in the post-process idneityf/select a subset of feature 'to use as the rows' (ie, in contrast to the 'use-all-appraoch' in [ªbove]).

      //!
      //! Intiate a matrix to hold the different cluster-algorithms:
      uint sampleData__cntMetrics_total = 0; uint sampleData__cntClusters = 0; uint cnt_totalClustersProduced_xmtEachConvergenceTest = 0;
      { //! Idneitfy the different 'coutns' wrt. our evlauation:

	assert(false); // FIXME: add something
      }
      assert(sampleData__cntMetrics_total > 0); assert(sampleData__cntClusters > 0);
      assert(cnt_totalClustersProduced_xmtEachConvergenceTest > 0); //! where the "cnt_totalClustersProduced_xmtEachConvergenceTest" is used wrt. the "case(a)" we describe (ie, where the latter is outlined/described in this fucntiosn JavaDoc).
      s_kt_matrix_t matrix__clusterMembeships; init__s_kt_matrix(&matrix__clusterMembeships, sampleData__cntMetrics_total, sampleData__cntClusters, /*isTo_allocateWeightColumns=*/false);

      
      //! ------------------------------------
      //!
      //! Iteration: build the matrices of correlations and clusters:
      for(uint clusterAlg_id = 0; clusterAlg_id < (uint)e_hpLysis_clusterAlg_undef; clusterAlg_id++) {
	const e_hpLysis_clusterAlg_t clusterAlg = (e_hpLysis_clusterAlg_t)clusterAlg_id;
	assert(clusterAlg != e_hpLysis_clusterAlg_undef); //! ie, to 'detect' any future erorrnous changes.
	//! 
	//! Iterate through the different cluster-correlation-metrics: the corrleation-emtrics themself:
	for(uint corrMetricBeforeClust_enum_preStep = 0; corrMetricBeforeClust_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; corrMetricBeforeClust_enum_preStep++) {
	  for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	    e_kt_correlationFunction_t corrMetricBeforeClust_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t corrMetricBeforeClust_preAdjust = (e_kt_categoryOf_correaltionPreStep_t)corrMetricBeforeClust_enum_preStep;
	    if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	       (corrMetricBeforeClust_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	       || 
#endif
describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(corrMetricBeforeClust_metric_id)
	       || isTo_use_directScore__e_kt_correlationFunction(corrMetricBeforeClust_metric_id)
	       ) {continue;} //! ie, as we then assuem 'this is Not of interest'.

	    //!
	    //! Iterate through the (complete set of) corr-emtrics to be used 'inside' the clustering:
	    for(uint corrMetricInsideClust_enum_preStep = 0; corrMetricInsideClust_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; corrMetricInsideClust_enum_preStep++) {
	      for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
		e_kt_correlationFunction_t corrMetricInsideClust_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t corrMetricInsideClust_preAdjust = (e_kt_categoryOf_correaltionPreStep_t)corrMetricInsideClust_enum_preStep;
		if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
		   (corrMetricInsideClust_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
		   || 
#endif
describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(corrMetricInsideClust_metric_id)
		   || isTo_use_directScore__e_kt_correlationFunction(corrMetricInsideClust_metric_id)
		   ) {continue;} //! ie, as we then assuem 'this is Not of interest'.

		//! -------------------------------------------------
		const uint stringOf_typeOf_adjust_size = 4;
		assert( (stringOf_typeOf_adjust_size-1) == (uint)e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none);
		const char *stringOf_typeOf_adjust[stringOf_typeOf_adjust_size] = {
		  "median",
		  "mean",
		  "STD",
		  "none",
		};
		const e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t mapOf_typeOf_adjust[stringOf_typeOf_adjust_size] = {
		  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median,
		  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean,
		  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD,
		  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none
		};	      
		//! -------------------------------------------------
		const uint stringOf_valueAdjustments_size = 5;
		assert(stringOf_valueAdjustments_size == e_kt_matrix_filter_adjustValues_undef);
		const char *stringOf_valueAdjustments[stringOf_valueAdjustments_size] = {
		  "logarithm(..)",
		  "subtract-abs(rows)",
		  "subtract-abs(cols)",
		  "feature-scaling(rows)",
		  "feature-scaling(cols)",
		};
		const e_kt_matrix_filter_adjustValues_t mapOf_valueAdjustments[stringOf_valueAdjustments_size] = {
		  e_kt_matrix_filter_adjustValues_log,
		  e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows,
		  e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_cols,
		  e_kt_matrix_filter_adjustValues_center_normalization_rows,
		  e_kt_matrix_filter_adjustValues_center_normalization_cols
		};
		//! -------------------------------------------------
		
		//!
		//! Iterate through the set of post-filtering-operation: adjust-values:
		for(uint valueAdjustment_id = 0; valueAdjustment_id < stringOf_valueAdjustments_size; valueAdjustment_id++) {
		  for(uint adjust_id = 0; adjust_id < stringOf_typeOf_adjust_size; adjust_id++) {
		    const e_kt_matrix_filter_adjustValues_t valueAdjustmenttype = mapOf_valueAdjustments[valueAdjustment_id];
		    const e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t typeOf_adjustment = mapOf_typeOf_adjust[adjust_id];
	

		    //! The operation:
		    assert(false); // FIXME: ... 
		    // adjustValue__s_kt_matrix_filter_t(&self, enum_type);


		    //!
		    //! Iterate through the set of post-filtering-operation: cell-mask:
		  
		  
		    //!
		    //! Iterate through the set of post-filtering-operation: adjust-values:
		  
		    //!
		    //! Iterate through the set of post-filtering-operation: subtract-or-add:
		  
		  }
		}
	      }
	    }
	  }
	}
      }


      //! ------------------------------------
      //!
      { //! Case(....): ...  <-- consider 'adding' this.
	//! Note: we here use the ranks of diffenret 'groups' (eg, "exeuction-time") to build rows and and where we use the complete set of [ªbove] 
	//! Use-case: ....??..

      }



      //!
      //!
      //! De-allocate:
      free__s_kt_matrix(&matrix__clusterMembeships);
    }



    //! -------------------------------------------------------------------
    //!
    { //! Evaluate the singificance of different 'k' in k-means-clustering
      //! Use-case-result-evaluation: ...
      assert(false); // FXIME: update [ªbove] use-case-applicaiton-descritpion:
      //! Use-case-result-evaluation(2): "When does the Silhouette index provide/assumes misleading/in-accraute results, and how do we idneitfy signfivcant result-differences (eg, 'how much should the Silhouette-index-difference be for an algorithm X to produce btter resultat than a different algorithmt Y?')" ... a use-case where we (among others) try to ivnestigate the validaty of 'new algorithsm claims to be more accruate' (eg, as seen in the work of ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"])  ... 
      assert(false); // FIXME: update our "kt_api.c" ... add support for a 'disjoitn-clustering-procedure' where we use the return-strucutre in our 'k-means-procedure' (ie, to 'store' the results).
      assert(false); // FIXME: write a new function in our "kt_api.c" ... where we try the maximize the 'k' score .... ... eg, "[min OR max]_{k=2, 3....}{[AVG, STD, kurtosis, skewness, min, max, AVG] for each 'setting'}" ... (eg, wrt. AVG(Silhouette in muliple runs)) .... and an optional "arrOf_scoresEachK" ... which we 'insert into a matrics' for each of our 'explored metrics' ... and thereafter use this 'metric of matrix [corr-metric][k-scores] (eg, which hold AVG(silhouette-index) and/or STD(silhouette-index)) to 'suggest groups of metrics' which 'exhibhits a simliartiy in variance among k-means clusters' <-- use MINE+disjotin-forest to 'idneitfy' the different 'clusters'.
      assert(false); // FIXME: consdier extenign [abov€] ... step(d) for each data-set identify the variance (in the rank-prediction) and thereafter rank the data-sets seperately based on the measures=[AVG, STD, Kurstosis, Skewness] (and thereafter write out the results): use the results to describe/classify ...??... ; step(e) apply a combiantorical appraoch (using differnet correlaiton-metic-groups as a 'limiting critera' <-- try to 'examplify this' <-- is it 'possible' o use AVG or STD as a 'trehshold' wrt. the groups?) to idneityf the metrics which provides the most consistent precdiction-qualities; step(f) cluster the results from step(e), where we in the clustering combien the latter results uisng/through .....??...; step() select the min-ranks and max-ranks and then ....??...
      assert(false); // FIXME[cluster-comparison]: [extend-above]: .... for m=[2 .... n/2] 'permtuate' each row in order to 'find the row with 'm' chunks with minimal STD, and then 'insert the filtered/updated regions' into a new matrix_m[][] ... and compute k-means-clusters for k=[2, ....] 'where we select the cluster with the highest cluster-predicotn-stability and highest-Silhouette-value-score' ... result: for all 'm' choose the clustering with the 'best-fit' (and use 'this' to describe boht how the different data-sets relate and how the differetn metrics relate ... where goal/hope is to demonstrate that we for 'different data-set-input-clusters' get signficantly different classifciations, ie, that 'muliple clsuter-comparison-metrics have overlapping-porperties which indicates their fcailtiy as cluster-signfature-vectors');
      assert(false); // FXIME: update [ªbove] use-case-applicaiton-descritpion ... eg, <--- in order to address this issue first 'compare the deviation in Silhouette-index-score which is expected for different k-means-clasisficoant-runs' ... ... 
      assert(false); // FIXME: update our "kt_api.c" ... a k-means-permtaution which selects 'k' from a k-range=[...], and where the 'k-clsuter' is chosen/selected based on ....??.. <-- provide+try different criterias (and then update our cmp-rotuine to evlauate 'these') .... eg, [k=2,....]: max{[Silhouette, Dunn, ...]}x clusterStability::[STD, Skewness, Kurtosis] ... and where we 'in our measurement-code' generate/build a "matrix[<m>][<m>]" where <m>="metric-combination to select the gold-standard-matrix", and where the 'resuting ehat-map is used to provide indications of simlairty and overlap in precdiciton-quality
      assert(false); // FIXME: wrt. [ªbove] try to explroe different 'k' thresholds ... <-- first 'ensure' that our appraoch is 'comptuationally feasible wrt. exeuction-time' ... and consider to use 'this proceudre' as an example of 'how our disjoitnk-appraoch may boost the analssys-performance'
      assert(false); // FIXME: apply k-means to [above] .... result: write out the clsuter-result which 'provide' the best k-means-seperation-score (testing the 'complete set' of different 'k', 'correlation-metrics' and 'k-means-centroid-identificaiton-algorithms'.
      assert(false); // FXIME: add something.

      assert(false); // FIXME: try to 'itnegrate' measures-of-statiblity' with 'cluster-simlairty-to-gold-stadnard'.
      assert(false); // FXIME: add something.
    }
    //! -----------------------------------------------------
    //!
    //! De-allocate:
    free__s_kt_matrix(&matrix_afterCorrelation); 
  }

  //! ----------------------------------------
  //! 
  //! De-allocate:
  free__s_kt_longFilesHandler_t(fileHandler__global);  fileHandler__global__ = *fileHandler__global;
}


//! The wrapper to our "divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t"
bool standAlone__divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t(const char ***matrixOf_stringsInEachCluster, const uint cnt_clusters, const uint max_cnt_vertices, const char *nameOf_file, const uint ideal_clusterCnt, const char *stringOf_resultPrefix,  bool inputMatrix__isAnAdjecencyMatrix) {
  //!
  //! What we expect:
  assert(matrixOf_stringsInEachCluster);
  assert(nameOf_file);
  assert(cnt_clusters > 0);
  assert(max_cnt_vertices > 0);

  //!
  //! Read the input-file
  s_kt_matrix_t matrixTo_evaluate;  setTo_empty__s_kt_matrix_t(&matrixTo_evaluate);
  const bool is_ok = readFrom__file__s_kt_matrix_t(&matrixTo_evaluate, nameOf_file);
  assert(is_ok); //! ie, as we expec thte operation was a sucesses.
  if(!is_ok || !matrixTo_evaluate.nrows || !matrixTo_evaluate.ncols) {
    fprintf(stderr, "!!\t Seems like we did Not manage to correcly load your input-file \"%s\", ie, please vlaidate correctness of your input-file-format (and/or the file-read-permissions). For quesitoni please contact the senior develoepr [oekseth@gamil.com]. Observation at [%s]:%s:%d\n", nameOf_file, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return false; //! ie, as there is 'no point in conituing'.
  }
  assert(matrixTo_evaluate.nrows > 0);

  //!
  //! Build the 'allocations' of user-specified clusters:
  const uint def_value = 0;
  uint *mapOf_nullHypothesis_vertexTo_centroidId = allocate_1d_list_uint(matrixTo_evaluate.nrows, def_value);
  for(uint row_id = 0; row_id < matrixTo_evaluate.nrows; row_id++) {
    mapOf_nullHypothesis_vertexTo_centroidId[row_id] = UINT_MAX; //! ie, as we assume taht 'each vertex by default is Not member of any clsuter', ie, a case which we 'hope' to fix in [”elow]
  }
  //! 
  //! Iterate through the vertex-string-memberships to identify the cluster-meberships.
  for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
    //! Iterate through each of the cluster-meberships:
    for(uint local_vertex_id = 0; local_vertex_id < max_cnt_vertices; local_vertex_id++) {
      const char *stringOf_vertex = matrixOf_stringsInEachCluster[cluster_id][local_vertex_id];
      if(stringOf_vertex && strlen(stringOf_vertex)) {
	const uint global_index = getIndexOf__atRow__s_kt_matrix(&matrixTo_evaluate, stringOf_vertex);
	if(global_index != UINT_MAX) {
	  assert(global_index < matrixTo_evaluate.nrows);
	  mapOf_nullHypothesis_vertexTo_centroidId[global_index] = cluster_id;
	} else {
	  fprintf(stderr, "!!\t Seems like your string=\"%s\" was Not found in the input-file \"%s\", ie, please vlaidate correctness of your calls. For quesitoni please contact the senior develoepr [oekseth@gamil.com]. Observation at [%s]:%s:%d\n", stringOf_vertex, nameOf_file, __FUNCTION__, __FILE__, __LINE__);
	}
      }
    }
  }
  
  
  //!
  //! The 'call':
  s_kt_assessPredictions_t self = setToEmpty__s_kt_assessPredictions_t();
  divergence_nullHypotheisisVs_metric__wrt_Kmeans__s_kt_assessPredictions_t(&self, mapOf_nullHypothesis_vertexTo_centroidId, &matrixTo_evaluate, ideal_clusterCnt, stringOf_resultPrefix, inputMatrix__isAnAdjecencyMatrix);
  
  //! -----------------------------------------------------
  //!
  //! De-allocate:
  free__s_kt_matrix(&matrixTo_evaluate); 
  free_1d_list_uint(&mapOf_nullHypothesis_vertexTo_centroidId);
  //! @return
  return true;
}
