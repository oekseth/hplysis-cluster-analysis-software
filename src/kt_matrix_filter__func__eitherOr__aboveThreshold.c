//!
//! Identify the thresholds:
if(self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent_scalarAbsMin != T_FLOAT_MIN_ABS) {
  t_float min_cnt_rows = self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[0];
  if(min_cnt_rows != T_FLOAT_MIN_ABS) {
    min_cnt_rows *= 0.01;
 }
  t_float min_cnt_columns = self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[1];
  if(min_cnt_columns != T_FLOAT_MIN_ABS) {
    //assert(max_cnt_columns >= min_cnt_columns);
    min_cnt_columns *= 0.01; //! ie, devidie by 100 to change form !%" to flaoting-point.
 }
  //! -----------------
  //!
  //! Apply the filters:
  const bool isTo_evaluateFor_rows = (min_cnt_rows    != T_FLOAT_MIN_ABS);
  const bool isTo_evaluateFor_cols = (min_cnt_columns != T_FLOAT_MIN_ABS);
  if( (isTo_evaluateFor_rows || isTo_evaluateFor_cols) ) {
    const uint default_value_float = 0;
    t_float *mapOf_count_rows = NULL; t_float *mapOf_count_cols = NULL;
    if(isTo_evaluateFor_rows) {mapOf_count_rows = allocate_1d_list_float(self->nrows, default_value_float);}
    if(isTo_evaluateFor_cols) {mapOf_count_cols = allocate_1d_list_float(self->ncols, default_value_float);}
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      if(self->mapOf_interesting_rows[row_id]) {
	const t_float *__restrict__ row = self->matrix[row_id];
	//! ---
	for(uint col_id = 0; col_id < self->ncols; col_id++) {
	  if(isOf_interest(row[col_id])) {
	    if(mathLib_float_abs(row[col_id]) > self->thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent_scalarAbsMin) {
	      if(isTo_evaluateFor_rows) {mapOf_count_rows[row_id] += 1;}
	      if(isTo_evaluateFor_cols) {mapOf_count_cols[col_id] += 1;}
	      /* if(isTo_evaluateFor_rows) {mapOf_count_rows[row_id] += row[col_id];} */
	      /* if(isTo_evaluateFor_cols) {mapOf_count_cols[col_id] += row[col_id];} */
	    }
	  }
	}
      }
    }
    //! 
    //! Apply the thresholds:
    if(isTo_evaluateFor_rows) {	
      const t_float nrows_inv = 1/(t_float)self->nrows;
      for(uint row_id = 0; row_id < self->nrows; row_id++) {
	//if( (mathLib_float_abs(mapOf_count_rows[row_id]) <= min_cnt_rows) ) {
	// printf("row[%u].cnt=%f=%f <? %f, given nrows=%u, at %s:%d\n", row_id, mapOf_count_rows[row_id], mapOf_count_rows[row_id]*nrows_inv,  min_cnt_rows, self->nrows, __FILE__, __LINE__);
	if(mapOf_count_rows[row_id]*nrows_inv < min_cnt_rows) {
	  if(self->mapOf_interesting_rows[row_id]) {
	    self->mapOf_interesting_rows[row_id] = false;
	    for(uint col_id = 0; col_id < self->ncols; col_id++) {
	      self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	    }
	  }
	}
      }
    }
    if(isTo_evaluateFor_cols) {
      const t_float ncols_inv = 1/(t_float)self->ncols;
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	// printf("col[%u].cnt=%f=%f <? %f, at %s:%d\n", col_id, mapOf_count_cols[col_id], mapOf_count_cols[col_id]*ncols_inv,  min_cnt_columns, __FILE__, __LINE__);
	if(self->mapOf_interesting_cols[col_id]) {
	  self->mapOf_interesting_cols[col_id] = false;
	  if(mapOf_count_cols[col_id]*ncols_inv < min_cnt_columns) {
	  //if( (mathLib_float_abs(mapOf_count_cols[col_id]) <= min_cnt_columns) ) {
	    //if( (mapOf_count_cols[col_id] <= min_cnt_columns)  || (mapOf_count_cols[col_id] >= max_cnt_columns) ) {
	    for(uint row_id = 0; row_id < self->nrows; row_id++) {
	      self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	    }
	  }
	}
      }
    }

    //! De-allcoate locally reserved memory:
    if(isTo_evaluateFor_rows) {free_1d_list_float(&mapOf_count_rows);}
    if(isTo_evaluateFor_cols) {free_1d_list_float(&mapOf_count_cols);}
  }
 } //! else we asusme no threshold is set.
