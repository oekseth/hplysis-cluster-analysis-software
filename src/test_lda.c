<span>/// &lt;summary&gt;</span><br /><span>///   Computes the
  Multi-Class Linear Discriminant Analysis
  algorithm.</span><br /><span>///
  &lt;/summary&gt;</span><br /><span>public</span> <span>virtual</span> <span>void</span>
  Compute()<br />{<br />    <span>// Compute entire data set
  measures</span><br />    Means = Tools.Mean(source);<br />
  StandardDeviations = Tools.StandardDeviation(source,
  totalMeans);<br />    <span>double</span> total =
  dimension;<br /><br />    <span>// Initialize the scatter
  matrices</span><br />    <span>this</span>.Sw
  = <span>new</span> <span>double</span>[dimension,
  dimension];<br />    <span>this</span>.Sb
  = <span>new</span> <span>double</span>[dimension,
  dimension];<br /><br /><br />    <span>// For each
  class</span><br />    <span>for</span> (<span>int</span> c = 0; c
  &lt; Classes.Count; c++)<br />    {<br />        <span>// Get the
  class subset</span><br />        <span>double</span>[,] subset =
  Classes[c].Subset;<br />        <span>int</span> count =
  subset.GetLength(0);<br /><br />        <span>// Get the class mean
  and number of samples</span><br />        <span>double</span>[] mean
  = Tools.Mean(subset);<br /><br /><br />        <span>// Continue
  constructing the Within-Class Scatter
  Matrix</span><br />        <span>double</span>[,] Swi =
  Tools.Scatter(subset, mean);<br />        Sw =
  Sw.Add(Swi);<br /><br />        <span>// Continue constructing the
  Between-Class Scatter
  Matrix</span><br />        <span>double</span>[] d =
  mean.Subtract(totalMeans);<br />        <span>double</span>[,] Sbi =
  d.Multiply(d.Transpose()).Multiply(total/count);<br />        Sb =
  Sb.Add(Sbi);<br /><br />        <span>// Store some additional
  information</span><br />        <span>this</span>.classScatter[c] =
  Swi;<br />        <span>this</span>.classCount[c] =
  count;<br />        <span>this</span>.classMeans[c] =
  mean;<br />        <span>this</span>.classStdDevs[c] =
  Tools.StandardDeviation(subset, mean);<br />
  }<br /><br /><br /><br />    <span>// Compute eigen value
  decomposition</span><br />    EigenValueDecomposition evd
  = <span>new</span>
  EigenValueDecomposition(Matrix.Inverse(Sw).Multiply(Sb));<br /><br />    <span>//
  Gets the eigenvalues and corresponding
  eigenvectors</span><br />    <span>double</span>[] evals =
  evd.RealEigenValues;<br />    <span>double</span>[,] eigs =
  evd.EigenVectors;<br /><br /><br />    <span>// Sort eigen values
  and vectors in ascending order</span><br />    eigs =
  Matrix.Sort(evals, eigs, <span>new</span>
  GeneralComparer(ComparerDirection.Descending, <span>true</span>));<br /><br /><br />    <span>//
  Calculate translation bias</span><br />    <span>this</span>.bias =
  eigs.Transpose().Multiply(totalMeans).Multiply(-1.0);<br /><br />    <span>//
  Store information</span><br />    <span>this</span>.EigenValues =
  evals;<br />    <span>this</span>.DiscriminantMatrix =
  eigs;<br /><br /><br />    <span>// Create
  projections</span><br />    <span>this</span>.result
  = <span>new</span> <span>double</span>[dimension,
  dimension];<br />    <span>for</span> (<span>int</span> i = 0; i
  &lt; dimension; i++)<br />        <span>for</span> (<span>int</span>
  j = 0; j &lt; dimension; j++)<br />            <span>for</span>
  (<span>int</span> k = 0; k &lt; dimension; k++)<br />
  result[i, j] += source[i, k] * eigenVectors[k,
  j];<br /><br /><br />    <span>// Computes additional information
  about the analysis and creates the</span><br />    <span>//
  object-oriented structure to hold the discriminants
  found.</span><br />    createDiscriminants();<br />
