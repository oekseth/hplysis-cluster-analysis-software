#ifndef hp_clusterFileCollection_simMetricSet_h
#define hp_clusterFileCollection_simMetricSet_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_clusterFileCollection_simMetricSet
   @brief provide logics to analyse a subset of the simlairty-metrics supported by hpLyssis Knitting-Tools sim-metric suit (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
**/

#include "hpLysis_api.h"

/**
   @enum e_hp_clusterFileCollection_simMetricSet
   @brief provide logics to analyse a subset of the simlairty-metrics supported by hpLyssis Knitting-Tools sim-metric suit (oekseth, 06. feb. 2017).
   @remarks 
   -- this enum is used to 'identify'/'say' the type of simalrity-emtircs to investigate, eg, 'none', MINE__Euclid, 'small-set', 'medium-set', all_nopnePostProcessing, all_inlcudingPostProcessingOptions ... and thereafter write an iteraiton to idneitfy the 'compelte number of data-sets to evlauate for'.
   -- use-case: to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
**/
typedef enum e_hp_clusterFileCollection_simMetricSet {
  //! Note: if we udpate [”elow] then remeber to also update our "data/local_downloaded/convert.pl".
  e_hp_clusterFileCollection_simMetricSet_MINE_Euclid,
  e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared,
  e_hp_clusterFileCollection_simMetricSet_subset__small,
  e_hp_clusterFileCollection_simMetricSet_subset__medium,
  e_hp_clusterFileCollection_simMetricSet_all_notPostProcess,
  e_hp_clusterFileCollection_simMetricSet_all,
  e_hp_clusterFileCollection_simMetricSet_undef //! ie, Not apply sim-metrics.
} e_hp_clusterFileCollection_simMetricSet_t;

/**
   @struct s_hp_clusterFileCollection_simMetricSet
   @brief hold a list of silmiaryt-metrics to evaluate (oekseth, 06. feb. 2017).
 **/
typedef struct s_hp_clusterFileCollection_simMetricSet {
  s_kt_correlationMetric_t *list;
  uint list_size;
} s_hp_clusterFileCollection_simMetricSet_t;

/**
   @brief idneitfy an array of simlarity-emtrics assicated to enum_id (oesketh, 06. feb. 2017).
   @param <enum_id> is the enum to cosntuct the object for.
   @param <includeEmptyCase> which if set tot true impleis taht we 'alos' include an enum 'descriibng' an 'undef' case.
   @return a new-allcoated list of sim-metrics
   @remarks in the routien we (for some of the enum-ids) include a "e_kt_correlationFunction_undef". The latter iot. to (simplify the caller-logics wrt.) explcitly handle the case where we have an 'undef' enum in the 'spec' (where the latter is used' to describe the case where sim-metric is Not to be applied').
 **/
static s_hp_clusterFileCollection_simMetricSet_t init__hp_clusterFileCollection_simMetricSet(const e_hp_clusterFileCollection_simMetricSet_t enum_id, const bool includeEmptyCase) {
  s_hp_clusterFileCollection_simMetricSet_t self_base;
  s_hp_clusterFileCollection_simMetricSet_t *self = &self_base;
  self->list_size = 0;

  //!
  //! Identify the number of metrics to allcoate:
  if(enum_id == e_hp_clusterFileCollection_simMetricSet_MINE_Euclid) {
    self->list_size = 2;
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared) {
    self->list_size = 2;
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_subset__small) {
    self->list_size = 0;
    for(uint met = 0; met < e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; met++) {
      const e_kt_correlationMetric_initCategory_timeComplexitySet__small_t met_enum = (e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)met;
      const s_kt_correlationMetric_t obj_metric = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(met_enum);
      if(isTo_use_directScore__e_kt_correlationFunction(obj_metric.metric_id) == false) {
	self->list_size++; //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
      }
    }
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_subset__medium) {
    self->list_size = e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef;
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_all_notPostProcess) {
    self->list_size = 0; //e_kt_correlationFunction_undef;
    //! Idneitfy the number of sim-metircs with a broad/general applicaiton:
    for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
      e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; 
      if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	 (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	 || 
#endif
	 describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	 || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	 ) {continue;} //! ie, as we then assuem 'this is Not of interest'
      //! Udpat ehte count 'for the itnersting corr-emtics':
      self->list_size++;
    }
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_all) {
    //! Idneitfy the number of sim-metircs with a broad/general applicaiton:
    for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
      for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; 
	if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	   (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	   || 
#endif
	   describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	   || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	   ) {continue;} //! ie, as we then assuem 'this is Not of interest'
	//! Udpat ehte count 'for the itnersting corr-emtics':
	self->list_size++;
      }
    }
    //self->list_size = e_kt_correlationFunction_undef * (e_kt_categoryOf_correaltionPreStep_none+1); 
  } else {
    fprintf(stderr, "!!\t(option-not-supported)\t Add support for the measure=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
  }
  //! ---- 
  if(includeEmptyCase) {
    self->list_size += 1; //! where '+1' is used to 'handle' the case where 'no silimarty-emtric' is used.
  }
  //! -----------------------------------------------------------------------------------------
  //!
  //! Allcoate memroy-space:
  assert(self->list_size > 0);
  self->list = alloc_generic_type_1d_xmtMemset(s_kt_correlationMetric_t, self->list, self->list_size);
  assert(self->list);

  //!
  //! Set the simlairty-emtrics, ie, itnate the objects:
  uint current_pos = 0;
  if(enum_id == e_hp_clusterFileCollection_simMetricSet_MINE_Euclid) {
    assert(self->list_size >= 2);
    init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/e_kt_correlationFunction_groupOf_MINE_mic, /*post-process=*/e_kt_categoryOf_correaltionPreStep_none); current_pos++;
    //printf("mine-mic=%u, at %s:%d\n", e_kt_correlationFunction_groupOf_MINE_mic, __FILE__, __LINE__);
    init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*post-process=*/e_kt_categoryOf_correaltionPreStep_none); current_pos++;
    assert(current_pos == 2); //! ie, what we expect.
    //init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/, /*post-process=*/); current_pos++;
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared) {
    assert(self->list_size >= 2);
    init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/e_kt_correlationFunction_groupOf_minkowski_cityblock, /*post-process=*/e_kt_categoryOf_correaltionPreStep_none); current_pos++;
    //printf("mine-mic=%u, at %s:%d\n", e_kt_correlationFunction_groupOf_MINE_mic, __FILE__, __LINE__);
    init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*post-process=*/e_kt_categoryOf_correaltionPreStep_none); current_pos++;
    assert(current_pos == 2); //! ie, what we expect.
    //init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/, /*post-process=*/); current_pos++;
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_subset__small) {
    for(uint met = 0; met < e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; met++) {
      const e_kt_correlationMetric_initCategory_timeComplexitySet__small_t met_enum = (e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)met;
      const s_kt_correlationMetric_t obj_metric = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(met_enum);
      if(isTo_use_directScore__e_kt_correlationFunction(obj_metric.metric_id) == false) {
	assert(current_pos < self->list_size); //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
	//! Update:
	self->list[current_pos] = obj_metric;
	//! --- 
	current_pos++;
      }
    }
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_subset__medium) {
    assert(self->list_size >= e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef);
    for(uint met = 0; met < e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef; met++) {
      assert(current_pos < self->list_size); //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
      s_kt_correlationMetric_t obj = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t((e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t)met); 
      self->list[current_pos] = obj;
      current_pos++;
    }
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_all_notPostProcess) {
    assert(self->list_size > 0); //e_kt_correlationFunction_undef;
    //! Idneitfy the number of sim-metircs with a broad/general applicaiton:
    for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
      e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; 
      if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	 (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	 || 
#endif
	 describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	 || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	 ) {continue;} //! ie, as we then assuem 'this is Not of interest'
      //! Udpat ehte count 'for the itnersting corr-emtics':
      assert(current_pos < self->list_size); //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
      init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/dist_metric_id, /*post-process=*/e_kt_categoryOf_correaltionPreStep_none); current_pos++;
    }
  } else if(enum_id == e_hp_clusterFileCollection_simMetricSet_all) {
    //! Idneitfy the number of sim-metircs with a broad/general applicaiton:
    for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
      for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; 
	if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	   (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	   || 
#endif
	   describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	   || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	   ) {continue;} //! ie, as we then assuem 'this is Not of interest'
	//! Udpat ehte count 'for the itnersting corr-emtics':
	assert(current_pos < self->list_size); //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
	init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/dist_metric_id, /*post-process=*/(e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep); current_pos++;
      }
    }
    //self->list_size = e_kt_correlationFunction_undef * (e_kt_categoryOf_correaltionPreStep_none+1); 
  } else {
    fprintf(stderr, "!!\t(option-not-supported)\t Add support for the measure=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)enum_id, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
  }
  //! ---- 
  if(includeEmptyCase) {
    assert(current_pos < self->list_size); //! ie, as we for simlyt is Not itnerested ine vlauating 'firect scores'
    init__s_kt_correlationMetric_t(&(self->list[current_pos]), /*metric=*/e_kt_correlationFunction_undef, /*post-process=*/e_kt_categoryOf_correaltionPreStep_none); current_pos++;
    //self->list_size += 1; //! where '+1' is used to 'handle' the case where 'no silimarty-emtric' is used.
  }
  //! /1licdate consistency:
  assert(current_pos == self->list_size);
  //! -----------------------------------------------------------------------------------------
  //!
  //! @return
  return self_base;
}

//! De-allocate the s_hp_clusterFileCollection_simMetricSet_t object (oekseth, 06. feb. 2017).
static void free__s_hp_clusterFileCollection_simMetricSet_t(s_hp_clusterFileCollection_simMetricSet_t *self) {
  if(self->list) {
    assert(self->list_size > 0);
    self->list_size = 0;
    free_generic_type_1d((self->list));  self->list = NULL; //! ie, de-allocate.
  }
}

#endif //! EOF
