  //! -----------------------
  { const char *stringOf_measureText = "dense row-iteration-4d: traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
	    sumOf_values += matrix[row_id_out][col_id_out];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  //! -----------------------
  { const char *stringOf_measureText = "dense row-iteration-4d(transposed): traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
	    sumOf_values += matrix_transposed[col_id_out][row_id_out];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }

{ //! Fast implementaiotns of Kendall using KLnight's algorithm:
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::fast::mask-none::weight-none): comptue Kendall's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true; //! ie, call the funciton in our "correlationType_kendall.c"

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, /*weight=*/NULL, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::fast::mask-none): comptue Kendall's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::fast::mask-explicit): comptue Kendall's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::fast::mask-implicit): comptue Kendall's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
}
//! ---------------------------------------------- Slow implemetnaiotns using Knedalls 'default' algorithm:
{
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::mask-implicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, weight, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::mask-explicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::mask-none): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, weight, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::dense-matrix::mask-none::weights-none): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	sumOf_values += kt_kendall(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, NULL, row_id, row_id_out, /*transpose=*/false, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }  
}
//! ***********************************************************************************************************************
//! --------------------------- Comapre implementations using the implemetnaiton foudn in the "cluster.c" -----------------
{
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::clusterC::dense-matrix::mask-explicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! --------------
	assert(mask1_int[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kendall__slow_naiveImplementaiton(/*ncols=*/size_of_array, matrix, matrix, mask1_int, mask2_int, weight, row_id, row_id_out, /*transpose=*/false);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
#if(__localConfig__in2dKendall__evalauteTransposedCase  == 1) 
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::clusterC::dense-matrix::mask-none::transposed): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
  	//! --------------
  	sumOf_values += kendall__slow_naiveImplementaiton(/*ncols=*/size_of_array, matrix_transposed, matrix_transposed, mask1_int_transposed, mask2_int_transposed, weight, row_id, row_id_out, /*transpose=*/true);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]);
  }
#endif //! endif("__localConfig__in2dKendall__evalauteTransposedCase == 1")
}
//! ***********************************************************************************************************************
//! --------------------------- An improvement fo wrt. the if-clause-sentences in the "clsuter.c"
{
  const bool isTo_useImprvoedIfClause = true;
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::clusterC--ifClauseImporved::clusterC::dense-matrix::mask-implicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! --------------
	sumOf_values += kendall__slow_naiveImplementaiton_improvedWrt_ifClause(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, weight, row_id, row_id_out, /*transpose=*/false, isTo_useImprvoedIfClause);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::clusterC--ifClauseImporved::clusterC::dense-matrix::mask-explicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! --------------
	assert(mask1_int[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kendall__slow_naiveImplementaiton_improvedWrt_ifClause(/*ncols=*/size_of_array, matrix, matrix, mask1_int, mask2_int, weight, row_id, row_id_out, /*transpose=*/false, isTo_useImprvoedIfClause);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::clusterC--ifClauseImporved::clusterC::dense-matrix::mask-none): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! --------------
	sumOf_values += kendall__slow_naiveImplementaiton_improvedWrt_ifClause(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, weight, row_id, row_id_out, /*transpose=*/false, isTo_useImprvoedIfClause);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::clusterC--ifClauseImporved::clusterC::dense-matrix::mask-none::weights-none): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! --------------
	sumOf_values += kendall__slow_naiveImplementaiton_improvedWrt_ifClause(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, NULL, row_id, row_id_out, /*transpose=*/false, isTo_useImprvoedIfClause);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }  
}
{ //! ***************************************************** test for a 'deidated' all-againsta-ll-function:
  t_float **matrix_result = allocate_2d_list_float(nrows, size_of_array, default_value_float);
  { //! ------------------- Fast implementaitons:
    { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::fast::maskNone::weightNone): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = true;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, /*weight=*/NULL, matrix_result, /*transpose=*/false, /*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
    { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::fast::maskNone): compute Kendall's Tau using Knight's algorithm: call-3d-func-computations(..): identifies the time-minimization-gains wrt. using a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = true;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, matrix_result, /*transpose=*/false, /*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
    { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::fast::maskImplicit): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = true;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, matrix_result, /*transpose=*/false, /*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
    { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::fast::maskExplicit): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = false;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, /*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, matrix_result, /*transpose=*/false, /*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
  }
  { //! ------------------- Slow implementaitons:
    { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
      self.forNonTransposed_useFastImplementaiton = false;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, /*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, matrix_result, /*transpose=*/false, /*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    } 
    { const char *stringOf_measureText = "sort-and-rank-2d(Kendall::slow::transposed): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      t_float **matrix_result_transposed = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
      self.forNonTransposed_useFastImplementaiton = false;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, /*ncols=*/size_of_array,  matrix, matrix, mask1, mask2, weight, matrix_result_transposed, /*transpose=*/true, /*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
      free_2d_list_float(&matrix_result_transposed, size_of_array);
    } 
  }     
  //! -------------------------------------
  //! De-allcoate locally reserved memory:
  free_2d_list_float(&matrix_result, nrows);
}

//! *****************************************************
#undef __localConfig__in2dKendall__evalauteTransposedCase
