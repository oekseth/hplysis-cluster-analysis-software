#ifndef measure_h
#define measure_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "libs.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h> // some systems require it
#include <sys/stat.h>
#include <sys/termios.h> // for winsize
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/times.h>
#include "types.h"
/**
   @brief Functionality for measurement
   @remarks Added to streamline the process.
 **/
class measure {
 private:
  clock_t clock_time_start;
  struct tms tms_start;
  struct tms tms_end;
  clock_t clock_time_end; 

  //! An system error message to be thrown.
  void err_sys(const char *err) const {
    fprintf(stderr, "!!\t %s I.e. an system error occured, in file %s\n", err, __FILE__);
    assert(false);
  }

 public:
  /**
     @brief Start the measurement with regard to time.
     @remarks 
     - Uses the global vairalbe in this file for this prupose.
     - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
  **/
  void start_time_measurement() {
#ifndef NDEBUG
    //! Clears the cache by allocating 100MB of memory- and the de-allocating it:
    // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses.
    uint size = 1024*1024*100;
    char *tmp = new char[size];
    assert(tmp);
    memset(tmp, 7, size);
    delete [] tmp; tmp = NULL; size = 0;
#endif
    tms_start = tms();
    if((clock_time_start = times(&tms_start)) == -1) // starting values
      err_sys("times error");
  }

  /**
     @brief Ends the measurement with regard to time.
     @return the clock tics on the system since the 'start_time_measurement()' method was called.
   **/
  double end_time_measurement() {
    double user_time = 0, system_time=0;
    return end_time_measurement(NULL, user_time, system_time);
  }

  /**
     @brief Ends the measurement with regard to time.
     @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
     @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
     @return the clock tics on the system since the 'start_time_measurement()' method was called.
   **/
  double end_time_measurement(double &user_time, double &system_time) {
    return end_time_measurement(NULL, user_time, system_time);
  }
  /**
     @brief Ends the measurement with regard to time.
     @param <msg> If string is given, print the status information to stdout.
     @return the clock tics on the system since the 'start_time_measurement()' method was called.
   **/
  double end_time_measurement(char *msg) {
    double user_time = 0, system_time=0;
    return end_time_measurement(msg, user_time, system_time);
  }

  /**
     @brief Ends the measurement with regard to time.
     @param <msg> If string is given, print the status information to stdout.
     @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
     @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
     @return the clock tics on the system since the 'start_time_measurement()' method was called.
   **/
  double end_time_measurement(char *msg, double &user_time, double &system_time) {
    if((clock_time_end = times(&tms_end)) == -1) err_sys("times error"); // starting values
    long clktck = 0; if((clktck =  sysconf(_SC_CLK_TCK)) < 0) 	err_sys("sysconf error");
    const clock_t t_real = clock_time_end - clock_time_start;
    const double time_in_seconds = t_real/(double)clktck;
    const double diff_user = tms_end.tms_utime - tms_start.tms_utime;
    if(diff_user) user_time   = (diff_user)/((double)clktck);
    const double diff_system = tms_end.tms_stime - tms_start.tms_stime;
    system_time = diff_system/((double)clktck);
    if(msg) printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
    //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
    return time_in_seconds;
  }

  /**
     @brief Set the results in the given parameters.
     @param <ticks_time> Sets the time values for the ticks.
     @param <user_time> Sets the time values for the cpu.
     @param <system_time> Sets the time values for the system.
   **/
  void get_result(double &ticks_time, double &user_time, double &system_time) const {
    //! Intialises.
    ticks_time = 0, user_time = 0, system_time = 0;
    long clktck = 0; if((clktck =  sysconf(_SC_CLK_TCK)) < 0) 	err_sys("sysconf error");
    //! Sets the time values for the ticks:
    const clock_t t_real = clock_time_end - clock_time_start;
    if(t_real) ticks_time = t_real/(double)clktck;
    //! Sets the time values for the user time:
    const double diff_user = tms_end.tms_utime - tms_start.tms_utime;
    if(diff_user) user_time   = (diff_user)/((double)clktck);
    //! Sets the time values for the system time:
    const double diff_system = tms_end.tms_stime - tms_start.tms_stime;
    system_time = diff_system/((double)clktck);
  }

  //! Generates a formatted result:
  void print_formatted_result(char *id_string) {
    assert(id_string);
    //! Get the time values for 'this':
    double time_in_seconds = 0, user_time = 0, system_time=0;
    get_result(time_in_seconds, user_time, system_time);
    printf("tick=%.4fs cpu=%.4fs system=%.4fs %s\n", time_in_seconds, user_time,  system_time, id_string);
  }
  /**
     @brief Compare two results verbously to stdout.
     @param <time_linear> The argument, assued to represent the optimal solution to the problem measured.
     @param <id_string> The verbous identifier for this comparison.
   **/
  void compare_with_linear_version(const class measure *time_linear, char *id_string) {
    assert(time_linear);
    assert(id_string);
    //! Get the time values for 'this':
    double time_in_seconds = 0, user_time = 0, system_time=0;
    get_result(time_in_seconds, user_time, system_time);
    //! Get the time values for the argument.
    double time_in_seconds_linear = 0;   double user_time_linear = 0, system_time_linear=0;
    time_linear->get_result(time_in_seconds_linear, user_time_linear, system_time_linear);

    const double max = 10000; // To make it obvious that its the best.
    //! The relative gain for the ticks:
    double diff_ticks = 0;
    if(time_in_seconds && !time_in_seconds_linear) diff_ticks = max;
    else if(time_in_seconds) diff_ticks = time_in_seconds / time_in_seconds_linear;

    //! The relative gain for the user time:
    double diff_user=0;
    if(user_time && !user_time_linear) diff_user =  max; 
    else if(user_time) diff_user  = user_time / user_time_linear;

    //! The relative gain for the system time:
    double diff_system=0;
    if(user_time && !system_time_linear) diff_system = max;
    if(system_time) diff_system  = system_time / system_time_linear;
    
    //! Prints the result:
    printf("tick=%.4fs (%.4fx) cpu=%.4fs (%.4fx) system=%.4fs (%.4fx) %s\n", time_in_seconds, diff_ticks, user_time, diff_user, system_time, diff_system,  id_string);
  }
  /**
     @brief De-allocates memory reserved.
     @remarks added for future use.
  **/
  void free_memory() {};
  //! Default constructor.
 measure() {
   clock_time_start = clock_t();
   clock_time_end   = clock_t();
   tms_start = tms();
   tms_end = tms();
  };
};


#endif
