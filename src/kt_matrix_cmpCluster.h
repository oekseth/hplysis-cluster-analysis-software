#ifndef kt_matrix_cmpCluster_h
#define kt_matrix_cmpCluster_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_matrix_cmpCluster
   @brief a wrapper to provide temrina-access adn high-elvel access to the itnernal data-matrix
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "kt_matrix.h"
#include "kt_matrix_setOf.h"
#include "e_kt_correlationFunction.h"
#include "e_kt_clusterComparison.h"
#include "kt_set_2dsparse.h"
#include "e_kt_api_dynamicKMeans.h" //! whioch includes the "e_kt_api_dynamicKMeans_t" enum used in [”elow] (eosekth, 60. feb. 2017).
//#include "kt_api.h"
#include "kt_api__centerPoints.h"

/**
   @enum e_kt_matrix_cmpCluster_metric
   @brief describes the diffenret clusterc-comaprison-metrics (CCMs) supported in oru hpLyssi software (oekseth, 06. jan. 2017)
 **/
typedef enum e_kt_matrix_cmpCluster_metric {  
  e_kt_matrix_cmpCluster_metric_randsIndex, //! ie, "Rands index", where the quation uses the 'formualte' specified [<in Rands orginal artilce>]
  e_kt_matrix_cmpCluster_metric_randsIndex_alt2, //! o
  //! Note: an example of a 'possible wekaness' wrt. the the "Adjusted Rand Index" (ARI) conserns the case where a cluster correctly 'hold' |cluster[i]|='1' (ie, for which we assume that "sum(1 Combinatorics 2 == 0) == 0"
  e_kt_matrix_cmpCluster_metric_randsAdjustedIndex, //! ie, "Rands adjusted index", which use ["http://www.diss.fu-berlin.de/diss/servlets/MCRFileNodeServlet/FUDISS_derivate_000000016251/diss-sharon-online.pdf", equation (3.8)] and ["http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.164.6189&rep=rep1&type=pdf" w/title "Comparing Clusterings - An Overview"]
  //e_kt_matrix_cmpCluster_metric_randsAdjustedIndex_alt2, //! which use the equation defined in 
  //! ----
  e_kt_matrix_cmpCluster_metric_chiSquared, //! which for cases where (m[i][j] == 'max-cnt-paris') results in '0': for speical cases the metric will Always return '0', ie, a non-dessciptive metric.
  //! ----
  e_kt_matrix_cmpCluster_metric_FowlesMallows,
  e_kt_matrix_cmpCluster_metric_Mirkin, //! where 'mian-copmtuation' is exactly simliar to "Fowles-Mallows"
  e_kt_matrix_cmpCluster_metric_Wallace, //! which is exactly simliar to "Fowles-Mallows"
  //! ----
  e_kt_matrix_cmpCluster_metric_Jaccard, //! at index=7
  e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity,
  e_kt_matrix_cmpCluster_metric_Jaccard_F1_score, //! which use the same approach as in "e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity" to identfy the "recall" and "precision", though in cotnrast to the latter combines the 'result' (ie, instead of 'returning' a pair ("https://clusteval.sdu.dk/1/clustering_quality_measures/18").
  e_kt_matrix_cmpCluster_metric_Minkowski, //! index=10
  e_kt_matrix_cmpCluster_metric_PR,
  e_kt_matrix_cmpCluster_metric_PR_alt2, //! where we use an alternative 'interpretaiotn' of the PR-metric
  e_kt_matrix_cmpCluster_metric_partitionDifference,
  //! ----
  // FIXME[article]: consider to comapre/discuss "f-measure", "entropy", and "Euclid" in the conttext of ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0665-2"] ... eg, where the latter authrops asserts that "Quantative results showing the F-measure and entropy values as a function of number of clusters. The F-measure results (top row) show a better performance for Algorithm 1 using data mappings (HC) than those for k-means (KM) clustering on normalized histograms directly." .... while nmot evlauating the prediciotn-accuracy of their evlauated CCMs
  e_kt_matrix_cmpCluster_metric_fMeasure,
  e_kt_matrix_cmpCluster_metric_maximumMatchMeasure, 
  e_kt_matrix_cmpCluster_metric_vanDogen,
  //! Note: the "van Dogen" measurement is specifically suited for 'standard adjustments sued in correlaiton-matrices' as the correlation-metic sub-divide the cases into 'max' sperately for 'columns' and 'rows'
  e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relative, //! index=17
  e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared,
  e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_min,
  e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_max, //! index=20
  //! ----
  e_kt_matrix_cmpCluster_metric_mutualInforation_Strehl_Ghosh,
  e_kt_matrix_cmpCluster_metric_mutualInforation_Fred_Jain,
  //! ------------
  e_kt_matrix_cmpCluster_metric_variationOfInformation,
  e_kt_matrix_cmpCluster_metric_variationOfInformation_oekseth_relative,
  //! --------------------------------
  // FIXME: implement and 'incldue' [”elow]
  // FIXME: cosnider to comapre "gapValue__algorithmPermutation" with the to-be-implemneted "C index" 
  //! Compute[gapValue__algorithmPermutation]: abs(log(W(gold-standard) - log(W))), where "W =  sum_{k < |C|}{sum_{(i, k) in C(k)}{d(i, k) in C(k)}/(2*|C(k)|)}" <-- for "d(i, k)" we test/evlaute both wrt. 'a simple summation' and 'compelx metics' (such as Euclid) <-- udpate our test-evlauated in oru "acm__clustMetricCmp.tex" wrt. 'such a test'. <-- citaiton: cosnider to 'refer' to "Estimating the number of clusters in a data set via the gap statistic"
  // e_kt_matrix_cmpCluster_metric_gapValue__algorithmPermutation,
  //! --------------------------------
  // e_kt_matrix_cmpCluster_metric_
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----  
  e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum,
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum, */
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenMax, */
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax, */
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_max_scores, */
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_max_ranks, */
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_min_scores, */
  /* e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_min_ranks, */

  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               Matrix-based measures                                        ----
  
  //! --------------------------------
  //! Compute[Silhouette]: (1/cnt-vertices)*sum{(minDist - vDist[i][clustId(i)])/max{minDist, vDist[i][clustId(i)]}}, where "minDist = min{vDist[i][k != clustId(i)]" (used to find the difference/deviation between 'best-fit to other clsuters' VS 'correcntess of being part in this cluster', ie, in cotnrast to other metrics 'this Silhouette-metric' evlauates teh accuracy/descrptivness of each vertex being part of 'this cluster', and thereafter 'sums' the agreement for the complete set of vertices').... where "vDist[i][clusterId(j)]" is the vertex-distance between vertex=i and clusterId(vertex=j) (a distance which is may be computed using the options 'represented' by "e_kt_matrix_cmpCluster_clusterDistance__config_metric__t"=[sum, avg, min, max] in hpLysis); ... for details wrt. "c[..][...]" see "Dunn". .... a high value in 'this metric' indicates that 'the clusters are good/reprsentaitve' (ie, as an relative decrease in the 'sum of weights in a given clsuter implicates that the cluster-veritces have a high degrree of closeness, hence the cluster-meberhsips are 'tight', a 'itghtness' which idnicates that the clsuter-sets are well-seperated (wrt. the data-matrix used as a clsute-bassis (eg, adjsuted through a corrleaiton-rpe-step)). 
  // FIXME: write an alternative permtaution of ... "Silhouette" ... where we ... use silhouttte-alg(1) in "~/Skrivebord/CCMs_overvierw_subset.png"
  // FIXME: write an alternative permtaution of ... "Silhouttte" ... where we ... use silhouttte-alg(2) in "~/Skrivebord/CCMs_overvierw_subset.png"
  // FIXME: write an alternative permtaution of ... "" ... where we ...
  e_kt_matrix_cmpCluster_metric_silhouetteIndex, //! which focus on vertex-distances to clusters (ie, in contrast to the 'other set-based approaches').
  //! Compute[DavisBouldin]: (1/cnt-clusters)*sum_{cluster_id=i}(max{(c[i][i] + c[j][j])/c[i][j]}) ... for details wrt. "c[..][...]" see "Dunn".
  e_kt_matrix_cmpCluster_metric_DavisBouldin, //! where small vlaues indicates good clusters (ie, as an increased "c[i][j]" inidcates larger distance betweenc clusters).
  //! Compute[Dunn]: min{min{c[i][j] / max_clustSize}}, where "max_clustSize = max{c[i][i]}", and where "c[i][j]" is the distance between cluster[i] and cluster[j], ie, for which "cluster[i][] describes the 'internal distance in a givne cluster'.
  e_kt_matrix_cmpCluster_metric_Dunn, //!   which aims at idneitfying clusters which are both compact and high-seperated: where the distance between differnet clusters 'considerably' excceds the internal clsuter deviaiton/difference (eg, as seen wrt. the 'inner' "max" seleciton-critera in Dunns equation).
  //! --------------------------------
  // FXIEM: implement and 'incldue' [”elow]
  //! Compute[CalinskiHarabasz]: "VRC(k) = (SS(between)/SS(within))*(N-k)/(k -1)", where the clatter Sub-set (SS) distances are computed wrt. "SS(between) = sum_{k < |clusters|}{(cnt-vertices in cluster(k))*||AVG(ceneotrid(cluster(k))) - AVG(<matrix>)||^2}" and "SS(within) = sum_{k < |clusters|}{sum_{x in C(k)}{||x - AVG(ceneotrid(cluster(k)))||^2 }}", where "||....||" describes a distnace/correlation-metic (suhc as Euclid). <-- citaiton: cosnider to 'refer to' article="A dendrite method for cluster analysis"
  // e_kt_matrix_cmpCluster_metric_CalinskiHarabasz, //! "Calinski-Harabasz criterion clustering evaluation object" ("https://se.mathworks.com/help/stats/clustering.evaluation.calinskiharabaszevaluation-class.html")
  //! --------------------------------
  //e_kt_matrix_cmpCluster_metric_,
  //! --------------------------------
  e_kt_matrix_cmpCluster_metric_undef,
} e_kt_matrix_cmpCluster_metric_t;

/**
   @enum e_kt_matrix_cmpCluster_categoriesOfMetrics
   @brief caterogies the "e_kt_matrix_cmpCluster_metric_t" eumes (oekseth, 06. jan. 2017).
 **/
typedef enum e_kt_matrix_cmpCluster_categoriesOfMetrics {
  e_kt_matrix_cmpCluster_categoriesOfMetrics_all,
  e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased,
  e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric,
  e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__subset_extremeCases,
  e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__all,
  // e_kt_matrix_cmpCluster_categoriesOfMetrics_,
  //! ---------
  e_kt_matrix_cmpCluster_categoriesOfMetrics_undef
} e_kt_matrix_cmpCluster_categoriesOfMetrics_t;

//! @reutrn the number of matrics assicated to a particular "e_kt_matrix_cmpCluster_categoriesOfMetrics_t" cateorgy-enum.
static uint getCntIn__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(const e_kt_matrix_cmpCluster_categoriesOfMetrics_t enum_id) {
  assert(enum_id != e_kt_matrix_cmpCluster_categoriesOfMetrics_undef);
  const uint cnt_matrixBased_all = (e_kt_matrix_cmpCluster_metric_undef - e_kt_matrix_cmpCluster_metric_silhouetteIndex);
  assert(cnt_matrixBased_all > 0);
  if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_all) {return e_kt_matrix_cmpCluster_metric_undef;
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased) {
    return (e_kt_matrix_cmpCluster_metric_silhouetteIndex - e_kt_matrix_cmpCluster_metric_randsIndex);
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric) {
    return cnt_matrixBased_all;
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__subset_extremeCases) {
    //! Note: call our "getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(..)" to 'get' for ”[elow]
    uint cnt_local = 0; //
    for(uint index = 0; index < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; index++) {
      const s_kt_correlationMetric_t obj_metric = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)index);
      if(isTo_use_directScore__e_kt_correlationFunction(obj_metric.metric_id)) {continue;}
      cnt_local++;
    }
    //e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef;
    assert(cnt_local > 0);
    return cnt_local*cnt_matrixBased_all;
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__all) {
    uint cnt_local = 0;
    for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
      for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
	e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;
	if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	   (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	   || 
#endif
describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	   || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	   ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
	cnt_local++;
      }
    }
    assert(cnt_local > 0);
    return cnt_local*cnt_matrixBased_all;
  } else {
    assert(false); //! ie, as we then need adding support 'for this issue'
  }


  assert(false); //! ie, an heads-up.
  return e_kt_matrix_cmpCluster_metric_undef; //! ie, a 'fall back'.
}

//! @reutrn the start-pos fo the givne enum, ie, to 'ensure' a density wrt.  a matrix-isenrtion (oekseth, 06. jan. 2017).
static uint getEnumStartPosOffset__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(const e_kt_matrix_cmpCluster_categoriesOfMetrics_t enum_id) {
  assert(enum_id != e_kt_matrix_cmpCluster_categoriesOfMetrics_undef);
  const uint cnt_matrixBased_all = (e_kt_matrix_cmpCluster_metric_undef - e_kt_matrix_cmpCluster_metric_silhouetteIndex);
  assert(cnt_matrixBased_all > 0);
  if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_all) {return 0;
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased) {
    return 0;
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric) {
    return e_kt_matrix_cmpCluster_metric_silhouetteIndex; //! ie, the fist index.
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__subset_extremeCases) {
    return e_kt_matrix_cmpCluster_metric_silhouetteIndex; //! ie, the fist index.
  } else if(enum_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__all) {
    return e_kt_matrix_cmpCluster_metric_silhouetteIndex; //! ie, the fist index.
  } else {
    assert(false); //! ie, as we then need adding support 'for this issue'
  }

		      /* //! Configure how we are to infer the metrics: */
		      /* s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t(); */
		      /* //! state that we are to use a complex comparsin-strategy: */
		      /* config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true; */
		      /* //! Set the tyep of clsuterign-comparison-strategy to use: */
		      /* config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between; */
		      /* //! Set the type of corrleaiton-metirc to be used: */
		      /* config_intraCluster.metric_complexClusterCmp_obj.typeOf_correlationPreStep = dist_metricCorrType; */
		      /* config_intraCluster.metric_complexClusterCmp_obj.metric_id = dist_metric_id; */

  assert(false); //! ie, an heads-up.
  return 0; //! ie, a 'fall back'.
}


//! @return the enum-category assicated to a particular enum.
static e_kt_matrix_cmpCluster_categoriesOfMetrics_t getEnumCategory__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(const e_kt_matrix_cmpCluster_metric_t enum_id) {
  if(enum_id == e_kt_matrix_cmpCluster_metric_randsIndex) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_randsIndex_alt2) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_randsAdjustedIndex) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_chiSquared) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_FowlesMallows) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Mirkin) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Wallace) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard_F1_score) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Minkowski) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_PR) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_PR_alt2) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_partitionDifference) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_fMeasure) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;} 
  else if(enum_id == e_kt_matrix_cmpCluster_metric_maximumMatchMeasure) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  //! ...
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relative) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_min) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_max) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  //! ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_mutualInforation_Strehl_Ghosh) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_mutualInforation_Fred_Jain) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_variationOfInformation) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_variationOfInformation_oekseth_relative) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased;}
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;}
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenMax) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;} */
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               Matrix-based measures                                        ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_silhouetteIndex) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_DavisBouldin) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Dunn) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric;}

  //! **************************************************************************************************'
  //! **************************************************************************************************'  
  else if(enum_id == e_kt_matrix_cmpCluster_metric_undef) {return e_kt_matrix_cmpCluster_categoriesOfMetrics_undef;}
  else {
    fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false);
    return e_kt_matrix_cmpCluster_categoriesOfMetrics_undef;
  }
}

/**
   @brief builds an object used to comptue for the complete set of CCMs supported by hpLysis.
   @param <cnt_datasets> si the number of data-sets to allocate in each matrix.
   @return an intiated object.
   @remarks the idea is to store a compelte set of CCM-resutls therby simplifying an evlauation of clstuer-predicitosn in the cotnext of muliple CCMs. In our 'baisc access-strategty' we assume a data-access-pattern of [CCM-cateogry][data_id < cnt_datasets][CCM_id], ie, where the latter asusmption is 'used' when storing the result
 **/
static s_kt_matrix_setOf_t init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(const uint cnt_dataSets) {
  assert(e_kt_matrix_cmpCluster_categoriesOfMetrics_undef != 0);
  s_kt_matrix_setOf_t obj_list = initAndReturn__s_kt_matrix_setOf_t(/*list_size=*/(uint)e_kt_matrix_cmpCluster_categoriesOfMetrics_undef, /*nrows=*/0, /*ncols=*/0);
  assert(obj_list.list_size == (uint)e_kt_matrix_cmpCluster_categoriesOfMetrics_undef);
  //!
  //! Iterate through each of the CCM-categories:
  for(uint ccm_cat_index = 0; ccm_cat_index < (uint)e_kt_matrix_cmpCluster_categoriesOfMetrics_undef; ccm_cat_index++) {
    const e_kt_matrix_cmpCluster_categoriesOfMetrics_t ccm_cat = (e_kt_matrix_cmpCluster_categoriesOfMetrics_t)ccm_cat_index;
    const uint cnt_measures = getCntIn__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm_cat);
    assert(cnt_measures > 0);
    //printf("(init)\tcnt_cols=%u given ccm_category=%u, at %s:%d\n", cnt_measures, ccm_cat_index,__FILE__, __LINE__);
    const bool is_ok = setMatrixSize__s_kt_matrix_setOf_t(&obj_list, /*list_id=*/ccm_cat_index, /*nrows=*/cnt_dataSets, /*ncols=*/cnt_measures);
    assert(is_ok); //! ie, what we expect. 
  }  
  //!
  //! @return
  return obj_list;
}

/**
   @brief export the generated results and provide lgocis for matrix-deivaiton-construciton and data-merging
   @param <self> is the object itnaited in "init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(..)"
   @param <stringOf_result>  which if Not set to NULL is used as the file-name-midfix to exprot the result.
   @param <supersetTo_update>  which if Not set to NULL is updated witht he cotnents of 'this': should be rpe-allcoted with enopugh 'space' for such an update.
   @param <mapOf_listElements__currentPos> is the next psotioin to 'insert at': udpated with 'knowledge of this'.
   @param <typeOf_extractToPrint> which if Not set to e_kt_matrix__exportFormats___extremeToPrint_undef is used to print an 'extreme case' wrt. the data-input
 **/
static void exportResult__andDeAllocate__kt_matrix_cmpCluster(s_kt_matrix_setOf_t *self, const char *stringOf_result, s_kt_matrix_setOf_t *supersetTo_update, uint *mapOf_listElements__currentPos, const e_kt_matrix__exportFormats___extremeToPrint_t typeOf_extractToPrint) {
  assert(self); assert(self->list_size);
  if(stringOf_result) {
    assert(strlen(stringOf_result));
    //! Export wr.t hte assumpitoan that data is ordered wrt.: "[CCM-cateogry][data_id < cnt_datasets][CCM_id]":
    for(uint list_id = 0; list_id < self->list_size; list_id++) {
      if(self->list[list_id].nrows > 0) {
	assert(strlen(stringOf_result) < 1000); //!§ ie, to simplify our logics.
	char stringOf_result_local[2000] = {'\0'}; 
	{
	  const char *string_spec = NULL;
	  if(list_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_all) {string_spec = "all";}
	  else if(list_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased) {string_spec = "setBased";}
	  else if(list_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_naiveDistanceMetric) {string_spec = "matrixBasedDefault";}
	  else if(list_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__subset_extremeCases) {string_spec = "matrixBasedHpLysisSub";}
	  else if(list_id == e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__all) {string_spec = "matrixBasedHpLysisAll";}
	  else {assert(false);} //! ie, as we then need adding support 'for this'.
	  sprintf(stringOf_result_local, "%s_%s", stringOf_result, string_spec);
	}
	//!
	//! Export the results 'as-is':
	s_kt_matrix_t matrix_transposed; init__copy_transposed__s_kt_matrix(&matrix_transposed, &(self->list[list_id]), /*isTo_updateNames=*/false);
	{
	  char result_file[2000] = {'\0'}; sprintf(result_file, "%s.%s.tsv", stringOf_result_local, "score");	  
	  const bool is_ok = export__singleCall__s_kt_matrix_t(&matrix_transposed, result_file, /*fileHandler=*/NULL);
	  assert(is_ok);
	}
	{
	  char result_file[2000] = {'\0'}; sprintf(result_file, "%s.%s.tsv", stringOf_result_local, "rank");
	  s_kt_matrix_t matrix_ranks; init__copy__useRanksEachRow__s_kt_matrix(&matrix_ranks, &matrix_transposed, /*isTo_updateNames=*/false);
	  const bool is_ok = export__singleCall__s_kt_matrix_t(&matrix_ranks, result_file, /*fileHandler=*/NULL);
	  assert(is_ok);
	  { //! Note: in order to facialte the comparsion of differences in the data-sets (seperately wrt. teach CCM) we first comptue the ranks (for teach data-set) and thereaafter transpsoe the matrix:
	    s_kt_matrix_t matrix_transposed; init__copy_transposed__s_kt_matrix(&matrix_transposed, &matrix_ranks, /*isTo_updateNames=*/false);	    
	    {
	      char result_file[2000] = {'\0'}; sprintf(result_file, "%s.%s.tsv", stringOf_result_local, "transposed.deviation");
	      const bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(&matrix_transposed, result_file, /*fileHandler=*/NULL, /*config_isToTranspose__whenGeneratingOutput=*/true, /*format=*/e_kt_matrix__exportFormats_tex, typeOf_extractToPrint);
	      assert(is_ok);
	    }
	    {
	      char result_file[2000] = {'\0'}; sprintf(result_file, "%s.%s.tsv", stringOf_result_local, "transposed.tex.deviation");	    
	      const bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(&matrix_transposed, result_file, /*fileHandler=*/NULL, /*config_isToTranspose__whenGeneratingOutput=*/true, /*format=*/e_kt_matrix__exportFormats_tsvMatrix, typeOf_extractToPrint);
	      assert(is_ok);
	    }
	    free__s_kt_matrix(&matrix_transposed);
	  }
	  free__s_kt_matrix(&matrix_ranks);
	}
	{ //! Ntoe: we 'choose' to first transpose the matrix in order to compare the deviaotn between the data-sets (for a gvien CCM) rather than between teh CCMs: 
	  {
	    char result_file[2000] = {'\0'}; sprintf(result_file, "%s.%s.tsv", stringOf_result_local, "deviation");
	    const bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(&matrix_transposed, result_file, /*fileHandler=*/NULL, /*config_isToTranspose__whenGeneratingOutput=*/true, /*format=*/e_kt_matrix__exportFormats_tex, typeOf_extractToPrint);
	    assert(is_ok);
	  }
	  {
	    char result_file[2000] = {'\0'}; sprintf(result_file, "%s.%s.tsv", stringOf_result_local, "tex.deviation");
	    
	    const bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(&matrix_transposed, result_file, /*fileHandler=*/NULL, /*config_isToTranspose__whenGeneratingOutput=*/true, /*format=*/e_kt_matrix__exportFormats_tsvMatrix, typeOf_extractToPrint);
	    assert(is_ok);
	  }
	}
	free__s_kt_matrix(&matrix_transposed);
      }
    }
  } 
  
  if(mapOf_listElements__currentPos && supersetTo_update) { //! Then udpate the superset with the 'elements' in thsi:
    //!
    //! Then we inser thte 'current data' as 'new rows' in the 'superset':
    for(uint list_id = 0; list_id < self->list_size; list_id++) {
      //! Get the current pos and therafter udpte for the next 'run':
      const uint new_currPos = mapOf_listElements__currentPos[list_id];
      mapOf_listElements__currentPos[list_id] += self->list[list_id].nrows;
      assert(mapOf_listElements__currentPos[list_id] <= supersetTo_update->list[list_id].nrows); //! ie, as we expect 'palce' to have been allcoated 'for this'.
      //!
      //! Copy:
      for(uint row_id = 0; row_id < self->list[list_id].nrows; row_id++) {
	for(uint col_id = 0; col_id < self->list[list_id].ncols; col_id++) { supersetTo_update->list[list_id].matrix[row_id + new_currPos][col_id] = self->list[list_id].matrix[row_id][col_id];}	
      }
      // const uint new_currPos = self->list[list_id].nrows + supersetTo_update__currentPos;
    }
    
  } // else {return supersetTo_update__currentPos;}
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(self); 
}

//! @return true if the measure is 'described' as a matrix-based measure.
static bool isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(const e_kt_matrix_cmpCluster_metric_t enum_id) {
  return (enum_id >= e_kt_matrix_cmpCluster_metric_variationOfInformation);
}
//! @return a string-represetnation of the enum_id (oekseth, 06. des. 2016).
const char *getStringOf__e_kt_matrix_cmpCluster_metric_t(const e_kt_matrix_cmpCluster_metric_t enum_id);
//! @return a string-represetnation of the enum_id (oekseth, 06. des. 2016).
const char *getStringOf__e_kt_matrix_cmpCluster_metric_t__short(const e_kt_matrix_cmpCluster_metric_t enum_id);
//! @return an enum-representaiton of the string (oekseth, 06. mar. 2017).
static const e_kt_matrix_cmpCluster_metric_t getEnumOf__e_kt_matrix_cmpCluster_metric_t(const char *cmp) {
  assert(cmp); assert(strlen(cmp));
  for(uint i = 0; i < e_kt_matrix_cmpCluster_metric_undef; i++) {
    {
      const char *str = getStringOf__e_kt_matrix_cmpCluster_metric_t((e_kt_matrix_cmpCluster_metric_t)i);
      // printf("\t\t(cmp::short::metric=%u) \"%s\" VS \"%s\", at %s:%d\n", i, cmp, str, __FILE__, __LINE__);
      if(str && strlen(str)) {
	if(strlen(str) == strlen(cmp)) {
	  if(0 == strncasecmp(str, cmp, strlen(cmp))) {
	    return (e_kt_matrix_cmpCluster_metric_t)i; //! ie, as hte string is then found
	  }
	}
      }
    }
    { //! Then test an alterntive 'version' (oekseth, 06. otk. 2017):
      const char *str = getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i);
      if(str && strlen(str)) {
	if(strlen(str) == strlen(cmp)) {
	  if(0 == strncasecmp(str, cmp, strlen(cmp))) {
	    return (e_kt_matrix_cmpCluster_metric_t)i; //! ie, as hte string is then found
	  }
	}
      }
    }
  }
  //! A fall-back:
  return e_kt_matrix_cmpCluster_metric_undef;
}
//! Updates the scalar_result with the idneified enum
static void getEnumOf__e_kt_matrix_cmpCluster_metric_t__retVal(const char *cmp,  e_kt_matrix_cmpCluster_metric_t *scalar_result) {
  assert(cmp); assert(strlen(cmp)); assert(scalar_result);
  *scalar_result = getEnumOf__e_kt_matrix_cmpCluster_metric_t(cmp);
}
//! Updates the scalar_result with the idneified enum
static void getEnumOf__e_kt_matrix_cmpCluster_metric_t__retVal__uint(const char *cmp,  uint *scalar_result) {
  assert(cmp); assert(strlen(cmp)); assert(scalar_result);
  *scalar_result = (uint)getEnumOf__e_kt_matrix_cmpCluster_metric_t(cmp);
}

/**
   @enum e_kt_matrix_cmpCluster_clusterDistance__config_metric
   @brief describes the dstatety to combine different scores for "vertexTo_clusters" in "s_kt_matrix_cmpCluster_clusterDistance" (oekseth, 06. des. 2016).
 **/
typedef enum e_kt_matrix_cmpCluster_clusterDistance__config_metric {
  // FIXME[article]: seems like [”elow] permtautiosn are simlair to the work of ["http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=499469"], awork which 'suggest' [”elow] evaluation-permtaution-appraoches for "Dunns index" (eosketh, 06. feb. 2017).
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum,
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg,
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__min,
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__max,
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD, //! eg, for "//e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC".
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, //! which if used 'implies' that we use the 'naive'a ppraoch, ie, the 'apporach used by most in reseharc-ltiterature',.
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__undef
} e_kt_matrix_cmpCluster_clusterDistance__config_metric__t;

//! @return teh string-rpesentaiton of the e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_id (oekseth, 06. des. 2016).
const char *getString__e_kt_matrix_cmpCluster_clusterDistance__config_metric__t(const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_id );

/**
   @enum e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint
   @brief describe how the mid-points are to be comptued for vertices (oekseth, 06. feb. 2017).
 **/
typedef enum e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint {
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex, //! ie, the default alterantive: use 'matrix as is'.
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal, //! the distance from the vertex-center to the 'midpoint' of the network, a 'midpoint-centoid' defined by the "e_kt_api_dynamicKMeans_t" enum.
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexCentroid, //! ie, the dsitance from each vertex (in a cluster) to the centroid.
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidVertex, //! ie, the 'inverse' of [abov€], a difference which becomes of signcinace in 'between-comutation' (and Not wrt. within-copmtuation) if 'average', 'max' .... (or any ot the other distnac-eocnfiguarion-propeites) are used.
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid, //! distance from centroids in a cluster: when comptuing for 'clsuter-within-dsitance' we (as the current defualt stragtwegy) use "centroidGlobal"
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal, //! where we compute centroid(cluster_i)--->centroid(network)
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal__mulByWeight, //! where we adjust [ªbove] by "*|vertices-in-cluster|".
  // e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_,
  // e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_,
  //! ------------------ 
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef,
} e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t;

/**
   @struct s_kt_matrix_cmpCluster_clusterDistance_config
   @brief configures how teh clsuter-distance-comparison is to be 'applied' (oekseth, 06. des. 2016).
 **/
typedef struct s_kt_matrix_cmpCluster_clusterDistance_config {
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_clusterDistance__vertexVertex;
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_clusterDistance__cluster_cluster;
  bool metric_complexClusterCmp_vertexCmp_isTo_use; //! which if set to false implies that we use the input-correlation-matrix for the vertex-vectrex-cmp
  bool metric_complexClusterCmp_obj_isTo_use; //! which if set to false implies that we use the input-correlation-matrix.
  s_kt_correlationMetric_t metric_complexClusterCmp_obj;  
  e_kt_clusterComparison_t metric_complexClusterCmp_clusterMethod; //! which describes how "metric_complexClusterCmp_obj" is to be used when buidling "cluster_cluster"
  //!
  //! 'Advandced' strategies for 'cofngiruting' the center-point for both clusters and a network in general:  
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t centroidMetric__vertexVertex__networkReference__between;
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t centroidMetric__vertexVertex__networkReference__within;
  e_kt_api_dynamicKMeans_t centroidMetric__vertexVertex__center; //! a 'center-poitn-algorithm' expected to be deinfed in our "kt_api.h": used for cofniguraiton.cases where a user asusmes that 'a center of a clsuter' is a desciptive reference-point when comapring clusters (oekseth, 06. feb. 2017).
  //! 
  //! KD-tree-logic-support:
  bool isTo_use_KD_treeInSimMetricComputations; //! which is used if the simrlairyt-metric is not a-propry comptued.
} s_kt_matrix_cmpCluster_clusterDistance_config_t;


/**
   @struct s_kt_matrix_cmpCluster_clusterDistance
   @brief provide data-containers to store the reslationship between cluster-partitions (oekseth, 06. des. 2016).
   @remarks for an example-applciaiton of the data-structures used in the s_kt_matrix_cmpCluster_clusterDistance see 
 ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"]
 **/
typedef struct s_kt_matrix_cmpCluster_clusterDistance {
  t_float **vertexTo_clusters; //! hold a format=[vertex][distance-to-each-cluster_id] distance.
  t_float **cluster_cluster; //! hold a format=[cluster_id][cluster_id] distance, ie, the distance between vertices in all clusters, eg, where "cluster_cluster[1][1]" describes the itnra-cluster-distance for cluster=1
  uint **cluster_cluster__count; //! which hold the vertex-count for eh vertices in "cluster_cluster", eg, used for DavisBouldin to get the 'realtive wigicnace of weight-sums' in each cluster-chunk'.
  uint *mapOf_vertexClusterIds; //! format=[vertex-->cluster_id]: describe/'hold' the cluster-id-mapping for each vertex, eg, used when 'finding' the cluster-distance 'to all vertices in the same clsuter' (ie, when accessing the "vertexTo_clusters" variable).
  uint nrows; //! ie, the number of vertices
  uint ncols; 
  uint cnt_cluster; //! ie, the number of clusters
  s_kt_set_2dsparse_t mappings_cluster_toVertex;
  t_float **input_matrix;

  //!
  //! Local-configruations:
  s_kt_matrix_cmpCluster_clusterDistance_config_t config; //! ie, the lcoal configruaiton-object, an object expected to be set in the "init__s_kt_matrix_cmpCluster_clusterDistance(...)" call.
  bool comptued_subDataStrucutres; //! which is used to 'know' if a data-pre-step-copmptaution' ahs been appleid/comptued (oesketh, 06. jun. 2017).
} s_kt_matrix_cmpCluster_clusterDistance_t;

//! Initiates the "s_kt_matrix_cmpCluster_clusterDistance_config_t" to default values (oekseth, 06. des. 2016).
//! @return a s_kt_matrix_cmpCluster_clusterDistance_config_t object which is set to empty.
s_kt_matrix_cmpCluster_clusterDistance_config_t setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
//! Initiates the "s_kt_matrix_cmpCluster_clusterDistance_t" to default values (oekseth, 06. des. 2016).
s_kt_matrix_cmpCluster_clusterDistance_t setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();

/**
   @enum e_kt_matrix_cmpCluster_clusterDistance__cmpType
   @brief Describe the different matrix-centered cluster-comparison-matrix supported in our "s_kt_matrix_cmpCluster_clusterDistance"
 **/
typedef enum e_kt_matrix_cmpCluster_clusterDistance__cmpType {
  // ----
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_iIndex,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_gammaMod,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_xieBeni,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_sdValidity,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_VNND,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_FigureOfMerit,
  // ----
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared,
  //  e_kt_matrix_cmpCluster_clusterDistance__cmpType_,
  // ----
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE,
  //e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum,
  /* e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum, */
  /* e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_scores_Euclid_within_divBy_betweenMax, */
  /* e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_ranks_Euclid_within_divBy_betweenMax, */
  // ----
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth, //! where we 'adjust' the latter Dunns index by  $max\{d(C_l, C_l), d(c_m, c_m)\}$, an appraoch/strategy which is asusmed/used/hyptoesed to give increased weight-signicnace of denser clusters.
  // e_kt_matrix_cmpCluster_clusterDistance__cmpType_
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin, //! where "Minkowski:Euclid" is the default"; 
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween, //! where we use the "self->cluster_cluster[cluster_id_in][cluster_id_out]"; for a 'differetn metric' update the "self->metric_clusterDistance__cluster_cluster" parameter/object.
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth, //! "Dunn-relative (oekseth)", where we 'adjust' Dunns index by  $max\{d(C_l, C_l), d(c_m, c_m)\}$, an appraoch/strategy which is asusmed/used/hyptoesed to give increased weight-signicnace of denser clusters.
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, //! which is expected to be last.
  // ----
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef
} e_kt_matrix_cmpCluster_clusterDistance__cmpType_t;


static e_kt_matrix_cmpCluster_clusterDistance__cmpType_t map_setBased_SSE_toMatrixBased__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_metric_t cmp_type) {
  if(cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {
    return  e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE; //_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum;
  /* } if(cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum)  { */
  /*   return e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum ; */
  /* } if(cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenMax)  { */
  /*   return  e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_scores_Euclid_within_divBy_betweenMax; */
  /* } if(cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax)  { */
  /*   return e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_ranks_Euclid_within_divBy_betweenMax ; */
  } 
  assert(false); //! ie, as theiu is Not supproted
  return e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE; // _vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum; //! ie, a fall-bacl
}

//! @return teh string-represetnation of the e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id in 1uesiton.
const char *getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id);

//! @return teh string-represetnation of the e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id in 1uesiton.
//! @remarks a mdoficaito of "getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(..)" where we 'return' shorter IDs, eg, to reduce clsutter in web-based viauzlaitions of the reuslts (oekseth, 06. mar. 2017)
const char *getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id);

//! @return an enum-representaiton of the string (oekseth, 06. mar. 2017).
static const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const char *cmp) {
  assert(cmp); assert(strlen(cmp));
  for(uint i = 0; i < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
    const char *str = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
    // printf("\t\t(cmp::short::metric=%u) \"%s\" VS \"%s\", at %s:%d\n", i, cmp, str, __FILE__, __LINE__);
    if(str && strlen(str)) {
      if(strlen(str) == strlen(cmp)) {
	if(0 == strncasecmp(str, cmp, strlen(cmp))) {
	  return (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i; //! ie, as hte string is then found
	}
      }
      //!
      //! Try a 'short-hand' version:
      // printf("\t\t(cmp::short::metric=%u) \"%s\" VS \"%s\", at %s:%d\n", i, cmp, str, __FILE__, __LINE__);
      str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
      if(str && strlen(str)) {
	if(strlen(str) == strlen(cmp)) {
	  if(0 == strncasecmp(str, cmp, strlen(cmp))) {
	    return (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i; //! ie, as hte string is then found
	  }
	}
      }

    }
  }
  //! A fall-back:
  return e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
}

//! @return an enum-representaiton of the string (oekseth, 06. mar. 2017).
static void getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal(const char *cmp,  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t *scalar_result) {
  assert(cmp); assert(strlen(cmp)); assert(scalar_result);
  *scalar_result = getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(cmp);
}
//! @return an enum-representaiton of the string (oekseth, 06. mar. 2017).
static void getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal__uint(const char *cmp,  uint *scalar_result) {
  assert(cmp); assert(strlen(cmp)); assert(scalar_result);
  *scalar_result = (uint)getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(cmp);
}

//! @return the 'extreme-poor-score' assicated to the ccm_id (oekseht, 06. mar. 2017)
const t_float getWorstCase_score__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id);

/**
   @struct s_kt_matrix_ccm_matrixBased
   @brief a new strucutre to hold the genralised result-set during a CCM-matrix-cotpmatuion (oesketh, 06. jun. 2017).
 **/
typedef struct s_kt_matrix_ccm_matrixBased {
  t_float sumOf_result__between;     t_float sumOf_result__within;
  t_float between_min; t_float between_max;
  t_float within_min; t_float within_max;
  t_float sumScores_total; //! ie, $\sum \sum d(x, y)$.
} s_kt_matrix_ccm_matrixBased_t;

//! Compute the cluster-comparison-metric of a 'genriec itnerpation' wrt betweneens and within-ness, ie, a gnerlaized appraoch which may be sued to simplify consturction of deidcted/spelied fucnitons (oekseth, 06. jun2. 2017)
//! @rerturn true upon success.
bool compute__generic__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec, s_kt_matrix_ccm_matrixBased_t *ret_object); //t_float *scalar_within, t_float *scalar_between);
/**
   @brief compute the cluster-comparison-metric of: Calinski-Harabasz (VRC) (oekseth, 06. feb. 2017)
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__min
   @param <isTo_adjustEachRowScore_by_log> which if set to true impleis taht we adjust each 'row-specific' score by an 'abs-log-tranformatiion' (ie, before inclduign the score into the 'sum'), thereby adjusting the 'impact' of 'extrem high' scores: in the 'naive appraoch' set "isTo_adjustEachRowScore_by_log = false".
   @param <distance_clusterCluster_internalSimMetric> which if Not set to e_kt_correlationFunction_undef is used to constuct a local verison of the "cluster_cluster" cluster-cluster clsuter-distance-metric.
   @param <isTo__useInternalObjectSpec> which if set to false implies that we re-compute the internal distance-spec based on thec rurent cofniguraitons.
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__VRC__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec);

/**
   @brief compute the cluster-comparison-metric of: Sum-of-Error (oekseth, 06. juni. 2017)
   @param <enum_type> is the enum-type to comptue for
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__min
   @param <isTo_adjustEachRowScore_by_log> which if set to true impleis taht we adjust each 'row-specific' score by an 'abs-log-tranformatiion' (ie, before inclduign the score into the 'sum'), thereby adjusting the 'impact' of 'extrem high' scores: in the 'naive appraoch' set "isTo_adjustEachRowScore_by_log = false".
   @param <distance_clusterCluster_internalSimMetric> which if Not set to e_kt_correlationFunction_undef is used to constuct a local verison of the "cluster_cluster" cluster-cluster clsuter-distance-metric.
   @param <isTo__useInternalObjectSpec> which if set to false implies that we re-compute the internal distance-spec based on thec rurent cofniguraitons.
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__SSE__s_kt_matrix_cmpCluster_clusterDistance_t(
							       //const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_type, 
							       const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec);

/**
   @brief compute the cluster-comparison-metric of: Silhouette (oekseth, 06. des. 2016)
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__min
   @param <isTo_adjustEachRowScore_by_log> which if set to true impleis taht we adjust each 'row-specific' score by an 'abs-log-tranformatiion' (ie, before inclduign the score into the 'sum'), thereby adjusting the 'impact' of 'extrem high' scores: in the 'naive appraoch' set "isTo_adjustEachRowScore_by_log = false".
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__Silhouette__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log);
/**
   @brief compute the cluster-comparison-metric of: Davis-Bouldin (oekseth, 06. des. 2016)
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__min
   @param <isTo_adjustEachRowScore_by_log> which if set to true impleis taht we adjust each 'row-specific' score by an 'abs-log-tranformatiion' (ie, before inclduign the score into the 'sum'), thereby adjusting the 'impact' of 'extrem high' scores: in the 'naive appraoch' set "isTo_adjustEachRowScore_by_log = false".
   @param <distance_clusterCluster_internalSimMetric> which if Not set to e_kt_correlationFunction_undef is used to constuct a local verison of the "cluster_cluster" cluster-cluster clsuter-distance-metric.
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__DavisBouldin__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric);
/**
   @brief compute the cluster-comparison-metric of: Dunn's Validation index (oekseth, 06. des. 2016)
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__max
   @param <isTo_adjustInternalByMax> which if set to true impleis that we use a CCM-permtuation suggested by oekseht, a permtuation whiche evluates the desctipvie-effect of adjsuting the sum by the max-score.
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__Dunns__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustInternalByMax);

/**
   @brief compute the cluster-comparison-metric of the matric specifid by enum_id (oekseth, 06. des. 2016)
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <enum_id> is the type fo cluster-comparison-metric to apply.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__max
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type);
/**
   @brief initates the s_kt_matrix_cmpCluster_clusterDistance_t object
   @param <self> is the object to intiate.
   @param <map_vertexToCluster> is the vertex--cluster mappings for the cnt_vertices
   @param <cnt_vertices> is the number of vertices in the adjcency-matrix matrix
   @param <cnt_features> the number of fequares: for an adjcency-matirx "cnt_features" should equal cnt_vertices.
   @param <cnt_cluster> is the number of clusters (ie, the number of unique clsuter-ids in map_vertexToCluster)
   @param <matrix> is the distance-matrix (which is to be used when assessing the distance between the clsuters).
   // @param <mapOf_vertexClusterIds> hold the cluster-ids' which is assicated to a particualr vertex.
   @param <config> is used to idneityf the type of centrlaity-approach to use.
   @remarks we currecntly support "cnt_vertices != cnt_features" for only a subse trof the CCM-metris: for questiosn and feature-support-request please cotnact senior developer [oesketh@gmail.com]
 **/
void init__s_kt_matrix_cmpCluster_clusterDistance(s_kt_matrix_cmpCluster_clusterDistance_t *self, const uint *map_vertexToCluster, const uint cnt_vertices, uint cnt_features, const uint cnt_clusters, t_float **matrix, s_kt_matrix_cmpCluster_clusterDistance_config_t config);
//! De-allcoates the "s_kt_matrix_cmpCluster_clusterDistance_t" self object.
void free__s_kt_matrix_cmpCluster_clusterDistance_t(s_kt_matrix_cmpCluster_clusterDistance_t *self);
//void init__s_kt_matrix_cmpCluster_clusterDistance_t(const uint 

/**
   @struct s_kt_matrix_cmpCluster
   @brief an object used to gernalize the construction/inference of cluster-simliarity-metrics (oekseth, 06. des. 2016).
   @remarks the clsuter-simliarity-emtircs which we 'here' explictly support  are those which 'diveres' from ordinary cluster-comparison-metircs: for detals see our "e_kt_matrix_cmpCluster_metric_t" enum
 **/
typedef struct s_kt_matrix_cmpCluster {
  uint cnt_vertices; //! ie, the number of vertices which the "conusion matrix" stored in matrixOf_coOccurence 'refers to'.
  s_kt_matrix_t matrixOf_coOccurence; //! which may be 'built' using "build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(...)"
  s_kt_matrix_t matrixOf_coOccurence_or; //! simliar to "matrixOf_coOccurence": expeted to hold 'the sum of pairs which may be in the set': to compute 'sum of pairs that are in different clusters' use "score = matrixOf_coOccurence_or.matrix[head_id][tail_id] - matrixOf_coOccurence.matrix[head_id][tail_id]"
  //!
  //! Remember the number of elements in each cluster:
  uint mapOfCluster1_size; 
  uint mapOfCluster2_size; 
  t_float *mapOfCluster1; 
  t_float *mapOfCluster2; 
  //! ----
  s_kt_matrix_cmpCluster_clusterDistance_t clusterDistances_cluster1; //! which hold a 'compute set' of distance-scores for "cluster 1"
  s_kt_matrix_cmpCluster_clusterDistance_t clusterDistances_cluster2; //! which hold a 'compute set' of distance-scores for "cluster 2"
  t_float **external_matrixOf_coOccurence_1; //! which is a reference to the 'external matrix' used in the itnaiotn (ie, fi any). Note: we do Not allcoate emmory for this
  t_float **external_matrixOf_coOccurence_2; //! which is a reference to the 'external matrix' used in the itnaiotn (ie, fi any). Note: we do Not allcoate emmory for this
} s_kt_matrix_cmpCluster_t;


//! Set the s_kt_matrix_cmpCluster_t object to empty.
void setTo_empty__s_kt_matrix_cmpCluster_t(s_kt_matrix_cmpCluster_t *self);
//! Initates an object
void init__s_kt_matrix_cmpCluster(s_kt_matrix_cmpCluster_t *self, const uint nrows, const uint ncols, const bool isTo_allocateWeightColumns) ;
//! De-allocates the object
void free__s_kt_matrix_cmpCluster(s_kt_matrix_cmpCluster_t *self);

//! Build a co-occurence-matrix of entites between the two clsuter-sets
//! @remarks the result is stored in the "self->matrixOf_coOccurence" parameter and may be 'used' in/from our "computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(..)" function
void build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(s_kt_matrix_cmpCluster_t *self, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, const bool isTo_includeWeight, const uint max_cntClusters, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2, s_kt_matrix_cmpCluster_clusterDistance_config_t config);

/**
   @struct s_kt_matrix_cmpCluster_result_scalarScore
   @brief a structure to hold the result of the comptuations (oekseth, 06. des. 2016).
   @remarks used to simplify the 'task to return results described by more than one scalar' (eg, as seen wrt. the metric of "Wallace").
 **/
typedef struct s_kt_matrix_cmpCluster_result_scalarScore {
  //! Note: for "Wallace": score_1="precision", which described 'reatio-relevant-documents/cnt-retrived-documents'; score_2="recall", which described 'retio-relevant-documents/cnt-relevant-documents'
  //! Note(2): similiar to "Wallace" the "e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity" 'describes' both "sensitivty" and "specifcity", ie, where the latter uses both "score_1" and "score_2" in 'its' result-set
  t_float score_1; //! which is expected to be used for all cases
  t_float score_2; //! which is used for: clsuter-comparison-metrics=["Wallace"]
} s_kt_matrix_cmpCluster_result_scalarScore_t;

//! Initaites the s_kt_matrix_cmpCluster_result_scalarScore_t strucutre
//! @return a structure where both valeus are set to '0' (oekseth@ des. 201&).
s_kt_matrix_cmpCluster_result_scalarScore_t setTo_empty__s_kt_matrix_cmpCluster_result_scalarScore_t();

//! @brief Compute the cluster-compiarson-metric score for a given cluster-comparison-metric cmp_type (oekseth, 06. des. 2016).
//! @return a cluster-comparison-metric-score which describes the simliarty between the rows and volumsn in self->external_matrixOf_coOccurence
// //! @remarks the "matrix_usedInClustering" parameter is only used for metrics=[e_kt_matrix_cmpCluster_metric_silhouetteIndex,]
void computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(const s_kt_matrix_cmpCluster_t *self, const e_kt_matrix_cmpCluster_metric_t cmp_type, s_kt_matrix_cmpCluster_result_scalarScore_t *scalar_result); //, t_float **matrix_usedInClustering);


/**
   @brief compute co-occurence-matrix for the complete set of CCMs supported by hpLyssi (oekseth, 06. jan. 2017).
   @remarks for details wrt. teh input-parameters please ee our "init__s_kt_matrix_cmpCluster(..)" and our "build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(..)" funcitons.
   @remarks 
   - input: the "self" paremter is expected to have been itnalized using a call to our "init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(..)"
   - result: we store the resutls in the order of: [CCM-cateogry][data_id < cnt_datasets][CCM_id]
   @remarks idea is to both simplify, and to provide, an analsys-appraoch where we evaluate the agreement-in-predictions between CCMs (for a chunk of data-sets which are assumed to be desciptive for a specific use-case). Note in thsi cotnext that a challenge in result-itnerptation conserns the simplity of the data-sets we compare (ie, muliple calls to this fucntion). However, from estalbisehd researhc-appraoches we observe taht the prediction-accuryc of algorithms (adn coneptual data-sets such as ontolgoeis) are evlauated using a data-set chunk/colleciton, hence we assert that it is feaisble/possihble to construct represetntive dat-sets.
 **/
static void computeForAllCmpMetrics__kt_matrix_cmpCluster(s_kt_matrix_setOf_t *self, const uint data_id, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint nrows, const uint ncols, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2) {
  assert(self); assert(self->list_size > 0); 
  assert(self->list[0].matrix);   assert(data_id < self->list[0].nrows);
  //! ---
  assert(mapOf_vertexToCentroid2);   assert(mapOf_vertexToCentroid1);
  assert(nrows > 0);   assert(ncols > 0);
  //!
  //! Irerate through the CCMs:
  for(uint ccm_index = 0; ccm_index < (uint)e_kt_matrix_cmpCluster_metric_undef; ccm_index++) {
    //! vAdliate conssitnecy:
    const e_kt_matrix_cmpCluster_metric_t ccm = (e_kt_matrix_cmpCluster_metric_t)ccm_index;
    const e_kt_matrix_cmpCluster_categoriesOfMetrics_t ccm_category = getEnumCategory__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm);
    assert(self->list[ccm_category].matrix);   assert(data_id < self->list[ccm_category].nrows);
    const uint cnt_cols = getCntIn__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm_category);
    assert(cnt_cols > 0);
    // printf("cnt_cols=%u VS %u, given ccm_category=%u, at %s:%d\n", cnt_cols, self->list[ccm_category].ncols, ccm_category,__FILE__, __LINE__);
    assert(cnt_cols == self->list[ccm_category].ncols); //! ie, our expectiaotn of itnaiton.
    //!
    //! Configure:
    //s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
    uint max_cntClusters = 0; //! ie, idneitfy the max-number-of-clsuters:
    { //! Fidn the max-clsuter-id.
      //! Ntoe: for simplfity we asusme that 'by defualt' all clsuter-dis in range [0 .... n] is used, ie, where "n" is dientified in [below]:
      for(uint i = 0; i < nrows; i++) {
	assert(UINT_MAX != mapOf_vertexToCentroid1[i]);
	max_cntClusters = macro_max(max_cntClusters, mapOf_vertexToCentroid1[i]);}
      for(uint i = 0; i < nrows; i++) {
	assert(UINT_MAX != mapOf_vertexToCentroid2[i]);
	max_cntClusters = macro_max(max_cntClusters, mapOf_vertexToCentroid2[i]);}
      assert(max_cntClusters > 0);
    }
    assert(max_cntClusters != UINT_MAX);
    max_cntClusters += 1;
    assert(max_cntClusters <= nrows); //! assumption: there are more vertices than clsuters, ie, as otehrwise would be odd.
    //! 
    //! Configure and compute:
    s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, nrows, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence_1, matrixOf_coOccurence_2, config_intraCluster);
    s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
    computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, /*metric_clusterCmp=*/ccm, &metric_result); 
    const t_float scalar_score_altTest = metric_result.score_1;
    //assert(local_col_id < obj_metricDataScores.ncols);
    //assert(row_id_global < obj_metricDataScores.nrows);
    assert(ccm_category < self->list_size);
    { //! Update for 'all':
      uint ccm_category = e_kt_matrix_cmpCluster_categoriesOfMetrics_all;
      self->list[ccm_category].matrix[data_id][ccm_index] = scalar_score_altTest;
    }
    //! Update for 'specific':
    { const uint offset = getEnumStartPosOffset__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm_category);
      assert(ccm_index >= offset);
      const uint local_pos = ccm_index - offset;
      assert(local_pos < self->list[ccm_category].ncols);
      self->list[ccm_category].matrix[data_id][local_pos] = scalar_score_altTest;      
    }
    assert(ccm_category != e_kt_matrix_cmpCluster_categoriesOfMetrics_all); //! ie,w aht we expect.
    //! 
    //! De-allocate:
    free__s_kt_matrix_cmpCluster(&obj_cmp);
    //!
    //! Confier to also evlauate for specific simlairty/correlaiton-metrics:
    if(ccm_category != e_kt_matrix_cmpCluster_categoriesOfMetrics_setBased) {
      //!
      //! Then e elvuate wrt. different matrix-based permtautions:
      { //! Subset: Update for the hpLysis-metrics:
	for(uint index = 0; index < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; index++) {
	  const s_kt_correlationMetric_t obj_metric = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)index);
	  if(isTo_use_directScore__e_kt_correlationFunction(obj_metric.metric_id)) {continue;}
	  //! 
	  //! Configure and compute:
	  s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	  build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, nrows, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence_1, matrixOf_coOccurence_2, config_intraCluster);
	  {
	    //! Configure how we are to infer the metrics:
	    s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	    //! state that we are to use a complex comparsin-strategy:
	    config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true;
	    //! Set the tyep of clsuterign-comparison-strategy to use:
	    //config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between;
	    //! Set the type of corrleaiton-metirc to be used:
	    config_intraCluster.metric_complexClusterCmp_obj = obj_metric;
	  }

	  s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
	  computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, /*metric_clusterCmp=*/ccm, &metric_result); 
	  const t_float scalar_score_altTest = metric_result.score_1;
	  //! 
	  //! Update:
	  assert(index < self->list[e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__subset_extremeCases].ncols);
	  self->list[e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__subset_extremeCases].matrix[data_id][index] = scalar_score_altTest;      
	  
	  //! 
	  //! De-allocate:
	  free__s_kt_matrix_cmpCluster(&obj_cmp);	  
	}
      }
      //! ----------------------------------------------------------
      //! 
      { //! All: Update for the hpLysis-metrics:
	uint index = 0;
	for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
	  for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
	    e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;
	    if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	       (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
	       || 
#endif
	       describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
	       || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
	       ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
	    //for(uint index = 0; index < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; index++) {

	    //! 
	    //! Configure and compute:
	    s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	    {
	      //! Configure how we are to infer the metrics:
	      s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, dist_metric_id, dist_metricCorrType);
	      s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	      //! state that we are to use a complex comparsin-strategy:
	      config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true;
	      //! Set the tyep of clsuterign-comparison-strategy to use:
	      //config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between;
	      //! Set the type of corrleaiton-metirc to be used:
	      config_intraCluster.metric_complexClusterCmp_obj = obj_metric;
	    }
	    build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, nrows, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence_1, matrixOf_coOccurence_2, config_intraCluster);
	    s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
	    computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, /*metric_clusterCmp=*/ccm, &metric_result); 
	    const t_float scalar_score_altTest = metric_result.score_1;
	    //! 
	    //! Update:
	    assert(index < self->list[e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__all].ncols);
	    self->list[e_kt_matrix_cmpCluster_categoriesOfMetrics_matrixBased_hpLysisCorrMetrics__all].matrix[data_id][index] = scalar_score_altTest;      
	    
	    //! 
	    //! De-allocate:
	    free__s_kt_matrix_cmpCluster(&obj_cmp);	  
	    //!
	    //!   Increment:
	    index++;
	  }
	}
      }
    }
  }

}

//! Compute CCM-scores for the naive CCM-metric-interpreations (oekseth, 06. feb. 2017)
static void computeForAllCmpMetrics__subsetNaive__kt_matrix_cmpCluster(s_kt_matrix_t *list_0, const uint data_id, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint nrows, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2) {
  assert(list_0);
  //assert(self); assert(self->list_size > 0); 
  assert(list_0->matrix);   assert(data_id < list_0->nrows);
  //! ---
  assert(mapOf_vertexToCentroid2);   assert(mapOf_vertexToCentroid1);
  assert(nrows > 0);   //assert(ncols > 0);
  const e_kt_matrix_cmpCluster_categoriesOfMetrics_t ccm_category = e_kt_matrix_cmpCluster_categoriesOfMetrics_all;
  //const e_kt_matrix_cmpCluster_categoriesOfMetrics_t ccm_category = getEnumCategory__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm);
  assert(list_0->matrix);   assert(data_id < list_0->nrows);
  const uint cnt_cols = getCntIn__e_kt_matrix_cmpCluster_categoriesOfMetrics_t(ccm_category);
  assert(cnt_cols > 0);
  // printf("cnt_cols=%u VS %u, given ccm_category=%u, at %s:%d\n", cnt_cols, self->list[ccm_category].ncols, ccm_category,__FILE__, __LINE__);
  assert(cnt_cols == list_0->ncols); //! ie, our expectiaotn of itnaiton.

  //!
  //! Irerate through the CCMs:
  for(uint ccm_index = 0; ccm_index < (uint)e_kt_matrix_cmpCluster_metric_undef; ccm_index++) {
    //! vAdliate conssitnecy:
    const e_kt_matrix_cmpCluster_metric_t ccm = (e_kt_matrix_cmpCluster_metric_t)ccm_index;

    //!
    //! Configure:
    //s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
    uint max_cntClusters = 0; //! ie, idneitfy the max-number-of-clsuters:
    { //! Fidn the max-clsuter-id.
      //! Ntoe: for simplfity we asusme that 'by defualt' all clsuter-dis in range [0 .... n] is used, ie, where "n" is dientified in [below]:
      for(uint i = 0; i < nrows; i++) {
	assert(UINT_MAX != mapOf_vertexToCentroid1[i]);
	max_cntClusters = macro_max(max_cntClusters, mapOf_vertexToCentroid1[i]);}
      for(uint i = 0; i < nrows; i++) {
	assert(UINT_MAX != mapOf_vertexToCentroid2[i]);
	max_cntClusters = macro_max(max_cntClusters, mapOf_vertexToCentroid2[i]);}
      assert(max_cntClusters > 0);
    }
    assert(max_cntClusters != UINT_MAX);
    max_cntClusters += 1;
    assert(max_cntClusters <= nrows); //! assumption: there are more vertices than clsuters, ie, as otehrwise would be odd.
    //! 
    //! Configure and compute:
    s_kt_matrix_cmpCluster_clusterDistance_config_t config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, nrows, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence_1, matrixOf_coOccurence_2, config_intraCluster);
    s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
    computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, /*metric_clusterCmp=*/ccm, &metric_result); 
    const t_float scalar_score_altTest = metric_result.score_1;
    list_0->matrix[data_id][ccm_index] = scalar_score_altTest;
    //! 
    //! De-allocate:
    free__s_kt_matrix_cmpCluster(&obj_cmp);
  }
}

//! Compute CCM-scores for the naive CCM-metric-interpreations (oekseth, 06. feb. 2017)
//! @remarks 'produce' an aritciifcal data-matrix using mapOf_vertexToCentroid2 as 'basiss'-
static void computeForAllCmpMetrics__subsetNaive__constructArtificalMatrix__kt_matrix_cmpCluster(s_kt_matrix_t *list_0, const uint data_id, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint nrows, t_float **matrixOf_coOccurence_1) {
  assert(nrows);
  t_float empty_0 = 0;
  t_float **matrixOf_coOccurence_2 = allocate_2d_list_float(nrows, nrows, empty_0);
  const t_float worst_score = 100; const t_float best_score = 1;
  //! Update wrt. the clsuters:
  for(uint i = 0; i < nrows; i++) {
    for(uint k = 0; k < nrows; k++) {
      if(mapOf_vertexToCentroid2[i] != mapOf_vertexToCentroid2[k]) {
	matrixOf_coOccurence_2[i][k] = worst_score;
      } else {matrixOf_coOccurence_2[i][k] = best_score;}
    }}
  //!
  //! The call:
  computeForAllCmpMetrics__subsetNaive__kt_matrix_cmpCluster(list_0, data_id, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, nrows, matrixOf_coOccurence_1, matrixOf_coOccurence_2);
  //!
  //! De-allcoate:
  assert(matrixOf_coOccurence_2);
  free_2d_list_float(&matrixOf_coOccurence_2, nrows); matrixOf_coOccurence_2 = NULL;
}

#endif //! EOF
