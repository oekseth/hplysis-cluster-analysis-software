#include "kt_hash_string.h"
/* #include "kt_sim_string.h" //! ie, the simplifed API for hpLysis */
/* #include "kt_sparse_sim.h" //! where the latter is used as an altneritve to the "kt_sim_string", ie, to afcilaitet/supprot comptaution of "Pairwise simalrity-meitrcs (PSMs)" */
#include <time.h>    // time()


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief demosntrate how our "kt_hash_string.h" may be sued to extract/idneityf the uqniue words in a set of setencnes/strings. 
   @author Ole Kristian Ekseth (oekseth, 06. aug. 2017).
**/
int main() 
#else
  void tut_hash_2_wordsFromStrings()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  {
    const char *file_inp = "data/aalberg_bib1_aug_start.subset.tsv"; 
    //    const char *file_inp = "data/aalberg_bib1_aug_start.tsv"; 
    s_kt_list_1d_string_t strMap_1 = initFromFile__s_kt_list_1d_string_t(file_inp);
    //! Compute: 
    s_kt_hash_string_t obj_hash = init__fromHash__s_kt_hash_string_t(&strMap_1, ' ');
    /*   printf("#\t Start-computing, at %s:%d\n", __FILE__, __LINE__); */
    /*   __tut_string_sim_2_stringArray__compute(&strMap_1, "result_tut_string_2_inpFile_sparse", /\*isTo_storeInMatrix=*\/false);     */
    /* } */
    //!
    //! De-allcoate: 
    free__s_kt_list_1d_string(&strMap_1);
    //!
    //! Write out keys:
    if(true) {
      const char *str_resultPrefix = "result_tut_hash_2";
      {
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s_words.tsv", str_resultPrefix);
	if(true) {printf("(info): export to result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__); }
	FILE *fileP_result = NULL;
	fileP_result = fopen(stringOf_resultFile, "w");
	if (fileP_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
	//!
	//! The call:
	dump__s_kt_hash_string_t(&obj_hash, fileP_result);
	//! then close the file-pointer: 
	assert(fileP_result);
	fclose(fileP_result); fileP_result = NULL;
      }
      { //! Write out the words to a sperate ahsh-bucket:
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s_hashBucket.tsv", str_resultPrefix);
	if(true) {printf("(info): export to result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__); }
	FILE *fileP_result = NULL;
	fileP_result = fopen(stringOf_resultFile, "w");
	if (fileP_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", stringOf_resultFile,  __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	}
	//!
	//! The call:
	dump__sortByBucket__s_kt_hash_string_t(&obj_hash, fileP_result);
	//! then close the file-pointer: 
	assert(fileP_result);
	fclose(fileP_result); fileP_result = NULL;
      }
    }
    //!
    //! De-allocate: 
    free__s_kt_hash_string_t(&obj_hash);
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

