  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     const t_float empty_0 = 0;
    t_float *new_list = allocate_1d_list_float(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = T_FLOAT_MAX; //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(self->list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      free_1d_list_float(&(self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
