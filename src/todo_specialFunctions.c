
static void bessel_besselmfirstcheb(t_float c,
     t_float* b0,
     t_float* b1,
     t_float* b2,
     ae_state *_state);
static void bessel_besselmnextcheb(t_float x,
     t_float c,
     t_float* b0,
     t_float* b1,
     t_float* b2,
     ae_state *_state);
static void bessel_besselm1firstcheb(t_float c,
     t_float* b0,
     t_float* b1,
     t_float* b2,
     ae_state *_state);
static void bessel_besselm1nextcheb(t_float x,
     t_float c,
     t_float* b0,
     t_float* b1,
     t_float* b2,
     ae_state *_state);
static void bessel_besselasympt0(t_float x,
     t_float* pzero,
     t_float* qzero,
     ae_state *_state);
static void bessel_besselasympt1(t_float x,
     t_float* pzero,
     t_float* qzero,
     ae_state *_state);




static t_float ibetaf_incompletebetafe(t_float a,
     t_float b,
     t_float x,
     t_float big,
     t_float biginv,
     ae_state *_state);
static t_float ibetaf_incompletebetafe2(t_float a,
     t_float b,
     t_float x,
     t_float big,
     t_float biginv,
     ae_state *_state);
static t_float ibetaf_incompletebetaps(t_float a,
     t_float b,
     t_float x,
     t_float maxgam,
     ae_state *_state);
































static void trigintegrals_chebiterationshichi(t_float x,
     t_float c,
     t_float* b0,
     t_float* b1,
     t_float* b2,
     ae_state *_state);








/*************************************************************************
Error function

The integral is

                          x
                           -
                2         | |          2
  erf(x)  =  --------     |    exp( - t  ) dt.
             sqrt(pi)   | |
                         -
                          0

For 0 <= |x| < 1, erf(x) = x * P4(x**2)/Q5(x**2); otherwise
erf(x) = 1 - erfc(x).


ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE      0,1         30000       3.7e-16     1.0e-16

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1988, 1992, 2000 by Stephen L. Moshier
*************************************************************************/
t_float errorfunction(t_float x, ae_state *_state)
{
    t_float xsq;
    t_float s;
    t_float p;
    t_float q;
    t_float result;


    s = (t_float)(ae_sign(x));
    x = mathLib_float_abs(x);
    if( ae_fp_less(x,0.5) )
    {
        xsq = x*x;
        p = 0.007547728033418631287834;
        p = -0.288805137207594084924010+xsq*p;
        p = 14.3383842191748205576712+xsq*p;
        p = 38.0140318123903008244444+xsq*p;
        p = 3017.82788536507577809226+xsq*p;
        p = 7404.07142710151470082064+xsq*p;
        p = 80437.3630960840172832162+xsq*p;
        q = 0.0;
        q = 1.00000000000000000000000+xsq*q;
        q = 38.0190713951939403753468+xsq*q;
        q = 658.070155459240506326937+xsq*q;
        q = 6379.60017324428279487120+xsq*q;
        q = 34216.5257924628539769006+xsq*q;
        q = 80437.3630960840172826266+xsq*q;
        result = s*1.1283791670955125738961589031*x*p/q;
        return result;
    }
    if( ae_fp_greater_eq(x,(t_float)(10)) )
    {
        result = s;
        return result;
    }
    result = s*(1-errorfunctionc(x));
    return result;
}


/*************************************************************************
Complementary error function

 1 - erf(x) =

                          inf.
                            -
                 2         | |          2
  erfc(x)  =  --------     |    exp( - t  ) dt
              sqrt(pi)   | |
                          -
                           x


For small x, erfc(x) = 1 - erf(x); otherwise rational
approximations are computed.


ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE      0,26.6417   30000       5.7e-14     1.5e-14

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1988, 1992, 2000 by Stephen L. Moshier
*************************************************************************/
t_float errorfunctionc(t_float x, ae_state *_state)
{
    t_float p;
    t_float q;
    t_float result;


    if( ae_fp_less(x,(t_float)(0)) )
    {
        result = 2-errorfunctionc(-x);
        return result;
    }
    if( ae_fp_less(x,0.5) )
    {
        result = 1.0-errorfunction(x);
        return result;
    }
    if( ae_fp_greater_eq(x,(t_float)(10)) )
    {
        result = (t_float)(0);
        return result;
    }
    p = 0.0;
    p = 0.5641877825507397413087057563+x*p;
    p = 9.675807882987265400604202961+x*p;
    p = 77.08161730368428609781633646+x*p;
    p = 368.5196154710010637133875746+x*p;
    p = 1143.262070703886173606073338+x*p;
    p = 2320.439590251635247384768711+x*p;
    p = 2898.0293292167655611275846+x*p;
    p = 1826.3348842295112592168999+x*p;
    q = 1.0;
    q = 17.14980943627607849376131193+x*q;
    q = 137.1255960500622202878443578+x*q;
    q = 661.7361207107653469211984771+x*q;
    q = 2094.384367789539593790281779+x*q;
    q = 4429.612803883682726711528526+x*q;
    q = 6089.5424232724435504633068+x*q;
    q = 4958.82756472114071495438422+x*q;
    q = 1826.3348842295112595576438+x*q;
    result = ae_exp(-mathLib_float_sqrt(x))*p/q;
    return result;
}


/*************************************************************************
Normal distribution function

Returns the area under the Gaussian probability density
function, integrated from minus infinity to x:

                           x
                            -
                  1        | |          2
   ndtr(x)  = ---------    |    exp( - t /2 ) dt
              sqrt(2pi)  | |
                          -
                         -inf.

            =  ( 1 + erf(z) ) / 2
            =  erfc(z) / 2

where z = x/sqrt(2). Computation is via the functions
erf and erfc.


ACCURACY:

                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE     -13,0        30000       3.4e-14     6.7e-15

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1988, 1992, 2000 by Stephen L. Moshier
*************************************************************************/
t_float normaldistribution(t_float x, ae_state *_state)
{
    t_float result;


    result = 0.5*(errorfunction(x/1.41421356237309504880)+1);
    return result;
}


/*************************************************************************
Inverse of the error function

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1988, 1992, 2000 by Stephen L. Moshier
*************************************************************************/
t_float inverf(t_float e, ae_state *_state)
{
    t_float result;


    result = invnormaldistribution(0.5*(e+1))/mathLib_float_sqrtt((t_float)(2));
    return result;
}

