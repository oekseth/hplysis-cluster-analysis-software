    //! 
    if(superset->weight != NULL) { //! then copy the weights:
      for(uint local_id = 0; local_id < superset->ncols; local_id++) {      
	const uint global_vertex = local_id;  
	t_float score = 0;
	//! Udpate the 'new' object with the 'knowledge' from the superset-object.
	get_weight__s_kt_matrix(superset, global_vertex, &score);
	set_weight__s_kt_matrix(&self, local_id, score);
      }    
    }
    if(isTo_updateNames) {
      if(superset->nameOf_rows != NULL) {
	bool addFor_column = false;
	for(uint local_id = 0; local_id < superset->nrows; local_id++) {      
	  const uint global_vertex = local_id;
	  char *stringTo_add = NULL;
	  //printf("----\n (get-string), at %s:%d\n", __FILE__, __LINE__);
	  get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
	  assert(global_vertex < superset->nrows);
	  assert(stringTo_add == superset->nameOf_rows[global_vertex]);
	  if(stringTo_add && strlen(stringTo_add)) {
	    set_string__s_kt_matrix(&self, index_offset + local_id, stringTo_add, addFor_column);
	  }
	}
      }
      //! ---------
      if(superset->nameOf_columns != NULL) {
	bool addFor_column = true;
	for(uint local_id = 0; local_id < superset->ncols; local_id++) {      
	  const uint global_vertex = local_id;
	  char *stringTo_add = NULL;
	  get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
	  assert(global_vertex < superset->ncols);
	  assert(stringTo_add == superset->nameOf_columns[global_vertex]);
	  if(stringTo_add && strlen(stringTo_add)) {
	    if( (superset_prefixInput_row == NULL) || !strlen(superset_prefixInput_row) ) {
	      set_string__s_kt_matrix(&self, index_offset+local_id, stringTo_add, addFor_column);
	    } else { //! then we first ened allcoating a string, construcitng a superset-string, and then isernitgn:
	      const char empty_0 = '\0';
	      char *str = KT__merge_1d_list_char(stringTo_add, strlen(stringTo_add), superset_prefixInput_row, strlen(superset_prefixInput_row));
	      assert(str);
	      //! Insert:
	      set_string__s_kt_matrix(&self, index_offset+local_id, str, addFor_column);	      
	      //! De-allcote:
	      free_1d_list_char(&str);
	    }
	  }
	}
      }
    }
    
    //! -----------------------
    //!
    //! Comptue the rank and thereafter insert
    for(uint row_id = 0; row_id < superset->nrows; row_id++) {
      const uint new_index = index_offset + row_id;
      assert(new_index < self.nrows);
#if(__localConfig__useRanks == 1)
      //! Get the ranks:
      t_float *list_rank =get_rank__correlation_rank(self->ncols, superset->matrix[row_id]); 
      assert(list_rank);
      //! Insert the ranks:
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	self->matrix[new_index][col_id] = list_rank[col_id];
      }
      //! De-allcoat ethe locally reserved memory:
      free_1d_list_float(&list_rank); list_rank = NULL; //! ie, de-allcoate.
#else //! Then we apply a 'simple copy-operation':
      memcpy(self.matrix[new_index], superset->matrix[row_id], sizeof(t_float)*self.ncols);
#endif
    }
