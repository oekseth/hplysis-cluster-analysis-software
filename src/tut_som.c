 #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"

/**
   @brief examplifies how to use "Self Orgnaising Map" (SOM) clustering to comptue results.
   @author Jan Christain Meyer (data) and Ole Kristian Ekseth (logics) (oekseth, janchris, 06. jan. 2017).
   @remarks we examplfiy: (a) how to comptue k-emans clsuters; (b) how to use the internal export-rotuiens for data-export; (c) how to programtailally access the cluster-resutls. 
   @remarks Extends "tut_mine.c" and "tut_buildSimilarityMatrix.c". Demonstrates features wrt.:
   -- input(a): use a differnet programmitc appraoch to specify the input, eg, in contrast to "tut_buildSimilarityMatrix.c"
   -- input(b): examplify the use to the "isTo_transposeMatrix" output, which may be utalized to investigate the 'inverse relationshisp in an adjcency-matrix' (and simlairly relationships between features in a vertex--feature matrix).
   -- computation: use SOM, while Not applying a pre-comptuation of the input-matrix
   -- result: a permtaution of "tut_kCluster.c" where:
   (a) hpLysis_api: we write out the mapping between each vertex and teh cluster-centroid.
   (b) direct-access(result-object): examplifies how to programaitally access the cluster-results: instead of the "s_kt_clusterAlg_fixed_resultObject_t obj_result_kMean" we use the "s_kt_clusterAlg_SOM_resultObject_t obj_result_SOM";
   (c) result(interpreation): the result never converges, which is mainly due to the small sample-size used as input
**/
int main() 
#endif
{
  const bool inputMatrix__isAnAdjecencyMatrix = true; const bool isTo_transposeMatrix = false;
  const uint cnt_Calls_max = 3; const uint arg_npass = 1000;
  const char *result_file = "test_tut_som.tsv";
  const uint nrows = 5;     const uint ncols = 5; 
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);  
  init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  const uint nclusters = 2;

  // Throwaway alias to use as shorthand notation for the input matrix                                                                                       
  t_float **m = obj_matrixInput.matrix;

  // Set distances between 5 nodes (adj.matrix)                                                                                                              
  // Every node is at distance 0 from itself                                                                                                                 
  // {0,1} are at distance 1 from each other                                                                                                                 
  // {2,3,4} are at distance 1 from each other                                                                                                               
  // {0,1} are at distance 10 from {2,3,4}                                                                                                                   
  m[0][0] = 0;  m[0][1] = 1;  m[0][2] = 10; m[0][3] = 10; m[0][4] = 10;
  m[1][0] = 1;  m[1][1] = 0;  m[1][2] = 10; m[1][3] = 10; m[1][4] = 10;
  m[2][0] = 10; m[2][1] = 10; m[2][2] = 0;  m[2][3] = 1;  m[2][4] = 1;
  m[3][0] = 10; m[3][1] = 10; m[3][2] = 1;  m[3][3] = 0;  m[3][4] = 1;
  m[4][0] = 10; m[4][1] = 10; m[4][2] = 1;  m[4][3] = 1;  m[4][4] = 0;

  /* Have a go at the clustering */

  //! Open teh file :
  FILE *file_out = fopen(result_file, "wb");
  //FILE *file_out = stdout;
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}

  //!
  //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:
  for(uint cnt_Calls = 0; cnt_Calls < cnt_Calls_max; cnt_Calls++) {
    s_hpLysis_api_t hp_cluster_config = setToEmpty__s_hpLysis_api_t(NULL, NULL);

    // (Symmetric) adjacency matrix                                                                                                                            
    hp_cluster_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
    hp_cluster_config.config.clusterConfig.isTo_transposeMatrix = isTo_transposeMatrix;
    hp_cluster_config.config.corrMetric_prior_use = false; //! ie, use data-as-is.

    /* Expect 2 clear clusters */
     printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
    const bool is_also_ok = cluster__hpLysis_api (
						  &hp_cluster_config, clusterAlg, &obj_matrixInput,
						  /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						  );
    assert(is_also_ok);

    //! 
    //! Export results to "file_out":
    assert(file_out);
    const s_kt_correlationMetric_t corrMetric_prior = hp_cluster_config.config.corrMetric_prior;
    fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    /* bool is_ok_e = export__hpLysis_api(&hp_cluster_config, /\*stringOf_file=*\/NULL, /\*file_out=*\/file_out, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV); */
    /* assert(is_ok_e); */
    //! Export the vertex-clsuterId-memberships:
    bool is_ok_e = export__hpLysis_api(&hp_cluster_config, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &obj_matrixInput);
    assert(is_ok_e);

    //! 
    //! 
    { //! Export: traverse teh generated result-matrix-object and write out the results:    
      /* const uint **vertex_clusterId = hp_cluster_config.obj_result_SOM.vertex_clusterid; */
      /* assert(vertex_clusterId); */
      const uint cnt_vertex = (isTo_transposeMatrix == false) ? nrows : ncols;
      //const uint cnt_vertex = hp_cluster_config.obj_result_SOM.cnt_vertex;
      assert(cnt_vertex > 0);
      fprintf(file_out, "SOM-clusterMemberships:\n");
      // uint max_cnt = 0;
      for(uint i = 0; i < cnt_vertex; i++) {
	uint grid_x = UINT_MAX; 	uint grid_y = UINT_MAX; uint centroid_id = UINT_MAX; t_float centroid_distance = T_FLOAT_MAX;
	//! Then call the funciton found in our "kt_clusterAlg_SOM_resultObject.h": 
	const bool is_ok = get_vertex_clusterPair__s_kt_clusterAlg_SOM_resultObject_t(&(hp_cluster_config.obj_result_SOM), /*vertex_id=*/i, &grid_x, &grid_y, &centroid_id, &centroid_distance);
	assert(is_ok); //! ie, as we expec thte vlaeus to be 'inide the threshold':
	fprintf(file_out, "%u->centroid(%u), w/distance=%f and in-grid[%u][%u]\n", i, centroid_id, centroid_distance, grid_x, grid_y);
	//max_cnt = macro_max(vertex_clusterId[i], max_cnt);
      }
      fprintf(file_out, "#! at %s:%d\n", __FILE__, __LINE__);
      //fprintf(file_out, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);
    }
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_cluster_config);	    
  }


  //!
  //! Close the file and the amtrix:
  assert(file_out); 
  if(file_out != stdout) {fclose(file_out); file_out = NULL;}
  free__s_kt_matrix(&obj_matrixInput);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
