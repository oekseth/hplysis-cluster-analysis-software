#ifndef measure_codeStyle_functionRef_api_h
#define measure_codeStyle_functionRef_api_h
 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file measure_codeStyle_functionRef_api
   @brief an access-point to test sytnetically our "measure_codeStyle_functionRef.h"
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
#include "types.h"
#include "def_intri.h"
#include "measure_codeStyle_functionRef.h"

//! The main-function to test our "measure_codeStyle_functionRef_api"
void main__measure_codeStyle_functionRef(int array_cnt, char **array);

#endif //! EOF
