#include "kt_resultS_ccmTime.h"

//! De-allcoet ethe object.
void imgExport_ppm_finalize ( imgExport_ppm_t *ppm ) {
    free ( ppm->data );
    free ( ppm );
}


//! Fucntion to export a given image
imgExport_ppm_t *init__imgExport_ppm_t (    float **matrix, const size_t nrows, const size_t ncols,     char **mapOf_names_rows, char **mapOf_names_cols )
{
    size_t y, x;
    imgExport_ppm_t *img = (imgExport_ppm_t*)malloc ( sizeof(imgExport_ppm_t) );
    img->nrows = nrows, img->ncols = ncols;

    // Allocate space for Rows x Columns x 3 Colour channels
    img->data = (uint8_t*)malloc ( sizeof(uint8_t) * nrows * ncols * 3 );

    // Set all 3 color channels to same value, create image in greyscale:
    // assumes row-major ordering of input matrix
    for ( y=0; y<nrows; y++ )
    {
        for ( x=0; x<ncols; x++ )
        {
            img->data[3*(y*ncols+x)+0] =
            img->data[3*(y*ncols+x)+1] =
            img->data[3*(y*ncols+x)+2] = (matrix[y][x] * 255.0);
        }
    }
    return img;
}

//! The call to wrtie otu the conctnets of a given file.
int imgExport_ppm_write ( const char *filename, imgExport_ppm_t *ppm ) {
    FILE *output = fopen ( filename, "w" );
    if ( output == NULL )
        return ENOENT;
    fprintf ( output, "P6 %zu %zu 255\n", ppm->ncols, ppm->nrows );
    fwrite ( ppm->data, sizeof(uint8_t), ppm->ncols*ppm->nrows*3, output );
    fclose ( output );
    //! @return:
    return 1;
}


//! Export a given matrix to a *ppm file
void kt_resultS_ccmTime__static__exportTo_ppm(s_kt_matrix_t mat_result, const char *file_pref) {
  //! Copy:
  s_kt_matrix_t mat_cpy = initAndReturn__copy__s_kt_matrix(&mat_result, true);
  //! Adjust(1): get max.
  t_float score_max = T_FLOAT_MIN_ABS;
  for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
      const t_float score = mat_cpy.matrix[row_id][col_id];
      if(isOf_interest(score)) {
	score_max = macro_max(score_max, score);
      }
    }
  }
  if( (score_max != T_FLOAT_MIN_ABS) && (score_max != 0) ) {
    //! Adjust(1):
    t_float score_max_inv = 1/score_max;
    for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
	const t_float score = mat_cpy.matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  mat_cpy.matrix[row_id][col_id] *= score_max_inv;
	}
      }
    }
    //! Export:
    char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_img.ppm", file_pref);
    //	    const char *file_name_result_img = "tmp_img.ppm";
    //     fprintf(stderr, "(info:export)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
    imgExport_ppm_t *picture = init__imgExport_ppm_t( mat_cpy.matrix, mat_cpy.nrows, mat_cpy.ncols, mat_cpy.nameOf_rows, mat_cpy.nameOf_columns);
    imgExport_ppm_write ( file_name_result, picture );
    imgExport_ppm_finalize ( picture );
    /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL); */
    /* assert(is_ok); */
    //! De-allocate:
  }
  free__s_kt_matrix(&mat_cpy);
}

//! @return an itnaliesd veirosn of the "s_kt_resultS_ccmTime_t" object.
s_kt_resultS_ccmTime_t init__s_kt_resultS_ccmTime_t() {
  s_kt_resultS_ccmTime_t self;
  self.mat_result_ccm  = setToEmptyAndReturn__s_kt_matrix_t();
  self.mat_result_time = setToEmptyAndReturn__s_kt_matrix_t();
  return self;
}

//! Export the result-set to both feature-amtrixes and correlation-sets, an appraoch used to simplify correlation anlaysis of the result set:
void free_andExport__s_kt_resultS_ccmTime_t(s_kt_resultS_ccmTime_t *self, const char *str_prefix) {
  assert(self);
  //! LAtter asusmptison are 'appleid' to simplify our export-lgocis:
  assert(str_prefix); assert(strlen(str_prefix));
  assert(strlen(str_prefix) < 1000);
  //! TODO[new-aritcle]: write a new aritlce where we examplify differnet compiel-time-erors which are chanenligng to itnerptedf/understand ... eg, wrt. the rror observed for: #define __MiF__export(mat, str_local) ({ char str_local[2000];  memset(str_local, '\0', 2000);
#define __MiF__export(mat, tag) ({ char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s.tsv", str_prefix, tag);  kt_resultS_ccmTime__static__exportTo_ppm((mat),  file_name); const bool is_ok = export__singleCall__s_kt_matrix_t(&(mat), file_name, NULL); assert(is_ok); })
#define __MiF__correlateAndExport(mat, tag, sim_metric) ({ \
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t(); \
    obj_metric.metric_id = sim_metric; \
    s_kt_matrix_base_t obj_result =initAndReturn__empty__s_kt_matrix_base_t(); \
    s_kt_matrix_base_t mat_input = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&(mat)); \
    assert(sim_metric != e_kt_correlationFunction_undef); \
    const bool is_ok = apply__hp_distance(obj_metric, &mat_input, NULL, &obj_result, init__s_hp_distance__config_t()); \
    assert(is_ok);    \
    /*! Export: */ \
    s_kt_matrix_t mat_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&obj_result); \
    mat_shallow.nameOf_rows = mat.nameOf_rows;  mat_shallow.nameOf_columns = mat.nameOf_rows; /*which is used to support out-writing of the names*/ \
    __MiF__export(mat_shallow, tag); \
    mat_shallow.nameOf_rows = NULL;  mat_shallow.nameOf_columns = NULL; /*which is used to support out-writing of the names*/ \
    /*! De-allocate: */ \
    free__s_kt_matrix_base_t(&obj_result);     })
  //! 
  //! 
  { //! Feature-matrix, for CCM-scores and Time[s]:  
    __MiF__export((self->mat_result_ccm), "ccm");
    __MiF__export((self->mat_result_time), "time");
  }
  { //! Kendall's Tau and MINE, used to depeict/inveistagte simalrity between different CCMs: 
    __MiF__correlateAndExport((self->mat_result_ccm), "sim_ccm_kendall", e_kt_correlationFunction_groupOf_rank_kendall_Dice);
    __MiF__correlateAndExport((self->mat_result_time), "sim_time_kendall", e_kt_correlationFunction_groupOf_rank_kendall_Dice);
    // ------
    __MiF__correlateAndExport((self->mat_result_ccm), "sim_ccm_mine", e_kt_correlationFunction_groupOf_MINE_mic);
    __MiF__correlateAndExport((self->mat_result_time), "sim_time_mine", e_kt_correlationFunction_groupOf_MINE_mic);
    // ---- 
    //!
    //! Comptue transpsoed, and then comptue:
    {
      s_kt_matrix_t mat_result_ccm_transp = initAndReturn_transpos__s_kt_matrix(&(self->mat_result_ccm));
      s_kt_matrix_t mat_result_time_transp = initAndReturn_transpos__s_kt_matrix(&(self->mat_result_time));
      //! 
      __MiF__correlateAndExport((mat_result_ccm_transp), "sim_ccm_kendall_transp", e_kt_correlationFunction_groupOf_rank_kendall_Dice);
      __MiF__correlateAndExport((mat_result_time_transp), "sim_time_kendall_transp", e_kt_correlationFunction_groupOf_rank_kendall_Dice);
      // ------
      __MiF__correlateAndExport((mat_result_ccm_transp), "sim_ccm_mine_transp", e_kt_correlationFunction_groupOf_MINE_mic);
      __MiF__correlateAndExport((mat_result_time_transp), "sim_time_mine_transp", e_kt_correlationFunction_groupOf_MINE_mic);
      //!
      //! De-allocate:
      free__s_kt_matrix(&mat_result_ccm_transp);
      free__s_kt_matrix(&mat_result_time_transp);
    }
    //    __MiF__correlateAndExport((self->mat_result_ccm), "ccm", );

  }
  //! 
  //! De-allocate: 
  free__s_kt_matrix(&(self->mat_result_ccm));
  free__s_kt_matrix(&(self->mat_result_time));
#undef __MiF__export
#undef __MiF__correlateAndExport
}


