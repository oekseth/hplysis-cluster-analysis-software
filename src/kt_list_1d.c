#include "kt_list_1d.h"
#include "kt_matrix_fileReadTuning.h"
#include "kt_list_1d_string.h"

//! ----------------------- start: HEAP-logics:
//! Note: [below] is sinpred by: "http://robin-thomas.github.io/max-heap/"
#define MiF__LCHILD(x) 2 * x + 1
#define MiF__RCHILD(x) 2 * x + 2
#define MiF__PARENT(x) (uint)((x - 1) * 0.5)

//! Insert the item into a pre-allcoated data-strucutre.
// Note: psotion the "newItem" node at the right in the max-heap.
// FIXME[eval]: compare/meausre difference between [â€elow] implmentations:
#define MiF__heap__insertNode(currentPos, list, list_size, newItem) ({ uint i = currentPos; while(i && __MiF__objLessThan(list[MiF__PARENT(i)], newItem) ) { list[i] = list[MiF__PARENT(i)]; i = MiF__PARENT(i);} assert(i < list_size); list[i] = newItem;})
//#define MiF__heap__insertNode(currentPos, list, list_size, newItem) ({ uint i = currentPos; while(i && (newItem > list[MiF__PARENT(i)]) ) { list[i] = list[MiF__PARENT(i)]; i = MiF__PARENT(i);} assert(i < list_size); list[i] = newItem;})
//! Note: [â€elow] is taken from "/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/external_cpy/valgrind-3.12.0/exp-sgcheck/tests/hackedbz2.c"
// FIXME[conceptual]: validte correctness of [below].
#define MiF__heap__insertNode__alt(currentPos, heap, list_size, newItem) ({ \
  loint zz;       \
  zz = currentPos;                             \
  while (newItem < heap[zz >> 1]) {      \
  heap[zz] = heap[zz >> 1];                       \
  zz >>= 1;                                       \
  }                                                  \
  heap[zz] = newItem;})
//! Apply a generic heapify-routine:
#define MiF__heapify(type_t, list, list_size, heap_index) ({ \
  loint cnt_iter = 0; uint largest = UINT_MAX;\
  while( /*(largest != heap_index) && */ (heap_index < (list_size+1) ) ) { \
  /*largest = (MiF__LCHILD(heap_index) < list_size && list[MiF__LCHILD(heap_index)] > list[heap_index]) ? MiF__LCHILD(heap_index) : heap_index ;*/ \
  largest = ( (MiF__LCHILD(heap_index) < list_size) && __MiF__objGreaterThan(list[MiF__LCHILD(heap_index)] , list[heap_index]) ) ? MiF__LCHILD(heap_index) : heap_index ; \
  /*if( (MiF__RCHILD(heap_index) < list_size) && (list[MiF__RCHILD(heap_index)] > list[largest]) ) { largest = MiF__RCHILD(heap_index); } */ \
  if( (MiF__RCHILD(heap_index) < list_size) && __MiF__objGreaterThan(list[MiF__RCHILD(heap_index)] , list[largest]) ) { largest = MiF__RCHILD(heap_index); } \
  if(largest != heap_index) { MF__SWAP_complexType(type_t, (list[heap_index]), (list[largest])) ; heap_index = largest; \
  } else {heap_index = (list_size+1);} /*! ie, then stop the tree-traversal-routine. */ \
  assert(cnt_iter++ < (list_size*list_size)); }})
//! Esnure that the heap-list-strucutre is satsified for all verties.
#define MiF__heapify__all(type_t, list, list_size) ({ for(int i = (list_size - 1) / 2; i >= 0; i--) {int tmp_index = i; MiF__heapify(type_t, list, list_size, tmp_index);}})
/* void deleteNode(maxHeap *hp) { */
/*   if(hp->size) { */
/*     printf("Deleting node %d\n\n", hp->elem[0].data) ; */
/*     hp->elem[0] = hp->elem[--(hp->size)] ; */
/*     hp->elem = realloc(hp->elem, hp->size * sizeof(node)) ; */
/*     heapify(hp, 0) ; */
/*   } else { */
/*     printf("\nMax Heap is empty!\n") ; */
/*     free(hp->elem) ; */
/*   } */
/* } */
/* void buildMaxHeap(maxHeap *hp, int *arr, int size) { */
/*   /\* int i ; *\/ */

/*   /\* // Insertion into the heap without violating the shape property *\/ */
/*   /\* for(i = 0; i < size; i++) { *\/ */
/*   /\*   if(hp->size) { *\/ */
/*   /\*     hp->elem = realloc(hp->elem, (hp->size + 1) * sizeof(node)) ; *\/ */
/*   /\*   } else { *\/ */
/*   /\*     hp->elem = malloc(sizeof(node)) ; *\/ */
/*   /\*   } *\/ */
/*   /\*   node nd ; *\/ */
/*   /\*   nd.data = arr[i] ; *\/ */
/*   /\*   hp->elem[(hp->size)++] = nd ; *\/ */
/*   /\* } *\/ */

/*   // Making sure that heap property is also satisfied */
/*   MiF__heapify__all(t_float, (self->list), (self->list_size)); */
/*   /\* for(i = (hp->size - 1) / 2; i >= 0; i--) { *\/ */
/*   /\*   heapify(hp, i) ; *\/ */
/*   /\* } *\/ */
/* } */
//! ----------------------- end: HEAP-logics:


//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_float_t init__s_kt_list_1d_float_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_float_t self; 
  self.list = NULL;   self.list_size = 0;   self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    const t_float empty_0 = 0;
    self.list = allocate_1d_list_float(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = T_FLOAT_MAX; //! ie, 'set to empty'.
    }
  }
  //! @return itnated object:
  return self;
}
s_kt_list_1d_float_t setToEmpty__s_kt_list_1d_float_t() {
  return init__s_kt_list_1d_float_t(0);
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
bool get_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = T_FLOAT_MAX;
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(*scalar_result != T_FLOAT_MAX) {return true;}
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
void set_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set) {
  assert(self);
#include "kt_list_1d__stub__alloc_float.c"
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}
//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set) {
  t_float val_to_set = valueTo_set; assert(valueTo_set != T_FLOAT_MAX);
  if(index < self->list_size) {
    const t_float old_value = self->list[index];
    if(old_value != T_FLOAT_MAX) {val_to_set += old_value;}
  }
  //! Set the value:
  set_scalar__s_kt_list_1d_float_t(self, index, val_to_set);
}
//! De-allcoates the s_kt_list_1d_float_t object.
void free__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self) {
  assert(self);
  if(self->list) {
    free_1d_list_float(&(self->list)); 
    self->list = NULL; self->list_size = 0;
  }
   self->current_pos = 0;
} 
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, const t_float valueTo_set) {
  assert(valueTo_set != T_FLOAT_MAX);
  const uint index = self->current_pos;
  assert(index != UINT_MAX);
  if(self->current_pos >= self->list_size) {
#include "kt_list_1d__stub__alloc_float.c"
  }
  //! 
  //! Insert the node:
#define __MiF__objLessThan(obj1, obj2) obj1 < obj2
  MiF__heap__insertNode(index, (self->list), (self->list_size), valueTo_set);
#undef __MiF__objLessThan
  //! 
  //! Incrmeent current-pos:
  self->current_pos++;
}
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self) {
  /* if(false) { */
  /*   t_float *list = self->list; */
  /*   int heap_index = 0; */
  /*   int largest = 1; */
  /*   MF__SWAP_complexType(t_float, (list[heap_index]), (list[largest])) ; heap_index = largest; */
  /* } */
  { //! Set the current-pos-key:
    self->current_pos = 0;
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i] != T_FLOAT_MAX) {
	self->current_pos = i;
      }
    }
    (self->current_pos)++;
  }
  // Making sure that heap property is also satisfied
#define __MiF__objGreaterThan(obj1, obj2) obj1 > obj2 
  MiF__heapify__all(t_float, (self->list), (self->list_size));
#undef __MiF__objGreaterThan
}
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self) {
  if(self->current_pos > 0) {
    assert(self->current_pos != UINT_MAX); //! ie, as we expect latter to ahve been set.
    assert(self->current_pos > 0);
    /*     printf("Deleting node %d\n\n", hp->elem[0].data) ; */
    self->list[0] = self->list[self->current_pos-1];
    self->current_pos--; //! ie, remove the element
    /*     hp->elem[0] = hp->elem[--(hp->size)] ; */
    /*     hp->elem = realloc(hp->elem, hp->size * sizeof(node)) ; */
    /*     heapify(hp, 0) ; */
    //! Star thte heap-operaitons:
    int tmp_index = 0;
#define __MiF__objGreaterThan(obj1, obj2) obj1 > obj2 
    MiF__heapify(t_float, (self->list), (self->current_pos), tmp_index);
#undef __MiF__objGreaterThan
  }
  /*   if(hp->size) { */

  /*   } else { */
  /*     printf("\nMax Heap is empty!\n") ; */
  /*     free(hp->elem) ; */
  /*   } */
}

//! *******************************************************************
//! *******************************************************************
//! *******************************************************************

//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_uint_t init__s_kt_list_1d_uint_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_uint_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    const uint empty_0 = 0;
    self.list = allocate_1d_list_uint(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = UINT_MAX; //! ie, 'set to empty'.
    }
  }
  //! @return itnated object:
  return self;
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
bool get_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = UINT_MAX;
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(*scalar_result != UINT_MAX) {return true;}
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
void set_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     const uint empty_0 = 0;
    uint *new_list = allocate_1d_list_uint(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = UINT_MAX; //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      free_1d_list_uint(&(self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}

//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set) {
  uint val_to_set = valueTo_set; assert(valueTo_set != UINT_MAX);
  if(index < self->list_size) {
    const uint old_value = self->list[index];
    if(old_value != UINT_MAX) {val_to_set += old_value;}
  }
  //! Set the value:
  set_scalar__s_kt_list_1d_uint_t(self, index, val_to_set);
}
//! De-allcoates the s_kt_list_1d_uint_t object.
void free__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self) {
  assert(self);
  if(self->list) {
    free_1d_list_uint(&(self->list)); 
    self->list = NULL; self->list_size = 0;
  }
  self->current_pos = 0;
}

//! *******************************************************************
//! *******************************************************************
//! *******************************************************************


//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_pairFloat_t init__s_kt_list_1d_pairFloat_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_pairFloat_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_pairFloat(list_size);    
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_pairFloat();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_pairFloat_t object
s_kt_list_1d_pairFloat_t setToEmpty__s_kt_list_1d_pairFloat_t() {
  return init__s_kt_list_1d_pairFloat_t(0);
}

//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_pairFloat(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_pairFloat_t obj = self->list[i];
      fprintf(f_out, "%u\t%u\t%f\n", obj.head, obj.tail, obj.score);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}

//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_pairFloat_t initFrom_sparseMatrix__s_kt_list_1d_pairFloat_t(const uint nrows, const uint ncols,  t_float **matrix) {
  if(nrows && (ncols == 3) && matrix) {
    s_kt_list_1d_pairFloat_t self = init__s_kt_list_1d_pairFloat_t(/*size=*/nrows); //! ie, the number of pairs.
    //!
    printf("reads w/nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
    //! Insert pairs:    
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_pairFloat(matrix[i], ncols);
#ifndef NDEBUG
      const uint head_id = self.list[i].head;
      /* if(matrix[i][0] >= (1000*1000)) { */
      /* printf("[%u] w/head=%f, tail=%f at %s:%d\n", i, matrix[i][0],matrix[i][1], __FILE__, __LINE__); */
      /* } */
      assert(matrix[i][0] < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
      assert(head_id < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
#endif
      if(MF__isEmpty__s_ktType_pairFloat(self.list[i]) == false) { //! then the valeu is set.
	self.current_pos = (i + 1); //! ie, the last index-pos.
      }
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_pairFloat_t();
  }
}
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_pairFloat_t initFromFile__s_kt_list_1d_pairFloat_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_pairFloat_t self = initFrom_sparseMatrix__s_kt_list_1d_pairFloat_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}

//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, s_ktType_pairFloat_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_pairFloat();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_pairFloat((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, s_ktType_pairFloat_t valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_pairFloat_t *new_list = MF__allocate__s_ktType_pairFloat(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_pairFloat(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      MF__free__s_ktType_pairFloat((self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}
//! De-allcoates the s_kt_list_1d_pairFloat_t object.
void free__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_pairFloat((self->list)); 
    self->list = NULL; self->list_size = 0;
    self->current_pos = 0;
  }
  self->current_pos = 0;
}
//! *******************************************************************
//! *******************************************************************
//! *******************************************************************
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_tripleUChar_t init__s_kt_list_1d_tripleUChar_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_tripleUChar_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_tripleUChar(list_size);    
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_tripleUChar();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_tripleUChar_t object
s_kt_list_1d_tripleUChar_t setToEmpty__s_kt_list_1d_tripleUChar_t() {
  return init__s_kt_list_1d_tripleUChar_t(0);
}

//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_tripleUChar(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_tripleUChar_t obj = self->list[i];
      fprintf(f_out, "%u\t%u\t%u\n", obj.head, obj.tail, obj.score);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}

//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleUChar_t initFrom_sparseMatrix__s_kt_list_1d_tripleUChar_t(const uint nrows, const uint ncols,  t_float **matrix) {
  if(nrows && (ncols == 3) && matrix) {
    s_kt_list_1d_tripleUChar_t self = init__s_kt_list_1d_tripleUChar_t(/*size=*/nrows); //! ie, the number of pairs.
    //!
    printf("reads w/nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
    //! Insert pairs:    
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_tripleUChar(matrix[i], ncols);
#ifndef NDEBUG
      const uint head_id = self.list[i].head;
      /* if(matrix[i][0] >= (1000*1000)) { */
      /* printf("[%u] w/head=%f, tail=%f at %s:%d\n", i, matrix[i][0],matrix[i][1], __FILE__, __LINE__); */
      /* } */
      assert(matrix[i][0] < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
      assert(head_id < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
#endif
      if(MF__isEmpty__s_ktType_tripleUChar(self.list[i]) == false) { //! then the valeu is set.
	self.current_pos = (i + 1); //! ie, the last index-pos.
      }
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_tripleUChar_t();
  }
}
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleUChar_t initFromFile__s_kt_list_1d_tripleUChar_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_tripleUChar_t self = initFrom_sparseMatrix__s_kt_list_1d_tripleUChar_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}


//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleUChar_t initFromFile__sizeKnown_s_kt_list_1d_tripleUChar_t(const char *input_file, const uint cnt_rows) {
  assert(input_file); assert(strlen(input_file));
  assert(cnt_rows > 0);
  s_kt_list_1d_tripleUChar_t self = init__s_kt_list_1d_tripleUChar_t(cnt_rows);
  //!
  //! Step(): Load data from file:
  s_kt_list_1d_string_t obj_string = initFromFile_sizeKnown_s_kt_list_1d_string_t(input_file, cnt_rows);
  assert(obj_string.nrows > 0);
  assert(obj_string.nrows >= self.list_size); //! ie, as we expec tthe user-rpoviuded input-argumetns are correct.


  //!
  //! Step(): Convert into values, and then insert into our list:
  uint cnt_strings = 0;
  get_count__s_kt_list_1d_string_t(&obj_string, &cnt_strings);
  for(uint i = 0; i < cnt_strings; i++) {
  //  for(uint i = 0; i < obj_string.nrows; i++) {
    const char *str = getAndReturn_string__s_kt_list_1d_string(&obj_string, i);
    assert(str);
    assert(strlen(str));
    t_float values[3] = {T_FLOAT_MAX, T_FLOAT_MAX, T_FLOAT_MAX};
    uint values_pos = 0;
    char buff[100]; uint buff_next = 0;
    for(uint k = 0; k < 100; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
    const uint str_size = (uint)strlen(str);
    //#if false    
    //if(false) {  uint s = 0; // FIXME: remove.
    for(uint s = 0; s < str_size; s++) {
      if(str[s] != '\0') {
	if( (str[s] != '\t') && (str[s] != ' ') ) {
	  buff[buff_next] = str[s];
	  buff_next++;
	  assert(buff_next < 100); //! whre '100' is assuemd being the maximum number of chars.
	} else { //! then convert to a char:
	  if(buff_next > 0) { //! Then insert data:
	    assert(values_pos < 3); //! where '3' is based on the above hard-code specifciation.
	    values[values_pos] = (t_float)atof(buff);
	    // printf("(kt-list-1d:inside)\t [%u]\t newVal=%s=%f\n", i, buff, values[values_pos]);
	    values_pos++;
	    //! Reset the coutner:
	    for(uint k = 0; k < buff_next; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
	    buff_next = 0;
	  }
	}
      } else {
	s = str_size; //! ie, then stop iteration.
      }
    }
    if(buff_next > 0) { //! Then insert data:	  
      assert(values_pos < 3); //! where '3' is based on the above hard-code specifciation.
      values[values_pos] = (t_float)atof(buff);
      // printf("(kt-list-1d:afterEach)\t [%u]\t newVal=%s=%f\n", i, buff, values[values_pos]);
      values_pos++;
      //! Reset the coutner:
      for(uint k = 0; k < buff_next; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
      buff_next = 0;
    }	      
    if(true) { 
      //! Step(): iinsert data into our struct:
      if(values_pos != 3) { //! then there might be afitfcats witht ehf roamt which we should support: 
	fprintf(stderr, "(kt-list-1d)\t values_pos:%u given str=[%s], values={%f, %f, %f}\n", values_pos, str, values[0], values[1], values[2]);
      }
      assert(values_pos == 3);           //! as we assume all data have been elvauted.
      assert(values[0] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
      assert(values[1] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
      assert(values[2] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
    //! Insert:
      self.list[i] = MF__initFromRow__s_ktType_tripleUChar(values, values_pos); //! whre latter macor is sued to simplify future copy-pasting of this code-chunk.
    }
    //#endif
  }
  //!
  //! Step():  
  //!
  //! Step():   De-allcoate:
  free__s_kt_list_1d_string(&obj_string);
  //! @return the new-generted list:
  return self;
}


//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint index, s_ktType_tripleUChar_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_tripleUChar();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_tripleUChar((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint index, s_ktType_tripleUChar_t valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_tripleUChar_t *new_list = MF__allocate__s_ktType_tripleUChar(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_tripleUChar(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      MF__free__s_ktType_tripleUChar((self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}
//! De-allcoates the s_kt_list_1d_tripleUChar_t object.
void free__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_tripleUChar((self->list)); 
    self->list = NULL; self->list_size = 0;
    self->current_pos = 0;
  }
  self->current_pos = 0;
}


//! *******************************************************************
//! *******************************************************************
//! *******************************************************************
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_tripleT_Float_t init__s_kt_list_1d_tripleT_Float_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_tripleT_Float_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_tripleT_Float(list_size);    
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_tripleT_Float();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_tripleT_Float_t object
s_kt_list_1d_tripleT_Float_t setToEmpty__s_kt_list_1d_tripleT_Float_t() {
  return init__s_kt_list_1d_tripleT_Float_t(0);
}

//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_tripleT_Float(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_tripleT_Float_t obj = self->list[i];
      fprintf(f_out, "%f\t%f\t%f\n", obj.head, obj.tail, obj.score);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}

//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleT_Float_t initFrom_sparseMatrix__s_kt_list_1d_tripleT_Float_t(const uint nrows, const uint ncols,  t_float **matrix) {
  if(nrows && (ncols == 3) && matrix) {
    s_kt_list_1d_tripleT_Float_t self = init__s_kt_list_1d_tripleT_Float_t(/*size=*/nrows); //! ie, the number of pairs.
    //!
    printf("reads w/nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
    //! Insert pairs:    
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_tripleT_Float(matrix[i], ncols);
#ifndef NDEBUG
      const uint head_id = self.list[i].head;
      /* if(matrix[i][0] >= (1000*1000)) { */
      /* printf("[%u] w/head=%f, tail=%f at %s:%d\n", i, matrix[i][0],matrix[i][1], __FILE__, __LINE__); */
      /* } */
      assert(matrix[i][0] < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
      assert(head_id < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
#endif
      if(MF__isEmpty__s_ktType_tripleT_Float(self.list[i]) == false) { //! then the valeu is set.
	self.current_pos = (i + 1); //! ie, the last index-pos.
      }
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_tripleT_Float_t();
  }
}
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleT_Float_t initFromFile__s_kt_list_1d_tripleT_Float_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_tripleT_Float_t self = initFrom_sparseMatrix__s_kt_list_1d_tripleT_Float_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}


//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleT_Float_t initFromFile__sizeKnown_s_kt_list_1d_tripleT_Float_t(const char *input_file, const uint cnt_rows) {
  assert(input_file); assert(strlen(input_file));
  assert(cnt_rows > 0);
  s_kt_list_1d_tripleT_Float_t self = init__s_kt_list_1d_tripleT_Float_t(cnt_rows);
  //!
  //! Step(): Load data from file:
  s_kt_list_1d_string_t obj_string = initFromFile_sizeKnown_s_kt_list_1d_string_t(input_file, cnt_rows);
  assert(obj_string.nrows > 0);
  assert(obj_string.nrows >= self.list_size); //! ie, as we expec tthe user-rpoviuded input-argumetns are correct.


  //!
  //! Step(): Convert into values, and then insert into our list:
  uint cnt_strings = 0;
  get_count__s_kt_list_1d_string_t(&obj_string, &cnt_strings);
  for(uint i = 0; i < cnt_strings; i++) {
  //  for(uint i = 0; i < obj_string.nrows; i++) {
    const char *str = getAndReturn_string__s_kt_list_1d_string(&obj_string, i);
    assert(str);
    assert(strlen(str));
    t_float values[3] = {T_FLOAT_MAX, T_FLOAT_MAX, T_FLOAT_MAX};
    uint values_pos = 0;
    char buff[100]; uint buff_next = 0;
    for(uint k = 0; k < 100; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
    const uint str_size = (uint)strlen(str);
    //#if false    
    //if(false) {  uint s = 0; // FIXME: remove.
    for(uint s = 0; s < str_size; s++) {
      if(str[s] != '\0') {
	if( (str[s] != '\t') && (str[s] != ' ') ) {
	  buff[buff_next] = str[s];
	  buff_next++;
	  assert(buff_next < 100); //! whre '100' is assuemd being the maximum number of chars.
	} else { //! then convert to a char:
	  if(buff_next > 0) { //! Then insert data:
	    assert(values_pos < 3); //! where '3' is based on the above hard-code specifciation.
	    values[values_pos] = (t_float)atof(buff);
	    // printf("(kt-list-1d:inside)\t [%u]\t newVal=%s=%f\n", i, buff, values[values_pos]);
	    values_pos++;
	    //! Reset the coutner:
	    for(uint k = 0; k < buff_next; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
	    buff_next = 0;
	  }
	}
      } else {
	s = str_size; //! ie, then stop iteration.
      }
    }
    if(buff_next > 0) { //! Then insert data:	  
      assert(values_pos < 3); //! where '3' is based on the above hard-code specifciation.
      values[values_pos] = (t_float)atof(buff);
      // printf("(kt-list-1d:afterEach)\t [%u]\t newVal=%s=%f\n", i, buff, values[values_pos]);
      values_pos++;
      //! Reset the coutner:
      for(uint k = 0; k < buff_next; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
      buff_next = 0;
    }	      
    if(true) { 
      //! Step(): iinsert data into our struct:
      if(values_pos != 3) { //! then there might be afitfcats witht ehf roamt which we should support: 
	fprintf(stderr, "(kt-list-1d)\t values_pos:%u given str=[%s], values={%f, %f, %f}\n", values_pos, str, values[0], values[1], values[2]);
      }
      assert(values_pos == 3);           //! as we assume all data have been elvauted.
      assert(values[0] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
      assert(values[1] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
      assert(values[2] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
    //! Insert:
      self.list[i] = MF__initFromRow__s_ktType_tripleT_Float(values, values_pos); //! whre latter macor is sued to simplify future copy-pasting of this code-chunk.
    }
    //#endif
  }
  //!
  //! Step():  
  //!
  //! Step():   De-allcoate:
  free__s_kt_list_1d_string(&obj_string);
  //! @return the new-generted list:
  return self;
}


//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint index, s_ktType_tripleT_Float_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_tripleT_Float();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_tripleT_Float((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint index, s_ktType_tripleT_Float_t valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_tripleT_Float_t *new_list = MF__allocate__s_ktType_tripleT_Float(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_tripleT_Float(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      // printf("\t(set-scalar:change-size)\t size(old):%u, size(new):%u, at %s:%d\n", old_list_size, self->list_size, __FILE__, __LINE__);
      assert(self->list != NULL); //! ie, as we for consitency expeccts that this is set/allocated.
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      MF__free__s_ktType_tripleT_Float((self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}
//! De-allcoates the s_kt_list_1d_tripleT_Float_t object.
void free__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_tripleT_Float((self->list)); 
    self->list = NULL; self->list_size = 0;
    self->current_pos = 0;
  }
  self->current_pos = 0;
}


//! *******************************************************************
//! *******************************************************************
//! *******************************************************************
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_tripleUint_t init__s_kt_list_1d_tripleUint_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_tripleUint_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_tripleUint(list_size);    
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_tripleUint();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_tripleUint_t object
s_kt_list_1d_tripleUint_t setToEmpty__s_kt_list_1d_tripleUint_t() {
  return init__s_kt_list_1d_tripleUint_t(0);
}

//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_tripleUint(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_tripleUint_t obj = self->list[i];
      fprintf(f_out, "%u\t%u\t%u\n", obj.head, obj.tail, obj.score);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}

//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleUint_t initFrom_sparseMatrix__s_kt_list_1d_tripleUint_t(const uint nrows, const uint ncols,  t_float **matrix) {
  if(nrows && (ncols == 3) && matrix) {
    s_kt_list_1d_tripleUint_t self = init__s_kt_list_1d_tripleUint_t(/*size=*/nrows); //! ie, the number of pairs.
    //!
    printf("reads w/nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
    //! Insert pairs:    
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_tripleUint(matrix[i], ncols);
#ifndef NDEBUG
      const uint head_id = self.list[i].head;
      /* if(matrix[i][0] >= (1000*1000)) { */
      /* printf("[%u] w/head=%f, tail=%f at %s:%d\n", i, matrix[i][0],matrix[i][1], __FILE__, __LINE__); */
      /* } */
      assert(matrix[i][0] < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
      assert(head_id < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
#endif
      if(MF__isEmpty__s_ktType_tripleUint(self.list[i]) == false) { //! then the valeu is set.
	self.current_pos = (i + 1); //! ie, the last index-pos.
      }
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_tripleUint_t();
  }
}
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleUint_t initFromFile__s_kt_list_1d_tripleUint_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_tripleUint_t self = initFrom_sparseMatrix__s_kt_list_1d_tripleUint_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}


//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleUint_t initFromFile__sizeKnown_s_kt_list_1d_tripleUint_t(const char *input_file, const uint cnt_rows) {
  assert(input_file); assert(strlen(input_file));
  assert(cnt_rows > 0);
  s_kt_list_1d_tripleUint_t self = init__s_kt_list_1d_tripleUint_t(cnt_rows);
  //!
  //! Step(): Load data from file:
  s_kt_list_1d_string_t obj_string = initFromFile_sizeKnown_s_kt_list_1d_string_t(input_file, cnt_rows);
  assert(obj_string.nrows > 0);
  assert(obj_string.nrows >= self.list_size); //! ie, as we expec tthe user-rpoviuded input-argumetns are correct.


  //!
  //! Step(): Convert into values, and then insert into our list:
  uint cnt_strings = 0;
  get_count__s_kt_list_1d_string_t(&obj_string, &cnt_strings);
  for(uint i = 0; i < cnt_strings; i++) {
  //  for(uint i = 0; i < obj_string.nrows; i++) {
    const char *str = getAndReturn_string__s_kt_list_1d_string(&obj_string, i);
    assert(str);
    assert(strlen(str));
    t_float values[3] = {T_FLOAT_MAX, T_FLOAT_MAX, T_FLOAT_MAX};
    uint values_pos = 0;
    char buff[100]; uint buff_next = 0;
    for(uint k = 0; k < 100; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
    const uint str_size = (uint)strlen(str);
    //#if false    
    //if(false) {  uint s = 0; // FIXME: remove.
    for(uint s = 0; s < str_size; s++) {
      if(str[s] != '\0') {
	if( (str[s] != '\t') && (str[s] != ' ') ) {
	  buff[buff_next] = str[s];
	  buff_next++;
	  assert(buff_next < 100); //! whre '100' is assuemd being the maximum number of chars.
	} else { //! then convert to a char:
	  if(buff_next > 0) { //! Then insert data:
	    assert(values_pos < 3); //! where '3' is based on the above hard-code specifciation.
	    values[values_pos] = (t_float)atof(buff);
	    // printf("(kt-list-1d:inside)\t [%u]\t newVal=%s=%f\n", i, buff, values[values_pos]);
	    values_pos++;
	    //! Reset the coutner:
	    for(uint k = 0; k < buff_next; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
	    buff_next = 0;
	  }
	}
      } else {
	s = str_size; //! ie, then stop iteration.
      }
    }
    if(buff_next > 0) { //! Then insert data:	  
      assert(values_pos < 3); //! where '3' is based on the above hard-code specifciation.
      values[values_pos] = (t_float)atof(buff);
      // printf("(kt-list-1d:afterEach)\t [%u]\t newVal=%s=%f\n", i, buff, values[values_pos]);
      values_pos++;
      //! Reset the coutner:
      for(uint k = 0; k < buff_next; k++) {buff[k] = '\0';} //! ie, a safegyuard aianst any 'future' bugs.
      buff_next = 0;
    }	      
    if(true) { 
      //! Step(): iinsert data into our struct:
      if(values_pos != 3) { //! then there might be afitfcats witht ehf roamt which we should support: 
	fprintf(stderr, "(kt-list-1d)\t values_pos:%u given str=[%s], values={%f, %f, %f}\n", values_pos, str, values[0], values[1], values[2]);
      }
      assert(values_pos == 3);           //! as we assume all data have been elvauted.
      assert(values[0] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
      assert(values[1] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
      assert(values[2] != T_FLOAT_MAX);  //! as we assume all data have been elvauted.
    //! Insert:
      self.list[i] = MF__initFromRow__s_ktType_tripleUint(values, values_pos); //! whre latter macor is sued to simplify future copy-pasting of this code-chunk.
    }
    //#endif
  }
  //!
  //! Step():  
  //!
  //! Step():   De-allcoate:
  free__s_kt_list_1d_string(&obj_string);
  //! @return the new-generted list:
  return self;
}


//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint index, s_ktType_tripleUint_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_tripleUint();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_tripleUint((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint index, s_ktType_tripleUint_t valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_tripleUint_t *new_list = MF__allocate__s_ktType_tripleUint(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_tripleUint(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      MF__free__s_ktType_tripleUint((self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}
//! De-allcoates the s_kt_list_1d_tripleUint_t object.
void free__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_tripleUint((self->list)); 
    self->list = NULL; self->list_size = 0;
    self->current_pos = 0;
  }
  self->current_pos = 0;
}


//! *******************************************************************
//! *******************************************************************
//! *******************************************************************
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_pair_t init__s_kt_list_1d_pair_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_pair_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0; // self.current_pos = 0;  
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_pair(list_size);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_pair();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_pair_t object
s_kt_list_1d_pair_t setToEmpty__s_kt_list_1d_pair_t() {
  return init__s_kt_list_1d_pair_t(0);
}
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_pair_t initFrom_sparseMatrix__s_kt_list_1d_pair_t(const uint nrows, const uint ncols, t_float **matrix) {
  if(nrows && (ncols == 2) && matrix) {
    s_kt_list_1d_pair_t self = init__s_kt_list_1d_pair_t(/*size=*/nrows); //! ie, the number of pairs.
    //!
    //! Insert pairs:
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_pair(matrix[i], ncols);
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_pair_t();
  }
}


//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_pair_t(const s_kt_list_1d_pair_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_pair(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_pair_t obj = self->list[i];
      fprintf(f_out, "%u\t%u\n", obj.head, obj.tail);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_fPair_t(const s_kt_list_1d_fPair_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_fPair(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_fPair_t obj = self->list[i];
      fprintf(f_out, "%f\t%f\n", obj.head, obj.tail);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}

//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_pair_t initFromFile__s_kt_list_1d_pair_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_pair_t self = initFrom_sparseMatrix__s_kt_list_1d_pair_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}

//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self, uint index, s_ktType_pair_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_pair();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_pair((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self, uint index, s_ktType_pair_t valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_pair_t *new_list = MF__allocate__s_ktType_pair(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_pair(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      MF__free__s_ktType_pair((self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
  //if(index >= current_pos) {current_pos = index + 1;} //! ie, the next psotion to insert at.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, const uint head, const uint tail, const t_float score) {
  assert(self);
  set_scalar__s_kt_list_1d_pairFloat_t(self, index, MF__initVal__s_ktType_pairFloat(head, tail, score)); //! ie, insert.
}
//! Add the pair to the enw set:
void add_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, const uint head, const uint tail, const t_float score) {
  assert(self);
  const uint prev_current_pos = self->current_pos;
  set_scalar__s_kt_list_1d_pairFloat_t(self, self->current_pos, MF__initVal__s_ktType_pairFloat(head, tail, score)); //! ie, insert.
  assert(prev_current_pos == self->current_pos); //! ie, as we asusemthe caller have not updated 'this'.
  self->current_pos++;
}

void push__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self, const uint head, const uint tail) {
  // assert(false == MF__isEmpty__s_ktType_kvPair(valueTo_set)); //! ie, as otehriwse would be a pointless inseriton.
  //assert(valueTo_set != T_FLOAT_MAX);
  //  printf("\t\tcurrent_pos=%u, at %s:%d\n", self->current_pos, __FILE__, __LINE__);
  const uint index = self->current_pos;
  set_scalar__s_kt_list_1d_pair_t(self, index, MF__initVal__s_ktType_pair(head, tail) );
  
  //! 
  //! Incrmeent current-pos:
  self->current_pos++;
}

//! De-allcoates the s_kt_list_1d_pair_t object.
void free__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_pair((self->list)); 
    self->list = NULL; self->list_size = 0;
    self->current_pos = 0;
  }
  //self->current_pos = 0;
}

//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_fPair_t init__s_kt_list_1d_fPair_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_fPair_t self; 
  self.list = NULL;   self.list_size = 0; // self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_fPair(list_size);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_fPair();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_fPair_t object
s_kt_list_1d_fPair_t setToEmpty__s_kt_list_1d_fPair_t() {
  return init__s_kt_list_1d_fPair_t(0);
}

//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_kvPair_t(const s_kt_list_1d_kvPair_t *self, const char *result_file) {
  assert(self);
  assert(result_file); assert(strlen(result_file));
  FILE *f_out = fopen(result_file, "wb");
  if(f_out == NULL) {
    fprintf(stderr, "!!\t Errror: unable tpo upen the result-file=\"%s\", ie, pelase vlaidate file-pat and permtissions. Now aborts the file-exprort-orutione. Observaiton at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  for(uint i = 0; i < self->list_size; i++) {
    if(MF__isEmpty__s_ktType_kvPair(self->list[i]) == false) { //! then the valeu is set.
      s_ktType_kvPair_t obj = self->list[i];
      //      fprintf(f_out, "%\t%f\n", obj.head, obj.tail);
    }
  }
  fclose(f_out); f_out = NULL;
  return true;
}

//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_fPair_t initFrom_sparseMatrix__s_kt_list_1d_fPair_t(const uint nrows, const uint ncols, t_float **matrix) {
  if(nrows && (ncols == 2) && matrix) {
    s_kt_list_1d_fPair_t self = init__s_kt_list_1d_fPair_t(/*size=*/nrows); //! ie, the number of fPairs.
    //!
    //! Insert fPairs:
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_fPair(matrix[i], ncols);
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_fPair_t();
  }
}



//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_fPair_t initFromFile__s_kt_list_1d_fPair_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_fPair_t self = initFrom_sparseMatrix__s_kt_list_1d_fPair_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}

//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_fPair_t(s_kt_list_1d_fPair_t *self, uint index, s_ktType_fPair_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_fPair();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_fPair((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_fPair_t(s_kt_list_1d_fPair_t *self, uint index, s_ktType_fPair_t valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_fPair_t *new_list = MF__allocate__s_ktType_fPair(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_fPair(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      MF__free__s_ktType_fPair((self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
  //if(index >= current_pos) {current_pos = index + 1;} //! ie, the next psotion to insert at.
}

//! De-allcoates the s_kt_list_1d_fPair_t object.
void free__s_kt_list_1d_fPair_t(s_kt_list_1d_fPair_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_fPair((self->list)); 
    self->list = NULL; self->list_size = 0;
  }
  //self->current_pos = 0;
}


//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_kvPair_t init__s_kt_list_1d_kvPair_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_kvPair_t self; 
  self.list = NULL;   self.list_size = 0;  self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    self.list = MF__allocate__s_ktType_kvPair(list_size);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = MF__init__s_ktType_kvPair();
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_1d_kvPair_t object
s_kt_list_1d_kvPair_t setToEmpty__s_kt_list_1d_kvPair_t() {
  return init__s_kt_list_1d_kvPair_t(0);
}
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_kvPair_t initFrom_sparseMatrix__s_kt_list_1d_kvPair_t(const uint nrows, const uint ncols, t_float **matrix) {
  if(nrows && (ncols == 2) && matrix) {
    s_kt_list_1d_kvPair_t self = init__s_kt_list_1d_kvPair_t(/*size=*/nrows); //! ie, the number of kvPairs.
    //!
    //! Insert kvPairs:
    for(uint i = 0; i < nrows; i++) {
      assert(matrix[i]);
      self.list[i] = MF__initFromRow__s_ktType_kvPair(matrix[i], ncols);
    }
    //! @return
    return self;
  } else {
    assert(false); //! ie, an heads-up.
    //! @return
    return setToEmpty__s_kt_list_1d_kvPair_t();
  }
}

//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_kvPair_t initFromFile__s_kt_list_1d_kvPair_t(const char *input_file) {
  assert(input_file); assert(strlen(input_file));
  s_kt_matrix_base_t mat = readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_base_t());
  s_kt_list_1d_kvPair_t self = initFrom_sparseMatrix__s_kt_list_1d_kvPair_t(mat.nrows, mat.ncols, mat.matrix);
  if(self.list_size == 0) {
    fprintf(stderr, "!!\t Was Not able to parse any relationships from file=\"%s\"; for latter we 'read' a matrix=[%u, %u], ie, not iaw. our epxectiaotns: if the latter erorr soudns confgusing, and reading the docuemtaiton does Not help, then please contact senior developer [oeksethh@gmail.com].  Observion at [%s]:%s:%d\n", input_file, mat.nrows, mat.ncols, __FUNCTION__, __FILE__, __LINE__);
  }
  //! De-allocate temproary read-object
  free__s_kt_matrix_base_t(&mat);
  //! @return
  return self;
}

//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, uint index, s_ktType_kvPair_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_kvPair();
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(false == MF__isEmpty__s_ktType_kvPair((*scalar_result))) {return true;} 
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, uint index, s_ktType_kvPair_t valueTo_set) {
  assert(self);
#include "kt_list_1d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
  //if(index >= current_pos) {current_pos = index + 1;} //! ie, the next psotion to insert at.
}

//! De-allcoates the s_kt_list_1d_kvPair_t object.
void free__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self) {
  assert(self);
  if(self->list) {
    MF__free__s_ktType_kvPair((self->list)); 
    self->list = NULL; 
  }
  self->list_size = 0;
  self->current_pos = 0;
}
//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const s_ktType_kvPair_t valueTo_set) {
  assert(false == MF__isEmpty__s_ktType_kvPair(valueTo_set)); //! ie, as otehriwse would be a pointless inseriton.
  assert(valueTo_set.tail != T_FLOAT_MAX);
  //assert(valueTo_set != T_FLOAT_MAX);
  //  printf("\t\tcurrent_pos=%u, at %s:%d\n", self->current_pos, __FILE__, __LINE__);
  const uint index = self->current_pos;
  assert(index != UINT_MAX);
#ifndef NDEBUG //! then vlaidte that the prviopus 'item' was set:
  if(index > 0) {
    const s_ktType_kvPair_t obj_this = self->list[index-1];
    assert(false == MF__isEmpty__s_ktType_kvPair(obj_this));
  }
#endif
  if(self->current_pos >= self->list_size) {
#include "kt_list_1d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  }
  assert(self->list);
  self->list[index] = valueTo_set;
  //! 
  //! Incrmeent current-pos:
  self->current_pos++;
}
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const s_ktType_kvPair_t valueTo_set) {
  assert(false == MF__isEmpty__s_ktType_kvPair(valueTo_set)); //! ie, as otehriwse would be a pointless inseriton.
  assert(valueTo_set.tail != T_FLOAT_MAX);
  //assert(valueTo_set != T_FLOAT_MAX);
  const uint index = self->current_pos;
  assert(index != UINT_MAX);
  if(self->current_pos >= self->list_size) {
#include "kt_list_1d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  }
  //! 
  //! Insert the node:
#define __MiF__objLessThan(obj1, obj2) obj1.tail < obj2.tail
  MiF__heap__insertNode(index, (self->list), (self->list_size), valueTo_set);
#undef __MiF__objLessThan
  //! 
  //! Incrmeent current-pos:
  self->current_pos++;
}
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self) {
  /* if(false) { */
  /*   t_float *list = self->list; */
  /*   int heap_index = 0; */
  /*   int largest = 1; */
  /*   MF__SWAP_complexType(t_float, (list[heap_index]), (list[largest])) ; heap_index = largest; */
  /* } */
  { //! Set the current-pos-key:
    self->current_pos = 0;
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i].tail != T_FLOAT_MAX) {
	self->current_pos = i;
      }
    }
    (self->current_pos)++;
  }
  // Making sure that heap property is also satisfied
#define __MiF__objGreaterThan(obj1, obj2) obj1.tail > obj2.tail
  MiF__heapify__all(s_ktType_kvPair_t, (self->list), (self->list_size));
#undef __MiF__objGreaterThan
}
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self) {
  if(self->current_pos > 0) {
    assert(self->current_pos != UINT_MAX); //! ie, as we expect latter to ahve been set.
    assert(self->current_pos > 0);
    /*     printf("Deleting node %d\n\n", hp->elem[0].data) ; */
    self->list[0] = self->list[self->current_pos-1];
    self->current_pos--; //! ie, remove the element
    /*     hp->elem[0] = hp->elem[--(hp->size)] ; */
    /*     hp->elem = realloc(hp->elem, hp->size * sizeof(node)) ; */
    /*     heapify(hp, 0) ; */
    //! Star thte heap-operaitons:
    int tmp_index = 0;
#define __MiF__objGreaterThan(obj1, obj2) obj1.tail > obj2.tail
    MiF__heapify(s_ktType_kvPair_t, (self->list), (self->current_pos), tmp_index);
#undef __MiF__objGreaterThan
  }
  /*   if(hp->size) { */

  /*   } else { */
  /*     printf("\nMax Heap is empty!\n") ; */
  /*     free(hp->elem) ; */
  /*   } */
}

//! ---------------------------------------------------------------------
//! ---------------------------------------------------------------------
//!
#define __MiFn__sort__type s_ktType_kvPair_t
// ------
#define __MiFn__sort __quicksort__s_kt_list_1d_kvPair_t
#define __MiFn__sort__localFunc __MiFn__sort
#define __MiF__objLessThan(obj1, obj2) obj1.tail < obj2.tail //! ie, sort by score.
#include "kt_list_1d__stub__quicksort.c"
//! ---------------------------------------------------------------------
#define __MiFn__sort__onKeys __quicksort__onKeys__s_kt_list_1d_kvPair_t
#define __MiF__objLessThan(obj1, obj2) obj1.head < obj2.head //! ie, sort by the keys.
#define __MiFn__sort__localFunc __MiFn__sort__onKeys
#include "kt_list_1d__stub__quicksort.c"
//!
//! ---------------------------------------------------------------------
//! ---------------------------------------------------------------------


//! Sort the data-set (oekseth, 06. jul. 2017).
void sort__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self) {
  if(self->list_size == 0) {return;} //! ie, as there is no elments to sort.
  uint list_size = self->current_pos;
  if( (list_size == 0) || (list_size > self->list_size)) { //! then we udpat the list-size-param.
    for(uint i = 0; i < self->list_size; i++) {
      if(false == MF__isEmpty__s_ktType_kvPair(self->list[i])) {
	list_size = i+1; //! ie, the max-number-of-elemtns with a value.
      }
    }
    self->current_pos = list_size; //! ie, to reduce any further ork, eg, wrt. our "merge_sorted_maxSizeAfter__s_kt_list_1d_kvPair_t(..)"
  }
  if(list_size <= 1) {return;} //! ie, as there is no elments to sort.
  //fprintf(stderr, "#\t list_size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
  //!
  //!    Then sort the list:
  __MiFn__sort(self->list, 0, list_size);
  //  __MiFn__sort(self->list, 0, list_size-1);
}

//! Sort the data-set though/on the keys (rather than the scores) (oekseth, 06. jul. 2017).
void sort__onKeys__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self) {
  if(self->list_size == 0) {return;} //! ie, as there is no elments to sort.
  uint list_size = self->current_pos;
  if( (list_size == 0) || (list_size > self->list_size)) { //! then we udpat the list-size-param.
    for(uint i = 0; i < self->list_size; i++) {
      if(false == MF__isEmpty__s_ktType_kvPair(self->list[i])) {
	list_size = i+1; //! ie, the max-number-of-elemtns with a value.
      }
    }
    self->current_pos = list_size; //! ie, to reduce any further ork, eg, wrt. our "merge_sorted_maxSizeAfter__s_kt_list_1d_kvPair_t(..)"
  }
  if(list_size <= 1) {return;} //! ie, as there is no elments to sort.
  //!
  //!    Then sort the list:
  __MiFn__sort__onKeys(self->list, 0, list_size-1);
}


//! Unify two sorted lists, updating "self" with the result (oekseth, 06. aug. 2017).
void merge_sorted_maxSizeAfter__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const s_kt_list_1d_kvPair_t *prev, const uint cntMax_afterEach) {
  assert(self); assert(prev); 
  assert(cntMax_afterEach != 0); //! ie, as lattter wodul be a pointless call.
  if(prev->current_pos == 0) { //! then there are no data to sort.
    return; 
  } else if(self->current_pos == 0) { //! then we extend teh list before sorting:
    //! 
    //! 
    uint index = prev->current_pos;
    assert(index != UINT_MAX);
    if(index > cntMax_afterEach) {index = cntMax_afterEach;} //! ie, where latter is assumed bieng the max-limit
    if(index >= self->list_size) {
#include "kt_list_1d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
    }
    assert(self->list_size >= prev->current_pos);
    //! Then copy and update:
    memcpy(self->list, prev->list, sizeof(s_ktType_kvPair_t)* (prev->current_pos));
    self->current_pos = prev->current_pos;
  } else { //! then we need to add an icrmeental sort-merge-appraoch:
    //#define __MiF__objLessThan(obj1, obj2) obj1.tail < obj2.tail
#define __MiF__objLessThan(obj1, obj2) obj1.head < obj2.head //! ie, sort by the keys.
    //!
    //!
#ifndef NDEBUG //! then validte teh sorted property:
    for(uint i = 1; i < prev->current_pos; i++) {
      assert(__MiF__objLessThan((prev->list[i-1]), (prev->list[i]))); //! ie, as a 'different sort-order' (or Not anysrot-order at all may give strange bus in other aprts of our softare).
    }
    for(uint i = 1; i < self->current_pos; i++) {
      assert(__MiF__objLessThan((self->list[i-1]), (self->list[i]))); //! ie, as a 'different sort-order' (or Not anysrot-order at all may give strange bus in other aprts of our softare).
    }
#endif    
    //! 
    //! 
    uint max_cnt = self->current_pos + prev->current_pos;
    if(max_cnt > self->list_size) {
      const uint index = max_cnt;
#include "kt_list_1d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
    }
    assert(max_cnt > 0);
    //!
    //! Start the merging-process:
    uint pos_self = 0; uint pos_prev = 0;
    //! 
    //! Insert the node:
    s_kt_list_1d_kvPair_t obj_tmp = init__s_kt_list_1d_kvPair_t(max_cnt);
    
    uint current_pos = 0; //! which is used to 'handle' equal elemetns.
    for(uint i = 0; i < max_cnt; i++) {
      if( (pos_self < self->current_pos) && (pos_prev < prev->current_pos) ) {
	if(__MiF__objLessThan(self->list[pos_self], prev->list[pos_prev])) {
	  obj_tmp.list[current_pos] = self->list[pos_self];
	  pos_self++;
	} else {
	  obj_tmp.list[current_pos] = prev->list[pos_prev];
	  pos_prev++;
	}
      } else if(pos_self < self->current_pos) { //! 
	obj_tmp.list[current_pos] = self->list[pos_self];
	pos_self++;
      } else {
	assert(pos_prev < prev->current_pos);
	obj_tmp.list[current_pos] = prev->list[pos_prev];
	pos_prev++;
      }
      if((current_pos==0) || (obj_tmp.list[current_pos].head != obj_tmp.list[current_pos-1].head) ) {
	current_pos++; //! else we are interesting in 'overwriting' this value in the next score.
      }
    }
    if(current_pos > cntMax_afterEach) { //! then we sort the data-set by the score and then remove the 'm' 'worst' elements:
      //!
      //! Then we are interestinged in sorting wrt. the scores and then select the top-most scores.
      sort__s_kt_list_1d_kvPair_t((&obj_tmp)); //! ie, sort by the 'scores' rather than the 'keys'.
      for(uint i = /*nn_numberOfNodes=*/cntMax_afterEach; i < /*cnt_added=*/current_pos; i++) {
	obj_tmp.list[i] = MF__init__s_ktType_kvPair();
	assert(MF__isEmpty__s_ktType_kvPair(obj_tmp.list[i])); //! ie, to reflect [above].
      }
      //! Note: 
      //! Note: while a sorted(list(keys)) are used for uniquness-testing the sorted(list(scores)) are used for rank-filtering. To address/overcome boht needs/requirements we use/apply a "sorted(list(keys))" wrt. the 'sort(keys)' during the merigng-procdure and thereafter 'sort(scores)' during the max-filter-proceudre.
      // max_cnt = cntMax_afterEach; //! ie, to avodi 'passing' the count-threshold.
    }
    //!
    //! Then update the object:
    free__s_kt_list_1d_kvPair_t(self); //! ie, de-allcoate the old object.    
    *self = obj_tmp;
    self->current_pos = current_pos;
#undef __MiF__objLessThan
    
    


  }
}

//! Udpat ethe object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const uint list_size) {
  assert(list_size > 0); //Â§! ie, as the 'free-memory' sohudl otheriwse have been called.
  { //! Enlarge list if nessseary:
    s_ktType_kvPair_t *new_list = MF__allocate__s_ktType_kvPair(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_kvPair(); //! ie, 'set to empty'.
      //      new_list[i] = setToEmpty__s_kt_list_1d_kvPair_t(); // MF__init__s_ktType_kvPair(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(self->list_size != 0) {
      const uint cnt_copy = macro_min(list_size, self->list_size);
      for(uint i = 0; i < cnt_copy; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      /* for(uint i = cnt_copy; i < self->list_size; i++) { */
      /* free__s_kt_list_1d_kvPair_t(&(self->list[i])); //! ie, to de-allcoat the 'overflow'. */
      /* } */
      if(self->list != NULL) {
	MF__free__s_ktType_kvPair((self->list)); 
	//MF__free__s_kt_list_1d_kvPair_t((self->list)); 
      }
    }
    //! Update pointer:
    self->list = new_list;    
    self->list_size = list_size;
  }
}

//! Filter the results to include only the subset of identifed entries (oekseth, 06. otk. 2017)
void filterResult__sort__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach) {
  uint current_pos = self->current_pos;
  
  //      printf("sorts the rows (w/cnt=%u, on the scores rather than IDs), at %s:%d\n", current_pos, __FILE__, __LINE__);
  sort__s_kt_list_1d_kvPair_t(self); //! ie, sort by the 'scores' rather than the 'keys'
#if(0 == 1)
  for(uint i = 1; i < current_pos; i++) {
    printf("compare[%u] %f VS %f, at %s:%d\n", i, self->list[i-1].tail, self->list[i].tail, __FILE__, __LINE__);
  }
#endif
#ifndef NDEBUG //! Then validate the sorted-proeprty:
  for(uint i = 1; i < current_pos; i++) {
    //printf("compare[%u] %f VS %f, at %s:%d\n", i, self->list[i-1].tail, self->list[i].tail, __FILE__, __LINE__);
    if(self->list[i].tail < self->list[i-1].tail) {
      fprintf(stderr, "!!\t compare[%u] %f VS %f, at %s:%d\n", i, self->list[i-1].tail, self->list[i].tail, __FILE__, __LINE__);
    }
    //fprintf(stderr, "compare[%u] %f VS %f, at %s:%d\n", i, self->list[i-1].tail, self->list[i].tail, __FILE__, __LINE__);
    //if(self->list[i].tail > (uint)self->list[i-1].tail);
    //assert((uint)self->list[i].tail <= (uint)self->list[i-1].tail);
    assert((uint)self->list[i].tail >= (uint)self->list[i-1].tail);
    //assert((uint)self->list[i].tail >= (uint)self->list[i-1].tail);
    //assert(self->list[i].tail > self->list[i-1].tail);
  }
#endif
  if( (current_pos > config_cntMin_afterEach) || (config_cntMin_afterEach == UINT_MAX) ) {
    if(current_pos > config_cntMax_afterEach) { //! then we sort the data-set by the score and then remove the 'm' 'worst' elements:
      assert(config_cntMin_afterEach != 0); 
      assert(config_cntMin_afterEach != UINT_MAX);
      for(uint i = /*nn_numberOfNodes=*/config_cntMax_afterEach; i < /*cnt_added=*/current_pos; i++) {
	//      for(uint i = /*nn_numberOfNodes=*/config_cntMax_afterEach; i < /*cnt_added=*/current_pos; i++) {
	// printf("\t\t(ignore-pair)\t(%u, %f), at %s:%d\n", self->list[i].head, self->list[i].tail, __FILE__, __LINE__);
	//if(self->list[i].tail != self->list[config_cntMax_afterEach-1].tail) { //! then 
	self->list[i] = MF__init__s_ktType_kvPair();
	assert(MF__isEmpty__s_ktType_kvPair(self->list[i])); //! ie, to reflect [above].
      }
      current_pos = config_cntMax_afterEach; //! ie, set the trheshold
    }
    if((current_pos *2) < self->list_size) { //! then we 'comrpess' the meomry-size-usage:
      assert(current_pos > 0);
      setTo_size__s_kt_list_1d_kvPair_t(self, current_pos);
    }
    //! Note: 
    //! Note: while a sorted(list(keys)) are used for uniquness-testing the sorted(list(scores)) are used for rank-filtering. To address/overcome boht needs/requirements we use/apply a "sorted(list(keys))" wrt. the 'sort(keys)' during the merigng-procdure and thereafter 'sort(scores)' during the max-filter-proceudre.
    // max_cnt = cntMax_afterEach; //! ie, to avodi 'passing' the count-threshold.
    
  }
  self->current_pos = current_pos;
  //  fprintf(stderr, "##\t\t\t current_pos[%u], given config_cntMin_afterEach=%u, at %s:%d\n", current_pos, config_cntMin_afterEach, __FILE__, __LINE__);
}
