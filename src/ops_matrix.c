/**
   @brief 
   @author Ole Kristian Ekseth
   @remarks 
   - for autmation call our "x_measure_all.pl"
 **/
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_mathMacros.h"
#include "types_base.h"
#include "types.h"

#ifndef macro_pluss
#define macro_pluss(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_add
#define macro_add(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_minus
#define macro_minus(term1, term2) ({term1 - term2;})
#endif
#ifndef macro_mul
#define macro_mul(term1, term2) ({term1 * term2;})
#endif



/**
   @brief code is taken from: "kt_math_matrix.c"
 **/

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast kt_func_multiplyMatrices_slow
#define __MiF__ops_each scalar_result += matrix[row_id][j] * matrix[col_id][j];
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_abs
#define __MiF__ops_each scalar_result += mathLib_float_abs(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast  defaultAritmethics_float_ceil
#define __MiF__ops_each scalar_result += mathLib_float_ceil(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast  defaultAritmethics_float_floor
#define __MiF__ops_each scalar_result += mathLib_float_floor(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_log
#define __MiF__ops_each scalar_result += mathLib_float_log(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_log2
#define __MiF__ops_each scalar_result += mathLib_float_log_2(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_log_abs
#define __MiF__ops_each scalar_result += mathLib_float_log_abs(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_log2_abs
#define __MiF__ops_each scalar_result += mathLib_float_log_2_abs(matrix[row_id][j] * matrix[col_id][j]);
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_log_shannon
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result +=macro_mul(val_1,  mathLib_float_log_2_abs(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_sqrt
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_sqrt(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_sqrt_abs
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each  scalar_result += macro_mul(val_1,  mathLib_float_sqrt_abs(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_cos
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_cos(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_sin
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_sin(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_atan
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_atan(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_exp
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_exp(val_1));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_pow2
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_pow(val_1, 2));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_pow3
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_pow(val_1, 3));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_float_pow4
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  mathLib_float_pow(val_1, 4));
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast defaultAritmethics_rand_srand
#define __MiF__ops_each_preStep_1 t_float val_1 = matrix[row_id][j] * matrix[col_id][j];
#define __MiF__ops_each scalar_result += macro_mul(val_1,  (t_float)rand());
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#if(1 == 2)
#define __funcName__fast 
#define __MiF__ops_each_preStep_1 
#define __MiF__ops_each 
#include "ops_matrix_slow_euclid.c"
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#endif
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//!
#define __funcName__fast dot_fast_1Add_1Add
#define __MiF__ops_each VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))
#include "ops_matrix_fastTiled.c"
//! ------------------------------------------------------------------------
//!
#define __funcName__fast dot_fast_1Add_1Div
#define __MiF__ops_each VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))
#include "ops_matrix_fastTiled.c"
//! ------------------------------------------------------------------------
//!
#define __funcName__fast dot_fast_1Add_1Mul
#define __MiF__ops_each VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))
#include "ops_matrix_fastTiled.c"

//! --------------------
//! Include functions which call other funcitons, testing cost of function calls, and different inlining strateiges:
#include "dot_slow_funcCalls_sameFile.c"



//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_dot_slow_IfClauses(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
      for(; j < size_adjusted; j++) {
	if(isOf_interest(matrix[row_id][j]) && isOf_interest(matrix[col_id][j])) {
	  scalar_result += matrix[row_id][j] * matrix[col_id][j];
	}
	//scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

#if(VECTOR_CONFIG_USE_FLOAT == 1) //! whcih is used to simplfiy our logics.
//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_dot_slow_IfClausesNan(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
      for(; j < size_adjusted; j++) {
	if(!isnan(matrix[row_id][j]) && !isnan(matrix[col_id][j])) {
	  scalar_result += matrix[row_id][j] * matrix[col_id][j];
	}
	//scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_dot_slow_transposed(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint j = 0; j < size_adjusted; j++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      for(uint row_id = 0; row_id < size_adjusted; row_id++) {
	// uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
	//if(!isnan(matrix[row_id][j]) && !isnan(matrix[col_id][j]))
	{
	  result[row_id][col_id] += matrix[row_id][j] * matrix[col_id][j];
	  // scalar_result += matrix[row_id][j] * matrix[col_id][j];
	}
	//scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      //result[row_id][col_id] += scalar_result;
    }
  }
}

//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_dot_slow_IfClausesNan_assert(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
      for(; j < size_adjusted; j++) {
	assert(!isnan(matrix[row_id][j]) && !isnan(matrix[col_id][j])); //! which my hopeuflly 'help' the compielr wrt. [below] strategy.
	if(!isnan(matrix[row_id][j]) && !isnan(matrix[col_id][j])) {
	  scalar_result += matrix[row_id][j] * matrix[col_id][j];
	}
	//scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

void kt_func_multiplyMatrices_slow_dot_ifClauseMaskAllTrue(t_float **matrix, char **matrix_mask, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
      for(; j < size_adjusted; j++) {
	//assert(!isnan(matrix[row_id][j]) && !isnan(matrix[col_id][j])); //! which my hopeuflly 'help' the compielr wrt. [below] strategy.
	if(matrix_mask[row_id][j] && matrix_mask[col_id][j]) {
	  scalar_result += matrix[row_id][j] * matrix[col_id][j];
	}
	//scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#include "ops_matrix_fastTiled_fastEuclid.c" //! for functions with "kt_func_metricInner_fast__fitsIntoSSE__float" prefix.
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#include "ops_matrix_fastTiled_float_mask_char.c" //! for functions with "float_mask_char" prefix.
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#include "ops_matrix_fastTiled_float_mask_char_explicit.c" //! for functions with "float_mask_char_explicit" prefix.

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid

void kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    uint int_j = 0; t_float scalar_result = 0; 
	    for(; int_j < SM; int_j += 1) {
	      scalar_result = macro_add(scalar_result, macro_mul(row_1[int_j], row_2[int_j]));
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}

//void kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float_useAssertAndIf
void kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float__useAssert(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    uint int_j = 0; t_float scalar_result = 0; 
	    for(; int_j < SM; int_j += 1) {
	      assert(row_1[int_j] != T_FLOAT_MAX);
	      assert(row_2[int_j] != T_FLOAT_MAX);
	      //! 
	      //if( (row_1[j] != T_FLOAT_MAX) && (row_2[j] != T_FLOAT_MAX) )
	      {
		scalar_result = macro_add(scalar_result, macro_mul(row_1[int_j], row_2[int_j]));
	      }
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}
void kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float_empty_if(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    uint int_j = 0; t_float scalar_result = 0; 
	    for(; int_j < SM; int_j += 1) {
	      //assert(row_1[int_j] != T_FLOAT_MAX);
	      //assert(row_2[int_j] != T_FLOAT_MAX);
	      //! 
	      //if( (row_1[j] != T_FLOAT_MAX) && (row_2[j] != T_FLOAT_MAX) )
	      if( (row_1[int_j] != T_FLOAT_MAX) && (row_2[int_j] != T_FLOAT_MAX) ) 
	      {
		scalar_result = macro_add(scalar_result, macro_mul(row_1[int_j], row_2[int_j]));
	      }
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}
void kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float_empty_if__useAssert(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    uint int_j = 0; t_float scalar_result = 0; 
	    for(; int_j < SM; int_j += 1) {
	      assert(row_1[int_j] != T_FLOAT_MAX);
	      assert(row_2[int_j] != T_FLOAT_MAX);
	      if( (row_1[int_j] != T_FLOAT_MAX) && (row_2[int_j] != T_FLOAT_MAX) ) 
		//! 
		//if( (row_1[j] != T_FLOAT_MAX) && (row_2[j] != T_FLOAT_MAX) )
		{
		  scalar_result = macro_add(scalar_result, macro_mul(row_1[int_j], row_2[int_j]));
		}
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}

//! -------------------------------------------
//! -------------------------------------------
//! Comptue inner distance-metric for cityblock
void dot_fast_dataTypes_float_load(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  assert( (nrows % VECTOR_FLOAT_ITER_SIZE) == 0);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
    //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOADU(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))); 
	    //	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADUU(&row_1[int_j]), VECTOR_FLOAT_LOADUU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))));
	      //vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADUU(&row_1[int_j]), VECTOR_FLOAT_LOADUU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! ------------------------------------------------------------------------
void dot_fast_dataTypes_16bit_load(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	      //	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! Comptue inner distance-metric for cityblock
void dot_fast_dataTypes_16bit_load_cityBlock(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! ------------------------------------------------------

#endif //! endif(VECTOR_CONFIG_USE_FLOAT == 1) //! whcih is used to simplfiy our logics.

//! Intalise the mask matrtrix
#define __initMask \
  char **matrix_mask = allocate_2d_list_char(nrows, ncols, default_value); \
  assert(matrix_mask); \
  for(uint row_id = 0; row_id < nrows; row_id++) {			\
    for(uint col_id = 0; col_id < ncols; col_id++) { matrix_mask[row_id][col_id] = default_value;} } \




//! The main-function to be called for startint hte class-based correctness-tests.
int main(const int array_cnt, char **array) {
  //! A simple input validation:
  if(array_cnt < 3) {
    fprintf(stderr, "Usage: %s <matrix-dimension: number> <operation>\n", array[0]);
    return -1;
  }
  assert((array[1]));
  assert((array[2]));
  assert(strlen(array[1]));
  assert(strlen(array[2]));
  //! 
  const uint nrows = (uint)atoi(array[2]); const uint ncols = nrows;
  assert(nrows > 0); 
  t_float **matrix      = NULL;   t_float **mat_result  = NULL;
  int16_t **matrix_int16      = NULL;   int16_t **mat_result_int16  = NULL;
  if(strstr(array[1], "16bit")) {
    int16_t def_value = 0;
    matrix_int16 = allocate_2d_list_int16_t(nrows, ncols, def_value);
    mat_result_int16 = allocate_2d_list_int16_t(nrows, nrows, def_value);
    assert(matrix_int16);
    assert(mat_result_int16);
    //!
    //! Set score:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	matrix_int16[row_id][col_id] = (1 + row_id) / (1 + col_id);
      }
    }
  } else {
    t_float def_value = 0;
    matrix = allocate_2d_list_float(nrows, ncols, def_value);
    mat_result = allocate_2d_list_float(nrows, nrows, def_value);
    assert(matrix);
    assert(mat_result);
    //!
    //! Set score:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	matrix[row_id][col_id] = (1 + row_id) / (1 + col_id);
      }
    }
  }


  //!
  //! 
#define __cmp(str)  ( (strlen(str) == strlen(array[1])) && (0 == strcmp(str, array[1])) ) 
  if(__cmp("dot_slow")) {
    kt_func_multiplyMatrices_slow(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_IfClauses")) {
    kt_func_multiplyMatrices_slow_dot_slow_IfClauses(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_abs")) {
    defaultAritmethics_float_abs(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_ceil")) {
    defaultAritmethics_float_ceil(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_floor")) {
    defaultAritmethics_float_floor(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_log")) {
    defaultAritmethics_float_log(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_log2")) {
    defaultAritmethics_float_log2(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_log_abs")) {
    defaultAritmethics_float_log_abs(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_log2_abs")) {
    defaultAritmethics_float_log2_abs(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_log_shannon")) {
    defaultAritmethics_float_log_shannon(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_sqrt")) {
    defaultAritmethics_float_sqrt(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_sqrt_abs")) {
    defaultAritmethics_float_sqrt_abs(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_cos")) {
    defaultAritmethics_float_cos(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_sin")) {
    defaultAritmethics_float_sin(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_atan")) {
    defaultAritmethics_float_atan(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_exp")) {
    defaultAritmethics_float_exp(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_pow2")) {
    defaultAritmethics_float_pow2(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_pow3")) {
    defaultAritmethics_float_pow3(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_float_pow4")) {
    defaultAritmethics_float_pow4(matrix, nrows, mat_result);
  } else if(__cmp("defaultAritmethics_rand_srand")) {
    defaultAritmethics_rand_srand(matrix, nrows, mat_result);
  } else if(__cmp("dot_fast_1Add_1Add-128")) {
    const uint SM = 128;
    dot_fast_1Add_1Add(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_1Add_1Div-128")) {
    const uint SM = 128;
    dot_fast_1Add_1Div(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_1Add_1Mul-128")) {
    const uint SM = 128;
    dot_fast_1Add_1Mul(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead(matrix, nrows, mat_result);        
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_static")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_static(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static(matrix, nrows, mat_result);        
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_static")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_static(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline(matrix, nrows, mat_result);        
  } else if(__cmp("dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline")) {
    dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline(matrix, nrows, mat_result);    
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead(matrix, nrows, mat_result);        
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_static")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_static(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static(matrix, nrows, mat_result);        
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_static")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_static(matrix, nrows, mat_result);    
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_inline")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_inline(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline")) {
    dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline(matrix, nrows, mat_result);        
#if(false)
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);        
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);        
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);    
#endif
#if(VECTOR_CONFIG_USE_FLOAT == 1) //! whcih is used to simplfiy our logics.
  } else if(__cmp("dot_slow_IfClausesNan")) {
    kt_func_multiplyMatrices_slow_dot_slow_IfClausesNan(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_IfClausesNanAssert")) {
    kt_func_multiplyMatrices_slow_dot_slow_IfClausesNan_assert(matrix, nrows, mat_result);
  } else if(__cmp("dot_slow_transposed")) {
    kt_func_multiplyMatrices_slow_dot_slow_transposed(matrix, nrows, mat_result);
  } else if(__cmp("ifClauseMaskAllTrue")) {
    char default_value = true;
    __initMask; //! ie, intalise the mask matrtrix
    //! Apply:
    kt_func_multiplyMatrices_slow_dot_ifClauseMaskAllTrue(matrix, matrix_mask, nrows, mat_result); \
    //! De-allcoate:
    assert(matrix_mask);     free_2d_list_char(&matrix_mask, nrows); matrix_mask = NULL;
  } else if(__cmp("ifClauseMaskAllFalse")) {
    char default_value = false;
    char **matrix_mask = allocate_2d_list_char(nrows, ncols, default_value);
    assert(matrix_mask);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) { matrix_mask[row_id][col_id] = default_value;} }
    //! Apply:
    kt_func_multiplyMatrices_slow_dot_ifClauseMaskAllTrue(matrix, matrix_mask, nrows, mat_result);
    //! De-allcoate:
    assert(matrix_mask);
    free_2d_list_char(&matrix_mask, nrows); matrix_mask = NULL;
    /*
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);
  } else if(__cmp("")) {
    (matrix, nrows, mat_result);
    */
  } else if(__cmp("dot_fast-32")) {
    const uint SM = 32;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast-64")) {
    const uint SM = 64;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__2Add(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_3Add-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__3Add(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add-1Abs-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__2Add_1Abs(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add-2Abs-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__2Add_2Abs(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_1Add-1Max-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__1Add_1Max(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add-1Max-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__1Add_2Max(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add1Mult-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__2Add_1Mul(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add2Mult-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__2Add_2Mul(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_2Add3Mult-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast__fitsIntoSSE__float__2Add_3Mul(matrix, matrix, nrows, ncols, mat_result, SM);
  /* } else if(__cmp("")) { */
  /*   const uint SM = 128; */
  /*   assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'. */
  /*   (matrix, matrix, nrows, ncols, mat_result, SM); */
  } else if(__cmp("dot_fast_xmtSSE-32")) {
    const uint SM = 32;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_xmtSSE-64")) {
    const uint SM = 64;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_xmtSSE-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_xmtSSE-128-wAsserts")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float__useAssert(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_xmtSSE-128-empty-if")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float_empty_if(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_xmtSSE-128-empty-if-wAsserts")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    kt_func_metricInner_fast_xmtSSE__fitsIntoSSE__euclid__float_empty_if__useAssert(matrix, matrix, nrows, ncols, mat_result, SM);        
  } else if(__cmp("dot_fast_dataTypes_float_load-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
    dot_fast_dataTypes_float_load(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("dot_fast_dataTypes_16bit_load-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);
  } else if(__cmp("dot_fast_dataTypes_16bit_load_cityBlock-128")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load_cityBlock(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);
  } else if(__cmp("float_mask_char_pair_implicit_removeHidden_getCount")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_implicit_removeHidden_getCount(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_implicit_removeHidden")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_implicit_removeHidden(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_replaceZeroFrom_nan")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_replaceZeroFrom_nan(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_replaceZeroFrom_inf")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_replaceZeroFrom_inf(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_zeroFilter_case1_add")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_zeroFilter_case1_add(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_zeroFilter_case1_div")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_zeroFilter_case1_div(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_case2_div")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_case3_div(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_case3_div")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_case3_div(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_pair_case0_div")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    float_mask_char_pair_case0_div(matrix, matrix, nrows, ncols, mat_result, SM);
  } else if(__cmp("float_mask_char_explicit_case_1_andSum")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    char default_value = true;
    __initMask; //! ie, intalise the mask matrtrix
    assert(matrix);
    assert(mat_result);
    //! Apply:
    float_mask_char_explicit_case_1_andSum(matrix, matrix, matrix_mask, matrix_mask, nrows, ncols, mat_result, SM);
    //! De-allcoate:
    assert(matrix_mask);     free_2d_list_char(&matrix_mask, nrows); matrix_mask = NULL; 
  } else if(__cmp("float_mask_char_explicit_case_2_andSum")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    char default_value = true;
    __initMask; //! ie, intalise the mask matrtrix
    assert(matrix);
    assert(mat_result);
    //! Apply:
    float_mask_char_explicit_case_2_andSum(matrix, matrix, matrix_mask, matrix_mask, nrows, ncols, mat_result, SM);
    //! De-allcoate:
    assert(matrix_mask);     free_2d_list_char(&matrix_mask, nrows); matrix_mask = NULL;   
    /*
      } else if(__cmp("")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    (matrix, matrix, nrows, ncols, mat_result, SM);
      } else if(__cmp("")) {
    const uint SM = 128;
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    assert(matrix);
    assert(mat_result);
    (matrix, matrix, nrows, ncols, mat_result, SM);    
    */
    //      } else if(__cmp("")) {
#endif //! endif(VECTOR_CONFIG_USE_FLOAT == 1) //! whcih is used to simplfiy our logics.
  } else {
    fprintf(stderr, "label=\"%s\" not known, at %s:%d\n", array[1], __FILE__, __LINE__);
    assert(false); //! ie, then add support for this paramter. 
  }
  //!
  //! Avoid compiation-paramters from 'dropping the compuaiton ... ie, comptue a scalar wr.t the results.
  t_float sum = 0;
  if(mat_result) {
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < nrows; col_id++) {
	sum += mat_result[row_id][col_id];
      }
    }
  } else {
    assert(mat_result_int16);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < nrows; col_id++) {
	sum += (t_float)mat_result_int16[row_id][col_id];
      }
    }
  }
  fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
  //!
  //! De-allocate:
  if(matrix) {
    free_2d_list_float(&matrix, nrows);
    free_2d_list_float(&mat_result, nrows);
  }
  if(matrix_int16) {
    free_2d_list_int16_t(&matrix_int16, nrows);
    free_2d_list_int16_t(&mat_result_int16, nrows);
  }
#undef __cmp
  return 1;
}
