#ifndef hp_api_entropy_h
#define hp_api_entropy_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "hp_entropy.h"

/**
   @brief read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
   @param <stringOf_enum> is used to dienitfy teh tyep of lgoics to be used (eg, wrt. CCM, sim-metircs and clust-alg).
   @param <stringOf_inputFile_1> is the input-data-set to use: expected to be a tab-seperated (tsv) file
   @param <isToOnlyWriteOut_avg> which if set to true impleis taht we only write out the average sum of score (rahter than seprately for each row).
   @param <max_theoreticCountSize>  which is used as optizianot heuritstc during comptuations.
   @param <isTo_applyPostScaling> which prove supprot for 'averag'ign' an eotpry-score for a gvien row.
   @param <computeFor_freq> which is an erlntive permtuation to compute entropy
   @return true upon scucess.
   @remarks the possible number of enum-strings may be seen by calling our "printOptions__hp_api_entropy()".
 **/
bool readFromFile__exportResult__hp_api_entropy(const char *stringOf_enum, const char *stringOf_inputFile_1,  const char *stringOf_exportFile, const bool isToOnlyWriteOut_avg, const uint max_theoreticCountSize, const bool isTo_applyPostScaling, const bool computeFor_freq);
//bool readFromFile__exportResult__hp_api_entropy(const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse);
/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_entropy(char **arg, const uint arg_size);
//static void printOptions____hp_api_entropy();

//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_entropy(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const bool isToOnlyWriteOut_avg__par, const uint max_theoreticCountSize, const bool isTo_applyPostScaling, const bool computeFor_freq, uint *scalar_cntArgs, char **listOf_args__2d);



#endif //! EOF
