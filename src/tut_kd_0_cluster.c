#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
/**
   @brief demosntrtes how to comptue kd-tree through the hpLysis-interface (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
**/
int main(const int array_cnt, char **array) 
#else
  int tut_kd_0_cluster(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
  const uint index_pos = 3;
  bool fileIs_speificed = false; //! which is used to simplify error-generiaotn.
  if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) {
    //! then set the file-name:
    file_name = array[index_pos];
    fileIs_speificed = true;
  }
  //!
  //! Load an arbitrary data-set:
  // const uint nrows = 1000; const uint ncols = 20;
  //const uint nrows = 1000; const uint ncols = 20;
  //  const uint nrows = 1000*10; const uint ncols = 5;
  //const uint nrows = 1000*1000; const uint ncols = 20;
  s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols);
  //! Then laod the file:
  assert(file_name); assert(strlen(file_name));
  import__s_kt_matrix_t(&mat_input, file_name);
  if(mat_input.nrows == 0) {
    if(fileIs_speificed) {
      fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
    } else { //! then we asusmet eh file-name was spefieid by the user.
      fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
    }
    return false;
  }
  //!
  //! Configure the clustering
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree;
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;

  //!
  //! Apply logics:
  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
  const uint arg_npass = 1000;
  uint nclusters = UINT_MAX; //! ie, not of itnerest as we use DB-SCAN with rank-based iflfter-proeprties.
  const bool is_also_ok = cluster__hpLysis_api (
						&obj_hp, clusterAlg, &mat_input,
						/*nclusters=*/ nclusters, /*npass=*/ arg_npass
						);
  //! 
  //! 
  { //! Export: traverse teh generated result-matrix-object and write out the results:    
    //! Note: the [”elow] calls implictly 'test' the/our "cuttree(..)" proceudre (first described in the work of "clsuter.c"):
    const uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
    assert(vertex_clusterId);
    const uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;
    assert(cnt_vertex > 0);
    FILE *file_out = stdout;
    fprintf(file_out, "clusterMemberships=[");
    uint max_cnt = 0;
    for(uint i = 0; i < cnt_vertex; i++) {fprintf(file_out, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
    //! Get the CCM-score:
    const t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
    fprintf(file_out, "], w/biggestClusterId=%u, ccm_score=%f, at %s:%d\n", max_cnt, ccm_score, __FILE__, __LINE__);
  }
  //! De-allocates the "s_hpLysis_api_t" object.
  free__s_hpLysis_api_t(&obj_hp);	    
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}
