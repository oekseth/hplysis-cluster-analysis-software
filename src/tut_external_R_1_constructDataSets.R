if(!require(devtools)) install.packages("devtools")
devtools::install <- github("kassambara/factoextra")

library(factoextra)
data("multishapes")
df <- multishapes[, 1:2]
set.seed(123)
km.res <- kmeans(df, 5, nstart = 25)
fviz_cluster(km.res, df, frame = FALSE, geom = "point")
