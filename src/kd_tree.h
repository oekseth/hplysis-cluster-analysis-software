#ifndef kt_kd_tree_h
#define kt_kd_tree_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_kd_tree_h
   @brief providee lgoics for kd-tree-comptautiosn (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_list_1d.h"
#include "kt_list_2d.h"
#include "kt_matrix.h"
#include "kt_set_1dsparse.h"
#include "hp_distance.h"
#include "kt_matrix_base.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_forest_findDisjoint.h"
/**
   @struct s_kd_tree_node
   @brief idneties the proerpteis fo each slit-branch-node in the kd-tree (oekseth, 06. jul. 2017)
 **/
typedef struct s_kd_tree_node {
  bool isSet;
  // FIXME[code]: incldue and re-name [below] ...   
  uint cut_dim;// dimension to cut; 
  t_float cut_val, cut_val_left, cut_val_right;  //cut value
  uint l,u;  // extents in index array for searching
  s_kt_list_1d_fPair_t box; //vector<interval> box; // [min,max] of the box enclosing all points  
  struct s_kd_tree_node *left, *right;  // pointers to left and right nodes. 
} s_kd_tree_node_t;



/**
   @struct s_kd_tree
   @brief a  wrapper for the KD-tree data-structure (oekseth, 06. jul. 2017).
 **/
typedef struct s_kd_tree {
  // FIXME[code]: clean/udpae our "s_kd_tree" struct 
  s_kd_tree_node_t *root;
  //const array2dfloat* data;
  s_kt_list_1d_uint_t map_index; //! which is used to re-arrange the points.
  const s_kt_matrix_t *mat_input;
  s_kt_matrix_t mat_inputTransp;
  static const int bucketsize = 50;  // global constant. 
  //  static const int bucketsize = 12;  // global constant. 
  //  bool config_perm_useRequsiveLoop;
} s_kd_tree_t;

//! An internal fucniton to support inaiotn fore both kd-tree and brute-force-matrix-operaitons.
s_kd_tree_t __build_tree__s_kd_tree(const s_kt_matrix_t *mat_input, const bool isTo_buildKD);

/**
   @brief constrct a K-D tree (oekset, 06. jul. 2017).
   @param <mat_input> is the feautre-amtrix to comptue for.
   @return an initiliazated object of type "s_kd_tree_t"
 **/
s_kd_tree_t build_tree__s_kd_tree(const s_kt_matrix_t *mat_input);
//! De-allcotes the s_kd_tree_t object
void free__s_kd_tree(s_kd_tree_t *self);

//! Spefiy the "s_kd_result_t" object (oekseth, 06. jul. 2017).
typedef struct s_kd_result {
  s_kt_list_1d_kvPair_t result;
  //s_kt_set_1dsparse_t result; //! ie the result-object deifned in our "kt_set_1dsparse.h".
} s_kd_result_t;

//! Initiate the "s_kd_result_t" object (oekseth, 06. jul. 2017).
static s_kd_result_t init__s_kd_result_t() {
  s_kd_result_t self; self.result = setToEmpty__s_kt_list_1d_kvPair_t();
  return self;
}
//! De-allcote the "s_kd_result_t" object (oekseth, 06. jul. 2017).
static void free__s_kd_result_t(s_kd_result_t *self) {
  assert(self);
  free__s_kt_list_1d_kvPair_t(&(self->result));
}
/**
   @enum e_kd_tree_searchStrategy
   @brief identies the kd-tree-appraoch to use (oekseth, 06. jul. 2017).
 **/
typedef enum e_kd_tree_searchStrategy {
  e_kd_tree_searchStrategy_n_nearest,
  e_kd_tree_searchStrategy_n_nearest_around_point,
  e_kd_tree_searchStrategy_r_nearest,
  e_kd_tree_searchStrategy_r_count,
  e_kd_tree_searchStrategy_r_nearest_around_point,
  e_kd_tree_searchStrategy_r_count_around_point,
  e_kd_tree_searchStrategy_brute_fast, //! ie, a fast-impemtatnion of a brute-force strategy
  e_kd_tree_searchStrategy_brute_slow, //! ie, a slow impemtantion of a brute-force-impemtantion-strategy.
  /* e_kd_tree_searchStrategy_, */
  /* e_kd_tree_searchStrategy_, */
  e_kd_tree_searchStrategy_undef
} e_kd_tree_searchStrategy_t;

/**
   @struct s_kd_searchConfig_config
   @brief configures the search-object of "s_kd_searchConfig" (oekseth, 06. jul. 2017)
 **/
typedef struct s_kd_searchConfig_config {
  bool rearrange;
  uint nn; // , nfound;
  t_float ballsize;
  t_float ballsize_input;
  uint correltime;
  bool isTo_sort;
  e_kd_tree_searchStrategy_t enum_id;
} s_kd_searchConfig_config_t;

//! @return an intiated "s_kd_searchConfig_config_t" object.
#define setToEmpty__s_kd_searchConfig_conf() ({ s_kd_searchConfig_config_t self; self.rearrange = false; self.nn = UINT_MAX; self.ballsize = T_FLOAT_MAX; self.ballsize_input = T_FLOAT_MAX; self.correltime = UINT_MAX; self.isTo_sort = false; self.enum_id = e_kd_tree_searchStrategy_n_nearest; self;})
//! Set to defailt proeprties:
#define init__s_kd_searchConfig_conf() ({ s_kd_searchConfig_config_t self; self.rearrange = false; self.nn = /*rank=*/10; self.ballsize = 10; self.ballsize_input = 10; self.correltime = UINT_MAX; self.isTo_sort = false; self.enum_id = e_kd_tree_searchStrategy_n_nearest; self;})

/**
   @struct s_kd_searchConfig
   @brief configures the search-object (oekseth, 06. jul. 2017).
 **/
typedef struct s_kd_searchConfig {
  s_kd_tree_t *self;
  s_kd_tree_t self_base;
  bool self_isOwnedByThis;
  /* friend class kt_tree_kd__; */
  /* friend class s_kd_tree_node; */
  const t_float *row_1;
  //vector<float>& qv;
  uint dim;
  // FIXME[tut+perf]: ... 
  // FIXME[tut+perf]: ... evlauat the "rearrange" option .... 
  s_kd_searchConfig_config_t conf;
  uint centeridx;
  s_kd_result *result;
  s_kt_matrix_base_t matShallow_1;
  s_kt_matrix_base_t matShallow_2;
  const s_kt_correlationMetric_t *obj_metric;
  s_hp_distance_t objDist_1_toMany;
  s_hp_distance_t objDist_many_toMany;
  s_kt_matrix_base_t mat_preComputed;
  s_allAgainstAll_config_t config_metric_global;
  t_float (*metric_each_nonRank)(const config_nonRank_each_t);
} s_kd_searchConfig_t;

#define init__s_kd_searchConfig(self, vec_search, obj_result) ({s_kd_searchConfig_t obj; obj.conf = setToEmpty__s_kd_searchConfig_conf(); obj.self = self; obj.dim = self->mat_input->ncols; obj.row_1 = vec_search; obj.result = obj_result; obj.self_isOwnedByThis = false; init__fast_s_allAgainstAll_config(&(obj.config_metric_global)); obj;}) 

//! @returns a locally configured search-object, ie, which may be sued for muliple comptuation-calls.
s_kd_searchConfig_t init__fromConf__s_kd_searchConfig_t(s_kd_tree_t *self, s_kd_searchConfig_config_t conf, const s_kt_correlationMetric_t *obj_metric);

//! @returns a locally configured search-object, ie, which may be sued for muliple comptuation-calls.
static s_kd_searchConfig_t init__s_kd_searchConfig_t(const e_kd_tree_searchStrategy_t enum_id, s_kd_tree_t *self, const uint nn_numberOfNodes, const uint minDiff_indexRange_centerPoint, const t_float ballsize, const bool isTo_sort, const s_kt_correlationMetric_t *obj_metric) {
  //!
  s_kd_searchConfig_config_t conf = setToEmpty__s_kd_searchConfig_conf();
  conf.nn = nn_numberOfNodes;
  conf.correltime = minDiff_indexRange_centerPoint;
  conf.ballsize = ballsize;
  conf.isTo_sort = isTo_sort;
  conf.enum_id = enum_id;
  //  conf. = ;
  //!
  return init__fromConf__s_kd_searchConfig_t(self, conf, obj_metric);
}


/**
   @brief Allocate 'from sacratch' a kd-tree using a brute-force-appraoch.
   @param <mat_input> is the matrix to construct for.
   @param <nn_numberOfNodes> is the rank-trehshold to be used: for the latter we first sort the data and thereafter select the "nn_numberOfNodes" nubmer of best-scroign entites.
   @param <ballsize> is the value-trehshold to be used wrt. the high-scroing entites.
   @param <obj_metric> is the simlairty-emtric to be sued: if the default emtrics is sufficnet then intiate the object using "setTo_default__andReturn__s_kt_correlationMetric_t()".
   @return the new-cosntructed cofniguraiton-object.
**/
static s_kd_searchConfig_t init_brute__s_kd_searchConfig_t(const s_kt_matrix_t *mat_input, const uint nn_numberOfNodes, const t_float ballsize,  s_kt_correlationMetric_t obj_metric) {
  s_kd_tree_t self = __build_tree__s_kd_tree(mat_input, /*isTo_buildKD=*/false);
  if(isEmpty__s_kt_correlationMetric_t(&obj_metric)) {
    obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
  }
  s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &self, nn_numberOfNodes, UINT_MAX, ballsize, /*isTo_sort=*/false, &obj_metric);
  obj_search.self_isOwnedByThis = true;
  obj_search.self_base = self;
  //!
  //! @return
  return obj_search;
}
/**
   @brief Allocate 'from sacratch' a kd-tree using a kd-tree-default-appraoch.
   @param <mat_input> is the matrix to construct for.
   @param <nn_numberOfNodes> is the rank-trehshold to be used: for the latter we first sort the data and thereafter select the "nn_numberOfNodes" nubmer of best-scroign entites.
   @param <ballsize> is the value-trehshold to be used wrt. the high-scroing entites.
   @param <obj_metric> is the simlairty-emtric to be sued: if the default emtrics is sufficnet then intiate the object using "setTo_default__andReturn__s_kt_correlationMetric_t()".
   @return the new-cosntructed cofniguraiton-object.
**/
s_kd_searchConfig_t init_KD__s_kd_searchConfig_t(const s_kt_matrix_t *mat_input, const uint nn_numberOfNodes, const t_float ballsize, s_kt_correlationMetric_t obj_metric);

//! De-allcote the "s_kt_correlationMetric_t" object.
static void free__s_kd_searchConfig_t(s_kd_searchConfig_t *self) {
  assert(self);
  if(self->obj_metric) {
    free__s_hp_distance(&(self->objDist_1_toMany));
    free__s_hp_distance(&(self->objDist_many_toMany));
    self->obj_metric = NULL;
    free__s_kt_matrix_base_t(&(self->mat_preComputed));
  }
  if(self->self_isOwnedByThis) {
    free__s_kd_tree(&(self->self_base));
    self->self_isOwnedByThis = false;
  }
}

/**
   @brief constuct a sparse 2d-matrix from a KD-simlairty-metic-comtpatuion (oekseth, 06. aug. 2017).
   @param <mat_input> is the dense matrix to compteu for.
   @param <nn_numberOfNodes_min> is the minimum number of nodes for the data-set to be of interest: set to '0' or UINT_MAX to ingor ethis proieprty.
   @param <nn_numberOfNodes_max> is the max number of nodes to be used during data-sorting: set to UINT_MAX to ingore this proerpty-
   @param <ballsize_max> is the max-distance-size to be included: to to T_FLOAT_MAX to ingore this property
   @param <obj_metric> is the simalrity-emtric to be used.
   @return the 2d-sparse-list of pair-simalirty-metric
 **/
s_kt_list_2d_kvPair_t get_sparse2d_fromKD__kd_tree(const s_kt_matrix_t *mat_input, const uint nn_numberOfNodes_min, const uint nn_numberOfNodes_max, const t_float ballsize_max, const s_kt_correlationMetric_t obj_metric);

/**
   @enum e_kd_tree_typeOf_scoreUnification
   @brief unifies the scores (oesketh, 06. apr. 2018).
 **/
typedef  enum e_kd_tree_typeOf_scoreUnification {
  e_kd_tree_typeOf_scoreUnification_sum,
  e_kd_tree_typeOf_scoreUnification_undef
} e_kd_tree_typeOf_scoreUnification_t;
/**
   @brief constuct a sparse 2d-matrix from a KD-simlairty-metic-comtpatuion (oekseth, 06. apr. 2018).
   @param <mat_1> is the dense matrix to compteu for.
   @param <mat_2> is the dense matrix to compteu for.
   @param <nn_numberOfNodes_min> is the minimum number of nodes for the data-set to be of interest: set to '0' or UINT_MAX to ingor ethis proieprty.
   @param <nn_numberOfNodes_max> is the max number of nodes to be used during data-sorting: set to UINT_MAX to ingore this proerpty-
   @param <ballsize_max> is the max-distance-size to be included: to to T_FLOAT_MAX to ingore this property
   @param <obj_metric> is the simalrity-emtric to be used.
   @param <score_merge>  the strategy to merge scores
   @param <scalar_result>  is the result which is idneifed.
 **/
void get_score_fromKD__kd_tree(const s_kt_matrix_t *mat_1, const s_kt_matrix_t *mat_2, const uint nn_numberOfNodes_min, const uint nn_numberOfNodes_max, const t_float ballsize_max, const s_kt_correlationMetric_t obj_metric, e_kd_tree_typeOf_scoreUnification_t score_merge, t_float *scalar_result);

/**
   @brief The main-funciton for data-searhcing.
   @remarks wrt. the sci-kit-learn library and the "http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KDTree.html#sklearn.neighbors.KDTree.query_radius" interface:
   -- a call to "query(row_1, k=3)" corresponds to "s_kd_searchConfig_t searchObject = init_KD__s_kd_searchConfig_t(&mat_input, 3, T_FLOAT_MAX, setTo_default__andReturn__s_kt_correlationMetric_t()); s_kd_result result = init__s_kd_result_t(); searchNode__s_kd_tree_t(&searchObject, row_1, UINT_MAX, &result);
**/
void searchNode__s_kd_tree_t(s_kd_searchConfig_t *searchObject, const t_float *row_1, uint center_index, s_kd_result *result);



// void query__vertex__s_kd_tree_t(s_kd_tree_t *self, const e_kd_tree_searchStrategy_t enum_id, const t_float *row_1, const uint center_index, const uint nn_numberOfNodes, const uint minDiff_indexRange_centerPoint, const t_float ballsize, const bool isTo_sort, s_kt_list_1d_uint_t *obj_result);


//! Comptue DB-scan givent he spficion given in the cofnig-object and thereafter return a mapping between each vertex and the clsuter-id the vertes is assited to (oekseth, 06. jul. 2017).
s_kt_list_1d_uint_t dbScan__s_kd_searchConfig_t(s_kd_searchConfig_t *config, const uint config_minCnt);

/**
   @brief comptue clsuters through the DB-sCAN algroithm without applcaiton of cluster-strapping (oekseht, 06. aug. 2017).
   @param <obj_adjcency> is the sparse list of adjcency-verteices we use.
   @return the list of "list[vertex]=clust-id" memerbships
   @remarks 
 **/
s_kt_list_1d_uint_t search__relationsFromDataStruct__kd_tree(s_kt_list_2d_kvPair_t *obj_adjcency);

//! Use the deulfat hpLysis-disjoitn-set version (oekseht, 06. aug. 2017).
//! @rmeakr sin cotnast the to DB-sCAN-algorithm-verison this impemtatnion address shortfalls/erorrs which are commonly osb served: for detials please contat the sneior develoepr [oekseth@gmail.com].
static s_kt_list_1d_uint_t search__relationsFromDataStruct__altVersion__kd_tree(s_kt_list_2d_kvPair_t *obj_adjcency) {
  assert(obj_adjcency); assert(obj_adjcency->list_size);
  //!
  //! Map the object inopt a differnet format:
  uint max_vertex_id = 0;
  for(uint head_id = 0; head_id < obj_adjcency->list_size; head_id++) {
    const uint cnt_tails = obj_adjcency->list[head_id].list_size;
    if(cnt_tails > 0) {
      uint cnt_added = 0;
      for(uint k = 0; k < cnt_tails; k++) {
	const s_ktType_kvPair_t obj_this = obj_adjcency->list[head_id].list[k];
	if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {	
	  cnt_added++;
	  const uint tail_id = obj_this.head;
	  assert(tail_id != UINT_MAX); 
	  max_vertex_id = macro_max(max_vertex_id, tail_id);
	}
      }
      if(cnt_added > 0) { max_vertex_id = macro_max(max_vertex_id, head_id);} 
    }	
  }
  max_vertex_id++; //! ie, to get hte max-indecs-count
  //! 
  //! Allocate:
  s_kt_set_2dsparse_t listOf_relations_sparse = initAndReturn__allocate__s_kt_set_2dsparse_t(max_vertex_id, /*isTo_allocateFor_scores=*/false);
  //  printf("at %s:%d\n", __FILE__, __LINE__);
  //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  for(uint head_id = 0; head_id < obj_adjcency->list_size; head_id++) {
    const uint cnt_tails = obj_adjcency->list[head_id].list_size;
    //!
    //! Write out the clusters: 
    if(cnt_tails > 0) {
      uint cnt_added = 0;
      for(uint k = 0; k < cnt_tails; k++) {
	const s_ktType_kvPair_t obj_this = obj_adjcency->list[head_id].list[k];
	if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {	
	  const uint tail_id = obj_this.head;
	  assert(tail_id != UINT_MAX); 
	  //max_vertex_id = macro_max(max_vertex_id, tail_id);
	  assert(tail_id < max_vertex_id);
	  //! add:
	  // printf("at %s:%d\n", __FILE__, __LINE__);
	  push__s_kt_set_2dsparse_t(&listOf_relations_sparse, head_id, tail_id);
	  // printf("at %s:%d\n", __FILE__, __LINE__);
	  cnt_added++;
	}
      }
      if(cnt_added > 0) { max_vertex_id = macro_max(max_vertex_id, head_id);} 
    }	
  }
  //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  //  max_vertex_id++; //! ie, to get hte max-indecs-count
  //!
  //! Allcoate and itnate the disjoitn-set-algorithm: 
  s_kt_forest_findDisjoint_t obj_disj = get_empty__s_kt_forest_findDisjoint();
  allocate__s_kt_forest_findDisjoint(&obj_disj, /*nrows=*/max_vertex_id, &listOf_relations_sparse);
  graph_disjointForests__s_kt_forest_findDisjoint(&obj_disj, /*isTo_identifyCentrlaityVertex=*/false);

  //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //!
  //! Allocate result and iterate:
  s_kt_list_1d_uint_t obj_result = init__s_kt_list_1d_uint_t(max_vertex_id);
  for(uint row_id = 0; row_id < max_vertex_id; row_id++) {
    //! 
    const uint cluster_id = get_foreestId_forVertex__s_kt_forest_findDisjoint(&obj_disj, row_id);
    if(cluster_id != UINT_MAX) {
      set_scalar__s_kt_list_1d_uint_t(&obj_result, /*index=*/row_id, cluster_id);
    }
  }

  //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //! De-allocate:
  free_s_kt_forest_findDisjoint(&obj_disj);
  free_s_kt_set_2dsparse_t(&listOf_relations_sparse);

  //  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  
  //! 
  //! @return
  return obj_result;
}



#endif //! EOF
