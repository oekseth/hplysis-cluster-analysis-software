#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#define TS(d,i,j) (d).ts[(i)*((d).n)+(j)]
#define TL(d,i,j) (d).tl[(i)*((d).n)+(j)]

typedef struct {
    size_t n;
    double *tl, *ts;
} mdata_t;

int mdata_initf ( mdata_t *init, const char *filename );
int mdata_initz ( mdata_t *init, size_t n );
void mdata_sort ( mdata_t *out, mdata_t *in );
void mdata_finalize ( mdata_t *remove );
