#ifndef correlationType_proximity_altAlgortihmComputation_h
#define correlationType_proximity_altAlgortihmComputation_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlationType_proximity_altAlgortihmComputation
   @brief extends our "correlationType_proximity.h" header-file with an latenritve impelmetatnion-strategy for the proxmity-measures.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/


#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "s_kt_correlationConfig.h"
#include "correlation_macros__distanceMeasures_proximity.h"
#include "correlation_macros__distanceMeasures_proximity_altAlgortihmComputation.h"
#include "e_correlationType.h"


//! Compute the proxmity for a 2x-2x conteitncy-table value and a proximity_type
//! @remarks for details wr.t the proxmity-table see http://www.ibm.com/support/knowledgecenter/SSLVMB_20.0.0/com.ibm.spss.statistics.help/alg_proximities_binary.htm
t_float kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(uint sumOf_case_1a, uint sumOf_case_1b, uint sumOf_case_2a, uint sumOf_case_2b, const e_correlationType__t proximity_type);

/**
   @brief compute a proximity-metric score  between two rows or columns in a matrix.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set "0" if the value is missing.
   @remarks 
   - Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
   - if score-thresohlds are Not to be sued, then set scoreTrheashold_max = scoreTrheashold_min
 **/
t_float kt_proximity_altAlgortihmComputation(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self);


#endif //! EOF
