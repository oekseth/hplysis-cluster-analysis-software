#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "kt_list_1d.h"
#include "hp_distance.h"
// ***********************************************************
#include "kt_storage_resultOfClustering.h" //!  lgocis for summarizing core-attributes of the data, explroed across different sim-metrics
//#include "kt_distributionTypesC.h" //! which simplfies access to distrubion-types.
// ***********************************************************
// FIXME: resolve all FIXMEs in hp_frameWork_CCM_boundaries.h
// FIXME: make use of the core-logics in hp_frameWork_CCM_boundaries.h
#include "hp_frameWork_CCM_boundaries.h" // FIXME: update our aritlce with this.




/* typedef struct s_eval_ccm_defineSyntMatrix { */
/*   /\** */
/*      @brief configure the generatiniong of the sytentic matrix. */
/*    **\/ */
/*   const char *filePrefix; */
/*   uint nrows; */
/*   t_float score_weak; */
/*   t_float score_strong; */
/*   uint cnt_clusters; */
/* } s_eval_ccm_defineSyntMatrix_t; */

/* s_eval_ccm_defineSyntMatrix_t init_s_eval_ccm_defineSyntMatrix_t(const char *filePrefix, const uint nrows, const t_float score_weak, const t_float score_strong, const uint cnt_clusters) { */
/*   s_eval_ccm_defineSyntMatrix_t self; */
/*   //! */
/*   //! */
/*   self.filePrefix   = filePrefix; */
/*   self.nrows        = nrows; */
/*   self.score_weak   = score_weak; */
/*   self.cnt_clusters = cnt_clusters; */
/*   //! */
/*   return self; */
/* } */

  //! Specificy an 'itnernal' wrapper-matcro to export the reuslts:
#define __Mi__export(matrix, suffix) ({					\
      char str_conc[2000] = {'\0'}; sprintf(str_conc, "%s%s.tsv", str_filePath, suffix); \
      bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); \
      memset(str_conc, '\0', 2000); sprintf(str_conc, "%s%s.transposed.tsv", str_filePath, suffix); \
      /*! Then export a tranpsoed 'verison': */				\
      is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); })
  //! -------------------------------------------------------------------------

  //! Steps: a) set file-name, export, de-allocate
#define _export_tut16_ccm() ({						\
      allocOnStack__char__sprintf(2000, str, "%s-preStep-artificalData-n%u-c%u-%s-f%ut%u-dataId%u", self->filePrefix, self->nrows, self->cnt_clusters, str_topoType, (uint)self->score_weak, (uint)self->score_strong, data_id); \
      s_kt_matrix_t *mat_export = &(obj_shape.matrix);			\
      __unsafeWithSideEffects_export_matrixToPPM(&(obj_shape.matrix), str); \
      if(data_id > 0) {free__s_hp_clusterShapes_t(&obj_shape);} else {shape_ref = obj_shape; } })


static void eval_ccm_data(s_eval_ccm_defineSyntMatrix_t *self, const uint cnt_data_iterations,  const bool isTo_apply_postClustering) {
  
  printf("\t(info)\t Start evalauting for case=\"%s\", at %s:%d\n", self->filePrefix, __FILE__, __LINE__);
  const uint list_metrics_size = 4;
  const s_kt_correlationMetric_t list_metrics[list_metrics_size] = {
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank),
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)
  };
  const char *list_metrics_str[list_metrics_size] = {"Euclidan", "Pearson", "Spearman", "Kendall"};
  //! -----------------------------------
  //! ----------------------------------------------------------------------
  //!
  //! Allocate result-matrix:
  const uint config_cnt_casesToEvaluate = (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef + /*ccm-matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef + list_metrics_size;
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, /*cols=*/cnt_data_iterations);
  //!
  //!    
  //! Epxliclty itnaitlise the matrix:
  for(uint i = 0; i < mat_result.nrows; i++) {   for(uint k = 0; k < mat_result.ncols; k++) {mat_result.matrix[i][k] = T_FLOAT_MAX;}}
  //!
  //! Set the iteraiton-block-configuraiton:
  assert(cnt_data_iterations > 0);
  assert(cnt_data_iterations <= self->nrows);
  const uint block_increment_size = (uint)((t_float)self->nrows / (t_float)cnt_data_iterations); 
  assert(block_increment_size > 0);
  assert(block_increment_size < self->nrows);
  uint size_left = self->nrows - block_increment_size;
  uint size_right = self->nrows - size_left;
  //! Validate:
  assert(size_left > 0);
  assert(size_right > 0);
  assert(size_left <= self->nrows);
  assert(size_right  <= self->nrows);
  //!
  //!
  s_hp_clusterShapes_t shape_ref; // = NULL; //! ie, a comparison-basis. 
  for(uint data_id = 0; data_id < cnt_data_iterations; data_id++) {
    { //! Set column-label:
      char str_local[3000]; sprintf(str_local, "block-%u--%u", size_left, size_right);
      set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/data_id, str_local, /*addFor_column=*/true);
    }
    {      
      const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd100-twoNestedLinear";
      s_hp_clusterShapes_t obj_shape;
      if(true) {
	const e_eval_ccm_groupOf_artificialDataClass_t  enum_id = e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit;
	obj_shape = generateData_s_eval_ccm_defineSyntMatrix_t(self,enum_id, size_left, size_right);
      } else {
	//  FIXME: remove below code-chunk when we have dineitifed reasons for the 'lack of score-sepeaiotn' in "hp_frameWork_CCM_boundaries.h"
	obj_shape = init_andReturn__s_hp_clusterShapes_t(self->nrows, /*score_weak=*/self->score_weak, /*score_strong=*/self->score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	//! Construct for set=1:
	if(data_id > 0) {
	  assert(shape_ref.matrix.matrix != NULL);
	  //assert(shape_ref != NULL); //! as we expect the below step have set it.
	  //printf("#\t\t\tcompare shape(0)=%p verrsus shape(%d)=%p\n",  obj_shape.matrix.matrix , data_id, shape_ref.matrix.matrix);
	  assert(obj_shape.matrix.matrix != shape_ref.matrix.matrix); //! ie, as we asusem the objects are different.
	}
	bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_left, /*cnt_clusters=*/1, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
	assert(is_ok);
	// FIXME: include below:
	assert(self->cnt_clusters > 1);
	is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, size_right, /*cnt_clusters=*/(self->cnt_clusters -1), /*cnt_clusterStartOffset=*/1, /*vertexCount__constantToAdd0*/100);
	//is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /*cnt_clusterStartOffset=*/nrows/2, /*vertexCount__constantToAdd0*/100);
	assert(is_ok);
      }
      if(data_id > 0) {
	assert(shape_ref.matrix.matrix != NULL);
	//assert(shape_ref != NULL); //! as we expect the below step have set it.
	assert(obj_shape.matrix.matrix != shape_ref.matrix.matrix); //! ie, as we asusem the objects are different.
	//!
	//! ----------------------------------------------------------------------------------------------
	{ //! Perform the comparison:	
	  const uint internalIndex_best = data_id;
	  const uint internalIndex_worst = data_id;  
	  s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(shape_ref.matrix.nrows, shape_ref.matrix.ncols,shape_ref.matrix.matrix);
	  //!
	  uint case_id = 0;
	  { //! Apply for matrix-based: 
	    //!
	    //! Apply logics: compute the matrix-based CCMs:
	    s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	    bool is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, obj_shape.map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
	    assert(is_ok);    
	    assert(obj_result.list_size != 0);
	    assert(mat_result.nrows >= obj_result.list_size);
	    assert(obj_result.list_size <= (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	    //!
	    //! Insert results, updating the worst scores:
	    for(uint i = 0; i < obj_result.list_size; i++) {
	      //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
	      // for(uint i = 0; i < mat_result.ncols; i++) {
	      const t_float score = obj_result.list[i];
	      if(score != T_FLOAT_MAX) {
		//if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best])  || (score < mat_result.matrix[i+case_id][internalIndex_best]))
		{
		  mat_result.matrix[i+case_id][internalIndex_best] = score;
		  //mat_result_indexMax.matrix[i+case_id][internalIndex_best] = data_reference_index;	      
		}
		// printf("cmp: %f ?> %f, at %s:%d\n", score , mat_result.matrix[i+case_id][internalIndex_worst], __FILE__, __LINE__);
		//if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst]))
		{
		  //printf("[data:%u]\tbetter:\tcmp: %f ?> %f, at %s:%d\n", data_id, score , mat_result.matrix[i+case_id][internalIndex_worst], __FILE__, __LINE__);
		  mat_result.matrix[i+case_id][internalIndex_worst] = score;
		  //mat_result_indexMax.matrix[i+case_id][internalIndex_worst] = data_reference_index;	      
		}
	      }
	      { //! Then we 'insert' for the columns
		if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		  char str_local[3000]; sprintf(str_local, "%s", getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i));
		  set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		  //set_stringConst__s_kt_matrix(&mat_result_indexMax, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		}
	      }
	    }
	    //!
	    //! De-allocate:
	    free__s_kt_list_1d_float_t(&obj_result);
	    //! Increment:
	    case_id += (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
	  }
	  { //! Apply for gold-based: 
	    { //! Set-based CCMs: 
	      //!
	      //! Apply logics: compute the matrix-based CCMs:
	      s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_metric_undef);
	      if(true) {
		assert(obj_shape.matrix.matrix != shape_ref.matrix.matrix); //! ie, as we asusem the objects are different.
		//! Note: below calcualtes for both CCM-gold (eg, Rand's Index) and CCM-matrix (eg, Silhouette)
		bool is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(obj_shape.map_clusterMembers, 
								 shape_ref.map_clusterMembers, 
								 /*vertices=*/obj_shape.map_clusterMembers_size,
								 obj_shape.matrix.matrix,
								 shape_ref.matrix.matrix, 							   
								 &obj_result); //! defined in our "hp_ccm.h"
		//!
		assert(is_ok);    
		assert(obj_result.list_size != 0);
		assert(mat_result.nrows >= obj_result.list_size);
		assert(obj_result.list_size <= (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef);
		assert(is_ok);    
	      } /* else { */
	      /*   // FIXME: udpate: matrix-init */
	      /*   // FIXME: udpate: row-names */
	      /*   // FIXME: complete below: */
	      /*   is_ok = ccm__singleMatrix__completeSet__hp_ccm(obj_shape.map_clusterMembers,  */
	      /* 						     shape_ref.map_clusterMembers, &obj_result); */
	      /*   //! */
	      /*   assert(is_ok);     */
	      /*   assert(obj_result.list_size != 0); */
	      /*   assert(mat_result.nrows >= obj_result.list_size); */
	      /*   assert(obj_result.list_size <= (uint)/\*ccm-gold=*\/e_kt_matrix_cmpCluster_metric_undef); */
	      /* } */	      
	      assert(obj_result.list_size != 0);
	      assert(mat_result.nrows >= obj_result.list_size);
	      assert(obj_result.list_size <= (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef);
	      //!
	      //! Insert results, updating the worst scores:
	      for(uint i = 0; i < obj_result.list_size; i++) {
		//for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
		// for(uint i = 0; i < mat_result.ncols; i++) {
		const t_float score = obj_result.list[i];
		//if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best]) || (score < mat_result.matrix[i+case_id][internalIndex_best])) 
		{
		  mat_result.matrix[i+case_id][internalIndex_best] = score;
		  // mat_result_indexMax.matrix[i+case_id][internalIndex_best] = data_reference_index;	      
		}
		//if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst]))
		{
		  mat_result.matrix[i+case_id][internalIndex_worst] = score;
		  //mat_result_indexMax.matrix[i+case_id][internalIndex_worst] = data_reference_index;	      
		}
		{ //! Then we 'insert' for the columns
		  if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		    char str_local[3000]; sprintf(str_local, "%s", getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i));
		    set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		    // set_stringConst__s_kt_matrix(&mat_result_indexMax, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);		
		  }
		}
	      }
	      //!
	      //! De-allocate:
	      free__s_kt_list_1d_float_t(&obj_result);
	      //! Increment:
	      case_id += (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef;
	      assert(case_id <= mat_result.nrows);
	    }
	  }
	  { //! Vector-based CCMs: 
	    //!
	    //! Apply logics: compute the matrix-based CCMs:
	    //assert(is_ok);    
	    //!
	    //! Insert results, updating the worst scores:
	    for(uint i = 0; i < list_metrics_size; i++) {
	      //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
	      // for(uint i = 0; i < mat_result.ncols; i++) {
	      t_float score = applyAndReturn__uint_rows_hp_distance(list_metrics[i], obj_shape.map_clusterMembers, shape_ref.map_clusterMembers, shape_ref.map_clusterMembers_size); // &score, init__s_hp_distance__config_t());
	      //if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best]) || (score < mat_result.matrix[i+case_id][internalIndex_best])) 
	      {
		mat_result.matrix[i+case_id][internalIndex_best] = score;
		// mat_result_indexMax.matrix[i+case_id][internalIndex_best] = data_reference_index;	      
	      }
	      //if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) 
	      {
		mat_result.matrix[i+case_id][internalIndex_worst] = score;
		//mat_result_indexMax.matrix[i+case_id][internalIndex_worst] = data_reference_index;	      
	      }
	      { //! Then we 'insert' for the columns
		if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		  const char *str_local = list_metrics_str[i];
		  set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		  //set_stringConst__s_kt_matrix(&mat_result_indexMax, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		}
	      }
	    }
	    //!
	    //! Increment:
	    case_id += (uint)/*ccm-gold=*/list_metrics_size;
	    assert(case_id <= mat_result.nrows);
	  }	
	}
	//! ----------------------------------------------------------------------------------------------	
      }
      //! Export:
      _export_tut16_ccm();
      //!
      //! Adjust the size of the data:
      size_left -= block_increment_size;
      if( (size_left <= 0) || (size_left >= self->nrows) ) {size_left = 1;} //! ie, boundary-condtion.
      size_right = self->nrows - size_left;
      //! Validate:
      assert(size_left > 0);
      assert(size_right > 0);
      assert(size_left <= self->nrows);
      assert(size_right  <= self->nrows);
    }
  }

  //!
  //! Export results:
  const char *str_filePath = self->filePrefix;
  //!
  //! Write out the result-matrices, both 'as-is' and 'transposed':
  __Mi__export(&mat_result, "result__ccmDataId");
  //__Mi__export(&mat_result_indexMax, "result__dataIndexCCM");  
  { //! Investigate the impact ... adjsuted to the averaged score ... hencce, to see a scale-invariant change in prediciton.scores
    s_kt_matrix_t mat_cpy = initAndReturn__copy__normalized__s_kt_matrix_t(&mat_result);
    __Mi__export(&mat_cpy, "result__ccmDataIdAvgRowNormalized");
    //!
    free__s_kt_matrix(&mat_cpy);
  }

  if(isTo_apply_postClustering)   {//! Idea: analyse relationships between CCMs: to apply the clustering-tut-2-step to below:
    //! Note(hyptosis): the relationshisp between CCMs (eg, their agreemtns) is often used to assert cosnstisny in cluster-rpedicitons. Hence, if diffeent interpretaitons of groudn-truth influences this relationship, then ....??... ... which implies that use of co-occreunces in strenght (eg, between the CCMs of ) are assotied with a high degree of False Postives and False Negatives % FIXME: possible to quantify this assertion (eg, from the sumary-tables)?
    //! Note(hyptosis): ... is there a well-defined depencency ... does the dpeencey (between the metrics change)? ... If these restuls 
    //! 
    // TODO: apply the clustering-tut-2-step to below ... after first computing a covariance-matrix for matrix[0]x[data-this]
    const uint map_ncluster_size = 4; 
    const uint map_ncluster[map_ncluster_size] = {2, 4, 8, 16}; //! ie, increase the splitpoing
    for(uint m = 0; m < map_ncluster_size; m++) { //! Analsye prediciotns
      // TODO: set the cluster-count object dynamcially.
      // FIXME: export for differrent clsuter-count-props ... autoamte this ... 
      uint cluster_count = map_ncluster[m]; //! ie, the ncluster-option.
      //uint cluster_count = 2; //! ie, the ncluster-option.
      // FIXME: a comparison of cluster-count=$2,|distributiosn|/2$ reveals how ...??... % FIXME: udpate after we have completed our eval.
      //uint cluster_count = e_distributionTypesC_undef;
      s_kt_storage_resultOfClustering_t obj_cluster = initAndReturn_s_kt_storage_resultOfClustering_t(/*ncluster=k-cluster=*/cluster_count);
      s_kt_storage_resultOfClustering_t obj_cluster_transp = initAndReturn_s_kt_storage_resultOfClustering_t(/*ncluster=k-cluster=*/cluster_count);
      //! Note: export-result: transposed: <none>
      //const char *resultPrefix_local = filePrefix;
      allocOnStack__char__sprintf(2000, str, "%s-postCCM-artificalData-clustersInPostEval%u-c%u-f%ut%u.tsv", self->filePrefix, cluster_count, self->cnt_clusters, (uint)self->score_weak, (uint)self->score_strong);	  

      //allocOnStack__char__sprintf(2000, str, "%s-postCCM-n%u-%u-nclust%u", filePrefix, dimensions_stepSize, dimensions_stepSize*dimensions_cnt, cluster_count);
      //! Step: apply Clustering --- movation is  to compare the resemablance to the exepected outcome, and aim at explrining tis difference.
      //! Apply: 
      assert(obj_cluster.data.mat_clusterIds.nrows == 0); //! as we expect this matrix to be emtpy at this time-point.
      analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(mat_result, str, /*isTo_transpose=*/false, &obj_cluster, &obj_cluster_transp);
      //! -------------------------------------------------------------
      assert(obj_cluster.data.mat_clusterIds.matrix != obj_cluster_transp.data.mat_clusterIds.matrix);
      assert(obj_cluster_transp.data.mat_clusterIds.matrix != NULL);
      {
	//! Note: 
	//! Note: 
	//const char *resultPrefix_local = "result-3-postCCM-randomDistributions";
	allocOnStack__char__sprintf(2000, str, "%s-postCCMDept2-clustersInPostEval%u-c%u-f%ut%u.tsv", self->filePrefix, cluster_count, self->cnt_clusters, (uint)self->score_weak, (uint)self->score_strong);	  
	//allocOnStack__char__sprintf(2000, str, "%s-postCCMDept2-n%u-%u-nclust%u", filePrefix, dimensions_stepSize, dimensions_stepSize*dimensions_cnt, cluster_count);
	s_kt_storage_resultOfClustering_t l_obj_cluster = initAndReturn_s_kt_storage_resultOfClustering_t(/*k-cluster=*/2);
	s_kt_storage_resultOfClustering_t l_obj_cluster_transp = initAndReturn_s_kt_storage_resultOfClustering_t(/*k-cluster=*/2);
	//! Step: apply Hierarhcical Clustering --- movation is  to compare the resemablance to the exepected outcome, and aim at explrining tis difference.
	//! Apply: 
	assert(l_obj_cluster.data.mat_clusterIds.nrows == 0); //! as we expect this matrix to be emtpy at this time-point.
	analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(obj_cluster.data.mat_clusterIds, str, /*isTo_transpose=*/false, &l_obj_cluster, &l_obj_cluster_transp);
	// ---------------
	//! De-allocate:
	free__s_kt_storage_resultOfClustering_t(&l_obj_cluster);
	free__s_kt_storage_resultOfClustering_t(&l_obj_cluster_transp);    
      }
      // ---------------
      //! De-allocate:
      free__s_kt_storage_resultOfClustering_t(&obj_cluster);
      free__s_kt_storage_resultOfClustering_t(&obj_cluster_transp);    	  
    }
  }
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_result);

#undef _export_tut16_ccm
}



// *********************************************************' evalaute for a single gold-CCM:EOF

static void __apply_tut_16(const char *filePrefix, const uint nrows, const t_float score_weak, const t_float score_strong, const uint cnt_clusters, const bool isTo_exportArtificalData, const bool apply_postClustering) {
  printf("\t(info)\t Start evalauting for case=\"%s\", at %s:%d\n", filePrefix, __FILE__, __LINE__);
  const uint list_metrics_size = 4;
  const s_kt_correlationMetric_t list_metrics[list_metrics_size] = {
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank),
    initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)
  };
  const char *list_metrics_str[list_metrics_size] = {"Euclidan", "Pearson", "Spearman", "Kendall"};
  //! -----------------------------------
  //! ----------------------------------------------------------------------
  //!
  //! Allocate result-matrix:
  const uint config_cnt_casesToEvaluate = (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef + /*ccm-matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef + list_metrics_size;
  //  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  enum {
    c_name,
    //! --- 
    c_relative_ne_eq,
    c_eq_relative,
    c_ne_relative,
    //! 
    c_eq_worst,
    c_eq_best,
    c_ne_worst,
    c_ne_best,
    //! 
    c_size,
  };
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, /*cols=*/c_size);
  //!
  //!  
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_name, "Metric", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_relative_ne_eq, "nEqual / Equal", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_eq_relative, "Equal: Worst/Best", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_eq_best, "Equal: Score(best)", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_eq_worst, "Equal: Score(worst)", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_ne_relative, "Different: Worst/Best", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_ne_best, "Different: Score(best)", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result, /*index=*/c_ne_worst, "Different: Score(worst)", /*addFor_column=*/true);      
  //! Epxliclty itnaitlise the matrix:
  for(uint i = 0; i < mat_result.nrows; i++) {   for(uint k = 0; k < mat_result.ncols; k++) {mat_result.matrix[i][k] = T_FLOAT_MAX;}}
  //!
  //! ---------------------------
  /* s_kt_matrix_t mat_result_indexMin = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, /\*cols=*\/c_size); */
  /* //! */
  /* //!   */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_name, "Metric", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_relative_ne_eq, "nEqual / Equal", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_eq_relative, "Equal: Worst/Best", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_eq_best, "Equal: Score(best)", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_eq_worst, "Equal: Score(worst)", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_ne_relative, "Different: Worst/Best", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_ne_best, "Different: Score(best)", /\*addFor_column=*\/true);       */
  /* set_stringConst__s_kt_matrix(&mat_result_indexMin, /\*index=*\/c_ne_worst, "Different: Score(worst)", /\*addFor_column=*\/true);       */
  /* //! Epxliclty itnaitlise the matrix: */
  /* for(uint i = 0; i < mat_result_indexMin.nrows; i++) {   for(uint k = 0; k < mat_result_indexMin.ncols; k++) {mat_result_indexMin.matrix[i][k] = T_FLOAT_MAX;}} */
  //!
  s_kt_matrix_t mat_result_indexMax = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, /*cols=*/c_size);
  //!
  //!  
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_name, "Metric", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_relative_ne_eq, "nEqual / Equal", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_eq_relative, "Equal: Worst/Best", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_eq_best, "Equal: Score(best)", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_eq_worst, "Equal: Score(worst)", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_ne_relative, "Different: Worst/Best", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_ne_best, "Different: Score(best)", /*addFor_column=*/true);      
  set_stringConst__s_kt_matrix(&mat_result_indexMax, /*index=*/c_ne_worst, "Different: Score(worst)", /*addFor_column=*/true);      
  //! Epxliclty itnaitlise the matrix:
  for(uint i = 0; i < mat_result_indexMax.nrows; i++) {   for(uint k = 0; k < mat_result_indexMax.ncols; k++) {mat_result_indexMax.matrix[i][k] = T_FLOAT_MAX;}}
  //!  

  long unsigned int data_reference_index = 0;

  //! Iterate through: data-cases:
  for(uint data_id = 0; data_id < 4; data_id++, data_reference_index++) {
    //! Set the indexes to update:
    // FIXME: valdiate tha assumption that all data-topologeis NOT matching the optmsal data-set is seen as 'wrost'.
    const uint internalIndex_best  = (data_id == 0) ? c_eq_best  : c_ne_best;
    const uint internalIndex_worst = (data_id == 0) ? c_eq_worst : c_ne_worst;
    assert(internalIndex_worst != internalIndex_best);
    //!
    //! Explroe differnet hypo-data-ids: 
    for(uint hypo_id = 0; hypo_id < 2; hypo_id++, data_reference_index++) {
      //    for(uint hypo_id = 0; hypo_id < 3; hypo_id++) {
      //!
      //! 
      //! Select the hypothesis:
      bool is_ok = true;
      s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      //!
      s_hp_clusterShapes_t obj_shape_2  = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      //! Construct for set=1:
      is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
      assert(is_ok);	 
      if(isTo_exportArtificalData && (hypo_id == 0) ) { //! then export:
	allocOnStack__char__sprintf(2000, str, "%s-artificalData-n%u-dataCase%u-f%ut%u.tsv", filePrefix, nrows, data_id, (uint)score_weak, (uint)score_strong);	  
	const bool is_ok = export__singleCall__s_kt_matrix_t(&(obj_shape.matrix), str, NULL); assert(is_ok);
      }
      if(data_id == 0) { //! then the two sets should be equal: 
	is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	assert(is_ok);	
      } else if(data_id == 1) { //! then all vertices are in the same cluster:
	is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, /*cnt_clusters=*/1, /*cnt_clusterStartOffset=*/0);
	assert(is_ok);
      } else if(data_id == 2) { //! then all vertices are in the same cluster:
	is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, /*cnt_clusters=*/(uint)((t_float)nrows/2), /*cnt_clusterStartOffset=*/0);
	assert(is_ok);
      } else if(data_id == 3) { //! then all vertices are in the same cluster:
	is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, /*cnt_clusters=*/(uint)((t_float)nrows/4), /*cnt_clusterStartOffset=*/0);
	assert(is_ok);		
      } else {assert(false);} //! ie, as we then need adding support for this case.
      //! Construct the comparison-basis:
      if(hypo_id == 0) { //! then we do noting, ie, leve the clusters as-is.
      } else if(hypo_id == 1) {	//! then we change the cluster-memberships:
	for(uint k = 0; k < obj_shape_2.map_clusterMembers_size; k++) {
	  assert(obj_shape_2.map_clusterMembers[k] != UINT_MAX);
	  obj_shape_2.map_clusterMembers[k] *= 5; //! ie, change the cluster-ids (while keeping the grousp unchanged, ie, as the id-change is cosnitent for all groups).
	}
	//} else if(hypo_id == 2) {	
      } else {assert(false);} //! ie, as we then need adding support for this case.
      //! ---------------------------------
      //! 
      //! Apply measurements:
      assert(obj_shape.map_clusterMembers);
      assert(obj_shape_2.map_clusterMembers);
      s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_shape_2.matrix.nrows, obj_shape_2.matrix.ncols, obj_shape_2.matrix.matrix);      
      uint case_id = 0;
      { //! Apply for matrix-based: 
	//!
	//! Apply logics: compute the matrix-based CCMs:
	s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, obj_shape.map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
	assert(is_ok);    
	assert(obj_result.list_size != 0);
	assert(mat_result.nrows >= obj_result.list_size);
	assert(obj_result.list_size <= (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	//!
	//! Insert results, updating the worst scores:
	for(uint i = 0; i < obj_result.list_size; i++) {
	  //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
	  // for(uint i = 0; i < mat_result.ncols; i++) {
	  const t_float score = obj_result.list[i];
	  if(score != T_FLOAT_MAX) {
	    if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best])  || (score < mat_result.matrix[i+case_id][internalIndex_best])) {
	      mat_result.matrix[i+case_id][internalIndex_best] = score;
	      mat_result_indexMax.matrix[i+case_id][internalIndex_best] = data_reference_index;	      
	    }
	    // printf("cmp: %f ?> %f, at %s:%d\n", score , mat_result.matrix[i+case_id][internalIndex_worst], __FILE__, __LINE__);
	    if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) {
	      //printf("[data:%u]\tbetter:\tcmp: %f ?> %f, at %s:%d\n", data_id, score , mat_result.matrix[i+case_id][internalIndex_worst], __FILE__, __LINE__);
	      mat_result.matrix[i+case_id][internalIndex_worst] = score;
	      mat_result_indexMax.matrix[i+case_id][internalIndex_worst] = data_reference_index;	      
	    }
	  }
	  { //! Then we 'insert' for the columns
	    if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
	      char str_local[3000]; sprintf(str_local, "%s", getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i));
	      set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
	      set_stringConst__s_kt_matrix(&mat_result_indexMax, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
	    }
	  }
	}
	//!
	//! De-allocate:
	free__s_kt_list_1d_float_t(&obj_result);
	//! Increment:
	case_id += (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
      }
      { //! Apply for gold-based: 
	{ //! Set-based CCMs: 
	  //!
	  //! Apply logics: compute the matrix-based CCMs:
	  s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_metric_undef);
	  if(true) {
	    //! Note: below calcualtes for both CCM-gold (eg, Rand's Index) and CCM-matrix (eg, Silhouette)
	    is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(obj_shape.map_clusterMembers, 
							     obj_shape_2.map_clusterMembers, 
							     /*vertices=*/obj_shape.map_clusterMembers_size,
							     obj_shape.matrix.matrix,
							     obj_shape_2.matrix.matrix, 							   
							     &obj_result); //! defined in our "hp_ccm.h"
	    //!
	    assert(is_ok);    
	    assert(obj_result.list_size != 0);
	    assert(mat_result.nrows >= obj_result.list_size);
	    assert(obj_result.list_size <= (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef);
	  } /* else { */
	  /*   // FIXME: udpate: matrix-init */
	  /*   // FIXME: udpate: row-names */
	  /*   // FIXME: complete below: */
	  /*   is_ok = ccm__singleMatrix__completeSet__hp_ccm(obj_shape.map_clusterMembers,  */
	  /* 						     obj_shape_2.map_clusterMembers, &obj_result); */
	  /*   //! */
	  /*   assert(is_ok);     */
	  /*   assert(obj_result.list_size != 0); */
	  /*   assert(mat_result.nrows >= obj_result.list_size); */
	  /*   assert(obj_result.list_size <= (uint)/\*ccm-gold=*\/e_kt_matrix_cmpCluster_metric_undef); */
	  /* } */
	  assert(is_ok);    
	  assert(obj_result.list_size != 0);
	  assert(mat_result.nrows >= obj_result.list_size);
	  assert(obj_result.list_size <= (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef);
	  //!
	  //! Insert results, updating the worst scores:
	  for(uint i = 0; i < obj_result.list_size; i++) {
	    //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
	    // for(uint i = 0; i < mat_result.ncols; i++) {
	    const t_float score = obj_result.list[i];
	    if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best]) || (score < mat_result.matrix[i+case_id][internalIndex_best])) {
	      mat_result.matrix[i+case_id][internalIndex_best] = score;
	      mat_result_indexMax.matrix[i+case_id][internalIndex_best] = data_reference_index;	      
	    }
	    if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) {
	      mat_result.matrix[i+case_id][internalIndex_worst] = score;
	      mat_result_indexMax.matrix[i+case_id][internalIndex_worst] = data_reference_index;	      
	    }
	    { //! Then we 'insert' for the columns
	      if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		char str_local[3000]; sprintf(str_local, "%s", getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i));
		set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		set_stringConst__s_kt_matrix(&mat_result_indexMax, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);		
	      }
	    }
	  }
	  //!
	  //! De-allocate:
	  free__s_kt_list_1d_float_t(&obj_result);
	  //! Increment:
	  case_id += (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef;
	  assert(case_id <= mat_result.nrows);
	}
	{ //! Vector-based CCMs: 
	  //!
	  //! Apply logics: compute the matrix-based CCMs:
	  assert(is_ok);    
	  //!
	  //! Insert results, updating the worst scores:
	  for(uint i = 0; i < list_metrics_size; i++) {
	    //for(uint i = 0; i < (uint)e_kt_matrix_cmpCluster_metric_undef; i++) {
	    // for(uint i = 0; i < mat_result.ncols; i++) {
	    t_float score = applyAndReturn__uint_rows_hp_distance(list_metrics[i], obj_shape.map_clusterMembers, obj_shape_2.map_clusterMembers, obj_shape_2.map_clusterMembers_size); // &score, init__s_hp_distance__config_t());
	    if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_best]) || (score < mat_result.matrix[i+case_id][internalIndex_best])) {
	      mat_result.matrix[i+case_id][internalIndex_best] = score;
	      mat_result_indexMax.matrix[i+case_id][internalIndex_best] = data_reference_index;	      
	    }
	    if((T_FLOAT_MAX == mat_result.matrix[i+case_id][internalIndex_worst]) || (score > mat_result.matrix[i+case_id][internalIndex_worst])) {
	      mat_result.matrix[i+case_id][internalIndex_worst] = score;
	      mat_result_indexMax.matrix[i+case_id][internalIndex_worst] = data_reference_index;	      
	    }
	    { //! Then we 'insert' for the columns
	      if(getAndReturn_string__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, /*addFor_column=*/false) == NULL) { //! then set the row-string: 
		const char *str_local = list_metrics_str[i];
		set_stringConst__s_kt_matrix(&mat_result, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
		set_stringConst__s_kt_matrix(&mat_result_indexMax, /*col_id=*/i+case_id, str_local, /*addFor_column=*/false);
	      }
	    }
	  }
	  //!
	  //! Increment:
	  case_id += (uint)/*ccm-gold=*/list_metrics_size;
	  assert(case_id <= mat_result.nrows);
	}	
	
      }
      //! ----------------------------------------
      if(isTo_exportArtificalData && (data_id == 0) && (hypo_id == 0) ) { //! then export:
	s_kt_matrix_t *mat_export = &(obj_shape.matrix); //! ie, a short-hand.
	//!
	allocOnStack__char__sprintf(2000, str, "%s-artificalData-n%u-%s-f%ut%u", filePrefix, nrows, "minInsideMaxOutside", (uint)score_weak, (uint)score_strong);	  
	//allocOnStack__char__sprintf(2000, str, "%s-artificalData-n%u-%s-f%ut%u.tsv", filePrefix, nrows, "minInsideMaxOutside", (uint)score_weak, (uint)score_strong);	  
	__unsafeWithSideEffects_export_matrixToPPM(&(obj_shape.matrix), str);
      }
      //! ----------------------------------------

      //!
      //! De-allocate:
      free__s_hp_clusterShapes_t(&obj_shape);      
      free__s_hp_clusterShapes_t(&obj_shape_2);      
    }
  }
  //!
  //! Compute the relative scores
#define use(col) ({ (mat_result.matrix[i][col]  != 0) && (mat_result.matrix[i][col] != T_FLOAT_MAX);})
  for(uint i = 0; i < mat_result.nrows; i++) {
    if(use(c_ne_best) && use(c_eq_best)) {
      mat_result.matrix[i][c_relative_ne_eq] = mat_result.matrix[i][c_ne_best] / mat_result.matrix[i][c_eq_best];
    }
    if(use(c_eq_best) && use(c_eq_worst)) {
      mat_result.matrix[i][c_eq_relative] = mat_result.matrix[i][c_eq_worst] / mat_result.matrix[i][c_eq_best];
    }
    if(use(c_ne_best) && use(c_ne_worst)) {
      mat_result.matrix[i][c_ne_relative] = mat_result.matrix[i][c_ne_worst] / mat_result.matrix[i][c_ne_best];
    }    
  }
#undef use    
  //! -------------------------------------------------------------------------
  //!
  //! Prepare data-export:
  /* char str_filePath[2000] = {'\0'};  */
  /* if(stringOf_resultDir && strlen(stringOf_resultDir)) { */
  /*   if(stringOf_resultDir[strlen(stringOf_resultDir)-1] == '/') { */
  /*     sprintf(str_filePath, "%s", stringOf_resultDir); */
  /*   } else {sprintf(str_filePath, "%s/", stringOf_resultDir);} */
  /* }   */
  const char *str_filePath = filePrefix;
  //!
  //! Write out the result-matrices, both 'as-is' and 'transposed':
  __Mi__export(&mat_result, "result__ccm");
  __Mi__export(&mat_result_indexMax, "result__dataIndexCCM");  

  if(false) // FIXME: remove!  
  {//! Idea: analyse relationships between CCMs: to apply the clustering-tut-2-step to below:
    //! Note(hyptosis): the relationshisp between CCMs (eg, their agreemtns) is often used to assert cosnstisny in cluster-rpedicitons. Hence, if diffeent interpretaitons of groudn-truth influences this relationship, then ....??... ... which implies that use of co-occreunces in strenght (eg, between the CCMs of ) are assotied with a high degree of False Postives and False Negatives % FIXME: possible to quantify this assertion (eg, from the sumary-tables)?
    //! Note(hyptosis): ... is there a well-defined depencency ... does the dpeencey (between the metrics change)? ... If these restuls 
    //! 
    // TODO: apply the clustering-tut-2-step to below ... after first computing a covariance-matrix for matrix[0]x[data-this]
    const uint map_ncluster_size = 4; 
    const uint map_ncluster[map_ncluster_size] = {2, 4, 8, 16}; //! ie, increase the splitpoing
    for(uint m = 0; m < map_ncluster_size; m++) { //! Analsye prediciotns
      // TODO: set the cluster-count object dynamcially.
      // FIXME: export for differrent clsuter-count-props ... autoamte this ... 
      uint cluster_count = map_ncluster[m]; //! ie, the ncluster-option.
      //uint cluster_count = 2; //! ie, the ncluster-option.
      // FIXME: a comparison of cluster-count=$2,|distributiosn|/2$ reveals how ...??... % FIXME: udpate after we have completed our eval.
      //uint cluster_count = e_distributionTypesC_undef;
      s_kt_storage_resultOfClustering_t obj_cluster = initAndReturn_s_kt_storage_resultOfClustering_t(/*ncluster=k-cluster=*/cluster_count);
      s_kt_storage_resultOfClustering_t obj_cluster_transp = initAndReturn_s_kt_storage_resultOfClustering_t(/*ncluster=k-cluster=*/cluster_count);
      //! Note: export-result: transposed: <none>
      //const char *resultPrefix_local = filePrefix;
      allocOnStack__char__sprintf(2000, str, "%s-postCCM-artificalData-clustersInPostEval%u-c%u-f%ut%u.tsv", filePrefix, cluster_count, cnt_clusters, (uint)score_weak, (uint)score_strong);	  

      //allocOnStack__char__sprintf(2000, str, "%s-postCCM-n%u-%u-nclust%u", filePrefix, dimensions_stepSize, dimensions_stepSize*dimensions_cnt, cluster_count);
      //! Step: apply Clustering --- movation is  to compare the resemablance to the exepected outcome, and aim at explrining tis difference.
      //! Apply: 
      assert(obj_cluster.data.mat_clusterIds.nrows == 0); //! as we expect this matrix to be emtpy at this time-point.
      analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(mat_result, str, /*isTo_transpose=*/false, &obj_cluster, &obj_cluster_transp);
      //! -------------------------------------------------------------
      assert(obj_cluster.data.mat_clusterIds.matrix != obj_cluster_transp.data.mat_clusterIds.matrix);
      assert(obj_cluster_transp.data.mat_clusterIds.matrix != NULL);
      {
	//! Note: 
	//! Note: 
	//const char *resultPrefix_local = "result-3-postCCM-randomDistributions";
	allocOnStack__char__sprintf(2000, str, "%s-postCCMDept2-clustersInPostEval%u-c%u-f%ut%u.tsv", filePrefix, cluster_count, cnt_clusters, (uint)score_weak, (uint)score_strong);	  
	//allocOnStack__char__sprintf(2000, str, "%s-postCCMDept2-n%u-%u-nclust%u", filePrefix, dimensions_stepSize, dimensions_stepSize*dimensions_cnt, cluster_count);
	s_kt_storage_resultOfClustering_t l_obj_cluster = initAndReturn_s_kt_storage_resultOfClustering_t(/*k-cluster=*/2);
	s_kt_storage_resultOfClustering_t l_obj_cluster_transp = initAndReturn_s_kt_storage_resultOfClustering_t(/*k-cluster=*/2);
	//! Step: apply Hierarhcical Clustering --- movation is  to compare the resemablance to the exepected outcome, and aim at explrining tis difference.
	//! Apply: 
	assert(l_obj_cluster.data.mat_clusterIds.nrows == 0); //! as we expect this matrix to be emtpy at this time-point.
	analyseRelationshipsInCoVarianceMatrix_kt_storage_resultOfClustering(obj_cluster.data.mat_clusterIds, str, /*isTo_transpose=*/false, &l_obj_cluster, &l_obj_cluster_transp);
	// ---------------
	//! De-allocate:
	free__s_kt_storage_resultOfClustering_t(&l_obj_cluster);
	free__s_kt_storage_resultOfClustering_t(&l_obj_cluster_transp);    
      }
      // ---------------
      //! De-allocate:
      free__s_kt_storage_resultOfClustering_t(&obj_cluster);
      free__s_kt_storage_resultOfClustering_t(&obj_cluster_transp);    	  
    }
  }
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_result);
  free__s_kt_matrix(&mat_result_indexMax);  
  // free__s_hp_clusterShapes_t(&obj_shape);
}


static void _depictExtremeArtificalData(const char *filePrefix) {

  // FIXME: add support+apply for: CCM-eval
  // FIXME: add support+apply for: sim-pre + CCM-eval
  // FIXME: add support+apply for: algo ..

  // FIXME: what do we store in the result-sets? ... how do we dineitfy TPs and TNs ...?? <--- if this test instead focus on the min--max seperationb betweeb different CCMs?
  
  //! Steps: a) set file-name, export, de-allocate
#define _export_tut16() ({						\
      allocOnStack__char__sprintf(2000, str, "%s-preStep-artificalData-n%u-c%u-%s-f%ut%u", filePrefix, nrows, cnt_clusters, str_topoType, (uint)score_weak, (uint)score_strong); \
      s_kt_matrix_t *mat_export = &(obj_shape.matrix);			\
      __unsafeWithSideEffects_export_matrixToPPM(&(obj_shape.matrix), str); \
      free__s_hp_clusterShapes_t(&obj_shape); })


  // FIXME: add something!
  
  //const uint weights_map_size = 1;
  const uint weights_map_size = 4;
  // FIXME: paper: is it true that 'strong scores' should be higher? ... how does this relationship/understanding relate to simialrty-meitrccs? ... 
  // FIXME: correlate below into a seperate matrix.
  // FIXME: upddate paper with below results.
  const uint weights_map[weights_map_size][2] = {
    {100,1},{100, 25},{100, 50}, 
    {100, 99}
    //{99, 100}
						 //{95, 100}
}; //! where the ",{100, 95},{95, 100}" is used to explreo the effects oc ases where there is hanrdly any sepration, as seen for ases where Eculdidena simlairty faind it challengign to handle otuliers (as exemplfied in resutls generate from our "tut-2" results).
  //const uint featureDims_stepSize = 100; //! ie, which is used to consturc the number of piitns in question
  //const uint featureDims_iterations = 10;
  //! ----
  //for(uint nrows_index = 0; nrows_index < featureDims_iterations; nrows_index++) {
  //! Method: Consturct feature-matrix of dimeions, and then analyse their itnernal simliarites.
  const uint nrows = 1000;
  const uint map_cluster_size = 5;
  const uint map_cluster[map_cluster_size] = {2, 4, 8, 16, 32};
  for(uint cluster_id = 0; cluster_id < map_cluster_size; cluster_id++) {
    const uint cnt_clusters = map_cluster[cluster_id];
    //! 
    for(uint weight_index = 0; weight_index < weights_map_size; weight_index++) {
      const t_float score_weak   = weights_map[weight_index][0];
      const t_float score_strong = weights_map[weight_index][1];
      //const uint nrows = (nrows_index +1) * featureDims_stepSize;    
      /* { //! then export: */
      /* 	const char *str_topoType = "sameScore"; */
      /* 	//__generateArtificalData_export(filePrefix, str_topoType, nrows, score_weak, score_strong */
      /* 	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /\*score_weak=*\/score_weak, /\*score_strong=*\/score_strong, /\*typeOf_vertexOrder=*\/e_hp_clusterShapes_vertexOrder_linear, /\*typeOf_scoreAssignment=*\/e_hp_clusterShapes_scoreAssignment_sameScore); */
      /* 	//! Construct for set=1: */
      /* 	const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /\*cnt_clusterStartOffset=*\/0); */
      /* 	assert(is_ok);	  */
      /* 	//! Export:  */
      /* 	 _export_tut16(); */
      /* } */
      { //! then export:
	const char *str_topoType = "minInsideMaxOutside";
	//__generateArtificalData_export(filePrefix, str_topoType, nrows, score_weak, score_strong
	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	//! Construct for set=1:
	const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	assert(is_ok);	 
	//! Export: 
	 _export_tut16();
      }      
      { //! then export:
      	const char *str_topoType = "minInsideMaxOutsideDecreasedScoreForSmallerClusters";
      	//__generateArtificalData_export(filePrefix, str_topoType, nrows, score_weak, score_strong
      	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters);
      	//! Construct for set=1:
      	const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
      	assert(is_ok);
      	//! Export:
      	 _export_tut16();
      }
      //! -----------------------------------
      { //! then export:
      	const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd1";
      	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      	//! Construct for set=1:
      	const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/1);
      	assert(is_ok);
      	//! Export:
      	 _export_tut16();
      }
      //! -----------------------------------
      { //! then export:
      	const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd100";
      	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      	//! Construct for set=1:
      	const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
      	assert(is_ok);
      	//! Export:
      	 _export_tut16();
      }
      { //! then export:
      	const char *str_topoType = "minInsideMaxOutsideLinearChanging-constantToAdd100-twoNestedLinear";
      	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      	//! Construct for set=1:
      	bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters/2, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
      	assert(is_ok);
      	// FIXME: include below:
	is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /*cnt_clusterStartOffset=*/cnt_clusters/2, /*vertexCount__constantToAdd0*/100);
	//is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /*cnt_clusterStartOffset=*/nrows/2, /*vertexCount__constantToAdd0*/100);
      	assert(is_ok);
      	//! Export:
      	 _export_tut16();
      }
      { //! then export:
      	const char *str_topoType = "minInsideMaxOutsideReverseChanging-constantToAdd100-twoNestedLinear";
      	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
      	//! Construct for set=1:
      	bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters/2, /*cnt_clusterStartOffset=*/0, /*vertexCount__constantToAdd0*/100);
      	assert(is_ok);
      	// FIXME: include below:
	is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters/2, /*cnt_clusterStartOffset=*/cnt_clusters/2, /*vertexCount__constantToAdd0*/100);
	//is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /*cnt_clusterStartOffset=*/nrows/2, /*vertexCount__constantToAdd0*/100);
      	assert(is_ok);
      	//! Export:
      	 _export_tut16();
      }
      /* { //! then export: */
      /* 	const char *str_topoType = "minInsideMaxOutsideRandomChanging-constantToAdd100-twoNestedLinear"; */
      /* 	s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /\*score_weak=*\/score_weak, /\*score_strong=*\/score_strong, /\*typeOf_vertexOrder=*\/e_hp_clusterShapes_vertexOrder_random, /\*typeOf_scoreAssignment=*\/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside); */
      /* 	//! Construct for set=1: */
      /* 	bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /\*cnt_clusterStartOffset=*\/0, /\*vertexCount__constantToAdd0*\/100); */
      /* 	assert(is_ok); */
      /* 	// FIXME: include below: */
      /* 	is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(&obj_shape, nrows/2, cnt_clusters, /\*cnt_clusterStartOffset=*\/nrows/2, /\*vertexCount__constantToAdd0*\/100); */
      /* 	assert(is_ok); */
      /* 	//! Export: */
      /* 	 _export_tut16(); */
      /* } */

      // FIXME: differnet eprmtautiosn of: 	      is_ok = buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, cnt_clusters_fixedToAdd, enum_id, /*cnt_seperateCalls=*/1, false, true, 2);
    }
  }
#undef _export_tut16
}

/**
   @brief caputre similiarty of CCMS wrt. the cases of 'cluster-predications are equal' versus 'cluster-rpedictions are Not equal'.
   @author Ole Kristian Ekseth (oekseth, 26. Dec. 2018).
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
   @remarks compile:
   -- g++ -O2 -g tut_ccm_16_compareMatrixAndGold_twoVectors.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_16.exe
**/
int main() 
#else
  static void apply_tut_ccm_16_compareMatrixAndGold_twoVectors(const char *filePrefix) //, const uint config_cnt_casesToEvaluate, const uint config_nrows_base, const uint config_clusterBase, const bool isTo_useRverseOrder_inInit, const bool isTo_setClusterSzei_toFixed_sizes, const t_float score_weak)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const char *filePrefix = "r-16";
  //  const char *filePrefix = "ccm_16_compareMatrixAndGold_twoVectors";
#endif


  //if(false)
  { //! Then call "hp_frameWork_CCM_boundaries.h"  ... use ARI-config ... <--- should we use a range of configmin--max-props?
    {
      // FIXME: ... aritcle ... construct a dot-plot of clusters in/from one data-ensamble ... use this eto explain difference beween CCM-gold verrsus CCM-matrix ... for this task, generate a radial plto ..
      // FIXME: cpcneptual: ... how may raidal plots be used to explain/elboarte other/lreated concepts?
      // FIXME: cpcneptual: ...
      // FIXME: cpcneptual: ...       
      // FIXME: ... code: ... 
    }
    // FIXME: ...
    static_tut_helloWorld();
  }


    
  
  { //! Export: a test for exploring linear relationshisp for a topology where only change is between two cluster-regions.
    //! Note: A motivation (for this meausrement) is to a) validate that CCMs are not erronlys implemented (ie, that they manage to give aprpxmaite clasisficoant of ddata), and b) iinvestigate the lineary, adn reares of sentiviety, for each CCM:
    //! Note:
    {
      //! Note:
      //! Note (paremter-exploration):      
      //! Note (paremter-exploration):      
      //! Note (paremter-exploration): ... motiation is to ... 
      // -------------
      // FIXME: for-loop: define: sim-metric-pre-step , none
      // FIXME: for-loop: define: alg-basic-config , none
      // FIXME: for-loop: define: 
      // FIXME: for-loop:       
      //! Note(results):
      //! Note(results):
      //! Note(results): framwork: ...
      //! Note(results): framwork: ...       
      //! Note(results): improtance of testing lienary of CCMs ... 
      //! Note(results): ... permtautions of van-Dogen decreases its prediciotn-accucrayc, hence ... 
      //! Note(results):  ... seems linear for the CCMs ...            
      // FIXME: investigate the impact of the "cnt_data_iterations" config-param.
      const uint cnt_data_iterations = 20;
      //!      
      //! Note: based on our other/related meusrements, we have that below cofnigrusions should results in a high seperation between CCMs (hence, the repsrestniveness of belwo cofniuraiton):
      const uint cnt_clusters = 2;
      const uint nrows = 100;
      //const t_float score_strong = 100;      const t_float score_weak = 1;
      const t_float score_strong = 1;      const t_float score_weak = 100;
      //! Apply logcs:
      const bool isTo_exportArtificalData = false;
      const bool apply_postClustering = true;
      //! 
      //! Configure:
      allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix, "linearTestCase", nrows, cnt_clusters, (uint)score_weak, (uint)score_strong, cnt_data_iterations);	  
      s_eval_ccm_defineSyntMatrix_t self = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters);
      //!
      //! Apply:
      eval_ccm_data(&self, cnt_data_iterations, /*isTo_apply_postClustering=*/false);
    }
    assert(false); // FIXME: remove. 
  }
  
  // -------------
  // OK: FIXME: updating name-mapping-table in "" ... to be capble handlign names in/from "fig-tut-16-MinMaxEachCell-boundaries.tsv" <-- dropped
  // FIXME: device a data-structure ... meaking it efficnet ... this comparison ... each prop hodls a struct ... based on an enum ... in a matrix-object ... 
  // FIXME: update "kt_matrix" with a new function ... find a row using a slow procedure ... 
  // FIXME: load the "-boundaries.tsv" file, ie, if specifed/found 
  // FIXME: if set ... test memberships of each predicted case ... updating a global counter-object  .... 
  // FIXME:
  // -------------
  // FIXME: construct a global-exterme-min-max-file ... use this ... then generate a post-summary-report ... listing unique/extreme file-props ... where a 'global id-cpunter' is used .... then exprot these result-files.
  // FIXME: add logics ... generate result-file ... "r-16-MinMaxEachCell-boundaries.tsv"
  // FIXME:  
  // -------------
  // FIXME: idea: update "hp_clusterShapes" with support of using one distribion for 'within-vertex-cluster-memberships', and a second distribution for 'between-vertex-clsuter-memberships'
  // FIXME: code:
  // FIXME: code:
  // FIXME: code:   
  // FIXME: eval: <-- how to use the findings from this eval?
  // FIXME: eval:
  // FIXME: eval:   
  // -------------
  // FIXME: idea:
  // FIXME: code:
  // FIXME: code:
  // FIXME: code:   
  // FIXME: eval:
  // FIXME: eval:
  // FIXME: eval: 
  // -------------
  // FIXME: idea:
  // FIXME: code:
  // FIXME: code:
  // FIXME: code:   
  // FIXME: eval:
  // FIXME: eval:
  // FIXME: eval: 
  // -------------  
  // FIXME: idea:
  // FIXME: code:
  // FIXME: code:
  // FIXME: code:   
  // FIXME: eval:
  // FIXME: eval:
  // FIXME: eval:   
  // FIXME:
  // FIXME:  
  // -------------
  
  

  
  if(true) {
    _depictExtremeArtificalData(filePrefix);
    // assert(false); // FIXME: remove!
  }

  //! ----------------------------------- default configurations:
  if(false) {
    const uint cnt_clusters = 5;
    const uint nrows = 1000;
    const t_float score_strong = 1;
    const t_float score_weak = 100;
    //! Apply logcs:
    const bool isTo_exportArtificalData = false;
    const char *filePrefix_local = filePrefix;
    const bool apply_postClustering = true;
    __apply_tut_16(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters, isTo_exportArtificalData, apply_postClustering);
  } else {
    //! Note: to exempliufy the dffierence across data-dimensions, a comparison of the heatmaps (Eg, between dim=100 VS dim=1000 in Fig. \ref{}) reveals how dimensioanlity influuences the aspect of .... % FIXME: validate that this is acutally the case, and then elbarote.
    //const uint featureDims_stepSize = 3;
    //const uint featureDims_iterations = 1;
    const uint featureDims_stepSize = 100; //! ie, which is used to consturc the number of piitns in question
    const uint featureDims_iterations = 10;
    //! ----
    const uint weights_map_size = 5;
    // FIXME: correlate below into a seperate matrix.
    const uint weights_map[weights_map_size][2] = {{1,10},{1,100},{1,1000}, {1, 1000} , {1, 10000} };
    //! ----
    for(uint nrows_index = 0; nrows_index < featureDims_iterations; nrows_index++) {
      //! Method: Consturct feature-matrix of dimeions, and then analyse their itnernal simliarites.
      const uint nrows = (nrows_index +1) * featureDims_stepSize;
      for(uint cntClusters_case = 1; cntClusters_case <= 4; cntClusters_case++) {
	//! Note: the estalibshed asusmption is that CCMs are accurete irrepsnvie of the number of clsuters in data:
	uint cnt_clusters = 1;
	if(cntClusters_case == 1)      {cnt_clusters = (uint)((t_float)2); }
	else if(cntClusters_case == 2) {cnt_clusters = (uint)((t_float)nrows/2.0); }
	else if(cntClusters_case == 3) {cnt_clusters = (uint)((t_float)nrows/4.0); }
	else if(cntClusters_case == 4) {cnt_clusters = (uint)((t_float)nrows); }		
	//! 
	for(uint weight_index = 0; weight_index < weights_map_size; weight_index++) {
	  const t_float score_weak = weights_map[weight_index][0];
	  const t_float score_strong = weights_map[weight_index][1];
	  //! set file-result-name
	  allocOnStack__char__sprintf(2000, filePrefix_local, "%s-n%u-c%u-f%ut%u", filePrefix, nrows, cnt_clusters, (uint)score_weak, (uint)score_strong);	  
	  //! Configure: export all data-topolgoeis to PPM (through only for case where nrows is largest.
	  const bool isTo_exportArtificalData = (nrows_index == (featureDims_iterations - 1) ); //! ie, export for the largest data-chunk.
	  //! Apply logcis:
	  const bool apply_postClustering = true;
	  __apply_tut_16(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters, isTo_exportArtificalData, apply_postClustering);
	}
      }
    }
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
  
