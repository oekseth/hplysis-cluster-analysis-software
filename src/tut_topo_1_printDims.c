//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kd_tree.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hpLysis_api.h"//! to define "hpLysis__globalInit__kt_api()"
#include "hp_clusterFileCollection.h"
#include "matrix_deviation.h"
//
#include "hp_frameWork_CCM_boundaries.h" //! sed to structually generate different cluster-partions.
// ***********************************************************
#include "kt_storage_resultOfClustering.h"
#include "kt_distributionTypesC.h" //! which simplfies access to distrubion-types.
// ***********************************************************
//!
//! ------------------- global-config: start:
//! 
//! 
#define MiF__isTo_evaluate_simMetric(sim_id) ((sim_id < e_kt_correlationFunction_groupOf_MINE_mic))
//!
static const uint metric_id_start = 0; //! ie, start at the first metrci=Kendall
//static const uint metric_id_start = e_kt_correlationFunction_groupOf_minkowski_euclid; //! ie, skip Kendall-metrics.
//!
//!
//! ------------------- global-config: end.

static uint to1_getCnt_simMetrics() {
  uint cnt = 0;
  for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
    //{    
    //e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
    for(uint  metric_idx = metric_id_start; metric_idx < (uint)e_kt_correlationFunction_undef; metric_idx++) {  
      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_idx;
      if(MiF__isTo_evaluate_simMetric(metric_id)) {
	cnt++;
      }
    }
  }
  return cnt;
}

static uint to1_getIndex_Euclid() {
  uint cnt = 0;
  for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
    //{    
    //e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
    for(uint  metric_idx = metric_id_start; metric_idx < (uint)e_kt_correlationFunction_undef; metric_idx++) {  
      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_idx;
      if(metric_id == e_kt_correlationFunction_groupOf_minkowski_euclid) {
	return cnt;
      }
      if(MiF__isTo_evaluate_simMetric(metric_id)) {
	cnt++;
      }
    }
  }
  assert(false); //! as we at this exec-state did NOT find the Euclidan simlarty, which seems odd.
  return UINT_MAX;
}

//! @brief identies the type of deviation-strategy to be used.
typedef enum e_to1_deviationStrategy {
  e_to1_deviationStrategy_histo,
  e_to1_deviationStrategy_raw, //! which could result in overlfow of numbers.
  // e_to1_deviationStrategy_,
  // e_to1_deviationStrategy_,
  // ---
  e_to1_deviationStrategy_undef
} e_to1_deviationStrategy_t;

//! A matrix used to simplify writing of new rows.
typedef struct s_to1_StorageMatrix {
  s_kt_matrix_t mat; //! Note: this matrix needs to be correctly sized (by the owner of this object), where dimensions reflects [|files|]x[|metrics|].
  uint current_row;
  bool use_all_simMetrics;
} s_to1_StorageMatrix_t;

static s_to1_StorageMatrix_t init_s_to1_StorageMatrix_t(const uint nrows, const bool use_all_simMetrics) {
  s_to1_StorageMatrix_t self;
  self.mat = setToEmptyAndReturn__s_kt_matrix_t();
  self.current_row = 0;
  self.use_all_simMetrics = use_all_simMetrics;
  if(use_all_simMetrics) {
    const uint ncols = to1_getCnt_simMetrics();
    assert(ncols > 0);
    self.mat = initAndReturn__s_kt_matrix(nrows, ncols);
  } else {
    assert(nrows == UINT_MAX); //! ie, as we then expet it NOT to be set.
  }
  //!
  return self;
}
static s_to1_StorageMatrix_t setToEmpty_s_to1_StorageMatrix_t() {
  return init_s_to1_StorageMatrix_t(0, false);
}
static uint cnt_notUsedRows_s_to1_StorageMatrix_t(s_to1_StorageMatrix_t *self) {
  assert(self->current_row <= self->mat.nrows);
  return (self->mat.nrows -  self->current_row);
}
/**
   @brief merges the mat data-object into this self object.
 **/
static void addFeatures_toMatrix_s_to1_StorageMatrix_t(s_to1_StorageMatrix_t *self, s_kt_matrix_t *mat, const char *str_data_label) {
  assert(mat->nrows < cnt_notUsedRows_s_to1_StorageMatrix_t(self)); //! ie, what we expect for cosnstency.
  assert(mat->ncols == self->mat.ncols); //! ie, a consistency.
  if(self->current_row == 0) { //! then se thte column-eahder:
    for(uint k = 0; k < mat->ncols; k++) {
      const char *str = getAndReturn_string__s_kt_matrix(mat, /*index=*/k, /*addFor_column=*/true);
      assert(str);
      assert(strlen(str));
      //! Set:
      set_stringConst__s_kt_matrix(&(self->mat), /*index=*/k, str_data_label, /*addFor_column=*/true);
    }
  }
  //! Note: we apply a time-costly strategy for coping. Thsi based on the asusmtion taht ex-ecutiont- is (for this eropation) not a problem, and b) could be of itnerst to add satnityt-ests (at thsi step).
  for(uint i = 0; i < mat->nrows; i++) {
    assert(self->current_row <= self->mat.nrows);
    { //! Set string:
      set_stringConst__s_kt_matrix(&(self->mat), /*index=*/self->current_row, str_data_label, /*addFor_column=*/false);
    }
    //! Set values:    
    for(uint k = 0; k < mat->ncols; k++) {
      const t_float s = mat->matrix[i][k];
      self->mat.matrix[self->current_row][k] = s;
    }
    //! Increment:
    self->current_row++;
    assert(self->current_row <= self->mat.nrows);
  }    
}

//!
//! Export data:
static void to1_static_exportRankedSimData(s_kt_matrix_t *mat_input, const	bool use_all_simMetrics, const char *file_name_resultPrefix) {
  { //! Pre: remove 'inf' and 'nan' values:
    for(uint i = 0; i < mat_input->nrows; i++) {
      for(uint k = 0; k < mat_input->ncols; k++) {
	const t_float s = mat_input->matrix[i][k];
	if(isOf_interest(s)) {
	  if(isfinite(s) == false) {
	    mat_input->matrix[i][k] = T_FLOAT_MAX; //! ie, then set the valeu to the default 'empty' value
	  } else {
	    assert(isinf(s) == false);
	    assert(isnan(s) == false);	    
	  }
	}
      }
    }
  }
  //!
  { //! Export: raw: scores:    
    allocOnStack__char__sprintf(2000, str_file, "%s-raw.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
    export__singleCall__s_kt_matrix_t(mat_input, str_file, NULL);
  }
  { //! Export: raw: ranks:
    s_kt_matrix_t mat = initAndReturn_ranks__s_kt_matrix(mat_input);
    allocOnStack__char__sprintf(2000, str_file, "%s-ranked.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
    export__singleCall__s_kt_matrix_t(&mat, str_file, NULL);
    //!
    free__s_kt_matrix(&mat);
  }
  { //! Export: tranposed(ranks):
    s_kt_matrix_t mat = setToEmptyAndReturn__s_kt_matrix_t();
    init__copy_transposed__thereafterRank__s_kt_matrix(&mat, mat_input, /*isTo_updateNames=*/true);
    //!
    allocOnStack__char__sprintf(2000, str_file, "%s-trans-then-ranked.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
    export__singleCall__s_kt_matrix_t(&mat, str_file, NULL);
    //!
    free__s_kt_matrix(&mat);
  }
  { //! Export: raw: mean, skewness, etc., for both 'default' and transposed.
    allocOnStack__char__sprintf(2000, str_file, "%s-deviation", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
    extractAndExport__deviations__singleCall__transposedAndNonTransposed_tsv__s_kt_matrix_t(mat_input, str_file);
  }    
  if(use_all_simMetrics) {
    { //! Export: relative to: Euclid
      //if( (val_min != T_FLOAT_MAX) && (val_min != 0) )
      s_kt_matrix_t mat = initAndReturn__copy__s_kt_matrix(mat_input, /*isTo_updateNames=*/true);
      { //! Apply logics: Adjust:
	//const s_kt_matrix_t *mat_input = &(self->mat);
	//!
	const uint index_search = to1_getIndex_Euclid();
	assert(index_search < mat_input->ncols);
	//!	
	for(uint i = 0; i < mat_input->nrows; i++) {
	  //!
	  //!
	  const t_float val_min = mat_input->matrix[i][index_search];
	  for(uint k = 0; k < mat_input->ncols; k++) {
	    const t_float s = mat_input->matrix[i][k];
	    if(isOf_interest(s)) {
	      mat.matrix[i][k] = s/val_min;
	      //val_max = macro_max(val_max, s);
	    }
	  }
	}	
      }
      //!
      allocOnStack__char__sprintf(2000, str_file, "%s-relativeTo-Euclid.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
      export__singleCall__s_kt_matrix_t(&mat, str_file, NULL);
      //!
      free__s_kt_matrix(&mat);
    }
  }
  { //! Export: relative to: min--max
    //!
    //! Step: get min--max-props:
    t_float val_min = T_FLOAT_MAX;
    //t_float val_max = T_FLOAT_MIN_ABS;
    //const s_kt_matrix_t *mat_input = &(self->mat);
    {
      for(uint i = 0; i < mat_input->nrows; i++) {
	for(uint k = 0; k < mat_input->ncols; k++) {
	  const t_float s = mat_input->matrix[i][k];
	  if(isOf_interest(s)) {
	    val_min = macro_min(val_min, s);
	    // val_max = macro_max(val_max, s);
	  }
	}
      }
    }
    if( (val_min != T_FLOAT_MAX) && (val_min != 0) ) {
      s_kt_matrix_t mat = initAndReturn__copy__s_kt_matrix(mat_input, /*isTo_updateNames=*/true);
      { //! Adjust:
	for(uint i = 0; i < mat_input->nrows; i++) {
	  for(uint k = 0; k < mat_input->ncols; k++) {
	    const t_float s = mat_input->matrix[i][k];
	    if(isOf_interest(s)) {
	      mat.matrix[i][k] = s/val_min;
	      //val_max = macro_max(val_max, s);
	    }
	  }
	}      
      }
      //!
      allocOnStack__char__sprintf(2000, str_file, "%s-relativeTo-min.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
      export__singleCall__s_kt_matrix_t(&mat, str_file, NULL);
      //!
      free__s_kt_matrix(&mat);
    }
  }
  { //! Export: extreme-prop: a variant where the lcoal props are used:
    s_kt_matrix_t mat = initAndReturn__copy__s_kt_matrix(mat_input, /*isTo_updateNames=*/true);
    allocOnStack__char__sprintf(2000, str_file_extreme_summary, "%s-local-relativeTo-min-max.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
    allocOnStack__char__sprintf(2000, str_file, "%s-local-relativeTo-min.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
    FILE *file_local = fopen(str_file_extreme_summary, "wb");
    { //! Evaluate + Adjust:
      //const s_kt_matrix_t *mat_input = &(self->mat);      
      for(uint i = 0; i < mat_input->nrows; i++) {
	uint    range_index[2] = {UINT_MAX, UINT_MAX};
	t_float range[2]       = {T_FLOAT_MAX, T_FLOAT_MIN_ABS};
	for(uint k = 0; k < mat_input->ncols; k++) {
	  const t_float s = mat_input->matrix[i][k];
	  if(isOf_interest(s)) {
	    //mat.matrix[i][k] = s/val_min;
	    if(range[0] > s) { //! then a new minima:
	      range[0] = s;
	      range_index[0] = k;
	    }
	    if(range[1] < s) { //! then a new maxima:
	      range[1] = s;
	      range_index[1] = k;
	    }	      
	  }
	}
	if(range[0] != T_FLOAT_MAX) {//! Export to file:
	  //! Note: Export: [data, name(best-metric, worst-metric) , score(best/worst)] % FIXME: update "free_s_to1_StorageMatrix_t(..)"	  
	  if(i == 0) { //! Wrte out a summary:
	    fprintf(file_local, "#! Data\t");
	    fprintf(file_local, "%s\t", "Metric: Worst");
	    fprintf(file_local, "%s\t", "Metric: Best");
	    fprintf(file_local, "%s\n", "Score");	    
	  }
	  { //! The data:
	    assert(range_index[0] < mat.ncols);
	    assert(range_index[1] < mat.ncols);	    
	    const char *str_worst = getAndReturn_string__s_kt_matrix(&mat, /*index=*/range_index[0], /*is-a-column=*/true);
	    const char *str_best = getAndReturn_string__s_kt_matrix(&mat, /*index=*/range_index[1], /*is-a-column=*/true);	    
	    fprintf(file_local, "#! Data\t");
	    fprintf(file_local, "%s\t", str_worst);
	    fprintf(file_local, "%s\t", str_best);
	    const t_float diff = range[1] / range[0];
	    fprintf(file_local, "%.2f\n", diff);
	  }

	}
	//! Adjsut locally:
	for(uint k = 0; k < mat_input->ncols; k++) {
	  const t_float s = mat_input->matrix[i][k];
	  if(isOf_interest(s)) {
	    const t_float val_min = range[0];
	    mat.matrix[i][k] = s/val_min;
	  }
	}
      }      
    }
    //!
    export__singleCall__s_kt_matrix_t(&mat, str_file, NULL);
    //!
    free__s_kt_matrix(&mat);
    //!
    fclose(file_local);
  }
  { //! Apply logics for metric-reccomendations

    // FIXME: add support for metric-reccomendations

    
    { //!
      const uint threshold_count = 4;
      const uint count_threshold[threshold_count] = {1, 5, 10, 15}; //! ie, Result-matrix: ... [metric]x[percent(best) | threshold=[1\%, 5\%, 10\%, 15\% ...]] ... 
      //! 
      s_kt_matrix_t mat_rankTrans = setToEmptyAndReturn__s_kt_matrix_t();
      init__copy_transposed__thereafterRank__s_kt_matrix(&mat_rankTrans, mat_input, /*isTo_updateNames=*/true);
      //! Initate result-matrix:
      s_kt_matrix_t mat_res = initAndReturn__s_kt_matrix(mat_rankTrans.nrows, threshold_count);
      { //! Set column-names:
	for(uint col_id = 0; col_id < mat_res.ncols; col_id++) { //! ie, each threshold-id:
	  allocOnStack__char__sprintf(2000, tag, "Threshold=%u", count_threshold[col_id]);
	  set_stringConst__s_kt_matrix(&mat_rankTrans, col_id, tag, /*addFor_column=*/true);
	}
      }
      //!
      for(uint row_id = 0; row_id < mat_rankTrans.nrows; row_id++) { //! ie, each metric-id:
	//!
	// FIXME: factor out below ... then write a simple dummy sanity-test for teh below ... 
	//!
	/**
	   @remarks idea: the use of best-matching simalirty-metrics can be mislieading given its possitve (high) sentitiy to trainign-data. To minize these, some argue that it is useful to select metrics which is sufficent good (ie, which is not pseciilesed towards the givne dat-aensmalbe) ... hopefully, giving the idneitfed algorithma  higher degree of robustness.
	   @remarks Steps:
	   -- input data: apply the ... simalrity-meitrcs (Table \ref{}) to a given data-ensamble, and caclualte the variance (from the generatedc coariance-matrix).
	   -- adjusted data: iteratve throguh ranked data-items: each $id(metric), id(data)=score$ combianaiton, where each \textit{score} refers to a \textit{sorted rank of variance} in the data.
	   -- threshold: idneitfy each \textit{score} which has a relative rank inside the erorr-boundary, a threshold set to $[1\%, 5\%, 10\%, 15\%]$.
	   -- count: for each $id(metric), threshold$ combination idneitfy the \textit{count} of metrics wtih a rank less than the threshold.
	   -- ... 
	 **/
	uint count[threshold_count] = {0, 0, 0, 0};
	t_float threshold_pst_1 = (t_float)(mat_rankTrans.ncols) / 100.0; //! ie, where '1%' correspons to $total/100$.

	for(uint data_id = 0; data_id < mat_rankTrans.ncols; data_id++) { //! ie, each metric-id:
	  const t_float rank = mat_rankTrans.matrix[row_id][data_id];
	  if(rank != T_FLOAT_MAX) {
	    // FIXME: is below correct?	   	    
	    //!
	    //! Update the counts, ie, fi they match:
	    for(uint count_index = 0; count_index < threshold_count; count_index++) {
	    // FIXME: alwasy correct ot chooise the min-value?
	      t_float threshold = threshold_pst_1 * count_threshold[count_index];
	      if(threshold == 0) {threshold = 1.0;} // TODO: validte correctness of this.
	      if(rank < threshold) {
		count[count_index]++;
	      }
	    }
	  }
	}
	//!
	//! Add results to the matrix:
	for(uint count_index = 0; count_index < threshold_count; count_index++) {
	  mat_rankTrans.matrix[row_id][count_index] = (t_float)count[count_index];
	}
      }
      //!
      //! Export the results:
      allocOnStack__char__sprintf(2000, str_file, "%s-metricPreference.tsv", file_name_resultPrefix);  //! ie, allcoate the "str_filePath."
      export__singleCall__s_kt_matrix_t(&mat_rankTrans, str_file, NULL);
      //!      
      //!      
      //! De-allocate:
      free__s_kt_matrix(&mat_rankTrans);
    }
  }
}

static void free_s_to1_StorageMatrix_t(s_to1_StorageMatrix_t *self, const char *file_name_resultPrefix) {  
  if(self->current_row == 0) {
    fprintf(stderr, "(info)\t Input-data seems to be empty: was this intentional (or a bug)? \t Observiaont at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    //!
    //! De-allcoate:
    free__s_kt_matrix(&(self->mat));
    return;
  }
  s_kt_matrix_t *mat_input = &(self->mat);
  const bool use_all_simMetrics = self->use_all_simMetrics;
  //!
  //! Export data
  to1_static_exportRankedSimData(mat_input, use_all_simMetrics, file_name_resultPrefix);
  //!
  //! De-allcoate:
  free__s_kt_matrix(&(self->mat));
  self->current_row = 0;
}


typedef struct s_to1 {
  const char *file_name;
  FILE *file_out;
  FILE *file_out_matrix;
  bool header_isAdded;
  // 
  bool strategy_isToApply_forAllSimMetric;  //! an optional swith to enblae the analsysi of all possible sim-meitrcs.
  e_to1_deviationStrategy_t strategy_metric_deviation; //! ie, the deviation strategy to be used.
s_kt_correlationMetric_t *metric_sim_onRawData; //! an optinal attribute.
  s_kt_list_1d_float_t *storage_1d_variance; //! which if set: then the vairnace-scores are added to this.
  s_to1_StorageMatrix_t *storage_matrix; //! which if set: the the results are stored into this object. 
} s_to1_t;

static s_to1_t init_s_to1_t(const char *file_name, const bool strategy_isToApply_forAllSimMetric) {
  //!
  s_to1_t self;
  //!
  self.file_name = file_name;
  self.file_out = fopen(file_name, "wb");
  self.file_out_matrix = NULL;
  self.header_isAdded = false;
  // ---
  self.strategy_isToApply_forAllSimMetric = strategy_isToApply_forAllSimMetric;
  self.metric_sim_onRawData = NULL; //! an optinal attribute: if 'NULL' then this is NOT to be used.
  self.strategy_metric_deviation = e_to1_deviationStrategy_histo; //! ie, the default.
  self.storage_1d_variance = NULL; //! ie, default assumtpion is that this is NOT to be used
  self.storage_matrix      = NULL; //! which if set: the the results are stored into this object. 
  //!
  printf("(info)\t Log-file generated at %s [from [%s]:%s:%d\n]", file_name, __FUNCTION__, __FILE__, __LINE__);
  //!
  assert(self.file_out);
  /* //! */
  /* { //! Add header: */
  /*   const char *label_data = "#! row-name:"; */
  /*   const char *label_data_dim_rows = "Rows"; */
  /*   const char *label_data_dim_cols = "Columns"; */
  /*   const char *str_case_label = "Algorithm"; */
  /*   const char *config_minCnt = "DBSCAN-configuration"; */
  /*   const char *time_total = "Time"; */
  /*   // const char * = "";      */
  /*   fprintf(stdout, "%s\t%s\t%s\t%s\t%s\t%s\n", label_data, label_data_dim_rows, label_data_dim_cols, str_case_label, config_minCnt, time_total); */
  /*   fprintf(self.file_out, "%s\t%s\t%s\t%s\t%s\t%s\n", label_data, label_data_dim_rows, label_data_dim_cols, str_case_label, config_minCnt, time_total); */
  /* } */
  // FIXME: close thsi object
  //! 
  return self;
}

static void free_s_to1_t(s_to1_t *self) {
  assert(self->file_out != NULL);
  fclose((self->file_out));
  self->file_out = NULL;
  if(self->file_out_matrix != NULL) {
    fclose((self->file_out_matrix));
    self->file_out_matrix = NULL;
  }
}

/* static void _add_to_result(s_to1_t *self, const char *label_data, const char *str_case_label, const float time_total, const uint config_minCnt, s_kt_matrix_t *mat_input) { */
/*   assert(self->file_out); */
/*   fprintf(self->file_out, "%s\t%u\t%u\t%s\t%u\t%.2f\n", label_data, mat_input->nrows, mat_input->ncols, str_case_label, config_minCnt, time_total); */
/* } */



//! Add the results to the result-set:
static void __to1_add_deviation_toResult(s_to1_t *self, const s_kt_matrix_t *mat_input, const char *stringOf_tagSample, const s_matrix_deviation_std_row_t obj_dev, const t_float val_min, const t_float val_max) {
  {
    if(self->storage_1d_variance != NULL) {
      t_float s = obj_dev.skewness;
      if(s == T_FLOAT_MAX) {s = 0;} //! ie, to avoid the avovalue from being ingored ... which could cause sinsoties in the itnernal indexes in the sparse list.
      push__s_kt_list_1d_float_t(self->storage_1d_variance, s);
    }
  }
  { //! Step: Write out the data:
    assert(self->file_out != NULL);
    if(self->header_isAdded == false) { //! then write out the header.
      for(uint i = 0; i < 2; i++) {
	FILE *fp = self->file_out;
	if(i == 1) {fp = stdout;}
	assert(fp != NULL);
	//! Note: use mulipel liens to print the rsult. This to simplify debugging; givne hos 'frrprint' worsk, this would not result in a peromance-dregaiton (due to buffering of valeus to write out).
	fprintf(fp, "%s\t", "#! Data");
	fprintf(fp, "%s\t", "nrows");
	fprintf(fp, "%s\t", "ncols");
	fprintf(fp, "%s\t", "min");	  
	fprintf(fp, "%s\t", "max");
	fprintf(fp, "%s\t", "mean");
	fprintf(fp, "%s\t", "variance");
	fprintf(fp, "%s\t", "skewness");
	fprintf(fp, "%s\n", "kurtosis");   
	//!
      }
      self->header_isAdded = true;	
    } //! end: the header is updated.
      //!
    { //! Set data:
      for(uint i = 0; i < 2; i++) {
	FILE *fp = self->file_out;
	if(i == 1) {fp = stdout;}
	assert(fp != NULL);
	//! Note: use mulipel liens to print the rsult. This to simplify debugging; givne hos 'frrprint' worsk, this would not result in a peromance-dregaiton (due to buffering of valeus to write out).
	fprintf(fp, "%s\t", stringOf_tagSample);
	fprintf(fp, "%u\t", mat_input->nrows);
	fprintf(fp, "%u\t", mat_input->ncols);
	fprintf(fp, "%.2f\t", val_min);	  
	fprintf(fp, "%.2f\t", val_max);
	fprintf(fp, "%.2f\t", obj_dev.mean);
	fprintf(fp, "%.2f\t", obj_dev.variance);
	fprintf(fp, "%.2f\t", obj_dev.skewness);
	fprintf(fp, "%.2f\n", obj_dev.kurtosis);   
	//!
      }
    }
  }
}

//static void _to1_analyze_matrix_case_(s_to1_t *self, s_kt_matrix_t *mat_input, const char *stringOf_tagSample) {
static bool _to1_analyze_matrix_case_histo(s_to1_t *self, s_kt_matrix_t *mat_input, const char *stringOf_tagSample) {
  assert(self);
  assert(mat_input);
  assert(mat_input->nrows > 0);
  assert(mat_input->ncols > 0);
  bool is_added = false;
  //s_kt_list_1d_float_t list_scores = convert_matrixToList_1d_s_kt_matrix_t(mat_input);
  //if(list_scores.list_size > 0) {
  {
    //!
    //!
    //! The histogram setup ("kt_list_2d.h");
    const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count;    
    // FIXME: meausrement: ... use below histo-type to estiamte the seperatbility across different dimensions ... apply a row-based data-nromsaiton before rpign the socreos out (to get an approxmiate of the seperaily, rhater thant he sim-scores themself) .... hyptoesis: if the sepraility decreases with icnreasing number of dimeisons, then ... 
    //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_sum;
    //const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_count_averageOnBins;
    const uint config_cnt_histogram_bins = 10;
    //!
    //! Specify the differnet types of randomzaiton-fucntisoinw we are to explore:
    const uint config_maxValue = 100; //! ie, the value-spread.
    //!
    const bool config_isTo_forMinMax_useGlobal = true;
    //!
    //! Compute the historgram:
    s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(mat_input);
    s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(&vec_1, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
    //s_kt_list_1d_float_t list_histo = applyTo_matrix__kt_aux_matrix_binning(mat_input, config_cnt_histogram_bins, config_isTo_forMinMax_useGlobal, enum_id_scoring);
    assert(list_histo.list_size > 0); // TODO: vlaidte corredness of this asseriotn.
    assert(list_histo.current_pos > 0); // TODO: vlaidte corredness of this asseriotn.
    //!
    { //! Export results:
      //!
      //! Step: get min--max-props:
      t_float val_min = T_FLOAT_MAX; t_float val_max = T_FLOAT_MIN_ABS;
      for(uint i = 0; i < list_histo.current_pos; i++) {
	const t_float s = list_histo.list[i];
	if(isOf_interest(s)) {
	  val_min = macro_min(val_min, s);
	  val_max = macro_max(val_max, s);
	}
      }
      if(val_min != val_max) { //! then the matrix is NOT empty:
	//!
	//! Step: Get deviations:
	s_matrix_deviation_std_row_t obj_dev = setToEmptyAndReturn__s_matrix_deviation_std_row();
	get_forRow_sampleDeviation__matrix_deviation(list_histo.list, list_histo.current_pos, &obj_dev);
	//!
	//! Add the results to the result-set:
	__to1_add_deviation_toResult(self, mat_input, stringOf_tagSample, obj_dev, val_min, val_max);
	//!
	is_added = true;
      }
    }
    //}
    free__s_kt_list_1d_float_t(&list_histo);
  }
  return is_added;
}

static bool _to1_analyze_matrix_case_raw(s_to1_t *self, s_kt_matrix_t *mat_input, const char *stringOf_tagSample) {
  assert(self);
  assert(mat_input);
  assert(mat_input->nrows > 0);
  assert(mat_input->ncols > 0);
  //!
  bool is_added = false;
  //! Step: get min--max-props:
  t_float val_min = T_FLOAT_MAX; t_float val_max = T_FLOAT_MIN_ABS;
  {
    for(uint i = 0; i < mat_input->nrows; i++) {
      for(uint k = 0; k < mat_input->ncols; k++) {
	const t_float s = mat_input->matrix[i][k];
	if(isOf_interest(s)) {
	  val_min = macro_min(val_min, s);
	  val_max = macro_max(val_max, s);
	}
      }
    }      
  }
  if(val_min != val_max) { //! then the matrix is NOT empty:
    //!
    //! Step: Get deviations:
    s_matrix_deviation_std_row_t obj_dev = setToEmptyAndReturn__s_matrix_deviation_std_row();
    get_forMatrix_sampleDeviation__matrix_deviation(mat_input->matrix, mat_input->nrows, mat_input->ncols, &obj_dev);
    //!
    //! Add the results to the result-set:
    __to1_add_deviation_toResult(self, mat_input, stringOf_tagSample, obj_dev, val_min, val_max);
    //!
    is_added = true;
  }
  return is_added;  
}

static bool to1_analyze_matrix(s_to1_t *self, s_kt_matrix_t *mat_input, const char *stringOf_tagSample) {
  if(self->strategy_metric_deviation = e_to1_deviationStrategy_histo) {
    return _to1_analyze_matrix_case_histo(self, mat_input, stringOf_tagSample);
  } else if(self->strategy_metric_deviation = e_to1_deviationStrategy_raw) {
    return _to1_analyze_matrix_case_raw(self, mat_input, stringOf_tagSample);
    //  } else if(self->strategy_metric_deviation = e_to1_deviationStrategy_) {
  } else {
    fprintf(stderr, "!!\t Add support for enum=%u, at %s:%d\n", self->strategy_metric_deviation, __FILE__, __LINE__);
    assert(false); //! ie, a a heads-up.
    return false;
  } 

}


static void __to1_apply_forAllSimMetric(s_to1_t *self, s_kt_matrix_t *mat_data, const char *stringOf_tagSample) { //s_kt_correlationMetric_t metric_local) {
  //!
  assert(mat_data->nrows > 0);
  assert(mat_data->ncols > 0);      
  //!
  //!
  //#define MiF__isTo_evaluate_simMetric(sim_id) (!isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id))
  //!
  // FIXME: ... feasible ... to also iteratei through ... [e_kt_categoryOf_correaltionPreStep_none, e_kt_categoryOf_correaltionPreStep_rank, ... ]??
  //const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_none;
  //!



  s_kt_list_1d_float_t list_scores = init__s_kt_list_1d_float_t(/*max-size=*/e_kt_correlationFunction_undef * e_kt_categoryOf_correaltionPreStep_none);
  //s_kt_list_1d_float_t list_scores = init__s_kt_list_1d_float_t(/*max-size=*/e_kt_correlationFunction_undef);
  self->storage_1d_variance = &list_scores;

  { //! Pre: fix issue where score is set to 'inf':
    for(uint i = 0; i < mat_data->nrows; i++) {
      for(uint k = 0; k < mat_data->ncols; k++) {
	const t_float s = mat_data->matrix[i][k];
	if(isinf(s)) {
	  mat_data->matrix[i][k] = T_FLOAT_MAX; //! ie, then set to 'empty'.
	}
      }
    }
  }
  
  uint enum_id = UINT_MAX;
  for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
    //{    
    e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
    for(uint  metric_idx = metric_id_start; metric_idx < (uint)e_kt_correlationFunction_undef; metric_idx++) {  
      //!
      assert(mat_data->nrows > 0);
      assert(mat_data->ncols > 0);      
      //!
      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_idx;
      //!
      //!
      //! Construct the reference-string:
      char str_obj[1000]; 	
      { //! Set: row-header:
	char str_pre[1000]; memset(str_pre, '\0', 1000);
	// printf("typeOf_correlationPreStep=%u, at %s:%d\n", typeOf_correlationPreStep, __FILE__, __LINE__);
	if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {sprintf(str_pre, "rank");}
	else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {sprintf(str_pre, "binary");}
	else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) {sprintf(str_pre, "none");}
	else {assert(false);} //! ie, then add support for this.
	//!
	const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id);
	//!
	//! Combine 'pre' and 'metric-base';
	memset(str_obj, '\0', 1000); sprintf(str_obj, "%s %s", str_pre, str_metric);
      }	
      //!
      if(MiF__isTo_evaluate_simMetric(metric_id)) {
	if(enum_id != UINT_MAX) {
	  enum_id++;
	} else {enum_id = 0;} //! ie, then the first index.
      } else {
	//fprintf(stderr, "does NOT evaluate for %s, at %s:%d\n", str_obj, __FILE__, __LINE__);
	continue;}	
      assert(MiF__isTo_evaluate_simMetric(metric_id) == true); //! ie, what we expect

      //!
      s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
      obj_metric.metric_id = metric_id;
      obj_metric.typeOf_correlationPreStep = typeOf_correlationPreStep;
      //! 
      //!
      assert(mat_data->nrows > 0);
      assert(mat_data->ncols > 0);      
      //!
      s_kt_matrix_base_t mat_shallow       = initAndReturn__notAllocate__s_kt_matrix_base_t(mat_data->nrows, mat_data->ncols, mat_data->matrix);
      s_kt_matrix_base_t mat_result        = initAndReturn__empty__s_kt_matrix_base_t();
      s_hp_distance__config_t local_config = init__s_hp_distance__config_t();
      assert(mat_shallow.matrix != NULL);
      const bool is_ok = apply__hp_distance(obj_metric, &mat_shallow, &mat_shallow, &mat_result, local_config);
      // const bool is_ok = apply__rows_hp_distance(list_metrics[i], row_1, row_2, ncols, &scalar_result);
      assert(is_ok);
      //printf("\t(correlate-and-export)\t ncols=%u, at %s:%d\n", mat_result.ncols, __FILE__, __LINE__);
      bool value_was_added = false;
      if(mat_result.ncols > 0) { //! then we assume data is allcoated:
	//! Convert to a 1d-list, calcuate variance, then add to result-veoctr, a result-vecotr which is then exported.
	s_kt_matrix_t mat_data = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result);
	//!
	//! Adjust prefix:
	char str[1000]; memset(str, '\0', 1000);
	sprintf(str, "%s-%s", stringOf_tagSample, str_obj);
	//!
	//! Calidatuion matrix-deviaiton:
	value_was_added = to1_analyze_matrix(self, &mat_data, str); //, obj_metric);
      } else {
	;
      }
      if(value_was_added == false) {
	if(self->storage_1d_variance != NULL) {
	  // printf("\t\t(info)\t metric:%u: no data was added, at %s:%d\n", metric_id, __FILE__, __LINE__);
	  push__s_kt_list_1d_float_t(self->storage_1d_variance, /*variance=*/0); //! ie, to keep the order
	}
      }	      
      //! De-allocte:
      free__s_kt_matrix_base_t(&mat_result);      
    }
  }
  if(list_scores.current_pos > 0) { // TODO: vlaidte corredness of this asseriotn.    
    { //! Export the results to a file:
      if(self->file_out_matrix == NULL) { //! then write out the header.
	char str_file[1000]; memset(str_file, '\0', 1000);
	//! Ensure that the file-name does not cost of the file-suffix (eg, ".tsv").
	const char *file_name_atSuffix = strrchr(self->file_name, '.');
	assert(file_name_atSuffix != NULL);
	assert(strlen(file_name_atSuffix));
	const loint len_baseName = (loint)(strlen(self->file_name) - strlen(file_name_atSuffix));
	assert(len_baseName > 0);
	printf("name:\"%s\" w/size:%u, suffix:\"%s\"  w/size:%u --- len(offset):%u, at %s:%d\n", self->file_name, (uint)strlen(self->file_name), file_name_atSuffix, (uint)strlen(file_name_atSuffix), (uint)len_baseName, __FILE__, __LINE__);
	assert(len_baseName < strlen(self->file_name));
	//! Add the file-base-path:
	strncpy(str_file, self->file_name, len_baseName);
	//! Add the 'new' file-suffix:
	sprintf(str_file + strlen(str_file), "%s", "-matrix.tsv");
	//! Open the new file:
	printf("(info)\t Opens matrix-file:\"%s\", at %s:%d\n", str_file, __FILE__, __LINE__);
	self->file_out_matrix = fopen(str_file, "wb");
	assert(self->file_out_matrix != NULL);
	//!
	{ //! Add the header-text:
	  uint enum_id = UINT_MAX;
	  fprintf(self->file_out_matrix, "%s\t", "#! Data");
	  for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
	    //{    
	    e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
	    for(uint  metric_idx = metric_id_start ; metric_idx < (uint)e_kt_correlationFunction_undef; metric_idx++) {  
	      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_idx;
	      //!
	      //!
	      //! Construct the reference-string:
	      char str_obj[1000]; 	
	      { //! Set: row-header:
		char str_pre[1000]; memset(str_pre, '\0', 1000);
		// printf("typeOf_correlationPreStep=%u, at %s:%d\n", typeOf_correlationPreStep, __FILE__, __LINE__);
		if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {sprintf(str_pre, "rank");}
		else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {sprintf(str_pre, "binary");}
		else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) {sprintf(str_pre, "none");}
		else {assert(false);} //! ie, then add support for this.
		//!
		const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id);
		//!
		//! Combine 'pre' and 'metric-base';
		memset(str_obj, '\0', 1000); sprintf(str_obj, "%s %s", str_pre, str_metric);
	      }
	      //!
	      if(MiF__isTo_evaluate_simMetric(metric_id)) {
		if(enum_id != UINT_MAX) {
		  enum_id++;
		} else {enum_id = 0;} //! ie, then the first index.
	      } else {
		//fprintf(stderr, "does NOT evaluate for %s, at %s:%d\n", str_obj, __FILE__, __LINE__);
		continue;}	
	      assert(MiF__isTo_evaluate_simMetric(metric_id) == true); //! ie, what we expect
	      //!
	      //!
	      fprintf(self->file_out_matrix, "%s", str_obj);
	      if(MiF__isTo_evaluate_simMetric(metric_idx +1)) {
		putc('\t', self->file_out_matrix);
	      } else {
		putc('\n', self->file_out_matrix);
	      }
	      if(self->storage_matrix != NULL) { //! Then update the result-matrix with column-header:
		assert(enum_id < self->storage_matrix->mat.ncols);
		set_stringConst__s_kt_matrix(&(self->storage_matrix->mat), enum_id, str_obj, /*addFor_column=*/true);
	      }	  	    
	    }
	  }
	}
      } //! End: of updating the file-header + matrix-header.

      //!
      //! Step: Set the score:
      fprintf(self->file_out_matrix, "%s\t", stringOf_tagSample);
      for(uint i = 0; i < list_scores.current_pos; i++) {
	const t_float s = list_scores.list[i];
	if(isOf_interest(s)) {
	  //!
	  fprintf(self->file_out_matrix, "%f", s);
	} else {
	  fprintf(self->file_out_matrix, "-");
	}
	//!
	if((i+1) != list_scores.current_pos) {
	  putc('\t', self->file_out_matrix);
	} else {
	  putc('\n', self->file_out_matrix);
	}	
	
      }           
    }
    if(self->storage_matrix != NULL)  { //! if an optianla matrix is set, then add the data to this.
      //!
      //printf("list_scores.pos:%u, while ncols:%u (metrics:%u), at %s%d\n", list_scores.current_pos, self->storage_matrix->mat.ncols, to1_getCnt_simMetrics(), __FILE__, __LINE__);
      assert(list_scores.current_pos == self->storage_matrix->mat.ncols); // TODO: validte correctness of this asseriton.
      const uint row_id = self->storage_matrix->current_row;
      //printf("storage{nrows:%u, current:%u"
      assert(self->storage_matrix->current_row < self->storage_matrix->mat.nrows);      
      //! Set values:
      for(uint i = 0; i < list_scores.current_pos; i++) {
	const t_float s = list_scores.list[i];
	self->storage_matrix->mat.matrix[row_id][i] = s;	
      }      
      //! Increment:
      self->storage_matrix->current_row++;      
    }
  }
  //! De-allcoate:
  free__s_kt_list_1d_float_t(&list_scores);
  self->storage_1d_variance = NULL; //! ie, clear this relationship.
}

static void to1_analyze_setOf_matrix(s_to1_t *self, const s_kt_correlationMetric_t obj_metric, s_kt_matrix_t *mat_list, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *stringOf_resultFile) { // e_kt_correlationFunction_t sim_pre, const e_hpLysis_clusterAlg clusterAlg, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm, const char *stringOf_resultFile) {
  assert(mat_list);
  assert(mat_input_size);
  //assert(clusterAlg != e_hpLysis_clusterAlg_undef);
  //!
  //!
  //!
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    s_kt_matrix_t mat_input = mat_list[data_id];
    assert(mat_input.nrows > 0);
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }    
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
    //!
    //! Apply:
    if( (self->metric_sim_onRawData != NULL) && (self->strategy_isToApply_forAllSimMetric == false)  ){ //! then apply a sim-metric-pre-step
      //!
      //!
      //! Apply logics:
      s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_input);
      assert(vec_1.matrix != NULL);
      s_kt_matrix_base_t vec_result = initAndReturn__empty__s_kt_matrix_base_t();
      // hp_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index; //! ie, an appraoch to reduce data-ndimesnality; used in algorithms such as minibatch and k-means++
      // FIXME: add support for transofmring the 2d-matrix into a 1d-matrix (utilizing the proerpties in the "hp_config")?? 
      const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_1,  &vec_result, /*config=*/hp_config);	    
      assert(is_ok);
      //!
      s_kt_matrix_t mat_data = get_deepCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
      assert(self->strategy_isToApply_forAllSimMetric == false); //! ie, to simplify this analsysi-process.
      to1_analyze_matrix(self, &mat_data, stringOf_tagSample); //, obj_metric);

      //!
      //! De-allocate:
      //! Note: two steps are used to simplify standardization.
      free__s_kt_matrix_base_t(&vec_result);
      free__s_kt_matrix(&mat_data);
    } else {
      if(self->strategy_isToApply_forAllSimMetric == false) {
	to1_analyze_matrix(self, &mat_input, stringOf_tagSample); //, obj_metric);
      } else { //! then evalaute for all sim-metrics on this data:
	assert(mat_input.nrows > 0);
	__to1_apply_forAllSimMetric(self, &mat_input, stringOf_tagSample);
      }	
    }
  }
  
}


  
//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t to1_readFile(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) {
	fprintf(stderr, "!!\t Unable to read file=\"%s\" w/tag=\"%s\", at %s:%d\n", data_obj.file_name, data_obj.tag, __FILE__, __LINE__);
	assert(false); }
    } else { //! then a sysntetic (or: artifical) data:
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}


static void to1_apply(s_to1_t *self, const s_kt_correlationMetric_t obj_metric, const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = to1_readFile(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    //!
    //! 
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
    assert(mat_collection.list[data_id].nrows > 0);
  }
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) { assert(mat_collection.list[data_id].nrows > 0); }
  //!
  //! Apply logics: 
  to1_analyze_setOf_matrix(self, obj_metric, mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix);
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}

static void to1_applyToFileSet(s_to1_t *self, s_kt_correlationMetric_t obj_metric) {
  const char *resultPrefix = "tut-r-to1";
  {
    //const char *resultPrefix = "tut_80DataSets_";
    {
#define __MiCo__useLocalVariablesIn__dataRealFileLoading 1
#ifdef __MiCo__useLocalVariablesIn__dataRealFileLoading
      const uint sizeOf__nrows = 100;
      const uint sizeOf__ncols = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      const bool isTo__evaluate__simMetric__MINE__Euclid = true;
      const char *stringOf_resultDir = "";
      // ---
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

      s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
      // printf("at %s:%d\n", __FILE__, __LINE__);
      //!
      //! We are interested in a more performacne-demanind approach: 

      fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
      fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
      //!
      fileRead_config__syn.isTo_transposeMatrix = false;
      fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
      fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
      fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
      fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
      //! --------------------------------------------
      //!
      //! File-specific cofnigurations: 
      s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
      //fileRead_config.isTo_transposeMatrix = true;
      fileRead_config.isTo_transposeMatrix = false;
      fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
      fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
      fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
      s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
      //fileRead_config.isTo_transposeMatrix = true;
      fileRead_config.isTo_transposeMatrix = true;
      //!

#endif

      { //const char *nameOf_experiment = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
	allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
	to1_apply(self, obj_metric, mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset);
      }
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
    }
  }
  { //! Load for different real-life data-sets:
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
    const bool isTo_transposeMatrix = false;
    //const char *resultPrefix = "tut-k4-4";
    /*     { */
    /* #include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used. */
    /*       allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment); */
    /*       to1_apply(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset); //, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix); */
    /*       //__eval__dataCollection__tut_3(mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix); */
    /*     } */
    /*     {  */
    /* #include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used. */
    /*       allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment); */
    /*       to1_apply(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset); //, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix); */
    /*     } */
    {
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      to1_apply(self, obj_metric, mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset); //, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);
    }
    /*     { */
    /* #include "tut__aux__dataFiles__differentDimensions_wrapper.c" */
    /*       //! Apply: */
    /*       allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment); */
    /*       to1_apply(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset); //, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix);       */
    /*     } */
    {
      const uint config__kMeans__defValue__k__min = 2;   const uint config__kMeans__defValue__k__max = 4;
      const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min;   
#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
      //!
      //!
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      to1_apply(self, obj_metric, mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset); //, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);   
    }
  }
  //! ----------------------------------------------------------
  { //! Data: evalaute for different dimensions:
    const uint center_relative_max = 4; //! which is used as the center-point when generating randomzied functions (Eq. \ref{}) .... Eq.: $nrows / center$ ....
    const uint config_cnt_histogram_bins = 10;
    const uint dimensions_cnt_start = 19;
    const uint dimensions_cnt = 20;
    const uint dimensions_stepSize = 200;
    const bool globalConfig_sRandReset = false; //! which if set to 'true' implies that each vector gets the same randomzied values (hence, increases accuracy of predciton-comparison).    
    const uint cnt_times_random = 100; //! ie, the 'nrows' property.
    //!
    
    for(uint dist_id = 0; dist_id < e_distributionTypesC_undef; dist_id++) {
      for(uint center_relative = 1; center_relative <= center_relative_max; center_relative++) {
	//for(uint center_relative = 1; center_relative <= center_relative_max; center_relative++, mat_result_index++) {
	//for(uint rand_id = 0; rand_id < setOfRandomNess_size; rand_id++) {      
	//! Construct: the object holding the randomizatin-distributions:
	const uint ncols = config_cnt_histogram_bins;
	const uint center_index = (uint)(/*center=*/(t_float)ncols/(t_float)center_relative);

	// const e_kt_randomGenerator_type_t type_randomness_in = setOfRandomNess[rand_id];
	//! Traverse different types of simliarty-metrics:
	//for(uint i = 0; i < list_metrics_size; i++, mat_corrAll_index++)
	{
	  //assert(mat_corrAll_index < mat_corrAll.nrows);
	  //printf("\t(info) rand=%u/%u, metric=%u/%u, at %s:%d\n", dist_id, e_distributionTypesC_undef, i, list_metrics_size, __FILE__, __LINE__);
	  /* #ifndef tutBiasConfig_testAllBaseMetrics */
	  /* 	  const s_kt_correlationMetric_t curr_metric = list_metrics[i]; */
	  /* #else */
	  /* 	  const s_kt_correlationMetric_t curr_metric = initAndReturn__s_kt_correlationMetric_t(/\*metric=*\/(e_kt_correlationFunction_t)i, /\*prestep=*\/list_metrics_preStep); */
	  /* #endif //! #ifndef tutBiasConfig_testAllBaseMetrics */
	
	  s_distributionTypesC_t rand_1 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, center_index);
	  //s_distributionTypesC_t rand_2 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, center_index);	    


	  //! Initaite the histogram-matrix
	  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(/*nrows=*/dimensions_cnt, /*ncols=*/config_cnt_histogram_bins);
	  //!
	  //! Construct vectors, and for each vector comptue scores:
	  for(uint dim_index = dimensions_cnt_start; dim_index < dimensions_cnt; dim_index++) {
	    //! Note: the below "srand(0)" step is important: it ensures that the random values are the same in the beginning of each radnomied sample-trial.
	    // FIXME: what is the effect of 'fixing' the randomzaiton for each run? do we observe an icnrease in prediciton-variance if the andomzaiton is not fixed? If so, may this prediciton-differences (attributable to differrent randomized vectors) be used to assess the acuray-threshold of predcitons relating to randomzied sampling?
	    // FIXME[paper]: describe the differnet ranozmaiton-strategies we apply (for this testing) ... does the randomization-types give rise to different predictions?
	    if(globalConfig_sRandReset)  {
	      srand(0);
	    }
	    //! 
	    const uint ncols = (dim_index+1)*dimensions_stepSize;
	    //! Initaite the local placeholder for the results, as used as input to the result-matrix:
	    s_kt_matrix_t mat_scores = initAndReturn__s_kt_matrix(/*nrows=*/cnt_times_random, ncols);

	    //! Get inputs for the histogram
	    for(uint rand_trial = 0; rand_trial < cnt_times_random; rand_trial++) {
	      //! Construct the random vector, ie, load randomness:
	      //s_kt_randomGenerator_vector_t rand_1 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	      //s_kt_randomGenerator_vector_t rand_2 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
	      //! Call randomness:
	      //randomassign__speicficType__math_generateDistributions(&rand_1);
	      //randomassign__speicficType__math_generateDistributions(&rand_2);	    
	      //! 
	      //! Compute the similairty:
	      s_kt_list_1d_float_t vec_scores_1 = get_values_s_distributionTypesC_t(&rand_1, ncols);
	      for(uint b = 0; b < vec_scores_1.list_size; b++) {
		mat_scores.matrix[rand_trial][b] = vec_scores_1.list[b];
	      }
	      free__s_kt_list_1d_float_t(&vec_scores_1);
	    }

	    //!
	    //! Step:
	    // FIXME: use below:
	    allocOnStack__char__sprintf(2000, str, "%s-%u", getStringOfDistribution_s_distributionTypesC(&rand_1), center_index);
	    //! Step: Apply: Get topo-prop, and update result-file:
	    const char *stringOf_tagSample = str;
	    to1_analyze_matrix(self, &mat_scores, stringOf_tagSample); //, obj_metric);    

	    //! Free:
	    free__s_kt_matrix(&mat_scores);
	  }
	}
      }
    }
  }
  //!
  //! ----------------------------------------------------------
  //!  
  if(false) //! Note: given the construction of "s_hp_clusterShapes_t", the below syn-data has a fonstant/fixed mean-value.
    { //! Task: generate different coveriance-clsuter-partions:
      uint cnt_clusters = 2;
      const uint nrows = 100;    
      //const t_float score_strong = 100;      const t_float score_weak = 1;
      const t_float score_strong = 1;      const t_float score_weak = 100;
      const uint cnt_data_iterations_spec = 20; //! where this attribute needs to be fixed when analsysing hytpsis, ie, to simplify the engerration of tailore-made hytpsos to compare with.
      //for(uint nrows = 10; nrows < 100; nrows += 10)
      {
	for(uint type_id = 0; type_id < e_eval_ccm_groupOf_artificialDataClass_undef; type_id++) {
	  //!
	  //!
	  //! Explore diffenret cluster-midpoints:
	  //! Set the iteraiton-block-configuraiton:
	  assert(cnt_data_iterations_spec > 0);
	  uint cnt_data_iterations = cnt_data_iterations_spec;
	  if(cnt_data_iterations >= nrows) {
	    cnt_data_iterations = nrows / 2;
	    assert(cnt_data_iterations > 0);
	  }
	  assert(cnt_data_iterations <= nrows);
	  const uint block_increment_size = (uint)((t_float)nrows / (t_float)cnt_data_iterations); 
	  assert(block_increment_size > 0);
	  assert(block_increment_size < nrows);
	  uint size_left = nrows - block_increment_size;
	  uint size_right = nrows - size_left;
	  //! Validate:
	  assert(size_left > 0);
	  assert(size_right > 0);
	  assert(size_left <= nrows);
	  assert(size_right  <= nrows);
	  //!
	  // //! Init: the list to be used when comparing the results to altatnive/possible hyptoesis. 
	  //s_kt_list_1d_float_t list_hypoLocal = init__s_kt_list_1d_float_t(cnt_data_iterations);
	  //!
	  for(uint data_id = 0; data_id < cnt_data_iterations; data_id++) {
	    const e_eval_ccm_groupOf_artificialDataClass_t type_data = (e_eval_ccm_groupOf_artificialDataClass_t)type_id; // eg,: e_eval_ccm_groupOf_artificialDataClass_minInsideMaxOutsideLinearChanging_constantToAdd100_twoNestedLinear_dynamicSplit;
	    //!
	    const char *str_topoType_base = get_string_s_eval_ccm_defineSyntMatrix_t(type_data);
	    allocOnStack__char__sprintf(2000, str_topoType, "%s-d%u", str_topoType_base, data_id);
	    s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t("syn-topo-test", nrows, score_weak, score_strong, cnt_clusters);
	    s_hp_clusterShapes_t obj_shape = generateData_s_eval_ccm_defineSyntMatrix_t(&self_artiFicialData, type_data, size_left, size_right);
	    //!
	    //! Step: Apply: Get topo-prop, and update result-file:
	    const char *stringOf_tagSample = str_topoType;
	    to1_analyze_matrix(self, &(obj_shape.matrix), stringOf_tagSample); //, obj_metric);    
	    // FIXME:
	    //! De-alloate:
	    free__s_hp_clusterShapes_t(&obj_shape);  	  
	  }
	}
      }
    }
  
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief provides a template for explroing the time-cost-effects of DBSCAN-strateiges, and its cofnigurations   
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017 and 06. dec. 2020).
   @remarks 
   -- compile: g++ -I../src/  -O2  -mssse3   -L ../src/  tut_topo_1_printDims.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_to1
**/
int main()
  #endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif

  if(false) // FIXME: remove.
  {
    //!
    const char *file_name = "r-to1-deviationAll.tsv";
    //! Init result-file:
    const bool strategy_isToApply_forAllSimMetric = false;
    s_to1_t self = init_s_to1_t(file_name, strategy_isToApply_forAllSimMetric);
    self.strategy_metric_deviation = e_to1_deviationStrategy_raw;
    
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
    //! 
    //! Apply to colelction of real-lifew and sytentic, data:
    to1_applyToFileSet(&self, obj_metric);
    //!
    free_s_to1_t(&self);
  }
  //! ---------------- 
  if(false) // FIXME: remove
  {
    //!
    const char *file_name = "r-to1-deviationHisto.tsv";
    //! Init result-file:
    const bool strategy_isToApply_forAllSimMetric = true;
    //const bool strategy_isToApply_forAllSimMetric = false;
    s_to1_t self = init_s_to1_t(file_name, strategy_isToApply_forAllSimMetric);
    self.strategy_metric_deviation = e_to1_deviationStrategy_histo;
    
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
    //! 
    //! Apply to colelction of real-lifew and sytentic, data:
    to1_applyToFileSet(&self, obj_metric);
    //!
    free_s_to1_t(&self);
  }
  //! ----------------
  {
    // FIXME: code: solve bug ... file not intiased ... and memory-eror 
    // FIXME: code: kendall .. otpiannly deactive Kendal ... generate files for both cases ... compare the resutls .... switch=metric_id_start

    //!
    // FIXME: init object seperately for each ... :
    const char *file_name_base = "r-to1-deviationHisto";
    const char *obj_globalStorage_file_name = "r-to1-globalFeatures-deviationHisto";
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
    //! Init result-file:
    const bool strategy_isToApply_forAllSimMetric = true;
    //! -------
    //! Config: <all>
    const uint nrows = 5; 
    //const uint nrows = 100; 
    //! -------
    //! Config: func-noise:
    const uint y_adjustment_index_size = 5;
    //! -------
    //! Config: data-distrubionts: 
    const uint center_relative_max = 10; //! which is used as the center-point when generating randomzied functions (Eq. \ref{}) .... Eq.: $nrows / center$ ....
    //! -------
    //const bool strategy_isToApply_forAllSimMetric = true;
    //const bool strategy_isToApply_forAllSimMetric = false;

    /* FIXME: competel below .,.. then make sue of thsi ... */
    const uint obj_globalStorage_cntTotal = nrows
      * (e_kt_normType_undef + 1)
      * (
	 s_dataStruct_matrix_dense_typeOf_sampleFunction_undef
	 + (s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef * s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef * y_adjustment_index_size )
	 + (e_distributionTypesC_undef * center_relative_max) // * (dimensions_cnt - dimensions_cnt_start) *
	 )
	 ;
    //! Init: the object
    s_to1_StorageMatrix_t obj_globalStorage = init_s_to1_StorageMatrix_t(/*nrows=*/obj_globalStorage_cntTotal, /*use_all_simMetrics=*/true); //setToEmpty_s_to1_StorageMatrix_t();
    // OK: FIXME: write: new merge-function ... a) add data, b) set row-name to data_label ... named "addFeatures_toMatrix_s_to1_StorageMatrix_t(...)".
    // OK: FIXME: write: export-function ... then call this
    //
    // FIXME: test-exec: correct static sice of matrix to exprot ot 
    // FIXME: test-exec:
    // FIXME: test-exec:     
    // FIXME: write: sim-dist-func + nroamlsiaiotn-eprmtaution .. 
    // FIXME: write:
    // 
    // FIXME: test-exec: results are exported
    // FIXME: test-exec: results are meaninful
    // FIXME: test-exec:
    // FIXME: test-exec:
    // FIXME: test-exec:     
    // FIXME: write: 
    // FIXME: write:     
    //obj_globalStorage.mat = initAndReturn__s_kt_matrix(/*nrows=*/, ncols);
    // FIXME: 
#define MiF_addData_toMatrix(mat_features, data_label)  ({	\
	 assert(mat_features.nrows == nrows); \
	 printf("nrows:%u, total:%u, remaining:%u, at %s:%d\n", mat_features.nrows, obj_globalStorage_cntTotal, cnt_notUsedRows_s_to1_StorageMatrix_t(&obj_globalStorage), __FILE__, __LINE__); \
	 assert(mat_features.nrows < cnt_notUsedRows_s_to1_StorageMatrix_t(&obj_globalStorage)); /*! ie, what we expect for cosnstency.*/ \
      /*addFeatures_toMatrix_s_to1_StorageMatrix_t(&obj_globalStorage, &mat_features, data_label); *//* FIXME: remove this line-callcompetel below:*/		\
	 obj_globalStorage.current_row += mat_features.nrows; /* FIXME: remove this, and include above!:*/ \
	   })
    
    // FIXME: extend: ... Kendall seems to be aocnsitencly an extrme outcome ... add support ... for iteraitng acorss differnet post-processing-steps .... update all functiosn making use of: e_kt_correlationFunction_undef ... <-- get wroking ... then: doe sthe extrem-outecomes change?
    // FIXME: 
    // FIXME: extend: ... when this is working ... merge the results (ie, treating then as a 'complete' ensamble) ... <-- a better strategy to comrpehend information? <-- manual investiaiotn?
    // FIXME: extend: ...     
    // FIXME: extend: ...     
    // FIXME:         

    //!
    //!   
    // FIXME:
    // FIXME:     
    //! 
    //! Apply to colelction of real-lifew and sytentic, data:
    // to1_applyToFileSet(&self, obj_metric);
    {
      // FIXME: move below into a new '.c' file ... updae our 'tut-topo-2'.
#define MiF_call() ({ \
	for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) { assert(mat_collection.list[data_id].nrows > 0); } \
	  to1_analyze_setOf_matrix(&self, obj_metric, mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix__dataSubset); })
      /**
	 @brief a header-colleciton of logics to apply before each iteration-step.
	 @future 
	 -- coding-style: an leratvie strategy is to fork out this into a class-style-arhciecutre
      **/
#define __MiF_apply_beforeIteration					\
      e_kt_normType_t enum_norm = (e_kt_normType_t)norm_id;		\
      const char *str_norm = "rawData";					\
      if(norm_id != e_kt_normType_undef) {				\
	str_norm = get_str__humanReadable__e_kt_normType_t(enum_norm);	\
      }									\
      allocOnStack__char__sprintf(2000, str_class, "%s-%s", str_class_base, str_norm); \
      allocOnStack__char__sprintf(2000, file_name, "%s-%s.tsv", file_name_base, str_class); \
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s", str_class); \
      allocOnStack__char__sprintf(2000, file_name_resultPrefix, "%s-%s-variance", file_name_base, str_class); \
      /*! Init result-file:*/						\
      s_to1_t self = init_s_to1_t(file_name, strategy_isToApply_forAllSimMetric); \
      self.strategy_metric_deviation = e_to1_deviationStrategy_histo;	\
      /*! Intalize result-matrix:*/					\
      /*FIXME: is below 'nrows' proerpty correctly itnatied?*/ \
      s_to1_StorageMatrix_t storage_matrix = init_s_to1_StorageMatrix_t(/*nrows=*//*nrows*/mapOf_realLife_size, /*use_all_simMetrics=*/true); \
      self.storage_matrix                  = &storage_matrix;		\
      /**/								\
      s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0); \
      s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t(); \
      /**/								\
      uint data_id = 0;							\

      /**
	 @brief a tail-colleciton of logics to apply after each iteration-step.
	 @future 
	 -- coding-style: an leratvie strategy is to fork out this into a class-style-arhciecutre
      **/      
#define __MiF_apply_afterIteration					\
      /*! Apply logics: */						\
	MiF_call();							\
	/*! De-allocate:*/						\
	free__s_kt_matrix_setOf_t(&mat_collection);			\
	free__s_kt_list_1d_string(&arrOf_stringNames);			\
	free_s_to1_t(&self);						\
	free_s_to1_StorageMatrix_t(&storage_matrix, file_name_resultPrefix); \


      //! 
      //! Add: matrix:
#define __MiF_apply_forEachFile						\
      assert(mat_collection.list[data_id].nrows == 0); /*! ie, to avoid the need for de-allocation.*/ \
      assert(data_id < mapOf_realLife_size);				\
      if(norm_id != e_kt_normType_undef) {				\
      mat_collection.list[data_id] = mat_data; /*! ie, copy the cotnent.*/ \
      MiF_addData_toMatrix(mat_data, tag); /*ie, to construct a library of lines to compare*/ \
    } else {								\
      s_kt_matrix_t mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_data, enum_norm); \
      mat_collection.list[data_id] = mat_norm; /*! ie, copy the cotnent.*/ \
      MiF_addData_toMatrix(mat_norm, tag); /*ie, to construct a library of lines to compare*/ \
      /*! De-allocate:*/						\
      free__s_kt_matrix(&mat_data);					\
    }									\
      /*! Add: string: */						\
      set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag); \
      /*! Increment:*/							\
      data_id++;							\


      // ******
      //!
      { //! Test for discrete/seggregated cases (wie, for cases where we do NOT mix different functions.
	// FIXME(future): ... if a seggreation is well-defined ... then explroe the effects of combining different data-tyeps <-- how are these results to be analysed?
	//!
	//const uint nrows = 100; 
	const uint ncols = 100;
	//const char *str_class = "discrete-functions";

	// FIXME: how to usmmarize the below resutls (eg, intio a signle row for eahc result-atmrix)?
	
	//!
	//! Apply: Data-norm-layer:
	// FIXME: paper: ... nromaliaiton: does the chocie of dat-anroamsiaotn affects the entropy? relationship etween best---worst sim-emtric VS data-normaliation?
	// FIXME: paper: ... what combiantiosn to explore? ... how tos elect (atomcially?) which comaiton to explore? % FIXME: is the assumption of 'increased entropy through metric-tuning (eg, selective/accurate use of similiarty-meitrcs)' the research-hyptossi explroed in this work?
	// FIXME: paper::case=dim): ... these data-sets are seprated ... each data represents a well-deied dimsnainlity ... the ?? establisehd assumption is that different strategies are needed to segregate the data from each otehr ... correcvt? 
	// FIXME: paper::case=dim): ...
	// FIXME: paper::case=dim): ...
	// FIXME: paper::case=dim): ...
	// FIXME: paper::case=dim): ... 
	// FIXME: paper::case=dim):
	// FIXME: paper::case=dim):	
	// %%%%%%%%%%%%%% FIXME: code: how to print out: [given data-data-type=X][choose simMetric=X]? <-- what data-transoations are needed? ... % FIXME: could we use the existing strategy for data-transofrmation?
	// %%%%%%%%%%%%%% FIXME: code: 
	// %%%%%%%%%%%%%% FIXME: code: 
	// %%%%%%%%%%%%%% FIXME: 
	// %%%%%%%%%%%%%% FIXME: 
	// FIXME: paper(data-func x data-func): ... if we write out reccomendations for data-types based on the simliarty-metric ... does the results make sense? are the results (eg, reccomended metrics) strongly related to the the anlaysed data?
	// FIXME: paper(data-func x data-func): ... 
	// FIXME: paper(data-func x data-func): ... 
	// FIXME: paper(data-func x data-func): ... 
	// FIXME: paper: 		
	// FIXME: paper: 		
	// FIXME: paper: 		
	// FIXME: paper: 		
	for(uint norm_id = 0; norm_id <= e_kt_normType_undef; norm_id++) {
	  { //! Use-case: Case: Possible to seperate data based on differences in entropy between: base-functions:
	    const char *str_class_base = "discrete-functions";
	    //! Set dimensions, and intiate object:
	    const uint mapOf_realLife_size = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef;
	    __MiF_apply_beforeIteration
	      //! 
	      //! Iterate through base-types:
	      for(uint type_func = 0; type_func < s_dataStruct_matrix_dense_typeOf_sampleFunction_undef; type_func++) {
		//! Init-data:
		s_kt_matrix_t mat_data = initAndReturn__s_kt_matrix(nrows, ncols);
		//!
		//! Set string:
		const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function = (s_dataStruct_matrix_dense_typeOf_sampleFunction_t)type_func;
		allocOnStack__char__sprintf(2000, tag, "%s", static_getStrOf_enum_function(typeOf_function));	      
		//! 
		//! Construct data:
		static__constructSample_data_by_function__s_dataStruct_matrix_dense_t(mat_data.matrix, nrows, /*row_startPos=*/0, ncols, /*column_startPos=*/0, typeOf_function);
		//! 
		//! Add: matrix:
		__MiF_apply_forEachFile
		  }
	    //! Evaluate this data-collection:
	    __MiF_apply_afterIteration	  
	      }
	  { //! Use-case: 
	    const char *str_class_base_base = "discrete-NoiseFunctions";
	    for(uint type_levelNoise = 0; type_levelNoise < s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef; type_levelNoise++) {	    
	      //! FIXME: explore different configaurions of: y_adjustment_index
	      for(uint y_adj = 0; y_adj < y_adjustment_index_size; y_adj++) {
		t_float y_adjustment_index = T_FLOAT_MAX;
		if(y_adj != 0) {
		  y_adjustment_index = 0.5 + y_adj; //! eg, where '1.5' approxmiates 0.5*PI
		}
		const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise = (s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t)type_levelNoise;
		//const char *str_class_base_base = "discrete-NoiseFunctions";
		const char *str_noiseLevel = static_getStrOf_enum_noise(typeOf_noise);
		if(typeOf_noise == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low) { str_noiseLevel = "low";} //! ie, to simplify/improve readaiblity.
		allocOnStack__char__sprintf(2000, str_class_base, "%s-%s-adjCase%u", str_class_base_base, str_noiseLevel, y_adj);	      
		//! Set dimensions, and intiate object:
		const uint mapOf_realLife_size = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef;
		__MiF_apply_beforeIteration
		  //! 
		  //! Iterate through base-types:
		  for(uint type_func = 0; type_func < s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef; type_func++) {
		    //! 
		    //! Init-data:
		    s_kt_matrix_t mat_data = initAndReturn__s_kt_matrix(nrows, ncols);
		    //!
		    //! Set string:
		    const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function = (s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t)type_func;
		    allocOnStack__char__sprintf(2000, tag, "%s", static_getStrOf_enum_NoiseFunction(typeOf_function));	      
		    //! 
		    //! Construct data:
		    static__constructSample_data_by_function_noise(mat_data.matrix, nrows, /*row_startPos=*/0, ncols, /*column_startPos=*/0, typeOf_function, typeOf_noise, y_adjustment_index);
		    //! 
		    //! Add: matrix:
		    __MiF_apply_forEachFile
		      }
		//! Evaluate this data-collection:
		__MiF_apply_afterIteration	  
		  }
	    }
	  }
	}
      }
#undef __MiF_apply_beforeIteration
#undef __MiF_apply_afterIteration
#undef __MiF_apply_forEachFile
#undef MiF_call
    }

    //*******************************************************
    { //! case=dim: Data: evalaute for different dimensions:


    
      // const uint nrows = 100; 
      const uint ncols = 100;	
      s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
      const bool strategy_isToApply_forAllSimMetric = true;
      const char *file_name_base = "r-to1-deviationHisto";
      const char *str_class_base = "discrete-NoiseFunctions";
    
#define MiF_call() ({ \
	for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) { assert(mat_collection.list[data_id].nrows > 0); } \
	to1_analyze_setOf_matrix(&self, obj_metric, mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix__dataSubset); })
      /**
	 @brief a header-colleciton of logics to apply before each iteration-step.
	 @future 
	 -- coding-style: an leratvie strategy is to fork out this into a class-style-arhciecutre
      **/
      //      /*s_to1_StorageMatrix_t storage_matrix = init_s_to1_StorageMatrix_t(/*nrows=*/nrows, /*use_all_simMetrics=*/true);*/ 
#define __MiF_apply_beforeIteration					\
      e_kt_normType_t enum_norm = (e_kt_normType_t)norm_id;		\
      const char *str_norm = "rawData";					\
      if(norm_id != e_kt_normType_undef) {				\
	str_norm = get_str__humanReadable__e_kt_normType_t(enum_norm);	\
      }									\
      allocOnStack__char__sprintf(2000, str_class, "%s-%s", str_class_base, str_norm); \
      allocOnStack__char__sprintf(2000, file_name, "%s-%s.tsv", file_name_base, str_class); \
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s", str_class); \
      allocOnStack__char__sprintf(2000, file_name_resultPrefix, "%s-%s-variance", file_name_base, str_class); \
      /*! Init result-file:*/						\
      s_to1_t self = init_s_to1_t(file_name, strategy_isToApply_forAllSimMetric); \
      self.strategy_metric_deviation = e_to1_deviationStrategy_histo;	\
      /*! Intalize result-matrix:*/					\
      s_to1_StorageMatrix_t storage_matrix = init_s_to1_StorageMatrix_t(/*nrows=*//*nrows*/mapOf_realLife_size, /*use_all_simMetrics=*/true); \
      self.storage_matrix                  = &storage_matrix;		\
      /**/								\
      s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0); \
      s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t(); \
      /**/								\
      uint data_id = 0;							\

      /**
	 @brief a tail-colleciton of logics to apply after each iteration-step.
	 @future 
	 -- coding-style: an leratvie strategy is to fork out this into a class-style-arhciecutre
      **/      
#define __MiF_apply_afterIteration					\
      /*! Apply logics: */						\
	MiF_call();							\
	/*! De-allocate:*/						\
	free__s_kt_matrix_setOf_t(&mat_collection);			\
	free__s_kt_list_1d_string(&arrOf_stringNames);			\
	free_s_to1_t(&self);						\
	free_s_to1_StorageMatrix_t(&storage_matrix, file_name_resultPrefix); \


      //! 
      //! Add: matrix:
#define __MiF_apply_forEachFile						\
      assert(mat_collection.list[data_id].nrows == 0); /*! ie, to avoid the need for de-allocation.*/ \
      assert(data_id < mapOf_realLife_size);				\
      assert(mat_data.nrows > 0); \
      if(norm_id != e_kt_normType_undef) {				\
      mat_collection.list[data_id] = mat_data; /*! ie, copy the cotnent.*/ \
    } else {								\
      s_kt_matrix_t mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_data, enum_norm); \
      assert(mat_norm.nrows > 0); \
      mat_collection.list[data_id] = mat_norm; /*! ie, copy the cotnent.*/ \
      /*! De-allocate:*/						\
      free__s_kt_matrix(&mat_data); \
      assert(mat_norm.nrows > 0); \
    }									\
      /*! Add: string: */						\
      set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag); \
      assert(mat_collection.list[data_id].nrows > 0); \
      /*! Increment:*/							\
      data_id++;							\




      const uint config_cnt_histogram_bins = 10;
      const uint dimensions_cnt_start = 19;
      const uint dimensions_cnt = 20;
      const uint dimensions_stepSize = 200;
      const bool globalConfig_sRandReset = false; //! which if set to 'true' implies that each vector gets the same randomzied values (hence, increases accuracy of predciton-comparison).    
      const uint cnt_times_random = 100; //! ie, the 'nrows' property.
      //!
    
      for(uint norm_id = 0; norm_id <= e_kt_normType_undef; norm_id++) {
	{ //! Use-case: Case: Possible to seperate data based on differences in entropy between: base-functions:
	  const char *str_class_base_base = "distributions";	  
	  for(uint dist_id = 0; dist_id < e_distributionTypesC_undef; dist_id++) {	    
	    //! Set dimensions, and intiate object:
	    const uint mapOf_realLife_size =  center_relative_max *  (dimensions_cnt - dimensions_cnt_start);
	    s_distributionTypesC_t rand_1 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, /*center_index=*/0);
	    allocOnStack__char__sprintf(2000, str_class_base, "%s-%s", str_class_base_base, getStringOfDistribution_s_distributionTypesC(&rand_1));

	    __MiF_apply_beforeIteration
	      //!
	      //!
	      for(uint center_relative = 1; center_relative <= center_relative_max; center_relative++) {
		//for(uint center_relative = 1; center_relative <= center_relative_max; center_relative++, mat_result_index++) {
		//for(uint rand_id = 0; rand_id < setOfRandomNess_size; rand_id++) {      
		//! Construct: the object holding the randomizatin-distributions:
		const uint ncols = config_cnt_histogram_bins;
		const uint center_index = (uint)(/*center=*/(t_float)ncols/(t_float)center_relative);

		// const e_kt_randomGenerator_type_t type_randomness_in = setOfRandomNess[rand_id];
		//! Traverse different types of simliarty-metrics:
		//for(uint i = 0; i < list_metrics_size; i++, mat_corrAll_index++)
		{
		  //assert(mat_corrAll_index < mat_corrAll.nrows);
		  //printf("\t(info) rand=%u/%u, metric=%u/%u, at %s:%d\n", dist_id, e_distributionTypesC_undef, i, list_metrics_size, __FILE__, __LINE__);
		  /* #ifndef tutBiasConfig_testAllBaseMetrics */
		  /* 	  const s_kt_correlationMetric_t curr_metric = list_metrics[i]; */
		  /* #else */
		  /* 	  const s_kt_correlationMetric_t curr_metric = initAndReturn__s_kt_correlationMetric_t(/\*metric=*\/(e_kt_correlationFunction_t)i, /\*prestep=*\/list_metrics_preStep); */
		  /* #endif //! #ifndef tutBiasConfig_testAllBaseMetrics */
		  
		  s_distributionTypesC_t rand_1 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, center_index);
		  //s_distributionTypesC_t rand_2 = init_s_distributionTypesC((e_distributionTypesC_t)dist_id, center_index);	    
		  
		  
		  //! Initaite the histogram-matrix
		  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(/*nrows=*/dimensions_cnt, /*ncols=*/config_cnt_histogram_bins);
		  //!
		  //! Construct vectors, and for each vector comptue scores:
		  for(uint dim_index = dimensions_cnt_start; dim_index < dimensions_cnt; dim_index++) {
		    //! Note: the below "srand(0)" step is important: it ensures that the random values are the same in the beginning of each radnomied sample-trial.
		    // FIXME: what is the effect of 'fixing' the randomzaiton for each run? do we observe an icnrease in prediciton-variance if the andomzaiton is not fixed? If so, may this prediciton-differences (attributable to differrent randomized vectors) be used to assess the acuray-threshold of predcitons relating to randomzied sampling?
		    // FIXME[paper]: describe the differnet ranozmaiton-strategies we apply (for this testing) ... does the randomization-types give rise to different predictions?
		    if(globalConfig_sRandReset)  {
		      srand(0);
		    }
		    //! 
		    const uint ncols = (dim_index+1)*dimensions_stepSize;
		    //! Initaite the local placeholder for the results, as used as input to the result-matrix:
		    s_kt_matrix_t mat_scores = initAndReturn__s_kt_matrix(/*nrows=*/cnt_times_random, ncols);
		    
		    //! Get inputs for the histogram
		    for(uint rand_trial = 0; rand_trial < cnt_times_random; rand_trial++) {
		      //! Construct the random vector, ie, load randomness:
		      //s_kt_randomGenerator_vector_t rand_1 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
		      //s_kt_randomGenerator_vector_t rand_2 = initAndReturn__s_kt_randomGenerator_vector_t(/*config__nclusters=*/config_maxValue, ncols, type_randomness_in, /*isTo_initIntoIncrementalBlocks=*/false, /*simMatrix=*/NULL);
		      //! Call randomness:
		      //randomassign__speicficType__math_generateDistributions(&rand_1);
		      //randomassign__speicficType__math_generateDistributions(&rand_2);	    
		      //! 
		      //! Compute the similairty:
		      s_kt_list_1d_float_t vec_scores_1 = get_values_s_distributionTypesC_t(&rand_1, ncols);
		      for(uint b = 0; b < vec_scores_1.list_size; b++) {
			mat_scores.matrix[rand_trial][b] = vec_scores_1.list[b];
		      }
		      free__s_kt_list_1d_float_t(&vec_scores_1);
		    }
		    
		    //!
		    //! Step:
		    // FIXME: use below:
		    allocOnStack__char__sprintf(2000, str, "%s-%u", getStringOfDistribution_s_distributionTypesC(&rand_1), center_index);
		    //! Step: Apply: Get topo-prop, and update result-file:
		    //! 
		    //! Add: matrix:
		    s_kt_matrix_t mat_data = mat_scores; //! ie, to satisfy the macro
		    assert(mat_data.nrows > 0);
		    const char *tag = str;
		    //__MiF_apply_forEachFile
		    // FIXME: include above, remove below!
		    {
		      assert(mat_collection.list[data_id].nrows == 0); /*! ie, to avoid the need for de-allocation.*/ 
		      assert(data_id < mapOf_realLife_size);				
		      assert(mat_data.nrows > 0); 
		      if(norm_id != e_kt_normType_undef) {				
			mat_collection.list[data_id] = mat_data; /*! ie, copy the cotnent.*/ 
		      } else {								
			s_kt_matrix_t mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_data, enum_norm); 
			assert(mat_norm.nrows > 0); 
			mat_collection.list[data_id] = mat_norm; /*! ie, copy the cotnent.*/ 
			/*! De-allocate:*/						
			free__s_kt_matrix(&mat_data); 
			assert(mat_norm.nrows > 0); 
		      }									
		      /*! Add: string: */						
		      set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag); 
		      assert(mat_collection.list[data_id].nrows > 0); 
		      /*! Increment:*/							
		      data_id++;			
		    }
		    //assert(mat_data.nrows > 0);
		      //! Free:
		      //free__s_kt_matrix(&mat_scores);
		  }
		}
	      }
		assert(data_id == mapOf_realLife_size);
		//! Evaluate this data-collection:
		//__MiF_apply_afterIteration
		// FIXME: include above, remove below!
		{
		  /*! Apply logics: */						
		  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
		    assert(mat_collection.list[data_id].nrows > 0);
		  } 
		  to1_analyze_setOf_matrix(&self, obj_metric, mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix__dataSubset); 
		  /*! De-allocate:*/					
		  free__s_kt_matrix_setOf_t(&mat_collection);			
		  free__s_kt_list_1d_string(&arrOf_stringNames);			
		  free_s_to1_t(&self);						
		  free_s_to1_StorageMatrix_t(&storage_matrix, file_name_resultPrefix); 
		}
		  }	      	    
	}
      } //! end-norm:
#undef __MiF_apply_beforeIteration
#undef __MiF_apply_afterIteration
#undef __MiF_apply_forEachFile
#undef MiF_call    
    }
    //! 
    //if(obj_globalStorage.current_row > 0)
    if(obj_globalStorage.current_row > 0) { //! Analyse the glboal object:
      //! 
      //! Apply: sim-dist-func + nroamlsiaiotn-eprmtaution
      {
	// FIXME: when reuslts are meaningful ... move this block to a new tut .... then call this funciton ... where params are: "mat_input, obj_globalStorage_file_name, ...."
	const s_kt_matrix_t *mat_input = &(obj_globalStorage.mat);
	  //! -------
	//! Init matrix:
	s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
	{ //! Pre: test that the data-set fits into memroy:
	  const loint cnt_rows_new = mat_input->nrows * mat_input->nrows;
	  const bool is_ok = (cnt_rows_new < (loint)UINT_MAX); //! ie, compare the data-trhesholds.
	  if(is_ok == false) {
	    fprintf(stderr, "!!\t(error)\t You request the caclcuation of rows:%u into rows x rows, which does not fit the proerpteis of teh data-contianer. f this message sound cofnsuing, and your are itnerested in fixing this, then please cotnact the dclopers of the hpLysis software. Observviaotn at [%s]:%s:%d\n", mat_input->nrows, __FUNCTION__, __FILE__, __LINE__);
	  } else {
	    //! allcoate: 
	    mat_result = initAndReturn__s_kt_matrix(/*nrows=*/to1_getCnt_simMetrics(), /*ncols=*/(uint)cnt_rows_new);
	    { //! Intiate the column-header:
	      uint col_id = 0;
	      //!
	      for(uint data_1 = 0; data_1 < mat_input->nrows; data_1++) {
		//! Get
		const char *str_1 = getAndReturn_string__s_kt_matrix(mat_input, /*index=*/data_1, /*addFor_column=*/false);
		assert(str_1);
		assert(strlen(str_1));
		//!
		for(uint data_2 = 0; data_2 < mat_input->nrows; data_2++) {
		  //! Get
		  const char *str_2 = getAndReturn_string__s_kt_matrix(mat_input, /*index=*/data_2, /*addFor_column=*/false);
		  assert(str_2);
		  assert(strlen(str_2));
		  //! Join:
		  allocOnStack__char__sprintf(2000, str_obj, "%s -- %s", str_1, str_2);		  
		  //! Set:
		  set_stringConst__s_kt_matrix(&mat_result, /*index=*/col_id, str_obj, /*addFor_column=*/true);
		  //!
		  col_id++;
		}
	      }
	    }
	  }
	  assert(is_ok);
	}
	
	//!
	//! Calculcate simliarty:
	{
	  uint enum_id = UINT_MAX;
	  for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
	    //{    
	    e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
	    for(uint  metric_idx = metric_id_start; metric_idx < (uint)e_kt_correlationFunction_undef; metric_idx++) {  
	      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_idx;
	      //!
	      //!
	      //! Construct the reference-string:
	      char str_obj[1000]; 	
	      { //! Set: row-header:
		char str_pre[1000]; memset(str_pre, '\0', 1000);
		// printf("typeOf_correlationPreStep=%u, at %s:%d\n", typeOf_correlationPreStep, __FILE__, __LINE__);
		if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {sprintf(str_pre, "rank");}
		else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {sprintf(str_pre, "binary");}
		else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) {sprintf(str_pre, "none");}
		else {assert(false);} //! ie, then add support for this.
		//!
		const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id);
		//!
		//! Combine 'pre' and 'metric-base';
		memset(str_obj, '\0', 1000); sprintf(str_obj, "%s %s", str_pre, str_metric);
	      }	
	      //!
	      if(MiF__isTo_evaluate_simMetric(metric_id)) {
		if(enum_id != UINT_MAX) {
		  enum_id++;
		} else {enum_id = 0;} //! ie, then the first index.
	      } else {
		//fprintf(stderr, "does NOT evaluate for %s, at %s:%d\n", str_obj, __FILE__, __LINE__);
		continue;}	
	      assert(MiF__isTo_evaluate_simMetric(metric_id) == true); //! ie, what we expect
	      { //! Set header: row:
		assert(enum_id < mat_result.nrows);
		set_stringConst__s_kt_matrix(&mat_result, /*index=*/enum_id, str_obj, /*addFor_column=*/false);		
	      }

	      //!
	      //! Configure sim-metric:
	      s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
	      obj_metric.metric_id = metric_id;
	      obj_metric.typeOf_correlationPreStep = typeOf_correlationPreStep;	      
	      //!
	      s_hp_distance__config_t local_config = init__s_hp_distance__config_t();	      
	      //!
	      //!
	      //! Apply sim-metric:
	      uint col_id = 0;
	      for(uint data_1 = 0; data_1 < mat_input->nrows; data_1++) {
		for(uint data_2 = 0; data_2 < mat_input->nrows; data_2++) {		
		  t_float *row_1 = mat_input->matrix[data_1];
		  assert(row_1);
		  //!
		  t_float *row_2 = mat_input->matrix[data_2];
		  assert(row_2);
		  //!
		  t_float scalar_result = T_FLOAT_MAX;
		  apply__rows_hp_distance(obj_metric, row_1, row_2, mat_input->ncols, &scalar_result);
		  if(scalar_result != T_FLOAT_MAX) {
		    //! Add the results to the result-matrix:
		    assert(col_id < mat_result.ncols);
		    mat_result.matrix[enum_id][col_id] = scalar_result;
		  }
		  col_id++;
		}
	      }
	    }
	  }
	}
	    
	{ //! then we export:
	  //! Transpose matrix:
	  s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_result);


	  // FIXME: is this the correct order of ... 
	  
	  //! Iterate through data-nrosaiton-strategies:
	  for(uint norm_id = 0; norm_id <= e_kt_normType_undef; norm_id++) {
	    e_kt_normType_t enum_norm = (e_kt_normType_t)norm_id;		
	    const char *str_norm = "rawData";					
	    if(norm_id != e_kt_normType_undef) {				
	      str_norm = get_str__humanReadable__e_kt_normType_t(enum_norm);	
	    }									
	    //allocOnStack__char__sprintf(2000, str_class, "%s-%s", str_class_base, str_norm); 
	    
	    //! Apply noramlization:
	    s_kt_matrix_t mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_transp, enum_norm);

	    //! -------------------
	    //!
	    //! Define the export-file-prefix:
	    allocOnStack__char__sprintf(2000, str_class, "%s-%s", obj_globalStorage_file_name, str_norm); 
	    //!
	    //! Export data
	    const bool use_all_simMetrics = true;
	    const char *file_name_resultPrefix = str_class;
	    to1_static_exportRankedSimData(&mat_norm, use_all_simMetrics, file_name_resultPrefix);
	    
	    //! De-allcote:
	    free__s_kt_matrix(&mat_norm);	  
	  }
	  //! De-allocate:
	  free__s_kt_matrix(&mat_transp);
	  free__s_kt_matrix(&mat_result);	  	  
	}
      }
    }
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
  

#undef MiF__isTo_evaluate_simMetric
//#undef MiF__isTo_evaluate_simMetric
