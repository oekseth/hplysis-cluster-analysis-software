{
  //! ----------------------------------------------------------
  assert(rowOf_id != NULL);
  uint size        = rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  if(size == UINT_MAX) {  }
  assert(size != UINT_MAX);
  //if(size == UINT_MAX) {size = 0;}
  uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
  if(current_pos == UINT_MAX) { //! then we need to searhc through the lsit fiding the 'list non-set score':
    // FIXME: vlaidte [below] assumption:
    current_pos = 0;
  }
  assert(current_pos != UINT_MAX);
  uint new_size = row_size + size + 2;
  //! ----------------------------------------------------------
  if((current_pos + /*cntToInsert=*/row_size + 2) >= size) {
    if(new_size < current_pos) {new_size = 2*current_pos;}
    const uint new_size_alloc = 64 + 2*(new_size);  const uint default_value_uint = 0;
    // printf("new_size=%u given current_pos=%u, old_size=%u and cnt-inserted=%u, at %s:%d\n", new_size_alloc, current_pos, size, row_size, __FILE__, __LINE__);
    assert(new_size_alloc >= 32); //! ie, what we expect.
    assert(new_size_alloc > current_pos);
    const uint new_size_alloc_minusTwo = new_size_alloc - 2;
    assert(current_pos < new_size_alloc_minusTwo);
    { //! Udpate wrt. the index-pointers:
      uint *row_cpy_id = allocate_1d_list_uint(new_size_alloc, default_value_uint);
      //! Copy the old data:
      memcpy(row_cpy_id, rowOf_id, sizeof(uint)*(current_pos+2));
      //! De-allcoate the old data:
      free_1d_list_uint(&rowOf_id);
      row_cpy_id[e_kt_set_sparse_colAttribute_size] = new_size_alloc_minusTwo;
      assert(new_size_alloc_minusTwo > 0);
      //! Update the poitner:
      rowOf_id = row_cpy_id;
      assert(rowOf_id);
      assert(current_pos < rowOf_id[e_kt_set_sparse_colAttribute_size]);
      // row_size = new_size_alloc;
    }
    assert(rowOf_id); assert(current_pos < rowOf_id[e_kt_set_sparse_colAttribute_size]);
    if(rowOf_scores) { //! then udpate wrt. the scores:
      const t_float default_value_float = 0;
      t_float *row_cpy_scores = allocate_1d_list_float(new_size_alloc_minusTwo, default_value_float);
      //! Copy the old data:
      memcpy(row_cpy_scores, rowOf_scores, sizeof(t_float)*(current_pos));
      //! De-allcoate the old data:
      assert((void*)rowOf_scores != (void*)rowOf_id);
      free_1d_list_float(&rowOf_scores);
      //! Update the poitner:
      rowOf_scores = row_cpy_scores;
    }
    assert(rowOf_id); assert(current_pos < rowOf_id[e_kt_set_sparse_colAttribute_size]);
  } else {
    assert(rowOf_id);
    assert(rowOf_id[e_kt_set_sparse_colAttribute_size] > 0);
    assert(current_pos < rowOf_id[e_kt_set_sparse_colAttribute_size]);
  }
  //! Update the current-pos:
  assert(row_size > 0);
  rowOf_id[e_kt_set_sparse_colAttribute_currentPos] = current_pos + row_size;
  assert(rowOf_id[e_kt_set_sparse_colAttribute_currentPos] < rowOf_id[e_kt_set_sparse_colAttribute_size]);
  assert(rowOf_id);
  assert(current_pos < rowOf_id[e_kt_set_sparse_colAttribute_size]);
  //!
  //! Add the data
#if(__localConfig__isTo__addRow == 1)
  //! Add the new data:
  memcpy(&(rowOf_id[2+current_pos]), row, row_size*sizeof(uint));
  if(rowOf_scores) { //! then udpate wrt. the scores:
    memcpy(&rowOf_scores[current_pos], row, row_size*sizeof(t_float));
  }
#else
  rowOf_id[2+current_pos] = vertex_to_add;
#ifdef __localConfig__isTo__setScore
  assert(rowOf_scores);
  rowOf_scores[current_pos] = scoreTo_set;
  //rowOf_scores[2+current_pos] = scoreTo_set;
  //printf("\n(top-element) vertex=%u w/score=%f, (given current-pos=%u) at %s:%d\n", rowOf_id[2+current_pos], rowOf_scores[current_pos], current_pos, __FILE__, __LINE__);
#endif
  //printf("\n(top-element) vertex=%u, (given current-pos=%u) at %s:%d\n", rowOf_id[2+current_pos], current_pos, __FILE__, __LINE__);
  // printf("current_pos=%u (given old-curr-pos=%u), value=%u, at %s:%d\n", rowOf_id[e_kt_set_sparse_colAttribute_currentPos], current_pos, rowOf_id[2+current_pos], __FILE__, __LINE__);
#endif
}
