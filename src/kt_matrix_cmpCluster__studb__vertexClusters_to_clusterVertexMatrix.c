if(isTo_includeWeight) {
  fprintf(stderr, "!!\t Add support for this option, eg, bty providing a distance-matrix holding the distance from each verex to the centroid-vertex (of the clsuters they belong to). Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); //! (oekseth, 06. des. 2016).
 }

uint *clusterId_toCentroid_1 = allocate_1d_list_uint(max_cntClusters, defaultValue);

//    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

//!
//! Allocate a count-array:
assert(countArray == NULL);
assert(countArray_size == 0);
const uint defaultValue_0 = 0;
countArray = allocate_1d_list_float(max_cntClusters, defaultValue_0);
countArray_size = max_cntClusters;
//printf("countArray_size=%u, at %s:%d\n", countArray_size, __FILE__, __LINE__);
assert(countArray_size > 1);

//uint *clusterId_toCentroid_2 = allocate_1d_list_uint(max_cntClusters, defaultValue);
for(uint i = 0; i < max_cntClusters; i++) {
  clusterId_toCentroid_1[i] = UINT_MAX;
  //= clusterId_toCentroid_2[i] = UINT_MAX;
  //for(uint k = 0; k < cnt_vertices; k++) {matrix_1[i][k] = matrix_2[i][k] = T_FLOAT_MAX;}
 }    
uint *mapOfCluster_local = allocate_1d_list_uint(cnt_vertices, defaultValue_0);

//    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

//!
//! 'map' a cluster-to-centrodi-mappings into a [cluster-id][vertex-id]='0|1'.
uint debug_cntVertices_withKnownClusters = 0;
for(uint vertex_id = 0; vertex_id < cnt_vertices; vertex_id++) {
  const uint center_id = vertexToCentroid[vertex_id];
  if(center_id != UINT_MAX) {
    debug_cntVertices_withKnownClusters++;
    uint cluster_id_current = UINT_MAX;
    //! Iterate through the 'list' iot. to the currnet 'mapping' to a k-means-id-clsuter:
    uint last_uintMaxCase = UINT_MAX;
    // printf("TODO: vlaidate correctness of [below] if-calsue ... ie, try testing differnet appraochses', at %s:%d\n", __FILE__, __LINE__); //! <-- seems correct (oekseth, 06. des. 2016).
    for(uint cluster_id = 0; (cluster_id < max_cntClusters) & (last_uintMaxCase == UINT_MAX); cluster_id++) {
      if(clusterId_toCentroid_1[cluster_id] != UINT_MAX) {
	if(clusterId_toCentroid_1[cluster_id] == center_id) {cluster_id_current = cluster_id;} //! ie, as we then have 'found a matching cluster-centodi-mapping'.
      } else {last_uintMaxCase = cluster_id;}
    }
    if(last_uintMaxCase != UINT_MAX) {
      if(cluster_id_current == UINT_MAX) { //! then we 'add' a enw centroid':
	assert(last_uintMaxCase != UINT_MAX);
	clusterId_toCentroid_1[last_uintMaxCase] = vertex_id;
	cluster_id_current = last_uintMaxCase;
      }
      //! Update the matrix-index-table:
      assert(cluster_id_current != UINT_MAX);
      //! TODO: consider using a 'mapping-table' to weight the different clsuter-di-memberships (eg, wrt. teh 'itnernal singifcance of vertices in each set'.
      //printf("\t\t cluster[%u] \t\t vertex=%u, at %s:%d\n", cluster_id_current, vertex_id, __FILE__, __LINE__);
      localMatrix[cluster_id_current][vertex_id] = 1;
      //!
      //! Update the cluster-count:
      t_float score_vertex = 1;
      if(isTo_includeWeight) {
	fprintf(stderr, "!!\t TODO: consider to updaitng this code-chunk to 'inlclude the weight' at this exec-point, eg, by using the vertex--centroid dsitance, at %s:%d\n", __FILE__, __LINE__);
      }
      assert(cluster_id_current != UINT_MAX);
      assert(cluster_id_current < countArray_size);
      countArray[cluster_id_current] += score_vertex;
      //!
      //! Update the lcoal-mapping-table holding the vertex-->clusterId membership:
      mapOfCluster_local[vertex_id] = cluster_id_current;
    } else {
      mapOfCluster_local[vertex_id] = UINT_MAX; //! ie, as we then assume the vertex is Not assigned to any clusters.
    }
  }      
 }
assert(debug_cntVertices_withKnownClusters > 0); //! ie, as 'this call' should otehriwse Not ahve been made
//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

//!
//! Update the max-clsuter-count:
localMaxCount__clusters = 0; //! ie, reset.
for(uint cluster_id = 0; cluster_id < max_cntClusters; cluster_id++) {
  if(clusterId_toCentroid_1[cluster_id] != UINT_MAX) {
    localMaxCount__clusters++;
  }
 }
assert(localMaxCount__clusters > 0);
countArray_size = localMaxCount__clusters; //! ie, update the variable.

//! --------------------------------------------------------------------------------------------
//!
if(matrixOf_coOccurence != NULL) {//! Update the centroid-mappings:
  //if(matrixOf_coOccurence->nrows == matrixOf_coOccurence->ncols) 
  {
    // printf("(\t init clusterDistances_cluster, given countArray_size:%u, localMaxCount__clusters:%u, at %s:%d\n", countArray_size, localMaxCount__clusters, __FILE__, __LINE__); // FIXME: remove
    init__s_kt_matrix_cmpCluster_clusterDistance(&(clusterDistances_cluster), mapOfCluster_local, cnt_vertices, cnt_vertices, localMaxCount__clusters, matrixOf_coOccurence, config);
    //init__s_kt_matrix_cmpCluster_clusterDistance(&(clusterDistances_cluster), mapOfCluster_local, countArray_size, countArray_size, localMaxCount__clusters, matrixOf_coOccurence, config);
    assert(clusterDistances_cluster.nrows != 0); 
  } //! else we assuem teh matrix is Not an adjcency-matrix.
  //assert(clusterDistances_cluster.nrows != 0);
 }
free_1d_list_uint(&mapOfCluster_local);


//!
//! De-allocate:
free_1d_list_uint(&clusterId_toCentroid_1);

#undef localMatrix
#undef vertexToCentroid
#undef localMaxCount__clusters
#undef countArray
#undef countArray_size
#undef clusterDistances_cluster
