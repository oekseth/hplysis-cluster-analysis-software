use strict;

my $tmpFile_norm  = "tmp_norm_" . rand() . ".tsv";
my $tmpFile_sim   = "tmp_sim_" . rand() . ".tsv";

my $index_fileSpace    = 0;
my $index_metricSpace = 0;

sub _cluster {
    my ($file_data, $norm_trans, $norm_metric, $metric_sim, $metric_clusterAlg) = @_;
    if(!(-f $file_data)) {
	printf(STDERR "!!\t unable fiding file=\"%s\", at %s:%d\n", $file_data, __FILE__, __LINE__);
    }
    my $cmd = "";
    #my ($norm_trans, $norm_metric, $metric_sim, $metric_clusterAlg, $metric_sim__asInputToClust, $config) = @_;    
    system("rm -f $tmpFile_norm");
    $cmd = "./x_hp_norm  -ops=" . $norm_metric . " -file1=" . $file_data . " -out=" . $tmpFile_norm; # . " -transpose=" . $norm_trans;
    print $cmd . "\n";
    system($cmd);
    #$cmd = "./x_hp_sim_dense -ops=$metric_sim -file1=$tmpFile_norm -out=$tmpFile_sim -filterRankMax=inf" . " -transpose=" . $norm_trans;
    if(-f $tmpFile_norm) {
	system("rm -f $tmpFile_sim");
	$cmd = "./x_hp -ops=$metric_sim -file1=$tmpFile_norm -out=$tmpFile_sim -filterRankMax=inf";
	print $cmd . "\n";
	system($cmd);
	if(-f $tmpFile_sim) {
	    my $file_clusters = "eval_data_" . $index_fileSpace . "_metric_" . $index_metricSpace . ".tsv";
	    system("rm -f $file_clusters");
	    $cmd = "./x_hp -ops=" . $metric_clusterAlg . "  -file1=" . $tmpFile_sim . " -out=" . $file_clusters;
	    print $cmd  . "\n";
	    system($cmd);
	}
    }
}

sub _alg_x_metric_cluster {
    $index_metricSpace = 0; #! ie, reset.    
    my ($file_data, $norm_trans) = @_;
    foreach my $metric_sim (
	"e_kt_correlationFunction_groupOf_minkowski_euclid",
  "e_kt_correlationFunction_groupOf_minkowski_euclid",
  "e_kt_correlationFunction_groupOf_minkowski_cityblock",
	"e_kt_correlationFunction_groupOf_minkowski_minkowski",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Gower",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Soergel",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Canberra",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian",
  "e_kt_correlationFunction_groupOf_intersection_WaveHedges",
  "e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt",
  "e_kt_correlationFunction_groupOf_intersection_Czekanowski",
  "e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef",
  "e_kt_correlationFunction_groupOf_innerProduct_innerProduct",
  "e_kt_correlationFunction_groupOf_innerProduct_harmonicMean",
  "e_kt_correlationFunction_groupOf_innerProduct_cosine",
  "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook",
  "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef",
  "e_kt_correlationFunction_groupOf_innerProduct_Dice",
  "e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef",
  "e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance",
  "e_kt_correlationFunction_groupOf_fidelity_fidelity",
  "e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya",
  "e_kt_correlationFunction_groupOf_fidelity_Hellinger",
  "e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef",
  "e_kt_correlationFunction_groupOf_fidelity_Matusita",
  "e_kt_correlationFunction_groupOf_squared_Euclid",
  "e_kt_correlationFunction_groupOf_squared_Pearson",
  "e_kt_correlationFunction_groupOf_squared_Neyman",
  "e_kt_correlationFunction_groupOf_squared_squaredChi",
  "e_kt_correlationFunction_groupOf_squared_probabilisticChi",
  "e_kt_correlationFunction_groupOf_squared_divergence",
  "e_kt_correlationFunction_groupOf_squared_Clark",
  "e_kt_correlationFunction_groupOf_shannon_KullbackLeibler",
  "e_kt_correlationFunction_groupOf_shannon_Jeffreys",
  "e_kt_correlationFunction_groupOf_shannon_kDivergence",
  "e_kt_correlationFunction_groupOf_shannon_Topsoee",
  "e_kt_correlationFunction_groupOf_shannon_JensenShannon",
  "e_kt_correlationFunction_groupOf_combinations_Taneja",
  "e_kt_correlationFunction_groupOf_combinations_KumarJohnson",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric",	
	"e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine"
	) {
	foreach my $norm_metric (
	    "row-Relative-Avg-Abs",
	    "row-Relative-Rank",
	    "" #! ie, none.
	    ) {
	    foreach my $metric_clusterAlg (
		#"algorithm::k-means::altAlg::miniBatch",
		"algorithm::k-means::avg",
		"algorithm::k-means::rank",
		"algorithm::k-means::medoid",
		"algorithm::hpCluster",
		"algorithm::disjoint::kdTree", "algorithm::disjoint::kdTree::CCM",
		"algorithm::HCA::single", "algorithm::HCA::max",
		"algorithm::HCA::average", "algorithm::HCA::centroid", "algorithm::Kruskal::HCA",
		"algorithm::k-means::altAlg::miniBatch",
		"algorithm::random::best"
		) {
		_cluster($file_data, $norm_trans, $norm_metric, $metric_sim, $metric_clusterAlg);
		$index_metricSpace += 1;
	    }
	}
    }
}

sub _transp {
    my ($input_file) = @_;    
    open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
    my @arr_mat = ();
    { #! Open the file: 
	my $line_count= 0;
	# my @arr_first = ();
	while (my $line = <FILE_IN>) {
	    chomp($line); #! ie, remvoe the trail-newline.   
	    my @arrOf_cols = split("\t", $line); 
#	    my @arrOf_cols = split($line, "\t");
	    #push(@matrix_input, \@arrOf_cols);
	    if($line_count == 0) {
		# printf(FILE_OUT "%s\n", $line);
		#@arrOf_rowHead = @arrOf_cols;
		#printf(STDERR "rows=\"%s\", and line=\"$line\", at %s:%d\n", join(", ", @arrOf_rowHead), __FILE__, __LINE__);
	    } else {
		if(scalar(@arrOf_cols) != 0) {
		    shift(@arrOf_cols);
		    push(@arr_mat, \@arrOf_cols);
		}
	    }
	    $line_count += 1;
	}
    }
    close(FILE_IN);
    if(scalar(@arr_mat) == 0) {croak("!!\t Did Not find any data for file=$input_file");}
    #! Write out the transposed matrix:
    $input_file = "tmp_" . rand() . ".tsv";
    my $result_file = $input_file; # . "_result.tsv";
    open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");
    print("ref=" . ref($arr_mat[0]));
    my @arr = ("#! matrix ");
    for(my $row = 0; $row < scalar(@arr_mat); $row += 1) {	    
	push(@arr, "row-" . $row);
    }
    print(FILE_OUT join("\t", @arr) . "\n");
    for(my $col = 0; $col < scalar(@{$arr_mat[0]}); $col += 1) {	    
	my @arr = ("col-" . $col);
	for(my $row = 0; $row < scalar(@arr_mat); $row += 1) {	    
	    push(@arr, $arr_mat[$row][$col]);
	}
	# print(STDOUT join("\t", @arr)  . "\n");
	print(FILE_OUT join("\t", @arr)  . "\n");
    }    
    close(FILE_OUT);
    return $result_file;
}

my @arr_files = (
"data/local_downloaded/56_msq.csv.hpLysis.tsv",
"data/local_downloaded/20_chorSub.csv.hpLysis.tsv",
"data/local_downloaded/13_pulpfiber.csv.hpLysis.tsv",
"data/local_downloaded/4_randu.csv.hpLysis.tsv",
"data/local_downloaded/32_cf.csv.hpLysis.tsv",
"data/local_downloaded/airquality.csv.hpLysis.tsv",
"data/local_downloaded/39_UScrime.csv.hpLysis.tsv",
"data/local_downloaded/46_pottery.csv.hpLysis.tsv",
"data/local_downloaded/40_Hedonic.csv.hpLysis.tsv",
"data/local_downloaded/38_Melanoma.csv.hpLysis.tsv",
"data/local_downloaded/49_affect.csv.hpLysis.tsv",
"data/local_downloaded/54_Holzinger.csv.hpLysis.tsv",
"data/local_downloaded/47_smoking.csv.hpLysis.tsv",
"data/local_downloaded/0_airquality.csv.hpLysis.tsv",
"data/local_downloaded/51_bfi.csv.hpLysis.tsv",
"data/local_downloaded/19_Hartnagel.csv.hpLysis.tsv",
"data/local_downloaded/1_attitude.csv.hpLysis.tsv",
"data/local_downloaded/37_gilgais.csv.hpLysis.tsv",
"data/local_downloaded/42_cancer.csv.hpLysis.tsv",
"data/local_downloaded/52_burt.csv.hpLysis.tsv",
"data/local_downloaded/21_votes.repub.csv.hpLysis.tsv",
"data/local_downloaded/attitude.csv.hpLysis.tsv",
"data/local_downloaded/57_msq.csv.hpLysis.tsv",
"data/local_downloaded/33_Arbuthnot.csv.hpLysis.tsv",
"data/local_downloaded/3_LifeCycleSavings.csv.hpLysis.tsv",
"data/local_downloaded/45_phosphate.csv.hpLysis.tsv",
"data/local_downloaded/8_alcohol.csv.hpLysis.tsv",
"data/local_downloaded/31_aldh2.csv.hpLysis.tsv",
"data/local_downloaded/43_pbc.csv.hpLysis.tsv",
"data/local_downloaded/23_Airline.csv.hpLysis.tsv",
"data/local_downloaded/l.hpLysis.tsv",
"data/local_downloaded/62_Nursing.csv.hpLysis.tsv",
"data/local_downloaded/16_wood.csv.hpLysis.tsv",
"data/local_downloaded/5_rock.csv.hpLysis.tsv",
"data/local_downloaded/50_affect.csv.hpLysis.tsv",
"data/local_downloaded/34_Cavendish.csv.hpLysis.tsv",
"data/local_downloaded/36_Boston.csv.hpLysis.tsv",
"data/local_downloaded/44_heptathlon.csv.hpLysis.tsv",
"data/local_downloaded/14_salinity.csv.hpLysis.tsv",
"data/local_downloaded/60_Fertility.csv.hpLysis.tsv",
"data/local_downloaded/41_bladder.csv.hpLysis.tsv",
"data/local_downloaded/59_barro.csv.hpLysis.tsv",
"data/local_downloaded/53_epi.bfi.csv.hpLysis.tsv",
"data/local_downloaded/iris.data.hpLysis.tsv",
"data/local_downloaded/35_environmental.csv.hpLysis.tsv",
"data/local_downloaded/17_neuro.csv.hpLysis.tsv",
"data/local_downloaded/12_NOxEmissions.csv.hpLysis.tsv",
"data/local_downloaded/2_crimtab.csv.hpLysis.tsv",
"data/local_downloaded/63_Pines.csv.hpLysis.tsv",
"data/local_downloaded/15_toxicity.csv.hpLysis.tsv",
"data/local_downloaded/27_Cigarette.csv.hpLysis.tsv",
"data/local_downloaded/9_ambientNOxCH.csv.hpLysis.tsv",
"data/local_downloaded/28_Crime.csv.hpLysis.tsv",
"data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv",
"data/local_downloaded/0_phosphate.csv.hpLysis.tsv",
"data/local_downloaded/24_BudgetItaly.csv.hpLysis.tsv",
"data/local_downloaded/7_USJudgeRatings.csv.hpLysis.tsv",
"data/local_downloaded/11_milk.csv.hpLysis.tsv",
"data/local_downloaded/6_swiss.csv.hpLysis.tsv",
"data/local_downloaded/26_Cigar.csv.hpLysis.tsv",
"data/local_downloaded/55_msq.csv.hpLysis.tsv",
"data/local_downloaded/29_Electricity.csv.hpLysis.tsv",
"data/local_downloaded/22_nuts.csv.hpLysis.tsv",
"data/local_downloaded/18_Ginzberg.csv.hpLysis.tsv",
"data/local_downloaded/48_affect.csv.hpLysis.tsv",
"data/local_downloaded/30_Forward.csv.hpLysis.tsv",
"data/local_downloaded/10_coleman.csv.hpLysis.tsv",
"data/local_downloaded/58_neo.csv.hpLysis.tsv",
"data/local_downloaded/25_BudgetUK.csv.hpLysis.tsv",
    );

foreach my $file_name (@arr_files) {     
    #my $file_name = "./data/local_downloaded/56_msq.csv.hpLysis.tsv";
#    if($index_fileSpace > 32) 
    { #! ie, to avoid computaiton of previosu cases.
	my $file_transp = 1;
	if($file_transp) {
	    $file_name = _transp($file_name);
	    print("trans-file: " . $file_name . "\n");
	    if(!(-f $file_name)) {
		print("!!\t Unable to find file-name: trans-file: " . $file_name . "\n");
	    }
	}
	# exit;
	my $norm_trans = 0;
	_alg_x_metric_cluster($file_name, $norm_trans);
    }
    $index_fileSpace    += 1;
}
