#ifndef fast_log_approxFloatValues_h
#define fast_log_approxFloatValues_h

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


/**

   // FIXME[jc]: may you validate correctness of [below]?

 * Absolute error bounded by 1e-6 for normalized inputs
 Returns a finite number for +inf input
   Returns -inf for nan and <= 0 inputs.
   Continuous error. 
   //! Src: "https://github.com/jhjourdan/SIMD-math-prims/blob/master/simd_math_prims.h"
... Generated in Sollya using :
    > f = remez(log(x)-(x-1)*log(2),
            [|1,(x-1)*(x-2), (x-1)*(x-2)*x, (x-1)*(x-2)*x*x,
              (x-1)*(x-2)*x*x*x|], [1,2], 1, 1e-8);
    > plot(f+(x-1)*log(2)-log(x), [1,2]);
    > f+(x-1)*log(2)

    ... 89.970756366f = 127 * log(2) - constant term of polynomial
*/
#define logapprox(val) ({ union { t_float f; int i; } valu; valu.f = val; t_float exp = valu.i >> 23; \
  t_float addcst = (val > 0) ? -89.970756366f : T_FLOAT_MIN; valu.i = (valu.i & 0x7FFFFF) | 0x3F800000; t_float x = valu.f; \
  x * (3.529304993f + x * (-2.461222105f + x * (1.130626167f + x * (-0.288739945f + x * 3.110401639e-2f)))) + (addcst + 0.69314718055995f*exp);})
#define logapprox_gt_one(val) ({ union { t_float f; int i; } valu; valu.f = val; t_float exp = valu.i >> 23; \
  t_float addcst =  -89.970756366f; valu.i = (valu.i & 0x7FFFFF) | 0x3F800000; t_float x = valu.f; \
  x * (3.529304993f + x * (-2.461222105f + x * (1.130626167f + x * (-0.288739945f + x * 3.110401639e-2f)))) + (addcst + 0.69314718055995f*exp);})


#if (mineAccuracy_computeLog_useAbs == 1) //! then we assume that 'taking' the "abs' is correct wrt. corrrectness of the resutls (ie, wrt. the applciaotn of the algorithm):
//! ---------------------------
#define log_range_vector_get_memoryLocation(vec) ({vec;})
#define log_range_vector_get_memoryLocation_abs(vec) ({vec;})
// TODO: if we choose to use ["elo]ł, then update our itnristnictc calls.
#define log_range_0_one(val) (logapprox_gt_one(abs(val)))
#define log_range_0_one_abs(val) (logapprox(abs(val)))
#define log_range_0_one_inputIsAdjusted(val) (logapprox_gt_one(abs(val)))
#define log_range_0_one_inputIsAdjusted_abs(val) (logapprox(abs(val)))
//! ---------------------------***************-----------------------

#else //! then we use the 'naive' approach found in the "mine" software:
//! ---------------------------
#define log_range_vector_get_memoryLocation(vec) ({vec;})
#define log_range_vector_get_memoryLocation_abs(vec) ({vec;})
// TODO: if we choose to use ["elo]ł, then update our itnristnictc calls.
#define log_range_0_one(val) (logapprox_gt_one(val))
#define log_range_0_one_abs(val) (logapprox(val))
#define log_range_0_one_inputIsAdjusted(val) (logapprox_gt_one(val))
#define log_range_0_one_inputIsAdjusted_abs(val) (logapprox(val))
//! ---------------------------***************-----------------------

#endif

//! End of File (EOF)
#endif //! EOF
