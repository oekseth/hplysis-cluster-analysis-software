/**
   @brief apply lgocis to copmute for Dynamic k-means, supprotfing both 'hca' and k-emans-permtautions (oekjseth, 06. mar. 2017).
 **/

  assert(self);
  s_kt_clusterAlg_config__ccm config__CCM = self->config.config__CCM__dynamicKMeans;
  if(config__CCM.isTo_pply__CCM == false) {return true;}
  if(config__CCM.k_clusterCount__min >= config__CCM.k_clusterCount__max) {
    fprintf(stderr, "!!\t An inconsistnecy in the CCM-configuraiton: k-count=[%u, %u], ie, please udpate by reading the docuemtaiton and tut-exampesl. However, if latter does Not help, then contact senior devleoper [oesketh@gmail.com]. Observiaont at [%s]:%s:%d\n", config__CCM.k_clusterCount__min, config__CCM.k_clusterCount__max, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, a compilatior-optmaizaiton
    return false;
  }
//! 
  //! By default always 'free':
  free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean));

  //!
  //! Inlitaze the vlaue using an 'CCM-specfic' worst-case-vlaue':
  t_float currentResult = get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(&config__CCM);
//!
//! Avoid/ensure that we 'do not evalaute more clusters than possible diemsions of the data-set:
assert(config__CCM.matrix_unFiltered);
assert(config__CCM.matrix_unFiltered->nrows > 0);
const uint nrows_half = (uint)((t_float)config__CCM.matrix_unFiltered->nrows*0.5);
assert(nrows_half > 0);
const uint nclusters_max = macro_min(config__CCM.k_clusterCount__max, nrows_half);
assert(nclusters_max < config__CCM.matrix_unFiltered->nrows);
//assert(nclusters_max < config__CCM.matrix_unFiltered->nrows);
//!
//! Iterate:
const uint nrows = self->obj_result_HCA.nrows;
	assert(nrows > 0);
//fprintf(stderr, "nrows=%u, matrix.nrows=%u, at %s:%d\n", nrows, config__CCM.matrix_unFiltered->nrows, __FILE__, __LINE__);
  for(uint nclusters = config__CCM.k_clusterCount__min; nclusters < nclusters_max; nclusters++) {
    assert(nclusters < config__CCM.matrix_unFiltered->nrows); //! ie, as we otheriwse 'would' have a poitness call.
    assert(nclusters >= 2);
    //printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
    s_kt_clusterAlg_fixed_resultObject_t obj_tmp = __MiF__getKClusterObject();
    assert(nrows == self->obj_result_HCA.nrows); //! ie, as we expect latter to be unahcned.
    //!
    //s_kt_clusterAlg_fixed_resultObject_t obj_tmp = cuttree__kt_clusterAlg_hca(&(self->obj_result_HCA), nclusters);
    if(obj_tmp.vertex_clusterId != NULL) {
      {
	const s_kt_matrix_base_t *matrix = config__CCM.matrix_unFiltered;
	assert(matrix);
	//	fprintf(stderr, "matrix-rows=[%u VS %u], at %s:%d\n", matrix->nrows, nrows, __FILE__, __LINE__);
	assert(nrows > 0);
	assert(matrix->nrows == nrows);
      }
      const bool isImproved = apply__CCM__updateBestScore__s_kt_clusterAlg_config__ccm_t(&config__CCM, &obj_tmp, currentResult, &(self->dynamicKMeans__best__score));
      if(isImproved) {
	free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
	self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
	self->dynamicKMeans__best__nClusters = nclusters;
	currentResult = self->dynamicKMeans__best__score;
	//  = ccm__local;
      } else { //! then the 'previosu result' 'had' a better score
	free__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp);
      }
    } //! elwe we assume 'no itneresting clsuters were gneerated'.
  }
//  assert(currentResult != get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(&config__CCM));
