
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__avg
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_sum
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divMax_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__divAbsMinus_sq_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__mul
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_headIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__div_tailIsNumerator
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__min
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__max
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_1
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__useScore_2
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__2log_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
else if(metric_id == e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus) {
if(weight != NULL) {
#define __stub_metricGroup__useWeights 1 //! ie, as we are then intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
} else {
#define __stub_metricGroup__useWeights 0 //! ie, as we are then Not intersted in adjusting the score by the second-row-valeus 'weight'.
#define __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse correlation_macros__distanceMeasures__directScore__sqrt_abs_minus
//! ---------------------------------------------------------------------------
//!
//! Apply the logics:
#include "kt_distance__stub__metricGroup__directScore.c"
}
}
#endif
