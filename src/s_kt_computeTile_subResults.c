#include "s_kt_computeTile_subResults.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


//! Initiate the "s_kt_computeTile_subResults_t" object.
void setTo_empty__s_kt_computeTile_subResults(s_kt_computeTile_subResults_t *self) {
  assert(self);
  //! Lcoal information wrt. the 'space' of allcoated data:
  self->_cnt_rows = 0;  self->_cnt_cols = 0;
  //! ---------------------
  self->matrixOf_result_cnt_interesting_cells = NULL;
  self->matrixOf_result_numerator = NULL;
  self->matrixOf_result_denumerator = NULL;
  /* self->matrixOf_result_numeratorLimitedTo_row1 = NULL; */
  /* self->matrixOf_result_numeratorLimitedTo_row_1and2 = NULL; */
  /* self->matrixOf_result_numeratorLimitedTo_row2 = NULL; */
  self->matrixOf_result_numeratorLimitedTo_row_1and2 = NULL;
  self->matrixOf_result_denumeratorLimitedTo_row1 = NULL;
  self->matrixOf_result_denumeratorLimitedTo_row_1and2 = NULL;
  self->matrixOf_result_denumeratorLimitedTo_row2 = NULL;
  
  //! 
  //! Local mapping-information:
  self->globalResult_index_row = 0; //! which is used as 'a point of reference' wrt. index-updates: used to simplify correctness-testing of our appraoch.
  self->globalResult_index_col = 0; //! which is used as 'a point of reference' wrt. index-updates: used to simplify correctness-testing of our appraoch.
}


//! @return tru if this object has bexplcitly been set to 'empty' (oekseth, 06. otk. 2016).
bool isEmpty__s_kt_computeTile_subResults(const s_kt_computeTile_subResults_t *self) {
  if(self->_cnt_rows != 0) {return false;}
  if(self->_cnt_cols != 0) {return false;}
  if(self->matrixOf_result_cnt_interesting_cells != NULL) {return false;}
  if(self->matrixOf_result_numerator != NULL) {return false;}
  if(self->matrixOf_result_denumerator != NULL) {return false;}
  if(self->matrixOf_result_numeratorLimitedTo_row_1and2 != NULL) {return false;}
  if(self->matrixOf_result_denumeratorLimitedTo_row1 != NULL) {return false;}
  if(self->matrixOf_result_denumeratorLimitedTo_row_1and2 != NULL) {return false;}
  if(self->matrixOf_result_denumeratorLimitedTo_row2 != NULL) {return false;}

  //!
  //! At this exeuciton-point we assume the object is set:
  return true;
}

/**
   @param <self> is the s_kt_computeTile_subResults objhect to update (oekseth, 06. setp. 2016)
   @param <max_chunkSize_rows> is the max number of row to be evlaauted seperately in the all-against-all memory-tile-comptauion.
   @param <max_chunkSize_columns> is the max number of columns to be evlaauted seperately in the all-against-all memory-tile-comptauion.
   @param <typeOf_distanceFormula> describes the degree of 'arptuiality' in the distance-corrlation-equations: used to avodi allcoating memory which will anyhow nto be seud, ie, to 'move compelxtiy fromt eh caller ot this fucniton'.
   @param <isTo_updateCountVariable> which is to be set to tru if (a) eitehr implcit or epxlcit masks are used && the distance-metric (which is being comptued) need this knowledge for correct comptuation of a given metric.
   @param <isTo_findMaxValue> which if to be set to "true" if we are to itnate the result-value with "T_FLOAT_MIN_ABS", ie, to avodi intaiting with "0" to resutl in errnrous results
   @param <isTo_findMinValue> which if to be set to "true" if we are to itnate the result-value with "T_FLOAT_MAX", ie, to avodi intaiting with "0" to resutl in errnrous results
 **/
void init__s_kt_computeTile_subResults(s_kt_computeTile_subResults_t *self, const uint max_chunkSize_rows, const uint max_chunkSize_columns, const enum e_template_correlation_tile_typeOf_distanceUpdate typeOf_distanceFormula, const bool isTo_updateCountVariable, const bool isTo_findMaxValue, const bool isTo_findMinValue) {
  //! What we expect:
  assert(self);
  assert(max_chunkSize_rows > 0);
  assert(max_chunkSize_rows != UINT_MAX);
  assert(max_chunkSize_columns != UINT_MAX);
  //! Apply default intiaton:
  setTo_empty__s_kt_computeTile_subResults(self);


  if(!isTo_updateCountVariable && (typeOf_distanceFormula == e_template_correlation_tile_typeOf_distanceUpdate_scalar)) {return;} //! ie, as we tehna ssuem taht the 'resulting' distance-scroes may be udpated directly.

  //! Udpat ethe count-variables:
  self->_cnt_rows = max_chunkSize_rows;  self->_cnt_cols = max_chunkSize_columns;

  //! Allocate memory-space
  const uint default_value_uint = 0;
  t_float default_value_float = 0;  
  if(isTo_findMaxValue == true) {default_value_float = T_FLOAT_MIN_ABS;}
  else if(isTo_findMinValue) {default_value_float = T_FLOAT_MAX;}

  if(typeOf_distanceFormula == e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator) {
    self->matrixOf_result_numerator = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
    self->matrixOf_result_denumerator = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
  } else if(typeOf_distanceFormula == e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex) {
    self->matrixOf_result_numerator = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
    self->matrixOf_result_numeratorLimitedTo_row_1and2 = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
    self->matrixOf_result_denumerator = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
    self->matrixOf_result_denumeratorLimitedTo_row1 = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
    self->matrixOf_result_denumeratorLimitedTo_row2 = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
    self->matrixOf_result_denumeratorLimitedTo_row_1and2 = allocate_2d_list_float(max_chunkSize_rows, max_chunkSize_columns, default_value_float);
  } else if(typeOf_distanceFormula != e_template_correlation_tile_typeOf_distanceUpdate_scalar) {
    assert(false); //! ie, as we then need to add supprot for thsui case ... which could be deu to an include oupdate of the "e_template_correlation_tile_typeOf_distanceUpdate" enum (oekseth, 06. sept. 2016).
  }
  if(isTo_updateCountVariable) { //! then we allcoate space 'for this count-variable':
    self->matrixOf_result_cnt_interesting_cells = allocate_2d_list_uint(max_chunkSize_rows, max_chunkSize_columns, default_value_uint);
  }
}

//! Clear old data
//! @remarks is used to avoid unneccesary re-allcoatesions for each [index1...][index2...] tile which is evlauated/computed.
void clearOldData(s_kt_computeTile_subResults_t *self, const enum e_template_correlation_tile_typeOf_distanceUpdate typeOf_distanceFormula, const bool isTo_updateCountVariable, const bool isTo_findMaxValue, const bool isTo_findMinValue) {
  assert(self);
  if(!isTo_updateCountVariable && (typeOf_distanceFormula == e_template_correlation_tile_typeOf_distanceUpdate_scalar)) {return;} //! ie, as we tehna ssuem taht the 'resulting' distance-scroes may be udpated directly.



  //! Udpat ethe count-variables:
  assert(self->_cnt_rows > 0); assert(self->_cnt_cols > 0);

 const uint default_value_uint = 0;  
  t_float default_value_float = 0;  
  if(isTo_findMaxValue == true) {default_value_float = T_FLOAT_MIN_ABS;}
  else if(isTo_findMinValue) {default_value_float = T_FLOAT_MAX;}

  //! Reset:
  if(typeOf_distanceFormula == e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator) {
    memset_local_2d(self->matrixOf_result_numerator, self->_cnt_rows, self->_cnt_cols, default_value_float); 
    memset_local_2d(self->matrixOf_result_denumerator, self->_cnt_rows, self->_cnt_cols, default_value_float); 
  } else if(typeOf_distanceFormula == e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex) {
    memset_local_2d(self->matrixOf_result_numerator, self->_cnt_rows, self->_cnt_cols, default_value_float); 
    memset_local_2d(self->matrixOf_result_denumerator, self->_cnt_rows, self->_cnt_cols, default_value_float); 
    memset_local_2d(self->matrixOf_result_numeratorLimitedTo_row_1and2, self->_cnt_rows, self->_cnt_cols, default_value_float); 
    memset_local_2d(self->matrixOf_result_denumeratorLimitedTo_row1, self->_cnt_rows, self->_cnt_cols, default_value_float); 
    memset_local_2d(self->matrixOf_result_denumeratorLimitedTo_row2, self->_cnt_rows, self->_cnt_cols, default_value_float); 
    memset_local_2d(self->matrixOf_result_denumeratorLimitedTo_row_1and2, self->_cnt_rows, self->_cnt_cols, default_value_float); 
  } else if(typeOf_distanceFormula != e_template_correlation_tile_typeOf_distanceUpdate_scalar) {
    assert(false); //! ie, as we then need to add supprot for thsui case ... which could be deu to an include oupdate of the "e_template_correlation_tile_typeOf_distanceUpdate" enum (oekseth, 06. sept. 2016).
  }
  if(isTo_updateCountVariable) { //! then we allcoate space 'for this count-variable':
    memset_local_2d(self->matrixOf_result_cnt_interesting_cells, self->_cnt_rows, self->_cnt_cols, default_value_uint); 
  }

  //! 
  //! Local mapping-information:
  self->globalResult_index_row = 0; //! which is used as 'a point of reference' wrt. index-updates: used to simplify correctness-testing of our appraoch.
  self->globalResult_index_col = 0; //! which is used as 'a point of reference' wrt. index-updates: used to simplify correctness-testing of our appraoch.
}

//! De-allcoate a given object of type "s_kt_computeTile_subResults_t"
void free__s_kt_computeTile_subResults(s_kt_computeTile_subResults_t *self) {
  assert(self);
  if(self->matrixOf_result_cnt_interesting_cells != NULL) {free_2d_list_uint(&self->matrixOf_result_cnt_interesting_cells, self->_cnt_rows);};
  if(self->matrixOf_result_numerator != NULL) {free_2d_list_float(&self->matrixOf_result_numerator, self->_cnt_rows);};
  if(self->matrixOf_result_denumerator != NULL) {free_2d_list_float(&self->matrixOf_result_denumerator, self->_cnt_rows);};
  /* self->matrixOf_result_numeratorLimitedTo_row1 != NULL) {free_2d_list_float(&self->matrixOf_result_);}; */
  if(self->matrixOf_result_numeratorLimitedTo_row_1and2 != NULL) {free_2d_list_float(&self->matrixOf_result_numeratorLimitedTo_row_1and2, self->_cnt_rows);}
  /* self->matrixOf_result_numeratorLimitedTo_row2 != NULL) {free_2d_list_float(&self->matrixOf_result_);}; */
  if(self->matrixOf_result_denumeratorLimitedTo_row1 != NULL) {free_2d_list_float(&self->matrixOf_result_denumeratorLimitedTo_row1, self->_cnt_rows);};
  if(self->matrixOf_result_denumeratorLimitedTo_row_1and2 != NULL) {free_2d_list_float(&self->matrixOf_result_denumeratorLimitedTo_row2, self->_cnt_rows);};
  if(self->matrixOf_result_denumeratorLimitedTo_row2 != NULL) {free_2d_list_float(&self->matrixOf_result_denumeratorLimitedTo_row_1and2, self->_cnt_rows);};
  //! Rset counters:
  self->_cnt_rows = 0;  self->_cnt_cols = 0;
}

