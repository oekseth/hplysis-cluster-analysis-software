  //!
//s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  if(true) {
    fileRead_config__syn.imaginaryFileProp__nrows = 10;
    fileRead_config__syn.imaginaryFileProp__ncols = 10;
  } else { //! then we are interested in a more performacne-demanind approach: 
    fileRead_config__syn.imaginaryFileProp__nrows = 400;
    fileRead_config__syn.imaginaryFileProp__ncols = 400;
  }
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  s_kt_matrix_fileReadTuning_t fileRead_config__syn_latency = fileRead_config__syn;
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  /* s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat__type__overhead = fileRead_config__syn; */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat__type__latency = fileRead_config__syn; */
  //!
  //! Read the input-file:
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  const char *config__fileName__binaryMatrix = "data/vilje-cost-rack1-half.dat"; //! a matrix which may be given at request to "jancrhis@ntnu.no" at NTNUs super-comptuer-faciltity:
  mdata_t dat;   mdata_initf ( &dat, config__fileName__binaryMatrix );
  if(!dat.n) {
    fprintf(stderr, "!!\t Seems like the cosntructed matrix was empty when laoding from \"%s\", ie, please invesetivgate. OBservaiton at [%s]:%s:%d\n", config__fileName__binaryMatrix, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
  }
  //! Itniate:
  s_kt_matrix_t matrix_concatToAll__type__overhead = initAndReturn__s_kt_matrix(dat.n, dat.n);
  s_kt_matrix_t matrix_concatToAll__type__latency = initAndReturn__s_kt_matrix(dat.n, dat.n);
  //!
  //! Load the Vilje super-comptuer matrix (into our two data-strucutres), where logics are foudn in  the "matrixdata.h" written by "jancrhis@ntnu.no":
  for(uint i=0; i<(uint)dat.n; i++ ) {
    for(uint j=0; j<(uint)dat.n; j++ ) {
      matrix_concatToAll__type__overhead.matrix[i][j] = TS(dat,i,j);
      matrix_concatToAll__type__latency.matrix[i][j]  = TL(dat,i,j);
    }
  }
  //! De-allcoate the 'parser':
  mdata_finalize ( &dat );


  //! 
  //! Update the configuraiton-object:
  assert(matrix_concatToAll__type__latency.ncols > 0);
  assert(matrix_concatToAll__type__overhead.ncols > 0);
  fileRead_config__syn.mat_concat = &matrix_concatToAll__type__overhead;
  fileRead_config__syn_latency.mat_concat = &matrix_concatToAll__type__latency;

  const uint mapOf_realLife_size = 2;
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    //! Note: in [”elow] we inaite the "s_hp_clusterFileCollection_t" struct defined in our "hp_clusterFileCollection.h":
    {/*tag=*/"vilje_superComputer__overhead", /*file_name=*/NULL, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/true, /*k_clusterCount=*/10, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"vilje_superComputer__latency", /*file_name=*/NULL, /*fileRead_config=*/fileRead_config__syn_latency, /*fileIsAdjecency=*/true, /*k_clusterCount=*/10, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg}
  };
