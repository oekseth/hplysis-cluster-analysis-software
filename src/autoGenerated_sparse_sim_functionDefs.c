/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @brie defines seperately each sparse simlairty/correlation-fucnton, a strategy used for opmtiziaotn (oekseth, 6. mar. 2017).
   @remarks last generated at Fri Oct 13 17:49:55 2017 from "build_codeFor_tiling.pl"
 **/
//! ---------------------------------------------------------------------------------------------------------------------------------------------------------
//!
//! Define muliple functions:
//! Note: the distance-macros are expected to be found in our "correlation_macros__distanceMeasures.h"
//! 

//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "euclid" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_euclid
#define functName__weight kt_sparse_computeForMetric_weight_euclid
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_euclid
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__euclid //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__euclid__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__euclid__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "cityBlock" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_cityBlock
#define functName__weight kt_sparse_computeForMetric_weight_cityBlock
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_cityBlock
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_cityBlock
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__cityBlock //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__cityBlock__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "minkowski" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_minkowski
#define functName__weight kt_sparse_computeForMetric_weight_minkowski
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_minkowski
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_minkowski
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "minkowski__subCase__zeroOne" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_minkowski__subCase__zeroOne
#define functName__weight kt_sparse_computeForMetric_weight_minkowski__subCase__zeroOne
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_minkowski__subCase__zeroOne
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__zeroOne
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__zeroOne //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__zeroOne__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "minkowski__subCase__pow3" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_minkowski__subCase__pow3
#define functName__weight kt_sparse_computeForMetric_weight_minkowski__subCase__pow3
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_minkowski__subCase__pow3
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__pow3
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__pow3 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__pow3__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski_pow3__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "minkowski__subCase__pow4" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_minkowski__subCase__pow4
#define functName__weight kt_sparse_computeForMetric_weight_minkowski__subCase__pow4
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_minkowski__subCase__pow4
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__pow4
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__pow4 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__pow4__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski_pow4__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "minkowski__subCase__pow5" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_minkowski__subCase__pow5
#define functName__weight kt_sparse_computeForMetric_weight_minkowski__subCase__pow5
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_minkowski__subCase__pow5
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__pow5
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__pow5 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__pow5__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski_pow5__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "chebychev" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_chebychev
#define functName__weight kt_sparse_computeForMetric_weight_chebychev
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_chebychev
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_chebychev
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__chebychev //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__chebychev__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__chebychev__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 1 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "chebychev_minInsteadOf_max" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_chebychev_minInsteadOf_max
#define functName__weight kt_sparse_computeForMetric_weight_chebychev_minInsteadOf_max
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_chebychev_minInsteadOf_max
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_chebychev_minInsteadOf_max
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__chebychev //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__chebychev__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__chebychev__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 1 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Sorensen" ---------------------------------------------------------------------------------------
//! @description: the 'Sorensen' metric is widely used in ecology: a different name for the 'Sorensen' metric is the 'Bray-Curtis' metric.
#define functName__xmtWeight kt_sparse_computeForMetric_Sorensen
#define functName__weight kt_sparse_computeForMetric_weight_Sorensen
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Sorensen
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Sorensen
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Gower" ---------------------------------------------------------------------------------------
//! @description: Gower differs from Chebychev with respect to the normalization of the entites signficance in the data-set, ie, (1/d). In the 'default' verison we assume that the error-rate is set to zero, ie, as explained in the work of Gower 	tep{http://cbio.mines-paristech.fr/~jvert/svn/bibli/local/Gower1971general.pdf}. 
#define functName__xmtWeight kt_sparse_computeForMetric_Gower
#define functName__weight kt_sparse_computeForMetric_weight_Gower
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Gower
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Gower
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Gower //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Gower__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__absoluteDifferenc__Gower__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Soergel" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Soergel
#define functName__weight kt_sparse_computeForMetric_weight_Soergel
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Soergel
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Soergel
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Soergel //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Soergel__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Kulczynski_absDiff" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Kulczynski_absDiff
#define functName__weight kt_sparse_computeForMetric_weight_Kulczynski_absDiff
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Kulczynski_absDiff
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Kulczynski_absDiff
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Kulczynski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Kulczynski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Canberra" ---------------------------------------------------------------------------------------
//! @description: difference between 'Canberra' and 'Sorensen' conserns how 'Canberra' normalizes the absoltue difference at the individual level.
#define functName__xmtWeight kt_sparse_computeForMetric_Canberra
#define functName__weight kt_sparse_computeForMetric_weight_Canberra
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Canberra
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Canberra
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Canberra //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Canberra__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Lorentzian" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Lorentzian
#define functName__weight kt_sparse_computeForMetric_weight_Lorentzian
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Lorentzian
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Lorentzian
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Lorentzian //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Lorentzian__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_mean //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_mean__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_mean__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean_standardDeviation_userDefinedPower" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_userDefinedPower
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPower
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_userDefinedPower
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPower
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow_zeroOne //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow_zeroOne__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean_standardDeviation_pow2" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow2
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow2
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow2
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow2
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean_standardDeviation_pow3" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow3
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow3
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow3
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow3
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean_standardDeviation_pow4" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow4
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow4
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow4
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow4
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "oekseth_forMasks_mean_standardDeviation_pow5" ---------------------------------------------------------------------------------------
//! @description: Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used)
#define functName__xmtWeight kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow5
#define functName__weight kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow5
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow5
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow5
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "intersection" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_intersection
#define functName__weight kt_sparse_computeForMetric_weight_intersection
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_intersection
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_intersection
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__intersection //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__intersection__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "intersectionInverted" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_intersectionInverted
#define functName__weight kt_sparse_computeForMetric_weight_intersectionInverted
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_intersectionInverted
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_intersectionInverted
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__intersectionAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__intersectionAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__intersectionAlt__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "WaveHedges" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_WaveHedges
#define functName__weight kt_sparse_computeForMetric_weight_WaveHedges
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_WaveHedges
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_WaveHedges
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "WaveHedges_alt" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_WaveHedges_alt
#define functName__weight kt_sparse_computeForMetric_weight_WaveHedges_alt
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_WaveHedges_alt
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_WaveHedges_alt
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges_alt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges_alt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Czekanowski" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Czekanowski
#define functName__weight kt_sparse_computeForMetric_weight_Czekanowski
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Czekanowski
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Czekanowski
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Czekanowski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Czekanowski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Czekanowski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Czekanowski_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Czekanowski_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Czekanowski_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Czekanowski_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Czekanowski_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__CzekanowskiAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__CzekanowskiAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__CzekanowskiAlt__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Motyka" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Motyka
#define functName__weight kt_sparse_computeForMetric_weight_Motyka
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Motyka
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Motyka
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Motyka //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Motyka__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Motyka__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Motyka_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Motyka_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Motyka_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Motyka_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Motyka_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__MotykaAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__MotykaAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Motyka__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Kulczynski" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Kulczynski
#define functName__weight kt_sparse_computeForMetric_weight_Kulczynski
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Kulczynski
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Kulczynski
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Kulczynski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Kulczynski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Kulczynski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Ruzicka" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Ruzicka
#define functName__weight kt_sparse_computeForMetric_weight_Ruzicka
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Ruzicka
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Ruzicka
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Ruzicka //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Ruzicka__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Ruzicka__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Tanimoto" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Tanimoto
#define functName__weight kt_sparse_computeForMetric_weight_Tanimoto
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Tanimoto
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Tanimoto
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Tanimoto //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Tanimoto__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Tanimoto__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Tanimoto_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Tanimoto_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Tanimoto_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Tanimoto_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Tanimoto_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__TanimotoAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__TanimotoAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__TanimotoAlt__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "innerProduct" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_innerProduct
#define functName__weight kt_sparse_computeForMetric_weight_innerProduct
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_innerProduct
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_innerProduct
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__innerProduct //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__innerProduct__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "harmonicMean" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_harmonicMean
#define functName__weight kt_sparse_computeForMetric_weight_harmonicMean
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_harmonicMean
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_harmonicMean
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__harmonicMean //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__harmonicMean__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__harmonicMean__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "cosine" ---------------------------------------------------------------------------------------
//! @description: The 'cosine' metric (or: 'angular metric')  measure the angle between two vectors
#define functName__xmtWeight kt_sparse_computeForMetric_cosine
#define functName__weight kt_sparse_computeForMetric_weight_cosine
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_cosine
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_cosine
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__cosine__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "HumarHasseBrook_PCA_or_Jaccard" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_HumarHasseBrook_PCA_or_Jaccard
#define functName__weight kt_sparse_computeForMetric_weight_HumarHasseBrook_PCA_or_Jaccard
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_HumarHasseBrook_PCA_or_Jaccard
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_HumarHasseBrook_PCA_or_Jaccard
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__KumarHassebrook_or_Jaccard__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Dice" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Dice
#define functName__weight kt_sparse_computeForMetric_weight_Dice
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Dice
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Dice
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__Dice__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Jaccard_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Jaccard_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Jaccard_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Jaccard_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Jaccard_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__JaccardAltDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Dice_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Dice_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Dice_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Dice_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Dice_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__DiceAltDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "sampleCoVariance" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_sampleCoVariance
#define functName__weight kt_sparse_computeForMetric_weight_sampleCoVariance
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_sampleCoVariance
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_sampleCoVariance
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCoVariance //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Brownian" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Brownian
#define functName__weight kt_sparse_computeForMetric_weight_Brownian
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Brownian
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Brownian
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Brownian_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Brownian_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Brownian_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Brownian_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Brownian_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__permutated_innerRootInDenumerator__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "fidelity" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_fidelity
#define functName__weight kt_sparse_computeForMetric_weight_fidelity
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_fidelity
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_fidelity
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Bhattacharyya" ---------------------------------------------------------------------------------------
//! @description: the metric is adjusted in order to 'handle' empty valeus by taking the 'abs' of the v*u product, an appraoch also applifed for 'fidelity', 'Hellinger', 'Matusitat' and 'squared chord'; in order to simplify the indiciation of special cases 'using' 'abs(..)' in combiaton with the 'sqrt(..)' funciton please search for all applications of the 'mathLib_float_sqrt_abs(..)' call in the soruce-code of hpLysis.
#define functName__xmtWeight kt_sparse_computeForMetric_Bhattacharyya
#define functName__weight kt_sparse_computeForMetric_weight_Bhattacharyya
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Bhattacharyya
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Bhattacharyya
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Bhattacharyya__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Hellinger" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Hellinger
#define functName__weight kt_sparse_computeForMetric_weight_Hellinger
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Hellinger
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Hellinger
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Hellinger__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Matusita" ---------------------------------------------------------------------------------------
//! @description: Matusita
#define functName__xmtWeight kt_sparse_computeForMetric_Matusita
#define functName__weight kt_sparse_computeForMetric_weight_Matusita
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Matusita
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Matusita
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Hellinger_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Hellinger_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Hellinger_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Hellinger_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Hellinger_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Hellinger_altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Matusita_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Matusita_altDef
#define functName__weight kt_sparse_computeForMetric_weight_Matusita_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Matusita_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Matusita_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "squaredChord" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_squaredChord
#define functName__weight kt_sparse_computeForMetric_weight_squaredChord
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_squaredChord
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_squaredChord
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "squaredChord_altDef" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_squaredChord_altDef
#define functName__weight kt_sparse_computeForMetric_weight_squaredChord_altDef
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_squaredChord_altDef
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_squaredChord_altDef
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Euclid_squared" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Euclid_squared
#define functName__weight kt_sparse_computeForMetric_weight_Euclid_squared
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Euclid_squared
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Euclid_squared
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Euclid //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Euclid__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Euclid__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Pearson_squared" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Pearson_squared
#define functName__weight kt_sparse_computeForMetric_weight_Pearson_squared
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Pearson_squared
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Pearson_squared
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Pearson //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Pearson__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Pearson__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Neyman" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Neyman
#define functName__weight kt_sparse_computeForMetric_weight_Neyman
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Neyman
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Neyman
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Neyman //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Neyman__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Neyman__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "squaredChi" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_squaredChi
#define functName__weight kt_sparse_computeForMetric_weight_squaredChi
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_squaredChi
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_squaredChi
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__squaredChi__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "probabilisticChi" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_probabilisticChi
#define functName__weight kt_sparse_computeForMetric_weight_probabilisticChi
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_probabilisticChi
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_probabilisticChi
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__probabilistic_or_divergence__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "squared_divergence" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_squared_divergence
#define functName__weight kt_sparse_computeForMetric_weight_squared_divergence
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_squared_divergence
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_squared_divergence
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__divergence //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__divergence__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__probabilistic_or_divergence__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Clark" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Clark
#define functName__weight kt_sparse_computeForMetric_weight_Clark
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Clark
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Clark
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Clark //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Clark__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Clark__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "addativeSymmetricSquaredChi" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_addativeSymmetricSquaredChi
#define functName__weight kt_sparse_computeForMetric_weight_addativeSymmetricSquaredChi
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_addativeSymmetricSquaredChi
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_addativeSymmetricSquaredChi
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Pearson_productMoment_generic" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Pearson_productMoment_generic
#define functName__weight kt_sparse_computeForMetric_weight_Pearson_productMoment_generic
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Pearson_productMoment_generic
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_generic
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_generic__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Pearson_productMoment_absolute" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Pearson_productMoment_absolute
#define functName__weight kt_sparse_computeForMetric_weight_Pearson_productMoment_absolute
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Pearson_productMoment_absolute
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_absolute
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_absolute__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Pearson_productMoment_uncentered" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Pearson_productMoment_uncentered
#define functName__weight kt_sparse_computeForMetric_weight_Pearson_productMoment_uncentered
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Pearson_productMoment_uncentered
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_uncentered
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_uncentered__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Pearson_productMoment_uncenteredAbsolute" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Pearson_productMoment_uncenteredAbsolute
#define functName__weight kt_sparse_computeForMetric_weight_Pearson_productMoment_uncenteredAbsolute
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Pearson_productMoment_uncenteredAbsolute
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_uncenteredAbsolute
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_uncenteredAbsolute__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Kullback_Leibler" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Kullback_Leibler
#define functName__weight kt_sparse_computeForMetric_weight_Kullback_Leibler
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Kullback_Leibler
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Kullback_Leibler
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__KullbackLeibler //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__KullbackLeibler__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__KullbackLeibler__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Jeffreys" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Jeffreys
#define functName__weight kt_sparse_computeForMetric_weight_Jeffreys
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Jeffreys
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Jeffreys
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__Jeffreys //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__Jeffreys__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__Jeffreys__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "kDivergence" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_kDivergence
#define functName__weight kt_sparse_computeForMetric_weight_kDivergence
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_kDivergence
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_kDivergence
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__kDivergence //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__kDivergence__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__kDivergence__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Topsoee" ---------------------------------------------------------------------------------------
//! @description: A different name for the 'Topsoee' metric is 'information theory'.
#define functName__xmtWeight kt_sparse_computeForMetric_Topsoee
#define functName__weight kt_sparse_computeForMetric_weight_Topsoee
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Topsoee
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Topsoee
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__Topsoee //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__Topsoee__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__Topsoee__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "JensenShannon" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_JensenShannon
#define functName__weight kt_sparse_computeForMetric_weight_JensenShannon
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_JensenShannon
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_JensenShannon
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__JensenShannon //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__JensenShannon__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__JensenShannon__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "JensenDifference" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_JensenDifference
#define functName__weight kt_sparse_computeForMetric_weight_JensenDifference
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_JensenDifference
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_JensenDifference
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__JensenDifference //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__JensenDifference__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__JensenDifference__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "Taneja" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_Taneja
#define functName__weight kt_sparse_computeForMetric_weight_Taneja
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_Taneja
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_Taneja
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__combination__Taneja //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__combination__Taneja__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__combination__Taneja__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "KumarJohnson" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_KumarJohnson
#define functName__weight kt_sparse_computeForMetric_weight_KumarJohnson
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_KumarJohnson
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_KumarJohnson
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__combination__KumarJohnson //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__combination__KumarJohnson__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__combination__KumarJohnson__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "averageOfChebyshevAndCityblock" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_averageOfChebyshevAndCityblock
#define functName__weight kt_sparse_computeForMetric_weight_averageOfChebyshevAndCityblock
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_averageOfChebyshevAndCityblock
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_averageOfChebyshevAndCityblock
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "VicisWaveHedges" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_VicisWaveHedges
#define functName__weight kt_sparse_computeForMetric_weight_VicisWaveHedges
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_VicisWaveHedges
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_VicisWaveHedges
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "VicisWaveHedgesMax" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_VicisWaveHedgesMax
#define functName__weight kt_sparse_computeForMetric_weight_VicisWaveHedgesMax
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_VicisWaveHedgesMax
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_VicisWaveHedgesMax
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "VicisWaveHedgesMin" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_VicisWaveHedgesMin
#define functName__weight kt_sparse_computeForMetric_weight_VicisWaveHedgesMin
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_VicisWaveHedgesMin
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_VicisWaveHedgesMin
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "VicisSymmetric" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_VicisSymmetric
#define functName__weight kt_sparse_computeForMetric_weight_VicisSymmetric
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_VicisSymmetric
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_VicisSymmetric
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "VicisSymmetricMax" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_VicisSymmetricMax
#define functName__weight kt_sparse_computeForMetric_weight_VicisSymmetricMax
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_VicisSymmetricMax
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_VicisSymmetricMax
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "downwardMutability_symmetricMax" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_downwardMutability_symmetricMax
#define functName__weight kt_sparse_computeForMetric_weight_downwardMutability_symmetricMax
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_downwardMutability_symmetricMax
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_downwardMutability_symmetricMax
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__symmetricMax__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"



//! ------------------------------------------------------------------------------------------------------------------
//!
//! @Metric: "downwardMutability_symmetricMin" ---------------------------------------------------------------------------------------
//! @description: 
#define functName__xmtWeight kt_sparse_computeForMetric_downwardMutability_symmetricMin
#define functName__weight kt_sparse_computeForMetric_weight_downwardMutability_symmetricMin
#define functName__adjust_xmtWeight kt_sparse_computeForMetric_adjust_downwardMutability_symmetricMin
#define functName__adjust_weight kt_sparse_computeForMetric_adjust_weight_downwardMutability_symmetricMin
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__symmetricMin__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
//! ---
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix). 
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0 
//! 
//! Apply [above] specificaiotns:
#include "template_sparseSimMetric__nonOptimized__wrapper.c"


