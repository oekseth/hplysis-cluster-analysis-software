  const bool isLast = ( (self->cnt_dataObjects_toGenerate + 1) == self->cnt_dataObjects_written);
  //! ------------------------------------------------
  FILE *stream_out = self->stream_out;
  if (stream_out == NULL) {
    fprintf(stderr, "!!\t The result-file was not opened: did you remeber to call the \"export_config__s_kt_matrix_t(..)\" before 'this fucntion-call' was made?: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }  
  if(
     (self->isTo_inResult_isTo_useJavaScript_syntax == false) &&
     (self->isTo_inResult_isTo_useJSON_syntax == false)
     )  { //! Then we write otu in a simple tSV-sytnex, a sytnex 'which makaes it easy to do a 'grep' on values for large-data-sets:
    if(stringOf_mainStructOwner) {
      fprintf(stream_out, "\n#! Generate 1d-weight-table for: %s\n", stringOf_mainStructOwner);
    }
    const bool isTo_useFormat_tsv = false; //! where we use 'commo' in order to have a 'denser' format (ie, sinmplify the redaiblity)
    const char *stringOf_format = NULL;
    char format_sep = ',';
    if(isTo_useFormat_tsv) {
      stringOf_format = "tab"; format_sep = '\t';
    } else {
      stringOf_format = "comma"; format_sep = ',';
    }
    if(stringOf_label) {
      fprintf(stream_out, "#! Format=%s, label=\"%s\", ncols=%u\tvector:\t", stringOf_format, stringOf_label, arrOf_values_size);
    } else {
      fprintf(stream_out, "#! Format=%s, ncols=%u\tvector:\t", stringOf_format, arrOf_values_size);
    }
    for(uint col_id = 0; col_id < arrOf_values_size; col_id++) {
      fprintf(stream_out, __internalMacro__formatSpec_cell "%c", arrOf_values[col_id], format_sep);
    }
    fputc('\n', stream_out);
  } else {
    assert(stringOf_label);
    if(self->cnt_dataObjects_toGenerate == 1) {fprintf(stream_out, "var %s = [", stringOf_label); //! ie, as we then asusme we are to exprot using a 'simple' ajva-scirpt-wrapper.
    } else {
      if(stringOf_mainStructOwner) { //! then we assuem we are to itnaite
	if(strlen(stringOf_mainStructOwner)) {
	  fprintf(stream_out, "%s = {\n", stringOf_mainStructOwner);
	} else {
	  /* if( */
	  /*    //self->isTo_inResult_isTo_useJSON_syntax &&  */
	  /*    (self->cnt_dataObjects_written > 0) ) { */
	  /*   //! Then add a speratro for the JSON-data-set: */
	  /*   fprintf(stream_out, ",\n"); */
	  /* } */

	  fprintf(stream_out, "\"%s\" : {\n", stringOf_mainStructOwner); //! ie, as we then assuemt eh result is to be exported into a JSON compaitble reuslt-format:
	}
      } else {
	/* if( */
	/*    //self->isTo_inResult_isTo_useJSON_syntax &&  */
	/*    (self->cnt_dataObjects_written > 0) ) { */
	/*   //! Then add a speratro for the JSON-data-set: */
	/*   fprintf(stream_out, ",\n"); */
	/* } */
      }
    }
    //! Include the 'sopecifci' hash-varialbe for 'the table to be exported:
    if(self->isTo_inResult_isTo_useJSON_syntax == false) {
      fprintf(stream_out, "var %s = [", stringOf_label);
    } else {
      fprintf(stream_out, "\"%s\" : [", stringOf_label);
    }
    //!
    //! Write out the data:
    for(uint col_id = 0; col_id < arrOf_values_size; col_id++) {
#if(__internalMacro__valuesToBeQuoted == 1)
      if( (col_id+1) != self->ncols) { 
	fprintf(stream_out, "\"" __internalMacro__formatSpec_cell "\", ", arrOf_values[col_id]);
      } else {fprintf(stream_out, "\"" __internalMacro__formatSpec_cell "\" ]", arrOf_values[col_id]);}
#else //! then we asusme the 'nubmers are to be repsentaed as numbers', ie, which 'makes the valeus easiser to read':
      if( (col_id+1) != self->ncols) { 
	fprintf(stream_out,  __internalMacro__formatSpec_cell ", ", arrOf_values[col_id]);
      } else {fprintf(stream_out, __internalMacro__formatSpec_cell "]", arrOf_values[col_id]);}
#endif
    }
    //! Complete the data-set, ie, if 'requested':
    if(isLast) {
      fprintf(stream_out, "\n}");
      if(self->isTo_inResult_isTo_useJSON_syntax == false) { fprintf(stream_out, ";");}
      fprintf(stream_out, "\n}");
    } else {
      if(self->isTo_inResult_isTo_useJSON_syntax == false) { fprintf(stream_out, ";\n");}
      else {fprintf(stream_out, ",\n");} //! ie, then 'include' a seperator.
    }
  }
  self->cnt_dataObjects_written++;
