#ifndef correlation_inCategory_rank_kendall_h
#define correlation_inCategory_rank_kendall_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlation_inCategory_rank_kendall.h
   @brief logics to infer Kendall's correlation for a comptue-all-against-all applicaiton-case
   @author Ole Kristina Ekseth (oekseth, 06. sept. 2016).
**/

#include "correlation_inCategory_matrix_base.h"
#include "correlation_rank_rowPair.h"
#include "s_kt_correlationConfig.h"
#include "correlationType_kendall.h"

//! Comptue Kendalls-tau for each cell in the input-matrices.
void ktCorr_compute_allAgainstAll_distanceMetric_kendall(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, t_float weight[], t_float **resultMatrix, bool transpose, const e_kt_correlationFunction_t metric_id, s_allAgainstAll_config_t *config_allAgainstAll); 
//const enum e_typeOf_optimization_distance typeOf_optimization = e_typeOf_optimization_distance_asFastAsPossible,  const uint CLS = 64, const bool isTo_use_continousSTripsOf_memory = true, uint iterationIndex_2 = UINT_MAX, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef, void *s_inlinePostProcess = NULL, const e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_undef);

#endif //! EOF
