assert(globalOffset__betweeDataSets < global_cntRows);
uint *mapOf_vertexToCentroid1__gold = __localGold[data_subset_id]; //! defined in [ªbvoe] 'inlcude'.
const t_float empty_0 = 0;
t_float **matrixOf_coOccurence_gold = allocate_2d_list_float(cnt_vertices, cnt_vertices, empty_0);
assert(matrixOf_coOccurence_gold != NULL);
for(uint i = 0; i < cnt_vertices; i++) {for(uint k = 0; k < cnt_vertices; k++) {matrixOf_coOccurence_gold[i][k] = __localMatrix[i][k];}}
assert(cnt_vertices > 0);
assert(mapOf_vertexToCentroid1__gold != NULL); 
//!
//!
//! Apply logics:
#include "measure_kt_matrix_cmpCluster__CMP__randomPerturbatedDataSets.c"
//! 
//! De-allocate:
assert(matrixOf_coOccurence_gold);
free_2d_list_float(&matrixOf_coOccurence_gold, cnt_vertices);
#undef __localMatrix
#undef __localGold

//! ------------
//! ------------
global_cntDataSets_current++; //!
//! ---------- add a 'space' between each 'data-set':
if(global_cntDataSets_current < global_cntDataSets) {
  global_cntRows_current += globalOffset__betweeDataSets; //! ie, whcih si used as a 'visual spac'e to sperate the differetn data-sets.
  
 }
