#ifndef kt_clusterAlg_dbScan_h
#define kt_clusterAlg_dbScan_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_clusterAlg_dbScan
   @brief The function-api to our high-performance clustering-methods wrt. the DB-SCAN algorithm (oekseth, 06. setp. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "e_kt_correlationFunction.h"

#include "kt_api_config.h"
#include "kt_clusterAlg_hca_resultObject.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_forest_findDisjoint.h"
#include "hp_distanceCluster.h"

/* #include "e_kt_api_dynamicKMeans.h" */
/* #include "kt_api__centerPoints.h" */
/* #include "def_memAlloc.h" */

/**
   @enum e_kt_clusterAlg_dbScan_howToSetFilter
   @brief provide lgocis to set diffenret filter-intereptaiton-appraoches (oekseth, 06. apr. 2017)
 **/
typedef enum e_kt_clusterAlg_dbScan_howToSetFilter {
  e_kt_clusterAlg_dbScan_howToSetFilter_useExplicitSpec, //! ie, to not 'dynamcially infer' the set of valeus/thresholds to apply/use
  e_kt_clusterAlg_dbScan_howToSetFilter_dataIsAlaredyMasked, //! which if set impleis that we assume the matrix is already 'maksed', ie, where goal/task is to connstruct a preresntive/descirptive data-subset.
  e_kt_clusterAlg_dbScan_howToSetFilter_undef
} e_kt_clusterAlg_dbScan_howToSetFilter_t;


/**
   @struct s_kt_clusterAlg_dbScan
   @brief configures the DB-scan algorithm (oekseth, 06. apr. 2017)
   @remakrs our broad number/set of configuraiton-options are deisgned to simplify/'enable' the speviidficonat/applicaion of DB-scan without any prior knopwledge of a data-set, thereby simplifying comaprison between different clster-algorithms. 
 **/
typedef struct s_kt_clusterAlg_dbScan {
  e_kt_clusterAlg_dbScan_howToSetFilter_t config_filter;
  bool useRelativeScore_betweenMinMax; bool isTo_adjustToRanks_beforeTrehsholds_forRows;
  bool config_applyFiltersOnRanks_adjustedTo_0_100; //! ie, where the ranks are adjusted/normalized to a s'core' in range [0, 100], thereby simplifying a 'gnereic spericiaton' of the db-scan-appraoch.
  bool isTo_insertIntoCompressedMatrix_beforeComptautiosn;
  t_float nodeCount_min; t_float nodeCount_max; //! ie, the min-max-spec set wrt. the number of outgoing edges from the node, ie, for the node to be 'perscived' as of interest.
  t_float edgeScore_min; t_float edgeScore_max; //! ie, the min-max-spec set wrt. the number of outgoing edges from the node, ie, for the node to be 'perscived' as of interest.
} s_kt_clusterAlg_dbScan_t;

//! @reutnr an intlazed version of our "s_kt_clusterAlg_dbScan_t" db-scan cofnigruation-object (oekseht, 06. arp. 2017)
static s_kt_clusterAlg_dbScan_t setToDefault__s_kt_clusterAlg_dbScan_t() {
  s_kt_clusterAlg_dbScan_t self;
  self.config_filter = e_kt_clusterAlg_dbScan_howToSetFilter_useExplicitSpec;
  self.useRelativeScore_betweenMinMax = true; self.isTo_adjustToRanks_beforeTrehsholds_forRows = true;
  self.config_applyFiltersOnRanks_adjustedTo_0_100 = true; //! which is used to gernealise our appraoch, ie, to 'enable' the speviidficonat/applicaion of DB-scan without any prior knopwledge of a data-set, thereby simplifying comaprison between different clster-algorithms.
  self.isTo_insertIntoCompressedMatrix_beforeComptautiosn = true;
  self.nodeCount_min = 0;   self.nodeCount_max = 10;
  self.edgeScore_min = 0;   self.edgeScore_max = 10;
  //! --------------------------
  return self;
}

//! A dummy wrapper to our "s_kt_clusterAlg_dbScan_t" free-memroy appraoch
static void free__s_kt_clusterAlg_dbScan_t(s_kt_clusterAlg_dbScan_t *self) {
  assert(self); //! and where we assuemt hta t no memroy is allcoated
}

//! Comptue disjoint-forests for a pre-masked data-set
bool compute__maskedMatrix__kt_clusterAlg_dbScan(const s_kt_matrix_t *matrix_input, const s_kt_list_1d_uint_t *obj_mapOf_rootVertices_pointer, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_hp_distanceCluster_t *objMetric__postMerge, const s_kt_api_config_t config);

/**
   @brief Compute the HCA-poartiosn based on the exetensive set of inptu-speficaitons/logics (oekseht, 06. mai. 2017)
   @param <obj_1> is the input-matrix
   @param <obj_result> is the reuslt-object
   @param <threshold_min> describes the minimum-edge-score for relationshisp to be included
   @param <threshold_max> describes the maximum-edge-score for relationshisp to be included
   @param <nodeThres_min> is the minimum number of interesting/relevant non-filtered cells/relatiionships;
   @param <nodeThres_max> is the maximum number of interesting/relevant non-filtered cells/relatiionships;
   @param <useRelativeScore_betweenMinMax> if set to true then we assume the "threshold_min, threshold_max" paramters/values refers to the "threshold(min) = score(i, j)-global(min)" and "threshold(max) = score(i, j)-global(max)". 
   @param <isTo_adjustToRanks_beforeTrehsholds_forRows> then we first comptue tha ranks before evlauting the score-thresohlds, eg, to only incldue the cells/vertices which have the top-10-per-cent score-values
   @param <forRankAdjustment_adjsutTo_0_100> if set/used then we assume the thresohld-paramters are per-cent-scores which are used in combaintion with the "useRelativeScore_betweenMinMax" option, ie, for which we udpate/ajdust the locally set socre-apratmers.
   @param <isTo_insertIntoCompressedMatrix_beforeComptautiosn> which is a permconace-tuning-paramter we use, ie, does not affect/influence the predicont-accurayc;
   @param <objMetric__postMerge> if set then we only comptue disjtointness for the (subset of) vertices with interesitng cells in range [nodeThres_min, nodeThres_max] and thereafter use the objMetric__postMerge to idneitfy/choose the membership-vertices which each vertex is to be part of.
   @param <config> provide knwoledge of of the iinptu-data is to be itnerpted/understood, eg, wrt. the case/situation where the input-amtrix si an adjcency-amtrix and for the use-case where the transposed versions (of the amtrix) is to be interpted/evalauted.
   @param <use_bruteDisjointAlg> which if set impleis that we use the dBSCAN clsuter-algorithm (instead of the defualt "hp-cluster" algorithm).
   @remarks
   --- in/through the "objMetric__postMerge" option we idneitfy clsuter-em,bershsip by first comptuign a reprenstive feautre-vector (of each vertex) and then applying the described/configured seleciton-criteria. An example-strategy/appraoch is to first comptue the meidan in each clsuter and thereafter assign the evlauted vertices to the given/chosen clsuter. A different appraoch (which ahs a higher time-cost) is to comptue the dsitance between non-asisnged versus asisnged vertices and then relate/assign each non-assigned vertex to the clsuter assited/to the cloest assigned vertex. 
**/
bool  compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, t_float threshold_min, t_float threshold_max, t_float nodeThres_min, t_float nodeThres_max, const bool nodeThresh__useDirectSCoreInteadOfCount, const bool useRelativeScore_betweenMinMax, const bool isTo_adjustToRanks_beforeTrehsholds_forRows, const bool forRankAdjustment_adjsutTo_0_100, const bool isTo_insertIntoCompressedMatrix_beforeComptautiosn, s_hp_distanceCluster_t *objMetric__postMerge, const s_kt_api_config_t config, const bool use_bruteDisjointAlg);

#endif //! EOF
