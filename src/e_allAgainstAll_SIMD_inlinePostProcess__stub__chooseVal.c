if(score != T_FLOAT_MAX) {
  //! Note: "cmp_type" is expected to be of type "e_allAgainstAll_SIMD_inlinePostProcess_t" (oeksth, 06. des. 2016).
  //! Note: "score" is expected to 'hold' the score of a given dsitance-comptaution-function
  if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {ret_val = macro_min(ret_val, score);}
  else if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {ret_val = macro_max(ret_val, score);}
  else {ret_val += score;}
 }
