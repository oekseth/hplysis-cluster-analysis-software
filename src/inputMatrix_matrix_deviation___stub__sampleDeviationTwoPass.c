// Comptute the sampel deviation (eosekth, 06. sept. 2016).
uint ncols_adjusted = 0;

//!
//! Compute the mean:
for(uint r = 0; r < nrows; r++) {
  const t_float *row = matrix[r];
  for(uint i = 0; i < ncols; i++) {
    if(isOf_interest(row[i])) {
      t_float value = row[i];
      uint cnt = 1;
#if(__localConfig__typeOf_mask == 1) //! ie, e_template_correlation_tile_maskType_mask_explicit)
      if(!mask[i]) {value = 0; cnt = 0;}
#elif(__localConfig__typeOf_mask == 2) //! ie, e_template_correlation_tile_maskType_mask_implicit) 
      if(!isOf_interest(row[i])) { value = 0; cnt = 0;}
#endif
      ncols_adjusted += cnt;
      //assert(!isinf(value));     assert(T_FLOAT_MAX != value);
      assert(isOf_interest(value));
      mean += value;
    }
    //assert(!isinf(mean));
  }
 }
// ---------------------
const t_float ncols_inv = (t_float)1/(ncols_adjusted);

t_float skewness = 0;   t_float kurtosis = 0; t_float variance = 0; 
if(ncols_adjusted == 0) {
  mean = 0; 
 } else {
  assert(ncols_adjusted != 0);
  //printf("ncols_inv=%f, ncols_adjusted=%u, mean-before-adjustment=%f, at %s:%d\n", ncols_inv, ncols_adjusted, mean, __FILE__, __LINE__);
  mean = mean * ncols_inv;
  // assert(mean != 0); //! a case which may not alwyas hodl, though expected to hold in the debug-test-cases we use.

  //!
  //! Comptue the variacne using a two-pass algorithm:



  t_float variance_step1 = 0;

  const uint ncols_minusOne = ncols - 1;
  for(uint r = 0; r < nrows; r++) {
    const t_float *row = matrix[r];
    for(uint i = 0; i < ncols_minusOne; i++) {
      if(isOf_interest(row[i])) {
	const t_float diff = mathLib_float_abs(row[i] - mean);
	//assert(diff >= 0);
	t_float value = mathLib_float_sqrt(diff);
#if(__localConfig__typeOf_mask == 1) //! ie, e_template_correlation_tile_maskType_mask_explicit)
	if(!mask[i]) {value = 0;}
#elif(__localConfig__typeOf_mask == 2) //! ie, e_template_correlation_tile_maskType_mask_implicit) 
	if(!isOf_interest(row[i])) { value = 0;}
#endif
	assert(isOf_interest(value));
	/* assert(value != T_FLOAT_MAX); */
	/* assert(!isnan(value)); */
	variance_step1 += value;
      }
    }
  }
  t_float variance_step2 = 0;
  for(uint r = 0; r < nrows; r++) {
    const t_float *row = matrix[r];
    for(uint i = 0; i < ncols_minusOne; i++) {
      // FIXME[JC]: may you validate correnctess  of using the "abs(..)" in [”elow] and [ªbov€] ... which we have 'trindocued' to avodi engative vlaeus ... ?? (oekseht, 06. otk. 2016).
      if(isOf_interest(row[i])) {
	t_float diff = mathLib_float_abs(row[i] - mean);
#if(__localConfig__typeOf_mask == 1) //! ie, e_template_correlation_tile_maskType_mask_explicit)
	if(!mask[i]) {diff = 0;}
#elif(__localConfig__typeOf_mask == 2) //! ie, e_template_correlation_tile_maskType_mask_implicit) 
	if(!isOf_interest(row[i])) { diff = 0;}
#endif
	variance_step2 += diff;
      }
    }
  }
 
  const t_float diff = variance_step1 - variance_step2;
  variance = ( (diff > 0) && (ncols_adjusted > 1) ) ? diff / (ncols_adjusted -1) : 0;
  const t_float stddev = sqrt(variance);

  // printf("variance=%f, given mean=%f and ncols_adjusted=%u and variance1=%f, variance2=%f, at %s:%d\n", variance,  mean, ncols_adjusted, variance_step1, variance_step2, __FILE__, __LINE__);  
  //!
  //! Compute kewness and kurtosis:
  if(stddev > 0) {
    const t_float stddev_inv = (t_float)1/stddev;
    for(uint r = 0; r < nrows; r++) {
      const t_float *row = matrix[r];
      for(uint i = 0; i < ncols_minusOne; i++) {
	if(isOf_interest(row[i]) == false) {continue;}
	const t_float diff = mathLib_float_abs(row[i] - mean);
	const t_float diff_relative = diff * stddev_inv;
	const t_float diff_relative_sqrt = mathLib_float_sqrt(diff_relative);
	t_float diff_relative_sqrt_mult = diff_relative_sqrt * diff_relative; //! ie, sqrt(delta/variance)*(delta/variance).
	// FIXME: 
	t_float diff_relative_sqrt_sqrt = mathLib_float_sqrt(diff_relative_sqrt);


#if(__localConfig__typeOf_mask == 1) //! ie, e_template_correlation_tile_maskType_mask_explicit)
	if(!mask[i]) {
	diff_relative_sqrt_mult = 0;
	diff_relative_sqrt_sqrt = 0;
      }
#elif(__localConfig__typeOf_mask == 2) //! ie, e_template_correlation_tile_maskType_mask_implicit) 
	if(!isOf_interest(row[i])) { 
      diff_relative_sqrt_mult = 0;
      diff_relative_sqrt_sqrt = 0;
    }
#endif
	//! Update:
	skewness += diff_relative_sqrt_mult;
	kurtosis += diff_relative_sqrt_sqrt;      
    
	if(skewness != 0) { //! which ?? could result from an overlfodw in numbers?? (oekseth, 06.01.2021)
	  assert(skewness != 0); //! ie, based on expectaiotns from [ªbove]:
	  
	  //! Complete the comptuation:
	  // assert(!isnan(skewness));
	  skewness = skewness * ncols_inv;
	  // assert(!isnan(skewness));
	  if(ncols_adjusted > 3) {
	    kurtosis = kurtosis / (ncols_adjusted - 3);    
	    // assert(!isnan(kurtosis));
	  } else {kurtosis = 0;}
	}
  }
 }
  }
}
#undef __localConfig__typeOf_mask
