{
  
  { const char *stringOf_measureText = "sort-and-rank(dense matrix): evaluate the time-cost of ranking, ie, an operation which (similar to the pre-step of sparse correlation-computation) involve sorting:";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float **matrix_tmp_1 = NULL; // t_float **matrix_tmp_2 = NULL;
    //! Allocate memory and compute the rank for the matrices.
    //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
    ktCorrelation_compute_rankFor_matrices(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, &matrix_tmp_1, &matrix_tmp_1, /*transpose=*/false, /*needTo_useMask_evaluation=*/false, UINT_MAX);
    assert(matrix_tmp_1); 	// assert(matrix_tmp_2);
    free_2d_list_float(&matrix_tmp_1, nrows); 
    //free_2d_list_float(&matrix_tmp_2);
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "sort-and-rank(dense matrix): evaluate the time-cost of ranking, ie, an operation which (similar to the pre-step of sparse correlation-computation) involve sorting:";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float **matrix_tmp_1 = NULL; // t_float **matrix_tmp_2 = NULL;
    //! Allocate memory and compute the rank for the matrices.
    //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
    ktCorrelation_compute_rankFor_matrices(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, &matrix_tmp_1, &matrix_tmp_1, /*transpose=*/false, /*needTo_useMask_evaluation=*/false, UINT_MAX);
    assert(matrix_tmp_1); 	// assert(matrix_tmp_2);
    free_2d_list_float(&matrix_tmp_1, nrows); 
    //free_2d_list_float(&matrix_tmp_2);
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "sort-and-rank(dense matrix::implicit-mask): evaluate the time-cost of ranking, ie, an operation which (similar to the pre-step of sparse correlation-computation) involve sorting:";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float **matrix_tmp_1 = NULL; // t_float **matrix_tmp_2 = NULL;
    //! Allocate memory and compute the rank for the matrices.
    //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
    ktCorrelation_compute_rankFor_matrices(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, &matrix_tmp_1, &matrix_tmp_1, /*transpose=*/false, /*needTo_useMask_evaluation=*/true, UINT_MAX);
    assert(matrix_tmp_1); 	// assert(matrix_tmp_2);
    free_2d_list_float(&matrix_tmp_1, nrows); 
    //free_2d_list_float(&matrix_tmp_2);
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "sort-and-rank(dense matrix::implcit-mask): evaluate the time-cost of ranking, ie, an operation which (similar to the pre-step of sparse correlation-computation) involve sorting:";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float **matrix_tmp_1 = NULL; // t_float **matrix_tmp_2 = NULL;
    //! Allocate memory and compute the rank for the matrices.
    //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
    ktCorrelation_compute_rankFor_matrices(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, &matrix_tmp_1, &matrix_tmp_1, /*transpose=*/false, /*needTo_useMask_evaluation=*/true, UINT_MAX);
    assert(matrix_tmp_1); 	// assert(matrix_tmp_2);
    free_2d_list_float(&matrix_tmp_1, nrows); 
    //free_2d_list_float(&matrix_tmp_2);
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
}
//! ---------------------------------------------------
{ //! Evaluate wrt. rank-logics in our "correlation_rank_rowPair.h"
  { const char *stringOf_measureText = "sort-and-rank-2d(dense-matrix): used as a point-of-reference wrt. a non-optimized computation-strategies for rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//const uint row_id_out = 1; {
	s_correlation_rank_rowPair_t self; s_correlation_rank_rowPair_allocate(&self, /*ncols=*/size_of_array);
	s_correlation_rank_rowPair_compute_nonTransposed(&self, row_id, row_id_out, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, e_distance_rank_typeOf_computation_ideal, /*needTo_useMask_evaluation=*/false);
	//! ------------------------
	//! De-allcoate
	s_correlation_rank_rowPair_free(&self);
      }
	    
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  /* { const char *stringOf_measureText = "sort-and-rank-2d(dense-matrix): used as a potin-of-reference wrt. a non-optimized computation-strategies for rank-based correaltion-metrics"; */
  /*   //! ------------------------------- */
  /*   //! Start the clock: */
  /*   start_time_measurement();  //! ie, start measurement for a 'compelte matrix' */
  /*   //! */
  /*   //! */
  /*   //! The experiemnt: */
  /*   for(uint row_id = 0; row_id < nrows; row_id++) { */
  /*     //const uint row_id_out = 1; { */
  /*     for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) { */
  /* 	s_correlation_rank_rowPair_t self; s_correlation_rank_rowPair_allocate(&self, /\*ncols=*\/size_of_array); */
  /* 	s_correlation_rank_rowPair_compute_transposed(&self, row_id, row_id_out, /\*ncols=*\/size_of_array, matrix, matrix, /\*mask1=*\/NULL, /\*mask2=*\/NULL, e_distance_rank_typeOf_computation_ideal, /\*needTo_useMask_evaluation=*\/false); */
  /* 	//! ------------------------ */
  /* 	//! De-allcoate */
  /* 	s_correlation_rank_rowPair_free(&self); */
  /*     } */
	    
  /*   } */
  /*   //! -------------- */
  /*   __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	   */
  /* } */
  /* //! ---------------- */
  /* { const char *stringOf_measureText = "sort-and-rank-2d(dense-matrix::mask-implicit): used as a potin-of-reference wrt. a non-optimized computation-strategies for rank-based correaltion-metrics"; */
  /*   //! ------------------------------- */
  /*   //! Start the clock: */
  /*   start_time_measurement();  //! ie, start measurement for a 'compelte matrix' */
  /*   //! */
  /*   //! */
  /*   //! The experiemnt: */
  /*   for(uint row_id = 0; row_id < nrows; row_id++) { */
  /*     //const uint row_id_out = 1; { */
  /*     for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) { */
  /* 	s_correlation_rank_rowPair_t self; s_correlation_rank_rowPair_allocate(&self, /\*ncols=*\/size_of_array); */
  /* 	s_correlation_rank_rowPair_compute_transposed(&self, row_id, row_id_out, /\*ncols=*\/size_of_array, matrix, matrix, /\*mask1=*\/NULL, /\*mask2=*\/NULL, e_distance_rank_typeOf_computation_ideal, /\*needTo_useMask_evaluation=*\/true); */
  /* 	//! ------------------------ */
  /* 	//! De-allcoate */
  /* 	s_correlation_rank_rowPair_free(&self); */
  /*     } */
	    
  /*   } */
  /*   //! -------------- */
  /*   __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	   */
  /* } */
  //! -----------
}
