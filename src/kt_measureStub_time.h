#ifndef kt_measureStub_time_h
#define kt_measureStub_time_h
//! 
//! The wrapper call for "tut_time_*." test-stubs (oekseth, 06. otk. 2017).
bool kt_measureStub_time(const int array_cnt, char **array);


#endif //! EOF
