#ifndef row_chunk_h
#define row_chunk_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */



/* #ifndef configIsCalledFromExternal_software */
#include "def_intri.h"
#include "types.h"
#include "configure_hcaMem.h"

/**
   @struct s_dense_closestPair_t
   @brief provide data-structures to idnetify and update the row-chunks
   @remarks this dat-astrucutre is used in order to reduce the nubmer fo rows which need to be iterated wrt. row-identifciation.
   @author Ole Kristian Ekseth (oekseth)
**/
typedef struct s_rowChunk { 
  uint _bucket_sizeOfRows;  uint _cnt_rowBuckets; uint _cnt_columnBuckets;
  t_float **matrixOf_globalColumnTiles; //! which store for [columnBucket][rowBucket];
  uint **matrixOf_globalColumnTiles_rowId;
  char **matrixOf_globalColumnTiles_bucketIsUpTo_date; //! which is used to idnetify those buckets which need to be udpated.
} s_rowChunk_t;

#define s_rowChunk_getCnt_rowBuckets(self) ({self->_cnt_rowBuckets;})
#define s_rowChunk_getSize_rowBucketEach(self) ({self->_bucket_sizeOfRows;})

//! Intialises the s_rowChunk_t object
void init_s_rowChunk(s_rowChunk_t *self, const uint cnt_bucketsEachRows, const uint nrows);

//! De-allcoates an s_rowChunk_t object.
void free_s_rowChunk(s_rowChunk_t *self);

//! @return true if the [column-chunk][row-chunk] has nto chagned.
#define s_rowChunk_hasNotChanged(self, columnBucket_id, rowBucket_id) ({ \
  assert(self); assert_local(columnBucket_id < self->_cnt_columnBuckets); assert_local(rowBucket_id < self->_cnt_rowBuckets); \
  assert(self->matrixOf_globalColumnTiles_bucketIsUpTo_date);		\
  self->matrixOf_globalColumnTiles_bucketIsUpTo_date[columnBucket_id][rowBucket_id];})
//! Set the [column-chunk][row-chunk]  to changed.
#define s_rowChunk_setToChanged(self, columnBucket_id, rowBucket_id) ({ \
      assert(self); assert_local(columnBucket_id < self->_cnt_columnBuckets); assert_local(rowBucket_id < self->_cnt_rowBuckets); \
      self->matrixOf_globalColumnTiles_bucketIsUpTo_date[columnBucket_id][rowBucket_id] = true; \
})

//! @return the assicated row-chunk-if for the row-id in question.
#define get_idOf_rowChunk(self, row_id) ({ \
      assert(self);  \
      uint rowBucket_id = (row_id != 0) ? (row_id / self->_cnt_rowBuckets) : 0; \
      rowBucket_id;}) //! ie, return.

//! Set the min-pos for the given (column-chunk)(row-chunk).
#define s_rowChunk_setMinPos(self, columnBucket_id, rowBucket_id, row_id, score) ({ \
  assert(self); assert_local(columnBucket_id < self->_cnt_columnBuckets); assert_local(rowBucket_id < self->_cnt_rowBuckets); \
  self->matrixOf_globalColumnTiles[columnBucket_id][rowBucket_id] = score; \
  self->matrixOf_globalColumnTiles_rowId[columnBucket_id][rowBucket_id] = row_id; })
//! Set the min-pos for the given (column-chunk)(row-chunk).
#define s_rowChunk_setMinPos_ifImproved(self, columnBucket_id, rowBucket_id, row_id, score) ({ \
  assert(self); assert_local(columnBucket_id < self->_cnt_columnBuckets); assert_local(rowBucket_id < self->_cnt_rowBuckets); \
  if(self->matrixOf_globalColumnTiles[columnBucket_id][rowBucket_id] > score) {\
    self->matrixOf_globalColumnTiles[columnBucket_id][rowBucket_id] = score; \
    self->matrixOf_globalColumnTiles_rowId[columnBucket_id][rowBucket_id] = row_id; \
    assert(self->matrixOf_globalColumnTiles_bucketIsUpTo_date); \
    /*! Update hte bucket-status: */ \
    self->matrixOf_globalColumnTiles_bucketIsUpTo_date[columnBucket_id][rowBucket_id] = false; \
  } })

#endif //! EOF
