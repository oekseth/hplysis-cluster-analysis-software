#ifndef e_kt_clusterAlg_exportFormat_h
#define e_kt_clusterAlg_exportFormat_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file e_kt_clusterAlg_exportFormat
   @brief enumerate diffferent cluster-export-foramts (oekseth, 06. jan. 2017).
 **/

/**
   @enum e_kt_clusterAlg_exportFormat
   @brief enumerate different exprot-formats (oekseth, 06. jan. 2017).
   @remarks used to genralize our exprot-format-calls, tehreby simplifying gneeirc itnerface-writing.
 **/
typedef enum e_kt_clusterAlg_exportFormat {
  e_kt_clusterAlg_exportFormat_vertex_toClusterIds,   //! which 'maps' each vertex to the cluster-id (ie, k-cluster-id=5)
  e_kt_clusterAlg_exportFormat_vertex_toCentroidIds, //! which 'maps' each vertex to the vertex-centroid-id (eg, "gene(a) in-clsuter pathway(b)").
  e_kt_clusterAlg_exportFormat_distanceToCentroids, //! the distance from each vertex to the centdis in each candiate-vertex-set.
  //! -------------
  e_kt_clusterAlg_exportFormat_all,
  //e_kt_clusterAlg_exportFormat_
} e_kt_clusterAlg_exportFormat_t;

#endif //! EOF
