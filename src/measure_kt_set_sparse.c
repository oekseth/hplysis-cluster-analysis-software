#include "measure_kt_set_sparse.h" 
 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "kt_set_1dsparse.h"
#include "kt_set_2dsparse.h"
#include "correlation_rank.h"
#include "correlation_rank_rowPair.h"
#include "correlationType_kendall.h"
#include "correlation_inCategory_rank_kendall.h"

static  const uint arrOf_chunkSizes_size = 6;
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
static  const uint base_size =  VECTOR_INT_ITER_SIZE_short;
static  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      
							      100*base_size,
							      kilo*base_size, 
							      2*kilo*base_size,
							      3*kilo*base_size, 4*kilo*base_size,
							      //80*kilo*base_size, 160*kilo*base_size,
							      //300*kilo*base_size, 600*kilo*base_size,
};



//! Test the correctenss of our approaches, ie, wrt. our "kt_set_1dsparse" and "kt_set_2dsparse" (oekseth, 06. otk. 2016).
static void test_correctness() {
  const uint arrOf_chunkSizes_size = 3;
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint base_size =  VECTOR_INT_ITER_SIZE_short;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      
							      100*base_size,
							      200*base_size, 
							      /* 2*kilo*base_size, */
							      /* 3*kilo*base_size, 4*kilo*base_size, */
							      //80*kilo*base_size, 160*kilo*base_size,
							      //300*kilo*base_size, 600*kilo*base_size,
};


  {
    { //! A simple applocation-de-allcoation for 'empty' cases:
      //! Allocate 2d-list
      s_kt_set_1dsparse_t obj_1; setTo_Empty__s_kt_set_1dsparse_t(&obj_1);
      s_kt_set_2dsparse_t obj_2; setTo_Empty__s_kt_set_2dsparse_t(&obj_2);
      //! De-allcoate:
      free_s_kt_set_2dsparse_t(&obj_2);     free_s_kt_set_1dsparse_t(&obj_1);
    }
    { //! A simple applocation-de-allcoation for 'partial empty' cases:
      //! Allocate 2d-list
      //! -------------------------------
      const uint nrows = 200; const uint ncols = 0;
      t_float **matrix = NULL;       t_float *row = NULL;  char **matrix_mask = NULL; char *mask = NULL;
      const bool isTo_useImplictMask = false; const bool isTo_allocateFor_scores = false;
      //! -------------------------------
      //! Perform muliple iteraitons 
      for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
	//! -------------------------------
	s_kt_set_1dsparse_t obj_1; setTo_Empty__s_kt_set_1dsparse_t(&obj_1);
	s_kt_set_2dsparse_t obj_2; setTo_Empty__s_kt_set_2dsparse_t(&obj_2);
	//! -------------------------------
	//! Allocate:
	allocate__s_kt_set_2dsparse_t(&obj_2, nrows, ncols, matrix, matrix_mask, isTo_useImplictMask, isTo_allocateFor_scores);
	allocate__s_kt_set_1dsparse_t(&obj_1, nrows, row, mask, isTo_useImplictMask, isTo_allocateFor_scores);
	//! ******************************************************************************* 
	const uint size_of_array = arrOf_chunkSizes[chunk_index];
	{ //! Test push-and-pop:
	  printf("#\t size_of_array=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
	  for(uint col_id = 0; col_id < size_of_array; col_id++) {
	    //! For the 1d-lsit add the lement to the stack:
	    push__s_kt_set_1dsparse_t(&obj_1, /*vertex_to_add=*/size_of_array - col_id);

	    for(uint row_id = 0; row_id < nrows; row_id++) {
	      //!
	      //! Add an entry to the stack:
	      push__s_kt_set_2dsparse_t(&obj_2, row_id, /*vertex_to_add=*/size_of_array - col_id);
	      //! Validate that the entites are set iaw. [above], ie, that the 'memory-enlarge' operation does not corrupt the results:
	      for(uint col_old = 0; col_old < col_id; col_old++) {
		const uint vertex = size_of_array - col_old;
		const uint inserted_vertex = obj_2.matrixOf_id[row_id][2+col_old];
		if(vertex != inserted_vertex) {
		  printf("[row=%u]\tcol_id=%u/%u, size_of_array=%u, vertex(expected, actual)=(%u, %u), at %s:%d\n", row_id, col_old, col_id, size_of_array, vertex, inserted_vertex, __FILE__, __LINE__);		
		}
		assert(vertex == inserted_vertex);
	      }
	    }
	  }
	  { //! ----------------------------
	  //! Tehreafter pop the elemnets for the 1d-object:
	    for(uint col_id = 0; col_id < size_of_array; col_id++) {
	      t_float score = T_FLOAT_MAX;
	      const uint vertex = pop__s_kt_set_1dsparse_t(&obj_1, &score);
	      if(vertex != col_id) {
		printf("col_id=%u, size_of_array=%u, vertex=%u, at %s:%d\n", col_id, size_of_array, vertex, __FILE__, __LINE__);
	      }
	      assert(vertex != UINT_MAX);
	      assert(vertex == /*vertex_to_add=*/(col_id));
	      /* if(isTo_allocateFor_scores == true) { */
	      /*   assert(score != T_FLOAT_MAX); //! ie, as we then expect 'this' to have been implictly inserted. */
	      /* } */
	    }
	    t_float score = T_FLOAT_MAX;
	    const uint vertex = pop__s_kt_set_1dsparse_t(&obj_1, &score);
	    assert(vertex == UINT_MAX); //! ie, as we expec tthat 'at this eexduction-point' there are no mroe vertices to pop.
	  }
	  //! ----------------------------
	  //! Tehreafter pop the elemnets for the 1d-object:
	  for(uint col_id = 0; col_id < size_of_array; col_id++) {
	    for(uint row_id = 0; row_id < nrows; row_id++) {
	      t_float score = T_FLOAT_MAX;
	      const uint vertex = pop__s_kt_set_2dsparse_t(&obj_2, row_id, &score);
	      assert(vertex == /*vertex_to_add=*/(col_id));
	    }
	    /* if(isTo_allocateFor_scores == true) { */
	    /*   assert(score != T_FLOAT_MAX); //! ie, as we then expect 'this' to have been implictly inserted. */
	    /* } */
	  }
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    t_float score = T_FLOAT_MAX;
	    const uint vertex = pop__s_kt_set_2dsparse_t(&obj_2, row_id, &score);
	    assert(vertex == UINT_MAX); //! ie, as we expec tthat 'at this eexduction-point' there are no mroe vertices to pop.
	  }
	}	
	//! -------------------------------
	{ //! Test wrt. inseriotn of 'complete rows':
	  const uint cnt_innerChunk = 100; const uint cnt_insertions = size_of_array / cnt_innerChunk;
	  const uint default_value_uint = 0;
	  uint *row_cpy = allocate_1d_list_uint(cnt_innerChunk, default_value_uint);
	  for(uint i = 0; i < cnt_innerChunk; i++) {row_cpy[i] = i;} //! ie, to simplify correctness-testing:
	  //!
	  //! Test for the 1d-set
	  for(uint i = 0; i < cnt_insertions; i++) {
	    add_rowSparse__s_kt_set_1dsparse_t(&obj_1, row_cpy, cnt_innerChunk, /*weight=*/NULL);
	    //! Validate correctness:
	    for(uint prev = 0; prev < i; prev ++) {
	      const uint start_pos = prev * cnt_innerChunk;
	      //const uint end_pos = (prev+1) * cnt_innerChunk;
	      for(uint k = 0; k < cnt_innerChunk; k++) {
		const uint pos = start_pos + k;
		const uint vertex = obj_1.rowOf_id[2+pos];
		assert(vertex == row_cpy[k]); //! ie, what we expect.
	      }
	    }
	  }

	  //!
	  //! Test for the 2d-set
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    for(uint i = 0; i < cnt_insertions; i++) {
	      add_rowSparse__s_kt_set_2dsparse_t(&obj_2, row_id, row_cpy, cnt_innerChunk, /*weight=*/NULL);
	      //! Validate correctness:
	      for(uint prev = 0; prev < i; prev ++) {
		const uint start_pos = prev * cnt_innerChunk;
		//const uint end_pos = (prev+1) * cnt_innerChunk;
		for(uint k = 0; k < cnt_innerChunk; k++) {
		  const uint pos = start_pos + k;
		  const uint vertex = obj_2.matrixOf_id[row_id][2+pos];
		  assert(vertex == row_cpy[k]); //! ie, what we expect.
		}
	      }
	    }
	  }
	  //! -------------------------------
	  { //! Test wrt. sorting:
	    //! Teh call:
	    sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(&obj_2, /*isTo_sortKeys=*/true);
	    //!  Validate the sorted proeprty:
	    for(uint row_id = 0; row_id < nrows; row_id++) {
	      const uint cnt_elements = obj_2.matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos] + 1;
	      assert(cnt_elements > 0);
	      uint prev_index = obj_2.matrixOf_id[row_id][2];
	      for(uint col_id = 1; col_id < cnt_elements; col_id++) {
		assert(prev_index <= obj_2.matrixOf_id[row_id][2+col_id]);
		prev_index = obj_2.matrixOf_id[row_id][2+col_id];
	      }
	    }
	  }

	  //! -------------------------------
	  //! De-allocate
	  free_1d_list_uint(&row_cpy);
	}
	//! -------------------------------
	//! De-allcoate:
	free_s_kt_set_2dsparse_t(&obj_2);     free_s_kt_set_1dsparse_t(&obj_1);
      }      
    }
  }
}

//! A strucutre to test the time-cost wrt. implict memory-linkage.
typedef struct testStruct_links {
  uint key; t_float score; testStruct_links *link_parent;
} testStruct_links_t;
//! Initaite the testStruct_links_t object
static void allocate_testStruct_links_t(testStruct_links_t *self, const uint key, const t_float score, testStruct_links *link_parent) {
  assert(self);
  self->key = key;
  self->score = score;
  self->link_parent = link_parent;
}
static testStruct_links_t *memAlloc__testStruct_links(const uint key, const t_float score, testStruct_links *link_parent) {
  testStruct_links_t *self = (testStruct_links_t*)malloc(sizeof(testStruct_links_t));
  allocate_testStruct_links_t(self, key, score, link_parent);
  return self;
}
//! Allocate a 1d-list from a row of elements.
//! @return the last chil which is allcoated in a 1d-list.
static testStruct_links_t *memAlloc_recursive__testStruct_links(const t_float *row, const uint row_size) {
  uint key = 0;
  testStruct_links_t *link_parent = NULL;
  testStruct_links_t *last_child = memAlloc__testStruct_links(key, row[key], link_parent);
  for(uint key = 1; key < row_size; key++) {
    testStruct_links_t *link_parent = last_child; //! ie, the previous parent
    last_child = memAlloc__testStruct_links(key, row[key], link_parent);
  }
  return last_child;
}
//! De-allcoate a particular object.
static void free__testStruct_links(testStruct_links_t *self) {
  assert(self);
  free(self);
}
//! Let each vertex call its parent: we assume that the list is 1d, ie, theat a parent is freed only once, ie, to simplify our test-setup.
static void free__recursive_testStruct_links(testStruct_links_t *self) {
  assert(self);
  if(self->link_parent) {
    assert(self->link_parent != self);
    free__recursive_testStruct_links(self->link_parent); 
  }
  free(self);
}
static void get_sumOf_elements_recursive__testStruct_links_t(testStruct_links_t *self, t_float *scalar_sumOf) {
  assert(self);
  *scalar_sumOf += self->score;
  if(self->link_parent) {
    assert(self->link_parent != self);
    get_sumOf_elements_recursive__testStruct_links_t(self->link_parent, scalar_sumOf); //! ie, then 'let' the parent add its elements.
  }
}

//! Compare the exueicont-itme of a poitner-based list-traversal:
static void __compare__list_pointerAccess() {
    //! -------------------------------
  //! Perform muliple iteraitons 
  const uint nrows = 1000; //*1000; 
  //  const uint nrows = 10000; //*1000; 
  //  const uint nrows = 100000; //*1000; 
  //const uint cnt_iter = 40*50;
  const uint cnt_iter = 100*500;
  //  const uint cnt_iter = 100*50;
  //const uint cnt_iter = 10*50;
  const uint cnt_chinks = 10; 
  //const uint chunk_size = nrows * 100*40; 
  const uint chunk_size = nrows * 100*10*100; 
  //const uint chunk_size = nrows * 100*5; 
  for(uint chunk_index = 0; chunk_index < cnt_chinks; chunk_index++) {
  //for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint size_of_array = chunk_size*(chunk_index+1);
    //const uint size_of_array = nrows*(10+arrOf_chunkSizes[chunk_index]);
    assert(nrows < size_of_array);
    assert(nrows < (1000*1000*1000));
    const uint val_0 = 0;
    uint *arrOf_scores = allocate_1d_list_uint(size_of_array, val_0);
    for(uint i = 0; i < size_of_array; i++) {arrOf_scores[i] = (uint)rand() % size_of_array;}
    assert(nrows < size_of_array);
    //! -------------------------------
    t_float resultOf_lastMeasurement = 0;
    { const char *stringOf_measureText = "hpLysis-listBased-iteration"; //! For a sparse set:
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      uint sumOf_values = 0;
      const uint *__restrict__ list = arrOf_scores;
      for(uint i = 0; i < cnt_iter; i++) {
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  sumOf_values += list[row_id];
	}	
      }
      //! Complete:
      resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, resultOf_lastMeasurement); 
    }
    { const char *stringOf_measureText = "hpLysis-listBased-iteration--compareSearch-inLast"; //! For a sparse set:
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      uint sumOf_values = 0;
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint i = 0; i < cnt_iter; i++) {
	  sumOf_values += arrOf_scores[row_id];
	}	
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, resultOf_lastMeasurement); 
    }
    { const char *stringOf_measureText = "random-access";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	uint sumOf_values = 0;
	for(uint i = 0; i < cnt_iter; i++) {
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    const uint index = rand() % (size_of_array-1);
	    sumOf_values += arrOf_scores[index];
	  }  
	}
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, resultOf_lastMeasurement); 
    }
    assert(arrOf_scores);free_1d_list_uint(&arrOf_scores); arrOf_scores = NULL;
  }


}


//! Idneitfy the perofmrance-penelaties wrt. our 'clases' when comapred to (a) linked-lsits and (b) correlation-comptuation from dense matrices.
static void data_access() {
  __compare__list_pointerAccess();
  assert(false);

  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_chunkSizes_size, default_value_float);

  const bool isTo_evlauateTimeCost_metrics = false;
  const bool use_exntiveEval = false;



  //! -------------------------------
  //! Perform muliple iteraitons 
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    const uint nrows = 200; 
    printf("----------\n# \t nrows=%u and list-size=%u, at %s:%d\n", nrows,size_of_array, __FILE__, __LINE__);
    //s_kt_set_1dsparse_t obj_1; setTo_Empty__s_kt_set_1dsparse_t(&obj_1);
    //! -------------------------------
    const uint ncols = 0;
    const t_float default_value_float = 0;     const char default_value_char = 0;
    t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
    char **mask1 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
    char **mask2 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
    int **mask1_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
    int **mask2_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
    t_float *weight = allocate_1d_list_float(size_of_array, default_value_float);
    t_float *row = NULL;  char **matrix_mask = NULL; char *mask = NULL;
    const bool isTo_useImplictMask = false; const bool isTo_allocateFor_scores = true;
    //! -------------------------------
    s_kt_set_2dsparse_t obj_2; setTo_Empty__s_kt_set_2dsparse_t(&obj_2);
    //! -------------------------------
    //! Allocate:
    allocate__s_kt_set_2dsparse_t(&obj_2, nrows, ncols, /*matrix=*/NULL, matrix_mask, isTo_useImplictMask, isTo_allocateFor_scores);
    testStruct_links_t **arrOf_linkedListRef_endPos = (testStruct_links_t**)malloc(sizeof(testStruct_links_t)*nrows);
    //! -------------------------------------------
    //! Udpat ethe valeus speciflcayy:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < size_of_array; col_id++) {
	matrix[row_id][col_id] = (col_id + 10)/(row_id  +0.5);
      }
      //! Update the given sparse row:
      assert(matrix[row_id]);
      add_row_maskImplicit__s_kt_set_2dsparse_t(&obj_2, row_id, matrix[row_id], /*ncols=*/size_of_array);
      //! Allocate for the linked list:
      arrOf_linkedListRef_endPos[row_id] = memAlloc_recursive__testStruct_links(matrix[row_id], /*ncols=*/size_of_array);
    }
    //allocate__s_kt_set_1dsparse_t(&obj_1, nrows, row, mask, isTo_useImplictMask, isTo_allocateFor_scores);
    //! ******************************************************************************* 


    { //! Traversal time for data-sets: we expec thtere to  not be any difference.
      { const char *stringOf_measureText = "dense row-iteration: traversal-time for a 3d-matrix-iteration: test for naive-contius-summation using"; //! For a sparse set:
#define __use_Result_to_updateTableComparisonResults 1
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	t_float sumOf_values = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	  //for(uint row_id = 0; row_id < nrows; row_id++) {
	    for(uint col_id = 0; col_id < size_of_array; col_id++) {
	      sumOf_values += matrix[row_id][col_id];
	    }
	  }
	}
	
	const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
	mapOf_timeCmp_forEachBucket[chunk_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif
#undef __use_Result_to_updateTableComparisonResults
      }
#define __use_Result_to_updateTableComparisonResults 0
      if(use_exntiveEval) { const char *stringOf_measureText = "dense row-iteration-4d: traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	t_float sumOf_values = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	    //for(uint row_id = 0; row_id < nrows; row_id++) {
	    for(uint col_id = 0; col_id < size_of_array; col_id++) {
	      for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
		sumOf_values += matrix[row_id][col_id];
	      }
	    }
	  }
	}
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
      }
      { const char *stringOf_measureText = "sparse row-iteration (t_float-weights): traversal-time for a 3d-matrix-iteration: test the overhead wrt. the 'need' to idneitfy the list-size explcitly for each call, an overhead which is expected to be in-singificant"; //! For a sparse set: //! For a dense matrix:
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	t_float sumOf_values = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {	  
	  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	  //for(uint row_id = 0; row_id < nrows; row_id++) {
	    uint row_size = 0; const t_float *row = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&obj_2, row_id, &row_size);
	    // printf("[row=%u]\trow_size=%u, size_of_array=%u, at %s:%d\n", row_id, row_size, size_of_array, __FILE__, __LINE__);
	    assert(row_size == size_of_array); //! ie, what we expect
	    assert(row); assert(row_size > 0);
	    for(uint col_id = 0; col_id < row_size; col_id++) {
	      sumOf_values += row[col_id];
	    }
	  }
	}
	
	//const t_float resultOf_lastMeasurement = 
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
      }
      { const char *stringOf_measureText = "sparse row-iteration (uint-index): traversal-time for a 3d-matrix-iteration: test the overhead wrt. the 'need' to idneitfy the list-size explcitly for each call, an overhead which is expected to be in-singificant"; //! For a sparse set: //! For a dense matrix:
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	t_float sumOf_values = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {	  
	  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	  //for(uint row_id = 0; row_id < nrows; row_id++) {
	    uint row_size = 0; const uint *row = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&obj_2, row_id, &row_size);
	    // printf("[row=%u]\trow_size=%u, size_of_array=%u, at %s:%d\n", row_id, row_size, size_of_array, __FILE__, __LINE__);
	    assert(row_size == size_of_array); //! ie, what we expect
	    assert(row); assert(row_size > 0);
	    for(uint col_id = 0; col_id < row_size; col_id++) {
	      sumOf_values += row[col_id];
	    }
	  }
	}
	
	//const t_float resultOf_lastMeasurement = 
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
      }
      { const char *stringOf_measureText = "pointers: linked-row-iteration: for a set of implcit links, eg, the dfault approach wrt. stack-allocations and data-trveral in high-level programming-languages: in this example we test an ideal case where the alloncaiton-order relates directly ot the travesal-order, ie, where we in practise exepcts considerably worse perfomorance wr.t linked-list data-iteraiton";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	t_float sumOf_values = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {	  
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    get_sumOf_elements_recursive__testStruct_links_t(arrOf_linkedListRef_endPos[row_id], &sumOf_values);
	  }
	}
	
	//const t_float resultOf_lastMeasurement = 
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
      }
    }
    //! -------------------------------
    { //! Comapre the exeuction-time overehad of saprse matrices wrt. comptuation of correlation-metrics, ie, when compared to a dense-list-of-values:
      { const char *stringOf_measureText = "correlation: dense-mulitplication: time-cost of accessing two different 'memory-strains'."; //! Evaluate for a dense matrix of entites:
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	//! The experiemnt:
	t_float sumOf_values = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	    for(uint col_id = 0; col_id < size_of_array; col_id++) {
	      sumOf_values += matrix[row_id][col_id] * matrix[row_id_out][col_id];
	    }
	  }
	}
	
	//const t_float resultOf_lastMeasurement = 
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
      }

      if(isTo_evlauateTimeCost_metrics) { //!  Evalaute the time-cost of ranking, ie, an operation which (similar to the pre-step of sparse correlation-computation) involve sorting:
#include "measure__stub__3d__correlationRank__timeCost__sparseMatrixInput.c"

      }
      //! ---------------------------------------------------      
      if(isTo_evlauateTimeCost_metrics) { //! Compare wrt. comptuation of Kendall:
#define __localConfig__in2dKendall__evalauteTransposedCase 0
      t_float **matrix_transposed = allocate_2d_list_float(size_of_array, nrows, default_value_float);
#include "measure__stub__2d__kendall.c"
      free_2d_list_float(&matrix_transposed, size_of_array);
	//! ----------------
      }
      //! ---------------------------------------------------
      if(isTo_evlauateTimeCost_metrics) { //! Evaluate for a sparse matrix of entites:
#include "measure__stub__3d__correlationDelta__euclid__sparseMatrixInput.c"
      }
    }


    //! -------------------------------
    //! De-allcoate:
    free_s_kt_set_2dsparse_t(&obj_2); //     free_s_kt_set_1dsparse_t(&obj_1);
    free_2d_list_float(&matrix, nrows);
    free_2d_list_char(&mask1, nrows);
    free_2d_list_char(&mask2, nrows);
    free_2d_list_int(&mask1_int, nrows);
    free_2d_list_int(&mask2_int, nrows);
    free_1d_list_float(&weight);
    for(uint row_id = 0; row_id < nrows; row_id++) {	  
      free__recursive_testStruct_links(arrOf_linkedListRef_endPos[row_id]);
    }
    free(arrOf_linkedListRef_endPos);
  }
  //!
  //!  De-allocate locally reserved memory
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}

//! The main assert function.
void measure_kt_set_sparse__main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  if(isTo_processAll || (0 == strncmp("test_correctness", array[2], strlen("test_correctness"))) ) {
    test_correctness();
    cnt_Tests_evalauted++;
  }



  if(isTo_processAll || (0 == strncmp("data_access", array[2], strlen("data_access"))) ) {
    data_access();
    cnt_Tests_evalauted++;
  }



  /* if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) { */
  /*   assert(false); // FIXME: add something */
  /*   cnt_Tests_evalauted++; */
  /* } */



  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}


