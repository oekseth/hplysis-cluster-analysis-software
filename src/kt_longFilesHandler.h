#ifndef kt_longFilesHandler_h
#define kt_longFilesHandler_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_longFilesHandler
   @brief provide logics to support the sue of long file-names, ie, file-naems which includes a 'bunch' of emta-ifnromation.
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016).
**/


#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "measure_base.h"

/**
   @struct s_kt_longFilesHandler
   @brief provide logics to support the sue of long file-names, ie, file-naems which includes a 'bunch' of emta-ifnromation.
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016).
**/
typedef struct s_kt_longFilesHandler {
  FILE *stream_out;
  bool stream_out__isAllocated;
  long long unsigned int currentCounter;
  const char *result_directory;
} s_kt_longFilesHandler_t;

/**
   @brief itnaites an object to write out a 'mapping' between 'ifnroamtive files-names' and 'file-naems which are short enough to be acceptable on a gvine system' (oekseth, 06. des. 2016)
   @param <self> is the object to hold teh data
   @param <result_directory> which if Not set to NULL describes the directory 'to store the data in'
   @param <file_nameMeta> is the file-naem to exprot the 'file-name-mappigns' to.
   @param <stringOf_header> if Not set to NULL then we write this strin 'to the top' of the file
   @param <stream_out> which if Not set to NULL implies that we use a use-specified file-dseciprtior.
   @return an itnaited object
   @remarks a use-case for "stream_out" conserns:
   -- system-speicifc: eg, to use "stdout" or "stderr"
   -- result-concatnation: to 'merge' muliple header-files, eg, by suing the same "stream_out" in muliple objects
 **/
s_kt_longFilesHandler_t initAndReturn__s_kt_longFilesHandler_t(const char *result_directory, const char *file_nameMeta, const char *stringOf_header, FILE *stream_out);

//! De-allcoates the s_kt_longFilesHandler_t object
void free__s_kt_longFilesHandler_t(s_kt_longFilesHandler_t *self);

/**
   @brief allcoates a file-naem which is accrtaapble 'on file-systems' and include 'into the result-file' a mapping between 'the enw-file-name' and 'internal-file-anems'.
   @param <self> is the object to hold teh data
   @param <long_file_name> is the file-name to 'store' in teh exprot-file
   @param <prefix> which if Not set to NULL is 'inlcuded' prior to the 'unqiue file-tag'
   @return a new-allcoated file-naem whcih 'is short enough to be used on all/most OS (oekseth, 06. des. 2016).
 **/
char *allocateDenseFileName__s_kt_longFilesHandler_t(s_kt_longFilesHandler_t *self, const char *long_file_name, const char *prefix);

#endif //! EOF
