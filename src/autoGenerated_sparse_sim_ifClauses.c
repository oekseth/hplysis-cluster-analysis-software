/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @brie defines seperately each sparse simlairty/correlation-fucnton, a strategy used for opmtiziaotn (oekseth, 6. mar. 2017).
   @remarks last generated at Fri Oct 13 17:49:55 2017 from "build_codeFor_tiling.pl"
 **/
//! ---------------------------------------------------------------------------------------------------------------------------------------------------------
//!
//! Define muliple functions:
//! Note: the distance-macros are expected to be found in our "correlation_macros__distanceMeasures.h"
//! 

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_euclid;} else {return &kt_sparse_computeForMetric_adjust_euclid;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_euclid;} else {return &kt_sparse_computeForMetric_euclid;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_cityBlock;} else {return &kt_sparse_computeForMetric_adjust_cityBlock;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_cityBlock;} else {return &kt_sparse_computeForMetric_cityBlock;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_minkowski;} else {return &kt_sparse_computeForMetric_adjust_minkowski;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_minkowski;} else {return &kt_sparse_computeForMetric_minkowski;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__zeroOne;} else {return &kt_sparse_computeForMetric_adjust_minkowski__subCase__zeroOne;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_minkowski__subCase__zeroOne;} else {return &kt_sparse_computeForMetric_minkowski__subCase__zeroOne;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__pow3;} else {return &kt_sparse_computeForMetric_adjust_minkowski__subCase__pow3;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_minkowski__subCase__pow3;} else {return &kt_sparse_computeForMetric_minkowski__subCase__pow3;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__pow4;} else {return &kt_sparse_computeForMetric_adjust_minkowski__subCase__pow4;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_minkowski__subCase__pow4;} else {return &kt_sparse_computeForMetric_minkowski__subCase__pow4;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_minkowski__subCase__pow5;} else {return &kt_sparse_computeForMetric_adjust_minkowski__subCase__pow5;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_minkowski__subCase__pow5;} else {return &kt_sparse_computeForMetric_minkowski__subCase__pow5;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_chebychev) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_chebychev;} else {return &kt_sparse_computeForMetric_adjust_chebychev;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_chebychev;} else {return &kt_sparse_computeForMetric_chebychev;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_chebychev_minInsteadOf_max;} else {return &kt_sparse_computeForMetric_adjust_chebychev_minInsteadOf_max;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_chebychev_minInsteadOf_max;} else {return &kt_sparse_computeForMetric_chebychev_minInsteadOf_max;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Sorensen;} else {return &kt_sparse_computeForMetric_adjust_Sorensen;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Sorensen;} else {return &kt_sparse_computeForMetric_Sorensen;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Gower) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Gower;} else {return &kt_sparse_computeForMetric_adjust_Gower;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Gower;} else {return &kt_sparse_computeForMetric_Gower;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Soergel) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Soergel;} else {return &kt_sparse_computeForMetric_adjust_Soergel;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Soergel;} else {return &kt_sparse_computeForMetric_Soergel;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Kulczynski_absDiff;} else {return &kt_sparse_computeForMetric_adjust_Kulczynski_absDiff;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Kulczynski_absDiff;} else {return &kt_sparse_computeForMetric_Kulczynski_absDiff;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Canberra) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Canberra;} else {return &kt_sparse_computeForMetric_adjust_Canberra;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Canberra;} else {return &kt_sparse_computeForMetric_Canberra;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Lorentzian;} else {return &kt_sparse_computeForMetric_adjust_Lorentzian;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Lorentzian;} else {return &kt_sparse_computeForMetric_Lorentzian;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPower;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_userDefinedPower;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPower;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_userDefinedPower;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow2;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow2;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow2;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow2;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow3;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow3;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow3;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow3;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow4;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow4;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow4;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow4;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_oekseth_forMasks_mean_standardDeviation_pow5;} else {return &kt_sparse_computeForMetric_adjust_oekseth_forMasks_mean_standardDeviation_pow5;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_oekseth_forMasks_mean_standardDeviation_pow5;} else {return &kt_sparse_computeForMetric_oekseth_forMasks_mean_standardDeviation_pow5;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_intersection) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_intersection;} else {return &kt_sparse_computeForMetric_adjust_intersection;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_intersection;} else {return &kt_sparse_computeForMetric_intersection;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_intersection_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_intersectionInverted;} else {return &kt_sparse_computeForMetric_adjust_intersectionInverted;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_intersectionInverted;} else {return &kt_sparse_computeForMetric_intersectionInverted;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_WaveHedges) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_WaveHedges;} else {return &kt_sparse_computeForMetric_adjust_WaveHedges;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_WaveHedges;} else {return &kt_sparse_computeForMetric_WaveHedges;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_WaveHedges_alt;} else {return &kt_sparse_computeForMetric_adjust_WaveHedges_alt;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_WaveHedges_alt;} else {return &kt_sparse_computeForMetric_WaveHedges_alt;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Czekanowski) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Czekanowski;} else {return &kt_sparse_computeForMetric_adjust_Czekanowski;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Czekanowski;} else {return &kt_sparse_computeForMetric_Czekanowski;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Czekanowski_altDef;} else {return &kt_sparse_computeForMetric_adjust_Czekanowski_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Czekanowski_altDef;} else {return &kt_sparse_computeForMetric_Czekanowski_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Motyka) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Motyka;} else {return &kt_sparse_computeForMetric_adjust_Motyka;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Motyka;} else {return &kt_sparse_computeForMetric_Motyka;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Motyka_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Motyka_altDef;} else {return &kt_sparse_computeForMetric_adjust_Motyka_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Motyka_altDef;} else {return &kt_sparse_computeForMetric_Motyka_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Kulczynski) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Kulczynski;} else {return &kt_sparse_computeForMetric_adjust_Kulczynski;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Kulczynski;} else {return &kt_sparse_computeForMetric_Kulczynski;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Ruzicka) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Ruzicka;} else {return &kt_sparse_computeForMetric_adjust_Ruzicka;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Ruzicka;} else {return &kt_sparse_computeForMetric_Ruzicka;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Tanimoto) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Tanimoto;} else {return &kt_sparse_computeForMetric_adjust_Tanimoto;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Tanimoto;} else {return &kt_sparse_computeForMetric_Tanimoto;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Tanimoto_altDef;} else {return &kt_sparse_computeForMetric_adjust_Tanimoto_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Tanimoto_altDef;} else {return &kt_sparse_computeForMetric_Tanimoto_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_innerProduct) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_innerProduct;} else {return &kt_sparse_computeForMetric_adjust_innerProduct;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_innerProduct;} else {return &kt_sparse_computeForMetric_innerProduct;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_harmonicMean) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_harmonicMean;} else {return &kt_sparse_computeForMetric_adjust_harmonicMean;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_harmonicMean;} else {return &kt_sparse_computeForMetric_harmonicMean;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_cosine) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_cosine;} else {return &kt_sparse_computeForMetric_adjust_cosine;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_cosine;} else {return &kt_sparse_computeForMetric_cosine;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_HumarHasseBrook_PCA_or_Jaccard;} else {return &kt_sparse_computeForMetric_adjust_HumarHasseBrook_PCA_or_Jaccard;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_HumarHasseBrook_PCA_or_Jaccard;} else {return &kt_sparse_computeForMetric_HumarHasseBrook_PCA_or_Jaccard;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Dice) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Dice;} else {return &kt_sparse_computeForMetric_adjust_Dice;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Dice;} else {return &kt_sparse_computeForMetric_Dice;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Jaccard_altDef;} else {return &kt_sparse_computeForMetric_adjust_Jaccard_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Jaccard_altDef;} else {return &kt_sparse_computeForMetric_Jaccard_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Dice_altDef;} else {return &kt_sparse_computeForMetric_adjust_Dice_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Dice_altDef;} else {return &kt_sparse_computeForMetric_Dice_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_sampleCoVariance;} else {return &kt_sparse_computeForMetric_adjust_sampleCoVariance;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_sampleCoVariance;} else {return &kt_sparse_computeForMetric_sampleCoVariance;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Brownian;} else {return &kt_sparse_computeForMetric_adjust_Brownian;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Brownian;} else {return &kt_sparse_computeForMetric_Brownian;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Brownian_altDef;} else {return &kt_sparse_computeForMetric_adjust_Brownian_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Brownian_altDef;} else {return &kt_sparse_computeForMetric_Brownian_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_fidelity) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_fidelity;} else {return &kt_sparse_computeForMetric_adjust_fidelity;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_fidelity;} else {return &kt_sparse_computeForMetric_fidelity;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Bhattacharyya;} else {return &kt_sparse_computeForMetric_adjust_Bhattacharyya;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Bhattacharyya;} else {return &kt_sparse_computeForMetric_Bhattacharyya;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Hellinger) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Hellinger;} else {return &kt_sparse_computeForMetric_adjust_Hellinger;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Hellinger;} else {return &kt_sparse_computeForMetric_Hellinger;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Matusita) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Matusita;} else {return &kt_sparse_computeForMetric_adjust_Matusita;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Matusita;} else {return &kt_sparse_computeForMetric_Matusita;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Hellinger_altDef;} else {return &kt_sparse_computeForMetric_adjust_Hellinger_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Hellinger_altDef;} else {return &kt_sparse_computeForMetric_Hellinger_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Matusita_altDef;} else {return &kt_sparse_computeForMetric_adjust_Matusita_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Matusita_altDef;} else {return &kt_sparse_computeForMetric_Matusita_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_squaredChord) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_squaredChord;} else {return &kt_sparse_computeForMetric_adjust_squaredChord;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_squaredChord;} else {return &kt_sparse_computeForMetric_squaredChord;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_squaredChord_altDef;} else {return &kt_sparse_computeForMetric_adjust_squaredChord_altDef;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_squaredChord_altDef;} else {return &kt_sparse_computeForMetric_squaredChord_altDef;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Euclid) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Euclid_squared;} else {return &kt_sparse_computeForMetric_adjust_Euclid_squared;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Euclid_squared;} else {return &kt_sparse_computeForMetric_Euclid_squared;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Pearson_squared;} else {return &kt_sparse_computeForMetric_adjust_Pearson_squared;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Pearson_squared;} else {return &kt_sparse_computeForMetric_Pearson_squared;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Neyman) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Neyman;} else {return &kt_sparse_computeForMetric_adjust_Neyman;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Neyman;} else {return &kt_sparse_computeForMetric_Neyman;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_squaredChi) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_squaredChi;} else {return &kt_sparse_computeForMetric_adjust_squaredChi;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_squaredChi;} else {return &kt_sparse_computeForMetric_squaredChi;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_probabilisticChi) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_probabilisticChi;} else {return &kt_sparse_computeForMetric_adjust_probabilisticChi;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_probabilisticChi;} else {return &kt_sparse_computeForMetric_probabilisticChi;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_divergence) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_squared_divergence;} else {return &kt_sparse_computeForMetric_adjust_squared_divergence;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_squared_divergence;} else {return &kt_sparse_computeForMetric_squared_divergence;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Clark) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Clark;} else {return &kt_sparse_computeForMetric_adjust_Clark;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Clark;} else {return &kt_sparse_computeForMetric_Clark;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_addativeSymmetricSquaredChi;} else {return &kt_sparse_computeForMetric_adjust_addativeSymmetricSquaredChi;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_addativeSymmetricSquaredChi;} else {return &kt_sparse_computeForMetric_addativeSymmetricSquaredChi;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_generic;} else {return &kt_sparse_computeForMetric_adjust_Pearson_productMoment_generic;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Pearson_productMoment_generic;} else {return &kt_sparse_computeForMetric_Pearson_productMoment_generic;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_absolute;} else {return &kt_sparse_computeForMetric_adjust_Pearson_productMoment_absolute;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Pearson_productMoment_absolute;} else {return &kt_sparse_computeForMetric_Pearson_productMoment_absolute;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_uncentered;} else {return &kt_sparse_computeForMetric_adjust_Pearson_productMoment_uncentered;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Pearson_productMoment_uncentered;} else {return &kt_sparse_computeForMetric_Pearson_productMoment_uncentered;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Pearson_productMoment_uncenteredAbsolute;} else {return &kt_sparse_computeForMetric_adjust_Pearson_productMoment_uncenteredAbsolute;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Pearson_productMoment_uncenteredAbsolute;} else {return &kt_sparse_computeForMetric_Pearson_productMoment_uncenteredAbsolute;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_shannon_KullbackLeibler) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Kullback_Leibler;} else {return &kt_sparse_computeForMetric_adjust_Kullback_Leibler;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Kullback_Leibler;} else {return &kt_sparse_computeForMetric_Kullback_Leibler;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_shannon_Jeffreys) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Jeffreys;} else {return &kt_sparse_computeForMetric_adjust_Jeffreys;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Jeffreys;} else {return &kt_sparse_computeForMetric_Jeffreys;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_shannon_kDivergence) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_kDivergence;} else {return &kt_sparse_computeForMetric_adjust_kDivergence;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_kDivergence;} else {return &kt_sparse_computeForMetric_kDivergence;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_shannon_Topsoee) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Topsoee;} else {return &kt_sparse_computeForMetric_adjust_Topsoee;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Topsoee;} else {return &kt_sparse_computeForMetric_Topsoee;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_shannon_JensenShannon) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_JensenShannon;} else {return &kt_sparse_computeForMetric_adjust_JensenShannon;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_JensenShannon;} else {return &kt_sparse_computeForMetric_JensenShannon;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_shannon_JensenDifference) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_JensenDifference;} else {return &kt_sparse_computeForMetric_adjust_JensenDifference;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_JensenDifference;} else {return &kt_sparse_computeForMetric_JensenDifference;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_combinations_Taneja) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_Taneja;} else {return &kt_sparse_computeForMetric_adjust_Taneja;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_Taneja;} else {return &kt_sparse_computeForMetric_Taneja;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_combinations_KumarJohnson) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_KumarJohnson;} else {return &kt_sparse_computeForMetric_adjust_KumarJohnson;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_KumarJohnson;} else {return &kt_sparse_computeForMetric_KumarJohnson;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_averageOfChebyshevAndCityblock;} else {return &kt_sparse_computeForMetric_adjust_averageOfChebyshevAndCityblock;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_averageOfChebyshevAndCityblock;} else {return &kt_sparse_computeForMetric_averageOfChebyshevAndCityblock;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_VicisWaveHedges;} else {return &kt_sparse_computeForMetric_adjust_VicisWaveHedges;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_VicisWaveHedges;} else {return &kt_sparse_computeForMetric_VicisWaveHedges;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_VicisWaveHedgesMax;} else {return &kt_sparse_computeForMetric_adjust_VicisWaveHedgesMax;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_VicisWaveHedgesMax;} else {return &kt_sparse_computeForMetric_VicisWaveHedgesMax;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_VicisWaveHedgesMin;} else {return &kt_sparse_computeForMetric_adjust_VicisWaveHedgesMin;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_VicisWaveHedgesMin;} else {return &kt_sparse_computeForMetric_VicisWaveHedgesMin;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_VicisSymmetric;} else {return &kt_sparse_computeForMetric_adjust_VicisSymmetric;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_VicisSymmetric;} else {return &kt_sparse_computeForMetric_VicisSymmetric;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_VicisSymmetricMax;} else {return &kt_sparse_computeForMetric_adjust_VicisSymmetricMax;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_VicisSymmetricMax;} else {return &kt_sparse_computeForMetric_VicisSymmetricMax;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_downwardMutability_symmetricMax;} else {return &kt_sparse_computeForMetric_adjust_downwardMutability_symmetricMax;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_downwardMutability_symmetricMax;} else {return &kt_sparse_computeForMetric_downwardMutability_symmetricMax;}
    }
  }

  if(typeOf_metric == e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin) {
    if(isTo_adjustBy_cntElements) {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_adjust_weight_downwardMutability_symmetricMin;} else {return &kt_sparse_computeForMetric_adjust_downwardMutability_symmetricMin;}
    } else {
      if(isTo_useWeight) {return &kt_sparse_computeForMetric_weight_downwardMutability_symmetricMin;} else {return &kt_sparse_computeForMetric_downwardMutability_symmetricMin;}
    }
  }
