
__Mi__localType *newPtr = NULL, *lastPtr = NULL;
//int pos, i, n,splitPos;
__Mi__localType__wrapper newKey, lastKey;
//s_db_baseNodeId_typeOfKey_int_t newKey, lastKey;
//int check = 1;
  
if (ptr == NULL) {
  *newnode = NULL;
  *upKey = key;
  assert(upKey);
  return e_db_cmpResult_InsertIt; 
 }
const int n = ptr->n;
int pos = __MiF__search__direct(key, ptr->key, n); //! ie, find the 'closest tree-position' for "key" in the tree-strucutre.
// FIXME: update [”elow] using a macro-cmp-strategy.
if( (pos < n) && __MiF__cmp__equal(key, ptr->key[pos])) { //! then key is found.
  //(key == ptr->key[pos]) ) { //! then key is found.
  return e_db_cmpResult_Duplicate;
 }
//! A new recursive inveeistgation using the 'closest' child-node:
e_db_cmpResult_t value = __MiF__search__recursive(ptr->pointers[pos], key, &newKey, &newPtr, result_set);
if(value != e_db_cmpResult_InsertIt) {
  return value; //! then we assume the 'key' is found in the strucutre.
 }
const int last_indexEach = __M__constant - 1;
//! 
if(n < last_indexEach) { //! then the tree 'is not full', ie, the tree does not have a 'work' (where "M" is the 'order of the B-tree').
  pos = __MiF__search__direct(newKey, ptr->key, n); //! search for the vertex using the [ªbove] 'idnetified closest child':
  //! Ensure sorted property: move/shift the (keys, pointers) to the right of the 'to-be-inserted' key.
  for (int i=n; i>pos; i--) {
    ptr->key[i] = ptr->key[i-1];
    ptr->pointers[i+1] = ptr->pointers[i];
  }
  /*Key is inserted at exact location*/
  ptr->key[pos] = newKey;
  ptr->pointers[pos+1] = newPtr;
  ptr->n++; /*incrementing the number of keys in node*/
  return e_db_cmpResult_Success;
 } else if(pos == last_indexEach) { /*If keys in nodes are maximum and position of node to be inserted is last*/
  lastKey = newKey;
  lastPtr = newPtr;
 } else { //! then we 'need to make room' for the 'vertex to add', ie, by 'moving the children 'above the surrent start' forward:
  lastKey = ptr->key[__M__constant - 2];
  lastPtr = ptr->pointers[__M__constant -1 ];
  for(int i = __M__constant - 2; i>pos; i--) {
    assert(i >= 0);
    ptr->key[i]  = ptr->key[i-1];
    ptr->pointers[i+1]  = ptr->pointers[i];
  }
  ptr->key[pos] = newKey;
  ptr->pointers[pos+1]  = newPtr;
 }
const int splitPos = last_indexEach/2;
(*upKey) = ptr->key[splitPos];
(*newnode) = (__Mi__localType*)malloc(sizeof(__Mi__localType));/*Right node after split*/
ptr->n = splitPos; /*No. of keys for left splitted node*/
(*newnode)->n = last_indexEach - splitPos;/*No. of keys for right splitted node*/
for (int i=0; i < (*newnode)->n; i++) {
  (*newnode)->pointers[i] = ptr->pointers[i + splitPos + 1];
  if(i < ((*newnode)->n - 1)) {
    (*newnode)->key[i] = ptr->key[i + splitPos + 1];
  } else {
    (*newnode)->key[i] = lastKey;
  }
 }
(*newnode)->pointers[(*newnode)->n] = lastPtr;
return e_db_cmpResult_InsertIt;

