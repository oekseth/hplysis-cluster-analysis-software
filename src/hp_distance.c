#include "hp_distance.h"

//! @return an tiantied verison of the "s_hp_distance__config_t" object.
s_hp_distance__config_t init__s_hp_distance__config_t() {
  s_hp_distance__config_t self;
  //! --------------------------------------------------------------------
  //!
  //! For the 'each' and 'one-to-many-case' specify the indcesses to use:
  self.obj_1_index1 = UINT_MAX; //! which if the 'index' to choose in obj_1
  self.obj_2_index2 = UINT_MAX; //! which if the 'index' to choose in obj_1
  self.typeOf__postProcessing = e_hp_distance__subLogics_undef;
  //! --------------------------
  //! @return
  return self;
}

//! @return an itnated verison of the "s_hp_distance_t" object.
s_hp_distance_t init__s_hp_distance(const s_kt_correlationMetric_t obj_metric, 
				    const bool needTo_useMask_evaluation__input,
				    const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2,
				    s_hp_distance__config_t local_config
				    ) {
#define __Mi__isToReturnFalseOnError 0
  s_hp_distance__config_t *static_config = &local_config;
#define __Mi__callerFunction __FUNCTION__
#include "hp_distance__stub__get__dataSeperately.c" //! ie, get the dat-anames expected by our "kt_distance.h"
  s_hp_distance_t self;
  self.static_config = local_config; // init__s_hp_distance__config_t();
  self.obj_metric = obj_metric;
  self.obj_1 = obj_1;
  if(obj_2 != NULL) {
    self.obj_2 = obj_2;
  } else {
    self.obj_2 = obj_1;
  }
  self.needTo_useMask_evaluation = needTo_useMask_evaluation__input; //! a aprameter which may be set 'locally' usinig: const bool needTo_useMask_evaluation__input = matrixMakesUSeOf_mask__noSSE(nrows, ncolumns, data, data, mask, mask, transpose, /*iterationIndex_2=*/UINT_MAX);
  self.metric_each = NULL;
  self.metric_oneToMany = NULL;
  //self.metric_oneToMany_nonRank = NULL;
  //!
  //! Build a distance-matrix:
  //const bool needTo_useMask_evaluation__input = matrixMakesUSeOf_mask__noSSE(nrows, ncolumns, data, data, mask, mask, transpose, /*iterationIndex_2=*/UINT_MAX);
  //const bool needTo_useMask_evaluation = matrixMakesUSeOf_mask__noSSE(nrows, ncolumns, data, data, mask, mask, transpose, /*iterationIndex_2=*/UINT_MAX);
  //fprintf(stderr, "needTo_useMask_evaluation='%u', at %s:%d\n", needTo_useMask_evaluation, __FILE__, __LINE__);


  /* Set the metric function as indicated by dist */
  //t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);
  //t_float (*metric_each) (config_nonRank_each_t) 

  //!
  //! Idenitfy the 'class' of logic to apply:
  bool use__oneToMany = false;   bool use__oneToMOne = false;
  if( (self.obj_1->nrows == 1) || (self.static_config.obj_1_index1 != UINT_MAX) ) {
    if( (self.obj_2->nrows == 1) || (self.static_config.obj_2_index2 != UINT_MAX) )  {
    //if(self.obj_2->nrows == 1) {
      //printf("--- mat2.size=%u, mat2.index=%u, at %s:%d\n", self.obj_2->nrows, self.static_config.obj_2_index2, __FILE__, __LINE__);
      use__oneToMOne = true;}
    else {use__oneToMany = true;
      assert(subsetIndex_isSetFor_1 == true);    
    }
  } else {
    if( (self.obj_2->nrows == 1) || (self.static_config.obj_2_index2 != UINT_MAX) )  {
      if(self.obj_1->nrows == 1) {
	//printf("--- at %s:%d\n", __FILE__, __LINE__);
	use__oneToMOne = true;}
      /* else {use__oneToMany = true;  */
      /* 	assert(subsetIndex_isSetFor_1 == true); */
      /* } */
    } 
  }
  //t_float *weight = NULL; char **mask = NULL; 
  if(use__oneToMOne) {
    assert(subsetIndex_isSetFor_1 == true);
    assert(obj_metric.metric_id != e_kt_correlationFunction_undef);
    self.metric_each = setmetric__correlationComparison__each(obj_metric.metric_id, weight, mask, mask, /*needTo_useMask_evaluation=*/self.needTo_useMask_evaluation); 
  } else if(use__oneToMany) {
    assert(obj_metric.metric_id != e_kt_correlationFunction_undef);
    self.metric_oneToMany = setmetric__correlationComparison__oneToMany(obj_metric.metric_id, weight, mask, mask, /*needTo_useMask_evaluation=*/self.needTo_useMask_evaluation); 
  }
  //! --------------------------------------------------------------------
  //!  
  setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(&(self.obj_kendallCompute));
    assert(self.obj_metric.metric_id != e_kt_correlationFunction_undef);
  self.isTo_useOptimizedMetricFor_kendall = isTo_use_kendall__e_kt_correlationFunction(self.obj_metric.metric_id);
  if(self.isTo_useOptimizedMetricFor_kendall) { //(dist == 'k') ) {
    //assert(transpose == 0); //! ie, to simplify our code.
    
    //! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure:
    //! Note In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
    init__s_correlationType_kendall_partialPreCompute_kendall(&(self.obj_kendallCompute), nrows_1, ncols, data_1, mask, self.needTo_useMask_evaluation);
  }
  //! --------------------------------------------------------------------
  //!
  //! @return
  return self;
}


//! De-allcoates the s_hp_distance_t object (oekseth, 06. feb. 2017)
void free__s_hp_distance(s_hp_distance_t *self) {
  // printf("de-allcoates  object, at %s:%d\n", __FILE__, __LINE__);
  if(self->isTo_useOptimizedMetricFor_kendall) { //(dist == 'k') ) {
  //if(self->isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(self->obj_metric.metric_id)) { //(dist == 'k') ) {
    //if(isTo_copmuteResults_optimal && (dist == 'k') ) {
  // printf("de-allcoates the kendall object, at %s:%d\n", __FILE__, __LINE__);
    free__s_correlationType_kendall_partialPreCompute_kendall(&(self->obj_kendallCompute));
  } //! else we assuemt he "s_correlationType_kendall_partialPreCompute_kendall" data-structure is empty.
}
 

//! provide a convenieicne-fucntion to kcomptue simalrity between entites (oekseth, 06. feb. 2017):
bool apply__extensive__hp_distance(s_hp_distance_t *self, const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2, s_kt_matrix_base_t *obj_result) {
  bool is_ok = true;
#define __Mi__isToReturnFalseOnError 1
  s_hp_distance__config_t *static_config = &(self->static_config);
#define __Mi__callerFunction __FUNCTION__
#include "hp_distance__stub__get__dataSeperately.c" //! ie, get the dat-anames expected by our "kt_distance.h"
  assert(ncols > 0);
  assert(nrows_1 >= 1);
  if(nrows_2 != UINT_MAX) {assert(nrows_2 >= 1);}

  const e_cmp_masksAre_used_t masksAre_used = (self->needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  // printf("masksAre_used=%u, at %s:%d\n", masksAre_used, __FILE__, __LINE__);
  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, mask, weight, /*transpose=*/false, masksAre_used, obj_1->nrows, obj_1->ncols);
  if(self->metric_each != NULL) { 
    assert(subsetIndex_isSetFor_1);     assert(subsetIndex_isSetFor_2); //! ie, to know 'from where' we are to 'select the subsets'.
    if( subsetIndex_isSetFor_1 && (self->static_config.obj_1_index1 >= nrows_1) ) {
      fprintf(stderr, "!!\t An incosnsntency in your input: for the 'each' comptaution we expected nrows_1=nrows_2 > %u, an assumption which does Not hold, ie, '%u' and '%u':  please udpate your API. OBserviaotn at [%s]:%s:%d\n", self->static_config.obj_1_index1, nrows_1, nrows_2, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;	
    } else if( (nrows_2  != UINT_MAX) && subsetIndex_isSetFor_2 && ( (self->static_config.obj_2_index2 >= nrows_2) ) ) {
      fprintf(stderr, "!!\t An incosnsntency in your input: for the 'each' comptaution we expected nrows_1=nrows_2 > %u, an assumption which does Not hold, ie, '%u' and '%u':  please udpate your API. OBserviaotn at [%s]:%s:%d\n", self->static_config.obj_2_index2, nrows_1, nrows_2, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;	
    } else if( (nrows_2 == UINT_MAX)  && subsetIndex_isSetFor_2 && (self->static_config.obj_2_index2 >= nrows_1) ) {
      fprintf(stderr, "!!\t An incosnsntency in your input: for the 'each' comptaution we expected nrows_1=nrows_2 > %u, an assumption which does Not hold, ie, '%u' and '%u':  please udpate your API. OBserviaotn at [%s]:%s:%d\n", self->static_config.obj_2_index2, nrows_1, nrows_2, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;	

    }
    assert(self->obj_metric.metric_id != e_kt_correlationFunction_undef);
    const t_float distance = kt_compare__each(self->obj_metric.metric_id, self->obj_metric.typeOf_correlationPreStep, 
					      nrows_1, ncols, 
					      data_1, data_2, /*index1=*/self->static_config.obj_1_index1, /*index2=*/self->static_config.obj_2_index2, config_metric, self->metric_each,
					      (self->isTo_useOptimizedMetricFor_kendall == false) ? NULL : &(self->obj_kendallCompute)
					      );    
    if( (obj_result->matrix == NULL) ) {
      //! Thenw e allcoat ehte object:
      *obj_result = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/1);
      assert(obj_result->matrix);
      obj_result->matrix[0][0] = distance; //! ie, the comptued score.
    }
  } else if(self->metric_oneToMany != NULL) {
    if(subsetIndex_isSetFor_1 == false) {
      fprintf(stderr, "!!\t An inconsiotent cofniguration for matrix1 w/dims(mat1)=[%u, %u]: for a one-to-many-computation we expect the 'first input-matirx provided as funciton-argument' to describe a matrix with (a) either one row, or alternativly to have explcitly set the 'obj_1_index1' object-parameter. In brief the latter does Not hold, ie, please ivnestigate. For quesitonis please contact senior developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", nrows_1, ncols, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;	
    }
    assert(subsetIndex_isSetFor_1);    //! ie, to know 'from where' we are to 'select the subsets'.
    //assert(subsetIndex_isSetFor_2); //! ie, to know 'from where' we are to 'select the subsets'.
    if( self->static_config.obj_1_index1 >= nrows_1) {
      fprintf(stderr, "!!\t An incosnsntency in your input: for the 'each' comptaution we expected nrows_1=nrows_2 > %u, an assumption which does Not hold, ie, '%u' and '%u':  please udpate your API. OBserviaotn at [%s]:%s:%d\n", self->static_config.obj_1_index1, nrows_1, nrows_2, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;	
    }
    t_float **local__resultMatrix = NULL;
    const uint nrows__local = 1; //! ie, as we 'only' comptue for one row of valeus, ie, "one-to-many".
    nrows_2 = (nrows_2 == UINT_MAX) ? nrows_1 : nrows_2;
    if( (obj_result->matrix == NULL) || (self->static_config.typeOf__postProcessing != e_hp_distance__subLogics_undef) ) {
      const t_float val_0 = 0;
      // printf("alloctes result-amtrix, at %s:%d\n", __FILE__, __LINE__);
      local__resultMatrix = allocate_2d_list_float(nrows__local, nrows_2, val_0);
      //arrOf_result_tmp = obj_result->matrix[0];
    } else {      
      if( (obj_result->nrows != nrows__local) || (obj_result->ncols != nrows_2) ) {
	fprintf(stderr, "!!\t An incosnsntency in your input: result-object is Not set iaw, the spec: please udpate your API. OBserviaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up.
	return false;	
      } 
      local__resultMatrix = obj_result->matrix; //! ie, the matrix to use.
    }
    for(uint j = 0; j < nrows_2; j++) {local__resultMatrix[0][j] = T_FLOAT_MAX;} //! ie, intalize.
    

    //!
    //! Apply logics:
    kt_compare__oneToMany(self->obj_metric.metric_id, self->obj_metric.typeOf_correlationPreStep,
    			  //nygrid, //
    			  nrows_2,
    			  ncols, data_1, data_2, /*index1=*/self->static_config.obj_1_index1,  &config_metric,
    			  (self->isTo_useOptimizedMetricFor_kendall == false) ? NULL : &(self->obj_kendallCompute),
    			  //(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute,
    			  /*arrOf_result=*/local__resultMatrix[0],
    			  self->metric_oneToMany);


    //! ---------------------------------------------------------------------
    //! 
    //! Apply post-prcoesisng, ie, if reuqested:
    t_float *clusters = NULL;
    if(self->static_config.typeOf__postProcessing != e_hp_distance__subLogics_undef) {
      assert(obj_result);
      if(obj_result->matrix != NULL) {
	assert(obj_result->matrix);
	assert(obj_result->matrix[0]);
	assert(obj_result->nrows > 0);
	assert(obj_result->nrows == 1);
	assert(obj_result->ncols == 1); //! ie, as we assume 'the result-row is to be condensed into one scalar'.
	clusters = obj_result->matrix[0];
      }
    }
    if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_min) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      const uint i = 0;  {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_min__index) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      const uint i = 0;  {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	uint min_index = 0; t_float min_score = T_FLOAT_MAX;
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    if(row_local[j] < min_score) {
	      min_score = row_local[j];
	      min_index = j;
	      clusters[i] = j;
	    }
	    //clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_max) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      const uint i = 0;  {
	//for(uint i = 0; i < nrows_1; i++)   	{
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_max__index) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      const uint i = 0;  {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	uint min_index = 0; t_float min_score = T_FLOAT_MIN_ABS;
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    if(row_local[j] > min_score) {
	      min_score = row_local[j];
	      min_index = j;
	      clusters[i] = j;
	    }
	    //clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_undef) {
      if(obj_result->matrix == NULL) {
	*obj_result = initAndReturn__notAllocate__s_kt_matrix_base_t(nrows__local, nrows_2, local__resultMatrix);
	obj_result->matrix__isAllocated = true; //! ie, as we 'delegate't he resposnbile of this poitner ot the return-object.
      } else {assert(obj_result->matrix == local__resultMatrix);}
      //obj_result->matrix = local__resultMatrix; //! ie, the matrix 'we have allocated'.
    } else {
      fprintf(stderr, "!!\t An incosnsntency in your input: have not yet added support for post-rpcoes-stype=%u: please udpate your API. OBserviaotn at [%s]:%s:%d\n", self->static_config.typeOf__postProcessing, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;
    }
    //! 
    //! Dea--lcoate:
    if(local__resultMatrix != obj_result->matrix) {
      assert(local__resultMatrix);
      free_2d_list_float(&local__resultMatrix, nrows__local); local__resultMatrix = NULL;
    }
  } else {
    t_float **local__resultMatrix = obj_result->matrix;
    if( (obj_result->matrix == NULL) || (self->static_config.typeOf__postProcessing != e_hp_distance__subLogics_undef) ) {
      const t_float val_0 = 0;
      assert(nrows_2 != UINT_MAX);
      //fprintf(stderr, "allocate a matrxi=[%u, %u], at %s:%d\n", nrows_1, nr
      local__resultMatrix = allocate_2d_list_float(nrows_1, nrows_2, val_0);
      //printf("alloctes result-amtrix=%p, at %s:%d\n", local__resultMatrix, __FILE__, __LINE__);
      /* if(obj_result->matrix == NULL) { */
      /* 	obj_result->matrix = local__resultMatrix; */
      /* 	obj_result->nrows = nrows_1; */
      /* 	obj_result->ncols = nrows_2; */
      /* } */
    } else {
      if(nrows_2 == UINT_MAX) {nrows_2 = nrows_1;} //! ie, to simplfiy [below]
      if( (obj_result->nrows != nrows_1) || (obj_result->ncols != nrows_2) ) {
	fprintf(stderr, "!!\t You have requested to get the result-matrix (and not a post-procssed result of the resut-simliarty-matrix). Given teh latter we observe an Error in your configuraiton of the 'all-against-all-computatioin' function-param \"obj_result\": we observe an incosnsntency in your input, where the result-object is Not set iaw, the spec: expected the result-matrix to be in dimensions=[%u, %u] though the obj_result was acutally in dimensions=[%u, %u]; based on the latter please udpate your API. OBserviaotn at [%s]:%s:%d\n", nrows_1, nrows_2, obj_result->nrows, obj_result->ncols, __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up.
	return false;	
      } 
    }
    //! Intaite the result-object:
    for(uint i = 0; i < nrows_1; i++) { for(uint j = 0; j < nrows_2; j++) {local__resultMatrix[i][j] = T_FLOAT_MAX;}} //! ie, intalize.
    //!
    //! Apply logics:
    t_float **verfiy_nonLeake = local__resultMatrix;
    //printf("include [below], at %s:%d\n", __FILE__, __LINE__);
    assert(self->obj_metric.metric_id != e_kt_correlationFunction_undef);
    kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(self->obj_metric.metric_id, self->obj_metric.typeOf_correlationPreStep, nrows_1, ncols, data_1, data_2, /*weight=*/NULL, /*resultMatrix=*/local__resultMatrix, /*mask1=*/NULL, /*mask2=*/NULL, /*transpose=*/false, /*isTo_invertMatrix_transposed=*/true, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/nrows_2, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*(void*)&obj_spec=*/NULL, e_cmp_masksAre_used_undef, NULL,
								  // FIXME: add for [”elow].
								  NULL
								  );	
    assert(local__resultMatrix == verfiy_nonLeake); //! ie, to simplify a varieficiton wrt. memory-leaks.
    //! ---------------------------------------------------------------------
    //! 
    //! Apply post-prcoesisng, ie, if reuqested:
    t_float *clusters = NULL;
    if(self->static_config.typeOf__postProcessing != e_hp_distance__subLogics_undef) {
      assert(obj_result);
      if( (obj_result->matrix == NULL) ) {
	//! Thenw e allcoat ehte object:
	*obj_result = initAndReturn__s_kt_matrix_base_t(/*nrows=*/1, /*ncols=*/nrows_1);
	assert(obj_result->matrix);
      }
      assert(obj_result->matrix);
      assert(obj_result->matrix[0]);
      assert(obj_result->nrows == 1);
      assert(obj_result->ncols == nrows_1);
      clusters = obj_result->matrix[0];
    }
    if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_min) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      for(uint i = 0; i < nrows_1; i++)    {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_min__index) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      for(uint i = 0; i < nrows_1; i++)    {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	uint min_index = 0; t_float min_score = T_FLOAT_MAX;
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    if(row_local[j] < min_score) {
	      min_score = row_local[j];
	      min_index = j;
	      clusters[i] = j;
	    }
	    //clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_max) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      for(uint i = 0; i < nrows_1; i++)    {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_eachVector_select_max__index) {
      //!
      //! Update our "clusters" list wrt. [ªbofe]:
      for(uint i = 0; i < nrows_1; i++)    {
	const t_float *__restrict__ row_local = local__resultMatrix[i];
	assert(row_local);
	uint min_index = 0; t_float min_score = T_FLOAT_MIN_ABS;
	for(uint j = 0; j < nrows_2; j++)	{
	  if(isOf_interest(row_local[j])) {
	    if(row_local[j] > min_score) {
	      min_score = row_local[j];
	      min_index = j;
	      clusters[i] = j;
	    }
	    //clusters[i] = macro_min(clusters[i], row_local[j]);
	  }
	}
      }
    } else if(self->static_config.typeOf__postProcessing == e_hp_distance__subLogics_undef) {
      if(obj_result->matrix == NULL) {
	*obj_result = initAndReturn__empty__s_kt_matrix_base_t();
	obj_result->nrows = nrows_1;
	obj_result->ncols = nrows_2;
	obj_result->matrix = local__resultMatrix;
	obj_result->matrix__isAllocated = true; //! ie, as we 'delegate't he resposnbile of this poitner ot the return-object.
	//*obj_result = initAndReturn__notAllocate__s_kt_matrix_base_t(nrows_1, nrows_2, local__resultMatrix);
	/* printf("sets result-amtrix=%p, at %s:%d\n", local__resultMatrix, __FILE__, __LINE__); */
	/* printf("at %s:%d\n", __FILE__, __LINE__); */
	assert(obj_result->matrix == local__resultMatrix); //! ie, what we expect.
      } else {assert(obj_result->matrix == local__resultMatrix);}
      //obj_result->matrix = local__resultMatrix; //! ie, the matrix 'we have allocated'.
    } else {
      fprintf(stderr, "!!\t An incosnsntency in your input: have not yet added support for post-rpcoes-stype=%u: please udpate your API. OBserviaotn at [%s]:%s:%d\n", self->static_config.typeOf__postProcessing, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      return false;
    }
    //! 
    //! Dea--lcoate:
    if(local__resultMatrix != obj_result->matrix) {
      assert(local__resultMatrix);
      free_2d_list_float(&local__resultMatrix, nrows_1); local__resultMatrix = NULL;
    }
  }


  //!
  //! @returnt he success-stae:
  return is_ok;
}

//! provide a convenieicne-fucntion to kcomptue simalrity between entites (oekseth, 06. feb. 2017).
bool apply__hp_distance(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2, s_kt_matrix_base_t *obj_result, const s_hp_distance__config_t local_config) {
  assert(obj_1);
#define __Mi__isToReturnFalseOnError 1
  s_hp_distance__config_t static_config_local = local_config;
  s_hp_distance__config_t *static_config = &static_config_local;
  //printf("obj_1_index1=%u, obj_2_index2=%u, at %s:%d\n", static_config->obj_1_index1, static_config->obj_2_index2, __FILE__, __LINE__);
  if(obj_1->nrows == 1) {static_config_local.obj_1_index1 = 0;} //! ie, to 'handle' the case wrt. the 'first index'.
  if(obj_2 && (obj_2->nrows == 1)) {static_config_local.obj_2_index2 = 0;} //! ie, to 'handle' the case wrt. the 'first index'.
  else if(!obj_2 && (obj_1->nrows == 1) ) {static_config_local.obj_2_index2 = 0;} //! ie, to 'handle' the case wrt. the 'first index'.
  assert(obj_1->matrix);
#define __Mi__callerFunction __FUNCTION__
#include "hp_distance__stub__get__dataSeperately.c" //! ie, get the dat-anames expected by our "kt_distance.h"
  assert(data_1);
  const bool needTo_useMask_evaluation__input = matrixMakesUSeOf_mask__noSSE(nrows_1, ncols, data_1, data_2, mask, mask, /*transpose=*/false, /*iterationIndex_2=*/nrows_2);
  s_hp_distance_t self = init__s_hp_distance(obj_metric, needTo_useMask_evaluation__input, obj_1, obj_2, static_config_local);
  //self.static_config = static_config_local;

  //! ----------------
  if(self.metric_each != NULL) {  //! then we expect call wrt. the local idnex'.
    //printf("obj_1.nrows=%u, obj_2.nrows=%u, at %s:%d\n", nrows_1, nrows_2, __FILE__, __LINE__);
    assert(subsetIndex_isSetFor_1);     assert(subsetIndex_isSetFor_2); //! ie, to know 'from where' we are to 'select the subsets'.
  }
  const bool is_ok = apply__extensive__hp_distance(&self, obj_1, obj_2, obj_result);
  //!
  //! De-allcoate the local object:
  free__s_hp_distance(&self);
  //!
  //! @return the success-state:
  return is_ok;
}

bool apply__rows_hp_distance(const s_kt_correlationMetric_t obj_metric, t_float *row_1, t_float *row_2, const uint ncols, t_float *scalar_result) {
  assert(ncols > 0);
  assert(row_1);
  assert(row_2);
  //!
  //! The calll:
  assert(obj_metric.metric_id != e_kt_correlationFunction_undef);
  *scalar_result = kt_compare__each__maskImplicit__useRowPointersAsInput(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, 
									 ncols, row_1, row_2);
  //!
  //! @return
  return true; //! ie, as we asusme the operaiton went smooth.
}
bool apply__uint_rows_hp_distance(const s_kt_correlationMetric_t obj_metric, const uint *row_1_u, const uint *row_2_u, const uint ncols, t_float *scalar_result) {
  assert(ncols > 0);
  assert(row_1_u);
  assert(row_2_u);
  //!
  //! Convert:
  const t_float empty_0 = 0;
  t_float *row_1 = allocate_1d_list_float(ncols, empty_0);
  t_float *row_2 = allocate_1d_list_float(ncols, empty_0);
  //!
  //! Copy:
  for(uint i = 0; i < ncols; i++) {
    uint val = row_1_u[i]; assert(val < (uint)T_FLOAT_MAX);
    row_1[i] = val;
    val = row_2_u[i]; assert(val < (uint)T_FLOAT_MAX);
    row_2[i] = val;
  }

  //!
  //! The call:
  const bool is_ok = apply__rows_hp_distance(obj_metric, row_1, row_2, ncols, scalar_result);
  assert(is_ok);

  //!
  //! De-allcoate and return:
  free_1d_list_float(&row_1);
  free_1d_list_float(&row_2);
  return is_ok;
}

//! provide lgocis to unify a cluster-cneotrid-list and a set og 'flboal vertices' (oekseth, 06. feb. 2017)
//static void __assign(Dataset const &x, Dataset const &c, unsigned short *assignment) {
bool findExtremeIndex__inSecondMatrix__aux__hp_distance(const s_kt_correlationMetric_t obj_metric, const uint *mapOf__clusterVertexCentroids, s_kt_matrix_base_t *matrix_input, const uint k_clusterCount, uint *mapOf_columnToRandomScore, const bool isTo__findMin) {
//static bool __assign__vertices__toClusters(const uint *mapOf__clusterVertexCentroids, s_kt_matrix_base_t *matrix_input, const uint k_clusterCount, uint *mapOf_columnToRandomScore, const bool isTo__findMin) {
  assert(matrix_input);  
  assert(mapOf__clusterVertexCentroids);
  assert(mapOf_columnToRandomScore);
  assert(k_clusterCount < (uint)T_FLOAT_MAX); //! ie, as we expect to use "t_float" data-type as a tempeorary storage tom 'genrlaize' our appraoch.

  //! Build a sub-matrix based on the randomly selected values:
  s_kt_matrix_base_t mat_seeds = initAndReturn__fromSubset__s_kt_matrix_base_t(matrix_input, /*candidate_ids=*/mapOf__clusterVertexCentroids, /*candidate_ids_size=*/k_clusterCount); //! ie, the subset of feature-vectors.
  assert(mat_seeds.nrows > 1); assert(mat_seeds.ncols > 1);
  assert(mat_seeds.ncols == matrix_input->ncols);
  //! 
  //! apply an all-agaisnt-all-comparison: 
  s_hp_distance__config_t local_config = init__s_hp_distance__config_t();
  if(isTo__findMin) {
    local_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index; ///! ie, those the 'lowest index which is found'.
  } else {
    local_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_max__index; ///! ie, those the 'lowest index which is found'.
  }
  //s_kt_correlationMetric_t obj_metric = setTo_empty__andReturn__s_kt_correlationMetric_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1);
  /* local_config.obj_1_index1 = chosen_pts[0]; //! ie, as we comptue "[0] x [all]" for "matrix_input". */
  /* assert(local_config.obj_1_index1 < matrix_input->nrows); */
  s_kt_matrix_base_t distance_to_candidates = initAndReturn__empty__s_kt_matrix_base_t();
  const bool is_ok = apply__hp_distance(obj_metric, /*obj_1=*/matrix_input, /*obj_2=*/&mat_seeds, /*obj_result=*/&distance_to_candidates, local_config);
  assert(distance_to_candidates.nrows == 1);
  assert(distance_to_candidates.ncols == matrix_input->nrows); //! ie, as we expect to have comptued [seeds] x [input-matrix]
  assert(is_ok);
  //! -------------------------------------------------------
  //!
  //! Iterate through the vertices and select the best score:
  for(uint i = 0; i < distance_to_candidates.ncols; i++) {
    const uint cluster_id = (uint)distance_to_candidates.matrix[0][i];
    assert(cluster_id < k_clusterCount);
    mapOf_columnToRandomScore[i] = cluster_id;
  }
  //!
  //! De-allocat hte lcoally allcoated variables:
  free__s_kt_matrix_base_t(&mat_seeds);
  free__s_kt_matrix_base_t(&distance_to_candidates);
  //!
  //! @return
  return is_ok;
}

s_kt_list_2d_kvPair_t apply__2d__storeSparse__hp_distance(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores) {
  if(obj_2 == NULL) {obj_2 = obj_1;}
  if(obj_1->ncols != obj_2->ncols) {
    fprintf(stderr, "!!\t (columns-different-dims)\t The two input-objects has different column-size, ie, where latter result in mulitple interpreaitons (wrt. how to address the latter). In your inptu obj_1.ncols=%u while obj_2.ncols=%u. Pleaase set the column-size to the same number, ie, udpating your input. In brief we drop computing your simalrity-emtric-request. For deitlas please see documetnaiton. However if latter does Not help then do not hestitate cotnacting senior devleoper [oesketh@gmail.com]. Observation at %s:%d\n", obj_1->ncols , obj_2->ncols, __FILE__, __LINE__);
    assert(false);
    return setToEmpty__s_kt_list_2d_kvPair_t();
  }
  //! Itnate and allocate memory: 
  s_kt_list_2d_kvPair_filterConfig_t obj_result = init__s_kt_list_2d_kvPair_filterConfig_t(config_cntMin_afterEach, config_cntMax_afterEach, config_ballSize_max, isTo_ingore_zeroScores);
      //  const e_cmp_masksAre_used_t masksAre_used = (self->needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  // printf("masksAre_used=%u, at %s:%d\n", masksAre_used, __FILE__, __LINE__);
      //  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, mask, weight, /*transpose=*/false, masksAre_used, obj_1->nrows, obj_1->ncols);
  const uint nrows_1 = obj_1->nrows; const uint nrows_2 = obj_2->nrows;
  assert(nrows_1 > 0);
  assert(nrows_2 > 0);
  t_float **data_1 = obj_1->matrix;
  t_float **data_2 = obj_2->matrix;
  const uint ncols = obj_1->ncols;
  //! 
  //! The call: 
    //printf("include [below], at %s:%d\n", __FILE__, __LINE__);
  //printf("ncols=%u, nrows=[%u, %u], at %s:%d\n", ncols, nrows_1, nrows_2, __FILE__, __LINE__);
    assert(obj_metric.metric_id != e_kt_correlationFunction_undef);
  kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, nrows_1, ncols, data_1, data_2, /*weight=*/NULL, /*resultMatrix=local__resultMatrix=*/NULL, /*mask1=*/NULL, /*mask2=*/NULL, /*transpose=*/false, /*isTo_invertMatrix_transposed=*/true, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/nrows_2, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*(void*)&obj_spec=*/NULL, e_cmp_masksAre_used_undef, NULL,
								  // FIXME: add for [”elow].
								  &obj_result
								  );
  return obj_result.self; //! ie, where we assume taht the other 'parts' of "obj_result" does Not requrei de-allcoation.
}
