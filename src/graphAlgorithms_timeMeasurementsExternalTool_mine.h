#ifndef graphAlgorithms_timeMeasurementsExternalTools_mine_h
#define graphAlgorithms_timeMeasurementsExternalTools_mine_h

/**
   @file 
   @brief provide implementaitons of algorithms for computation of distance, rank and correlation
**/

#include "types.h"

/**
   @namespace graphAlgorithms_timeMeasurementsExternalTools_mine
   @brief evaluate/benchmark different sections of the "Mine" library through a combination of different parameters.
   @author Ole Kristian Ekseth (oekseth).
**/
namespace graphAlgorithms_timeMeasurementsExternalTools_mine {
  void main();
}


#endif
