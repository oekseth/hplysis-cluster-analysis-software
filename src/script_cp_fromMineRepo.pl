#!/usr/bin/perl

=head
    @brief provide lgoics for copying a subset of the files from the MINE software repo, a repo written by [oekseth@gmail.com] (oekseth, 06. nov. 2016).
    @remarks to undrestand the validaty of the 'direct' MINE-software-inslusion note that MINE author is the same person as the author of  he hpLysis clustering-software    
=cut


my @arrOf_files = (
    "mine_matrix.c", "mine_matrix__func__spec.h",
    "mine.c", "rapidMic_core.c", "fast_log.c",
    "rapidMic_core.h", "func_hp3.h",
    "configure_optimization.h",
    "fast_log_floatValues.h", "fast_log_approxFloatValues.h",
    );

my $pathTo_Repo = "../../mine-data-analysis/src/"; #! ie, the expected path to the repo
my $localDir = ""; #! ie, to simplify linking
#my $localDir = "mine/";
#! ------------
my $stringOf_result_linker = "#! MINE linkage-files:\n";
my $stringOf_result_convertToC = "#! MINE conversion-proceudre to simplify building of C++ software-chain:\n";
foreach my $file (@arrOf_files) {
    my $path = $pathTo_Repo . $file;
    my $new_path = $localDir . $file;
    system("cp $path $new_path");
    system("hg add $new_path");
    
    
    if($file =~/^(.+)\.c$/) {
	#! Update the linkage-text:
	$stringOf_result_linker .= " " . $new_path;
	$stringOf_result_convertToC .= 'set_source_files_properties("' . $new_path . '"  PROPERTIES LANGUAGE CXX )' . "\n";
	#! Copy the ehader-file:
	$file = $1 . ".h"; #! ie, then also to include the ehader-file.
	#! Copy:
	my $path = $pathTo_Repo . $file;
	my $new_path = $localDir . $file;
	system("cp $path $new_path");
	system("hg add $new_path");
    }
}

#! Write out the linkage-texts:
print $stringOf_result_linker . "\n";
print $stringOf_result_convertToC . "\n";
