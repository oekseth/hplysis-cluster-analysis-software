{
  const uint list_size = size_of_array;
    { const char *stringOf_measureText = "for-loop-branch-overhead: counter:type-t and sum:size_t";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      size_t sum = 0; 
      for(size_t i = 0; i < (size_t)list_size; i++) {
	for(size_t k = 0; k < (size_t)list_size; k++) {
	  sum += (size_t)i;
	}
      }

      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    } 
    { const char *stringOf_measureText = "for-loop-branch-overhead: counter:type-t and sum:long long int";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
	  long long int sum = 0; 
	  for(size_t i = 0; i < (size_t)list_size; i++) {
	    for(size_t k = 0; k < (size_t)list_size; k++) {
	      sum += (long long int)i;
	    }
	  }

      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    } 
    { const char *stringOf_measureText = "for-loop-branch-overhead: counter:uint and sum:uint";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
	  uint sum = 0; 
	  for(uint i = 0; i < list_size; i++) {
	    for(uint k = 0; k < list_size; k++) {
	      sum += (uint)i;
	    }
	  }


      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    } 
    { const char *stringOf_measureText = "for-loop-branch-overhead: counter:uint and sum:uint";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
	  uint sum = 0; 
	  for(uint i = 0; i < list_size; i++) {
	    for(uint k = 0; k < list_size; k++) {
	      sum += (uint)i;
	    }
	  }

      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    } 
    { const char *stringOf_measureText = "for-loop-branch-overhead: counter:type-t and sum:uint";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
	  uint sum = 0; 
	  for(size_t i = 0; i < (size_t)list_size; i++) {
	    for(size_t k = 0; k < (size_t)list_size; k++) {
	      sum += (uint)i;
	    }
	  }

      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    } 
}
