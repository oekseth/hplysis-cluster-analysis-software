//#include <my_global.h>
#include <mysql.h>
//§ ---------------------------------------
typedef unsigned int uint;
/* #include <time.h> */
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* #include <string.h> */
#include <unistd.h>
#include <inttypes.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/times.h>
#include <stdio.h>
/* #include <unistd.h> */
/* #include <sys/types.h> */
#include <stdlib.h>
#include <string.h>
#include <time.h>
/* #include <sys/times.h>  */
/* #include "libs.h" */
/* #include <stdlib.h> */
/* #include <stdio.h> */
/* #include <fcntl.h> */
/* #include <unistd.h> */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */
/* #include <sys/types.h> // some systems require it */
/* #include <sys/stat.h> */
/* #include <sys/termios.h> // for winsize */
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* #include <stddef.h> */
/* #include <string.h> */
/* #include <unistd.h> */
/* #include <signal.h> */
/* #include <sys/times.h> */
//#include "measure.h"
/* #include "def_memAlloc.h" */

static const uint kilo = 1000;
static clock_t clock_time_start; static clock_t clock_time_end;
static struct tms tms_start;
static  struct tms tms_end;
static  void start_time_measurement() {
  //tms_start = tms();
  clock_time_start = times(&tms_start);
  // if(() == -1) // starting values
  //   err_sys("times error");
}

const float FLT_MAX = 1000000; // TOdO: consider removing this

/**
   @brief Ends the measurement with regard to time.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float end_time_measurement(const char *msg, const float prev_time_inSeconds) {
  float user_time = 0, system_time=0;
  //return end_time_measurement(msg, user_time, system_time, prev_time_inSeconds);
  clock_time_end = times(&tms_end);
  long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
  const clock_t t_real = clock_time_end - clock_time_start;
  const float time_in_seconds = t_real/(float)clktck;
  const float diff_user = tms_end.tms_utime - tms_start.tms_utime;
  if(diff_user) user_time   = (diff_user)/((float)clktck);
  const float diff_system = tms_end.tms_stime - tms_start.tms_stime;
  system_time = diff_system/((float)clktck);
  if(msg) {
    // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
    if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) {
      printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
    } else {
      //float diff = prev_time_inSeconds / time_in_seconds;
      float diff = time_in_seconds / prev_time_inSeconds;
      // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
      printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg);
    }
  }
  //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
  return (float)time_in_seconds;
}

//#include "measure_base.h"

void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "Error: \"%s\"\n", mysql_error(con));
  mysql_close(con);
  exit(1);        
}



static char isTo__useIndex__onTriplet = 0; //! Note: based on observations we do Not observe any perofmrance-improvmeents when using our "index_triplet" in [bewlow], ie, where latter indicates that our "db_ds_directMapping.h" API results in a perofmrnace-boost (when comarped in disk-driven MySQL).
static char isTo__useIndex__onTriplet__usingBTree = 1;

#include <math.h>

//! A tes-tappraoch added to evlauate imapct of MySQL on searhces for pre-compteud simantic-siamlrity-emausres (oekseth, 06. jun. 2017).
static void __applyLogics__pairWiseSearch(MYSQL *con, const uint cntTo_insert, const uint cnt_searches_adjusted_db_rel, const char isTo_useRandomAccess, const char isTo__onlyPrint__time__insertAndSearch) {
  start_time_measurement();
  if (mysql_query(con, "DROP TABLE IF EXISTS s_db_rel")) {
    finish_with_error(con);
  }
  const uint config__keysToUse = 4; //! ie, as we asusme [ªbove] key has '4' elemnets  
  uint current_search = 0;
  const uint nrows = cntTo_insert;
#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} else {if(current_search++ > nrows) {current_search = 0;} val_ret = current_search;} val_ret;})
  
  if (mysql_query(con, "CREATE TABLE s_db_rel(head INT, tail INT, distance FLOAT)")) {      
    finish_with_error(con);
  }
  /* if(isTo__useIndex__onTriplet) { //! then we 'make use of' index to try imnproveing the search-speed: */
  /*   if (mysql_query(con, "CREATE INDEX index_triplet ON s_db_rel (head, rt, tail)")) { */
  /*     fprintf(stderr, "!!\t Unable to use the enw-created 'triplet-index', at %s:%d\n", __FILE__, __LINE__); */
  /*     finish_with_error(con); */
  /*   } */
  /* } else if(isTo__useIndex__onTriplet__usingBTree) { */
  /*   if (mysql_query(con, "CREATE INDEX index_triplet USING BTREE ON s_db_rel (head, rt, tail)")) { */
  /*     fprintf(stderr, "!!\t Unable to use the enw-created 'triplet-index', at %s:%d\n", __FILE__, __LINE__); */
  /*     finish_with_error(con); */
  /*   } */
  /* } */
  uint row_id_local = 0;
  const uint nrows__adjustedTo_db_rel = cntTo_insert;
  const uint nrows_each = (uint)sqrtf(cntTo_insert);
  printf("nrows_each=%u, at %s:%d\n", nrows_each, __FILE__, __LINE__);
  for(uint row_1 = 0; row_1 < nrows_each; row_1++) {
    for(uint row_2 = 0; row_2 < nrows_each; row_2++) {	  //!
      //for(uint row_id = 0; row_id < nrows__adjustedTo_db_rel; row_id++) {
      //! Construct a 'relaiton' and then 'insert the realtion:
      char str_loca[2000] = {'\0'}; sprintf(str_loca, "INSERT INTO s_db_rel VALUES(%u, %u, 1)", row_1, /*rt=*/row_2);
      if (mysql_query(con, str_loca)) {
	finish_with_error(con);
      }
      row_id_local += config__keysToUse;
    }  
  }
  const float cmp_time_insert = 0;   const float cmp_time_search = 0;
  float endTime_insert = 0; float endTime_search = 0;
 {
    char str_local[2000] = {'\0'}; sprintf(str_local, "mySQL:rel:insert");
    if(isTo__onlyPrint__time__insertAndSearch) {
      endTime_insert = end_time_measurement(NULL, cmp_time_search);
    } else {
      endTime_insert = end_time_measurement(str_local, cmp_time_insert);
    }
  }
  //! 
  //! Search:
  start_time_measurement();
  uint max_rowId = nrows - config__keysToUse - 1;
  uint cnt_found = 0;
  printf("(start-search)\tnrows_each=%u, at %s:%d\n", nrows_each, __FILE__, __LINE__);
  for(uint search_id = 0; search_id < cnt_searches_adjusted_db_rel; search_id++) {
    uint row_id_1 = __Mi__getRandVal(search_id);
    uint row_id_2 = __Mi__getRandVal(search_id);
    //if(row_id > max_rowId) {row_id = max_rowId;}
    
    //printf("------------, ex.3, at %s:%d\n", __FILE__, __LINE__);
    // const uint row_id_local = row_id_1;
    //char str_loca[2000] = {'\0'}; sprintf(str_loca, "SELECT relation_id FROM s_db_rel WHERE head=%u", row_id_local);
    //char str_loca[2000] = {'\0'}; sprintf(str_loca, "SELECT relation_id FROM s_db_rel WHERE head=%u", row_id_local);
    char str_loca[2000] = {'\0'}; 
    /* if(isTo__useIndex__onTriplet || isTo__useIndex__onTriplet__usingBTree) { //! then we 'make use of' index to try imnproveing the search-speed: */
    /*   sprintf(str_loca, "SELECT distance FROM s_db_rel  USE INDEX(index_triplet) WHERE head=%u AND rt=%u AND tail=%u", row_id_local, /\*rt=*\/row_id_local+1,  /\*tail=*\/row_id_local+2); */
    /*   //sprintf(str_loca, "SELECT relation_id FROM s_db_rel  WITH(INDEX(index_triplet)) WHERE head=%u AND rt=%u AND tail=%u", row_id_local, /\*rt=*\/row_id_local+1,  /\*tail=*\/row_id_local+2); */
    /* } else { */
    sprintf(str_loca, "SELECT distance FROM s_db_rel WHERE head=%u AND tail=%u", row_id_1, /*rt=*/row_id_2);
      //    }
    //char str_loca[2000] = {'\0'}; sprintf(str_loca, "SELECT * FROM s_db_rel WHERE head=%u AND rt=%u AND tail=%u", row_id_local+3, row_id_local, /*rt=*/row_id_local+1, /*tail=*/row_id_local+2);
    { //! Apply MSQL-based lgocis:
      if (mysql_query(con, 
		      //"SELECT Name FROM Cars WHERE Id=2; SELECT Name FROM Cars WHERE Id=2;"
		      str_loca
		      )) 
	{
	  finish_with_error(con);
	}
  
      int status = 0;
      do {  
	MYSQL_RES *result = mysql_store_result(con);
        
	if (result == NULL) 
	  {
	    finish_with_error(con);
	  }
            
	MYSQL_ROW row = mysql_fetch_row(result);
      
	if(row) {
	  cnt_found++;
	  //printf("%s\n", row[0]);
	}
      
	mysql_free_result(result);
                 
	status = mysql_next_result(con); 
     
	if (status > 0) {
	  finish_with_error(con);
	}
      
      } while(status == 0);
    }
    /* s_db_rel_t key = __M__init__s_db_rel_t(); */
    /* key.head = row_id; */
    /* key.tailRel.rt = row_id + 1; */
    /* key.tailRel.tail = row_id + 2; */
    /* key.tailRel.relation_id = row_id + 3; */
    /* //! Appply lgoics: */
    /* s_db_rel_t result_obj = __M__init__s_db_rel_t(); */
    /* //result +=  */
    /* //printf("key = %u, at %s:%d\n", (uint)key.head, __FILE__, __LINE__); */
    /* (t_float)find__s_db_ds_bTree_rel_t(&obj, key, &result_obj, (isTo_useKeyBasedSearchPattern == false)); */
    /* result += (t_float)result_obj.head; */
  }

  {
    char str_local[2000] = {'\0'}; sprintf(str_local, "mySQL:rel:search w/cnt_Found=%u", cnt_found);
    if(isTo__onlyPrint__time__insertAndSearch) {
      endTime_search = end_time_measurement(NULL, cmp_time_insert);
    } else {
      endTime_search = end_time_measurement(str_local, cmp_time_search);
    }
    //! Update oru result-set:
  }
  printf("%f\t%f\t # at %s:%d\n", endTime_insert, endTime_search, __FILE__, __LINE__);
}

static void __applyLogics__relation__createTable__andInsert(MYSQL *con, const uint cntTo_insert, const uint cnt_searches_adjusted_db_rel, const char isTo_useRandomAccess, const char isTo__onlyPrint__time__insertAndSearch) {
  start_time_measurement();
  if (mysql_query(con, "DROP TABLE IF EXISTS s_db_rel")) {
    finish_with_error(con);
  }
  const uint config__keysToUse = 4; //! ie, as we asusme [ªbove] key has '4' elemnets  
  uint current_search = 0;
  const uint nrows = cntTo_insert;
#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} else {if(current_search++ > nrows) {current_search = 0;} val_ret = current_search;} val_ret;})
  
  if (mysql_query(con, "CREATE TABLE s_db_rel(relation_id INT, head INT, rt INT, tail INT, distance FLOAT)")) {      
    finish_with_error(con);
  }
  if(isTo__useIndex__onTriplet) { //! then we 'make use of' index to try imnproveing the search-speed:
    if (mysql_query(con, "CREATE INDEX index_triplet ON s_db_rel (head, rt, tail)")) {
      fprintf(stderr, "!!\t Unable to use the enw-created 'triplet-index', at %s:%d\n", __FILE__, __LINE__);
      finish_with_error(con);
    }
  } else if(isTo__useIndex__onTriplet__usingBTree) {
    if (mysql_query(con, "CREATE INDEX index_triplet USING BTREE ON s_db_rel (head, rt, tail)")) {
      fprintf(stderr, "!!\t Unable to use the enw-created 'triplet-index', at %s:%d\n", __FILE__, __LINE__);
      finish_with_error(con);
    }
  }
  uint row_id_local = 0;
  const uint nrows__adjustedTo_db_rel = cntTo_insert;
  for(uint row_id = 0; row_id < nrows__adjustedTo_db_rel; row_id++) {
    //! Construct a 'relaiton' and then 'insert the realtion:
    char str_loca[2000] = {'\0'}; sprintf(str_loca, "INSERT INTO s_db_rel VALUES(%u, %u, %u, %u, 1)", row_id_local+3, row_id_local, /*rt=*/row_id_local+1, /*tail=*/row_id_local+2);
    if (mysql_query(con, str_loca)) {
      finish_with_error(con);
    }
    row_id_local += config__keysToUse;
  }  
  const float cmp_time_insert = 0;   const float cmp_time_search = 0;
  float endTime_insert = 0; float endTime_search = 0;
 {
    char str_local[2000] = {'\0'}; sprintf(str_local, "mySQL:rel:insert");
    if(isTo__onlyPrint__time__insertAndSearch) {
      endTime_insert = end_time_measurement(NULL, cmp_time_search);
    } else {
      endTime_insert = end_time_measurement(str_local, cmp_time_insert);
    }
  }
  //! 
  //! Search:
  start_time_measurement();
  uint max_rowId = nrows - config__keysToUse - 1;
  uint cnt_found = 0;
  for(uint search_id = 0; search_id < cnt_searches_adjusted_db_rel; search_id++) {
    uint row_id = __Mi__getRandVal(search_id);
    //if(row_id > max_rowId) {row_id = max_rowId;}
    
    //printf("------------, ex.3, at %s:%d\n", __FILE__, __LINE__);
    const uint row_id_local = row_id;
    //char str_loca[2000] = {'\0'}; sprintf(str_loca, "SELECT relation_id FROM s_db_rel WHERE head=%u", row_id_local);
    //char str_loca[2000] = {'\0'}; sprintf(str_loca, "SELECT relation_id FROM s_db_rel WHERE head=%u", row_id_local);
    char str_loca[2000] = {'\0'}; 
    if(isTo__useIndex__onTriplet || isTo__useIndex__onTriplet__usingBTree) { //! then we 'make use of' index to try imnproveing the search-speed:
      sprintf(str_loca, "SELECT relation_id FROM s_db_rel  USE INDEX(index_triplet) WHERE head=%u AND rt=%u AND tail=%u", row_id_local, /*rt=*/row_id_local+1,  /*tail=*/row_id_local+2);
      //sprintf(str_loca, "SELECT relation_id FROM s_db_rel  WITH(INDEX(index_triplet)) WHERE head=%u AND rt=%u AND tail=%u", row_id_local, /*rt=*/row_id_local+1,  /*tail=*/row_id_local+2);
    } else {
      sprintf(str_loca, "SELECT relation_id FROM s_db_rel WHERE head=%u AND rt=%u AND tail=%u", row_id_local, /*rt=*/row_id_local+1,  /*tail=*/row_id_local+2);
    }
    //char str_loca[2000] = {'\0'}; sprintf(str_loca, "SELECT * FROM s_db_rel WHERE head=%u AND rt=%u AND tail=%u", row_id_local+3, row_id_local, /*rt=*/row_id_local+1, /*tail=*/row_id_local+2);
    { //! Apply MSQL-based lgocis:
      if (mysql_query(con, 
		      //"SELECT Name FROM Cars WHERE Id=2; SELECT Name FROM Cars WHERE Id=2;"
		      str_loca
		      )) 
	{
	  finish_with_error(con);
	}
  
      int status = 0;
      do {  
	MYSQL_RES *result = mysql_store_result(con);
        
	if (result == NULL) 
	  {
	    finish_with_error(con);
	  }
            
	MYSQL_ROW row = mysql_fetch_row(result);
      
	if(row) {
	  cnt_found++;
	  //printf("%s\n", row[0]);
	}
      
	mysql_free_result(result);
                 
	status = mysql_next_result(con); 
     
	if (status > 0) {
	  finish_with_error(con);
	}
      
      } while(status == 0);
    }
    /* s_db_rel_t key = __M__init__s_db_rel_t(); */
    /* key.head = row_id; */
    /* key.tailRel.rt = row_id + 1; */
    /* key.tailRel.tail = row_id + 2; */
    /* key.tailRel.relation_id = row_id + 3; */
    /* //! Appply lgoics: */
    /* s_db_rel_t result_obj = __M__init__s_db_rel_t(); */
    /* //result +=  */
    /* //printf("key = %u, at %s:%d\n", (uint)key.head, __FILE__, __LINE__); */
    /* (t_float)find__s_db_ds_bTree_rel_t(&obj, key, &result_obj, (isTo_useKeyBasedSearchPattern == false)); */
    /* result += (t_float)result_obj.head; */
  }
  {
    char str_local[2000] = {'\0'}; sprintf(str_local, "mySQL:rel:search w/cnt_Found=%u", cnt_found);
    if(isTo__onlyPrint__time__insertAndSearch) {
      endTime_search = end_time_measurement(NULL, cmp_time_insert);
    } else {
      endTime_search = end_time_measurement(str_local, cmp_time_search);
    }
    //! Update oru result-set:
  }
  printf("%f\t%f\t # at %s:%d\n", endTime_insert, endTime_search, __FILE__, __LINE__);
}


/**
   @remarks Source:
   -- http://zetcode.com/db/mysqlc/
   @remarks 
   -- Compile: gcc external_mySql.c -o version  `mysql_config --cflags --libs` -std=c99 -O0 -g; ./version
**/
int main(int argc, char **argv)
{
  printf("MySQL client version: %s\n", mysql_get_client_info());
 

      printf("at .... %s:%d\n", __FILE__, __LINE__);

  MYSQL *con = mysql_init(NULL);

  if (con == NULL) 
    {
      fprintf(stderr, "Error(pointer):\t%s\n", mysql_error(con));
      exit(1);
    }

  if (mysql_real_connect(con, "localhost", "klatremus", "klatremus!", 
			 //! Note: mysql> GRANT ALL ON testdb.* to user12@localhost;
			 /*table=*/"testdb", 0, NULL, 
			 CLIENT_MULTI_STATEMENTS
			 //0
			 ) == NULL) 
    {
      fprintf(stderr, "Error(connect): %s\n", mysql_error(con));
      mysql_close(con);
      exit(1);
    }  
  printf("at %s:%d\n", __FILE__, __LINE__);
  /* if (mysql_query(con, "CREATE DATABASE testdb"))  */
  /*   { */
  /*     fprintf(stderr, "Error(database:create)\t%s\n", mysql_error(con)); */
  /*     mysql_close(con); */
  /*     exit(1); */
  /*   } */
    {
      const uint nrows_base = 1000*100; 
      //const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
      const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).

    uint cntTo_insert = 100;
    uint cnt_searches_adjusted_db_rel = 100000;
    char isTo_useRandomAccess = 0;
    char isTo__onlyPrint__time__insertAndSearch = 0;
    if(argc == 5) { //! then we assuem the arguments are: <exec> <cntTo_insert> <cnt_searches_adjusted_db_rel> <isTo_useRandomAccess> <isTo__onlyPrint__time__insertAndSearch>
      if(strlen(argv[1])) {
	cntTo_insert = atoi(argv[1]);
      }
      if(strlen(argv[2])) {
	cnt_searches_adjusted_db_rel = atoi(argv[2]);
      }
      if(strlen(argv[3])) {
	isTo_useRandomAccess = (char)atoi(argv[3]);
      }
      if(strlen(argv[4])) {
	isTo__onlyPrint__time__insertAndSearch = (char)atoi(argv[4]);
      }
    } else if(1) {
      cntTo_insert = nrows_base;
      cnt_searches_adjusted_db_rel = cnt_searches;
    }
    //      printf("at .... %s:%d\n", __FILE__, __LINE__);
    if(1) {
      const uint map_vertices_size = 4; uint map_vertices[4] = {1000, 10*1000, 1000*1000, 1000*1000*100}; //! Src: "https://github.com/sharispe/sm-tools-evaluation".
      const uint cntTo_insert = 1000*40;
      for(uint i = 0; i < map_vertices_size; i++) {
	const uint cnt_vertices = map_vertices[i];      
	const uint cnt_searches_adjusted_db_rel = cnt_vertices; //1000*100;
	printf("\n\n#\t\t at |V|=%u, .... %s:%d\n", cnt_searches_adjusted_db_rel, __FILE__, __LINE__);
	__applyLogics__pairWiseSearch(con, cntTo_insert, cnt_searches_adjusted_db_rel, isTo_useRandomAccess, isTo__onlyPrint__time__insertAndSearch);
      }
    } else {
      __applyLogics__relation__createTable__andInsert(con, cntTo_insert, cnt_searches_adjusted_db_rel, isTo_useRandomAccess, isTo__onlyPrint__time__insertAndSearch);
    }
  }

  mysql_close(con);
  exit(0);
}
