#include "hp_image_segment.h"
#include "kt_clusterAlg_hca_resultObject.h"

#define _mat(color) self->mat_RGB[color].matrix[row_id][col_id] 
#define _mat_c(color, _r, _c) self->mat_RGB[color].matrix[_r][_c] 
#define __MiF__getPixel(self, row_id, col_id) ({		 \
      s_hp_image_pixel_t obj_rgb = init__s_hp_image_pixel_t();	 \
      obj_rgb.pos_row = row_id;					 \
      obj_rgb.pos_col = col_id;					 \
      obj_rgb.RGB[0]  = _mat(0);				 \
      obj_rgb.RGB[1]  = _mat(1);				 \
      obj_rgb.RGB[2]  = _mat(2);				 \
      obj_rgb;}) //! ie, return.

//! @returen an 'empty' veriosn of the "s_hp_image_pixel_t" object (oekseth, 06. feb. 2018).
s_hp_image_pixel_t init__s_hp_image_pixel_t() {
  s_hp_image_pixel_t self;
  self.pos_row = UINT_MAX;
  self.pos_col = UINT_MAX;
  self.RGB[0]  = UCHAR_MAX;
  self.RGB[1]  = UCHAR_MAX;
  self.RGB[2]  = UCHAR_MAX;
  //!
  //! @return
  return self;
}


//!
//! Parse the string:
const char *__getIndexInt_fromChar(const char *row_input, const char sep, int *local_index) {
  *local_index = INT_MAX; //! ie, intiate. 
  assert(strlen(row_input));
  assert(sep != *row_input);
  { //! Remove any leading white spaces:
    const char *end = row_input + strlen(row_input);
    while( (*row_input == ' ') && (row_input < end) ) {
      row_input++;
    }
  }
  const char *str_start = row_input;
  const char *str_stop  = strchr(str_start, sep);
  if(str_stop != NULL) {
    //! 
    //! Extract the subset of chars which 'formts' the data.
    char str_localArray[12]; //! ie, the max number of characters which are expected for a gvien index.
    // printf("sep=%c, given row_input=\"%s\", at %s:%d\n", sep, row_input, __FILE__, __LINE__);
    assert(str_stop > str_start);
    const uint str_size = (uint)(str_stop - str_start);
    assert((str_size +1) < 12);
    bool is_digit = true;
    for(uint i = 0; i < str_size; i++) {
      const char digit = str_start[i]; 
      bool is_digit = isdigit(digit);
      // printf("[%u]=%c, given row_input=\"%s\", at %s:%d\n", i, digit, row_input, __FILE__, __LINE__);
      assert(is_digit);
      //! Update:
      str_localArray[i] = digit;
    }
    //    if(str_size < 12) 
    {
      for(uint i = str_size; i < 12; i++) {
	str_localArray[i] = '\0';
      }
    }
    //! 
    //! Conver thte char into the stirng.
    if(is_digit) {
      *local_index = atoi((char*)str_localArray);
    } 
    return str_stop;
  } else {
    fprintf(stderr, "!!\t Format error: input-string=\"%s\" did not correspond to exepcations. Please udpate your software. For quesitons contact the senior devleoper [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", str_start, __FUNCTION__, __FILE__, __LINE__);
    return NULL;
  }
}

//! Extract the RGB value from the text-string of: "9,0: (138,138,146)  #8A8A92  rgb(138,138,146)"
//! @return the identifed object.
s_hp_image_pixel_t extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(const char *row_input) {
  //!
  //! Intiialize: 
  s_hp_image_pixel_t self = init__s_hp_image_pixel_t();
  
  //! 
  //! Conver thte char into the stirng.
  int local_index = INT_MAX;
  const char *str_stop = __getIndexInt_fromChar(row_input, ',', &local_index);
  if(local_index != INT_MAX) { //! then the row-position is valid.
    self.pos_row = (uint)local_index;
    //!
    assert(str_stop != NULL);
    assert(*str_stop != '\0');
    { //! then parse: "column":
      int local_index = INT_MAX;
      str_stop = __getIndexInt_fromChar(str_stop + 1, ':', &local_index);
      if(local_index != INT_MAX) { //! then the row-position is valid.
	self.pos_col = (uint)local_index;
    //!
	assert(str_stop != NULL);
	assert(*str_stop != '\0');
	//!
	//! Start parsing RGB-scores:
	str_stop += 3; //! ie, increment past ": ("
	const uint arr_sep_size = 3; const char arr_sep[arr_sep_size] = {',', ',', ')'};
	for(uint i = 0; i < arr_sep_size; i++) { //! Parse RGB:
	  int local_index = INT_MAX;
	  str_stop = __getIndexInt_fromChar(str_stop, arr_sep[i], &local_index);  
	  // printf("RGB[%u] has index=[%d], given string=\"%s\", at %s:%d\n", i, local_index, row_input, __FILE__, __LINE__);
	  if((local_index != INT_MAX) && (local_index != USHRT_MAX) ) { //! then the row-position is valid.
	    assert(local_index >= 0);
	    assert(local_index < 256);
	    self.RGB[i] = (unsigned char)local_index;
	    //! Validae that is is Not any errors
	    assert(str_stop != NULL);
	    assert(*str_stop != '\0');	    
	  } else {
	    self.RGB[i] = 255; // TODO: vlaidate this ie, taht thsi cofnirms to hex='FF'
	    // FIXME: should we re-icnldue bleow format-erorr-warning? <-- removed, as it seemed liek this occred for hex=FF, as udpated in above (oekseth, 06.11.2020).
	    // fprintf(stderr, "??\t Possible Format error?: input-string=\"%s\" did not correspond to exepcations. Please udpate your software. For quesitons contact the senior devleoper [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", row_input, __FUNCTION__, __FILE__, __LINE__);
	  }
	  str_stop += 1; //! ie, increment past seperator.
	}
      } else {
	fprintf(stderr, "!!\t Format error: input-string=\"%s\" did not correspond to exepcations. Please udpate your software. For quesitons contact the senior devleoper [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", row_input, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else {
    fprintf(stderr, "!!\t Format error: input-string=\"%s\" did not correspond to exepcations. Please udpate your software. For quesitons contact the senior devleoper [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", row_input, __FUNCTION__, __FILE__, __LINE__);
  }

  
  //!
  //! 
  //!
  //! @return
  return self;
}

/**
   @brief export the s_hp_image_pixel_t object to a result file.
   @remarks correctness of this funcitonr equires cosnistency with the "extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(..)" function.,
 **/
void writeOut__toFile__format_imageMagicText_s_hp_image_pixel_t(s_hp_image_pixel_t self, FILE *file_out) {
  assert(file_out);  
  assert(self.pos_row != UINT_MAX);
  assert(self.pos_col != UINT_MAX);
  //!
  //! Get hexadecimal:
#define __rgb_vars self.RGB[0], self.RGB[1], self.RGB[2]
  char hexcol[16];   snprintf(hexcol, sizeof hexcol, "%02x%02x%02x", __rgb_vars);
  //! Export the RGB value from the text-string of: "9,0: (138,138,146)  #8A8A92  rgb(138,138,146)":
  fprintf(file_out, "%u,%u: (%u,%u,%u) #%s rgb(%u,%u,%u)\n",
	  self.pos_row, self.pos_col, 
	  __rgb_vars,
	  hexcol,
	  __rgb_vars);
#undef __rgb_vars
}

// r,g,b values are from 0 to 1 
  // h = [0,360], s = [0,1], v = [0,1] 
  //              if s == 0, then h = -1 (undefined)
//! Src: "http://coecsl.ece.illinois.edu/ge423/spring05/group8/finalproject/hsv_writeup.pdf"
static void __RGBtoHSV__s_hp_image_pixel_t_( t_float r, t_float g, t_float b, t_float *hue, t_float *saturation, t_float *value )  { 
  assert(r != T_FLOAT_MAX);
  assert(r < 256);
  assert(g != T_FLOAT_MAX);
  assert(g < 256);
  assert(b != T_FLOAT_MAX);
  assert(b < 256);
  //!
  //! Adjust into range [0 .... 1]:
  if(r != 0) {r = r / 255;}
  if(g != 0) {g = g / 255;}
  if(b != 0) {b = b / 255;}
  //! 
  //! 
  *hue        =        T_FLOAT_MAX;                                //        v
  *saturation        =        T_FLOAT_MAX;                                //        v
  *value        =        T_FLOAT_MAX;                                //        v
  //!
  const t_float min = macro_min( r, macro_min(g, b )); 
  const t_float max = macro_max( r, macro_max(g, b )); 
  const t_float delta = max - min;
  *value = max;
  if( max != 0 ) {
    *saturation = delta / max;              // s 
  } else {        
    // r = g = b = 0               // s = 0, v is undefined 
    *saturation = 0; 
    *hue = -1; 
    return;        
  }
  if( r == max ) {     *hue = ( g - b ) / delta;}         // between yellow & magenta
  else if( g == max ) { *hue = 2 + ( b - r ) / delta;}    // between cyan & yellow 
  else {                 *hue = 4 + ( r - g ) / delta;}   // between magenta & cyan
  //! 
  //! Adjst: 
  *hue        *=        60;                                //        degrees        
  if( *hue < 0 ) {*hue += 360;} //! ie, then a 'positive' angle.
}

//! Idneitfy the Hue, SAturaion and Value of an RGB pixel (oekseht, 06. okt. 2017).
void RGBtoHSV__s_hp_image_pixel_t_( s_hp_image_pixel_t self, t_float *hue, t_float *saturation, t_float *value )  { 
  __RGBtoHSV__s_hp_image_pixel_t_((t_float)self.RGB[0], (t_float)self.RGB[1], (t_float)self.RGB[2], 
				  hue, saturation, value);				  
}

//! @reutnr an intiatd verrison of the "s_hp_image_segment_t" object (oekseht, 06. feb . 2018).
s_hp_image_segment_t init__s_hp_image_segment_t() {
  s_hp_image_segment_t self;
  self.mat_RGB[0] = initAndReturn__empty__s_kt_matrix_base_t();
  self.mat_RGB[1] = initAndReturn__empty__s_kt_matrix_base_t();
  self.mat_RGB[2] = initAndReturn__empty__s_kt_matrix_base_t();
  //! 
  //! 
  return self;
}

//! @return a deep copy of the cpy object
s_hp_image_segment_t copy__s_hp_image_segment_t(const s_hp_image_segment_t *cpy) {
  s_hp_image_segment_t self;
  for(uint i = 0; i < 3; i++) {
    self.mat_RGB[i] = initAndReturn__allocate__s_kt_matrix_base_t(cpy->mat_RGB[i].nrows, cpy->mat_RGB[i].ncols, (cpy->mat_RGB[i].matrix));
  }
  //! 
  //! 
  return self;  
}

//! DE-allocates the "s_hp_image_segment_t" object (oekseth, 06. feb. 2018).
void free__s_hp_image_segment_t(s_hp_image_segment_t *self) {
  assert(self);
  free__s_kt_matrix_base_t(&(self->mat_RGB[0]));
  free__s_kt_matrix_base_t(&(self->mat_RGB[1]));
  free__s_kt_matrix_base_t(&(self->mat_RGB[2]));
}


// ---------------------------
typedef struct {
  //unsigned char red,green,blue;
  unsigned char values[3]; 
} s_hp_ppm_rgbPixel;

typedef struct {
     int nrows, ncols;
     s_hp_ppm_rgbPixel *data;
} s_hp_ppm;

#define CREATOR "RPFELGUEIRAS"
#define RGB_COMPONENT_COLOR 255

static s_hp_ppm *_read_format_ppm_subtype_N6(const char *input_file) {
  //enum {red,green,blue};
  // FIXME: validate that bleow is comaptilbe with "export_ppm.c"

  //! Open file for reading:
  FILE *fp_in = fopen(input_file, "rb");
  if (!fp_in) {
    fprintf(stderr, "!!\t Unable to open file=\"%s\" at %s:%d\n", input_file, __FILE__, __LINE__);
    exit(1);
  }

  //! Reader the image-header:
  char buff[16];  
  if (!fgets(buff, sizeof(buff), fp_in)) {
    fprintf(stderr, "!!\t Problems wiht reading the file-format for file=\"%s\" at %s:%d\n", input_file, __FILE__, __LINE__);
    perror(input_file);
    exit(1);
  }

  //! Validate correcntess of the image-sbutype-format:
  if( (buff[0] != 'P' || buff[1] != '6') ) {
    fprintf(stderr, "!!\t Problems wiht reading the file-format for file=\"%s\": file-format was expected to be of subtype 'P6', which is not the case, at %s:%d\n", input_file, __FILE__, __LINE__);
    exit(1);
  }

  //!
  //! Init object: 
  s_hp_ppm *img = (s_hp_ppm *)malloc(sizeof(s_hp_ppm));
  if (!img) {
    fprintf(stderr, "!!\t Unable to allocate memory; at this point htis could idnciate that the error occured ina  different part of the progrma, eg, in your script? Observaiton at %s:%d\n", __FILE__, __LINE__);
    exit(1);
  }

  //! Parse comments-secitons (if any) in the file:
  int c = getc(fp_in);
  while( (c == '#')  && !feof(fp_in) ) {
    while ( (getc(fp_in) != '\n') && !feof(fp_in) ) {
      c = getc(fp_in);
    }
  }

  ungetc(c, fp_in);
  //read image size information
  if (fscanf(fp_in, "%d %d", &img->nrows, &img->ncols) != 2) {
    fprintf(stderr, "!!\t Problems with parsing file=\"%s\": the image has an invalid size. Observaiton at %s:%d\n\n", input_file, __FILE__, __LINE__);
    exit(1);
    }

    //read rgb component
    int rgb_comp_color = 0;
    if (fscanf(fp_in, "%d", &rgb_comp_color) != 1) {
      fprintf(stderr, "!!\t Problems with parsing file=\"%s\": the RGB-component was not proerlty defined. Observaiton at %s:%d\n", input_file, __FILE__, __LINE__);
         exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color!= RGB_COMPONENT_COLOR) {
      fprintf(stderr, "!!\tProblems with parsing file=\"%s\": the file does not have 8-bits components. Observaiton at %s:%d\n", input_file, __FILE__, __LINE__);
         exit(1);
    }

    while (fgetc(fp_in) != '\n') ;
    //memory allocation for pixel data
    img->data = (s_hp_ppm_rgbPixel*)malloc(img->nrows * img->ncols * sizeof(s_hp_ppm_rgbPixel));

    if (!img) {
      const t_float f_size = (t_float)(img->nrows * img->ncols*sizeof(s_hp_ppm_rgbPixel))/(1.0*1000*1000); //! ie, to make the file-size easier to parse ... by mapping it to an MB-format.
      fprintf(stderr, "!!\t Problems with parsing file=\"%s\":  Unable to allocate %.2f Mb of memory. Observaiton at %s:%d\n", input_file, f_size, __FILE__, __LINE__);
      exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->nrows, img->ncols, fp_in) != img->ncols) {
      fprintf(stderr, "!!Problems with parsing file=\"%s\": Was not able to correctly read the pixels in the input-data. Observaiton at %s:%d\n", input_file, __FILE__, __LINE__);
      //!
      //! De-allocate:
      free((img->data)); img->data = NULL;
      //! 
      exit(1);
    }

    fclose(fp_in);
    return img;
}
// ----------


/**
   @brief loads a PPM-file into memroy
   @remarks
   -- motivaiton: expeiremtns nidnciates that it takes considerably shroter time to generate 'ppm-files' than txt-fiels (when using ImageMaci's convert tool as a benchmark).
 */
s_hp_image_segment_t ppm_import__s_hp_image_segment_t(const char *input_file) {
  assert(input_file);
  assert(strlen(input_file));

  //! 
  const char *data_file_txt = input_file;
  char *text_file = NULL;
  {
    const uint cnt_chars = (uint)strlen(input_file);
    if(
       (input_file[cnt_chars-3] != 'p') ||
       (input_file[cnt_chars-2] != 'p') ||
       (input_file[cnt_chars-1] != 'm') 
       ) { //! then we first convert the image file to the exepctd format using th ImageMaxics suit of converters.
      
      const uint text_file_size = (uint)strlen(input_file) + 10;
      char empty_c = '\0';
      text_file = allocate_1d_list_char(text_file_size, empty_c);
      assert(text_file);
      sprintf(text_file, "%s.ppm", input_file);
      assert(strlen(text_file));
      const uint tmp_cmd_size = text_file_size*3;
      char *tmp_cmd  = allocate_1d_list_char(tmp_cmd_size, empty_c);
      sprintf(tmp_cmd, "convert %s -compress none %s", input_file, text_file);
      // printf("(system::convert)\t %s\t #! at %s:%d\n", tmp_cmd, __FILE__, __LINE__);
      //! The call:
      system(tmp_cmd);
      data_file_txt = (const char*)text_file;
      free_1d_list_char(&tmp_cmd);
      //assert(false); // FIXME: remove.
    }
  }
  //! 
  //! Open file for reading:
  //FILE *file_inp = fopen(data_file_txt, "rb");
  //!
  //!
  s_hp_ppm *o_data = _read_format_ppm_subtype_N6(input_file);
  assert(o_data);
  assert(o_data->nrows > 0);
  assert(o_data->ncols > 0);  

  //!
  //! Allocate sizes:
  s_hp_image_segment_t self = init__s_hp_image_segment_t();
  //!
  //! Add the data:
  for(uint i_rgb = 0; i_rgb < 3; i_rgb++) {
    const uint max_nrows = o_data->nrows;   const uint max_ncols = o_data->ncols;
    self.mat_RGB[i_rgb] = initAndReturn__s_kt_matrix_base_t(max_nrows, max_ncols);
    uli list_pos = 0;
    for(uint row_id = 0; row_id < o_data->nrows; row_id++) {
      for(uint col_id = 0; col_id < o_data->ncols; col_id++, list_pos++) {
	self.mat_RGB[i_rgb].matrix[row_id][col_id] = o_data->data[list_pos].values[i_rgb];
      }
    }
  }
  //!
  //! De-allocate:
  free((o_data->data)); o_data->data = NULL;
  //! 
  //! @return
  return self;  
}

/**
   @brief loads a txt-file into memroy
 */
s_hp_image_segment_t txt_import__s_hp_image_segment_t(const char *input_file) {

  assert(input_file);
  assert(strlen(input_file));

  //! 
  const char *data_file_txt = input_file;
  char *text_file = NULL;
  {
    const uint cnt_chars = (uint)strlen(input_file);
    if(
       (input_file[cnt_chars-3] != 't') ||
       (input_file[cnt_chars-2] != 'x') ||
       (input_file[cnt_chars-1] != 't') 
       ) { //! then we first convert the image file to the exepctd format using th ImageMaxics suit of converters.
      
      const uint text_file_size = (uint)strlen(input_file) + 10;
      char empty_c = '\0';
      text_file = allocate_1d_list_char(text_file_size, empty_c);
      assert(text_file);
      sprintf(text_file, "%s.txt", input_file);
      assert(strlen(text_file));
      const uint tmp_cmd_size = text_file_size*3;
      char *tmp_cmd  = allocate_1d_list_char(tmp_cmd_size, empty_c);
      sprintf(tmp_cmd, "convert %s -compress none %s", input_file, text_file);
      // printf("(system::convert)\t %s\t #! at %s:%d\n", tmp_cmd, __FILE__, __LINE__);
      //! The call:
      system(tmp_cmd);
      data_file_txt = (const char*)text_file;
      free_1d_list_char(&tmp_cmd);
      //assert(false); // FIXME: remove.
    }
  }
  //! 
  //! Open file for reading:
  FILE *file_inp = fopen(data_file_txt, "rb");
  assert(file_inp);
  s_hp_image_segment_t self = init__s_hp_image_segment_t();
  //! 
  //! 
  const uint max_cnt_chars_eachLine = 500; //! where latter is an uppper estimae, ie, where each 'count' has a max of 12 chars, and there are max '12' such 'blocks'.
  //char line[max_cnt_chars_eachLine]; for(uint i = 0; i < max_cnt_chars_eachLine; i++) {line[i] = '\0';}
  const char empty_c = '\0';
  // FIXME: de-allcoate [”elow].
  char *line = allocate_1d_list_char(max_cnt_chars_eachLine, empty_c);
  //char line[max_cnt_chars_eachLine]; for(uint i = 0; i < max_cnt_chars_eachLine; i++) {line[i] = '\0';}
 //!
  //! Reading: Idenitfy dimensions:
  // TODO[perf]: if exeuciton-tiem is of improtance, then cosnider to instead direclty insert data inot memroy
  uint max_nrows = 0, max_ncols = 0;
  size_t len = 0;   ssize_t read;
  if(true) {
    // iffprintf(file_out, "# ImageMagick pixel enumeration: %u,%u,255,rgb\n", self->mat_RGB[0].nrows, self->mat_RGB[0].ncols);
    read = getline((char**)&line, &len, file_inp); 
    assert(line);
    assert(strlen(line) > 1);
    if(line[0] == '#') {
      const char *pos_start = strchr(line, ':');
      pos_start += 2; //! ie, for ": ".
      int local_index_nrows = INT_MAX;
      const char *str_stop = __getIndexInt_fromChar(pos_start, ',', &local_index_nrows);
      str_stop += 1; //! ie, for ",".
      int local_index_ncols = INT_MAX;
      str_stop = __getIndexInt_fromChar(str_stop, ',', &local_index_ncols);
      //!
      //! Update: 
      max_nrows = local_index_nrows;
      max_ncols = local_index_ncols;
      // printf("(dims)\t nrows=%u, ncols=%u, given line=\"%s\", at %s:%d\n", max_nrows, max_ncols, line, __FILE__, __LINE__);
    } else {
      fprintf(stderr, "!!\t Format error: input-string=\"%s\" (with size=%u, and read(len)=%d) did not correspond to exepcations. Tried reading from input_file=\"%s\". Please udpate your software. For quesitons contact the senior devleoper [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", line, (uint)strlen(line), (int)len, data_file_txt, __FUNCTION__, __FILE__, __LINE__);
    }
  } else {
    while ((read = getline((char**)&line, &len, file_inp)) != -1) {
      if(line[0] != '#') {
	//  printf("Retrieved line of length %zu :\n", read);
	printf("%s, at %s:%d\n", line, __FILE__, __LINE__);
	assert(read < max_cnt_chars_eachLine);
	line[read] = '\0'; //! ie, a 'trailing' char.
	assert(strlen(line) > 10);
	//! 
	//! Idneitfy the 'meaning' of the line:
	s_hp_image_pixel_t obj_rgb = extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(line);
	//! 
	//! Update the max-counts for rows and columns:
	max_nrows = macro_max(max_nrows, obj_rgb.pos_row);
	max_ncols = macro_max(max_ncols, obj_rgb.pos_col);
      }
    }
    //! Reset the file-poisiton: 
    rewind(file_inp);
    //! Adjsut wrt. the last inserted indexes:
    max_nrows++, max_ncols++;
  }
  assert(max_nrows != 0);
  assert(max_ncols != 0);
  //!
  //! Allocate sizes:
  self.mat_RGB[0] = initAndReturn__s_kt_matrix_base_t(max_nrows, max_ncols);
  self.mat_RGB[1] = initAndReturn__s_kt_matrix_base_t(max_nrows, max_ncols);
  self.mat_RGB[2] = initAndReturn__s_kt_matrix_base_t(max_nrows, max_ncols);
  //!
  //! Reading: store the data: 
  len = 0;   
  while ((read = getline((char**)&line, &len, file_inp)) != -1) {
    if(line[0] != '#') {
      //  printf("Retrieved line of length %zu :\n", read);
      // printf("%s, at %s:%d\n", line, __FILE__, __LINE__);
      assert(read < max_cnt_chars_eachLine);
      line[read] = '\0'; //! ie, a 'trailing' char.
      assert(strlen(line) > 10);
      //! 
      //! Idneitfy the 'meaning' of the line:
      s_hp_image_pixel_t obj_rgb = extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(line);
      const uint row_id = obj_rgb.pos_row;     const uint col_id = obj_rgb.pos_col;
      assert(row_id != UINT_MAX);
      assert(col_id != UINT_MAX);
      assert(row_id < max_nrows);
      assert(col_id < max_ncols);
      //! 
      //! Update the result object:      
#define _self(color) self.mat_RGB[color].matrix[row_id][col_id]
#define __rgb_vars obj_rgb.RGB[0], obj_rgb.RGB[1], obj_rgb.RGB[2]      
      _self(0) = obj_rgb.RGB[0];
      _self(1) = obj_rgb.RGB[1];
      _self(2) = obj_rgb.RGB[2];
      //! 
      //      printf("(color)=(%u, %u, %u) given line=\"%s\", at %s:%d\n", __rgb_vars, line, __FILE__, __LINE__);
      //! 
#undef _self
#undef __rgb_vars
    }
  }
  //!
  //! Close 'internals':
  if(text_file) {
    free_1d_list_char(&text_file);
  }
  fclose(file_inp);
  //! 
  //! @return
  return self;
}

/**
   @brief Export the object to a given file (oekseth, 06. feb. 2018).
   @param <input_file> is the file name to exprot the data to.
   @remarks uses the "Imagemagic" software for the export routines.
   @return the object which holds the input data.
**/
s_hp_image_segment_t import__s_hp_image_segment_t(const char *input_file) {
  assert(input_file);
  assert(strlen(input_file));
  // TODO: consider testing out parsing of BMP-files: https://rosettacode.org/wiki/Canny_edge_detector
  if(false) {
    return txt_import__s_hp_image_segment_t(input_file);
  } else { //! where the PPM-parsing seems to go faster (as the data is stored in a binary file, rhater than in a txt-file).
    return ppm_import__s_hp_image_segment_t(input_file);
  } 
}

void ppm_export__s_hp_image_segment_t(s_hp_image_segment_t *self, const char *result_file) { //, const char *format_export) {
  const char *data_file_txt = result_file;
  char *text_file = NULL;
  {
    const uint cnt_chars = (uint)strlen(result_file);
    // printf("result_file=\"%s\", at %s:%d\n", result_file, __FILE__, __LINE__);
    if(
       (result_file[cnt_chars-3] != 'p') ||
       (result_file[cnt_chars-2] != 'p') ||
       (result_file[cnt_chars-1] != 'm') 
       ) { //! then we first convert the image file to the exepctd format using th ImageMaxics suit of converters.
      const uint text_file_size = (uint)strlen(result_file) + 10;
      char empty_c = '\0';
      text_file = allocate_1d_list_char(text_file_size, empty_c);
      assert(text_file);
      sprintf(text_file, "%s.ppm", result_file);
      assert(strlen(text_file));    
      data_file_txt = (const char*)text_file;
    }
    /* sprintf(tmp_cmd, "convert %s -compress none %s", result_file, text_file); */
    /* //! The call: */
    /* system(tmp_cmd); */
    /* data_file_txt = (const char*)text_file; */
  }
  //! 
  //! Open file for writing:
  FILE *file_out = fopen(data_file_txt, "wb");
  assert(file_out);
  (void)fprintf(file_out, "P6\n%d %d\n255\n", self->mat_RGB[0].nrows, self->mat_RGB[0].ncols);
  for (uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++)  {
    for (uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++)    {
      //      if(self->mat_RGB[0].matrix[i][j] != T_FLOAT_MAX)
      {
	const unsigned char color[3] = {
	  (uchar)self->mat_RGB[0].matrix[row_id][col_id],
	  (uchar)self->mat_RGB[1].matrix[row_id][col_id],
	  (uchar)self->mat_RGB[2].matrix[row_id][col_id]
	};
	//! Src: https://rosettacode.org/wiki/Bitmap/Write_a_PPM_file
	/* color[0] = i % 256;  /\* red *\/ */
	/* color[1] = j % 256;  /\* green *\/ */
	/* color[2] = (i * j) % 256;  /\* blue *\/ */
	//printf("\t color:[%u, %u, %u]\t at %s:%d\n", color[0], color[1], color[2], __FILE__, __LINE__);
	(void) fwrite(color, 1, 3, file_out);
      }
    }
  }
  (void) fclose(file_out);
}

//! Exprot image throguh a txt-intermidary
void txt_export__s_hp_image_segment_t(s_hp_image_segment_t *self, const char *result_file) { //, const char *format_export) {
  const char *data_file_txt = result_file;
  char *text_file = NULL;
  {
    const uint cnt_chars = (uint)strlen(result_file);
    // printf("result_file=\"%s\", at %s:%d\n", result_file, __FILE__, __LINE__);
    if(
       (result_file[cnt_chars-3] != 't') ||
       (result_file[cnt_chars-2] != 'x') ||
       (result_file[cnt_chars-1] != 't') 
       ) { //! then we first convert the image file to the exepctd format using th ImageMaxics suit of converters.
      const uint text_file_size = (uint)strlen(result_file) + 10;
      char empty_c = '\0';
      text_file = allocate_1d_list_char(text_file_size, empty_c);
      assert(text_file);
      sprintf(text_file, "%s.txt", result_file);
      assert(strlen(text_file));    
      data_file_txt = (const char*)text_file;
    }
    /* sprintf(tmp_cmd, "convert %s -compress none %s", result_file, text_file); */
    /* //! The call: */
    /* system(tmp_cmd); */
    /* data_file_txt = (const char*)text_file; */
  }
  //! 
  //! Open file for writing:
  FILE *file_out = fopen(data_file_txt, "wb");
  assert(file_out);
  //! Write otu the format: eg, # ImageMagick pixel enumeration: 481,321,255,rgb
  fprintf(file_out, "# ImageMagick pixel enumeration: %u,%u,255,rgb\n", self->mat_RGB[0].nrows, self->mat_RGB[0].ncols);
  //! 
  //!  Write out each cell: 
  for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {
    for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
      if(self->mat_RGB[0].matrix[row_id][col_id] != T_FLOAT_MAX) {
	//! Consturct the ojbect:
	const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, row_id, col_id);
	//! 
	//! Export the object:
	writeOut__toFile__format_imageMagicText_s_hp_image_pixel_t(obj_rgb, file_out);	 
      } else {
	printf("(omit-cell) \t [%u][%u]\t at %s:%d\n", row_id, col_id, __FILE__, __LINE__);
      }
    }
  }
  //!
  //!
  //!
  //! Close 'internals':
  fclose(file_out);
  if(text_file) { //! then we first need to convert to the corrct result format: 
    const uint text_file_size = (uint)strlen(result_file) + 40;
    const uint tmp_cmd_size = text_file_size*3;
    char empty_c = '\0';
    char *tmp_cmd  = allocate_1d_list_char(tmp_cmd_size, empty_c);
    //sprintf(tmp_cmd, "bash tmp_convert.bash %s %s", text_file, result_file);
    sprintf(tmp_cmd, "convert %s -compress none %s", text_file, result_file);
    //! The call:
    // printf("(system::convert)\t \"%s\"\t #! at %s:%d\n", tmp_cmd, __FILE__, __LINE__);
    // exec(tmp_cmd);
    //system("bash tmp_convert.bash");
    system(tmp_cmd);
    //! De.-akllcoate:
    free_1d_list_char(&tmp_cmd);
    free_1d_list_char(&text_file);
    //  assert(false); // FIXME: remove.
  }
}
/**
   @brief Export the object to a given file (oekseth, 06. feb. 2018).
   @param <self> is the object which holds the input data.
   @param <result_file> is the file name to exprot the data to.
   @remarks uses the "Imagemagic" software for the export routines.
**/
void export__s_hp_image_segment_t(s_hp_image_segment_t *self, const char *result_file) { //, const char *format_export) {
  if(true) {
    ppm_export__s_hp_image_segment_t(self, result_file);
  } else {
    //! Exprot image throguh a txt-intermidary
    txt_export__s_hp_image_segment_t(self, result_file);
  }
}

//! @return a matrix which hold the 'Hue' component of the input file.
s_kt_matrix_base_t fetch_Hue_s_hp_image_segment_t(s_hp_image_segment_t *self) {
  assert(self->mat_RGB[0].nrows > 0);
  assert(self->mat_RGB[0].ncols > 0);
  //! Intiate:
  s_kt_matrix_base_t mat_ret = initAndReturn__s_kt_matrix_base_t(self->mat_RGB[0].nrows, self->mat_RGB[0].ncols);
  //! Iterate: 
  for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
    for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {
      //! Consturct the ojbect:
      const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, row_id, col_id);
/*       s_hp_image_pixel_t obj_rgb = init__s_hp_image_pixel_t(); */
/* #define _self(color) self->mat_RGB[color].matrix[row_id][col_id] */
/*       // obj_rgb.pos_row = row_id;       obj_rgb.pos_col = col_id; */
/*       obj_rgb.RGB[0]  = _self(0);  */
/*       obj_rgb.RGB[1]  = _self(1);  */
/*       obj_rgb.RGB[2]  = _self(2);  */
      //! 
      //! Convert to HSV: 
      t_float hsv[3] = {0, 0, 0};
      RGBtoHSV__s_hp_image_pixel_t_(obj_rgb, &hsv[0], &hsv[1], &hsv[2]);
      //! Store:
      mat_ret.matrix[row_id][col_id] = hsv[0];
#undef _self
    }
  }
  //!
  //!
  return mat_ret;
}

s_kt_matrix_base_t fetch_features__s_hp_image_segment_t(s_hp_image_segment_t *self, const e_hp_image_segment_t enum_id, uint radius) {
//s_kt_matrix_base_t fetch_features__s_hp_image_segment_t(s_hp_image_segment_t *self, const e_hp_image_segment_t enum_id, uint radius, t_float scaling) {
  assert(self->mat_RGB[0].nrows > 0);
  assert(self->mat_RGB[0].ncols > 0);
  //! Intiate:
  long long int cnt_rows = self->mat_RGB[0].nrows * self->mat_RGB[0].ncols;
  assert(cnt_rows < (long long int) UINT_MAX);
  if(cnt_rows > (long long int) UINT_MAX) {
    fprintf(stderr, "!!\t(critical-error::ABORTS)!!\t The max number of cells=%lld is outside the supported range, ie, given matrix-diemsnions[%u, %u] Please requrest a code update wrt. the 'uint' data-type, contacting the senior devloper [oekseth@gmail.com] wrt. the latter. Observation at [%s]:%s:%d\n", cnt_rows, self->mat_RGB[0].nrows, self->mat_RGB[0].ncols, __FUNCTION__, __FILE__, __LINE__);
    exit(2);
  }
  uint cnt_cols = 9; //! ie, "3+3+2+1", where latter is the default for 'radius=1' and Hue.
  //! 
  //! To correctly set the radius we compue .... eg, for "(r(1)=1)", "(r(2)=4)", "(r(3)=9)"
  if(radius <= 1) {
    if( (enum_id == e_hp_image_segment_Hue) || (enum_id == e_hp_image_segment_Hue_relative) ) {
      radius = 1;
    }
  }
  uint cnt_pixels = radius * radius; //! ie, a 'squared' rectangle.
  assert(cnt_pixels > 0);
  //!
  //! 
  assert(enum_id != e_hp_image_segment_undef);
  if(enum_id == e_hp_image_segment_RGB) {
    cnt_cols = radius * 3; //! where '3' represents "RGB".
  } else if(enum_id == e_hp_image_segment_HSV) {
    cnt_cols = cnt_pixels * 3; //! where '3' represents "RGB".
  } else if(enum_id == e_hp_image_segment_Hue) {
    //! 
    //! 
    cnt_cols = cnt_pixels; //! where '3' represents "RGB".
  } else if(enum_id == e_hp_image_segment_Hue_relative) {
    //! 
    //! 
    cnt_cols = cnt_pixels; //! where '3' represents "RGB".
  } else { //! then a 'fall-back'
    assert(false); //! ie, an heads-up.
    //!
    //! The fall-back
    cnt_cols = cnt_pixels; //! where '3' represents "RGB".
  }
  //!
  //!
  s_kt_matrix_base_t mat_ret = initAndReturn__s_kt_matrix_base_t((uint)cnt_rows, cnt_cols);
  assert((self->mat_RGB[0].nrows * self->mat_RGB[0].ncols) <= (uint)cnt_rows); //! ie, as it oehrwise could be an sisue of voerlofw
  //! Iterate: 
  loint curr_row_pos = 0;
  for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
    for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++, curr_row_pos++) {
      //for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {      
      //printf("\t pos:%llu < max:%u (given dim:[%u][%u], matPos:[%u][%u])\t at %s:%d\n", curr_row_pos, mat_ret.nrows, self->mat_RGB[0].nrows, self->mat_RGB[0].ncols, row_id, col_id,  __FILE__, __LINE__);
      //!
      assert(row_id < self->mat_RGB[0].nrows);
      assert(col_id < self->mat_RGB[0].ncols);	     
      assert(curr_row_pos < (loint)cnt_rows);
      assert(curr_row_pos < (loint)mat_ret.nrows);
      //!
      //! Aux data:
      t_float hsv_this[3] = {0, 0, 0};					
      {
	const s_hp_image_pixel_t obj_rgb_this = __MiF__getPixel(self, row_id, col_id); 
	/*! Convert to HSV: */						
	RGBtoHSV__s_hp_image_pixel_t_(obj_rgb_this, &hsv_this[0], &hsv_this[1], &hsv_this[2]); 
      }

      // FIXME: for radius consider instead to use a 'circle' (instead of a rectangle) as a boudning box ... where we ....??...


      //! -------------------------------------------------------------------
      //!
      //! Define the local funcitosn: 
#define __MiF__RGB(_r, _c) ({ \
	  if((_r < self->mat_RGB[0].nrows) && (_c < self->mat_RGB[0].ncols) ) { \
	assert(curr_row_pos < mat_ret.nrows); \
	  mat_ret.matrix[curr_row_pos][/*col_id=*/0] = _mat_c(0, _r, _c); \
	  mat_ret.matrix[curr_row_pos][/*col_id=*/1] = _mat_c(1, _r, _c); \
	  mat_ret.matrix[curr_row_pos][/*col_id=*/2] = _mat_c(2, _r, _c);    }})
#define __MiF__HSV(_r, _c) ({ \
	const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, _r, _c); \
	/*! Convert to HSV: */ \
	t_float hsv[3] = {0, 0, 0}; \
	RGBtoHSV__s_hp_image_pixel_t_(obj_rgb, &hsv[0], &hsv[1], &hsv[2]); \
	mat_ret.matrix[curr_row_pos][/*col_id=*/0] = hsv[0]; \
	mat_ret.matrix[curr_row_pos][/*col_id=*/1] = hsv[1]; \
	mat_ret.matrix[curr_row_pos][/*col_id=*/2] = hsv[2]; })
#define __MiF__Hue(_r, _c) ({ \
	const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, _r, _c); \
	/*! Convert to HSV: */ \
	t_float hsv[3] = {0, 0, 0}; \
	RGBtoHSV__s_hp_image_pixel_t_(obj_rgb, &hsv[0], &hsv[1], &hsv[2]); \
	mat_ret.matrix[curr_row_pos][/*col_id=*/0] = hsv[0]; })
#define __MiF__Hue_relative(_r, _c) ({ \
	const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, _r, _c); \
	/*! Convert to HSV: */ \
	t_float hsv[3] = {0, 0, 0}; \
	RGBtoHSV__s_hp_image_pixel_t_(obj_rgb, &hsv[0], &hsv[1], &hsv[2]); \
	mat_ret.matrix[curr_row_pos][/*col_id=*/0] = ( (hsv_this[0] != 0) && (hsv[0] != 0) ) ? hsv[0]/hsv_this[0] : 0; }) //! ie, relative adjustment.
      //! -------------------------------------------------------------------
      //! 
      //! 
      if(radius <= 1) {
	assert(radius == 1);
	if(enum_id == e_hp_image_segment_RGB) {
	  assert(mat_ret.ncols == (radius * 3));
	  __MiF__RGB(row_id, col_id);
	} else {
	  //! 
	  if(enum_id == e_hp_image_segment_HSV) {
	    assert(mat_ret.ncols == (radius * 3));
	    __MiF__HSV(row_id, col_id);
	  } else if(enum_id == e_hp_image_segment_Hue) {
	    assert(mat_ret.ncols == (radius * 1));
	    __MiF__Hue(row_id, col_id);
	  } else if(enum_id == e_hp_image_segment_Hue_relative) {
	    assert(mat_ret.ncols == (radius * 1));
	    __MiF__Hue_relative(row_id, col_id);
	  } else {
	    assert(false); //! ie, then add support for this use-case
	  }
	}
	//! 
	//! Increment:
	assert(curr_row_pos < (loint)cnt_rows);
	assert(curr_row_pos < (loint)mat_ret.nrows);
	//curr_row_pos++;
	assert(curr_row_pos <= (loint)cnt_rows);
	assert(curr_row_pos <= (loint)mat_ret.nrows);
	//printf("\t\t(upd) pos:%llu < max:%u (given dim:[%u][%u], matPos:[%u][%u])\t at %s:%d\n", curr_row_pos, mat_ret.nrows, self->mat_RGB[0].nrows, self->mat_RGB[0].ncols, row_id, col_id,  __FILE__, __LINE__);
      } else { // if(radius == 2) {
      //! Idneitfy midpoint: 
	const int midpoint = (int)((t_float)radius / 2); //! ie, the center-point: 
	assert(midpoint > 0);
	if(radius == 2) {assert(midpoint == 1);}
#define __MiF__addBlock() ({						\
	  for(int _r = (int)row_id - midpoint; _r < (row_id + midpoint); _r++) { \
	  for(int _c = (int)col_id - midpoint; _c < (col_id + midpoint); _c++) { \
	    if( (_r >= 0) && (_c >= 0) ) { \
	      __MiF__localLogics(_r, _c); /*ie, insert*/	\
	      /*curr_row_pos++*/; /*ie, increment*/		\
	      assert(curr_row_pos <= (loint)cnt_rows);\
	      assert(curr_row_pos <= (loint)mat_ret.nrows);\
	    }   }  } })
	//! 
	//! Apply task-speciffic logics: 
	if(enum_id == e_hp_image_segment_RGB) {
#define __MiF__localLogics __MiF__RGB
	  __MiF__addBlock();
#undef __MiF__localLogics
	} else if(enum_id == e_hp_image_segment_HSV) {
#define __MiF__localLogics __MiF__HSV
	  __MiF__addBlock();
#undef __MiF__localLogics
	} else if(enum_id == e_hp_image_segment_Hue) {
#define __MiF__localLogics __MiF__Hue
	  __MiF__addBlock();
#undef __MiF__localLogics
	} else if(enum_id == e_hp_image_segment_Hue_relative) {
#define __MiF__localLogics __MiF__Hue_relative
	  __MiF__addBlock();
#undef __MiF__localLogics
	} else {
	  assert(false); // then we need to  add supproit for this.
	}
	/* //! Remove 'self': */
	/* assert(curr_row_pos > 0); */
	/* curr_row_pos--;       */
      }
    }
  }
  //!
  //!
  return mat_ret;
}

/**
   @enum e_hp_image_segment_typeOf_clusterMerge
   @brief identifies the type of merge operaiton which is to be applied for a given cluster (oekseth, 06. feb. 2018).
 **/
typedef enum e_hp_image_segment_typeOf_clusterMerge {
  e_hp_image_segment_typeOf_clusterMerge_average,
  //  e_hp_image_segment_typeOf_clusterMerge_,
  e_hp_image_segment_typeOf_clusterMerge_undef
} e_hp_image_segment_typeOf_clusterMerge_t;

/**
   @struct
   @brief provide wrapper logics to merge pixel information.
 **/
typedef struct s_hp_image_adjustpixels {
  e_hp_image_segment_typeOf_clusterMerge_t merge_type;
  //! sperately for each RGB value: 
  s_kt_list_1d_float_t obj_count[3];
  //! sperately for each RGB score: 
  s_kt_list_1d_float_t obj_score[3];
} s_hp_image_adjustpixels_t;

//! @return an itnalized veirons of the "s_hp_image_adjustpixels_t" object (oekseth, 06. feb. 2018).
s_hp_image_adjustpixels_t init__s_hp_image_adjustpixels_t(e_hp_image_segment_typeOf_clusterMerge_t merge_type) {
  s_hp_image_adjustpixels_t self; self.merge_type = merge_type;
  for(uint i = 0; i < 3; i++) {
    //! sperately for each RGB value: 
    self.obj_count[i] = setToEmpty__s_kt_list_1d_float_t();
    //! sperately for each RGB score: 
    self.obj_score[i] = setToEmpty__s_kt_list_1d_float_t();
  }
  //!
  //!
  return self;
}

//! De-allocate. 
void free__s_hp_image_adjustpixels_t(s_hp_image_adjustpixels_t *self) {
  //!
  for(uint i = 0; i < 3; i++) {
    free__s_kt_list_1d_float_t(&(self->obj_count[i]));
    free__s_kt_list_1d_float_t(&(self->obj_score[i]));
  }
}

//! Update the "cluster_id" with knowledge of the "obj_rgb".
void addColor_toPixel__s_hp_image_adjustpixels_t(s_hp_image_adjustpixels_t *self, const uint cluster_id, const s_hp_image_pixel_t obj_rgb) {
  //!
  for(uint i = 0; i < 3; i++) {
    if(obj_rgb.RGB[i] != T_FLOAT_MAX) {
      increment_scalar__s_kt_list_1d_float_t(&(self->obj_score[i]), cluster_id, /*count=*/obj_rgb.RGB[i]);
      increment_scalar__s_kt_list_1d_float_t(&(self->obj_count[i]), cluster_id, /*count=*/1); //! ie, increment the count.
    }
  }
}

//! Idneitfy the 'glboal' color for all vertices: 
void start_globalUpdate__s_hp_image_adjustpixels_t(s_hp_image_adjustpixels_t *self) { //, s_hp_image_segment_t *obj_data) {
  if(self->merge_type != e_hp_image_segment_typeOf_clusterMerge_average) {
    assert(false); // ie, as we hten need to add support for this use case. 
  }  
  //for(uint vertex_id = 0; vertex_id < list_vertexToCluster->list_size; vertex_id++) {    
  //! 
  //! 
  //! Compute average:
  for(uint cluster_id = 0; cluster_id < self->obj_count[0].list_size; cluster_id++) {
    if(self->obj_score[0].list[cluster_id] == T_FLOAT_MAX) {continue;} //! ie, as then the latter is Not set.
    for(uint i = 0; i < 3; i++) {
      if(
	 (self->obj_score[i].list[cluster_id] != 0) &&
	 (self->obj_count[i].list[cluster_id] != 0) ) {
	self->obj_score[i].list[cluster_id] = (
					 self->obj_score[i].list[cluster_id] 
					 / 
					 self->obj_count[i].list[cluster_id]);     
      } else {
	self->obj_score[i].list[cluster_id] = T_FLOAT_MAX;
      }
    }
  }    
}

/**
   @brief merge the clusters for 'fixed' cluster partiions (oekseth, 06. feb. 2018).
   @param <self> is the object which hold the image data to be adjusted
   @param <merge_type> is the type of merge-oepraiton whcih is to be applied
   @param <> 
 **/
void adjust_mergeClusters_fixed__s_hp_image_segment_t(s_hp_image_segment_t *self, e_hp_image_segment_typeOf_clusterMerge_t merge_type, const s_kt_list_1d_uint_t *list_vertexToCluster) {
  assert(self);
  assert(list_vertexToCluster);
  assert(list_vertexToCluster->list_size > 0);
  //! 
  //! Identify max number of clusters:
  uint cnt_clusters = 0;
  for(uint i = 0; i < list_vertexToCluster->list_size; i++) {
    const uint cluster_id = list_vertexToCluster->list[i];
    if(cluster_id != UINT_MAX) {cnt_clusters = macro_max(cnt_clusters, cluster_id);}
  }
  if(cnt_clusters != 0) {
    cnt_clusters++;
    //! Construct an object ot hold the scores to be used:
    s_hp_image_adjustpixels_t obj_merge = init__s_hp_image_adjustpixels_t(merge_type);

    for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
      for(uint i = 0; i < 3; i++) {
	obj_merge.obj_score[i].list[cluster_id] = 0;
	obj_merge.obj_count[i].list[cluster_id] = 0;
      }
    }
    //! 
    //! Increment through the clusters, and idneitfy common scores:
    uint vertex_id = 0;
    for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
      for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {
	assert(vertex_id < list_vertexToCluster->list_size);
	const uint cluster_id = list_vertexToCluster->list[vertex_id];
	if(cluster_id != UINT_MAX) {
	  //! 
	  const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, row_id, col_id); 
	  //! Update the "cluster_id" with knowledge of the "obj_rgb":
	  addColor_toPixel__s_hp_image_adjustpixels_t(&obj_merge, cluster_id, obj_rgb);
	}
	//!
	vertex_id++;
      }
    }
    assert(vertex_id <= list_vertexToCluster->list_size);
    //! 
    //! Compute average, and then udpate the global strucutre:
    start_globalUpdate__s_hp_image_adjustpixels_t(&obj_merge); //, self);
    //! 
    //! Adjust the scores: 
    vertex_id = 0;
    for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
      for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {
	assert(vertex_id < list_vertexToCluster->list_size);
	const uint cluster_id = list_vertexToCluster->list[vertex_id];
	if(cluster_id != UINT_MAX) {
	  //!
	  for(uint i = 0; i < 3; i++) {
	    if(obj_merge.obj_score[i].list[cluster_id] != T_FLOAT_MAX) {
	      _mat(i) = obj_merge.obj_score[i].list[cluster_id]; //! ie, then update the score.
	    }
	  }
	}
	//!
	vertex_id++;
      }
    }
    //!
    //! De-allocate:
    free__s_hp_image_adjustpixels_t(&obj_merge);    
  } //! else there is nothing to 'do', ie, as all vertices are then assumed to be in different lcusters.
}

/**
   @brief merge the clusters for hiearhcical (HCA) clusters (oekseth, 06. feb. 2018).
   @param <self> is the object which hold the image data to be adjusted
   @param <merge_type> is the type of merge-oepraiton whcih is to be applied
   @param <>    
   @remarks we iterate through a tree of vertices, eg, {v1, v2}=>n1, where "n1 >= 0" refers to an index while "n1 < 0" implies a reference to an internal index. 
 **/
void adjust_mergeClusters_HCA__s_hp_image_segment_t(s_hp_image_segment_t *self, e_hp_image_segment_typeOf_clusterMerge_t merge_type, const s_kt_list_1d_uint_t *list_vertexToCluster, const s_kt_clusterAlg_hca_resultObject_t *obj_hca) {
    //! -----------------------
    //!
    //! Find the root-node(s):
  //    s_kt_clusterAlg_hca_resultObject_2d_t hca_2dStack = build_2dStruct__s_kt_clusterAlg_hca_resultObject_2d_t(hca_obj);

  //! 
  //! Step: idneitfy the largest 'negative' value of vertices 'which ..... ' .... two 'imaginary' vertices may never improve a vertex.

  //! 
  int min_node_id = INT_MAX;       int max_node_id = 0;
  for(uint node_id_local = 0; node_id_local < obj_hca->nrows; node_id_local++) {
    const Node_t node = obj_hca->mapOf_nodes[node_id_local];
    min_node_id = macro_min(min_node_id, node.right);
    min_node_id = macro_min(min_node_id, node.left);
    if(node.left != INT_MAX) {
      max_node_id = macro_max(max_node_id, node.left);
    }
    if(node.right != INT_MAX) {
      max_node_id = macro_max(max_node_id, node.right);
    }
    //printf("node.right=%d, max_node_id=%d, at %s:%d\n", node.right, max_node_id, __FILE__, __LINE__);
  }
  assert(min_node_id <= 0); assert(min_node_id <= max_node_id);
  if(max_node_id == min_node_id) {
    fprintf(stderr, "!!\t No vertices were set in your input-object: please investigate. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //!
  //! 

  //! 
  //! Step: identify vertex-pairs which (a) are to be merged and (b) which does Not have any 'unresolved' dependencies (ie, which does Not have nay negatve edge neighbours). 
  uint offsetToRemove__internalToRealLife = (uint)(-1*min_node_id);

  s_kt_set_2dsparse_t stack2d; setTo_Empty__s_kt_set_2dsparse_t(&(stack2d));
  s_kt_list_1d_uint_t mapOf_internalStartNodes = setToEmpty__s_kt_list_1d_uint_t();
  for(uint node_id_local = 0; node_id_local < obj_hca->nrows; node_id_local++) {
    const Node_t node = obj_hca->mapOf_nodes[node_id_local];
    if(node.left == node.right) {continue;} //! ie, to avoid isnerting a self-cyclic loop.
    if( (node.right != INT_MAX) && (node.left != INT_MAX) ) {
      //printf("node.right=%d, max_node_id=%d, at %s:%d\n", node.right, max_node_id, __FILE__, __LINE__);
      assert(node.right <= (int)max_node_id);
      // FIXME: add optiopn to 'invert' [below] ... ie, to be able to color 'inveresly'.
      const int local_right = node.right + (int)offsetToRemove__internalToRealLife;
      const int local_left  = node.left + (int)offsetToRemove__internalToRealLife;
      assert(local_right < mapOf_internalStartNodes.list_size);
      assert(local_left < mapOf_internalStartNodes.list_size);      
      //!
      //! Update the counter wrt. the 'incoming nodes':
      increment_scalar__s_kt_list_1d_uint_t(&mapOf_internalStartNodes, local_right, /*icnrmeent-value=*/1); //! where we udpate only for "right" as the paris hold 'right-to-left' data-structre (oekseth, 06. feb. 2018).
      //!
      //! Add the left-->right relationship:
      // printf("\t (append-to-list)\t %d(%u)-->%d(%u), w/score=%f, at %s:%d\n", node.left, local_left, node.right, local_right, node.distance, __FILE__, __LINE__);
      push__idScorePair__s_kt_set_2dsparse_t(&(stack2d), local_left, local_right, node.distance);
    }  
  }

  //! Construct an object ot hold the scores to be used:
  s_hp_image_adjustpixels_t obj_merge = init__s_hp_image_adjustpixels_t(merge_type);


  //! 
  // s_kt_list_1d_pair_t obj_v_to_pixels = init__s_kt_list_1d_pair_t(self->mat_RGB[0].nrows * self->mat_RGB[0].ncols);
  { //! Step: Insert the 'global' pixel scores:
    //! 
    //! Increment through the clusters, and idneitfy common scores:
    uint vertex_id = 0;
    for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
      for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {
	//! 
	const s_hp_image_pixel_t obj_rgb = __MiF__getPixel(self, row_id, col_id); 
	//! Update the "cluster_id" with knowledge of the "obj_rgb":
	addColor_toPixel__s_hp_image_adjustpixels_t(&obj_merge, /*cluster_id=*/(uint)offsetToRemove__internalToRealLife + vertex_id, obj_rgb);
	//!
	// assert(vertex_id < obj_v_to_pixels.list_size);
	// obj_v_to_pixels.list[vertex_id] = MF__initVal__s_ktType_pair(row_id, col_id);
	//!
	vertex_id++;
      }
    }
  }
  
  //! 
  //! Step: Find the root-node(s):
  s_kt_set_1dsparse_t obj_stack;
  { //! consturcT: a new-intalized stack of start-nodes wrt. teh self object (oekseth, 06. feb. 2017).
    assert(mapOf_internalStartNodes.list_size > 0);
    allocate__simple__s_kt_set_1dsparse_t(&obj_stack, mapOf_internalStartNodes.list_size);
    uint cnt_added = 0;
    //! Traverse the data-structure, 'settting' the root-node(s):
    for(uint i = 0; i < mapOf_internalStartNodes.list_size; i++) {
      const uint cnt_incominb = mapOf_internalStartNodes.list[i];
      if(cnt_incominb  != UINT_MAX) {
	assert(cnt_incominb != UINT_MAX);
	// printf("\t(consider::add-start-node)\t '%u' w/cnt=%u, at %s:%d\n", i, cnt_incominb, __FILE__, __LINE__);
	if(cnt_incominb == 0) {
	  //printf("\t\t(add-start-node)\t '%u' w/cnt=%u, at %s:%d\n", i, cnt_incominb, __FILE__, __LINE__);
	  push__s_kt_set_1dsparse_t(&obj_stack, /*vertex=*/i);
	  cnt_added++;
	}
      }
    }
    if(cnt_added == 0) {
      fprintf(stderr, "!!\t Seems like you have a cyclic network (or alterantivly that the 'tree used for input' cosnists of isolated nodes), ie, please investigate your input: if this warning seems odd, then pelase contact the senior devleoper at [oesketh@gmial.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
  }


  //! 
  //! Step(color-update-step): (a) merge (colors of) vertex-pairs, and then (b) update the global data-strucutre. 
  { //! Traverse the data-set, and update the 'glboal' data-setructure:
    bool has_data = true; uint cnt_evaluated = 0;
    //const uint __scoreIsVisitee = UINT_MAX - 2;
    //assert(mapOf_cntChildsInParents_size < __scoreIsVisitee);
    //!
    //! Allocat eha 'boolean lsit' to ensure taht a vertex is visited only once, ie, as a 'guanrtee' for clyclid relationships.
    const char empty_0 = 0;
    uint *__mapIsVisited = allocate_1d_list_uint(mapOf_internalStartNodes.list_size, empty_0);


    do {
      t_float scalar_dummy = T_FLOAT_MAX;
      const uint node_id_local = pop__s_kt_set_1dsparse_t(&obj_stack, &scalar_dummy);
      if(node_id_local != UINT_MAX) { //! then 'the tiem' is Not the last item.
	cnt_evaluated++;
	//!
	//! Investigate if 'this' is the 'last time' a vertex 'adds this':
	assert(__mapIsVisited[node_id_local] != UINT_MAX);
	if(__mapIsVisited[node_id_local] != UINT_MAX) { __mapIsVisited[node_id_local]++; }
	if( (__mapIsVisited[node_id_local] != UINT_MAX) && (__mapIsVisited[node_id_local] >= mapOf_internalStartNodes.list[node_id_local]) ) {
	  //!
	  //! Then update the stack:
	  uint list_toAdd_size = 0;
	  const uint *list_toAdd = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(stack2d), node_id_local, &list_toAdd_size);
	  //assert(node_id_local >= hca_2dStack.offsetToRemove__internalToRealLife);
	  // printf("vertex[%u = %d] has cnt-children=%u, at %s:%d\n", node_id_local, node_id_local - (int)hca_2dStack.offsetToRemove__internalToRealLife, list_toAdd_size, __FILE__, __LINE__);
	  if(list_toAdd && list_toAdd_size) {	      
	    assert(list_toAdd_size > 0);
	    uint listOf_scores_size = 0;
	    const t_float *listOf_scores = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&(stack2d), node_id_local, &listOf_scores_size);
	    assert(listOf_scores); assert(listOf_scores_size == list_toAdd_size);
	    for(uint k = 0; k  < list_toAdd_size; k++) {
	      const uint child_index = list_toAdd[k];
	      assert(child_index < mapOf_internalStartNodes.list_size);
	      // FIXME[new-option]: ... to adjust the contribiotn of each vertex wrt. teh distance-color-udpates based on its/the simliarty of the veritces ... when compared top the average score .... 
	      // FIXME[web]: is it correct to 'omit' the imaignary nodes <-- cnosider/suggest to udpate our web-itnerface wr.t 'such' an dhten udpate [”elow] (oekseth, 06. feb. 2017).
	      if(obj_merge.obj_score[0].list[node_id_local] != T_FLOAT_MAX) { //! then we udpate the 'child' with infromation about 'this':
		//! 
		//! Get the udpates score: 
		s_hp_image_pixel_t obj_rgb = init__s_hp_image_pixel_t();
		obj_rgb.RGB[0] = obj_merge.obj_score[0].list[node_id_local];
		obj_rgb.RGB[1] = obj_merge.obj_score[1].list[node_id_local];
		obj_rgb.RGB[2] = obj_merge.obj_score[2].list[node_id_local];
		//! Update the "cluster_id" with knowledge of the "obj_rgb":
		addColor_toPixel__s_hp_image_adjustpixels_t(&obj_merge, /*cluster_id=*/(uint)child_index, obj_rgb);		
	      }
	      /* { //! Increment the sum using the 'knowledge from teh parent': */
	      /* 	//! Note: in [”elow] we itnroduce/supports/handles a compelxtiy (which is stringcly speaking not neccesary for HCA): to supprot the case where there are muliple 'shortest paths' in an heirhariccal tree, a 'support' which is included in order to support 'future needs'.		 */
	      /* 	const bool isA_realNode = ((child_index >= offsetToRemove__internalToRealLife)  */
	      /* 				   && (node_id_local >= offsetToRemove__internalToRealLife) //! ie, where >=" is used to 'handle' the '0-index-case'. */
	      /* 				   ); */
	      /* 	{ */
	      /* 	  int this_id_xmtImgAdjustment   = (int)node_id_local - (int)offsetToRemove__internalToRealLife; */
	      /* 	  int child_id_xmtImgAdjustment  = (int)child_index - (int)offsetToRemove__internalToRealLife;		     */
		  
	      /* 	  if(isA_realNode) { */
	      /* 	    /\* assert(this_id_xmtImgAdjustment < (int)self->nrows); *\/ */
	      /* 	    /\* assert(child_id_xmtImgAdjustment < (int)self->nrows);*\/ */
	      /* 	    assert(this_id_xmtImgAdjustment >= 0); */
	      /* 	    assert(child_id_xmtImgAdjustment >= 0); */
	      /* 	    //!  */
	      /* 	    //! Get the row--colo idneitfy of the vertex. */
	      /* 	    assert(false); */
	      /* 	    //!  */
	      /* 	    assert(false); */
	      /* 	    // FIXME: update the 'global' strucutre wrt. the udpated color.  */
	      /* 	    assert(false); */
	      /* 	  } //else { 		  } */
	      /* 	  // FIXME: updat eht elgovla scoring-talbe ...  */
	      /* 	  assert(false); */
	      /* 	} */
	      /* } */
	      //! -----------------------------------------------------------------------------
	      //!
	      //! Add the child tot he stack:
	      push__s_kt_set_1dsparse_t(&obj_stack, /*vertex(local-scope)=*/(uint)child_index);	    
	    }
	  }
	}
      } else {has_data = false;} //! ie, as we then asusme teh alst element is found.
      if(cnt_evaluated > (mapOf_internalStartNodes.list_size*3)) { //! where "*3" is used as a node may not ahve mreo tahn 2 chidlrne, ie, an overeestimate.
	fprintf(stderr, "!!\t Seems like your data-set does Not hold a 1:1 mapping between the nodes: though the latter may be correct we for simplicy abort at this eeuxeiocn-point: if the 'latter' is of itnerest then pelase forward a feature-request to [eosekth@gmail.com] (thereby remoivngt htis 'cirtical brea'). Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	break;
      }
    } while(has_data == true);
    //! 
    if(cnt_evaluated == 0) {
      fprintf(stdout, "(info)\t Was not able to idneityf vertices in your data-set: could be due to (a) your network is cyclic; alternativly the network cosnists of 'isoalated non-connected nodes' (a case soemthimes occuring for HCA-dataset). If latter result is Not as expected, then please cotnact senior devleoper [oeksethh@gmail.com]. Observiaotni at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
    
  }
  //! 
  //! Step: 

    //! -----------------------
    //!
    //! De-allcoate the lcoally reserved data-entites:
    //free_1d_list_uint(&mapOf_cntChildsInParents);
    free_s_kt_set_1dsparse_t(&obj_stack);
    //free_1d_list_uint(&__mapIsVisited);
  free_s_kt_set_2dsparse_t(&(stack2d));
    //! 
    //! Adjust the scores: 
  uint vertex_id = 0;
    for(uint row_id = 0; row_id < self->mat_RGB[0].nrows; row_id++) {
      for(uint col_id = 0; col_id < self->mat_RGB[0].ncols; col_id++) {
	// assert(vertex_id < list_vertexToCluster->list_size);
	const uint cluster_id = (uint)offsetToRemove__internalToRealLife + vertex_id; // list_vertexToCluster->list[vertex_id];
	if(cluster_id != UINT_MAX) {
	  //!
	  for(uint i = 0; i < 3; i++) {
	    if(obj_merge.obj_score[i].list[cluster_id] != T_FLOAT_MAX) {
	      _mat(i) = obj_merge.obj_score[i].list[cluster_id]; //! ie, then update the score.
	    }
	  }
	}
	//!
	vertex_id++;
      }
    }
    //!
    //! De-allocate:
    free__s_hp_image_adjustpixels_t(&obj_merge);    
    // free__s_kt_list_1d_pair_t(&(obj_v_to_pixels));
}

s_hp_image_segment_t apply_blur__s_hp_image_segment_t(s_hp_image_segment_t *self, const e__blur enum_id, const t_float radius) {
  assert(self);
  s_hp_image_segment_t obj_new = init__s_hp_image_segment_t();
  for(uint color_id = 0; color_id < 3; color_id++) {
    assert(self->mat_RGB[color_id].nrows > 0);
    assert(self->mat_RGB[color_id].ncols > 0);
    //! Apply blurring: 
    obj_new.mat_RGB[color_id] = apply_blur__kt_aux_matrix_blur(&(self->mat_RGB[color_id]), enum_id, radius);
  }

  //! @return
  return obj_new;
}
s_hp_image_segment_t apply_blur__s_hp_image_segment_t(s_hp_image_segment_t *self, const e_kt_normType_t enum_id) {
  assert(self);
  s_hp_image_segment_t obj_new = init__s_hp_image_segment_t();
  for(uint color_id = 0; color_id < 3; color_id++) {
    assert(self->mat_RGB[color_id].nrows > 0);
    assert(self->mat_RGB[color_id].ncols > 0);
    //! Apply blurring: 
    obj_new.mat_RGB[color_id] = initAndReturn__copy__normalizedByEnum__kt_aux_matrix_norm(&(self->mat_RGB[color_id]), enum_id, true);
  }

  //! @return
  return obj_new;
}

s_hp_image_segment_t apply_binning__s_hp_image_segment_t(s_hp_image_segment_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring) {
  assert(self);
  s_hp_image_segment_t obj_new = init__s_hp_image_segment_t();
  for(uint color_id = 0; color_id < 3; color_id++) {
    assert(self->mat_RGB[color_id].nrows > 0);
    assert(self->mat_RGB[color_id].ncols > 0);
    //! Apply blurring: 
    s_kt_list_1d_float_t obj_histo = applyTo_matrix__kt_aux_matrix_binning(&(self->mat_RGB[color_id]), cnt_bins, isTo_forMinMax_useGlobal, enum_id_scoring);
    const uint nrows = 1; const uint row_id = 0;
    obj_new.mat_RGB[color_id] = initAndReturn__s_kt_matrix_base_t(nrows, obj_histo.list_size);
    for(uint col_id = 0; col_id < obj_histo.list_size; col_id++)  {
      obj_new.mat_RGB[color_id].matrix[row_id][col_id] = obj_histo.list[col_id];
    }
    free__s_kt_list_1d_float_t(&obj_histo);
  }

  //! @return
  return obj_new;
}
