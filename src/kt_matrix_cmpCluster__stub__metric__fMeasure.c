t_float sumOf_1_sizes = 0;
for(uint cluster_1 = 0; cluster_1 < self->mapOfCluster1_size; cluster_1++) { 
  
  const t_float cntInCluster_1 = self->mapOfCluster1[cluster_1]; 
  t_float max_sum = T_FLOAT_MIN_ABS;
  for(uint cluster_2 = 0; cluster_2 < self->mapOfCluster2_size; cluster_2++) { 
    const t_float cntInCluster_2 = self->mapOfCluster2[cluster_2]; 
    //! What we expect:
    assert(cntInCluster_2 != 0);       assert(cntInCluster_2 != T_FLOAT_MAX);
    assert(cntInCluster_1 != 0);       assert(cntInCluster_1 != T_FLOAT_MAX);
    //! Copmute the local sum:
    const t_float sum_num = 2*cntInCluster_1*cntInCluster_2;
    const t_float sum_denum = cntInCluster_1 + cntInCluster_2;
    if(sum_num != sum_denum) {      
      const t_float sum_local = sum_num / sum_denum;
      //! update the max-score-property:
      max_sum = macro_max(max_sum, sum_local);
    }
  }
  //! Update the global-score-property:
  if(max_sum != T_FLOAT_MIN_ABS) {
    score += cntInCluster_1*max_sum;
  } 
  sumOf_1_sizes += cntInCluster_1;
 }
//!
//! Adjust the sum:
if( (sumOf_1_sizes != 0)  && (score != 0) ) {
  score = score / sumOf_1_sizes; //! ie, the relative 'importance' of the clusters.o
 }
