#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h" //! ie, the 'geneeralized' API for hpLysis

/**
   @brief demosntrates applciality of our "standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(..)" to idneitfy clsuter-coutn and clsuter-accurayc for mulipel dat-asets.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks in this example we:
   -- input: use a 'wrapper-object-spec' to simplify handling, integration, and copmparison of different-formatted data.
   -- evlauation: combines HCA with simlarity-emtics and CCMs to 'get' scalar valeus to simplify cluster-ocmaprison: in this exampel we (for each data-set) writes our ccm-score and nclsuter-count.
**/
int main() 
#else
  void tut_clust_dynamicK_6_find_nCluster_multipleData_Euclid(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const e_kt_correlationFunction_t metric_id, e_hpLysis_clusterAlg_t clustAlg, e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
  //!
  //! Specify the data-set to use: 

#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
  e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
  const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  //e_kt_correlationFunction_t = e_kt_correlationFunction_groupOf_minkowski_euclid;
#endif 

  //! Swet the data-strings:
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    const s_hp_clusterFileCollection data_obj = mapOf_realLife[data_id];
    const char *stringOf_tagSample = mapOf_realLife[data_id].file_name;
    if(stringOf_tagSample == NULL) {stringOf_tagSample =  mapOf_realLife[data_id].tag;}
    //char str_local[3000]; sprintf(str_local, "%u_%s", data_id, stringOf_tagSample);
    //!
    //! Load data:
    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
    s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
    assert(obj_matrixInput.nrows > 0); //! ie, as we exepct data to have been loaded.
    { //! Validate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
      const s_kt_matrix_t *matrix_input_1 = &obj_matrixInput;
      assert(matrix_input_1);
      assert(matrix_input_1->nrows);
      assert(matrix_input_1->ncols);
      long long int cnt_interesting = 0;
      for(uint i = 0; i < matrix_input_1->nrows; i++) {
	for(uint j = 0; j < matrix_input_1->ncols; j++) {
	  cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
      assert(cnt_interesting > 0); //! ie, as we otherwse have a poitnless input to the clsutering
    }
    //!
    //! Apply the logics:
    const bool isAn__adjcencyMatrix = data_obj.inputData__isAnAdjcencyMatrix;
    t_float result__ccm = 0; uint result__ncluster = 0;
    const bool is_ok = standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_matrixInput, isAn__adjcencyMatrix, metric_id, config__ccmMatrixBased__toUse, clustAlg, &result__ccm, &result__ncluster);
    assert(is_ok);
    //!
    //! Print result:
    if(true) {
      printf("\t [%u]:\"%s\" ccm='%f' for |cluster|='%u', at %s:%d\n", data_id, stringOf_tagSample, result__ccm, result__ncluster, __FILE__, __LINE__);
    } else {
      printf("\t [%u]:\"%s\" has 'ideal' cluster-count=%u, a cluster-count associated to a result-accurayc='%f', at %s:%d\n", data_id, stringOf_tagSample, result__ncluster, result__ccm, __FILE__, __LINE__);
    }
    //! De-allcoate:
    free__s_kt_matrix(&obj_matrixInput);
  }

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
