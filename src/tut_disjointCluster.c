#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"

/**
   @brief examplifies how to use k-means clustering to comptue results.
   @author Ole Kristian Ekseth  (oekseth,  06. jan. 2017).
   @remarks we examplify: how to use threshold-mask-variables to idneitfiy signicant regions, sigifant regiosn which are 'extracted' using disjtoitnf-roest-clsutering. The stnregth (and wkenass) of this appraoch cosnerns the direct relatioship between prior assumptiosn and clsuter-results: for disjoitn-clsutering to produce emanignful and correct results the 'remval' of in-signicnat cells need to be correct, a c'rorectness' which depends on acucrate prior assumptions wrt. the data. 
   @remarks we examplfiy: (a) how to comptue disjoint-clusters clsuters; (b) how to use the internal export-rotuiens for data-export; (c) how to programtailally access the cluster-resutls: from the latter we obseve how the 'same' generaiotn-and-exprotr-otuiens in k-means clsutering ("tut_kCluster.c") may be sued for disjtoint-forest-clustering, ie, where differnece si fodun wrt. the 'need' to specifiy mask-trheshold-fitlers. 
   @remarks Extends "tut_mine.c" and "tut_buildSimilarityMatrix.c". Demonstrates features wrt.:
   -- input: use a differnet programmitc appraoch to specify the input,ieg, in contrast to "tut_buildSimilarityMatrix.c"
   -- computation: use a standard k-means AVG-strategy, while Not applying a pre-comptuation of the input-matrix
   -- result: a permtaution of "tut_buildSimilarityMatrix.c" where:
   (a) we write out the mapping between each vertex and teh cluster-centroid.
   (b) examplifies how to programaitally access the cluster-results.
**/
int main() 
#endif
{
  const bool inputMatrix__isAnAdjecencyMatrix = true;
  const uint cnt_Calls_max = 1; const uint arg_npass = 1000;
  const char *result_file = "test_tut_disjoint.tsv";
  const uint nrows = 4;     const uint ncols = 4; 
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);  
  init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;                                                                              
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  const uint nclusters = 2; //! which for this fonfiguraiton is ignored.
  //! ------------- Set threhsholds:
  const t_float threshold_min = 0.5; const t_float threshold_max = 3; const bool isTo_applyValueThresholds = true;
  //! -------------
  //! ----------------------------------------------------------------------------------
  //! 
  { //! Add data:
    uint mapOf_scoreDistributions_case1[nrows][nrows] = {
      //! List the clusterId assicated to each vertex:
      {1, 1, 0, 0}, //! Case1.1: ie, all in the same cluster, eg, where vertex[0] is in cluster='0'
      {1, 1, 0, 0}, //! Case1.2: ie, all in different clusters.
      {0, 0, 1, 1}, //! Case1.3: ie, a 'strongly skewed cluster': permtuation-1
      {0, 0, 1, 1}, //! Case1.4: ie, a 'strongly skewed cluster': permtuation-2
    };  
    assert(obj_matrixInput.nrows == nrows);
    assert(obj_matrixInput.ncols == ncols);
    //! Copy the [ªbove] data-set into the 'new data-set':
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	//if(row_id != col_id) 
	//if(row_id < col_id) 
	  {
	    obj_matrixInput.matrix[row_id][col_id] = mapOf_scoreDistributions_case1[row_id][col_id];
	  }
      }
    }
  }

  //! Open teh file :
  FILE *file_out = fopen(result_file, "wb");
  //FILE *file_out = stdout;
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}

  //!
  //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:
  for(uint cnt_Calls = 0; cnt_Calls < cnt_Calls_max; cnt_Calls++) {
    s_hpLysis_api_t hp_cluster_config = setToEmpty__s_hpLysis_api_t(NULL, NULL);

    // (Symmetric) adjacency matrix                                                                                                                            
    hp_cluster_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
    hp_cluster_config.config.corrMetric_prior_use = false; //! ie, use data-as-is.
    //! Set the htresholds:
    hp_cluster_config.config.config__valueMask.isTo_applyValueThresholds = isTo_applyValueThresholds;
    hp_cluster_config.config.config__valueMask.threshold_min = threshold_min;
    hp_cluster_config.config.config__valueMask.threshold_max = threshold_max;
    hp_cluster_config.config.config__valueMask__afterSimMatrix.threshold_min = T_FLOAT_MAX; //! ie, do Not use
    hp_cluster_config.config.config__valueMask__afterSimMatrix.threshold_max = T_FLOAT_MAX; //! ie, do Not use
    /* hp_cluster_config.opt_kCount__disjointThresholdFilter__scoreRelative_min = T_FLOAT_MAX; //! ie, do Not use */
    /* hp_cluster_config.opt_kCount__disjointThresholdFilter__scoreRelative_max = T_FLOAT_MAX; //! ie, do Not use */

    /* Expect 2 clear clusters */
    // printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
    const bool is_also_ok = cluster__hpLysis_api (
						  &hp_cluster_config, clusterAlg, &obj_matrixInput,
						  /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						  );
    assert(is_also_ok);

    //! 
    //! Export results to "file_out":
    assert(file_out);
    const s_kt_correlationMetric_t corrMetric_prior = hp_cluster_config.config.corrMetric_prior;
    fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
    /* bool is_ok_e = export__hpLysis_api(&hp_cluster_config, /\*stringOf_file=*\/NULL, /\*file_out=*\/file_out, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV); */
    /* assert(is_ok_e); */
    //! Export the vertex-clsuterId-memberships:
    bool is_ok_e = export__hpLysis_api(&hp_cluster_config, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &obj_matrixInput);
    assert(is_ok_e);

    //! 
    //! 
    { //! Export: traverse teh generated result-matrix-object and write out the results:    
      const uint *vertex_clusterId = hp_cluster_config.obj_result_kMean.vertex_clusterId;
      assert(vertex_clusterId);
      const uint cnt_vertex = hp_cluster_config.obj_result_kMean.cnt_vertex;
      assert(cnt_vertex > 0);
      fprintf(file_out, "clusterMemberships=[");
      uint max_cnt = 0;
      for(uint i = 0; i < cnt_vertex; i++) {fprintf(file_out, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
      fprintf(file_out, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);
    }
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&hp_cluster_config);	    
  }


  //!
  //! Close the file and the amtrix:
  assert(file_out); 
  if(file_out != stdout) {fclose(file_out); file_out = NULL;}
  free__s_kt_matrix(&obj_matrixInput);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

