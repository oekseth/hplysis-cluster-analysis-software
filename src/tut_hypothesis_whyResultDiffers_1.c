#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_evalHypothesis_algOnData.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
//#include "aux_sysCalls.h"
/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks examplifeis appliciaotn of two CCMs, mulipel clsuter-algorithms and and muliple data-sets.
   @remarks provide a gneelriaed cofniguraiton wrt. "__Mi__fileToHold__dataLoads", alg_useHCA_asPrestep=[true, false], alg=["hcaAndKMeans",   "KMeans",  "hca",  "", ], sim=[MINE_EUCLID, all], etc.
   @remarks may be seen as an evlauation-script which 'rpoduce' a [iteration x clusterAlg=[none, hcaBefore]x[kmeans, kmedoid]][data_out] for different rela-life example-sets ... produce a CCM-result-amtrix which may be sued as input to a "highcharts" data-set. ... 
   @remarks a motvaiton 'behidn this script' is to investigate resutl-accracy wrt. the appraoch to compute hca-k-means, ie, for which we evlauate a number of data-sets-files. In this example we dmonstrate how HCA-pre-step signicntly improves the result-accruacy and exeuciton-time for several cases data-input-cases. 
   @remarks results: (among others) compare different "k-means++" impmenetaiton-strategies, eg, wrt. the exeuction-time-effect and and result-accuracy of different param,eters wrt. [sim-metric-preStep, randomizaiton-before-clusterAlg, clusterAlg, ranodmziaiotn-inside-clsuter-alg, cluster-centroid-identivicaiton, etc.]. 
   @remarks Demonstrates features wrt.:
   -- input: merge two 'matemcialcal' fucntiosn into one matrix
   -- computation(a): HCA+kmeans: use of two differnet permtautiosn for the same 'main algorithm' call, wher ehte latter is described by "clustAlg"
   -- computation(b): "kmeans++": for k-means algorithms use tow different randomenss-intlaitions, ie, to examplify the 'initaiton' and applciaiton fo different k-means permtautions.
   -- computation(c): CCM: (a) idneitfy the clust-result-accuracy through applicoatn ot two speciifc/defiend clsuter-resutls and then (b) evaluate the simlairty-matrices (cosnisteing of CCM-scores) through applicioant of CCMs and 'inside-for-loop-consturcted' golst-stadnard-null-hypothesis;
   -- result(a): how results from mmuliple clusterings may be exported into one single-unified java-script file.
   -- result(b): write out the CCM-score for a givne comptaution.
   -- result(c): write out the CCM-scores wrt. the CCM-matrics simlairty with different gold-standard-hypothesis;
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
   @todo wet. 'cornete perofmrnace-measurements' ... try out different configurations for "arg_npass", ie, as the latter seems to be 'hiogly indicative' of the exeuction-time <--- cosnider updating our aritce wrt. the latter, ie, that exeuciton-tiem of k-means apprxoimate 'infitnte' for high cofnigruation-values of the "arg_npass" input-cofniguraiton-parameter.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
    const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
    const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
    const bool isTo__testHCA_komb__inKmeans = false;
    const bool isTo__evaluate__simMetric__MINE__Euclid = true;
    const char *stringOf_resultDir = "";
#define __Mi__algSet 0 //! ie, the type of algorithms to evaluate.
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
#endif
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------


#ifdef __Mi__createDirectory
    if(stringOf_resultDir && strlen(stringOf_resultDir)) {
      char str_local[2000] = {'\0'}; sprintf(str_local, "mkdir -p %s", stringOf_resultDir);
      MF__system(str_local); //! ie, then create the directory, assumign we are on a LINUX-compatible system.
    }
#endif

 const uint sizeOf__nrows = 100;
 const uint sizeOf__ncols = 100;
 //const uint arg_npass = 200;
const uint arg_npass = 600;
 //const uint arg_npass = 10000;
const uint cntIterations__iterativeAlgs = 2;
#if(__Mi__algSet == 0) //! "hcaAndKMeans":
#if(1 == 1) //! udpated at (oekseth, 06. jul. 2017).
static const uint setOfAlgsToEvaluate__size = e_hpLysis_clusterAlg_undef;
static const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, //! an implemtnation which is inpsired by the poor implementaions found in the work of ("https://algorithmicthoughts.wordpress.com/2013/07/26/machine-learning-mini-batch-k-means/" and "https://github.com/siddharth-agrawal/Mini-Batch-K-Means")
  //! ------------------------------------------------------------
    e_hpLysis_clusterAlg_disjoint, //! ie, 'use disjoitnness of regions to idneitfy clusters': a fast appraoch which depends on masking of corrleation-valeus to be accurate.
    e_hpLysis_clusterAlg_disjoint_mclPostStrategy, //! which si a pemrtuation of "e_hpLysis_clusterAlg_disjoint_mclPostStrategy" where the MCL-strategy of merging 'non-core-vertices' (in a seprate post-sep after disjtoint-fores-tdineticiaotns) is applied/used (oekseth, 06. jul. 2017).
    e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, //! which is a permtuation of our "e_hpLysis_clusterAlg_disjoint" where 
    e_hpLysis_clusterAlg_disjoint_MCL, //! ie, the CML-algorithm (oesketh, 06. jul. 2017).
  //! --------------------
    e_hpLysis_clusterAlg_disjoint_kdTree,
    e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN,
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG, //! use disjoint-forest to find the k-clsuters and therafter apply AVG-cluster-appraoch.
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank, //! use disjoint-forest to find the k-clsuters and therafter apply rank-cluster-appraoch.
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid, //! use disjoint-forest to find the k-clsuters and therafter apply medoid-cluster-appraoch.
  //! --------------------
  e_hpLysis_clusterAlg_kCluster__SOM, //! ie, use Self-Organsiing Maps (SOMs) for clustering
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_kruskal_hca, //! where we construct a "Minimum Spanning Tree" (MST), and then 'assumes' that sorted order assiated to the constructed tree' describes a hioerarhccial cluster (ie, an itnerpetaiton often seen in research). 
  e_hpLysis_clusterAlg_kruskal_fixed, //! where we set a 'max-count' wrt. the clusters
  e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, //! wherew we use a user-specifeid (or defualt) Cluster Comparison Metric (CCM) to idneityf the min-max-clusters thresholds.
  //! ------------------------------------------------------------
  //! Note: [below] is sued as reference-frames wrt. clustering-acccuracy, eg, to comapre/elvuate the correctness/prediocnt-acucryac of clsutierng (oesketh, 06. jul. 2017).
    e_hpLysis_clusterAlg_random_best,
    e_hpLysis_clusterAlg_random_worst,
    e_hpLysis_clusterAlg_random_average
    };
#else
const uint setOfAlgsToEvaluate__size = 3 + 4;
const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  /*     ---    */
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
};
#endif
#elif(__Mi__algSet == 1) //! "kmeans":
 const uint setOfAlgsToEvaluate__size = 3;
const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
};
#elif(__Mi__algSet == 2) //! "HCA":
 const uint setOfAlgsToEvaluate__size = 4;
const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
};
#endif


//const uint sizeOf__nrows = 100; const uint sizeOf__ncols = 100;
  const bool config__isTo__useDummyDatasetForValidation = false; //! a aprameter used to reduce the exeuction-tiem when 'rpoking' assert(false) and memory-errors (oekseth, 06. feb. 2017).
  if(config__isTo__useDummyDatasetForValidation == true) {
    fprintf(stderr, "!!\t Uses a dummy data-set for validation, ie, where the reuslt of this call is pointless: do Not use this option if you are interested in clsuter-results (and Not dummy exzeuciton-time emasurements), an observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
 const uint setOfRandomNess_in_size = 4;
 const e_kt_randomGenerator_type_t setOfRandomNess_in[setOfRandomNess_in_size] = {
  e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random,
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss,
  e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss
 };
 const uint setOfRandomNess_out_size = 7;
 const e_kt_randomGenerator_type_t setOfRandomNess_out[setOfRandomNess_out_size] = {
  e_kt_randomGenerator_type_hyLysisDefault,
  e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
  e_kt_randomGenerator_type_posixRan__srand__time__modulo,
  e_kt_randomGenerator_type_binominal__incrementalInList,
  //! Different 'explicit' procuedrew chih makes use of 'partial randomness' to idneitfy clusters, ie, (a) 'randomness for a subset ov ertices' adn thereafter (b) 'set clsuter-embership to the closest cadnidate/random-assignec-sltuer-vertex':
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random,
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss,
  e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss
 };

 uint arrOf_simAlgs_size = 0; e_kt_correlationFunction_t *arrOf_simAlgs = NULL;

 if(isTo__evaluate__simMetric__MINE__Euclid) {
   arrOf_simAlgs_size = 2; arrOf_simAlgs = alloc_generic_type_1d_xmtMemset(e_kt_correlationFunction_t, arrOf_simAlgs, arrOf_simAlgs_size); assert(arrOf_simAlgs);
   arrOf_simAlgs[0] = e_kt_correlationFunction_groupOf_MINE_mic;
   arrOf_simAlgs[1] = e_kt_correlationFunction_groupOf_minkowski_euclid;
 } else { //! then include the 'compelte set' of simliarty-metrics in the pre-clsuter-step:
   arrOf_simAlgs_size = 0;
   for(uint i = 1; i < e_kt_correlationFunction_undef; i++) {   
     if(false == isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)i)) {
       arrOf_simAlgs_size++;
     }
   }
   arrOf_simAlgs = alloc_generic_type_1d_xmtMemset(e_kt_correlationFunction_t, arrOf_simAlgs, arrOf_simAlgs_size); assert(arrOf_simAlgs);
   uint pos = 0;
   for(uint i = 1; i < e_kt_correlationFunction_undef; i++) {   
     if(false == isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)i)) {
       assert(pos < arrOf_simAlgs_size);
       arrOf_simAlgs[pos] = (e_kt_correlationFunction_t)i;
       pos++;
     }
   }
   assert(pos == arrOf_simAlgs_size);
 }

  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  // printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! We are interested in a more performacne-demanind approach: 

  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
#ifndef __MiC__isTo_useTrans
  fileRead_config__syn.isTo_transposeMatrix = false;
#else
  fileRead_config__syn.isTo_transposeMatrix = true;
#endif
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
//s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();

#include __Mi__fileToHold__dataLoads

  //! ---------------------------------------
  //!
  //! The 'call':
  s_hp_evalHypothesis_algOnData_t obj_eval = init__s_hp_evalHypothesis_algOnData_t(mapOf_realLife, mapOf_realLife_size);
  obj_eval.isTo__testHCA_komb__inKmeans = isTo__testHCA_komb__inKmeans;
  obj_eval.arg_npass = arg_npass;
  obj_eval.sizeOf__nrows = sizeOf__nrows;
  obj_eval.sizeOf__ncols = sizeOf__ncols;
  obj_eval.cntIterations__iterativeAlgs = cntIterations__iterativeAlgs;
  obj_eval.config__isTo__useDummyDatasetForValidation = config__isTo__useDummyDatasetForValidation;
  obj_eval.setOfAlgsToEvaluate__size = setOfAlgsToEvaluate__size;
  obj_eval.setOfAlgsToEvaluate = setOfAlgsToEvaluate;
  obj_eval.setOfRandomNess_in_size = setOfRandomNess_in_size;
  obj_eval.setOfRandomNess_in = setOfRandomNess_in;
  obj_eval.setOfRandomNess_out_size = setOfRandomNess_out_size;
  obj_eval.setOfRandomNess_out = setOfRandomNess_out;
  obj_eval.arrOf_simAlgs_size = arrOf_simAlgs_size;
  obj_eval.arrOf_simAlgs = arrOf_simAlgs;
  //!
  //! Apply logics:
  const bool is_ok = apply__s_hp_evalHypothesis_algOnData_t(&obj_eval, stringOf_resultDir);
  assert(is_ok);

  //!
  //! @return
  free_generic_type_1d(arrOf_simAlgs);
#ifndef __M__calledInsideFunction
  return true;
#endif
}
#undef __Mi__algSet 
