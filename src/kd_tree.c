#include "kd_tree.h"
#include "correlation_sort.h"

/**

 **/
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
/**

 **/

/**
   @remarks overall observations:
   -- : .... most desciptiosn of "K-D trees" focus on a fixed feautre space, eg, "(x, y, z)" space coordinates. Therefore, ... 
   -- : 
   -- splitting: while the features are used for splitting, and vector comparison, only one of the feature-columsn are used when splitting a rwo-block into two pices.
   -- : 
   -- : 
   @remarks summary of axiallary functiosn:
   -- "init__s_kd_tree_node__iterNode(..)": stores proeprties of a given 'node': holds a 'box' seperately for each column. Povides pointers for both a 'left child' and a 'rigth child'.
   -- "kt_tree_kd__spread_in_coordinate(..)": finds the min--max score for a given column.
   // FIXME: 
   // FIXME: ... 
   // FIXME[concpetual] ... 
   // FIXME[concpetual] ... rewrite [""] ... into a DBSCAN-desipriotn ... 
   // FIXME[concpetual] ... rewrite [""] ... into a DBSCAN-desipriotn ... 
   // FIXME[concpetual] .... "curse in dimensionality" ... to microbrenchmakr the latter, we .... 
   // FIXME[concpetual] ... sytnehtci data ... for increased feature-siz ... [timining, CCM=[Rands]]x[DBSCAN, kdTree]x[local-configuraitons] .... provides informaitons/details of .... 
   // FIXME[concpetual] ... infer clusters from the row-ordering ... new function .. traverse the tree .... and apply an optioanl tree-based algorithm to thre 'ramining block' (ie, if any) 
   // FIXME[concpetual] ... how kd-tree and DBSCAN relates to HCA-max cluster algorithms ... thereoritcal and pratical eampels ... demosntrating their weaknesses and strenghts ... exprient with different synthetic 'hardcoded' datasets ... ... 
   // FIXME[concpetual] ... combine kd-tree and kmeans++ .... eg, to use kmeans++ for construct 'k' kd-trees ... and then ...??... ... hwere latter results are sued to evaluate ... 
   // FIXME[concpetual] ... 
   // FIXME[concpetual] ... why data-normalizaiton may not address issues in applcaiblity of Minkowski based metrics ... eg, wrt. "There is also evidence to suggest that unless the target vector is drawn from the same distribution as the k d-tree points, p rformance can be greatly worsened" ["https://www.ri.cmu.edu/pub_files/pub1/moore_andrew_1991_1/moore_andrew_1991_1.pdf"]
   // FIXME[concpetual] ... why should it be correct to split rows based on the simliarty to a single feature?
   // FIXME[timing]: meausre the exeuction-time of large feuatre-vecotrs, both for DBSCAN, and for "k-d trees" ... for increasing feautre -szeis .... investigating the asssertion by \cite{} .... 
   // FIXME: generate small data-ensambles .... use these as input to K-D trees  ... write out the K-D trees (wrt. [k-d-tree permutations]x[row-ordering, ...], and wrt. construciton of 'dense' trees (where ....)) ... vdalidate that the results correspnds to our exepcations (eg, of inaccurate splitting) ... write out a table of the memberships ... then apply for different data noramlziation strateies .... we expect ... 
   // FIXME: write a permtaution "kt_tree_kd__select_on_coordinate(..)" ... priting out the sequences of nubmers for different data-inputs.... using latter to explain the details, and wekaeeses, of the splitting-citiera in K-D trees .... and compare the results wiht the interpreaitons in "kt_tree_kd__select_on_coordinate_value(..)" ... 
   -- "kt_tree_kd__select_on_coordinate(..)": re-orders the rows of [l...u] to reflect the sorted order property of "column" ... Update mapping-table: ensure property [l..k][constant] < [k+1..u][constant] ... in the function we update the row-order, ie, to reflect the seperation-order wrt. closeness between/across a given feautre .... evaluate rows at a cosntant feature-vector. Re-order the rows to reflect the sorted property for the dimension .... during the internal iteration: re-order the two index-positions, ie, to reflect the score at the given feature-vector. ...  the result of each iteration: at this point:  "__scoreReMap(i, c) >= __score(t, c)"
   // FIXME[concpetual] ... if we have an accruate tree-partion, will the results be more accurate? .... eg, to use HCA-algorithms for the pariting ... thereby addressing issues wrt. the 'curse in dimensiohnaltiy'? .... 
   // FIXME[concpetual] ... 


   -- "kt_tree_kd__select_on_coordinate_value(..)": Order the vertices along the fixed/given diemsnion: center the vertices along the alpha threshold. From the/our impmetnaiton we observe how [l ... m][fixed] < alpha <= [m+1.....u].
   -- "(..)": 
   -- "__setSplitPoint(..)": copies the data derived from the reqcursive iteraiton strategy ... idea is to use the infrmation, dervied from the 'recursive child iteraiotn steps', to idneitfy/set the min--max boundaries. 
   -- "__MiF__updateResult(..)": 
   -- "__interSect_childrenAndNode(..)": 
   -- "(..)": 
   -- "build_tree_for_range__s_kd_tree__nonReqursive(..)": 
   -- "__build_tree__s_kd_tree(..)": 
   -- "search__kd_tree(..)":  ... the 'main-method' ... 
   -- "__MiF__startTimer(..)": used for debuggin:remember the exueion-time of different operaitons.
   -- "s_kd_tree_node__process_terminal_node_fixedball(..)": 
   -- "s_kd_tree_node__process_terminal_node(..)": 
   -- "__MiF__updateResult_search(..)": 
   -- "box_in_search_range(..)": 
   -- "(..)": 
   -- "(..)": 
   -- "(..)": 
   -- "(..)": 
   -- "(..)": 
   @remarks research-quesitons:
   -- "For large dimensions (20 is already large) do not expect this to run significantly faster than brute force. High-dimensional nearest-neighbor queries are a substantial open problem in computer science." ["https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html"]

// FIXME[eval]: cosndier implemetning: "radix sort" ["http://www.codercorner.com/RadixSortRevisited.htm"] (and then evalaute how 'this' may speed up the comptuatios of KD-trees wrt. ...); "Cover tree" ["https://github.com/DNCrane/Cover-Tree"] and "Annoy" ["https://github.com/spotify/annoy"]
 **/

// --- 
// ok:FIXME[code]: ... "kd_tree" ... provide wrapepr-lgocis to our "k-means-result-object" ... instead re-write the object .... add a enw config-struct ... then fix compiation-errros ... 
// ok:FIXME[code]: ... "kd_tree" ... db-scan-permtatuion .... pre-comptue the kd-tree ... <-- dropped, ie, no need for the latter.
// ok:FIXME[code]: ... add support for kd-tree .... update our "hpLysis_api" with ... new enums (and related support) 
// --- 
// ok:FIXME[code]: ... add support for kd-tree .... update our "hpLysis_api" with ... 'basic' call-logics ... 
// ok:FIXME[code]: ... add support for kd-tree .... update our "hpLysis_api" with ... CCM-iterative wrt. teh "nn" rank-rpoerpty.
// ok:FIXME[tut+eval]: ... new tut ... for db-scan-kd-tree .... for all of the different kd-tree-combinations .... "tut_kd_0_cluster.c" ... results ... kd-tree and CCM ... fixe memory-leaks .... works!
// ok:assert(false); // ok:FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ...  add support for different ranoiaotn-pre-steps.
// ok:FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. a function for evlauation of a givne data-set
// ok:FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. colleciton of files ... and min-max-extrems .... andd result-export .... 
// ok:FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. from different file-collections .... 
// ok:FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. update our "kt_graphAlgorithms_main.cxx"
// ok:FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. GEO-data-set of .....??...

// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. get-compiling .... 
// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. get-compiling .... 
// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. get-compiling .... 

// FIXME[CCM]: ...  add supprot for a 'sparse-distance-amtrix' .... then evaluate of a pre-comptued KD-tree-appraoch usigna  rank-trheshodl may correclty dientify/classify the latter ... 
// FIXME[CCM]: ... givne the accurayc/correctness of SSE-CCM ... consider to explore/use the 'sum-of-direct-distances' to/for each vertex .... explore how KD-tree with thresholds may be used for the latter .... ie, to 'inernally sum' the dsitances assited/for each cluster (during the DB-SCAN-phase) ... elvuate for [sum-total, sum-total/|vertices-in-cluster|] .... <--- todo[ccm::conceptual]: is it sufficent to only compute "[vertex(i) in cluster(k)][centroid(cluster(k))]" ... ie, as the latter will signcalty reudce exeuciotn-time and memory-requirement <-- todo[artilce::ccm]: update our CMC-artilce wrt. the latte r.... ie, how the perforamcen-voerhead wrt. simalrity-emtrics isgncalty prevents feasibel intgrated use of CCM-evaluation-appraoches (eg, wrt. KD-tree-consisnteyc evlauation through SSE-CCM and Silhouette-CCM).
// FIXME[CCM]: ... 
// FIXME[CCM]: ... 


// FIXME[tut+eval::eval]: ...  validate for [ªbove] samll data-set using valgirnd ... fxiing memory-leaks/errors ... 
// FIXME[tut+eval::eval]: ...  validate for [ªbove] samll data-set using valgirnd ... 
// FIXME[tut+eval::eval]: ...  validate for [ªbove] samll data-set using valgirnd ... 

// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. 
// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. 


// FIXME[artilce::"tut_kd_1_cluster_multiple_simMetrics.c"]: ... describe our mesurement-procedure .... provide detials of how we evaluate kd-trees .... paramter-sapce in our \hpFile{tut_kd_1_cluster_multiple_simMetrics.c} .... 
// FIXME[artilce::"tut_kd_1_cluster_multiple_simMetrics.c"]: ... 
// FIXME[artilce::"tut_kd_1_cluster_multiple_simMetrics.c"]: ... 
// -------- 
// FIXME[tut+eval]: ... new tut ... explore the different kd-tree-db-scan-configuraitons .... explore for all of the combinations of [cluster-alg][sim-metric] for (a) a sample-data-set generate  through oru \hpFile{} and (b) the IRIS data-set ....
// FIXME[tut+eval::eval]: ...  comptue seperate matrices for ... 
// FIXME[artilce]: update our aritlce wrt. these reuslts ... 
// FIXME[artilce]: ... \hpFile{./x_measures tut_kd_1_cluster_multiple_simMetrics} .... 
// FIXME[artilce]: ... \hpFile{config_tut_hypothesis_whyResultDiffers_1__config.c} ... where we call our \hpFile{tut_hypothesis_whyResultDiffers_1.c} ... to evalaute for the use-cases of [ 'all-metrics', ['Euclid', 'MINE'] ] x [clsuterAlgs=[all, [medoid, means, rank], [hca-max, hca-single, hca-average, hca-centroid]]] ... 
// FIXME[artilce]: ... \hpFile{tut_norm_3_cluster_kd_dbScan_80DataSets.c} ... normalizaiton-effects ..... of interest in aritlces wrt. kd-tree, db-scan, data-normalizaiotns ..... 
// FIXME[artilce]: ... 
// FIXME[artilce]: ... 
// FIXME[artilce]: ... 
// --- 
// ------------------------------------------------------------------------------   extend "tut_kd_1_cluster_multiple_simMetrics.c" with hard-coded ysntitc data-sets-ids.
// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. 
// FIXME[tut+eval]: ... new tut ... "tut_kd_1_cluster_multiple_simMetrics.c" .. 


// ------------------------------------------------------------------------------  
// FIXME[code::"hpLysis_api"]: ... add new option to copmtue mulipel runs for k-means ... then choose/select either the worst-scoring or best-scroing eneit (based on the CCM stored after the clustering) ..... and add an option to support parpalle-evlauatiosn wrt. latter .... 
// FIXME[tut+eval]: ... new tut ... "tut_kd_3_cluster_multiple_simMetrics_clusterAlgs.c" ..  comptue for [alg-id=[kd-tree, SLINK, k-means(worst), random(worst), ][sim-id]=ccm ... in the 'main-method' allow/suppor thte first arugment to be the file to comptue for .... 
// FIXME[artilce] ... "tut_kd_3_cluster_multiple_simMetrics_clusterAlgs.c" ... visualize silmiarty .... CCM-score-difference .... for 6 different data-sets ... (1) 6 synetic ... (2) therafter 6 real-life .... (3) and then a figure-ensamble which use/choose the key-traits we observe .... (4) implciaiotn/effect of different matrix-CCMs ..... (5) implicaiton/effect of different set-based CCMs ... (6) a summary-figure wrt. the latter, ie, a fgirue which stress/higlight the need to accurately suppoort/provide .... (7) synetic-data-sets with increasing feature-size where goald is to idnetify the threshold-need for a brute/full simalrity-matrix-comtpatuison, and wrt. how acucrate simalrity-emtrics may coprrectly address the latter issue (where we repateat above evlauation-rpcoedure demostrnating how the altter obsrion hold) .... .... (8) GEO-data-sets (where we repateat above evlauation-rpcoedure demostrnating how the altter obsrion hold), (9) GEO-data-sets (where we use random-masking to evlaute the sensisity to inferioatm-loss wrt. kd-trees and db-scan-brute)  .... ... we are interested in capturing/describing the prediction-influence of .... 
// FIXME[artilce] ... update our dBscan-abstract with the \hpFile{tut_kd_3_cluster_multiple_simMetrics_clusterAlgs.c}, ie, where latter may be used to evlauate a users own data-set wrt. implcion/effect of DB-scan-kd-tree-permtuations 
// FIXME[artilce] ... in artilce ......??....    .... \hpFile{tut_kd_1_cluster_multiple_simMetrics.c} .... we evaluate the implcaiton-effect of different CCMs and simlairty-metics for a given data-set. We comptue for both matrix-based CCMs and gold-based CCMs. We meausre the implciaton-evaluate three synetic and three real-life data-sets. For the real-life data-sets we for gold-CCM-comparison use MINE+HCA as a reference-frame. To evaluate correctness and quality-accurayc of the altter we for the synteitc data-sets combiend MINE+HCA with knowledge of sytentic seperation between the prediction-results. .... Our meausremnt-evlauion may be sued to gain isnisghts into how ......??.... 
// FIXME[artilce] ... \hpFile{} .... 
// FIXME[artilce] ... \hpFile{} .... 
// ----------------------------------------: tut-eval .... "date; ./x_measures tut_hypothesis_whyResultDiffers_1_all 1>out_2.txt 2>err_2.txt; date"
// FIXME[tut+eval]: ... re-comptue for the small data-set ... construct tikz-legends for each 'case' ... 
// FIXME[tut+eval]: ... constuct latex-tables ... min-max ... for the different nroamziaotn-data-types .... 
// FIXME[tut+eval]: ... 
// ----- 
// FIXME[code::bug]: .... "hp_evalHypothesis_algOnData.c" ... validate correcntess wrt. the 'odd hig sim-metric-numbers' givne wr.t the max-sim-emtric-id-score in the summary-result.-ifles
// FIXME[code::eval]: ....  "hp_evalHypothesis_algOnData.c" ... inlcude the 'extnesive' k-means-cofnigruation-evaluation .... update latter .... select/idneitfy the min-max-results wrt. infleucne of random perburaitons ..... 
// FIXME[code::eval]: ....  "hp_evalHypothesis_algOnData.c" ... 
// ----- 
// FIXME[artilce] ... \hpFile{} .... describe the different result-data-caterogies which are generated .... what they say/infomr .... 
// FIXME[artilce] ... \hpFile{} .... 
// FIXME[tut+eval]: ... for a givne data-set-ensamble .... the different perspecitves which are provided ... 
// FIXME[tut+eval]: ... discussion of summary-result-views ... how these provide an ingith into differnet perpsecitves 
// FIXME[tut+eval]: ... data-nroamlizaiton .... inlfuence of latter .... 
// FIXME[tut+eval]: ... 
// FIXME[tut+eval]: ... 
// FIXME[tut+eval]: ... 
// ----- 
// FIXME[artilce] ... \hpFile{} .... visualize results .... 
// FIXME[artilce] ... \hpFile{} .... inlcude/ocnsutrct latex-tables for both data-normalization and raw data-scores .... 
// ----- 
// FIXME[artilce] ... \hpFile{} .... 
// FIXME[artilce] ... \hpFile{} .... 


// ----------------------------------------: tut-eval .... 
// FIXME[tut+eval]: ... "tut_ccm_15_useCase_singleMatrix_differentPatterns.c" ... use predionct-results to ......??.... 
// FIXME[tut+eval]: ... 


// ----------------------------------------:  kd-tree-predicton-capabilities wrt. similarity-metrics
// FIXME[conceptual]: ... scaffold a tut-test-based appraoch .... implicaiton/difference/effects of kd-tree-applicaitons .... 
// FIXME[conceptual]: ... scaffold 'usage' wrt. the suggested/described test-setups wrt. perofmrance-elvauaitons ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code::"hpLysis_api"]: ... 
// ----
// FIXME[tut+eval]: ... IRIS .... 
// FIXME[tut+eval]: ... 
// FIXME[tut+eval]: ... 
// ---- 
// FIXME[code::tut::"tut_eval_3_kdTree.c"]: ... use lgocis from our \hpFile{tut_kd_3_cluster_multiple_simMetrics_clusterAlgs.c} and our \hpFile{tut_kd_1_cluster_multiple_simMetrics.c} ... get tut to work/compile .... for  ... intogreat inot our "x_measure" ... 
// FIXME[code::dbScan]: ... paramter-identiciaotns ... enumeration wrt. ooptions ... 
// FIXME[tut+eval::"dbScan"::"tut_eval_3_kdTree.c"]: ... function ... a data-set "s_kt_matrix_t obj_matrixInput" .... DB-SCAN...paramter-space=[node-thresholds, rank-value-thresholds] .... and CCM-config-thresholds (ie, for an iterative-appraoch) .... seperately compute for [algorithm-id][simliarty-metric] = [CCM(matrix), permtaution-id] ... where we evaluate for different kd-tree-strategies and different DB-SCAN-alterntives/appraoches .... construct seperate visualizioants for fig1=[[data1, data2, data2] x [ccm1, ccm2]],fig2=[[data1, data2, data2] x [score, permutation-id]], fig3=[ccm1, ccm2, ccm3, ccm4, ccm5, ccm6], fig4=[[data1, data2, data2] x [raw, pre-normalization-step]], fig5=[syntetic data-sets with increasing feature-count .... ie, where each fig has a 'fixed' feature-count], fig6=[GEO-data-sets], fig=[], fig=[] ... fig7=[ccm-id-for-fixed-alg][simliarty-metric]=ccm-score (where we compare (among others) the rpediocnt-difference wrt. using different CCMs 'inside the kd-tree-algorithm' (while a fized CCM outside the evlaution-space), ie, a figure which focus on the relationship between test-appraoch-best (eg, wrt. convergnce-meausre-emtrics) versus pclsuter-algorithm-accurayc, a result which is sued to enhance/demosntrte the view/perspective of .....??... ) ... 
// FIXME[tut+eval::"dbScan"]: ... write a min-max-permtution wrt. [above] ... ie, use muliple data-sets ... and "hp_evalHypothesis_algOnData.c" as a template .... 
// FIXME[tut+eval::"dbScan"]: ... add support for gold-CCMs ... use MINE+HCA+CCM to idneitfy the best-fit gold-standard-cluster ... then compare wrt. the latter .... 
// FIXME[tut+eval::"dbScan"]: ... add-for .... data-nraomziaotns (ie, using the data-enum examplifed in our \hpFile{hp_evalHypothesis_algOnData}) ... 
// FIXME[tut+eval::"dbScan"]: ... evalaute acucrayc of differnet kd-tree-valuation-appraoches .... add-for .... [similarity-metircs]x[gold-ccm] (ie, instead of usign clsutering directly, where we use both real-lfie data-sets and approx. 180 different sytneitc data-sets, ie, where clsutering is apriory known) .... 
// FIXME[tut+eval::"dbScan"]: ... add-for .... different 'extreme' sytneitc data-sets .... ie, where gold-case ie 'explctily' known/defined/stated.
// FIXME[tut+eval::"dbScan"]: ... add-for .... 
// FIXME[tut+eval::"dbScan"]: ... 
// FIXME[article]: ... kd-tree ... $$ ... in our \hpFile{tut_eval_3_kdTree.c} .... we explore DB-scan permtautions in/through our sytnetic data-sets .... use different internal CCMs ... different filter-criterias .... enumerate alternatives   
// FIXME[article]: ... kd-tree ... $$ ... idneitfy use-cases where DB-SCAN and kd-tree are able to correctly idneitfy clusters ...  explore all of the/our sytnetic-data-set-cases.
// FIXME[code::dbScan]: ... 
// --- 
// FIXME[article]: ... kd-tree ... $$ ... demonstrate that the choise of simlairty-metrics has a profund/signcat inlfuence on cluster-predioncts .... use matrix-based CCMs ... 
// FIXME[article]: ... kd-tree ... CCM-gold ... $[alg][sim-id] = [[ARI, Rand(1), Rand(2)](result x [kdTree, SLINK]), (result x hard-codedGold(al1, alt2))]$ (\hpFile{}) .... 
// FIXME[article]: ... kd-tree ... $$ ... 
// FIXME[article]: ... kd-tree ... $$ ... 
// FIXME[article]: ... kd-tree ... 
// --- 
// FIXME[code]: ... get tut to work/compile .... for CCM ... 
// FIXME[code::k-means]: ... add support for CCM-configuraitons wrt. cluster-convergence .... 
// FIXME[code::k-means]: ... add enumeraitons wrt. CCM-configruaitons .... 
// FIXME[code]: ... get tut to work/compile .... 
// FIXME[article::CCM]: ... CCM-library ... k-means .... enumerate different CCM-types ... indeintfy the best-worst-case wrt. CCM-scores .... for a given 'base' CCM .... 
// --- 
// FIXME[code::k-means]: ... 
// FIXME[code::k-means]: ... idneitfy an enumeraiton-appraoch wrt. the different k-means-strategies ... ie, a foor-loop where a 'prev-counter' is used to idneitfy the next cofnigruation-step .... when latter is met the iteraiotn stops ...  
// FIXME[code]: ... get tut to work/compile .... 
// FIXME[tut+eval::"k-means"]: ... k-means ... $$ ... new tut ... use [above] enumeraiton-strategy to explore k-means-permtuations .... use mulipel iteraionts ... select min-max .... evaluate init=[[k-means++], rand(..), [HCA]] .... 
// FIXME[article::"k-means"]: ... evaluate the implication of different k-means-strategies .... for cases of ...??.... we observe how a $d(C_i, C_k) = \sum d(v_x \in C_i, c(C_k))$ outperforms the more costly $d(C_i, C_k) = \sum\sum d(v_x \in C_i, v_y \in C_k)$ ... broad applicaiton (of oru appraoch) is seen wrt. ......??....


// ---------------------------------------- DB-scan:: dense-simliarty-matrix-comptautions
// FIXME[conceptual]: ... 
// FIXME[conceptual]: ... 
// FIXME[conceptual]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// ---- 
// FIXME[article]: ... 
// FIXME[article]: ... 
// ----------------------------------------

// ----------- QUERY(...)::performance-evaluation
// FIXME[conceptual]: ... read soruce-code of "sci-kit" ... why latter outperofrms our appraoch .... 
// FIXME[conceptual]: ...
// FIXME[conceptual]: ... compile in optmized mode .... try fiding the reasosn for performance-bottlenecks ... 
// FIXME[code]: ... 
// --- 



// FIXME[tut+perf]: perf-evalaute/compare brute-force-permtuations .... 
// FIXME[tut+perf]: ... 
// FIXME[tut+perf]: evlauat the perf-voerherad for samll data-sets wrt. the 'local' cofngirautions of eacch call ..... 
// --- 
// FIXME[tut+perf]: ... write "tut_kd_1_performanceEvaluate.c" ... add different search-calls
// FIXME[code+tut+perf]: ... evaluate different permtations wrt. .... 
// FIXME[code+tut+perf]: ... evaluate different permtations wrt. .... 
// FIXME[code+tut+perf]: ... new eval-function .... result-difference for differnt options .... for different clsuter-cofnig-calls .... eg, wrt. the "rearrage" option ....
// FIXME[code+tut+perf]: ... 
// FIXME[code]: ...  
// FIXME[code]: ... 
// --- 
// FIXME[code]: ... get kd-tree-building to compelte .... evlauate/try/expplreo different performacne-tuning-strategies .... new test-funciotn "tut__altLAng_sciPy_kd.py"
// --- 
// FIXME[tut+perf]: ... compare with sci-kit-learn-time to build a kd-tree .... compare witht the "query(..)" API .... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[tut+perf]: ... 
// FIXME[code]: ... add-config .... 
// FIXME[code]: ... 
// FIXME[tut+perf]: ... 
// FIXME[tut+perf]: ... 
// FIXME[tut+perf]: ... test different exeuciton-time-differences wrt. different evlaution-strategies ..... 
// FIXME[tut+perf]: ... different ballpoint-sizes .... how latter influecne results .... generate a result-API-chart-amtrix where we ......??... 
// FIXME[tut+perf]: ... query(...) time-diff .... naive-brute-force VS fast-computaiton .... for [naive, 1-many, many-many] ... use latter to construct a time-graph ... to be sued in our ......??....


// ----------- DB-SCAN
// FIXME[conceptual]: ... 
// FIXME[conceptual]: ... 
// FIXME[code]: ... 
// FIXME[tut+perf]: ... compare with sci-kit-learn-time to build a kd-tree .... compare witht the "dbScan(..)" API .... 
// FIXME[tut+perf]: ... the brute-force-strategy with our opmtized DB-scan-eval-straty .... use latter perf-difference to argue for .......??....
// FIXME[tut+perf]: ...
// FIXME[tut+perf]: ...
// -----------
// FIXME[conceptual]: ... function-calls ... move to seperate files ... test perf-implciaiotn-effect of 'not callign fucntiosn' .... 
// FIXME[conceptual]: ... 
// FIXME[tut+perf]: ... "kt_tree_kd__spread_in_coordinate(...)" SSE .... optmzied mode .... different test-settings .... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// -------------------------------------
// FIXME[conceptual]: ... 
// FIXME[conceptual]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// -------------------------------------
// FIXME[conceptual]: ... 
// FIXME[conceptual]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// -------------------------------------
// FIXME[article]: .... time of kdTree(iterative) VS kdTree(reqursive) is approx. the same, ie, isnigciant/inivsible time-improvement-benefit ... 
// FIXME[article]: .... comparison of hpFile{tut__altLAng_sciPy_kd.py} VS \hpFile{tut_kd_1_performanceEvaluate.c} .... for which we observe that our appraoch goes at least 3x faster (when compared to C-debug-mode) while .....??... in C-opmtized-compilation-mode .... 
// FIXME[article]: .... 
// -------------------------------------
// FIXME[conceptaul]: ... 
// FIXME[conceptaul]: ... how kd-tree manages to reduce searhc-time ... (when comapred to a brute-force-aprpaoch) .... 
// FIXME[conceptaul]: ... 
// FIXME[conceptaul]: ... why is the ball-size 'glued' with the heap-strucutre .... why ia a 'heap' used in the kd-tree-traversal? .... 
// FIXME[conceptaul]: ... 
// FIXME[conceptaul]: ... a new data-struct for {index, score} .... idneityf the correct/proper strucutre to be sued wrt. storing the results .... consider using the different sturrues ... then merge at ......??...
// FIXME[code]: ... a new data-struct for {index, score} .... 
// FIXME[code]: ... a heapify-routine for the latter ... where the 'score' is used as the sort-property .... and accessor-routien is to .......??....
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// -----------
// FIXME[code]: ... include the search-rotueins from our "tmp.c" .... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// -----------
// FIXME[conceptual]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// -----------
// FIXME[code]: ... include DB-scan routines .... scafofld-routine ... 
// FIXME[code]: ... include DB-scan routines .... include an latnetive-appraoch ... where we handle/taget the non-symmetirc option ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// -----------
// FIXME[code]: ... integrate the different kd-tree-permtuatiosn in the R-package .... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// -----------
// FIXME[code]: ... clsuter-permuation .... remember the vertex-divison-paths wrt. the "MiF__index" .... then use the reuslt to cosntuct k-means-fixed-clsuters through ......??... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// ----------- 
// FIXME[code]: ... 
// FIXME[code]: ... 
// --
// FIXME[code]: ... get compiling .... 
// -----------
// FIXME[code]: ... get compiling .... 
// FIXME[code+tut]: ... update our "kt_list" wrt. a min-heapify-structure ... then write a new tut which ......??... 
// FIXME[code+tut+perf]: ... impment a binary-heap .... into our hpLysis 
// FIXME[concpetaul]: evlauate/idneitfy/desicrb the external kd-tree-impemtantion by ... the R-c++-DBN-scan-impmetnation ... 
// FIXME[concpetaul]: evlauate/idneitfy/desicrb the external kd-tree-impemtantion by ... sci-kit-learn ... 
// FIXME[code+tut+perf]: ... comapre our non-requsirve-while-kd-tree-eval-strategy .... with the resuve-alt ... then with sci-cit-learn
// FIXME[code+tut+perf]: ... simlairty-emtircs .... use differnet simarlity-emtircs 'during the cconstruciton' versus 'in the kd-tree-searhc' ... 
// 
// 
// ------------------------------------- 
// FIXME[pre]: ... idneityf cases where our kd-tree-impemtantion may sigcnatly out-perofmr estlibhed aprpaoches .... eg, wrt. large feuatre-spaces .... eg, wrt. time-cost of isimlairty-emtric-comtpatuions druign inseriton and searhcing, ... where the cost of heap-access is found/seen wrt. ...??...
// FIXME[pre]: ... explain why and how the "heap" is used in the kd-tree ... b oth wrt. concpetaul undersitnadfg ... and inflcue/relavance on exueciont-time-cost ... 
// FIXME[pre]: ... argue/idneityf ... why our appraoch ... sigicnalty otu-erpforms elitbhed kd-tree-impeltmatnions .... 
// FIXME[pre]: ... 
// FIXME[pre]: ... 
// FIXME[tut+eval]: ... 
// FIXME[tut+eval]: ... 
// FIXME[code]: ... get updaed code-chunk to compile .... 
// FIXME[code]: ... configuraiotns .... add enums .... support fidfferent interptions/strateiges wrt. kd-tree-comptatuions .... eg, use of SSE, interanl/local distance-metric versus hp_distance(..), ..... 
// FIXME[code]: ... 
// 
// 
// ------------------------------------- DB-SCAN(kd-tree): ....
// FIXME[concpetual]: ... understand how the kd-tree is used in db-scan .... and then suggest broader applciaotns wrt. kd-tree .... 
// FIXME[article-text]: ... describe differences between our dense-amtrix-appraoch versus the kd-tree-db-scan-tieraiton-phase .... cocneptaulise/gneleirze the latter .... use the cocnpetaulised ufndernstaindg to idneity/describe broader appraoches wrt. kd-trees in clsuter-analsysis ..... eg, wrt. k-means and Kruskal .... 
// FIXME[conceptaul]: ... evaluate/compare how a kd-tree-appraoch may be used as an alterntive to HCA .... ie, to use the hierarchical kd-tree-feature-organisation as the/a clustierng .... eg, by first traqnspsoign the inptu-matrix .... for HCA-clsutering an approach is to use the 'm' vertex in '[l....m .... u]' as a split-center-node ... where the feature-comparison-logics is hanlded/addressed through ......??.....
// FIXME[related-work]: ... inveiste how/hwen/cases where kd-tree- is used as a stand-alone cluster-appraoch/procuedre .... and try relaing kd-tree to PCA-aprpaoches wrt. dimesnionality-reduciton .... and 'non-descirptive feautre-diemsnion-deulge' .... 
// FIXME[article-text]: ... 
// FIXME[article-text]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[pre]: ... 
// ------------------------------------- hp_distance(...)
// FIXME[pre]: ... 
// FIXME[concpetual]: ... feautre-vector-suppoirt .... "dis_from_bnd(...)" ... ie, to use two matrix-cmps as 'second-input-matrix' ... where a borader (Applciaotn wrt. the latter) cosnerns how the latter provides uspprot for ........??... 
// FIXME[concpetual]: ... figure out how [ªbove] may be supproted without facing the challenges of high exeuciotn-time wrt. the compaition-operation .... eg, by 'only' supproiting thios for a 1d-feature-comparisn ... 
// FIXME[code]: ... try dsingina pre-stpe where we ... comptue min-max using our "kt_math_matrix.h" as a template .... supproting: !mask and T_FLOAT_MAX masks ... here latter appraoxh will at max. introduce a 2x perf-overhead ... ie, shoudl be acceptable ... 
// FIXME[concpetual]: ... 
// FIXME[code::sim]: ... first update our tiling ... then our user-itnerfaces .... 
// FIXME[code::tut]: ... write simplictic use-cases wrt. this ... 
// FIXME[code::sim]: ... add simliar support for sparse-matrices ... 
// FIXME[code::sim]: ... 
// FIXME[code]: ... prvoide/add supprot for missing-values .... in our appraoch ... eg, wrt. comparison of min-max-feautre-scores .... 
// FIXME[pre]: ... write a C/C++ verios of the heap-strucutre used in 'this appraoch' ... 
// FIXME[pre]: ... write an iterative verison of the requcitve data-seahrc-procuedre ... validate that the løatter sigicnalty irmpvoed kd-tree-perofmrance ... 
// FIXME[pre]: ... udpate our "hp_distance(..)" ... add supprot for the distnace-comtpatuion-rotueisn wrt. tree-budling and tree-searhcing
// 
// 
// ------------------------------------- saprse-data 
// 
// FIXME[cocneptaul]: idneitfy/describe how sparse-data-structres may be supported through this/our evlauaiton-strategy ... eg, bby first transposing the input-matirx ..... 
// FIXME[article-text]: ... 
// FIXME[article-text]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[code]: ... 
// FIXME[pre]: ... 
// ------------------------------------- 
// 
// 
// ------------------------------------- perforamnce-evlauation 
// FIXME[pre]: ... time-cost of ... [heap-access, distnace-comptautions, ...] .... where latter is used to evlauate ......??....
// FIXME[code]: ... log-functioantliy .... captrue perofmrance-cost of differetn evalaution-parts .... and wrt. different opmzaiton-strategies .... eg, the use/preference of SSE .... 
// FIXME[tut+eval]: ... time-graph demostnrting rowth in exueciont-time ... for kd-tree .... to describe/detect infeixliby-cases wrt. kd-tree ... 
// FIXME[tut+eval]: ... sytneitc-eval .... otpziaotn-time-point where the cost of large-feautre-voector-cotmatuiosn signcalty delays kd-tree-exueciotn-tiem .... 
// FIXME[tut+eval]: ... write a Kruskal-permtuation .... where we use the trhesholds from MCL and DB-SCAN to cosntruct a sparse data-set .... then call/load our kruskal-algorithm .... evalaute the accurayc wrt. the latter .... where we 'divide' by the CCM-clsuter-rpedion-acucrayc seperately/respecitvely from MCL and DB-SCAN .... where results are used to .......??.... 
// FIXME[pre]: ... time-cost of ... 
// FIXME[pre]: ... 
  // FIXME[tut+eval]: .... test/elvuaate a db-scan-permtaution .... a direct-disjtoitn-forest-procuedre .... explroe different value-based thresholds ... reuqireu clsuter-coutn > threshold .... assign vertices to not-yet-assinged cluster susign simalrity-metircs .... (ie, where we drop the intermeidate-simalrity-emtric-aprpaoch .... ie, where latter is/rpeserntes/captures hte differencE) ..... ie, use our proceudre as-is .... though add a new min-cklsuter-count varialbe to our strucutre + evlauaiton-appraoch .... 
  // FIXME[tut+eval]: .... 
  // FIXME[tut+eval]: .... a matirx-comparison .... use the "nn_numberOfNodes" ... compare with an explcit matirx .... ie, where we comptue the siimlairty of/between teh matrices into a scalar score through .....??.... .... then evaluate/appply the latter for real-life data-sets .... where 'this' result is used to .......??....
  // FIXME[tut+eval]: .... test/elvuaate a db-scan-permtaution .... 
  // FIXME[tut+eval]: .... use the "MiF__index" as a sort-order-option to Kruskal (ie, where kd-tree is used to wieght/order the vertices based on their centlraity) .... then seperatley explroe different pemrtuaions .... eg, to use the latter as a PCA-appraoch wrt. vertex-ordering ..... 
  // FIXME[tut+eval]: .... evalaute centrlaity ... where we asusme the "MiF__index" represents/captures one type of centlriaty .... compare with other/altnetitve centlriaty-meausres .... where a gold-standard-seleciotn-case is to use ........??.....
// -------------------------------------
// FIXME[pre]: ... 
// FIXME[pre]: ... 
// -------------------------------------

// FIXME: ocmpelte tinegration from the "home/klatremus/poset_src/externalLibs/benchmarkCode_fastParallel/dbscan-v1.0.0/parallel_multicore/kdtree2.cpp" (and the assited dbscan.cpp code).
// FIXME: update oru ariltce .... argue for the correcntess/acpteiblity of using teh 'benchmark-cod'e by \cite{} as a temaptle .... while Not usign the altter ion thre rela-lfie exeuciton-time-code .... ie, for which the latter code is Not incldued wrt.t he public-lincecne .... 

typedef struct __kd_tree__bm {
  uint cnt_loops;
  t_float time_split;
  t_float time_coord;
  t_float time_intersect;
} __kd_tree__bm_t;
typedef struct __kd_tree__bm__search {
  uint cnt_calls;
  uint cnt_loops;
  t_float time_terminal_node;
} __kd_tree__bm__search_t;


__kd_tree__bm_t __kdGlobal_timinng;
static void initGlobal____kd_tree__bm_t() {
 __kd_tree__bm_t self;
  self.cnt_loops = 0;
  self.time_split = 0;
  self.time_coord = 0;
  self.time_intersect = 0;
  //! ----------------
  __kdGlobal_timinng = self;
}
__kd_tree__bm__search_t __kdGlobal_timinng_search;
static void initGlobal____kd_tree__bm__search_t() {
 __kd_tree__bm__search_t self;
  self.cnt_calls = 0;
  self.cnt_loops = 0;
  self.time_terminal_node = 0;
  //! ----------------
  __kdGlobal_timinng_search = self;
}

#ifndef NDEBUG
#define __MiF__startTimer() ({  start_time_measurement(); })
#define __MiF__updateResult(var_update) ({ float result = end_time_measurement(NULL, 0); if( (result != FLT_MAX) ) {__kdGlobal_timinng.var_update += result;} } )
#define __MiF__startTimer_search() ({  start_time_measurement(); })
#define __MiF__updateResult_search(var_update) ({ float result = end_time_measurement(NULL, 0); if( (result != FLT_MAX) ) {__kdGlobal_timinng_search.var_update += result;} } )
#else
#define __MiF__startTimer() ({ ;})
#define __MiF__updateResult(var_update) ({ ;})
#define __MiF__startTimer_search() ({ ;})
#define __MiF__updateResult_search(var_update) ({ ;})
#endif


#define MiF__index self->map_index.list //! which is sued to abbreviate the access-code to our "s_kd_tree_t" strucutre (oekseth, 06. jul. 2017).
//#define MiF__box box.list
#define MiF__matrix self->mat_input->matrix
//!----------------------------------
#define MiC__useTransp 1
// **************************
#if(MiC__useTransp == 0)
//! Get matrix-score:
#define __score(row_id, col_id) ({self->mat_input->matrix[row_id][col_id];})
#define __scoreReMap(row_id_, col_id) ({__score(MiF__index[row_id_], col_id);})
#else
//! Get matrix-score:
#define __score(row_id, col_id) ({self->mat_inputTransp.matrix[col_id][row_id];}) //! ie, then 'swap' the rows and columns
#define __scoreReMap(row_id_, col_id) ({__score(MiF__index[row_id_], col_id);})
#endif


#define MF__isEmpty__s_kd_tree_node_t(obj) obj.isSet
//! @return a paritally itnated veirson of the object:
#define MF__toEmpty__s_kd_tree_node() ({NULL;})
//#define MF__toEmpty__s_kd_tree_node() ({s_kd_tree_node_t self; self.isSet = false; self;})
#define MF__toEmpty__s_kd_tree_node_notNull() ({s_kd_tree_node_t self; self.isSet = false; self;})
//! @return an itnitaed veriosn of the object.
#define MF__init__s_kd_tree_node(dim) ({s_kd_tree_node_t *self = (s_kd_tree_node_t*)malloc(sizeof(s_kd_tree_node_t)); \
  self->isSet = true;						     \
  self->cut_dim = dim;						     \
  self->cut_val = 0;						     \
  self->l = 0;						     \
  self->u = 0;						     \
  self->box = init__s_kt_list_1d_fPair_t(dim); /*setToEmpty__s_kt_list_1d_fPair_t();*/ \
  self->left = NULL; self->right = NULL; \
  /*self->left = MF__toEmpty__s_kd_tree_node();*/			\
  /*self->right = MF__toEmpty__s_kd_tree_node();	*/		\
  self;})

//! De-allcotes the s_kd_tree_node_t object
static void free__s_kd_tree_node_t(s_kd_tree_node_t *self) {
  assert(self);
  if(self->isSet) {
    free__s_kt_list_1d_fPair_t(&(self->box));
    if(self->left) {
      free__s_kd_tree_node_t((self->left));
      free(self->left);
    }
    if(self->right) {
      free__s_kd_tree_node_t((self->right));
      free(self->right);
    }
  }
  //! Mark as unset: 
  //*self = MF__toEmpty__s_kd_tree_node();
}

// **************************************
// **************************************
// **************************************
// **************************************
// **************************************
// **************************************


/**
   @brief sets the [min, max] score-speraiton between [l, u].
   @param <self> is the object holding the data.
   @param <c> is the 'fixed' column used to compare the rows.
   @param <l> is the first row-index to evaluate.
   @param <u> is the last row index to evalaute.
   @param <scalar_min> is the min-score identifed.
   @param <scalar_max> is the max-score identifed.
   @remarks identifies the spread in coordinates, an 'observaiton' which is used to ....??..
   @remarks used for the applicaitons of:
   -- "__setSplitPoint(..)": max-spread: Update the mapping-table and idneitfy a max-spread wrt. the scores: for each row-segment box "l...u" the column[i] = min--max is computed. The resulsts are used to .... 
   -- "build_tree_for_range__s_kd_tree(..)": ... if box "l...u" is les than box-size threshold, then reutrn the resutls from 'our funciton'; otherwise idneitfy the min-max-feature-bound for each vertex. For the latter step we: identify the split point (calling our "__setSplitPoint(..)"), a 'split' which is used to idnitfy if the "l...split" or "split....u" is to be used for .... ; thereafter idneityf the bounding-vector between the left--right child-nodes:  (callling our "__interSect_childrenAndNode(..)"), a funciton which .... 
   -- "build_tree_for_range__s_kd_tree__nonReqursive(..)": .... A non-requisrve procedure to construct a kd-tree. .... 
   -- "(..)": 
**/
static void kt_tree_kd__spread_in_coordinate(const s_kd_tree_t *self, const uint c, const uint l, const uint u, t_float *scalar_min, t_float *scalar_max)
{
  assert(self);
  const s_kt_matrix_t *mat_input = self->mat_input;
  assert(mat_input);
  // -------------
  // Note[tut+perf]: seems like SSE does not result in any perofmrance-imrpvoements .... which is (delibved to be) due to aritmethci overhead wrt. comptuations. 
  // FIXME[tut+perf]: validate [below] asusmption wrt. our optmized comptuation-procedure. 
  // -------------
  // FIXME[tut+eval]: clasisfy the different perfomrance-bottlenecks ... use altter to elvauate/describe/idneitfy ......??... 
  // FIXME[code::SSE]: ... ivnesitgate if a 'pre-mapping-step' ... to cosntruct a cotninues list of list_local = [mat_input[MiF__index[i-1]]] .... combiend with sSE may reduce the exueicont-time .... 
  // FIXME[tut+eval]: dientify the min--max-range where SSE will imrpvoe the exueciont-tiem-benefit .... 
  t_float smin = __scoreReMap(l, c); ///mat_input->matrix[MiF__index[l]][c];
  t_float smax = smin;

  uint i = 0;
#if(1 == 2) //! then we use SSE:
  const uint cnt_ele = ( u - l );
  assert(cnt_ele <= u);
  if(cnt_ele > 800) { //! then we apply/use SSE: 
    //assert(VECTOR_FLOAT_ITER_SIZE == 4); //! ie, as we otherise need to udpate [”elow] <-- TODO: cosndier rmeoving this.
    const uint cnt_sse = cnt_ele - VECTOR_FLOAT_ITER_SIZE;
    //const uint  cnt_sse = l + macro__get_maxIntriLength_float(cnt_ele);
    /* assert(l != cnt_sse); */
    /* assert(cnt_sse != cnt_ele); */
    /* assert(cnt_sse <= u); */
    i = l;
    VECTOR_FLOAT_TYPE vec_inp = VECTOR_FLOAT_SET(__scoreReMap(i, c), __scoreReMap(i+1, c), __scoreReMap(i+2, c), __scoreReMap(i+3, c));
    VECTOR_FLOAT_TYPE vec_min = vec_inp;     VECTOR_FLOAT_TYPE vec_max = vec_inp;
    i += VECTOR_FLOAT_ITER_SIZE;
    //assert((i+3) <= u);
    //! 
    //! Iterate:
    for(; i < cnt_sse; i += VECTOR_FLOAT_ITER_SIZE) {
      VECTOR_FLOAT_TYPE vec_inp = VECTOR_FLOAT_SET(__scoreReMap(i, c), __scoreReMap(i+1, c), __scoreReMap(i+2, c), __scoreReMap(i+3, c));
      //! Update min-max:
      vec_min = VECTOR_FLOAT_MIN(vec_min, vec_inp);
      vec_max = VECTOR_FLOAT_MAX(vec_max, vec_inp);
      // assert((i+3) <= u);
    }
    //!
    //! Update the result-scalar: 
    smin = VECTOR_FLOAT_storeAnd_horizontalMin(vec_min);
    smax = VECTOR_FLOAT_storeAnd_horizontalMax(vec_max);
  }
#endif
  //!
  //! Then the non-SSE-part:
#if(1 == 0) //! where 'this appraoch' in a defbug-modes goes slower.
  for (i=l+1; i<= u; i += 1) {
    t_float lmin = __scoreReMap(i, c); //mat_input->matrix[MiF__index[i-1]] [c];
    t_float lmax = __scoreReMap(i, c); //mat_input->matrix[MiF__index[i]  ] [c];
    //! Update:
    smin = macro_min(smin, lmin);
    smax = macro_max(smax, lmax);
  }
#else
  for (i=l+2; i<= u; i+=2) {
    /* t_float lmin = mat_input->matrix[MiF__index[i-1]] [c]; */
    /* t_float lmax = mat_input->matrix[MiF__index[i]  ] [c]; */
    t_float lmin = __scoreReMap(/*row=*/(i-1), c); //mat_input->matrix[MiF__index[i-1]] [c];
    t_float lmax = __scoreReMap(/*row=*/i, c); //mat_input->matrix[MiF__index[i]  ] [c];

    if (lmin > lmax) {
      MF__SWAP_complexType(t_float, lmin,lmax); 
    }
    smin = macro_min(smin, lmin);
    smax = macro_max(smax, lmax);
  }
#endif
  //! Test for the 'boundary' case:
  if(i == (u+1)) {
    t_float last = __scoreReMap(u, c); //mat_input->matrix[MiF__index[u]] [c];
    smin = macro_min(last, smin);
    smax = macro_max(last, smin);
  }
  *scalar_min = smin;
  *scalar_max = smax;
  //printf("Spread in coordinate %d=[%f,%f], at %s:%d\n",c,smin,smax, __FILE__, __LINE__);
}


//! Update mapping-table: ensure property "[l..k][constant] < [k+1..u][constant]", ie, wher e'k' dentoes the midpoint.
//! Note: in the function we update the row-order, ie, to reflect the seperation-order wrt. closeness between/across a given feautre.
static void kt_tree_kd__select_on_coordinate(s_kd_tree_t *self, const uint c, const uint k, uint l, uint u) {  
  // FIXME[conceptaul]: try writing 'legal' permtautions of [below] ... ie, different splti-alterntiaves .... to explreo how .....??...  
  // TODO[perf]: would it go faster if first 'ctracted' a vector: t_float *vec_1 = [l....u][u] ... ??
  while (l < u) { //! evaluate rows at a cosntant feature-vector. Re-order the rows to reflect the sorted property for the dimension.
    const uint t = MiF__index[l];
    uint m = l; //! ie, the start-position.
    // TODO[tut+eval]: inveistte the exeuciotn-time-effect .... by using a transposed input-matirx as input .... 
    for (uint i=l+1; i<=u; i++) { //! ie, the range of rows to evaluate.
      if ( __scoreReMap(i, c) < __score(t, c)) {
      //if ( self->mat_input->matrix[ MiF__index[i] ][c] < self->mat_input->matrix[t][c]) {
	m++;
	MF__SWAP_complexType(t_float, MiF__index[i], MiF__index[m]); //! then re-order the two index-positions, ie, to reflect the score at the given feature-vector.
      }
    } //! and at this point:  "__scoreReMap(i, c) >= __score(t, c)"
    MF__SWAP_complexType(t_float, MiF__index[l], MiF__index[m]);

    assert(m >= 1); //! ie, to avoid negative numbers.
    if (m <= k) l = m+1; //! ie, as matrix[remap(l)..remap((l+m+))][c]<matrix[remap(l)][c]
    if (m >= k) u = m-1; //! ie, to reduce the update-sequence
  } // while loop
}


//! Order the vertices along the fixed/given diemsnion: center the vertices along the alpha threshold. From the/our impmetnaiton we observe how [l ... m][fixed] < alpha <= [m+1.....u].
static uint kt_tree_kd__select_on_coordinate_value(s_kd_tree_t *self, const uint c, const t_float alpha, const uint low_bound, const uint up_bound) {
  // FIXME: re-write the name of these functions
  // FIXME[tut+eval]: .... 
  // FIXME[tut+eval]: .... desicrbe/evlauate the impact of different prior alpha-critera .... sugest/argue ho the latter reuqires dieltaed domain-knoweldge .... eg, were a direct-disjtoint-froest-appraoch may equally/sufficelntly be used .... 
// FIXME[code+tut+perf]: ... find the min-chunk-size for kd-trees to be imrpvoed by using a macor-inline-fucntion-call .... 
// FIXME[code]: ... change into macros for ... kt_tree_kd__select_on_coordinate_value(..), kt_tree_kd__select_on_coordinate(..), kt_tree_kd__spread_in_coordinate(..)
  // TODO[perf]: evlauat ethe time-effect of 'moving' this to an inlined macro-function.
  assert(low_bound >= 0);
  assert(up_bound > 0);
  assert(low_bound < up_bound);
  uint lb = low_bound, ub = up_bound;
  while (lb < ub) {
    if(__scoreReMap(lb, c) <= alpha) { //! where 'this conditon' always ensures that all values/scores 'on the left side' is less than the "alpha" threshold.
      lb++; // good where it is.
    } else {
      MF__SWAP_complexType(t_float, MiF__index[lb], MiF__index[ub]); 
      ub--;
    }
  }
  if (__scoreReMap(lb, c) <= alpha) {
    return(lb);
  } else {
    return(lb-1);
  }
}


// **************************************
// **************************************
// **************************************

/**
   @brief splits the rows [remap(l) ... remap(u)] by idnetifiying the column with the highest degree of seperation
   @remarks this function details the weakness of the K-D tree appraoch (hence capturing cases where topology si not proeprly/accrately addressed/handled):
   -- find fixed column: choose the column with the highest score-difference, a strategy which is inaccrate for cases where ....  
   -- splitting point: the maximum number of 'moves' (or: steps) for re-ordering of the rows, applying different permtautions of soring .... wekaness/colunerabilities of the later concenrs how it ... 
 **/
static uint  __setSplitPoint(s_kd_tree_node_t *node, s_kd_tree_t *self, uint l, uint u, s_kd_tree_node* parent, const uint dim) {
  // TODO[perf]: evalaute the perf-differnce of moving this into a macro
  uint c = UINT_MAX;
  t_float maxspread = T_FLOAT_MIN_ABS;
  uint m; 
  
  //! Update the mapping-table and idneitfy a max-spread wrt. the scores.
  for (uint i=0; i<dim; i++) { //! then 
    if ((parent == NULL) || (parent->cut_dim == i)) { //! then we assume the parent is the root.
      kt_tree_kd__spread_in_coordinate(self, i,l,u,
				       //! Then implcitly update our bodungin-box using the [below] score-trategy:
				       &(node->box.list[i].head), &(node->box.list[i].tail));
    } else {
      node->box.list[i] = parent->box.list[i];
    }
    
    const t_float spread = node->box.list[i].tail - node->box.list[i].head; //! ie, the score-difference between the smallest, and largest row, for columns=c.
    //if (spread > maxspread || c == -1) {
    if (spread > maxspread) {
      maxspread = spread;
      c=i; 
    }
  }

  //! Note: at this point "c" should describe the column with the highest variance/spread, hence (hopefully) be prepresententitve splittingpoint (or: midpoint).
  
  
  //!
  //! Identify the split-poitn between the rows, ie, to find the row-seperiaotn with the hgiest deviaotns in scores.
  // FIXMe[tut+eval]: evaluate the prediocnt-eprofmrance-ssientity to [below] optiosn/cases.
  assert(c != UINT_MAX);
  // FIXME: add a new enum to test [”elow] .... and evaluate the diffferent use-cases for the different implemetantiosn.
  if (false) {
    m = (l+u)*0.5;
    kt_tree_kd__select_on_coordinate(self, c,m,l,u);  
  } else {   //! then comptuete the sum of elements which are along thsi 'diagonal':   
    t_float sum; 
    t_float average;
    // FIXME[eval]: descibe differnt strategies for midpoint selection ... their prediction-infleunce .... 
    if (true) { 	//! then idneitfy the average-sum:
      sum = 0.0;
      for (int k=l; k <= u; k++) {
	sum += __scoreReMap(k, c); //self->mat_input->matrix[MiF__index[k]][c];
      }
      average = (sum != 0.0) ? sum / (t_float)(u-l+1) : 0;
    } else {
      average = (node->box.list[c].tail + node->box.list[c].head)*0.5; //! ie,t eh average of the top--bottom node->
    }      
    m = kt_tree_kd__select_on_coordinate_value(self, c,average,l,u);
  }
  
  if(m <=l || m >= u) {
    m = (l + u) *0.5;
  }
  // move the self->map_indexices around to cut on dim 'c'.
  node->cut_dim = c; //! where "c" is the fallback-feature-id.
  node->l = l;
  node->u = u;
  //! 
  //! @return
  return m;
}

/**
   @brief copies the data derived from the reqcursive iteraiton strategy.
   @remarks idea is to use the infrmation, dervied from the 'recursive child iteraiotn steps', to idneitfy/set the min--max boundaries.
 **/
static void __interSect_childrenAndNode(s_kd_tree_node *node, const uint dim) {
  assert(node); assert(dim > 0);
  // FIXME[tut+eval]: evlauate/idneitfy the time-cost for buliding b-trees at/for icnreasing data-szies ... try suign altter as an arugment for the impraotnce of our 'all-out' simalirty-metric-cotmpatuion-strategy .... 
  
  
  //! Fall-back: copy from 'this' if the proeprties were Not set:
  const uint c = node->cut_dim;
  if (node->right == NULL) {
    // FIXME[code]: write a new macor for copying the "s_kt_list_1d_fPair_t" in our "kt_list_1d.h" ... then update our "kt_tree_kd.c"
    // FIXME: generalie [”elow] ... into a macro.
    assert(node->left != NULL); //! as we expect it to be intlised by default.
    for (uint i=0; i<dim; i++) {
      node->box.list[i] = node->left->box.list[i]; 
    }
    node->cut_val = node->left->box.list[c].tail;
    node->cut_val_left = node->cut_val_right = node->cut_val;
  } else if (node->left == NULL) { 
    // FIXME: generalie [”elow] ... into a macro.
    for (uint i=0; i<dim; i++) {
      node->box.list[i] = node->right->box.list[i]; 
    }
    node->cut_val =  node->right->box.list[c].tail;
    node->cut_val_left = node->cut_val_right = node->cut_val;
  } else {
    // TODO[perf]: evlauat ehte impact of 'movinig' this for-loop to the 'first' evaluation-case.
    node->cut_val_right = node->right->box.list[c].head;
    node->cut_val_left  = node->left->box.list[c].tail;
    node->cut_val = (node->cut_val_left + node->cut_val_right) * 0.5; // ie, instead of "/ 2.0"; 
    //!
    //! Find the min-max-bound for each of the features, ie, where latter is used as the seleciton-ciritera during tree-traversal
    //! Note: when usign [”elow] min-max-seleion-strategy the idneitficicoant of the min-max-bound wrt. neighbourhood-vertices are lmitied/bound by the tree-dpeth, hence kd-tree-searhc-phase has a lgoarithmic time-cost (when comapred to a burte-force-appraoch). Howev ergivne the ismpflciaotns wr.t the kd-tree-aprpaoch the data-structre produce in-accurate results for large feuatre-sapces, hence kd-tree should not be used for high-deimsional feautre-data-sets.
    for (int i=0; i<dim; i++) { //! then choose the 'maximal pssobile range' of the score bondaries.
      node->box.list[i].head = macro_min(node->left->box.list[i].head,
					 node->right->box.list[i].head);
      node->box.list[i].tail = macro_max(node->left->box.list[i].tail,
					 node->right->box.list[i].tail);
      
    }
  }
}


static s_kd_tree_node *build_tree_for_range__s_kd_tree(s_kd_tree_t *self, uint l, uint u, s_kd_tree_node* parent, const uint dim) {
  assert(self);
  const uint bucketsize = self->bucketsize;
  //assert(l < l); 
  if (u < l) {
    return NULL; // MF__toEmpty__s_kd_tree_node_notNull(); // no data in this node.
  }
  s_kd_tree_node_t *node = MF__init__s_kd_tree_node(dim); //! ie, {l, u, left, right, box : [{upper, head}]}      
  assert(dim <= node->box.list_size);

  // FIXME[code]: move the non-requsirve lgoic-chunk from our "build_tree_for_range__s_kd_tree(...)" and into a new function .... 

  if ((u-l) <= bucketsize) { //! then the ndoe is aleaf/temrianl-node:
      // FIXME[tut+eval]: does the order of the features result in a change in the kd-tree-preduciton .... ie, as rows[l...u] is re-ordered in the mappgin-table based on each feature "i" .... ??
    for (uint i=0; i < dim; i++) { //! then order the rows for each of the dimensions
      // FIXME[tut+eval]: evlaua teht eime-cost-beneift of 'moivng' thsi fucntion inside the kd-tree-cotmaptuion-stpe .... eg, wrt. opzioants-trategyies for kd-tree .... 
      // FIXME[tut+eval]: try supporitng all simalrity-emtrics in our kd-tree-aprpaoch ... use altter as an arugemtn for the benefit/preferitaonlity wrt. our db-scan-impmetnations .... 
      kt_tree_kd__spread_in_coordinate(self, i,l,u, &(node->box.list[i].head), &(node->box.list[i].tail)); //! ie, the score-seperation for "[l...u][i]"
    }
    /* node->cut_dim = 0;  */     /* node->cut_val = 0.0; */
    node->l = l; node->u = u;
    //node->left = node->right = MF__toEmpty__s_kd_tree_node();    
    return(node);
  } else { //! then apprxomiate the bounding-box for the vertex.
    //!
    //! Idneitfy the min-max-feature-bound for each vertex.
    //! 
    //! Idneitfy the split-row and split-feature:
    /*  -------------------------------------------------------
       FIXME[eval]: comarpe our split-prodcure to "/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/externalLibs__scikit_learn/scikit-learn-master/sklearn/neighbors/binary_tree.pxi" .... where [”elow] is exanlty simliar to our "__setSplitPoint(..)"
    for j in range(n_features):
        max_val = data[node_indices[0] * n_features + j] #! ie, [row-id][j]
        min_val = max_val
        for i in range(1, n_points):
            val = data[node_indices[i] * n_features + j]
            max_val = fmax(max_val, val)
            min_val = fmin(min_val, val)
        spread = max_val - min_val
        if spread > max_spread:
            max_spread = spread
            j_max = j
    return j_max
    ------------------------------------------------------- */
    const uint m = __setSplitPoint(node, self, l, u, parent, dim);
    // const uint c = node->cut_dim; //! ie, the split-feature-id.
    // FIXME: fork [below] out into a seperate funciton-call.


    // FIXME[cocneptaul]: re-write our "build_tree_for_range__s_kd_tree(..)" .... enw permtuation ... into a non-requisve unction .... first idneitfy cases where non-requsiion may yeald sigicnat time-savinigs .... where we iterate through list1={nodeParent, l, m, isLeft={yes,no,none}} .... and where we evalaut the internal-index-prop ... when "right" is covered/evalauted ... ie, where we sue a DFS-inseriotn-proceudre .... 
    // FIXME[code]: write new strucutres and inti-functions for a non-requsirce-support whielt-raversa-loop ..... "list1={internalIndex, l, m, isLeft}" .... 
    // FIXME[code]: construcrt/wirte n iterative-rveriosn of the [”elow] reusion-appraoch .... a sperate funciton ..... then scaffold a perf+eval comapging the latter .... ie, the imapct/invluence of reuscion ....     

    //!
    //! Apply recursive strategies: 
    if(l < m) {
      node->left = build_tree_for_range__s_kd_tree(self,  l,m,node, dim);
    }
    if((m+1) < u) {
      node->right = build_tree_for_range__s_kd_tree(self, m+1,u,node, dim);
    }
    //!
    //! Idneityf the bounding-vector between the left--right child-nodes: 
    __interSect_childrenAndNode(node, dim); //! ie, udpate 'this' with data from the 'recursively constructed' childrens.
    return(node);
  }
}

//! A lcoal stuructre used to replce/avoid the defualt reucsirve loop-call-seqeucnce. 
typedef struct s_kd_tree_node__iterNode {
  s_kd_tree_node_t *node;
  uint low; uint median;
} s_kd_tree_node__iterNode_t;
#define init__s_kd_tree_node__iterNode(node, l, med) ({s_kd_tree_node__iterNode_t self; self.node = node; self.low = l; self.median = med; self;})
#define alloc__s_kd_tree_node__iterNode_t(size) ({assert(size > 0); s_kd_tree_node__iterNode_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_kd_tree_node__iterNode_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
#define free__s_kd_tree_node__iterNode_t(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

//! A non-requisrve procedure to construct a kd-tree.
static s_kd_tree_node *build_tree_for_range__s_kd_tree__nonReqursive(s_kd_tree_t *self, uint l_input, uint u_input, s_kd_tree_node* parent, const uint dim) {
  assert(self);
  const uint bucketsize = self->bucketsize;

  const uint stack_size = (self->mat_input->nrows * 2);
  s_kd_tree_node__iterNode_t *stack = alloc__s_kd_tree_node__iterNode_t(stack_size); uint stack_pos = 0;
  s_kd_tree_node_t *root_node = NULL;
  do {
    uint l = l_input; uint u = u_input;
    s_kd_tree_node_t *node = NULL; //MF__init__s_kd_tree_node(dim); //! ie, {l, u, left, right, box : [{upper, head}]}      
    s_kd_tree_node_t *parent = NULL;
    bool node_isLow = false; bool isSelfRef = false;
    if(root_node != NULL) {
      //assert(u < l); 
      assert(stack_pos > 0);
      s_kd_tree_node__iterNode_t tmp = stack[--stack_pos];      
      parent = tmp.node;
      assert(parent);
      //! ---- 
      
      if(tmp.low != tmp.median) {
	node = MF__init__s_kd_tree_node(dim); //! ie, {l, u, left, right, box : [{upper, head}]}      
	//! ---- 
	if(tmp.low < tmp.median) {node_isLow = true; parent->left = node;  l = tmp.low; u = tmp.median; assert(l <= u);}	
	else {parent->right = node; l = tmp.median; u = tmp.low; assert(l <= u);}
	assert(l <= u); //! ie, based on [ªbove].
      } else { isSelfRef = true; }
      //! ---- 
      //printf("#\t iter-node, where isSelfRef=%u at %s:%d\n", isSelfRef, __FILE__, __LINE__);
    } else {      
      //printf("#\t root-node at %s:%d\n", __FILE__, __LINE__);
      node = MF__init__s_kd_tree_node(dim); //! ie, {l, u, left, right, box : [{upper, head}]}      
      root_node = node;
    }
    /* if (u < l) { */
    /*   return NULL; // MF__toEmpty__s_kd_tree_node_notNull(); // no data in this node.  */
    /* } */
      
    //! Updat hte parent-ndoe with this reference.
    
    // FIXME[code]: move the non-requsirve lgoic-chunk from our "build_tree_for_range__s_kd_tree(...)" and into a new function ....     
    //printf("(ull)=(%u-%u)=%u <= %u, at %s:%d\n", u, l, (u-l), bucketsize, __FILE__, __LINE__);
    if( !isSelfRef && ((u-l) <= bucketsize) ) { //! then the ndoe is aleaf/temrianl-node:
      // FIXME[tut+eval]: does the order of the features result in a change in the kd-tree-preduciton .... ie, as rows[l...u] is re-ordered in the mappgin-table based on each feature "i" .... ??
      assert(node);     assert(dim <= node->box.list_size);
      __MiF__startTimer();
      for (uint i=0; i < dim; i++) { //! then order the rows for each of the dimensions
	// FIXME[tut+eval]: evlaua teht eime-cost-beneift of 'moivng' thsi fucntion inside the kd-tree-cotmaptuion-stpe .... eg, wrt. opzioants-trategyies for kd-tree .... 
	// FIXME[tut+eval]: try supporitng all simalrity-emtrics in our kd-tree-aprpaoch ... use altter as an arugemtn for the benefit/preferitaonlity wrt. our db-scan-impmetnations .... 
	kt_tree_kd__spread_in_coordinate(self, i,l,u, &(node->box.list[i].head), &(node->box.list[i].tail));
      }
      __MiF__updateResult(time_coord);
      /* node->cut_dim = 0;  */     /* node->cut_val = 0.0; */
      node->l = l; node->u = u;
      //node->left = node->right = MF__toEmpty__s_kd_tree_node();    
      // return(node);
    } else { //! then apprxomiate the bounding-box for the vertex.
      //!
      //! Idneitfy the min-max-feature-bound for each vertex.
      //! 
      //! Idneitfy the split-row and split-feature:
      if(node) {assert(dim <= node->box.list_size);}
      else{assert(isSelfRef == true);}
      __MiF__startTimer();
      const uint m = (node) ? __setSplitPoint(node, self, l, u, parent, dim) : UINT_MAX; //! ie, identify the 'row' which maximizes the number of moves (hence seperation).
      __MiF__updateResult(time_split);
      // const uint c = node->cut_dim; //! ie, the split-feature-id.
      // FIXME: fork [below] out into a seperate funciton-call.                 
      bool __hasAddedChild = false;
      if(isSelfRef == false) {
	bool add_low = (l < m);
	bool add_up = ((m+1) < u);
	if(add_low || add_up) { //! then we include a self-refrence, ie, to apply post-processing when the DFS 'from thsi exec-point' ahs completed:
	  assert(stack_pos < stack_size);
	  stack[stack_pos++] = init__s_kd_tree_node__iterNode(node, UINT_MAX, UINT_MAX); //! ie, to explctly state taht we ahve a self-ference
	  __hasAddedChild = true;
	}
	if(add_up) {
	  assert(stack_pos < stack_size);
	  stack[stack_pos++] = init__s_kd_tree_node__iterNode(node, u, (m+1));
	  //node->right = build_tree_for_range__s_kd_tree(self, m+1,u,node, dim);
	}
	if(add_low) {
	  assert(stack_pos < stack_size);
	  stack[stack_pos++] = init__s_kd_tree_node__iterNode(node, l, m);
	  //node->left = build_tree_for_range__s_kd_tree(self,  l,m,node, dim);
	}
      }
      if(!__hasAddedChild || !isSelfRef) {
	//!
	//! Idneityf the bounding-vector between the left--right child-nodes: 
	// FIXME: try figureing out how to 'drop' [”elow] if-clause.
	if(parent) {
	  assert(parent);
	  __MiF__startTimer();
	  __interSect_childrenAndNode(parent, dim);
	  __MiF__updateResult(time_intersect);
	}
      }
      // return(node);
    }
#ifndef NDEBUG
    __kdGlobal_timinng.cnt_loops++;
#endif
  } while(stack_pos > 0);
  //!
  //! De-allocte:
  assert(stack);
  free__s_kd_tree_node__iterNode_t(stack);
  //!
  //! @return
  return root_node;
}

//! An internal fucniton to support inaiotn fore both kd-tree and brute-force-matrix-operaitons.
s_kd_tree_t __build_tree__s_kd_tree(const s_kt_matrix_t *mat_input, const bool isTo_buildKD) {
#ifndef NDEBUG
  initGlobal____kd_tree__bm_t();
  initGlobal____kd_tree__bm__search_t();
#endif
  assert(mat_input); assert(mat_input->matrix);
  const uint nrows = mat_input->nrows;
  const uint dim = mat_input->ncols; 
  assert(nrows > 1);
  s_kd_tree_t self_glob;
  self_glob.root = NULL; // MF__toEmpty__s_kd_tree_node();
  self_glob.mat_input = mat_input;
  self_glob.mat_inputTransp = setToEmptyAndReturn__s_kt_matrix_t();
#if(MiC__useTransp == 1)
  const bool is_ok = init__copy_transposed__s_kt_matrix(&(self_glob.mat_inputTransp), mat_input, false);
  assert(is_ok);
#endif
  if(nrows <= 1) {
    self_glob.map_index = init__s_kt_list_1d_uint_t(0);
    return self_glob; //! ie, as there is only one row there is no poin in indexing the latter. 
  }
  if(isTo_buildKD) { //! then we cosntruct a kd-tree.
    self_glob.map_index = init__s_kt_list_1d_uint_t(nrows);
    //  self_globbucketsize, 
    // FIXME[code]: move "ind" intoa  enw object ... use a "uint *ind" ... then udpate all of oru correcenss/usages wrt. this .... 
    //#pragma omp parallel for shared(N, ind) private(i)
    s_kd_tree_t *self = &self_glob;
    for (uint i=0; i<nrows; i++) {
      MiF__index[i] = i;
    }
    // FIXME[cocneptaul]: update our aritlce ... wrt. our non-requisve unction .... first idneitfy cases where non-requsiion may yeald sigicnat time-savinigs .... where we iterate through list1={nodeParent, l, m, isLeft={yes,no,none}} .... and where we evalaut the internal-index-prop ... when "right" is covered/evalauted ... ie, where we sue a DFS-inseriotn-proceudre .... 
    // FIXME[code+tut+perf]: ... time-cost-beneift ... usign iteration instead of recusion .... ie, max-trehshold wrt. code-overhead .... 
    if(true) { 
      self_glob.root = build_tree_for_range__s_kd_tree__nonReqursive(&self_glob, 0,nrows-1,NULL, dim);
    } else {
      self_glob.root = build_tree_for_range__s_kd_tree(&self_glob, 0,nrows-1,NULL, dim);
    }
#ifndef NDEBUG
    if(false) {    
      printf("#(stat)\t cnt(loops)=%u, time(split=%f, coord=%f, intersect=%f), at %s:%d\n", 
	     __kdGlobal_timinng.cnt_loops, 
	     __kdGlobal_timinng.time_split,
	     __kdGlobal_timinng.time_coord,
	     __kdGlobal_timinng.time_intersect,
	     __FILE__, __LINE__);
    }
#endif
  } else {
    self_glob.map_index = setToEmpty__s_kt_list_1d_uint_t();
    self_glob.root = NULL;
  }
   // #ifdef _DEBUG
   // cout << "regular_median_total " << regular_median << " not_regular_median_total " << not_regular_median << endl;
   // #endif
  return self_glob;
}

/**
   @brief constrct a K-D tree (oekset, 06. jul. 2017).
   @param <mat_input> is the feautre-amtrix to comptue for.
   @return an initiliazated object of type "s_kd_tree_t"
 **/
s_kd_tree_t build_tree__s_kd_tree(const s_kt_matrix_t *mat_input) {
  return __build_tree__s_kd_tree(mat_input, /*isTo_buildKD=*/true);
}
//! De-allcotes the s_kd_tree_t object
void free__s_kd_tree(s_kd_tree_t *self) {
#ifndef NDEBUG
  if(false) {
    printf("#(stat::search)\t cnt(calls)=%u, cnt(loops)=%u,cnt(loops/calls)=%.2f, time(node(terminal)=%f), at %s:%d\n----------------------------------\n", 
	   __kdGlobal_timinng_search.cnt_calls, 
	   __kdGlobal_timinng_search.cnt_loops,
	   (t_float)__kdGlobal_timinng_search.cnt_calls / (t_float)__kdGlobal_timinng_search.cnt_loops,
	   __kdGlobal_timinng_search.time_terminal_node,
	   /* __kdGlobal_timinng.time_coord, */
	   /* __kdGlobal_timinng.time_intersect, */
	   __FILE__, __LINE__);
  }
#endif
  assert(self);
  if(self->root) {free__s_kd_tree_node_t(self->root); free(self->root);}
  free__s_kt_list_1d_uint_t(&(self->map_index));
  free__s_kt_matrix(&(self->mat_inputTransp));
}

// ***************************************************************************************************************************************************
// ***************************************************************************************************************************************************
// ***************************************************************************************************************************************************

// FIXME: change name of "s_kd_searchConfig" to ....



// FIXME[code]: move all of [”elow] code to "kt_tree_search.c"
// #include "kt_tree_search.c" //! ie, the inernal lgocis for data-searhcing.

#define macro_squared(v1) ({v1 * v1;})

//! find the abs-divergence.
// FIXME[conceptaul]: describe SSE-verison of [”elow] .... eg, .....??...
// FIXME[tut+perf]: evalaute the use-case where we if "if(x < v_max) { ... }"
#define dis_from_bnd(x, v_min, v_max) ({t_float score = 0.0;   if (x > v_max) {score = (x - v_max); } else if (x < v_min) {score = (v_min - x);} score;})


/**
   @brief Compare the 'row' to the min--max score seperately for each feature
   @remarks the function exemplfies how:
   // FIXME[eval]: construct dataseets which maizine the goodness, and 'poority', of K-D trees, using [below] obsercaitons .... where 'sound' datasets for K-D trees may be idneitfed through ....??.... 
   -- row-boundaries: assume that the 'head' and 'tail' bounaries are representitve; if the bounaries where idnetifed thorugh a 'min--max' appraoch, non-uniformly distributed columns would result in inaccurate predictions. 
   -- distance metric: while the 'squared(..)' metric is advanagous for cases where large score difference always indicates low simliarty, hte metric is stronlgy skwed/inaccurate for cases which does not obey/hold the latter property/requirement (eg, as seen for sinusoidal waves);
   -- all features: idneitfy the distance betwween the 'row box' and the featurev-vector by seperatley investigting for all features; the latter exemplfies a major weakness of K-D trees: while all features are used, only a signular feature-column was used to idneitfy the splitting point of rows
**/
inline bool box_in_search_range(const s_kd_tree_node_t *self, const s_kd_searchConfig_t *sr) { //, const uint dim) { 
  t_float sum =0.0; 
  // TODO: replace [below] with a call to our "hp_distance(..)" .... where we 'handle' the issue/proeprty of by .....??... <-- FIXME: consider to write a new wrapper ... for 1d-list-traversals .... wrt. our "template_correlation_tile_innerMostColumn.c" ... ie, where we use an ....??... SSE-routine ... (ie, to handle two different feautre-vectors .... macro_max( ... ) ....??... 
  bool is_ok = true;
  for (int i=0; (i < sr->dim) && is_ok; i++) {
    sum += macro_squared(dis_from_bnd(sr->row_1[i], self->box.list[i].head, self->box.list[i].tail));
    if (sum > sr->conf.ballsize) {is_ok = false;}
  }
  return is_ok;
}

/**
   @brief add to the temproray result strucutre (eg, heap) if distance between "vector(search)=sr"  and "box=rows[reordered(l)...reordered(u)]" is inside the user-deifned threshold; examples of user-defined thresohlds are 'dsitance score', 'closest vertices' (for which the min-heap is used), etc.;
   @remarks in the evaluation process, we first idneityf the dsitance between the "vector(search)=sr" and the terminal vecotrs. The result is then evlauated based on the user-deinfed threshodls. When applying the thresholds, we iterate through the newly consturcted vector, where latter hodls the simliarites between "vector(search)" and "box(rows)=[reordered(l)...reordered(u)]"
 **/
void s_kd_tree_node__process_terminal_node(const s_kd_tree_t *self, s_kd_searchConfig_t *sr, const s_kd_tree_node_t *node) {
  //#if(0 == 1)
  const uint centeridx  = sr->centeridx;
  const uint correltime = sr->conf.correltime;
  //uint nn = sr->nn; 
  const uint dim        = sr->dim;
  t_float ballsize = sr->conf.ballsize;
  //
  const bool rearrange = sr->conf.rearrange; 
  // FIXME[code]: change/mofiy varilabe-names for funciton .... s_kd_tree_node__process_terminal_node()

  assert(node);
  const uint l = node->l;
  const uint u = node->u;

  //!
  //! Generlaize the funciotn: add scores tempraoly to a float-list:
#if(1 == 1)
  s_kt_matrix_base_t mat_result = initAndReturn__s_kt_matrix_base_t(1, /*size=*/(u-l)+1);   uint current_pos = 0;
#endif
  if(false) {
  //if(sr->obj_metric != NULL) {
    //! Then a deep copy:
    s_kt_matrix_base_t mat_input = initAndReturn__s_kt_matrix_base_t(mat_result.ncols, dim);
    if (rearrange) {
      current_pos = 0;
      assert(l <= u);
      for (uint i=l; i<=u; i++) {
	uint current_index = MiF__index[i]; //UINT_MAX;  // sr->ind[i]; 
	for (uint k=0; k<dim; k++) {
	  mat_input.matrix[current_pos][k] = MiF__matrix[current_index][k]; //! here we do Not use "memcpy(..)" givne oru eepctioant of feautre-vount < 20 (ie, as a feature-count higher than the altter will result in in-accurate clsuter-rpediction-results). 
	}
	current_pos++;
      }
      assert(current_pos > 0);
    } else {
      current_pos = 0;
      for (uint i=l; i<=u; i++) {
	for (uint k=0; k<dim; k++) {
	  mat_input.matrix[current_pos][k] = MiF__matrix[/*current_index=*/i][k]; //! here we do Not use "memcpy(..)" givne oru eepctioant of feautre-vount < 20 (ie, as a feature-count higher than the altter will result in in-accurate clsuter-rpediction-results). 
	}
	current_pos++;
      }
      assert(current_pos > 0);
    }    
    assert(current_pos > 0);
    //!
    //! Comptue distance: 
    //! Set the matrix:
    // FIXME: validte correnctess of [”elow] ... the to use a poeirnt-reference to 'emulate' a matrix-reference.
    //!
    //! Comptue teh score-matrix, ie, call oru "hp_distance(..)":
    s_kt_matrix_base_t mat_result = initAndReturn__empty__s_kt_matrix_base_t();
#if(1 == 0)
    const bool is_ok = apply__extensive__hp_distance(&(sr->objDist_1_toMany), &(sr->matShallow_2), &mat_input, &mat_result);
    assert(is_ok);
#else //! then we reduce the perofmrance-lag by calling the fucniton directly: 
    //!
    //! Apply logics:
#define self (&(sr->objDist_1_toMany))
    const uint nrows_2 = sr->matShallow_2.nrows;
    const uint ncols = dim;
#define data_1 sr->matShallow_2.matrix
#define data_2 mat_input.matrix
    mat_result = initAndReturn__s_kt_matrix_base_t(1, nrows_2);
#define local__resultMatrix mat_result.matrix
    const e_cmp_masksAre_used_t masksAre_used = (self->needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
    t_float *weight = NULL;
    char **mask = NULL;
    s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, mask, weight, /*transpose=*/false, masksAre_used, /*nrows=*/1, ncols);
    // printf("\t (compute-sim-score), current_pos=%u, at %s:%d\n", current_pos, __FILE__, __LINE__);
    kt_compare__oneToMany(self->obj_metric.metric_id, self->obj_metric.typeOf_correlationPreStep,
    			  //nygrid, //
    			  nrows_2,
    			  ncols, data_1, data_2, /*index1=*/self->static_config.obj_1_index1,  &config_metric,
    			  (self->isTo_useOptimizedMetricFor_kendall == false) ? NULL : &(self->obj_kendallCompute),
    			  //(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute,
    			  /*arrOf_result=*/local__resultMatrix[0],
    			  self->metric_oneToMany);
#undef self
#undef local__resultMatrix
#undef data_1
#undef data_2
#endif
    //!
    //! De-allocate
    free__s_kt_matrix_base_t(&mat_input);
  } else if( (sr->obj_metric != NULL) && sr->metric_each_nonRank  ) { //! then we use/apply a lsower proceudre using mroe idnidrect funciotn-calls.
    //assert(sr->metric_each_nonRank);
    // FIXME[tut+perf]: cosndier usign 'this example' as a sue-case wrt. the improtanc eof using 'close integration' wrt. simaliryt-metrics. 
    //sr->metric_each_nonRank = setmetric__correlationComparison__each(sr->obj_metric->metric_id, /*self.weight=*/NULL, NULL, NULL, /*needTo_useMask_evaluation=*/false);
    //#define __compute() ({config_nonRank_each_t config_metric; init__config_nonRank_each(&config_metric, dim, MiF__matrix[current_index], /*sr->row_1=*/sr->row_1,  /*self.weight=*/NULL, NULL, NULL, sr->config_metric_global); const t_float score = T_FLOAT_MIN_ABS; score;})
    #define __compute() ({config_nonRank_each_t config_metric; init__config_nonRank_each(&config_metric, dim, MiF__matrix[current_index], /*sr->row_1=*/sr->row_1,  /*self.weight=*/NULL, NULL, NULL, sr->config_metric_global); const t_float score = sr->metric_each_nonRank(config_metric); score;})

    if (rearrange) {
      for (uint i=l; i<=u; i++) {
	const uint current_index = MiF__index[i]; //UINT_MAX;  // sr->ind[i]; 
	//config_nonRank_each_t config_metric; init__config_nonRank_each(&config_metric, dim, MiF__matrix[current_index], sr->row_1,  /*self.weight=*/NULL, NULL, NULL, sr->config_metric_global);
	const t_float scalar_result = __compute();	
	/* const bool is_ok = apply__rows_hp_distance(*(sr->obj_metric), MiF__matrix[current_index], /\*sr->row_1=*\/sr->matShallow_2.matrix[0], dim, &scalar_result); */
	/* //	const bool is_ok = apply__rows_hp_distance(sr->obj_metric, MiF__matrix[current_index], sr->row_1, dim, &scalar_result); */
	/* assert(is_ok); */
	//if(isOf_interest(scalar_result)) {sumOf_scores += scalar_result;}
	mat_result.matrix[0][current_pos] = scalar_result;
	current_pos++;
      }
    } else {
      for (uint i=l; i<=u; i++) {
	const uint current_index = i; //UINT_MAX;  // sr->ind[i]; 
	const t_float scalar_result = __compute();	
	/* const bool is_ok = apply__rows_hp_distance(*(sr->obj_metric), MiF__matrix[current_index], /\*sr->row_1=*\/sr->matShallow_2.matrix[0], dim, &scalar_result); */
	/* //printf("score[%u]=%f, at %s:%d\n", l, scalar_result, __FILE__, __LINE__); */
	/* //	const bool is_ok = apply__rows_hp_distance(*(sr->obj_metric), MiF__matrix[current_index], sr->row_1, dim, &scalar_result); */
	/* assert(is_ok); */
	//if(isOf_interest(scalar_result)) {sumOf_scores += scalar_result;}
	mat_result.matrix[0][current_pos] = scalar_result;
	current_pos++;
      }
    }

#undef __compute
  } else if(sr->obj_metric != NULL) { //! then we use/apply a lsower proceudre using mroe idnidrect funciotn-calls.
    // FIXME[tut+perf]: cosndier usign 'this example' as a sue-case wrt. the improtanc eof using 'close integration' wrt. simaliryt-metrics. 
    if (rearrange) {
      for (uint i=l; i<=u; i++) {
	t_float scalar_result = 0;
	uint current_index = MiF__index[i]; //UINT_MAX;  // sr->ind[i]; 
	const bool is_ok = apply__rows_hp_distance(*(sr->obj_metric), MiF__matrix[current_index], /*sr->row_1=*/sr->matShallow_2.matrix[0], dim, &scalar_result);
	//	const bool is_ok = apply__rows_hp_distance(sr->obj_metric, MiF__matrix[current_index], sr->row_1, dim, &scalar_result);
	assert(is_ok);
	//if(isOf_interest(scalar_result)) {sumOf_scores += scalar_result;}
	mat_result.matrix[0][current_pos] = scalar_result;
	current_pos++;
      }
    } else {
      for (uint i=l; i<=u; i++) {
	t_float scalar_result = 0;
	uint current_index = i; //UINT_MAX;  // sr->ind[i]; 
	const bool is_ok = apply__rows_hp_distance(*(sr->obj_metric), MiF__matrix[current_index], /*sr->row_1=*/sr->matShallow_2.matrix[0], dim, &scalar_result);
	//printf("score[%u]=%f, at %s:%d\n", l, scalar_result, __FILE__, __LINE__);
	//	const bool is_ok = apply__rows_hp_distance(*(sr->obj_metric), MiF__matrix[current_index], sr->row_1, dim, &scalar_result);
	assert(is_ok);
	//if(isOf_interest(scalar_result)) {sumOf_scores += scalar_result;}
	mat_result.matrix[0][current_pos] = scalar_result;
	current_pos++;
      }
    }
  } else {
    if (rearrange) {
      for (uint i=l; i<=u; i++) {
	t_float sumOf_scores = 0;
	uint current_index = MiF__index[i]; //UINT_MAX;  // sr->ind[i]; 
	// FIXME[code]: consider to use a transpsoed distance-amtrix ... 
	for (uint k=0; k<dim; k++) {
	  //sumOf_scores += macro_squared(__score(i, k) - sr->row_1[k]);
	  sumOf_scores += macro_squared(MiF__matrix[current_index][k] - sr->row_1[k]);
	}
#if(1 == 1)
	mat_result.matrix[0][current_pos] = sumOf_scores; current_pos++;
#endif
      }
    } else {
      // FIXME[code]: tes tthe effecdt of re-mappgint he amtrix before the comptuation .... ie, to constuct a matrix using the order wrt. the "current_index" .... "MiF__index" ... 
      for (uint i=l; i<=u; i++) {
	t_float sumOf_scores = 0;
	for (uint k=0; k<dim; k++) {
	  //sumOf_scores += macro_squared(__score(/*current_index*/i, k) - sr->row_1[k]);
	  sumOf_scores += macro_squared(MiF__matrix[/*current_index=*/i][k] - sr->row_1[k]);
	} // end if rearrange. 
#if(1 == 1)
	mat_result.matrix[0][current_pos] = sumOf_scores; current_pos++;
#endif
      }
    }
  }
  //!
  //! Iterate through the newly consturcted vector, where latter hodls the simliarites between "vector(search)" and "box(rows)=[reordered(l)...reordered(u)]"
  for(uint i = 0; i < current_pos; i++) {
    const t_float sumOf_scores = mat_result.matrix[0][i];
    const uint current_index = i+l;
    // FIXME[code]: write a permutation to 'this' .... where we use [”elow] routine asa a post-processign .... ie, after we have evalauted all of the "[l ... u] x row_1" data-set-ids, ie, to avoid overhead wrt. function-calls (to simliarty-metric-rotuines) .... 
    // FIXME[code]: use a sroted list instead of a heap .... comapre perf-differecne wrt. this .... 
    //      if(isOf_interest(sumOf_scores) && !isinf(sumOf_scores)) {    printf("score[%u]=%f, thresh=%f, at %s:%d\n", i,sumOf_scores, ballsize, __FILE__, __LINE__); }
    if(sumOf_scores < ballsize) {
      // FIXME[code]: seperate [”elow] into different outer for-loops.
      bool isTo_addToHeap = true;
      if (centeridx != UINT_MAX) {
	// we are doing decorrelation interval
	assert(current_index != UINT_MAX);
	if (mathLib_float_abs((int)current_index - (int)centeridx) < correltime) {isTo_addToHeap = false;}; // skip this point. 
	//if (mathLib_float_abs(current_index - centeridx) < correltime) continue; // skip this point. 
      }
      //
      if(isTo_addToHeap) {
	if (
	    sr->result->result.current_pos < sr->conf.nn
	    ) {
	  // Set the ball radius to the largest on the list (maximum priority).
	  // FIXME: consider inserting the index and score into seperate/distint lsits .... OR ... cosntruct two objects ..... sparse-list + maxHeap(float) ... 
#if(1 == 1)
	  addTo_heap_max__s_kt_list_1d_kvPair_t(&(sr->result->result), MF__initVal__s_ktType_kvPair(current_index, sumOf_scores));
#endif
	  if (sr->result->result.current_pos == sr->conf.nn) {
	    assert(sr->result->result.current_pos > 0);
	    ballsize = sr->result->result.list[0].tail; //! ie, the max-heap.
	    //ballsize = sr->result.max_value();
	  }
	} else {
	  // FIXME: consider inserting the index and score into seperate/distint lsits .... 
	  //! Replace the first element [0] with e, and re-heapifying.
#if(1 == 1)
	  deleteHeapNode_max__s_kt_list_1d_kvPair_t(&(sr->result->result));
#endif
	  assert(sr->result->result.current_pos > 0);
	  ballsize = sr->result->result.list[0].tail; //! ie, the max-heap.
	  //#endif
	}
      }
    }
  } //! ie, end of the row-iteraiton [l..u]
  sr->conf.ballsize = ballsize;
  //!
  //! De-alloicate:
#if(1 == 1)
  free__s_kt_matrix_base_t(&mat_result);
#endif
}

static void s_kd_tree_node__process_terminal_node_fixedball(const s_kd_tree_t *self, s_kd_searchConfig_t *sr, const s_kd_tree_node_t *node) {
  uint centeridx  = sr->centeridx;
  uint correltime = sr->conf.correltime;
  uint dim        = sr->dim;
  t_float ballsize = sr->conf.ballsize;
  //
  bool rearrange = sr->conf.rearrange; 


  // FIXME[code]: change/mofiy varilabe-names for funciton .... ()
  // FIXME[code]: change/mofiy varilabe-names for funciton .... s_kd_tree_node__process_terminal_node_fixedball()
  assert(node);
  const uint l = node->l;
  const uint u = node->u;


  for (uint i=l; i<=u; i++) {
    uint current_index = MiF__index[i]; 
    t_float sumOf_scores = 0;
    //bool early_exit; 

    // FIXME[code]: seperate [”elow] into different outer for-loops.
    if (rearrange) {
      // FIXME: replace [”elow] with call to our "hp_distance(..)".
      for (int k=0; k<dim; k++) {
	sumOf_scores += macro_squared(MiF__matrix[i][k] - sr->row_1[k]);
      }
    } else {
      // FIXME: replace [”elow] with call to our "hp_distance(..)".
      for (int k=0; k<dim; k++) {
	sumOf_scores += macro_squared(MiF__matrix[current_index][k] - sr->row_1[k]);
      }
      // if(early_exit) continue; // next iteration of mainloop
    } // end if rearrange. 
    if (sumOf_scores < ballsize) {
      // FIXME[code]: seperate [”elow] into different outer for-loops.
      if (centeridx != UINT_MAX) {
	// we are doing decorrelation interval
	if (mathLib_float_abs((int)current_index - (int)centeridx) < correltime) continue; // skip this point. 
      }
      assert(false); // FIXME: validte taht [below] does not cofnliget with the heap-proeprty .... 
      push__s_kt_list_1d_kvPair_t(&(sr->result->result), MF__initVal__s_ktType_kvPair(current_index, sumOf_scores));
/* #if(0 == 1)  */
/*       { */
/* 	kt_tree_kd___result e; */
/* 	e.idx = indexofi; */
/* 	e.dis = sumOf_scores; */
/* 	// FIXME: consider inserting the index and score into seperate/distint lsits ....  */
/* 	sr->result.push_back(e); */
/*       } */
/* #endif */
    } 
 }
}
//! Note: the 'main-method':
static void search__kd_tree(const s_kd_tree_t *self, const s_kd_tree_node_t *node, s_kd_searchConfig_t *sr) {
  // FIXME: re-wwrite [below] documetnation .... 
  // the core search routine.
  // This uses true distance to bounding box as the
  // criterion to search the secondary node. 
  //
  // This results in somewhat fewer searches of the secondary nodes
  // than 'search', which uses the vdiff vector,  but as this
  // takes more computational time, the overall performance may not
  // be improved in actual run time. 
  //
  assert(node);
  const s_kd_tree_node_t *left = node->left;
  const  s_kd_tree_node_t *right = node->right;
  if ( (left == NULL) && (right == NULL)) {
    // we are on a terminal node
    if(true) {
      __MiF__startTimer();
      if (sr->conf.nn == 0) {
	s_kd_tree_node__process_terminal_node_fixedball(self, sr, node);
      } else {
	s_kd_tree_node__process_terminal_node(self, sr, node);
      }
      __MiF__updateResult_search(time_terminal_node);
    } else {
      // FIXME[concpetual]: merge [below] functions ..... then itnegrate/apply/add support 
      // FIMXE[tut+eval]: identify/cpature an aprpaoch to descirbe/elvauate perf-difference wrt. different strategies for such an eavluation-aprpaoch .... 
      assert(false); // FIXME[code]: ... 
      assert(false); // FIXME[code]: ... 
      assert(false); // FIXME[code]: ... 
    }
  } else { 
    assert(left != right);
    //! Then we first idneityf a feature-searhc-ectorer before cotninuting our data-search: 
    const s_kd_tree_node_t *ncloser, *nfarther;

    t_float extra = T_FLOAT_MAX;
    assert(node->cut_dim < self->mat_input->ncols);
    assert(sr->row_1);
    const t_float qval = sr->row_1[node->cut_dim]; 
    // FIXME[cocneptaul]: fiugre out how to handle value-pre-steps ... such as 'ranking'/soritng .... where for for the latter have a hacne wr.t the meaning/implciaton of each index-score ... 
    // FIXME[cocneptaul]: figure out how to handle the case/issue where "cut_dim" is Not set in the inptu-feature-vector ... 
    //assert(isOf_interest(qval)); //! ie, else add supprot for this case/issue.
    // value of the wall boundary on the cut dimension. 
    if (qval < node->cut_val) {
      ncloser = left;
      nfarther = right;
      if(isOf_interest(node->cut_val_right) && isOf_interest(qval)) {
	extra = node->cut_val_right - qval;
      } /* else { */
      /* 	assert(false); //! ie, then add supprot for this case/issue .... eg, udpate the split-point in the kd-tree-inti-rotuine ...  */
      /* } */
    } else {
      ncloser = right;
      nfarther = left;
      if(isOf_interest(node->cut_val_left) && isOf_interest(qval)) {
	extra = qval - node->cut_val_left; 
      } /* else { */
      /* 	assert(false); //! ie, then add supprot for this case/issue  .... eg, udpate the split-point in the kd-tree-inti-rotuine ...  */
      /* } */
    };

    // FIXME: write alterntive .... use iterative search .... where the "nfarther" clause is hanlded/attributed by ....??.... eg, where each child first test if .....??.... 
    if (ncloser != NULL) {search__kd_tree(self, ncloser, sr);}

    // FIXME[cocneptaul]: how correctly to handle/address the empty-value-case
    if ((nfarther != NULL) && (!isOf_interest(qval) || (macro_squared(extra) < sr->conf.ballsize)) ) {
      // first cut      
      bool isIn_range = false;
      //if(true) {
      if(sr->obj_metric == NULL) {
	isIn_range = box_in_search_range(nfarther, sr);
      } else { //! then we provide/add supprot for [”elow] "box_in_search_range(..)" wrt. 320+ sim-emtric-supprots for "dis_from_bnd(..)":
	//printf("## Use:obj_metric, at %s:%d\n", __FILE__, __LINE__);
	if(true) { //! then extract a feature-vector from the bounding-block and compotue teh dsitaicne using the latter.
	  t_float empty_0 = 0;
	  t_float *tmp_vec = allocate_1d_list_float((sr->dim), empty_0);
	  for (int i=0; i < sr->dim; i++) {	    
	    tmp_vec[i] = dis_from_bnd(sr->row_1[i], nfarther->box.list[i].head, nfarther->box.list[i].tail);
	  }
	  //! 
	  //! Compute distance:
	  t_float score = 0;
	  //#if(1 == 2)
	  // FIXME[code::perf::"kd_tree.c"]: ... 
	  // FIXME[code::perf::"kd_tree.c"]: ... test/meausre the 'perfrofrmance-imapct' of usign a 'driect' sium-metic-eahc-call to [”elow] .... ie, wrt, our 'one-to-many' distanc-ecall-logic .... 
	  const uint ncols = sr->dim;
	  //if(sr->obj_metric->typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) 
	  if( (sr->obj_metric != NULL) && sr->metric_each_nonRank  ) { //! then we use/apply a lsower proceudre using mroe idnidrect funciotn-calls.
	    //if(false) 
	    //	    {
	    
	    //void (*metric_oneToMany_nonRank) (config_nonRank_oneToMany_t) metric_oneToMany_nonRank = setmetric__correlationComparison__oneToMany(metric_id, self->weight, self->mask1, self->mask2, needTo_useMask_evaluation);
	    //! 
	    //! Compute the score:
	    //s_allAgainstAll_config_t config_metric_global; init__fast_s_allAgainstAll_config(&config_metric_global);
 	    config_nonRank_each_t config_metric; init__config_nonRank_each(&config_metric, ncols, tmp_vec, sr->row_1, /*self.weight=*/NULL, 
									   NULL,
									   NULL,
									   sr->config_metric_global);
	    assert(sr->metric_each_nonRank);
	    score = sr->metric_each_nonRank(config_metric);
	    //fprintf(stderr, "\n\n(info)\t masksAre_used='%u', at %s:%d\n", self.masksAre_used, __FILE__, __LINE__);
	    //!
	    //! The call:
	    // printf("(info)\t (start)\tCompute non-ranked correlation-matrix, where default power-value=%f, at %s:%d\n", self.configMetric_power, __FILE__, __LINE__);    
	    //printf("FIXME: include [”elow], at %s:%d\n", __FILE__, __LINE__);
	    //config_nonRank_oneToMany_t obj_config; init__config_nonRank_oneToMany(&obj_config, /*index1=*/0, /*nrows=*/1, /*ncols=*/ncols, sr->matShallow_2.matrix, data1, data2,  self->mask1, self->mask2,  self->weight, metric_id, e_typeOf_metric_correlation_pearson_undef, self, arrOf_result);      
	    //! The call:
	    //metric_oneToMany_nonRank(obj_config); //! ie, call the simlairty-metric-function
	    // postProcessingNeedsTo_beEvaluated = false; //! ie, as we asusem taht our 'gneriec' aprpaoch has handled this case
	  } else {
	    //assert(false);
	    //s_allAgainstAll_config_t self = sr->config_metric_global;
	    //#include "kt_compare__each.c" //! ie, the operation.
	  /*   t_float *row_cpy = allocate_1d_list_float((sr->dim), empty_0); */
	  /* for (int i=0; i < sr->dim; i++) {	    	     */
	    score = kt_compare__each__maskImplicit__useRowPointersAsInput(sr->obj_metric->metric_id, sr->obj_metric->typeOf_correlationPreStep, ncols, tmp_vec, sr->matShallow_2.matrix[0]); //sr->row_1);
	  }
	  //#endif

	  //assert(false); // FIXME: call a generic pre-fetcehd funciton-object .... use itni-preodcuree from our fixed-clsuter-alg-implmetantinon.
	  //!
	  //!
	  if(score <= sr->conf.ballsize) {isIn_range = true;}
	  //!
	  //! De-allocte:
	  free_1d_list_float(&tmp_vec);
	} else {
	  assert(false); // FIXME[conceptual]: .... SSE ... 
	  assert(false); // FIXME[code]: .... 
	  assert(false); // FIXME[code]: .... 
	}
      }
      if(isIn_range) { //, /*dim=*/self->mat_input->ncols)) {
	// FIXME[code::permtuation]: ... 
	// FIXME[code::permtuation]: ... recusion versus traversal .... 
	// FIXME[code::permtuation]: ... direct-macor-call versus funciton-call .... 
	//	nfarther->
	search__kd_tree(self, nfarther, sr); 
      }      
    }
  }
}

// ******************************************************************
// ******************************************************************
// ******************************************************************


// ***************************************'
// ***************************************'
// ***************************************'


static void search_oneToMany_brute__kd_tree(s_kd_searchConfig_t *sr, const t_float *row_1, const t_float ballsize, const uint nn_numberOfNodes, s_kt_list_1d_kvPair_t *result, const bool usePreComputed, const uint center_index) {
  assert(sr);
  s_kd_tree_t *self = sr->self;
  //! Note: correspodns to "kdtree2::n_nearest_brute_force(..)".
// FIXME[tut+perf]: ... evaluat the time-diff between this VS 'our' appraoch .... use result-dfiference as an arugment wrt. ifnesailbiy of usign/applying poor-perofmring appraoches for evaluating efficnecy of comptuer-arhdware ..... 
// FIXME[tut+perf]: ... 
  assert(result);
  assert(row_1);
  //free_s_kt_set_1dsparse_t(result);
  //allocate__simple__s_kt_set_1dsparse_t(result);
  //allocate__s_kt_set_1dsparse_t(result, /*ncols=*/self->mat_input->ncols, NULL, NULL, false, /*isTo_allocateFor_scores=*/true);
  //result.clear();
  uint cnt_added = 0;  
  if(usePreComputed) {
    assert(center_index != UINT_MAX);
    assert(sr->mat_preComputed.nrows > center_index);
    const t_float *vec_result = sr->mat_preComputed.matrix[center_index];
    assert(vec_result);
    const uint cnt_iter = sr->mat_preComputed.ncols;
    assert(cnt_iter > 0);
#define _score vec_result[i]
    if(ballsize != T_FLOAT_MAX) {
      for(uint i = 0; i < cnt_iter; i++) {
	if(isOf_interest(_score)) {
	  if(_score < ballsize) {
	    push__s_kt_list_1d_kvPair_t((result), MF__initVal__s_ktType_kvPair(/*vertex=*/i, _score));
	    cnt_added++;
	    //push__pair__s_kt_set_1dsparse_t(result, /*vertex=*/i, /*score=*/score);
	  }	  
	}
      }
    } else {
      for(uint i = 0; i < cnt_iter; i++) {
	if(isOf_interest(_score)) {
	  push__s_kt_list_1d_kvPair_t((result), MF__initVal__s_ktType_kvPair(/*vertex=*/i, _score));
	  cnt_added++;
	}
      }
    }
#undef _score
  } else if(sr->obj_metric != NULL) { //! then we intiate the searhc-wrapper object:
    //! Set the matrix:
    // FIXME: validte correnctess of [”elow] ... the to use a poeirnt-reference to 'emulate' a matrix-reference.
    const t_float empty_0 = 0;
    t_float *vec_copy = allocate_1d_list_float((self->mat_input->ncols), empty_0);
    assert(vec_copy);
    memcpy(vec_copy, row_1, sizeof(t_float)*(self->mat_input->ncols));
    sr->matShallow_2.matrix = &(vec_copy); 
    sr->matShallow_2.nrows = 1;
    sr->matShallow_2.ncols = self->mat_input->ncols;    
    //!
    //! Comptue teh score-matrix:
    s_kt_matrix_base_t mat_result = initAndReturn__empty__s_kt_matrix_base_t();
    const bool is_ok = apply__extensive__hp_distance(&(sr->objDist_1_toMany), &(sr->matShallow_2), &(sr->matShallow_1), &mat_result);
    assert(is_ok);
    const uint cnt_iter = self->mat_input->nrows; //! ie, as we have comptued for the vertices.
    assert(cnt_iter > 0);
    assert(mat_result.nrows == 1);
    assert(mat_result.ncols == cnt_iter);
#define _score mat_result.matrix[0][i]
    if(ballsize != T_FLOAT_MAX) {
      for(uint i = 0; i < cnt_iter; i++) {
	if(isOf_interest(_score)) {
	  printf("score[%u]=%f, thresh=%f, at %s:%d\n", i, _score, ballsize, __FILE__, __LINE__);
	  if(_score < ballsize) {
	    push__s_kt_list_1d_kvPair_t((result), MF__initVal__s_ktType_kvPair(/*vertex=*/i, _score));
	    cnt_added++;
	    //push__pair__s_kt_set_1dsparse_t(result, /*vertex=*/i, /*score=*/score);
	  }	  
	}
      }
    } else {
      for(uint i = 0; i < cnt_iter; i++) {
	if(isOf_interest(_score)) {
	  printf("score[%u]=%f, at %s:%d\n", i, _score, __FILE__, __LINE__);
	  push__s_kt_list_1d_kvPair_t((result), MF__initVal__s_ktType_kvPair(/*vertex=*/i, _score));
	  cnt_added++;
	}
      }
    }
#undef _score
    //!
    //! De-allcoate local matrix:
    free__s_kt_matrix_base_t(&mat_result);
    assert(vec_copy); free_1d_list_float(&vec_copy);
  } else {
    for(uint i=0; i< self->mat_input->nrows; i++) {
      t_float score = 0.0;
      //kdtree2_result e; 
      for (int j=0; j< self->mat_input->ncols; j++) {
	score += macro_squared( MiF__matrix[i][j] - row_1[j]);
      }
      if(score < ballsize) {
	push__s_kt_list_1d_kvPair_t((result), MF__initVal__s_ktType_kvPair(/*vertex=*/i, score));
	cnt_added++;
	//push__pair__s_kt_set_1dsparse_t(result, /*vertex=*/i, /*score=*/score);
      }
    }
  }
  //!
  //! Support a rank-based filter-prodcure:
  if( (nn_numberOfNodes != 0) && (nn_numberOfNodes != UINT_MAX) && (nn_numberOfNodes < cnt_added) ) {
    sort__s_kt_list_1d_kvPair_t((result));
    //! The filter the vertices which are outisde the threshold:
    // FIXME: validate correcntess of [”elow] expected sort-order .... alterntively reverse the removal-roder:
    for(uint i = nn_numberOfNodes; i < cnt_added; i++) {
      result->list[i] = MF__init__s_ktType_kvPair();
      assert(MF__isEmpty__s_ktType_kvPair(result->list[i])); //! ie, to reflect [above].
    }
    result->current_pos = nn_numberOfNodes;
  }
  //s_kt_set_1dsparse_t *obj_sparse = result;
  //! Sort the data-set (oekseth, 06. jul. 2017).
  // MF__sort_forScores__s_kt_set_1dsparse_t(result);
}

//! @returns a locally configured search-object, ie, which may be sued for muliple comptuation-calls.
s_kd_searchConfig_t init__fromConf__s_kd_searchConfig_t(s_kd_tree_t *self, s_kd_searchConfig_config_t conf, const s_kt_correlationMetric_t *obj_metric) {
//s_kd_searchConfig_t init__s_kd_searchConfig_t(const e_kd_tree_searchStrategy_t enum_id, s_kd_tree_t *self, const uint nn_numberOfNodes, const uint minDiff_indexRange_centerPoint, const t_float ballsize, const bool isTo_sort, const s_kt_correlationMetric_t *obj_metric) {
  //!
  //! Intilaise object: 
  //! Note: an alterntive to the "ballsize" paramter is to use the "nn_numberOfNodes", ie, where latter is sued as a rank-threshold wrt. dinetiiconat of best-fit nodes .... 
  s_kd_searchConfig_t obj_search = init__s_kd_searchConfig(self, NULL, NULL);
  s_kd_searchConfig_t *sr = &obj_search;
  sr->row_1 = NULL;
  sr->centeridx = UINT_MAX;
  sr->conf.enum_id = conf.enum_id;
  sr->conf.isTo_sort = conf.isTo_sort;
  sr->obj_metric = obj_metric;
  sr->mat_preComputed = initAndReturn__empty__s_kt_matrix_base_t();
  sr->conf.ballsize = conf.ballsize; 
  sr->conf.ballsize_input = conf.ballsize; //! ie, 'enalbe' a recet of the ballsize-proerpty for muliple/co-current 'runs'
  sr->conf.nn = conf.nn; //_numberOfNodes; 
  sr->metric_each_nonRank = NULL;
  if(obj_metric != NULL) {
    // TODO[€val]: consider instead to apply [”elow] for 'squared-euclid' (eosekth, 60. jul. 2017).
    if(obj_metric->metric_id == e_kt_correlationFunction_groupOf_minkowski_euclid) { //! then we sue the 'fast verison'.
      obj_metric = NULL;
      sr->obj_metric = NULL;
    }
  }
  sr->metric_each_nonRank = NULL;
  //! 
  //! 
  // printf("##(init), at %s:%d\n", __FILE__, __LINE__);
  if(obj_metric != NULL) { //! then we intiate the searhc-wrapper object:
    // printf("## Set-obj-metric, at %s:%d\n", __FILE__, __LINE__);
    //! 
    //! Investigate if the data-set ahs empty valeus, ie, to correctly cofngirue the comtpatuion-appraoch: 
    uint index_emptyValueRow = 0; bool isTo_useMask = false;
    for(uint row_id = 0; row_id < self->mat_input->nrows; row_id++) {
      uint cnt_emptyCells = 0;
      for(uint col_id = 0; col_id < self->mat_input->ncols; col_id++) {
	cnt_emptyCells += !isOf_interest(self->mat_input->matrix[row_id][col_id]);
      }
      if(cnt_emptyCells > 0) {index_emptyValueRow = row_id; isTo_useMask = true;}
    }
    if( (sr->obj_metric->typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) && !isTo_use_kendall__e_kt_correlationFunction(obj_metric->metric_id) && !isTo_use_MINE__e_kt_correlationFunction(obj_metric->metric_id) ) { //! 
      
      sr->metric_each_nonRank = setmetric__correlationComparison__each(sr->obj_metric->metric_id, /*self.weight=*/NULL, NULL, NULL, /*needTo_useMask_evaluation=*/isTo_useMask);
      if(sr->metric_each_nonRank == NULL) {
	fprintf(stderr, "## Set-obj-metric for metric_id=\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(obj_metric->metric_id), __FILE__, __LINE__);
      }
      assert(sr->metric_each_nonRank);
    } else { //! then the metirc is a rank-based metric, ie, for which we need to apply pre-call-sequence-steps.
      sr->metric_each_nonRank = NULL;
    }
    //    assert(sr->metric_each_nonRank);
    //! Intlaise matrix-rows:
    sr->matShallow_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(self->mat_input);
    //! 
    //! Set the second base-matrix: 
    sr->matShallow_2 = initAndReturn__empty__s_kt_matrix_base_t();
    // FIXME: validte correnctess of [”elow] ... the to use a poeirnt-reference to 'emulate' a matrix-reference.
    sr->matShallow_2.matrix = &(self->mat_input->matrix[index_emptyValueRow]);
    sr->matShallow_2.nrows = 1;
    sr->matShallow_2.ncols = self->mat_input->ncols;
    //! 
    //! Intitet the hp-config-object for both many-many and many-one:    
    sr->objDist_1_toMany = init__s_hp_distance(*obj_metric, isTo_useMask, &(sr->matShallow_2), &(sr->matShallow_1), init__s_hp_distance__config_t());
    sr->objDist_many_toMany = init__s_hp_distance(*obj_metric, isTo_useMask, &(sr->matShallow_1), NULL, init__s_hp_distance__config_t());
  }
#define __isCase(case_id) (case_id == conf.enum_id)
  if(__isCase(e_kd_tree_searchStrategy_undef) || __isCase(e_kd_tree_searchStrategy_n_nearest)) {
    sr->centeridx = UINT_MAX;
    sr->conf.correltime = 0;
    //    sr->nn = nn_numberOfNodes; 
  } else if(__isCase(e_kd_tree_searchStrategy_n_nearest_around_point)) {
    //sr->centeridx = center_index;
    sr->conf.correltime = conf.correltime; // minDiff_indexRange_centerPoint;
    //    sr->nn = nn_numberOfNodes; 
  } else if(__isCase(e_kd_tree_searchStrategy_r_nearest)) {
    // FIXME: figure out how the vdiff works .... 
    sr->centeridx = UINT_MAX;
    sr->conf.correltime = 0;
    sr->conf.nn = UINT_MAX; 
    sr->conf.ballsize = conf.ballsize; 
  } else if(__isCase(e_kd_tree_searchStrategy_r_count)) {
    sr->centeridx = UINT_MAX;
    sr->conf.correltime = 0;
    sr->conf.nn = UINT_MAX; 
    sr->conf.ballsize = conf.ballsize;
  } else if(__isCase(e_kd_tree_searchStrategy_r_nearest_around_point)) {
    //sr->centeridx = center_index;
    sr->conf.correltime = conf.correltime; ///*correltime=*/minDiff_indexRange_centerPoint;
    sr->conf.ballsize = conf.ballsize; 
    sr->conf.nn = UINT_MAX; 
  } else if(__isCase(e_kd_tree_searchStrategy_r_count_around_point)) {
    // sr->centeridx = center_index;
    sr->conf.correltime = conf.correltime; // /*correltime=*/minDiff_indexRange_centerPoint;
    //sr->correltime = correltime;
    sr->conf.ballsize = conf.ballsize; 
    sr->conf.nn = UINT_MAX; 
  } else if(__isCase(e_kd_tree_searchStrategy_brute_fast)) { 
    if(sr->obj_metric != NULL) { //! then we pre-comptue the matrix:
      //! 
      //! Then comptue an all-agaisnta-ll matrix:
      // FIXME: in [”elow] explcit add supprot for threshold-based comptuations ..... 
      const bool is_ok = apply__extensive__hp_distance(&(sr->objDist_many_toMany), &(sr->matShallow_1), NULL, &(sr->mat_preComputed));
      assert(is_ok);
      assert(sr->mat_preComputed.nrows > 0);
      assert(sr->mat_preComputed.ncols > 0);
    }
  }   
#undef __isCase
  return obj_search;
}

/**
   @brief constuct a sparse 2d-matrix from a KD-simlairty-metic-comtpatuion (oekseth, 06. aug. 2017).
   @param <mat_input> is the dense matrix to compteu for.
   @param <nn_numberOfNodes_min> is the minimum number of nodes for the data-set to be of interest: set to '0' or UINT_MAX to ingor ethis proieprty.
   @param <nn_numberOfNodes_max> is the max number of nodes to be used during data-sorting: set to UINT_MAX to ingore this proerpty-
   @param <ballsize_max> is the max-distance-size to be included: to T_FLOAT_MAX to ingore this property
   @param <obj_metric> is the simalrity-emtric to be used.
   @return the 2d-sparse-list of pair-simalirty-metric
 **/
s_kt_list_2d_kvPair_t get_sparse2d_fromKD__kd_tree(const s_kt_matrix_t *mat_input, const uint nn_numberOfNodes_min, const uint nn_numberOfNodes_max, const t_float ballsize_max, const s_kt_correlationMetric_t obj_metric) {
  //!
  //! Intiate:
  assert(mat_input); assert(mat_input->nrows);
  s_kt_list_2d_kvPair_t obj_result = init__s_kt_list_2d_kvPair_t(mat_input->nrows, 0);

  //    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
  s_kd_tree_t obj_kd = build_tree__s_kd_tree(mat_input);
  //    end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
  // ----------------- 
  //    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
  const uint nrows = mat_input->nrows;
  assert(nrows > 0);
  const uint cnt_search = mat_input->nrows;
  s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest_around_point, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  assert(cnt_search <= nrows);
  t_float score_global = 0;
  for(uint row_id = 0; row_id < cnt_search; row_id++) {
      s_kd_result result = init__s_kd_result_t(); 
      searchNode__s_kd_tree_t(&obj_search, NULL, row_id, &result);
      //! Get average-score:
      //      t_float score_local = 0; uint score_cnt = 0;
      bool isTo_add = true;
      if( (nn_numberOfNodes_min != 0) && (nn_numberOfNodes_min != UINT_MAX) ) { 
	uint cnt_interesting = 0;
	for(uint i = 0; i < result.result.list_size; i++) {
	  t_float score = result.result.list[i].tail;
	  if(isOf_interest(score)) { 
	    cnt_interesting++;
	  }
	}
	isTo_add = (cnt_interesting >= nn_numberOfNodes_min);
      }
      if(isTo_add) {
	for(uint i = 0; i < result.result.list_size; i++) {
	  t_float score = result.result.list[i].tail;
	  if(isOf_interest(score)) { 
	    push__s_kt_list_2d_kvPair_t(&obj_result, row_id, result.result.list[i]); // MF__initVal__s_ktType_kvPair(/*index=*/
	  }
	}
      }
      //! De-allocte:
      free__s_kd_result_t(&result);
    }
    //fprintf(stderr, "#! sum(total)=%f, at %s:%d\n", score_global, __FILE__, __LINE__);
    //end_time_measurement("search:brute:fast", 0); //! ie, stop the timer and write out the timiing-difference.
    free__s_kd_searchConfig_t(&obj_search);    
    free__s_kd_tree(&obj_kd);

    return obj_result;
}


/**
   @brief constuct a sparse 2d-matrix from a KD-simlairty-metic-comtpatuion (oekseth, 06. apr. 2018).
   @param <mat_1> is the dense matrix to compteu for.
   @param <mat_2> is the dense matrix to compteu for.
   @param <nn_numberOfNodes_min> is the minimum number of nodes for the data-set to be of interest: set to '0' or UINT_MAX to ingor ethis proieprty.
   @param <nn_numberOfNodes_max> is the max number of nodes to be used during data-sorting: set to UINT_MAX to ingore this proerpty-
   @param <ballsize_max> is the max-distance-size to be included: to to T_FLOAT_MAX to ingore this property
   @param <obj_metric> is the simalrity-emtric to be used.
   @param <score_merge>  the strategy to merge scores
   @param <scalar_result>  is the result which is idneifed.
 **/
void get_score_fromKD__kd_tree(const s_kt_matrix_t *mat_1, const s_kt_matrix_t *mat_2, const uint nn_numberOfNodes_min, const uint nn_numberOfNodes_max, const t_float ballsize_max, const s_kt_correlationMetric_t obj_metric, e_kd_tree_typeOf_scoreUnification_t score_merge, t_float *scalar_result) {
  if(score_merge == e_kd_tree_typeOf_scoreUnification_undef) {score_merge = e_kd_tree_typeOf_scoreUnification_sum;}
  //!
  //! Intiate:
  assert(mat_1); assert(mat_1->nrows);
  //s_kt_list_2d_kvPair_t obj_result = init__s_kt_list_2d_kvPair_t(mat_input->nrows, 0);

  //    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
  s_kd_tree_t obj_kd = build_tree__s_kd_tree(mat_1);
  //    end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
  // ----------------- 
  //    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
  const uint nrows = mat_1->nrows;
  assert(nrows > 0);
  uint cnt_search = mat_1->nrows;
  if(mat_2) {
    assert(mat_2->nrows > 0);
    cnt_search = mat_2->nrows;
  }
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest_around_point, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_n_nearest, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);

  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_n_nearest_around_point, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_count, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);

  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest_around_point, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_count_around_point, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_slow, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  //s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest_around_point, &obj_kd, /*nn_numberOfNodes=*/nn_numberOfNodes_max, UINT_MAX, /*ballsize=*/ballsize_max, /*isTo_sort=*/false, &obj_metric);
  assert(cnt_search <= nrows);
  t_float score_global = 0;
  if(score_merge == e_kd_tree_typeOf_scoreUnification_sum) {
    *scalar_result = 0;
  } else {
    assert(false); //! ie, then add support for this use case.
  }
  long long unsigned int cnt_interesting_edges = 0;

  const bool isTo_sumForEachRow = true;
  
  for(uint row_id = 0; row_id < cnt_search; row_id++) {
    t_float score_local = 0;
    s_kd_result result = init__s_kd_result_t(); 
      if(!mat_2 || (mat_1->matrix == mat_2->matrix) ) {
	searchNode__s_kd_tree_t(&obj_search, NULL, row_id, &result);
      } else {
	assert(mat_2);
	assert(row_id < mat_2->nrows);
	const t_float *row_2 = mat_2->matrix[row_id];
	assert(row_2);
	searchNode__s_kd_tree_t(&obj_search, row_2, row_id, &result);
      }
      //! Get average-score:
      //      t_float score_local = 0; uint score_cnt = 0;
      bool isTo_add = true;
      if( (nn_numberOfNodes_min != 0) && (nn_numberOfNodes_min != UINT_MAX) ) { 
	uint cnt_interesting = 0;
	for(uint i = 0; i < result.result.list_size; i++) {
	  t_float score = result.result.list[i].tail;
	  if(isOf_interest(score)) { 
	    cnt_interesting++;
	  }
	}
	isTo_add = (cnt_interesting >= nn_numberOfNodes_min);
      }
      if(isTo_add) {
	uint count_local = 0;
	t_float score_max = T_FLOAT_MIN_ABS;
	for(uint i = 0; i < result.result.list_size; i++) {
	  t_float score = result.result.list[i].tail;
	  if(isOf_interest(score)) { 
	    if(score_merge == e_kd_tree_typeOf_scoreUnification_sum) {
	      if(isTo_sumForEachRow == false) {
		*scalar_result += mathLib_float_abs(score);
		cnt_interesting_edges++;
	      } else {
		const t_float score_adj = mathLib_float_abs(score);
		score_max = macro_max(score_max, score_adj);
		score_local += score_adj;
		count_local++;
	      }
	    } else {
	      assert(false); //! ie, then add support for this use case.
	    }
	  }
	}
	if(isTo_sumForEachRow == true) {
	  if(score_local != 0) {
	    //t_float score_adj = (score_local / ((t_float)count_local * score_max)); //(t_float)count_local);
	    t_float score_adj = (score_local / score_max); //(t_float)count_local);
	    //	    t_float score_adj = (score_local / (t_float)count_local);
	    *scalar_result += score_adj;
	    //printf("score_adj=%f, at %s:%d\n", score_adj, __FILE__, __LINE__);
	    //	    printf("score_adj=%f, at %s:%d\n", score_adj, __FILE__, __LINE__);
	  }
	}
      }
      //! De-allocte:
      free__s_kd_result_t(&result);
  }
  //  printf("sum=%f, cnt(evalauted)=%d, given min=%d, max=%d, cnt_search=%u, at %s:%d\n", *scalar_result, (uint)cnt_interesting_edges, nn_numberOfNodes_min, nn_numberOfNodes_max, cnt_search, __FILE__, __LINE__);
  if(*scalar_result != 0) {
    if(isTo_sumForEachRow == false) {
      assert(cnt_interesting_edges != 0);
      *scalar_result = *scalar_result / (t_float)cnt_interesting_edges;
    } else {
      ;
    }
  }
  //fprintf(stderr, "#! sum(total)=%f, at %s:%d\n", score_global, __FILE__, __LINE__);
  //end_time_measurement("search:brute:fast", 0); //! ie, stop the timer and write out the timiing-difference.
  free__s_kd_searchConfig_t(&obj_search);    
  free__s_kd_tree(&obj_kd);

    //return obj_result;
}



//! The main-funciton for data-searhcing.
void searchNode__s_kd_tree_t(s_kd_searchConfig_t *searchObject, const t_float *row_1, uint center_index, s_kd_result *result) {
  //void searchNode__s_kd_tree_t(e_kd_tree_searchStrategy_t enum_id, s_kd_tree_t *self, const t_float *row_1, const uint center_index, const uint nn_numberOfNodes, const uint minDiff_indexRange_centerPoint, const t_float ballsize, const bool isTo_sort, s_kd_result *result) {  
  //!
  //! Intilaise object: 
  //! Note: an alterntive to the "ballsize" paramter is to use the "nn_numberOfNodes", ie, where latter is sued as a rank-threshold wrt. dinetiiconat of best-fit nodes .... 
  s_kd_searchConfig_t *sr = searchObject;
  //#if(0 == 1)
  sr->row_1 = row_1;
  sr->result = result;
  sr->conf.ballsize = sr->conf.ballsize_input; //! ie, to recent any repvisou-resutls.
#define __isCase(case_id) (case_id == enum_id)
  e_kd_tree_searchStrategy_t enum_id = sr->conf.enum_id;
  if(__isCase(e_kd_tree_searchStrategy_brute_fast) &&  (sr->obj_metric == NULL) ) {
    enum_id = e_kd_tree_searchStrategy_brute_slow; //! ie, to simplify our logic-appraoch.
  } else if(__isCase(e_kd_tree_searchStrategy_brute_fast) && (row_1 != NULL) ) { //! then we need to investiagte fit eh row-id is an internal row, ie, if we have areldy pre-comptued for the latter.
    bool is_found = false;
    for(uint row_id = 0; (row_id < sr->self->mat_input->nrows) && (is_found == false); row_id++) {    
      if(row_1 == sr->self->mat_input->matrix[row_id]) { //! ie, compare the pointers.
	is_found = true;
	center_index = row_id;
      }      
    }
    if(is_found == false) { // then we ahve Not pre-comptued for the given row-index-set:
      enum_id = e_kd_tree_searchStrategy_brute_slow;
    } 
  }
  if(row_1 == NULL) {
    assert(center_index < sr->self->mat_input->nrows);
    if(center_index >= sr->self->mat_input->nrows) {
      fprintf(stderr, "!!\t The requreested row-index was outside the range of defined rows, ie, [%u] >= |%u|. Please fix the latter issue. Observation at [%s]:%s:%d\n", center_index, sr->self->mat_input->nrows, __FUNCTION__, __FILE__, __LINE__);
      return;
    } else {
      row_1 = sr->self->mat_input->matrix[center_index];
    }
  }
  sr->row_1 = row_1;
  assert(row_1);
  t_float *vec_copy = NULL;
  if(row_1) {
    const t_float empty_0 = 0;
    vec_copy = allocate_1d_list_float((sr->self->mat_input->ncols), empty_0);
    assert(vec_copy);
    memcpy(vec_copy, sr->row_1, sizeof(t_float)*(sr->self->mat_input->ncols));
    sr->matShallow_2.matrix = &(vec_copy); 
    sr->matShallow_2.nrows = 1;
    sr->matShallow_2.ncols = sr->self->mat_input->ncols;    
  }

  const bool isTo_sort = sr->conf.isTo_sort;
  //!
  //! Identify the case: 
  bool __isApplied = false;
  /* if(__isCase(e_kd_tree_searchStrategy_undef) || __isCase(e_kd_tree_searchStrategy_n_nearest)) { */
  /* } else  */
  if(__isCase(e_kd_tree_searchStrategy_n_nearest_around_point)) {
    sr->centeridx = center_index;
  } else if(__isCase(e_kd_tree_searchStrategy_r_nearest_around_point)) {
    sr->centeridx = center_index;
  } else if(__isCase(e_kd_tree_searchStrategy_r_count_around_point)) {
    sr->centeridx = center_index;
  } else if(__isCase(e_kd_tree_searchStrategy_brute_slow)) {
    //!
    //!
    search_oneToMany_brute__kd_tree(sr, row_1, sr->conf.ballsize, sr->conf.nn, &(result->result), /*usePreComputed=*/false, center_index);
    //!
    //! Mark as 'applied':
    __isApplied = true;
  } else if(__isCase(e_kd_tree_searchStrategy_brute_fast)) {
    //!
    //!
    search_oneToMany_brute__kd_tree(sr, row_1, sr->conf.ballsize, sr->conf.nn, &(result->result), /*usePreComputed=*/true, center_index);
    //!
    //! Mark as 'applied':
    __isApplied = true;
    //} else if(__isCase()) {
  /* } else { */
  /*   fprintf(stderr, "!!\t Add support for this option, at %s:%d\n", __FILE__, __LINE__); */
  /*   assert(false); */
  }
  //! 
  if(__isApplied == false) { //! Apply logics:
    assert(sr->self->root);
    // printf("(start-search)\t at %s:%d\n", __FILE__, __LINE__);
    search__kd_tree(sr->self, sr->self->root, sr);
  }

  if(isTo_sort) { //! thne sort the data-set:
    sort__s_kt_list_1d_kvPair_t(&(result->result));
  }
  if(vec_copy) { free_1d_list_float(&vec_copy); vec_copy = NULL;}
#undef __isCase
}
//! Allocate 'from sacratch' a kd-tree using a kd-tree-default-appraoch.
s_kd_searchConfig_t init_KD__s_kd_searchConfig_t(const s_kt_matrix_t *mat_input, const uint nn_numberOfNodes, const t_float ballsize, s_kt_correlationMetric_t obj_metric) {
  s_kd_tree_t self = __build_tree__s_kd_tree(mat_input, /*isTo_buildKD=*/true); //! ie, as we are to use  KD-tree.
  if(isEmpty__s_kt_correlationMetric_t(&obj_metric)) {
    obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
  }
  // FIXME[tut+perf]: evlauate differnet combaiotns wrt. [”elow] .... ie, find the best-fit defualt setting for kd-tree .... eg, by generating a table simlairt to oru "./results/differentHyp/dataRealKT_tut_whyResultsDifferskMeans__hcaPreStep_simMetricResult_minMaxsummary_norm_3.tsv" result-fiel were each kd-trree-alterntive is agivne a serpate latex-table-column.
  s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_count_around_point, &self, nn_numberOfNodes, UINT_MAX, ballsize, /*isTo_sort=*/false, &obj_metric);
  obj_search.self_isOwnedByThis = true;
  obj_search.self_base = self;
  //!
  //! @return
  return obj_search;
}

//! Comptue DB-scan givent he spficion given in the cofnig-object and thereafter return a mapping between each vertex and the clsuter-id the vertes is assited to (oekseth, 06. jul. 2017).
s_kt_list_1d_uint_t dbScan__s_kd_searchConfig_t(s_kd_searchConfig_t *config, const uint config_minCnt) {
  assert(config); assert(config->self); assert(config->self->mat_input);
  assert(config->self->mat_input->nrows);
  //#if(0 == 1) 
// FIXME: incldue [”ellow] wehn we have agreed/conlcued wrt. the correct aprpaoch/understnaidng of DB-SCAN and kd-trees ......
// FIXME[cocneptaul]: use  [below] to descirbe hwy the un-symmetirc proerpty of dB-scan .... results in ernrorus/unrepdciabilie reuslt-clsuters ... 
  s_kt_list_1d_uint_t map_clusterId = init__s_kt_list_1d_uint_t(config->self->mat_input->nrows);
  s_kt_list_1d_uint_t map_isVisisted = init__s_kt_list_1d_uint_t(config->self->mat_input->nrows);
  s_kt_list_1d_uint_t map_isNoise = init__s_kt_list_1d_uint_t(config->self->mat_input->nrows);
 const uint nrows = config->self->mat_input->nrows;
 for(uint row_id = 0; row_id < nrows; row_id++) {
   map_clusterId.list[row_id] = UINT_MAX;
   map_isVisisted.list[row_id] = false;
   map_isNoise.list[row_id] = false;
 }
 //!
 //! Iterate: 
uint current_cluster_id = 0;
 for(uint row_id = 0; row_id < nrows; row_id++) {
   if(map_isVisisted.list[row_id] == false) {
    map_isVisisted.list[row_id] = true;
    //! 
    //! Get the neasrest-poitns:
    s_kd_result result = init__s_kd_result_t(); 
    searchNode__s_kd_tree_t(config, NULL, row_id, &result);

    
    if(result.result.current_pos < config_minCnt) {
      map_isNoise.list[row_id] = true;
    } else { //! then we beign a new cluster-partion:
      s_kt_list_1d_uint_t arrOf_points = init__s_kt_list_1d_uint_t(result.result.current_pos);
      //! Iterate through the points:
      uint cnt_added = 0;
      for(uint i = 0; i < result.result.current_pos; i++) {
	const uint row_id_2 = result.result.list[i].head;
	//assert(row_id_2 < nrows);
	if(row_id_2 < nrows) {
	  if(map_isVisisted.list[row_id_2] == false) {
	    push__s_kt_list_1d_uint_t(&arrOf_points, row_id_2);
	    cnt_added++;
	  }
	}
      }
      assert(cnt_added == arrOf_points.current_pos);
      //! De-alloate the input-list:
      free__s_kd_result_t(&result);
      //#if(0 == 1)
      //! 
      //! Iterate through the points:
      while(arrOf_points.current_pos > 0) {
	//! Note: [below] is teh potins which were below the user-speific threshodl.
	// FIXME: is it psosible to colelct vertex-ids seperately .... ie, without the use of a heap-strucutre-overhead?? <-- consider usign a new/extra suture ... to hodl the vertex-ids .... 
	const uint row_id_2 = pop__s_kt_list_1d_uint_t(&arrOf_points); // result.result.list[i].head;
	if(row_id_2 != UINT_MAX) {
	  assert(row_id_2 < nrows);
	  if(map_isVisisted.list[row_id_2] == false) {
	    map_isVisisted.list[row_id_2] = true;
	    //! Get the neasrest-poitns:
	    s_kd_result result_2 = init__s_kd_result_t(); 
	    searchNode__s_kd_tree_t(config, NULL, row_id_2, &result_2);
	    //!
	    //!
	    if(result_2.result.current_pos >= config_minCnt) {	  
	      //!
	      //! Join
	      for(uint k = 0; k < result_2.result.current_pos; k++) {
		const uint row_id_3 = result_2.result.list[k].head;
		assert(row_id_3 != UINT_MAX);
		if(row_id_3 < nrows) {
		//assert(row_id_3 < nrows);
		  if(map_isVisisted.list[row_id_3] == false) {
		    push__s_kt_list_1d_uint_t(&arrOf_points, row_id_3);
		  }
		}
	      }
		//result.push(row_id_3);
	    }
	    //! De-alloate the input-list:
	    free__s_kd_result_t(&result_2);
	  }
	  //! If not alraedy assigned the a clsuter then set the clsuter-id:
	  if(map_clusterId.list[row_id_2] == UINT_MAX) { //! then set the clsuter-id:
	    map_clusterId.list[row_id_2] = current_cluster_id;
	    map_isNoise.list[row_id] = false;
	    // push(arrOf_points, row_id_2);
	  }
	  
	}
      }
      //#endif
      free__s_kt_list_1d_uint_t(&arrOf_points);
      //!
      //! Increement the clsuter-id.
      current_cluster_id++;
    }    
   }
 }
 //! De-allocate:
 free__s_kt_list_1d_uint_t(&map_isVisisted);
 free__s_kt_list_1d_uint_t(&map_isNoise);
 //! @return
 return map_clusterId;
}
// #endif // FIXME: remove.

/**
   @brief comptue clsuters through the DB-sCAN algroithm without applcaiton of cluster-strapping (oekseht, 06. aug. 2017).
   @param <obj_adjcency> is the sparse list of adjcency-verteices we use.
   @return the list of "list[vertex]=clust-id" memerbships
   @remarks 
 **/
s_kt_list_1d_uint_t search__relationsFromDataStruct__kd_tree(s_kt_list_2d_kvPair_t *obj_adjcency) { //, const t_float ballsize, const uint nn_numberOfNodes) {
  assert(obj_adjcency);
  assert(obj_adjcency->list_size > 0);
  /* assert(config); assert(config->self); assert(config->self->mat_input); */
  /* assert(config->self->mat_input->nrows); */
  //#if(0 == 1) 
// FIXME: incldue [”ellow] wehn we have agreed/conlcued wrt. the correct aprpaoch/understnaidng of DB-SCAN and kd-trees ......
// FIXME[cocneptaul]: use  [below] to descirbe hwy the un-symmetirc proerpty of dB-scan .... results in ernrorus/unrepdciabilie reuslt-clsuters ... 
  //! Idneitfy the maximum number of rows:
  uint nrows = 0;
  loint cnt_intersting = 0;
  for(uint row_id = 0; row_id < obj_adjcency->list_size; row_id++) {
    const uint cnt_max = obj_adjcency->list[row_id].list_size;
    if(cnt_max > 0) {
      loint cnt_intersting_local = 0;
      for(uint i = 0; i < cnt_max; i++) {
	if(false == MF__isEmpty__s_ktType_kvPair(obj_adjcency->list[row_id].list[i])) {
	  const uint tail_id = obj_adjcency->list[row_id].list[i].head;
	  assert(tail_id != UINT_MAX);
	  nrows = macro_max(nrows, tail_id);
	  cnt_intersting_local++;
	}
      }
      if(cnt_intersting_local > 0) {
	nrows = macro_max(nrows, row_id);
	cnt_intersting++;
      }
    }
  }
  if(cnt_intersting == 0) {
    return init__s_kt_list_1d_uint_t(0); //! ie, as the data-set then did Not hold any itneresting relationships.
  }
  nrows++; //! ie, incrmeent past the last-index.

  s_kt_list_1d_uint_t map_clusterId = init__s_kt_list_1d_uint_t(nrows);
  s_kt_list_1d_uint_t map_isVisisted = init__s_kt_list_1d_uint_t(nrows);
  s_kt_list_1d_uint_t map_isNoise = init__s_kt_list_1d_uint_t(nrows);
  // const uint nrows = config->self->mat_input->nrows;
 for(uint row_id = 0; row_id < nrows; row_id++) {
   map_clusterId.list[row_id] = UINT_MAX;
   map_isVisisted.list[row_id] = false;
   map_isNoise.list[row_id] = false;
 }
 //!
 //! Iterate: 
uint current_cluster_id = 0;
 for(uint row_id = 0; row_id < nrows; row_id++) {
// for(uint row_id = 0; row_id < obj_adjcency->list_size; row_id++) {
   if(map_isVisisted.list[row_id] == false) {
    map_isVisisted.list[row_id] = true;
    //! 
    //! Get the neasrest-poitns:

    /* s_kd_result result = init__s_kd_result_t();  */
    /* searchNode__s_kd_tree_t(config, NULL, row_id, &result); */

    
    /* if(result.result.current_pos < config_minCnt) { */
    /*   map_isNoise.list[row_id] = true; */
    /* } else { //! then we beign a new cluster-partion: */
    {
      s_kt_list_1d_uint_t arrOf_points = init__s_kt_list_1d_uint_t(100); //result.result.current_pos);
      //! Iterate through the points:
      uint cnt_added = 0;
      const uint cnt_max = obj_adjcency->list[row_id].list_size;
      for(uint i = 0; i < cnt_max; i++) {
	if(false == MF__isEmpty__s_ktType_kvPair(obj_adjcency->list[row_id].list[i])) {
	  //	  for(uint i = 0; i < result.result.current_pos; i++) {
	  const uint row_id_2 = obj_adjcency->list[row_id].list[i].head;
	  //	  const uint row_id_2 = result.result.list[i].head;
	  assert(row_id_2 < nrows);
	  if(map_isVisisted.list[row_id_2] == false) {
	    push__s_kt_list_1d_uint_t(&arrOf_points, row_id_2);
	    cnt_added++;
	  }
	}
      }
      //	assert(cnt_added == arrOf_points.current_pos);
      //! De-alloate the input-list:
      //free__s_kd_result_t(&result);
      //#if(0 == 1)
      //! 
      //! Iterate through the points:
      while(arrOf_points.current_pos > 0) {
	//! Note: [below] is teh potins which were below the user-speific threshodl.
	// FIXME: is it psosible to colelct vertex-ids seperately .... ie, without the use of a heap-strucutre-overhead?? <-- consider usign a new/extra suture ... to hodl the vertex-ids .... 
	const uint row_id_2 = pop__s_kt_list_1d_uint_t(&arrOf_points); // result.result.list[i].head;
	if(row_id_2 != UINT_MAX) {
	  assert(row_id_2 < nrows);
	  if(map_isVisisted.list[row_id_2] == false) {
	    map_isVisisted.list[row_id_2] = true;
	    /* //! Get the neasrest-poitns: */
	    /* s_kd_result result_2 = init__s_kd_result_t();  */
	    /* searchNode__s_kd_tree_t(config, NULL, row_id_2, &result_2); */
	    //!
	    //!
	    //	    if(result_2.result.current_pos >= config_minCnt) 
	    {	  
	      //!
	      //! Join
	      const uint cnt_max_2 = obj_adjcency->list[row_id_2].list_size;
	      for(uint k = 0; k < cnt_max_2; k++) {
		if(false == MF__isEmpty__s_ktType_kvPair(obj_adjcency->list[row_id_2].list[k])) {
		  //	  for(uint i = 0; i < result.result.current_pos; i++) {
		  const uint row_id_3 = obj_adjcency->list[row_id_2].list[k].head;
		/*   for(uint k = 0; k < result_2.result.current_pos; k++) { */
		/* const uint row_id_3 = result_2.result.list[k].head; */
		  assert(row_id_3 != UINT_MAX);
		  assert(row_id_3 < nrows);
		  if(map_isVisisted.list[row_id_3] == false) {
		    push__s_kt_list_1d_uint_t(&arrOf_points, row_id_3);
		  }
		}
		//result.push(row_id_3);
	      }
	    //! De-alloate the input-list:
	      //	    free__s_kd_result_t(&result_2);
	    }
	    //! If not alraedy assigned the a clsuter then set the clsuter-id:
	    if(map_clusterId.list[row_id_2] == UINT_MAX) { //! then set the clsuter-id:
	      map_clusterId.list[row_id_2] = current_cluster_id;
	      map_isNoise.list[row_id] = false;
	      // push(arrOf_points, row_id_2);
	    }
	    
	  }
	}
      }
      //#endif
      free__s_kt_list_1d_uint_t(&arrOf_points);
      //!
      //! Increement the clsuter-id.
      current_cluster_id++;
    }  
   }
 }
 //! De-allocate:
 free__s_kt_list_1d_uint_t(&map_isVisisted);
 free__s_kt_list_1d_uint_t(&map_isNoise);
 //! @return
 return map_clusterId;
}
