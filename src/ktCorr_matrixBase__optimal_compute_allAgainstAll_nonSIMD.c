/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "correlation_macros__distanceMeasures_wrapper.h"

/**
   @file ktCorr_matrixBase__optimal_compute_allAgainstAll_nonSIMD
   @brief a slow 'inner aprt' of a fucntion hwihc is used to ivnesitgate the improtance of our SSE-tempalte-funciton wrt. perfomrqance.
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/
{ //! the 'inner part' of the funcitons: for details please see the caller fo this 'funciton'.
  // FIXME: as a future wok try to describe "https://en.wikipedia.org/wiki/Strassen_algorithm" wrt. matrix-operations ... eg, wrt. "http://www.cquestions.com/2011/09/strassens-matrix-multiplication-program.html" ... "http://math.ewha.ac.kr/~jylee/SciComp/sc-crandall/strassen.c" ...  "https://github.com/sangeeths/stanford-algos-1/blob/master/strassen-recursive-matrix-multiplication.c" <-- first ask Jan-Christain to evaluate/'suggest' the perofmrance-benefit.
  //! Indefy the 'boundaries' of our operation:
  const char *__restrict__ rmask1 = NULL; const char *__restrict__ rmask2 = NULL; t_float *__restrict__ rres;  const t_float *__restrict__ rmul1; const t_float *__restrict__  rmul2; 
  assert(config_allAgainstAll->CLS > 0); 
  //printf("CLS=%u, at %s:%d\n", CLS, __FILE__, __LINE__);
  const uint SM = (uint)(config_allAgainstAll->CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  assert(nrows > 0); assert(ncols > 0); assert(SM > 0);
  uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);
  uint cnt_iterations = 0; const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM; 

  assert(config_allAgainstAll->isTo_use_continousSTripsOf_memory); //! ie, what we expect.
  if(config_allAgainstAll->isTo_use_continousSTripsOf_memory == false) {
    fprintf(stderr, "!!\t Your memory-allcoation rotuien were not as effective as we had expected, ie, please update your allcoation-rotuines: nwo drop the comptuation, ie, your call is pointless. For questions, please cotnact the authro at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  
  //! Note: [”elow] two chunks are a copy-paste of the euclidiaon algorithm, ie, with difference of the 'summations' to use.
  if(transpose == 0) {
  
    // // FIXME: set [”elow] to false.
    // if(false) {
    //   t_float sum = 0;
    //   for(uint index1 = 0; index1 < nrows; index1++) {
    // 	for(uint index2 = 0; index2 < nrows; index2++) {
    // 	  for(uint i = 0; i < ncols; i++) {
    // 	    sum += resultMatrix[index1][index2];
    // 	  }
    // 	}
    //   }
    // }
    
    // const uint total_cnt_iterations = nrows * ncols * ncols;   uint debug_cnt_iterations = 0;
    // const t_float *__restrict__ pointer_dataLastPos1 = (isTo_use_continousSTripsOf_memory) ? data1[0] + total_cnt_iterations : NULL; //! which is used to validate that we do not overwrite memory.
    // const t_float *__restrict__ pointer_resultLastPos1 = (isTo_use_continousSTripsOf_memory) ? resultMatrix[0] + total_cnt_iterations : NULL; //! which is used to validate that we do not overwri

    //! Iterate:
    // FIXME: try to improve [”elow] parallisation
    uint index1 = 0;
    //! Handle the case where the 'second tieration' is in a different name-space (eg, comaprsion of two different matrices as seen in the "kemans" algorithm).
    if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = nrows;}
    uint numberOf_chunks_rows_2 = config_allAgainstAll->iterationIndex_2 / SM;  
    uint chunkSize_row_last_2 = SM; __get_updatedNumberOF_lastSizeChunk_entry(config_allAgainstAll->iterationIndex_2, SM, &numberOf_chunks_rows_2, &chunkSize_row_last_2);
    
    //#pragma omp parallel for schedule(guided)
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
    //for(uint cnt_index1 = 0; cnt_index1 < numberOf_chunks_rows; cnt_index1++) {
    for(uint cnt_index1 = 0; cnt_index1 < numberOf_chunks_rows; cnt_index1++, index1 += SM) {
      const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_rows) ? SM : chunkSize_row_last;
      //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
      uint numberOf_chunks_rows_local = 0; uint chunkSize_row_last_local = 0;  
      if(data1 == data2) {
	__get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, &numberOf_chunks_rows_local, &chunkSize_row_last_local);
      } else { //! else the matrix is assumed not to be symmetric.
	numberOf_chunks_rows_local = numberOf_chunks_rows_2; chunkSize_row_last_local = chunkSize_row_last_2;
      }
      for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_rows_local; cnt_index2++, index2 += SM) {
	const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_rows_local) ? SM : chunkSize_row_last_local;
	uint i = 0;	
	if(!mask1 || !mask2) { //! then we asusem that 'unused' columsn are described/prerenseted by "T_FLOAT_MIN_ABS" or "FLT_MAX"
	  for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
	    const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; 
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    //assert(resultMatrix[index1]);
	    //const uint debug_startPos_rres = (index1 * ncols) + index2;
	    // assert(resultMatrix + debug_startPos_rres);
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[index1][i];
		 i2 < chunkSize_index1;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		   rres += ncols,  rmul1 += ncols
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {
	      // assert(rres <= pointer_resultLastPos1);
	      // assert( ((rres + ncols*chunkSize_index1) -1) <= pointer_resultLastPos1);

	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      
	      uint k2 = 0; 
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[index2][i];  
		   k2 < chunkSize_index2; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmul2 += ncols
		   ) {				

		//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		// _mm_prefetch (&data2[8], _MM_HINT_NTA);
		
		//! The operation:		
		for (uint j2 = 0; j2 < chunkSize_cnt_i; ++j2) {
		  if(isOf_interest(rmul1[j2]) && isOf_interest(rmul2[j2]))  {
		    // TODO: update our documetnation wwtih [below] observaiton.
		    //! Note: the 'only' difference between euclidaion, city-block and correlation is (a) the muliplicaiton-valeu to use in comparion of cells, and (b) teh post-processing-routines.
		    const t_float result_tmp = __macro__get_distance(rmul1[j2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric); 

		    // const uint debug_currentPos = debug_startPos_rres + k2 + (i2*ncols);
		    // //assert( debug_currentPos < (nrows * nrows));
		    // assert(debug_cnt_iterations <= total_cnt_iterations);
		    // FIXME: include [below]
		    rres[j2] += result_tmp;
		  }
		}
	      }
	    }
	  }
	} else { //! Test for the case where "mask" is set/used
	  for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
	    const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; 
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[index1][i], rmask1 = &mask1[index1][i];
		 i2 < chunkSize_index1;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		   rres += ncols, 
		   rmask1 += ncols, rmul1 += ncols
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {
	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      _mm_prefetch (&mask1[8], _MM_HINT_NTA);
	      
	      uint k2 = 0; 
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[index2][i], 
		     rmask2 = (!mask2) ? NULL : &mask1[index2][i];
		   k2 < chunkSize_index2; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmask2 +=  ncols, rmul2 += ncols
		   ) {				
		//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		// _mm_prefetch (&data2[8], _MM_HINT_NTA);
		
		//! The operation:
		for (uint j2 = 0; j2 < chunkSize_cnt_i; ++j2) {
		  if(rmask1[j2] && mask2[j2]) {	
		    // TODO: update our documetnation wwtih [below] observaiton.
		    //! Note: the 'only' difference between euclidaion, city-block and correlation is (a) the muliplicaiton-valeu to use in comparion of cells, and (b) teh post-processing-routines.
		    rres[k2] += __macro__get_distance(rmul1[j2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
		  }
		}
	      }
	    }
	  }
	}
      }
    }

  } else {
    
    //! Note: the idea (in the comptuation of the distance in a transposed matrix) is to 'move' "i" and "index1" vertical and "index1"  along the vertical axis. The implicaiton is that 'the sum of columns' is paritally updated for each row: instead of seperately coputing "[i][index1] x [i][index2]" for each pair of ("index1", "index2"), we for each "i" update ("index1", "index2"). The implication is that we accessess different column-index for "data1" and "data2": otherwise the 'space' [data2[i][index1] .... data2[i][index2]] would never have been fixed, ie, we would not have computed an 'all-against-all-sum'. From the latter we infer/observe that "index1" may be 'seen' as 'axis0' in the interprreation/comptuation, while "index" is interpreted as 'axix2' (in the comptuation/'loop'). 
    //! Note(2): to understand why "j2" is used to uupdate/identify "resultMatrix[index1][index2]" and "data1[i][index1]" note that if "k2" had been used, then we would have computed the 'non-transposed' matrix, ie, the results would have been wrong: in order to update different resultMatrix[index1][index2] we need to 'shift' (the columns) between each new-identifed value, hence the need to use the inner "j2" counter. Wrt. the "rmul1[j2]" we observe that the counters need to be different (between "rmul1" and "rmul2"), ie, as we otehrwise would compute ....
    //! Note(3): in this approxh we order the for-loops based on comparison of results from "e_typeOf_optimization_distance_none_xmtOptimizeTransposed" and "e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal"
    // FIXME: update [abov€] and [”elow] based on results from measurements of [ªbov€] (eg, wrt. "e_typeOf_optimization_distance_none_xmtOptimizeTransposed").
    bool debug_lastRow_seen = false;
    const uint total_cnt_iterations = nrows * ncols * ncols;
    //! Iterate:

    // FIXME: update [below] adn [ªbove] by 'computing only for index2 <= index1'

    // FIXME[jan-christian]: may you verify correctness of [below] 'euclidiaon transpsoe through memory-tiling'?

    //! Handle the case where the 'second tieration' is in a different name-space (eg, comaprsion of two different matrices as seen in the "kemans" algorithm).
    if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = ncols;}
    uint numberOf_chunks_cols_2 = config_allAgainstAll->iterationIndex_2 / SM;  
    uint chunkSize_col_last_2 = SM; __get_updatedNumberOF_lastSizeChunk_entry(config_allAgainstAll->iterationIndex_2, SM, &numberOf_chunks_cols_2, &chunkSize_col_last_2);


    // FIXME: try to improve [”elow] parallisation
    //#pragma omp parallel //for schedule(dynamic,3) //! where "3" is based on observaitons
    for(uint cnt_i = 0, i=0; cnt_i < numberOf_chunks_rows; cnt_i++, i += SM)  {
      const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_rows) ? SM : chunkSize_row_last;
	
      if(chunkSize_cnt_i != SM) {
	if(false) {printf("(info)\t last-row for i=%u, cnt_i=%u, and numberOf_chunks_rows=%u, at %s:%d\n", i, cnt_i, numberOf_chunks_cols, __FILE__, __LINE__);}
	assert(debug_lastRow_seen == false); debug_lastRow_seen = true;
      }
      if(!mask1 || !mask2) { //! then we asusem that 'unused' columsn are described/prerenseted by "T_FLOAT_MIN_ABS" or "FLT_MAX"
	for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_cols; cnt_index1++, index1 += SM) {
	  const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	  //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
	  uint numberOf_chunks_cols_local = 0; uint chunkSize_cols_last_local = 0;  
	  if(data1 == data2) {
	    __get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, &numberOf_chunks_cols_local, &chunkSize_cols_last_local);
	  } else { //! else the matrix is assumed not to be symmetric.
	    numberOf_chunks_cols_local = numberOf_chunks_cols_2; chunkSize_cols_last_local = chunkSize_col_last_2;
	  }
	  for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_cols_local; cnt_index2++, index2 += SM) {
	    const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_cols_local) ? SM : chunkSize_cols_last_local;
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    /**
	       Note: similar to for "transposed == 0" we find this procedure/code rather complex/non-intutive. To help the understanding (and identificaiton of bugs: sadly it is diffuclt to rwrite code without any bugs, eg, wrt. splelling errors).  In below code we:
	       ----------------------------------------------------------------------------------------
	       data1-row[index1 + i2*ncols] ... column[index2] | | | | | | column[index2 + chunkSize_index1] 
	       .                        result[index1+0][...]          +=    data2-row[index1 + 0]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+1][...]          +=    data2-row[index1 + 1]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+2][...]          +=    data2-row[index1 + 2]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+k2][...]         +=    data2-row[index1 + k2]    ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       ----------------------------------------------------------------------------------------
	       From the above depidction of 'comptuation for each matrix-tile' we infer that the column-index for "result" is be the same as the column-index in is to be the same for "result" as for "data2": when 'overlapping' the rows of "data1" and "data2" "index2" is used in "result" to update the 'current estimates/comptuations'.
	    **/
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[i][index1], rmask1 = (!mask1) ? NULL : &mask1[i][index1];
		 i2 < chunkSize_cnt_i;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"		   
		   rres += ncols, rmul1 += ncols, //! ie, as the 'inner index' for both res and mul1 is the same.		   
		   rmask1 += (!rmask1) ? 0 : ncols
		 
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {

	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      if(mask1) {_mm_prefetch (&mask1[8], _MM_HINT_NTA);}
	      
	      uint k2 = 0; 
	      assert(index2 < ncols); //! ie, given [below]
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[i][index2], 
		     rmask2 = (!mask2) ? NULL : &mask1[i][index2];
		   k2 < chunkSize_index1; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmask2 += (!rmask2) ? 0 : ncols, rmul2 += ncols
		   ) {		
	      
		if(isOf_interest(rmul1[k2])) {

		  // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		  //! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		  // _mm_prefetch (&data2[8], _MM_HINT_NTA);


		  //! The operation:
		  for (uint j2 = 0; j2 < chunkSize_index2; ++j2) { //! which is used to 'move' the poiner to a new column:

		    if(isOf_interest(rmask2[j2])) {		      
		      assert((i + i2) < ncols); //! and if wrong then udpate [below].
		      // FIXME: in [”elow] validate correctness of "index" wrt. the weight
		      rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
		    }
		  }
		}
		// printf("cnt_iterations=%u, debug_max_inner=%u, at %s:%d\n", cnt_iterations, debug_max_inner, __FILE__, __LINE__);
	      }
	      assert(debug_cntIterations_inner <= (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
	    }
	  }
	}
      } else {
	for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_cols; cnt_index1++, index1 += SM) {
	  const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	  //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
	  uint numberOf_chunks_cols_local = 0; uint chunkSize_cols_last_local = 0;  
	  if(data1 == data2) {
	    __get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, &numberOf_chunks_cols_local, &chunkSize_cols_last_local);
	  } else { //! else the matrix is assumed not to be symmetric.
	    numberOf_chunks_cols_local = numberOf_chunks_cols; chunkSize_cols_last_local = chunkSize_col_last;
	  }
	  for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_cols_local; cnt_index2++, index2 += SM) {
	    const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_cols_local) ? SM : chunkSize_cols_last_local;
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    /**
	       Note: similar to for "transposed == 0" we find this procedure/code rather complex/non-intutive. To help the understanding (and identificaiton of bugs: sadly it is diffuclt to rwrite code without any bugs, eg, wrt. splelling errors).  In below code we:
	       ----------------------------------------------------------------------------------------
	       data1-row[index1 + i2*ncols] ... column[index2] | | | | | | column[index2 + chunkSize_index1] 
	       .                        result[index1+0][...]          +=    data2-row[index1 + 0]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+1][...]          +=    data2-row[index1 + 1]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+2][...]          +=    data2-row[index1 + 2]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+k2][...]         +=    data2-row[index1 + k2]    ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       ----------------------------------------------------------------------------------------
	       From the above depidction of 'comptuation for each matrix-tile' we infer that the column-index for "result" is be the same as the column-index in is to be the same for "result" as for "data2": when 'overlapping' the rows of "data1" and "data2" "index2" is used in "result" to update the 'current estimates/comptuations'.
	    **/
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[i][index1], rmask1 = (!mask1) ? NULL : &mask1[i][index1];
		 i2 < chunkSize_cnt_i;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"		   
		   rres += ncols, rmul1 += ncols, //! ie, as the 'inner index' for both res and mul1 is the same.		   
		   rmask1 += (!rmask1) ? 0 : ncols
		 
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {

	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      if(mask1) {_mm_prefetch (&mask1[8], _MM_HINT_NTA);}
	      
	      uint k2 = 0; 
	      assert(index2 < ncols); //! ie, given [below]
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[i][index2], 
		     rmask2 = (!mask2) ? NULL : &mask1[i][index2];
		   k2 < chunkSize_index1; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmask2 += (!rmask2) ? 0 : ncols, rmul2 += ncols
		   ) {		
	      
		if(rmask1[k2]) {

		  // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		  //! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		  // _mm_prefetch (&data2[8], _MM_HINT_NTA);

		  //! The operation:
		  for (uint j2 = 0; j2 < chunkSize_index2; ++j2) { //! which is used to 'move' the poiner to a new column:
		    if(rmask2[j2]) {
		      assert((i + i2) < ncols); //! and if wrong then udpate [below].
		      // FIXME: in [”elow] validate correctness of "index" wrt. the weight
		      rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);		      
		    }
		  }
		}
		// printf("cnt_iterations=%u, debug_max_inner=%u, at %s:%d\n", cnt_iterations, debug_max_inner, __FILE__, __LINE__);
	      }
	    }
	  }
	}
      }
    }
    //printf("cnt_iterations=%u, total_cnt_iterations=%u, at %s:%d\n", cnt_iterations, total_cnt_iterations, __FILE__, __LINE__);

  }
}
