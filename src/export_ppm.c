#include "export_ppm.h"
#include <stdio.h>
#include <errno.h>


ppm_image_t *
exportTo_image (
    float **matrix, const size_t nrows, const size_t ncols,
    char **mapOf_names_rows, char **mapOf_names_cols
)
{
    size_t y, x;
    ppm_image_t *img = (ppm_image_t*)malloc ( sizeof(ppm_image_t) );
    img->nrows = nrows, img->ncols = ncols;

    // Allocate space for Rows x Columns x 3 Colour channels
    img->data = (uint8_t*)malloc ( sizeof(uint8_t) * nrows * ncols * 3 );

    // Set all 3 color channels to same value, create image in greyscale:
    // assumes row-major ordering of input matrix
    for ( y=0; y<nrows; y++ )
    {
        for ( x=0; x<ncols; x++ )
        {
	  //printf("(export-ppm)\t [%i][%i]:%f\n", (int)y, (int)x, matrix[y][x]);
        img->data[3*(y*ncols+x)+0] =
            img->data[3*(y*ncols+x)+1] =
            img->data[3*(y*ncols+x)+2] = (matrix[y][x] * 255.0);
        }
    }
    return img;
}


int
ppm_image_write ( const char *filename, ppm_image_t *ppm )
{
    FILE *output = fopen ( filename, "w" );
    if ( output == NULL )
        return ENOENT;
    fprintf ( output, "P6\n%zu %zu\n255\n", ppm->ncols, ppm->nrows ); //! Note: a change in fofrmrat to simplify writing of a PPM-import-scirpt (oekseth, 06. nov. 2020).
    //fprintf ( output, "P6 %zu %zu 255\n", ppm->ncols, ppm->nrows );
    fwrite ( ppm->data, sizeof(uint8_t), ppm->ncols*ppm->nrows*3, output );
    fclose ( output );
    //! @return:
    return 1;
}


void
ppm_image_finalize ( ppm_image_t *ppm )
{
    free ( ppm->data );
    free ( ppm );
}
