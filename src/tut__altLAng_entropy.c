
#define __MiF__endTime(str_local) ({float prev_time_inSeconds = FLT_MAX; const t_float prev_time_inSeconds__ignore = end_time_measurement(/*msg=*/str_local, prev_time_inSeconds); fprintf(stdout, "sum=%f\n", sum); prev_time_inSeconds__ignore;})


static t_float *scoreList;
static t_float *scoreList_index;
static const uint __tut_max_count = 100; //! reflecintg our "tut__altLAng_entropy.R"

#define __MiF__logLocal(count) ({scoreList_index[(uint)count];})
/* #define VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_sum_float) ({ \ */
/*   union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_sum_float; \ */
/*   vec_mappedTo_function = VECTOR_FLOAT_SET(log_index_fast((v.buffer[0]), log_index_fast(v.buffer[1]), log_index_fast(v.buffer[2]), log_index_fast(v.buffer[3])); \ */
/*   vec_mappedTo_function; }) //! ie, @return the result.                                                                                                                                                    */

#define __MiF__log_SSE(vec_sum_float) ({ \
  union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_sum_float; \
    VECTOR_FLOAT_TYPE vec_mappedTo_function = VECTOR_FLOAT_SET(__MiF__logLocal(v.buffer[0]), __MiF__logLocal(v.buffer[1]), __MiF__logLocal(v.buffer[2]), __MiF__logLocal(v.buffer[3])); \
  vec_mappedTo_function; }) //! ie, @return the result.        
#define __MiF__log_SSE_fromMem(arr, i) ({VECTOR_FLOAT_TYPE vec_sum_float = VECTOR_FLOAT_LOADU(&arr[i]); __MiF__log_SSE(vec_sum_float);}) \

 
//! An SSE-funciton to comptue teh sum.
#define __MiF__sum(arr, arr_size) ({uint i = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOADU(&arr[i]); \
      const uint ncols_offset = arr_size - VECTOR_FLOAT_ITER_SIZE; for(i = VECTOR_FLOAT_ITER_SIZE; i < ncols_offset; i += VECTOR_FLOAT_ITER_SIZE) { vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOADU(&arr[i])); } sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); sum;})
#define __MiF__sum__(arr, arr_size) ({uint i = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOADU(&arr[i]); \
      const uint ncols_offset = arr_size - VECTOR_FLOAT_ITER_SIZE; for(i = VECTOR_FLOAT_ITER_SIZE; i < ncols_offset; i += VECTOR_FLOAT_ITER_SIZE) { vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOADU(&arr[i])); } sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); for(; i < ncols; i++) { sum = macro_pluss(sum, arr[i]);} sum;})
//! An SSE-fucntion to fech/get the sum of SSE-log-scores.
#define __MiF__sum_logScores(arr, arr_size) ({uint i = 0; VECTOR_FLOAT_TYPE vec_result = __MiF__log_SSE_fromMem(arr, i); \
      const uint ncols_offset = arr_size - VECTOR_FLOAT_ITER_SIZE; for(i = VECTOR_FLOAT_ITER_SIZE; i < ncols_offset; i += VECTOR_FLOAT_ITER_SIZE) { vec_result = VECTOR_FLOAT_ADD(vec_result, __MiF__log_SSE_fromMem(arr, i)); } sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); for(; i < ncols; i++) { sum = macro_pluss(sum, __MiF__logLocal(arr[i]));} sum;})




//! Idea: comptue Euclid xmt if-calsue-testign and xmt memory-fetching.:
static t_float getTimeOfMatrixCompXmtMem(const uint nrows, const uint ncols) {
  start_time_measurement();
  //  start.time = proc.time(); //! ie, start time-emasuremetns:
  t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
    //    for(rowOut in 1:nrows) {
      for(uint col = 0; col < ncols; col++) {
	sum = sum + (rowIn * rowOut);
      }
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("getTimeOfMatrixCompXmtMem");
}

// scoreList = array(10);

static t_float entropyLog(const uint nrows, const uint ncols, const char operatorType) {
  start_time_measurement(); //! start.time = proc.time(); //! ie, start time-emasuremetns:
  t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
  /* for(rowIn in 1:nrows) { */
  /*   for(rowOut in 1:nrows) { */
      if(operatorType == 0) {
	for(uint col = 0; col < ncols; col++) { 	//         for(col in 1:ncols) {
          sum = sum + scoreList[col];
        }
      } else {
	for(uint col = 0; col < ncols; col++) {
  //         for(col in 1:ncols) {
          sum = sum + __MiF__logLocal(scoreList[col]);
        }
      }
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("entropyLog");
}

static t_float entropyLogSum(const uint nrows, const uint ncols, const char operatorType) {
  start_time_measurement(); //! start.time = proc.time(); //! ie, start time-emasuremetns:
  t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
  /* for(rowIn in 1:nrows) { */
  /*   for(rowOut in 1:nrows) { */
      if(operatorType == 0) {
	sum += __MiF__sum( scoreList, ncols );
      } else {
	sum += __MiF__sum_logScores(scoreList, ncols);
      }
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("entropyLogSum");
}

static t_float entropyLogFixedNumber(const uint nrows, const uint ncols, const char operatorType) {
  start_time_measurement(); //! start.time = proc.time(); //! ie, start time-emasuremetns:
      t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
  /* for(rowIn in 1:nrows) { */
  /*   for(rowOut in 1:nrows) { */
      for(uint col = 0; col < ncols; col++) {
      //      for(col in 1:ncols) {
	sum = sum + __MiF__logLocal(col % __tut_max_count);
      }
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("entropyLogFixedNumber");
}
static t_float entropyLogSumIfClause(const uint nrows, const uint ncols, const char operatorType) {
  start_time_measurement(); //! start.time = proc.time(); //! ie, start time-emasuremetns:
  t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
  /* for(rowIn in 1:nrows) { */
  /*   for(rowOut in 1:nrows) { */
      if(operatorType == 0) {
	sum = macro_pluss(sum, __MiF__sum(scoreList, ncols));
	//sum( ifelse(scoreList > 0, scoreList), 0)
      } else {
	sum = macro_pluss(sum, __MiF__sum_logScores(scoreList, ncols));
	//sum += sum( ifelse(scoreList > 0, __MiF__logLocal(scoreList), 0) )
      }
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("entropyLogSumIfClause");
}

static t_float getEntropyColSum(const uint ncols, const char operatorType) {
  t_float sum = 0;
  if(operatorType == 0) {
    sum = __MiF__sum( scoreList, ncols );
  } else {
    sum =  __MiF__sum_logScores(scoreList, ncols);
  }
  return sum;
}

static t_float entropyLogSumFuncCallEach(const uint nrows, const uint ncols, const char operatorType) {
  start_time_measurement(); //! start.time = proc.time(); //! ie, start time-emasuremetns:
  t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
  /* for(rowIn in 1:nrows) { */
  /*   for(rowOut in 1:nrows) { */
      sum += getEntropyColSum(ncols, operatorType);
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("entropyLogSumFuncCallEach");
}
static t_float getEntropyCol(const uint ncols, const char operatorType) {
  t_float sum = 0;
  if(operatorType == 0) {
    for(uint col = 0; col < ncols; col++) {
      //         for(col in 1:ncols) {
      sum = sum + scoreList[col];
    }
  } else {
    for(uint col = 0; col < ncols; col++) {
      //         for(col in 1:ncols) {
      sum = sum + __MiF__logLocal(scoreList[col]);
    }
  }
  return sum;
}
static t_float entropyLogFuncCallEach(const uint nrows, const uint ncols, const char operatorType) {
  start_time_measurement(); //! start.time = proc.time(); //! ie, start time-emasuremetns:
  t_float sum = 0;
  for(uint rowIn = 0; rowIn < nrows; rowIn++)  {
    for(uint rowOut = 0; rowOut < nrows; rowOut++)  {
  /* for(rowIn in 1:nrows) { */
  /*   for(rowOut in 1:nrows) { */
      sum += getEntropyCol(ncols, operatorType);
    }
  } 
  //! Compelte time-measurements:
  return __MiF__endTime("entropyLogFuncCallEach");
}

static void tut__altLAng_entropy__start_measurement() {
  const uint scoreList_index_size = __tut_max_count + 1;
  const uint empty_0 = 0;
  scoreList_index = allocate_1d_list_float(scoreList_index_size, empty_0);
  for(uint i = 1; i < __tut_max_count; i++) {scoreList_index[i] = mathLib_float_log((t_float)i);}
  //! 
  //! Note: [below] is a direct re-writign of our "Rscript tut__altLAng_entropy.R":
  uint ncols_prev = 1000;
  for(uint case_id = 1; case_id <= 20; case_id++) {
    //for(caseId in 1:20) {
#if(1 == 1)
    const uint nrows = 20;
    //const uint nrows = 128*case_id*1;
    const uint ncols = ncols_prev; ncols_prev *= 10;
#else
    const uint nrows = 128*case_id*1;
    //const uint ncols = 128*case_id*1; 
    const uint ncols = 128*1000;
#endif
    uint empty_0 = 0;
    scoreList = allocate_1d_list_float(ncols, empty_0);
    // scoreList = allocate_1d_list_uint(ncols, empty_0);
    { //! Set default values:
      for(uint i = 0; i < ncols; i++) { //! a procued reflecintg our "tut__altLAng_entropy.R"
	scoreList[i] = rand() % __tut_max_count; 
      }
    }
    //    const char operatorType = 0; //! ie, sum
    const char operatorType = 1; //! ie, log(...)
     const t_float case0 = getTimeOfMatrixCompXmtMem(nrows, ncols);
     const t_float case1 = entropyLog(nrows, ncols, operatorType);
     const t_float case2 = entropyLogSum(nrows, ncols, operatorType);
     const t_float case3 = entropyLogFixedNumber(nrows, ncols, operatorType);
     const t_float case4 = entropyLogSumIfClause(nrows, ncols, operatorType);
     const t_float case5 = entropyLogFuncCallEach(nrows, ncols, operatorType);
     const t_float case6 = entropyLogSumFuncCallEach(nrows, ncols, operatorType);
     //! Construct string:

     // #row,+-xmt-mem,log,sum(log),entropyLogFixedNumber,entropyLogSumIfClause,entropyLogFuncCallEach,entropyLogSumFuncCallEach
     fprintf(stdout, "matrix[%d, %d]\t%f%s%f%s%f%s%f%s%f%s%f%s%f\n", nrows, ncols, case0, "\t", case1, "\t", case2, "\t", case3, "\t", case4, "\t", case5, "\t", case6);
     fprintf(stderr, "matrix[%d, %d]\t%f%s%f%s%f%s%f%s%f%s%f%s%f\n", nrows, ncols, case0, "\t", case1, "\t", case2, "\t", case3, "\t", case4, "\t", case5, "\t", case6);
     //!
     //! De-allocte
     // free_1d_list_uint(&scoreList); scoreList = NULL;
     free_1d_list_float(&scoreList); scoreList = NULL;
  }
}
