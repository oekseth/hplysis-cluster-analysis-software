  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  const uint __cnt_cases__eachDim = (config__matrixDims__maxSizeApprox - config__matrixDims__minSize)/config__matrixDims__stepMult;

  //! --------------------------
  //!
  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint mapOf_realLife_size = __cnt_cases__eachDim * __cnt_cases__eachDim * __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  for(uint row_id = 1; row_id <= __cnt_cases__eachDim; row_id++) {
    const uint size_rows = row_id * config__matrixDims__minSize * config__matrixDims__stepMult;
    assert(size_rows < config__matrixDims__maxSizeApprox); // TODO: validate correctness of this.
    for(uint col_id = 1; col_id <= __cnt_cases__eachDim; col_id++) {
      //! Budil the sample-string:
      //! Note: though [”elow] will result in a memory-leaksge we assume 'this leakge' will be insigidncat, ie, where 'this leakge' is allowed to reduce the change of the user (ie, you) to be cofnused wr.t how to apply the lgocis we exmaplify (in this code-chunk-example).
      const char empty_0 = 0; const uint char_Def_Size = 1000;
      const uint size_cols = col_id * config__matrixDims__minSize * config__matrixDims__stepMult;
      assert(size_cols < config__matrixDims__maxSizeApprox); // TODO: validate correctness of this.
      char *stringOf_sampleData_type = allocate_1d_list_char(char_Def_Size, empty_0); sprintf(stringOf_sampleData_type, "%s_r%u_c%u", config__nameOfDefaultVariable__2, size_rows, size_cols);
      for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
	const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
	assert(current_pos < mapOf_realLife_size);
	//!
	//! Set the dimsions wr.t the current object:
	fileRead_config__syn.imaginaryFileProp__nrows = size_rows;
	fileRead_config__syn.imaginaryFileProp__ncols = size_cols;
	//!
	//! Add the object:
	mapOf_realLife[current_pos].tag = stringOf_sampleData_type;
	mapOf_realLife[current_pos].file_name = config__nameOfDefaultVariable__2; //! ie, the mathemtical-sample-da-taset to use.
	mapOf_realLife[current_pos].fileRead_config = fileRead_config__syn;
	mapOf_realLife[current_pos].inputData__isAnAdjcencyMatrix = false;
	mapOf_realLife[current_pos].k_clusterCount = k_clusterCount;
	mapOf_realLife[current_pos].mapOf_vertexClusterId = NULL;
	mapOf_realLife[current_pos].mapOf_vertexClusterId_size = 0;
	mapOf_realLife[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
	mapOf_realLife[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
	//
	//mapOf_realLife[current_pos] = {/*tag=*/stringOf_sampleData_type, /*file_name=*/stringOf_sampleData_type, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/k_clusterCount, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans};
	//!
	current_pos++;
      }
    }
  }
