{ //! The function for perofmrance-testing:

  //! Note: we expect the "stringOf_measureText" to be set in the caller.

  /* const uint arrOf_cnt_buckets_size = 7; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40, */
  /* 												  80, 160 */
  /* 												  //, 320, 740, 1000 */
  /* }; */

#ifndef __macro_funcName_toCall
#error "!!\t The funciton-anem to call is not specirfied, ie, please investigate you call, wrt. an 'itnernal' fucntion-name"
#endif
  #ifndef __macro_includeTransposedInCall
#error "!!\t The __macro_includeTransposedInCall macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name"
#endif
#ifndef __macro_includeTransposedInCall__andUseTiling
#error "!!\t The __macro_includeTransposedInCall__andUseTiling macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name"
#endif
#ifndef __use_Result_to_updateTableComparisonResults
#error "!!\t The __use_Result_to_updateTableComparisonResults macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name"
#endif


  printf("\n-----------\t start-block:: for tag=\"%s\", at %s:%d\n", stringOf_measureText, __FILE__, __LINE__);

  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
    { 

      //! Allcoate an itniate memory
      const t_float default_value_float = 0;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float **result = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      for(uint i = 0; i < size_of_array; i++) {
	for(uint out = 0; out < size_of_array; out++) {
	  matrix[i][out] = 1/(out+2) + (out+1)/(i+1); //! ie, itnaite.
	}
      }
#if(__macro_includeTransposedInCall == 1) 
      t_float **matrix_transposed = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      kt_mathMacros_SSE__transpose(size_of_array, size_of_array, matrix, matrix_transposed);
#endif



      //!
      //! The experiemnt:
      start_time_measurement();  //! ie, start measurement


      //! Compute:
      assert( (size_of_array % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, as we expect (for simplicty) the matrix to have been adjsutdd wrt. SSE-inti-length.

      //! Compute:
      assert( (size_of_array % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, as we expect (for simplicty) the matrix to have been adjsutdd wrt. SSE-inti-length.
      const uint size_adjusted = size_of_array; 
#if(__macro_includeTransposedInCall == 1) 
      assert(matrix_transposed);
#if(__macro_includeTransposedInCall__andUseTiling == 1) 
      assert(SM != 0); //! ie, the tiling-size
      printf("SM=%u, size_of_array=%u, at %s:%d\n", SM, size_of_array, __FILE__, __LINE__);
      assert( (size_of_array % SM) == 0); //! ie, as we expect (for simplicty) the matrix to have been adjsutdd wrt. SSE-inti-length.      
      assert( (SM % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, as we expect (for simplicty) the matrix to have been adjsutdd wrt. SSE-inti-length.
      __macro_funcName_toCall(matrix, matrix_transposed, size_adjusted, result, SM);
#else //! then we do Not use tiling wrt. matrix-multiplicaiton.
      __macro_funcName_toCall(matrix, matrix_transposed, size_adjusted, result);
#endif 
#else //! then we do not use a trnaspoed call/inclsuion:
      __macro_funcName_toCall(matrix, size_adjusted, result);
#endif
      /* kt_compute_eigenVector_centrality(matrix, size_of_array, size_of_array, arrOf_result, self); */
	  
      /*   //! Update: */
      /*   result += righttails; */
	  
      /*   ; //result += __get_artimetnic_results(i, i); */
      /* } */
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif
      
      { //! write out the sum opf valeus, ie, to avoid an 'optmized compaition' from 'removing' the call
	t_float sum = 0; for(uint i = 0; i < size_of_array; i++) { sum += result[0][i]; } 
	printf("info:sum\t sum=%.2f, at %s:%d\n", sum, __FILE__, __LINE__);
      }

      //! De-allcoate lcoally reserved memory:
      free_2d_list_float(&matrix, size_of_array);
#if(__macro_includeTransposedInCall == 1) 
      free_2d_list_float(&matrix_transposed, size_of_array);
#endif
      free_2d_list_float(&result, size_of_array);
    }
  }
}
