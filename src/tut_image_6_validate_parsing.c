#include "tut_wrapper_image_segment.c"

/**
   @brief examplfies strategies for image import and exprot
   @author Ole Kristian Ekseth (oekseth, 06. nov. 2020).
   @remarks
   --- exemplifes strategies for image-parsing, and simplfied lgocis to validat coissiten in export-lgocis with import-logics    
   @remarks
   -- compile: g++ -I.  -g -O0  -mssse3   -L .  tut_image_6_validate_parsing.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_img; ./tut_img
**/
//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main(const int array_cnt, char **array) 
//#else
{
  tut_wrapper_image_segment(array_cnt, array); //! ie, apply the test-logics
}
