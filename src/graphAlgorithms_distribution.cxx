#include "graphAlgorithms_distribution.h"

/**
   @brief distribution of values: compute the number of vertices/entries which are 'inside' each 'block of values'.
   @param <arrOf> is the input-list of values.
   @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
   @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
   @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
   @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
   @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
   @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
   @param <valueSpace_range> the fixed value-min-max deviation for each bucket: if set, then we expecs the numberOf_buckets to be set to UINT_MAX
 **/
void graphAlgorithms_distribution::narrowDown_usingFixedSpaceOf_value_uint(const list_generic<type_uint> &arrOf, list_generic<type_uint2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts, list_generic<type_uint> &arrOf_result_sums, const uint numberOf_buckets, const uint valueSpace_min, const uint valueSpace_maxMinsOne,  uint valueSpace_range) {
  // FIXME: write correctness-tests for this funciton ..n and exmaplify use-cases.
  //!  What we expect:
  assert(arrOf.get_current_pos());
  assert(arrOf.is_empty() == false);
  assert(arrOf_result_scoreMinMax.get_current_pos() == 0);
  assert(arrOf_result_counts.get_current_pos() == 0);
  assert(arrOf_result_sums.get_current_pos() == 0);
  assert(valueSpace_min != valueSpace_maxMinsOne); //! as the call would otherwise be pointless.
  //! We expect teither the "valueSpace_range" or the "numberOf_buckets" prameter to be sued:
  if(valueSpace_range != UINT_MAX) {assert(numberOf_buckets == UINT_MAX);}
  if(numberOf_buckets != UINT_MAX) {assert(valueSpace_range == UINT_MAX);}
  assert(numberOf_buckets != 0); //! ie, as this call would be pointless.

  //! Construct a copy of the list, ie, to handle the 'search-cotrnaitns':
  list_generic<type_uint> arrOf_copy; 
  uint min_max[2] = {UINT_MAX, 0}; //! which is used if the caller/users has not defined a-priory the value-space.
  for(uint i = 0; i < arrOf.get_current_pos(); i++) {
    const uint score = arrOf[i].get_key();
    if(score < valueSpace_min) {continue;}
    if(score >= valueSpace_maxMinsOne) {continue;}
    //! Then the valeu is of itnerest:
    arrOf_copy.push(score);
    if(numberOf_buckets != UINT_MAX) {
      //! Then update the min-max-range:
      if(score < min_max[0]) {
	min_max[0] = score;
      }
      if(score > min_max[1]) {
	min_max[1] = score;
      }
    }
  }  
  assert(arrOf_copy.get_current_pos() <= arrOf.get_current_pos()); //! ie, as otherwise 'such' would be indicattive of an error.
  //! Call teh sorting-routine (which sorts the input-list if thye lsit is nto arleayd sorted): 'hodls' the major cost of this operaiotn, on average extiatmed to "n*log(n)".
  arrOf_copy.apply_merge_sort(/*isTo_remove_overlap=*/false);

  //! Idnetify the sice of each bucket, ie, fi not explictly specified by the caller/user:
  list_uint arrOf_tmp_bucketScoreTrhesholds;
  if(numberOf_buckets != UINT_MAX) {
    assert(min_max[0] <= min_max[1]);
    const uint diff = min_max[1] - min_max[0]; 
    // FIXME: how may we 'spread out' the 'orduing-accarucies' ... ie, wrt. simple logics 'in our traversal'?
    valueSpace_range = diff / numberOf_buckets;
    const uint tailSum_inLast = diff - (valueSpace_range * numberOf_buckets);
    if(true) {printf("(info) tthe tail-sum-difference is '%u' given %u number of buckets and valueSpace_range=%u, at %s:%d\n", tailSum_inLast, numberOf_buckets, valueSpace_range, __FILE__, __LINE__);}
    if(tailSum_inLast > 3) {
      assert(tailSum_inLast < numberOf_buckets); //! ie, as otherwise would imply that [ªbove] 'divison' would have 'un-expected' artifacts.
      uint current_threshold = 0;
      for(uint i = 0; i < tailSum_inLast; i++) {
	arrOf_tmp_bucketScoreTrhesholds.push(current_threshold);
	current_threshold += valueSpace_range + 1; //! where '+1' is due to the differnece in buckets.
      }
      for(uint i = tailSum_inLast; i < numberOf_buckets; i++) {
	arrOf_tmp_bucketScoreTrhesholds.push(current_threshold);
	current_threshold += valueSpace_range;
      }
    } //! else we hypothese/assert/assuem that the differnece (in bucket-size) will not be signficant wrt. the result, ie, no piitn in the assicated emmory-access-overhead.
  }  

  //! Iterate through the set of interesting valeus, 'palcing' proeprties (fo teh valeus) in each of the buckets:
  const uint bucket_size = min_max[0] + valueSpace_range; //! ie, to know when 'we are to shift the buckets'.
  //uint min_max_currentRange[2] = {min_max[0], min_max[0] + valueSpace_range}; //! which is used if the caller/users has not defined a-priory the value-space.
  uint current_bucket = 0; uint curentBucket_count = 0; uint currentBucket_sum = 0; uint score_MinMax[2] = {UINT_MAX, 0};
  for(uint i = 0; i < arrOf_copy.get_current_pos(); i++) {
    const uint score = arrOf_copy[i].get_key();
    uint bucketScore_threshold = (current_bucket * bucket_size);
    if( (numberOf_buckets != UINT_MAX) && (arrOf_tmp_bucketScoreTrhesholds.get_current_pos()) ) { //! then we asusem mroe accurete 'bucket-sizes' are specifeid/'avilable':
      const uint bucketScore_threshold_tmp = arrOf_tmp_bucketScoreTrhesholds[current_bucket].get_key();
      assert(bucketScore_threshold_tmp != UINT_MAX); //! ie, as we expec thte bucket 'to be known'.
      assert(bucketScore_threshold_tmp >= bucketScore_threshold); //! ie, as we expec thte "bucketScore_threshold" to be the 'minmim bucket-size'.
      //! Update:
      bucketScore_threshold = bucketScore_threshold_tmp;
    }
    if(score >= bucketScore_threshold) {
      //! What we expect:
      assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
      assert(arrOf_result_counts.get_current_pos() == current_bucket);
      assert(arrOf_result_sums.get_current_pos() == current_bucket);
      //! Update the result-container:
      arrOf_result_scoreMinMax.push(type_uint2x(score_MinMax[0], score_MinMax[1]));
      arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
      
      //! Then we reset and increment:
      curentBucket_count = 0; currentBucket_sum = 0; score_MinMax[0] = UINT_MAX; score_MinMax[1] = 0;
      current_bucket++;      
    }
    //! Increment:
    curentBucket_count++; currentBucket_sum += score; 
    //! Then update the min-max-range for the given index:
    if(score < score_MinMax[0]) {
      score_MinMax[0] = score;
    }
    if(score > score_MinMax[1]) {
      score_MinMax[1] = score;
    }
  }
  { //! Finalize:
    //! What we expect:
    assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
    assert(arrOf_result_counts.get_current_pos() == current_bucket);
    assert(arrOf_result_sums.get_current_pos() == current_bucket);
    //! Update the result-container:
    arrOf_result_scoreMinMax.push(type_uint2x(score_MinMax[0], score_MinMax[1]));
    arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
  }
  //! De-allcoate:
  arrOf_tmp_bucketScoreTrhesholds.free_memory();
  arrOf_copy.free_memory();

  if(true) {printf("(info)\t updated %u buckets with a value-spread of [%u, %u], at [%s]:%s:%d\n", current_bucket, min_max[0], bucket_size, __FUNCTION__, __FILE__, __LINE__);};
}

//! Idneitfy the 'value-space' assicated to each 'bucket'.
float get_numberOf_elements_inEach_bucket(const float numberOf_values_actual, const uint numberOf_buckets, list_generic<type_float> &arrOf_tmp_bucketScoreTrhesholds) {
  // FIXME: write correnctess-tests for [”elow].
  float valueSpace_range = 0;
  valueSpace_range = numberOf_values_actual / numberOf_buckets;
  // FIXME: try to improve [”elow] ... does [below] handle cases for number-ranges less than 1 ... eg, [0.001 .... 0.1]?
  const float tailSum_inLast = (uint)(numberOf_values_actual - (valueSpace_range * numberOf_buckets));
  if(true) {printf("(info) tthe tail-sum-difference is '%f' given %u number of buckets and valueSpace_range=%f, at %s:%d\n", tailSum_inLast, numberOf_buckets, valueSpace_range, __FILE__, __LINE__);}
  if(tailSum_inLast > 3) {
    assert(tailSum_inLast < numberOf_buckets); //! ie, as otherwise would imply that [ªbove] 'divison' would have 'un-expected' artifacts.
    uint current_threshold = 0;
    for(uint i = 0; i < tailSum_inLast; i++) {
      arrOf_tmp_bucketScoreTrhesholds.push(current_threshold);
      current_threshold += valueSpace_range + 1; //! where '+1' is due to the differnece in buckets.
    }
    for(uint i = tailSum_inLast; i < numberOf_buckets; i++) {
      arrOf_tmp_bucketScoreTrhesholds.push(current_threshold);
      current_threshold += valueSpace_range;
    }
  } //! else we hypothese/assert/assuem that the differnece (in bucket-size) will not be signficant wrt. the result, ie, no piitn in the assicated emmory-access-overhead.
  return valueSpace_range;
}

/**
   @brief distribution of values: compute the number of vertices/entries which are 'inside' each 'block of values'.
   @param <arrOf> is the input-list of values.
   @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
   @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
   @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
   @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
   @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
   @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
   @param <valueSpace_range> the fixed value-min-max deviation for each bucket: if set, then we expecs the numberOf_buckets to be set to UINT_MAX
 **/
void graphAlgorithms_distribution::narrowDown_usingFixedSpaceOf_value_float(const list_generic<type_float> &arrOf, list_generic<type_float_2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts,  list_generic<type_float> &arrOf_result_sums, const uint numberOf_buckets, const float valueSpace_min, const float valueSpace_maxMinsOne, float valueSpace_range) {
  // FIXME: write correctness-tests for this funciton ..n and exmaplify use-cases.
  
  //!  What we expect:
  assert(arrOf.get_current_pos());
  assert(arrOf.is_empty() == false);
  assert(arrOf_result_scoreMinMax.get_current_pos() == 0);
  assert(arrOf_result_counts.get_current_pos() == 0);
  assert(arrOf_result_sums.get_current_pos() == 0);
  assert(valueSpace_min != valueSpace_maxMinsOne); //! as the call would otherwise be pointless.
  //! We expect teither the "valueSpace_range" or the "numberOf_buckets" prameter to be sued:
  if(valueSpace_range != FLT_MAX) {assert(numberOf_buckets == UINT_MAX);}
  if(numberOf_buckets != UINT_MAX) {assert(valueSpace_range == FLT_MAX);}
  assert(numberOf_buckets != 0); //! ie, as this call would be pointless.

  //! Construct a copy of the list, ie, to handle the 'search-cotrnaitns':
  list_generic<type_float> arrOf_copy; 
  float min_max[2] = {FLT_MAX, T_FLOAT_MIN_ABS}; //! which is used if the caller/users has not defined a-priory the value-space.
  for(uint i = 0; i < arrOf.get_current_pos(); i++) {
    const float score = arrOf[i].get_key();
    if(score < valueSpace_min) {continue;}
    if(score >= valueSpace_maxMinsOne) {continue;}
    //! Then the valeu is of itnerest:
    arrOf_copy.push(score);
    if(numberOf_buckets != UINT_MAX) {
      //! Then update the min-max-range:
      if(score < min_max[0]) {
	min_max[0] = score;
      }
      if(score > min_max[1]) {
	min_max[1] = score;
      }
    }
  }  
  assert(arrOf_copy.get_current_pos() <= arrOf.get_current_pos()); //! ie, as otherwise 'such' would be indicattive of an error.
  //! Call teh sorting-routine (which sorts the input-list if thye lsit is nto arleayd sorted): 'hodls' the major cost of this operaiotn, on average extiatmed to "n*log(n)".
  arrOf_copy.apply_merge_sort(/*isTo_remove_overlap=*/false);

  //! Idnetify the sice of each bucket, ie, fi not explictly specified by the caller/user:
  list_generic<type_float> arrOf_tmp_bucketScoreTrhesholds;  
  if(numberOf_buckets != UINT_MAX) {
    assert(min_max[0] <= min_max[1]);
    //! Idneitfy the 'value-space' assicated to each 'bucket'.
    valueSpace_range = get_numberOf_elements_inEach_bucket(/*numberOf_values_actual=*/min_max[1] - min_max[0], numberOf_buckets, arrOf_tmp_bucketScoreTrhesholds);
  }  

  //! Iterate through the set of interesting valeus, 'palcing' proeprties (fo teh valeus) in each of the buckets:
  const float bucket_size = min_max[0] + valueSpace_range; //! ie, to know when 'we are to shift the buckets'.
  //uint min_max_currentRange[2] = {min_max[0], min_max[0] + valueSpace_range}; //! which is used if the caller/users has not defined a-priory the value-space.
  uint current_bucket = 0; uint curentBucket_count = 0; float currentBucket_sum = 0; float score_MinMax[2] = {FLT_MAX, T_FLOAT_MIN_ABS};
  for(uint i = 0; i < arrOf_copy.get_current_pos(); i++) {
    const float score = arrOf_copy[i].get_key();
    float bucketScore_threshold = (current_bucket * bucket_size);
    if( (numberOf_buckets != UINT_MAX) && (arrOf_tmp_bucketScoreTrhesholds.get_current_pos()) ) { //! then we asusem mroe accurete 'bucket-sizes' are specifeid/'avilable':
      const float bucketScore_threshold_tmp = arrOf_tmp_bucketScoreTrhesholds[current_bucket].get_key();
      assert(bucketScore_threshold_tmp != FLT_MAX); //! ie, as we expec thte bucket 'to be known'.
      assert(bucketScore_threshold_tmp >= bucketScore_threshold); //! ie, as we expec thte "bucketScore_threshold" to be the 'minmim bucket-size'.
      //! Update:
      bucketScore_threshold = bucketScore_threshold_tmp;
    }
    if(score >= bucketScore_threshold) {
      //! What we expect:
      assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
      assert(arrOf_result_counts.get_current_pos() == current_bucket);
      assert(arrOf_result_sums.get_current_pos() == current_bucket);
      //! Update the result-container:
      type_float_2x obj_current; obj_current.set_key(/*index=*/0, score_MinMax[0]); obj_current.set_key(/*index=*/1, score_MinMax[1]);
      arrOf_result_scoreMinMax.push(obj_current);
      arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
      
      //! Then we reset and increment:
      curentBucket_count = 0; currentBucket_sum = 0; score_MinMax[0] = FLT_MAX; score_MinMax[1] = T_FLOAT_MIN_ABS;
      current_bucket++;      
    }
    //! Increment:
    curentBucket_count++; currentBucket_sum += score; 
    //! Then update the min-max-range for the given index:
    if(score < score_MinMax[0]) {
      score_MinMax[0] = score;
    }
    if(score > score_MinMax[1]) {
      score_MinMax[1] = score;
    }
  }
  { //! Finalize:
    //! What we expect:
    assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
    assert(arrOf_result_counts.get_current_pos() == current_bucket);
    assert(arrOf_result_sums.get_current_pos() == current_bucket);
    //! Update the result-container:
    type_float_2x obj_current; obj_current.set_key(/*index=*/0, score_MinMax[0]); obj_current.set_key(/*index=*/1, score_MinMax[1]);
    arrOf_result_scoreMinMax.push(obj_current);
    arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
  }
  //! De-allocate:
  arrOf_tmp_bucketScoreTrhesholds.free_memory();
  arrOf_copy.free_memory();

  if(true) {printf("(info)\t updated %u buckets with a value-spread of [%f, %f], at [%s]:%s:%d\n", current_bucket, min_max[0], bucket_size, __FUNCTION__, __FILE__, __LINE__);};

}

/**
   @brief distribution of counts: in contrast to "narrowDown_usingFixedSpaceOf_value_uint(..)" and "narrowDown_usingFixedSpaceOf_value_float(..)" this funciton use a 'fixed' window of counts, ie, not a 'fixed window of value-spread' (as used in the latteR).
   @param <arrOf> a sorted list of values
   @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
   @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
   @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
   @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
   @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
   @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
 **/
void graphAlgorithms_distribution::narrowDown_usingFixedSpaceOf_count_uint(const list_generic<type_uint> &arrOf, list_generic<type_uint2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts,  list_generic<type_uint> &arrOf_result_sums, const uint numberOf_buckets, const uint valueSpace_min, const uint valueSpace_maxMinsOne) {
  // FIXME: write correctness-tests for this funciton ..n and exmaplify use-cases.
  //!  What we expect:
  assert(arrOf.get_current_pos());
  assert(arrOf.is_empty() == false);
  assert(arrOf.is_sorted());
  assert(arrOf_result_scoreMinMax.get_current_pos() == 0);
  assert(arrOf_result_counts.get_current_pos() == 0);
  assert(arrOf_result_sums.get_current_pos() == 0);
  assert(valueSpace_min != valueSpace_maxMinsOne); //! as the call would otherwise be pointless.
  assert(numberOf_buckets != 0); //! ie, as this call would be pointless.
  assert(numberOf_buckets != UINT_MAX); //! ie, as this call would be pointless.
  assert(numberOf_buckets < arrOf.get_current_pos());

  //! Identify number of elements 'inside the range':
  uint cnt_elements_interestign = 0;
  for(uint i = 0; i < arrOf.get_current_pos(); i++) {
    const uint score = arrOf[i].get_key();
    if(score < valueSpace_min) {continue;}
    if(score >= valueSpace_maxMinsOne) {continue;}  
    cnt_elements_interestign++;
  }
  if(cnt_elements_interestign == 0) {return;} //! ie, as no elements matched the user-selection-critiera, a case which may 'legally' happen.
  assert(cnt_elements_interestign <= arrOf.get_current_pos());

  //! Idnetify the sice of each bucket, ie, fi not explictly specified by the caller/user:
  list_generic<type_float> arrOf_tmp_bucketScoreTrhesholds;  
  //! Idneitfy the 'value-space' assicated to each 'bucket'.
  const uint bucket_size = (uint)get_numberOf_elements_inEach_bucket(/*numberOf_values_actual=*/(float)cnt_elements_interestign, numberOf_buckets, arrOf_tmp_bucketScoreTrhesholds);


  //! Place the valeus in different buckets:
  uint current_bucket = 0; uint curentBucket_count = 0; uint currentBucket_sum = 0; uint score_MinMax[2] = {UINT_MAX, 0};
  for(uint i = 0; i < arrOf.get_current_pos(); i++) {
    const uint score = arrOf[i].get_key();
    if(score < valueSpace_min) {continue;}
    if(score >= valueSpace_maxMinsOne) {continue;}  
    uint bucketScore_threshold = (current_bucket * bucket_size);

    if( (numberOf_buckets != UINT_MAX) && (arrOf_tmp_bucketScoreTrhesholds.get_current_pos()) ) { //! then we asusem mroe accurete 'bucket-sizes' are specifeid/'avilable':
      const uint bucketScore_threshold_tmp = (uint)arrOf_tmp_bucketScoreTrhesholds[current_bucket].get_key();
      assert(bucketScore_threshold_tmp != UINT_MAX); //! ie, as we expec thte bucket 'to be known'.
      assert(bucketScore_threshold_tmp >= bucketScore_threshold); //! ie, as we expec thte "bucketScore_threshold" to be the 'minmim bucket-size'.
      //! Update:
      bucketScore_threshold = bucketScore_threshold_tmp;
    }
    if(curentBucket_count >= bucketScore_threshold) {
      //! What we expect:
      assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
      assert(arrOf_result_counts.get_current_pos() == current_bucket);
      assert(arrOf_result_sums.get_current_pos() == current_bucket);
      //! Update the result-container:
      type_uint2x obj_current(score_MinMax[0], score_MinMax[1]);
      arrOf_result_scoreMinMax.push(obj_current);
      arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
      
      //! Then we reset and increment:
      curentBucket_count = 0; currentBucket_sum = 0; score_MinMax[0] = UINT_MAX; score_MinMax[1] = 0;
      current_bucket++;      
    }
    //! Increment:
    curentBucket_count++; currentBucket_sum += score; 
    //! Then update the min-max-range for the given index:
    if(score < score_MinMax[0]) {
      score_MinMax[0] = score;
    }
    if(score > score_MinMax[1]) {
      score_MinMax[1] = score;
    }
  }
  { //! Finalize:
    //! What we expect:
    assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
    assert(arrOf_result_counts.get_current_pos() == current_bucket);
    assert(arrOf_result_sums.get_current_pos() == current_bucket);
    //! Update the result-container:
    type_uint2x obj_current(score_MinMax[0], score_MinMax[1]);
    arrOf_result_scoreMinMax.push(obj_current);
    arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
  }
  
  //! De-allocate:
  arrOf_tmp_bucketScoreTrhesholds.free_memory();
}

/**
   @brief distribution of counts: in contrast to "narrowDown_usingFixedSpaceOf_value_uint(..)" and "narrowDown_usingFixedSpaceOf_value_float(..)" this funciton use a 'fixed' window of counts, ie, not a 'fixed window of value-spread' (as used in the latteR).
   @param <arrOf> a sorted list of values
   @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
   @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
   @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
   @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
   @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
   @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
 **/
void graphAlgorithms_distribution::narrowDown_usingFixedSpaceOf_count_float(const list_generic<type_float> &arrOf, list_generic<type_float_2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts,  list_generic<type_float> &arrOf_result_sums, const uint numberOf_buckets, const float valueSpace_min, const float valueSpace_maxMinsOne) {
  // FIXME: write correctness-tests for this funciton ..n and exmaplify use-cases.
  //!  What we expect:
  assert(arrOf.get_current_pos());
  assert(arrOf.is_empty() == false);
  assert(arrOf.is_sorted());
  assert(arrOf_result_scoreMinMax.get_current_pos() == 0);
  assert(arrOf_result_counts.get_current_pos() == 0);
  assert(arrOf_result_sums.get_current_pos() == 0);
  assert(valueSpace_min != valueSpace_maxMinsOne); //! as the call would otherwise be pointless.
  assert(numberOf_buckets != 0); //! ie, as this call would be pointless.
  assert(numberOf_buckets != UINT_MAX); //! ie, as this call would be pointless.
  assert(numberOf_buckets < arrOf.get_current_pos());

  //! Identify number of elements 'inside the range':
  uint cnt_elements_interestign = 0;
  for(uint i = 0; i < arrOf.get_current_pos(); i++) {
    const float score = arrOf[i].get_key();
    if(score < valueSpace_min) {continue;}
    if(score >= valueSpace_maxMinsOne) {continue;}  
    cnt_elements_interestign++;
  }
  if(cnt_elements_interestign == 0) {return;} //! ie, as no elements matched the user-selection-critiera, a case which may 'legally' happen.
  assert(cnt_elements_interestign <= arrOf.get_current_pos());

  //! Idnetify the sice of each bucket, ie, fi not explictly specified by the caller/user:
  list_generic<type_float> arrOf_tmp_bucketScoreTrhesholds;  
  //! Idneitfy the 'value-space' assicated to each 'bucket'.
  const uint bucket_size = (uint)get_numberOf_elements_inEach_bucket(/*numberOf_values_actual=*/cnt_elements_interestign, numberOf_buckets, arrOf_tmp_bucketScoreTrhesholds);


  //! Place the valeus in different buckets:
  uint current_bucket = 0; uint curentBucket_count = 0; float currentBucket_sum = 0; float score_MinMax[2] = {FLT_MAX, T_FLOAT_MIN_ABS};
  for(uint i = 0; i < arrOf.get_current_pos(); i++) {
    const float score = arrOf[i].get_key();
    if(score < valueSpace_min) {continue;}
    if(score >= valueSpace_maxMinsOne) {continue;}  
    uint bucketScore_threshold = (current_bucket * bucket_size);

    if( (numberOf_buckets != UINT_MAX) && (arrOf_tmp_bucketScoreTrhesholds.get_current_pos()) ) { //! then we asusem mroe accurete 'bucket-sizes' are specifeid/'avilable':
      const uint bucketScore_threshold_tmp = (uint)arrOf_tmp_bucketScoreTrhesholds[current_bucket].get_key();
      assert(bucketScore_threshold_tmp != FLT_MAX); //! ie, as we expec thte bucket 'to be known'.
      assert(bucketScore_threshold_tmp >= bucketScore_threshold); //! ie, as we expec thte "bucketScore_threshold" to be the 'minmim bucket-size'.
      //! Update:
      bucketScore_threshold = bucketScore_threshold_tmp;
    }
    if(curentBucket_count >= bucketScore_threshold) {
      //! What we expect:
      assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
      assert(arrOf_result_counts.get_current_pos() == current_bucket);
      assert(arrOf_result_sums.get_current_pos() == current_bucket);
      //! Update the result-container:
      type_float_2x obj_current; obj_current.set_key(/*index=*/0, score_MinMax[0]); obj_current.set_key(/*index=*/1, score_MinMax[1]);
      arrOf_result_scoreMinMax.push(obj_current);
      arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
      
      //! Then we reset and increment:
      curentBucket_count = 0; currentBucket_sum = 0; score_MinMax[0] = FLT_MAX; score_MinMax[1] = T_FLOAT_MIN_ABS;
      current_bucket++;      
    }
    //! Increment:
    curentBucket_count++; currentBucket_sum += score; 
    //! Then update the min-max-range for the given index:
    if(score < score_MinMax[0]) {
      score_MinMax[0] = score;
    }
    if(score > score_MinMax[1]) {
      score_MinMax[1] = score;
    }
  }
  { //! Finalize:
    //! What we expect:
    assert(arrOf_result_scoreMinMax.get_current_pos() == current_bucket);
    assert(arrOf_result_counts.get_current_pos() == current_bucket);
    assert(arrOf_result_sums.get_current_pos() == current_bucket);
    //! Update the result-container:
    type_float_2x obj_current; obj_current.set_key(/*index=*/0, score_MinMax[0]); obj_current.set_key(/*index=*/1, score_MinMax[1]);
    arrOf_result_scoreMinMax.push(obj_current);
    arrOf_result_counts.push(curentBucket_count);  arrOf_result_sums.push(currentBucket_sum);
  }
  
  //! De-allocate:
  arrOf_tmp_bucketScoreTrhesholds.free_memory();
}



/**
   @brief the system test for class rule_set.
   @param <print_info> if set a limited status information is printed to STDOUT.
   @return true if test completed.
**/
bool graphAlgorithms_distribution::assert_class(const bool print_info) {    
  const uint list_size = 10; const uint list[list_size] = {2, 2, 3,
							    5, 6, 8,
							   14, 20, 25, 29};

  printf("At %s:%d\n", __FILE__, __LINE__);  assert(false);


  // TODO: .... for a given matrix ... by counts using ...??... <--- first describe an applicaiton/use-case 'for such', ie, to identify 'emtrics of quality'.
  // FIXME: .... 'disjoitness in graphs': (a) move "graph_disjointForests" to this folder; (b) write a new 'CMkae-include-file' for this directory; (c) remove "Nip" from this CMakeLists file and update both the "nip" and "traverse/" direcotries; (d) write an example where we compute dsitributions for disjoint forests.
  //! ----------------------------
  // FIXME[assert]: .... for buckets seperated by even .... number of vertices, ie, distribution of values ... "narrowDown_usingFixedSpaceOf_value_uint(..)" and "narrowDown_usingFixedSpaceOf_value_float(..)" 
  // FIXME[assert]: .... for buckets seperated by even .... min-max range, ie, distribution of counts... "narrowDown_usingFixedSpaceOf_count_uint(..)" and "narrowDown_usingFixedSpaceOf_count_float(..)"
  // FIXME: .... 
  // FIXME: .... 
}
