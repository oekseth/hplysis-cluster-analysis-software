#include "kt_sparse_sim_config.h"

/**
   @brief provide generlaised logics to comptue simliarty-matircs for two sparse data-sets (oekseth, 06. mar. 2017)
   @param <self> is the local configuraiton to use.
   @param <obj_1> is the inner/first 'sparse matrix' to evaluate
   @param <obj_2> is the outer/second 'sparse matrix' to evaluate
   @param <weight> which should bew set if the assiated amtcro-param [”elow] is used.
   
   @param <result_mat> wich if set implies that we update the "result_mat" with the result.
   @param <result_sparse> optioanl: should be used if "result_mat" si set to NULL: if set implies that we insert reuslts into a sparse non-sorted result-object.

   @param <startPos__obj_1> 
   @param <startPos__obj_2> similar to "startPos__obj_1" "startPos__obj_2" should be inside the range [0, obj_2->_nrows]

   @remarks parallisation: 
   -- idea is to apply/add the following 'parallel procedure' in our simlairty-emtric appraoch: step(a) parition data-sets into ('b1' < nrows_1, 'b2' < nrows_2) blocks, step(b) build a mapping-list wrt. 'these blocks', step(c) apply the parallisation, step(d) 'for each [...row1...][...row2...] set comptue simlairty-emtric and post-process the results.
   -- "startPos__obj_1" and "startPos__obj_2" parameters are used to falciate the seperation wrt. different parallel computing-threads.


   @remarks The implict parameters we expect to be defined (through C-templates) are: 
   @tparam <TEMPLATE_distanceConfiguration_useWeights> bool: set to "1" if weights are to be used.
   @tparam <TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores> bool: extends "TEMPLATE_distanceConfiguration_useWeights" set to "1" if weights are to be applied after the simlairty-metric (ie, instead of 'before').
   @tparam <TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate> enum "e_template_correlation_tile_typeOf_distanceUpdate": the different catoegies of distnace-comptautiosn to be applied.
   @tparam <TEMPLATE_distanceConfiguration_macroFor_metric_includePower> bool: if set to "1" impleis that we call prowide the self->configMetric_power as an argument
   @tparam <TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax> bool: if set to "1" we 'take the max' (Rather than the sum) of scores.
   @tparam <TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin> bool: if set to "1" we 'take the min' (Rather than the sum) of scores.
   @tparam <__templateInternalVariable__isTo_updateCountOf_vertices> bool: if set to "1" then we udpate the 'count of interesting vertices', ie, a 'count' which we then use to adjust the scores in our post-processing-step.
 **/
void TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany(const s_kt_sparse_sim_config_t *self, s_kt_set_2dsparse_t *obj_1, s_kt_set_2dsparse_t *obj_2, s_kt_matrix_base_t *result_mat, s_kt_set_2dsparse_t *result_sparse, const t_float *weight, uint startPos__obj_1, uint startPos__obj_2, const bool config__listIsAlreadySorted) { 
  assert(self);
  const t_float configMetric_power = self->configMetric_power;
  assert(obj_1);
  if(obj_2 == NULL) {obj_2 = obj_1;}
  const uint nrows_1 = obj_1->_nrows;
  const uint nrows_2 = obj_2->_nrows;
  //! Handle casse where the parallel-start-pos-arguments are set to 'empty':
  if(startPos__obj_1 == UINT_MAX) {startPos__obj_1 = 0;}
  if(startPos__obj_2 == UINT_MAX) {startPos__obj_2 = 0;}
  assert(startPos__obj_1 < nrows_1);
  assert(startPos__obj_2 < nrows_2);

  if(result_mat) {
    //! Note[below]: the [below] ">=" is used to 'support' the parallel exueciotn (ie, where 'row-sizes' are a subset of teh evlauted space).
    assert(result_mat->nrows >= nrows_1); 
    assert(result_mat->ncols >= nrows_2);
  } else {
    assert(result_sparse);
    assert(result_sparse->_nrows == nrows_1);
  }

  const bool __useSymmetricOptimization = (obj_1 == obj_2);
  if(__useSymmetricOptimization) {assert(startPos__obj_2 >= startPos__obj_1);} //! ie, our defulat assumption

  //!
  //!
  //! The experiemnt:
  if(config__listIsAlreadySorted == false) {
    sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(obj_1, /*isTo_sortKeys=*/true);
    if(obj_1 != obj_2) {
      sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(obj_2, /*isTo_sortKeys=*/true);
    }
  } //! else we asusme the 'caller' ahve already sorted the list (where latter is of ciritcal improtance in/for parallel applciaotn/shceudling).
#if(optimize_parallel_useNonSharedMemory_MPI == 1)
//! ie, an heads-upt to 'then' add a wrapper wrt. [”elow];
#error "Update this code-chunk with new/updated code" 
#endif
  e_kt_sparse_sim_config__howToHandleNoMatches_t specToHandle_noMatches = self->specToHandle_noMatches;
  if(specToHandle_noMatches == e_kt_sparse_sim_config__howToHandleNoMatches_undef) {specToHandle_noMatches = e_kt_sparse_sim_config__howToHandleNoMatches_none;}
  for(uint row_id = startPos__obj_1; row_id < nrows_1; row_id++) {	  
    const uint row_id_pluss = row_id + 1;
    const bool start_row_2 = (__useSymmetricOptimization) ? macro_max(row_id_pluss, startPos__obj_2) : startPos__obj_2;
    //printf("row_id=%u, at %s:%d\n", row_id, __FILE__, __LINE__);
    //if(__useSymmetricOptimization) {assert(startPos__obj_2 >= row_id_pluss);} //! ie, to 'avoid' a symmetric data-comptuation
    //!
    //! Iterate:
    for(uint row_id_out = start_row_2; row_id_out < nrows_2; row_id_out++) {
      //! Get seprate rows wrt. the keys and the scores:
      uint row_size   = 0; const uint *__restrict__ row_1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(obj_1, row_id, &row_size);
      uint row_size_2 = 0; const uint *__restrict__ row_2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(obj_2, row_id_out, &row_size_2);
      if(!row_size) {continue;}
      if(!row_size_2) {continue;}
      // printf("[%u:%u]x[%u:%u], at %s:%d\n", row_id, row_size, row_id_out, row_size_2, __FILE__, __LINE__);

      const t_float *__restrict__ rowOf_scores_1 = get_sparseRow_ofScores__s_kt_set_2dsparse_t(obj_1, row_id, &row_size);
      const t_float *__restrict__ rowOf_scores_2 = get_sparseRow_ofScores__s_kt_set_2dsparse_t(obj_2, row_id_out, &row_size_2);
      // printf("[row=%u]\trow_size=%u, size_of_array=%u, at %s:%d\n", row_id, row_size, size_of_array, __FILE__, __LINE__);
      /* assert(row_size == size_of_array); //! ie, what we expect */
      /* assert(row_size_2 == size_of_array); //! ie, what we expect */
      assert(row_1); assert(row_size > 0);
      assert(row_2); assert(row_size_2 > 0);
      //! --------
      // TODO[ªlg]: consider 'makign use of all cases' in [”elow] ... ie, to 'also inlcude' for the " != " case
#define __Mi__updateForNoMatches 0
      uint cntLocal_intersect = 0; uint cntLocal_1 = 0; uint cntLocal_2 = 0;
      uint cntLocal_onlyIn_1_added = 0; uint cntLocal_onlyIn_2_added = 0;
      //! --------
const uint cnt_iterateBoth = macro_max(row_size, row_size_2);
      //      const uint cnt_iterateBoth = (row_size <= row_size_2) ? row_size : row_size_2; // = row_size + row_size_2;

      uint col_id = 0;
      t_float result = 0;
#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)
      s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, /*isTo_findMaxValue=*/false, /*isTo_findMinValue=*/true); //! defined in our "e_template_correlation_tile.h"
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)
      s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, /*isTo_findMaxValue=*/true, /*isTo_findMinValue=*/false); //! defined in our "e_template_correlation_tile.h"
#else
      s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, /*isTo_findMaxValue=*/false, /*isTo_findMinValue=*/false); //! defined in our "e_template_correlation_tile.h"
#endif
      t_float sumOf_1 = 0;       t_float sumOf_2 = 0; 
bool use_1 = true; bool use_2 = true;
      for(; col_id < cnt_iterateBoth; col_id++) {
	//! Gvient he 'sprasenes's of the input we expect both 'sets' to be of itnerest:
	assert_possibleOverhead(isOf_interest(row_1[cntLocal_1]));
	assert_possibleOverhead(isOf_interest(row_2[cntLocal_2]));
	//! 
	//! Apply logics:
      if(use_1 && use_2 && (row_1[cntLocal_1] == row_2[cntLocal_2]) ) {
	//	if(row_1[cntLocal_1] == row_2[cntLocal_2]) {
	t_float score_1 = rowOf_scores_1[cntLocal_1];  t_float score_2 = rowOf_scores_2[cntLocal_2];
#if(TEMPLATE_distanceConfiguration_useWeights == 1) && (TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 0)
	  //! Then a''ply' the scores:
	  score_1 *= weight[row_1[cntLocal_1]];
	  score_2 *= weight[row_2[cntLocal_2]];
#endif
	  //! ------------------------------------------------------------------------------------- 
	  //!
	  //! Compute the distance:                                                                                                                                                                                          
	  //printf("Compute for value-pair(%f, %f), at %s:%d\n", term1, term2, __FILE__, __LINE__);                                                                                                                          
	  //	  printf("Compute for value-pair(%c, %c), at %s:%d\n", (char)row_1[cntLocal_1], (char)row_2[cntLocal_2], __FILE__, __LINE__);                                                                                                                          
#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) )
	  t_float result_tmp = TEMPLATE_distanceConfiguration_macroFor_metric(score_1, score_2);
#if(TEMPLATE_distanceConfiguration_useWeights == 1) && (TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 1)
	  result_tmp *= weight[row_2[cntLocal_2]];
#endif
#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 1) )
	  t_float result_tmp = TEMPLATE_distanceConfiguration_macroFor_metric(score_1, score_2, configMetric_power);
#if(TEMPLATE_distanceConfiguration_useWeights == 1) && (TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 1)
	  result_tmp *= weight[row_2[cntLocal_2]];
#endif
#else //! then we 'provide' a complex resutl-object intot eh caller, eg, to handle mulipel result-set which may first be merged 'when all valeus are comptued':                                                                   
#if( (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) )
	  TEMPLATE_distanceConfiguration_macroFor_metric(score_1, score_2, objLocalComplex_nonSSE);
#else
	  TEMPLATE_distanceConfiguration_macroFor_metric(score_1, score_2, objLocalComplex_nonSSE, configMetric_power);
#endif
#endif

#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
	  //printf("result_tmp[%u]=%f -->sum=%f, at %s:%d\n", j2, result_tmp, result + result_tmp, __FILE__, __LINE__);                                                                                                      
	  //printf("\t\t\t[%u]\t (sum-values)\t %f VS %f, at %s:%d\n", j2, result, result_tmp, __FILE__, __LINE__);                                                                                                          
	  result  += result_tmp;
#elif(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) //! tehnw e are itnerested in the max-value                                                    
	  //! ----                                                                                                                                                                                                           
#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)
	  //printf("\t\t\t [%u]\t (cmp-val)\t %f VS %f, at %s:%d\n", j2, result, result_tmp, __FILE__, __LINE__);                                                                                                            
	  result = macro_min(result, result_tmp);
	  //printf("\t\t\t\t (result)\t %f at %s:%d\n", result, __FILE__, __LINE__);                                                                                                                                         
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)
	  //printf("\t\t\t[%u]\t (cmp-val)\t %f VS %f, at %s:%d\n", j2, result, result_tmp, __FILE__, __LINE__);                                                                                                             
	  result = macro_max(result, result_tmp);
#else
#warning "Investigate this issue, ie, are we to sum 'sum of values'?"
	  //! ----                                                                                                                                                                                                           
#endif //! endif(type-of-sum)                                                                                                                                                                                                      
#endif

#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0)  ) //! then we are to update  the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice:                                                                                                                                             
	  //printf("\t\t\t\t(updated-the-weight\t at %s:%d\n", __FILE__, __LINE__);                                                                                                                                          
#if(TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 1)
	  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(weight);
	  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(weight_index < ncols);
	  objLocalComplex_nonSSE.cnt_interesting_cells += weight[/*weight_index=*/row_1[cntLocal_1]]; //! ie, update the temproal result-vector wrt. the counts                                                                                    
#else //! then we assume the weight-table is Not set (oekseth, 06. des. 2016)                                                                                                                                                      
	  objLocalComplex_nonSSE.cnt_interesting_cells += 1; //! ie, update the temproal result-vector wrt. the counts: "+1" is used as we at this exueciton-point assuems that the cell is not 'remvoed by/through maksing'.
#endif
#elif((TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (__templateInternalVariable__isTo_updateCountOf_vertices == 1) ) //! tehnw e are itnerested in the max-value                                                                                                                                                                                                                
	  //printf("\t\t\t\t(updated-only-the-coutns-wrt.the-weight\t at %s:%d\n", __FILE__, __LINE__);                                                                                                                      
	  objLocalComplex_nonSSE.cnt_interesting_cells += 1; //! ie, update the temproal result-vector wrt. the counts: "+1" is used as we at this exueciton-point assuems that the cell is not 'remvoed by/through maksing'.
#endif
	  //! --------------------------------------------------------
	  //!
#if(__Mi__updateForNoMatches == 1)
	  arrOf_tmp_intersect[cntLocal_intersect++] = row_1[cntLocal_1];
#endif
	  cntLocal_1++;    cntLocal_2++;
	  cntLocal_intersect++;
	  use_1 = (cntLocal_1 < row_size); //! where htis synes is used to aovid branchign.
	  use_2 = (cntLocal_2 < row_size_2); //! where htis synes is used to aovid branchign.
#if(0 == 1)
	  printf("Compute for value-pair(%c, %c), use(next)=(%s, %s), given pos=[%u<%u , %u<%u], at %s:%d\n", (char)row_1[cntLocal_1], (char)row_2[cntLocal_2], 
		 (use_1) ? "true" : "false",
		 (use_2) ? "true" : "false",
		 cntLocal_1, row_size,
		 cntLocal_2, row_size_2,
		 __FILE__, __LINE__);                                                                           
#endif
	  /* if(cntLocal_1 >= row_size) {use_1 = false;} */
	  /* if(cntLocal_2 >= row_size) {use_2 = false;} */
      } else if(use_1 && (!use_2 || (row_1[cntLocal_1] < row_2[cntLocal_2])) ) {
	  //	} else if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
	//	printf("\t (terate::row_1)\t given [%c VS %c], at %s:%d\n", row_1[cntLocal_1] , row_2[cntLocal_2], __FILE__, __LINE__);                                                                                                                          
	  sumOf_1 += rowOf_scores_1[cntLocal_1];  // TODO[perofmrance]: cosndier to 'oinly activate this' for a specific input-macor-apram (oekseth, 06. mar. 2017).
#if(__Mi__updateForNoMatches == 1)
	  sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
#endif
	  cntLocal_onlyIn_1_added++;
	  cntLocal_1++;  
	use_1 = (cntLocal_1 < row_size); //! where htis synes is used to aovid branchign.
	//if(cntLocal_1 >= row_size) {use_1 = false;}
      } else if(use_2) { //if(row_1[cntLocal_1] < row_2[cntLocal_2]) {
	//	printf("\t (terate::row_2)\t given [%c VS %c], at %s:%d\n", row_1[cntLocal_1] , row_2[cntLocal_2], __FILE__, __LINE__);                                                                                                                          
	//	printf("\t (terate::row_2)\t at %s:%d\n", __FILE__, __LINE__);                                                                                                                          
	  sumOf_2 += rowOf_scores_2[cntLocal_2];  // TODO[perofmrance]: cosndier to 'oinly activate this' for a specific input-macor-apram (oekseth, 06. mar. 2017).
#if(__Mi__updateForNoMatches == 1)
	  sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
#endif
	  cntLocal_onlyIn_2_added++;
	  cntLocal_2++;  
	use_2 = (cntLocal_2 < row_size_2); //! where htis synes is used to aovid branchign.
	  //arrOf_tmp_onlyIn_row2[cntLocal_onlyIn_2_added++] = row_2[cntLocal_2++];
	} //else { //! then the elemnts are equal:
	//}
      }

      // FIXME: introspect upon the correctness of [below] asusmption ... ie, taht the 'interseciton' case si covered in [ªbove] for-loop-iteriaotn (oekseth, 06. otk. 2016).
#if(__Mi__updateForNoMatches == 1)
      if(col_id < row_size) {
	assert(col_id >= row_size_2); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_2
	for(uint col_id = cntLocal_1; col_id < row_size; col_id++) {
	  sumOf_values += __localMacro_intersectingWeights_match_row1(row_1[cntLocal_1++]);
	}
      } else if(col_id < row_size_2) {
	assert(col_id >= row_size); //! ie, as we then assume that we do Not need to ivnestigate wrt. row_1
	for(uint col_id = cntLocal_2; col_id < row_size_2; col_id++) {
	  sumOf_values += __localMacro_intersectingWeights_match_row2(row_2[cntLocal_2++]);
	}
      } 
#endif
      
      //! We expect the sets to be completely overalpping, ie, givne our test-case:
      /* assert_possibleOverhead(cntLocal_1 == size_of_array); */
      /* assert_possibleOverhead(cntLocal_2 == size_of_array); */
      /* assert_possibleOverhead(cntLocal_intersect == size_of_array); */
      //const t_float configMetric_power = self.configMetric_power;
      //! --------------------------------------
      //! Post-process:
      t_float result_adjusted = T_FLOAT_MAX;
      const uint ncols = cntLocal_intersect;
      // printf("\t posProcess[%u:%u]x[%u:%u], at %s:%d\n", row_id, row_size, row_id_out, row_size_2, __FILE__, __LINE__);
#include "template_correlation_tile_applyMetric_postProcessing.c"        
      //printf("\t posProcess:completed[%u:%u]x[%u:%u], at %s:%d\n", row_id, row_size, row_id_out, row_size_2, __FILE__, __LINE__);
      //!
      //! PRovide logics for data-adjustment:
      //! FOte: for details wr.t latter enum see our "kt_sparse_sim_config.h".
      if( (result_adjusted != T_FLOAT_MAX) && (result_adjusted != 0) ) {
	if(specToHandle_noMatches == e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_maxCntElements) {
	  const t_float val = macro_max(sumOf_1, sumOf_2);
	  if(val != 0) {result_adjusted *= val;}
	} else if(specToHandle_noMatches == e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_cntRow1AndRow2_multiply) {
	  const t_float val = macro_mul(sumOf_1, sumOf_2);
	  if(val != 0) {result_adjusted *= val;}
	} else if(specToHandle_noMatches == e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_cntRow1AndRow2_pluss) {
	  const t_float val = macro_pluss(sumOf_1, sumOf_2);
	  if(val != 0) {result_adjusted *= val;}
	}
      }
      
      //! Update the result-container:
      if( (result_adjusted != T_FLOAT_MAX)
	  && (self->threshold_min < result_adjusted) 
	  && (self->threshold_max > result_adjusted) 
	  ) {
	if(result_mat) {
	  result_mat->matrix[row_id][row_id_out] = result_adjusted;
	  if(__useSymmetricOptimization) {
	    result_mat->matrix[row_id_out][row_id] = result_adjusted; //! ie, then udpate 'also' for the symmetic proerpty-case.
	  }
	} else {
	  assert(result_sparse);
	  if(result_sparse->matrixOf_scores) {
	    push__idScorePair__s_kt_set_2dsparse_t(result_sparse, row_id, row_id_out, result_adjusted);
	    if(__useSymmetricOptimization) { //! ie, then udpate 'also' for the symmetic proerpty-case.
	      push__idScorePair__s_kt_set_2dsparse_t(result_sparse, row_id_out, row_id, result_adjusted);
	    }
	  } else { //! then we 'only add the index-pairt which is inside the speicfied thresohld':
	    push__s_kt_set_2dsparse_t(result_sparse, row_id, row_id_out);
	    if(__useSymmetricOptimization) { //! ie, then udpate 'also' for the symmetic proerpty-case.
	      push__s_kt_set_2dsparse_t(result_sparse, row_id_out, row_id);
	    }
	  }
	}
      }
      //printf("\t posProcess:completed::listAdd[%u:%u]x[%u:%u], at %s:%d\n", row_id, row_size, row_id_out, row_size_2, __FILE__, __LINE__);
    }
  }
}
  
