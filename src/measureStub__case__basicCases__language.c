#include "kt_matrix_extendedLogics.h"

static t_float __sampleFunction__EuclidPermutation(t_float **matrix_1, t_float **matrix_2, const uint col_id, const uint row_id_1, const uint nrows) {
  t_float sum = 0;
  for(uint row_id_2 = 0; row_id_2 < nrows; row_id_2++) { 
    const t_float score_1 = matrix_1[row_id_1][col_id];
    const t_float score_2 = matrix_2[row_id_2][col_id];
    if(isOf_interest(score_1) && isOf_interest(score_2)) {
      sum += correlation_macros__distanceMeasures__euclid(score_2, score_1);
    }
  }
  return sum;
}

//! @brief comptue teh sum of score sin a matrix (oeksth, 0.6 mar. 2017)
//! @return the sum of scors in a matrix
//! @remarks is used (in this cotnext) to 'avoid' the "-O2" compailtion-optin from 'dsitacaring' our compatuioion (ie, as we are onlly interested int he compuation-time and nto the result)
static t_float get_sumOf_scoresInMatrix(t_float **matrix, const uint nrows, const uint ncols, const bool isToRpint_sumToSTDERR) {
  assert(matrix); assert(nrows > 0); assert(ncols > 0);
  t_float sum = 0;
  for(uint i = 0; i < nrows; i++) {
    assert(matrix[i]);
    for(uint k = 0; k < ncols; k++) {
      const t_float score = matrix[i][k];
      if(isOf_interest(score)) {sum += score;}
    }
  }
  if(isToRpint_sumToSTDERR) {fprintf(stderr, "(sum-of-scores)\t sum: %f, at %s:%d\n", sum, __FILE__, __LINE__);}
  return sum;
}

static int measureStub__case__basicCases__language__setOf(const char *str_filePath, const uint *listOf_rows, const uint listOf_rows_size, const uint *listOf_cols, const uint listOf_cols_size, const bool useColVal_alsoForRow) {
  //printf("... at %s:%d\n", __FILE__, __LINE__);
  //! Note: [below] appraoch may be compared to Python ("tut__altLAng_sciPy__listAccess.py") and Perl ("") ... (oekseth, 06. feb. 2017).
  static uint test_case_counter = 0;
  float prev_time_inSeconds = T_FLOAT_MAX;
  float prev_time_inSeconds__ignore = T_FLOAT_MAX;
  float system_time = 0;       float user_time = 0;

  //!
  //! Initate the result-matrix:
  const uint cnt_measurementCalls = 13; //! ie, based on [”elow]:
  const uint cnt_sizeComb = listOf_rows_size * listOf_cols_size;
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(cnt_sizeComb, cnt_measurementCalls);
  assert(mat_result.ncols == cnt_measurementCalls);

#define __MiF__setScore() ({if(false) {fprintf(stderr, "!!\t Insert at [%u/%u][%u]: init=%u VS current=%u, at %s:%d\n", currPos__nrows, mat_result.nrows, currPos__ncols, mat_result.ncols, currPos__ncols, __FILE__, __LINE__);} if(currPos__ncols >= mat_result.ncols) {fprintf(stderr, "!!\t Following ncols-expectiaon did Not hold: init=%u VS current=%u, at %s:%d\n", mat_result.ncols, currPos__ncols, __FILE__, __LINE__); } assert(currPos__ncols < mat_result.ncols);  assert(currPos__nrows < mat_result.nrows); mat_result.matrix[currPos__nrows][currPos__ncols] = prev_time_inSeconds__ignore; /*Increment columns:*/ currPos__ncols++;}) //! ie, //! Update teh measurement and icnrement the counter.
#define __MiF__set_columnName() ({if(currPos__nrows == 0) {if(currPos__ncols >= mat_result.ncols) {fprintf(stderr, "!!\t Following ncols-expectiaon did Not hold: init=%u VS current=%u, at %s:%d\n", mat_result.ncols, currPos__ncols, __FILE__, __LINE__); } assert(currPos__ncols < mat_result.ncols); set_stringConst__s_kt_matrix(&mat_result, currPos__ncols, str_local, /*addFor_column=*/true);}}) //! ie, set the column-name:

  /* enum { */
  /*   __lE__simple_mem_SSE_b_32, */
  /*   __lE__simple_mem_SSE_b_64, */
  /*   __lE__simple_mem_SSE_b_128, */
  /*   __lE__simple_xmtMem_xmtSSE, */
  /*   __lE__simple, */
  /*   __lE__simple_restrictedMemAccess, */
  /*   __lE__simple_mem_and_mask, */
  /*   __lE__simple_mem_and_mask_transposed, */
  /*   __lE__simple_mem_and_mask_transposed_funcCall, //! ie, emulating Clsuter-C */
  /*   // __lE__, */
  /*   // ----  */
  /*   __lE__undef, */
  /* }; */
  /* //!  */
  /* //! Allocate sparse for the alorithms: */
  /* s_kt_matrix_t mat_result_each[__lE__undef]; */
  /* for(uint k = 0; k < __lE__undef; k++) { */
  /*   mat_result_each[k] = initAndReturn__s_kt_matrix( */
  /* } */

  uint currPos__nrows = 0;   

  for(uint i = 0; i < listOf_rows_size; i++) {
    for(uint x = 0; x < listOf_cols_size; x++) {      
      uint currPos__ncols = 0;
      //! ------------------------- 
      const uint ncols = listOf_cols[x];
      const uint nrows = (useColVal_alsoForRow == false) ? listOf_rows[i] : ncols; 
      assert((nrows * ncols) < (1000*1000*600)); // TODO: consider removing this cosntrint ... which is incldued to 'allwo'æ our perf-mreausmrenet tob e seeuxeiocnted on a laptop, ie, a'gnerlised test-applicaiton'.
      { //! Set the string:
	allocOnStack__char__sprintf(2000, str_local, "matrix=[%u, %u]", nrows, ncols); //! ie, intate a new variable "str_local". 
	//allocOnStack__char(2000, str_local); //! ie, intate a new variable "str_local". 
	//char str_local[2000]; memset(str_local, '\0', 
	assert(currPos__nrows < mat_result.nrows);
	set_stringConst__s_kt_matrix(&mat_result, currPos__nrows, str_local, /*addFor_column=*/false);
      }
      assert(nrows > 0);      assert(ncols > 0);
      assert( (nrows % 128) == 0); //! ie, to simplify our SSE-test-rotuines.
      assert( (ncols % 128) == 0); //! ie, to simplify our SSE-test-rotuines.
      //assert()
      printf("-----------------------------------------------------------\n\n\n #(case:%u) Start an evlauation of floating-poiint-compelxity for a matrix with dims=[%u, %u]. For quesitons please contact senior developer at [oekseth@gmail.com]. Observation at %s:%d\n", test_case_counter++, nrows, ncols, __FILE__, __LINE__);
      //printf("\t\t nrows=%u, ncols=%u, at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
      //const uint nrows = 30*128; 	const uint ncols = 2*128;
      s_kt_matrix_t mat_val = initAndReturn__s_kt_matrix(nrows, ncols);/* initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /\*maxVal=*\/100,  */
								       /* 		    /\*typeof_randomness=*\/e_kt_randomGenerator_type_posixRan__srand__time__modulo, */
								       /* 		    //e_kt_randomGenerator_type_hyLysisDefault, */
								       /* 		    /\*isTo_initIntoIncrementalBlocks=*\/false);
									*/
      for(uint i = 0; i < nrows; i++) {for(uint k = 0; k < ncols; k++) {mat_val.matrix[i][k] = (t_float) (rand() % ncols);}} //! ie, itnaite.
      printf("(info: randomness completed: starts timeing)\n");
      { //! Extend [ªbov€]: masking:
	//s_kt_matrix_t mat_val = initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /*maxVal=*/100, /*typeof_randomness=*/e_kt_randomGenerator_type_hyLysisDefault, /*isTo_initIntoIncrementalBlocks=*/false);
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	const char *str_local = "simple-plus-memory[SSE+tiling:SM=64]";
	{t_float sum = 0;
	  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, nrows);
	  start_time_measurement();
	  kt_func_metricInner_fast__fitsIntoSSE__euclid__float(mat_val.matrix, mat_val.matrix, nrows, ncols, mat_result.matrix, /*SM=*/64);
	  prev_time_inSeconds__ignore = prev_time_inSeconds = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds); //, system_time, prev_time_inSeconds);
	  get_sumOf_scoresInMatrix(mat_result.matrix, mat_result.nrows, mat_result.ncols, true); //! ie,print ouyt the score, therey 'avoding' issues wrt. a 'ignoreance of the pritn-oerpation in "-O2" compailtion-mode (oekseht, 06. amr.2 107).
	  //! 
	  //free__s_kt_matrix(&mat_val);
	  free__s_kt_matrix(&mat_result);
	}
	//! 
	//! -------------------------------------------
	if(currPos__nrows == 0) { //! Set the string:
	  assert(currPos__ncols < mat_result.ncols); 
	  set_stringConst__s_kt_matrix(&mat_result, currPos__ncols, str_local, /*addFor_column=*/true);
	}	
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
      }
      { //! Extend [ªbov€]: masking:
	//s_kt_matrix_t mat_val = initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /*maxVal=*/100, /*typeof_randomness=*/e_kt_randomGenerator_type_hyLysisDefault, /*isTo_initIntoIncrementalBlocks=*/false);
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	// start_time_measurement();
	t_float sum = 0;
	const char *str_local = "simple-plus-memory[SSE+tiling:SM=32]";
	{
	  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, nrows);
	  start_time_measurement();
	  kt_func_metricInner_fast__fitsIntoSSE__euclid__float(mat_val.matrix, mat_val.matrix, nrows, ncols, mat_result.matrix, /*SM=*/32);
	  prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	  get_sumOf_scoresInMatrix(mat_result.matrix, mat_result.nrows, mat_result.ncols, true); //! ie,print ouyt the score, therey 'avoding' issues wrt. a 'ignoreance of the pritn-oerpation in "-O2" compailtion-mode (oekseht, 06. amr.2 107).
	//free__s_kt_matrix(&mat_val);
	  free__s_kt_matrix(&mat_result);
	}
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
      }
      { //! Extend [ªbov€]: masking:
	//s_kt_matrix_t mat_val = initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /*maxVal=*/100, /*typeof_randomness=*/e_kt_randomGenerator_type_hyLysisDefault, /*isTo_initIntoIncrementalBlocks=*/false);
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	t_float sum = 0;
	const char *str_local = "simple-plus-memory[SSE+tiling:SM=128]";
	{
	  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, nrows);
	  start_time_measurement();
	  kt_func_metricInner_fast__fitsIntoSSE__euclid__float(mat_val.matrix, mat_val.matrix, nrows, ncols, mat_result.matrix, /*SM=*/128);
	  prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	  get_sumOf_scoresInMatrix(mat_result.matrix, mat_result.nrows, mat_result.ncols, true); //! ie,print ouyt the score, therey 'avoding' issues wrt. a 'ignoreance of the pritn-oerpation in "-O2" compailtion-mode (oekseht, 06. amr.2 107).
	  //free__s_kt_matrix(&mat_val);
	  free__s_kt_matrix(&mat_result);
	}
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
      }
      {
	start_time_measurement();
	t_float sum = 0;
	//printf("... at %s:%d\n", __FILE__, __LINE__);
	for(uint row_id_1 = 0; row_id_1 < nrows; row_id_1++) {
	  for(uint row_id_2 = 0; row_id_2 < nrows; row_id_2++) { 
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      sum += (t_float)(col_id + row_id_2);
	    }
	  }
	}
	const char *str_local = "simple-plus-xmt-memoryAccess";
	prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
	//printf("... at %s:%d\n", __FILE__, __LINE__);
	//prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, msg, &user_time, &system_time, prev_time_inSeconds);
      }
      { //! Apply randomness and otehrwise simliar to [ªbove];:
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	start_time_measurement();
	t_float sum = 0;
	for(uint row_id_1 = 0; row_id_1 < nrows; row_id_1++) {
	  for(uint row_id_2 = 0; row_id_2 < nrows; row_id_2++) { 
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score_1 = mat_val.matrix[row_id_1][col_id];
	      const t_float score_2 = mat_val.matrix[row_id_2][col_id];
	      sum += correlation_macros__distanceMeasures__euclid(score_2, score_1);
	      //sum += (mat_val.matrix[row_id_1][col_id] + mat_val.matrix[row_id_2][col_id]);
	    }
	  }
	}
	const char *str_local = "simple-plus-memory";
	prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	//get_sumOf_scoresInMatrix(mat_result.matrix, mat_result.nrows, mat_result.ncols, true); //! ie,print ouyt the score, therey 'avoding' issues wrt. a 'ignoreance of the pritn-oerpation in "-O2" compailtion-mode (oekseht, 06. amr.2 107).
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
	//prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, msg, &user_time, &system_time, prev_time_inSeconds);
      }
      { //! Extend [ªbove]: "restricted memopry-pointer-access"
	//s_kt_matrix_t mat_val = initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /*maxVal=*/100, /*typeof_randomness=*/e_kt_randomGenerator_type_hyLysisDefault, /*isTo_initIntoIncrementalBlocks=*/false);
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	start_time_measurement();
	t_float sum = 0;
	for(uint row_id_1 = 0; row_id_1 < nrows; row_id_1++) {
	  const t_float *__restrict__ row_1 = mat_val.matrix[row_id_1];
	  for(uint row_id_2 = 0; row_id_2 < nrows; row_id_2++) { 
	    const t_float *__restrict__ row_2 = mat_val.matrix[row_id_2];
	    for(uint col_id = 0; col_id < ncols; col_id++) {
#if(1 == 1)
	      sum += correlation_macros__distanceMeasures__euclid(mat_val.matrix[row_id_1][col_id], mat_val.matrix[row_id_2][col_id]);
#else
	      const t_float score_1 = mat_val.matrix[row_id_1][col_id];
	      const t_float score_2 = mat_val.matrix[row_id_2][col_id];
	      sum += correlation_macros__distanceMeasures__euclid(score_2, score_1);
#endif
	      //sum += (row_1[col_id] + mat_val.matrix[row_id_2][col_id]);
	    }
	  }
	}
	const char *str_local = "simple-plus-memory(restrictedMemoryPointers)";
	prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
	//free__s_kt_matrix(&mat_val);
	//prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, msg, &user_time, &system_time, prev_time_inSeconds);
      }
      { //! Extend [ªbov€]: masking:
	//s_kt_matrix_t mat_val = initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /*maxVal=*/100, /*typeof_randomness=*/e_kt_randomGenerator_type_hyLysisDefault, /*isTo_initIntoIncrementalBlocks=*/false);
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	start_time_measurement();
	t_float sum = 0;
	for(uint row_id_1 = 0; row_id_1 < nrows; row_id_1++) {
	  for(uint row_id_2 = 0; row_id_2 < nrows; row_id_2++) { 
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score_1 = mat_val.matrix[row_id_1][col_id];
	      const t_float score_2 = mat_val.matrix[row_id_2][col_id];
	      if(isOf_interest(score_1) && isOf_interest(score_2)) {
		sum += correlation_macros__distanceMeasures__euclid(score_2, score_1);
	      }
	    }
	  }
	}
	const char *str_local ="simple-plus-memoryAndMask";
	prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
	//free__s_kt_matrix(&mat_val);
	//prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, msg, &user_time, &system_time, prev_time_inSeconds);
      }
      { //! Extend [ªbov€]: masking:
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	start_time_measurement();
	t_float sum = 0;
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  for(uint row_id_1 = 0; row_id_1 < nrows; row_id_1++) {
	    for(uint row_id_2 = 0; row_id_2 < nrows; row_id_2++) { 
	      const t_float score_1 = mat_val.matrix[row_id_1][col_id];
	      const t_float score_2 = mat_val.matrix[row_id_2][col_id];
	      if(isOf_interest(score_1) && isOf_interest(score_2)) {
		sum += correlation_macros__distanceMeasures__euclid(score_2, score_1);
	      }
	    }
	  }
	}
	const char *str_local = "simple-plus-memoryAndMask::transposedAccess";
	prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
      }
      { //! Extend [ªbov€]: masking:
	assert(mat_val.nrows == nrows);
	assert(mat_val.ncols == ncols);
	start_time_measurement();
	t_float sum = 0;
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  for(uint row_id_1 = 0; row_id_1 < nrows; row_id_1++) {
	    sum += __sampleFunction__EuclidPermutation(mat_val.matrix, mat_val.matrix, col_id, row_id_1, nrows);
	  }
	}
	const char *str_local ="simple-plus-memoryAndMask::transposedAccess::functionCallEmulatingClusterC";
	prev_time_inSeconds__ignore = end_time_measurement__dims(nrows, ncols, /*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	//! 
	//! -------------------------------------------
	__MiF__set_columnName(); //! ie, set the column-name. 
	__MiF__setScore(); //! ie, update teh measurement and icnrement the counter.	
	//! -------------------------------------------
      }
      //! --------------------------------------------
      //!
      //! De-allocate:
      free__s_kt_matrix(&mat_val);
      //! Icnrement:
      currPos__nrows++;
    }
  }
  { //! Write result into a latex-comparible data-format (oekseth, 06. jun. 2017): 
    /* s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_result); */
    const uint cnt_sizeComb = listOf_rows_size * listOf_cols_size;
    assert(mat_result.nrows == cnt_sizeComb);
    //!
    //! Iterate through the result-cases: 
    for(uint case_id = 0; case_id < mat_result.ncols; case_id++) {
      fprintf(stderr, "\n\n#!  %s\n", getAndReturn_string__s_kt_matrix(&mat_result, /*index=*/case_id, /*for-column=*/true));
      uint measure_index = 0;
      for(uint i = 0; i < listOf_rows_size; i++) {
	for(uint x = 0; x < listOf_cols_size; x++) {      
	  //! ------------------------- 
	  const uint ncols = listOf_cols[x];
	  const uint nrows = (useColVal_alsoForRow == false) ? listOf_rows[i] : ncols; 
	  const loint cnt_comparisons = nrows*nrows*ncols;
	  const t_float time_s = mat_result.matrix[measure_index][case_id];
	  if(
	     (time_s != T_FLOAT_MAX) 
	     && 
	     (time_s != 0) ) {
	    fprintf(stderr, "(%lld, %f) ", cnt_comparisons, time_s);
	  }
	  //! 
	  measure_index++;
	}
      }
      fprintf(stderr, "\n");
    }
  }
  //! Specificy an 'itnernal' wrapper-matcro to export the reuslts:
#define __Mi__export(matrix, suffix) ({ \
    char str_conc[2000] = {'\0'}; sprintf(str_conc, "%s%s.tsv", str_filePath, suffix); \
    bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); \
    memset(str_conc, '\0', 2000); sprintf(str_conc, "%s%s.transposed.tsv", str_filePath, suffix); \
    /*! Then export a tranpsoed 'verison': */ \
    is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); })
  { //! Write otu the results, and de-allocate: 
    __Mi__export(&mat_result, "result__time");
    free__s_kt_matrix(&mat_result);
  }
#undef __Mi__export
  //! @return
  return true;
}

static int measureStub__case__basicCases__language(const uint nrows, const uint ncols) {
  const uint listOf_rows_size = 1; const uint listOf_cols_size = 1;
  uint listOf_rows[listOf_rows_size] = {nrows};   uint listOf_cols[listOf_cols_size] = {ncols};
  //! The call:
  allocOnStack__char__sprintf(2000, str_filePath, "synt_simMetricEval_dim_%u_%u", nrows, ncols); //! ie, allcoate the "str_filePath" variable.
  //const char *str_filePath, 
  const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/false);
  assert(is_ok);
  return is_ok;
}
