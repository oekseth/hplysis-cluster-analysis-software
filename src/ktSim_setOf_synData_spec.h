#ifndef ktSim_setOf_synData_spec_h
#define ktSim_setOf_synData_spec_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file ktSim_setOf_synData_spec
   @brief a proceudre to construct sytentic data-funcions (oekseth, 06. june 2017).
   @author Ole Kristian Ekseth (oekseth, 06. june 2017)).
**/

#include "kt_matrix.h"

/**
   @enum e_ktSim_setOf_synData_spec__localConf__multType
   @brief describes/idneitifes how to combine a "score" an a "factorMult",  eg, wrt. \url{row_1[m] = score_1*factor_mult;  row_2[m] = score_2*factor_mult;}. 
 **/
typedef enum e_ktSim_setOf_synData_spec__localConf__multType {
  e_ktSim_setOf_synData_spec__localConf__multType_first,
  e_ktSim_setOf_synData_spec__localConf__multType_second,
  e_ktSim_setOf_synData_spec__localConf__multType_both,
  e_ktSim_setOf_synData_spec__localConf__multType_undef
} e_ktSim_setOf_synData_spec__localConf__multType_t;

typedef struct s_ktSim_setOf_synData_spec__localConf {
  t_float mulFactor_m;
  uint cases1_cnt;
  uint ncols_local;
  t_float multFactor;
  //uint cases1_cnt;
  t_float factor_mult;
  const uint *indexPos_posToInvert; uint indexPos_posToInvert_size; //! where the later is optioanl and is used to 'revliey invert' teh data-sets. To exmaplify the latter we for $indexPos_posToInvert = [1, 5$ and input-scores=$[0, 1, 2, 3, 4, 5, 6]$ would get $[0, 1/1, 1/2, 1/3, 1/4, 5, 5]$. 
  e_ktSim_setOf_synData_spec__localConf__multType_t cases_factor_increase_typeOf_multInner;
} s_ktSim_setOf_synData_spec__localConf_t;

//! @return a 'splitting-array' to describe positons where a 'splitting' is to be appleid wrt. the feautre-based vlaue-ocnstruciton.
static uint *get_splitArray_fromScalar__s_ktSim_setOf_synData_spec__localConf_t(const uint split_id, uint *indexPos_posToInvert_size, const uint ncols_local) {
  //! Allocate the splliting
  const uint empty_0 = 0;
  uint *indexPos_posToInvert = (split_id > 0) ? allocate_1d_list_uint(split_id, empty_0) : 0; 
  *indexPos_posToInvert_size = split_id; //! where the later is optioanl and is used to 'revliey invert' teh data-sets. To exmaplify the latter we for $indexPos_posToInvert = [1, 5] $ and input-scores=$[0, 1, 2, 3, 4, 5, 6]$ would get $[0, 1/1, 1/2, 1/3, 1/4, 5, 5]$.
  if(split_id > 0) {     //! Set the splliting:
    uint split_blockSize = ncols_local / split_id;
    uint col_id = split_blockSize;
    for(uint i = 0; i < split_id; i++) {
      indexPos_posToInvert[i] = col_id; 
      uint next_index = col_id + split_blockSize;
      if(next_index >= ncols_local) {
	*indexPos_posToInvert_size = i + 1; //! ie, asw ethen hhvaa reacehd the 'end' fo the elvauaiton-appraoch.
	return indexPos_posToInvert;
      }
      col_id = next_index;
    }
  }
  return indexPos_posToInvert;
}

//! @return an tinalised veriosn of "s_ktSim_setOf_synData_spec__localConf_t"
static s_ktSim_setOf_synData_spec__localConf_t init__s_ktSim_setOf_synData_spec__localConf_t() {
  s_ktSim_setOf_synData_spec__localConf_t self;
  self.mulFactor_m = 1;
  self.cases1_cnt = 1;
  self.ncols_local = 10;
  self.multFactor = 1;
  self.factor_mult = 1;
  self.indexPos_posToInvert = NULL; self.indexPos_posToInvert_size = 0;
  self.cases_factor_increase_typeOf_multInner = e_ktSim_setOf_synData_spec__localConf__multType_first;  
  //! @return
  return self;
}
#define MF__fromConfig__s_ktSim_setOf_synData_spec_t() ({s_ktSim_setOf_synData_spec_t synData_spec = init__s_ktSim_setOf_synData_spec(); 	synData_spec.local_conf.mulFactor_m = mulFactor_m; 	synData_spec.local_conf.cases1_cnt = cases1_cnt; 	synData_spec.local_conf.ncols_local = ncols_local; 	synData_spec.local_conf.multFactor = multFactor; 	synData_spec.local_conf.factor_mult = factor_mult; assert(cases_factor_increase_typeOf_multInner < e_ktSim_setOf_synData_spec__localConf__multType_undef); 	synData_spec.local_conf.cases_factor_increase_typeOf_multInner = (e_ktSim_setOf_synData_spec__localConf__multType_t)cases_factor_increase_typeOf_multInner;   synData_spec.local_conf.indexPos_posToInvert = indexPos_posToInvert; synData_spec.local_conf.indexPos_posToInvert_size = indexPos_posToInvert_size; synData_spec; })

typedef struct s_ktSim_setOf_synData_spec {
  s_ktSim_setOf_synData_spec__localConf_t local_conf;
  // bool isTo_storeFeatures;
  uint cntSimMetricsEvaluated;
  s_kt_matrix_t matrixOf_features; //! which hold the number of idneitfed features wrt. oru evlauation.
} s_ktSim_setOf_synData_spec_t;
//! @return an tinalised veriosn of "s_ktSim_setOf_synData_spec"
s_ktSim_setOf_synData_spec_t init__s_ktSim_setOf_synData_spec() {
  s_ktSim_setOf_synData_spec self;
  self.local_conf = init__s_ktSim_setOf_synData_spec__localConf_t();
  self.cntSimMetricsEvaluated = 0;
  self.matrixOf_features = setToEmptyAndReturn__s_kt_matrix_t();
  //! @return
  return self;
}
static void free__s_ktSim_setOf_synData_spec_t(s_ktSim_setOf_synData_spec_t *self) {
  assert(self);
  free__s_kt_matrix(&(self->matrixOf_features));
}

//! Construct a feature-vector for the givne row-spec.
void constructFeatureVectors__s_ktSim_setOf_synData_spec_t(const s_ktSim_setOf_synData_spec_t *self, t_float *row_1, t_float *row_2, const uint i, const uint k) ;

//! Consutrct a datga-set capturing/desciribn the feautre-matrix-data-set we use/apply/evlaute in thsi evaluation-appraoch:
s_kt_matrix_t constructMAtrixOf_featureScores__s_ktSim_setOf_synData_spec_t(const s_ktSim_setOf_synData_spec_t *self);


#endif //! EOF
