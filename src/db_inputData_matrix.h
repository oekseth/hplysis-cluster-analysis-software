#ifndef db_inputData_matrix_h
#define db_inputData_matrix_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "db_ds_directMapping.h"
#include "kt_matrix.h"


//! Load relationships and synonyms into an s_db_ds_directMapping_t object
s_db_ds_directMapping_t loadData__s_db_ds_directMapping__db_inputData_matrix(const char *nameOfFile__relations, const char *nameOfFile__synonyms, const bool isTo_applyPreNormalizaiotnStep__onSynonyms, const e_db_ds_directMapping__initConfig_t objConfig);


/**
   @brief prodcue two data-files to examplify evlauation and applicaiotn of our ipnut-file-laoding proceudre (oekseth, 06. mar. 2017)
   @remarks Generate sample-data-sets to be 'used' to evlauate our appraoch.
**/
//!  prodcue two data-files to examplify evlauation and applicaiotn of our ipnut-file-laoding proceudre (oekseth, 06. mar. 2017)
void generateSample__storeInFiles__db_inputData_matrix(const uint cnt_heads, const uint cnt_pred, const uint nrows_each, const t_float fraciton_syns, const char *fName__rel, const char *fName__syn);
#endif //! EOF
