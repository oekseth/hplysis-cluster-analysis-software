
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
if(obj_baseToInclude.nrows) {
  assert(obj_baseToInclude.nrows != UINT_MAX);
  for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
    //assert(obj_baseToInclude.matrix[row_id]);
    assert(row_id < obj_baseToInclude.nrows);
    assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
    const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
    assert(row);
  }
 }
#endif

bool reading__wentOk = true;

s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
assert(data_obj.nameOf_rows == NULL);
//! ---
if(stringOf_sampleData_type_realLife == NULL) {
  // printf("\t build syntethic given tag=\"%s\", w/dims=[%u, %u] and noiseFraction=[%u, %u], at %s:%d\n", stringOf_sampleData_type, nrows, ncols, fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns, __FILE__, __LINE__);
  build_dataset_fromInputOrSample(&data_obj, /*inputFile=*/NULL, nrows, ncols, /*readData_fromStream=*/false, stringOf_sampleData_type, fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns, stringOf_sampleData_type_realLife, /*isTo_transpose=*/isTo_transposeMatrix);
  //assert(false); // FIXME: remove! <-- evaluate tiem-sigin of [ªbov€] for the 'binominal' case.
  if( (data_obj.cnt_rows == 0) || (data_obj.cnt_columns == 0) ) {
    fprintf(stderr, "\n!!\t Error(math-sample):\t\t Seems like your sample=\"%s\" did not have any chars, ie, as the EOF symbol was the first symbol to encountered (during the file-read-phase): could be that your file is Not redable: one example-reason (which we have observed ourself) conserns the case where you have forotten to explcitly close the file-handler (eg, to Not have called the \"closeFileHandler__s_kt_matrix_t(..)\" funcitoni in our s_kt_matrix_t structure-object); for quesiton please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n\n", stringOf_sampleData_type, __FUNCTION__, __FILE__, __LINE__);
    reading__wentOk = false;
  }

 } else { //! then we do Not set the nrows-count (ie, as we expect each file-input to have different input-sizes):
  //printf("\t build real-life, at %s:%d\n", __FILE__, __LINE__);
  build_dataset_fromInputOrSample(&data_obj, /*inputFile=*/stringOf_sampleData_type_realLife, UINT_MAX, UINT_MAX, /*readData_fromStream=*/false, stringOf_sampleData_type, fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns, NULL, /*isTo_transpose=*/isTo_transposeMatrix);
  if( (data_obj.cnt_rows == 0) || (data_obj.cnt_columns == 0) ) {
    fprintf(stderr, "\n!!\t Error(real-life):\t\t Seems like your input-file=\"%s\" did not have any chars, ie, as the EOF symbol was the first symbol to encountered (during the file-read-phase): could be that your file is Not redable: one example-reason (which we have observed ourself) conserns the case where you have forotten to explcitly close the file-handler (eg, to Not have called the \"closeFileHandler__s_kt_matrix_t(..)\" funcitoni in our s_kt_matrix_t structure-object); for quesiton please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n\n", stringOf_sampleData_type_realLife, __FUNCTION__, __FILE__, __LINE__);
    reading__wentOk = false;
  }

 }
//__macro__buildDataSet();

#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
if(obj_baseToInclude.nrows) {
  for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
    //assert(obj_baseToInclude.matrix[row_id]);
    assert(row_id < obj_baseToInclude.nrows);
    assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
    const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
    //if(row) 
    //{
    assert(row);
  }
 }
#endif

if(reading__wentOk == true) {
  assert(data_obj.cnt_rows > 0);
  assert(data_obj.cnt_columns > 0);
  if(obj_baseToInclude.nrows == 0) { //! where "obj_baseToInclude" is an object which may be used to 'merge' different data-sets.
    assert(data_obj.cnt_rows > 0); assert(data_obj.cnt_columns > 0);
#ifdef __MiC__inputType__matrixBase
    obj_matrixInput = initAndReturn__s_kt_matrix_base_t(data_obj.cnt_rows, data_obj.cnt_columns);
#else
    init__s_kt_matrix(&obj_matrixInput, data_obj.cnt_rows, data_obj.cnt_columns, /*isTo_allocateWeightColumns=*/false);
#endif
    assert(obj_matrixInput.nrows > 0);  assert(obj_matrixInput.ncols > 0);
  } else { //! then we combine 'the latter'
#ifdef __MiC__inputType__matrixBase
    obj_matrixInput = initAndReturn__s_kt_matrix_base_t(data_obj.cnt_rows + obj_baseToInclude.nrows, macro_max(data_obj.cnt_columns, obj_baseToInclude.ncols));
#else
    init__s_kt_matrix(&obj_matrixInput, data_obj.cnt_rows + obj_baseToInclude.nrows, macro_max(data_obj.cnt_columns, obj_baseToInclude.ncols), /*isTo_allocateWeightColumns=*/false);
#endif
    assert(obj_baseToInclude.matrix);
    //! Then copy the 'old data':
    for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
      //assert(obj_baseToInclude.matrix[row_id]);
      assert(row_id < obj_baseToInclude.nrows);
      assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
      const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
      //if(row) 
      //{
      assert(row);
      for(uint col_id = 0; col_id < obj_baseToInclude.ncols; col_id++) {
	if(row[col_id] != 0) {
	assert_possibleOverhead(col_id < obj_baseToInclude.ncols);
	assert_possibleOverhead(col_id < obj_matrixInput.ncols);
	obj_matrixInput.matrix[row_id][col_id] = row[col_id];
	} //! else we assume the cell-valeu is not set.
      }
      //}  
    }
  }
  assert(obj_matrixInput.nrows > 0);  assert(obj_matrixInput.ncols > 0);
#ifndef __MiC__inputType__matrixBase
  if(data_obj.nameOf_rows && isTo__copyNamesIfSet) { //! Parse the row-idnetifiers (oekseth, 06. feb. 2017):
    for(uint row_id = 0; row_id < data_obj.cnt_rows; row_id++) {
      const char *str_local = data_obj.nameOf_rows[row_id];
      if(str_local && strlen(str_local)) {
	set_stringConst__s_kt_matrix(&obj_matrixInput, row_id, str_local, /*addFor_column=*/false);
      }
    }
  }
  if(data_obj.nameOf_columns  && isTo__copyNamesIfSet) { //! Parse the column-idnetifiers (oekseth, 06. feb. 2017):
    for(uint row_id = 0; row_id < data_obj.cnt_columns; row_id++) {
      const char *str_local = data_obj.nameOf_columns[row_id];
      if(str_local && strlen(str_local)) {
	set_stringConst__s_kt_matrix(&obj_matrixInput, row_id, str_local, /*addFor_column=*/true);
      }
    }
  }
#endif //! else we asusem the strings are Not of itnerest (oekseth, 06. mar. 2017)
  
  //!
  //! Then we copy the result into the 'current set':
  for(uint row_id = 0; row_id < data_obj.cnt_rows; row_id++) {
    assert(data_obj.matrixOf_data[row_id]);
    const t_float *__restrict__ row = get_row__s_dataStruct_matrix_dense(&data_obj, row_id);
    assert(row);
    for(uint col_id = 0; col_id < data_obj.cnt_columns; col_id++) {
      if(row[col_id] != T_FLOAT_MAX) {
	obj_matrixInput.matrix[row_id+obj_baseToInclude.nrows][col_id] = row[col_id];
      } //! else we assume the cell-valeu is not set.
    }
  }
 }
  //! ----------------------------------------------
  //!
  //! De-allocate:
  free_s_dataStruct_matrix_dense_t(&data_obj);

#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
if(obj_baseToInclude.nrows && reading__wentOk) {
  for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
    //assert(obj_baseToInclude.matrix[row_id]);
    assert(row_id < obj_baseToInclude.nrows);
    assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
    const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
    //if(row) 
    //{
    assert(row);
  }
 }
#endif
