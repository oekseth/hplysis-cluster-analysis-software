  //! --------------------------------------------
  //! 
  //! De-allocate:
  const bool bothMAtrices_equal = (obj_1__ == obj_2__);
  if(obj_1__->matrix != matrix_input_1->matrix) {
    free__s_kt_matrix(&obj_cpy_ranksEachRow_1);          
  }
  if( obj_2__ && (bothMAtrices_equal == false) ) {
    if(obj_2__->matrix != matrix_input_2->matrix) {
      free__s_kt_matrix(&obj_cpy_ranksEachRow_2);          
    }
  }
