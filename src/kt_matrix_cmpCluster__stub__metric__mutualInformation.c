//!
//! Specify the 'return-varialbes':
t_float entropy_cluster1 = 0;    t_float entropy_cluster2 = 0;     t_float entry_both = 0;
t_float sumOf_1_sizes = 0;


//! 
//! Compute entropy for: "clsuter 1":
for(uint head_id = 0; head_id < nrows; head_id++) {
  assert(head_id < self->mapOfCluster1_size);
  t_float cntInCluster_1 = self->mapOfCluster1[head_id]; 
  if(cntInCluster_1 != 0) {
    assert(cntInCluster_1 != T_FLOAT_MAX);
    // TODO: valdiate correctness of [”elow] sumOf_1_sizes update.
    sumOf_1_sizes += cntInCluster_1;

    cntInCluster_1 = cntInCluster_1 / nrows; //! ie, adjust.
    //! Update:
    entropy_cluster1 += (cntInCluster_1 * mathLib_float_log_2_abs(cntInCluster_1));
  }
 }
entropy_cluster1 = mathLib_float_changeSign(entropy_cluster1); //! ie, chang ethe sign.
//! 
//! Compute entropy for: "clsuter 2":
for(uint head_id = 0; head_id < ncols; head_id++) {
  assert(head_id < self->mapOfCluster2_size);
  t_float cntInCluster_2 = self->mapOfCluster2[head_id]; 
  if(cntInCluster_2 != 0) {
    assert(cntInCluster_2 != T_FLOAT_MAX);
    cntInCluster_2 = cntInCluster_2 / ncols; //! ie, adjust
    //! Update:
    entropy_cluster2 += (cntInCluster_2 * mathLib_float_log_2_abs(cntInCluster_2));
  }
 }
entropy_cluster2 = mathLib_float_changeSign(entropy_cluster2); //! ie, chang ethe sign.
//! ---------------------------
//!
//! Comptue the 'intersection' between the two clusters:
for(uint head_id = 0; head_id < nrows; head_id++) {
  assert(head_id < self->mapOfCluster1_size);
  //! Idenitfy the 'realtive number of clusters' (ie, the praobility) in "cluster 2":
  t_float cntInCluster_1 = self->mapOfCluster1[head_id]; 
  if(cntInCluster_1 != 0) {
    assert(cntInCluster_1 != T_FLOAT_MAX);
    cntInCluster_1 = cntInCluster_1 / nrows; //! ie, adjust
    //!
    //! Iterate::
    for(uint tail_id = 0; tail_id < ncols; tail_id++) {
      t_float sum_and = self->matrixOf_coOccurence.matrix[head_id][tail_id];    	
      assert(sum_and != T_FLOAT_MAX);
      if(sum_and != 0) {
	sum_and = sum_and / sumOf_1_sizes;
	//! Idenitfy the 'realtive number of clusters' (ie, the praobility) in "cluster 2":
	t_float cntInCluster_2 = self->mapOfCluster2[tail_id]; 
	if(cntInCluster_2 != 0) {
	  assert(cntInCluster_2 != T_FLOAT_MAX);
	  cntInCluster_2 = cntInCluster_2 / ncols; //! ie, adjust
	  //!
	  //! Update the score:
	  const t_float denum = cntInCluster_1 * cntInCluster_2;
	  if(denum != 0) {
	    const t_float relativeWeight = sum_and / denum;
	    entry_both += sum_and*mathLib_float_log_2_abs(relativeWeight);
	  }
	} 
      }
    }
  }
 }
