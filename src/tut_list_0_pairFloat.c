#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_set_2dsparse.h" //! provide 2d-list-functoinatliy.

/**
   @brief describe basic lgoics wrt. our "kt_list_1d.h" API.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- general: use logics in our "kt_list_1d.h" 
   -- overall: demosntrate insert and 'reading' from our "s_kt_list_1d_pairFloat_t", where we use a 'covnert-into-2d' through our "convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(..)" (defined in our "kt_set_2dsparse.h").
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 5; const uint ncols = 3; 
  //!
  //! Allocate:
  const uint list_size = nrows*ncols;
  s_kt_list_1d_pairFloat_t vec_1 = init__s_kt_list_1d_pairFloat_t(list_size);
  assert(vec_1.list_size == list_size);
  //!
  //! Set values:
  uint curr_pos = 0;
  const t_float distance_default = 1;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      assert(curr_pos < vec_1.list_size);
      vec_1.list[curr_pos] = MF__initVal__s_ktType_pairFloat(row_id, col_id, distance_default);
      //! What we expect:
      s_ktType_pairFloat_t obj = vec_1.list[curr_pos];
      assert(obj.head == row_id);
      assert(obj.tail == col_id);
      assert(obj.score == distance_default);
      //! Increment:
      curr_pos++;
    }
  }
  assert(curr_pos == list_size);
  //!
  //! Convert into a 2d-list:
  s_kt_set_2dsparse_t sparse_1 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(&vec_1);
  assert(sparse_1._nrows == nrows); //! ie, gvien our [ªbove] isneriotn-policy.
  //!
  //! Access the 2d-list and write out the results:
  for(uint row_id = 0; row_id < sparse_1._nrows; row_id++) {
    uint key_val_size = 0; uint list_val_size = 0;
    const uint *key_val = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&sparse_1, row_id, &key_val_size);
    const t_float *list_val = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&sparse_1, row_id, &list_val_size);
    //! What we expect:
    assert(key_val_size == ncols);
    assert(list_val_size == ncols);
    assert(key_val);
    assert(list_val);
    //!
    //! Read through the 'sparse' data-set:
    for(uint col_id = 0; col_id < key_val_size; col_id++) {
      assert(key_val[col_id] == col_id);
      assert(list_val[col_id] == distance_default);
      printf("[%u][%u] = %f, at %s:%d\n", row_id, col_id, list_val[col_id], __FILE__, __LINE__);
    }
  }

  //!
  //! De-allocates:
  free__s_kt_list_1d_pairFloat_t(&vec_1);
  free_s_kt_set_2dsparse_t(&sparse_1);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}


