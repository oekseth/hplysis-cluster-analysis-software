//! De-allcoate if lareayd set:
  s_kt_clusterAlg_fixed_resultObject_t prev_clusterResult = initAndReturn__s_kt_clusterAlg_fixed_resultObject_t();
  //    assert(obj_result); free__s_kt_clusterAlg_fixed_resultObject_t(obj_result);
    //! Note: corresponds to: matrix_interpret(&obj_local, self->conf_postEval_pruning);
    //! Note: in cotnrast to the deuflat DB-SCAN evaluate the score of [i][i] instead of the count, ie, which examplies the "nodeThresh__useDirectSCoreInteadOfCount" param.
  const bool is_ok = compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(&obj_local, &prev_clusterResult, //obj_result, 
										 ///*obj_result*/&(self->obj_result_kMean), 
										 /*threshold_min=*/T_FLOAT_MIN_ABS, /*threshold_max=*/self->conf_postEval_pruning_cell,
										 /*nodeThres_min=*/T_FLOAT_MIN_ABS, /*nodeThres_max=*/self->conf_postEval_pruning_node,
										 /*nodeThresh__useDirectSCoreInteadOfCount=*/self->nodeThresh__useDirectSCoreInteadOfCount, //! ie, a 'unuqi apramter' wrt. the MCL-algorithm
										 /*useRelativeScore_betweenMinMax=*/false, 
										 /*isTo_adjustToRanks_beforeTrehsholds_forRows=*/false,
										 /*forRankAdjustment_adjsutTo_0_100=*/false,
										 /*isTo_insertIntoCompressedMatrix_beforeComptautiosn=*/false, //! ie, as we assuemt hat node-thresohlds are Not applied
										 &(self->objMetric__postMerge),
									       /*config=*/clusterConfig, false); 
    assert(is_ok);
    { //! Investigate if we are to bach-track.
      uint max_clusterCount = 0;
      get_cnt_clusters__s_kt_clusterAlg_fixed_resultObject_t(&prev_clusterResult, &max_clusterCount);      
      /* fprintf(stderr, "(info)\t cnt-clusters-idneitifed=%u, at %s:%d\n", max_clusterCount, __FILE__, __LINE__); */
      /* if(max_clusterCount == 0) { */
      /* 	if(true) { //! then we write otu the generated matrix: */
      /* 	  const char *exportFile = "tmp.mcl_emptyDisjoint.tsv"; */
      /* 	  fprintf(stderr, "(info::debug)\t Exports file=\"%s\" before comptuign disjtoint-forests, at %s:%d\n", exportFile, __FILE__, __LINE__); */
      /* 	  const bool is_ok = export__singleCall__s_kt_matrix_t(&obj_local, exportFile, NULL); */
      /* 	  assert(is_ok); */
      /* 	} */
      /* 	// assert(false); // TODO: remove.  */
      /* } */

      if(max_clusterCount < self->convergence_minClusters) {
	// assert(obj_result); free__s_kt_clusterAlg_fixed_resultObject_t(obj_result);
	assert(obj_result); free__s_kt_clusterAlg_fixed_resultObject_t(obj_result);
	*obj_result = prev_clusterResult; //! ie, then copy the object.
      } else if(obj_result->vertex_clusterId != prev_clusterResult.vertex_clusterId) { //! then the mermoy-allcoatiosn differs, ie, a simple test for uniqueness.
	free__s_kt_clusterAlg_fixed_resultObject_t(&prev_clusterResult);
	//prev_clusterResult = *obj_result; //! ie, a shallow copy.
      }
    }
