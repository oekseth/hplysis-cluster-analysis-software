#define __MiF__search__recursive __recursiveIns__s_db_rel_case32_t
#define __Mi__localType s_db_node_typeOfKey_rel_case32_t
#define __M__constant __M__constant__case_32

#define __MiF__search__slowEvaluateAll search__slowEvaluateAll__rel_case32_t
static uint __MiF__search__slowEvaluateAll(const __Mi__localType *self, const __Mi__localType__wrapper key, __Mi__localType__wrapper *scalar_result) {
#include "db_ds_bTree__stubSearch__evaluateAll.c"
}/*End of search()*/


//! De-allocate the 'allcoated children' in each of the "pointes" references (oekseth, mar. 2017).
static void free_mem__s_db_rel_case32_t(__Mi__localType *self) {
#define __MiF__free free_mem__s_db_rel_case32_t
  //! The call:
#include "db_ds_bTree__free.c"
}

static int search__s_db_rel_case32_t(const __Mi__localType *root, const __Mi__localType__wrapper key, __Mi__localType__wrapper *scalar_result) {
#include "db_ds_bTree__stubSearch.c"
}/*End of search()*/


static e_db_cmpResult_t __recursiveIns__s_db_rel_case32_t(__Mi__localType *ptr, const __Mi__localType__wrapper key, __Mi__localType__wrapper *upKey, __Mi__localType **newnode) {
#include "db_ds_bTree__stubrecursiveIns.c"
}



static void inserts_db_rel_case32_t(__Mi__localType **root, const __Mi__localType__wrapper key) {
#include "db_ds_bTree__stubAlloc.c"
}/*End of insert()*/


#undef __MiF__search__slowEvaluateAll
#undef __MiF__search__recursive
#undef __Mi__localType
#undef __M__constant
