//! -----------------------------------------------------------------------
//!
//! Copmtue for the left-side, ie, for 'cluster 1':
//! Idneitfy the max-number-of-combiatnions:
const t_float max_cntPairs = (t_float)__computeCombinatorics__by2(cnt_vertices);
assert(max_cntPairs > 0);

t_float sumOf_pairsInCluster_1 = 0;
assert(self->mapOfCluster1);
//printf("mapOfCluster1_size=%u, nrows=%u, at %s:%d\n", self->mapOfCluster1_size, nrows, __FILE__, __LINE__);
assert(self->mapOfCluster1_size != UINT_MAX);
for(uint row_id = 0; row_id < self->mapOfCluster1_size; row_id++) {
  const t_float cnt_inCluster_tmp = self->mapOfCluster1[row_id];
  assert(cnt_inCluster_tmp != T_FLOAT_MAX);
  if(cnt_inCluster_tmp != 0) {
    const uint cnt_inCluster = (uint)mathLib_float_abs(cnt_inCluster_tmp); //! where 'abs' is used to 'allow' the use of combiantoris.
    sumOf_pairsInCluster_1 += (uint)__computeCombinatorics__by2(cnt_inCluster);
  }
 }
//!
//! Copmtue for the right-side, ie, for 'cluster 2':
t_float sumOf_pairsInCluster_2 = 0;
assert(self->mapOfCluster2);
assert(self->mapOfCluster2_size != UINT_MAX);
//printf("|cluster-2|=%u, at %s:%d\n", self->mapOfCluster2_size, __FILE__, __LINE__);
for(uint row_id = 0; row_id < self->mapOfCluster2_size; row_id++) {
  const t_float cnt_inCluster_tmp = self->mapOfCluster2[row_id];
  assert(cnt_inCluster_tmp != T_FLOAT_MAX);
  // printf("cluster2[%u] = |%f|, at %s:%d\n", row_id, cnt_inCluster_tmp, __FILE__, __LINE__);
  if(cnt_inCluster_tmp != 0) {
    const uint cnt_inCluster = (uint)mathLib_float_abs(cnt_inCluster_tmp); //! where 'abs' is used to 'allow' the use of combiantoris.
    sumOf_pairsInCluster_2 += (uint)__computeCombinatorics__by2(cnt_inCluster);
  }
 }

//! 
//! Compute for the 'total count of possible pairs'
t_float cnt_totalOverlaps = 0;
for(uint row_id = 0; row_id < nrows; row_id++) {
  assert_possibleOverhead(matrix[row_id]);
  for(uint col_id = 0; col_id < ncols; col_id++) {
    const t_float score_tmp = mathLib_float_abs(matrix[row_id][col_id]); //! where 'abs' is used to 'allow' the use of combiantoris.
    assert_possibleOverhead(score_tmp != T_FLOAT_MAX);
    assert_possibleOverhead(score_tmp >= 0);
    const uint score = (uint)score_tmp;
    cnt_totalOverlaps += (uint)__computeCombinatorics__by2(score); //! ie, the max-posisble-number-of-pairs for 'the given number of intersecting cluster-points'.
  }
 }
    

    
const t_float cnt_possiblePairs = (2 * sumOf_pairsInCluster_1 * sumOf_pairsInCluster_2)/(cnt_vertices * (cnt_vertices-1));

//!
//! Combine, ie, udpate the 'score to return':
const t_float cnt_candidates = cnt_totalOverlaps - cnt_possiblePairs; //! where cnt_possiblePairs may be '0' for cases such as "sumOf_pairsInCluster_2=0"
const t_float cnt_max = 0.5*(sumOf_pairsInCluster_1 + sumOf_pairsInCluster_2) - cnt_possiblePairs;
if( (cnt_candidates != 0) && (cnt_max != 0) ) {score = cnt_candidates / cnt_max;}  //! ie, the 'relative importance of the pair-simliarty when compared to the possilbe number of pairs adjusted wr.t the symmetry-attribute'.


if( (cnt_possiblePairs == sumOf_pairsInCluster_1) && (cnt_possiblePairs == sumOf_pairsInCluster_2) ) {
  // FIXME: validate correctness of this 'special handling ... and if correct tehn udpate our documetnaiton/artilce.
  score = 1;
 }

// (Adjusted-rand)score=0.000000 given cnt_candidates=0.000000 (cnt_totalOverlaps=3.000000, cnt_possiblePairs=3.000000), cnt_max=0.000000 (sumOf_pairsInCluster_1=3.000000, sumOf_pairsInCluster_2=3.000000), at /home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/kt_matrix_cmpCluster__stub__metric__Rand_adjusted.c:61

if(false) {
  printf("----\n(Adjusted-rand)\tscore=%f given cnt_candidates=%f (cnt_totalOverlaps=%f, cnt_possiblePairs=%f), cnt_max=%f (sumOf_pairsInCluster_1=%f, sumOf_pairsInCluster_2=%f), at %s:%d\n", 
	 score, cnt_candidates, cnt_totalOverlaps, cnt_possiblePairs, cnt_max, sumOf_pairsInCluster_1, sumOf_pairsInCluster_2, __FILE__, __LINE__);
 }
//assert(cnt_totalOverlaps != 0); // TODO: if this does not hold then consider dorpping 'this'.

//assert(false); // FIXME: remove
