//#include "kd_tree.h"
//#include "kt_matrix.h"
#include "hp_ccm.h"

//! Compute the score for two matrices.
static t_float tut_kd_1_matrix_sse__compute(s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_2, const uint nn_numberOfNodes_max) {
  // for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++)
  e_kt_normType_t isTo_normalize = e_kt_normType_avgRelative;
  /*
    e_kt_normType_avgRelative_abs,
    e_kt_normType_avg_row_abs,
  */
  //s_kt_matrix_t mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_1, isTo_normalize);
  const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;
  t_float scalar_result = 0;
  if(false) {
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
    obj_metric.metric_id = sim_pre;
    obj_metric.typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_rank;
    //  t_float ballsize = 100;
    //  t_float ballsize = 10;
    t_float ballsize = 3;
    get_score_fromKD__kd_tree(mat_1, mat_2, 0, nn_numberOfNodes_max, ballsize, obj_metric, e_kd_tree_typeOf_scoreUnification_sum, &scalar_result);
  } else {
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE;
    s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    //ccm_config.metric_complexClusterCmp_obj_isTo_use = true;
    //ccm_config.metric_clusterDistance__vertexVertex = sim_pre;
    //ccm_config.metric_clusterDistance__cluster_cluster = sim_pre;
    ccm_config.metric_complexClusterCmp_vertexCmp_isTo_use = true;
    //ccm_config. = ;
    ccm_config.isTo_use_KD_treeInSimMetricComputations = true;
    uint empty_value = 0; //! ie, all verices are in the same cluster.
    s_kt_matrix_base_t mat_shallow = initAndReturn__notAllocate__s_kt_matrix_base_t(mat_1->nrows, mat_1->ncols, mat_1->matrix);
    uint *mapOf_vertexToCentroids = allocate_1d_list_uint(mat_shallow.nrows, empty_value);
    for(uint i = 0; i < mat_shallow.nrows; i++) {mapOf_vertexToCentroids[i] = 0;}
    const bool is_ok = ccm__advanced__singleMatrix__hp_ccm(ccm_enum, &mat_shallow, mapOf_vertexToCentroids, ccm_config, &scalar_result);
    assert(is_ok);
    //! De-allcoate:
    free_1d_list_uint(&mapOf_vertexToCentroids);
  }
  return scalar_result;
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief   (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks 
   @remarks related-tut-examples:
   -- "tut_norm_1_differentNormStrategies.c": a subset of this tut-example, ie, where latter is itnroducedused to avoid overhweilinming users with detials of clsuter-applicaiton. 
   -- "tut_kd_1_cluster_multiple_simMetrics.c": logics to evaluate implicaiton of different patterns. 
   -- "tut_norm_2_cluster_kd_dbScan.c": investigiaotn into different data nroamziaont strateiges.
   -- "tut_kd_1_matrix_sse.c": funciton for testing/explroing a given setting.  

   @remarks for a deep-level inviestion, logics are found in:
   -- ""
   -- "kt_matrix_cmpCluster__ccmMatrix__betweenScore__sum.c": the dfault strategy for updating coutns of cluster peorpteis. 
**/
int main(const int array_cnt, char **array) 
#else
  int tut_kd_1_matrix_sse(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  //hpLysis__globalInit__kt_api();
  //! *************************************************************************
    const uint pos_offset = 0;
#else
  const uint pos_offset = 1;
#endif 
  if(true) {
    const char *name_file_result = array[pos_offset + 1];
    assert(name_file_result);
    assert(strlen(name_file_result));
    FILE *file = fopen(name_file_result, "w");
    if (file == NULL) {
      fprintf(stderr, "!!\t Unable to open the file \"%s\" at [%s]:%s:%d\n", name_file_result, __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
    //fprintf(file, "#!\trank=%d\tscore\n", nn_numberOfNodes_max);
    //fprintf(file, "score\t%f\n", score);
    fprintf(file, "score\t");
    //!
    //! 
#if(true)
    const uint arrOf_fileNames_size = 1;
    const char *arrOf_fileNames[arrOf_fileNames_size] = {
      "data/anthony_missingData/wine.csv.hpLysis.tsv"};
#else
    const uint arrOf_fileNames_size = 10;
    const char *arrOf_fileNames[arrOf_fileNames_size] = {
	"data/anthony_missingData/wine.csv.hpLysis.tsv",
	"data/anthony_missingData/wine1.csv.hpLysis.tsv",
	"data/anthony_missingData/wine2.csv.hpLysis.tsv",
	"data/anthony_missingData/wine3.csv.hpLysis.tsv",
	"data/anthony_missingData/wine4.csv.hpLysis.tsv",
	"data/anthony_missingData/wine5.csv.hpLysis.tsv",
	"data/anthony_missingData/wine6.csv.hpLysis.tsv",
	"data/anthony_missingData/wine7.csv.hpLysis.tsv",
	"data/anthony_missingData/wine8.csv.hpLysis.tsv",
	"data/anthony_missingData/wine9.csv.hpLysis.tsv"
    };
#endif
    for(uint i = 0; i < arrOf_fileNames_size; i++) {
      const char *name_file_1      = arrOf_fileNames[i];
      assert(name_file_1);
      assert(strlen(name_file_1));
      //!
      //! Parse data:
      s_kt_matrix_t mat_1 = readFromAndReturn__file__advanced__s_kt_matrix_t(name_file_1, initAndReturn__s_kt_matrix_fileReadTuning_t());
      t_float score = 0;
      //      if( (strlen(name_file_1) == strlen(name_file_2)) && (0 == strcmp(name_file_2, name_file_1) ) )
      { //! then teh files are the same:
	//! Compute: 
	score = tut_kd_1_matrix_sse__compute(&mat_1, NULL, /*nn_numberOfNodes_max=*/UINT_MAX);
	//! Store:
	fprintf(file, "%f", score);
	if((i  + 1) != arrOf_fileNames_size) {fprintf(file, "\t");}
	else {fprintf(file, "\n");}
	printf("score:\t%f\t %s\t at %s:%d\n",score, name_file_1, __FILE__, __LINE__);
	//! DE-allocate:
	free__s_kt_matrix(&mat_1);
      }
    }
        fclose(file); file = NULL;
  } else {
    if(array_cnt < 4) {
    fprintf(stderr, "Specify paramters: <file-1> <file-2> <result-file> [optional: <closest-points-count: all='inf'>]. For questions, contact Dr. Ekseth at [oekseth@gmail.com].\n");
    return -1;
  }
    const char *name_file_1      = array[pos_offset + 1];
    const char *name_file_2      = array[pos_offset + 2];
    const char *name_file_result = array[pos_offset + 3];
    assert(name_file_1);
    assert(name_file_2);
    assert(name_file_result);
    assert(strlen(name_file_1));
    assert(strlen(name_file_2));
    assert(strlen(name_file_result));
    uint nn_numberOfNodes_max = 10;
    if(array_cnt >= 5) {
    const char *str_maxPointsRank = array[pos_offset + 4];
    if(str_maxPointsRank && strlen(str_maxPointsRank)) {
    nn_numberOfNodes_max = (uint)atoi(str_maxPointsRank);
  }
  }
    //!
    //! Parse data:
    s_kt_matrix_t mat_1 = readFromAndReturn__file__advanced__s_kt_matrix_t(name_file_1, initAndReturn__s_kt_matrix_fileReadTuning_t());
    t_float score = 0;
    if( (strlen(name_file_1) == strlen(name_file_2)) && (0 == strcmp(name_file_2, name_file_1) ) ) { //! then teh files are the same:
    score = tut_kd_1_matrix_sse__compute(&mat_1, NULL, nn_numberOfNodes_max);
  } else {
    s_kt_matrix_t mat_2 = readFromAndReturn__file__advanced__s_kt_matrix_t(name_file_2, initAndReturn__s_kt_matrix_fileReadTuning_t());
    //! Apply:
    score = tut_kd_1_matrix_sse__compute(&mat_1, &mat_2, nn_numberOfNodes_max);
    //! DE-allocate:
    free__s_kt_matrix(&mat_2);
  }
    //! DE-allocate:
    free__s_kt_matrix(&mat_1);
  
    FILE *file = fopen(name_file_result, "w");
    if (file == NULL) {
    fprintf(stderr, "!!\t Unable to open the file \"%s\" at [%s]:%s:%d\n", name_file_result, __FUNCTION__, __FILE__, __LINE__);
    return false;
    }
    //printf("score: %f, at %s:%d\n", score, __FILE__, __LINE__);
    fprintf(file, "#!\trank=%d\tscore\n", nn_numberOfNodes_max);
    fprintf(file, "score\t%f\n", score);
    fclose(file); file = NULL;
  }
    //!
    //! @return
#ifndef __M__calledInsideFunction
    //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  //hpLysis__globalFree__kt_api();
    //! *************************************************************************  
#endif
    return true;
}
