 const char *nameOf_experiment = "syntetic_noise";  //! ie, a set of fucnitons with increased noise-permtuation-ratio
  /* const uint config__kMeans__defValue__k__min = 2;   const uint config__kMeans__defValue__k__max = 4; */
  /* const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min;  */
//const char *nameOf_experiment = "syntetic_noise"; //! A syntatctic data-set:
  //! -------------------------------------------------------------------------
  //!
  //!
  //! Intiate the sytnetic data-sets:
  const uint mapOf_functionStrings_base_size = 17;
  const char *mapOf_functionStrings_base[mapOf_functionStrings_base_size] = {
    "lines-different-ax",
    "lines-curved",
    "lines-different-ax-and-axx",
    "lines-ax-inverse-x",
    //! --
    "lines-circle",
    "lines-sinsoid",
    "lines-sinsoid-curved",
    //! [ªbove]: cnt=(3+4)=7;
    //! ------------------------ 
    "random",
    "uniform",
    "binomial_p05",
    "binomial_p010",
    "binomial_p005",
    //! --
    "flat",
    "linear-equal",
    "linear-differentCoeff-b",
    "linear-differentCoeff-a",
    "sinus",
    //! [ªbove]: cnt=(5+5)=10 ... ie, 17 cases.
  };
  
  //printf("at %s:%d\n", __FILE__, __LINE__);
  //! ---            
  static const uint stringOf_noise_size = 3;
  //assert((uint)s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef == stringOf_noise_size); //! ie, what we expect for [”elow]
  const char *stringOf_noise[stringOf_noise_size] = {
    "",
    "-medium",
    "-large",
  };
  //! --------------------------
  //!

  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint mapOf_realLife_size = mapOf_functionStrings_base_size * stringOf_noise_size * __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  //printf("at %s:%d\n", __FILE__, __LINE__);
  for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {      
    for(uint noise_id = 0; noise_id < stringOf_noise_size; noise_id++) {
      assert(base_id < mapOf_functionStrings_base_size);       assert(noise_id < stringOf_noise_size);
      //! Budil the sample-string:
      //! Note: though [”elow] will result in a memory-leaksge we assume 'this leakge' will be insigidncat, ie, where 'this leakge' is allowed to reduce the change of the user (ie, you) to be cofnused wr.t how to apply the lgocis we exmaplify (in this code-chunk-example).
      const char empty_0 = 0; const uint char_Def_Size = 1000;
      //printf("at %s:%d\n", __FILE__, __LINE__);
      char *stringOf_sampleData_type = allocate_1d_list_char(char_Def_Size, empty_0); sprintf(stringOf_sampleData_type, "%s%s", mapOf_functionStrings_base[base_id], stringOf_noise[noise_id]);
      for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
	const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
	assert(current_pos < mapOf_realLife_size);
	//printf("at %s:%d\n", __FILE__, __LINE__);
	//!
	//! Add the object:
	mapOf_realLife[current_pos].tag = stringOf_sampleData_type;
	mapOf_realLife[current_pos].file_name = stringOf_sampleData_type;
	mapOf_realLife[current_pos].fileRead_config = fileRead_config__syn;
	mapOf_realLife[current_pos].inputData__isAnAdjcencyMatrix = false;
	mapOf_realLife[current_pos].k_clusterCount = k_clusterCount;
	mapOf_realLife[current_pos].mapOf_vertexClusterId = NULL;
	mapOf_realLife[current_pos].mapOf_vertexClusterId_size = 0;
	mapOf_realLife[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
	mapOf_realLife[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
	//mapOf_realLife[current_pos] = {/*tag=*/stringOf_sampleData_type, /*file_name=*/stringOf_sampleData_type, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/k_clusterCount, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans};
	//!
	current_pos++;
      }
    }
  }
