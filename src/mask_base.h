#ifndef mask_base_h
#define mask_base_h

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"

//! @return true if the same 'inner index' is alwasy of interest for the different row-combinations.
const bool ktMask__vectorsAre_consistentlyInteresting(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char **mask1, char **mask2, const char transpose);
//#include "correlation_api.h"




#endif //! EOF
