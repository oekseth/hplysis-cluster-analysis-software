#ifndef kt_para_3dSchedule_h
#define kt_para_3dSchedule_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_para_3dSchedule
   @brief logics to idenitfying data-blocks to evalaute in parallel for symmetric data-matrices (oekseth, 06. okt. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
   - provide a set of different implemetnations, implementaitons which are inlcuded in order to test the improtance--implicaitons of diffneret cheudling-strategies.
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
/* #include "log_clusterC.h" */
/* #include "e_kt_correlationFunction.h" */

/**
   @brief identify the different scheuling-cases to be used in the assignment of the parallel blocks.
   @remarks supprots the following cases: 
   # case(a) sub-divide into ["nrows/cores"]["ncols/cores"]; 
   # case(b) rows: allocate each "bloc_id + core_id" to each core; 
   # case(c) rows: contious blocks (for the cores) seperately for each row, ie, where we count the last block as "ncols/2" (given the symmetric proeprty), and where the 'the core with the fwest number of cells' are given the 'start-at-index' 'ability'.
   # case(d) cols: (similar to case(b));
   # case(e) cols: (similar to case(c));
 **/
typedef enum e_kt_para_3dSchedule_typeOf_scheduleStrategy {
  e_kt_para_3dSchedule_typeOf_scheduleStrategy_simpleDivision_SymmstricOverhead, //! wherew we have two 'parallel categories' of 'schedulings', a 'scheuidlign comptued for "|rows|/|cores|", ie, 
  e_kt_para_3dSchedule_typeOf_scheduleStrategy_simpleDivision_unSymmstricOverhead,
  e_kt_para_3dSchedule_typeOf_scheduleStrategy_rows_iterativBlockAssignment,
  e_kt_para_3dSchedule_typeOf_scheduleStrategy_rows_iterativBlockAssignment_SMsymmetricAdjustment,
  e_kt_para_3dSchedule_typeOf_scheduleStrategy_cols_iterativBlockAssignment,
  e_kt_para_3dSchedule_typeOf_scheduleStrategy_cols_iterativBlockAssignment_SMsymmetricAdjustment,
  //e_kt_para_3dSchedule_typeOf_scheduleStrategy_
} e_kt_para_3dSchedule_typeOf_scheduleStrategy_t;

/**
   @struct s_kt_para_3dSchedule
   @brief data-strucutre and logics to shceudle a comptaution (eg, of a 2d-correlation-emtric using 3d-comptuatiosn) between muliple cores sharing the same memory-chunk.
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
 **/
typedef struct s_kt_para_3dSchedule {
  uint cnt_cores;
  uint nrows; uint ncols;
  uint SM_rows; //! ie, the block-size, a block-sizeed used in memory-tiling-optmizaiton: set to "1" if tiling is Nto to be used (eg, in comopuation of naive correlation-metric-implemntaitons or if tiling may not efficnetly be used, eg, in cases such as wrt. comptuation of Kendall's Tau.
  uint SM_cols;
  uint mapOf_blockId_rows_innerSize; //! is the number of 'inner elmenets' in the "mapOf_blockId_rows" and "mapOf_blockId_cols"
  uint **mapOf_blockId_startEachBlock_rows; //! where the row-id may be inferred throguh muliplicaiton with the "SM" value
  uint **mapOf_blockId_startEachBlock_cols; //! where the row-id may be inferred throguh muliplicaiton with the "SM" value
  uint **mapOf_blockId_startEachBlock_rows_size; //! where the row-id may be inferred throguh muliplicaiton with the "SM" value
  uint **mapOf_blockId_startEachBlock_cols_size; //! where the row-id may be inferred throguh muliplicaiton with the "SM" value
  //uint **mapOf_blockId_prevThreadId; //! which hold the 'id' of the prev-thread-id at a given 'current-pos': used to idnetify the block-size of each
  //uint **mapOf_blockId_cols; //! where the column-id may be inferred throguh muliplicaiton with the "SM" value
} s_kt_para_3dSchedule_t;

/**
   @brief allocates the s_kt_para_3dSchedule_t object
   @param <self> sit eh object to intiatlize.
   @param <cnt_cores> is the number of cores to idneitfy a 'scheudling policy' for.
   @param <nrows> is the number rows in the input-matrix
   @param <ncols> is the number columns in the input-matrix
   @param <SM> the block-size, a block-sizeed used in memory-tiling-optmizaiton: set to "1" if tiling is Nto to be used (eg, in comopuation of naive correlation-metric-implemntaitons or if tiling may not efficnetly be used, eg, in cases such as wrt. comptuation of Kendall's Tau.
   @param <schedule_type> the schedule-polocy: identiies how the schuelding is to be made, ie, may have sigificant imppact on the perofrmance.
 **/
static void init_s_kt_para_3dSchedule(s_kt_para_3dSchedule_t *self, const uint cnt_cores, const uint nrows, const uint ncols, const uint SM, const e_kt_para_3dSchedule_typeOf_scheduleStrategy_t schedule_type) {
  //! What we expect:
  assert(self);
  assert(cnt_cores > 0); assert(cnt_cores != UINT_MAX); assert(ncols > 0); assert(nrows > 0);
  
  //! -------------------------------------------
  //!
  //! Update the object with base-porperties:
  self->cnt_cores = cnt_cores;
  assert(SM != UINT_MAX); assert(SM != 0); 
  self->nrows = nrows;
  self->ncols = ncols;
  self->SM_rows = SM;   self->SM_cols = SM;
  self->mapOf_blockId_rows_innerSize = 0;
  self->mapOf_blockId_startEachBlock_rows = NULL;  
  self->mapOf_blockId_startEachBlock_cols = NULL;  
  self->mapOf_blockId_startEachBlock_rows_size = NULL;  
  self->mapOf_blockId_startEachBlock_cols_size = NULL;  
  //! -------------------------------------------
  const loint cnt_elements_total = nrows * ncols;
  //!
  //! Apply logics:
  switch(schedule_type) 
    { case e_kt_para_3dSchedule_typeOf_scheduleStrategy_simpleDivision_SymmstricOverhead: {
	//! Allocate:
	const uint localList_size = 2; 	
	//! -----------------------------------
	assert(localList_size >= 2); //! ie, as we otherwise have pointless allcoaiton-intiation.
	const uint default_value = 0;
	self->mapOf_blockId_startEachBlock_rows = allocate_2d_list_uint(cnt_cores, localList_size, default_value);
	self->mapOf_blockId_startEachBlock_cols = allocate_2d_list_uint(cnt_cores, localList_size, default_value);
	self->mapOf_blockId_startEachBlock_rows_size = allocate_2d_list_uint(cnt_cores, localList_size, default_value);
	self->mapOf_blockId_startEachBlock_cols_size = allocate_2d_list_uint(cnt_cores, localList_size, default_value);
	self->mapOf_blockId_rows_innerSize = localList_size - 1;
	//! -----------------------------------
	//!
	//! Specify the local-max-size wrt. the number of internal 'buckets':
	for(uint thread_id = 0; thread_id < cnt_cores; thread_id++) {
	  self->mapOf_blockId_startEachBlock_rows[thread_id][0] = localList_size - 1;
	  self->mapOf_blockId_startEachBlock_cols[thread_id][0] = localList_size - 1;
	  self->mapOf_blockId_startEachBlock_rows_size[thread_id][0] = localList_size - 1;
	  self->mapOf_blockId_startEachBlock_cols_size[thread_id][0] = localList_size - 1;
	  //! ----- 
	  assert(self->mapOf_blockId_startEachBlock_rows[thread_id][0] <= self->mapOf_blockId_rows_innerSize); //! ie, wrt. the max-number-of-elements.
	}
	//! 
	//! Update the mapping-size:
	const loint cnt_elements = nrows;
	loint cnt_elements_each = 0;
	loint cntThreads_firstCase = cnt_cores; loint cnt_elements_each_lastCase = 0;
	if(cnt_cores <= cnt_elements) {
	  cnt_elements_each = cnt_elements / (cnt_cores * SM);	
	  const loint cntThreads_lastCase = cnt_elements % cnt_cores;
	  if(cntThreads_lastCase != 0) { //! then the 'last chunk' is Not assicated with any threads.
	    cntThreads_firstCase = cnt_cores - cntThreads_lastCase;
	    assert( (cnt_elements % cntThreads_firstCase) == 0);
	    const loint cnt_remaining = cnt_elements - (cnt_elements_each * cntThreads_firstCase * SM);
	    assert(cnt_remaining > 0);
	    cnt_elements_each_lastCase = cnt_remaining / (cntThreads_lastCase * SM);
	  } //! else we assuemt aht all elements 'fits' into the current 'chunk of entites'.	  
	} else {
	  cnt_elements_each = 1; //! ie, then one row for each computer-thread.
	  cntThreads_firstCase = cnt_elements;
	  cnt_elements_each_lastCase = 0;
	}
#ifndef NDEBUG
	{ //! Wvalidate correctness:
	  assert(cnt_cores >= cntThreads_firstCase);
	  assert(cnt_elements_each < (loint)UINT_MAX); //! ie, as we otehrwise need to change the internal data-type
	  assert(cnt_elements_each_lastCase < (loint)UINT_MAX); //! ie, as we otehrwise need to change the internal data-type
	  const loint cntThreads_lastCase = cnt_cores - cntThreads_firstCase; 
	  const loint cnt_firstCase = cnt_elements_each * cntThreads_firstCase;
	  const loint cnt_lastCase = cnt_elements_each_lastCase *cntThreads_lastCase;
	  //! What we expect:
	  assert( (cntThreads_firstCase + cntThreads_lastCase) == cnt_cores);
	  printf("cnt_firstCase=%u, cnt_lastCase=%u, SM=%u, cnt_elements=%u, at %s:%d\n", (uint)cnt_firstCase, (uint)cnt_lastCase, (uint)SM, (uint)cnt_elements, __FILE__, __LINE__);
	  assert( (SM*(cnt_firstCase + cnt_lastCase)) == cnt_elements);
	}
#endif	
	loint cnt_elements_added = 0;	
	//! -----------------------------------
	//! 
	//! Specifically 'state' the mapping-polocieis for each thread:
	//! Note: handle the case where the 'total numbe rof elmenets' is Not divisalbe by the overall number of elmeents:
	uint thread_id = 0;
	uint cnt_rows_described = 0;
	for(; thread_id < cntThreads_firstCase; thread_id++) {
	  if(cnt_elements_added < cnt_elements_total) {
	    assert(cnt_rows_described < nrows);
	    const uint local_size = (uint)cnt_elements_each*SM;
	    self->mapOf_blockId_startEachBlock_rows[thread_id][1] = cnt_rows_described;
	    self->mapOf_blockId_startEachBlock_rows_size[thread_id][1] = local_size;
	    //! ---
	    self->mapOf_blockId_startEachBlock_cols[thread_id][1] = 0;
	    self->mapOf_blockId_startEachBlock_cols_size[thread_id][1] = ncols;
	    //! ---
	    cnt_elements_added += (loint)(cnt_elements_each * ncols);
	    assert(local_size == self->mapOf_blockId_startEachBlock_rows_size[thread_id][1]);
	    cnt_rows_described += local_size;
	  } else {self->mapOf_blockId_startEachBlock_rows_size[thread_id][1] == 0;} //! ie, what we expect
	}
	//printf("thread_id=%u, cntThreads_firstCase=%u, cnt_cores=%u, cnt-start-in-each=%u, cnt-rows-last-in-each=%u, given nrows=%u,  at %s:%d\n", (uint)thread_id, (uint)cntThreads_firstCase, (uint)cnt_cores, (uint)cnt_elements_each, (uint)cnt_elements_each_lastCase, nrows, __FILE__, __LINE__);
	for(; thread_id < cnt_cores; thread_id++) {
	  if(cnt_elements_added < cnt_elements_total) {
	    const uint local_size = (uint)cnt_elements_each_lastCase*SM;
	    assert(cnt_rows_described < nrows);
	    self->mapOf_blockId_startEachBlock_rows[thread_id][1] = cnt_rows_described;
	    self->mapOf_blockId_startEachBlock_rows_size[thread_id][1] = local_size;
	    //! ---
	    self->mapOf_blockId_startEachBlock_cols[thread_id][1] = 0;
	    self->mapOf_blockId_startEachBlock_cols_size[thread_id][1] = ncols;
	    //! ---
	    cnt_elements_added += (loint)(local_size * ncols);
	    cnt_rows_described += local_size;
	    assert(local_size == self->mapOf_blockId_startEachBlock_rows_size[thread_id][1]);
	  } else {self->mapOf_blockId_startEachBlock_rows_size[thread_id][1] == 0;} //! ie, what we expect
	}
	printf("(debug)\t cnt_elements_added=%llu/%llu while cnt_rows_described=%u/%u, at %s:%d\n", cnt_elements_added, cnt_elements_total, cnt_rows_described, nrows, __FILE__, __LINE__);
	assert(cnt_elements_added == cnt_elements_total); //! ie, what we expect, ie, taht 'exacatly' the complete set of cells are evaluated.
	assert(cnt_rows_described == nrows);



	//! -----------------------------------
	//! 
	//! Update the global object:
	self->SM_rows = cnt_elements_each;
	self->SM_cols = ncols; //! ie, as each vertex is assicated to 'the complete set of columns'.
	break;
      }  
    case e_kt_para_3dSchedule_typeOf_scheduleStrategy_simpleDivision_unSymmstricOverhead: {
	/* const uint local_size = 2; 	const uint default_value = 0; */
	/* self->mapOf_blockId = allocate_2d_list_uint(cnt_cores, local_size, default_value); */
	/* self->mapOf_blockId_rows_innerSize = local_size - 1; */
	/* //! Update the mapping-size: */
	/* const loint cnt_elements = nrows * ncols; */
	/* const loint cnt_elements_each = cnt_elements / cnt_cores; */
	/* loint cnt_elements_added = 0; */
	/* for(uint thread_id = 0; thread_id < cnt_cores; thread_id++) { */
	/*   self->mapOf_blockId[thread_id][0] = 1; */
	/*   self->mapOf_blockId[thread_id][0] = cnt_elements; */
	/*   cnt_elements_added += cnt_elements; */
	/* } */
	/* //! Handle the case where the 'total numbe rof elmenets' is Not divisalbe by the overall number of elmeents: */
	/* for(uint thread_id = 0; thread_id < cnt_cores; thread_id++) { */
	/*   if(cnt_elements_added < cnt_elements) { */
	/*     self->mapOf_blockId[thread_id][0] += 1; */
	/*     cnt_elements_added++; */
	/*   } */
	/* } */
	/* assert(cnt_elements_added == cnt_elements_total); //! ie, what we expect. */
	assert(false); // FIXME: add seomthing.

	assert(false); // FIXME: thereafter describe a 'permtuation of this' where we use a 'fxied count-appraoch' for the columns

	assert(false); // FIXME: similar to our "mine_matrix.c" ... write a 'case' where we allcoate 'sperately for each [row][col], ie, a case which may result in a signifance perfomance-overehad for small row--oclumns sizes ... and a considerably incrase wr.t memory-consutmpion.
	
	assert(false); // FIXME: use our "mine_matrix.c" as a tempalte wrt. a 'test-bed'.

	break;
      }
    case e_kt_para_3dSchedule_typeOf_scheduleStrategy_rows_iterativBlockAssignment: {
      
      assert(false); // FIXME: add seomthing.
      break;
    }
    case e_kt_para_3dSchedule_typeOf_scheduleStrategy_rows_iterativBlockAssignment_SMsymmetricAdjustment: {
      
      assert(false); // FIXME: add seomthing.
      break;
    }
    case e_kt_para_3dSchedule_typeOf_scheduleStrategy_cols_iterativBlockAssignment: {
      
      assert(false); // FIXME: add seomthing.
      break;
    }
    case e_kt_para_3dSchedule_typeOf_scheduleStrategy_cols_iterativBlockAssignment_SMsymmetricAdjustment: {
      
      assert(false); // FIXME: add seomthing.
      break;
    }
    };

  //! -------------------------------------------
  //!
  //! Validate: what we expect:
#ifndef NDEBUG
  uint cnt_rows_described = 0;
  loint cnt_cols_described = 0;
  for(uint thread_id = 0; thread_id < cnt_cores; thread_id++) {
    assert(self); 
    assert(thread_id < self->cnt_cores);
    assert(self->mapOf_blockId_startEachBlock_rows_size);
    assert(self->mapOf_blockId_startEachBlock_rows[thread_id]);
    assert(self->mapOf_blockId_startEachBlock_rows[thread_id][0] <= self->mapOf_blockId_rows_innerSize); //! ie, wrt. the max-number-of-elements.
    const uint cnt_local_size = self->mapOf_blockId_startEachBlock_rows_size[thread_id][0];
    assert(cnt_local_size >= 1); //! ie, the 'minimum assumption'.
    assert(cnt_local_size <= self->mapOf_blockId_rows_innerSize);
    for(uint k = 0; k < cnt_local_size; k++) {
      const uint cnt_rows = self->mapOf_blockId_startEachBlock_rows_size[thread_id][k + 1];
      const uint cnt_cols = self->mapOf_blockId_startEachBlock_cols_size[thread_id][k + 1];
      cnt_rows_described += cnt_rows;
      printf("[%u]\tcnt_local_size=%u, cnt_rows_described=%u/%u, cnt_cols=%u/%u, at %s:%d\n", thread_id, cnt_local_size, cnt_rows_described, nrows,  cnt_cols, ncols, __FILE__, __LINE__);
      cnt_cols_described += (loint)(cnt_cols*cnt_rows);
      assert(cnt_rows_described <= nrows);
    }
  }
  printf("(debug)\t cnt_cols_described=%llu/%llu while cnt_rows_described=%u/%u, at %s:%d\n", cnt_cols_described, (loint)(ncols), cnt_rows_described, nrows, __FILE__, __LINE__);
  assert(cnt_rows_described == nrows); //! ie, as we expect all rows to have been evlauated.
  assert(cnt_cols_described == (loint)(nrows*ncols)); //! ie, as we expect all cells to have been evlauated.
  //  assert(cnt_cols_described == (loint)ncols); //(loint)(nrows*ncols)); //! ie, as we expect all cells to have been evlauated.
#endif
}

//! De-allocate the s_kt_para_3dSchedule_t object.
void free__s_kt_para_3dSchedule(s_kt_para_3dSchedule_t *self);

/**
   @brief identify the max-number of blocks for a given core/thread id.
   @param <self> is the intialized object which hold the thread-id-allocations.
   @param <thread_id> is the core to get the allocatiosn for.
   @return the number of blocks to be evaluated for thread_id
 **/
static uint identify_maxCntBlocks_forAGivenThread__s_kt_para_3dSchedule_t(const s_kt_para_3dSchedule_t *self, const uint thread_id) {
  assert(self); 
  assert(thread_id < self->cnt_cores);
  assert(self->mapOf_blockId_startEachBlock_rows);
  assert(self->mapOf_blockId_startEachBlock_rows[thread_id]);
  assert(self->mapOf_blockId_startEachBlock_rows[thread_id][0] <= self->mapOf_blockId_rows_innerSize); //! ie, wrt. the max-number-of-elements.
  //! @return the number of blocks assicated to thread_id:
  return self->mapOf_blockId_startEachBlock_rows[thread_id][0]; //! ie, whwhere the first 'psotion' is belived to hold the max-number of insrted elements
}


/**
   @brief identify the block-size for the thread_id in question.
   @param <self> is the intialized object which hold the thread-id-allocations.
   @param <thread_id> is the core to get the allocatiosn for.
   @param <block_id> is the current block-id to get the row-chunk for
   @param <scalarStart_row_id> is the row-start-postion of the iteraiotn
   @param <scalarStart_col_id> is the column-start-postion of the iteration
   // @return the number of elemnets to iterate over, ie, (scalarStart_row_id + ret-val, scalarStart_col_id + ret-val).
   @remarks a slow permtaution of the ''id-fetching': may have signficant implicatiosn for small block-sizes.
 **/
static void slow1__idneitfy_indexRange__s_kt_para_3dSchedule_t(const s_kt_para_3dSchedule_t *self, const uint thread_id, const uint block_id, uint *scalarStart_row_id, uint *scalarStart_col_id, uint *scalarEnd_row_id, uint *scalarEnd_col_id) {
#include "kt_para_3dSchedule__func__findIndexRange.c" //! ie, apply the lgoics
}
/**
   @brief identify the block-size for the thread_id in question.
   @param <self> is the intialized object which hold the thread-id-allocations.
   @param <thread_id> is the core to get the allocatiosn for.
   @param <block_id> is the current block-id to get the row-chunk for
   @param <scalarStart_row_id> is the row-start-postion of the iteraiotn
   @param <scalarStart_col_id> is the column-start-postion of the iteration
   // @return the number of elemnets to iterate over, ie, (scalarStart_row_id + ret-val, scalarStart_col_id + ret-val).
   @remarks a slow permtaution of the ''id-fetching': may have signficant implicatiosn for small block-sizes.
 **/
void slow2__idneitfy_indexRange__s_kt_para_3dSchedule_t(const s_kt_para_3dSchedule_t *self, const uint thread_id, const uint block_id, uint *scalarStart_row_id, uint *scalarStart_col_id, uint *scalarEnd_row_id, uint *scalarEnd_col_id) ;
/**
   @brief identify the block-size for the thread_id in question.
   @param <self> is the intialized object which hold the thread-id-allocations.
   @param <thread_id> is the core to get the allocatiosn for.
   @param <block_id> is the current block-id to get the row-chunk for
   @param <scalarStart_row_id> is the row-start-postion of the iteraiotn
   @param <scalarStart_col_id> is the column-start-postion of the iteration
   // @return the number of elemnets to iterate over, ie, (scalarStart_row_id + ret-val, scalarStart_col_id + ret-val).   
 **/
static inline __attribute__((always_inline)) void idneitfy_indexRange__s_kt_para_3dSchedule_t(const s_kt_para_3dSchedule_t *self, const uint thread_id, const uint block_id, uint *scalarStart_row_id, uint *scalarStart_col_id, uint *scalarEnd_row_id, uint *scalarEnd_col_id) {
  #include "kt_para_3dSchedule__func__findIndexRange.c" //! ie, apply the lgoics
}


#endif //! EOF
