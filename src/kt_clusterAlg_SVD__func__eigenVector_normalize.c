  { //! Step(3): noramlize the input-vector:
    //! Note: in [”elow] we Investigte if the 'nroamlized eigen-vector' has converged:
    t_float max_value = T_FLOAT_MIN_ABS;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      if(arrOf_result[row_id] > max_value) {
	max_value = arrOf_result_updated[row_id];
      }
    }
    if(max_value == 0) { //! then we assume the input-data is set to "0"
      // printf("!!\t investigate this case, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      has_converged = true; //! ie, iot. stop the exeuction
    } else {
      //! 'Normalize' based on the max-value:
      const t_float max_value_inv = 1/max_value; //(max_value != 0) ? 1/max_value : 0;
      for(uint row_id = 0; row_id < nrows; row_id++) {
	//if(arrOf_result_updatedt[row_id] != 0) 
	{
	  arrOf_result[row_id] = arrOf_result_updated[row_id] * max_value_inv;
	  if(arrOf_result_prev[row_id] != arrOf_result[row_id]) {
	    t_float change_factor = arrOf_result_prev[row_id] / arrOf_result[row_id];
	    if(change_factor > 1) { change_factor = (1/change_factor);}
	    if(change_factor < self.maxError_forConvergence) {has_converged = false;} //! ie, as the 'change' is 'within' the 'accepted' error-rate.
	  }
	}
      }
    }
  }
