=head
 #! Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 #!
 #! This file is part of the poset library.
 #!
 #! the poset library is free software: you can redistribute it and/or modify
 #! it under the terms of the GNU General Public License as published by
 #! the Free Software Foundation, either version 3 of the License, or
 #! (at your option) any later version.
 #!
 #! the poset library is distributed in the hope that it will be useful,
 #! but WITHOUT ANY WARRANTY; without even the implied warranty of
 #! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #! GNU General Public License for more details.
 #!
 #! You should have received a copy of the GNU General Public License
 #! along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 #!
=cut


=head
   @brief a wrapper-template to build centrality-functions for different configurations
   @remarks last generated at Fri Dec  2 15:20:45 2016 from "build_codeFor_tiling.pl"
=cut

#!/usr/bin/perl -w
package knittingTools::gui_lib::call_hpLysis__terminalInputArgs;

sub get_arrOf_input_args {return ["input-file-1","input-file-2","input-file-1-mask","input-file-2-mask","input-file-1-columnWeights","input-file-2-columnWeights","result-file","result-format","cnt-rows","cnt-cols","isTo_transposeMatrix","input-use-stream-explicit-size","include-stringIdentifers-in-matrix","exportInto-javaScript-syntax","exportInto-JSON-syntax","result-isTo-exportInputFile","help--printAll-correlationMetrics","before-filter-correlation-type-id","before-filter-correlation-type-isTo-export","before-filter-correlation-preStep","after-filter-correlation-type-id","after-filter-correlation-type-isTo-export","after-filter-correlation-preStep","in-clustering-correlation-type-id","in-clustering-correlation-preStep","ifTwoMatrix-whenToMerge","hca_method","kmeans-number-of-clusters","kmeans-number-of-iterations","kmean-useMedian","kmean-alternativeAlgorithm-useMedoid","som-apply","som-Tau","som-number-of-iterations","som_gridSize_x","som_gridSize_y","pca-apply","sample-data-distribution","sample-data-distribution-secondMatrix","percent-toAppendWithSampleDistributionFor-rows","percent-toAppendWithSampleDistributionFor-columns","filter-eitherOr-percentValuesAboveCountForScalarValue-row-min","filter-eitherOr-percentValuesAboveCountForScalarValue-row-max","filter-eitherOr-percentValuesAboveCountForScalarValue-columns-min","filter-eitherOr-percentValuesAboveCountForScalarValue-columns-max","filter-eitherOr-absoluteValueMinValue","filter-eitherOr-absoluteValueMinValue-row","filter-eitherOr-absoluteValueMinValue-column","filter-eitherOr-valueDiff-rows-min","filter-eitherOr-valueDiff-rows-max","filter-eitherOr-valueDiff-cols-min","filter-eitherOr-valueDiff-cols-max","filter-eitherOr-STD-rows-min","filter-eitherOr-STD-rows-max","filter-eitherOr-STD-cols-min","filter-eitherOr-STD-cols-max","filter-eitherOr-kurtosis-rows-min","filter-eitherOr-kurtosis-rows-max","filter-eitherOr-kurtosis-cols-min","filter-eitherOr-kurtosis-cols-max","filter-eitherOr-skewness-rows-min","filter-eitherOr-skewness-rows-max","filter-eitherOr-skewness-cols-min","filter-eitherOr-skewness-cols-max","filter-cellMask-valueUpperLower-min","filter-cellMask-valueUpperLower-max","filter-cellMask-meanAbsDiff-row-min","filter-cellMask-meanAbsDiff-row-max","filter-cellMask-meanAbsDiff-column-min","filter-cellMask-meanAbsDiff-column-max","filter-cellMask-medianAbsDiff-row-min","filter-cellMask-medianAbsDiff-row-max","filter-cellMask-medianAbsDiff-column-min","filter-cellMask-medianAbsDiff-column-max","adjustValues-log","adjustValues-subract-from-row","adjustValues-subract-from-columns","adjustValues-sum-of-squares-rows","adjustValues-sum-of-squares-columns"];}

1;
