
#include "autoGenerated_template_correlation_tile_wrapper.c"

// /*
//  * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
//  *
//  * This file is part of the poset library.
//  *
//  * the poset library is free software: you can redistribute it and/or modify
//  * it under the terms of the GNU General Public License as published by
//  * the Free Software Foundation, either version 3 of the License, or
//  * (at your option) any later version.
//  *
//  * the poset library is distributed in the hope that it will be useful,
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of
// v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  * GNU General Public License for more details.
//  *
//  * You should have received a copy of the GNU General Public License
//  * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
//  */




// /**
//    @brief a wrapper-template to build tile-functions for different configurations
//    @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_notMask> name of funciton
//    @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit> name of function
//    @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit> name of function
//    @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_notMask_weight> name of function: use weight
//    @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit_weight> name of function: use weight
//    @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit_weight> name of function: use weight
//    @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask> name of funciton
//    @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit> name of function
//    @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit> name of function
//    @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask_weight> name of function: use weight
//    @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit_weight> name of function: use weight
//    @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit_weight> name of function: use weight
//    @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight> weight-macro-function
//    @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight> SSE-macro-fucntion for copmtuing weights
//    @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument> which if set impleis that we use the input-weight as argumetn when calling the distnace-macro-matrix.
//    @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax> which if set to "1" impleis that we do seelec tthe max-value instead of cocnatnating valeus.
//    @tparam<TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate> which refers to teh "e_template_correlation_tile_typeOf_distanceUpdate" enum and is used to simplify the comptaution of partial sets wrt. SSE-computaiton: relates to "s_template_correlation_tile_temporaryResult_SSE", "s_template_correlation_tile_temporaryResult" and "s_kt_computeTile_subResults_t".
//    @tparam<TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices> which if set to "1" is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
//    @tparam<TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction> ... 
//    @tparam<> ... 
//  **/
 

// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC_notMask
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC_notMask_weight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit_weight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit_weight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif


// #ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask_weight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit_weight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit_weight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif

// //! ---
// #ifndef TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif
// //! ---
// #ifndef TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif



// //! ----------------------------------------
// //! Build a enw function:
// //! Note: Not use weight for: euclid:
// #define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_notMask
// #define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
// #define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
// //! --
// #define TEMPLATE_distanceConfiguration_useWeights 0
// #define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// //#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// //! Apply [above] specificaiotns:
// #include "template_correlation_tile.cxx"
// //! ...... 
// #define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
// #define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
// #define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_explicit //! ie, Not use mask
// //! --
// #define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #define TEMPLATE_distanceConfiguration_useWeights 0
// #define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// //! Apply [above] specificaiotns:
// #include "template_correlation_tile.cxx"
// //! ...... 
// #define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
// #define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
// #define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_implicit //! ie, Not use mask
// //! --
// #define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #define TEMPLATE_distanceConfiguration_useWeights 0
// #define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// //! Apply [above] specificaiotns:
// #include "template_correlation_tile.cxx"

// //! ----------------------------------------
// //! Build a enw function:
// //! Note: Use weight for: euclid:
// #define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_notMask_weight
// #define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask_weight
// #define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
// //! --
// #define TEMPLATE_distanceConfiguration_useWeights 1
// #define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// //! Apply [above] specificaiotns:
// #include "template_correlation_tile.cxx"
// //! ...... 
// #define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit_weight
// #define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows  TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit_weight
// #define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_explicit //! ie, Not use mask
// //! --
// #define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #define TEMPLATE_distanceConfiguration_useWeights 1
// #define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// //! Apply [above] specificaiotns:
// #include "template_correlation_tile.cxx"
// //! ...... 
// #define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit_weight
// #define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit_weight
// #define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_implicit //! ie, Not use mask
// //! --
// #define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #define TEMPLATE_distanceConfiguration_useWeights 1
// #define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
// //! Apply [above] specificaiotns:
// #include "template_correlation_tile.cxx"




// //! -------------------------------------------------------------------
// //!            Reset template-parameters:
// //! -------------------------------------------------------------------
// #undef TEMPLATE_NAMEOF_MAIN_FUNC_notMask
// #undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
// #undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
// #undef TEMPLATE_NAMEOF_MAIN_FUNC_notMask_weight
// #undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit_weight
// #undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit_weight
// //! -------**: start: local-row-functions:
// #undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
// #undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
// #undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
// #undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask_weight
// #undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit_weight
// #undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit_weight
// //! -------**: end: local-row-functions:
// #undef TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
// #undef TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
// #undef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
// #undef TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
// #undef TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
// #undef TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
// #undef TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
