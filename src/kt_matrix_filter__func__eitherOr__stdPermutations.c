#ifndef __macro_getValue
#error "!!\t Please udpate your calls specifiying 'this' macro-varialbe'"
#endif
/* #ifndef __macro__setValueToEmpty */
/* #error "!!\t Please udpate your calls specifiying 'this' macro-varialbe'" */
/* #endif */
for(uint row_id = 0; row_id < cnt_rows; row_id++) {
  if(local_mapOf_interesting[row_id]) { //! then we inspect valeus wrt. the column:
    const t_float *__restrict__ row = local_matrix[row_id];   
    //! Intiate the object:
    s_matrix_deviation_std_row_t config_deviation; setTo_empty__s_matrix_deviation_std_row(&config_deviation);
    get_forRow_sampleDeviation__matrix_deviation_implicitMask(row, cnt_cols, &config_deviation);
    //!
    //! Compare wrt. the threshold:
    const t_float thresholdValue = __macro_getValue();
  
/* #ifndef __macro__setMatrixValue */
/*     printf("[%u]=%f inside-range[%f, %f]='%s', at %s:%d\n", row_id, thresholdValue, min_cnt_rows, max_cnt_rows,  */
/* 	   ((thresholdValue < min_cnt_rows) || (thresholdValue > max_cnt_rows) ) ? "no" : "yes", */
/* 	   __FILE__, __LINE__); */
/* #endif */

    if(
#ifdef __macro__setMatrixValue
       true
#else
       (thresholdValue < min_cnt_rows) || (thresholdValue > max_cnt_rows) 
#endif
       ) { //! then we asusme the value is Not of interest
      if(local_mapOf_interesting[row_id]) {
	local_mapOf_interesting[row_id] = false;
	for(uint col_id = 0; col_id < cnt_cols; col_id++) {
#ifdef __macro__setMatrixValue
	  if(__macro__getValueFromMatrix(row_id, col_id)) {
	    const t_float value = __macro__getValueFromMatrix(row_id, col_id);
	    __macro__setMatrixValue(row_id, col_id, mathLib_float_abs(thresholdValue - value));	    
	  }
#else
	  __macro__setValueToEmpty(row_id, col_id);
#endif
	}
      }
    }
  }
 }
//#undef __macro_getValue
#undef __macro__setValueToEmpty
/* #undef __macro__getValueFromMatrix */
/* #undef __macro__setMatrixValue */
