t_float sumOf_not = 0; t_float sumOf_and = 0;
for(uint head_id = 0; head_id < nrows; head_id++) {
  for(uint tail_id = 0; tail_id < ncols; tail_id++) {
    const t_float sum_or  = self->matrixOf_coOccurence_or.matrix[head_id][tail_id];
    const t_float sum_and = self->matrixOf_coOccurence.matrix[head_id][tail_id];
    assert(sum_or != T_FLOAT_MAX);	assert(sum_and != T_FLOAT_MAX);
    assert(sum_or >= sum_and);
    sumOf_not += (sum_or - sum_and);
    sumOf_and += sum_and;
  }
 }
