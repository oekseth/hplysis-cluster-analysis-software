#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_memAlloc.h"
#include "def_typeOf_float.h"
#include "def_intri.h"
#include "kt_mathMacros.h"
#include "types_base.h"
#include "types.h"
#include<math.h>

//! Src: http://homepages.inf.ed.ac.uk/rbf/BOOKS/PHILLIPS/cips2edsrc/DFT.C

/*   
   This is the forward 2D DFT.
   This is the centered format.
   This runs from -N/2 to N/2 + 1
   This runs from -M/2 to M/2 + 1
   This works for MxN matrices

   The time domain h has subscripts
   m n.  The freq domain H has
   subscripts u v.  These are both
   varied over M N.

   The angle p = -2jpienu/N  - 2jpiemv/M
             p = 2jpie[ (-nuM) - (mvN)]/MN

   Making substitutions to speed up processing

   for(i=0; i<ROWS; i++){
      for(j=0; j<COLS; j++){
           image1[i][j] = 0;
	   image2[i][j] = 0;
	   image3[i][j] = 0;
	   image4[i][j] = 0;
      }
   }


   for(i=0; i<ROWS; i++)
      for(j=0; j<COLS; j++)
           image3[i][j] = hi_pass[i][j];

   dft_2d(image1, image2, image3, image4);
*/
// short x[ROWS][COLS], y[ROWS][COLS], r[ROWS][COLS], i[ROWS][COLS];
void dft_2d(const int nrows, const int ncols, t_float **x, t_float **r, t_float **i) {
//void dft_2d(const int nrows, const int ncols, t_float **x, t_float **y, t_float **r, t_float **i) {
#define pie 3.1425927

  const int M_2      = nrows/2;
  const int N_2      = ncols/2;
  const t_float twopie_d = (2. * pie)/(nrows*ncols);
  assert(twopie_d != 0.0); //!< ie, as it otherwise would be pointless. 


  for(int v=-M_2; v<=M_2-1; v++){
    for(int u=-N_2; u<=N_2 -1; u++){
      printf("\n      v=%3d u%3d--", v, u);
      int um = u*nrows;
      int vn = v*ncols;
      
      t_float x0 = 0;
      t_float y0 = 0;
      
      for(int m=-M_2; m<=M_2 - 1; m++){
	const int mvn = m*vn;
	printf(" m%2d", m);
	for(int n=-N_2; n<=N_2 - 1; n++){
	  /* you can probably separate the following
	     to increase speed */
	  /**p  = 2. * pie * (n*u*M + m*v*N) / (N*M);**/
	  const t_float p  = twopie_d * (n*um + mvn);
	  //const t_float c  = cos((int)p);
	  const t_float c  = mathLib_float_cos(p);
	  assert(false);
	  const t_float s  = mathLib_float_sin(p);
	  /* the y array is all zero is remove it
	     from the calculations
	  */
	  /*****
               x0 = x0 + c*x[m+M_2][n+N_2] + s*y[m+M_2][n+N_2];
               y0 = y0 + c*y[m+M_2][n+N_2] - s*x[m+M_2][n+N_2];
	  *****/
	  x0 = x0 + c*x[m+M_2][n+N_2];
	  y0 = y0 - s*x[m+M_2][n+N_2];
	}  /* ends loop over n */
      }  /* ends loop over m */
      
      r[v+M_2][u+N_2] = x0;
      i[v+M_2][u+N_2] = y0;
    }  /* ends loop over u */
  }  /* ends loop over v */

}  /* ends dft_2d */




int main() {}
