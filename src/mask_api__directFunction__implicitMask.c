   assert(data); assert(nrows); assert(ncols);
  const t_float default_value_float = 0;

  //! Configurations:
  const t_float scoreTrheashold_min = self.scoreThreshold_min; const t_float scoreTrheashold_max = self.scoreThreshold_max;
  t_float emptyValue_toSet = T_FLOAT_MAX;
  if(e_mask_api_config_typeOf_emptyValue_0 == self.typeOf_implictEmptyValue_toSet) {emptyValue_toSet = 0;}
  else if(e_mask_api_config_typeOf_emptyValue_T_FLOAT_MAX == self.typeOf_implictEmptyValue_toSet) {emptyValue_toSet = T_FLOAT_MAX;}
  else {assert(false);} //! ie, as we then need to add support for this case.
  //! Set the SSE-vectors:
  VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
  VECTOR_FLOAT_TYPE vec_threshold_min = VECTOR_FLOAT_SET1(scoreTrheashold_min); VECTOR_FLOAT_TYPE vec_threshold_max = VECTOR_FLOAT_SET1(scoreTrheashold_max);
  //! SSE-sepcs:

#if(1)
#ifndef NDEBUG
// #warning "FIXME[JC]: may you ivnestigate why ouf VECTOR_FLOAT_convertFrom_CHAR_buffer(..) call results in a seg-fault, ie, whye teh latter 'results in out-of-boundary-memory-access'?"
#endif
const bool isTo_use_sseFast = false;
const uint cnt_elements_intri = 0;
#else //! then we use the approac belived to be fast:
  const bool isTo_use_sseFast =  ( (ncols > VECTOR_FLOAT_ITER_SIZE * 4) && (self.typeOf_optimization_SSE == e_mask_api_config_typeOf_SSE_fast)); //! ie, if SSE may imrpvoe the eprfomrance then we use 'it'.
  const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);
#endif

  t_float **new_matrix = data; //! ie, the default assumption
bool __mem_isAllocted = false;
  if(self.localConfig_allocateMatrix == true) {
    new_matrix = allocate_2d_list_float(nrows, ncols, default_value_float);
    __mem_isAllocted = true;
  }
  //! The iteration:


  if(mask != NULL) { //! then we assume explcit masks
    if(scoreTrheashold_min == scoreTrheashold_max) { //! then we assume the score-threshodls are not of itnerest
      for(uint row_id = 0; row_id < nrows; row_id++) {
	uint col_id = 0;
	if(isTo_use_sseFast) {
	  for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) {	    
	    assert( (col_id + VECTOR_FLOAT_ITER_SIZE) <= ncols);
	    assert(mask[row_id]);
	    VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&data[row_id][col_id]);
#if(__localConfig__use__uintMask == 1)
	    VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_convertFrom_UINT_data(mask[row_id], col_id);
#else
	    VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask[row_id][col_id]);
#endif
	    //! Adjust the valeu based on the mask:	    	    
	    //! The operation:
	    // FIXME[performance-evlauation]: cevaluate/comrept eh time-cost of muliplcaiton VS the 'alterntive non-SSE' of using IF-clauses.
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, vec_mask, vec_spec_emptyValue);

	    //! Store the result:
	    VECTOR_FLOAT_STORE(&new_matrix[row_id][col_id], vec_result);
	  }
	}
	for(; col_id < ncols; col_id++) {
	  if(!mask[row_id][col_id]) {
	    new_matrix[row_id][col_id] = emptyValue_toSet; //! ie, assign an 'implcit mask' proeprty.
	  } else {new_matrix[row_id][col_id] = data[row_id][col_id];} //! ie, then a simple copy-operaiton
	}
      }  
    } else { //! then we apply the score-thresholds:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	uint col_id = 0;
	if(isTo_use_sseFast) {
	  for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) {
	    VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&data[row_id][col_id]);
	    VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask[row_id][col_id]);
#if(configure_SSE_useFor_mask_explicit == 1) 
	    //! Then we do not rever thte order as the 'reverting the order twice' impleis that the roder is correct, ie, not need 'doing this explcitly':
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit_notRevertOrder(vec_input, vec_mask, vec_spec_emptyValue);
	    //! Apply min-max-filters:
	    // FIXME[perofmrance] test teh case where for [”elow] [”elow] cases:
	    vec_result = VECTOR_FLOAT_insideRange_notRevertOrder(vec_input, vec_threshold_min, vec_threshold_max);
#else //! then we use a strategy which may result is lower/slower performance:
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, VECTOR_FLOAT_SET1(T_FLOAT_MAX), vec_spec_emptyValue);
	    //! Apply min-max-filters:
	    // FIXME[perofmrance] test teh case where for [”elow] [”elow] cases:
	    vec_result = VECTOR_FLOAT_insideRange(vec_input, vec_threshold_min, vec_threshold_max);
#endif	    
	  }
	}
	for(; col_id < ncols; col_id++) {
	  const bool isOf_interest = __local_cellOF_interest_explicit(data, mask, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max);
	  if(isOf_interest == false) {
	    new_matrix[row_id][col_id] = emptyValue_toSet; //! ie, assign an 'implcit mask' proeprty.
	  } else {new_matrix[row_id][col_id] = data[row_id][col_id];} //! ie, then a simple copy-operaiton
	}
      }  
    }
  } else { //! then we assume implict masks are used.
    if(scoreTrheashold_min == scoreTrheashold_max) { //! then we assume the score-threshodls are not of itnerest
      //! Thenw sue a for-loop, ie, as we expec thatat users 'aware' of the possiblty/'topon' to map a 1d-array to ad-matrix would not call this fucniton 'for such a simple case' (eosekth, 06. sept. 2016).      
      if(self.localConfig_allocateMatrix == true) {
	if(new_matrix != data) {
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    memcpy(new_matrix[row_id], data[row_id], sizeof(t_float)*ncols);
	  }
	} //! else we asusemt eh data is the same (oekseth, 06. feb. 2017).
      }
      if(emptyValue_toSet != T_FLOAT_MAX) {
	//! Then we need to explictly update the values
	//! Note: If the default value is differengt from T_FLOAT_MAX then we need to update [”elow]:
	replace_T_FLOAT_MAX_withNewEmptyValue__mask_api(new_matrix, nrows, ncols, emptyValue_toSet, self);
      }
    } else { //! then we apply thes core-trhesholds:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	if(emptyValue_toSet == T_FLOAT_MAX) {
	  uint col_id = 0;
	  if(isTo_use_sseFast) {
	    for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) {
	      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&data[row_id][col_id]);
	      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_implicitMask(vec_input, VECTOR_FLOAT_SET1(T_FLOAT_MAX), vec_spec_emptyValue);
	      //! Store the result:
	      VECTOR_FLOAT_STORE(&new_matrix[row_id][col_id], vec_result);	      
	    }
	  }

	  for(; col_id < ncols; col_id++) {
	    bool isOf_interest = true; //! ie, default assumption
	    isOf_interest = __local_cellOF_interest_explicit_minMax(data, isOf_interest, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max);
	    if(isOf_interest == false) {
	      new_matrix[row_id][col_id] = emptyValue_toSet; //! ie, assign an 'implcit mask' proeprty.
	    }
	  }
	} else { //! then we need to introduce an extra bracnhing-statement: to improve the perofmrance we esperte the cases.
	  uint col_id = 0;
	  if(isTo_use_sseFast) {
	    for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) {
	      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&data[row_id][col_id]);
#if(configure_SSE_useFor_mask_explicit == 1) 
	      //! Then we do not rever thte order as the 'reverting the order twice' impleis that the roder is correct, ie, not need 'doing this explcitly':
	      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_implicitMask_notRevertOrder(vec_input, VECTOR_FLOAT_SET1(T_FLOAT_MAX), vec_spec_emptyValue);
	      //! Apply min-max-filters:
	      // FIXME[perofmrance] test teh case where for [”elow] [”elow] cases:
	      vec_result = VECTOR_FLOAT_insideRange_notRevertOrder(vec_input, vec_threshold_min, vec_threshold_max);
#else //! then we use a strategy which may result is lower/slower performance:
	      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_implicitMask(vec_input, VECTOR_FLOAT_SET1(T_FLOAT_MAX), vec_spec_emptyValue);
	      //! Apply min-max-filters:
	      // FIXME[perofmrance] test teh case where for [”elow] [”elow] cases:
	      vec_result = VECTOR_FLOAT_insideRange(vec_input, vec_threshold_min, vec_threshold_max);
#endif
	      //! Store the result:
	      VECTOR_FLOAT_STORE(&new_matrix[row_id][col_id], vec_result);	     	      
	    }
	  }

	  for(; col_id < ncols; col_id++) {
	    bool isOf_interest = true; //! ie, default assumption
	    isOf_interest = __local_cellOF_interest_explicit_minMax(data, isOf_interest, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max);
	    if(isOf_interest == false) {
	      new_matrix[row_id][col_id] = emptyValue_toSet; //! ie, assign an 'implcit mask' proeprty.
	    } else {
	      if(data[row_id][col_id] == T_FLOAT_MAX) {
		new_matrix[row_id][col_id] = emptyValue_toSet; //! ie, assign an 'implcit mask' proeprty.
	      }
	    }
	  }
	}
      }
    }
  }

//assert(__mem_isAllocted == false); //! ie, wht we expect at thsi exec-point
