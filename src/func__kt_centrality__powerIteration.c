

#if(__config__compute_eigenVector__isTo_useIncrementallyUpdatedVector == 1)
  t_float *__restrict__ arrOf_result_updated = arrOf_result;
#else 
  t_float *__restrict__ arrOf_result_updated = arrOf_result_prev;
#endif


{ //! Apply the power-interation:

    assert(nrows == ncols); //! ie, as we assume that the matrix is a neightbouthood/adjcency matrix.          



  //!
  //! Apply logics wrt. the power-iteraiton:
  has_converged = false;      uint cnt_iterations_real = 0;
    for(uint cnt_interations = 0; cnt_interations < self.max_cnt_iterations; cnt_interations++) {
      if(has_converged == false) { //! then we start/'apply' a enw iteration:
	//! Step(4): iterate until the algorithm converges:
	bool has_converged_local = false;
	{ //! Update the "B" vector wrt. centrliaty:
	  //! Note: Compute eigenvectors for one 'iterative step' in the eigenvector-computation/iteration  (oekseth, 06. mai. 2016).
	  //! Note(2) first implmeneted in our "lib_centrality.js" Java-Script library (oekseth, 06. des. 2015).

	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    //const uint size_cntTails = matrixOf_result.get_safe_list_object(row_id)->get_current_pos();
	    const t_float *__restrict__ row = matrix[row_id];
#if(__config__compute_eigenVector__isTo_useSSE == 0)
    //! ******************************************************************************
	    t_float scalar_result = 0;
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      //! IDenitfy the sum of distances:
#if(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
	      const t_float min_distance = row[col_id]; //tailObj.get_score();
#elif(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
	      const t_float min_distance = row[col_id] * mapOf_vertexSums[col_id];
#elif(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
	      const t_float tmp_val = row[col_id] * mapOf_vertexSums[col_id];
	      const t_float min_distance = (tmp_val != 0) ? mathLib_float_log(tmp_val) : 0;
#else
    //! ******************************************************************************
#error "!!\t Add support for this case"
#endif

	      //! Update the result:
	      scalar_result += min_distance;
	    }
    //! ******************************************************************************
#else //! thebn we use an SSE-version:
	    //! --------------------------------------
	    //! Initaite:
	    uint col_id = 0;
#if(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOAD(&row[col_id]); //tailObj.get_score();
#elif(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row[col_id]), VECTOR_FLOAT_LOAD(&mapOf_vertexSums[col_id]));
#elif(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row[col_id]), VECTOR_FLOAT_LOAD(&mapOf_vertexSums[col_id]));
	    vec_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(VECTOR_FLOAT_LOG(vec_result));
#else
    //! ******************************************************************************
#error "!!\t Add support for this case"
#endif

	    
	    //! -------------
	    //! Iterate:
	    col_id += VECTOR_FLOAT_ITER_SIZE;	    
	    for(uint col_id = 0; col_id < ncols; col_id += VECTOR_FLOAT_ITER_SIZE) {
#if(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum)
	      VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_LOAD(&row[col_id]); //tailObj.get_score();
#elif(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply)
	      VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row[col_id]), VECTOR_FLOAT_LOAD(&mapOf_vertexSums[col_id]));
#elif(__macro__typeOf_eigenVectorType_use == __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog)
	      VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row[col_id]), VECTOR_FLOAT_LOAD(&mapOf_vertexSums[col_id]));
	      vec_local = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(VECTOR_FLOAT_LOG(vec_local));
#else
	      //! ******************************************************************************
#error "!!\t Add support for this case"
#endif
	      
	      //!
	      //! Update the result:
	      vec_result = VECTOR_FLOAT_ADD(vec_result, vec_local);
	    }	    
	    const t_float scalar_result = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
    //! ****************************************************************************** End-of-SSE
#endif
	    arrOf_result[row_id] = scalar_result;
	  }
	  //! Step(3): noramlize the input-vector:
	  //! Note: in [”elow] we Investigte if the 'nroamlized eigen-vector' has converged:
	  uint cnt_changed = 0;    
#include "kt_clusterAlg_SVD__func__eigenVector_normalize.c"
	  //! @return true if the 'solution' has converged:
	  has_converged_local = (cnt_changed == 0);
	}
	//const bool has_converged_local = false; assert(false); // fIXEM: remvoe this and include [below].
	//const bool has_converged_local = __compute_eigenVector__i__iterate(arrOf_result, arrOf_result_prev, typeOf_useEitherOr, typeOf_not_useEitherOr_sumOf_tailDistances, typeOf_not_useEitherOr_divideBy_cntRelationsFromTail, powerIncrease_ofDistanceSum, maxError_forConvergence); cnt_iterations_real++;
	if(has_converged_local == false) {
	  //! Then we update the arrays, ie, based on 'previous' results:
	  memcpy(arrOf_result_prev, arrOf_result, sizeof(t_float)*nrows);
	} else {has_converged = true;}
      } //! else we 'perform iterations, iterations expected to have a lwo comptautional cost, ie, given the Giga-FLOPS on moderns comptuers.
    }
    //! De-allocate locally reserved memory:
    free_1d_list_float(&arrOf_result_prev);

    //! Print a status-message:
    if(true) {printf("cnt_iterations_real=%u, has_converged=%u, max_cnt_iterations=%u, at %s:%d\n", cnt_iterations_real, has_converged, self.max_cnt_iterations, __FILE__, __LINE__);}

}
