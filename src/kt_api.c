#include "kt_api.h" 

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

//! Note: in [below] we use the "__config_kt_api__isTo_includeSourceFilesDirectly" pre-processing-macro to 'know' if the user 'on their own' links the different code-files, ie, as we otherwise may 'as eaily' build one 'big chunk of lemenets', ie, by first 'procuding' a large file before starting the compilation-build-process (oekseth, 06. okt. 2016):

#ifndef __config_kt_api__isTo_includeSourceFilesDirectly 
#include "kt_distance.h"
#include "kt_distance_cluster.h"
#include "matrix_cmpAlg_ROC.h"
#include "kt_clusterAlg_fixed.h"
#include "kt_clusterAlg_fixed__alg__miniBatch.h"
#include "kt_clusterAlg_SVD.h"
#include "e_kt_correlationFunction.h"
#include "kt_clusterAlg_SOM.h"
#include "kt_clusterAlg_hca.h"
#include "kt_forest_findDisjoint.h"
#include "fast_log.h"
#include "kt_clusterAlg_kruskal.h"
#else //! then we 'manually' inlluce the set of soruce-files, ie, to avoid any complex linkage-processes
//! *********************************************************************************************
//! *********************************************************************************************
#include "kt_clusterAlg_hca.c" 
#include "kt_clusterAlg_SVD.c" 
#include "kt_clusterAlg_SOM.c" 
#include "kt_clusterAlg_fixed.c" 
#include "kt_centrality.c" 


//!
//! Fucntiosn for computation of distance-scores:
#include "kt_matrix_clusterDistance.c"
#include "kt_distance_cluster.c" 
#include "kt_distance.c" 
#include "kt_matrix_filter.c" 
#include "matrix_deviation.c" 
#include "matrix_cmpAlg_ROC.c"
#include "kt_distance_slow.c"
#include "distance_2rows_slow.c" 

//! Internal functiosn for comptaution fo distance-scores:
#include "correlation_api.c" 
//! Fucntions for: all-agaisnt-all correlation-comptuation:
//#include "correlation_inCategory_rank_spearman.c" 
#include "correlation_inCategory_rank_kendall.c" 
#include "correlation_inCategory_matrix_base.c"


//! Fucntions for correlation-comptuation when using a feature-pair (ie, comparison of two rows):
#include "correlation_api_rowCompare.c" 
#include "correlationType_kendall_partialPreCompute_kendall.c"
#include "correlationType_spearman.c"
#include "correlationType_kendall.c" 
#include "correlationType_delta.c" 
#include "correlationType_proximity.c" 
#include "correlationType_proximity_altAlgortihmComputation.c"

//! Auziilary funcitons for ranks, sort and distance-comptuation:
#include "correlation_inCategory_delta.c" 
#include "correlation_rank_rowPair.c"
#include "correlation_rank.c"

//!
//! Fucntiosn used itnernaly to irmpvoing the performance of our appraoch, eg, wrt. optmaized parallel scheudliong and wrt. idnetificaiton of disjoitn-sets of vertices (oekseth, 06. okt. 2016):
#include "kt_para_3dSchedule.c" 
#include "kt_forest_findDisjoint.c"
#include "kt_set_2dsparse.c"
#include "kt_set_1dsparse.c"


#include "correlation_sort.c" 
#include "correlation_base.c"
#include "correlation_s_allAgainstAll_SIMD_inlinePostProcess.c"

#include "config_nonRank_manyToMany.c"
#include "config_nonRank_oneToMany.c"
#include "config_nonRank_each.c"
#include "s_kt_computeTile_subResults.c"
#include "s_allAgainstAll_config.c"
#include "kt_math_matrix.c" //! which is perofmrance-tested in our "measure_externalCxx__nipals.c"xx" (oekseth, 06. otk. 2016).
#include "mask_api.c" 
#include "mask_base.c"

#include "math_generateDistribution.c"
#include "math_generateDistribution_incompleteBeta.c"
#include "math_baseCmp_operations_alglib.c"
#include "maskAllocate.c" 
#include "matrix_transpose.c"
#include "s_dense_closestPair.c" 
#include "rowChunk.c" 
#include  "dense_closestPair_base.c"
#include  "log_hca.c"
#include "log_clusterC.c"
#include "kt_terminal.c"
#include "kt_matrix.c"
#include "parse_main.c"
#include "kt_clusterAlg_kruskal.h"
//! *********************************************************************************************
#endif //! endif(inlcusion of the code-files 'manually');
#include "kt_clusterAlg_dbScan.h"

// FIXME: include [”elow] in our 'real- verison ... ei, when we do nto sue 'seprate linking'.
//#include "kt_centrality.c"


//! @return the mean of the list
t_float mean(const uint n, t_float *x) {
  t_float result = 0.;
  if(n > 0) {
    uint i;
    for (i = 0; i < n; i++) result += x[i];
    if(result != 0) {result /= (t_float)n;}
  }
  return result;
}
//! @return the median of the list
t_float median (const uint n, t_float *x) {
  return get_median(x, NULL, n);
}

//! @return true if 'empty valeus' are foudn in both obj_1 and obj_2 (oekseth, 06. des. 2016).
bool hasEmptyValues__auxFunction__kt_api(const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2) {
  assert(obj_1);
  bool has_emptyValues = false;
  if(obj_1) {
    for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
      for(uint col_id = 0; col_id < obj_1->ncols; col_id++) {
	if(!isOf_interest(obj_1->matrix[row_id][col_id])) { has_emptyValues = true;} } }
  }
  if(has_emptyValues == false) {
    if( (obj_2 != NULL) && (obj_1 != obj_2) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
      for(uint row_id = 0; row_id < obj_2->nrows; row_id++) {
	for(uint col_id = 0; col_id < obj_2->ncols; col_id++) {
	  if(!isOf_interest(obj_2->matrix[row_id][col_id])) { has_emptyValues = true;} } }
    }
  }
  //fprintf(stderr, "has_emptyValues='%u', at %s:%d\n", has_emptyValues, __FILE__, __LINE__);
  //!
  //! @return the status:
  return has_emptyValues;
}

//! @remarks a permtuation of the 'normal' distnace-fucntion, where we use "uint **mask" instead of "char **mask" iot. be back-compaitble wtih rpevisou/earlier "c clsuter" users of the c-clsuter software.
t_float** distancematrix(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, uint **mask_uint, t_float weights[], const uint transpose) {
  //char **mask = NULL;

  //! Compute the matrix, ie, the time-consuming operaiton:
  // t_float **matrix_result = distancematrix_char(metric_id, isTo_use_continousSTripsOf_memory, nrows, ncolumns, data, mask, weights, dist, transpose, isTo_use_continousSTripsOf_memory, isTo_use_fastFunction);
    
  
  const bool isTo_invertMatrix_transposed = true;
  t_float **matrix_result = NULL;
  const t_float default_value_float = 0;
  if(transpose == 0) {matrix_result = allocate_2d_list_float(nrows, nrows, default_value_float);}
  else {matrix_result = allocate_2d_list_float(ncolumns, ncolumns, default_value_float);}
  s_allAgainstAll_config_t config = get_init_struct_s_allAgainstAll_config();
  set_metaMatrix(&config, /*isTo_init=*/true, /*mask=*/NULL, /*mask=*/NULL, weights, transpose, /*masksAre_used=*/e_cmp_masksAre_used_undef, nrows, ncolumns);
  const bool isTo_use_fastFunction = true;
  if(isTo_use_fastFunction == true) {init_useFast__s_allAgainstAll_config(&config, /*masksAre_used=*/e_cmp_masksAre_used_undef);}
  config.isTo_invertMatrix_transposed = isTo_invertMatrix_transposed;
  
  if(mask_uint) {
    set_masks_typeOfMask_uint__s_allAgainstAll_config(&config, /*isTo_init=*/false, mask_uint, mask_uint, /*isTo_translateMask_intoImplictDistanceMatrix=*/true, nrows, ncolumns, data, data, /*isTo_use_continousSTripsOf_memory=*/true);
  }

  kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, data, /*result-matrix=*/matrix_result, config, NULL);

  //! If we have allcoated a temrpaory mask-list, then de-allcoate:
  //if(mask) {free_2d_list_char(&mask);}

  //! @return the comptued sitnace-matrix.
  return matrix_result;
}

//! A wrapper to call the "distancematrix" function using "s_kt_matrix" objects (oekseth, 06. otk. 2016).
bool kt_matrix__distancematrix(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1__, const s_kt_matrix_t *obj_2__, s_kt_matrix_t *obj_result, const s_kt_api_config_t config) {
  assert(obj_1__);
  if(obj_2__ == NULL) {obj_2__ = obj_1__;}
  assert(obj_2__);
  assert(obj_result);
  
  const s_kt_matrix_t *obj_1 = obj_1__;   const s_kt_matrix_t *obj_2 = obj_2__;
  s_kt_matrix_t localCopies__1;    s_kt_matrix_t localCopies__2; bool localCopeisAreAllocated = false; 
  setTo_empty__s_kt_matrix_t(&localCopies__1);
  setTo_empty__s_kt_matrix_t(&localCopies__2);
  
  bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(isTo_transposeMatrix && (config.isTo_applyDisjointSeperation == true)) { //! then we apply a pre-step wrt. disjtoin-forest-speration:
    //! Note: the itnali transpositon is used iot. simiplify [below] "case(c)" wrt. 'disjotin-forest-identicdaiton
    init__copy_transposed__s_kt_matrix(&localCopies__1, obj_1__, /*isTo_updateNames=*/true); obj_1 = &localCopies__1;
    obj_1 = &localCopies__1; //! ie, update the reference.
    if( (obj_2__ != NULL) && (obj_1__ != obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
    //if( (obj_2__ == NULL) || (obj_1__ == obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
      init__copy_transposed__s_kt_matrix(&localCopies__2, obj_2__, /*isTo_updateNames=*/true); obj_2 = &localCopies__2;
      obj_2 = &localCopies__2; //! ie, update the reference.
    } else {
      obj_2 = obj_1;
    }
    localCopeisAreAllocated = true;
    isTo_transposeMatrix = false;
    if(mayReplace_maskWith__zero__s_kt_correlationMetric_t(obj_metric)) {
      //! Set 'empoty values' to zero:
      for(uint row_id = 0; row_id < localCopies__1.nrows; row_id++) {
	for(uint col_id = 0; col_id < localCopies__1.ncols; col_id++) {
	  if(!isOf_interest(localCopies__1.matrix[row_id][col_id])) { localCopies__1.matrix[row_id][col_id] = 0; } } }
      //!
      if( (obj_2__ != NULL) && (obj_1__ != obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
	assert(localCopies__2.nrows);
	//! Set 'empoty values' to zero:
	for(uint row_id = 0; row_id < localCopies__2.nrows; row_id++) {
	  for(uint col_id = 0; col_id < localCopies__2.ncols; col_id++) {
	    if(!isOf_interest(localCopies__2.matrix[row_id][col_id])) { localCopies__2.matrix[row_id][col_id] = 0; } } }
      }      
    }
  } else if(config.in2dCorrMatrix__for_zeroMaskMetrics__applyOptimization && mayReplace_maskWith__zero__s_kt_correlationMetric_t(obj_metric)) {
    const bool is_ok = init__copy__s_kt_matrix(&localCopies__1, obj_1, /*isTo_updateNames=*/false);
    assert(is_ok);
    obj_1 = &localCopies__1;
    //! Set 'empoty values' to zero:
    for(uint row_id = 0; row_id < localCopies__1.nrows; row_id++) {
      for(uint col_id = 0; col_id < localCopies__1.ncols; col_id++) {
	if(!isOf_interest(localCopies__1.matrix[row_id][col_id])) { localCopies__1.matrix[row_id][col_id] = 0; } } }
    //!
    //! 
    if( (obj_2__ != NULL) && (obj_1__ != obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
      const bool is_ok = init__copy__s_kt_matrix(&localCopies__2, obj_2, /*isTo_updateNames=*/false);
      assert(is_ok);
      obj_2 = &localCopies__2;
      //! Set 'empoty values' to zero:
      for(uint row_id = 0; row_id < localCopies__2.nrows; row_id++) {
	for(uint col_id = 0; col_id < localCopies__2.ncols; col_id++) {
	  if(!isOf_interest(localCopies__2.matrix[row_id][col_id])) { localCopies__2.matrix[row_id][col_id] = 0; } } }
    }
  }
  assert(obj_1);
  assert(obj_2);
  const uint nrows_1 = obj_1->nrows;
  const uint nrows_2 = obj_2->nrows;   
  if(obj_result->matrix) {free_2d_list_float(&(obj_result->matrix), obj_result->nrows);}
  //! --------------------------------------
  
  uint max_rows = obj_1->nrows;   uint max_cols = obj_1->ncols; uint iterationIndex_2 = UINT_MAX;
  t_float *local_weights = obj_1->weight;
  if(isTo_transposeMatrix == 0) {
    if(obj_1->ncols != obj_2->ncols) {
      fprintf(stderr, "!!\t An incosnstiency in your configuration: the column-sizes=(%u, %u) in your two matrices differ, ie, it might be that the features actually are different, a difference which may result in a pointless clsuter-analsysis, ie, for which we abort the current clsuter-analsysis: for quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", obj_1->ncols, obj_2->ncols, __FUNCTION__, __FILE__, __LINE__);
      return false;
    } else {
      max_cols = obj_1->ncols;       max_rows = obj_1->nrows; 
      if(obj_1->nrows != obj_2->nrows) {
	if(obj_1->nrows < obj_2->nrows) {
	  iterationIndex_2 = obj_1->nrows;
	  max_rows = obj_2->nrows;
	} else {
	  iterationIndex_2 = obj_2->nrows;
	  max_rows = obj_1->nrows;	  
	}
      }
    }
  } else {
    if(obj_1->nrows != obj_2->nrows) {
      fprintf(stderr, "!!\t An incosnstiency in your configuration: the row-sizes=(%u, %u) in your two matrices differ, ie, it might be that the features actually are different, a difference which may result in a pointless clsuter-analsysis, ie, for which we abort the current clsuter-analsysis: for quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", obj_1->nrows, obj_2->nrows, __FUNCTION__, __FILE__, __LINE__);
      return false;
    } else {
      max_cols = obj_1->ncols;       max_rows = obj_1->nrows; 
      if(obj_1->ncols != obj_2->ncols) {
	if(obj_1->ncols < obj_2->ncols) {
	  iterationIndex_2 = obj_1->ncols;
	  max_cols = obj_2->ncols;
	} else {
	  iterationIndex_2 = obj_2->ncols;
	  max_cols = obj_1->ncols;	  
	}
      }
    }
  }
  //! -------------------------------------------
  //!
  //! Configure:;
  const bool isTo_invertMatrix_transposed = true;
  t_float **matrix_result = NULL;
  const t_float default_value_float = 0;
  const uint global_max_rows = max_rows;
  const uint global_max_cols = max_cols;
  //fprintf(stderr, "global_max_rows=%u, global_max_cols=%u, isTo_transposeMatrix=%d, at %s:%d\n", global_max_rows, global_max_cols, isTo_transposeMatrix, __FILE__, __LINE__);

  if(isTo_transposeMatrix == 0) {matrix_result = allocate_2d_list_float(obj_1->nrows, obj_2->nrows, default_value_float);}
  else {matrix_result = allocate_2d_list_float(obj_1->ncols, obj_2->ncols, default_value_float);}
  /* if(isTo_transposeMatrix == 0) {matrix_result = allocate_2d_list_float(max_rows, max_rows, default_value_float);} */
  /* else {matrix_result = allocate_2d_list_float(max_cols, max_cols, default_value_float);} */
  s_allAgainstAll_config_t config_self = get_init_struct_s_allAgainstAll_config();
  
  set_metaMatrix(&config_self, /*isTo_init=*/true, /*mask=*/NULL, /*mask=*/NULL, local_weights, isTo_transposeMatrix, /*masksAre_used=*/e_cmp_masksAre_used_undef, max_rows, max_cols);
  const bool isTo_use_fastFunction = true;
  if(isTo_use_fastFunction == true) {init_useFast__s_allAgainstAll_config(&config_self, /*masksAre_used=*/e_cmp_masksAre_used_undef);}
  config_self.isTo_invertMatrix_transposed = isTo_invertMatrix_transposed;
  config_self.iterationIndex_2 = iterationIndex_2;

  //! -------------------------------------------
  //!
  //! Compute:
  if(true) { //! ie, to reduce the algortihnm compelcity (oekseth, 06. jul. 2017).
    // FIXME[logic]: add supprot for the disjtoint-forest-appraoxh .... ie, first reoslve cases wrt. ..... correcntess of mem-allcoation wr.t the "matrix_result" matirx (oekseth, 06. jul. 2017).
  //if((config.isTo_applyDisjointSeperation == false) || !hasEmptyValues__auxFunction__kt_api(obj_1, obj_2) )  { //! then we use the matrix 'as-is':
#ifndef NDEBUG
#if(configureDebug_useExntensiveTestsIn__tiling == 1) 
    { //! then we vlaidate wrt. 'specific inptu-values' (oekseth, 06. arp. 2017):
      const s_kt_matrix_t *matrix_input = obj_1;
      assert(matrix_input);
      t_float **data = matrix_input->matrix;
      const uint nrows = matrix_input->nrows;
      const uint ncols = matrix_input->ncols;
      assert(data);
      for(uint i = 0; i < nrows; i++) {
	for(uint j = 0; j < ncols; j++) {
	  const t_float score = data[i][j];
	  assert(isinf(score) == false);
	  assert(isnan(score) == false);
	}
      }
    }
#endif
#endif  
    kt_compute_allAgainstAll_distanceMetric(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, max_rows, max_cols, obj_1->matrix, obj_2->matrix, /*result-matrix=*/matrix_result, config_self, NULL);
  } else { //! then we 'apply' a disjotint-forest-seperation:
    //! Note: in our code-stub we make use of the amcros and vriarbles wrt. our "kt_disjoint__stub__applyLogics_insdieEach.c"
    const uint cnt_rows_1_xmtTransposed = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;
    const uint cnt_rows_2_xmtTransposed = (isTo_transposeMatrix == false) ? obj_2->nrows : obj_2->ncols;
    const uint nrows = obj_1->nrows; const uint ncols = obj_1->ncols;
    if(config.inputMatrix__isAnAdjecencyMatrix && (!obj_2 || (obj_1 == obj_2)) )  {//! Case(1): 
      // assert(nrows == ncols); // TODO: consider re-including this (oekseth, 06. jul. 2017).
       //assert(cnt_rows_1_xmtTransposed == cnt_rows_2_xmtTransposed); 
    //! Terheafter 'cofngure' our test-appraoch to use our [above validate'd teest-code-chunk:
      const s_kt_matrix_t *matrix_disjointInput = obj_1;
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__distancematrix__stub__case_a.c"  //! ie, the same test-code-chunk as used in ª[bove]
      // #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
      //#undef __disjointConfig__useComplexInference__rowsDiffersFrom
      //fprintf(stderr, "# at %s:%d\n", __FILE__, __LINE__);
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>      
      //if(config.inputMatrix__isAnAdjecencyMatrix )  {//! Case(1):
    } else if( (cnt_rows_1_xmtTransposed == cnt_rows_2_xmtTransposed) && (!obj_2 || (obj_1 == obj_2))  ) { //! Case(2):
      const s_kt_matrix_t *matrix_disjointInput = obj_1;
      // assert(false); // FIXME: consider to 'drop' the use of the 'xternal column-mappigns when udpatign the matrix ... ie, as we would asusme 'taht only the rows are to be sued'.
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__distancematrix__stub__case_b.c"  //! ie, the same test-code-chunk as used in ª[bove]
      // #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
#define __disjointConfig__useComplexInference__rowsDiffersFrom 1
      //fprintf(stderr, "# at %s:%d\n", __FILE__, __LINE__);
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>      
    } else { //! Case(3):
      //! -----------------------------------------------------------------
      //! Apply the disjoitn-forest-comptuatiosn:
      //! Frist set a tmp-matrix-element to 'saisfy' the sepectiosn in our "kt_disjoint__stub__applyLogics_insdieEach.c"
      const s_kt_matrix *matrix_disjointInput   = obj_1;   const uint *keyMap_localToGlobal = NULL; //! ie, as we 'process the compelte chunk of vertices in one step'.
      assert(matrix_disjointInput->matrix);
      const s_kt_matrix *matrix_disjointInput_2 = obj_2;
      assert(matrix_disjointInput_2->matrix);
      const uint nrows_1 = matrix_disjointInput->nrows;       const uint nrows_2 = matrix_disjointInput_2->nrows;
      //! Terheafter 'cofngure' our test-appraoch to use our [above validate'd teest-code-chunk:
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__distancematrix__stub__case_c.c"  //! ie, the same test-code-chunk as used in ª[bove]
      //! -----------------------------------------------------------------
    //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
#define __disjointConfig__useComplexInference__rowsDiffersFrom 2
      //fprintf(stderr, "# at %s:%d\n", __FILE__, __LINE__);
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>
    }    
  }
  //! -------------------------------------------
  //!
  //! Udpate the result and return:
  obj_result->matrix = matrix_result;
  if(isTo_transposeMatrix == false) { obj_result->ncols = obj_result->nrows = obj_1->nrows; }
  else { obj_result->ncols = obj_result->nrows = obj_1->ncols; }
  //! Copy the names, ie, if the names 'are set':
  if(isTo_transposeMatrix && obj_1->nameOf_columns)  {
    for(uint vertex_id = 0; vertex_id < obj_1->ncols; vertex_id++) {
      char *string = obj_1->nameOf_columns[vertex_id];
      if(string && strlen(string)) {
	set_string__s_kt_matrix(obj_result, vertex_id, string, /*addFor_column=*/false); //! ie, update for the rows.
      }
    }    
    if( (obj_2__ != NULL) && (obj_1__ != obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
      for(uint vertex_id = 0; vertex_id < obj_2->ncols; vertex_id++) {
	char *string = obj_2->nameOf_columns[vertex_id];
	if(string && strlen(string)) {
	  set_string__s_kt_matrix(obj_result, vertex_id, string, /*addFor_column=*/true); //! ie, update for the columns
	}
      }    
    }
  } else if(obj_1->nameOf_rows)  {
    for(uint vertex_id = 0; vertex_id < obj_1->nrows; vertex_id++) {
      char *string = obj_1->nameOf_rows[vertex_id];
      if(string && strlen(string)) {
	set_string__s_kt_matrix(obj_result, vertex_id, string, /*addFor_column=*/false); //! ie, update for the rows:
      }
    }
    if( (obj_2__ != NULL) && (obj_1__ != obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
    //if( (obj_2__ == NULL) || (obj_1__ == obj_2__) ) { //! then the 'seocnd matrix' differs from the 'first matrix':
      for(uint vertex_id = 0; vertex_id < obj_2->nrows; vertex_id++) {
	char *string = obj_2->nameOf_rows[vertex_id];
	if(string && strlen(string)) {
	  set_string__s_kt_matrix(obj_result, vertex_id, string, /*addFor_column=*/false); //! ie, update for the rows:
	}
      }
    }
  }
  //!
  //! De-allcoate:
  // if(localCopeisAreAllocated) 
  {
    free__s_kt_matrix(&localCopies__1);
    free__s_kt_matrix(&localCopies__2);
  }

    //!
    //! @return
  return true;
}


/**
   @brief copmute the correlation-score for a given matrix.
   @param <metric_id> is the type of correlation to apply
   @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
   @param <nrows> is the number of rows
   @param <ncols> is the number of columns
   @param <data> is the data-set to be evaluated
   @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
   @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
   @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
   @param <cutoff> the curt-off which is used in computation of weights, ie, in the post-processing after the correlation-metric is applied
   @param <exponent> The exponent to be used to calculate the weights. 
   @param <isTo_use_fastFunction> which if set to false imples that we use the 'legacy' slow function.
   @param <CLS> which is the block-sise of the tiles to use
   @param <iterationIndex_2> which if set 'narrows' the ros (of columns) to be indvestigated for data2.
   @return a pointer to a newly allocated array containing the calculated weights for the rows (iftranspose==0) or columns (if transpose==1). 
   @remarks This function calculates the weights using the weighting scheme proposed by Michael Eisen: 
   - w[i] = 1.0 / sum_{j where d[i][j]<cutoff} (1 - d[i][j]/cutoff)^exponent
   - where the cutoff and the exponent are specified by the user.
**/
t_float* calculate_weights(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const t_float cutoff, const t_float exponent) { 
  return calculate_weights__extensiveParams(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weights, transpose, cutoff, exponent, /*CLS=*/64,/*iterationIndex_2=*/UINT_MAX);
}

/**
   @param <obj_metric> is the matrix to apply.
   @param <obj_1> is the object to comptue weights for.
   @param <obj_result> expected to be an empty object: to access the weights (comptued for the rows) c
   @return true if the oepration awas a success.
 **/
bool kt_matrix__calculate_weights(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result, const bool isTo_transposeMatrix, const t_float cutoff, const t_float exponent) { 
  assert(obj_1); assert(obj_result); 
  t_float *weight_table =  calculate_weights__extensiveParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, obj_1->nrows, obj_1->ncols, obj_1->matrix, /*mask=*/NULL, obj_1->weight, isTo_transposeMatrix, cutoff, exponent, /*CLS=*/64,/*iterationIndex_2=*/UINT_MAX);
  assert(weight_table);
  
  const bool is_ok = (weight_table != NULL);
  if(is_ok) {
    assert(obj_result);
    assert(obj_result->weight == NULL);
    obj_result->ncols = obj_1->nrows; //! ie, as we asusme the weighttable is assicated witht he column-size.
    obj_result->weight = weight_table;
  }
  return is_ok;
}
/**
   @brief identifes the distance between two clusters.
   @param <metric_id> is the type of correlation to apply
   @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
   @param <nrows> is the number of rows
   @param <ncols> is the number of columns
   @param <data> is the data-set to be evaluated
   @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
   @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
   @param <n1> is the number of elements in the first clsuter/matrix.
   @param <n2> is the number of elements in the first clsuter/matrix.
   @param <index1> identifies the vertices/rows in the first cluster
   @param <index2> identifies the vertices/rows in the second cluster
   @param <method> idneties the method to be sued wrt. comparion between teh lucters
   @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
   @remarks for the "method" param we support the following optioins:
   - method=='a': the distance between the arithmetic means of the two clusters
   - method=='m': the distance between the medians of the two clusters
   - method=='s': the smallest pairwise distance between members of the two clusters
   - method=='x': the largest pairwise distance between members of the two clusters
   - method=='v': average of the pairwise distances between members of the clusters
 **/
t_float clusterdistance(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t method, const uint transpose) {
  return clusterdistance__extensiveParams(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, n1, n2, index1, index2, method, transpose, 
					  /*isTo_use_continousSTripsOf_memory=*/true,
					  /*isTo_invertMatrix_transposed=*/true,
					  /*CLS=*/64,
					  /*isTo_useImplictMask=*/false
					  );

}


/**
   @brief identify teh clsuter-distance between two sets of valeus (oekseth, 06. nov. 2016).
   @param <obj_metric> is the metric to be used wrt. the ddistnace-comparison
   @param <obj_1> is the data-object which hold the matrix to evlauate
   @param <obj_indexOfValues_1> is epxected to hold a 'mapping' (ie, an index-table) for the rows to be part of cluster(1)
   @param <obj_indexOfValues_2> is epxected to hold a 'mapping' (ie, an index-table) for the rows to be part of cluster(2)
   @param <method_id> descirbes the type of cluster-comparison to be sued/applied
   @param <isTo_transposeMatrix> to be set to true if the data is to be transposed 'before evlauation': if so then we expec thte inex-tables to desribe the columsn (and Not the rows).
   @param <scalar_result> is the scalar flaoting-poitn result of the comparison (bertween the two index-sets in quesiton).
   @remarks the obj_indexOfValues_1 and obj_indexOfValues_2 input-arpaemter-vairalbes may be build using the "push_row_member__s_kt_member(..)" functioni, a deufntion defined in our "kt_matrix.h" header-file.
 **/
bool kt_matrix__clusterdistance(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_indexOfValues_1, const s_kt_matrix_t *obj_indexOfValues_2, const e_kt_clusterComparison_t method_id, const bool isTo_transposeMatrix, t_float *scalar_result) {
  assert(obj_1);
  //assert(obj_indexOfValues_1); assert(obj_indexOfValues_2);
  //! 
  const uint n1 = obj_indexOfValues_1->mapOf_rowIds_biggestIndexInserted;
  const uint n2 = obj_indexOfValues_2->mapOf_rowIds_biggestIndexInserted;
  assert(n1 > 0);   assert(n2 > 0);  
  uint *index1 = obj_indexOfValues_1->mapOf_rowIds;   uint *index2 = obj_indexOfValues_2->mapOf_rowIds;
  assert(index1);   assert(index2);

  // const char method = get_char_fromEnum(method_id);
  //!
  //! Apply the comptuation
  const t_float result = clusterdistance__extensiveParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, obj_1->nrows, obj_1->ncols, obj_1->matrix, /*mask=*/NULL, obj_1->weight, n1, n2, index1, index2, method_id, isTo_transposeMatrix, 
					  /*isTo_use_continousSTripsOf_memory=*/true,
					  /*isTo_invertMatrix_transposed=*/true,
					  /*CLS=*/64,
					  /*isTo_useImplictMask=*/false
					  );
  //!
  //! Update the result and thereafter return:
  *scalar_result = result;
  return (result != T_FLOAT_MAX);
}


  /**
     @brief compute the kmeans clustering of the vertices.
     @remarks 
     # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
     # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
     # "":
  **/
int kmeans_or_kmedian(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, float weight[], const uint npass, float** cdata, char **cmask, uint clusterid[], float* error, uint tclusterid[], uint counts[], uint mapping[], const bool isTo_use_mean) {
  return kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncolumns, data, mask, weight, npass, cdata, cmask, clusterid, error, NULL, counts, mapping, isTo_use_mean, /*isTo_preComputeDistanceMatrix_ifSpeedIncreases=*/true,
						 /*matrix2d_vertexTo_clusterId=*/NULL
);
}


/**
   @brief idnetify the cenotrids (and assicated errors) of each cluster.
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_result_clusterId_columnValue> if not set to NULL then this object hold data of the cluster-distance to each of the columns
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @param <isTo_transposeMatrix> which if set to true impleis that we first trnaspose the matrix (before comptuing the clusters)
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <isTo_useMean> which if set to false impleis that we use Median (instead or mean (or: average));
   @return true upon success.
**/
bool kt_matrix__kmeans_or_kmedian(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1__, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const uint npass, const bool isTo_useMean, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init)  {
  const s_kt_matrix_t *obj_1 = obj_1__; 
  s_kt_matrix_t localCopies__1;  bool localCopeisAreAllocated = false;

  const uint default_value_uint = 0;
  const t_float default_value_float = 0;
  bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(isTo_transposeMatrix && (config.isTo_applyDisjointSeperation == true)) { //! then we apply a pre-step wrt. disjtoin-forest-speration:
    //! Note: the itnali transpositon is used iot. simiplify [below] "case(c)" wrt. 'disjotin-forest-identicdaiton
    init__copy_transposed__s_kt_matrix(&localCopies__1, obj_1__, /*isTo_updateNames=*/true); obj_1 = &localCopies__1;
    localCopeisAreAllocated = true;
    isTo_transposeMatrix = false;
  }
  assert(isTo_transposeMatrix == false);
  const uint size_inner = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;
  const uint size_cluster = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;
  //printf("# size_cluster=%u, at %s:%d\n", size_cluster, __FILE__, __LINE__);
  //uint *mapping = allocate_1d_list_uint(nclusters, default_value_uint);
  
  if(obj_rand__init && obj_rand__init->nelements) {
    assert_possibleOverhead(0 != obj_rand__init->nelements);
    assert_possibleOverhead(size_inner <= obj_rand__init->nelements);
  }
  //!
  //! Intaite the result-object:
  // FIXME: update our inptu-object wrt. [”elow]  ... and writ a new tut-example exmaplfiying the use 'of such fucntioanltiy-api' (eosketh, 06. feb. 2017)
  const e_kt_randomGenerator_type_t typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame;
  //s_kt_matrix_base_t obj_matrix_input = initAndReturn__empty__s_kt_matrix_base_t(); //initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, ncolumns, data);
  // FIXME: consider using [below] instead of [above]
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_1->nrows, obj_1->ncols, obj_1->matrix);
  //s_kt_randomGenerator_vector_t obj_rand__init = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nclusters, size_cluster, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_matrix_input); //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  const uint *mapOf_clusterVertices__toClusterIds = NULL;
  if(obj_rand__init && obj_rand__init->nelements) {obj_rand__init->matrix_input = &obj_matrix_input; mapOf_clusterVertices__toClusterIds = obj_rand__init->mapOf_columnToRandomScore; } //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  

    //uint *tclusterid = allocate_1d_list_uint(size_inner, default_value_uint);
  //  printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
#include "kt_api__kMeans__init_resultObject.c"

  
  // fprintf(stderr, "size_inner=%u, at %s:%d\n", size_inner, __FILE__, __LINE__);
  const uint size_outer_cdata = (isTo_transposeMatrix == false) ? obj_1->ncols : obj_1->nrows;
  t_float **cdata = allocate_2d_list_float(nclusters, size_outer_cdata, default_value_float);
  for(uint i = 0; i < nclusters; i++) {for(uint k = 0; k < size_outer_cdata; k++) {cdata[i][k] = T_FLOAT_MAX;}}



  char **cmask = NULL;   
  // assert(false); 
  // FIXME: is it neccesary to update/allocte the "cmask" object ... and if so how is 'it' to be used?


  
  assert(isTo_transposeMatrix == false);

  //printf("## npass=%u, nclusters=%u, at %s:%d\n", npass, nclusters, __FILE__, __LINE__);

#define __applyLogics() ({const int ret_val = kmeans_or_kmedian__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, nclusters, obj_1->nrows, obj_1->ncols, obj_1->matrix, /*mask=*/NULL, obj_1->weight, npass, cdata, cmask, obj_result->vertex_clusterId, obj_result->cluster_errors, obj_rand__init, obj_result->cluster_vertexCount, obj_result->cluster_vertexCentroid, isTo_useMean, /*isTo_preComputeDistanceMatrix_ifSpeedIncreases=*/!config.performance__testImapcactOf__slowPerformance, obj_result->matrix2d_vertexTo_clusterId); /* assert(ret_val <= 1);  */  assert(ret_val >= 0);})


  //printf("## Comptue k-avg-clsuter, at %s:%d\n", __FILE__, __LINE__);
  __applyLogics();

#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
  if(isTo_transposeMatrix == false) { //! Then we validate that all of the veritces has been 'assigned' a clsutter-id:
    assert(obj_result->vertex_clusterId);
    for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
      const uint c_id = obj_result->vertex_clusterId[row_id];
      // printf("vertex[%u] has clsuter(%u), at %s:%d\n", row_id, c_id, __FILE__, __LINE__);
      assert(c_id != UINT_MAX);
    }
  }
#endif

  //! Handle the return-values:
  
  assert(cmask == NULL); //! ie, as we expect 'this' to have been reset


#undef __applyLogics
  assert(cdata);
// printf("(completed)## Comptue k-avg-clsuter, at %s:%d\n", __FILE__, __LINE__);
  if(obj_result_clusterId_columnValue) {
    free__s_kt_matrix(obj_result_clusterId_columnValue); //! ie, 'to be on the same size'.
    assert(obj_result_clusterId_columnValue->matrix == NULL);
    obj_result_clusterId_columnValue->matrix = cdata;
    obj_result_clusterId_columnValue->nrows = nclusters;
    obj_result_clusterId_columnValue->ncols = size_outer_cdata;
  } else {free_2d_list_float(&cdata, nclusters); cdata = NULL;}
  //assert(mapping); free_1d_list_uint(&mapping); mapping = NULL;
  //free__s_kt_randomGenerator_vector_t(&obj_rand__init);
/* if(tclusterid) { */
/*   free_1d_list_uint(&tclusterid); tclusterid = NULL; */
/*  } */
  //!
  //! De-allcoate:
  if(obj_1__->matrix != obj_1->matrix) {
    free__s_kt_matrix(&localCopies__1);
  }
  //! @return the status-value wrt. the comptuation:
  return true;
  //return (bool)ret_val;
}


/**
   @brief comptue k-means clsuters using the Mini-batch k-means clsutering-algorithm
   @param <config_kMeans> is the cofnig-parameter used to configure the minibatch-aglrotihm.
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nclusters> is the number of clusters to sub-divide the data-set into.   
   @param <config> is the genrelaized configuraiton for this funcciont
   @return true upon success.
   @remarks if the default cofnigruations wr.t the mini-batch is sufficent then we susggest intiating "config_kMeans" by: s_kt_clusterAlg_fixed__alg__miniBatch_t config_kMeans = initAndReturn__s_kt_clusterAlg_fixed__alg__miniBatch_t(); //! ie, use defualt settings.
   @remarks an example-code to call this calgorithm is seen in our "tut_clust_miniBatch.c" clsuteirng-example.
**/
bool kt_matrix__kmeans_nonDefaultAlg__miniBatch(const s_kt_clusterAlg_fixed__alg__miniBatch_t config_kMeans, const s_kt_matrix_t *obj_1__, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init) {
  const s_kt_matrix_t *obj_1 = obj_1__; 
  s_kt_matrix_t localCopies__1;  bool localCopeisAreAllocated = false;

  const uint default_value_uint = 0;
  const t_float default_value_float = 0;
  bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(isTo_transposeMatrix && (config.isTo_applyDisjointSeperation == true)) { //! then we apply a pre-step wrt. disjtoin-forest-speration:
    //! Note: the itnali transpositon is used iot. simiplify [below] "case(c)" wrt. 'disjotin-forest-identicdaiton
    init__copy_transposed__s_kt_matrix(&localCopies__1, obj_1__, /*isTo_updateNames=*/true); obj_1 = &localCopies__1;
    localCopeisAreAllocated = true;
    isTo_transposeMatrix = false;
  }
  assert(isTo_transposeMatrix == false);
    const uint size_inner = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;
    const uint size_cluster = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;
    //printf("# size_cluster=%u, at %s:%d\n", size_cluster, __FILE__, __LINE__);
  //uint *mapping = allocate_1d_list_uint(nclusters, default_value_uint);
    
  

  //!
  //! Intaite the result-object:
  // FIXME: update our inptu-object wrt. [”elow]  ... and writ a new tut-example exmaplfiying the use 'of such fucntioanltiy-api' (eosketh, 06. feb. 2017)
  const e_kt_randomGenerator_type_t typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame;
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_1->nrows, obj_1->ncols, obj_1->matrix);
  //s_kt_randomGenerator_vector_t obj_rand__init = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nclusters, size_cluster, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_matrix_input); //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  const uint *mapOf_clusterVertices__toClusterIds = NULL;
  if(obj_rand__init && obj_rand__init->nelements) {obj_rand__init->matrix_input = &obj_matrix_input; mapOf_clusterVertices__toClusterIds = obj_rand__init->mapOf_columnToRandomScore;} //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  /* if(obj_rand__init) { */
  /*   obj_rand__init->matrix_input = &obj_matrix_input; //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores. */
  /* } */


    //uint *tclusterid = allocate_1d_list_uint(size_inner, default_value_uint);
  // printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
#include "kt_api__kMeans__init_resultObject.c"

  
  // fprintf(stderr, "size_inner=%u, at %s:%d\n", size_inner, __FILE__, __LINE__);
  const uint size_outer_cdata = (isTo_transposeMatrix == false) ? obj_1->ncols : obj_1->nrows;
  t_float **cdata = allocate_2d_list_float(nclusters, size_outer_cdata, default_value_float);
  for(uint i = 0; i < nclusters; i++) {for(uint k = 0; k < size_outer_cdata; k++) {cdata[i][k] = T_FLOAT_MAX;}}



  char **cmask = NULL;   
  // assert(false); 
  // FIXME: is it neccesary to update/allocte the "cmask" object ... and if so how is 'it' to be used?


  
  assert(isTo_transposeMatrix == false);

  //printf("## npass=%u, nclusters=%u, at %s:%d\n", npass, nclusters, __FILE__, __LINE__);
  //s_kt_clusterAlg_fixed__alg__miniBatch_t config_kMeans = initAndReturn__s_kt_clusterAlg_fixed__alg__miniBatch_t(); //! ie, use defualt settings.
#define __applyLogics() ({const bool ret_val = kt_clusterAlg_fixed__alg__miniBatch(&config_kMeans, obj_1, nclusters, obj_result); /* assert(ret_val <= 1);  */  assert(ret_val);})


  //printf("## Comptue k-avg-clsuter, at %s:%d\n", __FILE__, __LINE__);
  __applyLogics();

#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
  if(isTo_transposeMatrix == false) { //! Then we validate that all of the veritces has been 'assigned' a clsutter-id:
    assert(obj_result->vertex_clusterId);
    for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
      const uint c_id = obj_result->vertex_clusterId[row_id];
      // printf("vertex[%u] has clsuter(%u), at %s:%d\n", row_id, c_id, __FILE__, __LINE__);
      assert(c_id != UINT_MAX);
    }
  }
#endif

  //! Handle the return-values:
  
  assert(cmask == NULL); //! ie, as we expect 'this' to have been reset


#undef __applyLogics
  assert(cdata);
// printf("(completed)## Comptue k-avg-clsuter, at %s:%d\n", __FILE__, __LINE__);
  /* if(obj_result_clusterId_columnValue) { */
  /*   assert(obj_result_clusterId_columnValue->matrix == NULL); */
  /*   obj_result_clusterId_columnValue->matrix = cdata; */
  /*   obj_result_clusterId_columnValue->nrows = nclusters; */
  /*   obj_result_clusterId_columnValue->ncols = size_outer_cdata; */
  /* } else { */free_2d_list_float(&cdata, nclusters); cdata = NULL;
  //assert(mapping); free_1d_list_uint(&mapping); mapping = NULL;
  //free__s_kt_randomGenerator_vector_t(&obj_rand__init);
/* if(tclusterid) { */
/*   free_1d_list_uint(&tclusterid); tclusterid = NULL; */
/*  } */
  //!
  //! De-allcoate:
  if(obj_1__->matrix != obj_1->matrix) {
    free__s_kt_matrix(&localCopies__1);
  }
  //! @return the status-value wrt. the comptuation:
  return true;
}

  /**
     @brief compute the clustering of the vertices, using a a 'fixed number of clsuters'.
     @remarks The kcluster routine performs k-means or k-median clustering on a given set of elements, using the specified distance measure. The number of clusters is given by the user. Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
     @remarks
     - For other values of dist, the default (Euclidean distance) is used.
     # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
     # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
     # "iffound": The number of times the optimal clustering solution was found. The value of ifound is at least 1; its maximum value is npass. If the number of clusters is larger than the number of elements being clustered, *ifound is set to 0 as an error code. Ifa memory allocation error occurs, *ifound is set to -1.
     # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
     # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
  **/
void kcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nclusters, uint nrows, uint ncolumns, float** data, char** mask, float weight[], uint transpose, uint npass, char method, uint clusterid[], float* error, uint* ifound) {
  kcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncolumns, data, mask, weight, transpose, npass, method, clusterid, error, ifound, 
				 /*isTo_use_continousSTripsOf_memor=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/false,
				 /*matrix2d_vertexTo_clusterId=*/NULL, NULL);
}

/**
   @brief comptue k-means or k-mesian clusters (oekseth, 06. nov. 2016).
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_result_clusterId_columnValue> if not set to NULL then this object hold data of the cluster-distance to each of the columns
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <isTo_useMean> which if set to false impleis that we use Median (instead or mean (or: average));
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
**/
bool kt_matrix__kluster(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1__, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters, const uint npass, const bool isTo_useMean, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init)  {
  //const bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  const s_kt_matrix_t *obj_1 = obj_1__; 
  s_kt_matrix_t localCopies__1;  bool localCopeisAreAllocated = false;

  bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(isTo_transposeMatrix && (config.isTo_applyDisjointSeperation == true)) { //! then we apply a pre-step wrt. disjtoin-forest-speration:
    //! Note: the itnali transpositon is used iot. simiplify [below] "case(c)" wrt. 'disjotin-forest-identicdaiton
    init__copy_transposed__s_kt_matrix(&localCopies__1, obj_1__, /*isTo_updateNames=*/true); obj_1 = &localCopies__1;
    localCopeisAreAllocated = true;
    isTo_transposeMatrix = false;
  }

  const uint size_inner = (isTo_transposeMatrix) ? obj_1->nrows : obj_1->ncols;
  const uint size_cluster = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;

  //!
  //! Intaite the result-object:
  //#include "kt_api__kMeans__init_resultObject.c"
  //! Intaite the result-opbject:
  init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, nclusters, size_inner);
  const char method = (isTo_useMean) ? 'm'  : 'x';



  //! -----------------------------
  //!
  //! Apply the dsijtoint-forest-routine:
  s_kt_forest_findDisjoint_t obj_disjoint;   setTo_Empty__s_kt_forest_findDisjoint(&obj_disjoint); const t_float __localConfig__specificEmptyValue = T_FLOAT_MAX;

/*   if(config.inputMatrix__isAnAdjecencyMatrix) { */
/*     assert(matrix_input->nrows == matrix_input->ncols); */
/*     s_kt_matrix *matrix_disjointInput = matrix_input;   const uint *keyMap_localToGlobal = NULL; //! ie, as we 'process the compelte chunk of vertices in one step'. */
/*     //! Terheafter 'cofngure' our test-appraoch to use our [above validate'd teest-code-chunk: */
/* #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove] */
/*   //! ----------------------------------------------------------------- */
/*   //! Apply the disjoitn-forest-comptuatiosn: */
/* #define __localConfig__specificEmptyValue 0 //! ie, to reflect [ªbove] assumption */
/* #include "kt_disjoint__stub__applyLogics_insdieEach.c" */
/* #undef __localConfig__applyLogics_pathTo_codeChunk */
/* #undef __localConfig__specificEmptyValue */
/*   } else { */

/*     assert(false); // FIXME: compelte! */

/*   } */

  // FIXME: update our inptu-object wrt. [”elow]  ... and writ a new tut-example exmaplfiying the use 'of such fucntioanltiy-api' (eosketh, 06. feb. 2017)
  const e_kt_randomGenerator_type_t typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame;
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_1->nrows, obj_1->ncols, obj_1->matrix);
  //s_kt_randomGenerator_vector_t obj_rand__init = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nclusters, size_cluster, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_matrix_input); //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  const uint *mapOf_clusterVertices__toClusterIds = NULL;
  if(obj_rand__init && obj_rand__init->nelements) {obj_rand__init->matrix_input = &obj_matrix_input; mapOf_clusterVertices__toClusterIds = obj_rand__init->mapOf_columnToRandomScore;} //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  //if(obj_rand__init) {obj_rand__init->matrix_input = &obj_matrix_input;} //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.

  //!
  //! The Call:
  const uint nrows = obj_1->nrows;     const uint ncols = obj_1->ncols;
  if((config.isTo_applyDisjointSeperation == false) || !hasEmptyValues__auxFunction__kt_api(obj_1, /*obj_2=*/NULL) )  { //! then we use the matrix 'as-is':
    //if(config.isTo_applyDisjointSeperation == false)  { //! then we use the matrix 'as-is':
    kcluster__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, nclusters, obj_1->nrows, obj_1->ncols, obj_1->matrix, /*mask=*/NULL, obj_1->weight, isTo_transposeMatrix, npass, method, 
				   /*clusterid=*/obj_result->vertex_clusterId,
				   /*error=*/&(obj_result->sumOf_errors), 
				   /*ifound=*/&(obj_result->cntTotal_clusterAnswerFound), 
				   /*isTo_use_continousSTripsOf_memor=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/false,
				   obj_result->matrix2d_vertexTo_clusterId,
				   obj_rand__init
				   );
  } else if(config.inputMatrix__isAnAdjecencyMatrix) {
    assert(nrows == ncols);

    assert(false); // FIXME: update our [”elow] wrt. the new-added obj_rand__init

      const s_kt_matrix_t *matrix_disjointInput = obj_1;
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__cluster__stub__avgOrRank.c"  //! ie, the same test-code-chunk as used in ª[bove]
      // #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]

      uint nclusters_adjusted = nclusters;
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
      //#undef __disjointConfig__useComplexInference__rowsDiffersFrom
#define __disjointConfig__useComplexInference__rowsDiffersFrom 1
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>          
  } else {
    assert(false); // FIXME: update our [”elow] wrt. the new-added obj_rand__init
      const s_kt_matrix_t *matrix_disjointInput = obj_1;
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__cluster__stub__avgOrRank.c"  //! ie, the same test-code-chunk as used in ª[bove]
      // #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]

      uint nclusters_adjusted = nclusters;
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
      //#undef __disjointConfig__useComplexInference__rowsDiffersFrom
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>          
  }



  if(config.stringOf_exportResult__heatMapOf_clusters != NULL) {
    //assert(obj_cpy_ranksEachRow.ncols > 0);     assert(obj_cpy_ranksEachRow.nrows > 0);
    // TODO: writ eout the rsult ... a matrix filtered/maskse/unisifed wrt. the clsuter-results. <-- add anew input-apram 'for this'.
    //!
    //! First remvoe the values:
    s_kt_matrix_t obj_cpy; init__copy__s_kt_matrix(&obj_cpy, obj_1, /*isTo_updateNames=*/true);
    setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(&obj_cpy, obj_result->vertex_clusterId, obj_result->cnt_vertex, obj_result->vertex_clusterId);
    //setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(obj_1, obj_result->vertex_clusterId, obj_result->cnt_vertex, obj_result->vertex_clusterId);
    //! Tehreafter export the result:
    //! Note: Constrct a local copy of the object, ie, to 'sastify' the 'local strict criteiras wrt. data-output':
    //s_kt_matrix_t obj_cpy; init__copy__s_kt_matrix(&obj_cpy, obj_1, /*isTo_updateNames=*/true);
    export__singleCall__s_kt_matrix_t(&obj_cpy, config.stringOf_exportResult__heatMapOf_clusters, config.fileHandler);
    free__s_kt_matrix(&obj_cpy);
  }
  //!
  //! De-allcoate:
  if(obj_1__->matrix != obj_1->matrix) {
    free__s_kt_matrix(&localCopies__1);
  }
  free_s_kt_forest_findDisjoint(&obj_disjoint);
  //free__s_kt_randomGenerator_vector_t(&obj_rand__init);
  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}


//! Apply disjtoinf-reost-clustering
bool kt_matrix__disjointClusterSeperation(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, t_float threshold_min, t_float threshold_max, const bool useRelativeScore_betweenMinMax, const bool isTo_adjustToRanks_beforeTrehsholds_forRows, s_hp_distanceCluster_t *objMetric__postMerge, const s_kt_api_config_t config) {
  return compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(obj_1, obj_result, threshold_min, threshold_max,
								   T_FLOAT_MAX, T_FLOAT_MAX,
								   /*nodeThresh__useDirectSCoreInteadOfCount=*/false,
								   useRelativeScore_betweenMinMax, isTo_adjustToRanks_beforeTrehsholds_forRows,
								   /*forRankAdjustment_adjsutTo_0_100=*/false,
								   /*isTo_insertIntoCompressedMatrix_beforeComptautiosn=*/false, //! ie, as we assuemt hat node-thresohlds are Not applied
								   objMetric__postMerge,
								   config, false);
}




/**
   @brief compute the k-medioids
   @remarks "For some data sets there may be more than one medoid, as with medians. A common application of the medoid is the k-medoids clustering algorithm, which is similar to the k-means algorithm but works when a mean or centroid is not definable. This algorithm basically works as follows. First, a set of medoids is chosen at random. Second, the distances to the other points are computed. Third, data are clustered according to the medoid they are most similar to. Fourth, the medoid set is optimized via an iterative process" ["https://en.wikipedia.org/wiki/Medoid"]. Key-poitns:
   - the algorithm "is more robust to noise and outliers as compared to k-means because it minimizes a sum of pairwise dissimilarities instead of a sum of squared Euclidean distances" ["https://en.wikipedia.org/wiki/K-medoids"]. The latter is due to the training-phase in the SOM: "because in the training phase weights of the whole neighborhood are moved in the same direction, similar items tend to excite adjacent neurons" ["https://en.wikipedia.org/wiki/Self-organizing_map"]. In breif, "SOM may be considered a nonlinear generalization of Principal components analysis (PCA)" ["https://en.wikipedia.org/wiki/Self-organizing_map"].
   - 
   @remarks from the documetnaiton:
   - The kmedoids routine performs k-medoids clustering on a given set of elements, using the distance matrix and the number of clusters passed by the user. 
   - Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
   @remarks a subset of the parameters are described below:
   # "nelements": The number of elements to be clustered.
   # "distmatrix":  The distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
   - number of rows is nelements, number of columns is equal to the row number
   # "npass": The number of times clustering is performed. 
   - Clustering is performed npass  times, each time starting from a different (random) initial assignment of genes to clusters. 
   - The clustering solution with the lowest within-cluster sum of distances is chosen.
   - Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
   # "clusterid": the cluster-identites assicated to each vertex
   - On input, ifnpass==0, then clusterid contains the initial clustering assignment from which the clustering algorithm starts; all numbers in clusterid should be between zero and nelements-1 inclusive. Ifnpass!=0, clusterid is ignored on input.
   - On output, clusterid contains the clustering solution that was found: clusterid contains the number of the cluster to which each item was assigned. On output, the number of a cluster is defined as the item number of the centroid of the cluster.
   # "error": The sum of distances to the cluster center of each item in the optimal k-medoids clustering solution that was found.
   # "iffound": If kmedoids is successful: the number of times the optimal clustering solution was found. 
   - The value of ifound is at least 1; its maximum value is npass. 
   - If the user requested more clusters than elements available, ifound is set to 0. If kmedoids fails due to a memory allocation error, ifound is set to -1.
**/
void kmedoids(uint nclusters, uint nelements, float** distmatrix, uint npass, uint clusterid[], float* error, uint* ifound) {
  kmedoids__extensiveInputParams(nclusters, nelements, distmatrix, npass, clusterid, NULL, error, ifound,
				 /*matrix2d_vertexTo_clusterId=*/NULL, NULL);


}


/**
   @brief comptue k-means or k-mesian clusters (oekseth, 06. nov. 2016).
   @param <obj_1> hold the inptu-data: input-matrix is expected to be an adjcency-matirx, ie, a matrix with the nsame number of columsn and rows.
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <obj_result_clusterId_columnValue> if not set to NULL then this object hold data of the cluster-distance to each of the columns
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @param <isTo_transposeMatrix> which if set to true impleis that we first trnaspose the matrix (before comptuing the clusters)
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <isTo_useMean> which if set to false impleis that we use Median (instead or mean (or: average));
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
**/
bool kt_matrix__kmedoids(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1__, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters, const uint npass, const s_kt_api_config_t config, s_kt_randomGenerator_vector_t *obj_rand__init) {
  //const bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  const s_kt_matrix_t *obj_1 = obj_1__; 
  s_kt_matrix_t localCopies__1;  bool localCopeisAreAllocated = false;   

  bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(isTo_transposeMatrix && (config.isTo_applyDisjointSeperation == true)) { //! then we apply a pre-step wrt. disjtoin-forest-speration:
    //! Note: the itnali transpositon is used iot. simiplify [below] "case(c)" wrt. 'disjotin-forest-identicdaiton
    init__copy_transposed__s_kt_matrix(&localCopies__1, obj_1__, /*isTo_updateNames=*/true); obj_1 = &localCopies__1;
    localCopeisAreAllocated = true;
    isTo_transposeMatrix = false;
  }
  
  assert(obj_1); assert(obj_1->nrows > 0);
  // TODO: validate the correnctess/need of [”elow] 'warning':
  if(config.inputMatrix__isAnAdjecencyMatrix == false) {
  //if(obj_1->nrows != obj_1->ncols) {
    fprintf(stderr, "!!\t(critical-error)\t The medoid-algorithm expectes an adjecency matrix as input. The latter asusmption does Not hold: We will now abort: you have provided a matrix which has different number of columns VS rows, ie, an iniput-matrix with diemsions=(%u, %u). From the latter we infer that the input-matrix is Not an adjcency-matricx, ie, in cotnrast to what we expect: pelase update your call (eg, by first calling our \"kt_matrix__distancematrix(..)\" function). For quesitons please contact the senior developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", obj_1->nrows, obj_1->ncols, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }


  const uint size_inner = (isTo_transposeMatrix) ? obj_1->nrows : obj_1->ncols;
  const uint size_cluster = (isTo_transposeMatrix == false) ? obj_1->nrows : obj_1->ncols;
  //! Intaite the result-opbject:
  // init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, nclusters, size_inner);


  //! -----------------------------
  //!
  //! Apply the dsijtoint-forest-routine:
  s_kt_forest_findDisjoint_t obj_disjoint; setTo_Empty__s_kt_forest_findDisjoint(&obj_disjoint);  const t_float __localConfig__specificEmptyValue = T_FLOAT_MAX;
  const uint nrows = obj_1->nrows; const uint ncols = obj_1->ncols;

  //uint *mapping = allocate_1d_list_uint(nclusters, default_value_uint);

  //!
  //! Intaite the result-object:
  const uint default_value_uint = 0;
  // FIXME: update our inptu-object wrt. [”elow]  ... and writ a new tut-example exmaplfiying the use 'of such fucntioanltiy-api' (eosketh, 06. feb. 2017)
  const e_kt_randomGenerator_type_t typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame;
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_1->nrows, obj_1->ncols, obj_1->matrix);
  //s_kt_randomGenerator_vector_t obj_rand__init = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nclusters, size_cluster, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_matrix_input); //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  const uint *mapOf_clusterVertices__toClusterIds = NULL;
  if(obj_rand__init && obj_rand__init->nelements) {obj_rand__init->matrix_input = &obj_matrix_input; mapOf_clusterVertices__toClusterIds = obj_rand__init->mapOf_columnToRandomScore;} //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  //if(obj_rand__init) {obj_rand__init->matrix_input = &obj_matrix_input;} //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
  // uint *tclusterid = allocate_1d_list_uint(size_inner, default_value_uint);
  //  printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
#include "kt_api__kMeans__init_resultObject.c"

  
  //! -------------------------------------------
  //!
  //! Compute:
  if((config.isTo_applyDisjointSeperation == false) || !hasEmptyValues__auxFunction__kt_api(obj_1, /*obj_2=*/NULL) )  { //! then we use the matrix 'as-is':
  //if(config.isTo_applyDisjointSeperation == false)  { //! then we use the matrix 'as-is':
    //!
    //! The Call:
    //printf("##\t at %s:%d\n", __FILE__, __LINE__); 
    kmedoids__extensiveInputParams(nclusters, obj_1->nrows, obj_1->matrix, npass, 
				   /*clusterid=*/obj_result->vertex_clusterId,
				   /*centroid-cluster-to-vertex=*/obj_result->cluster_vertexCentroid, 
				   /*error=*/&(obj_result->sumOf_errors), 
				   /*ifound=*/&(obj_result->cntTotal_clusterAnswerFound),
				   obj_result->matrix2d_vertexTo_clusterId,
				   //tclusterid
				   obj_rand__init
				   );
  } else {
      const s_kt_matrix_t *matrix_disjointInput = obj_1;
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__cluster__stub__medoid.c"  //! ie, the same test-code-chunk as used in ª[bove]
      // #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]

      uint nclusters_adjusted = nclusters;
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
      //#undef __disjointConfig__useComplexInference__rowsDiffersFrom
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>          
  }

  if(config.stringOf_exportResult__heatMapOf_clusters != NULL) {
    //assert(obj_cpy_ranksEachRow.ncols > 0);     assert(obj_cpy_ranksEachRow.nrows > 0);
    // TODO: writ eout the rsult ... a matrix filtered/maskse/unisifed wrt. the clsuter-results. <-- add anew input-apram 'for this'.
    //!
    //! First remvoe the values:
    //setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(&obj_cpy_ranksEachRow, obj_result->vertex_clusterId, obj_result->cnt_vertex);
    s_kt_matrix_t obj_cpy; init__copy__s_kt_matrix(&obj_cpy, obj_1, /*isTo_updateNames=*/true);
    setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(&obj_cpy, obj_result->vertex_clusterId, obj_result->cnt_vertex, obj_result->vertex_clusterId);
    //! Tehreafter export the result:
    //export__singleCall__s_kt_matrix_t(&obj_cpy_ranksEachRow, config.stringOf_exportResult__heatMapOf_clusters);
    //! Tehreafter export the result:
    //! Note: Constrct a local copy of the object, ie, to 'sastify' the 'local strict criteiras wrt. data-output':
    export__singleCall__s_kt_matrix_t(&obj_cpy, config.stringOf_exportResult__heatMapOf_clusters, config.fileHandler);
    free__s_kt_matrix(&obj_cpy);
  }
  //!
  //! De-allcoate:
  if(localCopeisAreAllocated) {
    free__s_kt_matrix(&localCopies__1);
  }
  //free_s_kt_forest_findDisjoint(&obj_disjoint);
  //! De-allocate
  //free__s_kt_randomGenerator_vector_t(&obj_rand__init);
  /* if(tclusterid) { */
  /*   free_1d_list_uint(&tclusterid); tclusterid = NULL; */
  /* } */
  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}


//! Algorithm-idea is to appxoximate the number of disjoitn-sets through use of dsijoitn-forest-algorithms and therafre apply a more sensitive/accurate clsuter-algorithm
bool kt_matrix__kCluster__dynamicDisjoint(const e_kt_api_dynamicKMeans_t alg_type, const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *opt_matrix_filtered, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint npass, t_float threshold_min, t_float threshold_max, const bool useRelativeScore_betweenMinMax, const uint countThreshold_min, const uint countThreshold_max, const s_kt_api_config_t config) {
  //! What we expect: 
  assert(obj_1);
  assert(obj_1->matrix);
  assert(obj_1->nrows > 0);   assert(obj_1->ncols > 0);
  //!
  //! 
  const s_kt_matrix_t *matrix_input = obj_1;
  s_kt_matrix_t obj_cpy_ranksEachRow; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);
  bool obj_cpy_ranksEachRow__isAllocated = false;
  bool hasApplied_filtierng = false;
  if( (opt_matrix_filtered != NULL) && opt_matrix_filtered->nrows) {
    if( (threshold_min != T_FLOAT_MIN_ABS) || (threshold_max != T_FLOAT_MAX) ) {
      //!
      //! Then we apply a pre-filtering-step before the find-cnt-disjoint-procedure:
      const bool is_ok = init__copy__s_kt_matrix(&obj_cpy_ranksEachRow, obj_1, /*isTo_updateNames=*/false);
      assert(is_ok);
      //!
      //! Apply the ranksthresholds:
      kt_matrix__filter__simpleMask__kt_api(&obj_cpy_ranksEachRow, threshold_min, threshold_max, useRelativeScore_betweenMinMax);
      //!
      //! Udpate the 'matrix to use':
      matrix_input = &obj_cpy_ranksEachRow;            
      hasApplied_filtierng = true;
      obj_cpy_ranksEachRow__isAllocated = true;
      //assert(matrix_input); 
    }
  } else {
    matrix_input = opt_matrix_filtered;
    //assert(matrix_input); 
  }
  if(matrix_input == NULL) {
    if(false) {
      fprintf(stderr, "(too-strict-filters)\t Matrix-to-evlauate is empty, whcih impleis that we are unable to infer clusters. If latter is Not as expected, we susggest either udpating your inptualgorithm-cofnoniguraitons, or altneritvly suign a more gneeric algorith. If latter suggestions sounds odd, then please contact senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
    return true;} //! ie, as we then assume the input-matrix did Not contan any interesting values (oekseht, 06. arp. 2017)
  uint cnt_empty = 0;
  {  //! Inveistate if there are any non-mepty-cells:
    assert(matrix_input); 
    for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
      for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	if(!isOf_interest(matrix_input->matrix[row_id][col_id])) {cnt_empty++;}
	else {printf("%u --> %u w/score=%f, at %s:%d\n", row_id, col_id, matrix_input->matrix[row_id][col_id], __FILE__, __LINE__);}
	assert(matrix_input->matrix[row_id][col_id] != T_FLOAT_MIN_ABS); //! ie, what we expect
      }
    }
  }

  { //! Investigate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
    const s_kt_matrix_t *matrix_input_1 = matrix_input;
    assert(matrix_input_1);
    assert(matrix_input_1->nrows);
    assert(matrix_input_1->ncols);
    long long int cnt_interesting = 0;
    for(uint i = 0; i < matrix_input_1->nrows; i++) {
      for(uint j = 0; j < matrix_input_1->ncols; j++) {
	cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
    if(cnt_interesting == 0) {
      if(hasApplied_filtierng == true) {
	fprintf(stderr, "(info)\t Matrix is without any interestign cells after filtering is applied, a filter with value-range=[%f, %f]. Observation at [%s]:%s:%d\n", threshold_min, threshold_max, __FUNCTION__, __FILE__, __LINE__);
      } else {
	fprintf(stderr, "(info)\t Matrix is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      }
      return true; //! ie, as we otherwse have a poitnless input to the clsutering
    }
  }



  //!
  //! Get the number of clusters:
  uint nclusters = 1;
  if( (cnt_empty != UINT_MAX) && (cnt_empty != 0) ) { //! then there might be mroe than one 'empty' cluster
    nclusters = find_cntClusters_inMatrix__s_kt_forest_findDisjoint(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix, /*mask=*/NULL, /*isTo_useImplictMask=*/true, countThreshold_min, countThreshold_max, config.inputMatrix__isAnAdjecencyMatrix);
    assert(nclusters != 0);
  } else {
       fprintf(stderr, "!!\t We observe that your input-matirx does not hold any 'empty cells'. The latter implies that your data-set describes one big cluster, ie, with clsuterc-ount=1. If this is Not what you expected we would reccomend updating the filter-criterias, eg, wrt. cofnigruation-parameters such as  \"isTo_applyValueThresholds\", \"isTo_adjustToRanks_beforeTrehsholds_forRows\", \"threshold_min\", and \"threshold_max\". IF the latter pramters douns unfaimliar we suggest reading the documetnation. However if your cluster-resutls 'stillø' does not reflect your prior asusmptions, please contact the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //!
  //printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
  if(nclusters != UINT_MAX) {//! Apply the logics:
    // printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
    //!
    //! Apply clustering:
    //! Note: in [”elow] we use "obj_1" and Not "matrix_input". This as we expect the "matrix_input" to be 'of lesser descriptivness' (ie, as we assume the 'filters which were applied' may result in ifnromation-loss if the filtered matrix is used in the clsuter-algorithm itself).
    if(alg_type == e_kt_api_dynamicKMeans_AVG) {
      printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
      const bool is_ok = kt_matrix__kmeans_or_kmedian(obj_metric, obj_1, obj_result, obj_result_clusterId_columnValue, nclusters, npass, /*isTo_useMean=*/true, config, NULL);
      assert(is_ok); //! ie, what we expect
    } else if(alg_type == e_kt_api_dynamicKMeans_AVG) {
      const bool is_ok = kt_matrix__kmeans_or_kmedian(obj_metric, obj_1, obj_result, obj_result_clusterId_columnValue, nclusters, npass, /*isTo_useMean=*/false, config, NULL);
      assert(is_ok); //! ie, what we expect
    } else if(alg_type == e_kt_api_dynamicKMeans_medoid) {
      const bool is_ok = kt_matrix__kmedoids(obj_metric, obj_1, obj_result, nclusters, npass, config, NULL);
      assert(is_ok); //! ie, what we expect
    } else {
      fprintf(stderr, "!!(error)\t You used an enum='%u' which is not yet supported, ie, please request support for this enum by contactign the devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", alg_type, __FUNCTION__, __FILE__, __LINE__);
      assert(false);
      return false;
    }
  } else { //! then we do Not cobnstruct an input-object, ie, as we then asusme 'all vertices are in the same set':
    if(nclusters == 1) {
#ifndef NDEBUG      
      fprintf(stderr, "(info)\t Note we do Not produce a result-set, as we have idnetifed only one clsuter: if this is Not as expected then please update your input-arguments. For questiosn please read the documetnaion, or alternativly contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif
    }
  }
  //!
  //! De-allcoate:
  //  if(obj_1->matrix != matrix_input->matrix) {
  if(obj_cpy_ranksEachRow__isAllocated == true) {
    free__s_kt_matrix(&obj_cpy_ranksEachRow);
  }

  //! ---------------------------------------------
  //! 
  return true; //! ie, as we asusme the oerpaiton was au success.
}



/**
   @brief combined a 'trainer' and a 'model-applicaiton' to orngaise/cluster a dataset.
   @remarks The somcluster routine implements a self-organizing map (Kohonen) on a rectangular grid, using a given set of vectors. The distance measure to be used to find the similarity between genes and nodes is given by dist.
   - "SOM forms a semantic map where similar samples are mapped close together and dissimilar ones apart" ["https://en.wikipedia.org/wiki/Self-organizing_map"]
   @remarks in below we describe a few number of the parameters:
   # "nxgrid": The number of grid cells horizontally in the rectangular topology of clusters.
   # "nygrid": The number of grid cells horizontally in the rectangular topology of clusters.
   # "inittau": The initial value of tau, representing the neighborhood function.
   # "niter": The number of iterations to be performed.
   # "celldata": float[nxgrid][nygrid][ncolumns] iftranspose==0; float[nxgrid][nygrid][nrows]    iftranpose==1; The gene expression data for each node (cell) in the 2D grid. This can be interpreted as the centroid for the cluster corresponding to that cell. If celldata is NULL, then the centroids are not returned. Ifcelldata is not NULL, enough space should be allocated to store the centroid data before callingsomcluster.
   # "clusterid": iftranspose==0; int[ncolumns][2] iftranspose==1; For each item (gene or microarray) that is clustered, the coordinates of the cell in the 2D grid to which the item was assigned. Ifclusterid is NULL, the cluster assignments are not returned. Ifclusterid is not NULL, enough memory should be allocated to store the clustering information before calling somcluster.
  **/
void somcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float *weight, uint transpose,  uint nxgrid, uint nygrid, t_float inittau, const uint niter, t_float*** celldata, uint **clusterid) {
  if( (nygrid == 0) || (nygrid == UINT_MAX) ) {nygrid = 1;} //! ie, to 'simluate' k-means.
  if( (nxgrid == 0) || (nxgrid == UINT_MAX) ) {nxgrid = 1;} //! ie, to 'simluate' k-means.
  if( (nxgrid * nygrid) <= 1) {nxgrid = 10;} //! ie,  widl-card-suggestion.
  //! Apply logics:
  somcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, transpose, nxgrid, nygrid, inittau, niter, celldata, clusterid, /*config=*/NULL);
}
/**
   @brief comptue a Self Orgnaising Map (SOM) (oekseth, 06. nov. 2016).
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nxgrid> is the number of cluster-'postiosn' alogn the first diemsnion
   @param <nygrid> is the number of cluster-'postiosn' alogn the second diemsnion
   @param <inittau> The initial value of tau, representing the neighborhood function.
   @param <isTo_transposeMatrix> which if set to true impleis that we first trnaspose the matrix (before comptuing the clusters)
   @param <npass> the number of 'iteraitons' in the k-means algorithm
   @param <config> is a genarlized object to configure the lgoics-applicaitons in our kt_api header-file (oekseth, 06. des. 2016).
   @return true upon success.
**/
bool kt_matrix__som(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1__, s_kt_clusterAlg_SOM_resultObject_t *obj_result, uint nxgrid, uint nygrid, const t_float inittau, const uint npass, const s_kt_api_config_t config)  {
  const uint max_absurd = 10*1000*1000; //! ie, where latter may incite an erorr
  if(nxgrid > max_absurd) {assert(false);}
  if( (nygrid == 0) || (nygrid == max_absurd) ) {nygrid = 1;} //! ie, to 'simluate' k-means.
  if( (nxgrid == 0) || (nxgrid >= max_absurd) ) {nxgrid = 1;} //! ie, to 'simluate' k-means.
  if( (nxgrid * nygrid) <= 1) {nxgrid = 10;} //! ie,  widl-card-suggestion.
  if(nygrid == UINT_MAX) {nygrid = 1;}
  assert(nxgrid != UINT_MAX);
  assert(nygrid != UINT_MAX);
  
  //  printf("allocates-SOM with SOM=[%u, %u], at %s:%d\n", nxgrid, nygrid,  __FILE__, __LINE__);
  //! Apply logics:
  const s_kt_matrix_t *obj_1 = obj_1__; 
  s_kt_matrix_t localCopies__1;  bool localCopeisAreAllocated = false;

  bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(isTo_transposeMatrix && (config.isTo_applyDisjointSeperation == true)) { //! then we apply a pre-step wrt. disjtoin-forest-speration:
    //! Note: the itnali transpositon is used iot. simiplify [below] "case(c)" wrt. 'disjotin-forest-identicdaiton
    init__copy_transposed__s_kt_matrix(&localCopies__1, obj_1__, /*isTo_updateNames=*/true); obj_1 = &localCopies__1;
    localCopeisAreAllocated = true;
    isTo_transposeMatrix = false;
  }
  
  assert(obj_1); assert(obj_1->nrows > 0);
  assert(obj_1); assert(obj_result);
  setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(obj_result, 
						  (isTo_transposeMatrix) ? obj_1->ncols : obj_1->nrows, 
						  nxgrid, nygrid);

  //! -----------------------------
  //!
  //! Apply the dsijtoint-forest-routine:
  s_kt_forest_findDisjoint_t obj_disjoint;  setTo_Empty__s_kt_forest_findDisjoint(&obj_disjoint); const t_float __localConfig__specificEmptyValue = T_FLOAT_MAX;
  const uint nrows = obj_1->nrows; const uint ncols = obj_1->ncols;

  //! -------------------------------------------
  //!
  //! Compute:
  if((config.isTo_applyDisjointSeperation == false) || !hasEmptyValues__auxFunction__kt_api(obj_1, /*obj_2=*/NULL) )  { //! then we use the matrix 'as-is':
  //if(config.isTo_applyDisjointSeperation == false)  { //! then we use the matrix 'as-is':
    
    //! The call:
    somcluster__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, obj_1->nrows, obj_1->nrows, obj_1->matrix, /*mask=*/NULL, obj_1->weight, isTo_transposeMatrix, nxgrid, nygrid, inittau, /*niter=*/npass, obj_result->celldata, obj_result->vertex_clusterid, /*config=*/NULL);
  } else if(config.inputMatrix__isAnAdjecencyMatrix) {
    assert(nrows == ncols);
      const s_kt_matrix_t *matrix_disjointInput = obj_1;
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__cluster__stub__som_case_a.c"  //! ie, the same test-code-chunk as used in ª[bove]
      // #define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]

      //uint nclusters_adjusted = nclusters;
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>          
  } else {
      const s_kt_matrix_t *matrix_disjointInput = obj_1;
#define __localConfig__applyLogics_pathTo_codeChunk "kt_matrix__cluster__stub__som_case_a.c"  //! ie, the same test-code-chunk as used in ª[bove]

      //uint nclusters_adjusted = nclusters;
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue T_FLOAT_MAX //! ie, to reflect [ªbove] assumption
#define __disjointConfig__useComplexInference__rowsDiffersFrom 1
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
  }

  //!

  //!
  //! De-allcoate:
  if(localCopeisAreAllocated) {
    free__s_kt_matrix(&localCopies__1);
  }
  free_s_kt_forest_findDisjoint(&obj_disjoint);

  //! Update the SOM-result object with the centroids (of eahc SOM-clsuter), thereby simplifying the reuslt-gernation (oekseth, 06. des. 2016)
  updateObj__afterSOM_computation__s_kt_clusterAlg_SOM_resultObject(obj_result, obj_1->matrix, isTo_transposeMatrix);
 //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}
/**
   @brief comptue teh ROC for a matrix.
   @param <arrOf_truth> is the truth-values to compare with.
   @param <nrows>  is the number of rows in the input-matrxi
   @param <arrOf_truth_size>  is the num,ber of column in arrOf_truth and data
   @param <data>  is the data we comptue for: is the data to compare the truth-values with: if arrOf_masks is not set, then we assume that T_FLOAT_MAX or T_FLOAT_MIN_ABS is used to 'mark' empty values.
   @param <mask> which if set is used to idneitfy the set of interesting masks.
   @param <isTo_useImplictMask> which if set to true impleis that we use implcit masks
   @param <arrOf_minThresholds> if set is used to infer the poitns which are to be evaluated/used.
   @param <arrOf_minThresholds_size> is the number of elments in the feature-row: expected to equal arrOf_truth_size
   @param <matrixOfResult__spec> is the idnetifed/'found' "specificity" value.
   @param <matrixOfResult__sens> is the idnetifed/'found' "sensitivity" value.
 **/
void get_matrix_ROC__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const char *mask, const bool isTo_useImplictMask, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens) {
  assert(arrOf_truth); assert(nrows); assert(data); assert(arrOf_minThresholds);
  if(!mask && !isTo_useImplictMask) {
    get_matrix_ROC_float__matrix_cmpAlg_ROC(arrOf_truth, nrows, data, arrOf_minThresholds, arrOf_truth_size, arrOf_minThresholds_size, matrixOfResult__spec, matrixOfResult__sens);
  } else if(isTo_useImplictMask && !mask) {
    get_matrix_ROC_typeOf_mask_implicit_float__matrix_cmpAlg_ROC(arrOf_truth, nrows, data, arrOf_minThresholds, arrOf_truth_size, arrOf_minThresholds_size, matrixOfResult__spec, matrixOfResult__sens);
  } else {
    get_matrix_ROC_typeOf_mask_explicit_float__matrix_cmpAlg_ROC(arrOf_truth, nrows, data, mask, arrOf_minThresholds, arrOf_truth_size, arrOf_minThresholds_size, matrixOfResult__spec, matrixOfResult__sens);
  }
}


/* /\** */
/*    @brief Determine the singular value decomposition (SVD) by a real m by n rectangular matrix. */
/*    @param <m> the number of rows of A (and u); */
/*    @param <n> is the number of columns of A (and u) and the order of v; */
/*    @param <u> contains the rectangular input matrix A to be decomposed: in the result represents/holds the orthogonal column vectors of the decomposition; */
/*    @param <w> result: the non-negative diagonal singular values; */
/*    @param <vt> result: the orthogonal column-matrix of the decomposition; */
/*    @return true upon succsess. */
/* **\/ */
/* uint svd(int m, uint n, float** u, float w[], float** vt) { */

/*   assert(false); // FIXME: udpate [”elow] ... call the 'fast-impelmtatnion' fot he SVD */
/*   return svd_slow(m, n, u, w, vt); */
/* } */
/**
   @brief use SVD to perform principal components analysis of a real nrows by ncolumns rectangular matrix.
   @param <nrows> The number of rows in the matrix u.
   @param <ncolumns> The number of columns in the matrix v.
   @param <u> is the distance-matricx: on input, the array containing the data to which the principal component analysis should be applied. The function assumes that the mean has already been subtracted of each column, and hence that the mean of each column is zero. On output, see below.
   @param <v> (not used in input)
   @param <mapOf_w> (not used in input)
   @return true upon success
   @remarks 
   On output:   
   # The eigenvalues of the covariance matrix are returned in w.
   # Ifnrows >= ncolumns, then   
   - u contains the coordinates with respect to the principal components;
   - v contains the principal component vectors.         
   # Ifnrows < ncolumns, then   
   - u contains the principal component vectors;
   - v contains the coordinates with respect to the principal components.   
   # For the above sets, the dot product v . u reproduces the data that were passed in u.
   # The arrays u, v, and w are sorted according to eigenvalue, with the largest eigenvalues appearing first.   
 **/
const bool pca(int nrows, int ncolumns, t_float** u, t_float** v, t_float* w) {
  assert(false); // FIXME: udpate [”elow] ... call the 'fast-impelmtatnion' fot he PCA

  return pca_slow(nrows, ncolumns, u, v, w);
}


/**
   @brief copmtue centrality, centrlaity-measures speifeid in the self object.
   @param <matrix> is the input-data to compute for: an adjencey matrix hwere weights may be used to 'describe' distance: if a relationship is Not to be inlcucded, the set toe 'distance' to "0";
   @param <nrows> is the number of rows in the matrix
   @param <ncols> is the number of columns in the matrix: as we expect an ajdcency-matrix ncols==nrows is what we expect
   @param <arrOf_result> hold the set of centrliaty-scores identifed in this fucntion.
   @param <self> is the configuration- and sepficiaotn-object for copmtaution of centrlaity-scores
   @return true if operation was a success, ie, if configuraiton confired to our possible range of 'value-support'.
 **/
const bool computeVector_centrality(t_float **matrix, const uint nrows, const uint ncols, t_float *arrOf_result, const s_kt_centrality_config_t self) {
  return kt_compute_eigenVector_centrality(matrix, nrows, ncols, arrOf_result, self);
}


  /**
     @brief comptues the clusters using a specified Hierarhcical Clustering Algorithm (HCA).
     @param <metric_id> is the type of correlation to apply
     @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
     @param <nrows> is the number of rows
     @param <ncols> is the number of columns
     @param <data> is the data-set to be evaluated
     @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
     @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
     @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
     @param <method> which describes the tree-clsutering procedure to use.
     @param <distmatrix> which if Not set to NULL is the distance-matrix we use for comptuations.
     @remarks The treecluster routine performs hierarchical clustering using pairwise single-, maximum-, centroid-, or average-linkage, as defined by method, on a given set of gene expression data, using the distance metric given by dist. If successful, the function returns a pointer to a newly allocated Tree struct containing the hierarchical clustering solution, and NULL ifa memory error occurs. The pointer should be freed by the calling routine to prevent memory leaks.
     @remarks in below we describe a few number of the parameters:
     # "method": Defines which hierarchical clustering method is used:
     - method=='s': pairwise single-linkage clustering
     - method=='m': pairwise maximum- (or complete-) linkage clustering
     - method=='a': pairwise average-linkage clustering
     - method=='c': pairwise centroid-linkage clustering
     - For the first three, either the distance matrix or the gene expression data is sufficient to perform the clustering algorithm. For pairwise centroid-linkage clustering, however, the gene expression data are always needed, even if the distance matrix itself is available.
     # "distmatrix": The distance matrix. Ifthe distance matrix is zero initially, the distance matrix will be allocated and calculated from the data by treecluster, and deallocated before treecluster returns. Ifthe distance matrix is passed by the calling routine, treecluster will modify the contents of the distance matrix as part of the clustering algorithm, but will not deallocate it. The calling routine should deallocate the distance matrix after the return from treecluster.
     # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
     # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
     @return A pointer to a newly allocated array of Node_t structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
  **/
Node_t* treecluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, float** data, uint** mask, float weight[], uint transpose, char method, float** distmatrix) {
  return treecluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, transpose, method, distmatrix,
				    /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_useImplictMask=*/true, /*isTo_use_fastFunction=*/true);
}

/**
   @brief comptue a hiearhcical tree (or: dendogram)
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <method_id> is the hierahricla clsuter algorithm (HCA) to apply.
   @param <isTo_transposeMatrix> which if set to true impleis that we first trnaspose the matrix (before comptuing the clusters)
   @return true upon success.
**/
bool kt_matrix__hca(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_hca_resultObject_t *obj_result, const e_kt_clusterAlg_hca_type_t method_id, const s_kt_api_config_t config)  {
  assert(obj_1); assert(obj_result);
  const bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  if(!config.inputMatrix__isAnAdjecencyMatrix || (obj_1->nrows != obj_1->ncols) ) {
    fprintf(stderr, "!!\t(criticla-error)\t Input does Not seem like an adjecency-matrix: we will now abort: you have provided a matrix which has different number of columns VS rows, ie, an iniput-matrix with diemsions=(%u, %u). From the latter we infer that the input-matrix is Not an adjcency-matricx, ie, in cotnrast to what we expect: pelase update your call (eg, by first calling our \"kt_matrix__distancematrix(..)\" function). For quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", obj_1->nrows, obj_1->ncols, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }  
  free__s_kt_clusterAlg_hca_resultObject_t(obj_result); //! ie, a 'safegaurd' wrt. [below] 'init-call' (oekseth, 06.mar. 2017)
  // setTo_empty__s_kt_clusterAlg_hca_resultObject_t(obj_result, (isTo_transposeMatrix) ? obj_1->ncols : obj_1->nrows);
  //assert(obj_result->nrows > 0);
  const char method = get_charConfigurationEnum_fromEnum__kt_clusterAlg_hca(method_id);

  bool isTo_use_fastFunction = true;
#ifndef NDEBUG
  if(false)  {
    isTo_use_fastFunction = false;
    fprintf(stdout, "(TODO)\t uses a wslow version of the HCA-algorithm: instead consider to use the fast Knitting-Tools hpLysis-HCA algorithm: if so then fix current errors int eh altter and thereafter udpate thsi code-chunk. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
#endif
  //! Note: [”elow] support for 'specifying direclty' the distanc-ematirx is omitted as Not all of the HCA-impmentaitons are designed witht he option to 'use the score directly' (oekseth, 06. jan. 2016) <-- todo: consider adding support for this, ie, find/idneityf an aritlce-tmeplate 'for scuh'. 
  /* if(obj_metric.inputMatrix__isAnAdjecencyMatrix && (obj_metric.metric_id == e_kt_correlationFunction_undef) ) { */
  /*   //! then we use the input-matrix as a distance-matrix (oekseth, 06. jan. 2017): */
  /*   printf("##\t Copmute a HCA without using a simliarty-metric, ie, use the input-matrix as-is, at %s:%d\n", __FILE__, __LINE__); */
  /*   obj_result->mapOf_nodes = treecluster__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, obj_1->nrows, obj_1->ncols, NULL, /\*mask=*\/NULL, obj_1->weight, isTo_transposeMatrix, method, /\*distMatrix=*\/obj_1->matrix, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*isTo_useImplictMask=*\/true, isTo_use_fastFunction); */
  /* } else */
  {
    //printf("at %s:%d\n", __FILE__, __LINE__);
    obj_result->mapOf_nodes = treecluster__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, obj_1->nrows, obj_1->ncols, obj_1->matrix, /*mask=*/NULL, obj_1->weight, isTo_transposeMatrix, method, /*distmatrix=*/NULL, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_useImplictMask=*/true, isTo_use_fastFunction);
    obj_result->nrows = (isTo_transposeMatrix) ? obj_1->ncols : obj_1->nrows; 
    assert(obj_result->nrows > 0);
    //fprintf(stderr, "transpose=%u, nrows=%u, at %s:%d\n", isTo_transposeMatrix, obj_result->nrows, __FILE__, __LINE__);
    //printf("at %s:%d\n", __FILE__, __LINE__);
  }

  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}

/**
   @brief compute Kruskal MST using a matrix obj_1 as input (oekseth, 06. feb. 2017).
   @param <obj_1> is the input-matrix: expected to be an adjcency-matrix, ie, where matrix[i][k] describes the dsitance between vertex(i)--vertex(k) (ie, in cotrnast to ffeature-matrix where the namespace of the rows and columns differs)
   @param <obj_result_hca> if set is updated with the consturcted/inferred tree of relationiships
   @param <obj_result_kMeans> if set is updated witht the distjoitn set of vertices.
   @param <config> if used then we use a mofidefed Kruskal-impelmetnation iot. try optmising the inferred set of clsuters: to split the MST into descrptive sub-units cahracterisisgn core-traits of the entwork
   @param <isAn__adjcencyMatrix> a contral-parameter: used to make the caller explcitly aware of oru expectiaont that the matrix is an ajdency-matrix: if set to false an error is returend.
   @return true if the cofngiruaiton went smooth.
   @remarks in thsi implementaiotn we asusme taht "obj_1" is a matrix with a large number of 'holes' (expected to be described using the "T_FLOAT_MAX" value). If the latter asusmption does Not hold then we expect a sgiciant slwo-down wrt. the exeuction. An example-case wrt. the latter 'maksed proeprty' is to: (a) comptue an all-agaisnt-all-amtrix using MINE; (b) comptue the ranks (for each vertex-row); (c) apply a max-rank-filter; (d) call this fucntion.
 **/
bool kt_matrix__cluster_inputMatrix__kt_clusterAlg_kruskal(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_hca_resultObject_t *obj_result_hca, s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMeans, const s_kt_clusterAlg_config__ccm_t *config, const bool isAn__adjcencyMatrix) {
  return cluster_inputMatrix__kt_clusterAlg_kruskal(obj_1, obj_result_hca, obj_result_kMeans, config, isAn__adjcencyMatrix);
}

/**
   @brief identify the number of disjoitn clusters in a matrix (oekseth,. 06. okt. 2016)
   @param <nrows> is the number of row in the amtrix
   @param <ncols> is the number of columns in the matrix
   @param <matrix> is score-matrix of vertices: note that oru assumption of 'score matrix' is a generalizaiton of an adjcency-matrix, ie, where '0' is considered to reflect a relationship: in order to validate that thios interpretation is proerply understood we inlcud ehte "isTo_useImplictMask", where this proerpty si to be set to true if "mask == NULL", ie, to increase thte change that the funciton is properly used.
   @param <mask> if set then we combien the '0' filter with the "mask[i][j] == 1" test
   @param <isTo_useImplictMask> which if set to true impleis that we investigate for valeus which have "T_FLOAT_MAX" (and if a cell is set to the altter valeu then we asusme the lvaue is Not of interest);
   @param <countThreshold_min> which is the numumum number of vertices which are to be in a given "idsjoitn forest" (ie, for the forest to be 'counted'): ignroed if countThreshold_min == countThreshold_max
   @param <countThreshold_max> similar to "countThreshold_min" with difference that we evlauate for the max-count:  ignroed if countThreshold_min == countThreshold_max
   @param <inputMatrix__isAnAdjecencyMatrix> which is used to investgate/'find' the 'interpretiaotn-case' wrt. the matrix-names-space to use
   @return the number of interesting disjoitn clsuters: if a critial erorr we return UINT_MAX in NDEBUG while call an "assert(false);" in #ifndef NDEBUG
   @remarks 
   - if valeu-fitlering is of itnerest then we suggest using oru rich library of mask-filters, eg, wrt. functions found in our "mask_api.h";
   - no-match: use T_FLOAT_MAX if "mask == NULL", ie, a '0' is interpred as a relationship (ie, as stated in above paramter-description);
 **/
uint find_cntClusters_inMatrix__kt_api(const uint nrows, const uint ncols, t_float **matrix, char **mask, const bool isTo_useImplictMask, const uint countThreshold_min, const uint countThreshold_max, const bool inputMatrix__isAnAdjecencyMatrix) {
  //! Then call our "kt_forest_findDisjoint.h" iot. copmute teh numer of disjoint entites:
  return find_cntClusters_inMatrix__s_kt_forest_findDisjoint(nrows, ncols, matrix, mask, isTo_useImplictMask, countThreshold_min, countThreshold_max, inputMatrix__isAnAdjecencyMatrix);
}

// FIXME: pre-compute tables for "student-t-dsitrubitons" ... and then write a wrapper-function to be used wrt. the lgocsi in our "matrix_deviation.h" ... <-- first update our correlation-metrics wrt. 'such a support' <-- need to idneityf use-acase-applicatiosn 'for this' (oekseth, 06. sept. 2016).



//! Apply an iterative algorithm to dientify lcusters with the best clsuter-seperation (oekseth, 06. feb. 2017)
bool kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm(s_kt_api_config_clustAlg__kt_matrix__oeksethAlg__iterativeDisjointSeperation__convergenceBy__ccm_t *self, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const s_kt_api_config_t config) {
  if(!self || !obj_1 || !obj_1->nrows || !obj_1->ncols) {
    fprintf(stderr, "!!\t Your configuraiton does not follow the minimum-requirements: we expect botht eh input-object and input-matrix to contain data; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
    return false;
  }

  //!
  //! Apply the simlairty-matrix and if neccesary transpose the result:
  assert(false); // FIXME: modify [”elow] to suppoort [ªbove]
  /* //! */
  /* kt_matrix__distancematrix */
  //! 
  //! Buidl a ranked version for the input:
  s_kt_matrix_t __localCopy = *obj_1;
  if(config.isTo_transposeMatrix == true) { //! then transpseo the matrix:    
    const bool is_ok = init__copy_transposed__thereafterRank__s_kt_matrix(&__localCopy, obj_1, /*isTo_updateNames=*/false);
    assert(is_ok);
  } else {
    const bool is_ok = init__copy__useRanksEachRow__s_kt_matrix(&__localCopy, obj_1, /*isTo_updateNames=*/false);
    assert(is_ok);
  }
  //! 
  //! Note(heuristic): we assume that it is correct to always select vertex-pairs with a rank-score [0, current_rank_max]
  assert(false); // FIXME: validte correctness of [ªbove] heuristic.
  //! Note(heuristic): to reduce the searhc-time for the optimal clusters we use 'back-and-forth' search: as the number of clusters is direclty related to the min--max-ranks, the latter provides/faiclates the 'back-and-forth' searhc-strategy' (ie, to find the cluster-sets which is most accruately/closely captured by a rank-trehshold).  
  t_float current_rank_min = 0; t_float current_rank_prev = 0;
  assert(self->mapOf_scoreAtMaxRank == NULL);
  const t_float empty_0 = 0; self->mapOf_scoreAtMaxRank = allocate_1d_list_float(__localCopy.nrows, empty_0); for(uint i = 0; i < __localCopy.nrows; i++) {self->mapOf_scoreAtMaxRank[i] = T_FLOAT_MAX;}
  self->mapOf_scoreAtMaxRank_size = __localCopy.nrows;
  assert(false); // FIXME: describe a trafversal--iteration-strategy wrt. 'how to sub-divide the matrix' .... eg, by starting at (cnt_dsijoint(graph, n/2) > thresh_minRank) ?  cnt_dsijoint(graph, (n/2)*0.5) :   cnt_dsijoint(graph, (n/2)*1.5) .... and when "(abs(current_rank_prev - current_rank_min) < thresh__iteraitoni)" then we use an iterative iteration  ....  When the "min-rank-count" is idnetifed, then we start idnetifying max-rank-count: in the 'max-identifiecation' we use the 'arleady dientifed' lower rank-threshold as a stopping-criteria, terhby reucing the worst-case search-complexity. ..... In the iteraiton (to dientify min-rank and max-rank wrt. the 'compelte set of cases ot co,mptue ccm-scores for) we 'for each comptued sijoint forest' comptue the assicated ccm-score, updaitng our local "mapOf_scoreAtMaxRank" ccm-score-table.
  t_float current_rank_max = T_FLOAT_MAX;
  assert(false); // FIXME: describe an 'tieratve appraoch' where we iterate 'inveestigating each rank-case' 
  assert(false); // FIXME: to resovle [below…] issue ... consider to 'genrate a summary' of [ªbove] identifed results ... splitting the 'evlauated sub-case' into "avg[0 .... bucket_size] .... avg[bucket_Size-1]"
  assert(false); // FIXME: may we assuem taht the Silhoutte-index will change iaw. a change in the rank-trehsholds <--- cosndier writeing a new tut-example inveestigating this case/assumption ... or altenraitvly update our "hp_clusterFileCollection.c" with a 'data-assicated matrix' .... where we for each data-set compute 'before starting at the sim-and-clust-for-loops' call our object ... computing for "self.mapOf_scoreAtMaxRank" ... ...??...
  assert(false); // FIXME[correctness-evlauation]: ... nd then 'incldue this' into a 'glboal result-matrix' <-- iot. 'emrge the data-sets' how may we 'handle the case where different data-asets have different "mapOf_scoreAtMaxRank_size"? <--- simlarty try writing a use-case wrt. 'this'.
  assert(false); // FIXME: add something



  //!
  //! De-allocate: 
  //free_1d_list_float(&mapOf_scoreAtMaxRank);
  free__s_kt_matrix(&__localCopy);
  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}


