#include "kt_sim_string.h" //! ie, the simplifed API for hpLysis
#include "kt_sparse_sim.h" //! where the latter is used as an altneritve to the "kt_sim_string", ie, to afcilaitet/supprot comptaution of "Pairwise simalrity-meitrcs (PSMs)"
#include <time.h>    // time()
#include "kt_hash_string.h"
#include "kd_tree.h" //! which provide an itnernace to an impemtantion-strategy for disjoitn-copmtautiosn from a saprse data-strucutre.
#include "hp_exportMatrix.h"


static void __tut_string_sim_2_stringArray__compute(s_kt_list_1d_string_t *strObj_1, const char *str_resultPrefix, const bool isTo_storeInMatrix) {
  //  for(uint metric_id = 0; metric_id < e_kt_sim_string_type_undef; metric_id++) {
  { uint metric_id = e_kt_sim_string_type_LevenshteinDistance;
    const e_kt_sim_string_type_t enum_id = (e_kt_sim_string_type_t)metric_id;
    //!
    //! The call:
    for(uint isTo_normalize_result = 0; isTo_normalize_result < 2; isTo_normalize_result++) {
    //	s_kt_sim_string_t obj_sim = init__s_kt_sim_string_t();
      s_kt_sim_string_t obj_sim = init__s_kt_sim_string_t();
      obj_sim.isTo_normalize_result = (bool)isTo_normalize_result;
      //! 
      //! Initate the post-simarlity-seleciton-fitler: 
      s_kt_list_2d_kvPair_filterConfig_t config_filter = init__s_kt_list_2d_kvPair_filterConfig_t(
												  /*config_cntMin_afterEach=*/2, 
												  /*config_cntMax_afterEach=*/5,
												  /*config_ballSize_max=*/T_FLOAT_MAX, 
												  /*isTo_ingore_zeroScores=*/false);
      if(isTo_storeInMatrix == true) { //! then we comptue for a 'full' matrix:
	config_filter = init__s_kt_list_2d_kvPair_filterConfig_t(
								 /*config_cntMin_afterEach=*/0, 
								 /*config_cntMax_afterEach=*/UINT_MAX,
								 /*config_ballSize_max=*/T_FLOAT_MAX, 
								 /*isTo_ingore_zeroScores=*/false);
      }
      //! 
      //!
      //! Compute: 
      s_kt_list_2d_kvPair_t obj_sparse  = setOf__compute__kt_sim_string(&obj_sim, 
									enum_id,
									///*isTo_sortBy_char=*/false,
									&config_filter, strObj_1, strObj_1);      

      //!
      //! Write out: 
      {
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s_%s_norm_%u", str_resultPrefix, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	//! The call:
	export__s_kt_list_2d_kvPair__hp_exportMatrix(stringOf_resultFile, obj_sparse, strObj_1, isTo_storeInMatrix);
      }
      //if(false) 
      { //! Generate a symmetrci-DB-SCAN-data-set-appraoch: we 'require': ([key1][key2] != null)--implies--([key2][key1] != null), ie, for the rank-filtered subset to be symemtrically cosnistent/agreeable.
	s_kt_list_2d_kvPair_t obj_sparse_subsetSymmetric = get_symmetricSubset__onKeys__s_kt_list_2d_kvPair_filterConfig_t(&obj_sparse);
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s_%s_norm_%u_subsetSymmetric", str_resultPrefix, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	//! The call:
	export__s_kt_list_2d_kvPair__hp_exportMatrix(stringOf_resultFile, obj_sparse_subsetSymmetric, strObj_1, isTo_storeInMatrix);
	//!
	//! De-allcoate: 
	//free__s_kt_sparse_sim_t(&obj_sim);
	free__s_kt_list_2d_kvPair_t(&obj_sparse_subsetSymmetric);
      }
      //!
      //! De-allcoate: 
      //free__s_kt_sparse_sim_t(&obj_sim);
      free__s_kt_list_2d_kvPair_t(&obj_sparse);
      //assert(false); // FIXME: remove
    }
  }
}


 #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief a simple use-case-example where we dmeosntrate the use/applicaiton of "string-simliarty-metrics (StSM)" and validate their correctness
   @author Ole Kristian Ekseth (oekseth, 06. aug. 2017).
   @remarks
   --- we extend the default set of StSMs with "Pairwise simalrity-meitrcs (PSMs)", ie, as the latter is (for many cotnenxts) sued as the deuflat reference-frame wrt. simalrity-emtric-comtpatuions. 
**/
int main() 
#else
  void tut_string_sim_2_stringArray()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  if(true) {
    const uint list_str_size = 3+3;
    char *list_str[list_str_size] = {
      "hamsun", "hamsum", "gamsun",
      //! Note[below]: examplify for use-cases where the strings are 'xtremely dis-simliar .... to goal/Motivaiotn is to vlaidte and exmply cases where score-differences is profunc/makred .... from the [”elow] results we observe how (among others) the "levenshtein-distance" manages to capture our itnal/prioer expeciotan/assumptiosnw rt. silairty.
      "lavransdatter", "lavran", "la",
    };
    //!
    //!
    s_kt_list_1d_string_t strMap_1 = init_andReturn__s_kt_list_1d_string_t(/*nrows=*/list_str_size);
    for(uint i = 0; i < list_str_size; i++) {
      //! 
      //! Simply access to the strings:
      const char *str1 = list_str[i];   
      //! Validate that the strings are set:
      assert(str1); assert(strlen(str1));
      //! Insert:
      set_stringConst__s_kt_list_1d_string(&strMap_1, /*index=*/i, str1);
    }
    //!
    {//! Compute: 
      __tut_string_sim_2_stringArray__compute(&strMap_1, "result_tut_string_2_test1_mat", /*isTo_storeInMatrix=*/true);
      __tut_string_sim_2_stringArray__compute(&strMap_1, "result_tut_string_2_test1_sparse", /*isTo_storeInMatrix=*/false);
    }
    {//! Compute: 
    
    }
    //!
    //! De-allcoate: 
    free__s_kt_list_1d_string(&strMap_1);
  }
  // ********************************************************************
  // ********************************************************************
  {
    const char *file_inp = "data/aalberg_bib1_aug_start__authors.tsv"; 
    //    const char *file_inp = "data/aalberg_bib1_aug_start.tsv"; 
    s_kt_list_1d_string_t strMap_1 = initFromFile__s_kt_list_1d_string_t(file_inp);
    {//! Compute: 
      printf("#\t Start-computing, at %s:%d\n", __FILE__, __LINE__);
      __tut_string_sim_2_stringArray__compute(&strMap_1, "result_tut_string_2_inpFile_aut_sparse", /*isTo_storeInMatrix=*/false);    
      s_kt_hash_string_t obj_hash = init__fromHash__s_kt_hash_string_t(&strMap_1, ' ');
      //! Call:
      __tut_string_sim_2_stringArray__compute(&(obj_hash.obj_strings), "result_tut_string_2_inpFile_aut_sparse_words", /*isTo_storeInMatrix=*/false);    
      //!
      //! De-allocate: 
      free__s_kt_hash_string_t(&obj_hash);
    }
    //!
    //! De-allcoate: 
    free__s_kt_list_1d_string(&strMap_1);
  }
  // ********************************************************************
  // ********************************************************************
  {
    const char *file_inp = "data/aalberg_bib1_aug_start.subset.tsv"; 
    //    const char *file_inp = "data/aalberg_bib1_aug_start.tsv"; 
    s_kt_list_1d_string_t strMap_1 = initFromFile__s_kt_list_1d_string_t(file_inp);
    {//! Compute: 
      printf("#\t Start-computing, at %s:%d\n", __FILE__, __LINE__);
      __tut_string_sim_2_stringArray__compute(&strMap_1, "result_tut_string_2_inpFile_sparse", /*isTo_storeInMatrix=*/false);    
      s_kt_hash_string_t obj_hash = init__fromHash__s_kt_hash_string_t(&strMap_1, ' ');
      //! Call:
      __tut_string_sim_2_stringArray__compute(&(obj_hash.obj_strings), "result_tut_string_2_inpFile_sparse_words", /*isTo_storeInMatrix=*/false);    
      //!
      //! De-allocate: 
      free__s_kt_hash_string_t(&obj_hash);
    }
    //!
    //! De-allcoate: 
    free__s_kt_list_1d_string(&strMap_1);
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

