#ifndef ex_intrisintics_h
#define ex_intrisintics_h

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <math.h>
#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"
#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"
#include "immintrin.h"

#ifdef __SSE4_1__
#include <smmintrin.h>
#endif


typedef unsigned int uint;


// FIXME[ok]: include the 'sample-aritmetic-question-cases' from our "graphAlgorithms_timeMeasurements_syntetic.cxx"


// FIXME[jc]: ... as a generic throguth ... how may we theretoically suggestion/match/compare/infer the performance/time cost-overehad wrt. usign a number of itnrisitnicts lgocicall calls 'when compared to simpler atihmetic operations' ... ie, for a cetian type of logical operations ... is there a max-limit 'before the standard C-style-prograimming-style' becomes/is faster?
// FIXME[jc]: ... as a generic throguth ... are tryign/workign on identifying 'classes' of operations (ie, a taxonomoy/ontology of 'logical chunks' ... if you ahve the time, may you try to use [”elow] example-codes (and your own code) to suggest different 'classifcioatns' to be sued (eg, as 'compoents' to be used in/whenw riting a enw query-lanague?)?
// FIXME[jc]: ... as a generic throguth ... may you try to itnrospect uppon how we may use ”[elow] exampel-cases 'as the base for an artilce directed towardsa a tutorial bio-informaticans wrt. vector-programming'?
// FIXME[jc]: ... as a generic throguth ... consider to describe how itnrisitnictis 'foce3s' a develoepr to write better code ... ie, as the vectorizations gives a 'driect'/'rractical' heads-up wrt. memory-access-order.
// FIXME[jc]: ... as a generic throguth ... 



//! -------------------------------------------------------------------------------------
//! Part(1): concrete examples 'narrowd-down' logics of intrinsics.
//! -------------------------------------------------------------------------------------

void syntatic_wrt_intrisintistics() {
  { //! Comapre a few set of logical cases wrt. correctness.
    const uint list_size = 5; const float list[list_size] = {2 ,7, 3, 3, 12};       float result[list_size] = {0, 0, 0, 0, 0};
    //if(false) 
    
    { //! Write a new meset(..) for a 'float(..)' operation-call. <-- thereafter update the "getclustermedoids(..)" in 'this file'.
      const float default_value = FLT_MAX;
      { //! Naive:
	for(uint i = 0; i < list_size; i++) {result[i] = default_value;}
      }
      { //! Hopefully optimized.
	// FIXME[jc]: are there any meset(..) fucntiosn which are otpimized and 'acaptable' of using floats 'in their memset input-args'?
	const uint nclustersOptimized_size = (list_size > 4) ? list_size - 4 : 0;
	uint j = 0;
	if(nclustersOptimized_size > 0) {
	  for(; j < nclustersOptimized_size; j += 4) {
	    _mm_store_ps(&result[j], _mm_set1_ps(default_value));
	  }
	}
	//! Then update the 'remaining tail':
	for(; j < list_size; j++) {result[j] = default_value;}
      }
    }
    //if(false)
    { //! Comptue the sum of arrays: "a[i+1] += a[i]" and "cumhist[j][i] += cumhist[j-1][i]", where the latter is exmpalfieidn in "compute_cumhist(..)" in "mine.c"
      // FIXME[jc]: would you say that [above] 'alternative interpretaitons' results in the same itnrisitnstcs?
      //! Intaiteion:
      for(uint i = 0; i < list_size; i++) {result[i] = list[i];}
      
      { //! Naive:
	for(uint i = 1; i < list_size; i++) {result[i] += result[i-1];}
      }
      { //! Hopefully optimized.
	assert(false); // FIXME[jc]: any suggestiosn?
	assert(false); // FIXME: when this si completed, then write a performance-test ... and then update mine.c
      }
    }

    { //! Extrtract the max-value:
      // FIXME[jc]: may you describe how a max-value may be 'extracted'?
      float max_value = 0;
      { //! Naive:	  
	for(uint i = 0; i < list_size; i++) { 
	  if(list[i] > max_value) {max_value = list[i];}
	}
      }	
      { //! Hopefully optimized.
	const uint nclustersOptimized_size = (list_size > 4) ? list_size - 4 : 0;
	uint i = 0;
	if(nclustersOptimized_size > 0) {
	  float result_tmp[4] = {0, 0, 0, 0};
	  for(; i < nclustersOptimized_size; i += 4) {	      
	    //! Load the data:
	    _mm_store_ps(&result_tmp[0], 
			 _mm_max_ps(_mm_load_ps(&result[0]), _mm_load_ps(&list[i])));
	  }
	  //! Update the result:
	  // FIXME[jc]: do you know how to otpmize [”eloł]?
	  for(uint k = 0; k < 4; k++) { if(result_tmp[k] > max_value) {max_value = result_tmp[k];}}
	}
	for(; i < list_size; i++) { 	    
	  if(list[i] > max_value) {max_value = list[i];}
	}
      }
    }
    
    if(false)
      { //! Comptue the sum of arrays: "if(a[i] == b[i]) {s++;}" and "if(a[i] == a[i-1]) {s++;}"
	uint s = 0;
	{ //! Naive:	  
	  for(uint i = 0; i < list_size; i++) { 
	    if(result[i] == list[i]) {s++;} 
	  }
	}	
	{ //! Hopefully optimized.
	  const uint nclustersOptimized_size = (list_size > 4) ? list_size - 4 : 0;
	  uint i = 0;
	  if(nclustersOptimized_size > 0) {
	    int result_tmp[4] = {0, 0, 0, 0};
	    for(; i < nclustersOptimized_size; i += 4) {	      
	      //! Load the data:
	      const __m128 vec_data1 = _mm_load_ps(&list[i]); 
	      const __m128 vec_data2 = _mm_load_ps(&result[i]); 
	      // FIXME[jc]: may you validate [”elow] ... ie, that the result always proudde a string of "1" and "0" .. and how deo we 'ahdnle' the "nan" resutls ... ie, any 'optmzied stratgeis wrt. such'?
	      __m128 vec_cmp = _mm_cmpeq_ps(vec_data1, vec_data2);
	      //! Convert the float to int
	      __m128i vec_cmp_int = _mm_cvtps_epi32(vec_cmp);
	      assert(false); // FIXME[jc]: is [”elow] correct? <-- [”elow] does not compile, ie, 'how do we work with ints'?
	      // FIXME: include [below] when we have figure out why [”elow] does not work.
	      // _mm_add_epi32((__m128i*)result_tmp, vec_cmp_int);
	    }
	    //! Update the result:
	    for(uint k = 0; k < 4; k++) { s += result_tmp[k];}
	  }
	  for(; i < list_size; i++) { 	    
	    if(result[i] == list[i]) {s++;} 
	  }
	}
	assert(false); // FIXME: when this si completed, then write a performance-test ... and then update mine.c ... eg, wrt. the "GetClumpsPartition(..)" function.
      }
    if(false)
      { //! Comptue abs
	// Note: [”elow] is only an extract ... ie, weill need to work more on this ... 
	// __m128i sign  = _mm_cmpgt_epi64(_mm_setzero_si128(),a);// 0 > a
	// __m128i inv   = _mm_xor_si128(a,sign);                 // invert bits if negative
	// __m128i result =          _mm_sub_epi64(inv,sign);               // add 1
	assert(false); // FIXME: when this si completed, then write a performance-test.
      }


    if(false)
      { //! Test the AND operator:
	//float data1[4] = {1, 1, 2, 5};      float data2[4] = {1, 0, 2, 5}; //! ie, alt(1)
	float data1[4] = {3, 1, 2, 15};      float data2[4] = {1, 0, 2, 1}; //! ie, alt(2)
	const __m128 vec_data1 = _mm_load_ps(&data1[0]);
	const __m128 vec_data2 = _mm_load_ps(&data2[0]);
	//! Test the and:
	__m128 vec_cmp = _mm_and_ps(vec_data1, vec_data2);
	{
	  float result[4];  _mm_store_ps(result, vec_cmp); for(uint i = 0; i < 4; i++) {printf("[%u] = %f, ", i, result[i]);}
	  printf("-----------------***\n\n");
	}
	assert(false); // FIXME: why does [above] alt(1) result in result[3]="2" alt(2) in "0" ?
	assert(false); // FIXME: complete.
      }
    if(false)
      { //! Test the CMP operator:
	float data1[4] = {3, 0, 2, 1};      float data2[4] = {3, 0, 2, 15}; //! ie, alt(1)
	const __m128 vec_data1 = _mm_load_ps(&data1[0]);
	const __m128 vec_data2 = _mm_load_ps(&data2[0]);
	//! Test the and:
	__m128 vec_cmp = _mm_cmpeq_ps(vec_data1, vec_data2);
	{
	  float result[4];  _mm_store_ps(result, vec_cmp); for(uint i = 0; i < 4; i++) {printf("[%u] = %f, ", i, result[i]);}
	  printf("-----------------***\n\n");
	}
	assert(false); // FIXME: why does [ªbove] alt(1) 'produce' "-nan" (ie, instead of the expected "1")? ... and similar tor _mm_cmpge_ps (ie, where alt(1) altways return/'produce' 'when successful')?
	assert(false); // FIXME: complete.
      }

    /* { */
    /*   //! 1 byte -> [0-255] or [0x00-0xFF] */
    /*   uint8_t vec_test[4] = {8, 1, 2, 4}; */
    /*   //uint8_t vec_test = 8; */
      
    /*   uint64_t *vec_test_char = (uint64_t*)vec_test; */
    /*   //      uint64_t *vec_test_char = (uint64_t*)&vec_test; */
    /*   __m128i small_load = _mm_cvtsi64_si128( *vec_test_char );  */
    /*   //__m128i small_load = _mm_loadu_si128( (__m128i*)vec_test ); */
      
    /*   // void_mm_storel_epi64(_m128i * p, __m128i a)  */
    /*   // void_mm_store_sd(double * p, __m128d a)  */

    /*   { */
    /* 	//__m128i result; 	_mm_store_si128(&result, small_load); */
    /* 	int result[4]; 	_mm_store_si128((__m128i*)&result[0], small_load); */
    /* 	//long long int vec = (long long int)small_load;  */
    /* 	for(uint i = 0; i < 4; i++) {printf("[%u] = %d, ", i, result[i]);} */
    /* 	printf("-----------------***\n\n"); */
    /*   } */
    /*   // { */
    /*   // 	float result[4];  */
    /*   // 	_mm_store_ps(result, mask); for(uint i = 0; i < 4; i++) {printf("[%u] = %f, ", i, result[i]);} */
    /*   // 	printf("-----------------***\n\n"); */
    /*   // } */


    /*   assert(false); // FIXME: what does ... __m128i _mm_mulhi_epi16 ( __m128i a, __m128i b) // store high result, and with a simialr/equvialent instruciton for 'low result'. */
    /*   assert(false); // FIXME: what does ... __m128i _mm_mulhi_epi16 ( __m128i a, __m128i b) // store high result */
    /*   assert(false); // FIXME: what does ... __m128i _mm_sad_epu8(__m128i a, __m128i b) //! compute sum of absolute difference */
    /*   assert(false); // FIXME: what does ... __m128i _mm_slli_pi16(__m128i m, __m128i count) //! which is used for left-shift, ie, to ...??.. */
    /*   assert(false); // FIXME: what does ... _mm_movelh_ps */
    /*   assert(false); // FIXME: what does ...  */
    /*   assert(false); // FIXME: what does ... __m64 _mm_madd_pi16(__m64 m1, __m64 m2) */
    /*   assert(false); // FIXME: complete [ªbove]. */
  }
    
  /* { //! Validate the use of char as mask-oeprators: */
  /*   //! Intiate: */
  /*   char mask1[4] = {0, 1, 0, 1};       char mask2[4] = {1, 0, 0, 1}; */
  /*   const uint j2 = 0; */
      
  /*   const __m128 vec_empty_empty = _mm_set1_ps((float)UINT_MAX); 	  const __m128 vec_empty_ones = _mm_set1_ps(1); */



  /*   __m128i vec_mask_toFloat_1 = _mm_cvtsi32_si128(*(const int*)&mask1[j2]);  */
  /*   vec_mask_toFloat_1 = _mm_unpacklo_epi8(vec_mask_toFloat_1, */
  /* 					     //_mm_setzero_si128());  */
  /* 					     _mm_cmplt_epi8(vec_mask_toFloat_1, */
  /* 					     					//vec_empty_empty)); */
  /* 					     		    _mm_setzero_si128())); */
  /*   //vec_mask_toFloat_1 = _mm_and_ps(vec_mask_toFloat_1, vec_empty_UINT_MAX); */
  /*   __m128 vec_cmp_1 = _mm_cvtepi32_ps(vec_mask_toFloat_1); 	 */
  /*   //vec_cmp_1 = _mm_cmpneq_ps(vec_cmp_1, vec_empty_empty);  */
  /*   //vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); */

  /*   { */
  /* 	float result[4];  */
  /* 	_mm_store_ps(result, vec_cmp_1); for(uint i = 0; i < 4; i++) {printf("[%u] = %f, ", i, result[i]);} */
  /* 	printf("-----------------***\n\n"); */
  /*   } */

  /*   __m128i vec_mask_toFloat_2 = _mm_cvtsi32_si128(*(const int*)&mask2[j2]); */
  /*   vec_mask_toFloat_2 = _mm_unpacklo_epi8(vec_mask_toFloat_2, _mm_cmplt_epi8(vec_mask_toFloat_2, _mm_setzero_si128())); */
  /*   __m128 vec_cmp_2 = _mm_cvtepi32_ps(vec_mask_toFloat_2);  */
  /*   vec_cmp_2 = _mm_cmplt_ps(vec_cmp_2, vec_empty_empty);  vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones); */

  /*   __m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1 */

  /*   { */
  /* 	float result[4];  */
  /* 	_mm_store_ps(result, mask); for(uint i = 0; i < 4; i++) {printf("[%u] = %f, ", i, result[i]);} */
  /* 	printf("-----------------***\n\n"); */
  /*   } */
      
  /*   // //! First convert from int to float, and then compare: */
  /*   // __m128 mask = _mm_andnot_ps(_mm_cvtepi32_ps(vec_mask_toFloat_1), _mm_cvtepi32_ps(vec_mask_toFloat_2)); //! ie, where onlu [0]=3 has a value=1 */
  /*   //__m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1 */
  /*   mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order. */
      
  /*   { */
  /* 	float result[4];  */
  /* 	_mm_store_ps(result, mask); for(uint i = 0; i < 4; i++) {printf("[%u] = %f, ", i, result[i]);} */
  /* 	printf("-----------------***\n\n"); */
  /*   } */
      
  /*   assert(false); // FIXME: compelte ... and then udpate our "graphAlgorithms_distance.cxx" wrt. the aswsicated code. */
  /* } */
  /* { //! Validate the use of FLT_MAX as mask-oeprators: */
  /*   const __m128 vec_empty_1 = _mm_set1_ps(FLT_MAX);       const __m128 vec_empty_0 = _mm_set1_ps(1); */
  /*   const __m128 vec_data1 = _mm_set_ps(FLT_MAX, 1, FLT_MAX, 3);      */
  /*   const __m128 vec_data2 = _mm_set_ps(FLT_MAX, FLT_MAX, FLT_MAX, 3);      */
  /*   __m128 vec_cmp_1 = _mm_cmplt_ps(vec_data1, vec_empty_1); vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_0); */
  /*   __m128 vec_cmp_2 = _mm_cmplt_ps(vec_data2, vec_empty_1); vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_0); */
  /*   __m128 vec_cmp = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1 */
  /*   //__m128 vec_cmp = _mm_cmpneq_ps(vec_data1, vec_empty_1);              // identify the cases where the vlaeus is not FLT_MAX     */
  /*   //    __m128 vec_cmp = _mm_andnot_ps(vec_data1, vec_empty_1);              // identify the cases where the vlaeus is not FLT_MAX     */
  /*   //vec_cmp = _mm_andnot_ps(vec_data1, vec_cmp);              // identify the cases where the vlaeus is not FLT_MAX     */
  /*   //    __m128 vec_cmp = _mm_cmpneq_ps(vec_data1, vec_empty_1);              // identify the cases where the vlaeus is not FLT_MAX     */
  /*   if(true) { */
  /* 	float result[4] = {0, 0, 0, 0};  */
  /* 	//_mm_store_ps(result, vec_cmp); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);} */
  /* 	_mm_store_ps(result, vec_cmp); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);} */
  /* 	assert(result[0] != 0); //! ie, as the result is inverted. */
  /* 	printf("-----------------***\n\n"); */
  /* 	//_mm_store_ps(result, vec_result_shuffled); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);} */
  /* 	//_mm_store_ps(result, vec_masked1); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);} */
  /* 	printf("-----------------***\n\n"); */
  /* 	//printf("d = %f\n", d[0]); */
  /*   }     */
  /* } */
}




//! -------------------------------------------------------------------------------------
//! Part(2): concret examples wrt. "mine" and "c-cluster" libries: makes use of [ªbov€] 'simplfied logics':
//! -------------------------------------------------------------------------------------


void simple_examples() {
  const uint list_size = 5; const float list[list_size] = {2 ,7, 3, 3, 12};

  //! -----------------------------------------------------------------------------------------------
  { //! Test if all values are isnide a certain range.
    // FIXME: when [”elow] is optimized, then update our "__copy_rank_hadnleEmptyCells(..)" in our "graphAlgorithms_distance" .. wrt. tag="part-traverse-1d-cmp-ofInterest"
    {
      // FIXME: update [”elow]
      uint current_index = 0;
      for(uint i = 0; i <n; i++) {
	if(isOf_interest(arr1_tmp[i])) {
	  arr1_tmp[i] = arr1_tmp[current_index]; 
	  // FIXME: validate correctness of [”elow].
	  // arr1_tmp[count] = FLT_MAX; 
	  current_index++;
	}
	//! else/otehrwise the function/value is ingored. 
	//assert(i >= count);
      }
    }

  }
  //! -----------------------------------------------------------------------------------------------
  { //! Test if all values are isnide a certain range.
    // FIXME: when [”elow] is optimized, then update our "graphAlgorithms_distance::clusterdistance(..)" .. wrt. tag="part-traverse-1d-cmp-max"
    const uint nrows = list_size; const uint ncolumns = list_size;
    float return_distance = 0;
    { //! Naive code:
      for(uint i = 0; i < list_size; i++) {
	const uint index = list[i];
	if(index >= ncolumns) {return_distance = -1.0;}	
      }
    }
    { //! Code which is hopefully optimized.
      // FIXME[jc]: is it psossible to only test the max-value for 'the max-sum of eahc' .. and woudl this then optimzie teh perfomrance?
      uint i = 0; const uint size_first = (list_size > 4) ? list_size - 4 : 0; //! ie, to simplify our for-loop.
      if(size_first) {
	float result[4];
	for(i = 0; i < size_first; i += 4) {
	  _mm_store_ps(&result[0], 
		       _mm_max_ps(_mm_load_ps(&result[0]), _mm_load_ps(&list[i])));
	}
	//! Investitgate if the [ªbove] valeus are above the list-size:
	for(uint k = 0; k < 4; k++) {
	  if(result[k] >= ncolumns) {return_distance = -1.0;}
	}
      }
      if(return_distance == 0) { //! then we 'read through' the tail-distance-par:
	//! Handle the tail:
	for(; i < list_size; i++) {
	  if(list[i] >= ncolumns) {return_distance = -1.0;}		  
	}
      }
    }
  }


  //! -----------------------------------------------------------------------------------------------
  { //! Identify the sum of elemnts for both the y-axis and the x-axis, ie, the valeus [”elow] a certain trheshold.
    // FIXME: update the post-processing in our "graphAlgorithms_distance::calculate_weights(..)" when [”elow] is optimized ... <-- tag:"value-below-threshold". <-- when this code is udpated/insertied into our "graphAlgorithms_distance.cxx", then perform new time-eprformance-tests to evlauate the signfiance of the udpate.
    const float cutoff = 5.0; const float exponent = 10;
    float result[list_size] = {0, 0, 0, 0, 0}; const uint nelements = list_size;
    const uint i = list_size - 1; //! ie, the 'inner' for-loop.
    { //! Naive code:
      for(uint j = 0; j < i; j++) {
	const float distance = list[j];
	if(distance < cutoff) {
	  const float log_value = 1 - distance/cutoff; //! adjsut the dataset to the cut-off value.
	  assert(log_value != 0);
	  const float dweight = exp(exponent*log(log_value));
	  /* pow() causes a crash on AIX */
	  result[i] += dweight;
	  result[j] += dweight;
	}
      }
      //! "re-weight" the results:
      for(uint i = 0; i < nelements; i++) result[i] = 1.0/result[i];
    }
    { //! Code which is hopefully optimized.
      // FIXME[jc]: may you try to optimize [”elow]? <-- in this copntext, may you try to suggest a code-tmeplate/macro to simplify [”elow] <-- Note: a wild thought, ie, nore sure 'what I am asking for'.
      uint j = 0; const uint size_first = (i > 4) ? i - 4 : 0; //! ie, to simplify our for-loop.
      if(size_first) {
	__mi128 vec_cutoff = _mm_load1_ps(&cutoff); //! ie, set the cutoff.	
	// FXIME[jc]: is there any perofrmance-difference between "_mm_load1_ps(..)" and "_mm_set1_ps(..)" ??
	__mi128 vec_exponent = _mm_load1_ps(&exponent); //! ie, set the cutoff.
	float result_tmp[4] = {0, 0, 0, 0};
	for(j = 0; j < size_first; j += 4) {
	  __mi128 vec_distance = _mm_load_ps(&list[j]);
	  //! Handle the cutoff:
	  assert(false); // FIXME: [below] seems to prodcue teh erronosu reuslsts ... ie, due to the "nan" resutl-values.
	  const __mi128 vec_belowThreshold = _mm_cmplt_ps(vec_distance, vec_cutoff);
	  //! Update the values, ie, under the assumption that 'all valeus above thresholds' are set to '0':
	  vec_distance = _mm_mul_ps(vec_belowThreshold, vec_distance);
	  //! Compute the log-value:
	  // FIXME[jc]: wrt. [”elow] ... how may we handle "nan" and "0" ... ie, any need to specific handling wrt. [below]? <-- possible to use 'wrappers' to simplify
	  const __mi128 vec_logValue = _mm_sub_ps(_mm_set1_ps(1), _mm_div_ps(vec_distance, vec_cutoff));
	  //! Compute the exponent:
	  vec_distance = _mm_exp_ps(_mm_mul(vec_exponent, vec_logValue));  //! ie, exp(exponent*log(log_value));
	  //! Update the result, first wrt. the 'inner' value:
	  _mm_store_ps(&result[j], //! which is the sum of the 'previous' and the 'new-identifed value'
		       _mm_add_ps(_mm_load_ps(&result[j]), vec_distance)); //! ie, " += ".
	  
	  //! Then wrt. the 'outer' value:
	  _mm_store_ps(&result_tmp[0], //! which is the sum of the 'previous' and the 'new-identifed value'
		       _mm_add_ps(_mm_load_ps(&result_tmp[0]), vec_distance)); //! ie, " += ".
	  
	}
	// FIXME[jc]: is there any better stragy than [”elow]?
	for(uint k = 0; k < 4; k++) {
	  result[i] += result_tmp[k];
	}
      }
      //! Then complete [ªbove].
      for(; j < i; j++) {
	const float distance = list[j];
	if(distance < cutoff) {
	  const float log_value = 1 - distance/cutoff; //! adjsut the dataset to the cut-off value.
	  assert(log_value != 0);
	  const float dweight = exp(exponent*log(log_value));
	  /* pow() causes a crash on AIX */
	  result[i] += dweight;
	  result[j] += dweight;
	}
      }
      //! "re-weight" the results:
      uint i = 0; const uint size_first = (i > 4) ? nelements - 4 : 0; //! ie, to simplify our for-loop.
      if(size_first) {
	for(i = 0; i < size_first; i += 4) {
	  //! Compute: result[i] = 1.0/result[i];
	  // FIXME[jc]: wrt. [”elow] ... how may we handle "nan" and "0" ... ie, any need to specific handling wrt. [below]?
	  _mm_store_ps(&result[i], //! which is the sum of the 'previous' and the 'new-identifed value'
		       _mm_div_ps(_mm_set1_ps(1), _mm_load_ps(&result[i]))); //! ie, "1 / value"
	}
      }
      //! Then complete [ªbove].
      for(; i < nelements; i++) {
	result[i] = 1.0/result[i];
      }
    }
  }
}

//! -------------------------------------------------------------------------------------
//! Part(3): extensive fucntiosn which makes use of [ªbove] optimizations: a re-curring chalelnge (ie, possible bug/errors) conserns the "nan" values, ie, how to 'translate the altter with minimal cost' to the 'correct' "0" and "1"
//! -------------------------------------------------------------------------------------


// Note[jc]: this funciton is a subset of the "graphAlgorithms_distance::compute_rank_forVector(..)" call ... and similar to "compute_distance_array_implicitMask(..)" and "compute_distance_array_mask(..)" ... and when completed then update our "graphAlgorithms_distance::spearman(..)" <-- wrt. the latter, consider to update some of the 'naive legacy-funcitons' to 'hold' both optimized nav teh 'old naive impelmetnaitons'.
void compute_rank_forVector(const uint index1, const uint size, float **data, char **mask, float *tmp_array) {
  assert(size); assert(result_array); assert(tmp_array); assert(data);
  //! Idnetify the interesting vertices:
  if(mask) {
    for(uint i = 0; i < size; i++) {
      if(mask[index1][i]) {
	tmp_array[i] = data[index1][i];
      } else {tmp_array[i] = FLT_MAX;}
    }
  } else {
    for(uint i = 0; i < size; i++) {
      if(isOf_interest(data[index1][i])) {
	tmp_array[i] = data[index1][i];
      } else {tmp_array[i] = FLT_MAX;}
    }
  }
}


//! Idnetify a the assicated transpsoed array.
//! Note[jc]: this funciton is similar to "compute_rank_forVector(..)" (ie, also found in our "graphAlgorithms_distance.cxx") 
// FIXME[jc] is it possible to access mask[i][index1] and data[i][index1] with 'jumps' ... eg, to use "size*sizeof(float)" when 'reading each chunk into the vector? <-- if so, then try/consider to update our "graphAlgorithms_distance::compute_rank_forVector_firstTranspose(..)"
static void get_transposed_column(const uint index1, const uint size, float **data, char **mask, float *tmp_array) {
  //! Idnetify the interesting vertices:
  if(mask) {
    for(uint i = 0; i < size; i++) {
      if(mask[i][index1]) {
	tmp_array[i] = data[i][index1];
      } else {tmp_array[i] = FLT_MAX;}
    }
  } else {
    for(uint i = 0; i < size; i++) {
      if(isOf_interest(data[i][index1])) {
	tmp_array[i] = data[i][index1];
      } else {tmp_array[i] = FLT_MAX;}
    }
  }
}

//! Note[jc]: this funciotn refers to "graphAlgorithms_distance::find_closest_pair(..)"
//! @return the identifed distance for the pair in a symmetric matrix
//! @remarks This function searches the distance matrix to find the pair with the shortest distance between them. 
//!          The indices of the pair are returned in ip and jp; 
//! @remarks time-complexity estimated to be "n*n", ie, quadratic.
//! @remarks Memory-access-pattern of this funciton is:  two for-loops which in intship identify/find the pair and distance with the smallest value; in 'ideal' solutions/appraoches one cache-miss, otherwise "n" cache-misses. 
float find_closest_pair(const uint n, float** distmatrix, uint* ip, uint* jp) {
  //! What we expect:
  assert(distmatrix);
  assert(n > 0);
  //! Intialize:
  float distance = distmatrix[1][0];
  *ip = 1;  *jp = 0;
  //! Find the 'shortest' pair:

  // FIXME: write emmory-benchmarks to test the effects of intinitstics on/for this function

  if(true) {

    // FIXME[optimize]: may you jan-christiaon write an optmization of [”elow] code-chunk? <-- 

    float bestScores_distance[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};
    float bestScores_point_i[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};
    float bestScores_point_j[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

    // uint idx = 0; __m128 max = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX};

    for(uint i = 1; i < n; i++) { 

      assert(false); // FIXME: try to resovle errors in [”elow] 'optimzaiton-code'.
      
      uint j = 0;
      if((i+4) > 8) {
	const uint last_pos = i-4; //! ie, to 'allow' for overflow.
	for(uint j = 0; j < last_pos; j += 1) { 
	  // FIXME[jc]: wrt. [”elow] ... could "http://stackoverflow.com/questions/9877700/getting-max-value-in-a-m128i-vector-with-sse" result in better/imrpvoed perofmrance?
	  // const __m128 value = _mm_load_ps(&distmatrix[i][j]); // can either use an array or __m128 value calculated in loop
	  // const __m128 temp1 = _mm_shuffle_ps(value, value, _MM_SHUFFLE(1, 0, 3, 2));
	  // const __m128 temp2 = _mm_max_ps(value, temp1);
	  // const __m128 temp3 = _mm_shuffle_ps(temp2, temp2, _MM_SHUFFLE(2, 3, 0, 1));
	  // const __m128 temp4 = _mm_min_ps(temp3, temp2);
	  // max = _mm_min_ps(temp4, max);
        
	  // unsigned long index;
	  // const unsigned long mask = _mm_movemask_ps(_mm_cmpeq_ps(value, max));
	  // const unsigned char bit = _BitScanForward(&index, mask);

	  // if (bit)
          //   idx = i * 4 + index;

	  __m128 vec_result_tmp = _mm_loadu_ps(&bestScores_distance[0]);
	  const __m128 arr_data = _mm_load_ps(&distmatrix[i][j]);
	  //! Find the maximum value:
	  __m128 vec_max = _mm_max_ps(vec_result_tmp, arr_data);

	  // FIXME: is there a more 'precie' methiod to infer/'get' the assicated index-i and index-k?

	  //! Figure out if the values are in the max-set:
	  __m128 vec_isInMax_data1 = _mm_cmplt_ps(vec_max, arr_data);
	  __m128 vec_isNotInMax_result = _mm_cmpge_ps(vec_result_tmp, arr_data);
	  //__m128 vec_isInMax_inverted = _mm_and_ps(vec_result_tmp, _mm_setzero_ps());
	  
	  //! Identify the indices assicated to the max-index, first for the 'new set':
	  float flt_tmp_i = (float)i;
	  __m128 vec_data_data_index_i = _mm_mul_ps(vec_isInMax_data1, _mm_load1_ps(&flt_tmp_i));
	  // FIXME: instead of [”elow] try using the _mm_shuffle(...)
	  float flt_tmp_j[4] = {(float)(j+0), (float)(j+1), (float)(j+2), (float)(j+3)};
	  __m128 vec_data_data_index_j = _mm_mul_ps(vec_isInMax_data1, _mm_load_ps(&flt_tmp_j[0]));

	  //! Identify the indices assicated to the max-index, thereafter for the 'best-fit set':
	  __m128 vec_data_maxUpdated_index_i = _mm_mul_ps(vec_isNotInMax_result, _mm_load_ps(&bestScores_point_i[0]));
	  __m128 vec_data_maxUpdated_index_j = _mm_mul_ps(vec_isNotInMax_result, _mm_load_ps(&bestScores_point_j[0]));

	  //! Combine the results: as we expect only one of the indeices to be set, we use 'add':
	  _mm_store_ps(&bestScores_distance[0], vec_max); //! ie, sotre the max-values.
	  _mm_store_ps(&bestScores_point_i[0], _mm_add_ps(vec_data_maxUpdated_index_i, vec_data_data_index_i)); //! ie, max-valeus for store for index=i
	  _mm_store_ps(&bestScores_point_j[0], _mm_add_ps(vec_data_maxUpdated_index_j, vec_data_data_index_j)); //! ie, max-valeus for store for index=j
	}
      }
      //! Handle the not-yet-covered cases:
      for(; j < i; j++) { 
	const float temp = distmatrix[i][j];
	if(temp < bestScores_distance[0]) { //! then a shorter distance is found.
	  bestScores_distance[0] = temp;
	  bestScores_point_i[0] = i;
	  bestScores_point_j[0] = j;
	}
      }
    }
    //! Identify the min-point:
    for(uint i = 0; i < 4; i++) {
      const float temp = bestScores_distance[i];
      if(temp < distance) { //! then a shorter distance is found.
	distance = temp;
	*ip = bestScores_point_i[i];
	*jp = bestScores_point_i[i];
      }
    }
  } else {
    for(uint i = 1; i < n; i++) { 
      for(uint j = 0; j < i; j++) { 
	const float temp = distmatrix[i][j];
	if(temp < distance) { //! then a shorter distance is found.
	  distance = temp;
	  *ip = i;
	  *jp = j;
	}
      }
    }
  }
  return distance;
}

/** 
    @brief Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    @param <data> is the input-meatrix to transpose.
    @param <nrows> is the number of rows in data, ie, the number of columns in resultMatrix.
    @param <ncolumns> is the number of columns in data, ie, the number of rows in resultMatrix
    @param <resultMatrix> is the matrix which 'contain' the inverse data-set.
    @param <isTo_useIntrisinistic> which if set to false implies that we use default C lanauge-specific operations: mainly included for benchmarking.
**/
void compute_transposedMatrix(const uint nrows, const uint ncolumns, float **mul1, float **res, const bool isTo_useIntrisinistic) {
  //! What we expect:
  assert(nrows > 0);
  assert(ncolumns > 0);
  assert(mul1);
  assert(res);

  //! Note[jc]: this funciton refers to "graphAlgorithms_distance::compute_transposedMatrix(..)"

  //! Iterate:
  // Note: in [”elow] we assume continus memory-allcoation-rotuiens, ie, to 'increment' in the memory.
  // printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
  const uint size_innerOptimized = (isTo_useIntrisinistic && (ncolumns > 4)) ? ncolumns - 4 : 0;
  
  for (uint i = 0; i < nrows; i += 1) { //SM) {
    assert(mul1[i]);
    // const uint chunkSize_row = (cnt_i++ < numberOf_chunks_rows) ? SM : chunkSize_row_last;
    // uint cnt_j = 0; 
    uint j = 0;
    if(size_innerOptimized > 0) {
      for (; j < ncolumns; j += 4) { // SM) {
	//! Store a value without loading a cache line, ie, use intrinsic functions:
	// FIXME[jc]: would it go faster if we used _mm_load1_ps(..) .... and is there any other magics/trickery? .... and is there any other magics/trickery? .... and what is the difference wrt. _mm_stream_ps(..) and "_mm_store_ss(..)" ??
	__m128 vec_tmp = _mm_set_ps(mul1[j+0][i], mul1[j+1][i], mul1[j+2][i], mul1[j+3][i]);
	_mm_store_ps(&res[i][j], vec_tmp);
      }
    } 
    //! Compelte the update:
    for (; j < ncolumns; j += 1) { // SM) {
      res[j][i] = mul1[i][j];
    }
  }
}


/**
   @brief idnetify the centroids of each cluster.
   @param <nclusters> The number of clusters.
   @param <nelements> The total number of elements.
   @param <distance> Number of rows is nelements, number of columns is equal to the row number: the distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
   @param <clusterid> The cluster number to which each element belongs.
   @param <centroids> The index of the element that functions as the centroid for each cluster.
   @param <errors> The within-cluster sum of distances between the items and the cluster centroid.
   @param <isTo_useOptimizedVersion> which is included for debug-purposes to investigate time-effects.
   @remarks 
   - idea: identify the sum of distances to all members in a given cluster: for each clsuter 'allcoate' the centorid which has the minimum/lowest 'sum of distance to other members' (in the same cluster).
   - this function calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the element with the smallest sum of distances to the other elements.
 **/
void getclustermedoids(const uint nclusters, const uint nelements, float** distance, uint clusterid[], uint centroids[], float errors[], const bool isTo_useOptimizedVersion) {

  if(isTo_useOptimizedVersion == true) {
    // FIXME: update our "graphAlgorithms_kCluster::getclustermedoids(..)" wrt. the [”elow] otpimization.  

    //! First intiate:
    // FIXME[jc]: are there any meset(..) fucntiosn which are otpimized and 'acaptable' of using floats 'in their memset input-args'?
    { const uint nclustersOptimized_size = (nclusters > 4) ? nclusters - 4 : 0;
      uint j = 0;
      if(nclustersOptimized_size > 0) {
	for(; j < nclustersOptimized_size; j += 4) {
	  _mm_store_ps(&errors[j], _mm_set1_ps(FLT_MAX));
	}
      }
      //! Then update the 'remaining tail':
      for(; j < nclusters; j++) {errors[j] = FLT_MAX;}
    }
    //! Then start the work:
    const uint nclustersOptimized_size = (nelements > 4) ? nelements - 4 : 0;
    for(uint i = 0; i < nelements; i++) {
      const uint j = clusterid[i];
      float d = 0.0;
      uint k = 0;
      //! the optimized code-chunk:
      if(nclustersOptimized_size > 0) {
	float result_tmp[4] = {0, 0, 0, 0};
	for(; k < nclustersOptimized_size; k += 4) {
	  //! Load the data:
	  __mi128 vec_distance = _mm_load_ps(&distance[i][k]);
	  // FIXME[jc]: will the code 'by default' go faster if [”elow] is placed otuside this for-loop' ... ie, 'any maximum wrt. number of variables wrt. otpimziaiton'?
	  __mi128 vec_index_in = _mm_set1_ps((float)i);
	  __mi128 vec_index_out = _mm_set_ps((float)(k+0), (float)(k+1), (float)(k+2), (float)(k+3));
	  { //! Test case (i == k):
	    // FIXME[jc]: are the any more efficent calls than _mm_set_ps for [below]?
	    // FIXME[jc]: validate correcntess of [”elow] _mm_cmpneq_ps(..) 
	    __mi128 vec_indexComparison_loop = _mm_cmpneq_ps(vec_index_in, vec_index_out);
	    //! Update the distance: asusems that 'no-matched-valeus'==0
	    vec_distance = _mm_mul_ps(vec_distance, vec_indexComparison_loop);
	  }
	  
	  { //! Test case (clusterid[k] != j)
	    __mi128 vec_index_cluster = _mm_load_ps(&clusterid[k]);
	    // FIXME[jc]: validate correcntess of [”elow] _mm_cmneq_ps(..) 
	    __mi128 vec_indexComparison_loop = _mm_cmpeq_ps(vec_index_cluster, vec_index_out);
	    //! Update the distance: asusems that 'no-matched-valeus'==0
	    vec_distance = _mm_mul_ps(vec_distance, vec_indexComparison_loop);
	  }
	  // FIXME[jc]: commented out [”elow] ... as the docuemtnationasserts/says that the input-matrix is symmetric: may you itnrospect upon why the authros 'has chosen [”elow]' ... ie, any reasons 'for why to sue [”elow] (which I have missed)?
	  /* d += ( (i < k) ? distance[k][i] : distance[i][k]); //! then we sume the distances between the centrodi and the orther vertides. */

	  //! Update the sum:
	  _mm_add_ps(_mm_load_ps(result_tmp), vec_distance);
	}
	// FIXME[jc]: possible to perofmr/fo [”elow] 'sum of vector-valeus' more effective?
	for(uint m = 0; m < 4; m++) {
	  d += result_tmp[m];
	  // FIXME[jc]: should we include [below] "break" in [abov€] for-loop?
	  if(d > errors[j]) break;
	}
	assert(false); // FIXME: complete ... 
      }
      for(; k < nelements; k++) {
	if( (i == k) || (clusterid[k] != j) ) continue; //! then it is either a self-comparison of not part of the given clsuter.
	//! Update the distance to the other vertices in the same cluster.
	d += ( (i < k) ? distance[k][i] : distance[i][k]); //! then we sume the distances between the centrodi and the orther vertides.
	if(d > errors[j]) break;
      }
      if(d < errors[j]) { //! then the vertex is closer than any of the other centroids, ie, update.
	// FIXME[jc]: if we use _mm_set_ps(..) in [”elow] woudl the code then go faster .... ie, any 'achnce' to optimze [”elow]?
	errors[j] = d;
	centroids[j] = i;
      } 
    }
  } else {
    for(uint j = 0; j < nclusters; j++) errors[j] = FLT_MAX;
    for(uint i = 0; i < nelements; i++) {
      const uint j = clusterid[i];
      float d = 0.0;
      // FIXME: use  _mm_storeu_ps(..) 
      for(uint k = 0; k < nelements; k++) {
	// FIXME: write a 'test-bed' for [below].
	// FIXME: consider to udpate this function to first construct seperate lsits between the different clusters ... and then comptue the distance ... ie, to reduce the assicated exeuction-time.
	if( (i == k) || (clusterid[k] != j) ) continue; //! then it is either a self-comparison of not part of the given clsuter.
	//! Update the distance to the other vertices in the same cluster.
	d += ( (i < k) ? distance[k][i] : distance[i][k]); //! then we sume the distances between the centrodi and the orther vertides.
	if(d > errors[j]) break;
      }
      if(d < errors[j]) { //! then the vertex is closer than any of the other centroids, ie, update.
	errors[j] = d;
	centroids[j] = i;
      }
    }
  }
}

#include "e_kt_correlationFunction.h"
/* typedef enum e_optimal_compute_allAgainstAll_type { */
/*   e_optimal_compute_allAgainstAll_type_euclid, */
/*   e_optimal_compute_allAgainstAll_type_cityblock, */
/*   e_optimal_compute_allAgainstAll_type_spearman_or_correlation, */
/*   // FIXME: make use of [”elow] and then update our article witht he new-itnroduced correlation-emtric ...   and consider to include/inroroporate other correlation-metrics ("http://people.sc.fsu.edu/~jburkardt/c_src/correlation/correlation.html") ... writtten for an aritcle with title "A Review of Gaussian Random Fields and Correlation Functions" */

/*   e_optimal_compute_allAgainstAll_type_brownian_correlation, //! "Petter Abrahamsen,     A Review of Gaussian Random Fields and Correlation Functions,     Norwegian Computing Center, 1997." and "https://en.wikipedia.org/wiki/Distance_correlation" */
  
/* e_optimal_compute_allAgainstAll_type_undef */
/* } e_kt_correlationFunction_t; */

//! @return the distances assicated to "e_kt_correlationFunction_t"
      /* else if(typeOf_metric == e_kt_correlationFunction_brownian_correlation) { \ */
      /* const float max_val = max(val1, val2); \ */
      /* if(0 < max_val) {term = sqrt(min(val1, val2) / max_val);} else {term = 0;} \ */
      /* }									\ */
#define __macro__get_distance(val1, val2, weight, weight_index, typeOf_metric) ({ \
      float term = 0;							\
      if(typeOf_metric == e_kt_correlationFunction_spearman_or_correlation) { term = val1 * val2;} \
      else if(typeOf_metric == e_kt_correlationFunction_cityblock) {term = fabs(val1 - val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_euclid) { term = val1 - val2; term = term*term; } \
      else {assert(false);}						\
const float weight_local = (weight) ? weight[weight_index] : 1;		\
const float result_local = weight_local*term;				\
result_local;								\
})


//! @return the distances assicated to "e_kt_correlationFunction_t"
      /* else if(typeOf_metric == e_kt_correlationFunction_brownian_correlation) { \ */
      /* 	const __m128 vec_max = _mm_max_ps(_mm_sub_ps(_mm_setzero_ps(), term2), term1); \ */
      /* 	const __m128 vec_min = _mm_min_ps(_mm_sub_ps(_mm_setzero_ps(), term2), term1); \ */
      /* 	const __m128 vec_divFactor = _mm_div_ps(vec_min, vec_max); \ */
      /* 	const __m128 vec_compareMax_zeros = _mm_cmplt_ps(vec_max, _mm_setzero_ps()); \ */
      /* 	const __m128 vec_ofInterest = _mm_andnot_ps(vec_compareMax_zeros, vec_max); \ */
      /* 	const __m128 vec_valuesWithAnswer = _mm_and_ps(vec_compareMax_zeros, _mm_sqrt_ps(vec_divFactor)); \ */
      /* 	mul = _mm_or_ps(vec_ofInterest, vec_valuesWithAnswer); \ */
      /* }									\ */
#define __macro__get_distance_SIMD(val1, val2, weight, weight_index, typeOf_metric) ({ \
  __m128 mul; \
      if(typeOf_metric == e_kt_correlationFunction_spearman_or_correlation) { mul = _mm_mul_ps(term1, term2); } \
      else if(typeOf_metric == e_kt_correlationFunction_cityblock) {mul = _mm_sub_ps(term1, term2); mul = _mm_max_ps(_mm_sub_ps(_mm_setzero_ps(), mul), mul); ;} \
      else if(typeOf_metric == e_kt_correlationFunction_euclid) { mul = _mm_sub_ps(term1, term2); mul = _mm_mul_ps(mul, mul); } \
      else {assert(false);}						\
if(weight) {mul = _mm_mul_ps(mul, _mm_set1_ps(weight[weight_index]));}	\
mul;								\
})




// FIXME[jc]: compare the correctness and degree of optimzaliation wrt. __macro__get_distance(..) and "__macro__get_distance_SIMD(..)" ... and when the "__macro__get_distance_SIMD(..)" is correct, then consider/try writing an optimized 'verison' of the latter.


//! Note: use FLT_MAX to identify the valeus to compare.
static void compute_distance_array_implicitMask(const float *rmul1, const float *rmul2, const uint chunkSize_cnt_i, float *rres_scalar) const {
  // FIXME: when [below] is udpated/opimized then update __optimal_compute_allAgainstAll_SIMD(..)" in our "graphAlgorithms_distance.cxx" ... and our "graphAlgorithms_distance::compute_allAgainstAll_distanceMetric_correlation(...)"
  float result[4] = {0, 0, 0, 0}; //! which is used to reduce teh compleity of the summing-operaiton.
  const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1);
  for (uint j2 = 0; j2 < chunkSize_cnt_i; j2 += 4) {
    // TODO: update our documetnation wwtih [below] observaiton.		    
    //! Get the values:
    __m128 term1  = _mm_load_ps(&rmul1[j2]); __m128 term2  = _mm_load_ps(&rmul2[j2]);  
    //! Compute the AND mask:		    
    // FIXME[jc]: ask jan-christian to vlaidate correctness of [”elow] ... where intail correctnes-stests are seen/found int ehs tart of our evaluate_cmpOf_two_vectors_wrt_intrisinitstics() in our "graphAlgorithms_timeMeasurements_syntetic.cxx" 
    __m128 vec_cmp_1 = _mm_cmplt_ps(term1, vec_empty_empty); 
    vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); // FIXME[jc]: may omit/drop this call .. eg, wrt. the "nan" values?
    __m128 vec_cmp_2 = _mm_cmplt_ps(term2, vec_empty_empty);
    vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones); // FIXME[jc]: may omit/drop this call .. eg, wrt. the "nan" values?
    __m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1
    mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order. // FIXME[jc]: may omit/drop this call  ... ie, any funcitons which 'does not by default rever the resulsts?
    
    //! Get the distance:
    __m128 mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
    //! Make use of the mask:
    mul = _mm_mul_ps(mask, mul);
    //! Load the result-vector
    //! Add the values:
    __m128 arr_result_tmp = _mm_add_ps(mul, _mm_loadu_ps(&result[0])); //! ie, combien the sums.
    //! Store the result:
    _mm_store_ps(&result[0], arr_result_tmp);
  }
  //! Combine teh vectors:
  // FIXME: try to dientify a SIMD instruction which 'does [”elow] atuotmatically' <-- do joun jan-chrsitain ahve anys gugestiosn?
  for(uint i = 0; i < 4; i++) {rres_scalar += result[i];}		
}

//! Note: use "rmask1" and "rmask2" to identify the valeus to compare. 
static void compute_distance_array_mask(const float *rmul1, const float *rmul2, const char *rmask1, const char *rmask2, const uint chunkSize_cnt_i, float *rres_scalar) const {
  // FIXME: when [below] is udpated/opimized then update __optimal_compute_allAgainstAll_SIMD(..)" in our "graphAlgorithms_distance.cxx" ... and our "graphAlgorithms_distance::compute_allAgainstAll_distanceMetric_correlation(...)"
		
  //! The operation:
  float result[4] = {0, 0, 0, 0}; //! which is used to reduce teh compleity of the summing-operaiton.
  const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1);
  //! Note: we expect every valeu 'ot fit onto' chunkSize_cnt_i, ie, as the latter is tob e divisible by "4", ie, which explains why we do not sue a ssperate for-loop 'after [”elow]'.
  for (uint j2 = 0; j2 < chunkSize_cnt_i; j2 += 4) {
    // TODO: update our documetnation wwtih [below] observaiton.		    
    //! Get the values:
    __m128 term1  = _mm_loadu_ps(&rmul1[j2]); __m128 term2  = _mm_loadu_ps(&rmul2[j2]);  

    // FIXME: try to optimzie [below] code. <-- ask jan-crhistain for suggestions.
    const __m128 vec_mask1 = _mm_set_ps((float)rmask1[j2+0], (float)rmask1[j2+1], (float)rmask1[j2+2], (float)rmask1[j2+3]);
    const __m128 vec_mask2 = _mm_set_ps((float)rmask2[j2+0], (float)rmask2[j2+1], (float)rmask2[j2+2], (float)rmask2[j2+3]);
    __m128 mask = _mm_and_ps(vec_mask1, vec_mask2); //! ie, where onlu [0]=3 has a value=1
    // const __m128 vec_data2 = _mm_set_ps(FLT_MAX, FLT_MAX, FLT_MAX, 3);     

    // FIXME[jc]: [”elow] is included in order to examplify one of our 'approaches', ie, to 'unpack' the chars ... a copy-paste from the web ... ie, have not yet managed to fully grasp/understand [”elow].
    // 		  //! Compute the AND mask:		    
    // 		  // FIXME: ask jan-christian to vlaidate correctness of [”elow] ... where intail correctnes-stests are seen/found int ehs tart of our evaluate_cmpOf_two_vectors_wrt_intrisinitstics() in our "graphAlgorithms_timeMeasurements_syntetic.cxx"
    // 		  __m128i vec_mask_toFloat_1 = _mm_cvtsi32_si128(*(const int*)&mask1[j2]); 
    // 		  vec_mask_toFloat_1 = _mm_unpacklo_epi8(vec_mask_toFloat_1, _mm_cmplt_epi8(vec_mask_toFloat_1, _mm_setzero_si128()));
    // 		  __m128i vec_mask_toFloat_2 = _mm_cvtsi32_si128(*(const int*)&mask2[j2]);
    // 		  vec_mask_toFloat_2 = _mm_unpacklo_epi8(vec_mask_toFloat_2, _mm_cmplt_epi8(vec_mask_toFloat_2, _mm_setzero_si128()));
    // 		  //__m128 vec_cmp_1 = _mm_loadu_ps(&rmask1[j2]);  __m128 vec_cmp_2 = _mm_loadu_ps(&rmask2[j2]);
    // 		  //m128  (__m128i v1)		    __m128d 

    // 		  //! First convert from int to float, and then compare:
    // 		  __m128 mask = _mm_and_ps(_mm_cvtepi32_ps(vec_mask_toFloat_1), _mm_cvtepi32_ps(vec_mask_toFloat_2)); //! ie, where onlu [0]=3 has a value=1
    //__m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); //! ie, where onlu [0]=3 has a value=1
    mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
		  
    //! Get the distance:
    __m128 mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
    //! Make use of the mask:
    mul = _mm_mul_ps(mask, mul);

    //! Load the result-vector
    __m128 arr_result_tmp = _mm_loadu_ps(&result[0]);
    //! Add the values:
    arr_result_tmp = _mm_add_ps(mul, arr_result_tmp);
    //! Store the result:
    _mm_store_ps(&result[0], arr_result_tmp);
  }
  //! Combine teh vectors:
  // FIXME: try to dientify a SIMD instruction which 'does [”elow] atuotmatically' <-- do joun jan-chrsitain ahve anys gugestiosn?
  for(uint i = 0; i < 4; i++) {rres[k2] += result[i];}
}



// FIXME: for [”elow] write a new fucniton suing SIMD ... and then update the callers of/to the funciotn in our "graphAlgorithms_distance.cxx" 
//! @return the assicated and computed correlation-score.
float compute_score_correlation(float result, const float sum1, const float sum2, const float tweight, float denom1, float denom2, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const uint sizeOf_i) {



  const char flag = (sum1 || sum2); // FIXME: validate that a cell with value=0 is not of interest.
  
  if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_basic) {
    if(!tweight) return 0; /* usually due to empty clusters */
    result -= sum1 * sum2 / tweight;
    denom1 -= sum1 * sum1 / tweight;
    denom2 -= sum2 * sum2 / tweight;
    if(denom1 <= 0) return 1; /* include '<' to deal with roundoff errors */
    if(denom2 <= 0) return 1; /* include '<' to deal with roundoff errors */
    result = result / sqrt(denom1*denom2);
    result = 1. - result;
    return result;
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_absolute) {
    if(!tweight) return 0; /* usually due to empty clusters */
    //! Result-base = abs( sum( w(tail)*w(head_1,tail)*w(head_2,tail) ) - ( sum( w(tail)*w(head_1,tail) ) * (sum (w(tail)*w(head_2,tail) ) ) / sum(w(tail)) ) ) 
    result -= sum1 * sum2 / tweight;
    denom1 -= sum1 * sum1 / tweight;
    denom2 -= sum2 * sum2 / tweight;
    if(denom1 <= 0) return 1; /* include '<' to deal with roundoff errors */
    if(denom2 <= 0) return 1; /* include '<' to deal with roundoff errors */
    result = fabs(result) / sqrt(denom1*denom2);
    result = 1. - result;
    return result;
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered) {
    if(!flag) return 0.;
    if(denom1==0.) return 1.;
    if(denom2==0.) return 1.;
    result = result / sqrt(denom1*denom2);
    result = 1. - result;
    return result;
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered_absolute) {
    if(!flag) return 0.;
    if(denom1==0.) return 1.;     if(denom2==0.) return 1.;
    result = fabs(result) / sqrt(denom1*denom2);
    result = 1. - result;
    return result;
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_spearMan) {
    if(result==0.) return 1.; if(denom1==0.) return 1.;     if(denom2==0.) return 1.; // TODO: validate correctness of this.
    result /= sizeOf_i;
    denom1 /= sizeOf_i;
    denom2 /= sizeOf_i;
    const float avgrank = 0.5*(sizeOf_i - 1); /* Average rank */
    result -= avgrank * avgrank;
    denom1 -= avgrank * avgrank;
    denom2 -= avgrank * avgrank;
    if(denom1 <= 0) return 1.; /* include '<' to deal with roundoff errors */
    if(denom2 <= 0) return 1.; /* include '<' to deal with roundoff errors */
    assert((denom1*denom2) != 0);
    result = result / sqrt(denom1*denom2);
    result = 1. - result;
    //! @return the result:
    return result;
  } else {
    assert(false); //! ie, then add support for this.
  }
}

// FIXME: for [”elow] write a new fucniton suing SIMD ... and then update the callers of/to the funciotn in our "graphAlgorithms_distance.cxx" 
//! @return the assicated and computed correlation-score.
float compute_score_correlation_SIMD(float result, const float sum1, const float sum2, const float tweight, float denom1, float denom2, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const uint sizeOf_i) {
  // FIXME[jc]: if you have the time, it would be great if you could write your own version of this ... <-- if I have the time (ie, after I've improved "mine.c" on Thursday), I'll write a'my own' in a ssperte file ... and then we may comapre the versions ... if so, may you then itnrospect upon how we may use the different 'impelemtantion-versions' to describe the 'way of thinking' wrt. intrinsic programming-philosphy?


}


/**


   @remarks code is based on example at "http://stackoverflow.com/questions/13494173/how-to-efficiently-combine-comparisons-in-sse"
 **/
void simple_if_clause() {
  {
    float x1, x2, x3;
    float a1[], a2[], a3[], b1[], b2[], b3[];
    for (i=0; i < N; i++)
      { 
	if (x1 > a1[i] && x2 > a2[i] && x3 > a3[i] && x1 < b1[i] && x2 < b2[i] && x3 < b3[i]) {
	  //! Then all values are above an evlauated/compared threshold:
	    // do something with i
	  }
    }
  }
  {
    __m128 x; // x1, x2, x3, 0
    __m128 a[N]; // packed a1[i], a2[i], a3[i], 0 
    __m128 b[N]; // packed b1[i], b2[i], b3[i], 0

    for (int i = 0; i < N; i++)
      {
	__m128 gt_mask = _mm_cmpgt_ps(x, a[i]);
	__m128 lt_mask = _mm_cmplt_ps(x, b[i]);
	__m128 mask = _mm_and_ps(gt_mask, lt_mask);
	if (_mm_movemask_epi8 (_mm_castps_si128(mask)) == 0xfff0)
	  {
	    // do something with i
	  }
      }    
  }
  {
    __m128 x; // x1, x2, x3, 0
    __m128 a[N]; // packed a1[i], a2[i], a3[i], 0
    __m128 b[N]; // packed b1[i], b2[i], b3[i], 0
    __m128i ref_mask = _mm_set_epi32(0xffff, 0xffff, 0xffff, 0x0000);

    for (int i = 0; i < N; i++)
      {
	__m128 gt_mask = _mm_cmpgt_ps(x, a[i]);
	__m128 lt_mask = _mm_cmplt_ps(x, b[i]);
	__m128 mask = _mm_and_ps(gt_mask, lt_mask);
	if (_mm_testc_si128(_mm_castps_si128(mask), ref_mask))
	  {
	    // do stuff with i
	  }
      }
  }
}

#endif



