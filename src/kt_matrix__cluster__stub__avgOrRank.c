assert(isTo_transposeMatrix == false); // ie, to simplify our lgoics.

const uint size_inner = local_matrix->nrows;
assert(size_inner > 0);
  //! Intaite the result-opbject:
uint nclusters_adjusted = (nclusters/max_cntForests) + 1;
if(nclusters_adjusted == 1) {
  fprintf(stderr, "!!\t How are we to correctly handle this case ... ie, 'shall' we then instead use djsijoint-forest-clustering? OBserviaont at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  assert(false); // TODO: thereafter update our "kt_matrix__cluster__stub__medoid.c"
  assert(false); // TODO: handle this case.
 }
s_kt_clusterAlg_fixed_resultObject_t obj_local_result; init__s_kt_clusterAlg_fixed_resultObject_t(&obj_local_result, nclusters_adjusted, size_inner);


if(obj_result->vertex_clusterId != NULL) { //! then we 'copy' and rem-map the pre-defined-xluter-sets (eosekth, 06. jna. 2017)

  // TODO: consider to add supprot for this ... currnelty dropped given the 'poetnail ambigutiy' wrt. the idneitifed disjotinf-clsuter-sets

 }

//!
//! Handle cases where a user has defined/idneitifed a 'sepcific' set of centrlaity-measures:
s_kt_randomGenerator_vector_t *obj_rand__init__local = NULL;
s_kt_randomGenerator_vector_t obj_rand__init__local__ = initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t();
s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(local_matrix->nrows, local_matrix->ncols, local_matrix->matrix);
if(obj_rand__init != NULL) {
  //assert(mapOf_clusterVertices__toClusterIds__local != NULL);
  //! Construct a subset of the vertices using the [abov€] mapping-table (oekseth, 06. feb. 2017):
  obj_rand__init__local__ = initAndReturn__fromSuper__s_kt_randomGenerator_vector_t(mapOf_clusterVertices__toClusterIds__local, /*superset=*/obj_rand__init, &obj_matrix_input, nclusters_adjusted); //local_matrix);

  *obj_rand__init__local = obj_rand__init__local__;
 }

//! *****************************************************************************************************************************
//!
//! The Call:
kcluster__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, nclusters_adjusted, local_matrix->nrows, local_matrix->ncols, local_matrix->matrix, /*mask=*/NULL, local_matrix->weight, isTo_transposeMatrix, npass, method, 
			       /*clusterid=*/obj_local_result.vertex_clusterId,
			       /*error=*/&(obj_local_result.sumOf_errors), 
			       /*ifound=*/&(obj_local_result.cntTotal_clusterAnswerFound), 
			       /*isTo_use_continousSTripsOf_memor=*/true, /*isTo_invertMatrix_transposed=*/true, /*isTo_useImplictMask=*/false,
			       obj_local_result.matrix2d_vertexTo_clusterId,
			       obj_rand__init__local
			       );
//! *****************************************************************************************************************************
//!
//! Update the result:
//! De-allocate the lcall radnom-object:
if(obj_rand__init__local) {
  free__s_kt_randomGenerator_vector_t(&obj_rand__init__local__);
 }

//!
//! Iterate through the vertices and udpate the membership:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
// start: -------- type of mapping-table:
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
const uint cnt_local = arrOf_result_vertexMembers_size;
const uint *mapOf_local = arrOf_result_vertexMembers;
#elif (__disjointConfig__useComplexInference__rowsDiffersFrom == 0)
const uint cnt_local = arrOf_result_vertexMembers_size;
const uint *mapOf_local = arrOf_result_vertexMembers;
#else
const uint cnt_local = arrOf_result_row_size;
const uint *mapOf_local = arrOf_result_row;
#endif
// finish: -------- type of mapping-table:
if(cnt_local > 0) {
  mergeResult__s_kt_clusterAlg_fixed_resultObject_t(obj_result, &obj_local_result, mapOf_local);
 }
#else //! then we 'make' a direct-copy of the object:
mergeResult__s_kt_clusterAlg_fixed_resultObject_t(obj_result, &obj_local_result, /*arrOf_result_vertexMembers=*/NULL);
#endif
//!
//! De-allcoate:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
// start: -------- type of mapping-table:
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
if(arrOf_result_vertexMembers) {
  free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
 }	
#elif (__disjointConfig__useComplexInference__rowsDiffersFrom == 0)
if(arrOf_result_vertexMembers) {
  free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
 }	
#else
if(arrOf_result_row) {
  free_1d_list_uint(&arrOf_result_row); arrOf_result_row = NULL;
 }	
if(arrOf_result_col) {
  free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;
 }
#endif
// finish: -------- type of mapping-table:
#endif
free__s_kt_clusterAlg_fixed_resultObject_t(&obj_local_result);
