#ifndef kt_clusterAlg_config__ccm_h
#define kt_clusterAlg_config__ccm_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_clusterAlg_config__ccm
   @brief spesifies how and when CCM-based filtering is to be applied (oekseth, 06. feb. 2017). 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
 **/

#include "def_intri.h"
#include "kt_matrix_cmpCluster.h"
#include "kt_matrix_base.h"
#include "kt_matrix.h"
#include "kt_clusterAlg_fixed_resultObject.h"
/**
   @struct s_kt_clusterAlg_config__ccm
   @brief spesifies how and when CCM-based filtering is to be applied (oekseth, 06. feb. 2017). 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
 **/
typedef struct s_kt_clusterAlg_config__ccm {
  t_float ccmToApply__minVertexCount__each__fraction; //! a number between [0, 1]. Represents the minimum number of vertices to 'be in a clsuter' before we apply our CCM-evaluation: used to try/seek reduce the performacnebpenaly assicated wrt. the applciaton of CCM-emtrics.
  uint k_clusterCount__min;
  uint k_clusterCount__max;
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum; //! ie, the 'main-generalized-CCM-enum', such as ["VRC", "Silhouette", "Dunn", etc.] (oekseth, 06. mar. 2017).
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__t ccm_type;
  s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config;
  bool isTo_pply__CCM; //! whcih if set to "false" impleis that the CCMs are Not applied.
  const s_kt_matrix_base_t *matrix_unFiltered;
} s_kt_clusterAlg_config__ccm_t;

//! @return an itniated object of the "s_kt_clusterAlg_config__ccm_t" object (oekseth, 06. feb. 2017).
//!   @param <matrix_unFiltered> is the data-matrix to evlauate/use.
s_kt_clusterAlg_config__ccm_t initAndReturn__s_kt_clusterAlg_config__ccm_t(const s_kt_matrix_base_t *matrix_unFiltered);
//! A shallow copy of a configuraiotn-object and an input-matrix.
//! @reutnr a shallow itnaited object.
s_kt_clusterAlg_config__ccm_t copyObj__doNotUseCCM__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *matrix_unFiltered, const s_kt_matrix_t *matrix_input);
//t_float **matrix, uint nrows); //

/**
   @brief apply the cofnigured CCM-emtric and return the reuslt (eokseth, 06. feb. 2017).
   @param <self> is the cofniguraiton-object to use.
   @param <mapOf_vertexToCentroid1> is the cluster-mappings
   @param <matrix> is the data-matrix to evlauate/use.
   @return the CCM-score: if the CCM-score is Not deifned (eg, due to attirbutes of the cCM in quesiton) we return T_FLOAT_MAX
 **/
t_float apply__CCM__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *self, const uint *mapOf_vertexToCentroid1); //, const s_kt_matrix_t *matrix);

//! @return the CCM-specfic worst-case value (oekseht, 06. mar. 2017)
static t_float get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *self) {
  assert(self);
  return getWorstCase_score__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(self->ccm_enum);
}
/**
   @brief idneties the CCM-score for a "clusterResult" and updates the scalar_ccmBestScore if the CCM-score 'of this call' is better than "prev_ccmScore" (oekseth, 06. mar. 2017).
   @param <self> hold the CCM-spec
   @param <clusterResult> is the cluster-data-set to evaluate
   @param <prev_ccmScore> is the default CCM-score to compare with
   @param <scalar_ccmBestScore> is updated with the 'new' CCM-score if 'true' is reutnred (ie, the the CCM-score is imrpoved).
   @return true if the score is improved.
 **/
bool apply__CCM__updateBestScore__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *self, const s_kt_clusterAlg_fixed_resultObject_t *clusterResult, const t_float prev_ccmScore, t_float *scalar_ccmBestScore);


#endif //! EOF
