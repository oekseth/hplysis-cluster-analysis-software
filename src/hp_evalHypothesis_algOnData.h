#ifndef hp_evalHypothesis_algOnData_h
#define hp_evalHypothesis_algOnData_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_evalHypothesis_algOnData
   @brief provide logics to evaluate different hypothesis for a colleciton of data-sets and algorithm-configuraiotn-options (oekseth, 06. mar. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017). 
**/

#include "hp_clusterFileCollection.h"
#include "hp_exportMatrix.h"
/**
   @struct s_hp_evalHypothesis_algOnData
   @brief desibe parameter-space to explore (oekshe,t 06. mar. 2017).
   @remarks provide a set of configuraiton-options to investigate the parameter-space assicated to the results produced by cluster-algorithms 
   @remarks an example-applicaiotn wrt. 'results from this test' is to write reseharc-aritlces hwere we investgiate different appraoches for (a) type of randomness in 'initial step', (b) type of randomness wrt. the 'itneration-phase', (c) differentce in clsuter-results wrt. 'number of selected vertices used to represent/capture the underlying entwork' ... 
 **/
typedef struct s_hp_evalHypothesis_algOnData {
  // --- 
  uint arg_npass; 
  uint sizeOf__nrows; uint sizeOf__ncols;
  uint cntIterations__iterativeAlgs;
  bool isTo__testHCA_komb__inKmeans;
  bool config__isTo__useDummyDatasetForValidation; //! a aprameter used to reduce the exeuction-tiem when 'rpoking' assert(false) and memory-errors (oekseth, 06. feb. 2017).
  // --- 
  uint setOfAlgsToEvaluate__size;
  const e_hpLysis_clusterAlg_t *setOfAlgsToEvaluate;  
  // --- 
  uint setOfRandomNess_in_size;
  const e_kt_randomGenerator_type_t *setOfRandomNess_in;
  // --- 
  uint setOfRandomNess_out_size;
  const e_kt_randomGenerator_type_t *setOfRandomNess_out;
  // --- 
  uint arrOf_simAlgs_size;
  const e_kt_correlationFunction_t *arrOf_simAlgs;
  // --- 
  uint mapOf_realLife_size;
  const s_hp_clusterFileCollection_t *mapOf_realLife;
  // --- 
  s_hp_exportMatrix_t obj_exportMatrix;  
  // --- 
} s_hp_evalHypothesis_algOnData_t;

//! Initiates the s_hp_evalHypothesis_algOnData_t evlauation-appraoch by 'setting' default values.
//! @return a new-intated object.
s_hp_evalHypothesis_algOnData_t init__s_hp_evalHypothesis_algOnData_t(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size);
//! Use the "self" object to evlauate the simliarites betweeen differnet cluster-alg-configuraitons for the specifred input-data-set.
bool apply__s_hp_evalHypothesis_algOnData_t(const s_hp_evalHypothesis_algOnData_t *self, const char *stringOf_resultDir);

#endif //! EOF
