    t_float cnt_totalOverlaps = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {    
      for(uint col_id = 0; col_id < ncols; col_id++) {
	const t_float score = matrix[row_id][col_id];
	/* const t_float cntInCluster_1 = self->mapOfCluster1[row_id]; */
	/* const t_float cntInCluster_2 = self->mapOfCluster2[col_id]; */
	//! What we expect:
	assert_possibleOverhead(score != T_FLOAT_MAX);    
	cnt_totalOverlaps += (score * score);
      }
    }
    //!
    //! Get the squared-number-of-clusters for: 'cluster 1':
    t_float sumOf_clusterCountSquares_1 = 0;
    for(uint row_id = 0; row_id < self->mapOfCluster1_size; row_id++) {
      const t_float cntInCluster_1 = self->mapOfCluster1[row_id]; 
      assert(cntInCluster_1 != 0);
      assert(cntInCluster_1 != T_FLOAT_MAX);
      sumOf_clusterCountSquares_1 += (cntInCluster_1 * cntInCluster_1);
    }      
    //!
    //! Get the squared-number-of-clusters for: 'cluster 2':
    t_float sumOf_clusterCountSquares_2 = 0;
    for(uint row_id = 0; row_id < self->mapOfCluster2_size; row_id++) {
      const t_float cntInCluster_2 = self->mapOfCluster2[row_id]; 
      assert(cntInCluster_2 != 0);
      assert(cntInCluster_2 != T_FLOAT_MAX);
      sumOf_clusterCountSquares_2 += (cntInCluster_2 * cntInCluster_2);
    }
