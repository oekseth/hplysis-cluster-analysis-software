#include "hp_clusterShapes.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "math_generateDistribution.h" //! which is used for randomness.

//! Intalise the score-assignemnes:
static void __initScoreAssignments(s_hp_clusterShapes_t *self, e_hp_clusterShapes_scoreAssignment_t typeOf_scoreAssignment) {
  assert(self);
  assert(self->matrix.nrows); //! ie, as we expect the latter to have been intalized.
  assert(self->matrix.ncols); //! ie, as we expect the latter to have been intalized.
  if(typeOf_scoreAssignment == e_hp_clusterShapes_scoreAssignment_undef) {
    typeOf_scoreAssignment = e_hp_clusterShapes_scoreAssignment_minInside_maxOutside; //! ie, the default assumpåtioin
  }
  //!
  //! A 'default' cofnigruation:
  assert(self->score_weak != T_FLOAT_MAX);
  setDefaultValueTo__allCells__kt_matrix(&(self->matrix), self->score_weak);    
  self->typeOf_scoreAssignment = typeOf_scoreAssignment;
  //!
  //! Handle speicfic cases:
  if(typeOf_scoreAssignment == e_hp_clusterShapes_scoreAssignment_sameScore) {
    self->score_strong = self->score_weak; //! ie, tehn set the scores 'to teh same value'.
  } else if(typeOf_scoreAssignment == e_hp_clusterShapes_scoreAssignment_minInside_maxOutside) {
    if(self->score_weak == self->score_strong) {
      fprintf(stderr, "!!\t Seems like you have an icnosnstency in your confiuguration, ie, as score_weak=%f == %f=score_strong. If the latter warnign sounds odd then pelase cotnact the devleoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", self->score_weak, self->score_strong, __FUNCTION__, __FILE__, __LINE__);
    }
  } else if(typeOf_scoreAssignment == e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters) {
    if(self->score_weak == self->score_strong) {
      fprintf(stderr, "!!\t Seems like you have an icnosnstency in your confiuguration, ie, as score_weak=%f == %f=score_strong. If the latter warnign sounds odd then pelase cotnact the devleoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", self->score_weak, self->score_strong, __FUNCTION__, __FILE__, __LINE__);
      //! 
      //! Fix the isssue:
      self->score_weak = (10 + 10*self->score_strong); //! ie, where 'weka' in the cotnext of a 'simlairity-matrix' implices that 'high valeus are fartther away'.
    }
  } else {
    fprintf(stderr, "!!\t Add support for the enum=%u. If the latter warnign sounds odd then pelase cotnact the devleoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", typeOf_scoreAssignment, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an ehads-up.
  }
}

//! @return the score to be used describing vertice sin the same clsuter.
static t_float __get_internalScore__verticesInSameCluster(const s_hp_clusterShapes_t *self, const uint nrows, const uint clusterSize_local) {
  t_float scoreSize_local = self->score_strong;    
  if(self->typeOf_scoreAssignment == e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters) {
    assert(clusterSize_local > 0);
    const t_float diff_min_max = (self->score_weak - self->score_strong); //! where we assuem that a 'strong score' implies 'short distance' (hence, a counterintutie cmaprison).
    //const t_float diff_min_max = (self->score_strong - self->score_weak);
    if(diff_min_max <= 0) {
      fprintf(stderr, "!!\t (low-between-cluster-seperation)\t Obseves that difference(max-min)=%.2f (from weak:%.2f and strong:%.2f). While \this could indicate a bug, it may instead be an acceptable perulairty in ytour measuremetns. If latter assumtpion is true, then pelase ingore this warning. Observvation at [%s]:%s:%d\n", diff_min_max, self->score_strong, self->score_weak, __FUNCTION__, __FILE__, __LINE__);
    }
    assert(diff_min_max > 0); //! ie, givent eh 'tyep fof simlairty which we assume'.
    const t_float numerator = (1.0 + (t_float)clusterSize_local);
    assert(numerator > 0);
    //scoreSize_local = self->score_strong + diff_min_max * (2.0*numerator / (t_float)self->matrix.nrows);
    scoreSize_local = self->score_strong + diff_min_max * (1.0*numerator / (t_float)self->matrix.nrows);
    //scoreSize_local = self->score_strong + diff_min_max * (2.0*numerator / (t_float)self->matrix.nrows);
    // printf("scoreSize_local=%f, given %f + %f*(%f / %f), given score-range=[%f, %f], at %s:%d\n", scoreSize_local, self->score_strong, diff_min_max, numerator,  (t_float)self->matrix.nrows, self->score_weak, self->score_strong, __FILE__, __LINE__);
    assert(scoreSize_local >= self->score_strong); //! ie, givent eh 'tyep fof simlairty which we assume'.
    //! 
    //! @return strong + (weak - strong)*(1 + strong) / rows
    return scoreSize_local;
  } /* else { */
  /*   printf("(use-defult-scores)\t scoreSize_local=%f, at %s:%d\n", scoreSize_local,  __FILE__, __LINE__); */
  /* } */
  return scoreSize_local;
}

//! Initates the "s_hp_clusterShapes_t" object and return (oekseth, 06. jan. 2017).
const s_hp_clusterShapes_t setToEmptyAndReturn__s_hp_clusterShapes_t() {
  s_hp_clusterShapes_t self;
  //! 
  const t_float score_weak = 10;     const t_float score_strong = 1; //! where we use a 'linear step'fucntion' between these 'using' the "cnt_randomPermtuationsToUse" proeprty    
  e_hp_clusterShapes_scoreAssignment_t typeOf_scoreAssignment = e_hp_clusterShapes_scoreAssignment_minInside_maxOutside;
  self.score_weak = score_weak;
  self.score_strong = score_strong;
  setTo_empty__s_kt_matrix_t(&(self.matrix));
  self.map_clusterMembers = NULL; self.map_clusterMembers_size = 0;
  self.typeOf_scoreAssignment = typeOf_scoreAssignment;
  self.map_vertexInternalToRealWorld = NULL;
  //! ----
  self.globaRow_id = 0; //! ie, to 'facilate' the construction of muliple cluster-sets 'in the same chunk'


  //!
  //! @return
  return self;
}



//! Initates the "s_hp_clusterShapes_t" object and return (oekseth, 06. jan. 2017).
const s_hp_clusterShapes_t init_andReturn__s_hp_clusterShapes_t(const uint total_nrows, const t_float score_weak, const t_float score_strong, const e_hp_clusterShapes_vertexOrder_t typeOf_vertexOrder, const e_hp_clusterShapes_scoreAssignment_t typeOf_scoreAssignment) {
  s_hp_clusterShapes_t self;
  assert(total_nrows > 0);
  if(total_nrows == 0) {
    fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function, ie, as you did Not spefiy any input-nrows. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  self.score_weak = score_weak;
  self.score_strong = score_strong;
  self.typeOf_scoreAssignment = typeOf_scoreAssignment;
  //setTo_empty__s_kt_matrix_t(&(self.matrix));

  //! Intaite and set the default value:
  init__s_kt_matrix(&(self.matrix), total_nrows, total_nrows, /*isTo_allocateWeightColumns=*/false);

  const uint emptuy_0 = 0;
  self.map_clusterMembers = allocate_1d_list_uint(total_nrows, emptuy_0); self.map_clusterMembers_size = total_nrows;

  //! ----
  self.globaRow_id = 0; //! ie, to 'facilate' the construction of muliple cluster-sets 'in the same chunk'

  self.map_vertexInternalToRealWorld = allocate_1d_list_uint(total_nrows, emptuy_0);
  if( (typeOf_vertexOrder == e_hp_clusterShapes_vertexOrder_linear) || (typeOf_vertexOrder == e_hp_clusterShapes_vertexOrder_undef) ) {
    //! Then we intlaise the values 'as-is':
    for(uint i = 0; i < total_nrows; i++) {self.map_vertexInternalToRealWorld[i] = i;}
  } else if(typeOf_vertexOrder == e_hp_clusterShapes_vertexOrder_reverse) {
    //! Then we intlaise the values 'using reverse':
    for(uint i = 0; i < total_nrows; i++) {
      const uint new_index = (total_nrows - 1) - i;
      assert(new_index < total_nrows);
      self.map_vertexInternalToRealWorld[i] = new_index;}
  } else if(typeOf_vertexOrder == e_hp_clusterShapes_vertexOrder_random) {
    //! First inialtize, ie, to 'ensure' that all numbers are itnaited:
    for(uint i = 0; i < total_nrows; i++) {self.map_vertexInternalToRealWorld[i] = i;}
    //! Then we swap the values through the 'use of a random iniation':
    const float p_prob = 1.0/(t_float)total_nrows;
    for(uint i = 0; i < total_nrows; i++) {
      //const uint old_index = i;
      //const uint old_index = math_generateDistribution__binomial((total_nrows - 1), p_prob);
      const uint new_index = math_generateDistribution__binomial((total_nrows - 1), p_prob);
      //if(old_index != new_index) {
      assert(/*old_index=*/i < total_nrows);
      assert(new_index < total_nrows);
      //! Swap:
      // printf("---\n(beforeUpdat)[%u]->%u --- [%u]->%u, at %s:%d\n", self.map_vertexInternalToRealWorld[/*old_index=*/i], new_index, self.map_vertexInternalToRealWorld[new_index], /*old_index=*/i, __FILE__, __LINE__);
      const uint prev_mapping = self.map_vertexInternalToRealWorld[/*old_index=*/i] ;
      self.map_vertexInternalToRealWorld[/*old_index=*/i] = self.map_vertexInternalToRealWorld[new_index];
      self.map_vertexInternalToRealWorld[new_index] = prev_mapping;
      //self.map_vertexInternalToRealWorld[/*old_index=*/i] = new_index;
	//self.map_vertexInternalToRealWorld[new_index] = /*old_index=*/i;
      // printf("(afterUpdate)[%u]->%u --- [%u]->%u, at %s:%d\n", self.map_vertexInternalToRealWorld[/*old_index=*/i], new_index, self.map_vertexInternalToRealWorld[new_index], /*old_index=*/i, __FILE__, __LINE__);
      //}
    }
#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: conside rsuigna  differnet matrcro 'for this'.
    //!
    //! The validate ththat we 'find' for all vertices, ie, that 'all vertices have a mapping':
    // fprintf(stderr, "---------, at %s:%d\n", __FILE__, __LINE__);
    for(uint vertex = 0; vertex < total_nrows; vertex++) {
      uint cnt_found = 0;
      for(uint i = 0; i < total_nrows; i++) { 
	// printf("[%u] = %u (? %u), at %s:%d\n", i, self.map_vertexInternalToRealWorld[i], vertex, __FILE__, __LINE__);
	cnt_found += (vertex == self.map_vertexInternalToRealWorld[i]);}
      assert(cnt_found != 0); //! ie, as we expect only one mapping
      assert(cnt_found == 1); //! ie, as we expect only one mapping
    }
#endif
  } else {
    fprintf(stderr, "!!\t Add support for the enum=%u. If the latter warnign sounds odd then pelase cotnact the devleoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", typeOf_vertexOrder, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an ehads-up.
  } 

  //! Intalise the score-assignemnes:
  __initScoreAssignments(&self, typeOf_scoreAssignment);

  //!
  //! @return
  return self;
}

//! De-allocates the "s_hp_clusterShapes_t" object.
void free__s_hp_clusterShapes_t(s_hp_clusterShapes_t *self) {
  //!
  //! Dae-allcoate:
  free__s_kt_matrix(&(self->matrix));
  if(self->map_clusterMembers) {
    free_1d_list_uint(&(self->map_clusterMembers));
  }
  if(self->map_vertexInternalToRealWorld) {
    free_1d_list_uint(&(self->map_vertexInternalToRealWorld));
  }
  //! Reset the remaining variables to 'default':
  *self = setToEmptyAndReturn__s_hp_clusterShapes_t();
}


//! @return true if the object is empty (oekseth, 06.09.2020).
bool isEmpty__s_hp_clusterShapes_t(const s_hp_clusterShapes_t *self) {
  assert(self);
  // TODO: cosndider adding extre test-apremtsers
  if(self && (self->matrix.nrows > 0) ) {
    return false;
  }
  return true;
}

//! Assign the vertices to the current cluster:
//! @return the 'next' row-pos to insert at.
static uint __assignClusterMemberships(s_hp_clusterShapes_t *self, const uint cluster_id, const uint cnt_clusterStartOffset, const uint nrows, const uint clusterSize_local, const uint start_pos, uint row_id) {
  uint last_pos = row_id + clusterSize_local;       
  if(last_pos > nrows) {last_pos = nrows;} //! ie, to 'handle any overflows'.
assert(last_pos <= nrows);
  //! Get the score to be used describing vertice sin the same clsuter:
  const t_float scoreSize_local = __get_internalScore__verticesInSameCluster(self, nrows, clusterSize_local);
  //!
  //! Unify the wetices 'in this'
  for(; row_id < last_pos; row_id++) {
    //! Update the clsuter-mberships:
    const uint global_head = self->map_vertexInternalToRealWorld[(self->globaRow_id + row_id)];
    assert(global_head < self->matrix.nrows);
    const uint global_clusterId = cluster_id + cnt_clusterStartOffset;
    // printf("[%u]=%u, at %s:%d\n", global_head, global_clusterId, __FILE__, __LINE__);
    assert(global_head < self->map_clusterMembers_size);
    self->map_clusterMembers[global_head] = global_clusterId;
    //! 
    //! Updat ethe distance-matrix:
    for(uint col_id = start_pos; col_id < last_pos; col_id++) {
      const uint global_tail = self->map_vertexInternalToRealWorld[(self->globaRow_id + col_id)];
      assert(global_tail < self->matrix.ncols);
      set_cell__s_kt_matrix(&(self->matrix), /*head=*/global_head, /*tail=*/global_tail, scoreSize_local);
      //set_cell__s_kt_matrix(&(self->map_clusterMembers), cluster_id, /*row=*/(self->globaRow_id + row_id), /*score=isMember=*/1);
    }
  }
  //! @return
  return row_id;
}

//! @return the number-of-rows in a 'local iteraiton':
#define ___M___getNrowsLocal(linearIncrease_inEachCall, call_id) ({const uint nrows_local = linearIncrease_inEachCall * (call_id + 1); nrows_local;})


//! Identify the linearIncrease_inEachCall
//! @remarks we compute: "(|V| - cntIter*fixedOffset)/(sum-of-indexes)",
//! @return T_FLOAT_MAX if the result is in-cosnsitent.
static t_float __get_linearIncrease_inEachCall(const uint nrows, const uint cnt_seperateCalls, const t_float vertexCount__constantToAdd) {
  assert(nrows > 0);     assert(nrows != UINT_MAX);
  assert(cnt_seperateCalls > 0); assert(cnt_seperateCalls != UINT_MAX);
  assert(cnt_seperateCalls < nrows);
  assert(vertexCount__constantToAdd != T_FLOAT_MAX); //! ie, 'if Not to be used' then set to '0'.
  //! Sum the number of elements:
  t_float linearIncrease_inEachCall = 0;
  t_float sumOf_scores = 0;
  for(uint call_id = 0; call_id < cnt_seperateCalls; call_id++) {
    sumOf_scores += (call_id + 1);
  }
  //assert(cnt_seperateCalls > linearIncrease_inEachCall)
  const t_float b_offset = cnt_seperateCalls*vertexCount__constantToAdd; //! ie, "\sum(b)"
  if(b_offset < 0) {
    fprintf(stderr, "!!\t Seems like there is an incossotnecy wrt. the 'b'-variable=%f, ie, please investigate. Observation at [%s]:%s:%d\n", b_offset, __FUNCTION__, __FILE__, __LINE__);
    return T_FLOAT_MAX;
  }
  //! Note: in [”elow] we compute: "(|V| - cntIter*fixedOffset)/(sum-of-indexes)",
  linearIncrease_inEachCall = (((t_float)nrows - b_offset)/ (t_float)sumOf_scores); //! is the linear icnrease to be 'applied' for each cluster-size: if set to '1' all clusters 'have the same size'.      
  //linearIncrease_inEachCall = ((t_float)sumOf_scores / (t_float)cnt_seperateCalls); //! is the linear icnrease to be 'applied' for each cluster-size: if set to '1' all clusters 'have the same size'.      
/* #ifndef NDEBUG //! Then validate correctness of [ªbove]: */
/*   uint debug_nrows_cnt = 0; */
/*   for(uint call_id = 0; call_id < cnt_seperateCalls; call_id++) { */
/*     const uint nrows_local = ___M___getNrowsLocal(linearIncrease_inEachCall, call_id); //! ie, get, the number-of-rows in a 'local iteraiton'. */
/*     if((call_id+1) < cnt_clusterStartOffset) { */
/*       debug_nrows_cnt += nrows_local; */
/*     } else { */
/*       debug_nrows_cnt =  */
/*     } */
    
/*   } */
/*   printf("debug_nrows_cnt=%u, nrows=%u, at %s:%d\n", debug_nrows_cnt, nrows, __FILE__, __LINE__); */
/*   assert(debug_nrows_cnt == nrows); //! ie, what we expect wrt. consistency. */
/* #endif */
  //!
  //! @return
  return linearIncrease_inEachCall;
}


//! build a sample-matrix with 'defined' seperation between each cluster (oekseth, 06. nov. 2016).
bool buildSampleSet__disjointSquares__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusterStartOffset) {
  const t_float score_weak = self->score_weak; const t_float score_strong = self->score_strong; 
  assert(nrows >= cnt_clusters); 
  assert(self->matrix.nrows);
  assert(self->matrix.ncols);
  assert(self->map_clusterMembers);
  assert(self->map_clusterMembers_size);
  assert(nrows <= self->map_clusterMembers_size);
  assert(nrows <= self->matrix.ncols);
  //! ---- 
  assert(cnt_clusterStartOffset != UINT_MAX);
  assert(cnt_clusters != 0);
  assert(cnt_clusters != UINT_MAX);
  //! ---- 
  assert((self->globaRow_id + nrows) <= self->map_clusterMembers_size);
  assert((self->globaRow_id + nrows) <= self->matrix.ncols);
  assert((self->globaRow_id + nrows) <= self->matrix.ncols);
  
  if((nrows + self->globaRow_id) > self->matrix.nrows) {
    fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function: while the biggest inserted intdex (in this call) may become (%u + %u) you 8in your int-funciton) ahve only intaited for '%u' elements. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", nrows, self->globaRow_id, self->matrix.nrows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  // TODO: consider writign a new fucntion where we ... 'allow' for 'a grudal increase in difference between clusters', eg, to use a "percentReduction*(score_strong - score_weak)/cnt_in_each" to 'decrease the clear-cut-ness wrt. the clsuters'.
  // TODO: consider writign a new fucntion where we ... 
  // TODO: consider writign a new fucntion where we ... 

  uint row_id = 0;
  const uint cnt_in_each = (uint)((t_float)nrows/(t_float)cnt_clusters);
  //const uint nrows_floor = cnt_in_each * cnt_clusters;
  assert(self->map_vertexInternalToRealWorld);
  for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
    const uint start_pos = row_id; assert(start_pos < nrows);
    //printf("start_pos=%u, cluster_id=%u, cnt_clusters=%u, nrows=%u, cnt_in_each=%u, at %s:%d\n", start_pos, cluster_id, cnt_clusters, nrows, cnt_in_each, __FILE__, __LINE__);
    uint clusterSize_local = ((cluster_id +1) != cnt_clusters) ? cnt_in_each : (nrows - row_id); //! ie, if in the last cluster then 'update the remaining set of vertices'.
    if(clusterSize_local == 0) {clusterSize_local = (nrows - row_id);} //! ie, the 'remainder'.
    assert(clusterSize_local > 0);
    const uint debug_row_idBefore = row_id;
    //!
    //! Assign the vertices to the current cluster:
    row_id = __assignClusterMemberships(self, cluster_id, cnt_clusterStartOffset, nrows, clusterSize_local, start_pos, row_id);
    assert(row_id > debug_row_idBefore); //! ie, as we expect [ªbove] have 'resutled' in an 'iteraiotn' wrt the row-ids.
  }
  assert(row_id == nrows); //! ie, as we expect all trwos to have been set
  //! 
  //! Update the count:
  self->globaRow_id += nrows;
  //!
  //! @return:
  return true; //! ie, as we assume the 'operaiton' wa s acucess at this exeuction-point.
}


//! build a sample-matrix with 'a linear increased' seperation between each cluster (oekseth, 06. jan. 2017).
bool buildSampleSet__linearSquareIncrease__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusterStartOffset, const t_float vertexCount__constantToAdd) {
  const t_float score_weak = self->score_weak; const t_float score_strong = self->score_strong; 
  assert(nrows >= cnt_clusters); 
  assert(self->matrix.nrows);
  assert(self->matrix.ncols);
  assert(self->map_clusterMembers);
  assert(self->map_clusterMembers_size);
  assert(nrows <= self->map_clusterMembers_size);
  assert(nrows <= self->matrix.ncols);
  //! ---- 
  assert(cnt_clusterStartOffset != UINT_MAX);
  if(cnt_clusters == 0) {return true;} //! ie, as it then 'is ntohign to do'.
  assert(cnt_clusters != 0);
  assert(cnt_clusters != UINT_MAX);
  //! ---- 
  assert((self->globaRow_id + nrows) <= self->map_clusterMembers_size);
  assert((self->globaRow_id + nrows) <= self->matrix.ncols);
  assert((self->globaRow_id + nrows) <= self->matrix.ncols);
  
  if((nrows + self->globaRow_id) > self->matrix.nrows) {
    fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function: while the biggest inserted intdex (in this call) may become (%u + %u) you 8in your int-funciton) ahve only intaited for '%u' elements. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", nrows, self->globaRow_id, self->matrix.nrows, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return false;
  }
  // TODO: consider writign a new fucntion where we ... 'allow' for 'a grudal increase in difference between clusters', eg, to use a "percentReduction*(score_strong - score_weak)/cnt_in_each" to 'decrease the clear-cut-ness wrt. the clsuters'.
  // TODO: consider writign a new fucntion where we ... 
  // TODO: consider writign a new fucntion where we ... 

  //! ----------------------------------------------------------------------------------------------
  //!
  //! Identify the linearIncrease_inEachCall
  //! Note: in [”elow] we compute: "(|V| - cntIter*fixedOffset)/(sum-of-indexes)",
  t_float linearIncrease_inEachCall =  1;
  if(cnt_clusters < nrows) {
    linearIncrease_inEachCall =  __get_linearIncrease_inEachCall(nrows, /*cnt_seperateCalls=*/cnt_clusters, vertexCount__constantToAdd);
    if( (linearIncrease_inEachCall == 0) || (linearIncrease_inEachCall == T_FLOAT_MAX) ) {
      linearIncrease_inEachCall = 1; //! ie, to simplify the use of 'autoatmed procedures'.
    }
  } /* else { */
  /*   printf("(info)\t seems like you hav eonly specified one iteriaotn (given nrows=%u and cnt_seperateCalls=%u), ie, please vlaidate that this call was itneiotnaly (which it acutally may be, eg, if you are running a benchmark-script). Observation at [%s]:%s:%d\n", nrows, cnt_clusters, __FUNCTION__, __FILE__, __LINE__); */
  /* } */
  /*   fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function: cnt-clusters=%u, linearIncrease_inEachCall=%f, while the biggest inserted intdex (in this call) may become (%u + %u) you (in your int-funciton) ahve only intaited for '%u' elements. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", cnt_clusters, linearIncrease_inEachCall, nrows,  self->globaRow_id, self->matrix.nrows, __FUNCTION__, __FILE__, __LINE__); */
  /*   assert(false); */
  /*   return false; */
  /* } */


  uint row_id = 0;
  //const uint cnt_in_each = (uint)((t_float)nrows/(t_float)cnt_clusters);
  //const uint nrows_floor = cnt_in_each * cnt_clusters;
  assert(self->map_vertexInternalToRealWorld);
  //printf("----------------------, at %s:%d\n", __FILE__, __LINE__);
  for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
    const uint start_pos = row_id; 
    if(start_pos < nrows) {
      const t_float cnt_in_each_beforeB  = /*nrows-local=*/___M___getNrowsLocal(linearIncrease_inEachCall, cluster_id); //! ie, get, the number-of-rows in a 'local iteraiton'.
      assert(cnt_in_each_beforeB != T_FLOAT_MAX);
      const uint cnt_in_each = cnt_in_each_beforeB + vertexCount__constantToAdd;
      uint clusterSize_local = ((cluster_id +1) != (uint)cnt_clusters) ? (uint)cnt_in_each : (nrows - row_id); //! ie, if in the last cluster then 'update the remaining set of vertices'.
      if(clusterSize_local == 0) {clusterSize_local = (nrows - row_id);} //! ie, the 'remainder'.    
      if( (clusterSize_local + row_id) <= nrows) {
	// printf("\t (info)\t cnt_in_each=%u for cluster_id=%u, at %s:%d\n", clusterSize_local, cluster_id, __FILE__, __LINE__);
	//if(clusterSize_local != 0) {
	//assert(cnt_in_each_beforeB != 0);
	//const uint clusterSize_local = ((cluster_id +1) != (uint)cnt_clusters) ? (uint)cnt_in_each : (start_pos - row_id); //! ie, if in the last cluster then 'update the remaining set of vertices'.
	assert(clusterSize_local > 0);
	const uint debug_row_idBefore = row_id;
	//!
	//! Assign the vertices to the current cluster:
	row_id = __assignClusterMemberships(self, cluster_id, cnt_clusterStartOffset, nrows, clusterSize_local, start_pos, row_id);
	assert(row_id > debug_row_idBefore); //! ie, as we expect [ªbove] have 'resutled' in an 'iteraiotn' wrt the row-ids.
      } else {
	; //printf("(info::blank)\t cnt_in_each=%f for cluster_id=%u, given nrows=%u, cnt_clusters=%u, at %s:%d\n", cnt_in_each_beforeB, cluster_id, nrows, cluster_id, __FILE__, __LINE__);
	//assert(false);
      }
    } //! then we asusme the 'reainding vertice's are Not to get 'any clusters'.
  }
  //  printf("assigned nrows=%u, at %s:%d\n", row_id, __FILE__, __LINE__);
  assert(row_id == nrows); //! ie, as we expect all trwos to have been set
  //! 
  //! Update the count:
  self->globaRow_id += nrows;
  //!
  //! @return:
  return true; //! ie, as we assume the 'operaiton' wa s acucess at this exeuction-point.
}


//! Update the "s_hp_clusterShapes_t" object with muliple calls to a specific 'case-construciton-function' (oekseth, 06. jan. 2017). 
bool buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusters_fixedToAdd, const e_hp_clusterShapes_clusterAssignType_t enum_id, const uint cnt_seperateCalls, const bool isTo_startOnZero, const bool isTo_incrementDisjoint, const t_float linearIncrease_inEachCall__cntClusters__) {
  const t_float linearIncrease_inEachCall__cntClusters = (linearIncrease_inEachCall__cntClusters__ != 0) ? linearIncrease_inEachCall__cntClusters__ : 1;
  assert(self);
  assert(enum_id != e_hp_clusterShapes_clusterAssignType_undef);
  assert(nrows >= cnt_clusters); 
  assert(self->matrix.nrows);
  assert(self->matrix.ncols);
  assert(self->map_clusterMembers);
  assert(self->map_clusterMembers_size);
  assert(nrows <= self->map_clusterMembers_size);
  assert(nrows <= self->matrix.ncols);
  //! ---- 
  //assert(cnt_clusterStartOffset != UINT_MAX);
  assert(cnt_clusters != 0);
  assert(cnt_clusters != UINT_MAX);
  assert(cnt_clusters_fixedToAdd != UINT_MAX);
  //! ---- 
  assert((self->globaRow_id + nrows) <= self->map_clusterMembers_size);
  assert((self->globaRow_id + nrows) <= self->matrix.ncols);
  assert((self->globaRow_id + nrows) <= self->matrix.ncols);
  //! ----   
  if((nrows + self->globaRow_id) > self->matrix.nrows) {
    fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function: while the biggest inserted intdex (in this call) may become (%u + %u) you 8in your int-funciton) ahve only intaited for '%u' elements. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", nrows, self->globaRow_id, self->matrix.nrows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(nrows == 0) {
    fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function: while the biggest inserted intdex (in this call) may become (%u + %u) you 8in your int-funciton) ahve only intaited for '%u' elements. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", nrows, self->globaRow_id, self->matrix.nrows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  //! ----------------------------------------------------------------------------------------------
  //!
  //! Identify the linearIncrease_inEachCall
  //! Note: in [”elow] we compute: "(|V| - cntIter*fixedOffset)/(sum-of-indexes)",
  t_float linearIncrease_inEachCall =  1;
  if(cnt_seperateCalls < nrows) {
    linearIncrease_inEachCall = __get_linearIncrease_inEachCall(nrows, cnt_seperateCalls, /*vertexCount__constantToAdd=*/0);
    if( (linearIncrease_inEachCall == 0) || (linearIncrease_inEachCall == T_FLOAT_MAX) ) {
      fprintf(stderr, "!!\t Seems like you have an ivnalidatd 'call' in your function: linearIncrease_inEachCall=%f, while the biggest inserted intdex (in this call) may become (%u + %u) you 8in your int-funciton) ahve only intaited for '%u' elements. In brief pelase udpate your aPI-call. Observaiton at [%s]:%s:%d\n", linearIncrease_inEachCall, nrows, self->globaRow_id, self->matrix.nrows, __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
  } else {
    printf("(info)\t seems like you hav eonly specified one iteriaotn (given nrows=%u and cnt_seperateCalls=%u), ie, please vlaidate that this call was itneiotnaly (which it acutally may be, eg, if you are running a benchmark-script). Observation at [%s]:%s:%d\n", nrows, cnt_seperateCalls, __FUNCTION__, __FILE__, __LINE__);
  }

  //!
  //! Iterate through each 'block', seperately computing clusters:  
  uint cnt_vertices_inserted = 0; uint cnt_clusterStartOffset = 0;
  if(isTo_incrementDisjoint == true) {    //! then we find the max-size in the 'current' clsutering, ie, 'one above current-clsuter-count':
    for(uint i = 0; i < self->map_clusterMembers_size; i++) {
      if(self->map_clusterMembers[i] != UINT_MAX) {
	cnt_clusterStartOffset = macro_max(cnt_clusterStartOffset, self->map_clusterMembers[i]);
      }
    }
    cnt_clusterStartOffset++; //! ie, to 'speicify a new start-pos'.
  }
  for(uint call_id = 0; call_id < cnt_seperateCalls; call_id++) {
    //!
    //! Comptue the fraciton of entites to consturct 'clusters' for:
    uint cnt_clusters_local = cnt_clusters_fixedToAdd + (t_float)(1.0 + (t_float)call_id) * (t_float)cnt_clusters * linearIncrease_inEachCall__cntClusters;
    assert(cnt_clusters_local != 0);
    uint nrows_local = ___M___getNrowsLocal(linearIncrease_inEachCall, call_id); //! ie, get, the number-of-rows in a 'local iteraiton'.
    if(cnt_clusters_local > nrows_local) {cnt_clusters_local = nrows_local;} //! ie, as all vertices 'are then in the same clsuter'
    if((call_id +1) >= cnt_seperateCalls) {
      assert(cnt_vertices_inserted < nrows);
      nrows_local = nrows - cnt_vertices_inserted; //! ie, the 'reamininding set' to vertices.
    }
    //printf("\tcall[%u] |clusters|=%u, |rows|=%u, at %s:%d\n", call_id, cnt_clusters_local, nrows_local, __FILE__, __LINE__); 
    //!
    //! 
    if(nrows_local > 0) {
      if(enum_id == e_hp_clusterShapes_clusterAssignType_disjointSquares) {
	const bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(self, nrows_local, cnt_clusters_local, cnt_clusterStartOffset);
	assert(is_ok); //! ie, what we expect.
      } else if(enum_id == e_hp_clusterShapes_clusterAssignType_linearIncrease ) {
	const bool is_ok = buildSampleSet__linearSquareIncrease__hp_clusterShapes(self, nrows_local, cnt_clusters_local, cnt_clusterStartOffset, /*vertexCount__constantTo=*/0);
	assert(is_ok); //! ie, what we expect.
      } else {
	fprintf(stderr, "!!\t An un-supported call: please add support for enum_id=%u, or request a code-udpate to the senior developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
      }
      if(isTo_incrementDisjoint) {cnt_clusterStartOffset += cnt_clusters_local;}
      cnt_vertices_inserted += nrows_local;
    }
  }
  assert(cnt_vertices_inserted == nrows); //! ie, as we expect all vertices to have been inserted.

  //!
  //! @return:
  return true; //! ie, as we assume the 'operaiton' wa s acucess at this exeuction-point.
}
