{
  for(uint i = 0; i < n1; i++) {
    const uint k = (!map_index) ? i : map_index[i];
    for(uint j = 0; j < ncolumns; j++) {
	if(
#if (__typeOf_maskTo_use == 0)
	   true
#elif (__typeOf_maskTo_use == 1)
	   isOf_interest(data[k][j])
#else 
	   mask[k][j]
#endif
	   ) {
	  arr_cdata[j] += data[k][j];
	  arr_count[j] += (t_float)1; //! ie, increment.
	}
    }
  }
}


#undef __typeOf_maskTo_use
