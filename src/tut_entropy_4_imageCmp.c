#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#define __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#define tut4_notUseEntropyMetricsDirectly 1
#include "tut_entropy_3_rankAcrossFiles.c" //! ie, as we use API from the latter.
#undef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif
#include "hp_clusterShapes.h"
#include "kt_metric_aux.h" //! eg, sued to fethc the stirng-repsetnaiton of the corr-metrics.


#undef tut4_globalConfig_supportCostHeavyMatrixCmp
//#define tut4_globalConfig_supportCostHeavyMatrixCmp

// static const uint tut4_cnt_sim_metric = e_kt_correlationFunction_undef;
static const uint tut4_cnt_sim_metric = e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef;
// (e_kt_correlationFunction_t)sim_id
#define tut4_getMetricIdFromIndex(ind) ({s_kt_correlationMetric_t obj = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t((e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t)ind); obj.metric_id;})
static const uint tut4_cnt_sim = (e_kt_categoryOf_correaltionPreStep_none+1) * tut4_cnt_sim_metric;
// static const uint tut4_cnt_sim = (e_kt_categoryOf_correaltionPreStep_none+1) * tut4_cnt_sim_metric;

struct tut_mEntropy4 {
  const char *str; int alg_id; uint niter; bool exploreAllSimMetrics;
};
#if true
//! Note: "tut_ccm_17_ccmAlgDataSyn.c" is among others used as tempalte for below:
const uint tut4_alg_size = 6;
struct tut_mEntropy4 tut4_alg[tut4_alg_size] = {
  //    e_alg_dbScan_brute_linkedListSet,
  {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, /*niter=*/100, false},
  {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
  {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
  {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
  {"miniBatch-100", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/100, false},
  {"miniBatch-1000", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/1000, false},
};
#else
//! Note: "tut_ccm_17_ccmAlgDataSyn.c" is among others used as tempalte for below:
const uint tut4_alg_size = 29;
struct tut_mEntropy4 tut4_alg[tut4_alg_size] = {
  //    e_alg_dbScan_brute_linkedListSet,
  {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, /*niter=*/100, false},
  {"disjoint-DBSCAN-direct", e_hpLysis_clusterAlg_disjoint_DBSCAN, /*niter=*/100, false},
  {"disjoint-hpCluster-direct-mclPost", e_hpLysis_clusterAlg_disjoint_mclPostStrategy, /*niter=*/100, false},
  {"disjoint-DBSCAN-dynamic", e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, /*niter=*/100, false},
  {"disjoint-hpCluster-dynamic", e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, /*niter=*/100, false},
  {"disjoint-hpCluster-dynamic-bestSim", e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, /*niter=*/100, true},
  {"disjoint-KD-hpCluster-direct", e_hpLysis_clusterAlg_disjoint_kdTree, /*niter=*/100, false},
  {"disjoint-KD-hpCluster-dynamic", e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*niter=*/100, false},
  {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
  {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
  {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
  {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
  {"k-means-500", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/500, false},
  {"k-means-500", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/500, false},        
  {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
  {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
  {"k-rank-100", e_hpLysis_clusterAlg_kCluster__rank, /*niter=*/100, false},
  {"k-rank-100", e_hpLysis_clusterAlg_kCluster__rank, /*niter=*/100, false},
  {"k-medoid-100", e_hpLysis_clusterAlg_kCluster__medoid, /*niter=*/100, false},
  {"k-medoid-100", e_hpLysis_clusterAlg_kCluster__medoid, /*niter=*/100, false},
  {"miniBatch-10", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/10, false},
  {"miniBatch-10", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/10, false},
  {"miniBatch-100", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/100, false},
  {"miniBatch-1000", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/1000, false},
  {"HCA-single", e_hpLysis_clusterAlg_HCA_single, /*niter=*/100, false},
  {"HCA-centroid", e_hpLysis_clusterAlg_HCA_centroid, /*niter=*/100, false},
  {"HCA-max",  e_hpLysis_clusterAlg_HCA_max,/*niter=*/100, false},
  {"HCA-average", e_hpLysis_clusterAlg_HCA_average, /*niter=*/100, false},
  {"HCA-Kruskal",  e_hpLysis_clusterAlg_kruskal_hca, /*niter=*/100, false},
  // {"HCA-Kruskal-dynamic", e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, /*niter=*/100, false},
  //!    {"disjoint-hpCluster-direct",  /*niter=*/100, false},
  //!    {, false},   
};
#endif


//static const uint tut4_alg_size = 0; // FIXME: ... 
//static e_hpLysis_clusterAlg_t tut4_alg[tut4_alg_size] = {}   // FIXME: ... 


static s_kt_list_1d_uint_t _computeClusters(s_kt_matrix_t &mat_corr, struct tut_mEntropy4 obj_alg) {
  const e_hpLysis_clusterAlg_t alg_enum = (e_hpLysis_clusterAlg_t)obj_alg.alg_id;
  // fprintf(stderr, "\t Compute for algorithm:\"%s\", at %s:%d\n", obj_alg.str, __FILE__, __LINE__);
  assert(alg_enum != e_hpLysis_clusterAlg_undef); //! was this would indicate an error.
  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
  // time_result = (t_float)end_time_measurement(/*msg=*/map_algList[alg_id].str, FLT_MAX);	      
  //! 
  //! Apply logics:
  if(false) {
  //if(onlyComputeFor_fixedInputMatrix) {
    if(true) {
      obj_hp.config.corrMetric_prior_use = false;  //! ie, use data-as-is.
      obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //isAn__adjcencyMatrix;
    } else {
      obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //isAn__adjcencyMatrix;
      obj_hp.config.corrMetric_prior.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;		  
      obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = false;
    }
    uint nrows = mat_corr.nrows;
    uint ncols = nrows;
    if(true) { //! then we explroe other alternatives:
      obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //isAn__adjcencyMatrix;
      obj_hp.config.corrMetric_prior_use = true;  
      ncols = 3;
      //		  ncols = 10;
      //		  ncols = 24;
    }
    //!
    s_kt_matrix_base_t mat_sample = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
    s_kt_matrix_t mat_shal = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_sample);
    //! 
    // printf("\t\t Start: compute-cluster, at %s:%d\n", __FILE__, __LINE__);		
    // time_result = 0; start_time_measurement();
    const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_enum, &(mat_shal), /*nclusters=*/UINT_MAX, /*npass=*/obj_alg.niter);
    //! Stop measurements: 
    //const t_float time_result_local = (t_float)end_time_measurement(/*msg=*/obj_alg.str, FLT_MAX);	      
    //time_result = time_result_local; //! ie, updat ethe time-emasurement.
    // printf("\t\t OK:    compute-cluster, at %s:%d\n", __FILE__, __LINE__);
    assert(is_ok);
    free__s_kt_matrix_base_t(&mat_sample);	       
    /*
    if(alg_id != 0) {
      printf("\t\t Diff: %f\n", time_result / time_alg_first);
    } else {
      time_alg_first = time_result;
    }
    printf("--------------------\n");
    */
    //mat_resultTime.matrix[dim_index][alg_id] = time_result;
  } else {
    //!
    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //isAn__adjcencyMatrix;
    // obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = ccm_enum;
    obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
    // obj_hp.config.kdConfig.enum_id = self.enum_kdTree; ////! ie, the defualt enum-kd-type
    if(false) {
      //! Note: below invovles additional default steps wrt. simliarty-metrics, hence the results may differ.
      const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
      obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
      obj_hp.config.corrMetric_prior_use = true;
    }
    //! 
    // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
    const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_enum, &mat_corr, /*nclusters=*/UINT_MAX, /*npass=*/obj_alg.niter);
    assert(is_ok);
  }
  s_kt_list_1d_uint_t clusters = getClusters__hpLysis_api(&obj_hp);
  free__s_hpLysis_api_t(&obj_hp);
  return clusters;
}

/**
   @brief describe different 'matrix-to-scalar' conversions.
 **/
typedef enum e_Metro_baseType {
  e_Metro_baseType_entropyCols_entropyRows, //! construct [entropy x entropy] rows.
  e_Metro_baseType_entropyCols_corrRows,    //! construct [entropy x simMetric] rows.
  e_Metro_baseType_corrCols_entropyRows,    //! construct [simMetric x entropy] rows.
  //! 
  e_Metro_baseType_matrixMatrix_corr,  
#ifdef tut4_globalConfig_supportCostHeavyMatrixCmp
  e_Metro_baseType_matrixClusters_corr,  
  e_Metro_baseType_ClustersClusters_corr,
#endif //! #ifdef tut4_globalConfig_supportCostHeavyMatrixCmp
  //!
  e_Metro_baseType_undef
} e_Metro_baseType_t;

typedef struct s_Mentro { //! ie, "struct Matrix enoptry (s_Mentro)".
  e_Metro_baseType_t enum_matrixToScalar; //! ie, define how a matrix of valeus is to be 'turned inot a scalar number'.
  s_tut_entropy3_t *entr_self;
} s_Mentro_t;

static s_Mentro_t init__s_Mentro_t(e_Metro_baseType_t enum_matrixToScalar, s_tut_entropy3_t *entr_self) {
  s_Mentro_t self;
  self.enum_matrixToScalar = enum_matrixToScalar;
  self.entr_self = entr_self;
  //!
  return self;
}

// FIXME: use tut_disjointCluster_4_differentAlgDifferentSyntheticDataSets.c ... when exploring differnet lcsuter-algs.




static s_kt_list_1d_float_t _mergeColumns(s_Mentro_t &self, s_kt_matrix_t &mat_result, s_kt_matrix_t &mat_data, const uint cnt_metrics, s_kt_list_1d_float_t *referenceData, s_kt_matrix_t *data_reference) {
  s_kt_list_1d_float_t arr_scalar = setToEmpty__s_kt_list_1d_float_t();
  const bool isTo_computeCorr = (mat_data.nrows > 0);
  if(isTo_computeCorr == false) {assert(mat_result.nrows > 0);   assert(mat_result.ncols > 0);} //! ie, what we expect.
  else {
    //! Step(): Initiate result-data: 
    arr_scalar = init__s_kt_list_1d_float_t(cnt_metrics);
  }


  //for(uint i = 0; i < mat_result.ncols; i++) {
  if(self.enum_matrixToScalar == e_Metro_baseType_entropyCols_entropyRows)   {
    const uint cnt_metrics = e_kt_entropy_genericType_undef * tut4_cnt_sim;
    if(isTo_computeCorr == false) {
      ; // assert(cnt_metrics == mat_result.ncols);
    }
    //!
    //! Iterate through the combinations: 
    uint i = 0;
    for(uint in = 0; in < e_kt_entropy_genericType_undef; in++) {
      for(uint out = 0; out < e_kt_entropy_genericType_undef; out++) {
	// FIXME: add new for-loop wrt. post-scaling
	char str_buffer[3000];
	if(isTo_computeCorr == false) {
	  assert(i < mat_result.ncols);
	  const char *str1 = get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)in);
	  const char *str2 = get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)out);
	  sprintf(str_buffer, "%s--%s",
		  str1, str2
		  );
	  //! Add the string:
	  //!
	  //!
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, str_buffer, /*addFor_column=*/true);
	} else {
	  assert(i < arr_scalar.list_size); //! ie, what we expect.
	  //!
	  //! Step(): compute entropy for each row. For each comptued score: add entropy-score to a uint-list.
	  s_kt_list_1d_uint_t list_count = init__s_kt_list_1d_uint_t(mat_data.nrows);
	  for(uint row_id = 0; row_id < mat_data.nrows; row_id++){ 
	    s_kt_list_1d_uint_t list_count_r = init__s_kt_list_1d_uint_t(mat_data.ncols);	  
	    for(uint col_id = 0; col_id < mat_data.ncols; col_id++) {
	      const t_float s = mat_data.matrix[row_id][col_id];
	      if(isOf_interest(s)) {
		list_count_r.list[col_id] = (uint)s;
	      }
	    }
	    t_float result = 0;
	    uint max_theoreticCountSize = 0;
	    for(uint k = 0; k < list_count_r.list_size; k++) {
	      max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count_r.list[k]);
	    }
	    if(max_theoreticCountSize > 0) {
	      max_theoreticCountSize++;
	      compute_entropy__list__hp_entropy(&list_count_r, max_theoreticCountSize, e_kt_entropy_genericType_ML_Shannon, /*isTo_applyPostScaling=*/false, &result);	      
	      if(isOf_interest(result)) {
		if(result < 0) {result *= -1;}
		push__s_kt_list_1d_uint_t(&list_count, (uint)result);
	      }
	    }
	  }
	  //!
	  //! Step(): compute entropy for the 'sum of rows':
	  uint max_theoreticCountSize = 0;
	  list_count.list_size = list_count.current_pos; //! ie, adjsut to ease the compelxity of our API.
	  for(uint row_id = 0; row_id < list_count.list_size; row_id++) {
	    max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count.list[row_id]);
	  }	  
	  if(max_theoreticCountSize > 0) {
	    max_theoreticCountSize++;
	    t_float result = 0;
	    compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, e_kt_entropy_genericType_ML_Shannon, /*isTo_applyPostScaling=*/false, &result);
	    // FIXME: add something.
	    assert(i < arr_scalar.list_size); //! ie, what we expect.
	    arr_scalar.list[i] = result; //! ie, update.
	  }
	  //!
	  //! De-allocate: 
	  free__s_kt_list_1d_uint_t(&list_count);
	}
	//!
	i++;
	// return arr_scalar; // FIXME: remove.
      }
    } //! ie, end of 'merge(rows) for-loop' else 
  } else if( (self.enum_matrixToScalar == e_Metro_baseType_entropyCols_corrRows) || (self.enum_matrixToScalar == e_Metro_baseType_corrCols_entropyRows) ) {
    const uint cnt_metrics = e_kt_entropy_genericType_undef * tut4_cnt_sim;
    //const uint cnt_metrics = e_kt_entropy_genericType_undef * e_kt_entropy_genericType_undef * tut4_cnt_sim;
    if(isTo_computeCorr == false) {
      assert(cnt_metrics == mat_result.ncols);
    } else {
      assert(arr_scalar.list_size == cnt_metrics);
    }
    //!
    //! Iterate through the combinations: 
    uint i = 0;
    for(uint in = 0; in < e_kt_entropy_genericType_undef; in++) {      
      for(uint sim_id = 0; sim_id < tut4_cnt_sim_metric; sim_id++) {
	//for(uint sim_id = 1; sim_id <= tut4_cnt_sim_metric; sim_id++) {
	if(false == isTo_use_directScore__e_kt_correlationFunction(tut4_getMetricIdFromIndex(sim_id))) {
	  e_kt_correlationFunction_t sim_metric = tut4_getMetricIdFromIndex(sim_id);
	  for(uint post_index = 0; post_index <= e_kt_categoryOf_correaltionPreStep_none; post_index++) {
	    //! Set the corrleation-metric:
	    s_kt_correlationMetric_t obj_corr;   init__s_kt_correlationMetric_t(&obj_corr, tut4_getMetricIdFromIndex(sim_id), (e_kt_categoryOf_correaltionPreStep_t)post_index);
	    // for(uint out = 0; out < e_kt_entropy_genericType_undef; out++) {
	    if(isTo_computeCorr == false) {
	      char str_buffer[3000];
	      assert(i < mat_result.ncols);
	      sprintf(str_buffer, "%s--%s.%s",
		      get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)in),
		      get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(obj_corr.metric_id),
		      get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(obj_corr.typeOf_correlationPreStep)
		      );
	      //get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)out));
	      //! Add the string:
	      //!
	      //!
	      set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, str_buffer, /*addFor_column=*/true);
	    } else {
	      //printf("(tut-entr-4)\t %u < %u\n", i, cnt_metrics);
	      assert(i < cnt_metrics); //! ie, what we expect.

	      // FIXME: consider adding an ew for-lopo ... where we 'transpose' the matrix.
	      //!
	      //!	      
	      if(self.enum_matrixToScalar == e_Metro_baseType_entropyCols_corrRows) {
		// FIXME: ad support for different strategies to get the reference idnex (eg, 'median', 'average', 'kmeans++', ...):
		// assert(referenceData != NULL); //! ie, as we then expect that "s_kt_list_1d_float_t *referenceData" is set.
		// TODO: consider to make use of the "referenceData" vector.
		const uint reference_index = 0;
		//!
		//! Step(): compute the entorpy between each row:
		s_kt_list_1d_uint_t list_count = init__s_kt_list_1d_uint_t(mat_data.nrows);
		for(uint row_id = 0; row_id < mat_data.nrows; row_id++){ 
		  //! Step(): compute the correlation between 'reference-row' versus 'each row':
		  t_float result = T_FLOAT_MAX;
		  bool is_ok = apply__rows_hp_distance(obj_corr, mat_data.matrix[reference_index], mat_data.matrix[row_id], mat_data.ncols, &result);
		  if(is_ok && isOf_interest(result)) {
		    if(result < 0) {result *= -1;}
		    push__s_kt_list_1d_uint_t(&list_count, (uint)result);
		  }
		}
		//!
		//! Step(): compute the entopry (and update the rsult-strucutre):
		uint max_theoreticCountSize = 0;
		list_count.list_size = list_count.current_pos; //! ie, adjsut to ease the compelxity of our API.
		for(uint row_id = 0; row_id < list_count.list_size; row_id++) {
		  max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count.list[row_id]);
		}
		if(max_theoreticCountSize > 0) {
		  max_theoreticCountSize++;
		  t_float result = 0;
		  compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, e_kt_entropy_genericType_ML_Shannon, /*isTo_applyPostScaling=*/false, &result);
		  // FIXME: add something.
		  assert(i < cnt_metrics); //! ie, what we expect.
		  assert(i < arr_scalar.list_size); //! ie, what we expect.
		  arr_scalar.list[i] = result; //! ie, update.
		}
		//!
		//! De-allocate: 
		free__s_kt_list_1d_uint_t(&list_count);		
	      } else if(self.enum_matrixToScalar == e_Metro_baseType_corrCols_entropyRows) {
		//! Step(): compute the correlation between 'reference-row' versus 'each row':
		//! Note: during the correlation we compare 'entropy at previous data' versus 'entropy-scores in this data'.
		s_kt_list_1d_float_t list_score = init__s_kt_list_1d_float_t(mat_data.nrows);
		assert(referenceData);
		if(referenceData->list_size > 0) {
		  //assert(referenceData->list_size == mat_data.ncols); //! ie, as we expect a 1:1 match. <--- FIXME: will this always hold?
		  // assert(referenceData->list_size == list_score.list_size);      //! ie, as we expect a 1:1 match.
		}
		//!
		//! Iterate through the rows:
		for(uint row_id = 0; row_id < mat_data.nrows; row_id++) { 
		  s_kt_list_1d_uint_t list_count_r = init__s_kt_list_1d_uint_t(mat_data.ncols);	  
		  for(uint col_id = 0; col_id < mat_data.ncols; col_id++) {
		    const t_float s = mat_data.matrix[row_id][col_id];
		    if(isOf_interest(s)) {
		      list_count_r.list[col_id] = (uint)s;
		    }
		  }
		  t_float result = 0;
		  uint max_theoreticCountSize = 0;
		  for(uint k = 0; k < list_count_r.list_size; k++) {
		    max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count_r.list[k]);
		  }
		  if(max_theoreticCountSize > 0) {
		    max_theoreticCountSize++;
		    compute_entropy__list__hp_entropy(&list_count_r, max_theoreticCountSize, e_kt_entropy_genericType_ML_Shannon, /*isTo_applyPostScaling=*/false, &result);
		    list_score.list[row_id] = result;
		  }
		}
		//!
		//! Step: Compute correlation between data:
		t_float result = T_FLOAT_MAX;
		if(referenceData && (referenceData->list_size > 0) ) {
		  assert(list_score.list_size > 0);
		  const uint cnt_pts = macro_min(list_score.list_size, referenceData->list_size);
		  //assert(referenceData->list_size == list_score.list_size);		  
		  bool is_ok = apply__rows_hp_distance(obj_corr, referenceData->list, list_score.list, cnt_pts, &result);
		  assert(is_ok);
		} else {
		  bool is_ok = apply__rows_hp_distance(obj_corr, list_score.list, list_score.list, list_score.list_size, &result);
		  assert(is_ok);
		}
		arr_scalar.list[i] = result;
		//!
		//! De-allocate: 
		free__s_kt_list_1d_float_t(&list_score);		
	      } else {assert(false);} //! ie, then add support for this.
	    }
	    //!
	    i++;
	    // return arr_scalar; // FIXME: remove.	    
	  }
	}
      }
    } //! ie, end of 'merge(rows) for-loop'
    //} else if {
    //const uint cnt_metrics = e_kt_entropy_genericType_undef * tut4_cnt_sim;
    // assert(cnt_metrics == mat_result.ncols);
  } else if(self.enum_matrixToScalar == e_Metro_baseType_matrixMatrix_corr)    {
      const uint cnt_metrics = tut4_cnt_sim * e_kt_clusterComparison_undef * 2;
      if(isTo_computeCorr == false) {
	assert(cnt_metrics == mat_result.ncols);
      }
      uint i = 0;
      for(uint in = 0; in < e_kt_clusterComparison_undef; in++) {
	for(uint transp = 0; transp < 2; transp++) {
	  for(uint sim_id = 0; sim_id < tut4_cnt_sim_metric; sim_id++) {
	    //for(uint sim_id = 1; sim_id <= tut4_cnt_sim_metric; sim_id++) {
	    if(false == isTo_use_directScore__e_kt_correlationFunction(tut4_getMetricIdFromIndex(sim_id))) {
	      e_kt_correlationFunction_t sim_metric = tut4_getMetricIdFromIndex(sim_id);
	      for(uint post_index = 0; post_index <= e_kt_categoryOf_correaltionPreStep_none; post_index++) {
		//! Set the corrleation-metric:
		s_kt_correlationMetric_t obj_corr;   init__s_kt_correlationMetric_t(&obj_corr, tut4_getMetricIdFromIndex(sim_id), (e_kt_categoryOf_correaltionPreStep_t)post_index);
		// for(uint out = 0; out < e_kt_entropy_genericType_undef; out++) {
		char str_buffer[3000];
		if(isTo_computeCorr == false) {
		  assert(i < mat_result.ncols);
		  sprintf(str_buffer, "%s_%s-%s--%s.%s",
			  getStringOf__e_kt_clusterComparison_t((e_kt_clusterComparison_t)in),
			  get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)in),
			  (transp) ? "ord" : "dir",
			  get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(obj_corr.metric_id),
			  get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(obj_corr.typeOf_correlationPreStep)
			  );
		  //get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)out));
		  //! Add the string:
		  //!
		  //!
		  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, str_buffer, /*addFor_column=*/true);
		} else {
		  // FIXME: consider using these results (eg, for Euclid) as a reference wrt. new-idneidfied algorithms (ie, as this approach semes like a standard strategy, eg, as observd wrt. RMSE).
		  //!
		  //! Step(): tranpose (ie, if requested):
		  s_kt_matrix_t mat_1 = mat_data;
		  s_kt_matrix_t mat_2 = (data_reference) ? *data_reference : mat_data; //! ie, if not set then compare to 'itself'.
		  if(transp) { //! De-allocate: 
		    mat_1 = initAndReturn_transpos__s_kt_matrix(&mat_data);
		    if(data_reference && (data_reference->nrows > 0) ) {
		      mat_2 = initAndReturn_transpos__s_kt_matrix(data_reference);
		    }
		  }
		  //!
		  //! Step(): Compute correlation-matrix, and then infer score: 
		  t_float result = T_FLOAT_MAX;
		  if(mat_2.ncols > 0) {
		    assert(mat_1.ncols == mat_2.ncols);
		    scalar__getSimliarty__betwenMatrices__kt_distance_cluster((e_kt_clusterComparison_t)in, obj_corr, &mat_1, &mat_2, &result);
		  } else {
		    scalar__getSimliarty__betwenMatrices__kt_distance_cluster((e_kt_clusterComparison_t)in, obj_corr, &mat_1, &mat_1, &result);
		  }
		  //! Step(): Update results: 
		  arr_scalar.list[i] = result;
		  //!
		  if(transp) { //! Step(): De-allocate: 
		    free__s_kt_matrix(&mat_1);
		    if(data_reference && (data_reference->nrows > 0) ) {
		      free__s_kt_matrix(&mat_2);
		    }
		  }
		}
		//!
		i++;
		// return arr_scalar; // FIXME: remove.
	      }
	    }
	  }
	} //! where '2' implies that we evaluates both 'transposed' and 'non-transposed' matrix. For the API the "kt_distance_cluster.h" API is used.
      }  
    // FIXME: add support for below enums:
#ifdef tut4_globalConfig_supportCostHeavyMatrixCmp
  } else if(
	    (self.enum_matrixToScalar == e_Metro_baseType_matrixClusters_corr)
	    || (self.enum_matrixToScalar == e_Metro_baseType_ClustersClusters_corr)
	    ) {
    assert(tut4_alg_size > 0);
    //! Note: use "tut_ccm_11_compareMatrixBased.c" as a template wrt. CCM-eval.
    uint cnt_ccm = 0;
    bool is_matrixCCM = false;
    if(self.enum_matrixToScalar == e_Metro_baseType_matrixClusters_corr) {
      cnt_ccm = (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
      // cnt_metrics = tut4_cnt_sim * cnt_ccm * 2 * tut4_alg_size; //! where '2' is used to explroe the effets of matrix transpoions
      is_matrixCCM = true;
    } else if(self.enum_matrixToScalar == e_Metro_baseType_ClustersClusters_corr) {
      cnt_ccm = (uint)e_kt_matrix_cmpCluster_metric_undef;
      // cnt_metrics = tut4_cnt_sim * cnt_ccm * 2 * tut4_alg_size; //! where '2' is used to explroe the effets of matrix transpoions
    } else {assert(false);} //! ie, then add support for this.
    assert(cnt_ccm > 0);
    //!
    //!
    uint i = 0;
    for(uint transp = 0; transp < 2; transp++) {
      for(uint sim_id = 0; sim_id < tut4_cnt_sim_metric; sim_id++) {
	//for(uint sim_id = 1; sim_id <= tut4_cnt_sim_metric; sim_id++) {
	if(false == isTo_use_directScore__e_kt_correlationFunction(tut4_getMetricIdFromIndex(sim_id))) {
	  e_kt_correlationFunction_t sim_metric = tut4_getMetricIdFromIndex(sim_id);
	  for(uint post_index = 0; post_index <= e_kt_categoryOf_correaltionPreStep_none; post_index++) {
	    //! Set the corrleation-metric:
	    s_kt_correlationMetric_t obj_corr;   init__s_kt_correlationMetric_t(&obj_corr, tut4_getMetricIdFromIndex(sim_id), (e_kt_categoryOf_correaltionPreStep_t)post_index);	    
	    //!
	    s_kt_matrix_t mat_1 = mat_data;
	    s_kt_matrix_t mat_2 = (data_reference) ? *data_reference : mat_data; //! ie, if not set then compare to 'itself'.
	    s_kt_matrix_t mat_corrHypo_1 = setToEmptyAndReturn__s_kt_matrix_t();
	    s_kt_matrix_t mat_corrHypo_2 = setToEmptyAndReturn__s_kt_matrix_t();	    
	    if(isTo_computeCorr == true) {
	      s_kt_matrix_base_t mat_corrHypo_base_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_corrHypo_1);
	      //! Step(): tranpose (ie, if requested):
	      s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
	      s_kt_matrix_base_t m1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data);
	      s_kt_matrix_base_t mat_corrHypo_base_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_corrHypo_2);

	      if(transp) { //! De-allocate:
		// FIXME: valiate correctness opfp the memory-de-allcoation appraoch
		mat_1 = initAndReturn_transpos__s_kt_matrix(&mat_data);
		m1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_1);	      
	      }
	      // free__s_kt_matrix(&mat_data);
	      //! Step(): comptue adjcency-matrix for both data-sets:	      
	      {
		if(data_reference && (data_reference->nrows > 0) ) {
		  // s_kt_matrix_base_t m2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(data_reference);		    
		  if(transp) {
		    mat_2 = initAndReturn_transpos__s_kt_matrix(data_reference);
		  } 
		  s_kt_matrix_base_t m2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_2);		    
		  //! Step(): comptue adjcency-matrix for both data-sets:	      
		  if(is_matrixCCM) {
		    // FIXME: add a seperate enum where we do NOT comptu simlairty betweent he two matrices (ie, to rhater compute similiarty seerpatley for each matrix). 
		    assert(mat_data.nrows == data_reference->nrows); // TODO: cosnide rdorppign this strict ciriter, and then update ... 
		    assert(mat_data.ncols == data_reference->ncols); // TODO: cosnide rdorppign this strict ciriter, and then update ... 
		    //! 
		    bool is_ok = apply__hp_distance(obj_corr, &m1, &m2, &mat_corrHypo_base_1, hp_config);
		    assert(is_ok);
		  }  else { //! then we need both matrixes (ie, to get different clusters.
		    // FIXME: cosnidder to ... provide a amtrix of the clsuter-emmbershisp ... hence reuing exec-time by 2x.
		    //! 
		    bool is_ok = apply__hp_distance(obj_corr, &m1, &m1, &mat_corrHypo_base_1, hp_config);
		    assert(is_ok);
		    //! 
		    is_ok = apply__hp_distance(obj_corr, &m2, &m2, &mat_corrHypo_base_2, hp_config);
		    assert(is_ok);		    		    
		    //! Update: 
		    assert(mat_corrHypo_base_2.nrows > 0); //! ie, as we expect the operation was sucdcessful
		    assert(mat_corrHypo_base_2.ncols > 0); //! ie, as we expect the operation was sucdcessful
		    mat_corrHypo_2.nrows = mat_corrHypo_base_2.nrows;
		    mat_corrHypo_2.ncols = mat_corrHypo_base_2.ncols;
		    mat_corrHypo_2.matrix = mat_corrHypo_base_2.matrix;
		  }
		} else { //! then only the first matrix is computed:
		  //! Step(): comptue adjcency-matrix for both data-sets:	      
		  //! 
		  bool is_ok = apply__hp_distance(obj_corr, &m1, &m1, &mat_corrHypo_base_1, hp_config);
		  assert(is_ok);
		}
	      } 
	      //! Update: 
	      assert(mat_corrHypo_base_1.nrows > 0); //! ie, as we expect the operation was sucdcessful
	      assert(mat_corrHypo_base_1.ncols > 0); //! ie, as we expect the operation was sucdcessful
	      mat_corrHypo_1.nrows = mat_corrHypo_base_1.nrows;
	      mat_corrHypo_1.ncols = mat_corrHypo_base_1.ncols;
	      mat_corrHypo_1.matrix = mat_corrHypo_base_1.matrix;
	    }

	    //!
	    //! Identify distance from matrix to clusters: 
	    for(uint in = 0; in < tut4_alg_size; in++) {
	      for(uint in_ccm = 0; in_ccm < cnt_ccm; in_ccm++) {
		// for(uint out = 0; out < e_kt_entropy_genericType_undef; out++) {
		char str_buffer[3000];
		if(isTo_computeCorr == false) {
		  assert(i < mat_result.ncols);
		  sprintf(str_buffer, "%s_%s_%s--%s.%s",
			  (is_matrixCCM) ? getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)in_ccm) : getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)in_ccm),
			  tut4_alg[in].str,
			  //get_stringOf__short__e_hpLysis_clusterAlg_t((e_hpLysis_clusterAlg_t)tut4_alg[in].alg_id),
			  (transp) ? "ord" : "dir",
			  get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(obj_corr.metric_id),
			  get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(obj_corr.typeOf_correlationPreStep)
			  );
		  //get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)out));
		  //! Add the string:
		  //!
		  //!
		  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, str_buffer, /*addFor_column=*/true);
		}
	      }
	      if(isTo_computeCorr) {
		//! Step(): Compute clusters for the 'current' data-set:
		s_kt_list_1d_uint_t clusters = _computeClusters(mat_corrHypo_1, tut4_alg[in]);		    
		s_kt_list_1d_float obj_result = setToEmpty__s_kt_list_1d_float_t(); // init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
		if(is_matrixCCM) {
		  //! Step(): Apply logics: compute the matrix-based CCMs:
		  s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_corrHypo_1);		    
		  if(clusters.list != NULL) {
		    assert(clusters.list_size > 0);
		    bool is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, clusters.list, &obj_result); //! defined in our "hp_ccm.h"
		    assert(is_ok);
		  }
		  // assert(mat_result.ncols == obj_result.list_size);
		} else {
		  //! Step(): Compute clusters for the 'training' data-set:
		  s_kt_list_1d_uint_t clusters_2 = clusters;
		  if(mat_corrHypo_2.nrows > 0) { //! ie, as we expect it to have been computed.
		    clusters_2 = _computeClusters(mat_corrHypo_2, tut4_alg[in]);
		  } else {
		  }
		  //s_kt_list_1d_float obj_result_2 = setToEmpty__s_kt_list_1d_float_t(); // init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
		  const uint nrows = macro_min(clusters.list_size, clusters_2.list_size);
		  assert(nrows <= mat_corrHypo_1.nrows);
		  if(mat_corrHypo_2.nrows > 0) {
		    assert(nrows <= mat_corrHypo_2.nrows);
		  }
		  if((clusters.list != NULL)  && (clusters_2.list != NULL) ) { 
		    //! Apply: 
		    if(mat_corrHypo_2.nrows > 0) { //! ie, as we expect it to have been computed.
		      bool is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(clusters.list, clusters_2.list, nrows, mat_corrHypo_1.matrix, mat_corrHypo_2.matrix, &obj_result);
		      assert(is_ok);
		    } else {
		      if(mat_corrHypo_1.nrows > 0) {
			bool is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(clusters.list, clusters.list, nrows, mat_corrHypo_1.matrix, mat_corrHypo_1.matrix, &obj_result);
			assert(is_ok);
		      }
		    }
		  }
		  //!
		  //!	De-allocate: 
		  if(mat_corrHypo_2.nrows > 0) {
		    free__s_kt_list_1d_uint_t(&clusters_2);
		  }
		}
		//!
		//! Step(): Update the result-set:
		for(uint k = 0; k < obj_result.list_size; k++)  {
		  assert(i < arr_scalar.list_size);
		  arr_scalar.list[i] = obj_result.list[k];
		}
		// assert(i <= arr_scalar.list_size);
		i += cnt_ccm; //! where this is moved 'outsiode' the above for-loop to avoid incosnsitenices when there are no dientifed lusters (henc,e hwer ehte input-list is empty).
		//!
		//!	De-allocate: 
		free__s_kt_list_1d_uint_t(&clusters);
		free__s_kt_list_1d_float_t(&obj_result);
	      }
	    } //! and then all algorithm-cobmaitons (of specific ointerest) have been evlauaed. 
	    //!
	    if(transp) { //! Step(): De-allocate: 
	      free__s_kt_matrix(&mat_1);
	      if(data_reference && (data_reference->nrows > 0) ) {
		free__s_kt_matrix(&mat_2);
	      }
	    }
	    free__s_kt_matrix(&mat_corrHypo_1);
	    free__s_kt_matrix(&mat_corrHypo_2);
	  }
	  // return arr_scalar; // FIXME: remove.
	}
      }
    }
#endif //! #ifdef tut4_globalConfig_supportCostHeavyMatrixCmp
  } else {assert(false);} //! ie, then add support for this.
  //!
  //!
  return arr_scalar;
}




static void _describeMatrixColumns(s_Mentro_t &self, s_kt_matrix_t &mat_result) {
  assert(mat_result.ncols > 0); //! ie, as we expec thte matirx is itniated.
  s_kt_list_1d_float_t *referenceData = NULL;
  s_kt_matrix_t mat_data = setToEmptyAndReturn__s_kt_matrix_t();
  s_kt_matrix_t *data_reference = NULL;
  s_kt_list_1d_float_t dummy = _mergeColumns(self, mat_result, mat_data, /*cnt-metrics=*/mat_result.ncols, referenceData, data_reference);
  assert(dummy.list_size == 0); //! ie, what we expect

}

static s_kt_list_1d_float_t _computeArrScalar_fromMatrix(s_Mentro_t &self, s_kt_matrix_t &mat_data, const uint cnt_metrics, s_kt_list_1d_float_t *referenceData, s_kt_matrix_t *data_reference) {
  assert(mat_data.ncols > 0); //! ie, as we expec thte matirx is itniated.
  assert(mat_data.nrows > 0);
  assert(cnt_metrics > 0);
  //! 
  //! Step(): Initiate result-data: 
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  // s_kt_list_1d_float_t arr_scalar = init__s_kt_list_1d_float_t(cnt_metrics);
  //! 
  //! Step(): Compute a scalar (from the matrix):  
  s_kt_list_1d_float_t arr_scalar = _mergeColumns(self, mat_result, mat_data, /*cnt-metrics=*/cnt_metrics, referenceData, data_reference);
  //!
  //! Step(): return the data:
  return arr_scalar;
}

static uint _get_cntMetrics(s_Mentro_t &self) {
  uint cnt_metrics = 0;
  //const uint tut4_cnt_sim = e_kt_categoryOf_correaltionPreStep_none * tut4_cnt_sim_metric;
  if(self.enum_matrixToScalar == e_Metro_baseType_entropyCols_entropyRows)   { cnt_metrics = e_kt_entropy_genericType_undef * e_kt_entropy_genericType_undef;}
  //else if(self.enum_matrixToScalar == e_Metro_baseType_entropyCols_corrRows) { cnt_metrics = e_kt_entropy_genericType_undef * e_kt_entropy_genericType_undef * tut4_cnt_sim;}
  else if(self.enum_matrixToScalar == e_Metro_baseType_entropyCols_corrRows) { cnt_metrics = e_kt_entropy_genericType_undef  * tut4_cnt_sim;}  
  else if(self.enum_matrixToScalar == e_Metro_baseType_corrCols_entropyRows) { cnt_metrics = e_kt_entropy_genericType_undef  * tut4_cnt_sim;}
  //else if(self.enum_matrixToScalar == e_Metro_baseType_corrCols_entropyRows) { cnt_metrics = e_kt_entropy_genericType_undef * e_kt_entropy_genericType_undef * tut4_cnt_sim;}
  else if(self.enum_matrixToScalar == e_Metro_baseType_matrixMatrix_corr)    { cnt_metrics = tut4_cnt_sim * e_kt_clusterComparison_undef * 2;}  //! where '2' implies that we evaluates both 'transposed' and 'non-transposed' matrix. For the API the "kt_distance_cluster.h" API is used.
  // FIXME: add support for below enums:
#ifdef tut4_globalConfig_supportCostHeavyMatrixCmp
  else if(self.enum_matrixToScalar == e_Metro_baseType_matrixClusters_corr)  {
    const uint cnt_ccm = (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
    cnt_metrics = cnt_ccm * tut4_cnt_sim * 2 * tut4_alg_size;}  
  else if(self.enum_matrixToScalar == e_Metro_baseType_ClustersClusters_corr){
    const uint cnt_ccm = (uint)e_kt_matrix_cmpCluster_metric_undef;
    cnt_metrics = cnt_ccm * tut4_cnt_sim * 2 * tut4_alg_size;}
#endif //! #ifdef tut4_globalConfig_supportCostHeavyMatrixCmp
  else {assert(false);} //! ie, then add support for this.
  // else if(self.enum_matrixToScalar == ){ cnt_metrics = ;}  
  //} 
  // FIXME: consider ading support for: ... histograms. 
  // FIXME: consider ading support for: ... 
  // FIXME: consider ading support for: ... 
  assert(cnt_metrics > 0);
  return cnt_metrics;
}


#if config_useSyntDataSets == 1
static s_kt_matrix_t _computeEntropy_for_synMatrixImg(s_Mentro_t &self, const uint cnt_features, const uint cnt_clusters_min, const uint cnt_clusters_iter, const uint cnt_clusters_multEachStep) {
//static void _computeEntropy_for_synMatrixImg(s_tut_entropy3_t *self, const uint cnt_features, const uint cnt_clusters_min, const uint cnt_clusters_max) {

/**
   // FIXME: inclue below desceiption in our to-be-submitted paper. 
   @param <self> is the object which hold the overall settings (And result-object to store our 'updates' in).
   @param <nrows> is the [n, n] matrix-diemsinos to use: should be less than "total_nrows" intiated in the "init_andReturn__s_hp_clusterShapes_t(..)" funciton.
   @param <cnt_clusters> is the number of clsuters to evlauate/cmoptue for. Comptued using the equation: "cnt_clusters_fixedToAdd + (t_float)(1.0 + (t_float)call_id) * (t_float)cnt_clusters * linearIncrease_inEachCall__cntClusters". 
   @param <cnt_clusters_fixedToAdd> the 'fixed' cluster-number to 'always add' to equations, ie, the "b" in "y = ax + b". 
   @param <enum_id> is the type of cluster-cases to build for.
   @param <cnt_seperateCalls> is the number of calls to be made to "enum_id"
   @param <isTo_startOnZero> which for muliple calls implies that we 'use biggestInsertedClusterId + 1' as a cluster-start-positio.
   @param <isTo_incrementDisjoint> which if set to true ensures an 'non-overlapping proeprty' for the inserted clsuters (ie, where the latter viable is 'updated' during our comutpaiton).
   @param <linearIncrease_inEachCall__cntClusters> is the factor to increase the "cnt_clusters" with: computed using the formula "(1 + call_id)*cnt_clusters*linearIncrease_inEachCall__cntClusters".
   @return  true upon success
   @remarks use-cases (specific):
   -- "cnt-clusters = 2, isTo-startOnZero = true, isTo-incrementDisjoint = true, enum-id = disjoint-squares, cnt-seperateCalls = 3, linear-increase = 0": a linear increase in the vertex-size, ie, for which the 'number of vertices in each cluster', will increase linearly;
   -- "cnt-clusters = 1, isTo-startOnZero = true, isTo-incrementDisjoint = true, enum-id = disjoint-squares, cnt-seperateCalls = 3, linear-increase = 2": from the latter we 'get' a cluster-configuration where we observe a 'linar increase' in both the cluster-sizes and vertex-size;
   -- "isTo-startOnZero = true, isTo-incrementDisjoint = false,  enum-id = disjoint-squares, cnt-seperateCalls = 3, linear-increase = 0": produce 'overlaps' wrt. the cluster-memberships: in each 'iteration' extend/expand the previous cluster-memberships with a new 'collection'/slot of vertices. 
   -- 
   -- 
   @remarks in this fucniton we make use of the proeprty "[1, 2, 3, .... n] = (n*(n+1))/2", an equaiton defined in Rottman at page 111 (oekseht, 06. jan. 2016).
   @remarks in order to facilaite the 'adjustmen' of the clsuter-size' we use two varialbes, ie, both "cnt_seperateCalls" and "linearIncrease_inEachCall", e, wrt. the use-case  where " nrows[1] > nrows[0] &&  cntClusters[1] > cntClusters[0]". For this 'task' the "linearIncrease_inEachCall__cntClusters" parameter is used, eg, wrt. using a parameter-value " < 1" or " > 1".
   @remaks the idea behidn this function is to describe "muliple cluster-distributions" through the use of a 'fixed clsuter-allcoation-procedre' in cobmaitnion with a 'linear increase in clsuter-vertex-density'. 
 **/
  const t_float score_strong = 1;
  const t_float score_weak = 1000;
  const uint nrows = cnt_features;
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  assert(cnt_clusters_iter > 0);
  const uint cnt_metrics = _get_cntMetrics(self);
  mat_result =  initAndReturn__s_kt_matrix(/*nrows=*/(cnt_clusters_iter), /*ncols=*/(cnt_metrics));

  // FIXME: call this function.
  //! Step(): ITerate through a sytnethci amtrix
  s_kt_list_1d_float_t referenceData = setToEmpty__s_kt_list_1d_float_t();
  s_kt_matrix_t data_reference = setToEmptyAndReturn__s_kt_matrix_t(); //! which is sometimes used to compare data(1) to data(...)
  for(uint cnt_iter = 0; cnt_iter < cnt_clusters_iter; cnt_iter++) {
    const uint cnt_clusters = cnt_clusters_min + (cnt_iter * cnt_clusters_multEachStep); //! wherer "cnt_clusters_multEachStep" is used to avoid only a "+= 1" cluster-size-increase
    //for(uint cnt_clusters = cnt_clusters_min; cnt_clusters < cnt_clusters_max; cnt_clusters++) {
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);  
    //uint cnt_clusters_fixedToAdd = cnt_clusters / 5;
    const uint cnt_clusters_fixedToAdd = 2; /// macro_max(2, cnt_clusters_fixedToAdd);
    e_hp_clusterShapes_clusterAssignType_t enum_id = e_hp_clusterShapes_clusterAssignType_linearIncrease;
    bool is_ok = buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, cnt_clusters_fixedToAdd, enum_id, /*cnt_seperateCalls=*/1, false, true, 2);
  // bool buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusters_fixedToAdd, const e_hp_clusterShapes_clusterAssignType_t enum_id, const uint cnt_seperateCalls, const bool isTo_startOnZero, const bool isTo_incrementDisjoint, const t_float linearIncrease_inEachCall__cntClusters__) {
    //! 
    //! Step(): Compute a scalar (from the matrix):
    if(referenceData.list_size > 0) {
      assert(referenceData.list_size = obj_shape.matrix.ncols);
    }
    s_kt_list_1d_float_t arr_scalar = _computeArrScalar_fromMatrix(self, obj_shape.matrix, cnt_metrics, &referenceData, &data_reference);
    assert(arr_scalar.list_size > 0);
    assert(arr_scalar.list_size == cnt_metrics);
    assert(arr_scalar.list_size == mat_result.ncols);
    assert(cnt_iter < mat_result.nrows);    
    //!
    //! Step(): verbously idneitfy/describe which metric-citeri we use: 
    if(cnt_iter == 0) { //! then describe the metrics:
      _describeMatrixColumns(self, mat_result);
    }
    //! 
    //! Step(): Insert data
    for(uint k = 0; k < arr_scalar.list_size; k++) {
      assert(k < mat_result.ncols);
      mat_result.matrix[cnt_iter][k] = arr_scalar.list[k];
    }
    //! 
    // s_kt_matrix_base_t mat_shallow = getShallow_matrix_base__s_hp_clusterShapes_t(&obj_shape);
    //! Step(): Compute similarty: 
    // _compute_entropy_all(self, list_count, mat_result, /*index_data=*/i);
    //! Step():

    //!
    //! De-allocate:
    if(cnt_iter != 0) {
      free__s_kt_list_1d_float_t(&arr_scalar);
    } else { //! Then we update reference-data, ie, as some metrics are in need of this.
      referenceData = arr_scalar;
      free__s_kt_matrix(&data_reference); //! ie, for 'safety'.
      data_reference = initAndReturn__copy__s_kt_matrix(&(obj_shape.matrix), /*isTo_updateNames=*/false);
    }
    free__s_hp_clusterShapes_t(&obj_shape);
    // free__s_kt_matrix_base_t(&
  }
  free__s_kt_list_1d_float_t(&referenceData);
  free__s_kt_matrix(&data_reference);
  //!
  //! @return the result
  return mat_result;
}
//! ***********************************************************************************
//! ***********************************************************************************
//! ***********************************************************************************
#else //! #if config_useSyntDataSets == 1
static s_kt_matrix_t _computeEntropy_for_synMatrixImg(s_Mentro_t &self, const uint cnt_features, const uint cnt_clusters_min, const uint cnt_clusters_iter, const uint cnt_clusters_multEachStep) {
  const uint nrows = cnt_features;
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  assert(cnt_clusters_iter > 0);
  const uint cnt_metrics = _get_cntMetrics(self);
  mat_result =  initAndReturn__s_kt_matrix(/*nrows=*/(cnt_clusters_iter), /*ncols=*/(cnt_metrics));
  //! Step(): ITerate through a sytnethci amtrix
  s_kt_list_1d_float_t referenceData = setToEmpty__s_kt_list_1d_float_t();
  s_kt_matrix_t data_reference = setToEmptyAndReturn__s_kt_matrix_t(); //! which is sometimes used to compare data(1) to data(...)
  for(uint cnt_iter = 0; cnt_iter < cnt_clusters_iter; cnt_iter++) {
    assert(cnt_iter < tut3_arr_fileNamesInput_size);
    s_kt_matrix_t mat_features = mat_scalarImages[cnt_iter];
    assert(mat_features.nrows > 0);
    assert(mat_features.ncols > 0);    
    s_kt_list_1d_float_t arr_scalar = _computeArrScalar_fromMatrix(self, mat_features, cnt_metrics, &referenceData, &data_reference);
    assert(arr_scalar.list_size > 0);
    assert(arr_scalar.list_size == cnt_metrics);
    assert(arr_scalar.list_size == mat_result.ncols);
    assert(cnt_iter < mat_result.nrows);    
    //!
    //! Step(): verbously idneitfy/describe which metric-citeri we use: 
    if(cnt_iter == 0) { //! then describe the metrics:
      _describeMatrixColumns(self, mat_result);
    }
    //! 
    //! Step(): Insert data
    for(uint k = 0; k < arr_scalar.list_size; k++) {
      assert(k < mat_result.ncols);
      mat_result.matrix[cnt_iter][k] = arr_scalar.list[k];
    }
    //! 
    // s_kt_matrix_base_t mat_shallow = getShallow_matrix_base__s_hp_clusterShapes_t(&obj_shape);
    //! Step(): Compute similarty: 
    // _compute_entropy_all(self, list_count, mat_result, /*index_data=*/i);
    //! Step():

    //!
    //! De-allocate:
    if(cnt_iter != 0) {
      free__s_kt_list_1d_float_t(&arr_scalar);
    } else { //! Then we update reference-data, ie, as some metrics are in need of this.
      referenceData = arr_scalar;
      free__s_kt_matrix(&data_reference); //! ie, for 'safety'.
      data_reference = initAndReturn__copy__s_kt_matrix(&(mat_features), /*isTo_updateNames=*/false);
    }
  }
  free__s_kt_list_1d_float_t(&referenceData);
  free__s_kt_matrix(&data_reference);
  //!
  //! @return the result
  return mat_result;
}  
#endif //! #if config_useSyntDataSets == 1

//! 

typedef enum e_mEtro_rgbMerge {
  /**
     @brief defines how the RGB input vecotrs are to be merged:
   **/
  // FIXME: make use of below: 
  // FIXME: add new/different enums.
  e_mEtro_rgbMerge_onLoad_HSV_hue,
  e_mEtro_rgbMerge_onLoad_HSV_saturation,  
  e_mEtro_rgbMerge_afterCorr_average,
  e_mEtro_rgbMerge_afterCorr_min,
  e_mEtro_rgbMerge_afterCorr_max,      
  //! -------------------
  e_mEtro_rgbMerge_undef
} e_mEtro_rgbMerge_t;


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief 
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2019).
   @remarks describe basic lgoics wrt. our "hp_entropy.h" API.
   @remarks compilation: g++ -I.  -g -O0  -mssse3   -L .  tut_entropy_4_imageCmp.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_Mentro; date; time ./tut_Mentro
**/
int main()
#else
  static void tut_entropy_4_imageCmp()   
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif   
  //! Configure: 
  //const uint cnt_features = 200*1000;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
  //  const uint cnt_features = 20*1000;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
  // const uint cnt_features = 100; //! for only a single cluster--clluster this operaiton took '254 minutes' (when in deug-mode)
  //const uint cnt_features = 200; //! 446m38.899s --- fwer sim-metrics, compialtion=fast   
  const uint cnt_features = 400; //! 
  // const uint cnt_features = 10;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
  //const uint cnt_features = 600;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
  //! Configure: the syntehtic data-set: 
  const uint cnt_clusters_min = 2;
  const uint cnt_clusters_multEachStep = 1;
#if config_useSyntDataSets == 1
  const uint cnt_clusters_iter = 3;
  assert( (cnt_clusters_multEachStep * cnt_clusters_iter) < cnt_features); //! ie, as we otherwise have an inaocnceitey.
#else
  const uint cnt_clusters_iter = tut3_arr_fileNamesInput_size;
#endif
  //!
  //! Step(): init result-strucutres:
  s_tut_entropy_bestMetric_t bestMetric = init_bestMetric();
  // s_kt_matrix_t mat_hypo = _generate_matrix_hypo(self, /*cnt-cases=*/cnt_cases, isTo_appendHypoMatrix_toSampleDataSet); // , enum_normAfterMetricMerge);
  const bool isTo_appendHypoMatrix_toSampleDataSet = false;
  s_kt_correlationMetric_t  obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_undef, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank); // , self->enum_normAfterMetricMerge);    
  e_kt_normType_t enum_normAfterMetricMerge = e_kt_normType_rankRelative_abs; 
  //s_tut_entropy3_t entr_self = init__s_tut_entropy3_t("res_mEntropy", norm_arr[0], obj_metric, &bestMetric, e_tut_entropy3_combineInputData_scalar_undef, enum_normAfterMetricMerge);
  // s_kt_matrix_t mat_hypo = _generate_matrix_hypo(entr_self, /*cnt-cases=*/cnt_clusters_iter, isTo_appendHypoMatrix_toSampleDataSet); // , enum_normAfterMetricMerge);  
  //const uint cnt_clusters_multEachStep = 20;

  //const uint cnt_clusters_iter = 10;
  //!

  // FIXME: add a new/alternative for-loop where we compute a histogram, and then call a permtuation of: "tut_entropy_3_rankAcrossFiles()", using a funciton-reference to describe/idneitfy how the histogram is to be parsed/fetched.
  // ----
  //! Evaluate a subset of dat-anromszaiton-strategies: 
  //{ uint mergeRGB = 0;
  for(uint histo_id = 0; histo_id <= e_tut3_histogram_undef; histo_id++) {
    //! Note: motviation for applying histograms is to try capturing the signature of colro-value-spreads.
    e_tut3_histogram_t enum_histoAfterDataLoad = (e_tut3_histogram_t)histo_id;

#if config_useSyntDataSets == 1
    const uint rgb_conv = 0;
#else
    for(uint rgb_conv = 0; rgb_conv < e_tut3_rgbToScalar_undef; rgb_conv++)
#endif //! #if config_useSyntDataSets == 0
      {
	e_tut3_rgbToScalar_t type_conversion = (e_tut3_rgbToScalar_t)rgb_conv;
	for(uint mergeRGB = 0; mergeRGB < e_mEtro_rgbMerge_undef; mergeRGB++) {
	  //!
	  //! Load data:
#if config_useSyntDataSets == 0
	  load_dataFiles_applyRGB_merge_toMatrix(/*type_conversion=*/type_conversion, enum_histoAfterDataLoad);
#endif //! #if config_useSyntDataSets == 0
	  //! 
	  // FIXME: iterate throiuhg the "e_mEtro_rgbMerge_t" enum.
	  //{ uint norm_id = 0;
	  for(uint norm_id = 0; norm_id < norm_size; norm_id++) {
	    // FIXME: add a new data-normalization-for-loop. Then update our input-data-loading wrt. this.
	    //{ uint baseType =   e_Metro_baseType_ClustersClusters_corr;
	    for(uint baseType = 0; baseType < e_Metro_baseType_undef; baseType++) {
	      //!
	      //! Step(): init cofnigruation-strucutre:
	      printf("(tut-entr-4)\t START: base:%u, norm_id:%u, mergeRGB:%u, rgb_conv:%u \n", baseType, norm_id, mergeRGB, rgb_conv);
	      s_kt_correlationMetric_t  obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_cityblock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank); // , self->enum_normAfterMetricMerge);    
	      e_kt_normType_t enum_normAfterMetricMerge = e_kt_normType_rankRelative_abs; // FIXME: consider setting this dynamic.
	      s_tut_entropy3_t entr_self = init__s_tut_entropy3_t("res_mEntropy", norm_arr[norm_id], obj_metric, &bestMetric, e_tut_entropy3_combineInputData_scalar_undef, enum_normAfterMetricMerge);
	      entr_self.enum_RGB = type_conversion;
	      entr_self.enum_histoAfterDataLoad = enum_histoAfterDataLoad;	      
	      //!
	      //! Step(): Load data, and compute entropy
	      s_Mentro_t mentro = init__s_Mentro_t((e_Metro_baseType_t)baseType, &entr_self);    
	      s_kt_matrix_t mat_result = _computeEntropy_for_synMatrixImg(mentro, cnt_features, cnt_clusters_min, cnt_clusters_iter, cnt_clusters_multEachStep);
	      s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_result);
	      //!
	      //! Step(): 
	      // FIXME: for "entr_self" ensure that the 'group of etropy-matirx' option is NOT explored (ie, as the data-rows does NOT descirbe teh entopry-emtircs in question).
	      //!
	      //! Step(): 
	      // FIXME: merge the RGB matrices
	      // FIXME: compute HSV
	      // FIXME: comptue the ranks/histogram for ...; then insert this into ...:
	      //!
	      //! Step(): 
	      //!
	      s_kt_matrix_t mat_hypo = _generate_matrix_hypo(entr_self, /*cnt-cases=*/cnt_clusters_iter, isTo_appendHypoMatrix_toSampleDataSet); // , enum_normAfterMetricMerge);  
	      //!
	      //! 
	      //! 
	      //! Then apply:
	      // printf("mat_result:[%u, %u], mat_hypo:[%u, %u]\n", mat_result.nrows, mat_result.ncols, mat_hypo.nrows, mat_hypo.ncols);
	      main_tut_entropy_3(entr_self, mat_transp, mat_hypo); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
	      //main_tut_entropy_3(entr_self, mat_result, mat_hypo); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
	      //main_tut_entropy_3(self); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
	      //!
	      //! De-allocate
	      free__s_kt_matrix(&mat_result);
	      free__s_kt_matrix(&mat_transp);
	      free__s_kt_matrix(&mat_hypo);
	      //!
	      //! Step(): 
	      //!
	    }
	  }
#if config_useSyntDataSets == 0 //! then de-allcoate: 
	  for(uint i = 0; i < tut3_arr_fileNamesInput_size; i++) {
	    free__s_kt_matrix(&mat_scalarImages[i]);
	  }
#endif //! #if config_useSyntDataSets == 0
	}
      }
  }
  free_s_tut_entropy_bestMetric_t(bestMetric);  
  //free__s_kt_matrix(&mat_hypo);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}




