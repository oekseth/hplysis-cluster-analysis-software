//#ifndef __macro__setMatrixValueToEmpty
/* #error "!!\t Please udpate your calls specifiying 'this' macro-varialbe'" */
/* #endif */
/* #ifndef __macro__ */
#ifndef __macro__setMatrixValue
if( (min_cnt_rows != T_FLOAT_MAX) && (min_cnt_rows != T_FLOAT_MIN_ABS) ) {
  min_cnt_rows *= 0.01;
 }
if( (max_cnt_rows != T_FLOAT_MAX) && (max_cnt_rows != T_FLOAT_MIN_ABS) ) {
  max_cnt_rows *= 0.01;
 }
#endif

const t_float default_value_float_empty = 0;
t_float *arrOf_input = allocate_1d_list_float(cnt_cols, default_value_float_empty);
t_float *arrOf_tmp_intermediate = allocate_1d_list_float(cnt_cols, default_value_float_empty);

for(uint row_id = 0; row_id < cnt_rows; row_id++) {
  if(local_mapOf_interesting[row_id]) { //! then we inspect valeus wrt. the column:
    const t_float *__restrict__ row = local_matrix[row_id];   
    //!
    //! Get a localy copy of elements which are Not masked:
    uint cnt_elements = 0;
    for(uint col_id = 0; col_id < cnt_cols; col_id++) {if(isOf_interest(row[col_id])) {arrOf_input[cnt_elements++] = row[col_id];}} //! ie, copy only interesting elements.
    if(cnt_elements) {
      //!
      //! Compute the median:
      assert(cnt_elements <= cnt_cols); //! ie, as otherwise would be strange.
      assert(arrOf_input);      assert(arrOf_tmp_intermediate);
      //printf("calls median for list-size=%u, at %s:%d\n", cnt_elements, __FILE__, __LINE__);
      const t_float median = get_median(arrOf_input, arrOf_tmp_intermediate, cnt_elements);
      if(median != 0) {
	//!
	//! Compare wrt. the threshold
#ifdef __macro__setValueToEmpty
	const t_float thresh[2] = {median - median*min_cnt_rows, median + median*max_cnt_rows};	
#endif
	for(uint col_id = 0; col_id < cnt_cols; col_id++) {
	  if(isOf_interest(local_matrix[row_id][col_id])) {
#ifdef __macro__setMatrixValue
	    const t_float value = __macro__getValueFromMatrix(row_id, col_id);
	    __macro__setMatrixValue(row_id, col_id, mathLib_float_abs(median - value));
#else
	    if( (local_matrix[row_id][col_id] <= thresh[0]) || (local_matrix[row_id][col_id] >= thresh[1]) ) {
	      __macro__setValueToEmpty(row_id, col_id);
	    }
#endif
	  }
	}
      }
    } else { //! then we asusme no valus are of interest in the column.
      local_mapOf_interesting[row_id] = false; //! ie, mkae this explict in order to avoid any 'furterh waste of work'.
    }
  }
 }

free_1d_list_float(&arrOf_input);
free_1d_list_float(&arrOf_tmp_intermediate);

//#undef __macro_getValue
#undef __macro__setValueToEmpty
/* #undef __macro__getValueFromMatrix */
/* #undef __macro__setMatrixValue */
