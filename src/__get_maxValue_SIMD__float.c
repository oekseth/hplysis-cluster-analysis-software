{
  assert(nrows);   assert(ncols);  assert(data);
  const uint ncols_maxEffective_size = (ncols > SSE_iterSize) ? ncols - SSE_iterSize : 0;


  SSE_TYPE  vec_max;   type max_value_return = type_defaultValue;
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    uint index2 = 0;
    if(ncols_maxEffective_size > 0) {
      vec_max = SSE_load(&data[index1][index2]); index2 += SSE_iterSize; //! ie, intalise and then 'jump past' the inlisaiton-section:
      for(; index2 < ncols_maxEffective_size; index2 += SSE_iterSize) {
	//! Then udpate the max-count:
	vec_max = SSE_operator(SSE_load(&data[index1][index2]), vec_max);
      }
    }
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] type_operator max_value_return) {max_value_return = data[index1][index2];}
    }
  }
  if(ncols_maxEffective_size > 0) { //! then iterate through the vector-values:
    // FIXME[jc]: may you idneitfy an intristinc function for [”elow]?
    type result[SSE_iterSize]; SSE_store(result, vec_max);    
    for(uint i = 0; i < (uint)SSE_iterSize; i++) {
      if(result[i] type_operator max_value_return) {max_value_return = result[i];}
    }
  }
  //! @return the result:
  return max_value_return;
}
#undef type
#undef type_operator
#undef type_defaultValue
#undef SSE_TYPE
#undef SSE_store
#undef SSE_load
#undef SSE_operator
#undef SSE_iterSize

