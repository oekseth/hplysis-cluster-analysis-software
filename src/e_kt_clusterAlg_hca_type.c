#include "e_kt_clusterAlg_hca_type.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

#ifndef macro_min
#define macro_min(x, y) ((x) < (y) ? (x) : (y))
#endif

//! Investigate if the string equals "single|maximum|average|centroid" and thereafter return an internla char-id used in our procedure for the latter (oekseth, 06 otk. 2016).
//! @return the itnernal char-id corresponding to the enum-enum-option in quesiton.
char get_charConfigurationEnum_fromString__kt_clusterAlg_hca(const char *stringOf_enum) {
  assert(stringOf_enum); assert(strlen(stringOf_enum));
  if( 0 == strncmp(stringOf_enum, "single", macro_min(strlen(stringOf_enum), strlen("single")))) { return 's';}
  if( 0 == strncmp(stringOf_enum, "maximum", macro_min(strlen(stringOf_enum), strlen("maximum")))) { return 'm';}
  if( 0 == strncmp(stringOf_enum, "average", macro_min(strlen(stringOf_enum), strlen("average")))) { return 'a';}
  if( 0 == strncmp(stringOf_enum, "centroid", macro_min(strlen(stringOf_enum), strlen("centroid")))) { return 'c';}

  //! ---------------------------------------------------------------------------

  //!
  //! Warn if the user-configuraiton was not properly supported:
  fprintf(stderr, "!!\t None of the if-clauses matched for input-string=\"%s\", ie, please investigate your call: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_enum, __FUNCTION__, __FILE__, __LINE__);
  return 'm';
}


//! Idneitfies the char-value for an internal enum-id
//! @return the itnernal char-id corresponding to the enum-enum-option in quesiton.
char get_charConfigurationEnum_fromEnum__kt_clusterAlg_hca(const e_kt_clusterAlg_hca_type_t enum_id) {
  if(enum_id == e_kt_clusterAlg_hca_type_single) {return 's';}
  else if(enum_id == e_kt_clusterAlg_hca_type_max) {return 'm';}
  else if(enum_id == e_kt_clusterAlg_hca_type_average) {return 'a';}
  else if(enum_id == e_kt_clusterAlg_hca_type_centroid) {return 'c';}

  //! ---------------------------------------------------------------------------

  //!
  //! Warn if the user-configuraiton was not properly supported:
  fprintf(stderr, "!!\t None of the if-clauses matched for enum-id=\"%u\", ie, please investigate your call: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
  return 'm';
}
//! Idneitfies the char-value for an internal enum-id
//! @return the itnernal char-id corresponding to the enum-enum-option in quesiton.
e_kt_clusterAlg_hca_type_t get_enum_fromChar__kt_clusterAlg_hca(const char method) {
  if(method == 's') {return e_kt_clusterAlg_hca_type_single;}
  else if(method == 'm') {return e_kt_clusterAlg_hca_type_max;}
  else if(method == 'a') {return e_kt_clusterAlg_hca_type_average;}
  else if(method == 'c') {return e_kt_clusterAlg_hca_type_centroid;}


  //! ---------------------------------------------------------------------------

  //!
  //! Warn if the user-configuraiton was not properly supported:
  fprintf(stderr, "!!\t None of the if-clauses matched for enum-char=\"%c\", ie, please investigate your call: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", method, __FUNCTION__, __FILE__, __LINE__);
  return e_kt_clusterAlg_hca_type_max;
}

