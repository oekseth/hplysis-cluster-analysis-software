#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis

/**
   @brief examplify how our "hp_api_fileInputTight.h" may be used for: cluster-analsysis
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks provides an 'extract' of the lgocis examplfied in our "tut_hp_api_fileInputTight_4_dist_1_Euclid.c"
   -- input: in this example we (a) load an 'arbirary' input-file, and (b) 'covnert' latter ot a sparse data-set, and vliadate that 'our appraoch' amnage to rpoduce the correct resutls. 
   -- logic: apply a "k-means": for the "ncluster" use a wild-guess, where 'ncluster=2'  would reflect a 'dicect split' in the data-
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //!
  //! Define an input-file to use in analysis:
  const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 

  const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_kCluster__medoid;
  const uint ncluster = 2; const char *stringOf_ncluster = "2";
  //! Load the TSV-file:
  //! Note: advanced options in data-loading conserns noise-injection, both wrt. 'noise-functions' and 'merging of different data-sets'.
  s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(file_name, initAndReturn__s_kt_matrix_fileReadTuning_t()); //! where latter funciton is deifned in our "kt_matrix.h"
  s_kt_matrix_t mat_sparse_1 = copy__fromDenseToSparse__s_kt_matrix(&mat_input);
  const char *result_file_1 = "data/fromTutExamples/tut_hp_api_fileInputTight_9_kmeans_case1.tsv";
  bool is_ok = export__singleCall__s_kt_matrix_t(&mat_sparse_1, result_file_1, NULL);
  assert(is_ok); //! ie, what is expected.
  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(1): Apply clustering:
  //! Note: for a comprehensive lsit of the clsuter-algorithm "metric" used in below, see our "hpLysis_api.h".
  //! ------
  //! Compute using a "Hierarchical Cluster Algorithm (HCA)":
  //! Note: in below call we implictly use a CCM-based appraoch to 'collect' k-means-clusters for HCA:
  s_kt_matrix_t mat_cluster_hca = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
  assert(mat_input.nrows > 0);
  is_ok = apply__clustAlg__hp_api_fileInputTight(/*metric=*/enum_id, /*nclusters=*/ncluster, &mat_input, /*result=*/&mat_cluster_hca);
  assert(is_ok); //! ie, what is expected.
  // printf("nrows=%u, ncols=%u, at%s:%d\n", mat_cluster_hca.nrows, mat_cluster_hca.ncols, __FILE__, __LINE__);
  assert(mat_cluster_hca.ncols == 1); //! as we expect the HCA to have been 'translated' into a k-means-clustering-result.
  printf("result-matrix has dims=[%u, %u] while inputM=[%u, %u], at %s:%d\n", mat_cluster_hca.nrows, mat_cluster_hca.ncols, mat_input.nrows, mat_input.ncols, __FILE__, __LINE__);
  assert(mat_cluster_hca.nrows == mat_input.nrows); //! as we exepct each 'column' to hold/describe the cluster-id for eahc vertex.
  //! Export cluster-result:
  is_ok = export__singleCall__s_kt_matrix_t(&mat_cluster_hca, /*result-file=*/"tut_hp_9_result_clustAlg_kMeans.tsv", NULL); assert(is_ok); //! ie, what is expected.
  /* //! ------ */
  /* //! Step(b): Compute using a "K-means-clustering": */
  /* //! Note: in below call we implictly use a CCM-based appraoch to 'collect' best nclusters-k-means-clusters for k-means: */
  /* s_kt_matrix_t mat_cluster_kMeans = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate. */
  /* if(false) { */
  /*   is_ok = apply__clustAlg__hp_api_fileInputTight(/\*metric=*\/e_hpLysis_clusterAlg_HCA_average, /\*nclusters=*\/UINT_MAX, &mat_input, /\*result=*\/&mat_cluster_kMeans); */
  /*   assert(is_ok); //! ie, what is expected. */
  /* } else { */
  /*   is_ok = apply__clustAlg__hp_api_fileInputTight(/\*metric=*\/e_hpLysis_clusterAlg_kCluster__AVG, /\*nclusters=*\/UINT_MAX, &mat_input, /\*result=*\/&mat_cluster_kMeans); */
  /*   assert(is_ok); //! ie, what is expected. */
  /* } */
  /* assert(is_ok); //! ie, what is expected. */
  /* assert(mat_cluster_kMeans.ncols == 1); //! as we expect the result to 'store' a k-means-clustering-result. */
  /* assert(mat_cluster_kMeans.nrows == mat_input.nrows); //! as we exepct each 'column' to hold/describe the cluster-id for eahc vertex. */
  /* //! Export cluster-result: */
  /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cluster_kMeans, /\*result-file=*\/"helloWorld_result_clustAlg_ccm_kMeans.tsv", NULL); assert(is_ok); //! ie, what is expected. */


  const char *stringOf_enum = get_stringOf__e_hpLysis_clusterAlg_t(enum_id);
  const char *stringOf_inputFile_1 = result_file_1;
  const char *stringOf_inputFile_2 = NULL;
  const char *stringOf_exportFile = NULL; //! ie, to "stdout".
  const char *stringOf_exportFile__format = NULL;
  // const uint ncluster = 2; const char *stringOf_ncluster = "2";
  const bool inputFile_isSparse = true; const char *stringOf_isSparse = "1";
  //! 
  { //! Case(b): API-logis using strings:
    //! 
    //! Apply logics:
    uint arg_size = 0;
    char *listOf_args__2d[10];
    char *listOf_args = construct__terminalArgString__fromInput__hp_api_fileInputTight(/*debug_config__isToCall=*/true,  
										       stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse, &arg_size, listOf_args__2d);
    //const bool is_ok = readFromFile__exportResult__hp_api_fileInputTight(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse);
    assert(listOf_args); assert(strlen(listOf_args));
    MF__expand__bashInputArg__hp_api_fileInputTight("", "Use-case: clustering w/k-means and sparse-data", listOf_args__2d, arg_size);
    //printf("%s\t #! %s\n", listOf_args, "Use-case: CCM:matrix-based");
    //! ----------------------------------
    //! 
    //! Case(c): Bash-API using strings:
    //! 
    //! Apply logics:
    const bool is_ok = fromTerminal__hp_api_fileInputTight(listOf_args__2d, arg_size);
    assert(is_ok);
    //! De-allocate:
    if(listOf_args) {free_1d_list_char(&listOf_args); listOf_args = NULL;}
  }


  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  /* free__s_kt_matrix(&mat_computedSim); */
  /* free__s_kt_matrix(&mat_cluster_kMeans); */
  free__s_kt_matrix(&mat_cluster_hca);
  

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
