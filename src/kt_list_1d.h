#ifndef kt_list_1d_h
#define kt_list_1d_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_list_1d
   @brief provide a generic list-itnerface to 1d-lists (oekseth, 06. mar. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

//#include "kt_matrix_cmpCluster.h"
//#include "type_2d_float_nonCmp_uint.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_matrix_fileReadTuning.h" //! which is used to facilatie/provuide support for file-reading

//! Note: [below] is sinpred by: "https://stackoverflow.com/questions/26562491/define-a-preprocessor-macro-swapt-x-y"
#define MF__SWAP_int(a, b) { (a) ^= (b); (b) ^= (a); (a) ^= (b); } //! where latter use X-OR logics: only supported for "int", "uint", "char" (or any oter non-complex data-type).
//! A mcro for compled data-tyeps such as "flaot" and "struct{..}":
//#define MF__SWAP_complexType(type_t, a, b) { type_t __swap_temp;  __swap_temp = b;     b = a; a = __swap_temp; }
#define MF__SWAP_complexType(type_t, a, b) { type_t __swap_temp;  __swap_temp = (b);     (b) = (a); (a) = __swap_temp; }

/**
   @struct s_kt_list_1d_float
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_float {
  t_float *list; uint list_size;
  uint current_pos;
} s_kt_list_1d_float_t;
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_float_t init__s_kt_list_1d_float_t(uint list_size);
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_float_t(list_size);
}
//! @return an 'empty verison' of our s_kt_list_1d_float_t object
s_kt_list_1d_float_t setToEmpty__s_kt_list_1d_float_t();
//! @return an 'empty verison' of our s_kt_list_1d_float_t object
static s_kt_list_1d_float_t copy__s_kt_list_1d_float_t(const s_kt_list_1d_float_t *obj) {
  assert(obj);
  s_kt_list_1d_float_t self = init__s_kt_list_1d_float_t(obj->list_size);
  // FIXME: for speed consider suign memcpy inistead.
  for(uint i = 0; i < self.list_size; i++) {
    self.list[i] = obj->list[i];
  }
  return self;
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set);
//! Push the valeu to teh list, and reutnr the inserrted index (oekseth, 06. jun. 2017).
static uint push__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, t_float valueTo_set) {
  assert(self);
  const uint index = self->current_pos; self->current_pos++;
  set_scalar__s_kt_list_1d_float_t(self, index, valueTo_set);
  return index;
}
//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set);
//! De-allcoates the s_kt_list_1d_float_t object.
void free__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self);
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, const t_float valueTo_set);
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self);
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self);
//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_float_t(const s_kt_list_1d_float_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  uint biggest_index_plussOne = 0;
  if(self->list_size != 0) {
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i] != T_FLOAT_MAX) {biggest_index_plussOne = i+1;}
    }
  }
  //printf("float::size=%u/%u, at %s:%d\n", biggest_index_plussOne, self->list_size, __FILE__, __LINE__);
  *scalar_result = biggest_index_plussOne;
  //  *scalar_result = self->list_size;
}



//! ***************************************************************************
//! ***************************************************************************

/**
   @struct s_kt_list_1d_uint
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_uint {
  uint *list; uint list_size;
  uint current_pos; //! where latter is only updated if the the 'pushs(..)' funciton is used.
} s_kt_list_1d_uint_t;
//! Sum items using a dynamically allcoated list as input:
//! @remarks used in combination with SWIG-validation; exxplores the effects of using a 'static' function (rather than placing the funcitoin inside the 'object' file).
static int sumitems__s_kt_list_1d_uint(int *first, int nitems) {
  int i, sum = 0;
  for (i = 0; i < nitems; i++) {
    sum += first[i];
  }
  return sum;
}
//! Itnaites a s_ktType_kvPair_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_kvPair() ({s_ktType_kvPair_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_uint() ({uint rel; rel = UINT_MAX;  rel;}) //! ie, return
#define MF__initVal__s_ktType_uint(v1) ({uint rel; rel = v1;  rel;}) //! ie, return
#define MF__initFromRow__s_ktType_uint(row, row_size) ({assert(row); assert(row_size >= 2); uint rel; rel = (uint)row[0]; rel;}) //! ie, return
//! @return true if the uint is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_uint(rel) ({bool is_empty = ((rel == UINT_MAX) ); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
//! Allocate a new set of uint objects.
#define MF__allocate__s_ktType_uint(size) ({assert(size > 0); uint *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(uint, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
#define MF__allocate__s_ktType_s_kt_list_1d_uint_t(size) ({assert(size > 0); s_kt_list_1d_uint_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_uint objects.
#define MF__free__s_ktType_uint(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return
#define MF__free__s_kt_list_1d_uint_t(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_uint_t init__s_kt_list_1d_uint_t(uint list_size);
static s_kt_list_1d_uint_t setToEmpty__s_kt_list_1d_uint_t() {
  return init__s_kt_list_1d_uint_t(0);
}
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_uint_t(list_size);
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set);
//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set);
//! Push the valeu to teh list, and reutnr the inserrted index (oekseth, 06. jun. 2017).
static uint push__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint valueTo_set) {
  assert(self);
  const uint index = self->current_pos; self->current_pos++;
  set_scalar__s_kt_list_1d_uint_t(self, index, valueTo_set);
  return index;
}
//! Pop the entry, and treturn the value/key (oekseth, 06. jun. 2017).
static uint pop__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self) {
  assert(self);
  const uint index = self->current_pos; 
  if(index > 0) {self->current_pos--; return self->list[index - 1];}
  else {return UINT_MAX;} //! ie, as the value was then Not found. 
}
//! De-allcoates the s_kt_list_1d_uint_t object.
void free__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self);

//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_uint_t(const s_kt_list_1d_uint_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  uint biggest_index_plussOne = 0;
  if(self->list_size != 0) {
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i] != UINT_MAX) {biggest_index_plussOne = i+1;}
    }
  }
  *scalar_result = biggest_index_plussOne;
  //*scalar_result = self->list_size;
}


//! Convert a matrix to the "s_kt_list_1d_uint_t" object.
//! @remakrs we use a macor to avodi a cyclic compiler-dependcy duing linking (eosketh, 06. mar. 2017).
#define MF__floatToUint__matrix__atRow(row_id, mat_local, list_result) ({assert(row_id < mat_local.nrows); assert(mat_local.nrows > 0); assert(list_result.list_size > 0); /*copy: */for(uint i = 0; i < mat_local.ncols; i++) {list_result.list[i] = (uint)mat_local.matrix[row_id][i];}})



//! ***************************************************************************
//! ***************************************************************************

/**
   @struct s_kt_list_1d_uint_pointer
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_uint_node {
  uint key; s_kt_list_1d_uint_node *link_parent;
} s_kt_list_1d_uint_node_t;
//! Set the node:
static s_kt_list_1d_uint_node_t *setNode__s_kt_list_1d_uint_node_t(const uint key, s_kt_list_1d_uint_node_t *link_parent) {
  s_kt_list_1d_uint_node_t *self = (s_kt_list_1d_uint_node_t*)malloc(sizeof(s_kt_list_1d_uint_node_t));
  self->key = key; self->link_parent = link_parent;
  return self;
}
//! De-allcoates the s_kt_list_1d_uint_pointer_t object.
static void free__s_kt_list_1d_uint_node_t(s_kt_list_1d_uint_node_t *self) {
  assert(self);
  if(self->link_parent) {
    assert(self->link_parent != self);
    free__s_kt_list_1d_uint_node_t(self->link_parent); 
  }
  free(self);  
}

/**
   @struct s_kt_list_1d_uint_pointer
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_uint_pointer {
  //s_kt_list_1d_uint_node_t *root;
  s_kt_list_1d_uint_node_t *lastInserted; //! ie, the 'current-pos'.
} s_kt_list_1d_uint_pointer_t;
//! @return an itnainted verison:
static s_kt_list_1d_uint_pointer_t setToEmpty__s_kt_list_1d_uint_pointer_t() {
  s_kt_list_1d_uint_pointer_t self; 
  self.lastInserted = NULL;
  return self;
}
//! Push the valeu to teh list, and reutnr the inserrted index (oekseth, 06. jun. 2017).
static void push__s_kt_list_1d_uint_pointer_t(s_kt_list_1d_uint_pointer_t *self, uint valueTo_set) {
  assert(self);
  s_kt_list_1d_uint_node_t *new_node = setNode__s_kt_list_1d_uint_node_t(valueTo_set, self->lastInserted);
  self->lastInserted = new_node;
  //if(self->root == NULL) {
  /*   assert(self->lastInserted != NULL); */    
}

//! De-allcoates the s_kt_list_1d_uint_pointer_t object.
static void free__s_kt_list_1d_uint_pointer_t(s_kt_list_1d_uint_pointer_t *self) {
  assert(self);
  if(self->lastInserted != NULL) {
    free__s_kt_list_1d_uint_node_t(self->lastInserted); 
    self->lastInserted = NULL;
  }
}


//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

/**
   @struct s_ktType__pairFloat
   @brief maps a pair of vertices to a given score (oesketh, 06. mar. 2017).
 **/
typedef struct s_ktType_pairFloat {
  uint head;
  uint tail;
  t_float score;
} s_ktType_pairFloat_t;

//! Itnaites a s_ktType_pairFloat_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_pairFloat() ({s_ktType_pairFloat_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_pairFloat() ({s_ktType_pairFloat_t rel; rel.head = UINT_MAX; rel.tail = UINT_MAX; rel.score = T_FLOAT_MAX; rel;}) //! ie, return
#define MF__initVal__s_ktType_pairFloat(v1, v2, _score) ({s_ktType_pairFloat_t rel; rel.head = v1; rel.tail = v2;  rel.score = _score; rel;}) //! ie, return
#define MF__initFromRow__s_ktType_pairFloat(row, row_size) ({assert(row); assert(row_size >= 3); s_ktType_pairFloat_t rel; rel.head = (uint)row[0]; rel.tail = (uint)row[1]; rel.score = (t_float)row[2]; rel;}) //! ie, return
  

//! @return true if the s_ktType_pairFloat_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_pairFloat(rel) ({bool is_empty = ((rel.head == UINT_MAX) && (rel.tail == UINT_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_pairFloat(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) && (rel1.score == rel2.score) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_pairFloat_t objects.
#define MF__allocate__s_ktType_pairFloat(size) ({assert(size > 0); s_ktType_pairFloat_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_pairFloat_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_pairFloat objects.
#define MF__free__s_ktType_pairFloat(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_pairFloat
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_pairFloat_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_pairFloat {
  s_ktType_pairFloat_t *list; uint list_size; uint current_pos;
} s_kt_list_1d_pairFloat_t;
//! @return an 'empty verison' of our s_kt_list_1d_pairFloat_t object
s_kt_list_1d_pairFloat_t setToEmpty__s_kt_list_1d_pairFloat_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_pairFloat_t init__s_kt_list_1d_pairFloat_t(uint list_size);
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_pairFloat_t(list_size);
}
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_pairFloat_t initFrom_sparseMatrix__s_kt_list_1d_pairFloat_t(const uint nrows, const uint ncols, t_float **matrix);
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, const char *result_file);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_pairFloat_t initFromFile__s_kt_list_1d_pairFloat_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, s_ktType_pairFloat_t *scalar_result);
//! Get hte results, sorting the latter in a result-object.
//! Note: to simplify our SWIG-wrapper-code we 'trnsalte' teh head-idnex and tial-idnex to floats
static bool get_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, t_float *scalar_head, t_float *scalar_tail, t_float *scalar_score) {
  assert(self); assert(scalar_head); assert(scalar_tail); assert(scalar_score);
  s_ktType_pairFloat_t obj = MF__init__s_ktType_pairFloat();
  const bool is_ok = get_scalar__s_kt_list_1d_pairFloat_t(self, index, &obj);
  assert(is_ok); //! ie, what we expect.
  *scalar_head  = (t_float)obj.head;
  *scalar_tail  = (t_float)obj.tail;
  *scalar_score = (t_float)obj.score;
  //! @return the result:
  return is_ok;
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, s_ktType_pairFloat_t valueTo_set);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, uint index, const uint head, const uint tail, const t_float score);
//! Add the pair to the enw set:
void add_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self, const uint head, const uint tail, const t_float score);
//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_pairFloat_t(const s_kt_list_1d_pairFloat_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  if(self->current_pos != 0) {*scalar_result = self->current_pos;} //! ie, as we then assume that latter is in use.
  else {
    *scalar_result = self->list_size;
  }
}



//! De-allcoates the s_kt_list_1d_pairFloat_t object.
void free__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *self);


//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

/**
   @struct s_ktType__tripleUChar
   @brief maps a triplet of vertieces (eg, RGB values (oesketh, 06. mar. 2019).
 **/
typedef struct s_ktType_tripleUChar {
  uchar head;
  uchar tail;
  uchar score;
} s_ktType_tripleUChar_t;

//! Itnaites a s_ktType_tripleUChar_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_tripleUChar() ({s_ktType_tripleUChar_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_tripleUChar() ({s_ktType_tripleUChar_t rel; rel.head = UCHAR_MAX; rel.tail = UCHAR_MAX; rel.score = UCHAR_MAX; rel;}) //! ie, return
#define MF__initVal__s_ktType_tripleUChar(v1, v2, _score) ({s_ktType_tripleUChar_t rel; rel.head = v1; rel.tail = v2;  rel.score = _score; rel;}) //! ie, return
#define MF__initFromRow__s_ktType_tripleUChar(row, row_size) ({assert(row); assert(row_size >= 3); s_ktType_tripleUChar_t rel; rel.head = (uchar)row[0]; rel.tail = (uchar)row[1]; rel.score = (uchar)row[2]; rel;}) //! ie, return
  

//! @return true if the s_ktType_tripleUChar_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_tripleUChar(rel) ({bool is_empty = ((rel.head == UCHAR_MAX) && (rel.tail == UCHAR_MAX)  && (rel.score == UCHAR_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_tripleUChar(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) && (rel1.score == rel2.score) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_tripleUChar_t objects.
#define MF__allocate__s_ktType_tripleUChar(size) ({assert(size > 0); s_ktType_tripleUChar_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_tripleUChar_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_tripleUChar objects.
#define MF__free__s_ktType_tripleUChar(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_tripleUChar
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_tripleUChar_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_tripleUChar {
  s_ktType_tripleUChar_t *list; uint list_size; uint current_pos;
} s_kt_list_1d_tripleUChar_t;
//! @return an 'empty verison' of our s_kt_list_1d_tripleUChar_t object
s_kt_list_1d_tripleUChar_t setToEmpty__s_kt_list_1d_tripleUChar_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_tripleUChar_t init__s_kt_list_1d_tripleUChar_t(uint list_size);
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_tripleUChar_t(list_size);
}
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleUChar_t initFrom_sparseMatrix__s_kt_list_1d_tripleUChar_t(const uint nrows, const uint ncols, t_float **matrix);
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, const char *result_file);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleUChar_t initFromFile__sizeKnown_s_kt_list_1d_tripleUChar_t(const char *input_file, const uint cnt_rows);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleUChar_t initFromFile__s_kt_list_1d_tripleUChar_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint index, s_ktType_tripleUChar_t *scalar_result);
//! Get hte results, sorting the latter in a result-object.
//! Note: to simplify our SWIG-wrapper-code we 'trnsalte' teh head-idnex and tial-idnex to floats
static bool get_scalar__allVars__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint index, t_float *scalar_head, t_float *scalar_tail, t_float *scalar_score) {
  assert(self); assert(scalar_head); assert(scalar_tail); assert(scalar_score);
  s_ktType_tripleUChar_t obj = MF__init__s_ktType_tripleUChar();
  const bool is_ok = get_scalar__s_kt_list_1d_tripleUChar_t(self, index, &obj);
  assert(is_ok); //! ie, what we expect.
  *scalar_head  = (t_float)obj.head;
  *scalar_tail  = (t_float)obj.tail;
  *scalar_score = (t_float)obj.score;
  //! @return the result:
  return is_ok;
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint index, s_ktType_tripleUChar_t valueTo_set);
#ifndef SWIG // TODO: figure out why SWIG does not pprocuede the below:
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__allVars__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, uint index, const uint head, const uint tail, const t_float score);
//! Add the pair to the enw set:
void add_scalar__allVars__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self, const uint head, const uint tail, const t_float score);
#endif // #ifndef SWIG
//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_tripleUChar_t(const s_kt_list_1d_tripleUChar_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  if(self->current_pos != 0) {*scalar_result = self->current_pos;} //! ie, as we then assume that latter is in use.
  else {
    *scalar_result = self->list_size;
  }
}



//! De-allcoates the s_kt_list_1d_tripleUChar_t object.
void free__s_kt_list_1d_tripleUChar_t(s_kt_list_1d_tripleUChar_t *self);


//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

/**
   @struct s_ktType__tripleT_Float
   @brief maps a triplet of vertieces (eg, RGB values (oesketh, 06. mar. 2019).
 **/
typedef struct s_ktType_tripleT_Float {
  t_float head;
  t_float tail;
  t_float score;
} s_ktType_tripleT_Float_t;

//! Itnaites a s_ktType_tripleT_Float_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_tripleT_Float() ({s_ktType_tripleT_Float_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_tripleT_Float() ({s_ktType_tripleT_Float_t rel; rel.head = T_FLOAT_MAX; rel.tail = T_FLOAT_MAX; rel.score = T_FLOAT_MAX; rel;}) //! ie, return
#define MF__initVal__s_ktType_tripleT_Float(v1, v2, _score) ({s_ktType_tripleT_Float_t rel; rel.head = v1; rel.tail = v2;  rel.score = _score; rel;}) //! ie, return
#define MF__initFromRow__s_ktType_tripleT_Float(row, row_size) ({assert(row); assert(row_size >= 3); s_ktType_tripleT_Float_t rel; rel.head = (t_float)row[0]; rel.tail = (t_float)row[1]; rel.score = (t_float)row[2]; rel;}) //! ie, return
  

//! @return true if the s_ktType_tripleT_Float_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_tripleT_Float(rel) ({bool is_empty = ((rel.head == T_FLOAT_MAX) && (rel.tail == T_FLOAT_MAX)  && (rel.score == T_FLOAT_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_tripleT_Float(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) && (rel1.score == rel2.score) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_tripleT_Float_t objects.
#define MF__allocate__s_ktType_tripleT_Float(size) ({assert(size > 0); s_ktType_tripleT_Float_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_tripleT_Float_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_tripleT_Float objects.
#define MF__free__s_ktType_tripleT_Float(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_tripleT_Float
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_tripleT_Float_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_tripleT_Float {
  s_ktType_tripleT_Float_t *list; uint list_size; uint current_pos;
} s_kt_list_1d_tripleT_Float_t;
//! @return an 'empty verison' of our s_kt_list_1d_tripleT_Float_t object
s_kt_list_1d_tripleT_Float_t setToEmpty__s_kt_list_1d_tripleT_Float_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_tripleT_Float_t init__s_kt_list_1d_tripleT_Float_t(uint list_size);
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_tripleT_Float_t(list_size);
}
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleT_Float_t initFrom_sparseMatrix__s_kt_list_1d_tripleT_Float_t(const uint nrows, const uint ncols, t_float **matrix);
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, const char *result_file);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleT_Float_t initFromFile__sizeKnown_s_kt_list_1d_tripleT_Float_t(const char *input_file, const uint cnt_rows);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleT_Float_t initFromFile__s_kt_list_1d_tripleT_Float_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint index, s_ktType_tripleT_Float_t *scalar_result);
//! Get hte results, sorting the latter in a result-object.
//! Note: to simplify our SWIG-wrapper-code we 'trnsalte' teh head-idnex and tial-idnex to floats
static bool get_scalar__allVars__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint index, t_float *scalar_head, t_float *scalar_tail, t_float *scalar_score) {
  assert(self); assert(scalar_head); assert(scalar_tail); assert(scalar_score);
  s_ktType_tripleT_Float_t obj = MF__init__s_ktType_tripleT_Float();
  const bool is_ok = get_scalar__s_kt_list_1d_tripleT_Float_t(self, index, &obj);
  assert(is_ok); //! ie, what we expect.
  *scalar_head  = (t_float)obj.head;
  *scalar_tail  = (t_float)obj.tail;
  *scalar_score = (t_float)obj.score;
  //! @return the result:
  return is_ok;
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint index, s_ktType_tripleT_Float_t valueTo_set);
#ifndef SWIG // TODO: figure out why SWIG does not pprocuede the below:
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__allVars__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, uint index, const uint head, const uint tail, const t_float score);
//! Add the pair to the enw set:
void add_scalar__allVars__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self, const uint head, const uint tail, const t_float score);
#endif // #ifndef SWIG
//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_tripleT_Float_t(const s_kt_list_1d_tripleT_Float_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  if(self->current_pos != 0) {*scalar_result = self->current_pos;} //! ie, as we then assume that latter is in use.
  else {
    *scalar_result = self->list_size;
  }
}



//! De-allcoates the s_kt_list_1d_tripleT_Float_t object.
void free__s_kt_list_1d_tripleT_Float_t(s_kt_list_1d_tripleT_Float_t *self);


//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

/**
   @struct s_ktType__tripleUint
   @brief maps a triplet of vertieces (eg, RGB values (oesketh, 06. mar. 2019).
 **/
typedef struct s_ktType_tripleUint {
  uint head;
  uint tail;
  uint score;
} s_ktType_tripleUint_t;

//! Itnaites a s_ktType_tripleUint_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_tripleUint() ({s_ktType_tripleUint_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_tripleUint() ({s_ktType_tripleUint_t rel; rel.head = UINT_MAX; rel.tail = UINT_MAX; rel.score = UINT_MAX; rel;}) //! ie, return
#define MF__initVal__s_ktType_tripleUint(v1, v2, _score) ({s_ktType_tripleUint_t rel; rel.head = v1; rel.tail = v2;  rel.score = _score; rel;}) //! ie, return
#define MF__initFromRow__s_ktType_tripleUint(row, row_size) ({assert(row); assert(row_size >= 3); s_ktType_tripleUint_t rel; rel.head = (uint)row[0]; rel.tail = (uint)row[1]; rel.score = (uint)row[2]; rel;}) //! ie, return
  

//! @return true if the s_ktType_tripleUint_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_tripleUint(rel) ({bool is_empty = ((rel.head == UINT_MAX) && (rel.tail == UINT_MAX)  && (rel.score == UINT_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_tripleUint(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) && (rel1.score == rel2.score) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_tripleUint_t objects.
#define MF__allocate__s_ktType_tripleUint(size) ({assert(size > 0); s_ktType_tripleUint_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_tripleUint_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_tripleUint objects.
#define MF__free__s_ktType_tripleUint(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_tripleUint
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_tripleUint_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_tripleUint {
  s_ktType_tripleUint_t *list; uint list_size; uint current_pos;
} s_kt_list_1d_tripleUint_t;
//! @return an 'empty verison' of our s_kt_list_1d_tripleUint_t object
s_kt_list_1d_tripleUint_t setToEmpty__s_kt_list_1d_tripleUint_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_tripleUint_t init__s_kt_list_1d_tripleUint_t(uint list_size);
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_tripleUint_t(list_size);
}
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleUint_t initFrom_sparseMatrix__s_kt_list_1d_tripleUint_t(const uint nrows, const uint ncols, t_float **matrix);
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, const char *result_file);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only two columns.
s_kt_list_1d_tripleUint_t initFromFile__sizeKnown_s_kt_list_1d_tripleUint_t(const char *input_file, const uint cnt_rows);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_tripleUint_t initFromFile__s_kt_list_1d_tripleUint_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint index, s_ktType_tripleUint_t *scalar_result);
//! Get hte results, sorting the latter in a result-object.
//! Note: to simplify our SWIG-wrapper-code we 'trnsalte' teh head-idnex and tial-idnex to floats
static bool get_scalar__allVars__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint index, t_float *scalar_head, t_float *scalar_tail, t_float *scalar_score) {
  assert(self); assert(scalar_head); assert(scalar_tail); assert(scalar_score);
  s_ktType_tripleUint_t obj = MF__init__s_ktType_tripleUint();
  const bool is_ok = get_scalar__s_kt_list_1d_tripleUint_t(self, index, &obj);
  assert(is_ok); //! ie, what we expect.
  *scalar_head  = (t_float)obj.head;
  *scalar_tail  = (t_float)obj.tail;
  *scalar_score = (t_float)obj.score;
  //! @return the result:
  return is_ok;
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint index, s_ktType_tripleUint_t valueTo_set);
#ifndef SWIG // TODO: figure out why SWIG does not pprocuede the below:
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__allVars__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, uint index, const uint head, const uint tail, const t_float score);
//! Add the pair to the enw set:
void add_scalar__allVars__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self, const uint head, const uint tail, const t_float score);
#endif // #ifndef SWIG
//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_tripleUint_t(const s_kt_list_1d_tripleUint_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  if(self->current_pos != 0) {*scalar_result = self->current_pos;} //! ie, as we then assume that latter is in use.
  else {
    *scalar_result = self->list_size;
  }
}



//! De-allcoates the s_kt_list_1d_tripleUint_t object.
void free__s_kt_list_1d_tripleUint_t(s_kt_list_1d_tripleUint_t *self);


//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************



/**
   @struct s_ktType__pair
   @brief maps a pair of vertices to a given score (oesketh, 06. mar. 2017).
 **/
typedef struct s_ktType_pair {
  uint head;
  uint tail;
} s_ktType_pair_t;

//! Itnaites a s_ktType_pair_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_pair() ({s_ktType_pair_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_pair() ({s_ktType_pair_t rel; rel.head = UINT_MAX; rel.tail = UINT_MAX;  rel;}) //! ie, return
#define MF__initVal__s_ktType_pair(v1, v2) ({s_ktType_pair_t rel; rel.head = v1; rel.tail = v2;  rel;}) //! ie, return
// TODO[article::programming]: consider using [”elow] macor as an example of an inteirstgint 'artifact' wrt. macro-suage ... where 'our calling macro' for a "MF__initVal__s_ktType_pair(row_start, col_start)" tries to set "rel.row_start = row_start; rel.col_start = col_start;"
//#define MF__initVal__s_ktType_pair(head, tail) ({s_ktType_pair_t rel; rel.head = head; rel.tail = tail;  rel;}) //! ie, return
#define MF__initFromRow__s_ktType_pair(row, row_size) ({assert(row); assert(row_size >= 2); s_ktType_pair_t rel; rel.head = (uint)row[0]; rel.tail = (uint)row[1];  rel;}) //! ie, return
//! @return true if the s_ktType_pair_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_pair(rel) ({bool is_empty = ((rel.head == UINT_MAX) && (rel.tail == UINT_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_pair(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_pair_t objects.
#define MF__allocate__s_ktType_pair(size) ({assert(size > 0); s_ktType_pair_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_pair_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_pair objects.
#define MF__free__s_ktType_pair(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_pair
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_pair_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_pair {
  s_ktType_pair_t *list; uint list_size;
  uint current_pos;
} s_kt_list_1d_pair_t;
//! @return an 'empty verison' of our s_kt_list_1d_pair_t object
s_kt_list_1d_pair_t setToEmpty__s_kt_list_1d_pair_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_pair_t init__s_kt_list_1d_pair_t(uint list_size);
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_1d_pair_t initFrom_sparseMatrix__s_kt_list_1d_pair_t(const uint nrows, const uint ncols, t_float **matrix) ;
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_pair_t(const s_kt_list_1d_pair_t *self, const char *result_file);
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_1d_pair_t initFromFile__s_kt_list_1d_pair_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self, uint index, s_ktType_pair_t *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self, uint index, s_ktType_pair_t valueTo_set);

void push__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self, const uint head, const uint tail);

//! De-allcoates the s_kt_list_1d_pair_t object.
void free__s_kt_list_1d_pair_t(s_kt_list_1d_pair_t *self);

//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

/**
   @struct s_ktType__fPair
   @brief maps a fPair of vertices to a given score (oesketh, 06. mar. 2017).
 **/
typedef struct s_ktType_fPair {
  t_float head;
  t_float tail;
} s_ktType_fPair_t;

//! Itnaites a s_ktType_fPair_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_fPair() ({s_ktType_fPair_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_fPair() ({s_ktType_fPair_t rel; rel.head = T_FLOAT_MAX; rel.tail = T_FLOAT_MAX;  rel;}) //! ie, return
#define MF__initVal__s_ktType_fPair(v1, v2) ({s_ktType_fPair_t rel; rel.head = v1; rel.tail = v2;  rel;}) //! ie, return
// TODO[article::programming]: consider using [”elow] macor as an example of an inteirstgint 'artifact' wrt. macro-suage ... where 'our calling macro' for a "MF__initVal__s_ktType_fPair(row_start, col_start)" tries to set "rel.row_start = row_start; rel.col_start = col_start;"
//#define MF__initVal__s_ktType_fPair(head, tail) ({s_ktType_fPair_t rel; rel.head = head; rel.tail = tail;  rel;}) //! ie, return
#define MF__initFromRow__s_ktType_fPair(row, row_size) ({assert(row); assert(row_size >= 2); s_ktType_fPair_t rel; rel.head = (t_float)row[0]; rel.tail = (t_float)row[1];  rel;}) //! ie, return
//! @return true if the s_ktType_fPair_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_fPair(rel) ({bool is_empty = ((rel.head == T_FLOAT_MAX) && (rel.tail == T_FLOAT_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_fPair(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_fPair_t objects.
#define MF__allocate__s_ktType_fPair(size) ({assert(size > 0); s_ktType_fPair_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_fPair_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_fPair objects.
#define MF__free__s_ktType_fPair(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_fPair
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_fPair_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_fPair {
  s_ktType_fPair_t *list; uint list_size;
} s_kt_list_1d_fPair_t;
//! @return an 'empty verison' of our s_kt_list_1d_fPair_t object
s_kt_list_1d_fPair_t setToEmpty__s_kt_list_1d_fPair_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_fPair_t init__s_kt_list_1d_fPair_t(uint list_size);
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_fPair_t(const s_kt_list_1d_fPair_t *self, const char *result_file);
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_1d_fPair_t initFrom_sparseMatrix__s_kt_list_1d_fPair_t(const uint nrows, const uint ncols, t_float **matrix) ;
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_1d_fPair_t initFromFile__s_kt_list_1d_fPair_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_fPair_t(s_kt_list_1d_fPair_t *self, uint index, s_ktType_fPair_t *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_fPair_t(s_kt_list_1d_fPair_t *self, uint index, s_ktType_fPair_t valueTo_set);
//! De-allcoates the s_kt_list_1d_fPair_t object.
void free__s_kt_list_1d_fPair_t(s_kt_list_1d_fPair_t *self);

//! ***************************************************************************
//! ***************************************************************************
//! New struct: 
//! ***************************************************************************

/**
   @struct s_ktType__kvPair
   @brief maps a kvPair of vertices to a given score (oesketh, 06. mar. 2017).
 **/
typedef struct s_ktType_kvPair {
  uint head;
  t_float tail;
} s_ktType_kvPair_t;

//! Itnaites a s_ktType_kvPair_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_kvPair() ({s_ktType_kvPair_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_kvPair() ({s_ktType_kvPair_t rel; rel.head = UINT_MAX; rel.tail = T_FLOAT_MAX;  rel;}) //! ie, return
#define MF__initVal__s_ktType_kvPair(v1, v2) ({s_ktType_kvPair_t rel; rel.head = v1; rel.tail = v2;  rel;}) //! ie, return
// TODO[article::programming]: consider using [”elow] macor as an example of an inteirstgint 'artifact' wrt. macro-suage ... where 'our calling macro' for a "MF__initVal__s_ktType_kvPair(row_start, col_start)" tries to set "rel.row_start = row_start; rel.col_start = col_start;"
//#define MF__initVal__s_ktType_kvPair(head, tail) ({s_ktType_kvPair_t rel; rel.head = head; rel.tail = tail;  rel;}) //! ie, return
#define MF__initFromRow__s_ktType_kvPair(row, row_size) ({assert(row); assert(row_size >= 2); s_ktType_kvPair_t rel; rel.head = (uint)row[0]; rel.tail = (t_float)row[1];  rel;}) //! ie, return
//! @return true if the s_ktType_kvPair_t is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_kvPair(rel) ({bool is_empty = ((rel.head == UINT_MAX) && (rel.tail == T_FLOAT_MAX)); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
#define MF__isEqual__s_ktType_kvPair(rel1, rel2) ({bool is_equal = ((rel1.head == rel2.head) && (rel1.tail == rel2.tail) ); is_equal;}) //! ie, returnt rue if the tow relationships areequal
//! Allocate a new set of s_ktType_kvPair_t objects.
#define MF__allocate__s_ktType_kvPair(size) ({assert(size > 0); s_ktType_kvPair_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_ktType_kvPair_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
#define MF__allocate__s_ktType_s_kt_list_1d_kvPair_t(size) ({assert(size > 0); s_kt_list_1d_kvPair_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_kt_list_1d_kvPair_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_kvPair objects.
#define MF__free__s_ktType_kvPair(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return
#define MF__free__s_kt_list_1d_kvPair_t(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

/**
   @struct s_kt_list_1d_kvPair
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_kvPair_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_kvPair {
  s_ktType_kvPair_t *list; uint list_size; uint current_pos;
} s_kt_list_1d_kvPair_t;
//! @return an 'empty verison' of our s_kt_list_1d_kvPair_t object
s_kt_list_1d_kvPair_t setToEmpty__s_kt_list_1d_kvPair_t();
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_kvPair_t init__s_kt_list_1d_kvPair_t(uint list_size);
//! Esports the object ot a tsv-file
//! @remarks we expect the inptu to consist of only 3 columns.
//! @return true upon scucess.
bool export__tsv__s_kt_list_1d_kvPair_t(const s_kt_list_1d_kvPair_t *self, const char *result_file);
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_1d_kvPair_t initFrom_sparseMatrix__s_kt_list_1d_kvPair_t(const uint nrows, const uint ncols, t_float **matrix) ;
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_1d_kvPair_t initFromFile__s_kt_list_1d_kvPair_t(const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, uint index, s_ktType_kvPair_t *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, uint index, s_ktType_kvPair_t valueTo_set);
//! De-allcoates the s_kt_list_1d_kvPair_t object.
void free__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self);
//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const s_ktType_kvPair_t valueTo_set);
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const s_ktType_kvPair_t valueTo_set);
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self);
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self);
//! ***************************************************************************
//! ***************************************************************************

//! Sort the data-set (oekseth, 06. jul. 2017).
void sort__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self);
//! Sort the data-set though/on the keys (rather than the scores) (oekseth, 06. jul. 2017).
void sort__onKeys__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self);

//! Unify two sorted lists, updating "self" with the result (oekseth, 06. aug. 2017).
void merge_sorted_maxSizeAfter__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const s_kt_list_1d_kvPair_t *prev, const uint cntMax_afterEach);
//! Udpat ethe object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const uint list_size);


//! Filter the results to include only the subset of identifed entries (oekseth, 06. otk. 2017)
void filterResult__sort__s_kt_list_1d_kvPair_t(s_kt_list_1d_kvPair_t *self, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach);

//! **********************************************
#endif //! EOF
