#include "math_baseCmp_operations_alglib.h"

/*************************************************************************
ALGLIB 3.10.0 (source code generated 2015-08-19)
Copyright (c) Sergey Bochkanov (ALGLIB project).

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the hpLysis documentation 
the Free Software Foundation (www.fsf.org); either version 2 of the 
License, or 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
hpLysis documentation for more details.

A copy of the hpLysis documentation is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


//! ********************************
#if (configure_performance__alglibCmp__useMacrosinsteadOfFunctions == 0)

/************************************************************************
Real math functions
************************************************************************/
ae_bool ae_fp_eq(t_float v1, t_float v2)
{
    /* IEEE-strict floating point comparison */
    volatile t_float x = v1;
    volatile t_float y = v2;
    return x==y;
}

ae_bool ae_fp_neq(t_float v1, t_float v2)
{
    /* IEEE-strict floating point comparison */
    return !ae_fp_eq(v1,v2);
}

ae_bool ae_fp_less(t_float v1, t_float v2)
{
    /* IEEE-strict floating point comparison */
    volatile t_float x = v1;
    volatile t_float y = v2;
    return x<y;
}

ae_bool ae_fp_less_eq(t_float v1, t_float v2)
{
    /* IEEE-strict floating point comparison */
    volatile t_float x = v1;
    volatile t_float y = v2;
    return x<=y;
}

ae_bool ae_fp_greater(t_float v1, t_float v2)
{
    /* IEEE-strict floating point comparison */
    volatile t_float x = v1;
    volatile t_float y = v2;
    return x>y;
}

ae_bool ae_fp_greater_eq(t_float v1, t_float v2)
{
    /* IEEE-strict floating point comparison */
    volatile t_float x = v1;
    volatile t_float y = v2;
    return x>=y;
}

/* ae_bool ae_isfinite_stateless(t_float x, ae_int_t endianness) */
/* { */
/*     union _u */
/*     { */
/*         t_float a; */
/*         ae_int32_t p[2]; */
/*     } u; */
/*     ae_int32_t high; */
/*     u.a = x; */
/*     if( endianness==AE_LITTLE_ENDIAN ) */
/*         high = u.p[1]; */
/*     else */
/*         high = u.p[0]; */
/*     return (high & (ae_int32_t)0x7FF00000)!=(ae_int32_t)0x7FF00000; */
/* } */

/* ae_bool ae_isnan_stateless(t_float x,    ae_int_t endianness) */
/* { */
/*     union _u */
/*     { */
/*         t_float a; */
/*         ae_int32_t p[2]; */
/*     } u; */
/*     ae_int32_t high, low; */
/*     u.a = x; */
/*     if( endianness==AE_LITTLE_ENDIAN ) */
/*     { */
/*         high = u.p[1]; */
/*         low =  u.p[0]; */
/*     } */
/*     else */
/*     { */
/*         high = u.p[0]; */
/*         low =  u.p[1]; */
/*     } */
/*     return ((high &0x7FF00000)==0x7FF00000) && (((high &0x000FFFFF)!=0) || (low!=0)); */
/* } */

/* ae_bool ae_isinf_stateless(t_float x,    ae_int_t endianness) */
/* { */
/*     union _u */
/*     { */
/*         t_float a; */
/*         ae_int32_t p[2]; */
/*     } u; */
/*     ae_int32_t high, low; */
/*     u.a = x; */
/*     if( endianness==AE_LITTLE_ENDIAN ) */
/*     { */
/*         high = u.p[1]; */
/*         low  = u.p[0]; */
/*     } */
/*     else */
/*     { */
/*         high = u.p[0]; */
/*         low  = u.p[1]; */
/*     } */
    
/*     /\* 31 least significant bits of high are compared *\/ */
/*     return ((high&0x7FFFFFFF)==0x7FF00000) && (low==0);  */
/* } */

/* ae_bool ae_isposinf_stateless(t_float x, ae_int_t endianness) */
/* { */
/*     union _u */
/*     { */
/*         t_float a; */
/*         ae_int32_t p[2]; */
/*     } u; */
/*     ae_int32_t high, low; */
/*     u.a = x; */
/*     if( endianness==AE_LITTLE_ENDIAN ) */
/*     { */
/*         high = u.p[1]; */
/*         low  = u.p[0]; */
/*     } */
/*     else */
/*     { */
/*         high = u.p[0]; */
/*         low  = u.p[1]; */
/*     } */
    
/*     /\* all 32 bits of high are compared *\/ */
/*     return (high==(ae_int32_t)0x7FF00000) && (low==0);  */
/* } */

/* ae_bool ae_isneginf_stateless(t_float x, ae_int_t endianness) */
/* { */
/*     union _u */
/*     { */
/*         t_float a; */
/*         ae_int32_t p[2]; */
/*     } u; */
/*     ae_int32_t high, low; */
/*     u.a = x; */
/*     if( endianness==AE_LITTLE_ENDIAN ) */
/*     { */
/*         high = u.p[1]; */
/*         low  = u.p[0]; */
/*     } */
/*     else */
/*     { */
/*         high = u.p[0]; */
/*         low  = u.p[1]; */
/*     } */
    
/*     /\* this code is a bit tricky to avoid comparison of high with 0xFFF00000, which may be unsafe with some buggy compilers *\/ */
/*     return ((high&0x7FFFFFFF)==0x7FF00000) && (high!=(ae_int32_t)0x7FF00000) && (low==0); */
/* } */

/* ae_int_t ae_get_endianness() */
/* { */
/*     union */
/*     { */
/*         t_float a; */
/*         ae_int32_t p[2]; */
/*     } u; */
    
/*     /\* */
/*      * determine endianness */
/*      * two types are supported: big-endian and little-endian. */
/*      * mixed-endian hardware is NOT supported. */
/*      * */
/*      * 1983 is used as magic number because its non-periodic t_float  */
/*      * representation allow us to easily distinguish between upper  */
/*      * and lower halfs and to detect mixed endian hardware. */
/*      * */
/*      *\/ */
/*     u.a = 1.0/1983.0;  */
/*     if( u.p[1]==(ae_int32_t)0x3f408642 ) */
/*         return AE_LITTLE_ENDIAN; */
/*     if( u.p[0]==(ae_int32_t)0x3f408642 ) */
/*         return AE_BIG_ENDIAN; */
/*     return AE_MIXED_ENDIAN; */
/* } */

/* ae_bool ae_isfinite(t_float x) */
/* { */
/*     return ae_isfinite_stateless(x); */
/* } */

/* ae_bool ae_isnan(t_float x) */
/* { */
/*     return ae_isnan_stateless(x); */
/* } */

/* ae_bool ae_isinf(t_float x) */
/* { */
/*     return ae_isinf_stateless(x); */
/* } */

/* ae_bool ae_isposinf(t_float x) */
/* { */
/*     return ae_isposinf_stateless(x); */
/* } */

/* ae_bool ae_isneginf(t_float x) */
/* { */
/*     return ae_isneginf_stateless(x); */
/* } */

t_float ae_fabs(t_float x)
{
    return fabs(x);
}

ae_int_t ae_iabs(ae_int_t x)
{
    return x>=0 ? x : -x;
}

t_float ae_sqr(t_float x)
{
    return x*x;
}

t_float ae_sqrt(t_float x)
{
    return sqrt(x);
}

ae_int_t ae_sign(t_float x)
{
    if( x>0 ) return  1;
    if( x<0 ) return -1;
    return 0;
}

ae_int_t ae_round(t_float x)
{
    return (ae_int_t)(ae_ifloor(x+0.5));
}

ae_int_t ae_trunc(t_float x)
{
    return (ae_int_t)(x>0 ? ae_ifloor(x) : ae_iceil(x));
}

ae_int_t ae_ifloor(t_float x)
{
    return (ae_int_t)(floor(x));
}

ae_int_t ae_iceil(t_float x)
{
    return (ae_int_t)(ceil(x));
}

ae_int_t ae_maxint(ae_int_t m1, ae_int_t m2)
{
    return m1>m2 ? m1 : m2;
}

ae_int_t ae_minint(ae_int_t m1, ae_int_t m2)
{
    return m1>m2 ? m2 : m1;
}

t_float ae_maxreal(t_float m1, t_float m2)
{
    return m1>m2 ? m1 : m2;
}

t_float ae_minreal(t_float m1, t_float m2)
{
    return m1>m2 ? m2 : m1;
}

t_float ae_randomreal()
{
    int i1 = rand();
    int i2 = rand();
    t_float mx = (t_float)(RAND_MAX)+1.0;
    volatile t_float tmp0 = i2/mx;
    volatile t_float tmp1 = i1+tmp0;
    return tmp1/mx;
}

ae_int_t ae_randominteger(ae_int_t maxv)
{
    return rand()%maxv;
}

t_float   ae_sin(t_float x)
{
    return sin(x);
}

t_float   ae_cos(t_float x)
{
    return cos(x);
}

t_float   ae_tan(t_float x)
{
    return tan(x);
}

t_float   ae_sinh(t_float x)
{
    return sinh(x);
}

t_float   ae_cosh(t_float x)
{
    return cosh(x);
}
t_float   ae_tanh(t_float x)
{
    return tanh(x);
}

t_float   ae_asin(t_float x)
{
    return asin(x);
}

t_float   ae_acos(t_float x)
{
    return acos(x);
}

t_float   ae_atan(t_float x)
{
    return atan(x);
}

t_float   ae_atan2(t_float y, t_float x)
{
    return atan2(y,x);
}

t_float   ae_log(t_float x)
{
    return log(x);
}

t_float   ae_pow(t_float x, t_float y)
{
    return pow(x,y);
}

t_float   ae_exp(t_float x)
{
    return exp(x);
}

#endif


//! **************************************************
/*************************************************************************
This function creates ae_vector.
Vector size may be zero. Vector contents is uninitialized.

dst                 destination vector, assumed to be  uninitialized,  its
                    fields are ignored.
size                vector size, may be zero
datatype            guess what...
state               this parameter can be:
                    * pointer to current instance of ae_state, if you want
                      to automatically destroy this object  after  leaving
                      current frame
                    * NULL,   if  you  do  NOT  want  this  vector  to  be
                      automatically managed (say, if it is field  of  some
                      object)

Error handling:
* on failure (size<0 or unable to allocate memory) - calls ae_break() with
  NULL state pointer. Usually it results in abort() call.

dst is 
*************************************************************************/
void ae_vector_init(ae_vector *dst, ae_int_t size, ae_datatype datatype)
{
    /* ensure that size is >=0
       two ways to exit: 1) through ae_assert, if we have non-NULL state, 2) by returning ae_false */
    /* ae_assert( */
    /*     size>=0, */
    /*     "ae_vector_init(): negative size", */
    /*     NULL); */

    /* init */
    dst->cnt = size;
    dst->datatype = datatype;
    assert(false); // FIXME: add a permtaution of [”elow] ... 
    /* ae_assert( */
    /*     ae_db_malloc(&dst->data, size*ae_sizeof(datatype)), /\* TODO: change ae_db_malloc() *\/ */
    /*     "ae_vector_init(): failed to allocate memory", */
    /*     NULL); */
    dst->ptr.p_ptr = dst->data.ptr;
    dst->is_attached = ae_false;
}


/************************************************************************
This function creates copy of ae_vector. New copy of the data is created,
which is managed and owned by newly initialized vector.

dst                 destination vector
src                 well, it is source
state               this parameter can be:
                    * pointer to current instance of ae_state, if you want
                      to automatically destroy this object  after  leaving
                      current frame
                    * NULL,   if  you  do  NOT  want  this  vector  to  be
                      automatically managed (say, if it is field  of  some
                      object)
                      
Error handling:
* on failure calls ae_break() with NULL state pointer. Usually it  results
  in abort() call.

dst is assumed to be uninitialized, its fields are ignored.
************************************************************************/
void ae_vector_init_copy(ae_vector *dst, ae_vector *src)
{
    ae_vector_init(dst, src->cnt, src->datatype);
    
    assert(false); // FIXME: include a permtaution of [below]
    /* if( src->cnt!=0 ) */
    /*     memcpy(dst->ptr.p_ptr, src->ptr.p_ptr, (size_t)(src->cnt*ae_sizeof(src->datatype))); */
}

/************************************************************************
This function initializes ae_vector using X-structure as source. New copy
of data is created, which is owned/managed by ae_vector  structure.  Both
structures (source and destination) remain completely  independent  after
this call.

dst                 destination matrix
src                 well, it is source
state               this parameter can be:
                    * pointer to current instance of ae_state, if you want
                      to automatically destroy this object  after  leaving
                      current frame
                    * NULL,   if  you  do  NOT  want  this  vector  to  be
                      automatically managed (say, if it is field  of  some
                      object)
                      
Error handling:
* on failure calls ae_break() with NULL state pointer. Usually it  results
  in abort() call.

dst is assumed to be uninitialized, its fields are ignored.
************************************************************************/
void ae_vector_init_from_x(ae_vector *dst, x_vector *src)
{
  assert(false); // FIXME: include a permtaution of [belwo]
  ae_vector_init(dst, (ae_int_t)src->cnt, (ae_datatype)src->datatype);
    /* if( src->cnt>0 ) */
    /*   memcpy(dst->ptr.p_ptr, src->ptr, (size_t)(((ae_int_t)src->cnt)*ae_sizeof((ae_datatype)src->datatype))); */
}

/************************************************************************
This function initializes ae_vector using X-structure as source.

New vector is attached to source:
* DST shares memory with SRC
* both DST and SRC are writable - all writes to DST  change  elements  of
  SRC and vice versa.
* DST can be reallocated with ae_vector_set_length(), in  this  case  SRC
  remains untouched
* SRC, however, CAN NOT BE REALLOCATED AS LONG AS DST EXISTS

NOTE: is_attached field is set  to  ae_true  in  order  to  indicate  that
      vector does not own its memory.

dst                 destination vector
src                 well, it is source
state               this parameter can be:
                    * pointer to current instance of ae_state, if you want
                      to automatically destroy this object  after  leaving
                      current frame
                    * NULL,   if  you  do  NOT  want  this  vector  to  be
                      automatically managed (say, if it is field  of  some
                      object)
                      
Error handling:
* on failure calls ae_break() with NULL state pointer. Usually it  results
  in abort() call.

dst is assumed to be uninitialized, its fields are ignored.
************************************************************************/
void ae_vector_attach_to_x(ae_vector *dst, x_vector *src)
{
    volatile ae_int_t cnt;
    
    cnt = (ae_int_t)src->cnt;
    
    /* ensure that size is correct */
    assert(false); // FIXME: include a permtaution of [belwo]
    /* ae_assert(cnt==src->cnt,  "ae_vector_attach_to_x(): 32/64 overflow", NULL); */
    /* ae_assert(cnt>=0,         "ae_vector_attach_to_x(): negative length", NULL); */
    
    /* init */
    dst->cnt = cnt;
    dst->datatype = (ae_datatype)src->datatype;
    dst->ptr.p_ptr = src->ptr;
    dst->is_attached = ae_true;
    assert(false); // FIXME: add a permtaution of [”elow] ... 
    /* ae_assert( */
    /*     ae_db_malloc(&dst->data, 0,), */
    /*     "ae_vector_attach_to_x(): malloc error", */
    /*     NULL); */
}

/************************************************************************
This function changes length of ae_vector.

dst                 destination vector
newsize             vector size, may be zero
state               ALGLIB environment state

Error handling:
* if state is NULL, returns ae_false on allocation error
* if state is not NULL, calls ae_break() on allocation error
* returns ae_true on success

NOTES:
* vector must be initialized
* all contents is destroyed during setlength() call
* new size may be zero.
************************************************************************/
ae_bool ae_vector_set_length(ae_vector *dst, ae_int_t newsize)
{
    /* ensure that size is >=0
       two ways to exit: 1) through ae_assert, if we have non-NULL state, 2) by returning ae_false */
  assert(false); // FIXME: include a permtaution of [belwo]
    /* if( state!=NULL ) */
    /*     ae_assert(newsize>=0, "ae_vector_set_length(): negative size"); */
    if( newsize<0 )
        return ae_false;

    /* set length */
    if( dst->cnt==newsize )
        return ae_true;
    dst->cnt = newsize;
    assert(false); // FIXME: add a permtaution of [”elow] ... 
    /* if( !ae_db_realloc(&dst->data, newsize*ae_sizeof(dst->datatype)) ) */
    /*     return ae_false; */
    dst->ptr.p_ptr = dst->data.ptr;
    return ae_true;
}


/************************************************************************
This  function  provides  "CLEAR"  functionality  for vector (contents is
cleared, but structure still left in valid state).

The  function clears vector contents (releases all dynamically  allocated
memory). Vector may be in automatic management list  -  in this  case  it
will NOT be removed from list.

IMPORTANT: this function does NOT invalidates dst; it just  releases  all
dynamically allocated storage, but dst still may be used  after  call  to
ae_vector_set_length().

dst                 destination vector
************************************************************************/
void ae_vector_clear(ae_vector *dst)
{
    dst->cnt = 0;
    assert(false); // FIXME: include a permtaution of [belwo]
    // ae_db_free(&dst->data);
    dst->ptr.p_ptr = 0;
    dst->is_attached = ae_false;
}


/************************************************************************
This  function  provides "DESTROY"  functionality for vector (contents is
cleared, all internal structures are destroyed). For vectors it  is  same
as CLEAR.

dst                 destination vector
************************************************************************/
void ae_vector_destroy(ae_vector *dst)
{
    ae_vector_clear(dst);
}


/************************************************************************
This function efficiently swaps contents of two vectors, leaving other
pararemeters (automatic management, etc.) unchanged.
************************************************************************/
void ae_swap_vectors(ae_vector *vec1, ae_vector *vec2)
{
    ae_int_t cnt;
    ae_datatype datatype;
    void *p_ptr;
    
    assert(false); // FIXME: include a permtaution of [belwo]
    // ae_assert(!vec1->is_attached, "ALGLIB: internal error, attempt to swap vectors attached to X-object", NULL);
    // ae_assert(!vec2->is_attached, "ALGLIB: internal error, attempt to swap vectors attached to X-object", NULL);
    
    assert(false); // FIXME: include a permtaution of [belwo]
    // ae_db_swap(&vec1->data, &vec2->data);
    
    cnt = vec1->cnt;
    datatype = vec1->datatype;
    p_ptr = vec1->ptr.p_ptr;
    vec1->cnt = vec2->cnt;
    vec1->datatype = vec2->datatype;
    vec1->ptr.p_ptr = vec2->ptr.p_ptr;
    vec2->cnt = cnt;
    vec2->datatype = datatype;
    vec2->ptr.p_ptr = p_ptr;
}


//! **************************************************

t_float nulog1p(t_float x) {
    t_float z;
    t_float lp;
    t_float lq;
    t_float result;


    z = 1.0+x;
    if( ae_fp_less(z,0.70710678118654752440)||ae_fp_greater(z,1.41421356237309504880) )
    {
        result = ae_log(z);
        return result;
    }
    z = x*x;
    lp = 4.5270000862445199635215E-5;
    lp = lp*x+4.9854102823193375972212E-1;
    lp = lp*x+6.5787325942061044846969E0;
    lp = lp*x+2.9911919328553073277375E1;
    lp = lp*x+6.0949667980987787057556E1;
    lp = lp*x+5.7112963590585538103336E1;
    lp = lp*x+2.0039553499201281259648E1;
    lq = 1.0000000000000000000000E0;
    lq = lq*x+1.5062909083469192043167E1;
    lq = lq*x+8.3047565967967209469434E1;
    lq = lq*x+2.2176239823732856465394E2;
    lq = lq*x+3.0909872225312059774938E2;
    lq = lq*x+2.1642788614495947685003E2;
    lq = lq*x+6.0118660497603843919306E1;
    z = -0.5*z+x*(z*lp/lq);
    result = x+z;
    return result;
}


t_float nuexpm1(t_float x) {
    t_float r;
    t_float xx;
    t_float ep;
    t_float eq;
    t_float result;


    if( ae_fp_less(x,-0.5)||ae_fp_greater(x,0.5) )
    {
        result = ae_exp(x)-1.0;
        return result;
    }
    xx = x*x;
    ep = 1.2617719307481059087798E-4;
    ep = ep*xx+3.0299440770744196129956E-2;
    ep = ep*xx+9.9999999999999999991025E-1;
    eq = 3.0019850513866445504159E-6;
    eq = eq*xx+2.5244834034968410419224E-3;
    eq = eq*xx+2.2726554820815502876593E-1;
    eq = eq*xx+2.0000000000000000000897E0;
    r = x*ep;
    r = r/(eq-r);
    result = r+r;
    return result;
}


t_float nucosm1(t_float x) {
    t_float xx;
    t_float c;
    t_float result;


    if( ae_fp_less(x,-0.25*ae_pi)||ae_fp_greater(x,0.25*ae_pi) )
    {
        result = ae_cos(x)-1;
        return result;
    }
    xx = x*x;
    c = 4.7377507964246204691685E-14;
    c = c*xx-1.1470284843425359765671E-11;
    c = c*xx+2.0876754287081521758361E-9;
    c = c*xx-2.7557319214999787979814E-7;
    c = c*xx+2.4801587301570552304991E-5;
    c = c*xx-1.3888888888888872993737E-3;
    c = c*xx+4.1666666666666666609054E-2;
    result = -0.5*xx+xx*xx*c;
    return result;
}


