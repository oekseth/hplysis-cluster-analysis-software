#include "hp_entropy.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
// library(entropy)

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main() 
#else
  static void tut_entropy_x_time() 
#endif
{
  const uint max_theoreticCountSize = 10;
  uint nrows = 100; //*1000;
  for(uint case_id = 1; case_id <= 6; case_id++) {
    e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_ML_Shannon;
    s_kt_entropy_t obj_entropy = initSimple__s_kt_entropy(max_theoreticCountSize, /*vector_length=*/nrows, enum_entropy);  
    //case = 0;
    t_float case1 = 0;
    s_kt_list_1d_uint_t scoreList = init__s_kt_list_1d_uint_t(nrows);
    const t_float lambda_freqs  = 0.1;
    {
      start_time_measurement();
      //start.time <- proc.time(); //! ie, start time-emasuremetns:
      t_float nrowsHalve = (t_float)nrows/2;
      for(uint col_id = 0; col_id < (uint)nrowsHalve; col_id++) {
	scoreList.list[col_id] = max_theoreticCountSize;
	//  scoreList[col] = col;
      }
      for(uint col_id = (uint)nrowsHalve; col_id < (uint)nrows; col_id++) {
	scoreList.list[col_id] = 5;
	//  scoreList[col] = col;
      }
      const char *str_local = "initiated-data";
      case1 = end_time_measurement(/*msg=*/str_local, FLT_MAX);
    }
    t_float caseML = 0;
    {
      start_time_measurement();
      t_float H = 0;
      e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_ML_Shannon;
      kt_entropy__generic__vector(&obj_entropy, &scoreList, enum_entropy, /*isTo_applyPostScaling=*/false, /*lambda_default=*/0, &H);
      //! ----------
      //H = entropy(scoreList, lambda.freqs, "ML");  //entropy.plugin(freqs);
      //! ----------
      const char *str_local = "Shannon";
      caseML = end_time_measurement(/*msg=*/str_local, FLT_MAX);
      fprintf(stdout, "score:(%d, %f) ", nrows, H);
    }
    t_float caseMM = 0;
    {
      start_time_measurement();
      t_float H = 0;
      e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_MM_MillerMadow;
      kt_entropy__generic__vector(&obj_entropy, &scoreList, enum_entropy, /*isTo_applyPostScaling=*/false, /*lambda_default=*/0, &H);
      //! ----------
      const char *str_local = "MillerMadow";
      caseMM = end_time_measurement(/*msg=*/str_local, FLT_MAX);
      fprintf(stdout, "score:(%d, %f) ", nrows, H);
   }
    t_float caseJeffreys = 0;
    {
      start_time_measurement();
      t_float H = 0;
      e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_Dirichlet_Jeffreys;
      kt_entropy__generic__vector(&obj_entropy, &scoreList, enum_entropy, /*isTo_applyPostScaling=*/false, /*lambda_default=*/0, &H);
      //! ----------
      const char *str_local = "Jeffreys";
      caseJeffreys = end_time_measurement(/*msg=*/str_local, FLT_MAX);
      fprintf(stdout, "score:(%d, %f) ", nrows, H);
    }
    t_float caseCS = 0;
    {
      start_time_measurement();
      t_float H = 0;
      e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_CS_ChaoShen;
      kt_entropy__generic__vector(&obj_entropy, &scoreList, enum_entropy, /*isTo_applyPostScaling=*/false, /*lambda_default=*/0, &H);
      //! ----------
      const char *str_local = "ChaoShen";
      caseCS = end_time_measurement(/*msg=*/str_local, FLT_MAX);
      fprintf(stdout, "score:(%d, %f) ", nrows, H);
    }
    t_float caseShrink = 0;
    {
      start_time_measurement();
      t_float H = 0;
      e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_shrink;
      kt_entropy__generic__vector(&obj_entropy, &scoreList, enum_entropy, /*isTo_applyPostScaling=*/false, /*lambda_default=*/0, &H);
      //! ----------
      const char *str_local = "Shrink";
      caseShrink = end_time_measurement(/*msg=*/str_local, FLT_MAX);
      fprintf(stdout, "score:(%d, %f) ", nrows, H);
    } 
    fprintf(stdout, "list[%d]\t%f\t%f\t%f\t%f\t%f\t%f\n", nrows, case1, caseML, caseMM, caseJeffreys, caseCS, caseShrink);
    fprintf(stderr, "list[%d]\t%f\t%f\t%f\t%f\t%f\t%f\n", nrows, case1, caseML, caseMM, caseJeffreys, caseCS, caseShrink);
    //! 
    //! Increment:
    nrows = nrows * 10;
    //! 
    //! De-allocate:
    free__s_kt_entropy(&obj_entropy);
    free__s_kt_list_1d_uint_t(&scoreList); 
  }
}
