//printf("(info)\t isTo__useInternalObjectSpec=%d, between=%d, within=%d, at %s:%d\n", isTo__useInternalObjectSpec, vertexVertex__networkReference__between, vertexVertex__networkReference__within, __FILE__, __LINE__);

//!
  //! Provide logics for the c'ase' where we are interested in describing/using the 'proerpties of the distirbution'.
  const long long int current1dList_size = (long long int)self->nrows * (long long int)self->nrows; 
  const t_float empty_0 = 0;  t_float *current1dList = NULL; uint current1dList__currentPos = 0; 
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {current1dList  = allocate_1d_list_float(current1dList_size, empty_0); assert(current1dList);}




    //if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef)
  if(isTo__useInternalObjectSpec == false)  { //! then we assuem the user has 'epxlcitly' requested a compliation/usage 'of this'.
    //    printf("##\tat %s:%d\n", __FILE__, __LINE__);


    //! ------------------
    //! 
    //! Idenitfy cluster-centroids, calling our "kt_matrix__centroids(..)" defined in our "kt_api.h":
    s_kt_matrix_t obj_result_clusterToVertex; setTo_empty__s_kt_matrix_t(&obj_result_clusterToVertex);
    s_kt_matrix_t obj_result_clusterToVertex__globalCenter; setTo_empty__s_kt_matrix_t(&obj_result_clusterToVertex__globalCenter);
  //! Note(obj_result_clusterToVertex::medoid): matrix=[cluster-id][0] (describibing the sum-of-distance-s fromt eh cenotrid to each asiscated cluster-vertex), mapOf_rowIds=[clusterId-->vertexId]
    //! Note(obj_result_clusterToVertex::else): matrix=[cluster-id][row-id], ie, describing the 'center-point' (of each cluster-center for the set of vertices in a givne clsuter).
    // TODO[ªfutture]: cosnider to 'add support' for the case where 'centroids are used in combination with the "vertexVertex" distnace-case <--- for 'mean' and 'rank' how are we 'tehn to supprot/idneityf socres'? (oesketh, 06. feb. 2017).
    //#ifndef NDEBUG
							      bool __statProcess__centroidsAre_inferred = false;
      //#endif
    if(
       (vertexVertex__networkReference__within != e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex)
       ||
       (vertexVertex__networkReference__between != e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex)
       ) {
      // FIXME: make [below] optionabl ... ie Not comptue if Not used.
      { //! Apply logics: 'each clsuter':
	//printf("at %s:%d\n", __FILE__, __LINE__);
	s_kt_matrix_t obj_1; setTo_empty__s_kt_matrix_t(&obj_1);
	assert(self->input_matrix != NULL);
	obj_1.nrows = self->nrows;  obj_1.ncols = self->ncols; obj_1.matrix = self->input_matrix;
	uint *mapOf_vertexClusterIds = self->mapOf_vertexClusterIds;
	assert(mapOf_vertexClusterIds);
	assert(obj_result_clusterToVertex.nrows == 0); //! ie, as we expect 'this object' to Not have been intalized.
	assert(obj_result_clusterToVertex.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized.
	const bool is_ok = kt_matrix__centroids(&obj_1, mapOf_vertexClusterIds, &obj_result_clusterToVertex, self->cnt_cluster, /*typeOf_centroidAlg=*/metric_centroid); //! deifned in our "kt_api.h"
	assert(is_ok);
	setTo_empty__s_kt_matrix_t(&obj_1); //! ie, clear 'odl cofnigruaitons'.
	//#ifndef NDEBUG
	__statProcess__centroidsAre_inferred = true;
	//assert(obj_result_clusterToVertex.mapOf_rowIds);
	//#endif
      }
      if(
	 (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid) //! where we 'handle this as a vertex-->global', ie, as a 'within-clsuter' only contains/hold/describes one centid-vertex..
	 || (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal)
	 || (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal) 
	 || (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal__mulByWeight) 
	 ) 
	{ //! Apply logics: 'global center':
	s_kt_matrix_t obj_1; setTo_empty__s_kt_matrix_t(&obj_1);
	assert(self->input_matrix != NULL);
	obj_1.nrows = self->nrows;  obj_1.ncols = self->ncols; obj_1.matrix = self->input_matrix;
	//!
	//! Specify that all vertices are part of the same cluster:
	const uint empty_0 = 0; uint *mapOf_vertexClusterIds = allocate_1d_list_uint(self->nrows, empty_0);
	//!
	//! Apply logics:
	assert(mapOf_vertexClusterIds);
	const bool is_ok = kt_matrix__centroids(&obj_1, mapOf_vertexClusterIds, &obj_result_clusterToVertex__globalCenter, self->cnt_cluster, /*typeOf_centroidAlg=*/metric_centroid); //! deifned in our "kt_api.h"
	assert(is_ok);
	setTo_empty__s_kt_matrix_t(&obj_1); //! ie, clear 'odl cofnigruaitons'.
	free_1d_list_uint(&mapOf_vertexClusterIds);
      }
    } //! else we assume the 'centrodis' are not of speicfic interest.   
    const uint cnt_clusters = self->cnt_cluster;
    const uint cnt_vertices = self->nrows;
    if(!__statProcess__centroidsAre_inferred || obj_result_clusterToVertex.mapOf_rowIds) //! where 'this requirement' is added to address issues where not any clsuters assotied to the cenotrid were inferred (oekseh, 06. otk. 2017)
      { //! Idenitfy "within": 
	//printf("at %s:%d\n", __FILE__, __LINE__);
      //    printf("##\tat %s:%d\n", __FILE__, __LINE__);
      //! ------
      if(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex) {
#define __Mi__update__within  1
#define __Mi__update__between 0
#define __Mi_isToUpdate__s_kt_matrix_ccm_matrixBased_t 1
#include "kt_matrix_cmpCluster__ccmMatrix__betweenScore__sum.c" //! ie, apply logics:	
	//    printf("##\tat %s:%d\n", __FILE__, __LINE__);
      } else if(
		(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexCentroid) ||
		(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidVertex)
		)
	{
	  //    printf("##\tat %s:%d\n", __FILE__, __LINE__);
	//! Note[idea]: find the distnace from each cenotrid to the assicated vertices: 
	//for(uint row_id = 0; row_id < self->nrows; row_id++) {
	//!
	//! Iterate: for each cluster-combination:
	const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	for(uint cluster_id_in = 0; cluster_id_in < cnt_clusters; cluster_id_in++) {
	  t_float *row_centroid = NULL;
	  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	    uint vertex_centroid_id = UINT_MAX;
	    assert(obj_result_clusterToVertex.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in];
	    assert(vertex_centroid_id != UINT_MAX);
	    assert(vertex_centroid_id < self->nrows);
	    row_centroid = self->input_matrix[vertex_centroid_id];
	    assert(row_centroid);
	  } else {
	    // assert(obj_result_clusterToVertex.mapOf_rowIds);
	    /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	    /* assert(vertex_centroid_id != UINT_MAX); */
	    assert(obj_result_clusterToVertex.matrix);
	    row_centroid = obj_result_clusterToVertex.matrix[cluster_id_in];
	    assert(row_centroid);	    
	  }
	  //assert(vertex_centroid_id != UINT_MAX;
	  uint mappings_cluster1_size = 0; uint *mappings_cluster1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster1_size);
	  if(mappings_cluster1_size > 0) {
	    assert(mappings_cluster1); assert(self->input_matrix);
	    uint cnt_udpated = 0;
	    //! Identify the 'default' score:    
	    t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:
	    //!
	    //! Iterate:
	    for(uint i = 0; i < mappings_cluster1_size; i++) {
	      const uint ind1 = mappings_cluster1[i]; assert(ind1 != UINT_MAX);
	      assert(row_centroid);
	      t_float score = T_FLOAT_MAX;
	      if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
		score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, self->input_matrix[ind1], row_centroid);
	      } else {
		// FIXME: use an 'acjencye-boolean' to ivnegtate/vlidate that the matrix is an adjcency-matrix (and Not a feautre-amtrix, ie, for which [below] wodul give/porodcue errnrous results).
		assert(ind1 < self->ncols); 
		score = row_centroid[ind1]; 
	      }
	      if(score != T_FLOAT_MAX) {
		const t_float candidate_score = score;
		//sumOf_result__between += innerClusterDist__between;
		//! Udpate the score:
		min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used.
	      }
	    }
	    //! 
	    //! Then evlauationi of the clsuter is completed: 
	    if(cnt_udpated > 0) {
	      __clusterConsistency__postProcessScore(); 
	      assert(min_score != T_FLOAT_MAX);
	      sumOf_result__within += min_score;
	      ret_obj.within_min = macro_min(ret_obj.within_min, min_score);
	      ret_obj.within_max = macro_max(ret_obj.within_max, min_score);
	    }
	  }
	}
      } else if(
		(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid) //! where we 'handle this as a vertex-->global', ie, as a 'within-clsuter' only contains/hold/describes one centid-vertex..
		|| (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal)
		) {
	//printf("at %s:%d\n", __FILE__, __LINE__);
	//! Note[idea]: find the distance from each vertex to the 'global center-point':
	t_float *row_centroid = NULL;
	const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	  uint vertex_centroid_id = UINT_MAX;
	  assert(obj_result_clusterToVertex__globalCenter.mapOf_rowIds);
	  vertex_centroid_id = obj_result_clusterToVertex__globalCenter.mapOf_rowIds[/*cluster_id_in=*/0];
	  assert(vertex_centroid_id != UINT_MAX);
	  assert(vertex_centroid_id < self->nrows);
	  row_centroid = self->input_matrix[vertex_centroid_id];
	  assert(row_centroid);
	} else {
	  // assert(obj_result_clusterToVertex.mapOf_rowIds);
	  /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	  /* assert(vertex_centroid_id != UINT_MAX); */
	  assert(obj_result_clusterToVertex__globalCenter.matrix);
	  row_centroid = obj_result_clusterToVertex__globalCenter.matrix[/*cluster_id_in=*/0];
	  assert(row_centroid);	    
	}

	if(
	   (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid) //! where we 'handle this as a vertex-->global', ie, as a 'within-clsuter' only contains/hold/describes one centid-vertex..
	   ) {
	  //!
	  //! Iterate through each cluster:	
	  for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	    t_float *row_centroid_out = NULL;
	    uint vertex_centroid_id = UINT_MAX;
#ifndef NDEBUG
	    assert(__statProcess__centroidsAre_inferred);
#endif
	    assert(obj_result_clusterToVertex.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_out];
	    //! 
	    //! Get the vector:
	    if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	      assert(vertex_centroid_id != UINT_MAX);
	      assert(vertex_centroid_id < self->nrows);
	      row_centroid_out = self->input_matrix[vertex_centroid_id];
	      assert(row_centroid_out);
	    } else {
	      // assert(obj_result_clusterToVertex.mapOf_rowIds);
	      /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	      /* assert(vertex_centroid_id != UINT_MAX); */
	      assert(obj_result_clusterToVertex.matrix);
	      row_centroid_out = obj_result_clusterToVertex.matrix[cluster_id_out];
	      assert(row_centroid_out);	    
	    }
	    t_float score = T_FLOAT_MAX;
	    if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
	      score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, self->ncols, row_centroid_out, row_centroid);
	    } else {
	      assert(vertex_centroid_id < self->ncols); //! ie, as we then assuemt eh atrmix si an ajdcnecy-matrix.
	      score = row_centroid[vertex_centroid_id]; 
	    }
	    if(score != T_FLOAT_MAX) {
	      sumOf_result__within += score;
	      ret_obj.within_min = macro_min(ret_obj.within_min, score);
	      ret_obj.within_max = macro_max(ret_obj.within_max, score);
	    }
	  }
	} else {
	  //!
	  //! Iterate through each vertex and 'idneitfy' the distance to the glboal-center-vertex:
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {	  
	    //const uint row_id = ind1;
	    const uint ind1 = row_id;
	    t_float score = T_FLOAT_MAX;
	    if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
	      score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, self->ncols, self->input_matrix[ind1], row_centroid);
	    } else {
	      assert(row_id < self->ncols); //! ie, as we then assuemt eh atrmix si an ajdcnecy-matrix.
	      score = row_centroid[row_id]; 
	    }
	    if(score != T_FLOAT_MAX) {
	      sumOf_result__within += score;
	      ret_obj.within_min = macro_min(ret_obj.within_min, score);
	      ret_obj.within_max = macro_max(ret_obj.within_max, score);
	    }
	  }
	}	
      } else if(
		(vertexVertex__networkReference__within    == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal) 
		|| (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal__mulByWeight) 
		) {
	//printf("at %s:%d\n", __FILE__, __LINE__);
	//! Note[idea]: iterate through the clusters and comopute the score:
	const bool multByCluster_size = (vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal__mulByWeight);
	// TODO[future]: condier adding support for the "e_kt_matrix_cmpCluster_clusterDistance__config_metric__t" config-rpoerpty .... ie, 'add sperately' for "within" and "between" ... and then update our difnfernet CCMs.
	t_float *row_centroid = NULL;
	const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	  uint vertex_centroid_id = UINT_MAX;
	  assert(obj_result_clusterToVertex__globalCenter.mapOf_rowIds);
	  vertex_centroid_id = obj_result_clusterToVertex__globalCenter.mapOf_rowIds[/*cluster_id_in=*/0];
	  assert(vertex_centroid_id != UINT_MAX);
	  assert(vertex_centroid_id < self->nrows);
	  row_centroid = self->input_matrix[vertex_centroid_id];
	  assert(row_centroid);
	} else {
	  // assert(obj_result_clusterToVertex.mapOf_rowIds);
	  /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	  /* assert(vertex_centroid_id != UINT_MAX); */
	  assert(obj_result_clusterToVertex__globalCenter.matrix);
	  row_centroid = obj_result_clusterToVertex__globalCenter.matrix[/*cluster_id_in=*/0];
	  assert(row_centroid);	    
	}
	//!
	//! Iterate through each cluster:	
	for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	  t_float *row_centroid_out = NULL;
	  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	    uint vertex_centroid_id = UINT_MAX;
	    assert(obj_result_clusterToVertex.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_out];
	    assert(vertex_centroid_id != UINT_MAX);
	    assert(vertex_centroid_id < self->nrows);
	    row_centroid_out = self->input_matrix[vertex_centroid_id];
	    assert(row_centroid_out);
	  } else {
	    // assert(obj_result_clusterToVertex.mapOf_rowIds);
	    /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	    /* assert(vertex_centroid_id != UINT_MAX); */
	    assert(obj_result_clusterToVertex.matrix);
	    row_centroid_out = obj_result_clusterToVertex.matrix[cluster_id_out];
	    assert(row_centroid_out);	    
	  }
	  assert(row_centroid_out != row_centroid);
	  assert(row_centroid);
	  t_float score = 0;
	  if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
	    score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, row_centroid, row_centroid_out);
	    if(score != T_FLOAT_MAX) {
	      if(multByCluster_size) { //! then we muliply by the number of vertices in the cluyster:
		uint mappings_cluster2_size = 0; uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size);
		assert(mappings_cluster2_size != UINT_MAX);
		score = score*mappings_cluster2_size;
	      }
	    }
	  } else { //! then we comptue the symemtic-total-distnace between the vectors:
	    // TOOD[aritlce]: consider using a differnet funciotn that [below] for 'meringtin' two distance-vectors:
	    t_float score_1 = 0;
	    for(uint i = 0; i < nrows; i++) {
	      if(isOf_interest(row_centroid[i])) {score_1 += row_centroid[i];}}
	    t_float score_2 = 0;
	    for(uint i = 0; i < nrows; i++) {
	      if(isOf_interest(row_centroid_out[i])) {score_2 += row_centroid_out[i];}}
	    score = 0.5*score_1 + 0.5*score_2;
	  }
	  
	  if( (score != T_FLOAT_MAX) && (score != 0) ) {
	    sumOf_result__within += score;
	    ret_obj.within_min = macro_min(ret_obj.within_min, score);
	    ret_obj.within_max = macro_max(ret_obj.within_max, score);
	  }	  
	}
      } else if(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid) {
	//! Note[idea]: find the distance between centroids:
	assert(false); // ie, a fall-back.
      } else {
	fprintf(stderr, "!!\t(option-not-supported)\t Add support for the measure=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)vertexVertex__networkReference__within, __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up
	return init__s_kt_matrix_ccm_matrixBased(); //! ie, as 'lgocis' were Not comptued.
      }
    }
    //! 
    { //! Idenitfy "between": 
      //! ------
      if(
	 (vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex)
	 ||
	 (vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) //! where latter 'case' is handled in [below] to 'target' the test-eval-case where all valeus in a clsuter hav ehte same valeu-scores.
	 ) {
#define __Mi__update__within  0
#define __Mi__update__between 1
	// printf("##\tat %s:%d\n", __FILE__, __LINE__);
#include "kt_matrix_cmpCluster__ccmMatrix__betweenScore__sum.c" //! ie, apply logics:	
      } else if(
		(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidVertex)
		)
	{
	  //printf("##\tat %s:%d\n", __FILE__, __LINE__);
	//! Note[idea]: find the distnace from each cenotrid to the assicated vertices, ie, perform a [cluster_id][cluster_id-->verticesInSubset] iteration:
	//for(uint row_id = 0; row_id < self->nrows; row_id++) {
	//!
	//! Iterate: for each cluster-combination:
	const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	for(uint cluster_id_in = 0; cluster_id_in < cnt_clusters; cluster_id_in++) {
	  t_float *row_centroid = NULL;
	  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	    uint vertex_centroid_id = UINT_MAX;
	    assert(obj_result_clusterToVertex.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in];
	    assert(vertex_centroid_id != UINT_MAX);
	    assert(vertex_centroid_id < self->nrows);
	    row_centroid = self->input_matrix[vertex_centroid_id];
	    assert(row_centroid);
	  } else {
	    // assert(obj_result_clusterToVertex.mapOf_rowIds);
	    /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	    /* assert(vertex_centroid_id != UINT_MAX); */
	    assert(obj_result_clusterToVertex.matrix);
	    row_centroid = obj_result_clusterToVertex.matrix[cluster_id_in];
	    assert(row_centroid);	    
	  }
	  //assert(vertex_centroid_id != UINT_MAX;
	  //!
	  //! A squared iteration through the clusters:
	  for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	    if(cluster_id_out == cluster_id_in) {continue;} //! ie, as we are Not itnerested in a lsef---within-comparison.
	    uint mappings_cluster2_size = 0; uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size);
	    if(mappings_cluster2_size > 0) {
	      assert(mappings_cluster2); assert(self->input_matrix);
	      uint cnt_udpated = 0;
	      //! Identify the 'default' score:    
	      t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:
	      //!
	      //! Iterate:
	      for(uint i = 0; i < mappings_cluster2_size; i++) {
		const uint ind1 = mappings_cluster2[i]; assert(ind1 != UINT_MAX);
		assert(row_centroid);
		t_float score = row_centroid[ind1]; 
		if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
		  score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, self->input_matrix[ind1], row_centroid);
		}
		if(score != T_FLOAT_MAX) {
		  const t_float candidate_score = score;
		  //sumOf_result__between += innerClusterDist__between;
		  //! Udpate the score:
		  min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used.
		}
	      }
	      // TODO[future]: consider adding supprot tfor the 'case' where [”elow] 'caluse' is 'used' 'for the complete set of clsuters'.
	      if(cnt_udpated > 0) {
		__clusterConsistency__postProcessScore(); 
		assert(min_score != T_FLOAT_MAX);
		sumOf_result__between += min_score;
		ret_obj.between_min = macro_min(ret_obj.between_min, min_score);
		ret_obj.between_max = macro_max(ret_obj.between_max, min_score);
	      }
	    }
	  }
	}
      } else if(
		(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexCentroid)
		)
	{
	  //printf("##\tat %s:%d\n", __FILE__, __LINE__);
	  //! Note[idea]: find the distnace from each vertex to the 'centorid in the other clsuters', ie, perform a [cluster_id-->verticesInSubset][cluster_id] iteratio:n
	//!
	//! Iterate: for each cluster-combination:
	const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	for(uint cluster_id_in = 0; cluster_id_in < cnt_clusters; cluster_id_in++) {
	  //assert(vertex_centroid_id != UINT_MAX;
	  uint mappings_cluster1_size = 0; uint *mappings_cluster1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster1_size);
	  if(mappings_cluster1_size > 0) {
	    assert(mappings_cluster1); assert(self->input_matrix);
	    //!
	    //! Iterate:
	    for(uint i = 0; i < mappings_cluster1_size; i++) {
	      const uint ind1 = mappings_cluster1[i]; assert(ind1 != UINT_MAX);
	      uint cnt_udpated = 0;
	      //! Identify the 'default' score:    
	      t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:
	      //!
	      //! Iterate through each cluster:
	      for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {		
		t_float *row_centroid = NULL;
		if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
		  uint vertex_centroid_id = UINT_MAX;
		  assert(obj_result_clusterToVertex.mapOf_rowIds);
		  vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_out];
		  assert(vertex_centroid_id != UINT_MAX);
		  assert(vertex_centroid_id < self->nrows);
		  row_centroid = self->input_matrix[vertex_centroid_id];
		  assert(row_centroid);
		} else {
		  // assert(obj_result_clusterToVertex.mapOf_rowIds);
		  /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
		  /* assert(vertex_centroid_id != UINT_MAX); */
		  assert(obj_result_clusterToVertex.matrix);
		  row_centroid = obj_result_clusterToVertex.matrix[cluster_id_out];
		  assert(row_centroid);	    
		}
		assert(row_centroid);
		t_float score = row_centroid[ind1]; 
		if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
		  score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, self->input_matrix[ind1], row_centroid);
		}
		if(score != T_FLOAT_MAX) {
		  const t_float candidate_score = score;
		  //sumOf_result__between += innerClusterDist__between;
		  //! Udpate the score:
		  min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used.
		}
	      }
	      // TODO[future]: consider adding supprot tfor the 'case' where [”elow] 'caluse' is 'used' 'for the complete set of clsuters'.
	      if(cnt_udpated > 0) {
		__clusterConsistency__postProcessScore(); 
		assert(min_score != T_FLOAT_MAX);
		sumOf_result__between += min_score;
		ret_obj.between_min = macro_min(ret_obj.between_min, min_score);
		ret_obj.between_max = macro_max(ret_obj.between_max, min_score);
	      }
	    } 
	  }
	}
      } else if(
		//(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid) //! where we 'handle this as a vertex-->global', ie, as a 'within-clsuter' only contains/hold/describes one centid-vertex..
		(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal)
		) {
	//    printf("##\tat %s:%d\n", __FILE__, __LINE__);
	if( 
	   (/*typeOf_centroidAlg=*/metric_centroid == e_kt_api_dynamicKMeans_medoid) ||
	   obj_result_clusterToVertex__globalCenter.matrix
	    ) {
	  //! Note[idea]: find the distance from each vertex to the 'global center-point':
	  t_float *row_centroid = NULL;
	  const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	    uint vertex_centroid_id = UINT_MAX;
	    assert(obj_result_clusterToVertex__globalCenter.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex__globalCenter.mapOf_rowIds[/*cluster_id_in=*/0];
	    assert(vertex_centroid_id != UINT_MAX);
	    assert(vertex_centroid_id < self->nrows);
	    row_centroid = self->input_matrix[vertex_centroid_id];
	    assert(row_centroid);
	  } else {
	    // assert(obj_result_clusterToVertex.mapOf_rowIds);
	    /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	    /* assert(vertex_centroid_id != UINT_MAX); */
	    assert(obj_result_clusterToVertex__globalCenter.matrix);
	    row_centroid = obj_result_clusterToVertex__globalCenter.matrix[/*cluster_id_in=*/0];
	    assert(row_centroid);	    
	  }
	  //!
	  //! Iterate through each vertex and 'idneitfy' the distance to the glboal-center-vertex:
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {
	    const uint ind1 = row_id;
	    t_float score = row_centroid[row_id]; 
	    if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
	      score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, self->input_matrix[ind1], row_centroid);
	    }	  
	    if(isOf_interest(score)) {
	      sumOf_result__between += score;
	      ret_obj.between_min = macro_min(ret_obj.between_min, score);
	      ret_obj.between_max = macro_max(ret_obj.between_max, score);
	    }
	  }
	} //! else we asusem that not any global-centrailty-data were dineifed/found (oekseth, 06. otk. 2017).	
      } else if(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid) {
	//	printf("##\tat %s:%d\n", __FILE__, __LINE__);
	//! Note[idea]: find the distance between centroids:
	const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	for(uint cluster_id_in = 0; cluster_id_in < cnt_clusters; cluster_id_in++) {
	  t_float *row_centroid = NULL;
	  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	    uint vertex_centroid_id = UINT_MAX;
	    assert(obj_result_clusterToVertex.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in];
	    assert(vertex_centroid_id != UINT_MAX);
	    assert(vertex_centroid_id < self->nrows);
	    row_centroid = self->input_matrix[vertex_centroid_id];
	    assert(row_centroid);
	  } else {
	    // assert(obj_result_clusterToVertex.mapOf_rowIds);
	    /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	    /* assert(vertex_centroid_id != UINT_MAX); */
	    assert(obj_result_clusterToVertex.matrix);
	    row_centroid = obj_result_clusterToVertex.matrix[cluster_id_in];
	    assert(row_centroid);	    
	  }
	  uint cnt_udpated = 0;
	      //! Identify the 'default' score:    
	  t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:
	  //!
	  //! A squared iteration through the clusters:
	  for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	    if(cluster_id_out == cluster_id_in) {continue;} //! ie, as we are Not itnerested in a lsef---within-comparison.
	    t_float *row_centroid_out = NULL;
	    if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	      uint vertex_centroid_id = UINT_MAX;
	      assert(obj_result_clusterToVertex.mapOf_rowIds);
	      vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_out];
	      assert(vertex_centroid_id != UINT_MAX);
	      assert(vertex_centroid_id < self->nrows);
	      row_centroid_out = self->input_matrix[vertex_centroid_id];
	      assert(row_centroid_out);
	      assert(row_centroid); 
	      assert(row_centroid != row_centroid_out); // TODO: vlaidte this asseriton ... and cosndier adding a 'handler-loigcs' for such a case.
	    } else {
	      // assert(obj_result_clusterToVertex.mapOf_rowIds);
	      /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	      /* assert(vertex_centroid_id != UINT_MAX); */
	      assert(obj_result_clusterToVertex.matrix);
	      row_centroid_out = obj_result_clusterToVertex.matrix[cluster_id_out];
	      assert(row_centroid_out);	    
	      assert(row_centroid);
	      assert(row_centroid != row_centroid_out); // TODO: vlaidte this asseriton ... and cosndier adding a 'handler-loigcs' for such a case.
	    }
	    assert(row_centroid_out != row_centroid);
	    assert(row_centroid);
	    t_float score = 0;
	    if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
	      score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, row_centroid, row_centroid_out);
	      if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
		// printf("\t\t\t score=%f, given centroid(%u) x centroid(%u), at %s:%d\n", score, obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in], obj_result_clusterToVertex.mapOf_rowIds[cluster_id_out], __FILE__, __LINE__);
	      } else {
		//printf("\t\t\t score=%f, given centroid(%u) x centroid(%u), at %s:%d\n", score, cluster_id_in, cluster_id_out, __FILE__, __LINE__);
		if(false) { //! then write out the input-matrix: 
		  printf("row1=[");		    
		  for(uint i = 0; i < cnt_vertices; i++) {
		    printf("%f,", row_centroid[i]);
		    //printf("vertex[%u] = %u, at %s:%d\n", i, obj_2.map_clusterMembers[i], __FILE__, __LINE__);
		  }
		  printf("], at %s:%d\n", __FILE__, __LINE__);
		  printf("row2=[");		    
		  for(uint i = 0; i < cnt_vertices; i++) {
		    printf("%f,", row_centroid_out[i]);
		    //printf("vertex[%u] = %u, at %s:%d\n", i, obj_2.map_clusterMembers[i], __FILE__, __LINE__);
		  }
		  printf("], at %s:%d\n", __FILE__, __LINE__);
		}
	      }
	    } else { //! then we comptue the symemtic-total-distnace between the vectors:
	      // TOOD[aritlce]: consider using a differnet funciotn that [below] for 'meringtin' two distance-vectors:
	      t_float score_1 = 0;
	      for(uint i = 0; i < nrows; i++) {
		if(isOf_interest(row_centroid[i])) {score_1 += row_centroid[i];}}
	      t_float score_2 = 0;
	      for(uint i = 0; i < nrows; i++) {
		if(isOf_interest(row_centroid_out[i])) {score_2 += row_centroid_out[i];}}
	      score = 0.5*score_1 + 0.5*score_2;
	      // printf("\t\t\t score=%f, at %s:%d\n", score, __FILE__, __LINE__);
	    }
	    if( (score != T_FLOAT_MAX) && (score != 0) ) {
	      const t_float candidate_score = score;
	      //sumOf_result__between += innerClusterDist__between;
	      //! Udpate the score:
	      // printf("\t\t\tmin_score=%f, candidate_score=%f, at %s:%d", min_score, candidate_score, __FILE__, __LINE__);
	      min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used.
	    }
	  }
	  //!
	  // TODO[future]: consider adding supprot tfor the 'case' where [”elow] 'caluse' is 'used' 'for the complete set of clsuters'.
	  if(cnt_udpated > 0) {
	    __clusterConsistency__postProcessScore(); 
	    assert(min_score != T_FLOAT_MAX);
	    sumOf_result__between += min_score;
		ret_obj.between_min = macro_min(ret_obj.between_min, min_score);
		ret_obj.between_max = macro_max(ret_obj.between_max, min_score);
	  }
	}
      } else if(
		(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal) 
		|| (vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal__mulByWeight) 
		) {	
	if(
	   (/*typeOf_centroidAlg=*/metric_centroid == e_kt_api_dynamicKMeans_medoid) ||
	   obj_result_clusterToVertex__globalCenter.matrix 	   )
	{
	  //    printf("##\tat %s:%d\n", __FILE__, __LINE__);
	  //! Note[idea]: iterate through the clusters and comopute the score:
	  // TODO: consider merigng 'this' with the within-dsitance-comtpaution.
	  const bool multByCluster_size = (vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidGlobal__mulByWeight);
	  // TODO[future]: condier adding support for the "e_kt_matrix_cmpCluster_clusterDistance__config_metric__t" config-rpoerpty .... ie, 'add sperately' for "within" and "between" ... and then update our difnfernet CCMs.
	  t_float *row_centroid = NULL;
	  const e_kt_api_dynamicKMeans_t typeOf_centroidAlg = metric_centroid;
	  if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	    uint vertex_centroid_id = UINT_MAX;
	    assert(obj_result_clusterToVertex__globalCenter.mapOf_rowIds);
	    vertex_centroid_id = obj_result_clusterToVertex__globalCenter.mapOf_rowIds[/*cluster_id_in=*/0];
	    assert(vertex_centroid_id != UINT_MAX);
	    assert(vertex_centroid_id < self->nrows);
	    row_centroid = self->input_matrix[vertex_centroid_id];
	    assert(row_centroid);
	  } else {
	    // assert(obj_result_clusterToVertex.mapOf_rowIds);
	    /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	    /* assert(vertex_centroid_id != UINT_MAX); */
	    assert(obj_result_clusterToVertex__globalCenter.matrix);
	    row_centroid = obj_result_clusterToVertex__globalCenter.matrix[/*cluster_id_in=*/0];
	    assert(row_centroid);	    
	  }
	  //!
	  //! Iterate through each cluster:	
	  for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	    t_float *row_centroid_out = NULL;
	    if(typeOf_centroidAlg == e_kt_api_dynamicKMeans_medoid) { 
	      uint vertex_centroid_id = UINT_MAX;
	      assert(obj_result_clusterToVertex.mapOf_rowIds);
	      vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_out];
	      assert(vertex_centroid_id != UINT_MAX);
	      assert(vertex_centroid_id < self->nrows);
	      row_centroid_out = self->input_matrix[vertex_centroid_id];
	      assert(row_centroid_out);
	    } else {
	      // assert(obj_result_clusterToVertex.mapOf_rowIds);
	      /* vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
	      /* assert(vertex_centroid_id != UINT_MAX); */
	      assert(obj_result_clusterToVertex.matrix);
	      row_centroid_out = obj_result_clusterToVertex.matrix[cluster_id_out];
	      assert(row_centroid_out);	    
	    }
	    assert(row_centroid_out != row_centroid);
	    assert(row_centroid);
	    t_float score = 0;
	    if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) {
	      score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, cnt_vertices, row_centroid, row_centroid_out);
	      if(score != T_FLOAT_MAX) {
		if(multByCluster_size) { //! then we muliply by the number of vertices in the cluyster:
		  uint mappings_cluster2_size = 0; uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size);
		  assert(mappings_cluster2_size != UINT_MAX);
		  score = score*mappings_cluster2_size;
		}
	      }
	      //    }	  
	    } else { //! then we comptue the symemtic-total-distnace between the vectors:
	    
	    //!
	    //! Note: in ”[elow] we comptue/evaluate the dsitance between two centrodis, hence ....  (oekseth, 06. jun. 2017)

	    /* assert(false); // FIXME[coentpaul]: ....  */
	    /* assert(false); // FIXME[coentpaul]: .... valdiate [”elow] ... ie, correcness of using the 'total nrows" count ... repalce by 'ncols' ... validate correcntress ...  */

	    /* // FIXME: isntead add a new option/paramete rto dinetify/known if the innptu is an ajcuency-atmrix .... and udapte our cofnigruation-desipcriont-aritlce wrt. teh/such an itnerptaiton-topology ...  */
	    /* if(nrows == ncols) { */
	    /*   assert(self->mapOf_vertexClusterIds);  */
	    /*   const uint cluster_id_in = self->mapOf_vertexClusterIds[row_centroid */
	    /*   uint mappings_cluster1_size = 0; uint *mappings_cluster1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster1_size); */
	    /*   uint mappings_cluster2_size = 0; uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size); */
	    /*   assert(mappings_cluster2_size > 0); assert(mappings_cluster1_size > 0); */
	    /*   for(uint i = 0; i < mappings_cluster1_size; i++) { */
	    /* 	const uint ind1 = mappings_cluster1[i]; assert(ind1 != UINT_MAX); */
	    /* 	assert(ind1 < nrows); */
	    /* 	for(uint m = 0; m < mappings_cluster2_size; m++) { */
	    /* 	  const uint ind2 = mappings_cluster2[m]; assert(ind2 != UINT_MAX); */
	    /* 	  assert(ind2 < ncols); */
	    /* 	  const t_float score = self->input_matrix[ind1][ind2]; */
	    /* 	  if(isOf_interest(score)) { */
	    /* 	    sumOf_result__between += score; */
	    /* 	    ret_obj.between_min = macro_min(ret_obj.between_min, score); */
	    /* 	    ret_obj.between_max = macro_max(ret_obj.between_max, score); */
	    /* 	  } */
	    /* 	} */
	    /*   } */
	    /* } else { //! then we asusem inptu is a feature-amtrix: */
	      //assert(nrows == ncols); //! ie,as we expect an adjcnecy-amtrix at this posion.
	      
	      // TOOD[aritlce]: consider using a differnet funciotn that [below] for 'meringtin' two distance-vectors:
	      t_float score_1 = 0;
	      for(uint i = 0; i < nrows; i++) {
		if(isOf_interest(row_centroid[i])) {score_1 += row_centroid[i];}}
	      t_float score_2 = 0;
	      for(uint i = 0; i < nrows; i++) {
		if(isOf_interest(row_centroid_out[i])) {score_2 += row_centroid_out[i];}}
	      score = 0.5*score_1 + 0.5*score_2;
	    }	    
	    if( (score != T_FLOAT_MAX) && (score != 0) ) {
	      sumOf_result__between += score;
	      ret_obj.between_min = macro_min(ret_obj.between_min, score);
	      ret_obj.between_max = macro_max(ret_obj.between_max, score);
	    }
	    //	}	  
	} 
	} //! else we assuem that no cluster-cneotrid wehre inferred (oekseth, 06. otk. 2017) 
      } else {
	fprintf(stderr, "!!\t(option-not-supported)\t Add support for the measure=%u. If the latter message seems odd, then please give the senior devleoper an heads-up at [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", (uint)vertexVertex__networkReference__between, __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up
	return init__s_kt_matrix_ccm_matrixBased(); //! ie, as 'lgocis' were Not comptued.
	//return false; //! ie, as 'lgocis' were Not comptued.
      }
    }


    //!
    //! Dea-llcoat elocally reserved memory:
    free__s_kt_matrix(&obj_result_clusterToVertex);
    free__s_kt_matrix(&obj_result_clusterToVertex__globalCenter);
  } else { //! then we assume that the inti-funciton (for our "self" is designed to) accurately/correatly manages to 'capture' core-proeprties of the user-requested simlairty-metric.
    //! Iterate for $|C|x|C|$ adn apply logics: use the "self->cluster_cluster" to fetc/get the resutls.
    //    printf("##\tat %s:%d\n", __FILE__, __LINE__);
#define __Mi__update__within 1
#define __Mi__update__between 1
#define __Mi_isToUpdate__s_kt_matrix_ccm_matrixBased_t 1
#include "kt_matrix_cmpCluster__ccmMatrix__betweenScore__sum.c" //! ie, apply logics:
  }
  //! ------------------------------------------------------------------------------
  //!
  //! 
  if(current1dList) {free_1d_list_float(&current1dList); current1dList = NULL;} //! then de-allcoat ethe lcoal list of scores.
