char stringOf_resultPrefix[3000] = {'\0'}; sprintf(stringOf_resultPrefix, "clusterCollectionCategory_%s.nrows_%u", copmuteClusterSim__prefix, matrixOf_gold__corr.nrows);
const uint cnt_clustersPredictionsToEvaluate = __computeclusterSim__inputObj.nrows;
t_float **matrix_local = __computeclusterSim__inputObj.matrix;
//!
//! Build a uint-representaiton of the numbers:
uint **clusterPredictionDataSets__set2 = allocate_2d_list_uint(cnt_clustersPredictionsToEvaluate, matrixOf_gold__corr.ncols, def_0);  
for(uint i = 0; i < cnt_clustersPredictionsToEvaluate; i++) {   for(uint k = 0; k < matrixOf_gold__corr.nrows; k++) {clusterPredictionDataSets__set2[i][k] = matrix_local[i][k];}}

//!
//! Build a gold-standard:
//! Note: we expec tthe data-sets to ahve been constructed with an 'icnreased ambutity', ie, for which we 'cosntruct'/asisgn the gold-standard suing a 'reversed rank':
t_float *arrOf_1d_dataXdata_goldStandard = allocate_1d_list_float(cnt_clustersPredictionsToEvaluate, def_0);
t_float current_rank = cnt_clustersPredictionsToEvaluate -1 ;
for(uint i = 0; i < cnt_clustersPredictionsToEvaluate; i++) { arrOf_1d_dataXdata_goldStandard[i] = current_rank--;}

//!
//! The call:
standAlone__describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__s_kt_assessPredictions_t
//! Configure:
(
 /*cnt_vertices=*/matrixOf_gold__corr.ncols, /*cnt_clusters=*/nclusters_ideal, stringOf_resultPrefix,
 clusterPredictionDataSets__set1, /*cnt-clusterCases=*/1, 
 clusterPredictionDataSets__set2, /*cnt-clusterCases=*/cnt_clustersPredictionsToEvaluate, 
 arrOf_1d_dataXdata_goldStandard, 
 /*squaredDistanceMatrix__1*/matrixOf_gold__corr.matrix,
 NULL, NULL,
 config_min_rankThreshold,
 /*obj_result=*/__computeclusterSim__resultObj
 );
//!
//! De-allcoate a locally rserved matrix:
assert(clusterPredictionDataSets__set2);  free_2d_list_uint(&clusterPredictionDataSets__set2, cnt_clustersPredictionsToEvaluate); clusterPredictionDataSets__set2 = NULL;
assert(arrOf_1d_dataXdata_goldStandard); free_1d_list_float(&arrOf_1d_dataXdata_goldStandard); arrOf_1d_dataXdata_goldStandard = NULL;
#undef __computeclusterSim__inputObj
#undef __computeclusterSim__resultObj

