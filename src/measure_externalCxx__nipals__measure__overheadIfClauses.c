//! ---------
    { const char *stringOf_measureText_base = "artimetics-on-cells which demonstrates overhead wrt. using explicit if-sentences to evalaute correlation-metric-results";
      //! -------------------------------

      //!
      // 
      for(uint enum_id = 0; enum_id < e_typeOf_metric_correlation_pearson_undef; enum_id++) {
	for(char transpose = 0; transpose < 2; transpose++) {
	  for(char use_weight = 0; use_weight < 2; use_weight++) {
	    for(char use_mask = 0; use_mask < 3; use_mask++) {
	      for(char useTwoInputMatrices = 0; useTwoInputMatrices < 2; useTwoInputMatrices++) {
		t_float **matrix_1 = matrix_float; 
		t_float **matrix_2 = (useTwoInputMatrices == false) ? matrix_float : matrix_float_2;
		
		char **mask_1 = (use_mask == 1) ? matrix_char : NULL; 
		char **mask_2 = (use_mask == 1) ? (useTwoInputMatrices == false) ? matrix_char : matrix_char_2 : NULL; 
		t_float *weigth = (use_weight) ? arrOf_weight : NULL; 
		bool needTo_useMask_evaluation = (use_mask == 2); 
		e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation = (e_typeOf_metric_correlation_pearson_t)enum_id;
		//! Start the clock:
		char stringOf_measureText[2000] = {0};
		sprintf(stringOf_measureText, "%s at enum=%u and transpose=%s mask=%s  use-weight=%s implicit-mask=%s use-different-input-matrices=%s", stringOf_measureText_base, enum_id, 
			(transpose) ? "yes" : "no",
			(use_weight) ? "yes" : "no",
			(use_mask) ? "yes" : "no",
			(needTo_useMask_evaluation) ? "yes" : "no",
			(useTwoInputMatrices) ? "yes" : "no" //! where 'this' aprameter is of interest wrt. the 'ability' (eg, emasured wrt. time-perofmranc-eovehread) of the memroy-cahce-prefetcher to handle differnet inptu-amtrices.
			);
		start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
		const uint iterationIndex_2 = UINT_MAX;
		compute_score_correlation_forMatrix(size_of_array, size_of_array, matrix_1, matrix_2, mask_1, mask_2, matrix_result_float, transpose, weigth, typeOf_metric_correlation, needTo_useMask_evaluation, iterationIndex_2);
		//! Update the result-container:
		__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
	      }
	    }
	  }
	}
      }
    }
