#ifndef kt_matrix_setOf_h
#define kt_matrix_setOf_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_matrix_setOf
   @brief a wrapper to hold a list of s_kt_matrix_t elements (oekseth, 06. jan. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_matrix.h"

/**
   @struct s_kt_matrix_setOf
   @brief an ojbect to hold a number of s_kt_matrix_t objects (oekseth, 06. jan. 2017)
 **/
typedef struct s_kt_matrix_setOf {
  s_kt_matrix_t *list;
  uint list_size;
} s_kt_matrix_setOf_t;

/**
   @brief initiates the s_kt_matrix_setOf_t object  (oekseth, 06. jan. 2017).
   @param <list_size> is the nubmer of s_kt_matrix_setOf_t objects to allocate
   @param <nrows> is the number of rows to allocate in each s_kt_matrix_t object
   @param <ncols> is the number of columns to allocate in each s_kt_matrix_t object
   @return a new-intiated object
 **/
s_kt_matrix_setOf_t initAndReturn__s_kt_matrix_setOf_t(const uint list_size, const uint nrows, const uint ncols);

//! De-allcoates the s_kt_matrix_setOf_t object (oekseth, 06. jan. 2017)
void free__s_kt_matrix_setOf_t(s_kt_matrix_setOf_t *self);

/**
   @brief initiates a given s_kt_matrix_t matrix  (oekseth, 06. jan. 2017).
   @param <list_id> is the list-id ins_kt_matrix_setOf_t: should be less than self->list_size
   @param <nrows> is the number of rows to allocate in each s_kt_matrix_t object
   @param <ncols> is the number of columns to allocate in each s_kt_matrix_t object
   @return true fi the oepration was a scuccess.
 **/
bool setMatrixSize__s_kt_matrix_setOf_t(s_kt_matrix_setOf_t *self, const uint list_id, const uint nrows, const uint ncols);


#endif //! EOF
