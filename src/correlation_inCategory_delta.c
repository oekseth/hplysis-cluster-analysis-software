#include "correlation_inCategory_delta.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "correlation_inCategory_delta_tileMacros.h"

#if(VECTOR_FLOAT_ITER_SIZE == 4)  //! which we 'use' in order to simplify our testing of non-optmal-SSE-impelmetnations:
//! A slow 'version' of our "get_sumOf_values_denom_maskNotSet(..)"
t_float get_sumOf_values_denom_maskNotSet_slow_SSE(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1) {

  // FIXME[performance]: write a perofmrance-test using this funciton ... and then update our SSE-evaluation-strategy in our "clusterC_cmp.html" (eosketh, 06. sept. 2016).

  uint i = 0;
  t_float result_scalar_sum1 = 0; t_float result_scalar_denom1 = 0;
  if(ncols_intri > 0) {
    t_float result_sum1[4] = {0, 0, 0, 0};
    t_float result_denom1[4] = {0, 0, 0, 0}; 
    for(; i < ncols; i += 4) {
      VECTOR_FLOAT_TYPE mul = _mm_load_ps(&row[i]);
      if(weight) {mul = VECTOR_FLOAT_MUL(mul, VECTOR_FLOAT_SET1(weight[index1]));} 
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      VECTOR_FLOAT_STORE(result_sum1, VECTOR_FLOAT_ADD(_mm_load_ps(result_sum1), mul));
      //! Then muliple 'with it self':
      mul = VECTOR_FLOAT_MUL(mul, _mm_load_ps(&row[i]));
      VECTOR_FLOAT_STORE(result_denom1, VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOAD(result_denom1), mul));
    }
    //! Merge the sums:
    for(uint m = 0; m < 4; m++) {
      result_scalar_sum1 += result_sum1[m];
      result_scalar_denom1 += result_denom1[m];
    }

  }
  for(; i < ncols; i++) {
    const t_float mul = (weight) ? weight[index1]*row[i] : row[i];
    result_scalar_sum1 += mul;
    result_scalar_denom1 += (mul*row[i]);
  }
  
  //! @return:
  *scalar_denom1 = result_scalar_denom1;
  return result_scalar_sum1;
}
#endif

//! @return the sum of values for index1
t_float get_sumOf_values_denom_maskNotSet(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1) {
  uint i = 0;
  t_float result_scalar_sum1 = 0; t_float result_scalar_denom1 = 0;
  if(ncols_intri > 0) {
    
    VECTOR_FLOAT_TYPE vec_result_sum1;     VECTOR_FLOAT_TYPE vec_result_denom1;
    if(weight) {
      VECTOR_FLOAT_TYPE vec_weight = VECTOR_FLOAT_SET1(weight[index1]);
      //! ---
      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
      VECTOR_FLOAT_TYPE vec_mul   = VECTOR_FLOAT_MUL(vec_input, vec_weight);
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      vec_result_sum1 = vec_mul;
      //! Then muliple 'with it self':
      vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
      vec_result_denom1 = vec_mul;
      //! ---
      i += VECTOR_FLOAT_ITER_SIZE;
      for(; i < ncols; i += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
	VECTOR_FLOAT_TYPE vec_mul   = VECTOR_FLOAT_MUL(vec_input, vec_weight);
	//! Store the result in the temprorary container:
	// FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
	vec_result_sum1 = VECTOR_FLOAT_ADD(vec_result_sum1, vec_mul);
	//! Then muliple 'with it self':
	vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
	vec_result_denom1 = VECTOR_FLOAT_ADD(vec_result_denom1, vec_mul);
      }
    } else { //! then the "weights" are not set:      
      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
      VECTOR_FLOAT_TYPE vec_mul   = vec_input;
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      vec_result_sum1 = vec_mul;
      //! Then muliple 'with it self':
      vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
      vec_result_denom1 = vec_mul;
      //! ---
      i += VECTOR_FLOAT_ITER_SIZE;
      for(; i < ncols; i += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
	VECTOR_FLOAT_TYPE vec_mul   = vec_input;
	//! Store the result in the temprorary container:
	// FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
	vec_result_sum1 = VECTOR_FLOAT_ADD(vec_result_sum1, vec_mul);
	//! Then muliple 'with it self':
	vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
	vec_result_denom1 = VECTOR_FLOAT_ADD(vec_result_denom1, vec_mul);
      }

    }
    //! Merge the sums:
    result_scalar_sum1 = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_sum1);
    result_scalar_denom1 = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_denom1);
  }

  //! Then the non-SSE-iteration:
  if(weight) { 
    for(; i < ncols; i++) {
      const t_float mul = weight[index1]*row[i];
      result_scalar_sum1 += mul;
      result_scalar_denom1 += (mul*row[i]);
    }
  } else {
    for(; i < ncols; i++) {
      const t_float mul = row[i];
      result_scalar_sum1 += mul;
      result_scalar_denom1 += (mul*row[i]);
    }
  }
  
  //! @return:
  *scalar_denom1 = result_scalar_denom1;
  return result_scalar_sum1;
}



//! @return the sum of values for index1
t_float get_sumOf_values_denom_mask_implicit(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1) {
  uint i = 0;
  t_float result_scalar_sum1 = 0; t_float result_scalar_denom1 = 0;
#if(configure_SSE_useFor_mask_implict == 1)
  if(ncols_intri > 0) {
    
    VECTOR_FLOAT_TYPE vec_result_sum1;     VECTOR_FLOAT_TYPE vec_result_denom1;
    if(weight) {
      VECTOR_FLOAT_TYPE vec_weight = VECTOR_FLOAT_SET1(weight[index1]);
      //! ---
      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
      VECTOR_FLOAT_TYPE mask_ref; vec_input = VECTOR_FLOAT_dataMask_data1_vec(vec_input, mask_ref);
      VECTOR_FLOAT_TYPE vec_mul   = VECTOR_FLOAT_MUL(vec_input, vec_weight);
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      vec_result_sum1 = vec_mul;
      //! Then muliple 'with it self':
      vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
      //vec_mul = VECTOR_FLOAT_dataMask_data1_vec(vec_mul, vec_input); 
      vec_result_denom1 = vec_mul;
      //! ---
      i += VECTOR_FLOAT_ITER_SIZE;
      for(; i < ncols; i += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
	VECTOR_FLOAT_TYPE mask_ref; vec_input = VECTOR_FLOAT_dataMask_data1_vec(vec_input, mask_ref);
	VECTOR_FLOAT_TYPE vec_mul   = VECTOR_FLOAT_MUL(vec_input, vec_weight);
	//! Store the result in the temprorary container:
	// FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
	vec_result_sum1 = VECTOR_FLOAT_ADD(vec_result_sum1, vec_mul);
	//! Then muliple 'with it self':
	vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
	vec_result_denom1 = VECTOR_FLOAT_ADD(vec_result_denom1, vec_mul);
      }
    } else { //! then the "weights" are not set:      
      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
      VECTOR_FLOAT_TYPE mask_ref; vec_input = VECTOR_FLOAT_dataMask_data1_vec(vec_input, mask_ref);
      VECTOR_FLOAT_TYPE vec_mul   = vec_input;
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      vec_result_sum1 = vec_mul;
      //! Then muliple 'with it self':
      vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
      vec_result_denom1 = vec_mul;
      //! ---
      i += VECTOR_FLOAT_ITER_SIZE;
      for(; i < ncols; i += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
	VECTOR_FLOAT_TYPE mask_ref; vec_input = VECTOR_FLOAT_dataMask_data1_vec(vec_input, mask_ref);
	VECTOR_FLOAT_TYPE vec_mul   = vec_input;
	//! Store the result in the temprorary container:
	// FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
	vec_result_sum1 = VECTOR_FLOAT_ADD(vec_result_sum1, vec_mul);
	//! Then muliple 'with it self':
	vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
	vec_result_denom1 = VECTOR_FLOAT_ADD(vec_result_denom1, vec_mul);
      }

    }
    //! Merge the sums:
    result_scalar_sum1 = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_sum1);
    result_scalar_denom1 = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_denom1);
  }
#endif

  //! Then the non-SSE-iteration:
  if(weight) { 
    for(; i < ncols; i++) {
      if(isOf_interest(row[i])) {
	const t_float mul = weight[index1]*row[i];
	result_scalar_sum1 += mul;
	result_scalar_denom1 += (mul*row[i]);
      }
    }
  } else {
    for(; i < ncols; i++) {
      if(isOf_interest(row[i])) {
	const t_float mul = row[i];
	result_scalar_sum1 += mul;
	result_scalar_denom1 += (mul*row[i]);
      }
    }
  }
  
  //! @return:
  *scalar_denom1 = result_scalar_denom1;
  return result_scalar_sum1;
}

//! @return the sum of values for index1
t_float get_sumOf_values_denom_mask_explicit(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1, const char *row_mask) {
  uint i = 0;
  t_float result_scalar_sum1 = 0; t_float result_scalar_denom1 = 0;
#if(configure_SSE_useFor_mask_explicit == 1)
  if(ncols_intri > 0) {
    
    VECTOR_FLOAT_TYPE vec_result_sum1;     VECTOR_FLOAT_TYPE vec_result_denom1;
    if(weight) {
      VECTOR_FLOAT_TYPE vec_weight = VECTOR_FLOAT_SET1(weight[index1]);
      //! ---
      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
      VECTOR_FLOAT_TYPE vec_tmp; vec_input = VECTOR_FLOAT_charMask_data1_vec(vec_input, row_mask, i, vec_tmp);
      VECTOR_FLOAT_TYPE vec_mul   = VECTOR_FLOAT_MUL(vec_input, vec_weight);
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      vec_result_sum1 = vec_mul;
      //! Then muliple 'with it self':
      vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
      //vec_mul = VECTOR_FLOAT_dataMask_data1_vec(vec_mul, vec_input); 
      vec_result_denom1 = vec_mul;
      //! ---
      i += VECTOR_FLOAT_ITER_SIZE;
      for(; i < ncols; i += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
	VECTOR_FLOAT_TYPE vec_tmp; vec_input = VECTOR_FLOAT_charMask_data1_vec(vec_input, row_mask, i, vec_tmp);
	VECTOR_FLOAT_TYPE vec_mul   = VECTOR_FLOAT_MUL(vec_input, vec_weight);
	//! Store the result in the temprorary container:
	// FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
	vec_result_sum1 = VECTOR_FLOAT_ADD(vec_result_sum1, vec_mul);
	//! Then muliple 'with it self':
	vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
	vec_result_denom1 = VECTOR_FLOAT_ADD(vec_result_denom1, vec_mul);
      }
    } else { //! then the "weights" are not set:      
      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
      VECTOR_FLOAT_TYPE vec_tmp; vec_input = VECTOR_FLOAT_charMask_data1_vec(vec_input, row_mask, i, vec_tmp);
      VECTOR_FLOAT_TYPE vec_mul   = vec_input;
      //! Store the result in the temprorary container:
      // FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
      vec_result_sum1 = vec_mul;
      //! Then muliple 'with it self':
      vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
      vec_result_denom1 = vec_mul;
      //! ---
      i += VECTOR_FLOAT_ITER_SIZE;
      for(; i < ncols; i += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&row[i]);
	VECTOR_FLOAT_TYPE vec_tmp; vec_input = VECTOR_FLOAT_charMask_data1_vec(vec_input, row_mask, i, vec_tmp);
	VECTOR_FLOAT_TYPE vec_mul   = vec_input;
	//! Store the result in the temprorary container:
	// FIXME[JC]: may you udpate [”elow] after we have written an itrnsitnicts verison of the sum-macro?
	vec_result_sum1 = VECTOR_FLOAT_ADD(vec_result_sum1, vec_mul);
	//! Then muliple 'with it self':
	vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_input);
	vec_result_denom1 = VECTOR_FLOAT_ADD(vec_result_denom1, vec_mul);
      }

    }
    //! Merge the sums:
    result_scalar_sum1 = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_sum1);
    result_scalar_denom1 = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_denom1);
  }
#endif

  //! Then the non-SSE-iteration:
  if(weight) { 
    for(; i < ncols; i++) {
      if(row_mask[i]) {
	const t_float mul = weight[index1]*row[i];
	result_scalar_sum1 += mul;
	result_scalar_denom1 += (mul*row[i]);
      }
    }
  } else {
    for(; i < ncols; i++) {
      if(row_mask[i]) {
	const t_float mul = row[i];
	result_scalar_sum1 += mul;
	result_scalar_denom1 += (mul*row[i]);
      }
    }
  }
  
  //! @return:
  *scalar_denom1 = result_scalar_denom1;
  return result_scalar_sum1;
}



//! @return the assicated and computed correlation-score.
t_float compute_score_correlation(t_float result, const t_float sum1, const t_float sum2, const t_float tweight, t_float denom1, t_float denom2, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const uint sizeOf_i) {

  // TODO: replace 'this' by a macro, and then compare/evlauate the perofrmance-difference ... adn write an SSE-intri-verison. <-- currently 'not included' as teh tiem-cost is n*n while the 'tiem-cost' fo the caller is n*n*n, ie, optmizaitosn will result in in-signicant time-imrpoventemnts

  const char flag = (sum1 || sum2); // FIXME: validate that a cell with value=0 is not of interest.
  
#if(configure_generic_useMulInsteadOf_div == 1)
  assert(tweight != 0); //! ei, waht we then expect
  assert(sizeOf_i != 0);
  const t_float tweight_inv = 1 / tweight;
  const t_float sizeOf_i_inv = 1/sizeOf_i;
#endif

  if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_basic) {
#if(configure_generic_useMulInsteadOf_div == 1)
    return correlation_macros__distanceMeasures__postProcessCorrelation__pearson_basic(result, sum1, sum2, denom1, denom2, sizeOf_i, sizeOf_i_inv);
#else
    if(!tweight) return 0; /* usually due to empty clusters */
    return correlation_macros__distanceMeasures__postProcessCorrelation__pearson_basic__slow(result, sum1, sum2, denom1, denom2, sizeOf_i);
#endif
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_absolute) {
#if(configure_generic_useMulInsteadOf_div == 1)
    return correlation_macros__distanceMeasures__postProcessCorrelation__pearson_absolute(result, sum1, sum2, denom1, denom2, sizeOf_i, sizeOf_i_inv);
#else
    if(!tweight) return 0; /* usually due to empty clusters */
    //! Result-base = abs( sum( w(tail)*w(head_1,tail)*w(head_2,tail) ) - ( sum( w(tail)*w(head_1,tail) ) * (sum (w(tail)*w(head_2,tail) ) ) / sum(w(tail)) ) ) 
    return correlation_macros__distanceMeasures__postProcessCorrelation__pearson_absolute__slow(result, sum1, sum2, denom1, denom2, sizeOf_i);
#endif
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered) {
    return correlation_macros__distanceMeasures__postProcessCorrelation__pearson_unCentered(result, sum1, sum2, denom1, denom2);
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered_absolute) {
    return correlation_macros__distanceMeasures__postProcessCorrelation__pearson_unCentered_absolute(result, sum1, sum2, denom1, denom2);
  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_spearMan) {    
    //!  Compute and return the result:
#if(configure_generic_useMulInsteadOf_div == 1)
    return correlation_macros__distanceMeasures__postProcessCorrelation__spearman(result, denom1, denom2, sizeOf_i, sizeOf_i_inv);
#else
    return correlation_macros__distanceMeasures__postProcessCorrelation__spearman__slow(result, denom1, denom2, sizeOf_i);
#endif
  } else {
    assert(false); //! ie, then add support for this.
  }
}




//! Comptue the sum of elements 
static void compute_sumOf_values(const uint nrows, const uint ncols, t_float **data1, char **mask1, const t_float weight[], t_float *resultTable_sum1, t_float *resultTable_denom1, const bool transpose, const bool needTo_useMask_evaluation) {
  assert(resultTable_sum1 || resultTable_denom1); //! ie, as the call is otherwise pointless.

  // FIXME: iot. increase accuracy of our perofmranc-tests wrt. ...??... ... write an SSE-verison 'of this'. <-- move sub-parts of this funciton into seperate/different enums ... thereafter 'gneralize' these enums to 'allow' the use of fifferent valeu-ranges <-- instead use a differnet inptu-type than "t_float" ... and a new 'wrapper-set' for macro-names ... eg, to handlee 'sums-of-values' wrt. "unsigned short int"

  if(transpose == 0) {
    if(needTo_useMask_evaluation == false) {
      const uint ncols_intri = (ncols > 16) ? ncols - 4 : 0;
      for(uint index1 = 0; index1 < nrows; index1++) {
	//! Update:
	t_float scalar_denom1 = 0;
	resultTable_sum1[index1] = get_sumOf_values_denom_maskNotSet(index1, ncols, ncols_intri, data1[index1], weight, &scalar_denom1);
	resultTable_denom1[index1] = scalar_denom1;
      }
    } else {
      if(!mask1) {
	// FIXME[jc]: update [”elow] when we have written intrisnticts for the FLT_MAX case.
	for(uint index1 = 0; index1 < nrows; index1++) {
	  uint i = 0;
	  if(weight) {
	    for(; i < ncols; i++) {
	      const t_float term1 = data1[index1][i];
	      if(isOf_interest(term1))  {
		const t_float w = weight[i];
		if(resultTable_sum1) {
		  resultTable_sum1[index1] += w*term1;
		}
		if(resultTable_denom1) {
		  resultTable_denom1[index1] += w*term1*term1;
		}
	      }
	    }
	  } else {
	    for(; i < ncols; i++) {
	      const t_float term1 = data1[index1][i];
	      if(isOf_interest(term1))  {
		if(resultTable_sum1) {
		  resultTable_sum1[index1] += term1;
		}
		if(resultTable_denom1) {
		  resultTable_denom1[index1] += term1*term1;
		}
	      }
	    }
	  }
	}
      } else {      
	// FIXME[jc]: update [”elow] when we have written intrisnticts for the FLT_MAX case.
	for(uint index1 = 0; index1 < nrows; index1++) {
	  if(weight) {
	    for(uint i = 0; i < ncols; i++) {
	      const t_float term1 = data1[index1][i];
	      if(mask1[index1][i]) {
		const t_float w = weight[i];
		if(resultTable_sum1) {
		  resultTable_sum1[index1] += w*term1;
		}
		if(resultTable_denom1) {
		  resultTable_denom1[index1] += w*term1*term1;
		}
	      }
	    }
	  } else {
	    for(uint i = 0; i < ncols; i++) {
	      const t_float term1 = data1[index1][i];
	      if(mask1[index1][i]) {
		if(resultTable_sum1) {
		  resultTable_sum1[index1] += term1;
		}
		if(resultTable_denom1) {
		  resultTable_denom1[index1] += term1*term1;
		}
	      }
	    }
	  }
	}
      }
    }
  } else { //! then we comptue for the transposed.
    // FIXME: write a perofrmacne-benchamrk/test to evaluate the best memory-access-patterns of [”elow] ... ie, if 'such' matters.
    if(!mask1) {
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint i = 0; i < nrows; i++) {
	  const t_float term1 = data1[i][index1]; //! ie, a mewmory-access with a high cost.
	  if(isOf_interest(term1))  {
	    const t_float w = (weight) ? weight[i] : 1;
	    if(resultTable_sum1) {
	      resultTable_sum1[index1] += w*term1;
	    }
	    if(resultTable_denom1) {
	      resultTable_denom1[index1] += w*term1*term1;
	    }
	  }
	}
      }
    } else {      
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint i = 0; i < nrows; i++) {
	  const t_float term1 = data1[i][index1]; //! ie, a mewmory-access with a high cost.
	  if(mask1[i][index1]) {
	    const t_float w = (weight) ? weight[i] : 1;
	    if(resultTable_sum1) {
	      resultTable_sum1[index1] += w*term1;
	    }
	    if(resultTable_denom1) {
	      resultTable_denom1[index1] += w*term1*term1;
	    }
	  }
	}
      }
    } 
  }
}



//! Update, ie, adjust the score by the weight
void compute_score_correlation_forMatrix(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, t_float **resultMatrix, const bool transpose,  const t_float weight[], const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const bool needTo_useMask_evaluation, const uint iterationIndex_2) {

  //! Note: a compelxity in this approach si that the matrixes will not be symmetric if data1 != data2, ie, we 'causes' some extra if-calseus in [”elow].

  assert(nrows > 0); assert(ncols > 0); assert(resultMatrix); 
  // FIXME: validate correctness of [below].
  const uint size_ncols_nrows = (transpose == 0) ? /*size=*/nrows : /*size=*/ncols;    
  const uint size_ncols_nrows_2 = (iterationIndex_2 != UINT_MAX) ? /*size=*/iterationIndex_2 : /*size=*/size_ncols_nrows;    
  t_float *resultTable_sum1 = allocate_1d_list_float(size_ncols_nrows, /*default-value=*/0);
  t_float *resultTable_denom1 = allocate_1d_list_float(size_ncols_nrows, /*default-value=*/0);
  t_float *resultTable_sum2 = NULL; t_float *resultTable_denom2 = NULL;
  if(data1 != data2) {
    resultTable_sum2 = allocate_1d_list_float(size_ncols_nrows, /*default-value=*/0);
    resultTable_denom2 = allocate_1d_list_float(size_ncols_nrows, /*default-value=*/0);
  } else {
    resultTable_sum2 = resultTable_sum1;
    resultTable_denom2 = resultTable_denom1;
  }
  //! Comptue the assicated scores:
  compute_sumOf_values(nrows, ncols, data1, mask1, weight, resultTable_sum1, resultTable_denom1, transpose, needTo_useMask_evaluation);
  if(data1 != data2) {
    const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;
    compute_sumOf_values(nrows_2, ncols, data2, mask2, weight, resultTable_sum2, resultTable_denom2, transpose, needTo_useMask_evaluation);
  }
  const uint sizeOf_i = (transpose == 1) ? /*size=*/nrows : /*size=*/ncols;    
  //! Compute sum of weights:
  // //! Update, ie, adjust the score by the weight
  t_float tweight = 1;
  if(weight) {
    for(uint i = 0; i < sizeOf_i; i++)  {      
      tweight += weight[i];
    }
  }
  
  //#if(configure_generic_useMulInsteadOf_div == 1)
  if(tweight == 0) {
    const t_float default_value = (typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_spearMan) ? 1 : 0;
    for(uint index1 = 0; index1 < size_ncols_nrows; index1++) {
      for(uint index2 = 0; index2 < size_ncols_nrows_2; index2++) {
	resultMatrix[index1][index2] = default_value;
      }
    }
  } else
    //#endif
    //    if(tweight != 0)
 {
    //! Compute:    
   assert(tweight != 0);
    for(uint index1 = 0; index1 < size_ncols_nrows; index1++) {
      if(data1 == data2) {
	for(uint index2 = 0; index2 <= index1; index2++) {
	  // FIXME: write a macro-call for [below] when we have optimized ... and then use the call for other compute_score_correlation(..) function-calls
	  resultMatrix[index1][index2] = compute_score_correlation(resultMatrix[index1][index2], resultTable_sum1[index1], resultTable_sum1[index2], tweight, resultTable_denom1[index1], resultTable_denom2[index2], typeOf_metric_correlation, sizeOf_i);
	}
      } else {
	for(uint index2 = 0; index2 < size_ncols_nrows; index2++) {
	  resultMatrix[index1][index2] = compute_score_correlation(resultMatrix[index1][index2], resultTable_sum1[index1], resultTable_sum2[index2], tweight, resultTable_denom1[index1], resultTable_denom2[index2], typeOf_metric_correlation, sizeOf_i);
	}
      }
    }
  }

  if(data1 == data2) {
    //! Update the symmetirc 'items':
    //! Then we update the result, ie, as we only computed [above] for 'half of the entries':
    // FIXME: update [”elow] call when we know the 'best' perofrmacne-result-strategy.
    const uint size_ncols_nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : size_ncols_nrows;
    update_cells_above_firstFoorLoop( size_ncols_nrows, size_ncols_nrows_2, resultMatrix, transpose, /*isTo_use_speedTest_updateOrder_in_out=*/true, /*isTo_use_speedTest_inrisnistics=*/false);
  } //! elwe the matrix is nto expected to be symmetric.

  //! De-allcoate the locally reserve3d memory:
  free_1d_list_float(&resultTable_sum1);  free_1d_list_float(&resultTable_denom1);
  if(data1 != data2) {
    free_1d_list_float(&resultTable_sum2);  free_1d_list_float(&resultTable_denom2);
  }
}


//! Udate the result, ie, as we only computed [above] for 'half of the entries':
void update_cells_above_firstFoorLoop(const uint size_ncols_nrows, const uint size_ncols_nrows_2, t_float **resultMatrix, const bool transpose, const bool isTo_use_speedTest_updateOrder_in_out, const bool isTo_use_speedTest_inrisnistics) {

  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  // FIXME: update out perofmrance evlauations/benchmarks wrt. [below] ... testing (a) [below] cobmiantiosn and (b) use of intrisitncits.

  // if(isTo_use_speedTest_inrisnistics) {
  //   // FIXME: instead of [”elow] use _mm_storeu_ps .... ite, to dealy the "resultMatrix[index1][index2]" update
  //   assert(false); // FIXME: add soemthing ... first wriign a a 'sytnentic' benchamrk.
  // } else
    {
    //! Then we update the result, ie, as we only computed [above] for 'half of the entries':    
    if(isTo_use_speedTest_updateOrder_in_out) {
      if(transpose == 0) {
	assert(resultMatrix);
	//printf("|resultMatrix|=[%u, %u], at %s:%d\n", size_ncols_nrows, size_ncols_nrows_2, __FILE__, __LINE__);
	assert(size_ncols_nrows_2 != UINT_MAX);
	for(uint index1 = 0; index1 < size_ncols_nrows; index1++) {
	  for(uint index2 = index1+1; index2 < size_ncols_nrows_2; index2++) { //! where '+1' is used to avoid a 'self-comparisn'.
	    resultMatrix[index1][index2] = resultMatrix[index2][index1];
	  }
	}
      } else {
	for(uint index1 = 0; index1 < size_ncols_nrows; index1++) {
	  for(uint index2 = index1+1; index2 < size_ncols_nrows_2; index2++) { //! where '+1' is used to avoid a 'self-comparisn'.
	    resultMatrix[index1][index2] = resultMatrix[index2][index1];
	  }
	}
      }
    } else {
      if(transpose == 0) {
	for(uint index1 = 0; index1 < size_ncols_nrows; index1++) {
	  for(uint index2 = index1+1; index2 < size_ncols_nrows_2; index2++) { //! where '+1' is used to avoid a 'self-comparisn'.
	    resultMatrix[index2][index1] = resultMatrix[index1][index2];
	  }
	}
      } else {
	for(uint index1 = 0; index1 < size_ncols_nrows; index1++) {
	  for(uint index2 = index1+1; index2 < size_ncols_nrows_2; index2++) { //! where '+1' is used to avoid a 'self-comparisn'.
	    resultMatrix[index2][index1] = resultMatrix[index1][index2];
	  }
	}
      }
    }
  }
}



//! 
//! ---------------------------------------------------------
//!   Load the speciciatons for the optmized data-construction




// FIXME: write fucntion-wrappers for the "global_2dRows" function ... usign our to-be-written Perl-wrapper.
// FIXME[critical]: for the following distance-measures support "division by zero (and otehrwise Drop such support)": ...
// FIXME[critical]: for the following distance-measures support "power (ie, an extra argument)": Minkowski
// FIXME[critical]: for the following distance-measures support "max (instead of summing)":  Minkowski 
// FIXME[critical]: for the following distance-measures support "":  
// FIXME[critical]: for the following distance-measures support "":  





// FIXME: cirticla: ensure taht we use all of ª[bove] fucnitons in [below].





// FIXME: consider changing the names of these functiosn ... as the "nonSimd" 'name' is misleading .... instead write a global 'getter-funciton' ... and then use the same API/functioan-call-param 'for all of teh functions'.


/**
 @brief Then compute an 'inner square' of sums, where we expect all values to be of interest:
 @remarks:
 -- we compute correlations/distances/similarites for [index1 ... chunkSize_index1][index2 ... chunkSize_index2] += [index1 ... chunkSize_index1];
**/
void kt_computeTile_xmtPostPostCompute_notMask(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const uint SM, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) {
//void kt_computeTile_xmtPostPostCompute_notMask(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const s_allAgainstAll_config_t typeOf_metric, const uint SM, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) {

  

  // if(typeOf_metric == s_allAgainstAll_config_euclid) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_notMask__euclid(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_notMask__euclid__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   }
  // } else if(typeOf_metric == s_allAgainstAll_config_cityblock) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_notMask__cityBlock(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_notMask__cityBlock__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   }
  // // } else if(typeOf_metric == s_allAgainstAll_config_brownian_correlation) {
  // //   if(weight == NULL) {
  // //     kt_computeTile_xmtPostPostCompute_notMask__brownian(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  // //   } else {
  // //     kt_computeTile_xmtPostPostCompute_notMask__brownian__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  // //   }
  // } else if(typeOf_metric == s_allAgainstAll_config_spearman_or_correlation) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_notMask__spearman_or_correlation_sumOnly(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_notMask__spearman_or_correlation_sumOnly__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   }
  // } else {
  //   assert(false); // FIXME: add something .... need to write/add 'geneirc support' for the otehr distance-measures.
  // }


}





//! ---------------------------------------------------------------------------------------------------------------------------------------------------------
//! Then compute an 'inner square' of sums, where we expect all values to be of interest:
void kt_computeTile_xmtPostPostCompute_maskImplicit(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const uint SM, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) {
//void kt_computeTile_xmtPostPostCompute_maskImplicit(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const s_allAgainstAll_config_t typeOf_metric, const uint SM, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) {


  // if(typeOf_metric == s_allAgainstAll_config_euclid) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_mask_implicit__euclid(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_mask_implicit__euclid__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix,  SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   }
  // } else if(typeOf_metric == s_allAgainstAll_config_cityblock) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_mask_implicit__cityBlock(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_mask_implicit__cityBlock__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix,  SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   }
  // // } else if(typeOf_metric == s_allAgainstAll_config_brownian_correlation) {
  // //   if(weight == NULL) {
  // //     kt_computeTile_xmtPostPostCompute_mask_implicit__brownian(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  // //   } else {
  // //     kt_computeTile_xmtPostPostCompute_mask_implicit__brownian__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix,  SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  // //   }
  // } else if(typeOf_metric == s_allAgainstAll_config_spearman_or_correlation) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_mask_implicit__spearman_or_correlation_sumOnly(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_mask_implicit__spearman_or_correlation_sumOnly__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix,  SM,  /*mask1=*/NULL, /*mask2=*/NULL, listOf_inlineResults, self, complexResult);
  //   }
  // } else {
  //   assert(false); // FIXME: add something .... need to write/add 'geneirc support' for the otehr distance-measures.
  // }

  
}


//! -------------------------------------------------------------------------------------------------


//! Then compute an 'inner square' of sums, where we expect all values to be of interest:
void kt_computeTile_xmtPostPostCompute_maskExplicit(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const uint SM, char** mask1, char** mask2, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) 
//void kt_computeTile_xmtPostPostCompute_maskExplicit(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const s_allAgainstAll_config_t typeOf_metric, const uint SM, char** mask1, char** mask2, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) 
{



  // if(typeOf_metric == s_allAgainstAll_config_euclid) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_mask_explicit__euclid(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_mask_explicit__euclid__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  //   }
  // } else if(typeOf_metric == s_allAgainstAll_config_cityblock) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_mask_explicit__cityBlock(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_mask_explicit__cityBlock__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  //   }
  // // } else if(typeOf_metric == s_allAgainstAll_config_brownian_correlation) {
  // //   if(weight == NULL) {
  // //     kt_computeTile_xmtPostPostCompute_mask_explicit__brownian(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  // //   } else {
  // //     kt_computeTile_xmtPostPostCompute_mask_explicit__brownian__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  // //   }
  // } else if(typeOf_metric == s_allAgainstAll_config_spearman_or_correlation) {
  //   if(weight == NULL) {
  //     kt_computeTile_xmtPostPostCompute_mask_explicit__spearman_or_correlation_sumOnly(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  //   } else {
  //     kt_computeTile_xmtPostPostCompute_mask_explicit__spearman_or_correlation_sumOnly__weight(ncols, index1, index2, chunkSize_index1, numberOf_chunks_cols, chunkSize_index2, chunkSize_col_last, data1, data2, weight, resultMatrix, SM, mask1, mask2, listOf_inlineResults, self, complexResult);
  //   }
  // } else {
  //   assert(false); // FIXME: add something .... need to write/add 'geneirc support' for the otehr distance-measures.
  // } 
}




// FIXME: write a new class named "correlation_inCategory_rank_kendall" 
// FIXME: write a class named "correlation_api" ... and include "__optimal_compute_allAgainstAll_SIMD(..)" ... and incldue a 'wrapper' for "correlation_inCategory_rank_spearman"
