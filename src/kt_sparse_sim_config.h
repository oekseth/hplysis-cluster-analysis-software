#ifndef kt_sparse_sim_config_h
#define kt_sparse_sim_config_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_sparse_sim_config
   @brief a cofniguraitonw rt. our simliarty-based appraoch for sparse data-sets (oekseth, 06. mar. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

/**
   @enum e_kt_sparse_sim_config__howToHandleNoMatches
   @brief provide support for handling of cases where only a small subset of the elements matches (oekseth, 06. mar. 2017)
   @remarks this enum is designed to 'handle cases' where only a small subset of the elements matches, eg, for the use-case where "|v=pathway(i)|=100 and |v=pathway(i) cap y=protein(k)|=5". In the latter use-case, if we omit/discard the 'no-matches' from evaluation, the implicaiton is that 'a protein with only 5 common vertices (to a v=pathway with 100 vertices)' may be inferred to mreo simliar to 'v' than 'an x=protein with 70 common vertices'. In order to address the latter interpretation-cases we in our "kt_sparse_sim.h" provide mulitple configraution-options (for the latter). To proeprtly handle/designate the latter cases, we provide interpreation-options to: solve(a) distance*1 (ie, to assume that only intersections are to be considered: latter is the default appraoch for our matrix-based mask-appraoch); solve(b): distance*max(|v|, |y|); solve(c) distance*(sum(v)*sum(y)) (where we use the squared sum of possible distance-candidates to evaluate/udpate the distance-score); solve(d) distance*(sum(v)+sum(y)) (where we use the 'sum of scores' to evlauate the distance-simlairty-score). (For deatils wrt. the latter please see our "e_kt_sparse_sim_config__howToHandleNoMatches_t" in our "kt_sparse_sim_config.h").   
 **/
typedef enum e_kt_sparse_sim_config__howToHandleNoMatches {
  e_kt_sparse_sim_config__howToHandleNoMatches_none, 
  e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_maxCntElements, //! ie, max(|row1|, |row2|)
  e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_cntRow1AndRow2_multiply, //! ie, |row1|*|row2|
  e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_cntRow1AndRow2_pluss, //! ie, |row1|+|row2|
  /* e_kt_sparse_sim_config__howToHandleNoMatches,  */
  /* e_kt_sparse_sim_config__howToHandleNoMatches, */
  e_kt_sparse_sim_config__howToHandleNoMatches_undef
} e_kt_sparse_sim_config__howToHandleNoMatches_t;

/**
   @struct s_kt_sparse_sim_config
   @brief provide a cofniguraiton-object to oru "kt_sparse_sim" (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_sparse_sim_config {
  t_float threshold_min;
  t_float threshold_max;
  t_float configMetric_power; //! which is used for cases where a 'poer-metric' si requested.
  bool isTo_adjustBy_cntElements;
  bool isTo__useParallelExeuction;
  bool forMetricPreStep__sortUsingKeysAndNotVal; //! which is used 'when' then "e_kt_categoryOf_correaltionPreStep_rank" option is used/appliedo
  uint advanced__parallelConfig__maxCntBlocks__eachThread; //! which is used to configure outr parallel scheudling
  uint advanced__parallelConfig__maxCntBlocks__minRowCount; //! which is used to avoid 'paralle shcueling' for 'all to samll' dat-asets.
  e_kt_sparse_sim_config__howToHandleNoMatches_t specToHandle_noMatches;
} s_kt_sparse_sim_config_t;
//! @return an intlised object, an object which 'does Not use' any configuraiton-object-specs.
static s_kt_sparse_sim_config_t init__s_kt_sparse_sim_config_t() {
  s_kt_sparse_sim_config_t self;
  self.threshold_min = T_FLOAT_MIN_ABS;
  self.threshold_max = T_FLOAT_MAX;
  self.configMetric_power = 2.0;
  self.isTo_adjustBy_cntElements = false;
  // TODO: cosnider setting [”elow] option to true.
  self.isTo__useParallelExeuction = false; //! ie, to simplify debugging.
  self.forMetricPreStep__sortUsingKeysAndNotVal = true;
  self.advanced__parallelConfig__maxCntBlocks__eachThread = 4; //! which is used to configure outr parallel scheudling
  // TODO: expierment with [”elow]
  self.advanced__parallelConfig__maxCntBlocks__minRowCount = 100; //! which is used to configure outr parallel scheudling
  self.specToHandle_noMatches = e_kt_sparse_sim_config__howToHandleNoMatches_multiplyBy_maxCntElements;
  //! @return 
  return self;
}

#endif //! EOF
