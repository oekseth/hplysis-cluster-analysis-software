#include "kt_api_config.h"
#include "kt_longFilesHandler.h"

//! Intiate the "s_kt_api_config_t" object (oekseth, 06. des. 2016).
//#ifdef true //COMPILE_FULL
s_kt_api_config_t init__s_kt_api_config_t(bool isTo_transposeMatrix, bool isTo_applyDisjointSeperation, bool isTo_parallisaiton__sharedMemory, bool inputMatrix__isAnAdjecencyMatrix, s_kt_longFilesHandler *fileHandler)
//#else //! #ifdef COMPILE_FULL
// s_kt_api_config_t init__s_kt_api_config_t(bool isTo_transposeMatrix, bool isTo_applyDisjointSeperation, bool isTo_parallisaiton__sharedMemory, bool inputMatrix__isAnAdjecencyMatrix)
//#endif //! #ifdef COMPILE_FULL
{
  //#ifndef true //COMPILE_FULL
  // s_kt_longFilesHandler_t *fileHandler = NULL;
  //#endif // #ifdef COMPILE_FULL
  s_kt_api_config_t self;
  self.isTo_transposeMatrix = isTo_transposeMatrix;
  self.isTo_applyDisjointSeperation = isTo_applyDisjointSeperation;
  self.isTo_parallisaiton__sharedMemory = isTo_parallisaiton__sharedMemory;
  self.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
  self.in2dCorrMatrix__for_zeroMaskMetrics__applyOptimization = true; //! ie, which if set to "true" impleis that we replace the "T_FLOAT_MAX" with a '0' value for 'zero-case-metrics': for tdetails wrt. 'such metrics' please see our "mayReplace_maskWith__zero__s_kt_correlationMetric_t(..)"
  self.fileHandler = fileHandler;
  //! ------
  self.stringOf_exportResult__heatMapOf_clusters = NULL;
  //! -------------- 
  self.performance__testImapcactOf__slowPerformance = false; //! which is included to 'allow' an evlaution of the effect wrt. a non-improved applicaiton of distance-matrix-comptautions on data-sets.
  //! @return 
  return self;
}

//! A simplictc cofnigruation-object which xmt the tranpsoe-rpoerpty set the devfualt 'safe-gurard non-optmized' paramters (oekseth, 06. jan. 2017).
s_kt_api_config_t init__simple__s_kt_api_config_t(const bool isTo_transposeMatrix) {
 s_kt_api_config_t self;
  self.isTo_transposeMatrix = isTo_transposeMatrix;
  self.isTo_applyDisjointSeperation = true; // TODO: consider setting 'this to false' <-- currently we asusme that 'most uses' will be accessing the hpLyssi-software driectly, ie, hence 'the point' for this apramter.
  self.isTo_parallisaiton__sharedMemory = true; //isTo_parallisaiton__sharedMemory;
  self.inputMatrix__isAnAdjecencyMatrix = false;  //! ie, a safe-guard
  self.in2dCorrMatrix__for_zeroMaskMetrics__applyOptimization = true; //! ie, which if set to "true" impleis that we replace the "T_FLOAT_MAX" with a '0' value for 'zero-case-metrics': for tdetails wrt. 'such metrics' please see our "mayReplace_maskWith__zero__s_kt_correlationMetric_t(..)"
  self.fileHandler = NULL;
  //! ------
  self.stringOf_exportResult__heatMapOf_clusters = NULL;
  //! @return 
  return self;
}

