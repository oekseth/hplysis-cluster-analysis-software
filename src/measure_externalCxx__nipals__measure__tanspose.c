{
#undef __use_Result_to_updateTableComparisonResults
#define __use_Result_to_updateTableComparisonResults 1
  //! --------- new measurement:
  { //! test for different 'naive' strategies:
    { //! Float:
      const char *stringOf_measureText_case1 = "transpose:naive for type=float and update-sequence = input-->result";
      const char *stringOf_measureText_case2 = "transpose:naive for type=float and update-sequence = result-->input";
      t_float **matrix_input = matrix_float; 
      t_float **matrix_result = matrix_result_float;
#include "measure_externalCxx__nipals__func__testPerf__transpsoeNaive.c"
    }
    { //! Uint:
      const char *stringOf_measureText_case1 = "transpose:naive for type=uint and update-sequence = input-->result";
      const char *stringOf_measureText_case2 = "transpose:naive for type=uint and update-sequence = result-->input";
      uint **matrix_input = matrix_uint; 
      uint **matrix_result = matrix_result_uint;
#include "measure_externalCxx__nipals__func__testPerf__transpsoeNaive.c"
    }
    { //! Char:
      const char *stringOf_measureText_case1 = "transpose:naive for type=char and update-sequence = input-->result";
      const char *stringOf_measureText_case2 = "transpose:naive for type=char and update-sequence = result-->input";
      char **matrix_input = matrix_char; 
      char **matrix_result = matrix_result_char;
#include "measure_externalCxx__nipals__func__testPerf__transpsoeNaive.c"
    }
  }
  //! --------- new measurement:

  { //! Test for different 'pure-SSE' strategies:
    {
      const char *stringOf_measureText = "transpose:SSE for type=float, where we assume that the SSE-chunk 'covers' the compelete matrix";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      kt_mathMacros_SSE__transpose(size_of_array, size_of_array, matrix_float, matrix_result_float);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
    {
      const char *stringOf_measureText = "transpose:SSE for type=uint, where we assume that the SSE-chunk 'covers' the compelete matrix";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      kt_mathMacros_SSE__transpose_uint(size_of_array, size_of_array, matrix_uint, matrix_result_uint);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
    {
      const char *stringOf_measureText = "transpose:SSE for type=char, where we assume that the SSE-chunk 'covers' the compelete matrix";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      kt_mathMacros_SSE__transpose_char(size_of_array, size_of_array, matrix_char, matrix_result_char);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
  }
  //! --------- new measurement:
  { //! Test for different 'nonSSE-SSE' strategies, using logics/fucntions in oru "matrix_transpose.c"
    {
      const char *stringOf_measureText = "transpose:nonSSE--SSE for type=float, where we assume that the SSE-chunk 'covers' the compelete matrix";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      matrix__transpose__compute_transposedMatrix_float(size_of_array, size_of_array, matrix_float, matrix_result_float, /*isTo_useIntrisinistic=*/true);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
    {
      const char *stringOf_measureText = "transpose:nonSSE--SSE for type=uint, where we assume that the SSE-chunk 'covers' the compelete matrix";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      matrix__transpose__compute_transposedMatrix_uint(size_of_array, size_of_array, matrix_uint, matrix_result_uint, /*isTo_useIntrisinistic=*/true);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
    {
      const char *stringOf_measureText = "transpose:nonSSE--SSE for type=char, where we assume that the SSE-chunk 'covers' the compelete matrix";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      matrix__transpose__compute_transposedMatrix_char(size_of_array, size_of_array, matrix_char, matrix_result_char, /*isTo_useIntrisinistic=*/true);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
  }

}
