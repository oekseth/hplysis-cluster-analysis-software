#include "kt_clusterAlg_SVD.h"

/* //! Include the Lapack libraries: */
/* #include "lavd.h" */
/* #include "lasvd.h" */


#ifndef min
#define min(x, y)	((x) < (y) ? (x) : (y))
#endif
#ifndef max
#define	max(x, y)	((x) > (y) ? (x) : (y))
#endif


/**
   @brief Determine the singular value decomposition (SVD) by a real m by n rectangular matrix.
   @param <m> the number of rows of A (and u);
   @param <n> is the number of columns of A (and u) and the order of v;
   @param <u> contains the rectangular input matrix A to be decomposed: in the result represents/holds the orthogonal column vectors of the decomposition;
   @param <w> result: the non-negative diagonal singular values;
   @param <vt> result: the orthogonal column-matrix of the decomposition;
   @return true upon succsess.
**/
uint svd_LapackMatrixOperation(int m, uint n, float** u, float w[], float** vt)  {

  assert(false); // FXIEM[JC]: may you cindeu [below] wehn we have chosen a c-math-lbirary? (oekseth, 06. otjk. 2016)


  /* assert(m > 0); */
  /* assert(n > 0); */
  /* assert(u); */
  /* //! Construct a 'deep copy': */
  /* const double default_value_double = FLT_MAX; */
  /* const uint arrOf_2d_size = n*m; */
  /* double *arrOf_2d = allocate_1d_list_float_explicitType_double(arrOf_2d_size, default_value_double); // list_generic<double>::allocate_list(/\*size=*\/n * m, FLT_MAX, __FILE__, __LINE__); */
  /* assert(arrOf_2d); */
  /* { //! Construct a 'deep copy':  */
  /*   uint current_index = 0; */
  /*   for(uint i = 0; i < m; i++) { */
  /*     for(uint k = 0; k < n; k++) { */
  /* 	const float expected_value = u[0][current_index]; //! ie, as we expec the 'base-list' to be a 1d-list. */
  /* 	arrOf_2d[current_index] = expected_value; */
  /* 	//! We expect the dobule-poitner to abe a continous list, ie, 'try to provoke a memory-error': */
  /* 	// FIXME: udpate our code to verify [”elow] assumption. */
  /* 	assert(u[i][k] == expected_value); */
  /* 	current_index++; */
  /*     } */
  /*   } */
  /* } */
  
  /* // FIXME: in the compilation ensure that we manage to compile this using the "lapack" library (eg, at "/home/klatremus/poset_src/externalLibs/uffizi/CMakeLists.txt"). */
  /* // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description; */
  /* // FIXME: write an assert-class text-fucntion for this funciton. */
  /* // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption. */

  /* //! Construct objects described as "General Dense Rectangular uffizi_matrix Class" (http://lapackpp.sourceforge.net/html/classLaGenMatDouble.html#_details). */
  /* LaGenMatDouble laCorrMat(arrOf_2d, /\*cnt-rows=*\/m, /\*cnt-columns=*\/n); //(float *) corrMat.data(),(int)corrMat.Rows()); //(int)corrMat.Cols()); */
  /* LaGenMatDouble laEigenVecs(/\*cnt-rows=*\/m, /\*cnt-columns=*\/n); // which represents the 'sigma' matrix. */
  /* LaGenMatDouble la_u(/\*cnt-rows=*\/m, /\*cnt-columns=*\/n); */
  /* //! Construct an object described as "Real-valued Vector class" (http://lapackpp.sourceforge.net/html/classLaVectorDouble.html#_details). */
  /* LaVectorDouble laEigenVals(/\*cnt-rows=*\/m); */
  /* //! For details of the SVD procedure, see: http://lapackpp.sourceforge.net/html/lasvd_8h.html#4913cdb0732eba2a4765b34142020950 */
  /* LaSVD_IP(laCorrMat,laEigenVals,la_u,laEigenVecs); */


  /* //! Copy the eigenvalues into this internal structure: */
  /* for(uint row=0;row < /\*cnt-rows=*\/m; row++) { */
  /*   // FIXME: update our code to ensure that [”elow] assumption holds. */
  /*   assert(w[row] == FLT_MAX); */
  /*   w[row] = (float)laEigenVals(row); */
  /* } */
  /* //! Then copy the eigenvectors into this internal matrix: */
  /* for(uint row=0;row< /\*cnt-rows=*\/m; row++) { */
  /*   for(uint col=0;col < /\*cnt-columns=*\/n; col++) { */
  /*     u[row][col] = (float)la_u(row,col); */
  /*   } */
  /* } */
  /* //! Then copy the eigenvectors into this internal matrix: */
  /* for(uint row=0;row< /\*cnt-rows=*\/m; row++) { */
  /*   for(uint col=0;col < /\*cnt-columns=*\/n; col++) { */
  /*     // FIXME: validate that it is correct to update "vt", ie, that it is not "u" which should have been updated. */
  /*     vt[row][col] = (float)laEigenVecs(row,col); */
  /*   } */
  /* } */

  /* // FIXME: validate that 'this' deos not cause a memory-leak, ie, that [abvoe] contiatenrs are automatically de-allcoated. */

  //free_1d_list_float_explicitType_double(&arrOf_2d); 
  //delete [] arrOf_2d;

  return true;
}


static const t_float* sortdata = NULL; /* used in the quicksort algorithm */

/* ---------------------------------------------------------------------- */

static
int compare(const void* a, const void* b)
/* Helper function for sort. Previously, this was a nested function under
 * sort, which is not allowed under ANSI C.
 */
{ const int i1 = *(const int*)a;
  const int i2 = *(const int*)b;
  const t_float term1 = sortdata[i1];
  const t_float term2 = sortdata[i2];
  if (term1 < term2) return -1;
  if (term1 > term2) return +1;
  return 0;
}

/* ---------------------------------------------------------------------- */

static void sort(int n, const t_float data[], uint index[])
/* Sets up an index table given the data, such that data[index[]] is in
 * increasing order. Sorting is done on the indices; the array data
 * is unchanged.
 */
{ uint i;
  sortdata = data;
  for (i = 0; i < n; i++) index[i] = i;
  qsort(index, n, sizeof(uint), compare);
}


int svd_slow(int m, int n, t_float** u, t_float w[], t_float** vt) 
#include "kt_clusterAlg_SVD__func__svd_slow.c"
  return ierr;
}


/* ********************************************************************* */

/**
   @brief use SVD to perform principal components analysis of a real nrows by ncolumns rectangular matrix.
   @param <nrows> The number of rows in the matrix u.
   @param <ncolumns> The number of columns in the matrix v.
   @param <u> is the distance-matricx: on input, the array containing the data to which the principal component analysis should be applied. The function assumes that the mean has already been subtracted of each column, and hence that the mean of each column is zero. On output, see below.
   @param <v> (not used in input)
   @param <mapOf_w> (not used in input)
   @return true upon success
   @remarks 
   On output:   
   # The eigenvalues of the covariance matrix are returned in w.
   # Ifnrows >= ncolumns, then   
   - u contains the coordinates with respect to the principal components;
   - v contains the principal component vectors.         
   # Ifnrows < ncolumns, then   
   - u contains the principal component vectors;
   - v contains the coordinates with respect to the principal components.   
   # For the above sets, the dot product v . u reproduces the data that were passed in u.
   # The arrays u, v, and w are sorted according to eigenvalue, with the largest eigenvalues appearing first.   
 **/
const bool pca_slow(int nrows, int ncolumns, t_float** u, t_float** v, t_float* w) {
//const bool graphAlgorithms_svd::pca(uint nrows, uint ncolumns, float** u, t_float** v, list_generic<type_float> mapOf_w)  {  
    int i;
    int j;
    int error;
    const uint default_value_uint = 0; const t_float default_value_float = 0;
    uint* index = allocate_1d_list_uint(ncolumns, default_value_uint); //malloc(ncolumns*sizeof(int));
    t_float* temp = allocate_1d_list_float(ncolumns, default_value_float); //malloc(ncolumns*sizeof(t_float));
    //t_float* temp = malloc(ncolumns*sizeof(t_float));
    if (!index || !temp)
    {   if (index) free(index);
        if (temp) free(temp);
        return -1;
    }
    printf("cnt_rows=%d, cnt_columns=%u, at %s:%d\n", 
	   nrows, ncolumns,
	   __FILE__, __LINE__); // FIXME: remove this printf!
    error = svd_slow(nrows, ncolumns, u, w, v);
    if (error==0)
    {
      
      // FIXME: try to describe the 'concpet' behidn [below] procedure (oekseth, 06. otk. 2016).

      if (nrows >= ncolumns) { 
	
	//! Compute U = U*w
	for (j = 0; j < ncolumns; j++)      {
	  const t_float s = w[j];
	  for (i = 0; i < nrows; i++) u[i][j] *= s;
	}
	//! Identify the ranks of the "w" weight-table.
	sort(ncolumns, w, index);
	//! Center the extreme values, ie, swap 'min' and 'max' postions:
	for (i = 0; i < ncolumns/2; i++) {
	  j = index[i];
	  index[i] = index[ncolumns-1-i];
	  index[ncolumns-1-i] = j;
	}
	//! Apply the 'index-centering' on each feature-vector:
	for (i = 0; i < nrows; i++) {
	  for (j = 0; j < ncolumns; j++) temp[j] = u[i][index[j]];
	  for (j = 0; j < ncolumns; j++) u[i][j] = temp[j];
	}
	//! Center the values for each column:
	for (i = 0; i < ncolumns; i++) {
	  for (j = 0; j < ncolumns; j++) temp[j] = v[index[j]][i];
	  for (j = 0; j < ncolumns; j++) v[j][i] = temp[j];
	}
	//! Update the weight-table to reflect ... 
	for (i = 0; i < ncolumns; i++) temp[i] = w[index[i]];
	for (i = 0; i < ncolumns; i++) w[i] = temp[i];
      } else /* nrows < ncolumns */
        {   for (j = 0; j < nrows; j++)
            {   const t_float s = w[j];
	      for (i = 0; i < nrows; i++) v[i][j] *= s;
            }
	  sort(nrows, w, index);
	  for (i = 0; i < nrows/2; i++)
            {   j = index[i];
	      index[i] = index[nrows-1-i];
	      index[nrows-1-i] = j;
            }
	  for (j = 0; j < ncolumns; j++)
            {   for (i = 0; i < nrows; i++) temp[i] = u[index[i]][j];
	      for (i = 0; i < nrows; i++) u[i][j] = temp[i];
            }
	  for (j = 0; j < nrows; j++)
            {   for (i = 0; i < nrows; i++) temp[i] = v[j][index[i]];
	      for (i = 0; i < nrows; i++) v[j][i] = temp[i];
            }
	  for (i = 0; i < nrows; i++) temp[i] = w[index[i]];
	  for (i = 0; i < nrows; i++) w[i] = temp[i];
        }
    }
    free_1d_list_uint(&index);
    free_1d_list_float(&temp);
    return error;
}









