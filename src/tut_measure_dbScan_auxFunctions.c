#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "kt_matrix_extendedLogics.h"
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#define __MiV__mainIsDecelared
#include "tut_sse_matrixMul_cmpApproaches__stub.c"
#undef __MiV__mainIsDecelared


//! @brief comptue teh sum of score sin a matrix (oeksth, 0.6 mar. 2017)
//! @return the sum of scors in a matrix
//! @remarks is used (in this cotnext) to 'avoid' the "-O2" compailtion-optin from 'dsitacaring' our compatuioion (ie, as we are onlly interested int he compuation-time and nto the result)
static t_float get_sumOf_scoresInMatrix(t_float **matrix, const uint nrows, const uint ncols, const bool isToRpint_sumToSTDERR) {
  assert(matrix); assert(nrows > 0); assert(ncols > 0);
  t_float sum = 0;
  for(uint i = 0; i < nrows; i++) {
    assert(matrix[i]);
    for(uint k = 0; k < ncols; k++) {
      const t_float score = matrix[i][k];
      if(isOf_interest(score)) {sum += score;}
    }
  }
  if(isToRpint_sumToSTDERR) {fprintf(stderr, "(sum-of-scores)\t sum: %f, at %s:%d\n", sum, __FILE__, __LINE__);}
  return sum;
}

#endif

/**
   @brief examplfies axuialry funcitons used in the DB-SCAN algorithm (oekseht, 06. mari. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. mai. 2017).
   @remarks compile: g++ -I../src/  -O2  -mssse3   -L ../src/  tut_measure_dbScan_auxFunctions.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_tut
**/



static void __timeForLoopOrder_directMEmImpactOfTopologyAccess(const uint case_id, uint nrows, uint ncols) {
  const uint max_cnt = 1000*1000*100*6; //! where latter is used iot. reflect the main-mem-threshold on our laptop-test-paltform.
  const uint cnt_ele = nrows*ncols;
  if(cnt_ele > max_cnt) {
    fprintf(stderr, "!!\t Yoru requested matrix-dim=[%u, %u]=%u  was outside the expected max-threshodl=%u, ie, please vlaidate your setting, at %s:%d\n", nrows, ncols, cnt_ele, max_cnt, __FILE__, __LINE__);
    assert(false);
    return;
  }
  

  //assert(cntHeads_each__perCent <
  { //!
    //! Apply the search:
    printf("# Starts search, at %s:%d\n", __FILE__, __LINE__);
    //! Intiiate:
    const t_float emtpy_0 = 0;
    t_float **matrix = allocate_2d_list_float(nrows, ncols, emtpy_0);
    assert(matrix);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	const t_float score = (t_float)rand();
	if(score != 0.0) {
	  matrix[row_id][col_id] = T_FLOAT_MAX / score;
	}
      }
    }
    start_time_measurement(); //! ie, start the time-emasurement:
    { //! Logics: 
      const uint cnt_traversals = 1;	  
      //const uint cnt_traversals = 200;	  
      if(case_id == 0) { //! then we apply/use a
	t_float sum  = 0;
	// printf("#!  [%u, %u], at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
	for(uint t = 0; t < cnt_traversals; t++) {
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score = matrix[row_id][col_id];
	      sum += score;
	    }
	  }
	}
	printf("# sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      } else if(case_id == 1) { //! then a randomzied access is investigted wrt. data-traversals.
	t_float sum  = 0;
	for(uint t = 0; t < cnt_traversals; t++) {
	  for(uint row_id = 0; row_id < nrows; row_id++) {	      
	    uint row_id_ = rand() % nrows;
	    if(row_id_ >= nrows) {row_id_ = nrows - 1;}
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score = matrix[row_id_][col_id];
	      sum += score;
	    }
	  }
	}
	printf("# sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      } else if(case_id == 2) { //! then we apply/use a
	t_float sum  = 0;
	// printf("#!  [%u, %u], at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
	for(uint t = 0; t < cnt_traversals; t++) {
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    const t_float *__restrict__ row = matrix[row_id];
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score = row[col_id];
	      sum += score;
	    }
	  }
	}
	printf("# sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      } else if(case_id == 3) { //! then a randomzied access is investigted wrt. data-traversals.
	t_float sum  = 0;
	for(uint t = 0; t < cnt_traversals; t++) {
	  for(uint row_id = 0; row_id < nrows; row_id++) {	      
	    uint row_id_ = rand() % nrows;
	    if(row_id_ >= nrows) {row_id_ = nrows - 1;}
	    const t_float *__restrict__ row = matrix[row_id_];
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score = row[col_id];
	      sum += score;
	    }
	  }
	}
	printf("# sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      } else if(case_id == 4) { //! then a randomzied access is investigted wrt. data-traversals.
	t_float sum  = 0;
	// printf("#!  [%u, %u], at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint row_id_o = 0; row_id_o < nrows; row_id_o++) {
	    for(uint col_id = 0; col_id < ncols; col_id++) {
	      const t_float score = matrix[row_id][col_id];
	      sum += score;
	    }
	  }
	}
	printf("# sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      } else if(case_id == 5) { //! then a randomzied access is investigted wrt. data-traversals.
	const char *str_local = "simple-plus-memory[SSE+tiling:SM=64]";
	//!
	//! To simplify our SE we narrow out evlauation to only cover sSE-specific cases:
	assert(nrows >= 128); 	assert(ncols >= 128);
	const uint nrows_pre = nrows;
	uint cnt_buck = nrows/128;
	nrows = 128 * cnt_buck;
	ncols = 128 * (ncols/128);
	// printf("nrows=%u, pre=%u, cnt_buck=%u, at %s:%d\n", nrows, nrows_pre, cnt_buck, __FILE__, __LINE__);
	assert(nrows > 0);
	assert(ncols > 0);
	assert( (nrows % 128) == 0); //! ie, to simplify our SSE-test-rotuines.
	assert( (ncols % 128) == 0); //! ie, to simplify our SSE-test-rotuines.
	t_float prev_time_inSeconds = 0;
	t_float prev_time_inSeconds__ignore = 0;
	s_kt_matrix_t mat_val = initAndReturn__s_kt_matrix(nrows, ncols);/* initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /\*maxVal=*\/100,  */
	for(uint i = 0; i < nrows; i++) {for(uint k = 0; k < ncols; k++) {mat_val.matrix[i][k] = rand() % ncols;}} //! ie, itnaite.
	{t_float sum = 0;
	  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, nrows);
	  start_time_measurement();
	  kt_func_metricInner_fast__fitsIntoSSE__euclid__float(mat_val.matrix, mat_val.matrix, nrows, ncols, mat_result.matrix, /*SM=*/64);
	  prev_time_inSeconds__ignore = prev_time_inSeconds = end_time_measurement(/*msg=*/str_local, prev_time_inSeconds); //, system_time, prev_time_inSeconds);
	  get_sumOf_scoresInMatrix(mat_result.matrix, mat_result.nrows, mat_result.ncols, true); //! ie,print ouyt the score, therey 'avoding' issues wrt. a 'ignoreance of the pritn-oerpation in "-O2" compailtion-mode (oekseht, 06. amr.2 107).
	  //! 
	  //free__s_kt_matrix(&mat_val);
	  free__s_kt_matrix(&mat_result);
	}
	//! --------------------------------------------
	//!
	//! De-allocate:
	free__s_kt_matrix(&mat_val);
      } else {
	fprintf(stderr, "!!\t option Not supported: case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
	assert(false);
      }
    }
    { //! end tiem-measurement:
      char str_local[2000] = {'\0'}; sprintf(str_local, "[|%u|, |%u|] w/case_id=%u", nrows, ncols, case_id);
      float cmp_time_search = 0;
      const float endTime = end_time_measurement(str_local, cmp_time_search);
      //! Update oru result-set:
      fprintf(stdout, "endTime:%f\n", endTime);      
    }
    free_2d_list_float(&matrix, nrows);
  }
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main(const int array_cnt, char **array) 
#else
bool tut_measure_dbScan_auxFunctions(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  // TODO: cosnider adidng the MINE-spec-details
  // hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

    if(array_cnt >= 5) {
      //! 
      //! Fetch: the evation-case
      uint index = 2;
      const char *str_local = strstr(array[index], "case:");
      assert(str_local); assert(strlen(str_local));
      str_local += strlen("case:");
      const uint case_id = atoi(str_local);
      //assert(case_id <= 2); //! ie, what we expect
      //! 
      //! Fetch: the size-offset to start with:
      index = 3;
      //      const char *
      str_local = strstr(array[index], "nrows:");
      assert(str_local); assert(strlen(str_local));
      str_local += strlen("nrows:");
      const uint nrows = (uint)atoi(str_local);
      index = 4;
      str_local = strstr(array[index], "ncols:");
      assert(str_local); assert(strlen(str_local));
      str_local += strlen("ncols:");
      const uint ncols = (uint)atoi(str_local);
	//! Apply the search:
      __timeForLoopOrder_directMEmImpactOfTopologyAccess(case_id, nrows, ncols);
    } else {
      fprintf(stderr, "!!\t Option Not supported: please inveitgate, at %s:%d\n", __FILE__, __LINE__);
    }
    return true;

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  // TODO: cosnider adidng the MINE-spec-details
  // hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}
