/* #ifndef metricMacro_minkowski_h */
/* #define metricMacro_minkowski_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_minkowski
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "Minkowski".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/



//! Comput measure for: CityBlock;
#define correlation_macros__distanceMeasures__cityBlock(value1, value2) ({t_float term = mathLib_float_abs(value1 - value2); term;})
//#define correlation_macros__distanceMeasures__cityBlock(value1, value2) ({printf("value1=%f, value2=%f, diff=%f, at %s:%d\n", value1, value2, mathLib_float_abs(value1 - value2), __FILE__, __LINE__); t_float term = mathLib_float_abs(value1 - value2); term;}) //term = correlation_macros__distanceMeasures__cityBlock(value1, value2, term); term;})
//#define correlation_macros__distanceMeasures__cityBlock(value1, value2) ({t_float term = 0; mathLib_float_abs(value1 - value2); term;}) //term = correlation_macros__distanceMeasures__cityBlock(value1, value2, term); term;})
//! @return the adjusted weihgt
/* #define correlation_macros__distanceMeasures__cityBlock_weight(value1, value2, weight, weight_index) ({ \ */
/*   t_float term = 0; correlation_macros__distanceMeasures__cityBlock(value1, value2, term); \ */
/*   const t_float inp_result = correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index); inp_result;}) //! ie, return the adjusted weight */
#define correlation_macros__distanceMeasures__cityBlock__SSE(val1, val2) ({VECTOR_FLOAT_TYPE mul = VECTOR_FLOAT_SUB(term1, term2); mul = VECTOR_FLOAT_MAX(VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET_zero(), mul), mul); mul;}) //! ie, return.

//! Compute measure for: Euclid
#define correlation_macros__distanceMeasures__euclid(value1, value2) ({t_float term = value1 - value2; term = term*term; term;})
//#define correlation_macros__distanceMeasures__euclid(value1, value2) ({assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(value1) == false); assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(value2) == false);  t_float term = value1 - value2; term = term*term; term;})
#define correlation_macros__distanceMeasures__euclid__postProcess(inp_result) ({metric_macro_defaultFunctions__postProcess__validateEmptyCase(mathLib_float_sqrt(inp_result));})
//#define correlation_macros__distanceMeasures__euclid_getRetVal(value1, value2) ({t_float term = 0; correlation_macros__distanceMeasures__euclid(value1, value2, term); term;}) //! ie, return the inp_result.

#define correlation_macros__distanceMeasures__euclid__SSE(vec_1, vec_2) ({ \
  VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_SUB(vec_1, vec_2); \
  VECTOR_FLOAT_TYPE vec_mul  = VECTOR_FLOAT_MUL(vec_diff, vec_diff); \
  vec_mul;}) //! ie, return
// FIXME: what is the differnece between "euclid" and "expected valeu" ... ie, compare the fucntions matehmitcally
#define correlation_macros__distanceMeasures__euclid__SSE_addTo_result(vec_1, vec_2, vec_result) ({ \
  VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_SUB(vec_1, vec_2); \
  VECTOR_FLOAT_TYPE vec_mul  = VECTOR_FLOAT_MUL(vec_diff, vec_diff); \
  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_mul);})
/* #define correlation_macros__distanceMeasures__euclid_weight_getRetVal(value1, value2, weight, weight_index) ({ \ */
/*       t_float term = 0; correlation_macros__distanceMeasures__euclid(value1, value2, term); \ */
/*       const t_float ret_val = correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index); \ */
/*       ret_val;}) //! ie, return the adjusted weight */


/* #define correlation_macros__distanceMeasures__euclid_weight(value1, value2, term, weight, weight_index) ({ \ */
/*       term = correlation_macros__distanceMeasures__euclid_weight(value1, value2, weight, weight_index); term;}) */

#define correlation_macros__distanceMeasures__euclid__SSE_weight(vec_1, vec_2, vec_w, vec_result, vec_tweight) ({ \
  VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_SUB(vec_1, vec_2); \
  VECTOR_FLOAT_TYPE vec_mul  = VECTOR_FLOAT_MUL(vec_diff, vec_diff); \
  vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_w); \
  vec_tweight = VECTOR_FLOAT_ADD(vec_tweight, vec_w); \
  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_mul);})

// FIXME: correctness validate that [”elow] 'verisons' produce the same resutls ... and tehn compare the perofmrance-gain which may be saved through teh SSe-metric ... eg, for "Minkowski"

//! @return the correlations-core for: Minkowski
#define correlation_macros__distanceMeasures__minkowski(term1, term2, power) ({\
  const t_float diff = mathLib_float_abs(term1 - term2); \
  const t_float inp_result = mathLib_float_pow(diff, power); \
  inp_result;}) //! ie, return the result:

  /*  
	printf("in-macro)\t FIXME: icnldue the code into thsi, at %s:%d\n", __FILE__, __LINE__); 

  **/
  /*   printf("(\t(in-macro)\t inp_result=%f, result_upd=%f=%f, power=%f, power_inverted=%f, at [%s]:%s:%d\n", inp_result, result_upd, powf((float)inp_result, (float)power_inverted), (t_float)scalar_power, (t_float)power_inverted, __FUNCTION__, __FILE__, __LINE__); \ */
  /* } \ */

#define correlation_macros__distanceMeasures__minkowski__postProcess(inp_result, scalar_power) ({ \
      t_float result_upd = (t_float)metricMacro__constants__defaultValue__postProcess__allMatches; \
      if(inp_result != 0) { \
	const t_float power_inverted = 1.0 / (t_float)scalar_power;		\
	result_upd = mathLib_float_pow(inp_result, power_inverted);	\
      } \
  result_upd;})
#define correlation_macros__distanceMeasures__minkowski_pow3__postProcess(value) ({correlation_macros__distanceMeasures__minkowski__postProcess(value, /*power=*/3);})
#define correlation_macros__distanceMeasures__minkowski_pow4__postProcess(value) ({correlation_macros__distanceMeasures__minkowski__postProcess(value, /*power=*/4);})
#define correlation_macros__distanceMeasures__minkowski_pow5__postProcess(value) ({correlation_macros__distanceMeasures__minkowski__postProcess(value, /*power=*/5);})

//! @return the correlations-core for: Minkowski
#define correlation_macros__distanceMeasures__minkowski__SSE(term1, term2, power) ({\
  const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
  const VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_POWER(vec_diff, power); \
  vec_result;}) //! ie, return the result:

//! @return the correlations-core for: Minkowski
// FIXEM: consider to use a pre-allcoated talbe of pow-values for [”elow]
#define correlation_macros__distanceMeasures__minkowski__zeroOne(term1, term2, power) ({\
  const t_float diff = mathLib_float_abs(term1 - term2); \
  const t_float inp_result = mathLib_float_pow(diff, power); \
  inp_result;}) //! ie, return the result:
//! @return the correlations-core for: Minkowski
#define correlation_macros__distanceMeasures__minkowski__zeroOne__SSE(term1, term2, power) ({\
  const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
  const VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_fastpow_0_1(vec_diff, power); \
  vec_result;}) //! ie, return the result:

//! @return the correlations-core for: Minkowski -- pow=3
#define correlation_macros__distanceMeasures__minkowski__pow3(term1, term2) ({\
  const t_float diff = mathLib_float_abs(term1 - term2); \
  const t_float inp_result = diff * diff * diff;\
  inp_result;}) //! ie, return the result:


//! @return the correlations-core for: Minkowski --- pow=3
#define correlation_macros__distanceMeasures__minkowski__pow3__SSE(term1, term2) ({\
  const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
  const VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_diff, VECTOR_FLOAT_MUL(vec_diff, vec_diff)); \
  vec_result;}) //! ie, return the result:

//! @return the correlations-core for: Minkowski -- pow=4
#define correlation_macros__distanceMeasures__minkowski__pow4(term1, term2) ({\
  const t_float diff = mathLib_float_abs(term1 - term2); \
  const t_float inp_result = diff * diff * diff * diff;\
  inp_result;}) //! ie, return the inp_result:
//! @return the correlations-core for: Minkowski --- pow=4
#define correlation_macros__distanceMeasures__minkowski__pow4__SSE(term1, term2) ({\
  const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
  const VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_diff, VECTOR_FLOAT_MUL(vec_diff, VECTOR_FLOAT_MUL(vec_diff, vec_diff))); \
  vec_result;}) //! ie, return the result:
//! @return the correlations-core for: Minkowski -- pow=5
#define correlation_macros__distanceMeasures__minkowski__pow5(term1, term2) ({\
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(term1) == false); assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(term2) == false); \
  const t_float diff = mathLib_float_abs(term1 - term2); \
   const t_float inp_result = diff * diff * diff * diff * diff;\
   /*! Note: the results of this computaiotn amy result in 'inf', which is due t*/ \
  inp_result;}) //! ie, return the inp_result:
//! @return the correlations-core for: Minkowski --- pow=5
#define correlation_macros__distanceMeasures__minkowski__pow5__SSE(term1, term2) ({\
  const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
  const VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_diff, VECTOR_FLOAT_MUL(vec_diff, VECTOR_FLOAT_MUL(vec_diff, VECTOR_FLOAT_MUL(vec_diff, vec_diff)))); \
  vec_result;}) //! ie, return the result:


//! @return the correlations-core for: Chebychev
#define correlation_macros__distanceMeasures__chebychev(term1, term2) ({\
  const t_float diff = mathLib_float_abs(term1 - term2); \
  diff;}) //! ie, return the result:
#define correlation_macros__distanceMeasures__chebychev__postProcess(value) ({ metric_macro_defaultFunctions__postProcess__none(value);})
//! @return the correlations-core for: Chebychev
#define correlation_macros__distanceMeasures__chebychev__SSE(term1, term2) ({\
      const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      vec_diff;}) //! ie, return the result:

/* //! @return the correlations-core for: Chebychev */
/* #define correlation_macros__distanceMeasures__chebychev_minInsteadOfMax(term1, term2) ({\ */
/*   const t_float diff = mathLib_float_abs(term1 - term2); \ */
/*   diff;}) //! ie, return the result: */
/* #define correlation_macros__distanceMeasures__chebychev_minInsteadOfMax__postProcess(value) ({ metric_macro_defaultFunctions__postProcess__none(value);}) */
/* //! @return the correlations-core for: Chebychev */
/* #define correlation_macros__distanceMeasures__chebychev_minInsteadOfMax__SSE(term1, term2) ({\ */
/*       const VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \ */
/*       vec_diff;}) //! ie, return the result: */



// #endif //! EOF
