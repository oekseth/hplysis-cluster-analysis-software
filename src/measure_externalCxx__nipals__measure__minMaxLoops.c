{
  //! ---------
  {	
    { const char *stringOf_measureText = "simple-2d-loops--nonSSE: get-min(..) for type=float";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      const float min_value = get_minValue__float(size_of_array, size_of_array, matrix_float, /*isTo_use_fastFunction=*/false);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }	//! -- 
    { const char *stringOf_measureText = "simple-2d-loops--nonSSE--SSE: get-min(..) for type=float";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      const float min_value = get_minValue__float(size_of_array, size_of_array, matrix_float, /*isTo_use_fastFunction=*/true);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
  }
  //! ---------
  {	
    { const char *stringOf_measureText = "simple-2d-loops--nonSSE: get-min(..) for type=uint";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      const uint min_value = get_minValue__uint(size_of_array, size_of_array, matrix_uint, /*isTo_use_fastFunction=*/false);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }	//! -- 
    { const char *stringOf_measureText = "simple-2d-loops--nonSSE--SSE: get-min(..) for type=uint";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      const uint min_value = get_minValue__uint(size_of_array, size_of_array, matrix_uint, /*isTo_use_fastFunction=*/true);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
  }
  //! ---------
  {	
    { const char *stringOf_measureText = "simple-2d-loops--nonSSE: get-min(..) for type=char";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      const char min_value = get_minValue__char(size_of_array, size_of_array, matrix_char, /*isTo_use_fastFunction=*/false);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }	//! -- 
    { const char *stringOf_measureText = "simple-2d-loops--nonSSE--SSE: get-min(..) for type=char";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      const char min_value = get_minValue__char(size_of_array, size_of_array, matrix_char, /*isTo_use_fastFunction=*/true);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
  }
}
