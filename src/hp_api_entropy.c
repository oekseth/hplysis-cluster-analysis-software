#include "hp_api_entropy.h"

//! Read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
bool readFromFile__exportResult__hp_api_entropy(const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_exportFile, const bool isToOnlyWriteOut_avg, const uint max_theoreticCountSize, const bool isTo_applyPostScaling, const bool computeFor_freq) {
  //const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse) {  
  //! ----------------------------------
  //!  
  //! What we expect:
  if(!stringOf_enum || !strlen(stringOf_enum) || !stringOf_inputFile_1 || !strlen(stringOf_inputFile_1)) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  //! ----------------------------------
  //!
  //! Configure:
  char config_col_sep = '\0';
  {
    const char str_size = strlen(stringOf_inputFile_1);
    if(str_size > 5) {
      if( 
	 (stringOf_inputFile_1[str_size-4] == '.') &&
	 (stringOf_inputFile_1[str_size-3] == 't') &&
	 (stringOf_inputFile_1[str_size-2] == 's') &&
	 (stringOf_inputFile_1[str_size-1] == 'v') ) {config_col_sep = '\t';}
    } else if( 
	 (stringOf_inputFile_1[str_size-4] == '.') &&
	 (stringOf_inputFile_1[str_size-3] == 'c') &&
	 (stringOf_inputFile_1[str_size-2] == 's') &&
	 (stringOf_inputFile_1[str_size-1] == 'v') ) {config_col_sep = ',';}
  }
  if(config_col_sep == '\0') {
    fprintf(stderr, "!!\t Was  Not able to idneitfy the format fo our inptu-file: please use either '.tsv' of '.csv.' as a suffix (to your input-file), eg, 'input.csv'; while a '.tsv' impleis that we expect each column to be sepoerated by a 'tab' character we for a '.cvs' exptcs each feautre to be seperated by a ',' (ie, a comma) sperator. In brief please fix this issue. If quesiotns then pelase cotnact sneior delveoper  [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //! ----------------------------------
  e_kt_entropy_genericType_t metric_entropy = e_kt_entropy_genericType_ML_Shannon;
  bool isTo_computeForAll_entropyMetrics_base = false;
  if(stringOf_enum && strlen(stringOf_enum)) {
    const char *cmp_base = "all";
    if(strlen(stringOf_enum) == strlen(cmp_base) && (0 == strcmp(cmp_base, stringOf_enum) ) ) {
      isTo_computeForAll_entropyMetrics_base = true;
    } else {
      metric_entropy = get_enumForString__e_kt_entropy_genericType_t(stringOf_enum);
      if(metric_entropy == e_kt_entropy_genericType_undef) {
	metric_entropy = e_kt_entropy_genericType_ML_Shannon;
      }
    }
  }
  if(isTo_computeForAll_entropyMetrics_base == false) {
    //! ----------------------------------
    //!
    s_hp_entropy_fileReadConf_t file_config = init__s_kt_list_fileReadConf_t(stringOf_inputFile_1, stringOf_exportFile, config_col_sep);
    if(isToOnlyWriteOut_avg == true) {
      file_config.isTo_exportForEach = false;
    }
    //! ----------------------------------
    //!
    //! Apply Logics:
    bool is_ok = true;
    if(computeFor_freq == false) {
      fromFile__compute_entropy__matrix__hp_entropy(&file_config, max_theoreticCountSize, metric_entropy, isTo_applyPostScaling, NULL);
    } else {
      fromFile__compute_freq__matrix__hp_entropy(&file_config, max_theoreticCountSize, metric_entropy, isTo_applyPostScaling, NULL);    
    }
    if(isToOnlyWriteOut_avg == true) { //! then we write out for each entropy metric: 
      t_float score = file_config.result_summary_sum;
      if(score != 0) {score = score/file_config.result_summary_sum_cntRows;}
      printf("%s\t%s\t%f\t #! hpLysis-entropy\n", stringOf_inputFile_1, stringOf_enum, score);
      //printf("%s\t%s\t%f\t #! from %s:%d\n", stringOf_inputFile_1, stringOf_enum, score, __FILE__, __LINE__);
    }
  return is_ok;
  } else {
    for(uint met_id = 0; met_id < e_kt_entropy_genericType_undef; met_id++) {
      e_kt_entropy_genericType_t metric_entropy = (e_kt_entropy_genericType_t)met_id;
      //! ----------------------------------
      //!
      char *str_export = NULL;
      if(stringOf_exportFile && strlen(stringOf_exportFile)) {
	const uint size = strlen(stringOf_exportFile) + 1000;
	const char def_char = '\0';
	str_export = allocate_1d_list_char(size, def_char);
	sprintf(str_export, "%s_%s.tsv", stringOf_exportFile, get_stringOfEnum__e_kt_entropy_genericType_t(metric_entropy));
	printf("(info)\t Export to result-file=\"%s\", at %s:%d\n", str_export, __FILE__, __LINE__);
      }
      s_hp_entropy_fileReadConf_t file_config = init__s_kt_list_fileReadConf_t(stringOf_inputFile_1, str_export, config_col_sep);
      if(isToOnlyWriteOut_avg == true) {
      	file_config.isTo_exportForEach = false;
      }
      //! ----------------------------------
      //!
      //! Apply Logics:
      bool is_ok = true;
      if(computeFor_freq == false) {
	fromFile__compute_entropy__matrix__hp_entropy(&file_config, max_theoreticCountSize, metric_entropy, isTo_applyPostScaling, NULL);
      } else {
	fromFile__compute_freq__matrix__hp_entropy(&file_config, max_theoreticCountSize, metric_entropy, isTo_applyPostScaling, NULL);    
      }
      if(isToOnlyWriteOut_avg == true) { //! then we write out for each entropy metric: 
	t_float score = file_config.result_summary_sum;
	if(score != 0) {score = score/file_config.result_summary_sum_cntRows;}
	printf("%s\t%s\t%f\t #! hpLysis-entropy\n", stringOf_inputFile_1, get_stringOfEnum__e_kt_entropy_genericType_t(metric_entropy), score);
	//printf("%s\t%s\t%f\t #! from %s:%d\n", stringOf_inputFile_1, stringOf_enum, score, __FILE__, __LINE__);
      }
      if(str_export) {
	free_1d_list_char(&str_export); str_export = NULL;
      }
    }
  }
  /* const bool is_ok = readFromFile__stroreInStruct__exportResult__hp_api_entropy(stringOf_enum, &mat_1, &mat_2, &mat_result, stringOf_exportFile, stringOf_exportFile__format, nclusters, inputFile_isSparse); */
  /* assert(is_ok); */
  /* //! */
  /* //! De-allcoate: */
  /* free__s_kt_matrix(&mat_1); */
  /* free__s_kt_matrix(&mat_2); */
  /* free__s_kt_matrix(&mat_result); */
  //! @return the status-code.
  return true;
}

static const char *stringOf_enum__par = "-metric=";
static const char *stringOf_enum__par_config__isToUsePostScaling__par = "-use-postScaling=";


#define __MiV__support__freqEntropy 0
#if(__MiV__support__freqEntropy == 1)
static const char *stringOf_enum__par_config__isToUseComputeFreq__par = "-compute-forFrequency=";
#endif
static const char *stringOf_inputFile_1__par = "-file1=";
//static const char *stringOf_inputFile_2__par = "-file2=";
static const char *stringOf_exportFile__par = "-out=";
static const char *stringOf_export_isToOnlyWriteOut_avg = "-writeOutAverage=";
//static const char *stringOf_exportFile__format__par = "-outFormat=";
static const char *inputData__maxPossibleScore__par = "-maxScore=";
//static const char *inputFile_isSparse__par = "-inputIsSparse=";

//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_entropy(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_exportFile, const bool isToOnlyWriteOut_avg__par, const uint max_theoreticCountSize, const bool isTo_applyPostScaling, const bool computeFor_freq, uint *scalar_cntArgs, char **listOf_args__2d) {
//char *construct__terminalArgString__fromInput__hp_api_entropy(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse, uint *scalar_cntArgs, char **listOf_args__2d) {  
  assert(scalar_cntArgs);
  assert(listOf_args__2d);
  uint cnt_chars = 10000;
  const char def_0 = 0;
  //! Allocate:
  char *str_cmd = allocate_1d_list_char(cnt_chars, def_0);
  //! ----------------------------
  //!
  //! Define function
  char *curr = str_cmd;
  uint cnt = 0;
#define __MiF__insertIfValue(val, param) ({if(val && strlen(val)) {listOf_args__2d[cnt] = curr; cnt++; sprintf(curr, "%s%s", param, val); curr += strlen(curr); *curr = '\0'; curr++;}}) //! ie, 'insert value' if the inptu-value is set.
  //!
  //! Add values:
  __MiF__insertIfValue("x_hp_entropy" , ""); //! ie, as the first arguemtn is always the name of the progrma.
  __MiF__insertIfValue(stringOf_enum , stringOf_enum__par);
  __MiF__insertIfValue(stringOf_inputFile_1 , stringOf_inputFile_1__par);
  //__MiF__insertIfValue(stringOf_inputFile_2 , stringOf_inputFile_2__par);
  __MiF__insertIfValue(stringOf_exportFile , stringOf_exportFile__par);
  if(isTo_applyPostScaling) {
    //__MiF__insertIfValue(stringOf_export_isToOnlyWriteOut_avg , stringOf_export_isToOnlyWriteOut_avg__par);
    __MiF__insertIfValue("1", stringOf_export_isToOnlyWriteOut_avg);
  }
  //__MiF__insertIfValue(stringOf_exportFile__format , stringOf_exportFile__format__par);
  if( (max_theoreticCountSize != UINT_MAX) ) {
    allocOnStack__char__sprintf(2000, str_local, "%u", max_theoreticCountSize); //! ie, intate a new variable "str_local". 
    __MiF__insertIfValue(str_local , inputData__maxPossibleScore__par);
  }
  if(isTo_applyPostScaling) {
    __MiF__insertIfValue("1", stringOf_enum__par_config__isToUsePostScaling__par);
  }
#if(__MiV__support__freqEntropy == 1)
  if(computeFor_freq) {
    __MiF__insertIfValue("1", stringOf_enum__par_config__isToUseComputeFreq__par);
  }
#endif
#undef __MiF__insertIfValue
  *scalar_cntArgs = cnt;
  //! ----------------------------
  //!
  if(debug_config__isToCall) { //! then apply logic:
    const bool is_ok = readFromFile__exportResult__hp_api_entropy(stringOf_enum, stringOf_inputFile_1, stringOf_exportFile, isToOnlyWriteOut_avg__par, max_theoreticCountSize, isTo_applyPostScaling, computeFor_freq); //stringOf_exportFile__format, nclusters, inputFile_isSparse);
    assert(is_ok);
  }
  //! ----------------------------
  //!
  //! @return 
  return str_cmd;
}

/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_entropy(char **arg, const uint arg_size) {
  assert(arg);
  const char *stringOf_enum = NULL;
  //const char *stringOf_enum__nclusters = NULL;
  const char *stringOf_inputFile_1 = NULL;
  //const char *stringOf_inputFile_2 = NULL;
  const char *stringOf_exportFile = NULL;
  bool export_isToOnlyWriteOut_avg = false;
  //const char *stringOf_exportFile__format = NULL;
  uint max_theoreticCountSize = 0; 
  bool isTo_applyPostScaling = false;
  bool computeFor_freq = false;
  //bool inputFile_isSparse = false;
  //! ----------------------------------
  uint __cntParams_set = 0;

  if(arg_size == 2) {
#define __MiF__cmp(string) ({bool is_equal = false; if(strlen(string) == strlen(arg[1])) {if(0 == strncasecmp(string, arg[1], strlen(string))) {is_equal = true;}} is_equal;}) //! ie, a wrapper to return true if the format equals:
    
    /* if(__MiF__cmp("ccm_matrix")) { */
    /*   printOptions__ccm_matrixBased__hp_api_entropy(); */
    /* } */
    /* if(__MiF__cmp("ccm_goldXgold")) { */
    /*   printOptions__ccm_twoClusterResults__hp_api_entropy();  */
    /* } */
    /* if(__MiF__cmp("distance")) { */
    /*   printOptions__simMetric__hp_api_entropy(); */
    /* } */
    /* if(__MiF__cmp("clustAlg")) { */
    /*   printOptions__clustAlg__hp_api_entropy(); */
    /* } */
    FILE *fileP = stdout;
    if(__MiF__cmp("-ex")) {
      fprintf(fileP, "#! Exemple calls are:\n");
#define __MiF__writeOut(cmd, comment) ({ fprintf(fileP, "\t %s \t #! %s\n", cmd, comment);} )
      {
	__MiF__writeOut("./x_hp_entropy -metric=ML -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1", "Compute ML Shannon, and write out the average entropy of the rows (as defined in \"kt_entropy.h\").");
	__MiF__writeOut("./x_hp_entropy -metric=ML -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1", "Compute ML Shannon, and write for each row.");
	__MiF__writeOut("./x_hp_entropy -metric=MM -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1", "Compute MM Miller Madow, and write out the average entropy of the rows");
	__MiF__writeOut("./x_hp_entropy -metric=Jeffreys -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1", "Compute Dirichlet Jeffreys (Jeffreys), and write out the average entropy of the rows");
	__MiF__writeOut("./x_hp_entropy -metric=all -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1", "Compute seperately for all entropy-metrics using default configurations");
	__MiF__writeOut("./x_hp_entropy -metric=all -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1 -out=entropy_result.tsv", "Compute seperately for all entropy-metrics using default configurations, and store the details in the result-files");
      }
#undef __MiF__writeOut
    } else {
      fprintf(fileP, "#\tEntropy-metrics in hpLysis are: [");
      for(uint enum_entropy = 0; enum_entropy < e_kt_entropy_genericType_undef; enum_entropy++) {
	const char *str = get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)enum_entropy);
	fprintf(fileP, "\"%s\", ", str); 
      }
      fprintf(fileP, "], at %s:%d\n", __FILE__, __LINE__);
    }
#undef __MiF__cmp
    //!
    //! At this exueicotn-point we asusme the oerpation was a success:
    return true;    
  } else if(arg_size > 2) {
    //! ----------------------------------
    //!
    //! Then identify and evluate the input-parameters: 
#define __MiF__setIfFound(result, para) ({const char *val = strstr(arg[i], para); if(val) {val += strlen(para); result = val;}})
    for(uint i = 1; i < arg_size; i++) {
      // printf("eval: \"%s\", at %s:%d\n", arg[i], __FILE__, __LINE__);
      __MiF__setIfFound(stringOf_enum, stringOf_enum__par);
      //! -------------- 
      __MiF__setIfFound(stringOf_inputFile_1, stringOf_inputFile_1__par);
      //__MiF__setIfFound(stringOf_inputFile_2, stringOf_inputFile_2__par);
      //! -------------- 
      __MiF__setIfFound(stringOf_exportFile, stringOf_exportFile__par);
      //__MiF__setIfFound(stringOf_exportFile__format, stringOf_exportFile__format__par);
      //! -------------- 
      //! -------------- 
      const char *result = NULL;
      result = NULL; __MiF__setIfFound(result, stringOf_export_isToOnlyWriteOut_avg);       
      if(result) {export_isToOnlyWriteOut_avg = (bool)atoi(result);} //! ie, icnrement 'past' the operation.      
      //! -------------- 
      //!
      //! Add for 'numeric' input-data-cases:
      //! -------------- 
      //const char *result = NULL;
      __MiF__setIfFound(result, inputData__maxPossibleScore__par);       
      if(result) {max_theoreticCountSize = (uint)atoi(result);} //! ie, icnrement 'past' the operation.
      //! -------------- 
      result = NULL; __MiF__setIfFound(result, stringOf_enum__par_config__isToUsePostScaling__par);       
      if(result) {isTo_applyPostScaling = (bool)atoi(result);} //! ie, icnrement 'past' the operation.      
      //! -------------- 
#if(__MiV__support__freqEntropy == 1)
      result = NULL; __MiF__setIfFound(result, stringOf_enum__par_config__isToUseComputeFreq__par);       
#endif
      if(result) {computeFor_freq = (bool)atoi(result);} //! ie, icnrement 'past' the operation.      
    }
#undef __MiF__setIfFound
  }
  if( //(__cntParams_set == 0) || 
      (stringOf_inputFile_1 == NULL) || (stringOf_enum == NULL) ) {
    //!
    //! Then we write out the different optiosn described in [ªbove].
    printf("#! The simplified hpLysis interface provides support for:\n");
#define __MiF__printAg(opt, desc) ({if( (opt != NULL) && strlen(opt))  {printf("\t \"%s\": %s\n", opt, desc);} else {printf("%s\n", desc);}})
    //#define __MiF__printAg(opt, desc) ({printf("\t \"%s\": %s\n", opt, desc);})
    //! Write out arguments:
    __MiF__printAg(stringOf_enum__par, "The entopry-metric to apply: to compute for all base-metrics, write 'metrics=all'; to list entropy use the option '-metric=none' for details for each of the entorpy-emtrics please see our research-article");
    //    __MiF__printAg(stringOf_enum__par, "The type of algorithm of metric to apply: write [program-name] and either \"ccm_matrix\", \"ccm_goldXgold\", \"distance\", or \"clustAlg\" to get a list of the correct enum-alternatives");
    __MiF__printAg(stringOf_inputFile_1__par, "The input-file: expect integer-counts of scores");
    //__MiF__printAg(stringOf_inputFile_2__par, "The second input-matrix: (a) matrix-based CCMs: the hypothesized data-set; (b) goldXgold-CCM: an alternative set of hypothesis to compare the first input-data-set with; (c) distance: an alternative distance-matrix to be used as input: (d) clustAlg: ignored");
    //__MiF__printAg(stringOf_exportFile__par, "The format-type. We expect values to be separated by 'tabs'. Default format a 'dense matrix' (where each index describes the position in the matrix). If 'sparse' is used, we assume a row consists of 'three' (3) columns, where column[0]='row_id', column[1]='col_id', column[2]='distance'");
    __MiF__printAg(stringOf_exportFile__par, "The name of the result-file: if Not set then \"stdout\" is used for file-export");
    __MiF__printAg(stringOf_export_isToOnlyWriteOut_avg, "Set to '1' if only the average entropy is to be inlcuded in the result");
    //__MiF__printAg(stringOf_exportFile__format__par, "the format of the exported data-set: either \"tsv\", \"js\" or \"json\"");
    __MiF__printAg(inputData__maxPossibleScore__par, "the theorical max-number of counts, eg, the maximum number of categories an indiidual gene may be found in");
    __MiF__printAg(stringOf_enum__par_config__isToUsePostScaling__par, "Optional (boolean). If set then we apply lgoarithm-post-scaling to the entropy-score. Otherwise we only use lgoarithms for each entity");
#if(__MiV__support__freqEntropy == 1)
    __MiF__printAg(stringOf_enum__par_config__isToUseComputeFreq__par, "Optional (boolean). If set then we store the frequency-score for each vertex (ie, instead of comptuign the entropy-scores)");
#endif

    printf("\n For questions wrt. the hpLysis interface, please contact the senior developer [oekseth@gmail.com].\n");
    __MiF__printAg("", "--------------------------------------------");
    __MiF__printAg("-ex", "print example calls to demonstrate use.");
    //!
    //! At this exueicotn-point we asusme the oerpation was a success:
#undef __MiF__printAg
    return true;
  }
  //! ----------------------------------
  //!
  //! 
  if( (max_theoreticCountSize < 10) || (max_theoreticCountSize == UINT_MAX) ) {
    max_theoreticCountSize = 1000*1000;
  }
  //! ----------------------------------
  //!
  //! Apply the speicifed logics:
  //  printf("stringOf_inputFile_1=\"%s\", stringOf_inputFile_2=\"%s\", inputFile_isSparse=%d, at %s:%d\n", stringOf_inputFile_1, stringOf_inputFile_2, inputFile_isSparse, __FILE__, __LINE__);
  const bool is_ok = readFromFile__exportResult__hp_api_entropy(stringOf_enum, stringOf_inputFile_1, stringOf_exportFile, export_isToOnlyWriteOut_avg, max_theoreticCountSize, isTo_applyPostScaling, computeFor_freq); //stringOf_exportFile__format, nclusters, inputFile_isSparse);
  assert(is_ok);
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return is_ok;
}

