#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_hash_string.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.

/**
   @brief demonstrates a parsing-task where we laod data-rows into a string-1d-list, and then use logics in our "kt_hash_string.h" to split a string-set into (head, tail) parts.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- general: use logics in our "kt_hash_string.h" and our "kt_list_1d_string.h"
**/
int main() 
#else
  void tut_parse_8_fromSparseString_split_multipleTailsEachRow()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 5; 
  const char *mapOf_rows[nrows] = {
    "eide harriet  eide karl\t   eide kåre\t   eide karl\t   eide kåre",
"rem tore\t  bø tore ness tore\t   berg tore\t   pryser tore", 
"kerbel lucy \t  ebel uwe\t\tleber huck\t  keel aldo\t   key ellen\t   keel aldo",
"kovács ferenc \tkortner even\thorch franz\t kovacs david\t johns berit\t thiess frank",
"prøysen alf   \tprøysen elin"
  };
  //!
  //! Allocate:
  s_kt_list_1d_string_t obj_data = setToEmpty_andReturn__s_kt_list_1d_string_t();
  //  s_kt_list_1d_kvPair_t obj_data = init__s_kt_list_1d_kvPair_t(nrows);
  //!
  //! Set values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    assert(mapOf_rows[row_id]);
    set_stringConst__s_kt_list_1d_string(&obj_data, row_id, mapOf_rows[row_id]);
  }
  //! Load result:
  s_kt_hash_string_2dSparse_t obj_str_2d = init__fromHash__s_kt_hash_string_2dSparse_t(&obj_data, /*word_seperator_keyValues=*/'\t', /*word_seperator_valueValues=*/'\t', /*isTo_useDifferentMappingTables_forKeysIndexes=*/false);
  printf("current_pos=%u, and [%u][%u], at %s:%d\n", obj_str_2d.obj_2d.current_pos, obj_str_2d.obj_dense.obj_strings_cntInserted, obj_str_2d.obj_sparse.obj_strings_cntInserted, __FILE__, __LINE__);
  assert(obj_str_2d.obj_2d.current_pos >= nrows);
  // assert(obj_str_2d.obj_2d.current_pos == nrows); //! ie, as we expect two unqiuely inserted rows.
  assert(obj_str_2d.obj_dense.obj_strings_cntInserted  >= nrows);
  //  assert(obj_str_2d.obj_dense.obj_strings_cntInserted == 
  assert(0 == obj_str_2d.obj_sparse.obj_strings_cntInserted); //! ie, should reflect [ªbvoe]
  //  assert(obj_str_2d.obj_sparse.obj_strings_cntInserted == 6); //! ie, 

  //! 
  //! Comapre results: we expec tboth matrices to be equal:
  //  assert(obj_fromFile.list_size >= nrows);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    //assert(MF__isEqual__s_ktType_kvPair((obj_fromFile.list[row_id]), (obj_data.list[row_id] )));
  }

  //!
  //! De-allocates:
  free__s_kt_list_1d_string(&obj_data);
  free__s_kt_hash_string_2dSparse_t(&obj_str_2d);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

