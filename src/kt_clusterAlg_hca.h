#ifndef kt_clusterAlg_hca_h
#define kt_clusterAlg_hca_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @file kt_clusterAlg_hca
   @brief provide implementations for different Hierarchical Clustering Algorithms (HCAs)
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "kt_distance_cluster.h"
//#include "graphAlgorithms_distance.h"
#include "s_dense_closestPair.h"

#include "kt_clusterAlg_hca__node.h"

#include "kt_clusterAlg_hca_resultObject.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "e_kt_clusterAlg_hca_type.h"







/* /\** */
/*    @namespace kt_clusterAlg_hca */
/*    @brief provide implementations for different graph-algorithms. */
/*    @remarks the "c sltuering library" is used as template. */
/*    @author Ole Kristian Ekseth (oekseth). */
/* **\/ */
/* namespace graphAlgorithms_tree { */
/**
   @brief translates a tree of vertices into clusters based on the hierarchical clutering-results.
   @param <nelements> The number of elements that were clustered.
   @param <tree> The clustering solution. Each node in the array describes one linking event, with tree[i].left and tree[i].right representig the elements that were joined. The original elements are numbered 0..nelements-1, nodes are numbered -1..-(nelements-1). Size set to "nelements - 1"
   @param <nclusters> The number of clusters to be formed.
   @param <clusterid> The number of the cluster to which each element was assigned. Space for this array should be allocated before calling the cuttree routine. If a memory error occured, all elements in clusterid are set to -1. Size set to "nelements"
   @remarks The cuttree routine takes the output of a hierarchical clustering routine, and divides the elements in the tree structure into clusters based on the hierarchical clustering result. The number of clusters is specified by the user.
**/
void cuttree(const uint nelements, Node_t* tree, const uint nclusters, uint clusterid[]);

//! @return an tiantied object of 'k seperated lxuters' (oekseth, 06. feb. 2017).
static s_kt_clusterAlg_fixed_resultObject_t cuttree__kt_clusterAlg_hca(const s_kt_clusterAlg_hca_resultObject_t *self, uint nclusters) {
  assert(self);
  assert(self->nrows);
  assert(self->mapOf_nodes);
  assert(nclusters > 0);
  s_kt_clusterAlg_fixed_resultObject_t obj_result_kMeans;
  //! Allocate:  
  // fprintf(stderr, "nrows=%u, at %s:%d\n", self->nrows, __FILE__, __LINE__);
  init__s_kt_clusterAlg_fixed_resultObject_t(&obj_result_kMeans, nclusters, self->nrows);
  assert(obj_result_kMeans.cnt_vertex == self->nrows);
  assert(obj_result_kMeans.vertex_clusterId);
  //!
  //! Split:
  cuttree(self->nrows, self->mapOf_nodes, nclusters, obj_result_kMeans.vertex_clusterId);
  //! @return
  return obj_result_kMeans;
}


//! A slow permtuation fo teh "pcl-clsuter" HCA-algorithm
Node_t *pclcluster_slow(uint nrows, uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, char dist, uint transpose, const bool isTo_use_continousSTripsOf_memory);
/**
   @brief ...
   @remarks clusters using pairwise centroid-linking on a given set of gene expression data, using the distance metric given by dist.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix" The distance matrix. This matrix is precalculated by the calling routine treecluster. The pclcluster routine modifies the contents of distmatrix, but does not deallocate it.
   @return  a pointer to a newly allocated array of Node structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. See src/cluster.h for a description of the Node structure.
**/
Node_t* pclcluster (const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool needTo_useMask_evaluation);
//! A slow permtaution of the PSL cluster algorithm
Node_t* pslcluster_slow(const uint nrows, const uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, const char dist, const uint transpose);
/**
   @brief ... 
   @remarks The pslcluster routine performs single-linkage hierarchical clustering, using either the distance matrix directly, if available, or by calculating the distances from the data array. 
   - This implementation is based on the SLINK algorithm, described in: Sibson, R. (1973). SLINK: "An optimally efficient algorithm for the single-link cluster method". The Computer Journal, 16(1): 30-34.
   - The output of this algorithm is identical to conventional single-linkage hierarchical clustering, but is much more memory-efficient and faster. Hence, it can be applied to large data sets, for which the conventional single-linkage algorithm fails due to lack of memory.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix": The distance matrix. If the distance matrix is passed by the calling routine treecluster, it is used by pslcluster to speed up the clustering calculation. The pslcluster routine does not modify the contents of distmatrix, and does not deallocate it. Ifdistmatrix is NULL, the pairwise distances are calculated by the pslcluster routine from the gene expression data (the data and mask arrays) and stored in temporary arrays. Ifdistmatrix is passed, the original gene expression data (specified by the data and mask arguments) are not needed and are therefore ignored.
   @return A pointer to a newly allocated array of Node structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* pslcluster(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, float** data, uint** mask, float weight[], float** distmatrix, const uint transpose, const bool needTo_useMask_evaluation);
//! A slow permtautioni of the PML-cluster algorithm
Node_t* pmlcluster_slow(const uint nelements, float** distmatrix);
/**
   @brief Compute clusters using the PML cluster algorithm.
   @remarks The pmlcluster routine performs clustering using pairwise maximum- (complete-) linking on the given distance matrix.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix": The distance matrix, with nelements rows, each row being filled up to the diagonal. The elements on the diagonal are not used, as they are assumed to be zero. The distance matrix will be modified by this routine.
   @return A pointer to a newly allocated array of Node structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* pmlcluster(const uint nelements, float** distmatrix);
//! A slow permtuation of the PAL HCA clsuter-algorithm
Node_t* palcluster_slow(const uint nelements, float** distmatrix);
/**
   @brief comptue the PAL cluster.
   @remarks The palcluster routine performs clustering using pairwise average linking on the given distance matrix.
   @remarks in below we describe a few number of the parameters:
   # "distmatrix": The distance matrix, with nelements rows, each row being filled up to the diagonal. The elements on the diagonal are not used, as they are assumed to be zero. The distance matrix will be modified by this routine.
   @return A pointer to a newly allocated array of Node structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* palcluster(const uint nelements, float** distmatrix);
/**
   @brief ...
   @remarks The treecluster routine performs hierarchical clustering using pairwise single-, maximum-, centroid-, or average-linkage, as defined by method, on a given set of gene expression data, using the distance metric given by dist. If successful, the function returns a pointer to a newly allocated Tree struct containing the hierarchical clustering solution, and NULL ifa memory error occurs. The pointer should be freed by the calling routine to prevent memory leaks.
   @remarks in below we describe a few number of the parameters:
   # "method": Defines which hierarchical clustering method is used:
   - method=='s': pairwise single-linkage clustering
   - method=='m': pairwise maximum- (or complete-) linkage clustering
   - method=='a': pairwise average-linkage clustering
   - method=='c': pairwise centroid-linkage clustering
   - For the first three, either the distance matrix or the gene expression data is sufficient to perform the clustering algorithm. For pairwise centroid-linkage clustering, however, the gene expression data are always needed, even if the distance matrix itself is available.
   # "distmatrix": The distance matrix. Ifthe distance matrix is zero initially, the distance matrix will be allocated and calculated from the data by treecluster, and deallocated before treecluster returns. Ifthe distance matrix is passed by the calling routine, treecluster will modify the contents of the distance matrix as part of the clustering algorithm, but will not deallocate it. The calling routine should deallocate the distance matrix after the return from treecluster.
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
   @return A pointer to a newly allocated array of Node structs, describing the hierarchical clustering solution consisting of nelements-1 nodes. Depending on whether genes (rows) or microarrays (columns) were clustered, nelements is equal to nrows or ncolumns. 
**/
Node_t* treecluster__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, float** data, uint** mask, float weight[], uint transpose, char method, float** distmatrix, const bool isTo_use_continousSTripsOf_memory /* = true */, const bool isTo_useImplictMask /* = true */, const bool isTo_use_fastFunction /* = true */);



/**
   @brief the system test for class rule_set.
   @param <print_info> if set a limited status information is printed to STDOUT.
   @return true if test completed.
**/
bool assert_class(const bool print_info);
//};

#endif
