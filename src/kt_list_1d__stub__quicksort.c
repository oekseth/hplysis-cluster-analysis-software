//! ---------------------------------------------------------------------
//! ---------------------------------------------------------------------
//! 
//! Apply quick-sort.
static void __MiFn__sort__localFunc(__MiFn__sort__type *list, uint l, uint u) {
  //assert(l < u);
  //assert((l+1) < u); //! ie, to avoid a 'poitnless' cufnciotn-call, ie, an optmizoant-hueristic
  //int m, idx_temp;
  __MiFn__sort__type a_temp;
  if (l >= u)
    return;

  //  printf("(sort-range): [%u ... %u], at %s:%d\n", l, u, __FILE__, __LINE__);
  
  uint m = l;
  for (uint i=l+1; i<=u; i++)  {
    if(__MiF__objLessThan(list[i], list[l])) {
    //    if (list[i].tail < list[l].tail) {
      m++;	  
      /* idx_temp = idx[m]; */
      /* idx[m] = idx[i]; */
      /* idx[i] = idx_temp; */
      
      a_temp  = list[m];
      list[m] = list[i];
      list[i] = a_temp;
    }     
  }  
  /* idx_temp = idx[l]; */
  /* idx[l] = idx[m]; */
  /* idx[m] = idx_temp; */  
  a_temp  = list[l];
  list[l] = list[m];
  list[m] = a_temp;
  //  printf("range=[%u ... %u ... %u], at %s:%d\n", l, m, u, __FILE__, __LINE__); 

  if(m > 1) {
    __MiFn__sort__localFunc(list, l, m-1); 
  }
  __MiFn__sort__localFunc(list, m+1, u); 

/* #define __compare_swap(ind_1, ind_2) ({ if(__MiF__objLessThan(list[ind_2], list[ind_1])) { \ */
/*       a_temp  = list[ind_1]; \ */
/*       list[ind_1] = list[ind_2]; \ */
/*       list[ind_2] = a_temp; } }) */

/*   if(m > l) { */
/*     if( (l +1) < (m-1) ) { */
/*       //    printf("sort(1), at %s:%d\n", __FILE__, __LINE__); */
/*       __MiFn__sort__localFunc(list, l, m-1); */
/*     } else { //! then a local swap: */
/* /\* #define ind_1 l *\/ */
/* /\* #define ind_2 m-1 *\/ */
/*       // printf("swaps: %u--%u, at %s:%d\n", l, m-1, __FILE__, __LINE__); */
/*       __compare_swap(l, (m-1)); */
/* /\* #undef ind_2 *\/ */
/* /\* #undef ind_2 *\/ */
/*     } */
/*   } */
/*   //if( (m+0) < u) { */
/*   if( (m+1) <= u) { */
/*     //printf("sort(2), at %s:%d\n", __FILE__, __LINE__); */
/*     if((m+2) < u) { */
/*       __MiFn__sort__localFunc(list, m+1, u); */
/*     } else { */
/* /\* #define ind_1 m *\/ */
/* /\* #define ind_2 u *\/ */
/* //      printf("swaps: %u--%u, at %s:%d\n", m, u, __FILE__, __LINE__); */
/*       __compare_swap(m, u); */
/*       //      __compare_swap(); */
/* /\* #undef ind_2 *\/ */
/* /\* #undef ind_2 *\/ */
/*     } */
/*     //    __MiFn__sort__localFunc(list, m+1, u); */
/*   } */
#undef __compare_swap
}
//! ---------------------------------------------------------------------
//! ---------------------------------------------------------------------
//!
#undef __MiFn__sort__localFunc
#undef __MiF__objLessThan
