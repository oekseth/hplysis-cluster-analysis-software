#ifndef aux_exportMeta_h
#define aux_exportMeta_h

/**
   @file aux_exportMeta
   @breif provide gnerlaized funcitosn to construct meta-data-file-presentations (oekseth, 06. mar. 2017).
 **/
typedef enum e_aux_exportMeta__exportFormat {
  e_aux_exportMeta__exportFormat_highCharts,
  e_aux_exportMeta__exportFormat_undef
} e_aux_exportMeta__exportFormat_t;

/**
   @struct s_aux_exportMeta
   @brief provide gnerlaised lgocis to exprot data-meta-objects (oekseth, 06. mar. 2017).
 **/
typedef struct s_aux_exportMeta {
  FILE *fileP_meta;
  FILE *fileP_meta_DOM; //! which is sued for the HTML.
} s_aux_exportMeta_t;

//! Allcoats the s_aux_exportMeta_t object (oesketh, 06. mar. 2017)
//! @return the enw-allcoated object.
static s_aux_exportMeta_t init__s_aux_exportMeta_t(const char *stringOf_resultPrefix) {
  char nameOf_resultFile_meta_dom[2000] = {'\0'}; sprintf(nameOf_resultFile_meta_dom, "%s%s", stringOf_resultPrefix, "_meta.html");
  FILE *fileP_meta_DOM = fopen(nameOf_resultFile_meta_dom, "wb");
  assert(fileP_meta_DOM);
  char nameOf_resultFile_meta[2000] = {'\0'}; sprintf(nameOf_resultFile_meta, "%s%s", stringOf_resultPrefix, "_meta.js");
  FILE *fileP_meta = fopen(nameOf_resultFile_meta, "wb");
  assert(fileP_meta);
  //! 
  //! Inti object and udpate pointers
  s_aux_exportMeta_t self;
  self.fileP_meta = fileP_meta;
  self.fileP_meta_DOM = fileP_meta_DOM;
  //! @return
  return self;
}
//! De-allcoats the s_aux_exportMeta_t object (oesketh, 06. mar. 2017)
static void free__s_aux_exportMeta_t(s_aux_exportMeta_t *self) {
  assert(self);
  if(self->fileP_meta) {
    fclose((self->fileP_meta)); self->fileP_meta = NULL;
  }
  if(self->fileP_meta_DOM) {
    fclose((self->fileP_meta_DOM)); self->fileP_meta_DOM = NULL;
  }

}

//! Export meta-data-files assuming the input describes a line-plot (oesketh, 06. mar. 2017).
static void linePlot__aux_exportMeta(const uint DOM_ID, e_aux_exportMeta__exportFormat_t enum_id, FILE *fileP_meta, FILE *fileP_meta_DOM, const char *nameOf_resultFile_data, const char *string_description) {
  assert(fileP_meta);
  assert(fileP_meta_DOM);
  if(enum_id == e_aux_exportMeta__exportFormat_undef) {enum_id = e_aux_exportMeta__exportFormat_highCharts;}
  //!
  //! Apply logics:
  if(enum_id == e_aux_exportMeta__exportFormat_highCharts) {
    fprintf(fileP_meta_DOM, "\n<div id=\"container_%u\" style=\"min-width: 310px; height: 450px; margin: 0 auto\"></div>", DOM_ID);
    //! Add the JC-config:
    fprintf(fileP_meta, "\n"
	    "__load_chart("
	    "{\n"
	    "dom : \"container_%u\","
	    "inputFile : \"%s\","
	    "body_text : \"%s\","
	    "body_text_sub : \"performed in hpLysis\","
	    "}); //! geernated froum our [%s]:%s:%d by (oekseth, 06. mar. 2017)",
	    DOM_ID,
	    /*file=*/nameOf_resultFile_data, 
	    string_description, 
	    __FUNCTION__, __FILE__, __LINE__);        
  } else {assert(false);} //! ie,a s we then need adding supprot 'for this use-case'.  
}

//! Export meta-data-files assuming the input describes a line-plot (oesketh, 06. mar. 2017).
static void o_linePlot__s_aux_exportMeta(s_aux_exportMeta_t *self, const uint DOM_ID, e_aux_exportMeta__exportFormat_t enum_id,const char *nameOf_resultFile_data, const char *string_description) {
  assert(self);
  assert(self->fileP_meta);
  assert(self->fileP_meta_DOM);
  //!
  //! Apply logics:
  linePlot__aux_exportMeta(DOM_ID, enum_id, self->fileP_meta, self->fileP_meta_DOM, nameOf_resultFile_data, string_description);
}

#endif //! EOF
