
  //! --------------------------------------------------------------
//const uint index1 = obj_config.index1;
const uint nrows = obj_config.nrows;
const uint ncols = obj_config.ncols;
t_float **data1 = obj_config.data1;
t_float **data2 = obj_config.data2;
char** mask1  = obj_config.mask1;
char** mask2 = obj_config.mask2;
const t_float *weight = obj_config.weight;
const e_kt_correlationFunction_t typeOf_metric = obj_config.typeOf_metric;
const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation = obj_config.typeOf_metric_correlation;
s_allAgainstAll_config_t *config_allAgainstAll = obj_config.config_allAgainstAll;
void *s_inlinePostProcess = config_allAgainstAll->s_inlinePostProcess;
assert(config_allAgainstAll);
//const uint transpose = config_allAgainstAll->transpose;
s_allAgainstAll_config_t config = *config_allAgainstAll;
t_float **resultMatrix = obj_config.resultMatrix;

#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
// TODO: validate ath[below] 'proerpteis' amanges to cpature the inded usage of 'this code-chunk' (oekseth, 06. nov. 2016).
//printf("config.sparseDataResult=%p, at %s:%d\n", config.sparseDataResult, __FILE__, __LINE__);
if( (config.s_inlinePostProcess == NULL) && (config.sparseDataResult == NULL) ) {assert(resultMatrix);}
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
const t_float default_value = T_FLOAT_MIN; //! ie, then intiate the values to 'nearly 0', though with difference tha tthe latter is used as a 'mark' to state that 'a value has Not been set';
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
const t_float default_value = T_FLOAT_MAX; //! ie, where the selected-valeu will alwyas be 'below this'.
#else //! tehnw e are itnerested in the max-value
const t_float default_value = T_FLOAT_MIN_ABS; //! ie, where the selected-valeu will alwyas be 'below this'.
#endif //! otehrwise we assuemt that eh compelx object is udpated, a 'compelx object' which we anyhow choose to udpate (ie, aslos tof rthe 'only-compare-tow-rows' case).
#if(configureDebug_useExntensiveTestsIn__tiling_verbous == 1)
//printf("(info)\t intiates the result-matrix to default=%f, at %s:%d\n", default_value, __FILE__, __LINE__);
#endif

const uint nrows_2 = (config_allAgainstAll->iterationIndex_2 != UINT_MAX) ? config_allAgainstAll->iterationIndex_2 : nrows;

if(resultMatrix != NULL) {
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < nrows_2; col_id++) {
      resultMatrix[row_id][col_id] = default_value;
    }
  }
 } else {
  assert( (config.s_inlinePostProcess != NULL) || (config.sparseDataResult != NULL) );
 }

  t_float result = default_value;
#else
t_float result = 0;
#endif



//printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);

//t_float *arrOf_result = obj_config.arrOf_result;
  //! --------------------------------------------------------------
