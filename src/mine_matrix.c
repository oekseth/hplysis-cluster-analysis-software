#include "kt_list_2d.h"

/**
   @struct s_localPair
   @brief stores a pair of x-y-valeus (oesketh, 60. july 2016).
   @author Ole Kristian Ekseth (oekseth)
   @remarks used to avoid lag wrt. the use of 'only the triangular half' of a matrix during paralle comptuations
 **/
typedef struct s_localPair {
  uint x; uint y;
} s_localPair_t;
 

//! -----------------------------------------------------------------------
//! -----------------------------------------------------------------------
//! -----------------------------------------------------------------------
#ifndef __MINE__notInclude__s_mine_wrapper_t
#include "mine_matrix.h" 
//! Compute distance-resutls for cell
void __computeFor_cell(s_mine_wrapper_t *obj, const uint x, const uint y, int **arrOf_sorted, const bool isTo_writeOut_duringComptuation_humanized_dense, const bool isTo_compute_nonMine_correlations, int **mask_dummy, t_float *weight_dummy) {

  bool isSymmetric = false;
  //if(isTo_use_codeRewritingTo_reduceExeuctionTime()) 
#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1)
  {
    //! then we ivnestgiate if the relationship is symmetric:

#if (configure_parallel_2dLoop_symmetric_useFastScheduling == 1) //! ie, as we otherwise are interested in testing the overhead 'of not using this optmized approach'.
    assert(x < y); //! ie, what we then expect by the caller of this fucntion
#else //! then we need to evluate htis case explictly:
    if(y > x) {isSymmetric = true;}
#endif
  }
#endif
  if(isSymmetric == false) {
    if(isTo_compute_nonMine_correlations == false) {

      /* //! Then compute the relationships: */
      /* mine_matrix(obj->data_obj, obj->listOf_result, x, y, arrOf_sorted, isTo_writeOut_duringComptuation_humanized_dense, isTo_compute_nonMine_correlations); */

      mine_score *score = NULL;
      //! Intiate:
      mine_problem prob;
      prob.x = obj->data_obj->matrixOf_data[x];
      prob.y = obj->data_obj->matrixOf_data[y];
      prob.n = obj->data_obj->cnt_columns; //! ie, the number of features
      if(obj->local_isTo_use_parallelSchduling_slow == false) {
	//score = mine_compute_score_notInParallel(&prob, &param);
	      
	int *ix = NULL; int *iy = NULL;
	if(isTo_use_codeRewritingTo_reduceExeuctionTime()) {
	  ix = arrOf_sorted[x]; 		iy = arrOf_sorted[y];
	} else {
	  ix = argsort(prob.x, prob.n); 
	  iy = argsort(prob.y, prob.n);
	} 
	score = mine_compute_score_notAllocate_logValues(&prob, &(obj->param), /*sortedArr_prob_x=*/ix, /*sortedArr_prob_y=*/iy);

	if(isTo_use_codeRewritingTo_reduceExeuctionTime() == false) {
	  free(iy);   free(ix);
	}
      } else {
	assert(false); // FIXME: include ["elow]
	//score = mine_compute_score_slow(&prob, &param);
      }

      assert(score);
  
      if(isTo_writeOut_duringComptuation_humanized_dense) {
	if(true) { //! a 'case' which need to be active in optmized compilation, ie, to 'ensure' that the compiler does not 'optmizeze aways' the code-components of the Mine-software.impelemtantion.
	  if(obj->data_obj->nameOf_rows != NULL) { //! then we make use of the string-represetnation:
	    printf("\"%s\"-->\"%s\": mic=%.4f, mas=%.4f, mev=%.4f, mcn=%.4f|eps=%.3f, mcn-general=%.4f, gmic=%.4f|power=%.3f, tic=%.4f, at %s:%d\n",
		   obj->data_obj->nameOf_rows[x], obj->data_obj->nameOf_rows[y], //! ie, as we expect both "x" and "y" to be 'found' in the inptu-aprsed-columns
		   mine_mic(score),
		   mine_mas(score),
		   mine_mev(score),
		   mine_mcn(score, obj->computeScore_mcn_eps),  obj->computeScore_mcn_eps,
		   mine_mcn_general(score),
		   mine_gmic(score, obj->computeScore_gmic_power), obj->computeScore_gmic_power,
		   mine_tic(score),
		   __FILE__, __LINE__
		   );
	  } else {
	    printf("result[%u][%u]: mic=%.4f, mas=%.4f, mev=%.4f, mcn=%.4f|eps=%.3f, mcn-general=%.4f, gmic=%.4f|power=%.3f, tic=%.4f, at %s:%d\n",
		   x, y,
		   mine_mic(score),
		   mine_mas(score),
		   mine_mev(score),
		   mine_mcn(score, obj->computeScore_mcn_eps),  obj->computeScore_mcn_eps,
		   mine_mcn_general(score),
		   mine_gmic(score, obj->computeScore_gmic_power), obj->computeScore_gmic_power,
		   mine_tic(score),
		   __FILE__, __LINE__
		   );
	  }
	} //! else we assume this option is de-acitvies
      } else { //! then update the result-set:
	assert(obj->listOf_result);
	obj->listOf_result[e_mine_typeOf_scores_mic].matrixOf_data[x][y] = mine_mic(score);
	obj->listOf_result[e_mine_typeOf_scores_mas].matrixOf_data[x][y] = mine_mas(score);
	obj->listOf_result[e_mine_typeOf_scores_mev].matrixOf_data[x][y] = mine_mev(score);
	obj->listOf_result[e_mine_typeOf_scores_mcn].matrixOf_data[x][y] = mine_mcn(score, obj->computeScore_mcn_eps);
	obj->listOf_result[e_mine_typeOf_scores_mcn_general].matrixOf_data[x][y] = mine_mcn_general(score);
	obj->listOf_result[e_mine_typeOf_scores_gmic].matrixOf_data[x][y] = mine_gmic(score, obj->computeScore_gmic_power);
	obj->listOf_result[e_mine_typeOf_scores_tic].matrixOf_data[x][y] = mine_tic(score);
      }
	    
      //! De-allocate.
      if(score) {mine_free_score(&score);}
    } else { //! Then call the funciotns in the "cluster.c" library:
#if 1 == 0
      const t_float result_euclid = euclid(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      const t_float result_cityBlock = cityblock(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      //! Correlations:
      const t_float result_correlation = correlation(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      const t_float result_aCorrelation = acorrelation(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      const t_float result_uCorrelation = ucorrelation(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      const t_float result_uaCorrelation = uacorrelation(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      //! Correlations which use sort and rank:
      const t_float result_spearMan = spearman(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
      const t_float result_kendall = kendall(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0);
#else  //! then we test the exec-time only for one of the correlations, ie, to be used as a 'comparison' wrt. the 'ovrhead-cost of usign the Mine-correlation-emtric':
      //! Note[article]: we observe that for the following/below command-line-argumetns Spearmnas correlation-emtric goes 524x faster than our improved Mine-implemetnaiton, ie, an argumetn for reducign the exueciton-time of the mine-correlation-metric-implementaiton.
      assert(mask_dummy); assert(weight_dummy);
      // const t_float result_euclid = acorrelation(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0); const t_float result_aCorrelation = 1; 
      const t_float result_aCorrelation = acorrelation(/*n=*/(int)obj->data_obj->cnt_columns, /*data1=*/obj->data_obj->matrixOf_data, /*data2=*/obj->data_obj->matrixOf_data, mask_dummy, mask_dummy, weight_dummy, /*index1=*/x, /*index2=*/y, /*transpose=*/0); const t_float result_euclid = 1; 
      //! Then set the other vairalbes to 'dummy' values:

      //      
      const t_float result_cityBlock = 1; const t_float result_correlation = 1;  const t_float result_uCorrelation = 1; const t_float result_uaCorrelation = 1; const t_float result_spearMan = 1; const t_float result_kendall = 1;
#endif
      //! Include the result in the result-set:
      if(isTo_writeOut_duringComptuation_humanized_dense) {
	if(obj->data_obj->nameOf_rows != NULL) { //! then we make use of the string-represetnation:
	  printf("\"%s\"-->\"%s\": euclid=%.4f, cityBlock=%.4f, correlation=%.4f, aCorrelation=%.4f, uCorrelation=%.4f, uaCorrelation=%.4f, spearman=%.4f, kendall=%f, at %s:%d\n",
		 obj->data_obj->nameOf_rows[x], obj->data_obj->nameOf_rows[y], //! ie, as we expect both "x" and "y" to be 'found' in the inptu-aprsed-columns
		 (float)result_euclid, (float)result_cityBlock, 
		 (float)result_correlation, (float)result_aCorrelation, (float)result_uCorrelation, (float)result_uaCorrelation,
		 (float)result_spearMan, (float)result_kendall,
		 __FILE__, __LINE__
		 );
	} else {
	  printf("result[%u][%u]: euclid=%.4f, cityBlock=%.4f, correlation=%.4f, aCorrelation=%.4f, uCorrelation=%.4f, uaCorrelation=%.4f, spearman=%.4f, kendall=%f, at %s:%d\n", 
		 x, y,
		 (float)result_euclid, (float)result_cityBlock, 
		 (float)result_correlation, (float)result_aCorrelation, (float)result_uCorrelation, (float)result_uaCorrelation,
		 (float)result_spearMan, (float)result_kendall,
		 __FILE__, __LINE__
		 );
	}
      } else { //! then update the result-set:	      
	obj->listOf_result[e_mine_typeOf_scores_clusterC_euclid].matrixOf_data[x][y] = result_euclid;
	obj->listOf_result[e_mine_typeOf_scores_clusterC_cityblock].matrixOf_data[x][y] = result_cityBlock;
	//! Correlations:
	obj->listOf_result[e_mine_typeOf_scores_clusterC_correlation].matrixOf_data[x][y] = result_correlation;
	obj->listOf_result[e_mine_typeOf_scores_clusterC_correlation_absolute].matrixOf_data[x][y] = result_aCorrelation;
	obj->listOf_result[e_mine_typeOf_scores_clusterC_correlation_uncentered].matrixOf_data[x][y] = result_uCorrelation;
	obj->listOf_result[e_mine_typeOf_scores_clusterC_correlation_uncentered_absolute].matrixOf_data[x][y] = result_uaCorrelation;
	//! Correlations which use sort and rank:
	obj->listOf_result[e_mine_typeOf_scores_clusterC_spearman].matrixOf_data[x][y] = result_spearMan;
	obj->listOf_result[e_mine_typeOf_scores_clusterC_kendall].matrixOf_data[x][y] = result_kendall;
      }
    } 
  }
#if (configure_parallel_2dLoop_symmetric_useFastScheduling == 0) //! ie, as we otherwise will 'copy-paste' for the complete set of non-evlauated (x, y) pairs:
 else 
#endif
   { //! then we copy-paste the set of values form an 'earlier' copmtuation:
    for(uint enum_id = 0; enum_id < (uint)e_mine_typeOf_scores_undef; enum_id++) {
      obj->listOf_result[enum_id].matrixOf_data[y][x] = obj->listOf_result[enum_id].matrixOf_data[x][y];
    }
  }
}


//! Compute 2d-scores for the object
void compute_2d_scoreMatrix(s_mine_wrapper_t *obj, const bool isTo_copmuteOnly_forOneRow, const bool isTo_writeOut_duringComptuation_humanized_dense, const bool isTo_compute_nonMine_correlations) {
  //! Intalize the optmized log-orutine:
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 0) //! ie, as we then expec thte 'data-strucutre' tio Not have been loaded at 'run-time'.
  allocate_optmize_usePReComptued_floats();
#endif

  //! Make it possible for the caller to copmute only for one of the 'inner rows':
  const uint cnt_rows_in = (!isTo_copmuteOnly_forOneRow) ? obj->data_obj->cnt_rows : 1;

  {

    int **mask_dummy = NULL; t_float *weight_dummy  = NULL;
    if(isTo_compute_nonMine_correlations == true) {
      const int default_value_int = 1; //! ie, mark eveyr valeus as of interest.
      mask_dummy = allocate_2d_list_int(obj->data_obj->cnt_rows, obj->data_obj->cnt_columns, default_value_int);
      const t_float default_value_float = 1; //! ie, as we expect all veritces have the same weight.
      weight_dummy = allocate_1d_list_float(obj->data_obj->cnt_columns, default_value_float);
    }

    int **arrOf_sorted = NULL;
    if(isTo_use_codeRewritingTo_reduceExeuctionTime() == true) {
      arrOf_sorted = (int **) malloc(obj->data_obj->cnt_rows * sizeof(int*)); //allocate_2d_list_int(obj->data_obj->cnt_rows, obj->data_obj->cnt_columns);
      //! Comptute sthe 'sorted' vlaues:
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#if (optmize_parallel_2dLoop_useFast == 1)
    //printf("cnt-trheads=%u/%u, at %s:%d\n", (uint)omp_get_num_threads(), (uint)omp_get_max_threads(), __FILE__, __LINE__);
    //#pragma omp parallel
#pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
    //#pragma omp parallel for schedule(static, 5) //schedule(dynamic,3) //! where "3" is based on observaitons
#elif (optmize_parallel_2dLoop_useFast_slowScheduling == 1) //! then we make use of a parallel wrapper
#pragma omp parallel //! which we have observe for soem cases results in a signifcnat slow-down.
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
#else
    // printf("(info)\t does not make use of parallelsim wr.t the 2d-loop, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif
#endif
    //! Start the iteration:
      for(uint x = 0; x < cnt_rows_in; x++) {
	arrOf_sorted[x] = argsort(obj->data_obj->matrixOf_data[x], obj->data_obj->cnt_columns); 
      }

    } //! otherwise we assume it is not of interest to pre-comptue teh data-sets, eg, to test the exeuction-time-improtance of [ªbove] (oekseth, 06. july 2016).

#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1) //! then we intiate a set of pairs, ie, to ensure that the threads are givne an 'equal' block-size.
    const uint max_size = (uint)obj->data_obj->cnt_rows * (uint)cnt_rows_in;
    s_localPair_t *listOf_pairs = (s_localPair_t*)malloc(max_size*sizeof(s_localPair_t));
    assert(listOf_pairs);
    uint listOf_pairs_size = 0;
    //! Idnetify the ide of the pairs:
    for(uint x = 0; x < cnt_rows_in; x++) {
      for(uint y = 0; y < obj->data_obj->cnt_rows; y++) { //! ie, we comapre a set of rows, ie, not 'row x column' through instead 'roq x row':
#if (configure_parallel_2dLoop_symmetric_useFastScheduling == 1) //! ie, as we otherwise are interested in testing the overhead 'of not using this optmized approach'.
	if( (x < y) ) 
#endif
	  {
	    listOf_pairs[listOf_pairs_size].x = x; 
	    listOf_pairs[listOf_pairs_size].y = y;
	    //! Increment the count:
	    listOf_pairs_size++;
	  }	
      }
    }
    //printf("listOf_pairs_size=%u, max_size=%u, at %s:%d\n", listOf_pairs_size, max_size, __FILE__, __LINE__);
    assert(listOf_pairs_size > 0); //! ie, what we expect.
#endif


#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#if (optmize_parallel_2dLoop_useFast == 1)
    //printf("cnt-trheads=%u/%u, at %s:%d\n", (uint)omp_get_num_threads(), (uint)omp_get_max_threads(), __FILE__, __LINE__);
    //#pragma omp parallel
    #pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
    //#pragma omp parallel for schedule(static, 2) //schedule(dynamic,3) //! where "3" is based on observaitons
#elif (optmize_parallel_2dLoop_useFast_slowScheduling == 1) //! then we make use of a parallel wrapper
#pragma omp parallel //! which we have observe for soem cases results in a signifcnat slow-down.
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
#else
    // printf("(info)\t does not make use of parallelsim wr.t the 2d-loop, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif
#endif
    //! Start the iteration:
#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1)
    for(uint pair_id = 0; pair_id < listOf_pairs_size; pair_id++) 
#else
    for(uint x = 0; x < cnt_rows_in; x++) 
#endif
      {
      /* #if (optmize_parallel_2dLoop_useFast == 1) ||  (optmize_parallel_2dLoop_useFast_slowScheduling == 1) //! then we make use of a parallel wrapper */
      /*       const uint thread_id = (uint)omp_get_thread_num();      */
      /*       printf("[%u]\t thread_id=%u, at %s:%d\n", x, thread_id, __FILE__, __LINE__); */
      /* #endif */

#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1)
	const uint x = listOf_pairs[pair_id].x;
	const uint y = listOf_pairs[pair_id].y;
	//! Compute distance-resutls for cell:
	__computeFor_cell(obj, x, y, arrOf_sorted, isTo_writeOut_duringComptuation_humanized_dense, isTo_compute_nonMine_correlations, mask_dummy, weight_dummy);
#else 
	assert(obj->data_obj->matrixOf_data[x]);
	for(uint y = 0; y < obj->data_obj->cnt_rows; y++) { //! ie, we comapre a set of rows, ie, not 'row x column' through instead 'roq x row':
	  if(x != y) { //! ie, to avodi a 'self-comparison':
	    
	    //! Compute distance-resutls for cell:
	    __computeFor_cell(obj, x, y, arrOf_sorted, isTo_writeOut_duringComptuation_humanized_dense, isTo_compute_nonMine_correlations, mask_dummy, weight_dummy);
	  }
	}
#endif
      }  

    //! Free the optmized log-orutine: 
    if(mask_dummy) {free_2d_list_int(&mask_dummy);} 
    if(weight_dummy) {free_1d_list_float(&weight_dummy);}
    if(isTo_use_codeRewritingTo_reduceExeuctionTime() == true) {
      for(uint x = 0; x < cnt_rows_in; x++) {
	free(arrOf_sorted[x]);
      }
      free(arrOf_sorted);
    }
#if (optmize_2d_loop_makeUseOf_symmetricProperty == 1) //! then we intiate a set of pairs, ie, to ensure that the threads are givne an 'equal' block-size.
    assert(listOf_pairs_size > 0); assert(listOf_pairs);
    free(listOf_pairs);
#endif

  }
  //! Free the optmized log-orutine: 
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 0)
  free_optmize_usePReComptued_floats();
#endif
}


#endif //! "__MINE__notInclude__s_mine_wrapper_t"
//! -----------------------------------------------------------------------
//! -----------------------------------------------------------------------
//! ----------------------------------------------------------------------- 


#ifndef macro_max
#define macro_max(x, y) ((x) > (y) ? (x) : (y))
#endif
#ifndef macro_min
#define macro_min(x, y) ((x) < (y) ? (x) : (y))
#endif

#define MF__getInvertScore_MINE(score) ({(score != 0) ? 1/score : T_FLOAT_MAX;})

#ifdef configure_generic_isToInvertCorrelationScoresForMetric__MINE
#if(configure_generic_isToInvertCorrelationScoresForMetric__MINE == 1)
//! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
#define localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix) ({if(resultMatrix) { \
  for(uint row_id = 0; row_id < nrows_1; row_id++) {			\
  assert_possibleOverhead(resultMatrix[row_id]); \
  for(uint col_id = 0; col_id < nrows_2; col_id++) {			\
    const t_float score = resultMatrix[row_id][col_id];			\
    if( (score != T_FLOAT_MAX) && (score != T_FLOAT_MIN_ABS) ) { \
      /* if( ((uint)score < 0) || ((uint)score > max_value) ) {fprintf(stderr, "!!\t Investigate why score=%f given max_value=%f, at %s:%d\n", score, max_value, __FILE__, __LINE__);} assert_possibleOverhead((uint)score <= max_value); assert_possibleOverhead(score >= 0); */ \
      resultMatrix[row_id][col_id] = MF__getInvertScore_MINE(score);}	\
  } } }})
#else
#define localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix) ({;}) //! ie, then a 'dummy' funciton
#endif
#else
#define localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix) ({;}) //! ie, then a 'dummy' funciton
#endif


//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__MIC(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  //  assert(resultMatrix);
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_mic(score);})

  // printf("at %s:%d\n", __FILE__, __LINE__);

  //! 
  //! The logic:
  const t_float max_value = 1;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}

//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__MAS(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_mas(score);})


  //! 
  //! The logic:
  //printf("at %s:%d\n", __FILE__, __LINE__);
  const t_float max_value = 1;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}



//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__MEV(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  //  assert(resultMatrix);
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_mev(score);})
  //! 
  //! The logic:
  //printf("at %s:%d\n", __FILE__, __LINE__);
  const t_float max_value = 1;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}

//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__TIC(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  //  assert(resultMatrix);
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_tic(score);})
  //! 
  //! The logic:
  //printf("at %s:%d\n", __FILE__, __LINE__);
  const t_float max_value = 2;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}
//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__MCN_GENERAL(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  //  assert(resultMatrix);
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_mcn_general(score);})
  //! 
  //! The logic:
  // printf("at %s:%d\n", __FILE__, __LINE__);
  const t_float max_value = 1;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}

//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__MCN(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, const t_float eps, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  //  assert(resultMatrix);
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_mcn(score, eps);})
  //! 
  //! The logic:
  // printf("at %s:%d\n", __FILE__, __LINE__);
  const t_float max_value = 1;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}

//! Compute 2d-scores for the object
//! @remarks a function specifically written in order to reflect the needs of our hpLysis cluster-analysis software for fast correlation-metic copmtuations (oekseth, 06. nov. 2016).
void compute_2d_scoreMatrix__matrixAsInput__forType__GMIC(t_float **matrix_1, t_float **matrix_2, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **resultMatrix, const t_float power, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult) {
  if(sparseDataResult == NULL) {
    assert(resultMatrix);
  }
  //  assert(resultMatrix);
  assert(matrix_1);
  assert(matrix_2);
  assert(nrows_1 > 0);   assert(nrows_2 > 0);
  if( (matrix_1 == matrix_2) && (nrows_1 == 1) && (nrows_1 == nrows_2 ) ) {return; /*T_FLOAT_MAX;*/} //! ie, as 'we then comapre the row to itself', ie, a pointless call (though not necceisarly an erorr, ie, to simplify the lgocsi in the caller) (oekseth, 06. des. 2016).
  //! Speicyf the type of funciton:
#define __internalFunc__() ({mine_gmic(score, power);})
  //! 
  //! The logic:
  // printf("at %s:%d\n", __FILE__, __LINE__);
  const t_float max_value = 1;
#include "mine_matrix__func__spec.h" //! (oekseth, 06. nov. 2016)
  //! Reset internal vairalbes:
#undef __internalFunc__
  //! 'Invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
  localMineMacroFunction__invertScores(nrows_1, nrows_2, resultMatrix);
}




//void mine_matrix(s_dataStruct_matrix_dense_t *data_obj, s_dataStruct_matrix_dense_t *listOf_result, 
