#ifndef counting_sort_h
#define counting_sort_h
#include "libs.h"
//#include "list.h"

/**
   @Name: counting_sort -- Sorts input data based on the value used as index.
   -- As the counting sort preserves the order of elements having equal value (with regard to whats sorted upon), the counting sort algortim is regarded as a satble sort. (This, a cut- and -past-statment form Cormen, 2. edition, approx page 165, is of course not due to my own reserach, but i like the observation, due to its handiness.)
   @Major_change: 24.06.2011 by Ole Kristian Ekseth (oekseth)
 */
typedef class counting_sort {
 private:
  /**
     counting_procedure -- Sorts the array by uing their value as the index.
     @Changed: 24.06.2011 by oekseth
  */
  void counting_procedure(int *&array, const uint size, const uint k);

 public:
  //! Used to get the k-value if its not specified
  template<typename T> T get_max_value(T *&array, const uint size) {
    T max_value = array[0];
    for(uint i = 1; i < size; i++) {
      if(array[i] > max_value) max_value = array[i];
    }
    return max_value;
  }
  /**
     get_number_of_digits -- Returns the number of digits
     @Changed: 24.06.2011 by oekseth
  */
  uint get_number_of_digits(uint max_value) {
    uint cnt_digits = 0;
    while(max_value != 0) {
      max_value /= 10, cnt_digits++;
    }
    return cnt_digits;
  }

  // private:
  /**
     get_modulo
     @Changed: 24.06.2011 by oekseth
  */
  int get_modulo(int a, int d) {
    const int factor = a / d;
    if(factor > 0) { // Has some remaining 
      const int result = factor % 10;
      return result;
    } else return 0;
  }
 public:
  /**
     sort_array_at_decimal_pos -- Sorts the array by uing their value as the index.
     @param value_span -- the span of the values: e.g. for a standard 10-digit-system, this value is 10
     @param decimal_pos -- the position of the digit in the number string, with count starting at 0
     @Changed: 25.06.2011 by oekseth
  */
  void sort_array_at_decimal_pos(int *&array, const uint size, const uint value_span, const uint decimal_pos) {
    int *sorted = new int[size]; for(uint i = 0; i < size; i++) sorted[i] = 0;
    uint *count = new uint[size]; for(uint i = 0; i < size; i++) count[i] = 0;
    for(uint i = 0; i < size; i++) count[array[i]]++; // Counts the number of each indicis
    
    for(uint i = 1; i < size; i++) count[i] += count[i-1]; // the start position to insert on.
    
    for(int i = size-1; i > -1; i--) {
      const int value = array[i];
      const uint count_this_and_lower = --count[value]; // decrements, as one used
      sorted[count_this_and_lower] = array[i];
    }
    delete [] count;
    delete [] array;   array = sorted; // Updates the array.
  }

  /**
     sort_array -- Sorts the array by regarding tehir values as indexes in the sorting procedure
     @Changed: 22.06.2011 by oekseth
  */
  void sort_array(int *&array, const uint size, const uint k) {
    counting_procedure(array, size, k+1);
  }

  void delete_class() {}//{array.delete_class(); }
  counting_sort() {};//array = list_t(0);}

  /**
     assert_class -- Asserts the public functions of this class
     - Retriving the digits at the spesified position was for the author not a trivial task. Several tests are therefore written to (1) avoid suble mistakes and (2) dokument understanding of the operation.
     - The validation of the sorting procedure follows the standard recepie.
     @Changed: 25.06.2011 by oekseth
  */
  static bool assert_class();

} counting_sort_t;
#endif
