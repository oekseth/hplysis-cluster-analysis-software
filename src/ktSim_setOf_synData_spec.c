#include "ktSim_setOf_synData_spec.h"

/**
   @brief Construct a feature-vector for the givne row-spec.
   @remarks Motviation is to adjust the featues "f" (where $f \in data$), based on the following cases:
   -- init: $f_1  = (i+1)*self.local_conf.multFactor $
   -- init: $f_2  = (k+1)*self.local_conf.multFactor $
   --
   -- if(self.multiply == case1):   $ f_{1,m} = f_1 * local_conf.factor_mult; f_{2,m} = f_2$
   -- elif(self.multiply == case2): $ f_{2,m} = f_2 * local_conf.factor_mult; f_{1,m} = f_1$
   -- elif(self.multiply == case3): $ f_{1,m} = f_1 * local_conf.factor_mult; f_{2,m} = f_2 * local_conf.factor_mult; $
   -- 
   -- if(self.local_conf.mulFactor_m) : $ f_{1,m} *= m*self.local_conf.mulFactor_m; f_{2,m} *= m*self.local_conf.mulFactor_m; $
   --
   -- if(self->invert(f_m)) : invert = (invert == false); if(invert): $ f_{1,m} = 1/f_{1,m} $ %! ie, turn 180 degrees when a turn-point is set.
**/
void constructFeatureVectors__s_ktSim_setOf_synData_spec_t(const s_ktSim_setOf_synData_spec_t *self, t_float *row_1, t_float *row_2, const uint i, const uint k) {
  const uint ncols_local = self->local_conf.ncols_local;
  const t_float multFactor = self->local_conf.multFactor;
  const t_float factor_mult = self->local_conf.factor_mult;
  const t_float score_1 = (i+1)*multFactor; 	  const t_float score_2 = (k+1)*multFactor;	  
  printf("mulFactor_m=%f, ncols_local=%u, at %s:%d\n", self->local_conf.mulFactor_m, ncols_local, __FILE__, __LINE__);  
  //!
  //! Provide lgoci-supprot for data-splitting:
  bool isTo_inverse_1 = false; uint splitId_pos_1 = UINT_MAX;
  //!
  //! 
  for(uint m = 0; m < ncols_local; m++) {
    //!
    //! Provide lgocis to 'invert' a feature-vector:
    if(self->local_conf.indexPos_posToInvert_size > 0) { //! then we apply lgocis for 'ivnerintg' a feature-vecotr-score:
      const uint *arr_map = self->local_conf.indexPos_posToInvert;
      assert(arr_map);
      if(splitId_pos_1 == UINT_MAX) { //! then init:
	if(arr_map[0] == m) {/*go-to-next=*/splitId_pos_1 = 1; /*swap:*/if(isTo_inverse_1) {isTo_inverse_1 = false;} else {isTo_inverse_1 = true;}} //! ie, then invert.
	//! else: then feature-column is not marked as 'invert'
      } else if(splitId_pos_1 < self->local_conf.indexPos_posToInvert_size) {
	if(arr_map[splitId_pos_1] == m) {/*go-to-next=*/splitId_pos_1++; /*swap:*/if(isTo_inverse_1) {isTo_inverse_1 = false;} else {isTo_inverse_1 = true;}} //! ie, then invert.
	//! else: then feature-column is not marked as 'invert'
      }
    }
    //! --------------
    //!
    //!
    const char cases_factor_increase_typeOf_multInner = self->local_conf.cases_factor_increase_typeOf_multInner;
    if(cases_factor_increase_typeOf_multInner == 0) {
      row_1[m] = score_1*factor_mult;  row_2[m] = score_2;
    } else if(cases_factor_increase_typeOf_multInner == 1) {
      row_1[m] = score_1;  row_2[m] = score_2*factor_mult;
    } else if(cases_factor_increase_typeOf_multInner == 2) {
      row_1[m] = score_1*factor_mult;  row_2[m] = score_2*factor_mult;
    } else {
      assert(false); //! ie, as we then need adding support for ths.
    }
    //!
    //! Facialtate a 'change' between the different feautre-vecotrs. 
    const t_float mulFactor_m = self->local_conf.mulFactor_m;
    // assert(mulFactor_m != 0.0);
    if(mulFactor_m != 0) {
      const t_float prev_score = row_1[m];
      row_1[m] *= (m*mulFactor_m);
      row_2[m] *= (m*mulFactor_m);
      if((row_1[m] != 0) && isTo_inverse_1) {
	row_1[m] = 1/row_1[m]; //! ie, as twe then invetise the matrix, eg, for the 'case' to construct a sinus-vector.
      }
      printf("\trow1[%u] = %f = %f * %u * %f, isTo_inverse_1'=%s', at %s:%d\n", m, row_1[m],  prev_score, m, mulFactor_m, (isTo_inverse_1) ? "true" : "false", __FILE__, __LINE__);
    }
  }	        
}


//! Consutrct a datga-set capturing/desciribn the feautre-matrix-data-set we use/apply/evlaute in thsi evaluation-appraoch:
s_kt_matrix_t constructMAtrixOf_featureScores__s_ktSim_setOf_synData_spec_t(const s_ktSim_setOf_synData_spec_t *self) {
  //! Iterat through the diffneret funciton-cases:
  const uint cases1_cnt = self->local_conf.cases1_cnt;
  uint currentPos__col = 0;       const uint ncols_local = self->local_conf.ncols_local; 
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(cases1_cnt, ncols_local);
  // printf("mulFactor_m=%f, at %s:%d\n", self->local_conf.mulFactor_m, __FILE__, __LINE__);
  for(uint i = 0; i < cases1_cnt; i++) {
    //! 
    //! Note: to ensure a 'none-overlappign' wrt. our emasurement-evlauations (ie,  consistent increase in value-difference) we start the "k" iteraiton on "i+1": latter implies that the proerpty of "score_1 <= score_2" will always hold, ie, for which a high-numbered/indexed feature-positon/score  is associated to an increasing dot-product-value. Simliarty we for increasing 'i' 
    // TDOO[eval]: evlauate correnctess of [ªbov€] and [”elow] 'assumpiton' wrt. "i+1" ... and then update both oru docuemtatnion and our aritlce.
    uint k = i+1;
    //for(uint k = i+1; k < cases1_cnt; k++) 
    {
      const t_float empty_0 = 0;
      t_float *row_1 = allocate_1d_list_float(ncols_local, empty_0); t_float *row_2 = allocate_1d_list_float(ncols_local, empty_0);
      //! Construct a feature-vector for the givne row-spec.
      constructFeatureVectors__s_ktSim_setOf_synData_spec_t(self, row_1, row_2, i, k);
      {       //!add the row:
	assert(ncols_local == mat_result.ncols);
	assert(currentPos__col < mat_result.nrows);
	for(uint i = 0; i < ncols_local; i++) {
	  mat_result.matrix[currentPos__col][i] = row_1[i];
	}
      }
      //!
      //! De-allcoate the local rows:
      assert(row_1); assert(row_2);
      free_1d_list_float(&row_1); row_1 = NULL;
      free_1d_list_float(&row_2); row_2 = NULL;
      //! Icnrement column-pos:
      currentPos__col++;
    }
  }
  return mat_result;
}


