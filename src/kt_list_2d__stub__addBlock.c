
  for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
    assert(listOf_inlineResults[local_index1]);
    const uint row_id = globalStartPos_head + local_index1;
    assert(row_id < self->list_size);
    //! 
    s_kt_list_1d_kvPair_t *obj_row = &(self->list[row_id]);
    assert(obj_row);
    //!
    //! Enlarge the list fi neccessary:
    if( (obj_row->current_pos + chunkSize_index2) >= obj_row->list_size) {
      const uint index = obj_row->current_pos + chunkSize_index2;
      s_kt_list_1d_kvPair_t *self = obj_row; //! ie, for 'handle' [below] requirement.
#include "kt_list_1d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
      assert(index < obj_row->list_size);
    }

    //! Note: an alteritvge appraoch (to 'this funciotni') is to (a) first cosntruct a new object (for the input-row), (b) merge/unify the rows (through our new-added funciotn in our "kt_list_1d.c") before c'ontuing our pcidure here'. However the latter compelxity is pointless, ie, the 'sorted proeprty' is useless before teh complete row is evlauated (ie, as the last block may conian the best-scoring entites, hence it woudl be wrong to start fitlerilng before we know the scores of the best entities). 

    //!
    //! Push the elemtns to the list:
    uint current_pos = obj_row->current_pos;
    //    printf("block-range=[%u, %u], at %s:%d\n", chunkSize_index1, chunkSize_index2, __FILE__, __LINE__);
#define __insert() ({ \
    assert(obj_row); \
    assert(obj_row->list_size >= (current_pos + chunkSize_index2)); \
    assert(listOf_inlineResults[local_index1]);				\
      for(uint local_index2 = 0; local_index2 < chunkSize_index2; local_index2++) { \
      const t_float score = listOf_inlineResults[local_index1][local_index2]; \
      if(__toUse(score)) { \
	if(isOf_interest(score)) { 					\
	const uint tail_id = local_index2 + globalStartPos_tail;	\
	if(false) {printf("\t\tinsert[%u][%u]=%f, at %s:%d\n", row_id, tail_id, score, __FILE__, __LINE__);} \
	/*! Then push the element: */					\
	obj_row->list[current_pos] = MF__initVal__s_ktType_kvPair(tail_id, score); \
	/*! Increment: */ \
	current_pos++; }}}})
    //!
    //!
    if(isTo_ingore_zeroScores) {
      if( (config_ballSize_max != T_FLOAT_MAX) ) {
#define __toUse(score) (score !=  0) && (score < config_ballSize_max)
	//! Apply:
	__insert();
#undef __toUse
      } else {
#define __toUse(score) (score !=  0) 
	//! Apply:
	__insert();
#undef __toUse
      }
    } else { //! then we also need to evlauate/consider 'empty-scores':
      if( (config_ballSize_max != T_FLOAT_MAX) ) {
#define __toUse(score) (isOf_interest(score)) && (score < config_ballSize_max)
	//! Apply:
	__insert();
#undef __toUse
      } else {
#define __toUse(score) (isOf_interest(score)) 
	//! Apply:
	/* for(uint local_index2 = 0; local_index2 < chunkSize_index2; local_index2++) {  */
	/*   const t_float score = listOf_inlineResults[local_index1][local_index2];  */
	/*   if(__toUse(score)) {						 */
	/*     assert(isOf_interest(score));					 */
	/*     const uint tail_id = local_index2 + globalStartPos_tail;	 */
	/*     /\*! Then push the element: *\/					 */
	/*     obj_row->list[current_pos] = MF__initVal__s_ktType_kvPair(tail_id, score);  */
	/*     /\*! Increment: *\/  */
	/*     current_pos++; }} */
	__insert();
#undef __toUse
      }
    }  
    //!
    //! Update the object with the inserted-element-count
    const bool hasInserted_elemetns = (obj_row->current_pos != current_pos); //! ie, as the count has hcanged if elements ahve been inserted. 
    //!
    if(isLast_columnForBlock__local) { //! Apply soritng:
      //   fprintf(stderr, "(info)\t## Add psot-filter-step, at %s:%d\n", __FILE__, __LINE__);
      obj_row->current_pos = current_pos;
      //! Filter the results to include only the subset of identifed entries (oekseth, 06. otk. 2017)
      filterResult__sort__s_kt_list_1d_kvPair_t(obj_row, config_cntMin_afterEach, config_cntMax_afterEach);
    } else {
      //    printf("current_pos=%u, list_size=%u, at %s:%d\n", current_pos, obj_row->list_size, __FILE__, __LINE__);
      obj_row->current_pos = current_pos;
    }
    //    fprintf(stderr, "##\t\t\t current_pos[%u], given config_cntMin_afterEach=%u, at %s:%d\n", obj_row->current_pos, config_cntMin_afterEach, __FILE__, __LINE__);
    //#endif
    /* //!  */
    /* //! Call the child-strucutre addign the vertices to the set: */
    
    /* // FIXME: write a macro-funciton to avoid the need of [”elow] for-loop ... and then udpate our other/similar functions ... and test the perofrmance-effect of 'this imrpvoement'. */
    /* for(; local_index2 < chunkSize_index2; local_index2++) { */
    /*   scalar_sumVal += listOf_inlineResults[local_index1][local_index2]; */
    /* } */
  }
#undef __insert
