{ //! The function for perofmrance-testing:

  //! Note: we expect the "stringOf_measureText" to be set in the caller.

  /* const uint arrOf_cnt_buckets_size = 7; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40, */
  /* 												  80, 160 */
  /* 												  //, 320, 740, 1000 */
  /* }; */

#ifndef __macro_funcName_toCall
#error "!!\t The funciton-anem to call is not specirfied, ie, please investigate you call, wrt. an 'itnernal' fucntion-name"
#endif
/* #ifndef __macro_includeTransposedInCall */
/* #error "!!\t The __macro_includeTransposedInCall macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name" */
/* #endif */
/* #ifndef __macro_includeTransposedInCall__andUseTiling */
/* #error "!!\t The __macro_includeTransposedInCall__andUseTiling macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name" */
/* #endif */
#ifndef __use_Result_to_updateTableComparisonResults
#error "!!\t The __use_Result_to_updateTableComparisonResults macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name"
#endif


  printf("\n-----------\t start-block:: for tag=\"%s\", at %s:%d\n", stringOf_measureText, __FILE__, __LINE__);

  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
    { 

      assert( (size_of_array % VECTOR_FLOAT_ITER_SIZE) == 0);

      //! Allcoate an itniate memory
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float **result = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      for(uint i = 0; i < size_of_array; i++) {
	for(uint out = 0; out < size_of_array; out++) {
	  matrix[i][out] = 1/(out+2) + (out+1)/(i+1); //! ie, itnaite.
	}
      }
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      t_float *r_3 = allocate_1d_list_float(size_of_array, default_value_float);      
      t_float *r_4 = allocate_1d_list_float(size_of_array, default_value_float);      
      //! -------
      t_float *_rmul1 = r_1; t_float *row = r_1; t_float *rmul1 = r_1; t_float *mul1 = r_1; t_float *_mul1 = r_1; t_float *_row_input = r_1;  t_float *row_input_1 = r_1; t_float *_row_input_1 = r_1;
      t_float *_rmul2 = r_2; t_float *_row = r_1; t_float *row_input_2 = r_2; t_float *row_mean = r_2; t_float *_row_mean = r_2; t_float *_row_input_2 = r_1;
      t_float *row_std = r_3; t_float *_row_std = r_3; t_float *mapOf_means = r_3;
      t_float *_row_result = r_4;
      //! -------
      t_float scalar = 0.5; const t_float scalar_mean = 2.0; uint ncols_adjusted = size_of_array; uint nrows_adjusted = size_of_array; uint ncols = size_of_array; uint nrows = size_of_array; 
      t_float ncols_inverted = 1/ncols; t_float ncols_inv = 1/ncols;
/* #if(__macro_includeTransposedInCall == 1)  */
/*       t_float **matrix_transposed = allocate_2d_list_float(size_of_array, size_of_array, default_value_float); */
/*       kt_mathMacros_SSE__transpose(size_of_array, size_of_array, matrix, matrix_transposed); */
/* #endif */
      
      /* { // FIXME: remove [”elow] ... and update our "kt_mathMacros_SSE__computeGauss__ZScores__vector__inverted(..)" when the memory-erorr is resorveled/handlded. */
      /* 	float *__restrict__  row = _row; 					 */
      /* 	float *__restrict__ row_mean = _row_mean;				 */
      /* 	float *__restrict__ row_std = _row_std;				 */
      /* 	float *__restrict__ row_result = _row_result;				 */
      /* 	for(uint j = 0; j < ncols_adjusted; j++) { */
      /* 	//for(uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {  */
      /* 	  kt_mathMacros_SSE__matrix_subtractFrom_self_2InputVectors(matrix, nrows_adjusted, ncols_adjusted, _row_input_1, _row_input_2); */
      /* 	  //kt_mathMacros_SSE__computeGauss__ZScores__vector__inverted(_row, ncols_adjusted, row_mean, _row_std, _row_result); */
      /* 	} */
      /* 	//VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_DIV(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_LOADU(&row_mean[j])), VECTOR_FLOAT_LOADU(&row_std[j])));  */
      /* } */


      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:

      

#if(__macro__isTo_useInnerIteration == 1)
      for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	const uint index = i;
	__macro_funcName_toCall(); //matrix, size_adjusted, result);
      }
#else
      __macro_funcName_toCall(); //matrix, size_adjusted, result);
#endif

	  
      /*   //! Update: */
      /*   result += righttails; */
	  
      /*   ; //result += __get_artimetnic_results(i, i); */
      /* } */
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif
      
      { //! write out the sum opf valeus, ie, to avoid an 'optmized compaition' from 'removing' the call
	t_float sum = 0; for(uint i = 0; i < size_of_array; i++) { sum += result[0][i]; } 
	printf("info:sum\t sum=%.2f, at %s:%d\n", sum, __FILE__, __LINE__);
      }

      //! De-allcoate lcoally reserved memory:
      free_2d_list_float(&matrix, size_of_array);
      free_2d_list_float(&result, size_of_array);
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_1d_list_float(&r_3);
      free_1d_list_float(&r_4);
/* #if(__macro_includeTransposedInCall == 1)  */
/*       free_2d_list_float(&matrix_transposed); */
/* #endif */

    }
  }
}
 
