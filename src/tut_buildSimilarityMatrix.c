#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"

/**
   @brief examplifies how to build a file holding the simliarity-evlauation-metircs of all the simlarity-metrics supported by hpLysis  (oekseth, 06. jan. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. jan. 2017).
   @remarks Extends "tut_mine.c". Demonstrates features wrt.:
   -- input: applicaiton of muliple metrics: how to loop through different simliarity-metrics in order to evlauate large number of different agreemenet-evlauation-strategies.
   -- computation: (similar to "tut_mine.c").
   -- result: how to merge the complete set of resutl-sets into one file: among others examplfie the 'opening' of a speicifc reuslt-file, an 'open d fiel' which is sued as 'result-out-channel' to a alrge number of dfiferent corr-emtrics-results.
**/

int main() 
#endif
{
  //! Test the MINE-correlaiton-emtric for a sytnetic data-set:
  const bool dataIsBetweenOneAndZero = false; //! ie, our defualt assumption.
  const bool inputMatrix__isAnAdjecencyMatrix = true;
  const uint nrows = 4;     const uint ncols = 4; 
  const char *result_file = "test_tut_buildSimilarityMatrix.tsv";
  uint mapOf_scoreDistributions_case1[nrows][nrows] = {
    //! List the clusterId assicated to each vertex:
    {0, 0, 0}, //! Case1.1: ie, all in the same cluster, eg, where vertex[0] is in cluster='0'
    {0, 1, 2}, //! Case1.2: ie, all in different clusters.
    {0, 0, 1}, //! Case1.3: ie, a 'strongly skewed cluster': permtuation-1
    {0, 1, 1}, //! Case1.4: ie, a 'strongly skewed cluster': permtuation-2
  };  
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);  
  init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  assert(obj_matrixInput.nrows == nrows);
  assert(obj_matrixInput.ncols == ncols);
  //! Copy the [ªbove] data-set into the 'new data-set':
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      obj_matrixInput.matrix[row_id][col_id] = mapOf_scoreDistributions_case1[row_id][col_id];
    }
  }
  //! Open teh file :
  FILE *file_out = fopen(result_file, "wb");
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}
  //!
  //! Correlate:
  for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) {
    for(uint post_index = 0; post_index <= e_kt_categoryOf_correaltionPreStep_none; post_index++) {
      //! Set the corrleation-metric:
      s_kt_correlationMetric_t corrMetric_prior;   init__s_kt_correlationMetric_t(&corrMetric_prior, (e_kt_correlationFunction_t)metric_index, (e_kt_categoryOf_correaltionPreStep_t)post_index);
      if(!dataIsBetweenOneAndZero && describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(corrMetric_prior.metric_id)) {continue;} 
      if(!inputMatrix__isAnAdjecencyMatrix && isTo_use_directScore__e_kt_correlationFunction(corrMetric_prior.metric_id)) {continue;} 
      //! Ntoe: for dfiferent corr-emtrics see our "e_kt_correlationFunction.h"
      /* s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid); */


      //!
      //! COfnigure the api-object:
      s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
      hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
      hp_config.stringOfResultPrefix__exportCorr__prior = NULL; 
      hp_config.config.corrMetric_prior = corrMetric_prior; //! ie, update the metric.
      //if(true) { //! then ovverride the default correlation-metric:

      //} //! else we use the 'default metic' specified in our "hpLysis_api.c"
      // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //!
      //! Correlate and export:
      const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
      assert(is_ok);
      //! 
      //! Export results to "file_out":
      assert(file_out);
      // fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
      fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
      // printf("\t stream_out=%p, at %s:%d\n", file_out, __FILE__, __LINE__);
      const bool is_ok_e = export__hpLysis_api(&hp_config, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, NULL);
      assert(is_ok_e);


      //! De-allocates the "s_hpLysis_api_t" object.
      free__s_hpLysis_api_t(&hp_config);	    
    }
  }
  //!
  //! Close the file and the amtrix:
  assert(file_out); fclose(file_out); file_out = NULL;
  free__s_kt_matrix(&obj_matrixInput);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

