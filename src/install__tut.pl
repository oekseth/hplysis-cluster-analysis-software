
#my $config__resultDir = "";
my $config__resultDir = "tut/";
# my $config__ = "";

#!
#! Construct directorites (fi the diretories are not already 'cosntructed'):
if(length($config__resultDir)) {
    system("mkdir -p $config__resultDir");
}

sub __compile {
    my ($fileInp) = @_;
    my $src_dir = "../src/";
    my $src_libFile = "lib_x_hpLysis";
    my ($result_file) = ($fileInp =~/(.+)\.(c|cxx|cpp|h)$/i);
    if(!defined($result_file)) {
	printf(STDERR "!!\t Not able to correctly 'format' the input=\"%s\", ie, pelase ivnestgiate at %s:%d\n", $fileInp, __FILE__, __LINE__);
    } else {
	$result_file = $config__resultDir . $result_file;
	my $cmd = "g++ -I$src_dir  -g -O0  -mssse3   -L $src_dir  $fileInp  -l $src_libFile -o $result_file";
	$cmd .= " " . "-Wl,-rpath=. -fPIC"; #! where latter is used to avodi an error such as: "error while loading shared libraries: liblib_x_hpLysis.so: cannot open shared object file: No such file or directory".
	if($cmd =~/mysql/i) {
	    $cmd .= " `mysql_config --cflags --libs`"; #! ie, tehn 'append' a mySQL-link-cofnig-param
	}
	printf("compile: %s\t at %s:%d\n", $cmd, __FILE__, __LINE__);
	my $str = `$cmd`;
	if($str ne "") {
	    printf("!!\t Investigate:\n%s\n at %s:%d\n", $str, __FILE__, __LINE__);
	}
    }    
}



#!
#! Read through the local directory, and idneitfy the files to use:
my @arrOf_files = (); #"tut_sim_1_vec_Euclid.c");
{my $dir = '.';
 opendir(DIR, $dir) or die $!; 
 while (my $file = readdir(DIR)) {
     if($file =~  /tut_.*c$/){
	 if($file =~/(tut__aux__)|(config_tut_hypothesis_whyResultDiffers_1__config.c)/) {next;} #! ie, as we asusem latter is aucisaliry-fuiles.
	 #!
	 #! Add fiel to file-set:
	 push(@arrOf_files, $file);
     }     
 }
}

closedir(DIR);

#! 
#! For each: compile, and abort if the compilation fails.  
foreach my $file (@arrOf_files) {
    __compile($file);
}
