static void tut_perf_metric_slowVS_fast_rankBased__spearman() {
    //! We present a simplfied evlauaiton/comparison of Spearman and Kendall's Tau (oekseth, 06. jun. 2017):
    //! 
    //! The matrix-sizes to evaluate:
    const uint mapCases_size = 10;
    const uint b = 64; // 5*124;
    const uint mapCases[mapCases_size] = {
      b*1, b*2, b*3, b*4, b*5, 
      b*6, b*7, b*8, b*9, b*10
    };
#define __MiF__startMeasurement() ({s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols); \
      for(uint row_1 = 0; row_1 < nrows; row_1++) { \
	for(uint col_1 = 0; col_1 < ncols; col_1++) { \
	  mat_input.matrix[row_1][col_1] = 1/rand(); \
	} \
      } \
      start_time_measurement(); mat_input;})
#define __MiF__endMeasurement(str_local) ({ char str_tmp[1000];  \
	sprintf(str_tmp, "[%u, %u]:%s", nrows, ncols, str_local);	\
	const t_float time_result = end_time_measurement(/*msg=*/str_tmp, prev_time_inSeconds);	\
	fprintf(stdout, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);	\
	fprintf(stderr, "(%u, %f) ", nrows, time_result);		\
	/*! De-allcoate:*/						\
	free__s_kt_matrix(&mat_input);       })


    t_float prev_time_inSeconds = 0;
    //    if(false)
      { const char *str_measurement = "Spearman-hpLysis";
	__MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      t_float amx_Score = 0;
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	//!
	//! Apply logics:
	s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
	s_kt_matrix_base vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.
	//! Specify/define the simlairty-metric:
	s_kt_correlationMetric obj_metric = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O2_spearman);
															    //initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_coVariance, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
	// const char dist = 'k'; //! ie, Kendall
	// //! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = true;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 0;
	// char **matrix_mask = allocate_2d_list_char(nrows, ncols, empty_0);
	// t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_input);
	start_time_measurement();
	const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_1,  &vec_result, /*config=*/hp_config);
	assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	// for(uint row_1 = 0; row_1 < nrows; row_1++) {
	//   for(uint row_2 = 0; row_2 < nrows; row_2++) {
	//     metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	//   }
	// }
	__MiF__endMeasurement(str_measurement);
	t_float max_score = T_FLOAT_MIN_ABS;
	for(uint row_id = 0; row_id < vec_result.nrows; row_id++) {
	  for(uint col_id = 0; col_id < vec_result.ncols; col_id++) {
	    //!
	    //! Write out the result:
	    if(false) {
	      fprintf(stdout, "# Result: \t\t sim-score[%u][%u]=\"%f\", at [%s]:%s:%d\n", row_id, col_id, vec_result.matrix[row_id][col_id], __FUNCTION__, __FILE__, __LINE__);
	    }
	    const t_float score = vec_result.matrix[row_id][col_id];
	    if(isOf_interest(score)) {
	      max_score = macro_max(max_score, score);
	    }
	  }
	}
	//printf("max_score=%f, at %s:%d\n", max_score, __FILE__, __LINE__);
	amx_Score += max_score;
	//!
	//! De-allocate: 
	free__s_kt_matrix_base_t(&vec_result);
	// free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	// free_1d_list_float(&list_weight); list_weight = NULL;
      }
      printf("max_score=%f, at %s:%d\n", amx_Score, __FILE__, __LINE__);
    }
    //      if(false)
      { const char *str_measurement = "Spearman-hpLysis-seperate-row-calls";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	// const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = false;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 0;
	// char **matrix_mask = allocate_2d_list_char(nrows, ncols, empty_0);
	// t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_allAgainstAll_config_t self; setTo_empty__s_allAgainstAll_config(&self);
	self.forNonTransposed_useFastImplementaiton = true;
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    sum += kt_spearman(ncols, mat_input.matrix, mat_input.matrix, NULL, NULL, NULL, row_1, row_2, (uint)isTo_transposed, self); 
	    //metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	// free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	// free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
      //      if(false)
      { const char *str_measurement = "Kendall-hpLysis-func-slow-wrapper";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	// const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = false;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 0;
	// char **matrix_mask = allocate_2d_list_char(nrows, ncols, empty_0);
	// t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_allAgainstAll_config_t self; setTo_empty__s_allAgainstAll_config(&self);
	self.forNonTransposed_useFastImplementaiton = false;
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    sum += kt_spearman(ncols, mat_input.matrix, mat_input.matrix, NULL, NULL, NULL, row_1, row_2, /*transpose=*/(uint)isTo_transposed, self); 
	    //metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	// free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	// free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
      //            if(false)
      { const char *str_measurement = "Kendall-naive";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	//const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*func_metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = false;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 1; //! ie, mark all vertices as of interest.
	int **matrix_mask = allocate_2d_list_int(nrows, ncols, empty_0);
	t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    // printf("call-func, at %s:%d\n", __FILE__, __LINE__);
	    sum += c_spearman(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/(int)isTo_transposed);
	    //func_metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	free_2d_list_int(&matrix_mask, nrows); matrix_mask = NULL;
	//free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
      //      if(false)
      { const char *str_measurement = "Kendall-naive-transposed";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	//const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*func_metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = true;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 1; //! ie, mark all vertices as of interest.
	int **matrix_mask = allocate_2d_list_int(nrows, ncols, empty_0);
	t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    // printf("call-func, at %s:%d\n", __FILE__, __LINE__);
	    sum += c_spearman(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/(int)isTo_transposed);
	    //func_metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	free_2d_list_int(&matrix_mask, nrows); matrix_mask = NULL;
	//free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
}

static void tut_perf_metric_slowVS_fast_rankBased__kendall() {
    //! We present a simplfied evlauaiton/comparison of Spearman and Kendall's Tau (oekseth, 06. jun. 2017):
    //! 
    //! The matrix-sizes to evaluate:
    const uint mapCases_size = 10;
    const uint b = 64; // 5*124;
    const uint mapCases[mapCases_size] = {
      b*1, b*2, b*3, b*4, b*5, 
      b*6, b*7, b*8, b*9, b*10
    };
#define __MiF__startMeasurement() ({s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols); \
      for(uint row_1 = 0; row_1 < nrows; row_1++) { \
	for(uint col_1 = 0; col_1 < ncols; col_1++) { \
	  mat_input.matrix[row_1][col_1] = 1/rand(); \
	} \
      } \
      start_time_measurement(); mat_input;})
#define __MiF__endMeasurement(str_local) ({ char str_tmp[1000];  \
	sprintf(str_tmp, "[%u, %u]:%s", nrows, ncols, str_local);	\
	const t_float time_result = end_time_measurement(/*msg=*/str_tmp, prev_time_inSeconds);	\
	fprintf(stdout, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);	\
	fprintf(stderr, "(%u, %f) ", nrows, time_result);		\
	/*! De-allcoate:*/						\
	free__s_kt_matrix(&mat_input);       })


    t_float prev_time_inSeconds = 0;
    if(true)
      { const char *str_measurement = "Kendall-hpLysis";
	__MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      t_float amx_Score = 0;
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	//!
	//! Apply logics:
	s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
	s_kt_matrix_base vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.
	//! Specify/define the simlairty-metric:
	s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_coVariance, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
	// const char dist = 'k'; //! ie, Kendall
	// //! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = true;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 0;
	// char **matrix_mask = allocate_2d_list_char(nrows, ncols, empty_0);
	// t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_input);
	start_time_measurement();
	const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_1,  &vec_result, /*config=*/hp_config);
	assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	// for(uint row_1 = 0; row_1 < nrows; row_1++) {
	//   for(uint row_2 = 0; row_2 < nrows; row_2++) {
	//     metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	//   }
	// }
	__MiF__endMeasurement(str_measurement);
	t_float max_score = T_FLOAT_MIN_ABS;
	for(uint row_id = 0; row_id < vec_result.nrows; row_id++) {
	  for(uint col_id = 0; col_id < vec_result.ncols; col_id++) {
	    //!
	    //! Write out the result:
	    if(false) {
	      fprintf(stdout, "# Result: \t\t sim-score[%u][%u]=\"%f\", at [%s]:%s:%d\n", row_id, col_id, vec_result.matrix[row_id][col_id], __FUNCTION__, __FILE__, __LINE__);
	    }
	    const t_float score = vec_result.matrix[row_id][col_id];
	    if(isOf_interest(score)) {
	      max_score = macro_max(max_score, score);
	    }
	  }
	}
	//printf("max_score=%f, at %s:%d\n", max_score, __FILE__, __LINE__);
	amx_Score += max_score;
	//!
	//! De-allocate: 
	free__s_kt_matrix_base_t(&vec_result);
	// free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	// free_1d_list_float(&list_weight); list_weight = NULL;
      }
      printf("max_score=%f, at %s:%d\n", amx_Score, __FILE__, __LINE__);
    }
    if(true)
      { const char *str_measurement = "Kendall-hpLysis-seperate-row-calls";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	// const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = false;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 0;
	// char **matrix_mask = allocate_2d_list_char(nrows, ncols, empty_0);
	// t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_allAgainstAll_config_t self; setTo_empty__s_allAgainstAll_config(&self);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    sum += kt_kendall(ncols, mat_input.matrix, mat_input.matrix, NULL, NULL, NULL, row_1, row_2, /*transpose=*/isTo_transposed, e_kt_correlationFunction_groupOf_rank_kendall_coVariance, self); 
	    //metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	// free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	// free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
    //    if(false)
      { const char *str_measurement = "Kendall-hpLysis-func-call-direct";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	// const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = false;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 0;
	// char **matrix_mask = allocate_2d_list_char(nrows, ncols, empty_0);
	// t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_allAgainstAll_config_t self; setTo_empty__s_allAgainstAll_config(&self);
	self.forNonTransposed_useFastImplementaiton = false;
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    sum += kt_kendall(ncols, mat_input.matrix, mat_input.matrix, NULL, NULL, NULL, row_1, row_2, /*transpose=*/isTo_transposed, e_kt_correlationFunction_groupOf_rank_kendall_coVariance, self); 
	    //metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	// free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	// free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
      // if(false)
      { const char *str_measurement = "Kendall-naive";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	//const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*func_metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = false;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 1; //! ie, mark all vertices as of interest.
	int **matrix_mask = allocate_2d_list_int(nrows, ncols, empty_0);
	t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    // printf("call-func, at %s:%d\n", __FILE__, __LINE__);
	    sum += c_kendall(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/(int)isTo_transposed);
	    //func_metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	free_2d_list_int(&matrix_mask, nrows); matrix_mask = NULL;
	//free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
      { const char *str_measurement = "Kendall-naive-transposed";
	printf("str_measurement=\"%s\", at %s:%d\n", str_measurement, __FILE__, __LINE__);
      __MiF__startMeasurement__newCase();
	//! 
	//! Iterate throguh differetn matrix-cases: 
      for(uint case_id = 0; case_id < mapCases_size; case_id++) {
	/* Set the metric function as indicated by dist */
	//const char dist = 'k'; //! ie, Kendall
	//! Note: [”elow] is deifned in our "distance_2rows_slow.h"
	// t_float (*func_metric)
	//   (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) =
	//   setmetric_slow(dist);
	//! 
	//! Construct matrix: 
	uint nrows = mapCases[case_id];  uint ncols = nrows;
	const bool isTo_transposed = true;
	if(isTo_transposed) {
	  assert(nrows == ncols); //! ie, as we otehr need to add logics:
	}
	//! AllocatE:
	const char empty_0 = 1; //! ie, mark all vertices as of interest.
	int **matrix_mask = allocate_2d_list_int(nrows, ncols, empty_0);
	t_float *list_weight = allocate_1d_list_float(nrows, empty_0);
	s_kt_matrix_t mat_input = __MiF__startMeasurement();
	t_float sum = 0; 
	//! 
	//! Apply logics: 
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint row_2 = 0; row_2 < nrows; row_2++) {
	    // printf("call-func, at %s:%d\n", __FILE__, __LINE__);
	    sum += c_kendall(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/(int)isTo_transposed);
	    //func_metric(ncols, mat_input.matrix, mat_input.matrix, matrix_mask, matrix_mask, list_weight, row_1, row_2, /*transpose=*/isTo_transposed);
	  }
	}
	__MiF__endMeasurement(str_measurement);
	//!
	//! De-allocate: 
	free_2d_list_int(&matrix_mask, nrows); matrix_mask = NULL;
	//free_2d_list_char(&matrix_mask); matrix_mask = NULL;
	free_1d_list_float(&list_weight); list_weight = NULL;
      }
    }
}
