#include "measure_db.h"

#include "db_ds_directMapping.h"
#include "db_ds_bTree_keyValue.h"
#include "db_ds_bTree_rel.h"
#include "kt_matrix.h"
#include "db_inputData_matrix.h"

/* //! A strucutre to test the time-cost wrt. implict memory-linkage. */
/* typedef struct db_links { */
/*   uint key; t_float score; testStruct_links *link_parent; */
/* } db_links_t; */

//typedef struct db_node

/* //! Compare the exueicont-itme of a poitner-based list-traversal: */
/* static void __compare__list_pointerAccess() {  */
  
/* } */

//! The main test-function.
void measure_db__main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r
#define __M__calledInsideFunction
  //const bool isTo_useRandomAccess = true;
  uint current_search = 0;
#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} else {if(current_search++ > nrows) {current_search = 0;} val_ret = current_search;} val_ret;})
  //! -------------------------------------------------
  const uint cnt_synsEach = 5;
  const uint cnt_predicatesForTails = 20;
  const uint config__cntColsEach = 20;
  //! -------------------------------------------------

  
  //if(false)
  {
    const bool isTo_useKeyBasedSearchPattern = true; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
    printf("****** \n\n access::useOptimizedPattern, at %s:%d\n", __FILE__, __LINE__);
    { //! Use the 'default order' to improve access-time.
      const uint nrows_base = 1000*100; 
      //const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
      const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
    
      /* if(isTo_processAll || (0 == strncmp("list_pointerAccess", array[2], strlen("list_pointerAccess"))) ) { */
      /* 	__compare__list_pointerAccess(); */
      /* 	//! ------------------------------------  */
      /* 	cnt_Tests_evalauted++; */
      /* } else  */if(isTo_processAll || (0 == strncmp("synt__1", array[2], strlen("synt__1"))) ) {
	printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
	{const bool isTo_useRandomAccess = true;
#include "tut_db_bTree_simpleExample_1__keyValue.c"
	}
	{const bool isTo_useRandomAccess = false;
	  printf("\n\n# Starts non-randomized iteraiton, at %s:%d\n", __FILE__, __LINE__);
#include "tut_db_bTree_simpleExample_1__keyValue.c"
	}
	//! ------------------------------------ 
	cnt_Tests_evalauted++;
      } 
      if(isTo_processAll || (0 == strncmp("synt__2", array[2], strlen("synt__2"))) ) {
	printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
	{const bool isTo_useRandomAccess = true;
#include "tut_db_bTree_simpleExample_2__rel.c"
	}
	{const bool isTo_useRandomAccess = false;
	  printf("\n\n# Starts non-randomized iteraiton, at %s:%d\n", __FILE__, __LINE__);
#include "tut_db_bTree_simpleExample_2__rel.c"
	}
	//! ------------------------------------ 
	cnt_Tests_evalauted++;
      }  
      if(isTo_processAll || (0 == strncmp("synt__3", array[2], strlen("synt__3"))) ) {
	printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
	// ... examplify differnet use-case-patterns when using KnttingTools optmized data-access-pattern: for logics we use our "db_ds_directMapping.h". 
	{const bool isTo_useRandomAccess = true;
#include "tut_db_bTree_simpleExample_3__2dList.c"
	}
	//! ------------------------------------ 
	cnt_Tests_evalauted++;
      }  
    }
  } 
  { //! Use list-iteraiton, thereby 'clowing dow' the exueciotn-time:
    //! Note: we evaluate wrt. 'complete list-search': to 'dientify' the time-cost when the 'default mappinig' may Not be directly used.
    const bool isTo_useKeyBasedSearchPattern = false; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
    printf("****** \n\n access::evalauteAll\n\n at %s:%d\n", __FILE__, __LINE__);
    { //! Use the 'default order' to improve access-time.
      const uint nrows_base = 1000*10; //*100; 
      //const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
      const uint cnt_searches = 1000*100; //00; //*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
      //const uint cnt_searches = 1000*1000; //*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).    
      if(isTo_processAll || (0 == strncmp("synt__1", array[2], strlen("synt__1"))) ) {
	printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
	{const bool isTo_useRandomAccess = true;
#include "tut_db_bTree_simpleExample_1__keyValue.c"
	}
	{const bool isTo_useRandomAccess = false;
	  printf("\n\n# Starts non-randomized iteraiton, at %s:%d\n", __FILE__, __LINE__);
#include "tut_db_bTree_simpleExample_1__keyValue.c"
	}
	//! ------------------------------------ 
	cnt_Tests_evalauted++;
      } 
      if(isTo_processAll || (0 == strncmp("synt__2", array[2], strlen("synt__2"))) ) {
	printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
	{const bool isTo_useRandomAccess = true;
#include "tut_db_bTree_simpleExample_2__rel.c"
	}
	{const bool isTo_useRandomAccess = false;
	  printf("\n\n# Starts non-randomized iteraiton, at %s:%d\n", __FILE__, __LINE__);
#include "tut_db_bTree_simpleExample_2__rel.c"
	}
	//! ------------------------------------ 
	cnt_Tests_evalauted++;
      }  
      if(isTo_processAll || (0 == strncmp("synt__3", array[2], strlen("synt__3"))) ) {
	printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
	// ... examplify differnet use-case-patterns when using KnttingTools optmized data-access-pattern: for logics we use our "db_ds_directMapping.h". 
	//#include "tut_db_bTree_simpleExample_3__2dList.c"
	//! ------------------------------------ 
	cnt_Tests_evalauted++;
      }  
    }
  }
  //assert(false);
  //! ----------------- 
  if(isTo_processAll || (0 == strncmp("data__2a", array[2], strlen("data__2a"))) ) {

    //! @brief evlaute the exeuciton-tiem-difference wrt. B-tree for cominbaitons (where we insert in 'linear order' and 'access keys' in random order) .... where we wevlaute cases wrt. [w/key-size=4 Bytes, w/key-size=16 Bytes] x [cntChildren=[4, 8, 16, 32, 64]] ... and produce a web-apage using "highcharts"
    const uint nrows_base = 5000; 
    const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
    // const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
    const uint cnt_searches = 1000*1000*20; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
    const bool isTo_evaluate_2dListAccess = false; //! which may be set to 'false' iot. reuce the sigicnat time-perofmrance-pentaly assicated to 2d-list-access: for many cases we observe a performacne-differecen fo 400,000x (for allist-based access and cosndierably higher for a pointer-based access, ie, for cases where 'the B-tree-sorted-property' may nto be sued (eg, due to wild-cards in searches). 
    {
      const bool isTo_useKeyBasedSearchPattern = true; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
      printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
      {const bool isTo_useRandomAccess = true;
	const char *stringOf_resultDir = "data2__searchIsRandom_";
#include "tut_db_eval_dataAccess_2_bTree__randomSearch.c"
      }
      {const bool isTo_useRandomAccess = false;
	const char *stringOf_resultDir = "data2__searchIsIncremental_";
#include "tut_db_eval_dataAccess_2_bTree__randomSearch.c"
      }
    }
    //! ------------------------------------ 
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("data__2b", array[2], strlen("data__2b"))) ) {
    //! ------------------------------------ 
    const bool isTo_evaluate_2dListAccess = false; //! which may be set to 'false' iot. reuce the sigicnat time-perofmrance-pentaly assicated to 2d-list-access: for many cases we observe a performacne-differecen fo 400,000x (for allist-based access and cosndierably higher for a pointer-based access, ie, for cases where 'the B-tree-sorted-property' may nto be sued (eg, due to wild-cards in searches). 
    const uint nrows_base = 5000; 
    const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
    // const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
    const uint cnt_searches = 1000*1000*20; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
    uint local_query_index = 1;
    {
      const bool isTo_useKeyBasedSearchPattern = false; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
      printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
      {const bool isTo_useRandomAccess = true;
	const char *stringOf_resultDir = "data2__wildCards_searchIsRandom_"; //! ie, a file-prefix.
#include "tut_db_eval_dataAccess_2_bTree__randomSearch.c"
      }
      {const bool isTo_useRandomAccess = false;
	const char *stringOf_resultDir = "data2__wildCards_searchIsIncremental_"; //! ie, a file-prefix.
#include "tut_db_eval_dataAccess_2_bTree__randomSearch.c"
      }
    }
    //! ------------------------------------ 
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }  
  if(isTo_processAll || (0 == strncmp("useCase__1", array[2], strlen("useCase__1"))) ) {
    //!
    e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__bTree_forRelations__syn;
    uint cnt_base = 20;
    uint cnt_heads = 1000;
    const uint cnt_pred = 4;  uint nrows_each = 90; 
    const t_float fraction_syns = 0;
    uint local_query_index = 0;
    if(array_cnt == (3 + 2 + 3)) {
      uint cnt_relations = 0;
      //! 
      //! Then we parse the results:
      //!
      //! Eval: type of data-access:
      uint index = 3;
      if((0 == strncmp("-naive", array[index], strlen("-naive"))) ) {      
	objConfig = e_db_ds_directMapping__initConfig__bTree_forRelations__syn;
	//objConfig = e_db_ds_directMapping__initConfig__bTree_forRelations;
      } else if((0 == strncmp("-hpLysis", array[index], strlen("-hpLysis"))) ) {      
	objConfig = e_db_ds_directMapping__initConfig__synMappings__2dList__mapInverse;
	// objConfig = e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse;
	// objConfig = e_db_ds_directMapping__initConfig__synMappings__2dList;
	// objConfig = e_db_ds_directMapping__initConfig__synMappings;
      } else {
	fprintf(stderr, "!!\t No strings matched argument=\"%s\", at %s:%d\n", array[index], __FILE__, __LINE__);
	assert(false);} //! ie, then add support 'for this case'.
      //!
      //! Eval: type of data-access:
      index = 4; //! ie, the local index
      assert(array[index]);
      assert(strlen(array[index]));
      { //! Mvoe past hte query-prefix:
	const char *str_local = strstr(array[index], "c1:");
	assert(str_local); assert(strlen(str_local));
	str_local += strlen("c1:");
	const uint cnt_relations_kilo = atoi(str_local);
	assert(cnt_relations_kilo > 0);
	assert(cnt_relations_kilo < 1000);
	cnt_relations = 1000*cnt_relations_kilo;
      }
      //!
      //! Eval: per-cent of nrows_each
      index = 5; //! ie, the local index
      assert(array[index]);
      assert(strlen(array[index]));
      { //! Mvoe past hte query-prefix:
	const char *str_local = strstr(array[index], "c2:");
	assert(str_local); assert(strlen(str_local));
	str_local += strlen("c2:");
	t_float cntHeads_each__perCent = atof(str_local);
	assert(cntHeads_each__perCent < 100);
	assert(cntHeads_each__perCent > 0);
	cntHeads_each__perCent = cntHeads_each__perCent / 100; //! i,e adjsut to percent.
	cnt_heads = (uint)(cntHeads_each__perCent * (t_float)cnt_relations);
	assert(nrows_each > 0);
	nrows_each = (uint)((t_float)cnt_relations/(t_float)cnt_heads);
	if(nrows_each == 0) {
	  fprintf(stderr, "!!\t investigate issue where nrows_each=0: cnt_heads=%u, cnt_relations=%u, cntHeads_each__perCent=%f, at %s:%d\n", cnt_heads, cnt_relations, cntHeads_each__perCent, __FILE__, __LINE__);
	}
	assert(nrows_each > 0);
      }
      //!
      //! Eval: The query-type:
      index = 6; //! ie, the local index
      assert(array[index]);
      assert(strlen(array[index]));
      { //! Mvoe past hte query-prefix:
	const char *str_local = strstr(array[index], "c3:");
	assert(str_local); assert(strlen(str_local));
	str_local += strlen("c3:");
	if((0 == strncmp("q1", str_local, strlen("q1"))) ) {      
	  local_query_index = 1;
	} else if((0 == strncmp("q2", str_local, strlen("q2"))) ) {      
	  local_query_index = 2;
	} else {
	  fprintf(stderr, "!!\t Add support for query=\"%s\", at %s:%d\n", str_local, __FILE__, __LINE__);
	  assert(false);
	}
      }

      //!
      //! Eval: The number of queries to evaluate:
      index = 7; //! ie, the local index
      assert(array[index]);
      assert(strlen(array[index]));
      { //! Mvoe past hte query-prefix:
	const char *str_local = strstr(array[index], "cols:");
	assert(str_local); assert(strlen(str_local));
	str_local += strlen("cols:");
	cnt_base = atoi(str_local);
	assert(cnt_base > 0);
      }
    } else {
      assert(false); // TODO: consider removing this.
    }

    //    assert(false); // FIXME: integrate [above] in [”elow].

    const bool config__init__onlyUseNorm = false; //! which if set to false impleis that we 'compare' the xeuciton-time for 'non-data-nroamlizaiton-pre-step' VS 'data-normalization-pre-step'
    printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
/* #if(1 == 2) */
/*     const uint cnt_heads = 10000;  */
/*     const t_float fraction_syns = 0.002; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases.  */
/* #else */
/*     const uint cnt_heads = 10000; //! which results in approx. a 'odubling' of the exueciotn-time when compared to "cnt_heads = 5000" (when evaluated/compared for "e_db_searchNode_rel__sampleUseCases_wildCard_isTail"). */
/*     //const uint cnt_heads = 5000;  */
/*     //const t_float fraction_syns = 0.002; */

/*     //const t_float fraction_syns = 0.02; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases.  */
/* #endif */

    //const uint cnt_heads = 10000; const uint cnt_pred = 20; const uint nrows_each = 100; const t_float fraction_syns = 0.2; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases. 
  //! COfniguring the search for 'relationships':
    //
/* #if(1 == 1) */
/*     e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__bTree_forRelations__syn; */
/*     //const e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__bTree_forRelations; */
/* #else */
/*     e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__synMappings__2dList__mapInverse; */
/*     //const e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse; */
/*     //const e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__synMappings__2dList; */
/*     // const e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__synMappings; */
/* #endif */
    //const uint threshold__scalar__cntVerticesToInvestigate__max = UINT_MAX; //! which if set is use das a 'stop-trheshodl' wrt. vertex-inveistgaitons.
    const uint threshold__scalar__cntVerticesToInvestigate__max = 1500; //! which if set is use das a 'stop-trheshodl' wrt. vertex-inveistgaitons.
    //const uint threshold__scalar__cntVerticesToInvestigate__max = 500; //! which if set is use das a 'stop-trheshodl' wrt. vertex-inveistgaitons.
    //    const uint threshold__scalar__cntVerticesToInvestigate__max = 2*1000; //! which if set is use das a 'stop-trheshodl' wrt. vertex-inveistgaitons.

    const bool config__isTo_preCompute_preSearchConditions = true;
    bool configLogics__isToApply__synonymMappings__inFunction = false; //! which sie used to vinestiate time-effects/improtance of the synonym-re-mapping-step.
    //! Diemsniosn fot eh searhc-spac:
    uint inSearchCntToEvaluateMax__for__head      = cnt_base;
    uint inSearchCntToEvaluateMax__for__predicate = 0;
    //    const uint inSearchCntToEvaluateMax__for__predicate = 20;
    uint inSearchCntToEvaluateMax__for__tail      = 0;
    e_db_searchNode_rel__sampleUseCases_t typeOf_useCase = e_db_searchNode_rel__sampleUseCases_wildCard_isTail;
    //!
    //!
    if(local_query_index == 1) {
      inSearchCntToEvaluateMax__for__head      = cnt_base;
      inSearchCntToEvaluateMax__for__predicate = 0;
    //    const uint inSearchCntToEvaluateMax__for__predicate = 20;
      inSearchCntToEvaluateMax__for__tail      = 0;
      typeOf_useCase = e_db_searchNode_rel__sampleUseCases_wildCard_isTail;
    } else if(local_query_index == 2) {
      typeOf_useCase = e_db_searchNode_rel__sampleUseCases_wildCard_isHead;
      inSearchCntToEvaluateMax__for__head      = 0;
      inSearchCntToEvaluateMax__for__predicate = cnt_base;
      inSearchCntToEvaluateMax__for__tail      = cnt_base;
    } else {
      fprintf(stderr, "!!\t Add support for local_query_index=\"%u\", at %s:%d\n", local_query_index, __FILE__, __LINE__);
      assert(false);
    }
    //#endif
    const uint inSearchCntToEvaluateMax__for__preSelection__predicate  = 20;
    const uint inSearchCntToEvaluateMax__for__preSelection__tail       = 20;
    const uint inSearchCntToEvaluateMax__stepMult = 10; //! ie, mulitply by "inSearchCntToEvaluateMax__stepMult" for the [ªbove] 'size-properties'.
    const uint inSearchCntToEvaluateMax__each__cnt = 1; //! ie, '10' differten calls using our "inSearchCntToEvaluateMax__stepMult" to 'enlarge' the data-size 'for each call'.
    // const uint inSearchCntToEvaluateMax__each__cnt = 10; //! ie, '10' differten calls using our "inSearchCntToEvaluateMax__stepMult" to 'enlarge' the data-size 'for each call'.
    const bool isTo_useRandomAccess = true;
    if(false) {
      printf("performs a dummy run, at %s:%d\n", __FILE__, __LINE__);
      fprintf(stdout, "endTime:%f\n", 0.0);
    } else {
#include "tut_db_useCase__1_normalizeData.c"
    }
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("useCase__2", array[2], strlen("useCase__2"))) ) {
    printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
    const uint cnt_heads = 1000; const uint cnt_pred = 4; const uint nrows_each = 90; const t_float fraction_syns = 0.2; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases. 
    // TODO: consider using [”elow] prefix in our "tut_db_useCase__2_normalizeData.c"
    const char *stringOf_resultPRefix = "db_useCase__2";
    if(array_cnt > 3) {
      stringOf_resultPRefix = array[3];
    }
    //const uint cnt_heads = 10000; const uint cnt_pred = 20; const uint nrows_each = 100; const t_float fraction_syns = 0.2; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases. 
#include "tut_db_useCase__2_normalizeData.c"
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }  
  if(isTo_processAll || (0 == strncmp("data__1", array[2], strlen("data__1"))) ) {
    printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
    //! .... evlaute the exeuciton-tiem-difference wrt. (a) 'list-access', (b) B-tree w/key-size=4 Bytes, (c) B-tree w/key-size=16 Bytes, (d) 2d-list-traversla ... 
    //! Note
    // #include "tut_db_eval_dataAccess_1_bTree.c"
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }  
  if(isTo_processAll || (0 == strncmp("data__3", array[2], strlen("data__3"))) ) {
    printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
    //! @brief time-overhead of simplicitc queryies using a neaive implementaiton: compare the exeuction-time between accessing a "relation-id" by (a) accessing a b-tree to 'find' the index-value (usinig/through our "s_db_ds_bTree_keyInt_t") (and then 'fetch' the relation using our "db_ds_directMapping.h") VS (b) 'a direct list-access' (in our "db_ds_directMapping.h").
    assert(false); // FIXME[conceptaul::tut::"tut_db_eval_dataAccess_1_bTree__relationAccess.c"] ... describe how to itnegrate/use latter wrt. our relation-searches .... 
    // #include "tut_db_eval_dataAccess_3_bTree__relationAccess.c"
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }  
  if(isTo_processAll || (0 == strncmp("data__", array[2], strlen("data__"))) ) {
    printf("\n\n------- New case, at %s:%d\n", __FILE__, __LINE__);
    // #include "tut_db_eval_dataAccess_1_bTree.c"
    //! ------------------------------------ 
    cnt_Tests_evalauted++;
  }  
  //! ------------------------------------------------------------------------------------------------------------------
#undef __M__calledInsideFunction  
  //!
  //! De-allocate:
#undef __Mi__getRandVal

  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}


