
# FIXME: wrt. can-open-communicaiotn ... possilbe to use the "notify( can_id , data , timestamp )" call ... https://python-can.readthedocs.io/en/stable/listeners.html#can.Listener <-- test if this appraoch gives us udpate of received data.


network.connect(channel='can0', bustype='socketcan')
    # network.connect(bustype='kvaser', channel=0, bitrate=250000)
    # network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=250000)
    # network.connect(bustype='ixxat', channel=0, bitrate=250000)
    # network.connect(bustype='nican', channel='CAN0', bitrate=250000)

#! Add nodes to network:
# FIXME: what is an 'eds'?
    node = network.add_node(6, '/path/to/object_dictionary.eds')
    local_node = canopen.LocalNode(1, '/path/to/master_dictionary.eds')
    network.add_node(local_node)
    for node_id  in network:
       print(network[node_id])
    #!
    #! Print out details of the added 'eds' node:
    for obj in  node.object_dictionary.values():
       print('0x %X : %s' % (obj.index, obj.name))
    if isinstance(obj, canopen.objectdictionary.Record):
      for subobj    in    obj.values():
        print('%d : %s' % (subobj.subindex, subobj.name))

#!    You can access the objects using either index/subindex or names:
device_name_obj = node.object_dictionary['ManufacturerDeviceName']
vendor_id_obj = node.object_dictionary[0x1018][1]
    
# This will attempt to read an SDO from nodes 1 - 127
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.05)
for node_id  in network.scanner.nodes:
    print("Found node %d!" % node_id)


#!
#! Start transfer:
canopen Documentation, Release 0.8.1.dev1
# Send NMT start to all nodes
network.send_message(0x0, [0x1, 0])
node.nmt.wait_for_heartbeat()
assert
node.nmt.state == 'OPERATIONAL'

#!
#! Aleratnviely, for a gvien ndoe:
node.nmt.state = 'OPERATIONAL'
# Same as sending NMT start
node.nmt.send_command(0x1)

#!
#! Alterantively, for all nodes:
#! You can also change state of all nodes simulaneously as a broadcast message:    
network.nmt.state = 'OPERATIONAL'
