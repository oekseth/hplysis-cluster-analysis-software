#include "correlation_inCategory_rank_kendall.h" 

//! Comptue Kendalls-tau for each cell in the input-matrices.
static void __ktCorr_compute_allAgainstAll_distanceMetric_kendall_slow(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, t_float weight[], t_float **resultMatrix, bool transpose, const e_kt_correlationFunction_t metric_id, s_allAgainstAll_config_t *config_allAgainstAll) {
  //! FIXME: in [”elow]: test the effect of 'inlining' both the "transpose" caluse and the fucntion-code itself.
  s_allAgainstAll_config_t config; setTo_empty__s_allAgainstAll_config(&config); //! ie, 'state' taht we are to ause a slow verison (oekseth, 06. sept. 2016).
  if(transpose == 0) {
    if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = nrows;} //! then moth matrices have the same size.
    //    if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = nrows;} //! then moth matrices have the same size.
    //! Star tthe tieration:

    for(uint index1 = 0; index1 < nrows; index1++) {
      assert(data1[index1]);
      for(uint index2 = 0; index2 < config_allAgainstAll->iterationIndex_2; index2++) {
	assert(data2[index2]);
	const t_float distance = kt_kendall(nrows, data1, data2, mask1, mask2, weight, index1, index2, transpose, metric_id, config);
	if(resultMatrix) {  resultMatrix[index1][index2] = distance;}
	else {ktCorr__postProcess_afterCorrelation_for_value(index1, index2, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1 == data2));}
      }
    }
  } else { //! then we compare the columns:
    if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = ncols;} //! then moth matrices have the same size.
    //    if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = ncols;} //! then moth matrices have the same size.
    //! Iterate:    
    assert(config_allAgainstAll->iterationIndex_2 <= ncols);
    for(uint index1 = 0; index1 < ncols; index1++) {
      for(uint index2 = 0; index2 < config_allAgainstAll->iterationIndex_2; index2++) {
	const t_float distance  = kt_kendall(nrows, data1, data2, mask1, mask2, weight, index1, index2, transpose, metric_id, config);
	if(resultMatrix) {
	  assert(resultMatrix[index1]);
	  resultMatrix[index1][index2] = distance;}
	else {ktCorr__postProcess_afterCorrelation_for_value(index1, index2, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1 == data2));}
      }
    }
  }
}

//! Comptue Kendalls-tau for each cell in the input-matrices.
void ktCorr_compute_allAgainstAll_distanceMetric_kendall(const uint nrows, const uint ncols, t_float **data1__, t_float **data2__,  char** mask1__, char** mask2__, t_float weight[], t_float **resultMatrix, bool transpose, const e_kt_correlationFunction_t metric_id, s_allAgainstAll_config_t *config_allAgainstAll) { 
  assert(config_allAgainstAll);
  
// FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.

  //! Test if we may use our otopmized strategy for copmuting "spearman":
  bool mayUse_optimized_computaiton = true;
  if( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none) ) {
    mayUse_optimized_computaiton = false;
  } // else { //! then we 'investigate' proeprties assicated ot the matrix:
  //   mayUse_optimized_computaiton = ktMask__vectorsAre_consistentlyInteresting(nrows, ncols, data1_input, data2_input, mask1, mask2, transpose);
  // }

  //printf("at [%s]:%s:%d\n", __FUNCTION__);

  if( !mayUse_optimized_computaiton || (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none) ) {
    assert(config_allAgainstAll->sparseDataResult == NULL); //! ie, to simplify our impmetanitonr-equreimetns (oekseth, 60. aug. 2017).
    __ktCorr_compute_allAgainstAll_distanceMetric_kendall_slow(nrows, ncols, data1__, data2__, mask1__, mask2__, weight, resultMatrix, transpose, metric_id, config_allAgainstAll);
  } else {

    //! Allocate the conaitners to be used for the sorted lists:
    const uint size_x = (transpose == 0) ? nrows : ncols;   const uint size_y = (transpose == 0) ? ncols : nrows;
    //t_float **matrix_tmp_1 = allocate_2d_list_float(size_x, size_y, /*default-value=*/0);
    const t_float default_value_float = 0;
    t_float *matrix_tmp_1_vector = allocate_1d_list_float(size_y, default_value_float);

    t_float **data1 = data1__; t_float **data2 = data2__;  char** mask1 = mask1__; char** mask2 = mask2__;

    if(mask1 || mask2) {      
#ifndef NDEBUG
      fprintf(stderr, "!!(configuration-error)\t Explict masks are used, though we instead expected implict masks (to be used), implict masks which shoudl be specified through the T_FLOAT_MAX attribte: in order to construct implict masks please use functions defined in oru \"mask_api.h\", eg, \"applyImplicitMask__constructNewMatrix__mask_api(..)\" or \"applyImplicitMask__updateData__mask_api(..)\". For your information, the latter 'rrequirement' is included in order to simplify the application of mask-attributes when inferring Kendall's Tau (ie, the correlation-measure which we are now to 'construct'). In order to hadnle the case, we will now constrct two new matrices, with the implciation that the memory-cosnutmpion (on your current system) may double, ie, which explains why we give you an 'heads up': for questions. please contact the devleoepr at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif

      s_mask_api_config_t config_mask; setTo_empty__s_mask_api_config(&config_mask);
      //! Get uipdated version of the data-sets:
      if(mask1) { data1 = applyImplicitMask__constructNewMatrix__mask_api(data1, mask1, nrows, ncols, config_mask); }
      if(mask2) { 
	if(data1__ != data2__) { data2 = applyImplicitMask__constructNewMatrix__mask_api(data2, mask2, nrows, ncols, config_mask); }
	else {data2 = data1;}
      }

      //! Reset masks:
      mask1 = NULL;       mask2 = NULL;
    } 

    // FIXME: update our becnmark-tests .... add and test two differnet for-loops ... where we in the first 'sort for the inner column' ... before calling kendall ... and test the effect/improtance of computing "index1" only for values "index2 <= index1"
    // FIXME: call graphAlgorithms_distance::kendall(..)"

    //if( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmt_functionInlined) ) {
  // FIXME: use a 'specific' enum ... test and write Kings algorithm ... 
    //! Ntoe: test the effect of 'inlining' both the "transpose" caluse and the fucntion-code itself.
    assert(data1); assert(data2);
    const bool needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncols, data1, data2, mask1, mask2, transpose, config_allAgainstAll->iterationIndex_2);
    if(config_allAgainstAll->sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017). */
      assert(transpose == 0); //! ie, to simplify oru lgocis: toehrwise we need to udpate [”elow] to add a 'handler' for the transpsoed part.
    }
    if(transpose == 0) {
      //! Then we allocate a temporary list, ie, to 'avopid' the empty values:
      t_float *arr1_tmp = allocate_1d_list_float(ncols, /*default-value=*/0);
      t_float *arr2_tmp = allocate_1d_list_float(ncols, /*default-value=*/0);
      t_float *list_column = allocate_1d_list_float(ncols, /*default-value=*/0);
      t_float *vector1_sorted = allocate_1d_list_float(ncols, /*default-value=*/0);
      uint *list_column_index = allocate_1d_list_uint(ncols, /*default-value=*/0);

      //if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = ncols;} //! then moth matrices have the same size.
      if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = nrows;} //! then moth matrices have the same size.

      t_float *tmp_result = NULL; uint tmp_result_size = 0;
      if(config_allAgainstAll->sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017).
	const t_float empty_0 = 0;
	tmp_result_size = (data1 == data2) ? nrows : config_allAgainstAll->iterationIndex_2;
	tmp_result = allocate_1d_list_float(tmp_result_size, empty_0);
      }

      for(uint index1 = 0; index1 < nrows; index1++) {
	//! Step(1): sort: --------------------------------------------------------------------------------------
	//assert(matrix_tmp_1[index1]);
	ktCorrelation_compute_rank_forVector(index1, /*size=*/ncols, /*data=*/data1, /*mask=*/mask1, /*temp-array=*/list_column, /*the back-infex-reference=*/list_column_index, /*result-array=*/matrix_tmp_1_vector, e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, vector1_sorted);
	uint cnt_elements_inner = (data1 == data2) ? index1+1 : config_allAgainstAll->iterationIndex_2;
	if(config_allAgainstAll->sparseDataResult != NULL) { 
	  //! Then we need to epxlitly comptue for eahc, ie, to reduc ehte memroy consumpion (oekseth, 60. okt. 2017).
	  cnt_elements_inner = config_allAgainstAll->iterationIndex_2; assert(cnt_elements_inner != UINT_MAX); }
	for(uint index2 = 0; index2 < cnt_elements_inner; index2++) {
	  //! Step(2): matrix-computation: --------------------------------------------------------------------------------------	  
	  const t_float distance = kendall_improvment_kingsAlgorithm(ncols, vector1_sorted, data2[index2], (!mask2) ? NULL : mask2[index2], arr1_tmp, arr2_tmp,  list_column_index, metric_id, needTo_useMask_evaluation);
	  //const t_float distance = kendall_improvment_kingsAlgorithm(ncols, /*vector1_sorted=*/list_column, data2[index2], (!mask2) ? NULL : mask2[index2], arr1_tmp, arr2_tmp,  list_column_index, /*vector1_sorted=*/true, true, needTo_useMask_evaluation);
	  if(resultMatrix) {  resultMatrix[index1][index2] = distance;}
	  else if(config_allAgainstAll->sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017).
	    assert(index2 < tmp_result_size);
	    tmp_result[index2] = distance;
	  } else {ktCorr__postProcess_afterCorrelation_for_value(index1, index2, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1 == data2));}
	}
	if(config_allAgainstAll->sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017).
	  //fprintf(stderr, "(info)\t Apply fitler, where cnt_elements_inner=%u, nrows=%u, config_cntMax_afterEach=%u, at %s:%d\n", cnt_elements_inner, nrows, config_allAgainstAll->sparseDataResult->config_cntMax_afterEach, __FILE__, __LINE__);
	  //! Then merge the result: 
	  apply__s_kt_list_2d_kvPair_filterConfig_t((config_allAgainstAll->sparseDataResult), &tmp_result, 
						    /*globalStartPos_head=*/index1, /*globalStartPos_tail=*/0, 
						    /*chunkSize_index1=*/1, /*chunkSize_index2=*/cnt_elements_inner, 
						    /*isLast_columnForBlock=*/true, 
						    /*isTo_updateSyemmtricRelations=*/false //! where latter option is based on [ªbvoe], ie, to reduce emmroy consumpiton
						    );
						    ///*isTo_updateSyemmtricRelations=*/(data1 == data2));
	}
      }
      //! De-allcoate the temproary list:
      free_1d_list_float(&vector1_sorted);
      free_1d_list_float(&list_column);
      free_1d_list_uint(&list_column_index);
      free_1d_list_float(&arr1_tmp);
      free_1d_list_float(&arr2_tmp);
      if(config_allAgainstAll->sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017).
	assert(tmp_result != NULL);
	free_1d_list_float(&tmp_result); tmp_result = NULL;
      }
    } else { //! then we compare the columns:
      assert(config_allAgainstAll->sparseDataResult == NULL); //! ie, to simplify our impmetanitonr-equreimetns (oekseth, 60. aug. 2017).
    //! Then we allocate a temporary list, ie, to 'avopid' the empty values:
      t_float *arr1_tmp = allocate_1d_list_float(nrows, /*default-value=*/0);
      t_float *arr2_tmp = allocate_1d_list_float(nrows, /*default-value=*/0);
      t_float *list_column = allocate_1d_list_float(nrows, /*default-value=*/0);
      uint *list_column_index = allocate_1d_list_uint(nrows, /*default-value=*/0);
      t_float *vector1_sorted = allocate_1d_list_float(nrows, /*default-value=*/0);
      if(config_allAgainstAll->iterationIndex_2 == UINT_MAX) {config_allAgainstAll->iterationIndex_2 = ncols;} //! then moth matrices have the same size.

      for(uint index1 = 0; index1 < ncols; index1++) {
	//! Step(1): sort: --------------------------------------------------------------------------------------
	//assert(matrix_tmp_1[index1]);
	ktCorrelation_compute_rank_forVector_firstTranspose(index1, /*size=*/ncols, /*data=*/data1, /*mask=*/mask1, /*temp-array=*/list_column, list_column_index, /*result-array=*/matrix_tmp_1_vector, e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, vector1_sorted);
	uint cnt_elements_inner = (data1 == data2) ? index1+1 : config_allAgainstAll->iterationIndex_2;
	if(config_allAgainstAll->sparseDataResult != NULL) { 
	  //! Then we need to epxlitly comptue for eahc, ie, to reduc ehte memroy consumpion (oekseth, 60. okt. 2017).
	  cnt_elements_inner = config_allAgainstAll->iterationIndex_2; assert(cnt_elements_inner != UINT_MAX); }

	for(uint index2 = 0; index2 < cnt_elements_inner; index2++) {
	  //! Step(2): matrix-computation: --------------------------------------------------------------------------------------
	  if(mask1) {assert(false);} // FIXME: add support for this case ... eg, by using an 'implcit mask-strategy' for "kendall"
	  const t_float distance =  kendall_improvment_kingsAlgorithm(ncols, /*vector1_sorted=*/vector1_sorted, data2[index2], (!mask2) ? NULL : mask2[index2], arr1_tmp, arr2_tmp,  list_column_index, metric_id, needTo_useMask_evaluation);
	  //const t_float distance =  kendall_improvment_kingsAlgorithm(ncols, /*vector1_sorted=*/list_column, data2[index2], (!mask2) ? NULL : mask2[index2], arr1_tmp, arr2_tmp,  list_column_index, /*vector1_sorted=*/true, true, needTo_useMask_evaluation);
	  if(resultMatrix) {  resultMatrix[index1][index2] = distance;}
	  else {ktCorr__postProcess_afterCorrelation_for_value(index1, index2, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1 == data2));}
	}
      }
      /* if(config_allAgainstAll->sparseDataResult != NULL) { //! then we add the string to a tempraory strucutre, ie, before merging (oekseth, 60. aug. 2017). */
      /* 	//! Then merge the result:  */
      /* 	apply__s_kt_list_2d_kvPair_filterConfig_t((config_allAgainstAll->sparseDataResult), &tmp_result,  */
      /* 						  /\*globalStartPos_head=*\/index1, /\*globalStartPos_tail=*\/0,  */
      /* 						  /\*chunkSize_index1=*\/1, /\*chunkSize_index2=*\/cnt_elements_inner,  */
      /* 						  /\*isLast_columnForBlock=*\/true,  */
      /* 						  /\*isTo_updateSyemmtricRelations=*\/(data1 == data2)); */
      /* } */
      //! De-allcoate the temproary list:
      free_1d_list_float(&vector1_sorted);
      free_1d_list_float(&list_column);
      free_1d_list_uint(&list_column_index);
      free_1d_list_float(&arr1_tmp);
      free_1d_list_float(&arr2_tmp);

    }

    //! De-allcoate the locally reserve3d memory:
    free_1d_list_float(&matrix_tmp_1_vector);   
    //free_2d_list_float(&matrix_tmp_1);   

    if(data1 != data1__) {free_2d_list_float(&data1, size_x);} //! ie, de-allcoate the locally allcoated data-object:
    if(data1__ != data2__) {
      if(data2 != data2__) {free_2d_list_float(&data2, size_x);} //! ie, de-allcoate the locally allcoated data-object:
    }
  }
  if(resultMatrix) {
    if(data1__ == data2__) { //! then we copy wrt. the symmetric values:
      //! Complete:
      const uint size_ncols_nrows = (transpose == 0) ? /*size=*/nrows : /*size=*/ncols;    
      const uint size_ncols_nrows_2 = (config_allAgainstAll && config_allAgainstAll->iterationIndex_2 != UINT_MAX) ? config_allAgainstAll->iterationIndex_2 : size_ncols_nrows;
      assert(resultMatrix);
      update_cells_above_firstFoorLoop( size_ncols_nrows, size_ncols_nrows_2, resultMatrix, transpose, /*isTo_use_speedTest_updateOrder_in_out=*/true, /*isTo_use_speedTest_inrisnistics=*/false);
    }
  } //! else we assume [above] operation has idnetified the min-values (oekseth, 06. des. 2016).
}

