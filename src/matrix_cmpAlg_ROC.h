#ifndef matrix_cmpAlg_ROC_h
#define matrix_cmpAlg_ROC_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file matrix_cmpAlg_ROC
   @brief provide lgoics for comptuation of ROC
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"



/**
   @brief comptue the ROC
   @param <arrOf_truth> is the truth-values to compare with.
   @param <arrOf_data> is the data to compare the truth-values with: if arrOf_masks is not set, then we assume that T_FLOAT_MAX or T_FLOAT_MIN_ABS is used to 'mark' empty values.
   @param <arrOf_minThresholds> if set is used to infer the poitns which are to be evaluated/used.
   @param <arrOf_truth_size> is the number of elements in thge arrOf_truth array.
   @param <arrOf_masks_size> is the number of elements in thge arrOf_truth array.
   @param <arrOfResult__spec> is the idnetifed/'found' "specificity" value.
   @param <arrOfResult__sens> is the idnetifed/'found' "sensitivity" value.
**/
void get_forRow_ROC_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const t_float *arrOf_data, const t_float *arrOf_minThresholds,  const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float *arrOfResult__spec, t_float *arrOfResult__sens);
//! Compute ROC using implicti masks
void get_forRow_ROC_typeOf_mask_implicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const t_float *arrOf_data, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float *arrOfResult__spec, t_float *arrOfResult__sens);
//! Compute ROC using explicit masks
void get_forRow_ROC_typeOf_mask_explicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const t_float *arrOf_data, const char *mask, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float *arrOfResult__spec, t_float *arrOfResult__sens);

//! Compute ROC
void get_matrix_ROC_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const t_float *arrOf_minThresholds,  const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens);
//! Compute ROC using implicti masks
void get_matrix_ROC_typeOf_mask_implicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens);
//! Compute ROC using explicit masks
void get_matrix_ROC_typeOf_mask_explicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const char *mask, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens);


#endif //! EOF
