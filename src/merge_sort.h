#ifndef merge_sort_h
#define merge_sort_h
#include "libs.h"
//#include "list.h"

/**
   @Name: merge_sort -- Sorts input data based on the merge-sort algorithm.
   @Major_change: 22.06.2011 by Ole Kristian Ekseth (oekseth)
 */
typedef class merge_sort {
 private:
  /**! Swaps the elements */
  void swap(int &a, int &b) {const int temp = b; b = a; a = temp;}

  /**
     merge -- Merges to sorted lists
     @Changed: 22.06.2011 by oekseth
  */
  void merge(int *array, const uint p, const uint q, const uint r) {
    const uint size[2] = {q - p, r -q };
    int *list[2];
    list[0] = new int[size[0]+1]; for(uint i = 0; i < size[0]; i++) list[0][i] = array[p+i];
    list[1] = new int[size[1]+1]; for(uint i = 0; i < size[1]; i++) list[1][i] = array[q+i];
    list[0][size[0]] = INT_MAX; list[1][size[1]] = INT_MAX;
    //    printf("\t\t("); for(uint i = 0;i < size[0]+1; i++)printf("%d, ", list[0][i]); printf(")\n");
    //printf("\t\t("); for(uint i = 0;i < size[1]+1; i++)printf("%d, ", list[1][i]); printf(")\n");
    uint i = 0, j = 0;
    for(uint k = p; k < r; k++) {
      if(list[0][i] < list[1][j]) array[k] = list[0][i++]; else array[k] = list[1][j++];
    }
    delete [] list[0], delete [] list[1];
  } 
  /**
     merge_procedure -- Sorts the array by using a divide- and conquer-approach.
     @Changed: 22.06.2011 by oekseth
  */
  void merge_procedure(int *array, const uint p, const uint r) {

    if(p< (r-1)) {
      //      printf("p = %u, r = %u\n", p, r);      printf("\n");
      const uint q = (p+r)/2;
      //      printf("#\tDivides into [%u, %u] and [%u, %u]\n", p, q, q, r);
      merge_procedure(array, p, q);
      merge_procedure(array, q, r);
      //printf("(merge procdedure: before- and after for [%u, %u, %u].)\n", p, q, r);
      //printf("\t\t("); for(uint i = p;i < q; i++) printf("%d, ", array[i]); printf(")\n");
      //printf("\t\t("); for(uint i = q;i < r; i++) printf("%d, ", array[i]); printf(")\n");
      merge(array, p, q, r);
      //printf("\t\t("); for(uint i = p;i < r; i++) printf("%d, ", array[i]); printf(")\n");
      //printf("-----\n");
    } //else 
  }
 public:
  /**
     sort_array -- Sorts the array by using a divide- and conquer-approach.
     @Changed: 22.06.2011 by oekseth
  */
  void sort_array(int *array, const uint size) {
    merge_procedure(array, 0, size);
  }
  void delete_class() {}//{array.delete_class(); }
  merge_sort() {};//array = list_t(0);}
  //! Asserts the class
  static bool assert_class() {
    printf("--\tAsserts class 'merge_sort'.\n");
    merge_sort temp = merge_sort();
    const uint array_size = 10;
    int sorted_array[array_size] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    int a = 2, b = 4; const int old_a = a, old_b = b;
    temp.swap(a, b); assert(a == old_b && old_a == b);
    temp.sort_array(sorted_array, array_size);    
    //printf("array[%u] = %d\n", 0, sorted_array[0]); 
    for(uint i = 1; i < array_size; i++) {
      //      printf("array[%u] = %d\n", i, sorted_array[i]); 
      assert(sorted_array[i] >=sorted_array[i-1]);
    }
    //delete [] sorted_array;
    temp.delete_class();
    printf("ok\tAssert of class 'merge_sort' complete.\n");
    return true;
  }
} merge_sort_t;
#endif
