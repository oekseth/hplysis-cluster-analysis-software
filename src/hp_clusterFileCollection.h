#ifndef hp_clusterFileCollection_h
#define hp_clusterFileCollection_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_clusterFileCollection
   @brief provide logics to analyse a subset of the simlairty-metrics supported by hpLyssis Knitting-Tools sim-metric suit (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
**/

#include "hp_clusterFileCollection_simMetricSet.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.

/**
   @enum e_hp_clusterFileCollection__clusterAlg
   @brief a generic stategy to idenitfy teh type of cluster-algorithms to use.
**/
typedef enum e_hp_clusterFileCollection__clusterAlg {
  //! Note: if we udpate [”elow] then remeber to also update our "data/local_downloaded/convert.pl".
  e_hp_clusterFileCollection__clusterAlg_kMeans, //! ie, for [avg, rank, median]
  e_hp_clusterFileCollection__clusterAlg_HCA, //! ie, for [avg, rank, median]
  //e_hp_clusterFileCollection__clusterAlg_,
  e_hp_clusterFileCollection__clusterAlg_undef //! ie, Not apply clustering.
} e_hp_clusterFileCollection__clusterAlg_t;

/**
   @enum e_hp_clusterFileCollection__goldClustersDefinedBy
   @brief specifies how (if any) the gold-clsuter-set may be idenitfied (oekseth, 06. feb. 2017).
   @remarks expected to be used if "mapOf_vertexClusterId == NULL" in the "s_hp_clusterFileCollection_t" object
 **/
typedef enum e_hp_clusterFileCollection__goldClustersDefinedBy {
  e_hp_clusterFileCollection__goldClustersDefinedBy_rowString,
  //e_hp_clusterFileCollection__goldClustersDefinedBy_
  e_hp_clusterFileCollection__goldClustersDefinedBy_undef, //! ie, gold-data-sepc is Not explcitly set.
} e_hp_clusterFileCollection__goldClustersDefinedBy_t;
/**
   @struct s_hp_clusterFileCollection__clusterAlg
   @brief provied logics to access a set of clluster-algorithms in one 'unfied' iteriaotn-rpcoedure (oekseth, 06. feb. 2017). 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
 **/
typedef struct s_hp_clusterFileCollection__clusterAlg {
  e_hpLysis_clusterAlg_t *list; //! which hold a list of e_hp_clusterFileCollection__clusterAlg_t objects
  uint list_size;
} s_hp_clusterFileCollection__clusterAlg_t;

/**
   @brief idneitfy an array of simlarity-emtrics assicated to enum_id (oesketh, 06. feb. 2017).
   @param <enum_id> is the enum to cosntuct the object for.
   @param <includeEmptyCase> which if set tot true impleis taht we 'alos' include an enum 'descriibng' an 'undef' case.
   @return a new-allcoated list of cluster-algorithm-specs
**/
s_hp_clusterFileCollection__clusterAlg_t init__s_hp_clusterFileCollection__clusterAlg_t(const e_hp_clusterFileCollection__clusterAlg_t enum_id, const bool includeEmptyCase);
//! De-allocate the s_hp_clusterFileCollection_simMetricSet_t object (oekseth, 06. feb. 2017).
void free__s_hp_clusterFileCollection_simMetricSet_t(s_hp_clusterFileCollection__clusterAlg_t *self);
/**
   @struct s_hp_clusterFileCollection
   @brief define an entity to describe a data-set to generate (oekseth,, 06. feb. 2017).
**/
typedef struct s_hp_clusterFileCollection {
  const char *tag;
  const char *file_name;
  s_kt_matrix_fileReadTuning_t fileRead_config;
  bool inputData__isAnAdjcencyMatrix;
  //e_measure_cmpCluster__dataCategories_row_t fileInCategory;
  // bool isTo_transpose;
  //bool useInCondensedEvaluation;
  uint k_clusterCount; 
  //!
  //! Specify (if any) a gold-standard-data-set to be used:
  uint *mapOf_vertexClusterId;   uint mapOf_vertexClusterId_size;    
  e_hp_clusterFileCollection__goldClustersDefinedBy_t alt_clusterSpec; //! which is evlauated/used if "mapOf_vertexClusterId == NULL".
  //!
  //! Subsets of metrics to analyse:
  //! Note: if the [”elow] is to 'none' (ie, "e_hp_clusterFileCollection_simMetricSet_undef") implies that the latter is Not comptued/evaluated.
  e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust; 
  e_hp_clusterFileCollection_simMetricSet_t metric__insideClust;
  e_hp_clusterFileCollection__clusterAlg_t clusterAlg;
} s_hp_clusterFileCollection_t;

//! Intiate the s_hp_clusterFileCollection_t and return (oesketh, 06. feb. 2017).
static s_hp_clusterFileCollection_t init__s_hp_clusterFileCollection_t() {
  s_hp_clusterFileCollection_t self;
  self.tag = NULL;
  self.file_name  = NULL;
  self.fileRead_config =  initAndReturn__s_kt_matrix_fileReadTuning_t();
  self.inputData__isAnAdjcencyMatrix = false;
  //e_measure_cmpCluster__dataCategories_row_t fileInCategory;
  // bool isTo_transpose;
  //bool useInCondensedEvaluation;
  self.k_clusterCount = 2; 
  //!
  //! Specify (if any) a gold-standard-data-set to be used:
  self.mapOf_vertexClusterId = NULL;   self.mapOf_vertexClusterId_size = 0;    
  self.alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef; //! which is evlauated/used if "mapOf_vertexClusterId == NULL".
  //!
  //! Subsets of metrics to analyse:
  //! Note: if the [”elow] is to 'none' (ie, "e_hp_clusterFileCollection_simMetricSet_undef") implies that the latter is Not comptued/evaluated.
  self.metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid; 
  self.metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
  self.clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
  //!
  //! @return
  return self;
}

//! @return an itnernal string-representaiton of an itnenrla clsuter-alg-object.
const char *getStringOfMAppingToFile__s_hp_clusterFileCollection_t(const uint data_id, const uint cnt_insideDataLoop, const s_hp_clusterFileCollection_t data_obj, const uint cluster_index, const uint met_prior, const uint met_inside, char *str_suff) ;
//! @return an itnernal string-representaiton of an itnenrla clsuter-alg-object.
const char *getStringOfMAppingToFile__simMetric__s_hp_clusterFileCollection_t(const s_hp_clusterFileCollection_t data_obj, const uint cluster_index, const uint met_prior, const uint met_inside, char *str_suff);

typedef enum e_hp_clusterFileCollection___internalTraverseLogic {
  e_hp_clusterFileCollection___internalTraverseLogic__writeOut,
  e_hp_clusterFileCollection___internalTraverseLogic__updateCounters,
} e_hp_clusterFileCollection___internalTraverseLogic_t;

typedef struct s_hp_clusterFileCollection_localCounter {
  uint cnt_data;
  uint cnt__data_x_cluster_x_metPri;
  uint cnt__data_x_cluster_x_metPri__x__metInside;
  uint cnt__cluster_x_metPri__x__metInside;
} s_hp_clusterFileCollection_localCounter_t;
//! Apply logics using a gneeirc approach.
s_hp_clusterFileCollection_localCounter_t applyLogics__insideChunk__s_hp_clusterFileCollection_t(FILE *file_out, const s_hp_clusterFileCollection_t *list, const uint list_size, e_hp_clusterFileCollection___internalTraverseLogic_t typeOf_logic);
//! Write out the mappign betwene the file-name-indices and their 'actual meaning':
void writeOut__mappingFile__s_hp_clusterFileCollection_t(FILE *file_out, const s_hp_clusterFileCollection_t *list, const uint list_size);

/**
   @struct s_hp_clusterFileCollection_traverseSpec
   @brief provide lgocis to evlauate the perofmrance (eg, internal cosnistency and acucrayc) of a large number of simlairty-metric, clsuter-algorithms, data-sets and clsuter-comparison-metics (CCms) (oekseth, 06. feb. 2017).
 **/
typedef struct s_hp_clusterFileCollection_traverseSpec {
  bool config__isToPrintOut__iterativeStatusMessage; // = true;
  bool config__performance__testImapcactOf__slowPerformance; // = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  const char *pathTo_localFolder_storingEachSimMetric; // = "results/tutEx__difficultData/";
  const char *nameOf_resultFile__clusterMemberships; // = "tutResult_simMetrics.js";
  const char *nameOf_resultFile__clusterMemberships__deviation; // = "tutResult_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  uint config_arg_npass; // = 10000; 
  //!
  //! Result data:
  e_hpLysis_export_formatOf_t config_exportFormat; // = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
  //bool config__isToPrintOut__iterativeStatusMessage; 
  bool isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  bool postProcessing__clusterResultMatrices; //! if "true": then we apply clustierng wrt. the 'data collected in result-amtrices': (a) apply sim-matrix+cluster; (b) write out both clsuter-results and a 'maksed' matrix ... both for transposed and non-transposed ... a result which may be used to assert/idnetify/descirb ethe simlairty between: (a) CCMs; (b) cluster-algorithms; (c) simlairty-metrics; (d) improtance/impact of data-sets (when evaluating simlairty between clsuter-algorithms).
  e_hpLysis_clusterAlg_t postProcessing__clusterResultMatrices__clustAlg;
  uint postProcessing__clusterResultMatrices__nClusters; //! ie, that the clusters may be seperated based on 2-3 groups, a genericd assumption which should be tuned by the caller.
  //! ------------------ 
  uint option_scoreFrequency__cntBuckets; //! which if Not set to '0' is used to 'descride' the number of 'buckets' used to describe the score-freuqncy of a pre-clustering-computed simliarty-matrix (ie, if ore-clstuering-simalrity-amtrices-options are used in at least one of the data--configuation-objects).
} s_hp_clusterFileCollection_traverseSpec_t;

//! Perofrm a minimal intiation of the "pathTo_localFolder_storingEachSimMetric" object (oekseth, 06. feb. 2017)
s_hp_clusterFileCollection_traverseSpec_t initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
/**
   @brief apply the lgoics described in our s_hp_clusterFileCollection_traverseSpec_t self object (oesketh, 06. feb. 2017).
   @param <self>  is the set of lgocis to apply
   @param <mapOf_realLife> is the lsit of cofnigruation-objects to use.
   @param <mapOf_realLife_size> is the number of elmeents in mapOf_realLife
   @return true upon success.
 **/
bool traverse__s_hp_clusterFileCollection_traverseSpec(const s_hp_clusterFileCollection_traverseSpec *self, const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size);
#endif //! EOF
