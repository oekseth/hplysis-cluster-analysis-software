#include "measure_externalCxx__nipals.h"
 /*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */


#include "measure_base.h"

//! Included Knitting-Tools code-files:
#include "kt_mathMacros.h"
// #include "matrix_transpose.h" //! which we evlauate 'while' evlauating the 'default' transpsoe-operaitons.
// #include "mask_api.h"
// #include "correlation_base.h"
// #include "correlationType_delta.h"
// #include "correlation_inCategory_delta.h"

//! Inlcude 'directly' the test-files to evlauate:
#include "externalLibs__nipals_alt1/maths.cxx"
#include "externalLibs__nipals_alt1/pls.cxx"
#include "externalLibs__nipals_alt1/model.cxx"



Vector<float> *get_pls_Cpp_vector_FromFloat(const t_float *row, const uint ncols) {
  assert(row);
  Vector<float> *obj = new Vector<float> ((int)ncols);
  for(uint i = 0; i < ncols; i++) {
    obj->SetValue(i, row[i]);
  }
  return obj;
}
Matrix<float> *get_pls_Cpp_matrix_FromFloat(t_float **matrix, const uint nrows, const uint ncols) {
  assert(matrix);
  Matrix<float> *obj_matrix_x = new Matrix<float>(nrows, ncols);

  for(uint x = 0; x < nrows; x++) {
    for(uint i = 0; i < ncols; i++) {
      obj_matrix_x->SetValue(x, i, matrix[x][i]);
    }
  }
  return obj_matrix_x;
}





#include "kt_math_matrix.h"

/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "measure.h"
#include "maskAllocate.h"
#include "kt_clusterAlg_fixed.h"

// //static const uint kilo = 1000;
// static clock_t clock_time_start; static clock_t clock_time_end;
// static struct tms tms_start;
// static  struct tms tms_end;
// /**
//    @brief Start the measurement with regard to time.
//    @remarks 
//    - Uses the global vairalbe in this file for this prupose.
//    - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
// **/
// static  void start_time_measurement() {
// #ifndef NDEBUG
//   //! Clears the cache by allocating 100MB of memory- and the de-allocating it:
//   // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses.
//   uint size = 1024*1024*100;
  
//   const char default_value_char = 0;
//   char *tmp = allocate_1d_list_char(size, default_value_char);
//   assert(tmp);
//   memset(tmp, 7, size);
//   free_1d_list_char(&tmp); tmp = NULL; size = 0;
// #endif
//   tms_start = tms();
//   clock_time_start = times(&tms_start);
//   // if(() == -1) // starting values
//   //   err_sys("times error");
// }
// // /**
// //    @brief Ends the measurement with regard to time.
// //    @param <msg> If string is given, print the status information to stdout.
// //    @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
// //    @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
// //    @return the clock tics on the system since the 'start_time_measurement()' method was called.
// // **/
// // static float end_time_measurement(const char *msg, float &user_time, float &system_time, const float prev_time_inSeconds) {
// //   clock_time_end = times(&tms_end); 
// //   long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
// //   const clock_t t_real = clock_time_end - clock_time_start;
// //   const float time_in_seconds = t_real/(float)clktck;
// //   const float diff_user = tms_end.tms_utime - tms_start.tms_utime;
// //   if(diff_user) user_time   = (diff_user)/((float)clktck);
// //   const float diff_system = tms_end.tms_stime - tms_start.tms_stime;
// //   system_time = diff_system/((float)clktck);
// //   if(msg) {
// //     // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
// //     if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) {
// //       printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
// //     } else {
// //       //float diff = prev_time_inSeconds / time_in_seconds;
// //       float diff = time_in_seconds / prev_time_inSeconds;
// //       // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
// //       printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg);
// //     }
// //   }
// //   //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
// //   return (float)time_in_seconds;
// // }


// // /**
// //    @brief Ends the measurement with regard to time.
// //    @return the clock tics on the system since the 'start_time_measurement()' method was called.
// // **/
// // static float end_time_measurement(const char *msg, const float prev_time_inSeconds) {
// //   float user_time = 0, system_time=0;
// //   return end_time_measurement(msg, user_time, system_time, prev_time_inSeconds);
// // }



// //! Ends the time measurement for the array:
// static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
//   assert(stringOf_measureText);
//   assert(strlen(stringOf_measureText));
//   //! Get knowledge of the memory-intiaition/allocation pattern:
//   {
//     //! Provides an identficator for the string:
//     const char default_value_char = 0;
//     const uint stirng_size = 1000;
//     char *string = allocate_1d_list_char(stirng_size, default_value_char);
//     //char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
//     sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
//     //! Prints the data:
//     //! Complte the measurement:
//     const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
//     //time.print_formatted_result(string); 
//     free_1d_list_char(&string); string = NULL; 
    
//     return time_in_seconds;
//   } 
// }

static void test_imapctOf_inverse() {
  const uint mapOf_value_ranges_size = 3;
  const uint mapOf_value_ranges[mapOf_value_ranges_size] = {50, 1000, 105}; //! ie, char, 16-bit int, and float
  //const bool mapOf_hasDecimals[mapOf_value_ranges_size] = {false, false, true};

    const uint arrOf_cnt_buckets_size = 1; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1,
												    // 2, 3, 4, 5, 5, 7, 8, 9, 10,
												    //11, 12, 13, 14, 15
												   // 80, 160
												   //, 320, 740, 1000
    };
    const uint size_each_bucket = VECTOR_FLOAT_ITER_SIZE * 4;
    
    //!
    //! FOrFirst comptue for cases which is Not optmized wrt. pow-table:
    const uint cnt_cases = 2;
    for(uint case_id = 0; case_id < cnt_cases; case_id++) {
      for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
	const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
	s_kt_math_matrix_config_inverse_t self = getEmptyObject__s_kt_math_matrix_config_inverse();
	//char *stringOf_description[10000] = {0}; // = NULL;
	const char *stringOf_base = NULL;
	if(case_id == 0) {
	  stringOf_base = "use naive implementation";
	  self.in_coFactor_use_fast = false;
	} else {
	  stringOf_base = "use optimized implementation, through use 'ordinary' pow-look-up";
	  self.in_coFactor_use_fast = true;
	  self.in_coFactor_isToUse_preComputedPows = false;
	}
	const t_float default_value_float = 5;;
	//! Allocate memory
	t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
	    
	//!
	//! Start the comptuation:
	start_time_measurement();  //! ie, start measurement

	t_float **result = get_inverseMatrix(matrix, size_of_array, self);
	t_float prev_time_inSeconds = 0;
	__assertClass_generateResultsOf_timeMeasurements(stringOf_base, size_of_array, prev_time_inSeconds);
	free_2d_list_float(&result, size_of_array);
	free_2d_list_float(&matrix, size_of_array);
      }	
    }

    for(uint case_id = 0; case_id < cnt_cases; case_id++) {
      for(uint value_range_case = 0; value_range_case < mapOf_value_ranges_size; value_range_case++) {
 	const uint max_value = mapOf_value_ranges[value_range_case];
 	for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
	  const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
	  
	  s_kt_math_matrix_config_inverse_t self = getEmptyObject__s_kt_math_matrix_config_inverse();
	  uint size_char = 10000; uint default_value_char = 0;
	  char *stringOf_description = allocate_1d_list_char(size_char, default_value_char); //[10000] = {0}; // = NULL;
	  const char *stringOf_base = "optimized including teh use of a pow-table";
	  self.in_coFactor_isToUse_fastMemAlloc = true;  self.in_coFactor_isToUse_preComputedPows = true;  self.isToUse_fast_genericImplmentation__forLoops = true; self.isToUse_fast_genericImplmentation__ifClauseOrder = true;
	  
	  sprintf(stringOf_description, "%s w/max-value=%u", stringOf_base, max_value);
	  const t_float default_value_float = max_value;
	  //! Allocate memory
	  t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
	    
	  //!
	  //! Start the comptuation:
	  start_time_measurement();  //! ie, start measurement

	  t_float **result = get_inverseMatrix(matrix, size_of_array, self);
	  t_float prev_time_inSeconds = 0;
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_description, size_of_array, prev_time_inSeconds);
	  free_2d_list_float(&result, size_of_array);
	  free_2d_list_float(&matrix, size_of_array);
	  free_1d_list_char(&stringOf_description);
	}
      }
    }
    
 

    // FIXME[jc]: update our "clusterC_cmp.html" wrt. [”elow] wehn 'above' has passed.
    /**
       ... the difference in perofmrance wrt. valeu-ranges indicates ....??...
       ... the impact wrt. a 'genralized re-write' of a basic/importnat/popular matrix-copmtuation underlines our assertion wrt. .... 
       ... the difference wrt. ordering/structuring of 'if-clauses' provdes an indiation of ... 
     **/
}


// /**
//    @brief 
//  **/
// static void test__impactOf__() {

//   assert(false); // FIXME: amke use of this fucntion, ie, .-.. first udpate 'this' ... and then call 'this'.

// }

//! Test the case where we only investigate wrt. the summariotns, ie, not wrt. the 'inner emmory-access'.
static void test_matrixMult_dummIteration_idealMemAllocCase(t_float **matrix, const uint size_adjusted, t_float **result) {
  for(uint i = 0; i < size_adjusted; i++) {
    for(uint out = 0; out < size_adjusted; out++) {
      t_float result = 0;
      for(uint col = 0; col < size_adjusted; col++) {
	result += out * col; //matrix[i][out]  * matrix[i][col] 
      }
      matrix[i][out] = result;
    }
  }
}


/**
   @brief comapre teh exeuciotn-time of diffenrt iteraiotn-strategies
 **/
static void test__impactOf__matrixMulitplicaiton() {

  //! Speicy the matrix-sizes:
  const uint arrOf_cnt_buckets_size = 10; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 
												 2, 3, 4, 5, 5, 7, 8, 9, 10,
												  //11, 12, 13, 14, 15
												  // 80, 160
												  //, 320, 740, 1000
  };
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_cnt_buckets_size, default_value_float);
  const uint size_each_bucket = VECTOR_FLOAT_ITER_SIZE * 64; //! ie, approx. 2000 entites for each vertex.


#include "measure_externalCxx__nipals__templateFunc__setOf_matrixMul.c"

  //!
  //!  De-allocate locally reserved memory
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}




/**
   @brief idetnify the min-size ofr SSE-overheads to be effective
   @remarks Compare:
   - non-sse: the default strateg of many libraries;
   - SSE: the use of SSE to improve the performance
   - SSE-and-non-SSE: the combiantion of SSE and non-SSE (ie, 'rodinary') to resolve/handle cases where the colusmn and/or row-size is not divisable by VECTOR_FLOAT_ITER_SIZE value.
   - value-overhead-non-SSE: call the fucnions in "http://www.utdallas.edu/~herve." NIPALS libary, ie, to investigate the overeahd assicated with 'using' internal fucntion-calls.
 **/
static void test__impactOf__2d_matrixMemory_commonAccessStrategies(const bool testFor_optimized, const bool testFor_naive) {
  //!
  //! Start the tests (a test-case whch is factored out into a spserte file in order to avodi the test-funciton fromb ecoing all to lengthy):
#include "test__impactOf__2d_matrixMemory_commonAccessStrategies.c"
}



// static void test__matrixMul__matrixTransposed_vs_matrixNonPreTransposed() {

//   assert(false); // FIXME: amke use of this fucntion, ie, .-.. first udpate 'this' ... and then call 'this'.
//   assert(false); // FIXME: cosnider to update "test__NIPALS_innerForLoop__SSE(..)" (and our algorithm itsleF) based on 'tehse' results.



// }


/**
   @brief test the exeuction-time cost of the 'inner part' of NIPALs algorithm:
   @remarks in this sub-seciton we the iteraiotn-step in "/home/klatremus/poset_src/externalLibs/nipals_fastPLS/NIPALS_cpu.m": the iteration-step 'cosnsits' of:
   ----------
   while ( ( (t0-t)'*(t0-t) > epsilon) && (nstep < maxstep));
     nstep=nstep+1;
     t0=t;
     w=normaliz(Xres'*u);
     t=normaliz(Xres*w);
     c=normaliz(Yres'*t);
     u=Yres*c;
   end;
   ----------
 **/
static void test__NIPALS_innerForLoop__SSE(const bool testFor_optimized, const bool testFor_naive) {
  

  //! Speicy the matrix-sizes:
  const uint arrOf_cnt_buckets_size = 10; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 
												 2, 3, 4, 5, 5, 7, 8, 9, 10,
												  //11, 12, 13, 14, 15
												  // 80, 160
												  //, 320, 740, 1000
  };
    const uint size_each_bucket = VECTOR_FLOAT_ITER_SIZE * 100;
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_cnt_buckets_size, default_value_float);

#define __use_Result_to_updateTableComparisonResults 1



  if(testFor_optimized) {
#ifndef __use_Result_to_updateTableComparisonResults
#error "!!\t The __use_Result_to_updateTableComparisonResults macro-option, ie, please investigate you call, eg, wrt. \"kt_func_multiplyMatrices_slow(..)\" or an itnernal fucntion-name"
#endif

    const char *stringOf_measureText = "optimized: NIPALs inner-loop";
    printf("\n-----------\t start-block:: for tag=\"%s\", at %s:%d\n", stringOf_measureText, __FILE__, __LINE__);

    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"


      const uint ncols = size_of_array;
      const uint ncols_adjusted = ncols;   const uint nrows_adjusted = size_of_array;
      const t_float default_value_float = 0;
      const uint ksamples = 0;
      //! ---
      t_float scalar_convergence_dt = 0; uint step = 0;
      //! ----
      t_float **matrix_x = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
      //t_float **matrix_y = allocate_1d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
      t_float *row_u = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);
      t_float *row_y = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);
      t_float *row_t0 = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);
      t_float *row_t = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);
      t_float *row_v = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);
      t_float *row_w = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);
      //   ///! ----
      //  //  memset_local(row_u, default_value_float, nrows_adjusted);

      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'

      //!
      ///! Perform the lgoics in the inner NIPALs loop:
      t_float scalar_convergence_dt_global = 0;
      for(uint i = 0; i < 1000; i++) {
#include "measure_externalCxx__nipals__func__innerLoop.c"
	scalar_convergence_dt_global += scalar_convergence_dt;
      }
      printf("(debug)\t scalar_convergence_dt=%u, at %s:%d\n", (uint)scalar_convergence_dt_global, __FILE__, __LINE__);
    
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif


      //!
      //!  De-allocate locally reserved memory
      free_2d_list_float(&matrix_x, nrows_adjusted);
      free_1d_list_float(&row_u);
      free_1d_list_float(&row_y);
      free_1d_list_float(&row_t0);
      free_1d_list_float(&row_t);
      free_1d_list_float(&row_v);
      free_1d_list_float(&row_w);
    }
#undef __use_Result_to_updateTableComparisonResults
  }
  if(testFor_naive) {
#define __use_Result_to_updateTableComparisonResults 0

    const char *stringOf_measureText = "naive: NIPALs inner-loop";
    printf("\n-----------\t start-block:: for tag=\"%s\", at %s:%d\n", stringOf_measureText, __FILE__, __LINE__);

    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"


      const uint nrows = size_of_array; const uint ncols = size_of_array;
      const uint ncols_adjusted = ncols;   const uint nrows_adjusted = size_of_array;
      const t_float default_value_float = 0;
      const uint ksamples = 0;
      //! ---
      t_float scalar_convergence_dt = 0; uint step = 0;
      //! ----
      t_float **matrix_x = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
      //t_float **matrix_y = allocate_1d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
      t_float *row_u = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);

      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix_x, nrows, ncols);
      Vector<float> *Y = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      //! --
      Vector<float> *Vrow = get_pls_Cpp_vector_FromFloat(row_u, nrows);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      Vector<float> *Vcol = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      Vector<float> *u = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      Vector<float> *v = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      Vector<float> *w = get_pls_Cpp_vector_FromFloat(row_u, ncols);
      
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'


      //! Start the test:
      PLS obj = PLS();
      
      t_float scalar_convergence_dt_global = 0;
      for(uint i = 0; i < 1000; i++) {
	scalar_convergence_dt_global += obj.test_inner_by_oekseth(X, Y, t, t0, Vrow, Vcol, u, v, w, ksamples);
      }


      printf("(debug)\t scalar_convergence_dt=%u, at %s:%d\n", (uint)scalar_convergence_dt_global, __FILE__, __LINE__);

      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif


      //!
      //! De-allocat ethe locally reserve3d memory:
      free_2d_list_float(&matrix_x, nrows_adjusted);       free_1d_list_float(&row_u);
      delete X; delete Y;
      delete Vrow;       delete Vcol;
      delete t;          delete t0;
      delete u;       delete v;       delete w;

    }
    
#undef __use_Result_to_updateTableComparisonResults    
  }



  //!
  //!  De-allocate locally reserved memory
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}

static void test__NIPALS_complete(const bool testFor_optimized, const bool testFor_naive) {

  
  assert(false); // FXIME[JC»: compelte writign this function when you have (a) learned by-heat NIPALs and (b) indeitnfied an optmized matrix-ivnerse procedure.


  if(testFor_optimized) {

    assert(false); // FXIME: add seomthgin

  }


  if(testFor_naive) { const char *stringOf_measureText = "naive NIPALs implementation";

    assert(false); // FIXME: update [”elow] call wrt. the 'out-commented fucntions'
    const uint size_of_array = 1000; 
      const uint nrows = size_of_array; const uint ncols = size_of_array;
      const uint ncols_adjusted = ncols;   const uint nrows_adjusted = size_of_array;
      const t_float default_value_float = 0;
      const uint ksamples = 0;
      //! ---
      t_float scalar_convergence_dt = 0; uint step = 0;
      //! ----
      t_float **matrix_x = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
      //t_float **matrix_y = allocate_1d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
      t_float *row_u = allocate_1d_list_float(ncols_adjusted, nrows_adjusted);

      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix_x, nrows, ncols);
      Vector<float> *Y = get_pls_Cpp_vector_FromFloat(row_u, ncols);

      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'


      PLS obj = PLS();
      obj.runpls(X, Y, ksamples, NULL);


      t_float prev_time = 0;
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time); //mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 

      delete X; delete Y;
  }

}


//! The main-function to evaluate the perofmrance of enxternal NIPLAs impelmetnationw r.t our efficent NIPLAs implementaiton
void main__externalCXX_nipals(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg for 

  { //! Logics to test for NIPALs wrt. the 'inner iterative for-loop':
    if(isTo_processAll || (0 == strncmp("NIPALs_inner_optimized", array[2], strlen("NIPALs_inner_optimized"))) ) {
      const bool testFor_optimized = true; const bool testFor_naive = false;
      test__NIPALS_innerForLoop__SSE(testFor_optimized, testFor_naive);
      cnt_Tests_evalauted++;
    } else if(isTo_processAll || (0 == strncmp("NIPALs_inner_naive", array[2], strlen("NIPALs_inner_naive"))) ) {
      const bool testFor_optimized = false; const bool testFor_naive = true;
      test__NIPALS_innerForLoop__SSE(testFor_optimized, testFor_naive);
      cnt_Tests_evalauted++;
    } else if(isTo_processAll || (0 == strncmp("NIPALs_inner", array[2], strlen("NIPALs_inner"))) ) {
      const bool testFor_optimized = true; const bool testFor_naive = true;
      test__NIPALS_innerForLoop__SSE(testFor_optimized, testFor_naive);
      cnt_Tests_evalauted++;
    }
  }


  { //! Logics to test for NIPALs wrt. the 'inner iterative for-loop':
    if(isTo_processAll || (0 == strncmp("NIPALs_complete_optimized", array[2], strlen("NIPALs_complete_optimized"))) ) {
      const bool testFor_optimized = true; const bool testFor_naive = false;
      test__NIPALS_complete(testFor_optimized, testFor_naive);
      cnt_Tests_evalauted++;
    } else if(isTo_processAll || (0 == strncmp("NIPALs_complete_naive", array[2], strlen("NIPALs_complete_naive"))) ) {
      const bool testFor_optimized = false; const bool testFor_naive = true;
      test__NIPALS_complete(testFor_optimized, testFor_naive);
      cnt_Tests_evalauted++;
    } else if(isTo_processAll || (0 == strncmp("NIPALs_complete", array[2], strlen("NIPALs_complete"))) ) {
      const bool testFor_optimized = true; const bool testFor_naive = true;
      test__NIPALS_complete(testFor_optimized, testFor_naive);
      cnt_Tests_evalauted++;
    }
  }


  if(isTo_processAll || (0 == strncmp("2dMatrix_optimized", array[2], strlen("2dMatrix_optimized"))) ) {
    const bool testFor_optimized = true; const bool testFor_naive = false;
    test__impactOf__2d_matrixMemory_commonAccessStrategies(testFor_optimized, testFor_naive);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("2dMatrix_naive", array[2], strlen("2dMatrix_naive"))) ) {
    const bool testFor_optimized = false; const bool testFor_naive = true;
    test__impactOf__2d_matrixMemory_commonAccessStrategies(testFor_optimized, testFor_naive);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("2dMatrix", array[2], strlen("2dMatrix"))) ) {
    const bool testFor_optimized = true; const bool testFor_naive = true;
    test__impactOf__2d_matrixMemory_commonAccessStrategies(testFor_optimized, testFor_naive);
    cnt_Tests_evalauted++;
  }
  // if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) {
  //   
  //   cnt_Tests_evalauted++;
  // }
  

  if(isTo_processAll || (0 == strncmp("matrixMult", array[2], strlen("matrixMult"))) ) {
    //! Test macir-mulicplation-strategies iot. comapre teh exeuciotn-time of diffenrt iteraiotn-strategies:
    test__impactOf__matrixMulitplicaiton();
    cnt_Tests_evalauted++;
  }
  
  //! Test the exeuction-tiem assicated tod fifernet cases. wrt comptaution of ivnerse:
  if(isTo_processAll || (0 == strncmp("inverse", array[2], strlen("inverse"))) ) {
    test_imapctOf_inverse();
    cnt_Tests_evalauted++;
  }


  
  //.c:80

  assert(false); // FIXME: validate that we have tested for all functiosn in ... "kt_mathMacros.h"
  assert(false); // FIXME: validate that we have tested for all functiosn in ... NIPALs alrotihm


  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
