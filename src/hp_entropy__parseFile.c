  //!
  //!
  //! Intailize.
s_kt_entropy_t obj_entropy; bool obj_entropy_isAllocated = false;
  if( (max_theoreticCountSize == 0) || (max_theoreticCountSize == UINT_MAX) ) {
    fprintf(stderr, "!!\t Expected the max-therotiecal-lmit to be set, which is Not the case: pelase add support for this. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }      
  //!
  //!
  const char config_file_sep = file_config->config_file_sep;
  const char config_file_sep_newLine = file_config->config_file_sep_newLine;
  //! --------
  const bool config_rows__allHaveSameLength = file_config->config_rows__allHaveSameLength;
  const bool config_rows__ignoreFirstRow = file_config->config_rows__ignoreFirstRow;
  const bool config_rows__ingoreFirstColumn = file_config->config_rows__ingoreFirstColumn; //! which si to be set ot true if the first column in each row describes a stirng-idntifer. 
  { //! Load the file: 
    //    start_time_measurement();
    FILE *file = fopen(file_config->input_file, "rb");
    if(file == NULL) {
      fprintf(stderr, "!!\t Unable to open file=\"%s\" for writing, at [%s]:%s:%d\n", file_config->input_file, __FUNCTION__, __FILE__, __LINE__);
    }
    FILE *file_result = NULL;
    {
      if(file_config->result_file != NULL) {
	assert(strlen(file_config->result_file));
	file_result = fopen(file_config->result_file, "wb");
	if(file_result == NULL) {
	  fprintf(stderr, "!!\t Unable to open file=\"%s\" for writing, at [%s]:%s:%d\n", file_config->result_file, __FUNCTION__, __FILE__, __LINE__);
	}
      } else if(mat_result == NULL) {	
	file_result = stdout;
      }
    }
    
    assert(file);
    uint word_buffer_currentPos = 0;
#define  word_buffer_size  128
    char word_buffer[word_buffer_size] = {'\0'}; //! ie, as a number having mroe then 128 digits is aboute the T_FLOAT_MAX limit (oesketh, 06. jul. 2017).
    char c = '\0';
    s_kt_list_1d_uint_t obj_current_row = setToEmpty__s_kt_list_1d_uint_t();
    uint cnt_tabs = 0; uint cnt_tabs_total = 0;
    uint cnt_rows = 0; uint cnt_rows_total = 0;
    //!
    //!
    while ((c = getc(file)) != EOF) {
      if(c == config_file_sep) {
	if(!file_config->config_rows__ingoreFirstColumn || (cnt_tabs_total > 1) ) {
#if(1 == 1)
	  const uint score = (uint)atoi(word_buffer);
	  // printf("[%u]score=%u, at %s:%d\n", cnt_tabs, score, __FILE__, __LINE__);
#else
	  const t_float score = (t_float)atof(word_buffer);
#endif
	  if(file_config->config_rows__allHaveSameLength && (cnt_rows_total > 0)) {
	    obj_current_row.list[cnt_tabs] = score;
	  } else { //! then we need to provide lgocis to handle 'veroflows' wr.t the row.
	    set_scalar__s_kt_list_1d_uint_t(&obj_current_row, cnt_tabs, score); 
	  }
	  cnt_tabs++;
	}
	cnt_tabs_total++;
	// if(cnt_rows > self->ncols) {
	//   fprintf(stderr, "!!\t Your input-weight-file has more elements than the reserved number of columns, ie, where we expected %u <= %u, an error whcih could arise if your are using the 'tranposed option' in-cosnistnely: we expec thte weights to be deifned for the columsn (and not rows) For questions please contact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_rows, self->ncols, __FUNCTION__, __FILE__, __LINE__);
	// }
	//! Reset:
	for(uint i = 0; i < word_buffer_size; i++) {word_buffer[i] = '\0';}
	word_buffer_currentPos = 0;
      } else if(c == config_file_sep_newLine) {
	if(!file_config->config_rows__ingoreFirstColumn || (cnt_rows_total > 1)) {
	  //! Apply logics:
	  if(cnt_rows == 0) {obj_entropy = initSimple__s_kt_entropy(max_theoreticCountSize, /*vector_length=*/cnt_tabs, enum_entropy); obj_entropy_isAllocated = true; }
	  __MiF__parseRow();
	}
	cnt_tabs = 0;
	cnt_tabs_total = 0;
	//! Note: for cases where the first column is a string-id, we use an column-offset of "-1":
	// if(false) {
	//   //assert(pos_in_column_0_prev <= 1000);
	//   char buffer_local[1000]; strncpy(buffer_local, word_buffer, word_buffer_currentPos);
	//   printf("[%u][%u] = \"%s\"=%f, at %s:%d\n", cnt_rows, cnt_tabs-1, buffer_local, val,  __FILE__, __LINE__);
	// }
	//	    assert(val != INFINITY);       assert(val != NAN);
	//	    self->weight[cnt_rows] = val;      
	cnt_rows++;
	cnt_rows_total++;
      } else { //! then inser tthe value in the current word-buffer-chunk:
	if( (c != '-') ) {
	  word_buffer[word_buffer_currentPos] = c; 
	  word_buffer_currentPos++;
	}
      }
    }
    //! De-allcote: 
    free__s_kt_list_1d_uint_t(&obj_current_row);
    fclose(file);
    if( (file_result != NULL) && (file_result != stdout) ) {
      fclose(file_result);
    }
  }
  //!
  //! De-allcote
if(obj_entropy_isAllocated) {  free__s_kt_entropy(&obj_entropy); }
#undef __MiF__parseRow
