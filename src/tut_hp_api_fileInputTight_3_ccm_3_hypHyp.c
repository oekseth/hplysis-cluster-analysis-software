#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis

/**
   @brief examplify how our "hp_api_fileInputTight.h" may be used for: "gold1 X gold2" CCM-evaluation
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @reamrks Idea: provide an 'intutiion' wrt. the applicaiton of CCMs: in this example we 'see' how the goldxgold-CCMs are 'cocneptaully simlair' to simalirty-metirc: to 'produce' a scalar from a comparion of/betwee two rows. To illsutate/examplify the latter, we comptue a CCM for all combiantions of rows in a 'wild-card' input-data-matrix, ie, simliar to an 'all-against-all' silmairty-matrix-comptaution-approach. 
   @remarks modifies "tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.c" wrt.:
   -- CCM: evlauate the simlairty be tween two different hypothesis (rather tahn between one simliarty-matrix and one hypothesis).
   -- sim-metric: examplifes the use of Jeffreys and Euclidian simliarty-metric
   @remarks 
   -- input: in this example we (a) load an 'arbirary' input-file, and then (b) compute CCM (as an alternative to the applicaiton of simliarty-metrics).
   @remarks for a demosntation of a 'closer-to-real-life-example' please se our "tut_helloWorld.c" use-case.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  e_kt_matrix_cmpCluster_metric_t enum_id = e_kt_matrix_cmpCluster_metric_randsIndex;
  //!
  //! Define an input-file to use in analysis:
  const char *file_name = "data/local_downloaded/34_Cavendish.csv.hpLysis.tsv";
  //const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 

  //! Load the TSV-file:
  //! Note: advanced options in data-loading conserns noise-injection, both wrt. 'noise-functions' and 'merging of different data-sets'.
  s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(file_name, initAndReturn__s_kt_matrix_fileReadTuning_t()); //! where latter funciton is deifned in our "kt_matrix.h"


  //! Tarverse through eahc of the hypothesis:
  for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
    for(uint row_id_2 = 0; row_id_2 < mat_input.nrows; row_id_2++) {
      s_kt_list_1d_uint_t list_hypo_1    = init__s_kt_list_1d_uint_t(mat_input.nrows);
      s_kt_list_1d_uint_t list_hypo_2    = init__s_kt_list_1d_uint_t(mat_input.nrows);
      //! Insert the valeus gnerated in oru 'random call':
      MF__floatToUint__matrix__atRow(/*row_id=*/row_id, /*mat_input=*/mat_input, /*list_result=*/list_hypo_1);
      MF__floatToUint__matrix__atRow(/*row_id=*/row_id_2, /*mat_input=*/mat_input, /*list_result=*/list_hypo_2);
      //! 
      assert(list_hypo_1.list_size == list_hypo_2.list_size);
      //!
      //! Apply logics:
      t_float ccm_result = 0;
      bool is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(enum_id, &list_hypo_1, &list_hypo_2, &ccm_result);
      assert(is_ok);
      printf("[%u][%u]\t ccm_result='%f', at %s:%d\n", row_id, row_id_2, ccm_result, __FILE__, __LINE__);
      //! De-allcoate:
      free__s_kt_list_1d_uint_t(&list_hypo_1);
      free__s_kt_list_1d_uint_t(&list_hypo_2);
    }
  }
  const char *stringOf_enum = getStringOf__e_kt_matrix_cmpCluster_metric_t(enum_id);
  const char *stringOf_inputFile_1 = file_name;
  const char *stringOf_inputFile_2 = NULL;
  const char *stringOf_exportFile = NULL; //! ie, to "stdout".
  const char *stringOf_exportFile__format = NULL;
  const uint ncluster = UINT_MAX; const char *stringOf_ncluster = NULL;
  const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
  //! 
  { //! Case(b): API-logis using strings:
    //! 
    //! Apply logics:
    uint arg_size = 0;
    char *listOf_args__2d[10];
    char *listOf_args = construct__terminalArgString__fromInput__hp_api_fileInputTight(/*debug_config__isToCall=*/true,  
										       stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse, &arg_size, listOf_args__2d);
    //const bool is_ok = readFromFile__exportResult__hp_api_fileInputTight(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse);
    assert(listOf_args); assert(strlen(listOf_args));
    MF__expand__bashInputArg__hp_api_fileInputTight("", "Use-case: CCM:gold1Xgold2: |hypothesis|=[matrix]", listOf_args__2d, arg_size);
    //printf("%s\t #! %s\n", listOf_args, "Use-case: CCM:matrix-based");
    //! ----------------------------------
    //! 
    //! Case(c): Bash-API using strings:
    //! 
    //! Apply logics:
    const bool is_ok = fromTerminal__hp_api_fileInputTight(listOf_args__2d, arg_size);
    assert(is_ok);
    //! De-allocate:
    if(listOf_args) {free_1d_list_char(&listOf_args); listOf_args = NULL;}
  }

  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  //free__s_kt_matrix(&);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

