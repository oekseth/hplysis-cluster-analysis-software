#ifndef hp_distance_h
#define hp_distance_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_distance
   @brief a wrapper to provide simplfied access to an itnernal simliarty metrics in "kt_distance" (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
**/

#include "kt_matrix.h" 
#include "kt_distance.h"
#include "kt_matrix_base.h"

//#include "kt_randomGenerator_vector.h"
/**
   @enum e_hp_distance__subLogics_t
   @brief provide speciivc cofngiruations used in our 'genralised' distance-fucntion-comptaution (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
 **/
typedef enum e_hp_distance__subLogics {
  e_hp_distance__subLogics_eachVector_select_min, //! eg, used in the "kmeans--minibatch" algorithm
  e_hp_distance__subLogics_eachVector_select_min__index, //! index instead of value: eg, used in the "kmeans--minibatch" algorithm
  e_hp_distance__subLogics_eachVector_select_max, //! eg, used in the "kmeans++" algorithm
  e_hp_distance__subLogics_eachVector_select_max__index, //! index instead of value: eg, used in the "kmeans++" algorithm
  e_hp_distance__subLogics_undef
} e_hp_distance__subLogics_t;
/**
   @struct s_hp_distance__config
   @brief provide a cofnigruation to the genralised distance-comptaution-wrapper-fucntion in our "hp_distance.h" header-file (eopsekth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
 **/
typedef struct s_hp_distance__config {
  uint obj_1_index1; //! which if the 'index' to choose in obj_1
  uint obj_2_index2; //! which if the 'index' to choose in obj_2
  e_hp_distance__subLogics_t typeOf__postProcessing;
  //struct s_kt_matrix dummy; // FIXME: remove
  //s_kt_matrix_t dummy; // FIXME: remove
} s_hp_distance__config_t;

//! @return an tiantied verison of the "s_hp_distance__config_t" object.
s_hp_distance__config_t init__s_hp_distance__config_t();

/**
   @struct s_hp_distance
   @brief provide a cofnigruation to the genralised distance-comptaution-wrapper-fucntion in our "hp_distance.h" header-file (eopsekth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
 **/
typedef struct s_hp_distance {
  s_kt_correlationMetric_t obj_metric;
  bool needTo_useMask_evaluation;
  //! ----------------- 
  const s_kt_matrix_base_t *obj_1;
  const s_kt_matrix_base_t *obj_2;
  t_float (*metric_each) (config_nonRank_each_t);
  void (*metric_oneToMany) (config_nonRank_oneToMany_t);
  //! -----------------------------------------------------------
  struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
  bool isTo_useOptimizedMetricFor_kendall;
  //! -----------------------------------------------------------
  s_hp_distance__config_t static_config;
} s_hp_distance_t;

//! @return an itnated verison of the "s_hp_distance_t" object.
s_hp_distance_t init__s_hp_distance(const s_kt_correlationMetric_t obj_metric, 
				    const bool needTo_useMask_evaluation__input,
				    const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2,
				    s_hp_distance__config_t local_config
				    );
//! De-allcoates the s_hp_distance_t object (oekseth, 06. feb. 2017)
void free__s_hp_distance(s_hp_distance_t *self);

/**
   @brief provide a convenieicne-fucntion to kcomptue simalrity between entites (oekseth, 06. feb. 2017).
   @param <self> is the cofngiruation-seopcfic object to use.
   @param <obj_1> is the first matrix to compute for;
   @param <obj_2> si the second amtrix to copmtue for: set to NULL if we are to copmtue using an "obj_1 x obj_1" appraoch;
   @param <obj_result> is the result-set: if set to 'empty' then we allcoat ehte object 'inside this call'.
   @return true upon success.
   @remarks iot. copmtue a "oneToMany" appraoch then set "obj_1->nrows == 1" OR "obj_2->nrows == 1" 
   @remarks performance: for a large number of muliple calls to this funciton we suggest instead using lgocis defined in our "kt_distance.h", ie, as the "hp_distance" provdies a set of coenvenience-fucntison desinged to simplify use and applciaotn of the logics forund in Knitting-Tools extneisve fucntion-library.
 **/
bool apply__extensive__hp_distance(s_hp_distance_t *self, const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2,
				   s_kt_matrix_base_t *obj_result);


/**
   @brief provide a convenieicne-fucntion to kcomptue simalrity between entites (oekseth, 06. feb. 2017).
   @param <obj_metric> is the metric to apply.
   @param <obj_1> is the first matrix to compute for;
   @param <obj_2> si the second amtrix to copmtue for: set to NULL if we are to copmtue using an "obj_1 x obj_1" appraoch;
   @param <obj_result> is the result-set: if set to 'empty' then we allcoat ehte object 'inside this call'.
   @param <local_config> which is used if we are Not to compute an all-agaisnta-ll simalrity-emtric-appraoch.
   @return true upon success.
   @remarks iot. copmtue a "oneToMany" appraoch then set "obj_1->nrows == 1" OR "obj_2->nrows == 1" 
   @remarks performance: for a large number of muliple calls to this funciton we suggest instead using lgocis defined in our "kt_distance.h", ie, as the "hp_distance" provdies a set of coenvenience-fucntison desinged to simplify use and applciaotn of the logics forund in Knitting-Tools extneisve fucntion-library.
 **/
bool apply__hp_distance(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2, s_kt_matrix_base_t *obj_result, const s_hp_distance__config_t local_config);


/**
   @brief a permtuaiotn of "apply__hp_distance(..)" to simplify computiosn for rows (oekseth, 06. feb. 2017).
   @param <obj_metric> is the metric to apply.
   @param <row_1> is the first row to compute for;
   @param <row_2> si the second row to copmtue for: set to NULL if we are to copmtue using an "obj_1 x obj_1" appraoch;
   @param <scakar_result> is the result-set: if set to 'empty' then we allcoat ehte object 'inside this call'.
   @return true upon success.
   @remarks iot. copmtue a "oneToMany" appraoch then set "obj_1->nrows == 1" OR "obj_2->nrows == 1" 
   @remarks performance: for a large number of muliple calls to this funciton we suggest instead using lgocis defined in our "kt_distance.h", ie, as the "hp_distance" provdies a set of coenvenience-fucntison desinged to simplify use and applciaotn of the logics forund in Knitting-Tools extneisve fucntion-library.
 **/
bool apply__rows_hp_distance(const s_kt_correlationMetric_t obj_metric, t_float *row_1, t_float *row_2, const uint ncols, t_float *scalar_result);

/**
   @brief a permtuaiotn of "apply__hp_distance(..)" to simplify computiosn for rows (oekseth, 06. feb. 2017).
   @param <obj_metric> is the metric to apply.
   @param <row_1> is the first row to compute for;
   @param <row_2> si the second row to copmtue for: set to NULL if we are to copmtue using an "obj_1 x obj_1" appraoch;
   @param <scakar_result> is the result-set: if set to 'empty' then we allcoat ehte object 'inside this call'.
   @return true upon success.
   @remarks iot. copmtue a "oneToMany" appraoch then set "obj_1->nrows == 1" OR "obj_2->nrows == 1" 
   @remarks performance: for a large number of muliple calls to this funciton we suggest instead using lgocis defined in our "kt_distance.h", ie, as the "hp_distance" provdies a set of coenvenience-fucntison desinged to simplify use and applciaotn of the logics forund in Knitting-Tools extneisve fucntion-library.
   @remarks input: expects numbers to be less than T_FLOAT_MAX value.
 **/
bool apply__uint_rows_hp_distance(const s_kt_correlationMetric_t obj_metric, const uint *row_1, const uint *row_2, const uint ncols, t_float *scalar_result);
//! Comptue simliarty-metric for a row.
//! @return the comptued simlairty-score.
//! @remarks a wrapper-fucntion to our "apply__uint_rows_hp_distance(..)".
static t_float applyAndReturn__uint_rows_hp_distance(const s_kt_correlationMetric_t obj_metric, const uint *row_1, const uint *row_2, const uint ncols) {
  t_float ret_val = T_FLOAT_MAX;
  const bool is_ok = apply__uint_rows_hp_distance(obj_metric, row_1, row_2, ncols, &ret_val);
  assert(is_ok);
  return ret_val;
}


/**
   @brief provide lgocis to unify a cluster-cneotrid-list and a set og 'flboal vertices' (oekseth, 06. feb. 2017)
   @param <obj_metric> is disntace-mtrci to apply
   @param <mapOf__clusterVertexCentroids> of size k_clusterCount: maps each cluster[i] to a vertex in matrix_input
   @param <matrix_input> is the lboal distnace-matrix to be used
   @param <k_clusterCount> is the number of clusters for which to idneitfy the 'dieal internal index'.
   @param <mapOf_columnToRandomScore> is the result-list to update
   @param <isto__findmin> which if set to false impleis that we idneitfy the 'row-index' with the 'hgist distnace-score'.
   @return true if oepration went smooth
   @remarks idea is to idnietyf the cluster-cneotrid-vertices which are cloest to each vertex in matrix_input
   @remarks A local approach to comptuye the vertex-membershisp: appraoch is simialr to our 'inner' appraoch in our "kt_clusterAlg_fixed.c" (oekseth, 06. feb. 2017).
   @remarks  Note: the "assign" funciton si called After the 'specific idnaiton-funciton' ... eg, the "__math_generateDistribution__randomPermutation___namedInPacakge__fastKmeans__random(..)" function: "x" is the 'orignal inptu-data-set', 'c' is the result generated in the latter function, "assigment" is the 'to-be-defedined' intal cluster-assignemtn.
**/
bool findExtremeIndex__inSecondMatrix__aux__hp_distance(const s_kt_correlationMetric_t obj_metric, const uint *mapOf__clusterVertexCentroids, s_kt_matrix_base_t *matrix_input, const uint k_clusterCount, uint *mapOf_columnToRandomScore, const bool isTo__findMin);

/**
   @brief provide a convenieicne-fucntion to kcomptue simalrity between entites (oekseth, 06. feb. 2017).
   @param <obj_metric> is the metric to apply.
   @param <obj_1> is the first matrix to compute for;
   @param <obj_2> si the second amtrix to copmtue for: set to NULL if we are to copmtue using an "obj_1 x obj_1" appraoch;
   @param <config_cntMin_afterEach> the minimum set of unique vertices to be inside "config_ballSize_max" (ie, for the row to be of interest, eg, wrt. DB-SCAN-application)
   @param <config_cntMax_afterEach> the max-number of interesting vertices: vertices are sorted by score: set to T_FLOAT_MAX to ignore.
   @param <config_ballSize_max> is the max-vlaue-trehshold to be used: set to T_FLOAT_MAX to ignore.
   @param <isTo_ingore_zeroScores> which is to be set to "true" if 'zero-distance-scores' are Not of interest.
   @return the sparse 2d-object holding the sorted set/colleciton of [row-id] = "{(column-id, pair-score)}"
   @remarks iot. copmtue a "oneToMany" appraoch then set "obj_1->nrows == 1" OR "obj_2->nrows == 1" 
   @remarks performance: for a large number of muliple calls to this funciton we suggest instead using lgocis defined in our "kt_distance.h", ie, as the "hp_distance" provdies a set of coenvenience-fucntison desinged to simplify use and applciaotn of the logics forund in Knitting-Tools extneisve fucntion-library.
 **/
s_kt_list_2d_kvPair_t apply__2d__storeSparse__hp_distance(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_base_t *obj_1, const s_kt_matrix_base_t *obj_2, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores);
#endif //! EOF
