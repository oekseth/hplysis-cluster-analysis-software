
if( (enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__all) || (enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__setBased)) 
  { //! Evalaute for the non-matrix-scores:
      //! ----------------------------------------
      //!
      //! Iterate through the different non-matrix-based measures:
      for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) {	
	uint row_id = cmpMetric_id;
	const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id;
	if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp) ) {continue;} //! ie, as we thenn assume the metric is a matrix-base ematric (eg, Silhhoutte index)
	//!
	{ //! Add the name of this row:
	  char string_local[1000] = {'\0'}; sprintf(string_local, "%s", getStringOf__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp));
#if(__localConfig__isToApplyLogics == 1)
	  assert(row_id < obj_metricDataScores.nrows);
	  set_string__s_kt_matrix(&obj_metricDataScores, row_id, string_local, /*addFor_column=*/false);
	  if(obj_metricDataScores__subsetSetBased.ncols > 0) {
	    set_string__s_kt_matrix(&obj_metricDataScores__subsetSetBased, row_id_subsetSetBased__cnt, string_local, /*addFor_column=*/false);
	  }
#elif(__localConfig__isToApplyLogics == 2) //! endif(__localConfig__isToApplyLogics == 1)
#ifndef __internalDataUpdate__transposeOrder
	  assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.nrows);
	  set_string__s_kt_matrix(&__obj__localMatrix__defaultMetrics, __obj__localMatrix__defaultMetrics_cntRows, string_local, /*addFor_column=*/false);
#else
	  assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.ncols);
	  set_string__s_kt_matrix(&__obj__localMatrix__defaultMetrics, __obj__localMatrix__defaultMetrics_cntRows, string_local, /*addFor_column=*/true);
#endif
#endif //! endif(__localConfig__isToApplyLogics == 2)
	}	
	uint local_col_id = 0;
	//!
	//! Iterate through the data:
	for(uint data_1 = 0; data_1 < cnt_data_nrows; data_1++) {
	  //!
	  //! Iterate, comparing 'this' to the other/laterntive data-sets:
	  for(uint data_2 = 0; data_2 < cnt_data_ncols; data_2++) {
#if( (__localConfig__isToApplyLogics == 1) || (__localConfig__isToApplyLogics == 2) )
#if(__localConfig__isToApplyLogics == 1) //! then we 'apply' the clustering:
	    const uint cnt_clusters_1 = cnt_clusters;
	    const uint cnt_clusters_2 = cnt_clusters;
	    uint *mapOf_vertexToCentroid1 = arrOf_2d_data__set1[data_1];
	    uint *mapOf_vertexToCentroid2 = arrOf_2d_data__set2[data_2];
	    //! ---- 
	    s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
	    const uint max_cntClusters = macro_max(cnt_clusters_1, cnt_clusters_2);
	    t_float **matrixOf_coOccurence = NULL;
	    s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
	    build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence, matrixOf_coOccurence, config_intraCluster);

	    //! Comptue for the 'specific' cluster-comparison-score:
	    s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
	    computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
	    const t_float scalar_score_altTest = metric_result.score_1;
	    assert(local_col_id < obj_metricDataScores.ncols);
	    obj_metricDataScores.matrix[row_id][local_col_id] = scalar_score_altTest;
	    if(obj_metricDataScores__subsetSetBased.ncols > 0) {
	      assert(row_id_subsetSetBased__cnt < obj_metricDataScores__subsetSetBased.nrows);
	      assert(local_col_id < obj_metricDataScores__subsetSetBased.ncols);
	      obj_metricDataScores__subsetSetBased.matrix[row_id_subsetSetBased__cnt][local_col_id] = scalar_score_altTest;
	    }
	    //!
	    //! Updat ehte matrix with the score:
	    //!
	    //! De-allocate:
	    free__s_kt_matrix_cmpCluster(&obj_cmp);
#else //! then we build subset of the matrices:
	    if(__obj__localMatrix__defaultMetrics_cntRows == 0) { //!  then we 'add the column-name':
	      char string_local[1000] = {'\0'}; sprintf(string_local, "%s.%u.%u", prefix__clusterSets, data_1, data_2);
#ifndef __internalDataUpdate__transposeOrder
	      assert(local_col_id < __obj__localMatrix__defaultMetrics.ncols);
	      set_string__s_kt_matrix(&__obj__localMatrix__defaultMetrics, local_col_id, string_local, /*addFor_column=*/true);
#else
	      assert(local_col_id < __obj__localMatrix__defaultMetrics.nrows);
	      set_string__s_kt_matrix(&__obj__localMatrix__defaultMetrics, local_col_id, string_local, /*addFor_column=*/false);
#endif
	    }
	    assert(__localInputMatrix.matrix);
#ifndef __internalDataUpdate__transposeOrder
	    assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.nrows);
	    assert(row_id_global < __localInputMatrix.nrows);
	    assert(local_col_id < __localInputMatrix.ncols);
	    //! Update:
	    __obj__localMatrix__defaultMetrics.matrix[__obj__localMatrix__defaultMetrics_cntRows][local_col_id] = __localInputMatrix.matrix[row_id_global][local_col_id];
#else
	    if(__internalDataUpdate__transposeOrder__inputIsAlsoTransposed == false) {
	      assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.ncols);
	      assert(row_id_global < __localInputMatrix.ncols);
	      assert(local_col_id < __localInputMatrix.nrows);
	      assert(row_id_global < __localInputMatrix.nrows);
	      assert(local_col_id < __localInputMatrix.ncols);
	      //! Update:
	      __obj__localMatrix__defaultMetrics.matrix[local_col_id][__obj__localMatrix__defaultMetrics_cntRows] = __localInputMatrix.matrix[row_id_global][local_col_id];
	    } else {
	      assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.ncols);
	      /* assert(row_id_global < __localInputMatrix.ncols); */
	      /* assert(local_col_id < __localInputMatrix.nrows) */;
	      assert(row_id_global < __localInputMatrix.ncols);
	      assert(local_col_id < __localInputMatrix.nrows);
	      //! Update:
	      __obj__localMatrix__defaultMetrics.matrix[local_col_id][__obj__localMatrix__defaultMetrics_cntRows] = __localInputMatrix.matrix[local_col_id][row_id_global];
	    }
#endif //! (__internalDataUpdate__transposeOrder)
#endif //! endif(__localConfig__isToApplyLogics == 2)
	    local_col_id++; //! ie, increment
#else 
	    //fprintf(stderr, "row_id_global=%u, at %s:%d\n", row_id_global, __FILE__, __LINE__);
	    if(row_id_global == 0) {	      
	      matrixDimensionsCnt__cols++; //! ie, as we then assume 'this part' is used to update the columns.	      
	    }
	    
#endif //! #endif( (__localConfig__isToApplyLogics == 1) || (__localConfig__isToApplyLogics == 2)
	  }
	}
	assert(row_id == row_id_global);
	row_id_global++;
#if(__localConfig__isToApplyLogics == 2)
	__obj__localMatrix__defaultMetrics_cntRows++;
#endif
#if(__localConfig__isToApplyLogics == 1)
	row_id_subsetSetBased__cnt++;
#elif(__localConfig__isToApplyLogics == 0)
	matrixDimensionsCnt__subsetSetBased__nrows++;
	matrixDimensionsCnt__subsetSet__default_cntRows++;
#endif
      }
  } else {
#if(__localConfig__isToApplyLogics == 0) 
  //! Then idenitfy the number of coutns ... where a "for-loopp" is sued to simplify any use of 'future filters'.
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  for(uint data_1 = 0; data_1 < cnt_data_nrows; data_1++) {
    for(uint data_2 = 0; data_2 < cnt_data_ncols; data_2++) {
      matrixDimensionsCnt__cols++;
    }
  }
#endif
 }
//! ----------------------------------------
// kt_matrix__distancematrix__stub__case_b.c:49
//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
if( (enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__all) || 
    (enum_clusterResultAsInput != e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__setBased)) 
    { //! Evalaute for the matrix-scores: using the 'default' strategies for idnetifying simliary between different clusters:
      
      for(uint matrix_id = 0; matrix_id < cnt_matricesToEvaluate; matrix_id++) {
#if(__localConfig__isToApplyLogics == 1)
	const t_float def_val = 0;
	t_float **matrixOf_coOccurence = allocate_2d_list_float(cnt_vertices, cnt_vertices, def_val);
	// TODO[performance]: consider makionig [below] more efficent
	for(uint i = 0; i < cnt_vertices; i++) {
	  for(uint k = 0; k < cnt_vertices; k++) {	    
	    if(matrix_id == 0) {matrixOf_coOccurence[i][k] = distMatrix_0[i][k];}
	    else if(matrix_id == 1) {matrixOf_coOccurence[i][k] = distMatrix_1[i][k];}
	    else if(matrix_id == 2) {matrixOf_coOccurence[i][k] = distMatrix_2[i][k];}
	    else {assert(false);} //! ie, as we then need to add support 'for this case'.
	  }
	}
#endif //! endif(__localConfig__isToApplyLogics == 1)
	//! ----------------------------------------
	//!
	//! Iterate through the different matrix-based measures:
	//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
	for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) {
#if(__localConfig__isToApplyLogics == 1)
	  assert(row_id_global < obj_metricDataScores.nrows);
#endif //! endif(__localConfig__isToApplyLogics == 1)
	  //const uint row_id = row_id_global;
	  const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id;
	  if(isAMatrixBasedMeasure__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp)) {    
	    //! Then the metric is a 'matrix-based' metric:
	    //! --------------------
	    //! 
	    { //! Compute for: 'default':
#if(__localConfig__isToApplyLogics == 1)
	      const char *typeOf_distanceInference = "default";
	      s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();	      
	      assert(row_id_global < obj_metricDataScores.nrows);
	      //! Apply the locics:
#include "measure_kt_matrix_cmpCluster__stub__curated__metricVS_synteticData__matrixBased.c"
	      //! -----------------
#elif(__localConfig__isToApplyLogics == 2) //! then we build subset of the matrices:
	      uint local_col_id = 0;
	      for(uint data_1 = 0; data_1 < cnt_data_nrows; data_1++) {
		for(uint data_2 = 0; data_2 < cnt_data_ncols; data_2++) {
#ifndef __internalDataUpdate__transposeOrder
		    assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.nrows);
		    assert(__localInputMatrix.matrix);
		    assert(row_id_global < __localInputMatrix.nrows);
		    assert(local_col_id < __localInputMatrix.ncols);
		    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
		    //! Update:
		    __obj__localMatrix__defaultMetrics.matrix[__obj__localMatrix__defaultMetrics_cntRows][local_col_id] = __localInputMatrix.matrix[row_id_global][local_col_id];
		    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
#else
		    if(__internalDataUpdate__transposeOrder__inputIsAlsoTransposed == false) {
		      assert(row_id_global < __localInputMatrix.ncols);
		      assert(local_col_id < __localInputMatrix.nrows);
		      //! Update:
		      __obj__localMatrix__defaultMetrics.matrix[local_col_id][__obj__localMatrix__defaultMetrics_cntRows] = __localInputMatrix.matrix[row_id_global][local_col_id];
		    } else {
		      assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.ncols);
		      assert(__localInputMatrix.matrix);
		      assert(row_id_global < __localInputMatrix.ncols);
		      assert(local_col_id < __localInputMatrix.nrows);
		      assert(__obj__localMatrix__defaultMetrics_cntRows < __obj__localMatrix__defaultMetrics.ncols);
		      assert(local_col_id < __obj__localMatrix__defaultMetrics.nrows);
		      //! Update:
		      __obj__localMatrix__defaultMetrics.matrix[local_col_id][__obj__localMatrix__defaultMetrics_cntRows] = __localInputMatrix.matrix[local_col_id][row_id_global];
		    }
#endif //! (__internalDataUpdate__transposeOrder)
		    //__obj__localMatrix__defaultMetrics_cntRows++;
		}
	      }
	      //! --
	      __obj__localMatrix__defaultMetrics_cntRows++; //! ie, upda teh the count.
#endif //! endif(__localConfig__isToApplyLogics == 2)

	      //! Update the count:
	      row_id_global++;
#if(__localConfig__isToApplyLogics == 0)	      
	      matrixDimensionsCnt__subsetSet__default_cntRows++;
#endif
	   } 
	    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
	    //! -------------------------------------------------------------------
	    //!
	    //! Iterate through the corrleation-metircs:
	    if( (enum_clusterResultAsInput != e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased) ) {
	      if( (enum_clusterResultAsInput != e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__subset_extremeCases) ) {
		for(uint cluster_cmp_id = 0; cluster_cmp_id < e_kt_clusterComparison_undef; cluster_cmp_id++) {

		  e_kt_clusterComparison_t metric_clusterCmp_between = (e_kt_clusterComparison_t)cluster_cmp_id;
		  assert(metric_clusterCmp_between != e_kt_clusterComparison_undef);
		  //! Iterate through the different cluster-comparison-metrics: the post-process-types:
		  for(uint metric_cateogry_index = 0; metric_cateogry_index <= (uint)e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3; metric_cateogry_index++) {
		    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
		    //! Get configruations:
		    const e_kt_correlationMetric_initCategory_t metric_cateogry = (e_kt_correlationMetric_initCategory_t)enum_clusterResultAsInput;
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
		    if(metric_cateogry == e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O0) {continue;} //! ie, as we asusme the clsuter-gnerated input-matix is Not an adjcency-matrix.
#endif
		    const s_kt_correlationMetric_t metric_spec = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(metric_cateogry);
		    //		      setTo_empty__andReturn__s_kt_correlationMetric_t(metric_cateogry);
		    const e_kt_correlationFunction_t dist_metric_id = metric_spec.metric_id;
		    const e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = metric_spec.typeOf_correlationPreStep;
#if(__localConfig__isToApplyLogics == 1)
		    //!
		    //! Genearte an ifnroamtive string: 
		    assert(row_id_global < obj_metricDataScores.nrows);
		    char typeOf_distanceInference[2000] = {'\0'};
		    sprintf(typeOf_distanceInference, "%s::%s::%s",
			    getStringOf__e_kt_clusterComparison_t(metric_clusterCmp_between),
			    get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(dist_metricCorrType),
			    get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id));
		    
		    assert(strlen(typeOf_distanceInference));
		    //! Configure how we are to infer the metrics:
		    s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
		    //! state that we are to use a complex comparsin-strategy:
		    config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true;
		    //! Set the tyep of clsuterign-comparison-strategy to use:
		    config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between;
		      //! Set the type of corrleaiton-metirc to be used:
		    config_intraCluster.metric_complexClusterCmp_obj.typeOf_correlationPreStep = dist_metricCorrType;
		    config_intraCluster.metric_complexClusterCmp_obj.metric_id = dist_metric_id;
		    //! Apply the locics:
#include "measure_kt_matrix_cmpCluster__stub__curated__metricVS_synteticData__matrixBased.c"
		    //! -----------------
#endif //! endif(__localConfig__isToApplyLogics == 1)
		    //! Update the count:
		    row_id_global++;		    
		  }
		}
		//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
	      } else if( (enum_clusterResultAsInput != e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__all) ) {
		for(uint cluster_cmp_id = 0; cluster_cmp_id < e_kt_clusterComparison_undef; cluster_cmp_id++) {
		  e_kt_clusterComparison_t metric_clusterCmp_between = (e_kt_clusterComparison_t)cluster_cmp_id;
		  assert(metric_clusterCmp_between != e_kt_clusterComparison_undef);
		  //! Iterate through the different cluster-comparison-metrics: the post-process-types:
		  for(uint dist_enum_preStep = 0; dist_enum_preStep <= e_kt_categoryOf_correaltionPreStep_none; dist_enum_preStep++) {
		    for(uint clusterMetric_index = 0; clusterMetric_index < (uint)e_kt_correlationFunction_undef; clusterMetric_index++) {
		      //! Iterate through the different cluster-comparison-metrics: the corrleation-emtrics themself:
		      e_kt_correlationFunction_t dist_metric_id = (e_kt_correlationFunction_t)clusterMetric_index; e_kt_categoryOf_correaltionPreStep_t dist_metricCorrType = (e_kt_categoryOf_correaltionPreStep_t)dist_enum_preStep;
		      if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
			 (dist_metric_id == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly)
			 || 
#endif
			 describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(dist_metric_id)
			 || isTo_use_directScore__e_kt_correlationFunction(dist_metric_id)
			 ) {continue;} //! ie, as we then assuem 'this is Not of interest'.
		      //#if( (__localConfig__isToApplyLogics == 1) || (__localConfig__isToApplyLogics == 2) )
#if(__localConfig__isToApplyLogics == 1)
		      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
		      //!
		      //! Genearte an ifnroamtive string: 
		      assert(row_id_global < obj_metricDataScores.nrows);
		      char typeOf_distanceInference[2000] = {'\0'}; //memset(typeOf_distanceInference, 2000, '\0');
		      sprintf(typeOf_distanceInference, "%s::%s::%s",
			      getStringOf__e_kt_clusterComparison_t(metric_clusterCmp_between),
			      get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(dist_metricCorrType),
			      get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id));
	      
		      assert(strlen(typeOf_distanceInference));
		      //! Configure how we are to infer the metrics:
		      s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
		      //! state that we are to use a complex comparsin-strategy:
		      config_intraCluster.metric_complexClusterCmp_obj_isTo_use = true; 		  config_intraCluster.metric_complexClusterCmp_vertexCmp_isTo_use = true;
		      //! Set the tyep of clsuterign-comparison-strategy to use:
		      config_intraCluster.metric_complexClusterCmp_clusterMethod = metric_clusterCmp_between;
		      //! Set the type of corrleaiton-metirc to be used:
		      config_intraCluster.metric_complexClusterCmp_obj.typeOf_correlationPreStep = dist_metricCorrType;
		      config_intraCluster.metric_complexClusterCmp_obj.metric_id = dist_metric_id;
		      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
		      //! Apply the locics:
#include "measure_kt_matrix_cmpCluster__stub__curated__metricVS_synteticData__matrixBased.c"
		      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
		      //! -----------------
#endif //! endif(__localConfig__isToApplyLogics == 1)
		      //! Update the count:
		      row_id_global++;
		    }
		  }
		}
	      } else {
		assert(false); //! ie, as we then need to 'add support for this case'.
	      }
	    }
	  }
	  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
	}
	//! ------------------------------------------------------
#if(__localConfig__isToApplyLogics == 1)
	//! De-alllocate:
	assert(matrixOf_coOccurence); free_2d_list_float(&matrixOf_coOccurence, cnt_vertices); matrixOf_coOccurence = NULL;
#endif //! endif(__localConfig__isToApplyLogics == 1)
      }
    }

//! ***********************************************
//!
//! Reset macro-parameters:
#undef __localConfig__isToApplyLogics
