//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"
#include "tut_wrapper_image_segment.h"
#include "hp_image_segment.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "export_ppm.h"
#include <climits>
#include <cfloat>
//#endif
//#include "hp_distance.h"
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
//#include "aux_sysCalls.h"
//#include "kt_aux_matrix_norm.h" //! which hold the data-nraomizoant-enums.
//#include "hp_image_segment.h"

/**
   FIXME:
   .): write a main-wrapper
   .): "hp_image_segment": test: pasring: txt-file
   .): "hp_image_segment": test: ppm-file
   .): "hp_image_segment": test: bmp-file-parsing 
   .): "hp_image_segment": test: 
   .): "hp_image_segment": test: 
   .): "hp_image_segment": test: 
   .): 
   .): 
   .): image-back-forth: blur an image
   .): image-back-forth: normalise an image
   .): image-back-forth: infer the hue of an image
   .): image-back-forth: 
   .): image-back-forth: 
   // FIXME: move below to a new tut: 
   // FIXME: move below to a new tut: 
   // FIXME: move below to a new tut: "tut_image_4_normalize.c"
   // FIXME: move below to a new tut: "tut_image__.c"
   // FIXME: move below to a new tut: "tut_image_1_helloWorld.c" Foxued on semgntaiton. Turns a matrix inoit a collecion of triplets, and then apply a clustering-algorithm, before a new iamge is contstructed based on the average score.
   .): image-back-forth: clustering: k-means: average RGB-values
   .): image-back-forth: clustering: k-means: 
   .): image-back-forth: clustering: k-means: 
   .): image-back-forth: clustering: 
   .): image-back-forth: 
   .): 
   .): 
   .): 
 **/

/**
   @brief examplfies strategies for basic operaitons on images.
   @author Ole Kristian Ekseth (oekseth, 06. nov. 2020).
   @remarks
   --- exemplifes strategies for image-parsing, and simplfied lgocis to validat coissiten in export-lgocis with import-logics    
   @remarks
   -- compile: g++ -I.  -g -O0  -mssse3   -L .  tut_image_1_helloWorld.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_imgClust; ./tut_imgClust
**/
int main() { // const int array_cnt, char **array) {
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif

  { //! A simple refrheshment of clustering: uses "tut_disjointCluster_2_algComparisonSingleAlg.c" as a template:
    const uint nrows = 10;     const uint ncols = 10; 
    s_kt_matrix_t mat_data = initAndReturn__s_kt_matrix(nrows, ncols);
    //const t_float score_notMatch = T_FLOAT_MAX;  const t_float score_match = 1;
    const t_float score_notMatch = 100000;  const t_float score_match = 1;
    //  const t_float score_notMatch = 100;  const t_float score_match = 1;
    //  const t_float score_notMatch = 100;  const t_float score_match = 1;
    //! Set defult score:
    s_kt_matrix_t mat_adj = initAndReturn__s_kt_matrix(nrows, ncols);  
    if(false) {
      const char *file_name = "data/local_downloaded/0_iris.data.hpLysis.tsv";
      // const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set").
      import__s_kt_matrix_t(&mat_adj, file_name);
    } else {
      for(uint i = 0; i < mat_data.nrows; i++) {
	for(uint k = 0; k < mat_data.ncols; k++) {mat_data.matrix[i][k] = score_notMatch;}}
      //! Add relationships:
#define __addRel(head, tail) ({assert(head < nrows); assert(tail < ncols); mat_data.matrix[head][tail] = score_match;})
      //! Not: for illustrative purposes we avoid inlcuding vertex=0 in any of the clusters.
      //  __addRel(1, 2);
      __addRel(1, 5);
      __addRel(5, 6);
      //__addRel(2, 5);
      __addRel(6, 7);
      __addRel(7, 8);
      __addRel(8, 9);
      //!
      //! A seperate sub-tree:
      __addRel(2, 3);
      __addRel(3, 4);
      //  __addRel(3, 6);
      // __addRel(2, 4);
      __addRel(4, 5);  
#undef __addRel    
      
      //! Construct an ajdency-matrix:
      for(uint i = 0; i < mat_data.nrows; i++) {
	for(uint j = 0; j < mat_data.ncols; j++) {
	  if(mat_data.matrix[i][j] < score_notMatch) {
	    mat_adj.matrix[i][j] = mat_data.matrix[i][j];}}}
    }

    //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
    const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG; //! ie, k-means
    const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
    obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.
    //obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/true; 
    //obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
    //obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
    //obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
    if(false) {
      obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
      obj_hp.config.corrMetric_prior_use = true;
    }
    
    //! 
    //! Apply logics:
    // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
    const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_adj, /*nclusters=*/3, /*npass=*/100);
    //const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_adj, /*nclusters=*/UINT_MAX, /*npass=*/100);
    //const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
    //! Get the CCM-score:
    if(false) {
      t_float ccm_score = T_FLOAT_MAX; //!< hold the CCM-score.
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
    }
    //! Get the clusters:
    s_kt_list_1d_uint_t clusters = getClusters__hpLysis_api(&obj_hp);
    assert(clusters.list_size > 0);
    //printf("list:size:%u\n", clusters.list_size);
    //! De-allocte:
    free__s_hpLysis_api_t(&obj_hp);
    printf("list:size:%u\n", clusters.list_size);
    for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
      if(clusters.list[row_id] != UINT_MAX) {
	fprintf(stderr, "%u\t", clusters.list[row_id]);
      } else {
	fprintf(stderr, "-\t");
      }	   
    }
    //printf("list:size:%u\n", clusters.list_size);
    //! De-allocate:   
    free__s_kt_list_1d_uint_t(&clusters);
    free__s_kt_matrix(&mat_data);
    free__s_kt_matrix(&mat_adj);
  }


  // assert(false); // FIXME: remvoe this.

  const char *result_file = "tmp-1.ppm";
  //!
  { //!
    //! Step: Construct a sytentic matrix:
    //! Note: we here construct a clsuter-shape. This to provide the stepping-stoens for future, and mroe compelx, examples
    const uint nrows = 100;
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/1000, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/0);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/20);    
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/40,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/60);    
    //if(true)
    { //! Step: Export to PPM:
      //s_kt_matrix_t matrix = &(obj_shape.matrix); //! ie, a shallow copy.
      ppm_image_t *o_image = exportTo_image(obj_shape.matrix.matrix, obj_shape.matrix.nrows, obj_shape.matrix.ncols, NULL, NULL);
      ppm_image_write(result_file, o_image);
      //! De-allcote:
      ppm_image_finalize(o_image);
      //! De-allocate:
      // free__s_hp_image_segment_t(&obj_image);
    }
    //!
    //! De-alloctate:
    free__s_hp_clusterShapes_t(&obj_shape);
  }
  //! ------------------------------------------------------
  //! Step: Import from PPM:
  //const char *input_file = result_file;
  const char *input_file = "tmp-blur.ppm";
  s_hp_image_segment_t o_img = ppm_import__s_hp_image_segment_t(input_file);      
  //! ------------------------------------------------------
  
  // FIXME: adjsut this tut to .... cluster data ... 
  // FIXME: manage to get below meitc-epxlreoaitons less crude, ie, mroe ifned-grained
  
  
  // assert(false); // FIXME: complete!  
  
  
  //if(false)  // FIXME: remove
  { //! Apply Clustering
    const char *new_file_base = "tmp-after-clustering";
    // FIXME: new tut ... "tut_image_3_cluster.c" ....  explore effects of different clustering-algoriithms <-- what permtuatiosn to explore?
    
      
    //!
    //! Pre: turn iamge into a matrix: for simplity, we use the RGB-values:
    const uint nrows = o_img.mat_RGB[0].nrows;
    const uint ncols = o_img.mat_RGB[0].ncols;
    assert(nrows > 0);
    assert(ncols> 0);      
    s_kt_matrix_t mat_rgb = initAndReturn__s_kt_matrix(nrows * ncols, /*cols=*/3);
    loint global_row_id = 0;
    for (uint row_id = 0; row_id < o_img.mat_RGB[0].nrows; row_id++)  {
      for (uint col_id = 0; col_id < o_img.mat_RGB[0].ncols; col_id++, global_row_id++)    {
	for(uint i = 0; i < 3; i++) {
	  mat_rgb.matrix[global_row_id][i] =  o_img.mat_RGB[i].matrix[row_id][col_id];
	}
      }
    }

    // FIXME: move below to a new funciton ... then fork 'this code' otuside into a new "tut_image_exploreImpactOfConfigurations_simMetric.c"
    // FIXME: pdate bleow ... apply for all sim-metrics
    
    //!
    //for(uint sim_id = 0; sim_id < e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; sim_id++) {
      //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_id);
      //setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_id);
    { //! Step: apply clustering:
      //! Note: in [below] we examplify different permtauaions wrt. HCA: algorithm-calls:
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kruskal_hca;
      const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG; //! ie, k-means
      //const bool inputMatrix__isAnAdjecencyMatrix = false;
      s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);	
      // A (Symmetric) adjacency matrix?
      //obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
      obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.
      //const uint cnt_Calls_max = 3; 
      const uint arg_npass = 100;
      printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kruskal_fixed;
      for(uint ncluster_ind = 2; ncluster_ind < 10; ncluster_ind++) {
	// ----------------
	//!
	//! Step: Cluster:
	const uint nclusters = ncluster_ind;
	const bool is_also_ok = cluster__hpLysis_api (
						      &obj_hp, clusterAlg, &mat_rgb,
						      /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						      );
	assert(is_also_ok);
	// ---------------------------------
	//!
	//! Step: adjust the input-data:	 
	s_hp_image_segment_t o_img_adjusted = init__s_hp_image_segment_t();

	//!
	{ //! Step: logics: adjsutments of the image:
	  s_kt_list_1d_uint_t list_clusters = getClusters__hpLysis_api(&obj_hp);
	  if(list_clusters.list_size == 0) {
	    //! 
	    //! De-allocate:
	    free__s_hpLysis_api_t(&obj_hp);
	    printf("(Info)\t clusters:%u\tNot any clsuters were detected: drops performing operation, at %s:%d\n", nclusters, __FILE__, __LINE__);
	    continue;
	  } 
	  //!
	  //! Step: pre: a copy of the "o_img"
	  o_img_adjusted = copy__s_hp_image_segment_t(&o_img);
	  
	  assert(list_clusters.list_size > 0); //! ie, as we expect at least some mbers to be allcoated to clusters
	  s_kt_list_1d_float_t map_rgbColor[3];
	  s_kt_list_1d_uint_t  map_rgbColor_cnt[3];
	  assert(mat_rgb.ncols == 3); //! ie, what we expect
	  for(uint i = 0; i < 3; i++) {
	    map_rgbColor[i]     = init__s_kt_list_1d_float_t(nclusters);
	    map_rgbColor_cnt[i] = init__s_kt_list_1d_uint_t(nclusters);	      
	    for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
	      map_rgbColor[i].list[cluster_id] = 0.0; //! ie, default value.
	      map_rgbColor_cnt[i].list[cluster_id] = 0; //! ie, zero occurences
	    }
	  }
	  assert(list_clusters.list_size <= mat_rgb.nrows);
	  for(uint cell_id = 0; cell_id < list_clusters.list_size; cell_id++) {
	    if(list_clusters.list[cell_id] != UINT_MAX) {
	      const uint cluster_id = list_clusters.list[cell_id];
	      for(uint i = 0; i < 3; i++) {
		map_rgbColor[i].list[cluster_id] += mat_rgb.matrix[cell_id][i];
		map_rgbColor_cnt[i].list[cluster_id]++; //! ie, the count-incrementor
	      }
	    }
	  }
	  //!
	  //! Step: average the scores:
	  for(uint i = 0; i < 3; i++) {
	    for(uint cluster_id = 0; cluster_id < nclusters; cluster_id++) {
	      if(map_rgbColor_cnt[i].list[cluster_id] != 0) 
		if(map_rgbColor[i].list[cluster_id]  != 0) {
		  map_rgbColor[i].list[cluster_id] = map_rgbColor[i].list[cluster_id]  / (t_float)map_rgbColor[i].list[cluster_id] ;
		}
	    }
	  }
	  //!
	  //!	    
	  { //! Step: Adjust the scores:
	    loint global_row_id = 0;
	    for (uint row_id = 0; row_id < o_img.mat_RGB[0].nrows; row_id++)  {
	      for (uint col_id = 0; col_id < o_img.mat_RGB[0].ncols; col_id++, global_row_id++)    {
		for(uint i = 0; i < 3; i++) {
		  uint cluster_id = UINT_MAX;
		  if(global_row_id < list_clusters.list_size) { cluster_id = list_clusters.list[global_row_id];}
		  //!
		  if(cluster_id != UINT_MAX) {
		    o_img_adjusted.mat_RGB[i].matrix[row_id][col_id] = map_rgbColor[i].list[cluster_id];
		  }
		}
	      }
	    }
	  }	    
	  //! 
	  //! De-allocate:
	  free__s_kt_list_1d_uint_t(&list_clusters);
	  for(uint i = 0; i < 3; i++) {
	    free__s_kt_list_1d_float_t(&(map_rgbColor[i]));
	    free__s_kt_list_1d_uint_t(&(map_rgbColor_cnt[i]));
	  }
	}

	// ----------------
	//!
	//! Step: Export:
	char new_file[10000];
	sprintf(new_file, "%s-%s-ncluster%u.ppm", new_file_base, get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg), nclusters);
	export__s_hp_image_segment_t(&o_img_adjusted, new_file);   //! ie, the call.
	//! 
	//! De-allocate:
	free__s_hp_image_segment_t(&o_img_adjusted);
	free__s_hpLysis_api_t(&obj_hp);
	// ----------------
      }
      //! 
      //! De-allocate:
      free__s_kt_matrix(&mat_rgb);	
    }

  }
  //!  
  //! De-allcoate:      
  free__s_hp_image_segment_t(&o_img);      
      
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif  
}
