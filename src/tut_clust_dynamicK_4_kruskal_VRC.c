#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"

/**
   @brief examplifies how to use dynaimc Kruskal-HCA clustering in combianton with kMeans and "Dunns" (oekseth, 06. mar. 2017).
   @author Ole Kristian Ekseth (logics) (oekseth, janchris, 06. jan. 2017).
   @remarks we examplfiy: (a) how to comptue k-emans clsuters; (b) how to use the internal export-rotuiens for data-export; (c) how to programtailally access the cluster-resutls. 
   @remarks Extends "tut_hca.c", "tut_mine.c" and "tut_buildSimilarityMatrix.c". Demonstrates features wrt.:
   @remarks examplify the call/use of a CCM-based post-processing (a funciton fdefined in our "hpLysis_api.h") to 'maximize' the k-means-split of a HCA-result
**/
int main() 
#endif
{
  /* { */
  /*   int i = 10; */
  /*   int tmp = -i-1; */
  /*   printf("d=%d, at %s:%d\n", tmp, __FILE__, __LINE__); */
  /*   assert(false); */
  /* } */

  const bool inputMatrix__isAnAdjecencyMatrix = true;
  const uint cnt_Calls_max = 3; const uint arg_npass = 1000;
  const char *result_file = "test_tut_kCluster.tsv";
  //const uint nrows = 10;     const uint ncols = 10; 
  //const uint nrows = 3;     const uint ncols = 3; 
  //! Note: in [below] we examplify different permtauaions wrt. HCA: algorithm-calls:
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kruskal_hca;
  const uint nclusters = 1; //! ie, to Not use 

  s_kt_matrix_fileReadTuning_t obj_tuneRead = initAndReturn__s_kt_matrix_fileReadTuning_t();
  s_kt_matrix_t obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(/*input_file=*/"data/local_downloaded/54_Holzinger.csv.hpLysis.tsv", obj_tuneRead);
  assert(obj_matrixInput.nrows > 0);
  assert(obj_matrixInput.ncols > 0);

  /* Have a go at the clustering */

  //! Open teh file :
  FILE *file_out = fopen(result_file, "wb");
  //FILE *file_out = stdout;
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}

  //!
  //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:
  for(uint cnt_Calls = 0; cnt_Calls < cnt_Calls_max; cnt_Calls++) {
    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);

    // (Symmetric) adjacency matrix                                                                                                                            
    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
    obj_hp.config.corrMetric_prior_use = true; //! ie, use data-as-is.
    if(false) {
      //! Specify that we are to use the k-means++-implmeantion defined/used in the "fast_Kmeans" software:
      obj_hp.randomConfig.config_init.typeOf_randomNess = e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss; //! where the latter enum is define d in our "math_generateDistribution.h"
    }
    if(true) { //! Then we use the CCM-based dynamic-k-clksuter-approach (oekseth, 06. mar. 2017):
      obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true; //! ie, a 'sfeguard' to future-code-changes: whiel latter is by defualt set to true, explcitly calling latter will ensure that 'any future changes to API' will be 'detected' by 'this call'.
      obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC; //! ie, ot use the Silhouette enum.
    }

    //!
    //! Apply clustering:
     printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
     const bool is_also_ok = cluster__hpLysis_api (
						   &obj_hp, clusterAlg, &obj_matrixInput,
						   /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						   );
     assert(is_also_ok);
     
     //! 
    //! Export results to "file_out":
     assert(file_out);
     const s_kt_correlationMetric_t corrMetric_prior = obj_hp.config.corrMetric_prior;
     fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
     fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
     /* bool is_ok_e = export__hpLysis_api(&obj_hp, /\*stringOf_file=*\/NULL, /\*file_out=*\/file_out, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV); */
     /* assert(is_ok_e); */
     //! Export the vertex-clsuterId-memberships:
    bool is_ok_e = export__hpLysis_api(&obj_hp, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &obj_matrixInput);
    assert(is_ok_e);

    //! 
    //! 
    { //! Export: traverse teh generated result-matrix-object and write out the results:    
      //! Note: the [”elow] calls implictly 'test' the/our "cuttree(..)" proceudre (first described in the work of "clsuter.c"):
      const uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
      assert(vertex_clusterId);
      const uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;
      assert(cnt_vertex > 0);
      fprintf(file_out, "clusterMemberships=[");
      uint max_cnt = 0;
      for(uint i = 0; i < cnt_vertex; i++) {fprintf(file_out, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
      fprintf(file_out, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);
    }
    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&obj_hp);	    
  }


  //!
  //! Close the file and the amtrix:
  assert(file_out); 
  if(file_out != stdout) {fclose(file_out); file_out = NULL;}
  free__s_kt_matrix(&obj_matrixInput);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
