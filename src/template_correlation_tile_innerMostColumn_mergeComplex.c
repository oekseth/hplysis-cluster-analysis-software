#if(__template_correlation_tile_innerMostColumn_mergeComplex__config__useSSE == 1)
//printf("\t\t\tSSE:(merge-results-associated-to-vertex, at %s:%d\n", __FILE__, __LINE__);
	    { //! Start: scalaer udpate from complex object:
	      //! start: complex. ------------------------------------------------------------------------------------------------------------------------------------
	      const t_float sum_numerator = VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_sse.numerator);
	      const t_float sum_denumerator = VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_sse.denumerator);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_numerator != T_FLOAT_MIN);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_denumerator != T_FLOAT_MIN);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_numerator != T_FLOAT_MAX);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_denumerator != T_FLOAT_MAX);
	      //! Update:
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) ) // ----------
	      objLocalComplex_nonSSE.numerator   += sum_numerator;
	      objLocalComplex_nonSSE.denumerator += sum_denumerator;
#elif( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1) )
	      objLocalComplex_nonSSE.numerator   =  macro_min(objLocalComplex_nonSSE.numerator,   sum_numerator);
	      objLocalComplex_nonSSE.denumerator =  macro_min(objLocalComplex_nonSSE.denumerator, sum_denumerator);
#else //! Then find min-value:
	      objLocalComplex_nonSSE.numerator   =  macro_max(objLocalComplex_nonSSE.numerator,   sum_numerator);
	      objLocalComplex_nonSSE.denumerator =  macro_max(objLocalComplex_nonSSE.denumerator, sum_denumerator);
#endif // -----------------------------------------------------------------------------

#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 2) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex)
	      //! Then we udpate the result-object wrt. the nuermators and denumerator:
	      const t_float sum_denumeratorLimitedTo_row1 = VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_sse.denumeratorLimitedTo_row1);
	      const t_float sum_denumeratorLimitedTo_row2 = VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_sse.denumeratorLimitedTo_row2);
	      const t_float sum_numeratorLimitedTo_row_1and2 = VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_sse.denumeratorLimitedTo_row_1and2);
	      const t_float sum_denumeratorLimitedTo_row_1and2 = VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_sse.denumeratorLimitedTo_row_1and2);
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) ) // ------------(start:if-max):
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row1 += sum_denumeratorLimitedTo_row1;
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row2 += sum_denumeratorLimitedTo_row2;
	      //! -
	      objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2 += sum_numeratorLimitedTo_row_1and2;
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2 += sum_denumeratorLimitedTo_row_1and2;
	      //! -
#elif( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1) )
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row1 = macro_min(objLocalComplex_nonSSE.denumeratorLimitedTo_row1, sum_denumeratorLimitedTo_row1);
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row2 = macro_min(objLocalComplex_nonSSE.denumeratorLimitedTo_row2,  sum_denumeratorLimitedTo_row2);
	      //! -
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2 += macro_min(objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2, sum_denumeratorLimitedTo_row_1and2);
	      objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2 += macro_min(objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2, sum_numeratorLimitedTo_row_1and2);
#else //! then we 'extend' above to udpate wrt. 'more' variables:
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row1 = macro_max(objLocalComplex_nonSSE.denumeratorLimitedTo_row1, sum_denumeratorLimitedTo_row1);
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row2 = macro_max(objLocalComplex_nonSSE.denumeratorLimitedTo_row2,  sum_denumeratorLimitedTo_row2);
	      //! -
	      objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2 += macro_max(objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2, sum_denumeratorLimitedTo_row_1and2);
	      objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2 += macro_max(objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2, sum_numeratorLimitedTo_row_1and2);
	      //! -
#endif // ----------------------------------------------------------------------------- (end:if-max)
#endif //! endif("1) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator").
	      //! end: complex. ------------------------------------------------------------------------------------------------------------------------------------
	    } //! end: scalar update from compelx object: **************************************
#else //! ------- then we merge for a non-SSE ojbect:
const t_float sum_numerator = objLocalComplex_nonSSE.numerator;
const t_float sum_denumerator = objLocalComplex_nonSSE.denumerator;
if( 
   (sum_numerator != T_FLOAT_MAX) & (sum_numerator != T_FLOAT_MIN) 
   & (sum_denumerator != T_FLOAT_MAX) & (sum_denumerator != T_FLOAT_MIN) 
    ) {
	      // printf("\t\t\tnon-sse:(merge-results-associated-to-vertex for pair[%u][%u], at %s:%d\n", global_index1, global_index2, __FILE__, __LINE__);
	      //assert_possibleOverhead(global_index1 < nrows);
	      //assert_possibleOverhead(global_index2 < iterationIndex_2);
	      //! ***************************************************
	      //! start: non-complex. ------------------------------------------------------------------------------------------------------------------------------------


	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_numerator != T_FLOAT_MIN);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_denumerator != T_FLOAT_MIN);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_numerator != T_FLOAT_MAX);
	      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(sum_denumerator != T_FLOAT_MAX);
	      //! Update:
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) ) // ----------
	      complexResult->matrixOf_result_numerator[global_index1][global_index2]   += sum_numerator;
	      complexResult->matrixOf_result_denumerator[global_index1][global_index2] += sum_denumerator;
#elif( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1) )
	      complexResult->matrixOf_result_numerator[global_index1][global_index2]   =  macro_min(complexResult->matrixOf_result_numerator[global_index1][global_index2],   sum_numerator);
	      complexResult->matrixOf_result_denumerator[global_index1][global_index2] =  macro_min(complexResult->matrixOf_result_denumerator[global_index1][global_index2], sum_denumerator);
#else //! Then find min-value:
	      complexResult->matrixOf_result_numerator[global_index1][global_index2]   =  macro_max(complexResult->matrixOf_result_numerator[global_index1][global_index2],   sum_numerator);
	      complexResult->matrixOf_result_denumerator[global_index1][global_index2] =  macro_max(complexResult->matrixOf_result_denumerator[global_index1][global_index2], sum_denumerator);
#endif // -----------------------------------------------------------------------------

#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 2) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex)
	      //! Then we udpate the result-object wrt. the nuermators and denumerator:
	      const t_float sum_denumeratorLimitedTo_row1 = objLocalComplex_nonSSE.denumeratorLimitedTo_row1;
	      const t_float sum_denumeratorLimitedTo_row2 = objLocalComplex_nonSSE.denumeratorLimitedTo_row2;
	      //! -
	      const t_float sum_denumeratorLimitedTo_row_1and2 = objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2;
	      const t_float sum_numeratorLimitedTo_row_1and2 = objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2;
	      //! -
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) ) // --------------------(start:if-max):
	      complexResult->matrixOf_result_denumeratorLimitedTo_row1[global_index1][global_index2] += sum_denumeratorLimitedTo_row1;
	      complexResult->matrixOf_result_denumeratorLimitedTo_row2[global_index1][global_index2] += sum_denumeratorLimitedTo_row2;
	      //! -
	      complexResult->matrixOf_result_denumeratorLimitedTo_row_1and2[global_index1][global_index2] += sum_denumeratorLimitedTo_row_1and2;
	      //! -
#elif( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1) )
	      complexResult->matrixOf_result_numeratorLimitedTo_row1[global_index1][global_index2] = macro_min(complexResult->matrixOf_result_numeratorLimitedTo_row1[global_index1][global_index2], sum_numeratorLimitedTo_row1);
	      complexResult->matrixOf_result_denumeratorLimitedTo_row2[global_index1][global_index2] = macro_min(complexResult->matrixOf_result_denumeratorLimitedTo_row2[global_index1][global_index2],  sum_denumeratorLimitedTo_row2);
	      //! -
	      complexResult->matrixOf_result_numeratorLimitedTo_row_1and2[global_index1][global_index2] += macro_min(complexResult->matrixOf_result_numeratorLimitedTo_row_1and2[global_index1][global_index2], sum_numeratorLimitedTo_row_1and2);
	      complexResult->matrixOf_result_denumeratorLimitedTo_row_1and2[global_index1][global_index2] += macro_min(complexResult->matrixOf_result_denumeratorLimitedTo_row_1and2[global_index1][global_index2], sum_denumeratorLimitedTo_row_1and2);
#else //! then we 'extend' above to udpate wrt. 'more' variables:
	      complexResult->matrixOf_result_numeratorLimitedTo_row1[global_index1][global_index2] = macro_max(complexResult->matrixOf_result_numeratorLimitedTo_row1[global_index1][global_index2], sum_numeratorLimitedTo_row1);
	      complexResult->matrixOf_result_denumeratorLimitedTo_row2[global_index1][global_index2] = macro_max(complexResult->matrixOf_result_denumeratorLimitedTo_row2[global_index1][global_index2],  sum_denumeratorLimitedTo_row2);
	      //! -
	      complexResult->matrixOf_result_numeratorLimitedTo_row_1and2[global_index1][global_index2] += macro_max(complexResult->matrixOf_result_numeratorLimitedTo_row_1and2[global_index1][global_index2], sum_numeratorLimitedTo_row_1and2);
	      complexResult->matrixOf_result_denumeratorLimitedTo_row_1and2[global_index1][global_index2] += macro_max(complexResult->matrixOf_result_denumeratorLimitedTo_row_1and2[global_index1][global_index2], sum_denumeratorLimitedTo_row_1and2);
	      //! -
#endif // ----------------------------------------------------------------------------- (end:if-max)

#endif //! endif("e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex").	      



	    }
//! ***************************************************
//! end: non-complex. ------------------------------------------------------------------------------------------------------------------------------------
#endif

	      //! **********************************************************
#undef __template_correlation_tile_innerMostColumn_mergeComplex__config__useSSE
