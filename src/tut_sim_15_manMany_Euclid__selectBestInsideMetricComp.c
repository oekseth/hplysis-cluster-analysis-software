#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "kt_list_2d.h" //! which among others define the "s_kt_list_2d_kvPair_t" struct.

static void __tut_sim_15_manMany_Euclid__selectBestInsideMetricComp__validateMerge() {
    const uint nrows = 3;
    s_kt_list_2d_kvPair_t obj_2d = init__s_kt_list_2d_kvPair_t(nrows, 10);
    assert(obj_2d.list_size >= nrows);
    //!
    //! Construc tthe amtrix to insert:
    const uint chunk_size = 2; assert(chunk_size <= nrows);
    t_float listOf_inlineResults__[chunk_size][chunk_size] = 
      {
	{ 3, 2},
	{ 6, 4}
      };
    t_float empty_0 = 0;
    t_float **listOf_inlineResults = allocate_2d_list_float(chunk_size, chunk_size, empty_0);
    for(uint i = 0; i < chunk_size; i++) { for(uint k = 0; k < chunk_size; k++) { listOf_inlineResults[i][k] = listOf_inlineResults__[i][k];}}
    {
      //!
      //! Add the block:   
      insertBlockOf_scores__sorted__kt_list_2d(&obj_2d, (t_float**)listOf_inlineResults, /*pos-head=*/0, /*pos-tail=*/0, /*chunk_nrows*/chunk_size, /*chunk_cols=*/chunk_size, 
					       /*isLast_columnForBlock=*/false, UINT_MAX, UINT_MAX, T_FLOAT_MAX, false, false);
      //!
      //! Validate the inserted results:
      //! Note: result is expected to Not be sorted, ie, as the inserted block is Not the last:
      for(uint i = 0; i < chunk_size; i++) {
	assert(obj_2d.list[i].list);
	//§! we expec thtat reuslt-filtering-selciotn ahs Not been appleid/taken-place.
	assert(obj_2d.list[i].list_size >= chunk_size);
	printf("[%u]\t current_pos=%u, at %s:%d\n", i, obj_2d.list[i].current_pos,
	       __FILE__, __LINE__);
	assert(obj_2d.list[i].current_pos == chunk_size);
	for(uint k = 0; k < chunk_size; k++) {
	  const uint tail_id = obj_2d.list[i].list[k].head;
	  const t_float score = obj_2d.list[i].list[k].tail;
	  assert(tail_id == k);
	  assert(score == listOf_inlineResults[i][k]);
	}
      }
    }
    { //! Add a post-sorting step, and then apply sorting:
      const uint pos_tail = chunk_size;
      //!
      //! Add the block:   
      insertBlockOf_scores__sorted__kt_list_2d(&obj_2d, (t_float**)listOf_inlineResults, /*pos-head=*/0, /*pos-tail=*/pos_tail, /*chunk_nrows*/chunk_size, /*chunk_cols=*/chunk_size, 
					       /*isLast_columnForBlock=*/true, UINT_MAX, UINT_MAX, T_FLOAT_MAX, false, false);
      //!
      //! Validate the inserted results:
      //! Note: result is expected to Not be sorted, ie, as the inserted block is Not the last:
      
      for(uint i = 0; i < chunk_size; i++) {
	assert(obj_2d.list[i].list);
	const t_float sortedPairs_row1[4][2] = /*row=0*/{{3, 2.000000},{1, 2.000000},{0, 3.000000},{2, 3.000000},};
	const t_float sortedPairs_row2[4][2] = /*row=1*/{{3, 4.000000},{1, 4.000000},{0, 6.000000},{2, 6.000000},};



	//§! we expec thtat reuslt-filtering-selciotn ahs Not been appleid/taken-place.
	assert(obj_2d.list[i].list_size >= (chunk_size*2));
	printf("[%u]\t current_pos=%u, at %s:%d\n", i, obj_2d.list[i].current_pos,
	       __FILE__, __LINE__);
	assert(obj_2d.list[i].current_pos == (chunk_size*2));
	printf("/*row=%u*/{", i);
	for(uint k = 0; k < obj_2d.list[i].current_pos; k++) {
	  const uint tail_id = obj_2d.list[i].list[k].head;
	  const t_float score = obj_2d.list[i].list[k].tail;
	  printf("{%u, %f},", tail_id, score);
	  if(i == 0) {
	    assert(tail_id == (uint)sortedPairs_row1[k][0]);
	    assert(score == (t_float)sortedPairs_row1[k][1]);
	  } else if(i == 1) {
	    assert(tail_id == (uint)sortedPairs_row2[k][0]);
	    assert(score == (t_float)sortedPairs_row2[k][1]);
	  } else {
	    assert(false); //! ie, as we then need adding epxlit support for 'this'.
	  }
	  /* assert(tail_id == k); */
	  /* assert(score == listOf_inlineResults[i][k]); */
	}
	printf("}, \n");
      }
    }
    //!
    //! De-allocate:
    free__s_kt_list_2d_kvPair_t(&obj_2d);
    free_2d_list_float(&listOf_inlineResults, chunk_size); listOf_inlineResults = NULL;
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief demonstrates how a similartiy-metirc may be used to describe the simlairty between two vectors. 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks general: use logics in our "hp_distance" to comptue the simalrit between two vectors.
   @remarks simliartiy: comptue the sim-matrix="Euclid" between two vectors using our "hp_distance.h" API 
   @remarks "hp_distance": we call the "apply__rows_hp_distance(..)" function;
   @remarks generic: we use our "initSampleOfValues__rand__s_kt_matrix_base_t(..)" (defined in our "kt_matrix_base.h") to simplify the writign of 'statnized' code-examples: simplifeis the 'constuciton' of two uqniue vectors to compare.
   @remarks generalizaiton(example): to simplify the understanidng and re-use of our appraoch we have strucutred our "tut_sim_*" examples to use the same 'example-seutp', ie, where differneces is found wrt. the API-calls in to our "hp_distance", and wrt. different "s_kt_correlationMetric" calls.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#else
  void tut_sim_15_manMany_Euclid__selectBestInsideMetricComp()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif
  if(false) { //! Validate sub-step of our "insertBlockOf_scores__sorted__kt_list_2d(...)"
    __tut_sim_15_manMany_Euclid__selectBestInsideMetricComp__validateMerge();
  }


  const uint nrows = 5; const uint ncols = 100; 

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
  //! Specify/define the simlairty-metric:
  s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"

  //! Allocate two vectors, ie, matrices with demsinos [1, ncols]:
  //! Note: for simplicty we use [”elow] fucntion, defined in our "kt_matrix_base.h", ie, to set the vectors to default valeus, therby reducing the nubmer of code-lines in this use-case-example; in the latter randomenss is used, ie, for which we expect the vectors to be different.
  assert(nrows > 0);   assert(ncols > 0); //! ie, as we otherise have a poinbtelss' call
  s_kt_matrix_base vec_1 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  s_kt_matrix_base vec_2 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  if(true) { //! then we 'tindocue' a pattern wrt. the inferences, ie, to get a 'score' of interest/meianign.
    for(uint i = 0; i < nrows; i++) {
      for(uint k = 0; k < ncols; k++) {
	vec_1.matrix[i][k] = (t_float)i;
	//	vec_1.matrix[i][k] = (t_float)k;
	vec_2.matrix[i][k] = (t_float)k;
      }
    }
  }
  /* t_float *row_1 = vec_1.matrix[0]; //! ie, the first row. */
  /* t_float *row_2 = vec_2.matrix[0]; //! ie, the first row. */
  //!
  //! Apply logics:
  t_float scalar_result = T_FLOAT_MAX; //! ie, an 'empty values:

  const uint config_cntMax_afterEach = 2;
  s_kt_list_2d_kvPair_t obj_result = apply__2d__storeSparse__hp_distance(obj_metric, &vec_1, &vec_2, /*config_cntMin_afterEach=*/1, config_cntMax_afterEach, T_FLOAT_MAX, /*isTo_ignore_zeroScores=*/true);
  /* const bool is_ok = apply__rows_hp_distance(obj_metric, row_1, row_2, ncols, &scalar_result); */
  /* assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'. */
  //!
  //! Write out the result:
  // printf("cnt-rows=%u, at %s:%d\n", obj_result.list_size, __FILE__, __LINE__);
  { //! Then we write otut he scores:
    for(uint row_id = 0; row_id < obj_result.list_size; row_id++) {
      const uint chunk_size = obj_result.list[row_id].list_size;
      //printf("[%u]: chunk_size=%u, at %s:%d\n", row_id, chunk_size, __FILE__, __LINE__);
      uint cnt_ofInterest = 0;
      for(uint k = 0; k < chunk_size; k++) {
	const uint tail_id = obj_result.list[row_id].list[k].head;
	const t_float score = obj_result.list[row_id].list[k].tail;
	if(isOf_interest(score)) 	{
	  printf("\t[%u][%u]=%f, at %s:%d\n", row_id, tail_id, score, __FILE__, __LINE__);
	  cnt_ofInterest++;
	}
      }
      assert(cnt_ofInterest <= config_cntMax_afterEach);
    }

  }
  //  fprintf(stdout, "# Result: sim-score=\"%f\", at [%s]:%s:%d\n", scalar_result, __FUNCTION__, __FILE__, __LINE__);

  //!
  //! De-allocate:
  free__s_kt_matrix_base_t(&vec_1);
  free__s_kt_matrix_base_t(&vec_2);
  free__s_kt_list_2d_kvPair_t(&obj_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
 
