{
  //! --------- new measurement:
  { //! Comapre teh transpsoe-operation to 'simple' data-updates, for which we perofmrance-tests logics in our "mask_api.c"
    { const char *stringOf_measureText = "mask-function-naive(1): covnert-to-binary(..): evaluate time-cost of sequential value-update where one if-clause is used (eg, to be used as a 'point of reference' wrt. the branching-cost of memory-cache-misses)";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      construct_binaryMatrix__mask_api(matrix_float, size_of_array, size_of_array);
      //! Update the result-container:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    }
    //! ---------
    { //! Min-max for no-mask:
      { const char *stringOf_measureText = "mask-function-naive(2.a): get-min-max(..): in contrast to 'mask-function-naive(1)' we use to if-branches";
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_undef;
	t_float scalar_min = 0, scalar_max = 0;
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	get_minMax__allValuesOf_interest_mask_api(matrix_float, size_of_array, size_of_array, &scalar_min, &scalar_max, self); 
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
      //! ---------
      { const char *stringOf_measureText = "mask-function-SSE(3.a): get-min-max(..): in contrast to 'mask-function-naive(2.a)' we combien an 'naive' appraoch with an SSE-approach";
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_fast;
	t_float scalar_min = 0, scalar_max = 0;
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	get_minMax__allValuesOf_interest_mask_api(matrix_float, size_of_array, size_of_array, &scalar_min, &scalar_max, self); 
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
    }
    //! -----------
    { //! Min-max for explicit-mask:
      { const char *stringOf_measureText = "mask-function-naive(2.b): get-min-max--explicit-mask(..): in contrast to 'mask-function-naive(1)' we use to if-branches";
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_undef;
	t_float scalar_min = 0, scalar_max = 0; bool scalar_hasDecimals = false;
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	get_minMax__mask_api(matrix_float, matrix_char, size_of_array, size_of_array, &scalar_min, &scalar_max, &scalar_hasDecimals, self); 
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
      //! ---------
      { const char *stringOf_measureText = "mask-function-SSE(3.b): get-min-max--explicit-mask(..): in contrast to 'mask-function-naive(2.b)' we combien an 'naive' appraoch with an SSE-approach";
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_fast;
	t_float scalar_min = 0, scalar_max = 0; bool scalar_hasDecimals = false;
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	get_minMax__mask_api(matrix_float, matrix_char, size_of_array, size_of_array, &scalar_min, &scalar_max, &scalar_hasDecimals, self); 
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
    }
    //! ---------
    //! ---------
    { 
      {
	const char *stringOf_measureText = "mask-function-nonSSE: convert-uint-to-char(..): examplifeis ordered access of two different matrices";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	// 
	char **tem_list = convert_booleanMatrix_toCharMatrix__mask_api(size_of_array, size_of_array, matrix_uint, /*isTo_use_continousSTripsOf_memory=*/false);
	assert(tem_list);
	//printf("return mask=%p, at %s:%d\n", tmp, __FILE__, __LINE__);
	free_1d_list_char(&(tem_list[0]));
	free_generic_type_2d(tem_list);
	//free_1d_list_char(&tmp[0]);
	//free_2d_list_char(&tmp);
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
      //! ---
      {
	const char *stringOf_measureText = "mask-function-nonSSE--SSE: convert-uint-to-char(..): examplifeis ordered access of two different matrices";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	// 
	char **tmp_list = convert_booleanMatrix_toCharMatrix__mask_api(size_of_array, size_of_array, matrix_uint, /*isTo_use_continousSTripsOf_memory=*/true);
	assert(tmp_list);	free_2d_list_char(&tmp_list, size_of_array);
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
    }
    //! ---------
    { //! mask-type=uint:
      { const char *stringOf_measureText = "mask-function-nonSSE: convert-uint-to-implicitMask(..) w/explicit-mask-type=uint";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	// 
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_undef;
	//self.isTo_use_sseFast = false;
	applyImplicitMask_fromUintMask__updateData__mask_api(matrix_float, matrix_uint, size_of_array, size_of_array, self);
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
      //! --
      { const char *stringOf_measureText = "mask-function-nonSSE--SSE: convert-uint-to-implicitMask(..) w/explicit-mask-type=uint";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	// 
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_fast;
	//self.isTo_use_sseFast = true;
	applyImplicitMask_fromUintMask__updateData__mask_api(matrix_float, matrix_uint, size_of_array, size_of_array, self);
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
    }
    { //! mask-type=char:
      { const char *stringOf_measureText = "mask-function-nonSSE: convert-uint-to-implicitMask(..) w/explicit-mask-type=uint";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	// 
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_undef;
	//self.isTo_use_sseFast = false;
	applyImplicitMask__updateData__mask_api(matrix_float, matrix_char, size_of_array, size_of_array, self);
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
      //! --
      { const char *stringOf_measureText = "mask-function-nonSSE--SSE: convert-uint-to-implicitMask(..) w/explicit-mask-type=uint";
	//! -------------------------------
	//! Start the clock:
	start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	//!
	// 
	s_mask_api_config_t self;  setTo_empty__s_mask_api_config(&self);
	self.typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_fast;
	//self.isTo_use_sseFast = true;
	applyImplicitMask__updateData__mask_api(matrix_float, matrix_char, size_of_array, size_of_array, self);
	//! Update the result-container:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
      }
    }
  }
}
