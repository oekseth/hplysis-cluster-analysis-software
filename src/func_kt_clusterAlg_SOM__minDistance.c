 
{
  assert_possibleOverhead(arrOf_result_tmp);
  assert_possibleOverhead(transpose == false);


  //t_float closest = 0; assert(false); // TODO: inlcude a erptmtuation of [”elow]
  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, dummymask, weights, transpose, masksAre_used, nrows, ncolumns);

  //assert(iobject != iybest); //! ie, to avoid a self-comparison
  t_float closest = T_FLOAT_MAX; //! ie, as we are itnerested in fidnign the min-value.
  if(iobject != iybest) {
    
    /* assert_possibleOverhead(iobject < nygrid); */
    assert_possibleOverhead(iybest < nygrid);

    closest = kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, 
			       ncolumns,
			       //ncolumns, 
			       data, celldata[ixbest], /*index1=*/iobject, /*index2=*/iybest, config_metric, metric_each,
			       (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
  }
  // float closest = metric(ndata,data, celldata[ixbest], mask,dummymask,weights, iobject, iybest,transpose); //! ie, compare the 'current index' (in the data-set) to the 'best index' (in the 'result-set').
  // TODO: use a modified all-gaisnta-ll-comptuation wrt. [”elow].
     
  s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t obj_spec; init_ToMin__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(&obj_spec);
  obj_spec.return_distance = closest; obj_spec.index2_best = iybest;


  //config_metric.s_inlinePostProcess = (void*)&obj_spec;       config_metric.typeOf_postProcessing_afterCorrelation = 
  // s_allAgainstAll_config_t config_metric; 
  init__s_allAgainstAll_config(&config_metric, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/nygrid, /*typeOf_postProcessing_afterCorrelation=*/e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min, /*s_inlinePostProcess=*/(void*)&obj_spec, masksAre_used, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible); set_metaMatrix(&config_metric, /*isTo_init=*/false, mask, dummymask, weights, transpose, masksAre_used, nrows, ncolumns);	

  //printf("nrows=%u, ncols=%u, iterationIndex_2=%u, at %s:%d\n", nrows, ncolumns, nygrid, __FILE__, __LINE__);

  for(uint ix = 0; ix < nxgrid; ix++) {

    // TODO: validate taht we in our caller (eg, in peoformance-test) 'try to avodit eh sue of makss' ... ie, idneitfy the types of metrics 'where setting the vlaue to '0' resutls in correct results

    //printf("\n\n\n Comptue the one-to-many distance, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    //
    kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, 
			  //nygrid, //
			  nrows, 
			  ncolumns, data, celldata[ix], /*index1=*/iobject,  &config_metric, 
			  (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute,
			  /*arrOf_result=*/arrOf_result_tmp,
			  metric_oneToMany);


	
    // obj_spec.return_distance = closest; obj_spec.return_distance = closest;

    /* for(uint iy = 0; iy < nygrid; iy++) { */
    /*   //! Note: [”elow] "celldata" matrix contains/holds the 'random values' adjusted by the "standard deviation" (for each of the 'vecotrs' assicated to each [nx][ny]) */
    /*   // TODO: try to describe an appraoch where we manage to optmize 'this for-loop' ... eg, to 'merge' the data[iobject] iterations and 'translate into a dense matrix' the celldata[0... nxgrid] matrices for "0 ... iy ... nygrid" */
    /*   const t_float distance = 0; assert(false); // TODO: inlcude a erptmtuation of [”elow] */
    /*   // const float distance =  (distMatrix_preCopmtued) ? distMatrix_preCopmtued[ix][iobject][iy] : metric(ndata,data,celldata[ix], mask,dummymask,weights,iobject,iy,transpose); */
	
    const t_float distance = obj_spec.return_distance; const uint iy = obj_spec.index2_best;
    assert(iy != UINT_MAX); //! ie, as 'such' would otheriwse indicate that a min-distnace was not fodun, ie, idnicative of a code-erorr (oekseth, 06. otk. 201&)
    if(distance < closest) {
      ixbest = ix;
      iybest = iy;
      closest = distance;
    }
    obj_spec.return_distance = closest; obj_spec.return_distance = closest;

    assert(iy <= nygrid); //! ie, what we expect given our [ªbove] use of the "iterationIndex_2" parameter.
    /* } */
  }
}
