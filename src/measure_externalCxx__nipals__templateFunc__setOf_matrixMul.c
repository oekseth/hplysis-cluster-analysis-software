{  

  //! Define a vairlabe to hodl teh local comaprison-resutls:
  {
    //!
    //! First copmtue for the naive case:
    const char *stringOf_measureText = "naive strategy";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow
#define __macro_includeTransposedInCall 0
#define __macro_includeTransposedInCall__andUseTiling 0
#define __use_Result_to_updateTableComparisonResults 1
    //! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
    //! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
  }
  {
    //!
    //! First copmtue for the naive case:
    const char *stringOf_measureText = "ideal case: result[i][j] += counter*counter, ie, in-signficant memory-cache-misses";
#define __macro_funcName_toCall test_matrixMult_dummIteration_idealMemAllocCase
#define __macro_includeTransposedInCall 0
#define __macro_includeTransposedInCall__andUseTiling 0
#define __use_Result_to_updateTableComparisonResults 0
    //! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
    //! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
  }
  //! -----------------------------------------------------------
  { //! Compute for non-tiling:
    { const char *stringOf_measureText = "naive strategy: improved using transposed matrix-input";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 0
#define __use_Result_to_updateTableComparisonResults 0
      //! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
      //! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
    }
    { const char *stringOf_measureText = "naive strategy: improved using transposed matrix-input and the '__restrict__' compiler-keyword";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 0
#define __use_Result_to_updateTableComparisonResults 0
      //! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
      //! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
    }
    { const char *stringOf_measureText = "naive strategy: improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 0
#define __use_Result_to_updateTableComparisonResults 0
      //! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
      //! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
    }
  }
  //! -----------------------------------------------------------
  { //! Compute for tiling:
    { const uint SM = 8; //! '''''''''''''''''''''''''''''''''''''''''' WWJD:      
      { const char *stringOf_measureText = "tiling(SM=8): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=8): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=8): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE: a slower version where we update the 'sum' in each-for-loop";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
    }
    { const uint SM = 32; //! '''''''''''''''''''''''''''''''''''''''''' WWJD:      
      { const char *stringOf_measureText = "tiling(SM=32): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=32): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=32): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE: a slower version where we update the 'sum' in each-for-loop";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
    }
    { const uint SM = 64; //! '''''''''''''''''''''''''''''''''''''''''' WWJD:      
      { const char *stringOf_measureText = "tiling(SM=64): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=64): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=64): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE: a slower version where we update the 'sum' in each-for-loop";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
    }
    { const uint SM = 128; //! '''''''''''''''''''''''''''''''''''''''''' WWJD:      
      { const char *stringOf_measureText = "tiling(SM=128): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=128): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
      { const char *stringOf_measureText = "tiling(SM=128): improved using transposed matrix-input and the '__restrict__' compiler-keyword combined with SSE: a slower version where we update the 'sum' in each-for-loop";
#define __macro_funcName_toCall kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate
#define __macro_includeTransposedInCall 1
#define __macro_includeTransposedInCall__andUseTiling 1
#define __use_Result_to_updateTableComparisonResults 0
	//! Start the perofmrance-comparison:
#include "measure_externalCxx__nipals__templateFunc__matrixMul.c"
	//! Clear the macro-varialbes:
#undef __macro_funcName_toCall
#undef __macro_includeTransposedInCall
#undef __macro_includeTransposedInCall__andUseTiling
#undef __use_Result_to_updateTableComparisonResults
      }
    }
  }
}
