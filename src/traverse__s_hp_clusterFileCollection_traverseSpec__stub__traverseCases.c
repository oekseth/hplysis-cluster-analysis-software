assert(obj_clusterAlg.list != NULL);     assert(obj_clusterAlg.list_size > 0); //! ie, as we expect the object to have been set.
//! 
//! Iterate throguh the clustering-algorithms:
for(uint cluster_index = 0; cluster_index < obj_clusterAlg.list_size; cluster_index++) {
  const e_hpLysis_clusterAlg_t config__clusterAlg = obj_clusterAlg.list[cluster_index];
  if(self->config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "-- clusterAlg: \"%s\"\n", get_stringOf__e_hpLysis_clusterAlg_t(config__clusterAlg));}

  //assert(false); // FIXME: remove.
  //continue; // FIXME: remove.
  const bool config__typeOfOps__onlySimMetric = (config__clusterAlg == e_hpLysis_clusterAlg_undef);
  if( (config__typeOfOps__onlySimMetric == false) && (obj_matrixInput.nrows < config_kMeans_nclusters) ) {
    fprintf(stderr, "!!(odd-usage)\t For File=\"%s\" seems like you have requested an evlauation where nrows(%u) !> cnt_clusters(%u). If this is what you intended, then please ingore this observaiton. Oderwhise we suggesting invesitgiating the latter. OBservation at [%s]:%s:%d\n", stringOf_tagSample, obj_matrixInput.nrows, config_kMeans_nclusters, __FUNCTION__, __FILE__, __LINE__);
    fprintf(stderr, "!!\t File=\"%s\" resulsts in emtpy matrix: pelase validate your input-file. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
    continue;
  }
  //!
  //! Handle different cases wrt. simlairty-metrics:
  assert(objMetrics__prior.list_size >= 0);
  for(uint met_prior = 0; met_prior < objMetrics__prior.list_size; met_prior++) {
    //printf("\t\t- config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__);
    //! Set the metric:
    //printf("met_prior=%u <%u, at %s:%d\n", met_prior, objMetrics__prior.list_size,  __FILE__, __LINE__);
    const s_kt_correlationMetric_t corrMetric_prior = objMetrics__prior.list[met_prior]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
    const bool config_corrMetric_prior_use = (corrMetric_prior.metric_id != e_kt_correlationFunction_undef);
    if(self->config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "\t-- metric-prior: \"%s\":\"%s\"\n",  get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id));}
    for(uint met_prior__inside = 0; met_prior__inside < objMetrics__inside.list_size; met_prior__inside++) {
      //printf("\t\t-- config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__);
      //! Spefiy the string-object used to idneity the metic: 	  
      char str_suff[2000] = {'\0'}; 
      //const char *str_suff = 
      getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_id, cnt_matrices_exported++, data_obj, cluster_index, met_prior, met_prior__inside, str_suff);
      //char *str_suff = getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_obj, config__clusterAlg, met_prior, met_prior__inside);
      //char str_suff[2000] = {'\0'}; sprintf(str_suff, "%s.a_%u.c_%u_%u", data_obj.tag, cluster_index, met_prior, met_prior__inside);
      //! Set the metric:
      const s_kt_correlationMetric_t corrMetric_inside = objMetrics__inside.list[met_prior__inside]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
      //s_kt_correlationMetric_t corrMetric_inside; init__s_kt_correlationMetric_t(&corrMetric_inside, config_dist_metric_id__insideClustering, config_dist_metricCorrType);
      if(self->config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "\t\t-- metric-inside: \"%s\":\"%s\"\n",  get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_inside.typeOf_correlationPreStep), get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id));}
      //!
/* #else */
/*       const char *stringOf_tagSample = mapOf_realLife[data_id]; */
/*       const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
/*       if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name */
/*       const uint config_kMeans_nclusters = config_kMeans_nclusters__defaultAssumption; */
/*       assert(config_kMeans_nclusters > 0); */
/*       const bool config__clusterConfig__inputMatrix__isAnAdjecencyMatrix = config__clusterConfig__inputMatrix__isAnAdjecencyMatrix__default; */
/*       // const bool config__typeOfOps__onlySimMetric = false; */
/*       //! */
/*       //! Set the simliarty/correlation matrix: */
/*       s_kt_correlationMetric_t corrMetric_prior; init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType); */
/*       s_kt_correlationMetric_t corrMetric_inside; init__s_kt_correlationMetric_t(&corrMetric_inside, config_dist_metric_id__insideClustering, config_dist_metricCorrType); */
/* #endif */

      //printf("val='%f', at %s:%d\n", obj_matrixInput.matrix[0][0], __FILE__, __LINE__);
      //assert(obj_matrixInput.matrix[0][0] == 0); // FIXME: remvoe
      //!
      //! Configure:
      s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
      hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = config__clusterConfig__inputMatrix__isAnAdjecencyMatrix;
      /* if(config__clusterConfig__inputMatrix__isAnAdjecencyMatrix) { */
      /* 	fprintf(stderr, "(note)\t matrix is expected to be an ajdcnecy-matrix, at %s:%d\n", __FILE__, __LINE__); */
      /* 	assert(false); // FIXME: remove */
      /* } */
      hp_config.config.clusterConfig.performance__testImapcactOf__slowPerformance = self->config__performance__testImapcactOf__slowPerformance;
      hp_config.config.corrMetric_prior_use = config_corrMetric_prior_use;
      if(self->pathTo_localFolder_storingEachSimMetric) {
	//! then we export the result-matrix:
	char str_local[2000] = {'\0'}; sprintf(str_local, "%s%s.index_%u_%s.tsv", self->pathTo_localFolder_storingEachSimMetric, "simMatrix", data_id, str_suff);
	hp_config.stringOfResultPrefix__exportCorr__prior = str_local;
      }
      s_kt_matrix_t *matrix_cmp = NULL;     if(config_corrMetric_prior_use == false) {matrix_cmp = &obj_matrixInput;} //! which is then used when evlauating the cluster-emmberships.
      if(corrMetric_prior.metric_id != e_kt_correlationFunction_undef) {
	hp_config.config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:    
	//hp_config.corrMetric_prior = corrMetric_prior;
      } else {assert(hp_config.config.corrMetric_prior_use == false);}
      //! 
      if(corrMetric_inside.metric_id != e_kt_correlationFunction_undef) {
	hp_config.config.corrMetric_insideClustering = corrMetric_inside;
      } else {assert(config__typeOfOps__onlySimMetric == true);}
      assert(hp_config.config.corrMetric_prior.metric_id != e_kt_correlationFunction_undef);
      assert(hp_config.config.corrMetric_insideClustering.metric_id != e_kt_correlationFunction_undef);
      // printf("at %s:%d\n",  __FILE__, __LINE__); continue; // FIXME: remove.



      //!
      //! Start the timer: 
      start_time_measurement(); //! defined in our "measure_base.h"
      //!
      //! Call our hpLysis:	  
      if(config__typeOfOps__onlySimMetric 
	 // TODO: cosndier making [”elow] optioanl wrt. k-means clsuteirng
	 || (obj_matrixInput.nrows <= config_kMeans_nclusters) //! ie, where the altter would imply a 'poitnelss-clsutering' wrt. k-means.
	 ) { //! then we do Not apply clustering:
	//!
	//! Correlate: 
	const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
	assert(is_ok);
	//!
	//! Finalise the timing-oepraiton: 
	const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	// printf("simOnly[%u < %u==%u], at %s:%d\n", currentCounter__cluster_x_metPri__x__metInside, matResult__data_x_simAndAlg__time.ncols, obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__);
	//fprintf(stderr, "[%u < %u==%u], at %s:%d\n", currentCounter__cluster_x_metPri__x__metInside, matResult__data_x_simAndAlg__time.ncols, obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__);
	assert(currentCounter__cluster_x_metPri__x__metInside < matResult__data_x_simAndAlg__time.ncols);
	matResult__data_x_simAndAlg__time.matrix[data_id][currentCounter__cluster_x_metPri__x__metInside] = exex_time; 
#endif
	//! 
	//! -------------------------------------------------
      } else { //! then clsuteirng is applied:
	assert(obj_matrixInput.nrows > 0);
	assert(obj_matrixInput.ncols > 0);
	const bool is_ok = cluster__hpLysis_api(&hp_config, config__clusterAlg, &obj_matrixInput, config_kMeans_nclusters, self->config_arg_npass);
	assert(is_ok);
	//!
	//! Finalise the timing-oepraiton: 
	const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	//fprintf(stderr, "[%u < %u==%u], at %s:%d\n", currentCounter__cluster_x_metPri__x__metInside, matResult__data_x_simAndAlg__time.ncols, obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__);
	assert(currentCounter__cluster_x_metPri__x__metInside < matResult__data_x_simAndAlg__time.ncols);
	matResult__data_x_simAndAlg__time.matrix[data_id][currentCounter__cluster_x_metPri__x__metInside] = exex_time; //currentCounter__cluster_x_metPri__x__metInside++;
#endif
	    
	{ //! Export a masked verison of the simliarty-matrix: use t he clsuter-result to filter wrt. the socres:
	  //! Note: a use-case is to simplify visualizaiton of the cluster-results: to use an image-viwer to compare differences in applicaiton of cluster-results
	  char str__insert[2000] = {'\0'};  getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_id, cnt_matrices_exported++, data_obj, cluster_index, met_prior, met_prior__inside, str__insert);
	  assert(strlen(str__insert));
	  s_kt_matrix_t *matrix_sim = NULL;   if(hp_config.config.corrMetric_prior_use == false) {matrix_sim = &obj_matrixInput;}

	  { //! FORMAT="TSV":
	    char str_local__maskedMatrix[2000] = {'\0'};    sprintf(str_local__maskedMatrix, "%s%s.maskedMatrix.tsv", __dirPath, str__insert);
	    assert(strlen(str_local__maskedMatrix));
	    const e_hpLysis_export_formatOf_t exportFormat = e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_TSV; 
	    const bool is_ok = export__hpLysis_api(&hp_config, str_local__maskedMatrix, /*file_out=*/NULL, exportFormat, matrix_sim);
	    assert(is_ok);
	  }
	  { //! FORMAT="JS":
	    char str_local__maskedMatrix[2000] = {'\0'};    sprintf(str_local__maskedMatrix, "%s%s.maskedMatrix.js", __dirPath, str__insert);
	    assert(strlen(str_local__maskedMatrix));
	    const e_hpLysis_export_formatOf_t exportFormat = e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS; 
	    const bool is_ok = export__hpLysis_api(&hp_config, str_local__maskedMatrix, /*file_out=*/NULL, exportFormat, matrix_sim);
	    assert(is_ok);
	  }
	}
	//! 
	//! -------------------------------------------------
	{ //! For the 'summary' of STD-variations
	  fprintf(fileDesriptorOut__clusterResults, "\n//! Export cluster-results for data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	  { //! Export: traverse teh generated result-matrix-object and write out the results:    
	    const uint *vertex_clusterId = hp_config.obj_result_kMean.vertex_clusterId;
	    assert(vertex_clusterId);
	    const uint cnt_vertex = hp_config.obj_result_kMean.cnt_vertex;
	    uint *ranks = get_rank__uint__correlation_rank(cnt_vertex, vertex_clusterId); //! found in ourr "correlation_rank.h"
	    assert(ranks); //! ie, as we exepct at elast one cluster-id to have beens et
	    assert(cnt_vertex > 0);
	    fprintf(fileDesriptorOut__clusterResults, "//! clusterMemberships=[");
	    uint max_cnt = 0;
	    if(false) {
	      // fixme: figure out why [”elow] produce odd results.
	      for(uint i = 0; i < cnt_vertex; i++) { //! then we write out using the cluster-order:
		const uint vertex_id = ranks[i]; 
		assert(vertex_id < cnt_vertex);
		fprintf(fileDesriptorOut__clusterResults, "%u->%u, ", vertex_id, vertex_clusterId[vertex_id]); max_cnt = macro_max(vertex_clusterId[vertex_id], max_cnt);
	      }
	    } else {
	      for(uint i = 0; i < cnt_vertex; i++) {fprintf(fileDesriptorOut__clusterResults, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
	    }
	    if(ranks) {free_1d_list_uint(&ranks);}
	    fprintf(fileDesriptorOut__clusterResults, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);

#if( __Mi_c_mapOf_realLife__isStruct == 1)
	    //!
	    //! Apply different matrix-based CCMs:
	    assert(currentCounter__data_x_cluster_x_metPri__x__metInside < matResult__result_x_ccm.nrows);
	    assert(e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef == matResult__result_x_ccm.ncols);
	    //! Set the string:
	    set_stringConst__s_kt_matrix(&matResult__result_x_ccm, currentCounter__data_x_cluster_x_metPri__x__metInside, /*string=*/str_suff, /*addFor_column=*/false);
#endif
	    //!
	    //! Iterate: 
	    if(corrMetric_prior.metric_id != e_kt_correlationFunction_undef) {
	      for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
		const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
		const t_float score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&hp_config, ccm_type, /*mapOf_clusterIds=*/NULL, matrix_cmp);
		//!
		//! Write out the score:
		/* const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
		/* if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name */	
		if(score != T_FLOAT_MAX) {
		  fprintf(stdout, "#!\t\t ccm[\"%s\"]=%f \t %s\t data=\"%s\", sim-metric=\"%s\" and dist-enum-prestep='%u':\n", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), score, str_suff, stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
		}
#if( __Mi_c_mapOf_realLife__isStruct == 1)
		//!
		//! Update the result-object: 
		matResult__result_x_ccm.matrix[currentCounter__data_x_cluster_x_metPri__x__metInside][ccm_type_index] = score;
#endif
	      }		
	    } //! else we asusem the input-matrix is not set, ie, a simplfied asusmption
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	    //! Update counter:
	    currentCounter__data_x_cluster_x_metPri__x__metInside++;
#endif
	  }	  
	  // export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults__deviation, e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS, matrix_cmp);
	}
      }
      //! ------------------------------------------------------
      //!
      //! SitRep: analysis perofmred both for case(a) "build a simlairty-metirc", case(b) "apply clsuter directly", case(c) "build simlairty-emtric and then apply clustering":
      {//!
	//! Investgiate if the string is not set, and then update:
	assert(currentCounter__cluster_x_metPri__x__metInside < matResult__data_x_simAndAlg__time.ncols);
	const char *str_curr = getAndReturn_string__s_kt_matrix(&matResult__data_x_simAndAlg__time, currentCounter__cluster_x_metPri__x__metInside, /*addFor_column=*/true);
	if(!str_curr || !strlen(str_curr)) { 
	  char str__insert[2000] = {'\0'};  getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_id, cnt_matrices_exported++, data_obj, cluster_index, met_prior, met_prior__inside, str__insert);
	  set_string__s_kt_matrix(&matResult__data_x_simAndAlg__time, currentCounter__cluster_x_metPri__x__metInside, /*stringTo_add=*/str__insert, /*addFor_column=*/true);
	}
	currentCounter__cluster_x_metPri__x__metInside++;
      }
      if( (cluster_index == 0) && (met_prior__inside == 0) && config_corrMetric_prior_use) {
	//! Then we update the frquency-count-matrix:
	assert(hp_config.matrixResult__inputToClustering.nrows > 0);
	assert(matResult__scoreFrequency__local__currentPos < matResult__scoreFrequency__local.nrows); //! ie, as we expect the data-set to be insidej the range of allocated rows.
	//! Update 'the local matrix' with the value-frequency-deisitrubioin in "data" (oekseth, 06. feb. 2017):
	const bool is_ok = extractSubset_atIndex_type_frequencyMatrix__s_kt_matrix_t(&matResult__scoreFrequency__local, &(hp_config.matrixResult__inputToClustering), matResult__scoreFrequency__local__currentPos);
	assert(is_ok);
	{ //! Set the string-dienitfer for the matrix in question:
	  char str_local[2000] = {'\0'}; sprintf(str_local, "%u.m_%s.p_%u", data_id, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), corrMetric_prior.typeOf_correlationPreStep);
	  set_stringConst__s_kt_matrix(&matResult__scoreFrequency__local, matResult__scoreFrequency__local__currentPos, str_local, /*addFor_column=*/false);	  	  
	}
	//! ---- increment:
	matResult__scoreFrequency__local__currentPos++;
      }
      //!
      //! Update the result-file:
      { //! For the 'complete' simlairty-result-matrix:
	fprintf(fileDesriptorOut__clusterResults, "\n//! Export data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults, self->config_exportFormat, &obj_matrixInput);
      }
      { //! For the 'summary' of STD-variations
	fprintf(fileDesriptorOut__clusterResults, "\n//! Export data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults__deviation, e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_TSV, matrix_cmp);
      }
      if(
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	 (data_obj.alt_clusterSpec == e_hp_clusterFileCollection__goldClustersDefinedBy_rowString) || 
	 (data_obj.mapOf_vertexClusterId_size > 0)
#else
	 clusterConfig__rowNameIsSortedAndDescribesClusterId
#endif
	 ) { //! then we evlauate the cosnsitency usign different CCM-metrics:
	const uint empty_0 = 0;
	uint *mapOf_clusterIds = allocate_1d_list_uint(obj_matrixInput.nrows, empty_0);
	uint cluster_pos = 0;
	if(
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	   (data_obj.alt_clusterSpec == e_hp_clusterFileCollection__goldClustersDefinedBy_rowString) 
#else
	   true
#endif
	   ) {
	  assert(obj_matrixInput.nameOf_rows != NULL);
	  const char *str_curr = obj_matrixInput.nameOf_rows[0];
	  if(strstr(str_curr, "_BR_")) { //! then we asusem a pre-define d'order' is fiound in the clsuter-spec:
	    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
	      const char *str_curr = obj_matrixInput.nameOf_rows[i];
	      assert(str_curr); assert(strlen(str_curr));
	      const char *suff = strstr(str_curr, "_BR_");
	      assert(suff);  assert(strlen(suff));
	      suff += strlen("_BR_");
	      assert(suff);  assert(strlen(suff));
	      //! 
	      //! Se thte clsuter-idnex:
	      const int cluster_pos = atoi(suff);
	      mapOf_clusterIds[i] = (uint)cluster_pos;		
	    }
	  } else 
	    //if(false)  //! which 'may' be set if we are intestedied in investgiating the case where 'all vertices are in the same clsuter'.
	    {
	      mapOf_clusterIds[0] = cluster_pos;
	      for(uint i = 1; i < obj_matrixInput.nrows; i++) {
		const char *str_prev = obj_matrixInput.nameOf_rows[i-1];
		const char *str_curr = obj_matrixInput.nameOf_rows[i];
		assert(str_prev);	assert(str_curr);
		assert(strlen(str_prev));	assert(strlen(str_curr));
		//! Compare:
		if( (strlen(str_prev) != strlen(str_curr)) || (0 != strcmp(str_prev, str_curr)) ) {
		  cluster_pos++;
		}
		//! Update:
		//printf("vertex[%u] w/clusterId=%u, at %s:%d\n", i, cluster_pos, __FILE__, __LINE__);
		mapOf_clusterIds[i] = cluster_pos;
	      }
	    }
	} else {
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	  for(uint i = 0; i < data_obj.mapOf_vertexClusterId_size; i++) {
	    mapOf_clusterIds[i] = data_obj.mapOf_vertexClusterId[i];
	  }
#else
	  assert(false); //! ie, as we then need adding support f'ro this'.
#endif
	}
	//!
	if(corrMetric_prior.metric_id != e_kt_correlationFunction_undef) {
	  //! Apply different matrix-based CCMs:	
	  for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
	    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
	    const t_float score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&hp_config, ccm_type, mapOf_clusterIds, matrix_cmp);
	    //!
	    //! Write out the score:
	    /* const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
	    /* if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name */	
	    if(score != T_FLOAT_MAX) {
	      fprintf(stdout, "#!\t\t ccm[\"%s\"]=%f \t data=\"%s\", sim-metric=\"%s\" and dist-enum-prestep='%u':\n", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), score, stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	    }
	    //!
	    //! Update the result-object: 
	  }
	} //! else we asusem the input-matrix is not set, ie, a simplfied asusmption
	//!
	//! De-allcoate lcoally reserved memory:
	free_1d_list_uint(&mapOf_clusterIds);
      }

      //!
      //! De-allcoate lcoally reserved memory:
      free__s_hpLysis_api_t(&hp_config);
    }
    //#if( __Mi_c_mapOf_realLife__isStruct == 1)
  }
  /* printf("- de-allcoates: objMetrics__prior, at %s:%d\n", __FILE__, __LINE__); */
  /* printf("config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__); */
 }
