#include "hp_api_fileInputTight.h"
#include "hp_distance_wrapper.h"

//! Print the list of matrix-based CCMs (oekseth, 06. mar. 2017)
//! @remarks Print our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__ccm_matrixBased__hp_api_fileInputTight() {
  FILE *fileP = stdout;
  //! Note: to get the 'enum' from the string call "get_enumBasedOn_configuration(str)" (where latter is defined in our "kt_metric_aux.h").
  //! ----------------- 
  fprintf(fileP, "#\tOptions for \"ccm(matrix x gold)\" in hpLysis: [");
  for(uint i = 0; i < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
    const char *str = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
    if(str && strlen(str)) {
      fprintf(fileP, "\"%s\"", str); 
      if((i +1) != e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef) {
	fprintf(fileP, ", ");
      }
    }
  }
  fprintf(fileP, "], at %s:%d\n", __FILE__, __LINE__);
}
//! Print the list of "gold"x"gold" CCMs (oekseth, 06. mar. 2017)
//! @remarks pint our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__ccm_twoClusterResults__hp_api_fileInputTight()  {
  FILE *fileP = stdout;
  //! Note: to get the 'enum' from the string call "get_enumBasedOn_configuration(str)" (where latter is defined in our "kt_metric_aux.h").
  //! ----------------- 
  fprintf(fileP, "#\tOptions for \"ccm(gold x gold)\" in hpLysis: [");
  for(uint i = 0; i < e_kt_matrix_cmpCluster_metric_undef; i++) {
    const char *str = getStringOf__e_kt_matrix_cmpCluster_metric_t((e_kt_matrix_cmpCluster_metric_t)i);
    if(str && strlen(str)) {
      fprintf(fileP, "\"%s\"", str); 
      if((i +1) != e_hpLysis_clusterAlg_undef) {
	fprintf(fileP, ", ");
      }
    }
  }
  fprintf(fileP, "], at %s:%d\n", __FILE__, __LINE__);
}
//! Print the list of simliarty-metrics (oekseth, 06. mar. 2017)
//! @remarks pint our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__simMetric__hp_api_fileInputTight()  {
  FILE *fileP = stdout;
  //! Note: to get the 'enum' from the string call "get_enumBasedOn_configuration(str)" (where latter is defined in our "kt_metric_aux.h").
  //! ----------------- 
  fprintf(fileP, "#\tOptions for \"simMetrics\" in hpLysis: [");
  for(uint i = 0; i < e_kt_correlationFunction_undef; i++) {
    const char *str = get_stringOf_enum__e_kt_correlationFunction_t((e_kt_correlationFunction_t)i);
    if(str && strlen(str)) {
      fprintf(fileP, "\"%s\"", str); 
      if((i +1) != e_kt_correlationFunction_undef) {
	fprintf(fileP, ", ");
      }
    }
  }
  fprintf(fileP, "], at %s:%d\n", __FILE__, __LINE__);
}
//! Print the list of cluster-algorithms 'directly' supported through enums in our "hpLysis_api.h" (oekseth, 06. mar. 2017)
//! @remarks pint our the different string-options which our "hp_api_fileInputTight" supports wrt. 'string-options' (oekseth, 06. mar. 2017)
void printOptions__clustAlg__hp_api_fileInputTight()  {
  FILE *fileP = stdout;
  //! Note: to get the 'enum' from the string call "get_enumOf__e_hpLysis_clusterAlg_t(str)":
  //! ----------------- 
  fprintf(fileP, "#\tOptions for \"clustAlg\" in hpLysis: [");
  for(uint i = 0; i < e_hpLysis_clusterAlg_undef; i++) {
    const char *str = get_stringOf__e_hpLysis_clusterAlg_t((e_hpLysis_clusterAlg_t)i);
    if(str && strlen(str)) {
      fprintf(fileP, "\"%s\"", str); 
      if((i +1) != e_hpLysis_clusterAlg_undef) {
	fprintf(fileP, ", ");
      }
    }
  }
  fprintf(fileP, "], at %s:%d\n", __FILE__, __LINE__);
}

//! Print the comprensive list of enum-options supported by our "hp_api_fileInputTight.h" API (oekseth, 06. mar. 2017)
void printOptions__hp_api_fileInputTight() {
  printOptions__ccm_twoClusterResults__hp_api_fileInputTight();
  printOptions__simMetric__hp_api_fileInputTight();
  printOptions__clustAlg__hp_api_fileInputTight();
}


//! idnetifes the enum-id assicated to a given stringOf_enum (oekseth, 06. mar. 2017)
bool get_enumId_assicatedTo_string__hp_api_fileinputtight(const bool config__printAlternativesIfNotFound, const  e_hp_API_fileInputTight__logicType_t operation_type, const char *stringOf_enum, int *scalar_enum, bool *scalarBool_isFound) {
  //! ----------------------------------
  //!  
  //! What we expect:
  if(!stringOf_enum || !strlen(stringOf_enum) || !scalar_enum || !scalarBool_isFound) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  //! ----------------------------------
  //!  
  //! Apply lgoics: 
  *scalarBool_isFound = 0;
  bool __isApplied = false;
  if( (operation_type == e_hp_API_fileInputTight__logicType_undef) || (operation_type == e_hp_API_fileInputTight__logicType_sim) ) { //! Simliarty:
    e_kt_correlationFunction_t enum_id = get_enumBasedOn_configuration__kt_metric_aux(stringOf_enum, /*printAn_errorIf_notFound=*/false); //! deifned in our "kt_metric_aux.h".
    if(enum_id != e_kt_correlationFunction_undef) {
      __isApplied = true;
      *scalar_enum = (int)enum_id;
      *scalarBool_isFound = true;
    } else {
      if(config__printAlternativesIfNotFound) {
	printOptions__simMetric__hp_api_fileInputTight(); //! ie, then print out.
      }
    }
  }
  if( (operation_type == e_hp_API_fileInputTight__logicType_undef) || (operation_type == e_hp_API_fileInputTight__logicType_ccm_matrix) ) { //! CCM: matrix x gold
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id = getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(stringOf_enum);
    if(enum_id != e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef) {
      __isApplied = true; //! ie, as the 'string matched the enum'.
      __isApplied = true;
      *scalar_enum = (int)enum_id;
      *scalarBool_isFound = true;
    } else {
      if(config__printAlternativesIfNotFound) {
	printOptions__ccm_matrixBased__hp_api_fileInputTight(); //! ie, then print out.
      }
      //printOptions__simMetric__hp_api_fileInputTight(); //! ie, then print out.
    }
  }
  if( (operation_type == e_hp_API_fileInputTight__logicType_undef) || (operation_type == e_hp_API_fileInputTight__logicType_ccm_goldXgold) ) { //! CCM: gold x gold:
    const e_kt_matrix_cmpCluster_metric_t enum_id = getEnumOf__e_kt_matrix_cmpCluster_metric_t(stringOf_enum);
    if(enum_id != e_kt_matrix_cmpCluster_metric_undef) {
      __isApplied = true;
      *scalar_enum = (int)enum_id;
      *scalarBool_isFound = true;
    } else {
      if(config__printAlternativesIfNotFound) {
	printOptions__ccm_twoClusterResults__hp_api_fileInputTight(); //! ie, then print out.
      }
      //printOptions__simMetric__hp_api_fileInputTight(); //! ie, then print out.
    }
  }
  if( (operation_type == e_hp_API_fileInputTight__logicType_undef) || (operation_type == e_hp_API_fileInputTight__logicType_clustAlg) ) { //! clust-alg:
    e_hpLysis_clusterAlg_t enum_id = get_enumOf__e_hpLysis_clusterAlg_t(stringOf_enum);
    if(enum_id != e_hpLysis_clusterAlg_undef) {
      __isApplied = true;
      *scalar_enum = (int)enum_id;
      *scalarBool_isFound = true;
    } else {
      if(config__printAlternativesIfNotFound) {
	printOptions__clustAlg__hp_api_fileInputTight(); //! ie, then print out.
      }
    }
  }
  //! ----------------------------------
  //!  
  if(__isApplied == false) {//! Handle 'not-found' cases:
    fprintf(stderr, "!!\t Was Not able to handle the operation_type=\"%u\": please validate your API-calls, and if latter does Not help, then contact senior delvoeper [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", operation_type, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as we assume an error 'have occured'.
  } else {return true;} //! ie, as we then asusme optioern 'was a success'.  
}

//! computes a "matrix x gold" CCM based on data-input, and store result in "scalar_result" (oekseth, 06. mar. 2017).
bool apply__ccm__matrixBased__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id, s_kt_matrix_t *mat_1, s_kt_list_1d_uint_t *list_1, t_float *scalar_result) {
  //!
  //! What we expect:
  if(!mat_1 || !mat_1->nrows || !mat_1->ncols || !list_1 || !list_1->list_size || !scalar_result) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  } else if(mat_1->nrows != list_1->list_size) {
    //allocOnStack__char__sprintf(2000, str_local, ); //! ie, intate a new variable "str_local". 
    allocOnStack__char__sprintf(2000, str_local, "We expected the matrix.nrows == list.list_size, which is Not the case: nrows=%u while list_size=%u. ", mat_1->nrows, list_1->list_size); //! ie, intate a new variable "str_local". 
    MF__inputError_generic(str_local);
    assert(false);
    return false;
  }

  // printf("\n# Compute CCM for a matrix[%u, %u], using ccm=\"%s\", at %s:%d\n", mat_1->nrows, mat_1->ncols, getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id), __FILE__, __LINE__);
  
  //!
  //! Apply logics:
  s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(mat_1);  
  const bool is_ok = ccm__singleMatrix__hp_ccm(enum_id, &mat_shallow, list_1->list, scalar_result);
  assert(is_ok);
  // printf("scalar_result=%f, at %s:%d\n", *scalar_result, __FILE__, __LINE__);
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return is_ok;
}

bool apply__ccm__matrixBased__matrixInput__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_2, const uint mat_2_rowId, t_float *scalar_result) {
  //for(uint data_id = 0; data_id < mat_2->nrows; data_id++) 
  assert(mat_1); assert(mat_2); 
  if(mat_2_rowId >= mat_2->nrows) {
    fprintf(stderr, "!!\t provided a row-id outside the boundaries of the secodn inptu-amtirx, ie, %u !< %u. In biref please udpate your API-call. Observaiton at [%s]:%s:%d\n", mat_2_rowId, mat_2->nrows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  //  {
  //! Set headers: rows: 
  /* allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis[%u]", data_id); //! ie, intate a new variable "str_local".  */
  /* set_stringConst__s_kt_matrix(mat_result, /\*index=*\/data_id, str_local, /\*addFor_column=*\/false); */
  
  //! ----------------------------------
  //!
  //! Apply the speicifed logics:      
  s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->nrows);
  //! Set valeus:
  //for(uint i = 0; i < mat_2->ncols; i++) 
    const uint data_id = mat_2_rowId;
    //    {
    for(uint i = 0; i < mat_2->ncols; i++) {
      const t_float score = mat_2->matrix[data_id][i];
      if(score != T_FLOAT_MAX) {
	list_1.list[i] = score;
      } else {list_1.list[i] = UINT_MAX;}
    }
    
    //mat_2->matrix[0]
    //if(mat_2->nrows == 1) {
    // printf("\n\n file_1=[%u, %u], at %s:%d\n", mat_1->nrows, mat_1->ncols, __FILE__, __LINE__);
    const bool is_ok = apply__ccm__matrixBased__hp_api_fileInputTight(enum_id, mat_1, &list_1, scalar_result);
    assert(is_ok);
    //!
    //! De.-allcoate:
    free__s_kt_list_1d_uint_t(&list_1);
    return is_ok;
}


//! computes a "gold1 x gold2" CCM based on data-input, and store result in "scalar_result" (oekseth, 06. mar. 2017).
bool apply__ccm__twoClusters__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_metric_t enum_id, s_kt_list_1d_uint_t *list_1, s_kt_list_1d_uint_t *list_2, t_float *scalar_result) {
  //!
  //! What we expect:
  if(!list_2 || !list_2->list_size || !list_2->list_size || !list_1 || !list_1->list_size || !scalar_result) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  } else if(list_2->list_size != list_1->list_size) {
    allocOnStack__char__sprintf(2000, str_local, "We expected the list_1.list_size == list_2.list_size, which is Not the case: |list_1|=%u while |list_2|=%u. ", list_1->list_size, list_2->list_size); //! ie, intate a new variable "str_local". 
    MF__inputError_generic(str_local);
    assert(false);
    return false;
  }
  { //! Validate taht clsutere-mberships are set (oesketh, 06. mar. 2017):
    uint debug_cntFound_1 = 0;    uint debug_cntFound_2 = 0;
    const uint *mapOf_vertexToCentroid1 = list_1->list;
    const uint *mapOf_vertexToCentroid2 = list_2->list;
    for(uint i = 0; i < list_1->list_size; i++) {
      if(mapOf_vertexToCentroid1[i] != UINT_MAX) {
	debug_cntFound_1++;
      }
      if(mapOf_vertexToCentroid2[i] != UINT_MAX) {
	debug_cntFound_2++;
      }
    }
    //! if not any clsuter-e,mershisp are knwon then we asusme a bug is found wrt. the input-data':
    if(!debug_cntFound_1 || !debug_cntFound_2) {
      fprintf(stderr, "!!\t We expected clsuter (or centorid) membershisp to be set for both the 'gold-1' and 'gold-2', which is Not the case. In brief we sugget updating your API and/or your input-files. If latter does Not help (or soudns all to comples) then please contact senior delvoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      //! Given teh compielr somehints:
      assert(debug_cntFound_1 > 0);
      assert(debug_cntFound_2 > 0);
      //! Provide a 'fall-back':
      *scalar_result = T_FLOAT_MAX; return true; //! ie, as all vertices are in the same clsuter.
    }
  }  

 
  //!
  //! Apply logics:
  const bool is_ok = ccm__twoHhypoThesis__hp_ccm(enum_id, list_1->list, list_2->list, list_1->list_size, NULL, NULL, scalar_result);
  assert(is_ok);
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return is_ok;
}

bool apply__ccm__twoClusters__matrixInput__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_metric_t enum_id, s_kt_matrix_t *mat_1, const uint mat_1_rowId, s_kt_matrix_t *mat_2, const uint mat_2_rowId,  t_float *scalar_result) {
  assert(mat_1); assert(mat_2); 
  if(mat_1_rowId >= mat_1->nrows) {
    fprintf(stderr, "!!\t provided a row-id outside the boundaries of the secodn inptu-amtirx, ie, %u !< %u. In biref please udpate your API-call. Observaiton at [%s]:%s:%d\n", mat_1_rowId, mat_1->nrows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(mat_2_rowId >= mat_2->nrows) {
    fprintf(stderr, "!!\t provided a row-id outside the boundaries of the secodn inptu-amtirx, ie, %u !< %u. In biref please udpate your API-call. Observaiton at [%s]:%s:%d\n", mat_2_rowId, mat_2->nrows, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
  s_kt_list_1d_uint_t list_2 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
  uint cnt_added_0 = 0; uint cnt_added_1 = 0;
  const uint data_id = mat_1_rowId;
  const uint data_id_out = mat_2_rowId;
  //! -------------- 
  for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
    {
      const t_float score = mat_1->matrix[data_id][i];
      if(score != T_FLOAT_MAX) {
	list_1.list[i] = score;
	cnt_added_0++;
      } else {list_1.list[i] = UINT_MAX;}
    }
  }
  for(uint i = 0; i < mat_2->ncols; i++) { 	//! Set valeus:
    {
      const t_float score = mat_1->matrix[data_id_out][i];
      if(score != T_FLOAT_MAX) {
	list_2.list[i] = score;
	cnt_added_1++;
      } else {list_2.list[i] = UINT_MAX;}
    }
  }
  
  //! Comptue the CCM: 
  // t_float scalar_result = 0;
  bool is_ok = false;
  if(cnt_added_0 && cnt_added_1) {
    is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(enum_id, &list_1, &list_2, scalar_result);
    assert(is_ok);
  } else {
    *scalar_result = T_FLOAT_MAX;
  }
  //!
  //! Udpate result:
  /* assert(data_id < mat_result->nrows); */
  /* assert(data_id_out < mat_result->ncols); */
  // mat_result->matrix[data_id][data_id_out] = scalar_result;
  //!
  //! De-allcoate:
  free__s_kt_list_1d_uint_t(&list_1);
  free__s_kt_list_1d_uint_t(&list_2);
  return is_ok;
}
bool apply__simMetric_dense__extraArgs__hp_api_fileInputTight(const s_kt_correlationMetric_t obj_metric__, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_2, s_kt_matrix_t *mat_result) {
  s_kt_correlationMetric_t obj_metric = obj_metric__;
  if(obj_metric.metric_id == e_kt_correlationFunction_undef) {obj_metric.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;} //! ie, wher elatter is the deuflat pparoach in a alrge nubmer fo reseharc-ojurnals (oekseth, 06. arp. 2017).
  //!
  //! What we expect:
  if(!mat_1 || !mat_1->nrows || !mat_1->ncols || !mat_result) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  } else if(mat_2 && (mat_2->ncols != mat_1->ncols) ) {
    if( (mat_2->ncols == 0) && (mat_2->nrows == 0) ) {
      mat_2 = mat_1; //! ie, as we then assuemt atht he caller has explctilyset  "mat_2" to empty.
    } else {
      allocOnStack__char__sprintf(2000, str_local, "We expected the mat_1.ncols == mat_2.ncols, which is Not the case: ncols(1)=%u while ncols(2)=%u. ", mat_1->ncols, mat_2->ncols);
      MF__inputError_generic(str_local);
      assert(false);
      return false; 
    }     
  } 
  
  //!
  //! De-allcoate hte result-matrix if the reuslt-amtrix is already allcoated:
  free__s_kt_matrix(mat_result);
  //!
  //! Apply logics:
  *mat_result = applyAndReturn__altTypeAsInput__hp_distance_wrapper(obj_metric, mat_1, mat_2, init__s_hp_distance__config_t()); //! where 'the funciton' is deifned  in our "hp_distance_wrapper.h"
  
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return true;
}

//! Applies a dense matrix-based simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
bool apply__simMetric_dense__hp_api_fileInputTight(const e_kt_correlationFunction_t enum_id, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_2, s_kt_matrix_t *mat_result) {

  //! Set the metric:
  s_kt_correlationMetric_t obj_metric = setTo_empty__andReturn__s_kt_correlationMetric_t();   obj_metric.metric_id = enum_id;
  //! Apply logics:
  return apply__simMetric_dense__extraArgs__hp_api_fileInputTight(obj_metric, mat_1, mat_2, mat_result);
}
bool apply__simMetric_sparse__extraArgs__hp_api_fileInputTight(const s_kt_correlationMetric_t obj_metric, s_kt_list_1d_pairFloat_t *list_1,  s_kt_list_1d_pairFloat_t *list_2, s_kt_matrix_t *mat_result) {
  //!
  //! What we expect:
  if(!list_1 || !list_1->list_size) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }   
  //!
  //! De-allcoate hte result-matrix if the reuslt-amtrix is already allcoated:
  free__s_kt_matrix(mat_result); *mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  //!
  //! Convert into a 2d-list:
  // s_kt_list_1d_pairFloat_t vec_1 = init__s_kt_list_1d_pairFloat_t(/*list_size=*/0);
  s_kt_set_2dsparse_t sparse_1 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(list_1);
  //!
  //! Compute simliarty:
  s_kt_sparse_sim obj_sim = init__s_kt_sparse_sim_t(obj_metric);
  if(list_2 && list_2->list_size) {
    //! Convert frmo the 1d-list to a 2d-list:
    s_kt_set_2dsparse_t sparse_2 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(list_2);
    //!
    //! Apply logics:
    s_kt_matrix_base_t mat_shallow = computeStoreIn__matrix__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/&sparse_2, /*weight=*/NULL);
    *mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_shallow);
    //!
    //! De-allocates:
    free_s_kt_set_2dsparse_t(&sparse_2);
  } else {
    //!
    //! Apply logics:
    //*mat_result = 
    s_kt_matrix_base_t mat_shallow = computeStoreIn__matrix__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/NULL, /*weight=*/NULL);
    *mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_shallow);
  }
  //!
  //! De-allcoate the sparse cofnigruation-object:
  free__s_kt_sparse_sim_t(&obj_sim);
  free_s_kt_set_2dsparse_t(&sparse_1);
  
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return true;
}
//! applies a sparse 2d-list simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
bool apply__simMetric_sparse__hp_api_fileInputTight(const e_kt_correlationFunction_t enum_id, s_kt_list_1d_pairFloat_t *list_1,  s_kt_list_1d_pairFloat_t *list_2, s_kt_matrix_t *mat_result) { 
  //! Set the metric:
  s_kt_correlationMetric_t obj_metric = setTo_empty__andReturn__s_kt_correlationMetric_t();   obj_metric.metric_id = enum_id;
  //! Apply logics:
  return apply__simMetric_sparse__extraArgs__hp_api_fileInputTight(obj_metric, list_1, list_2, mat_result);
}

//! Compute clusters based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
//bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const s_kt_correlationMetric_t obj_metric, const uint nclusters, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep) {
bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const s_kt_correlationMetric_t obj_metric_pre, const s_kt_correlationMetric_t obj_metric_inside, const uint cnt_clusters_x, const uint cnt_clusters_y, t_float iterative_SOM_tauInit, uint iterative_maxIterCount, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep) {
  if(!mat_1 || !mat_1->nrows || !mat_1->ncols || !mat_result) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  //!
  //! Apply lgoics:
  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
  obj_hp.config.corrMetric_prior_use = isTo_useSimMetricInPreStep;
  obj_hp.config.corrMetric_prior = obj_metric_pre;
  //  printf("isTo_useSimMetricInPreStep=%u, prestep.metric_id=\"%s\", at %s:%d\n", isTo_useSimMetricInPreStep, get_stringOf_enum__e_kt_correlationFunction_t(obj_hp.config.corrMetric_prior.metric_id), __FILE__, __LINE__);
  //#define __MiF__handleUndef__simMetric(obj) ({if(obj.metric_id == e_kt_correlationFunction_undef) {obj.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;}}) //! ie, wher elatter is the deuflat pparoach in a alrge nubmer fo reseharc-ojurnals (oekseth, 06. arp. 2017).
  obj_hp.config.corrMetric_insideClustering = obj_metric_inside;
  //printf("allocates-SOM with SOM=[%u, %u], UINT_MAX=%u, at %s:%d\n", cnt_clusters_x, cnt_clusters_y, UINT_MAX,  __FILE__, __LINE__);
  obj_hp.config.algConfig_SOM__nxgrid = cnt_clusters_x;
  obj_hp.config.algConfig_SOM__nygrid = cnt_clusters_y;
  obj_hp.config.algConfig_SOM__nygrid = iterative_SOM_tauInit;
  if(obj_hp.config.corrMetric_prior.metric_id == e_kt_correlationFunction_undef) {obj_hp.config.corrMetric_prior.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;} //! ie, wher elatter is the deuflat pparoach in a alrge nubmer fo reseharc-ojurnals (oekseth, 06. arp. 2017).
  if(obj_hp.config.corrMetric_insideClustering.metric_id == e_kt_correlationFunction_undef) {obj_hp.config.corrMetric_insideClustering.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;} //! ie, wher elatter is the deuflat pparoach in a alrge nubmer fo reseharc-ojurnals (oekseth, 06. arp. 2017).
  /* __MiF__handleUndef__simMetric(obj_hp.config.corrMetric_prior); */
  /* __MiF__handleUndef__simMetric(obj_hp.config.corrMetric_insideClustering); */
  // TODO: cosnider adding input-parameter-spec for "nclusters" ... 'noẅ́ we isntead use an 'implcit speciicoan' through applciaiton of a 'default' CCM (oekseth, 06. mar. 2017).
  if( (iterative_maxIterCount == UINT_MAX) ) {iterative_maxIterCount = 300;} //! ie, our default assumption
  const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_id, mat_1, cnt_clusters_x, /*npass=*/iterative_maxIterCount); assert(is_ok);
  //!
  //!   
  //! Update our "mat_result" with the result:
  /**
     //! Note: the contents of teh result will denepdn upon the following cocniditons:
   -- Case(k-clusters-known): export "[vertex] = clusterId"
   -- else:Case(hca-clusters-known): export triplet=(child, parent, score)
   -- else:Case(SOM-clusters-known): export triplet=(vertex, som_x, som_y).
   **/
  free__s_kt_matrix(mat_result);
  if(isTo_inMatrixResult_storeSimMetricResult == false) {
    *mat_result = export_clusterMemberships__hpLysis_api(&obj_hp);
    //!
    { //! Cosnturct a tranpseod veriosn of the matrix, ie, ot enable back-foort betwene the differnet cufncitons, eg, wrt. clsuter gneriaotn and thereafter inptu to amtrix-CCM (oesketh, 06. apr. 2018).
      assert(mat_result->ncols == 1);
      s_kt_matrix_t mat_cpy = initAndReturn__s_kt_matrix(/*nrows=*/1, /*ncols=*/mat_1->nrows);
      for(uint row_id = 0; row_id < mat_result->nrows; row_id++) {
	mat_cpy.matrix[0][row_id] = mat_result->matrix[row_id][0];
	const char *str = getAndReturn_string__s_kt_matrix(mat_1, row_id, /*addFor_column=*/false);
	if(str && strlen(str)) {
	  set_stringConst__s_kt_matrix(&mat_cpy, row_id,  str, /*addFor_column=*/true);
	}
      }
      //! De-allcoate, and then udpat ehte pointers:
      free__s_kt_matrix(mat_result);
      *mat_result = mat_cpy;
    }
    //!
    { //! Handle cases where a clsuter membership is Not set (oekseth, 06. apr. 2018):
      uint max_cluster_id = 0;
      for(uint row_id = 0; row_id < mat_result->nrows; row_id++) {
	for(uint col_id = 0; col_id < mat_result->ncols; col_id++) {
	  const t_float score = mat_result->matrix[row_id][col_id];
	  if(isOf_interest(score)) {
	    max_cluster_id = macro_max(max_cluster_id, (uint)score);
	  }
	}
      }
      //!
      //printf("max_cluster_id=%u, at %s:%d\n", max_cluster_id, __FILE__, __LINE__);
      //! Then set the 'unset' clsuter-ids:
      for(uint row_id = 0; row_id < mat_result->nrows; row_id++) {
	for(uint col_id = 0; col_id < mat_result->ncols; col_id++) {
	  const t_float score = mat_result->matrix[row_id][col_id];
	  if(isOf_interest(score) == false) {
	    //    printf("set-clsuter-id, at %s:%d\n", __FILE__, __LINE__);
	    mat_result->matrix[row_id][col_id] = (t_float)(++max_cluster_id);
	  }
	}
      }
    }
  } else {
    // printf("store-simMatrix-in-matrix-result-var, at %s:%d\n", __FILE__, __LINE__);
    if(obj_hp.matrixResult__inputToClustering.nrows > 0) {
      const bool is_ok = init__copy__s_kt_matrix(mat_result, &(obj_hp.matrixResult__inputToClustering), true);
      //printf("constructed-matrix w/dims=[%u, %u], at %s:%d\n", mat_result->nrows, mat_result->ncols, __FILE__, __LINE__);
      assert(is_ok);
    } //! else we assume the simliaryt-emtirc to not be be-comptued.
  }
  if(result_clusterMemberships != NULL) { //! then we store a copy of the cluster-result-matrix in our data-set.
    *result_clusterMemberships = export__clusterAttributes__s_kt_clusterResult_condensed_t(&obj_hp);
    //printf("comptued clusters-result-obj, eg, w/arr_names_clustering.size=%u, at %s:%d\n", result_clusterMemberships->arr_names_clustering.nrows, __FILE__, __LINE__);
  }

  
  //! 
  //! De-allcoate
  free__s_hpLysis_api_t(&obj_hp);

  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return true;
}

//! Compute clusters based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
bool apply__clustAlg__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const uint nclusters, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result) {
  //! Set the metric:
  s_kt_correlationMetric_t obj_metric_pre = setTo_empty__andReturn__s_kt_correlationMetric_t();  // obj_metric.metric_id = enum_id;
  s_kt_correlationMetric_t obj_metric_inside = setTo_empty__andReturn__s_kt_correlationMetric_t();  // obj_metric.metric_id = enum_id;
  //! Apply logics:
  if(enum_id == e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN) { //! an update in order to address isseus where CCMs are needed ofr voncvercne-crita (oekseth, 06. okt. 2017)
    return apply__clustAlg__extraArgs__hp_api_fileInputTight(enum_id, obj_metric_pre, obj_metric_inside, nclusters, /*som_y=*/2, /*tauInit=*/0.02, /*number-of-max-iterative-iteraitons=*/300, mat_1, mat_result, NULL, /*isTo_inMatrixResult_storeSimMetricResult=*/false, /*isTo_useSimMetricInPreStep=*/true);
  } else {
    return apply__clustAlg__extraArgs__hp_api_fileInputTight(enum_id, obj_metric_pre, obj_metric_inside, nclusters, /*som_y=*/2, /*tauInit=*/0.02, /*number-of-max-iterative-iteraitons=*/300, mat_1, mat_result, NULL, /*isTo_inMatrixResult_storeSimMetricResult=*/false, /*isTo_useSimMetricInPreStep=*/false);
  }
}

//! Read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
bool readFromFile__stroreInStruct__exportResult__hp_api_fileInputTight(const char *stringOf_enum, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_2, s_kt_matrix_t *mat_result, const char *stringOf_exportFile, const char *stringOf_exportFile__format, uint nclusters, const bool inputFile_isSparse) {  
  //! ----------------------------------
  //!  
  //! What we expect:
  if(!mat_1 || !mat_1->nrows || !mat_1->ncols || !stringOf_enum || !strlen(stringOf_enum) ) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  // printf("mat_1\n: rows:%u, cols:%u", mat_1->nrows, mat_1->ncols);
  // printf("mat_2\n: rows:%u, cols:%u", mat_2->nrows, mat_2->ncols);
  //! ----------------------------------
  //!
  //! Idneitfy the type of logics to apply: 
  bool __isApplied = false;
  { //! Simliarty:
    e_kt_correlationFunction_t enum_id = get_enumBasedOn_configuration__kt_metric_aux(stringOf_enum, /*printAn_errorIf_notFound=*/false); //! deifned in our "kt_metric_aux.h".
    if(enum_id != e_kt_correlationFunction_undef) {
      __isApplied = true; //! ie, as the 'string matched the enum'.
      if(inputFile_isSparse == false) {      
	if(mat_2 && (mat_2->nrows > 0)) {
	  //	if(mat_2->nrows > 0) {
	  const bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(enum_id, mat_1, mat_2, mat_result);
	  assert(is_ok);
	} else {
	  const bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(enum_id, mat_1, NULL, mat_result);
	  assert(is_ok);
	}
      } else { //! Tehn we construct sparse data-sets from the input-set:
	if(mat_1->ncols != 3) {
	  allocOnStack__char__sprintf(2000, str_local, "Expected number-of-columns='3', though instead observes that your input-file-1 has '%u' columns. ", mat_1->ncols); //! ie, intate a new variable "str_local". 
	  MF__inputError_generic(str_local);
	  assert(false);
	  if(mat_1->ncols  < 3) {
	    return false;
	  }
	}
	//!
	//! Load data:
	s_kt_list_1d_pairFloat_t sparse_1 = init__s_kt_list_1d_pairFloat_t(mat_1->nrows);
	for(uint i = 0; i < mat_1->nrows; i++) {
	  const t_float *row = mat_1->matrix[i];
	  assert(row);
	  s_ktType_pairFloat_t rel = MF__initFromRow__s_ktType_pairFloat(row, mat_1->ncols);
	  //! Append:
	  set_scalar__s_kt_list_1d_pairFloat_t(&sparse_1, /*index=*/i, rel);
	}
	
	if(mat_2->ncols == 0) {
	  //!
	  //! Apply logics:
	  const bool is_ok = apply__simMetric_sparse__hp_api_fileInputTight(enum_id, &sparse_1, NULL, mat_result);
	  assert(is_ok);
	} else {
	  if(mat_2->ncols != 3) {
	    allocOnStack__char__sprintf(2000, str_local, "Expected number-of-columns='3', though instead observes that your input-file-1 has '%u' columns. ", mat_1->ncols); //! ie, intate a new variable "str_local". 
	    MF__inputError_generic(str_local);
	    assert(false);
	    if(mat_2->ncols  < 3) {
	      return false;
	    }
	  }
	  //!
	  //! Load data:
	  s_kt_list_1d_pairFloat_t sparse_2 = init__s_kt_list_1d_pairFloat_t(mat_2->nrows);
	  for(uint i = 0; i < mat_2->nrows; i++) {
	    const t_float *row = mat_2->matrix[i];
	    assert(row);
	    s_ktType_pairFloat_t rel = MF__initFromRow__s_ktType_pairFloat(row, mat_2->ncols);
	    //! Append:
	    set_scalar__s_kt_list_1d_pairFloat_t(&sparse_2, /*index=*/i, rel);
	  }
	  //!
	  //! Apply logics:
	  const bool is_ok = apply__simMetric_sparse__hp_api_fileInputTight(enum_id, &sparse_1, &sparse_2, mat_result);
	  assert(is_ok);

	  //! De-allcoate:
	  free__s_kt_list_1d_pairFloat_t(&sparse_2);
	}
	//! 
	//! De-allcoate:
	free__s_kt_list_1d_pairFloat_t(&sparse_1);
      }
      //const bool is_ok = apply__ccm__matrixBased__hp_api_fileInputTight(
    }
    
  }
  //! ----------------------------------------------------------
  //!
  //! 'Convert' the dat-asets into a 'masked case':
  s_kt_matrix_t mat_tmp_1 = setToEmptyAndReturn__s_kt_matrix_t();
  s_kt_matrix_t mat_tmp_2 = setToEmptyAndReturn__s_kt_matrix_t();
  if( (__isApplied == false) && (inputFile_isSparse) ) { //! then 'use masking' to 'vert to a dense set':
    mat_tmp_1 = copy__fromSparseToDense__s_kt_matrix(mat_1, /*isTo_allocateSquared=*/true);
    free__s_kt_matrix(mat_1); *mat_1 = mat_tmp_1; //! ie, update the memory-references. 
    if(mat_2->ncols != 0) {
      mat_tmp_2 = copy__fromSparseToDense__s_kt_matrix(mat_2, /*isTo_allocateSquared=*/false);
      free__s_kt_matrix(mat_2); *mat_2 = mat_tmp_2; //! ie, update the memory-references. 

      /* printf("mat-2.dims=[%u, %u], at %s:%d\n", mat_2->nrows, mat_2->ncols, __FILE__, __LINE__); */
      /* assert(false); */
    }
  }
  //! ----------------------------------------------------------
  //!
  //! Continue:
  if(__isApplied == false) { //! CCM: matrix-based:
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id = getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(stringOf_enum);
    //    printf("\n\n file_1=[%u, %u], enum=\"%s\"=%u =? undef=%u, at %s:%d\n", mat_1->nrows, mat_1->ncols, stringOf_enum, enum_id, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef, __FILE__, __LINE__);
    if(enum_id != e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef) {
      __isApplied = true; //! ie, as the 'string matched the enum'.
      if(mat_2->nrows != 0) { 
	if(mat_1->nrows != mat_2->ncols) {
	  allocOnStack__char__sprintf(2000, str_local, "(rows not matchin hypo)\t, ie, nrows(file_1:%u) == ncols(file_2:%u), which is Not the case. ", mat_1->nrows, mat_2->ncols); //! ie, intate a new variable "str_local". 
	  MF__inputError_generic(str_local);
	  assert(false);
	  return false;
	} else if(mat_1->nrows != mat_1->ncols) {	
	  if(true) { //! then we 'allow' users to sepficy a feautre-matrix for inptu to the CCM-evlauation-appraoch.
	    fprintf(stdout, "(info)\t Not an adjecency matrix. Need to address issues of feature-matrix input: rows(matrix-1)=%u != columns(matrix-1)=%u. where matrix[1][0]=%f and matrix[1][ncols-1]=%f. Expected a similarity matrix as input, which is not the case. Therefore compute the similarity matrix using the estalibshed appraoch, ie, before computing the cluster-metric (CCM). For questions please contact the senior developer [oekseth@gmail.com]. Observation  at [%s]:%s:%d\n", mat_1->nrows, mat_1->ncols, mat_1->matrix[1][0],mat_1->matrix[1][mat_1->ncols-1], __FUNCTION__, __FILE__, __LINE__);
	    //! 
	    //! 
	    const e_kt_correlationFunction_t sim_enum = e_kt_correlationFunction_groupOf_minkowski_euclid;
	    /* if(mat_2 && (mat_2->nrows > 0)) { */
	    /*   s_kt_matrix_t mat_tmp = initAndReturn__s_kt_matrix(/\*cnt-ccms=*\/mat_1->nrows, mat_2->nrows); */
	    /*   const bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(sim_enum, mat_1, mat_2, &mat_tmp); */
	    /*   assert(is_ok); */
	    /*   free__s_kt_matrix(mat_1); */
	    /*   free__s_kt_matrix(mat_2); */
	    /*   *mat_1 = mat_tmp; //! ie, 'fetch' the relationships. */
	    /* } else  */
	    {
	      s_kt_matrix_t mat_tmp = initAndReturn__s_kt_matrix(/*cnt-ccms=*/mat_1->nrows, mat_1->nrows);
	      const bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(sim_enum, mat_1, NULL, &mat_tmp);
	      assert(is_ok);
	      free__s_kt_matrix(mat_1);
	      *mat_1 = mat_tmp; //! ie, 'fetch' the relationships.
	    }	  
	  } else {
	    allocOnStack__char__sprintf(2000, str_local, "Expected nrows(file_1:%u) == ncols(file_2:%u), which is Not the case: latter assumption due to the 'input-exepctation' that 'each clsuter-mebership' is defined for each vertex using the first row; if mulitple rows are used, we asusem each 'row' describes a unique hyptothesis to evaluate.  ", mat_1->nrows, mat_2->ncols); //! ie, intate a new variable "str_local". 
	    MF__inputError_generic(str_local);
	    assert(false);
	    return false;
	  }
	}
      }
      if(mat_2->nrows == 0) { //! Then we construct sample-clsuters based on the "ncluster" option.
	if(nclusters > mat_1->nrows) {nclusters = 2;}
	if(nclusters == 0) {nclusters = 2;}
	fprintf(stdout, "(info)\t Need to address issues of cluster-memberships to evaluate. Therefore use the \"-nclusters=%u\" option to split the feature-rows into 'equal' parts, hence using the latter as an evaluation criteria. For questions please contact the senior developer [oekseth@gmail.com]. Observation  at [%s]:%s:%d\n", nclusters, __FUNCTION__, __FILE__, __LINE__);
	*mat_2 = initAndReturn__s_kt_matrix(/*cnt-hypo=*/1, /*cnt-rows=*/mat_1->nrows);
	const uint block_size = (uint)((t_float)mat_1->nrows / (t_float)nclusters);
	uint cluster_id = 0; uint cnt_ele = 0;
	const bool useRandom_fixedBlock_size = false;
	
	for(uint row_id = 0; row_id < mat_1->nrows; row_id++) {
	  if(useRandom_fixedBlock_size == false) {
	    cluster_id = (uint)rand() % nclusters; //! ie, evenly distributed among the clusters.
	  }
	  //! 
	  //! Set cluster-id:
	  mat_2->matrix[/*data_id=*/0][row_id] = (t_float)cluster_id;
	  //! 
	  if(useRandom_fixedBlock_size == true) {
	    //! Update cluster-id: 	  
	    if(cnt_ele > block_size) {
	      cluster_id++;
	      cnt_ele = 0; 
	    } //else if(block_size == UINT_MAX) {cluster_id = rand() % random_cntClusters;}
	    cnt_ele++;
	  }
	}	
      }
      // printf("\n\n file_1=[%u, %u], at %s:%d\n", mat_1->nrows, mat_1->ncols, __FILE__, __LINE__);
      // if(mat_1->nrows == 150) {assert(false);} // FIXME: remove
      //! Allcoate:
      *mat_result = initAndReturn__s_kt_matrix(/*cnt-ccms=*/mat_2->nrows, /*evaluation-cases-each=*/1);
      //!
      //! Set headers: columns: 
      set_stringConst__s_kt_matrix(mat_result, /*index=*/0, "CCM-score", /*addFor_column=*/true);
      for(uint data_id = 0; data_id < mat_2->nrows; data_id++) {
	//! Set headers: rows: 
	if( (mat_1 != NULL) && (mat_1->matrix != NULL) ) {
	  assert(data_id < mat_1->nrows);
	  const char *str_local = getAndReturn_string__s_kt_matrix(mat_1, data_id, /*addFor_column=*/false);
	  set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	} else {
	  allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis[%u]", data_id); //! ie, intate a new variable "str_local". 
	  set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	}
	
	//! ----------------------------------
	//!
	//! Apply the speicifed logics:      
	s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->nrows);
	//! Set valeus:
	for(uint i = 0; i < mat_2->ncols; i++) {
	  const t_float score = mat_2->matrix[data_id][i];
	  if(score != T_FLOAT_MAX) {
	    list_1.list[i] = score;
	  } else {list_1.list[i] = UINT_MAX;}
	}
	//mat_2->matrix[0]
	//if(mat_2->nrows == 1) {
	t_float scalar_result = 0;
	// printf("\n\n file_1=[%u, %u], at %s:%d\n", mat_1->nrows, mat_1->ncols, __FILE__, __LINE__);
	const bool is_ok = apply__ccm__matrixBased__hp_api_fileInputTight(enum_id, mat_1, &list_1, &scalar_result);
	assert(is_ok);
	//!
	//! Udpate result:
	mat_result->matrix[data_id][0] = scalar_result;
	//!
	//! De.-allcoate:
	free__s_kt_list_1d_uint_t(&list_1);
      }
    }
  }
  if(__isApplied == false) { //! CCM: "gold1 x gold2":
    const e_kt_matrix_cmpCluster_metric_t enum_id = getEnumOf__e_kt_matrix_cmpCluster_metric_t(stringOf_enum);
    //	printf("at %s:%d\n", __FILE__, __LINE__);
    if(enum_id != e_kt_matrix_cmpCluster_metric_undef) {
      __isApplied = true; //! ie, as the 'string matched the enum'.
      if(mat_2->ncols == 0) {
	// printf("at %s:%d\n", __FILE__, __LINE__);
	//! Allcoate:
	*mat_result = initAndReturn__s_kt_matrix(/*cnt-ccms=*/mat_1->nrows, /*evaluation-cases-each=*/mat_1->nrows);
	bool result_hasIndeitifedAtLEastOne = false;
	//! Then we apply a 'self-comparisn':
	for(uint data_id = 0; data_id < mat_1->nrows; data_id++) {	
	  { //! Set headers: columns: 
	    if( (mat_1 != NULL) && (mat_1->matrix != NULL) ) {
	      assert(data_id < mat_1->nrows);
	      const char *str_local = getAndReturn_string__s_kt_matrix(mat_1, data_id, /*addFor_column=*/false);
	      set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	    } else {
	      allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis[%u]", data_id); //! ie, intate a new variable "str_local". 
	      set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	    }
	  }
	  //! Iterate:
	  for(uint data_id_out = 0; data_id_out < mat_1->nrows; data_id_out++) {	
	    if(data_id == 0)  { //! Set headers: columns: 
	      if( (mat_2 != NULL) && (mat_2->matrix != NULL) ) {
		assert(data_id_out < mat_2->nrows);
		const char *str_local = getAndReturn_string__s_kt_matrix(mat_2, data_id_out, /*addFor_column=*/false);
		set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
	      } else { 
		if( (mat_1 != NULL) && (mat_1->matrix != NULL) ) { //! ie, as we then assuem that a matirx of clsuter-IDS is compared with itself:
		  assert(data_id < mat_1->nrows);
		  const char *str_local = getAndReturn_string__s_kt_matrix(mat_1, data_id_out, /*addFor_column=*/false);
		  set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
		} else {
		  allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis[%u]", data_id_out); //! ie, intate a new variable "str_local". 
		  set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
		}
	      }
	    } 
	    s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    s_kt_list_1d_uint_t list_2 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    uint cnt_added_0 = 0; uint cnt_added_1 = 0;
	    for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
	      {
		const t_float score = mat_1->matrix[data_id][i];
		if(score != T_FLOAT_MAX) {
		  list_1.list[i] = (uint)score;
		  cnt_added_0++;
		} else {list_1.list[i] = UINT_MAX;}
	      }
	    }
	    if(mat_2 && (mat_2->ncols > 0) ) {
	      for(uint i = 0; i < mat_2->ncols; i++) { 	//! Set valeus:
		{
		  assert(mat_2);
		  const t_float score = mat_2->matrix[data_id_out][i];
		  if(score != T_FLOAT_MAX) {
		    list_2.list[i] = score;
		    cnt_added_1++;
		  } else {list_2.list[i] = UINT_MAX;}
		}
	      }
	    } else {
	      for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
		{
		  const t_float score = mat_1->matrix[data_id_out][i];
		  if(score != T_FLOAT_MAX) {
		    list_2.list[i] = (uint)score;
		    cnt_added_1++;
		  } else {list_2.list[i] = UINT_MAX;}
		}
	      }
	    }
	    //! Comptue the CCM: 
	    t_float scalar_result = 0;
	    if(cnt_added_0 && cnt_added_1) {
	      // printf("compute, for cnt_added_1=%u, cnt_added_0=%u, at %s:%d\n", cnt_added_1, cnt_added_0, __FILE__, __LINE__);
	      const bool is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(enum_id, &list_1, &list_2, &scalar_result);
	      assert(is_ok);
	    } else {
	      scalar_result = T_FLOAT_MAX;
	    }
	    //!
	    //! Udpate result:
	    assert(data_id < mat_result->nrows);
	    assert(data_id_out < mat_result->ncols);
	    mat_result->matrix[data_id][data_id_out] = scalar_result;
	    if(isOf_interest(scalar_result)) {result_hasIndeitifedAtLEastOne = true;}
	    // printf("\t\t [%u][%u]=%f, at %s:%d\n", data_id, data_id_out, scalar_result, __FILE__, __LINE__);
	    //!
	    //! De-allcoate:
	    free__s_kt_list_1d_uint_t(&list_1);
	    free__s_kt_list_1d_uint_t(&list_2);
	  }
	}
	if(result_hasIndeitifedAtLEastOne == false) {
	  // MF__inputError_generic(str_local);
	  fprintf(stderr, "(info)\t Did not identify any scores which were regarded as significant in your data-sets, ie, which explains why the result-matrix is empty. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	}
      } else { //! Then we comapre the 'hyptothesis' in matrix(1) with 'hyptothesis' in matrix(2):
	if(mat_1->ncols != mat_2->ncols) {
	  allocOnStack__char__sprintf(2000, str_local, "Expected the number of hyptothesis (ie, columns) in the first matrix to equal the number of hyptothesis (ie, rows) in the second matrix. However, what we observe is that dim(file=1)=[%u, %u], while dim(file=2)?[%u, %u], ie, indicating an error in your data-input. ", mat_1->nrows, mat_1->ncols, mat_2->nrows, mat_2->ncols); //! ie, intate a new variable "str_local". 
	  MF__inputError_generic(str_local);
	  assert(false);
	  return false;
	} 
	//! Allcoate:
	//const uint cnt_hyp = mat_1->nrows * mat_2->nrows
	*mat_result = initAndReturn__s_kt_matrix(/*cnt-ccms=*/mat_1->nrows, /*evaluation-cases-each=*/mat_2->nrows);
	//! Then we apply a 'self-comparisn':
	for(uint data_id = 0; data_id < mat_1->nrows; data_id++) {	
	  { //! Set headers: columns: 
	    if( (mat_1 != NULL) && (mat_1->matrix != NULL) ) {
	      const char *str_local = getAndReturn_string__s_kt_matrix(mat_1, data_id, /*addFor_column=*/false);
	      set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	    } else {
	      allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data1[%u]", data_id); //! ie, intate a new variable "str_local". 
	      set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	    }
	  }
	  //! Iterate:
	  for(uint data_id_out = 0; data_id_out < mat_2->nrows; data_id_out++) {	
	    if(data_id == 0)  { //! Set headers: columns: 
	      if( (mat_2 != NULL) && (mat_2->matrix != NULL) ) {
		const char *str_local = getAndReturn_string__s_kt_matrix(mat_2, data_id_out, /*addFor_column=*/false);
		set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
	      } else {
		allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data2[%u]", data_id_out); //! ie, intate a new variable "str_local". 
		set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
	      }
	    } 
	    assert(mat_1->ncols == mat_2->ncols);
	    s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    s_kt_list_1d_uint_t list_2 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
	      {
		const t_float score = mat_1->matrix[data_id][i];
		if(score != T_FLOAT_MAX) {
		  list_1.list[i] = score;
		} else {list_1.list[i] = UINT_MAX;}
	      }
	      {
		const t_float score = mat_2->matrix[data_id_out][i];
		if(score != T_FLOAT_MAX) {
		  list_2.list[i] = score;
		} else {list_2.list[i] = UINT_MAX;}
	      }
	    }
	    //! Comptue the CCM: 
	    t_float scalar_result = 0;
	    const bool is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(enum_id, &list_1, &list_2, &scalar_result);
	    assert(is_ok);
	    //!
	    //! Udpate result:
	    assert(data_id < mat_result->nrows);
	    assert(data_id_out < mat_result->ncols);
	    mat_result->matrix[data_id][data_id_out] = scalar_result;
	    // printf("\t\t [%u/%u][%u/%u]=%f, at %s:%d\n", data_id, mat_1->nrows, data_id_out, mat_1->nrows, scalar_result, __FILE__, __LINE__);
	    //!
	    //! De-allcoate:
	    free__s_kt_list_1d_uint_t(&list_1);
	    free__s_kt_list_1d_uint_t(&list_2);
	  }
	}	
      }
    }
  }
  if(__isApplied == false) { //! "hpLysis_api"
    e_hpLysis_clusterAlg_t enum_id = get_enumOf__e_hpLysis_clusterAlg_t(stringOf_enum);
    if(enum_id != e_hpLysis_clusterAlg_undef) {
      if(mat_2->matrix != NULL) {
	allocOnStack__char__sprintf(2000, str_local, "Option Not supported: a matrix has been specified for 'input=2', though we in oru genrelaised interface does Not support latter option for enum=\"%s\"; if latter is of specific itnerest, then please contact the hpLysis developers. ", stringOf_enum); //! ie, intate a new variable "str_local". 
	MF__inputError_generic(str_local); assert(false);
      }
      //! ----------------------------------
      //!
      //! Apply the speicifed logics:      
      const bool is_ok = apply__clustAlg__hp_api_fileInputTight(enum_id, nclusters, mat_1, mat_result);
      assert(is_ok);
      __isApplied = true;
    }
  }
  //! ----------------------------------
  //!
  //! Write out the result:   
  if(__isApplied == true) {       
    if(mat_result->nrows != 0) {
      //!
      //! Provide support both for different 'export' formats, and for the 'use' of "stdout" file-descriptor:
      bool is_epxorted = false;
      bool isTo_useMatrixFormat = true;
      bool isTo_inResult_isTo_useJavaScript_syntax = false;
      bool isTo_inResult_isTo_useJSON_syntax = false;
      const char *suffix = "tsv";
      if(stringOf_exportFile__format && strlen(stringOf_exportFile__format)) {
#define __MiF__cmp(string) ({bool is_equal = false; if(strlen(string) == strlen(stringOf_exportFile__format)) {if(0 == strncasecmp(string, stringOf_exportFile__format, strlen(string))) {is_equal = true;}} is_equal;}) //! ie, a wrapper to return true if the format equals:
	//!
	//! Investigate different format-cases:
	if(__MiF__cmp("json")) {
	  is_epxorted = true;
	  isTo_inResult_isTo_useJavaScript_syntax = true;
	  isTo_inResult_isTo_useJSON_syntax = true;
	  suffix = "json";
	} else if(__MiF__cmp("js")) {
	  isTo_inResult_isTo_useJavaScript_syntax = true;
	  is_epxorted = true;
	  suffix = "js";
	}      
#undef __MiF__cmp
      }
      /* if(is_epxorted == false) { //! then we export 'using' atsv-format. */
      
      /* } */
      FILE *file_out = NULL;
      if(stringOf_exportFile && strlen(stringOf_exportFile)) {
	export_config__s_kt_matrix_t(mat_result, stringOf_exportFile);
	assert(mat_result->stream_out != NULL); //! ie, as we expect [ªbov€] to have been impluct set.
      } else { //! then we use the 'stadnard-out' file-dsescriptor:
	file_out = stdout;
	mat_result->stream_out = file_out;
      }
      //!
      //! Compelte configuraiton, and then exprot result:
      assert(mat_result->stream_out != NULL); //! ie, as we expect [ªbov€] to have been impluct set.
      export_config_extensive_s_kt_matrix_t(mat_result, NULL, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, 
					    /*matrix-variable-suffix=*/suffix, 
					    false, //(bool)(stringOf_id != NULL), 
					    /*cnt_dataObjects_toGenerate=*/1); //cnt_objs_toExport); 
      export__s_kt_matrix_t(mat_result);
      //! --------------------
      //!
      //! Close:
      if(!file_out) {
	closeFileHandler__s_kt_matrix_t(mat_result);      
      }
    } else {
      allocOnStack__char__sprintf(2000, str_local, "Result-set is empty, proably due to your call nor resulting in any identifed data-mining resutls. However, it may also be due to wrong call-paramters. In this appraoch we have investigated for matrix(1)=[%u, %u], matrix(2)=[%u, %u], nclusters=%u, data(isSparse)='%s', and ops=\"%s\". ", 
				  mat_1->nrows, mat_1->ncols, 
				  (mat_2) ? mat_2->nrows : 0, 
				  (mat_2) ? mat_2->ncols : 0, 
				  (nclusters != UINT_MAX) ? nclusters : (uint)INFINITY,
				  (inputFile_isSparse) ? "true" : "false",
				  stringOf_enum); //! ie, intate a new variable "str_local". 
      MF__inputError_generic(str_local);
      assert(false);
      __isApplied = false; //! ie, as latter is sued as the ret-val
    }
  } else {
    allocOnStack__char__sprintf(2000, str_local, "Unkown configuraiton described for enum=\"%s\". ", stringOf_enum); //! ie, intate a new variable "str_local". 
    MF__inputError_generic(str_local);
    assert(false);
  }


  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return __isApplied;
}

//! Read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
bool readFromFile__exportResult__hp_api_fileInputTight(const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse) {  
  //! ----------------------------------
  //!  
  //! What we expect:
  if(!stringOf_enum || !strlen(stringOf_enum) || !stringOf_inputFile_1 || !strlen(stringOf_inputFile_1)) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  //! ----------------------------------
  //!
  //! Read the input-file(s) and validate results: 
  s_kt_matrix_t mat_1 = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_inputFile_1, initAndReturn__s_kt_matrix_fileReadTuning_t());
  if(!mat_1.nrows || !mat_1.ncols) {
    allocOnStack__char__sprintf(2000, str_local, "Unable to read input-file=\"%s\". ", stringOf_inputFile_1);
    MF__inputError_generic(str_local);
    //MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  if(false) {
    printf("\n\n file_1=[%u, %u], at %s:%d\n", mat_1.nrows, mat_1.ncols, __FILE__, __LINE__);
    export__singleCall__s_kt_matrix_t(&mat_1, "tmp.tsv", NULL);
    assert(false);
  }
  //! Read the second input-file
  s_kt_matrix_t mat_2 = setToEmptyAndReturn__s_kt_matrix_t();
  if(stringOf_inputFile_2 && strlen(stringOf_inputFile_2)) {
    mat_2 = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_inputFile_2, initAndReturn__s_kt_matrix_fileReadTuning_t());
    if(!mat_2.nrows || !mat_2.ncols) {
      allocOnStack__char__sprintf(2000, str_local, "Unable to read input-file_2=\"%s\". ", stringOf_inputFile_2); //! ie, intate a new variable "str_local". 
      MF__inputError_generic(str_local);
      //MF__inputError_generic("The input-parameters were Not set");
      assert(false);
      return false;
    }     
  }
  //! Intiate result-matrix:
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  //! ----------------------------------
  //!
  //! Apply Logics:
  const bool is_ok = readFromFile__stroreInStruct__exportResult__hp_api_fileInputTight(stringOf_enum, &mat_1, &mat_2, &mat_result, stringOf_exportFile, stringOf_exportFile__format, nclusters, inputFile_isSparse);
  assert(is_ok);
  //!
  //! De-allcoate:
  free__s_kt_matrix(&mat_1);
  free__s_kt_matrix(&mat_2);
  free__s_kt_matrix(&mat_result);
  //! @return the status-code.
  return is_ok;
}

static const char *stringOf_enum__par = "-ops=";
static const char *stringOf_inputFile_1__par = "-file1=";
static const char *stringOf_inputFile_2__par = "-file2=";
static const char *stringOf_exportFile__par = "-out=";
static const char *stringOf_exportFile__format__par = "-outFormat=";
static const char *nclusters__par = "-nclusters=";
static const char *inputFile_isSparse__par = "-inputIsSparse=";


//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_fileInputTight(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse, uint *scalar_cntArgs, char **listOf_args__2d) {  
  assert(scalar_cntArgs);
  assert(listOf_args__2d);
  uint cnt_chars = 10000;
  const char def_0 = 0;
  //! Allocate:
  char *str_cmd = allocate_1d_list_char(cnt_chars, def_0);
  //! ----------------------------
  //!
  //! Define function
  char *curr = str_cmd;
  uint cnt = 0;
#define __MiF__insertIfValue(val, param) ({if(val && strlen(val)) {listOf_args__2d[cnt] = curr; cnt++; sprintf(curr, "%s%s", param, val); curr += strlen(curr); *curr = '\0'; curr++;}}) //! ie, 'insert value' if the inptu-value is set.
  //!
  //! Add values:
  __MiF__insertIfValue("x_hp" , ""); //! ie, as the first arguemtn is always the name of the progrma.
  __MiF__insertIfValue(stringOf_enum , stringOf_enum__par);
  __MiF__insertIfValue(stringOf_inputFile_1 , stringOf_inputFile_1__par);
  __MiF__insertIfValue(stringOf_inputFile_2 , stringOf_inputFile_2__par);
  __MiF__insertIfValue(stringOf_exportFile , stringOf_exportFile__par);
  __MiF__insertIfValue(stringOf_exportFile__format , stringOf_exportFile__format__par);
  if( (nclusters > 1) && (nclusters != UINT_MAX) ) {
    allocOnStack__char__sprintf(2000, str_local, "%u", nclusters); //! ie, intate a new variable "str_local". 
    __MiF__insertIfValue(str_local , nclusters__par);
  }
  if(inputFile_isSparse) {
    __MiF__insertIfValue("1", inputFile_isSparse__par);
  }
#undef __MiF__insertIfValue
  *scalar_cntArgs = cnt;
  //! ----------------------------
  //!
  if(debug_config__isToCall) { //! then apply logic:
    const bool is_ok = readFromFile__exportResult__hp_api_fileInputTight(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, nclusters, inputFile_isSparse);
    assert(is_ok);
  }
  //! ----------------------------
  //!
  //! @return 
  return str_cmd;
}

/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_fileInputTight(char **arg, const uint arg_size) {
  assert(arg);
  const char *stringOf_enum = NULL;
  //const char *stringOf_enum__nclusters = NULL;
  const char *stringOf_inputFile_1 = NULL;
  const char *stringOf_inputFile_2 = NULL;
  const char *stringOf_exportFile = NULL;
  const char *stringOf_exportFile__format = NULL;
  uint nclusters = 0; 
  bool inputFile_isSparse = false;
  //! ----------------------------------
  uint __cntParams_set = 0;

  if(arg_size == 2) {
#define __MiF__cmp(string) ({bool is_equal = false; if(strlen(string) == strlen(arg[1])) {if(0 == strncasecmp(string, arg[1], strlen(string))) {is_equal = true;}} is_equal;}) //! ie, a wrapper to return true if the format equals:
    //    printf("arg[1]=\"%s\", at %s:%d\n", arg[1], __FILE__, __LINE__);
    if(__MiF__cmp("-ops=ccm_matrix")) {
      printOptions__ccm_matrixBased__hp_api_fileInputTight();
    }
    if(__MiF__cmp("-ops=ccm_goldXgold")) {
      printOptions__ccm_twoClusterResults__hp_api_fileInputTight(); 
    }
    if(__MiF__cmp("-ops=distance")) {
      printOptions__simMetric__hp_api_fileInputTight();
    }
    if(__MiF__cmp("-ops=clustAlg")) {
      printOptions__clustAlg__hp_api_fileInputTight();
    }
    if(__MiF__cmp("-ex")) {
      FILE *fileP = stdout;
      fprintf(fileP, "#! Exemple calls are:\n");
      // -----------------------------------------------------------------------
      // -----------------------------------------------------------------------
#define __MiF__constructCMD() ({ \
	uint arg_size = 0; char *listOf_args__2d[10]; char *listOf_args = construct__terminalArgString__fromInput__hp_api_fileInputTight \
	  (/*debug_config__isToCall=*/false,  \
	   stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse, &arg_size, listOf_args__2d \
	   ); \
	assert(listOf_args); assert(strlen(listOf_args)); \
	MF__expand__bashInputArg__hp_api_fileInputTight("", string_desc, listOf_args__2d, arg_size); \
	/*! De-allocate:*/ \
	if(listOf_args) {free_1d_list_char(&listOf_args); listOf_args = NULL;} })
      // -----------------------------------------------------------------------
      // -----------------------------------------------------------------------
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "tut_hp_api_fileInputTight_4_dist_1_Euclid.c":
	const char *file_name = "data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv";
	// ------------------
	const char *stringOf_inputFile_1 = file_name;
	const char *stringOf_inputFile_2 = NULL; // random_file;
	const char *stringOf_exportFile = NULL; //! ie, to "stdout".
	const char *stringOf_exportFile__format = NULL;
	const uint ncluster = UINT_MAX; const char *stringOf_ncluster = NULL;
	const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_correlationFunction_t enum_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	  const char *string_desc = "#! Use-case: similarity of features for metric=Euclid";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_stringOf_enum__e_kt_correlationFunction_t(enum_id);
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_correlationFunction_t enum_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
	  const char *string_desc = "#! Use-case: similarity of features for metric=Kendall-Cosine";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_stringOf_enum__e_kt_correlationFunction_t(enum_id);
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_correlationFunction_t enum_id = e_kt_correlationFunction_groupOf_MINE_mic;
	  const char *string_desc = "#! Use-case: similarity of features for metric=MINE-mic";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_stringOf_enum__e_kt_correlationFunction_t(enum_id);
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
      }
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "tut_hp_api_fileInputTight_8_clustAlg_2_dynamicK_hca.c":
	const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
	const char *stringOf_inputFile_1 = file_name;
	const char *stringOf_inputFile_2 = NULL;
	const char *stringOf_exportFile = NULL; //! ie, to "stdout".
	const char *stringOf_exportFile__format = NULL;
	const char *stringOf_ncluster = NULL;
	const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
	{
	  const char *string_desc = "#! Use-case: cluster-analysis for alg=HCA-max";
	  const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_HCA_max;
	  const char *stringOf_enum = get_stringOf__e_hpLysis_clusterAlg_t(enum_id);
	  const uint ncluster = UINT_MAX;
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  const char *string_desc = "#! Use-case: cluster-analysis for DB-SCAN(kd), where paramters are identifed through application of the default CCM-matrix.";
	  const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
	  const char *stringOf_enum = get_stringOf__e_hpLysis_clusterAlg_t(enum_id);
	  const uint ncluster = 4;
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  const char *string_desc = "#! Use-case: cluster-analysis for k-means-avg";
	  const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_kCluster__AVG;
	  const char *stringOf_enum = get_stringOf__e_hpLysis_clusterAlg_t(enum_id);
	  const uint ncluster = 4;
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  const char *string_desc = "#! Use-case: cluster-analysis for k-means-avg, where clusters are idneitfed through a pre-HCA step";
	  const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_kCluster__AVG;
	  const char *stringOf_enum = get_stringOf__e_hpLysis_clusterAlg_t(enum_id);
	  const uint ncluster = UINT_MAX;
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
      }
      {
	//! Note: [below] example-chunk is manually validted in our "":
      }
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "":
      }
      //! 
      //!  Note: to simploify maitianance, we consturct bash-calsl based on on a funciotn (rather than hard-coding exampels) 8oekseth, 06. otk. 2017): 
      {
	//! Note: [below] example-chunk is manually validted in our "tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.c":
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette; //! ie, the CCM to use.
	const char *stringOf_inputFile_1 = "data/local_downloaded/11_milk.csv.hpLysis.tsv";
	const char *stringOf_inputFile_2 = NULL;
	// const char *stringOf_exportFile = NULL;
	const char *stringOf_exportFile = NULL; //! ie, to "stdout".
	const char *stringOf_exportFile__format = NULL;
	const char *stringOf_ncluster = NULL;
	const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
	{
	  const uint ncluster = UINT_MAX; 
	  const char *string_desc = "#! Use-case: CCM:matrix-based";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  const uint ncluster = 4; 
	  const char *string_desc = "#! Use-case: accuracy of hypothesis (CCM:matrix-based). Assumption: features are split in four (-nclusters=4) parts";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
      }
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "tut_hp_api_fileInputTight_2_ccm_2_matrixHyp_sparseInput.c":
	e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns; //! ie, the CCM to use.
	const char *stringOf_inputFile_1 = "data/fromTutExamples/tut_hp_api_fileInputTight_2_ccm_2_matrixHyp_sparseInput_case1.tsv"; //	const char *file_name = "data/local_downloaded/3_LifeCycleSavings.csv.hpLysis.tsv";
	// const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	//const char *stringOf_inputFile_1 = result_file_1;
	const char *stringOf_inputFile_2 = NULL; //random_file;
	const char *stringOf_exportFile = NULL; //! ie, to "stdout".
	const char *stringOf_exportFile__format = NULL;
	const uint ncluster = UINT_MAX; const char *stringOf_ncluster = NULL;
	const bool inputFile_isSparse = true; const char *stringOf_isSparse = "1";
	{
	  const uint ncluster = UINT_MAX; 
	  const char *string_desc = "#! Use-case(sparse-data): accuracy of hypothesis (CCM:matrix-based). Assumption: features are split in four (-nclusters=4) parts";
	  // const char *string_desc = "#! Use-case: CCM:matrix-based for sparse input-data";
	  // ------------------
	  const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
      }
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "tut_hp_api_fileInputTight_3_ccm_3_hypHyp.c":
	const char *file_name = "data/local_downloaded/34_Cavendish.csv.hpLysis.tsv";
	e_kt_matrix_cmpCluster_metric_t enum_id = e_kt_matrix_cmpCluster_metric_randsIndex;
	const char *stringOf_enum = getStringOf__e_kt_matrix_cmpCluster_metric_t(enum_id);
	const char *stringOf_inputFile_1 = file_name;
	const char *stringOf_inputFile_2 = NULL;
	const char *stringOf_exportFile = NULL; //! ie, to "stdout".
	const char *stringOf_exportFile__format = NULL;
	const uint ncluster = UINT_MAX; const char *stringOf_ncluster = NULL;
	const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
	{
	  const uint ncluster = 4; 
	  const char *string_desc = "#! Use-case: accuracy of hypothesis (CCM:gold-based). Assumption: features describe cluster membership";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  // const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
      }
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "":
      }
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "":
      }
    }
#undef __MiF__cmp
    //!
    //! At this exueicotn-point we asusme the oerpation was a success:
    return true;    
  } else if(arg_size > 2) {
    //! ----------------------------------
    //!
    //! Then identify and evluate the input-parameters: 
#define __MiF__setIfFound(result, para) ({const char *val = strstr(arg[i], para); if(val) {val += strlen(para); result = val;}})
    for(uint i = 1; i < arg_size; i++) {
      // printf("eval: \"%s\", at %s:%d\n", arg[i], __FILE__, __LINE__);
      __MiF__setIfFound(stringOf_enum, stringOf_enum__par);
      //! -------------- 
      __MiF__setIfFound(stringOf_inputFile_1, stringOf_inputFile_1__par);
      __MiF__setIfFound(stringOf_inputFile_2, stringOf_inputFile_2__par);
      //! -------------- 
      __MiF__setIfFound(stringOf_exportFile, stringOf_exportFile__par);
      __MiF__setIfFound(stringOf_exportFile__format, stringOf_exportFile__format__par);
      //! -------------- 
      //! -------------- 
      //!
      //! Add for 'numeric' input-data-cases:
      //! -------------- 
      const char *result = NULL;
      __MiF__setIfFound(result, nclusters__par);       
      if(result) {nclusters = (uint)atoi(result);} //! ie, icnrement 'past' the operation.
      //! -------------- 
      result = NULL; __MiF__setIfFound(result, inputFile_isSparse__par);       
      if(result) {inputFile_isSparse = (bool)atoi(result);} //! ie, icnrement 'past' the operation.      
    }
#undef __MiF__setIfFound
  }
  if( //(__cntParams_set == 0) || 
      (stringOf_inputFile_1 == NULL) || (stringOf_enum == NULL) ) {
    //!
    //! Then we write out the different optiosn described in [ªbove].
    printf("#! The simplified hpLysis interface provides support for:\n");
#define __MiF__printAg(opt, desc) ({if( (opt != NULL) && strlen(opt))  {printf("\t \"%s\": %s\n", opt, desc);} else {printf("%s\n", desc);}})
    //! Write out arguments:
    __MiF__printAg(stringOf_enum__par, "The type of algorithm of metric to apply: write [program-name] and either \"ccm_matrix\", \"ccm_goldXgold\", \"distance\", or \"clustAlg\" to get a list of the correct enum-alternatives");
    __MiF__printAg(stringOf_inputFile_1__par, "The first input-file: (a) for matrix-based CCMs data-set describes the distance-adjacency-matrix; (b) for goldXgold-CCMs the matrix describes a set of hypothesis (where each column-value annotates the 'hypothesized-membership' for a given index); (c) distance && clustAlg: matrix is expected to either be a feature-matrix or a similarity-matrix");
    __MiF__printAg(stringOf_inputFile_2__par, "The second input-matrix: (a) matrix-based CCMs: the hypothesized data-set; (b) goldXgold-CCM: an alternative set of hypothesis to compare the first input-data-set with; (c) distance: an alternative distance-matrix to be used as input: (d) clustAlg: ignored");
    //__MiF__printAg(stringOf_exportFile__par, "The format-type. We expect values to be separated by 'tabs'. Default format a 'dense matrix' (where each index describes the position in the matrix). If 'sparse' is used, we assume a row consists of 'three' (3) columns, where column[0]='row_id', column[1]='col_id', column[2]='distance'");
    __MiF__printAg(stringOf_exportFile__par, "If set denotes the file to used to export the result. Otherwise the \"stdout\" is used for file-export");
    __MiF__printAg(stringOf_exportFile__format__par, "the format of the exported data-set: either \"tsv\", \"js\" or \"json\"");
    __MiF__printAg(nclusters__par, "if set is used to infer the number of clusters. Alternatively the hpLysis 'dynamic k-means' is used to infer clusters (where a CCM is used to identify the optimal nclusters count of clusters)");
    __MiF__printAg(inputFile_isSparse__par, "The format-type. We expect values to be separated by 'tabs'. Default format a 'dense matrix' (where each index describes the position in the matrix). If 'sparse' is used, we assume a row consists of 'three' (3) columns, where column[0]='row_id', column[1]='col_id', column[2]='distance'");
    __MiF__printAg("", "--------------------------------------------");
    __MiF__printAg("-ex", "print example calls to demonstrate use.");
#undef __MiF__printAg
    printf("\n For questions wrt. the hpLysis interface, please contact the senior developer [oekseth@gmail.com].\n");
    //!
    //! At this exueicotn-point we asusme the oerpation was a success:
    return true;
  }
  //! ----------------------------------
  //!
  //! Apply the speicifed logics:
  //  printf("stringOf_inputFile_1=\"%s\", stringOf_inputFile_2=\"%s\", inputFile_isSparse=%d, at %s:%d\n", stringOf_inputFile_1, stringOf_inputFile_2, inputFile_isSparse, __FILE__, __LINE__);
  const bool is_ok = readFromFile__exportResult__hp_api_fileInputTight(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, nclusters, inputFile_isSparse);
  assert(is_ok);
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return is_ok;
}
