#include "parse_main.h"

#include <sys/socket.h>
//! Libraries assosicated to stat function
#include <sys/stat.h>
//! Get libraries required for copying a file into another location, as used in the n_server_inputParser::copy_file(..) function.
#include <unistd.h>
#include <fcntl.h> // which is used to perform actions on various descritpors (eg, socket-descstreamritpors or file-descriptors). Often the library is sued implict. However, sometimes it is practical using it directly, eg, in the context of setting the file-desctiropr: "int oldfl; oldfl = fcntl(sockfd, F_GETFL); if (oldfl == -1) {/* handle error */ } fcntl(sockfd, F_SETFL, oldfl & ~O_NONBLOCK);"
#if defined(__APPLE__) || defined(__FreeBSD__)
#include <copyfile.h>
#else
#include <sys/sendfile.h>
#endif
// #include <cstdlib> // in order to be anble to use the "free(...)" C-memory-deallocation-funciton, required when we are using the "C" "getline(..)" funciton (oekseth, 05. Juni 2015)
#include <stdio.h>
#include <ctype.h>


/**
   @brief test existence of file.
   @param <path_to_file> is the location of the file in question.
   @param <verify_file_is_regular> in order to investigate if the file has the protection which we expect.
   @param <sizeOf_file> is set to the size of the identified file: may be set though a false value is returned.
   @return true if the file exist.
**/
bool file_exists(const char *path_to_file, const bool verify_file_is_regular, uint *sizeOf_file) {
  struct stat sb;
  *sizeOf_file = 0; // ie, intiates the variable in question.
  if(stat(path_to_file, &sb) == 0) {
    assert(sb.st_size >= 0); // ie , what we expect.
    *sizeOf_file = (uint)sb.st_size;
    //return true;
    if(sb.st_mode) { // && (S_IFREG != 0)) {// where we investigate the protection=st_mode, and verifying that the file is regular=S_IFREG
      return true;
    } else if(!verify_file_is_regular) {return true;}
  } 
  return false;
}


/*   /\** */
/*      @brief loads a file into a char-buffer/pointer. */
/*      @param <filePath> is the path to the fiel which is to be laoded into the buffer */
/*      @param <append_newlineAt_endIf_notSet> if set, then we append a newline to the buffer if the buffer does nto end with a newline-seperator */
/*      @return an allcoated char-buffer/pointer holding the string in question. */
/*      @remarks included in this namespace to simplify the set of dependencies. */
/*   **\/ */
/* char *load_file_intoCharBuffer(const char *filePath, const bool append_newlineAt_endIf_notSet) { */
/*   uint file_size = 0;       */
/*   if(file_exists(filePath, true, &file_size)) {	 */
/*     if(file_size > 0) { 	   */
/*       { //! Load the file inot the buffer: */
/* 	FILE *filePointer_input = fopen(filePath, "rb"); */
/* 	if(filePointer_input) { */
/* 	  //! Allocate memory: */
/* 	  assert(false); // FIXEM: update ["elow] */

/* 	  const uint buffer_size = file_size + 10;  */
/* 	  int default_value_alloc = 0; */
/* 	  char *buffer =  allocate_1d_list_char(buffer_size, default_value_alloc); // where the latter offset is an extra offset. */
/* 	  assert(buffer); // ie, what we expect. */
/* 	  if(buffer) { */
/* 	    const int cnt_chars_read = (int)fread(buffer, file_size, sizeof(char), filePointer_input); */
/* 	    assert(cnt_chars_read > 0); // as otherwise would */
/* 	    //printf("\t cnt_chars_read=%u, file_size=%u, buffer=\"%s\", at %s:%d\n", (uint)cnt_chars_read, file_size, buffer, __FILE__, __LINE__); */
/* 	    //assert((uint)cnt_chars_read == file_size); // ie, what we expect	       */
/* 	    if(filePointer_input) {fclose(filePointer_input);} */
/* 	    if(append_newlineAt_endIf_notSet && (strlen(buffer) > 2) ) { */
/* 	      //! Then we are interested in the file ending with a newline-seperator: */
/* 	      if(buffer[strlen(buffer)-1] != '\n') { */
/* 		//! Then we need to append it, ie, top make satsified the parser which the server uses. */
/* 		buffer[strlen(buffer)] = '\n'; */
/* 	      } */
/* 	    } */
/* 	  } */
/* 	  //! @return the result. */
/* 	  return buffer; */
/* 	} else { */
/* 	  fprintf(stderr, "(unable-to-open-file)\tWas not able to open file=\"%s\" at your file-system, ie, please investigate the file-name, and its readability, on your file-system: for questions, please contact the develoeper at [oekseth@gmail.com]. Error generated at [%s]:%s:%d\n", filePath, __FUNCTION__, __FILE__, __LINE__); */
/* 	  if(filePointer_input) {fclose(filePointer_input);} */
/* 	  return NULL; */
/* 	} */
/*       } */
/*     } else { */
/*       fprintf(stderr, "(file-is-empty)\tSeems lke the file=\"%s\" does not contain any data, ie, is empty. Therefore, please investigate the 'readability' of your file (both wrt. the path itself, and the files access-rights). Given the mssing file, we do not see any point continuing, ie, we now aborts the ecuction. If questions, please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", filePath, __FUNCTION__, __FILE__, __LINE__);       */
/*     } */
/*   } else { */
/*     fprintf(stderr, "(file-does-not-exist)\tWas not able to find file=\"%s\". Therefore, please investigate the 'readability' of your file (both wrt. the path itself, and the files access-rights). Given the mssing file, we do not see any point continuing, ie, we now aborts the ecuction. If questions, please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", filePath, __FUNCTION__, __FILE__, __LINE__);       */
/*   } */
/*   return NULL; // ie, as we at this exeuction-point were unable to load the fiel into memory. */
/* } */
/*   /\** */
/*      @brief loads a file into a char-buffer/pointer. */
/*      @param <stream> is the stream to read from, such as stdin  */
/*      @param <append_newlineAt_endIf_notSet> if set, then we append a newline to the buffer if the buffer does nto end with a newline-seperator */
/*      @return an allcoated char-buffer/pointer holding the string in question. */
/*      @remarks  */
/*      - included in this namespace to simplify the set of dependencies. */
/*      - if the stream represents a 'file found on your file-system, we instead would reccomend the use of the "load_file_intoCharBuffer(...)" function */
/*   **\/ */
/* static char *load_stream_intoCharBuffer(FILE *stream, const bool append_newlineAt_endIf_notSet) { */
/*   uint file_size = 0;       */
/*   assert(stream); // ie, as we expect it to have been set. */
/*   if(!stream) {return NULL;} // ie, as it then is noting we may do/complete */

/*   int buffer_currentPos = 0; */
/*   int buffer_size = 128*100; // ie, approx 100 query-lines corresponding to approx 10KB which for most cases is expected to be usfficient for a large query, without causing a (noteworthy) overhead in exeuction-time nor memory consumtpion */

/*   assert(false); // FIXME: update: */
/*   char *buffer = NULL; // n_server_inputParser::allocate_string(buffer_size); // ie, the buffer which we insert the strings of the parsed stream into. */

/*   //! Local varialbe uses in the [below] loop through the input-stream: */
/*   char *lineptr = NULL; size_t lineptr_size = 0; */
/*   uint cnt_read_prev = 0; */
/*   do { */
/*       cnt_read_prev = 0; //! ie, resets. */
/*       lineptr = NULL; //! ie, reset. */
/*     const int cnt_read = (int)getline(&lineptr, &lineptr_size, stream); // which allocates memory while reading the line, including the terminaitng '\n' symbol (if set, ie, what the manual states) (oekseth, 05. Juni 2015). */
/*     if(lineptr && strlen(lineptr) ) { // ie, we asusme inptus was found: */
/*       cnt_read_prev = strlen(lineptr); */
/*       // printf("\t cnt_read=%u, strlenght=%u,  at %s:%d\n", (uint)cnt_read, (uint)strlen(lineptr), __FILE__, __LINE__);  */
/*       //! What we expect: */
/*       assert(lineptr_size); */
/*       assert(strlen(lineptr) <= lineptr_size); // an assumption which should be verfieid. */
/*       //printf("\t string=\"%s\", has a length=%u. while the 'getline(...)' function estimated it having a line-length='%u, at %s:%d\n", lineptr + buffer_currentPos, (uint)strlen(lineptr), (uint)lineptr_size, __FILE__, __LINE__); */
/*       //      assert(strlen(lineptr) == lineptr_size); // an assumption which should be verfieid. */
/*       //! Inveistage if we need to enlarge the size of the/our return-buffeR: */
/*       const int new_size = buffer_currentPos + strlen(lineptr) +1; */

/*       assert(false); // FIXME: update: */

/*       /\* if(new_size >= buffer_size) { *\/ */
/*       /\* 	char *buffer_updated = n_server_inputParser::allocate_string((new_size + 16) * 2); // where the latter offset is an extra offset. *\/ */
/*       /\* 	assert(buffer_updated); // ie, what we expect: otherwisze the [ªbove] 'called' routine is expected to have pritned an informative error.-message *\/ */
/*       /\* 	strncpy(buffer_updated, buffer, buffer_currentPos); // ie, copy the old buffer into the enw buffer. *\/ */
/*       /\* 	delete [] buffer; buffer = buffer_updated; buffer_updated = NULL; // ie, update the poiner, and for explictly clear the 'local' variable. *\/ */
/*       /\* } *\/ */
/*       //! Then the oepration: */
/*       strncpy(buffer + buffer_currentPos, lineptr, strlen(lineptr)); // ie, copy the current line into the 'global' buffer. */
/*       buffer_currentPos += strlen(lineptr); // ie, to avoid overwriting data for the next line to identify/receive: */
/*     } */
/*     if(lineptr) { */
/*       assert(cnt_read); //! ie, as we otherwise would expect htat no variabe is allocated. */
/*       free(lineptr); // where 'free(...) us used instead of the C++-version "delete []" as we assume that the ""getline(...)" function is implemented in C (oekseth, 05. juni 2015). */
/*     } */
/*   } while (cnt_read_prev > 0); */
/*     //  } while (lineptr != NULL); */
  

/*   if(append_newlineAt_endIf_notSet && (strlen(buffer) > 2) ) { */
/*     //! Then we are interested in the file ending with a newline-seperator: */
/*     if(buffer[strlen(buffer)-1] != '\n') { */
/*       //! Then we need to append it, ie, top make satsified the parser which the server uses. */
/*       buffer[strlen(buffer)] = '\n'; */
/*     } */
/*   } */
  
/*   //! @return the result. */
/*   return buffer; */
/* } */





//! Insert a value into a cell, a calue which is either a flaot-number or a string.
void __update_cell(s_dataStruct_matrix_dense_t *obj, const uint word_max_length, const uint row_id, const uint cnt_tabs, const char *word_buffer, const uint pos_in_column_0_prev, const char *input_file) {
  assert(obj); assert(word_buffer); //assert(cnt_tabs > 0);

  if(word_max_length > 0) { //! Then we asusme the first row is a column:
    // printf("cnt_tabs=%d, word_max_length=%u, at %s:%d\n", cnt_tabs, word_max_length, __FILE__, __LINE__);
    if(cnt_tabs == 0) {
      //! Then we 'handle' the case where the comment-row describes the id of the rows, ie, where the column is a string-sdescription of a row:
      assert(pos_in_column_0_prev > 0); //! ie, as we expect at least one value to have been inserted.
      //assert(row_id > 0);
      assert(strlen(word_buffer));
      if(pos_in_column_0_prev < (word_max_length-1)) {
	strncpy(obj->nameOf_rows[row_id], word_buffer, pos_in_column_0_prev);
      } else {
	//fprintf(stderr, "!!\t Unable to include the complete 'word' for string=\"%s\", at [%s]:%s:%d\n", 
	strncpy(obj->nameOf_rows[row_id], word_buffer, word_max_length-1);
      }
      if(false) {
	  /* assert(pos_in_column_0_prev <= 1000); */
	  /* char buffer_local[1000]; strncpy(buffer_local, word_buffer, pos_in_column_0_prev); */
	printf("[%u]=\"%s\" =? \"%s\", at %s:%d\n", row_id, word_buffer, obj->nameOf_rows[row_id], __FILE__, __LINE__);
      }
    } else { //if(cnt_tabs > 0) {
      assert(cnt_tabs != 0); //! ie, what we exepct.
      if(pos_in_column_0_prev > 0) { //! If the cell is a flaot-score, then isnert:
	const t_float val = (t_float)atof(word_buffer);
	//! Note: for cases where the first column is a string-id, we use an column-offset of "-1":
	// printf("[%u][%u] = %f, at %s:%d\n", row_id, cnt_tabs-1, val, __FILE__, __LINE__);
	const t_float value_thresho = (100*1000*1000);
	if(input_file != NULL) {
	  if(val >= value_thresho) {
	    char buffer_local[1000]; strncpy(buffer_local, word_buffer, pos_in_column_0_prev);
	    fprintf(stderr, "!!\t(possible-format-error)\t seems like an odd cell-value in your data-set: [%u][%u] = \"%s\"=%f, at row-id=\"%s\", and where input_file=\"%s\", at %s:%d\n", row_id, cnt_tabs-1, buffer_local, val, obj->nameOf_rows[row_id], input_file, __FILE__, __LINE__);
	  }
	  assert(val < value_thresho); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors (oekseth, 06. mar. 2017).
	  if(true) {
	    assert(pos_in_column_0_prev <= 1000);
	    char buffer_local[1000]; strncpy(buffer_local, word_buffer, pos_in_column_0_prev);
	    printf("[%u][%u] = \"%s\"=%f, at row-id=\"%s\", at %s:%d\n", row_id, cnt_tabs-1, buffer_local, val, obj->nameOf_rows[row_id], __FILE__, __LINE__);
	  }
	} //! else the inptu may be a random valeu, ie, a valeu 'outpassing' any resaonble vlaue-inptus (oekseht, 06. apr. 2017)
	assert(val != INFINITY);       assert(val != NAN);
	obj->matrixOf_data[row_id][cnt_tabs-1] = val; //! ie, set the value, 
      }
    }
  } else { //! then we assuem that all rows are 'numeric', ie, we do not use a 'numeric offset':
    if(false) {printf("[%u][%u]=\"%s\"=%f, at %s:%d\n", row_id, cnt_tabs, word_buffer, atof(word_buffer), __FILE__, __LINE__);}
    if(pos_in_column_0_prev > 0) { //! If the cell is a flaot-score, then isnert:
      // if(true) {printf("[%u][%u]=\"%s\"=%f, at %s:%d\n", row_id, cnt_tabs, word_buffer, atof(word_buffer), __FILE__, __LINE__);}
      const t_float val = (t_float)atof(word_buffer);
      assert(val != INFINITY);       assert(val != NAN);
      //printf("[%u][%u] = %f, at %s:%d\n", row_id, cnt_tabs, val, __FILE__, __LINE__);
      if((uint)val > (1000*1000)) { // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors (oekseth, 06. mar. 2017)
	fprintf(stderr, "(validate-parsing)\t Please validate that the following 'case' is not due to an error in our data-parsing: [%u][%u] = %f for string=\"%s\", at %s:%d\n", row_id, cnt_tabs, val, word_buffer, __FILE__, __LINE__);
      }
      //assert((uint)val < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors (oekseth, 06. mar. 2017)
      //! Note: for cases where the first column is a string-id, we use an column-offset of "-1":
      obj->matrixOf_data[row_id][cnt_tabs] = val; //! ie, set the value, 
    }
  }	  
}

//! Get proeprties assiated to a givne file:
void __get_facts_forFile(FILE *file, const char column_seperator, uint *cnt_rows, uint *cnt_columns, uint *word_max_length) {
  assert(file); assert(column_seperator != '\0'); assert(cnt_rows); assert(cnt_columns); assert(word_max_length);

  bool rowIsOf_interest = false; int cnt_tabs = 0; char prev_char = '\0'; uint pos_in_column_0 = 0;  uint pos_in_column_0_notDigits_andSep = 0;
  // char word_buffer[1000] = {'\0'};
  char c;

  //printf("----- starts-file-reading--------, at %s:%d\n", __FILE__, __LINE__);


  const char *columnString__case1 = "#! legend-id:";
  const char *columnString__case2 = "#! row-name";
  const char *columnString__case3 = "#! row-name:";
	
  bool __fileUse__strings = false;
  
  long int chars_visisted = 0; uint charsIn__currentColumn = 0;
  bool rowDescribes__aColumnHedader = false; //! which is 'used' to hadnle the case wheere the row-names are numbers (ie, where it is Not obivous that the input-dat-set describes both row-naems and column-nmae. (oekseth, 06. feb. 2017).
  //char prev_char = '\0';
  while ((c = getc(file)) != EOF) {
    charsIn__currentColumn++;     chars_visisted++;
    // printf(" c=%c, EOF=%c, -------------, at %s:%d\n", c, EOF, __FILE__, __LINE__);
    if(cnt_tabs == 0) { //! then we are at the first column
      { //! Handle the 'comment' case:


	// printf("-------------, at %s:%d\n", __FILE__, __LINE__);

	if(pos_in_column_0 == 0) {
	  if(c != '#') {rowIsOf_interest = true;} else {rowIsOf_interest = false; __fileUse__strings = true;}
	  // printf("rowIsOf_interest=%u, given c='%c', at %s:%d\n", rowIsOf_interest, c, __FILE__, __LINE__);
	} else if(!rowIsOf_interest && (pos_in_column_0 == 1) ) {
	  if(c != '!') {rowIsOf_interest = true;} else {rowIsOf_interest = false; __fileUse__strings = true;}
	  // printf("rowIsOf_interest=%u, given c='%c', at %s:%d\n", rowIsOf_interest, c, __FILE__, __LINE__);
	}
      }
      //printf("-------------, at %s:%d\n", __FILE__, __LINE__);
      //if( (pos_in_column_0 > 0) ||  ( (prev_char != '#') && (c != '!') ) ) { //! then we asusme the inptu is not a comment
      pos_in_column_0++;
      if( (pos_in_column_0 > 0) && rowIsOf_interest) {
	// printf("possible--char-of-interest='%c', at %s:%d\n", c, __FILE__, __LINE__);
	if(!isdigit(c) && !isspace(c) && (c != '.') ) {
	  // printf("char-of-interest='%c', at %s:%d\n", c, __FILE__, __LINE__);
	  pos_in_column_0_notDigits_andSep++;
	}
	// word_buffer[pos_in_column_0++] = c;
      }
    } else if(cnt_tabs == 1) { //! then we investigate the last word:

      // printf("-------------, at %s:%d\n", __FILE__, __LINE__);

      if(pos_in_column_0_notDigits_andSep > 0) { //! then we asusem the first column is not a number
	if(pos_in_column_0 > *word_max_length) {
	  *word_max_length = pos_in_column_0;
	}
      }
    } 
    if(rowIsOf_interest == false) { //! then we investigat ethe differnet column-attributes spediifed in the comment-column-row-header (oekseth, 06. feb. 2017).
      // printf("------------- cnt_tabs=%u, charsIn__currentColumn=%u, rowDescribes__aColumnHedader=%u, at %s:%d\n", cnt_tabs, charsIn__currentColumn, rowDescribes__aColumnHedader, __FILE__, __LINE__);
      if(cnt_tabs == 0) {
	if( (c != '\t') && ((charsIn__currentColumn == 1) || rowDescribes__aColumnHedader) ) {
	  assert(charsIn__currentColumn >= 1);
	  const uint index_char = charsIn__currentColumn-1;
	  if(columnString__case1[index_char] == c) {rowDescribes__aColumnHedader = true;}
	  else if(columnString__case2[index_char] == c) {rowDescribes__aColumnHedader = true;}
	  else if(columnString__case3[index_char] == c) {rowDescribes__aColumnHedader = true;}
	  else {rowDescribes__aColumnHedader = false;
	    //printf("(\t\t\t not-a-match)\t c='%u', at %s:%d\n", c, __FILE__, __LINE__);
	  }
	}
      } else if(rowDescribes__aColumnHedader && (c == '\t') ) { //! then we assume the 'last positions' represents a string-of-words:
	if(*word_max_length < charsIn__currentColumn) {*word_max_length = charsIn__currentColumn;}	  
	// printf("(\tcolumn-header::size)\t '%u', at %s:%d\n", *word_max_length, __FILE__, __LINE__);
      }
    }
    if(c == '\n') {
      if(__fileUse__strings == false) {cnt_tabs++; charsIn__currentColumn = 0;} //! which is 'added to the lgocis' in order ot handle the case where inptu-file does Not hold/contian a row-hader and column-header (oekseth, 06. mar. 2017)
      // printf("cnt_tabs=%u, pos_in_column_0=%u, at %s:%d\n", cnt_tabs, pos_in_column_0, __FILE__, __LINE__);

      if(rowIsOf_interest) {
	*cnt_rows += 1;
	if( (cnt_tabs > 0) && ( (prev_char == '\t') || (prev_char == ' ') ) ) {
	  cnt_tabs--; //! ie, to avoid inserting 'too many tabs' (oekseth, 06. par. 2018).
	  // printf("\t\t row=%u, cnt_columns=%u, prec_char='%c', at %s:%d\n", *cnt_rows, *cnt_columns, prev_char, __FILE__, __LINE__);
	}
	if(*cnt_columns < cnt_tabs) {
	  *cnt_columns = cnt_tabs;
	  // printf("\t row=%u, cnt_columns=%u, prec_char='%c', at %s:%d\n", *cnt_rows, *cnt_columns, prev_char, __FILE__, __LINE__);
	}
      }
      cnt_tabs = 0;
      pos_in_column_0 = 0; /*ie, reset the latter*/
      pos_in_column_0_notDigits_andSep = 0; charsIn__currentColumn = 0;
      rowDescribes__aColumnHedader = false;
    } else if(c == '\t') {cnt_tabs++; charsIn__currentColumn = 0;} 
    prev_char = c;
  }
  if( (prev_char != '\n') && !isblank(prev_char)) {
    cnt_tabs++; //! ie, as we then assume that the last column 'represents' an end of a line.
    if( (cnt_tabs > 0) && ( (prev_char == '\t') || (prev_char == ' ') ) ) {
      cnt_tabs--; //! ie, to avoid inserting 'too many tabs' (oekseth, 06. par. 2018).
      // printf("\t\t row=%u, cnt_columns=%u, prec_char='%c', at %s:%d\n", *cnt_rows, *cnt_columns, prev_char, __FILE__, __LINE__);
    }

    if(*cnt_columns < cnt_tabs) {
      //printf("\t row=%u, cnt_columns=%u, prec_char='%c', at %s:%d\n", *cnt_rows, *cnt_columns, prev_char, __FILE__, __LINE__);
      *cnt_columns = cnt_tabs;
    }
  }

  if(chars_visisted == 0) {
    fprintf(stderr, "\n!!\t Error:\t\t Seems like your input-file did not have any chars, ie, as the EOF symbol was the first symbol to encountered (during the file-read-phase): could be that your file is Not redable: one example-reason (which we have observed ourself) conserns the case where you have forotten to explcitly close the file-handler (eg, to Not have called the \"closeFileHandler__s_kt_matrix_t(..)\" funcitoni in our s_kt_matrix_t structure-object); for quesiton please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n\n", __FUNCTION__, __FILE__, __LINE__);
  }
  
  /* printf(" c=%c=%d, EOF=%c=%d, -------------, at %s:%d\n", c, (int)c, EOF, (int)EOF, __FILE__, __LINE__); */
  //   printf("### cnt_rows=%u, cnt_columns=%u, cnt_tabs=%u, at %s:%d\n\n\n", *cnt_rows, *cnt_columns, cnt_tabs,__FILE__, __LINE__);
}

  //! Allcoate the matrix:
  // const t_float default_value_float = 0; // TODO: validate correntess fo this wrt. 'empty values'.
  // obj->matrixOf_data = allocate_2d_list_float(obj->cnt_rows, obj->cnt_columns, default_value_float);

//! Load a stream into an "s_dataStruct_matrix_dense_t" object.
//! @return 1 if the operation was a success.
bool parse_matrix_fromStream_tab_usingInitatedObject(s_dataStruct_matrix_dense_t *obj, FILE *file, const uint word_max_length, const char *input_file) {
  assert(obj->cnt_columns > 0);   assert(obj->cnt_rows > 0);
  { //! Load the data:
    
    //! Reset the pointer:
    //(void)fseek(file, 0L, SEEK_SET);
    rewind(file);
    char c;

    uint cnt_tabs = 0; char prev_char = '\0'; uint pos_in_column_0 = 0; 
    //uint pos_in_column_0_prev = 0;
#define __MiC__reset_word_buffer_size 1000
    char word_buffer[__MiC__reset_word_buffer_size] = {'\0'}; //! ie, the biggest word-buffer we expect.
#define __MiF__reset_word_buffer() ({pos_in_column_0 = 0; memset(word_buffer, '\0', 1000);})
    uint max_cnt_Tabs = 0; uint row_id = 0; bool rowIsOf_interest = false; bool rowDescribe_columnHeader = false; uint rowDescribe_columnHeader_cnt = 0; 
    uint currentRow_cnt_isBlank = 0, currentRow_cnt_chars = 0;
    bool __fileUse__strings = false;    
    //printf("### Starts-parsing: at %s:%d\n", __FILE__, __LINE__);
    bool firstCol_isHeader = false; bool firstCol_isHeader_headerIsAdded = false;

    
    while ((c = getc(file)) != EOF) {
      

      
      //! Investitgate if the rows is a 'comment-row':
      if(cnt_tabs == 0) { //! then we are at the first column
	if(pos_in_column_0 == 0) {
	  if(c != '#') {rowIsOf_interest = true;} else {rowIsOf_interest = false;  __fileUse__strings = true;}
	} else if(!rowIsOf_interest && (pos_in_column_0 == 1) ) {
	  if(c != '!') {rowIsOf_interest = true;} else {rowIsOf_interest = false;  __fileUse__strings = true;}
	}
	  /* if(pos_in_column_0 <= 1) { //! then we evaluate wrt. start of the row:	   */
	  /* if( ( (prev_char != '#') && (c != '!') ) ) { //! then we asusme the inptu is not a comment */
	  /*   rowIsOf_interest = true; */
	  /*   printf("(of-interest)\t %c%c, given pos_in_column_0=%u, and buffer=\"%s\", at %s:%d\n", prev_char, c, pos_in_column_0, word_buffer, __FILE__, __LINE__); */
	  /*   // word_buffer[pos_in_column_0++] = c; */
	  /* } else { */
	  /*   printf("(not-of-interest)\t %c%c, given pos_in_column_0=%u, and buffer=\"%s\", at %s:%d\n", prev_char, c, pos_in_column_0, word_buffer, __FILE__, __LINE__); */
	  /* } */
      }

      /* if(row_id == 0) { */
      /* 	printf("%c\t (interesting=%u)\n", c, rowIsOf_interest); */
      /* 	//printf("%c\n", c); */
      /* } */
      
      //! -----------
      if( (c == '\t') || (c == '\n') ) { //! then we insert for the previous 'content':			
	//printf("rowIsOf_interest=%u, at %s:%d\n", rowIsOf_interest, __FILE__, __LINE__);
	if( (rowIsOf_interest == true) 
	    //&& (rowDescribe_columnHeader == false) //! a docndition 'added' to correclty hanlded row-ehaders (oekseth, 06. jan. 2017).
	    ) {
	  if(pos_in_column_0 > 0) {
	    //printf("#\t row is ordinary, where column=\"%s\", cnt_tabs=%u, at %s:%d\n", word_buffer, cnt_tabs, __FILE__, __LINE__);
	    //assert(false);

	    //! Insert a value into a cell, a calue which is either a flaot-number or a string.
	    __update_cell(obj, word_max_length, row_id, cnt_tabs, word_buffer, pos_in_column_0, input_file);
	    //assert(false); // FIXME: remove
	  }
	} else {
	  //printf("#\t test if row is a 'sepcial--purpose' row for word_buffer=\"%s\", at %s:%d\n", word_buffer, __FILE__, __LINE__);
	  //if(cnt_tabs > 0) 
	  {
	    if(cnt_tabs == 0) { //! then we test if the first column describes a column-header:
	      rowDescribe_columnHeader = false; //! ie, reset.
	      //printf("word_buffer=\"%s\", rowDescribe_columnHeader_cnt=%u, at %s:%d\n", word_buffer, rowDescribe_columnHeader_cnt, __FILE__, __LINE__);
	      if(
		 !rowDescribe_columnHeader_cnt 
		 && (
		     strcasestr(word_buffer, "#! legend-id")
		     || strcasestr(word_buffer, "#! row-name")
		     || strcasestr(word_buffer, "#! row name")
		     || strcasestr(word_buffer, "#! Row-name")
		     || strcasestr(word_buffer, "#! row name")
		     ) 
		 ) { //! ie, as found in both our "lib_line_chart.js" and our ANSI C "export_dataSet_s_dataStruct_matrix_dense_tab(..)" function (where the latter is found in our "s_dataStruct_matrix_dense.c" file).
		//printf("\t\t is-header=true, at %s:%d\n", __FILE__, __LINE__);
		rowDescribe_columnHeader = true;
		rowDescribe_columnHeader_cnt++; //! ie, to enusre that this 'case' is invetigated only once.
	      }
	    } else {
	      if(rowDescribe_columnHeader == true) { //! then we 'store' the column-name, an operation whcih is expected to be 'perofrmed' only for one row:
		//! Handle the case where the comment-row describes the id of the columns.
		assert(pos_in_column_0 > 0);
		assert(cnt_tabs > 0);
		assert(pos_in_column_0 <= __MiC__reset_word_buffer_size);
		strncpy(obj->nameOf_columns[cnt_tabs-1], word_buffer, pos_in_column_0);
		if(false) {printf("(set-column)\t \"%s\", at %s:%d\n", obj->nameOf_columns[cnt_tabs-1], __FILE__, __LINE__);}
		//assert(false); // FIXME: remove
	      }
	    }
	  }
	}
      }
      //! -----------
      if(c == '\n') {
	if(__fileUse__strings == false) {cnt_tabs++;} //! ie, an update to 'sovle' the case where a data-input does Not contain/provide a row-header (oekseth, 06. mar. 2017).
	//printf("end-of-row, at %s:%d\n", __FILE__, __LINE__);
	if(rowIsOf_interest) {
	  if(currentRow_cnt_isBlank != currentRow_cnt_chars) {
	    row_id++;
	  }
	  currentRow_cnt_isBlank = 0; currentRow_cnt_chars = 0;
	}
	if( (cnt_tabs > 0) && ( (prev_char == '\t') || (prev_char == ' ') ) ) {
	  cnt_tabs--; //! ie, to avoid inserting 'too many tabs' (oekseth, 06. par. 2018).
	  // printf("\t\t row=%u, cnt_columns=%u, prec_char='%c', at %s:%d\n", *cnt_rows, *cnt_columns, prev_char, __FILE__, __LINE__);
	}

	if(max_cnt_Tabs < cnt_tabs) {
	  max_cnt_Tabs = cnt_tabs;
	}
	cnt_tabs = 0;
	__MiF__reset_word_buffer(); /*ie, reset the latter*/		
      } else if(c == '\t') {cnt_tabs++;  
	//pos_in_column_0_prev = pos_in_column_0; 
	__MiF__reset_word_buffer(); 
	//pos_in_column_0 = 0; 
	currentRow_cnt_isBlank = 0;
      } else {
	assert(pos_in_column_0 < __MiC__reset_word_buffer_size);	
	word_buffer[pos_in_column_0++] = c;
	assert(pos_in_column_0 <= __MiC__reset_word_buffer_size); //! ie, our default assumption: if latter does Not hold then we expect users to update/modify their input-files (oekseht, 06. mar. 2017).
      }
      if(c == ' ') {currentRow_cnt_isBlank++;} //! ie, to 'handle' the set of empty rows.
      prev_char = c;
      currentRow_cnt_chars++;
    }

    if(prev_char != '\n') { //! then we assume that the last row wrongly did not end with a 'newline' ("\n") char: from exmpiracal exerpeicne with the "orthaGogue" software-tool we have often observed usch a case to be foudn in human curated/generated data-sets.
      if( (cnt_tabs > 0) && ( (prev_char == '\t') || (prev_char == ' ') ) ) {
	cnt_tabs--; //! ie, to avoid inserting 'too many tabs' (oekseth, 06. par. 2018).
	// printf("\t\t row=%u, cnt_columns=%u, prec_char='%c', at %s:%d\n", *cnt_rows, *cnt_columns, prev_char, __FILE__, __LINE__);
      }

      if(true) {
	//! Note: we expect only 'this case' to arise for non-header-fiels, ie, as it would be odd 'if such a case' described 'an empty row'.
	if( (currentRow_cnt_isBlank == 0) && (pos_in_column_0 > 0) ) { //! then we asusem that the last 'parsed' column contains interesting chunks of data:
	  //! Insert a value into a cell, a calue which is either a flaot-number or a string.
	  __update_cell(obj, word_max_length, row_id, cnt_tabs, word_buffer, pos_in_column_0, input_file);
	  row_id++;
	  if(max_cnt_Tabs < cnt_tabs) {
	    max_cnt_Tabs = cnt_tabs;
	  }
	}
      } else { //! else we assume that this case idnicates a syntax-error in a users inptu-data-set:
	fprintf(stderr, "!!\t your last row did nto end with a new-line, ie, your last row will be omitted, a case which may result in incosnstnecies in your restuls. Observat at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	assert(false);
      }
    }

    if(row_id <= obj->cnt_rows) {
      if(row_id == 0) {
	// TODO: cosndier to fix ["elow] issue ... ie, to 'handle' the reading of "stdin" streasm ... where challenge is that we need to a-priory know the max-lenght of the input-streams.
	fprintf(stderr, "!!\t seems like all your input-rows where ingored: we have earlier observed such an erorr for input-streams (and not files), ie, if this problem hampers you in your work, then please give the develoeprs an heads-up, ie, cotnact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      }
    }
    if(false) {printf("cnt_rows=%u, cnt_columns=%u, at %s:%d\n", obj->cnt_rows, obj->cnt_columns, __FILE__, __LINE__);}
    //! Update for the Columns:
    assert(max_cnt_Tabs <= obj->cnt_columns);
    assert(max_cnt_Tabs > 0);


    obj->cnt_columns = max_cnt_Tabs; //! ie, to 'oimit' the number of 'blanks' from the count    
    //! Update for the Rows:
    assert(row_id <= obj->cnt_rows);
    assert(row_id > 0);
    
    obj->cnt_rows = row_id; //! ie, to 'oimit' the number of 'blanks' from the count    

    
    // printf("cnt_rows=%u, cnt_cols=%u, at %s:%d\n", obj->cnt_rows, obj->cnt_columns, __FILE__, __LINE__);
  }
  //assert(false);
  //! @return true, ie, as we asusem the oepration as a success.
  return true;
}


//! Load a stream into an "s_dataStruct_matrix_dense_t" object.
//! @return 1 if the operation was a success.
bool parse_matrix_fromStream_tab(s_dataStruct_matrix_dense_t *obj, FILE *file, const uint fractionOf_toAppendWith_sampleData_typeFor_rows, const uint fractionOf_toAppendWith_sampleData_typeFor_columns) {
  assert(file);
  //! Intiate:
  obj->cnt_columns = 0; obj->cnt_rows = 0;
  
  uint word_max_length = 0;
  { //! Count the number of new-lines and coulumn-sizes
    uint cnt_rows = 0; uint cnt_columns = 0;    
    __get_facts_forFile(file, /*column_seperator=*/'\t', &cnt_rows, &cnt_columns, &word_max_length);

    if(fractionOf_toAppendWith_sampleData_typeFor_rows != UINT_MAX) {
      cnt_rows += cnt_rows + fractionOf_toAppendWith_sampleData_typeFor_rows; //! ie, as we then assume that the data-set is to be 'appended' with a sample-data-distribution.
    }
    if(fractionOf_toAppendWith_sampleData_typeFor_columns != UINT_MAX) {
      cnt_columns += cnt_columns + fractionOf_toAppendWith_sampleData_typeFor_columns; //! ie, as we then assume that the data-set is to be 'appended' with a sample-data-distribution.
    }

    // printf("\n ###\n ## cnt_rows=%u, cnt_columns=%u, word_max_length=%u, at %s:%d\n", cnt_rows, cnt_columns, word_max_length, __FILE__, __LINE__);
    // assert(word_max_length > 0); // FIXME: remvoe

    //! We expec the object to (a) have been itnatied though (b) not have been allcoated wrt. emmroy-refrences:
    assert(obj->matrixOf_data == NULL); assert(obj->nameOf_rows == NULL); assert(obj->nameOf_columns == NULL);
    if(!cnt_columns || !cnt_rows) {
      fprintf(stderr, "!!\t Seems like no input-file loaded: pelase vlaidate both the file-path and file-format. If the latter sounds odd then it might be an inconsistency in your configuraiton: if evaluation your API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
    assert(cnt_columns > 0);   assert(cnt_rows > 0);
    //printf("cnt_columnsTo_appendImaginary=%u, at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_columns, __FILE__, __LINE__);
    /* printf("cnt_rowsTo_appendImaginary=%u, at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_rows, __FILE__, __LINE__); */
    //! Allcoate the object:    
    assert(obj->nameOf_columns == NULL); //! ie, as we asusme this object is Not itnatieldied.
    if(false) {printf("cnt_rows=%u, cnt_columns=%u, at %s:%d\n", cnt_rows, cnt_columns, __FILE__, __LINE__);}
    init_s_dataStruct_matrix_dense_t(obj, cnt_rows, cnt_columns, word_max_length);
    if(false) {printf("cnt_rows=%u, cnt_columns=%u, at %s:%d\n", cnt_rows, cnt_columns, __FILE__, __LINE__);}
    assert(obj->cnt_columns > 0);   assert(obj->cnt_rows > 0);

    /* if(word_max_length > 0) { */
    /*   //! Then we allcoate the char-buffers: */
    /*   const uint new_word_size = word_max_length+1; */
    /*   const char default_value_alloc = '\0'; */
    /*   obj->nameOf_rows = allocate_2d_list_char(obj->cnt_rows, new_word_size, default_value_alloc); */
    /*   obj->nameOf_columns = allocate_2d_list_char(obj->cnt_columns, new_word_size, default_value_alloc); */
    /* } */
  }
  //! The data-loading:
  const bool is_ok = parse_matrix_fromStream_tab_usingInitatedObject(obj, file, word_max_length, /*input_file=*/NULL);

  // printf("cnt_rows=%u, cnt_cols=%u, at %s:%d\n", obj->cnt_rows, obj->cnt_columns, __FILE__, __LINE__);

  //! @return true, ie, as we asusem the oepration as a success.
  return is_ok;
}


//! Load the contents of a file into an "s_dataStruct_matrix_dense_t" object:
//! @return 1 if the operation was a success.
bool parse_matrix_fromFile_tab(s_dataStruct_matrix_dense_t *obj, const char *file_name, const uint fractionOf_toAppendWith_sampleData_typeFor_rows, const uint fractionOf_toAppendWith_sampleData_typeFor_columns) {
  assert(obj);
  FILE *file = fopen(file_name, "r");
  if (file == NULL) {
    fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }

  // printf("-------- file-name=\"%s\", at %s:%d\n", file_name, __FILE__, __LINE__);
  
  //! Laod a stream into an "s_dataStruct_matrix_dense_t" object:
  const bool ret_val = parse_matrix_fromStream_tab(obj, file, fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns);

  //  printf("cnt_rows=%u, cnt_cols=%u, at %s:%d\n", obj->cnt_rows, obj->cnt_columns, __FILE__, __LINE__);

  //! Close the input-file:
  fclose(file);
  //! @return true, ie, as we asusem the oepration as a success.
  return ret_val;
    
}


/**
   @brief load a data-set into the data_objh, either using input-data or a value-range-distribution defined by our "stringOf_sampleData_type" param.
   @param <data_obj> the object to hold the new-cosntructed data-set: if set to a string-value, the other function-arpameters are ginored.
   @param <input_file> which if set ideitnfied the input-file to use
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <readData_fromStream> which if set implies that the data-'foudn' in "stream" is used to buidl the data-matrix: has prioerty over the stringOf_sampleData_type funciton-parameter.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @param <fractionOf_toAppendWith_sampleData_typeFor_rows> identifies the percentage of sampel-data-dsitribiton rows to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <fractionOf_toAppendWith_sampleData_typeFor_columns> identifies the percentage of sampel-data-dsitribiton columns to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <stringOf_sampleData_type_realLife> which if set is used to idnentify 'in-line' real-life data-set which are to be used.
   @param <isTo_transposeMatrix> which if set to true implies that we transpose the matrix.
   @return true on success.
 **/
bool build_dataset_fromInputOrSample(s_dataStruct_matrix_dense_t *data_obj, const char *input_file, uint cnt_rows, uint cnt_columns, const bool readData_fromStream, const char *stringOf_sampleData_type, uint fractionOf_toAppendWith_sampleData_typeFor_rows,  uint fractionOf_toAppendWith_sampleData_typeFor_columns, const char *stringOf_sampleData_type_realLife, const bool isTo_transposeMatrix) {  
  //bool isTo_appendRealLife_datasetsWith_sampleDatasets = false;
  if(cnt_rows == UINT_MAX) {cnt_rows = 0;}   if(cnt_columns == UINT_MAX) {cnt_columns = 0;}
  uint cnt_rowsTo_apppend = 0;   uint cnt_rows_local = cnt_rows; uint cnt_rowsTo_appendImaginary = 0;
  uint cnt_columnsTo_apppend = 0;   uint cnt_columns_local = cnt_columns; uint cnt_columnsTo_appendImaginary = 0;
  /* assert(cnt_rows != UINT_MAX); */
  /* assert(cnt_columns != UINT_MAX); */

  if(stringOf_sampleData_type_realLife != NULL) {
    fprintf(stderr, "!!\t A dpericated option: pelase do Not use the \"build_dataset_fromInputOrSample\" option: instead use the \"input_file\" option, an observiaont aat %s:%d\n", __FILE__, __LINE__);
    assert(false); // i1, as this option is no longer supported.
  }

  //printf("cnt_columns=%u, at %s:%d\n", cnt_columns, __FILE__, __LINE__);

  //if(stringOf_sampleData_type && strlen(stringOf_sampleData_type)) {
  /* printf("fractionOf_toAppendWith_sampleData_typeFor_rows=%u, at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_rows, __FILE__, __LINE__); */
  /* printf("fractionOf_toAppendWith_sampleData_typeFor_columns=%u, at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_columns, __FILE__, __LINE__); */

  if(isTo_transposeMatrix) { //! then we 'swap' the 'order of allocation':
    const uint tmp = fractionOf_toAppendWith_sampleData_typeFor_rows;
    fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_columns;
    fractionOf_toAppendWith_sampleData_typeFor_columns = tmp;
  }

  
  //printf("fractionOf_toAppendWith_sampleData_typeFor=[%u, %u] and matrix-dims=[%u, %u], at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns, cnt_rows, cnt_columns, __FILE__, __LINE__);

  //  printf("cnt_rowsTo_appendImaginary=%u, cnt_columnsTo_appendImaginary=%u, at %s:%d\n", cnt_rowsTo_appendImaginary, cnt_columnsTo_appendImaginary, __FILE__, __LINE__);
  
  if(fractionOf_toAppendWith_sampleData_typeFor_rows != 0) {
    // printf("fractionOf_toAppendWith_sampleData_typeFor=[%u, %u], at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns, __FILE__, __LINE__);
    
    cnt_rowsTo_appendImaginary = cnt_rows_local * fractionOf_toAppendWith_sampleData_typeFor_rows*0.01; //! ie, where "*0.01" is as we expec the "fractionOf_toAppendWith_sampleData_typeFor_rows" to describe 'per-centage' and not 'actual increase'.

    if(cnt_rowsTo_appendImaginary == 0) {cnt_rowsTo_appendImaginary = 1;} //! ie, to 'correct' wrt. the 'appending', eg, fit he 'input-data-chunk' is too small.    
  }
  // printf("cnt_rowsTo_appendImaginary=%u, at %s:%d\n", cnt_rowsTo_appendImaginary, __FILE__, __LINE__); 

  if(input_file == NULL) {
    // if(fractionOf_toAppendWith_sampleData_typeFor_rows != 0) 
    {
      cnt_rows += cnt_rowsTo_appendImaginary;
      //isTo_appendRealLife_datasetsWith_sampleDatasets = true;
    }
  }


  if(fractionOf_toAppendWith_sampleData_typeFor_columns != 0) {
    //printf("fractionOf_toAppendWith_sampleData_typeFor=[%u, %u], at %s:%d\n", fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns, __FILE__, __LINE__);

    cnt_columnsTo_appendImaginary = cnt_columns_local * fractionOf_toAppendWith_sampleData_typeFor_columns*0.01; //! ie, where "*0.01" is as we expec the "fractionOf_toAppendWith_sampleData_typeFor_columns" to describe 'per-centage' and not 'actual increase'.
    if(cnt_columnsTo_appendImaginary == 0) {cnt_columnsTo_appendImaginary = 1;} //! ie, to 'correct' wrt. the 'appending', eg, fit he 'input-data-chunk' is too small.
  }


  // printf("allocate cnt-columns=%u+%u, given fractionOf_toAppendWith_sampleData_typeFor_columns=%u and cnt_columns_local=%u, at %s:%d\n", data_obj->cnt_columns, cnt_columnsTo_appendImaginary, fractionOf_toAppendWith_sampleData_typeFor_columns, cnt_columns_local, __FILE__, __LINE__);

  //printf("cnt_columnsTo_appendImaginary=%u, at %s:%d\n", cnt_columnsTo_appendImaginary, __FILE__, __LINE__); 

  if(input_file == NULL) {
    //if(fractionOf_toAppendWith_sampleData_typeFor_ != 0) 
    {
      cnt_columns += cnt_columnsTo_appendImaginary;
      //isTo_appendRealLife_datasetsWith_sampleDatasets = true;
    }
  }

  if( (cnt_rows == 0) && (cnt_columns == 0) ) {
    if( (input_file == NULL) ) {
      cnt_rows = 10; cnt_columns = 10;
      cnt_rowsTo_appendImaginary = 10;
      cnt_columnsTo_appendImaginary = 10;
      //! Then wrtie a warning statig 'what we have done' (oekseth, 06. des. 2016):
      fprintf(stderr, "!!\t(Note): For input_file=\"NULL\" you have enither specified any matrix-diemsions Nor any specific number of columsn to append to the matrix, ie, tour tmerinal-configruaiton is ambigous. To handle this case we for simplict has set the matrix-dimesniosn to nrows=%u and ncols=%u, ie, as we assume your terminal-call is an 'experimental black-box-calls'. In brief please forward any quesitons to the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_rows, cnt_columns,  __FUNCTION__, __FILE__, __LINE__);
    }
  }

  // printf("cnt_rowsTo_appendImaginary=%u, at %s:%d\n", cnt_rowsTo_appendImaginary, __FILE__, __LINE__); 

  //} //! else we need to 'wait' until we know the number of rows in the input-file.

  /* if(stringOf_sampleData_type_realLife && strlen(stringOf_sampleData_type_realLife)) { */
  /*   const char *current_source_dir = CMAKE_CURRENT_SOURCE_DIR; //! which is expect to be explcitly defined in our "CmakeLists.txt" fle */
  /*   if(!current_source_dir || !strlen(current_source_dir)) { */
  /*     fprintf(stderr, "!!\t Seems like a bug in the compialtion, ie, as the CMAKE_CURRENT_SOURCE_DIR macro-param is not defined: please investgiate your CmakeLists.txt file wrt. compialtion-params. For quesitons please cotnact the devleoepr at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
  /*     return 0; */
  /*   } */
    
  /*   //#define stringOfExample_dir CMAKE_CURRENT_SOURCE_DIR ## sample_data */
  /*   const char *stringOfExample_dir_folderName = "sample_data/"; */

  /*   printf("stringOfExample_dir=%s, at %s:%d\n", CMAKE_CURRENT_SOURCE_DIR, __FILE__, __LINE__); */
  /*   // */
  /*   assert(false); // FIXME: update [ªbove] ... valdiateing our assumption. */

  /*   //! Allcoate the object: */
  /*   // TODO: consider to 'make' [below] dynamic ... ie, to ...??.. (oekseth, 06. july 2016). */
  /*   const uint word_max_length = 1000; //! ie, our assumption, an assumption we expect 'to hold' for the set of inptud-ata.     */
  /*   init_s_dataStruct_matrix_dense_t(data_obj, /\*cnt_rows=*\/cnt_rows, /\*cnt_columns=*\/cnt_columns, word_max_length); */

    
  /*   assert(false); // FIXME: write a structure to idneitfy both the row-size, column-size and file-name ... and inlcud the fiels in the repo */

  /*   if(!cnt_columns || !cnt_rows) { */
  /*     fprintf(stderr, "!!\t Your did not specify the maximum number of rows and column in your input-data-set, ie, please investigate. To smootth out the work, we will abort the exeuction. For questions please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
  /*     return 0; */
  /*   } */

  /*   assert(false); // FIXME: add something. */

  /*   assert(input_file != NULL); */
  /* } */

  bool useSynteticData_as_input = false;
  if(input_file != NULL) {
    //! Then read the inptu-file into dense matrix-structurure:

    // cnt_rowsTo_appendImaginary = data_obj->cnt_rows * fractionOf_toAppendWith_sampleData_typeFor_rows*0.01; //! ie, where "*0.01" is as we expec the "fractionOf_toAppendWith_sampleData_typeFor_rows" to describe 'per-centage' and not 'actual increase'.
    // cnt_columnsTo_appendImaginary = data_obj->cnt_columns * fractionOf_toAppendWith_sampleData_typeFor_columns*0.01; //! ie, where "*0.01" is as we expec the "fractionOf_toAppendWith_sampleData_typeFor_columns" to describe 'per-centage' and not 'actual increase'.
    // printf("allocate cnt-columns=%u+%u, at %s:%d\n", data_obj->cnt_columns, cnt_columnsTo_appendImaginary, __FILE__, __LINE__);
    //assert(data_obj->cnt_columns > 0);
    // printf("cnt_rowsTo_appendImaginary=%u, at %s:%d\n", cnt_rowsTo_appendImaginary, __FILE__, __LINE__);
    /* printf("cnt_rows=%u, at %s:%d\n", data_obj->cnt_rows, __FILE__, __LINE__); */
    parse_matrix_fromFile_tab(data_obj, input_file, cnt_rowsTo_appendImaginary, cnt_columnsTo_appendImaginary);

    // printf("cnt_rows=%u, at %s:%d\n", data_obj->cnt_rows, __FILE__, __LINE__);
    //! Update locally the count:
    cnt_rows_local = data_obj->cnt_rows;     cnt_columns_local = data_obj->cnt_columns;
    //parse_matrix_fromFile_tab(data_obj, input_file, fractionOf_toAppendWith_sampleData_typeFor_rows, fractionOf_toAppendWith_sampleData_typeFor_columns);
    /* printf("(before)\tcnt_rows_local=%u, cnt_rowsTo_appendImaginary=%u, cnt_rows=%u, at %s:%d\n", cnt_rows_local, cnt_rowsTo_appendImaginary, data_obj->cnt_rows, __FILE__, __LINE__); */
    //printf("cnt_columns_local=%u, at %s:%d\n", cnt_columns_local, __FILE__, __LINE__);
    /* printf("(before)\tcnt_columns_local=%u, cnt_columnsTo_appendImaginary=%u, cnt_columns=%u, at %s:%d\n", cnt_columns_local, cnt_columnsTo_appendImaginary, data_obj->cnt_columns, __FILE__, __LINE__); */
    
  } else if(readData_fromStream != 0) {
    if(!cnt_columns || !cnt_rows) {
      fprintf(stderr, "!!\t Your did not specify the maximum number of rows and column in your input-data-set, ie, please investigate. To smootth out the work, we will abort the exeuction. For questions please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return 0;
    }
    //! Allcoate the object:
    // TODO: consider to 'make' [below] dynamic ... ie, to ...??.. (oekseth, 06. july 2016).
    const uint word_max_length = 1000; //! ie, our assumption, an assumption we expect 'to hold' for the set of inptud-ata.
    // printf("--------, cnt_rows=%u, cnt_columns=%u, at %s:%d\n", cnt_rows, cnt_columns, __FILE__, __LINE__);
    init_s_dataStruct_matrix_dense_t(data_obj, /*cnt_rows=*/cnt_rows, /*cnt_columns=*/cnt_columns, word_max_length);
    //! Then laod data from the inptu-stread into dense matrix-structurure:
    const bool is_ok = parse_matrix_fromStream_tab_usingInitatedObject(data_obj, /*stream=*/stdin, word_max_length, input_file);
    if(is_ok == false) {
      fprintf(stderr, "!!\t Seems like no input-file loaded: pelase vlaidate both the file-path and file-format. If the latter sounds odd then it might be an inconsistency in your configuraiton: if evaluation your API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
      return false;
    }
  } else {
    //! Construct the sampe-data-set:
    //printf("(after)\tcnt_rows=%u, cnt_columns=%u, at %s:%d\n", cnt_rows, cnt_columns, __FILE__, __LINE__);
    const bool is_ok = constructSample_data_identifiedFromString(data_obj, /*cnt_rows=*/cnt_rows, /*cnt_columns=*/cnt_columns, stringOf_sampleData_type);
    assert(data_obj->matrixOf_data);
    for(uint i = 0; i < cnt_rows; i++) {
      assert(data_obj->matrixOf_data[i]); //! ie, as we expect all rows to have been itnaited.
    }
    if(is_ok == false) {
      fprintf(stderr, "!!\t Your specificaitoni of data-input-tyeps did not conform to oru expectations. To smooth out the work, we will abort the exeuction. For questions please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return 0;
    }        
    useSynteticData_as_input = true;
  }  
  
  // printf("(before)\tcnt_rows_local=%u, cnt_rowsTo_appendImaginary=%u, cnt_rows=%u, at %s:%d\n", cnt_rows_local, cnt_rowsTo_appendImaginary, data_obj->cnt_rows, __FILE__, __LINE__);
  
  
  if(useSynteticData_as_input == false) {    
    //! Update:
    data_obj->cnt_rows += cnt_rowsTo_appendImaginary; //! ie, as we expec the 'number of rows' to have been set
    data_obj->cnt_columns += cnt_columnsTo_appendImaginary; //! ie, as we expec the 'number of rows' to have been se
  }
  if( (data_obj->cnt_rows > 0)  && (data_obj->cnt_columns > 0) ) {
    if(isTo_transposeMatrix) {
      s_dataStruct_matrix_dense_transpose(data_obj); //! ie, then we transpose the input-matrix.
    }
  }

  //printf("\n\n(after)\tcnt_rows_local=%u, cnt_rows=%u, at %s:%d\n", cnt_rows_local, data_obj->cnt_rows, __FILE__, __LINE__);

  if(useSynteticData_as_input == true) {
    data_obj->cnt_rows = cnt_rows; //! ie, as we expec the 'number of rows' to have been set
    data_obj->cnt_columns = cnt_columns; //! ie, as we expec the 'number of rows' to have been se
    return true; //! ie, as we then assume taht ["elow] operiaton is poitnless.
  }

  if( (fractionOf_toAppendWith_sampleData_typeFor_rows != 0) || (fractionOf_toAppendWith_sampleData_typeFor_columns != 0) )  {
    //if(isTo_appendRealLife_datasetsWith_sampleDatasets) {
    // assert(cnt_rowsTo_appendImaginary > 0); //! ei, what we expect:
    // if(cnt_columnsTo_appendImaginary == 0) {cnt_columnsTo_appendImaginary = 1;} //! ie, to 'correct' wrt. the 'appending', eg, fit he 'input-data-chunk' is too small.
    /* printf("(before)\tcnt_rows_local=%u, cnt_rowsTo_appendImaginary=%u, cnt_rows=%u, at %s:%d\n", cnt_rows_local, cnt_rowsTo_appendImaginary, data_obj->cnt_rows, __FILE__, __LINE__); */
    /* printf("(before)\tcnt_columns_local=%u, cnt_columnsTo_appendImaginary=%u, cnt_columns=%u, at %s:%d\n", cnt_columns_local, cnt_columnsTo_appendImaginary, data_obj->cnt_columns, __FILE__, __LINE__); */
    bool is_ok = false;
    //if(isTo_transposeMatrix == false) {
    
    // printf("(before)\tcnt_rows_local=%u, cnt_rowsTo_appendImaginary=%u, cnt_rows=%u, at %s:%d\n", cnt_rows_local, cnt_rowsTo_appendImaginary, data_obj->cnt_rows, __FILE__, __LINE__);

    /* data_obj->cnt_rows += cnt_rowsTo_appendImaginary; //! ie, as we expec the 'number of rows' to have been set */
    /* data_obj->cnt_columns += cnt_columnsTo_appendImaginary; //! ie, as we expec the 'number of rows' to have been se */
    /* printf("iterates in row-space [%u, %u], and isTo_transposeMatrix=%u, at %s:%d\n", cnt_rows_local, data_obj->cnt_rows, isTo_transposeMatrix, __FILE__, __LINE__); */
    /* printf("iterates in column-space [%u, %u], and isTo_transposeMatrix=%u, at %s:%d\n", cnt_columns_local, data_obj->cnt_columns, isTo_transposeMatrix, __FILE__, __LINE__); */
    //! Update:

      
    // printf("stringOf_sampleData_type=\"%s\", at %s:%d\n", stringOf_sampleData_type, __FILE__, __LINE__);
      
    if(stringOf_sampleData_type == NULL) {
      stringOf_sampleData_type = "uniform"; //! i,e our default asusmption.
    }
    if(isTo_transposeMatrix == false) {
      assert(cnt_rows_local <= data_obj->cnt_rows); //! ie, what we expect.
      assert(cnt_columns_local <= data_obj->cnt_columns); //! ie, what we expect.
      is_ok  = constructSample_data_identifiedFromString(data_obj, /*cnt_rows=*/cnt_rows_local, /*cnt_columns=*/cnt_columns_local, stringOf_sampleData_type);
    } else {
      assert(cnt_rows_local <= data_obj->cnt_columns); //! ie, what we expect.
      assert(cnt_columns_local <= data_obj->cnt_rows); //! ie, what we expect.
      is_ok  = constructSample_data_identifiedFromString(data_obj, /*cnt_rows=*/cnt_columns_local, /*cnt_columns=*/cnt_rows_local, stringOf_sampleData_type);
    }
    //printf("-------- compelted, at %s:%d\n", __FILE__, __LINE__); 
    /* } else { */
    /*   data_obj->cnt_columns += cnt_rowsTo_appendImaginary; //! ie, as we expec the 'number of rows' to have been set */
    /*   data_obj->cnt_rows += cnt_columnsTo_appendImaginary; //! ie, as we expec the 'number of rows' to have been set */
    /*   //! Update: */
      
    /*   // printf("stringOf_sampleData_type=\"%s\", at %s:%d\n", stringOf_sampleData_type, __FILE__, __LINE__); */
      
    /*   is_ok  = constructSample_data_identifiedFromString(data_obj, /\*cnt_rows=*\/cnt_rows_local, /\*cnt_columns=*\/cnt_columns_local, stringOf_sampleData_type); */
    /* } */

    if(is_ok == false) {
      fprintf(stderr, "!!\t Your specificaitoni of data-input-tyeps did not conform to oru expectations. To smooth out the work, we will abort the exeuction. For questions please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return 0;
    }        
    //printf("(after)\tcnt_rows_local=%u, cnt_rows=%u, at %s:%d\n", cnt_rows_local, data_obj->cnt_rows, __FILE__, __LINE__);
  }



  //! At this exeuciton-poitn we asusme the oepraiton was a success.
  return 1;
}
