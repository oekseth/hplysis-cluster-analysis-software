{
  assert(objCaller_postProcessResult);
  // t_float *result = objCaller_postProcessResult->listOf_result_afterCorrelation; //! ie, the list to update.
  // assert(result);
  //assert((SM % 4) == 0); //! otehrwise udpate [below].
  // assert(false); // FIXME: sub-divide [below] into 'sperate squares'.	    
	    
  const uint chunkSize_index2_iterSize = (chunkSize_index2 >= 256) ? macro__get_maxIntriLength_float(chunkSize_index2) : 0; //! where we use an 'extra-voerhead-thresohld' givne teh comptuational overheaf wrt. SSE-functinos (oekseth, 06. sept. 2016).

  t_float scalar_minVal = ((s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)objCaller_postProcessResult)->return_distance;
  VECTOR_FLOAT_TYPE vec_minVal = VECTOR_FLOAT_SET1(scalar_minVal);

  for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
    // const uint global_index1 = index1 + local_index1; 
    uint local_index2 = 0;
    if(chunkSize_index2_iterSize > 0) {
      // VECTOR_FLOAT_TYPE vec_result_inner = VECTOR_FLOAT_SET1(0);
      for(; local_index2 < chunkSize_index2_iterSize; local_index2 += VECTOR_FLOAT_ITER_SIZE) {
	//const uint global_index2 = index2 + local_index2;		  
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&listOf_inlineResults[local_index1][local_index2]);	
	vec_minVal = VECTOR_FLOAT_MIN(vec_minVal, vec_input);
      }
    }
    for(; local_index2 < chunkSize_index2; local_index2++) {
      if(scalar_minVal > listOf_inlineResults[local_index1][local_index2]) {
	scalar_minVal = listOf_inlineResults[local_index1][local_index2];
      }
    }
  }
  //! Merge the min-values identifed from our SSE-idnetificaiotn with the non-SSE-iterations:
  const t_float scalar_minVal_alt = VECTOR_FLOAT_storeAnd_horizontalMin(vec_minVal);
  if(scalar_minVal < scalar_minVal_alt) {scalar_minVal = scalar_minVal_alt;}
  //! Update the 'glboal max-val-score':
  ((s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)objCaller_postProcessResult)->return_distance = scalar_minVal;
}
