#ifndef aux_sysCalls_h
#define aux_sysCalls_h
/**
   @file aux_sysCalls
   @brief provide generic routiens for system-calls (oekseth, 06. feb. 2017)   
   @remarks inteded to simplify support of corss-operating-system applcaibiltiy.
 **/


#include <stdio.h>
#include <dirent.h>

//! A wrapper to correclty handle reutnr-value of systme-calls (oekseth, 06. jul. 2017).
#define MF__system(string_call) ({const int ret_val = system(string_call); if(ret_val == -1) {fprintf(stderr, "!!\t Was Not able to exeucte command=\"%s\", ie, please investigate. Observiaont at [%s]:%s:%d\n", string_call, __FUNCTION__, __FILE__, __LINE__);}})

/**
   @brief a generic approach to delete all files in a folder (oekseth, 06. feb. 2017)
   @param <pathToFolder> is the folder to remove files from
   @return true upon scucess
   @remarks the motivaiton behind 'this funciotn' is the observiaotn that (a) the "rm folder/\*" command is unable to work for large direcotry-setss and (b) to avoid our appraoch to be strongly biased towards Linux/Bsh. 
**/
static bool removeFiles_inFolder__aux_sysCalls(const char *pathToFolder) {
  if(!pathToFolder || !strlen(pathToFolder)) {return false;} //! ie, as the path was Not prodived.
  const int result = access(pathToFolder, W_OK);
  if (result != 0) {
    fprintf(stderr, "!!\t Unable to write to folder=\"%s\", ie, please vliadate the altter. Observaiton at [%s]:%s:%d\n", pathToFolder, __FUNCTION__, __FILE__, __LINE__);
    //assert(false);
    return false; //! ie, errornous path.
  } else {     //! 'Empty' the current oflder:      
    /* {  */
    /*   char str[2000]; sprintf(str, "find %s -name \"*\" -print0 | xargs -0 rmrm -f %%s*", self->pathTo_localFolder_storingEachSimMetric);   */
    /* 	  //char str[2000]; sprintf(str, "rm -f %s*", self->pathTo_localFolder_storingEachSimMetric);   */
    /* 	  fprintf(stderr, "(info)\t Clears the file-content in the direotyr to store resutls in \"%s\", at %s:%d\n", str, __FILE__, __LINE__); */
    /* 	  system(str); } */
    /*   } */
    //! 
    //! Src: "http://stackoverflow.com/questions/11007494/how-to-delete-all-files-in-a-folder-but-not-delete-the-folder-using-nix-standar"
    DIR *theFolder = opendir(pathToFolder);
    struct dirent *next_file;
    char filepath[260];
    
    const char last_char_inDir = pathToFolder[strlen(pathToFolder)-1];
    
    while ( (next_file = readdir(theFolder)) != NULL )  {
      if (0==strcmp(next_file->d_name, ".") || 0==strcmp(next_file->d_name, "..")) { continue; } //! ie, to avoid deleitng 'refernce-pointers':
      // build the path for each file in the folder      
      if(last_char_inDir == '/') {
	sprintf(filepath, "%s%s", pathToFolder, next_file->d_name);
      } else {
	sprintf(filepath, "%s/%s", pathToFolder, next_file->d_name);
      }
      //printf("remove: %s, at %s:%d\n", filepath, __FILE__, __LINE__);
      remove(filepath);
    }
    closedir(theFolder);
    return true;
  }
}

/* static int do_mkdir(const char *path, mode_t mode) */
/* { */
/*   struct stat st; */
/*   int         status = 0; */

/*   if (stat(path, &st) != 0) */
/*     { */
/*       /\* Directory does not exist. EEXIST for race condition *\/ */
/*       if (mkdir(path, mode) != 0 && errno != EEXIST) */
/* 	status = -1; */
/*     } */
/*   else if (!S_ISDIR(st.st_mode)) */
/*     { */
/*       errno = ENOTDIR; */
/*       status = -1; */
/*     } */

/*   return(status); */
/* } */



/* /\** */
/*    @brief a generic approach to delete all files in a folder (oekseth, 06. feb. 2017) */
/*    @param <pathToFolder> is the folder to remove files from */
/*    @return true upon scucess */
/*    @remarks the motivaiton behind 'this funciotn' is the observiaotn that (a) the "rm folder/\\*" command is unable to work for large direcotry-setss and (b) to avoid our appraoch to be strongly biased towards Linux/Bsh.  */
/* **\/ */
/* static bool mkdir__removeFiles_inFolder__aux_sysCalls(const char *pathToFolder) { */
/*   if(!pathToFolder || !strlen(pathToFolder)) {return false;} //! ie, as the path was Not prodived. */
/*   const int result = access(pathToFolder, W_OK); */
/*   if (result != 0) { //! then we create the directory:   */
/* } */

#endif
