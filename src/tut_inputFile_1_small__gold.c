#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "matrixdata.h" //! which is sued for the Vilje-matrix-data-loading in our "tut_inputFile_6.c" (eosekth, 60. feb. 2017).

/**
   @brief examplify an appraoch to evlauate differnet appraoches for clsutering (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks examplfies logics wrt.:
   -- (a) inpuut: how a matrix may be loaded 'directly' into the 'our extenvie evlauation-pipeline'
   -- (b) itniatinon and cofnigruation: how the "traverse__s_hp_clusterFileCollection_traverseSpec" funciton may be sued in cobmaitnion with "s_hp_clusterFileCollection_traverseSpec_t" in our "hp_clusterFileCollection.h" to analyse the realtionships between data, cluster-algorithms, simalirty-metics and clsuter-comparison-emtics (ccms).
   -- (c) configuraiton: how the "s_hp_clusterFileCollection" may be sued to speify different sets of input-data.
   -- (d) cold-standard-clsutering: we specify 'assumed clsuters' using the "mapOf_vertexClusterId" list.
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  //! ----------------------------------- 
  //! 
  //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
  //! ----------------------------------- 
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.

  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  if(true) {
    fileRead_config__syn.imaginaryFileProp__nrows = 10;
    fileRead_config__syn.imaginaryFileProp__ncols = 10;
  } else { //! then we are interested in a more performacne-demanind approach: 
    fileRead_config__syn.imaginaryFileProp__nrows = 400;
    fileRead_config__syn.imaginaryFileProp__ncols = 400;
  }
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  s_kt_matrix_fileReadTuning_t fileRead_config__syn_latency = fileRead_config__syn;
  //!
  //! Itniate a data-structure:
  const uint nrows = 10; const uint ncols = 10;
  s_kt_matrix_t matrix_concatToAll = initAndReturn__s_kt_matrix(nrows, ncols);
  const uint empty_0 = 0;
  uint *mapOf_vertexClusterId = allocate_1d_list_uint(nrows, empty_0);
  //!
  { //! Sepfiy a set of clsutere-mbershisp, ie, to simplfy 'mapping' of default scores:
    const uint cnt_clusters = 3; assert(cnt_clusters < nrows); //! ie, an tinaion to get a seperation between clusters.
    const t_float score_weak = 100; const t_float score_strong = 1;
    //! Udpate the s_kt_matrix_t object witht the scoress:
    setDefaultValueTo__allCells__kt_matrix(&matrix_concatToAll, score_weak);
    //!
    //! Udpate the 'strong' clsuter-emberships:
    uint row_id = 0;
    const uint cnt_in_each = (uint)((t_float)nrows/(t_float)cnt_clusters);
    //const uint nrows_floor = cnt_in_each * cnt_clusters;
    for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
      const uint start_pos = row_id; assert(start_pos < nrows);
      uint clusterSize_local = ((cluster_id +1) != cnt_clusters) ? cnt_in_each : (start_pos - row_id); //! ie, if in the last cluster then 'update the remaining set of vertices'.
      uint last_pos = row_id + clusterSize_local;       assert(last_pos <= nrows);
      if((cluster_id+1) == cnt_clusters) {last_pos = nrows;}
      //printf("clust[%u/%u] rows=[%u, %u/%u], at %s:%d\n", cluster_id, cnt_clusters, row_id, last_pos, nrows, __FILE__, __LINE__);
      for(; row_id < last_pos; row_id++) {
	mapOf_vertexClusterId[row_id] = cluster_id; //! ie, udpate oru cluster-id
	for(uint col_id = start_pos; col_id < last_pos; col_id++) {
	  set_cell__s_kt_matrix(&matrix_concatToAll, row_id, col_id, score_strong);
	  /* //! Update the clsuter-mberships: */
	  /* set_cell__s_kt_matrix(obj_clusterMembers, cluster_id, row_id, /\*score=*\/1); */
	}
      }
    }
    // printf("row_id=%u, nrows=%u, at %s:%d\n", row_id, nrows, __FILE__, __LINE__);
    assert(row_id == nrows); //! ie, as we expect all trwos to have been set
  }

  //! 
  //! Update the configuraiton-object:
  assert(matrix_concatToAll.ncols > 0);
  //assert(matrix_concatToAll__type__overhead.ncols > 0);
  fileRead_config__syn.mat_concat = &matrix_concatToAll; //__type__overhead;
  //fileRead_config__syn_latency.mat_concat = &matrix_concatToAll__type__latency;

  const uint mapOf_realLife_size = 1;
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    {/*tag=*/"small__goldStandardTest", /*file_name=*/NULL, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/true, /*k_clusterCount=*/10, /*mapOf_vertexClusterId=*/mapOf_vertexClusterId, /*mapOf_vertexClusterId_size=*/nrows, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,
     metric__beforeClust, metric__insideClust, clusterAlg},
    ///*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_all_notPostProcess, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_all_notPostProcess, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
  };

  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //!
  //! De-allocate:
  free__s_kt_matrix(&matrix_concatToAll);
  free_1d_list_uint(&mapOf_vertexClusterId);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
