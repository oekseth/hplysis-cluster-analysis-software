#ifndef fast_log_h
#define fast_log_h


/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <math.h>
#include <float.h>
#include <assert.h>

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <math.h>
#include <float.h>
#include <assert.h>


//! Simplify the configuration of our optmized "mine" software-implementaiton.
#include "configure_optimization.h" //! Whcih specifies the optmizaitons and macro-configurations



#include "def_intri.h"

//! Then we make use of a default assumption:
//! Note: for ("optmize_usePReComptued_floats", "VECTOR_CONFIG_USE_FLOAT") = (1, 1) time-difference is "2.72 x";
//! Note: for ("optmize_usePReComptued_floats", "VECTOR_CONFIG_USE_FLOAT") = (1, 0) time-difference is "2.38 x", where the difference may be explained by the increased memoyr-locality wrt. the flaots (ie, as 'mroe floats then firts into the memory-cache').


#define log_index_fast_fixedValueOf_1 log(1) /*ie, a fixed value which we use in copmutations*/




/**
   FIXME: wrt. optmizaiotn of the "log(..)" function ... example-cases are:
   -----
   * const t_float prob = (t_float) cumhist[p-1][i] / total; H -= prob * log(prob);"
   * const int sum = cumhist[p-1][i]; t_float prob = (t_float) sum / total; updated_value -= prob * log(prob);
   * const int sum = cumhist[s-1][i] - cumhist[t-1][i]; const t_float prob = (t_float) sum / total;
   * prob = (t_float) c[s-1] / total; H -= prob * log(prob);
   * sum = c[t-1] - c[s-1]; prob = (t_float) sum / total;
   -----
   
   <-- in [ªbove] the value "c[i]" will alwways be less thatn the total count of values <-- idneitfy the biggest valeus in the latter. <-- seems like "sum/total" implies a value-range [0, 1] <-- validate this assumption.


   // FIXME: try to write a fucniton which ... use approximate method for low-scoring values (ie, as the latter is assumed to have low influence on the final result*). <-- seems like the overhead assicated to cotnext-shift due to and "if ... else" cluase 'prhbit' such an approach


   // FIXME: idneityf the 'result-impact' of our 'rouding' wr.t the floats 


 **/



/* //#define identifyThresholds_log */
  
/* #ifdef identifyThresholds_log */
/* //! Note: [below] is set if we are interested in testing the overall time-effect of the 'default' log-operaiotn */
/* //#define log(val) (val * val) */
/* static const float log_coutn_Trheshold = 0.1; //! where 57 per-cent of the vlaeus are ["elow] 0. */
/* static const float log_coutn_Trheshold = 0.2; //! where 70 per-cent of the vlaeus are ["elow] the threshold */
/* static const float log_coutn_Trheshold = 0.3; //! where 78 per-cent of the vlaeus are ["elow] the threshold */
/* static const float log_coutn_Trheshold = 0.4; //! where 85 per-cent of the vlaeus are ["elow] the threshold */
/* static const float log_coutn_Trheshold = 0.5; //! where 94 per-cent of the vlaeus are ["elow] 0.5 */
/* static uint debug_cnt_logValues_below_threshold = 0; static uint debug_cnt_logValues_above_threshold = 0;  */
/* #define log_range_0_one(val) ({if(false) printf("val=%f, at %s:%d\n", val, __FILE__, __LINE__); if(val < log_coutn_Trheshold) {debug_cnt_logValues_below_threshold++;} else {debug_cnt_logValues_above_threshold++;} assert(val <= 1); log(val);}) */
/* #endif */



#ifndef optmize_usePReComptued_floats 
#warning "!!\t Expected the 'caller' of this header-file to define this macro, ie, investigate".
#endif


#if optmize_usePReComptued_floats == 1
extern t_float *static_listOf_values_forIndex; extern t_float *static_listOf_values_forIndex_midPoint; static const int static_listOf_values_forIndex_size = 1000*500; //! ie, we expect less than 1,000,000 unique indexes.


/* #define VALUE_TO_STRING(x) #x */
/* #define VALUE(x) VALUE_TO_STRING(x) */
/* #define VAR_NAME_VALUE(var) #var "="  VALUE(var) */


/* /\* Some example here *\/ */
/* /\* #pragma message(VAR_NAME_VALUE(VECTOR_CONFIG_USE_FLOAT)) *\/ */
/* /\* #pragma message(VAR_NAME_VALUE(macroConfig_use_posixMemAlign))  *\/ */
/* #pragma message(VAR_NAME_VALUE(optmize_usePReComptued_floats_accessMemory))  */

#if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values:
#if optmize_use_logValues_fromLocalTable != 1 //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]:

#include "fast_log_floatValues.h"
#endif


#if VECTOR_CONFIG_USE_FLOAT == 1

//! @return the log-valeus for an vector
#define VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_sum_float) ({ \
  union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_sum_float; \
  vec_mappedTo_function = VECTOR_FLOAT_SET(log_index_fast(v.buffer[0]), log_index_fast(v.buffer[1]), log_index_fast(v.buffer[2]), log_index_fast(v.buffer[3])); \
  vec_mappedTo_function; }) //! ie, @return the result.

#else //! then we asusme the type is a "double":

//! @return the log-valeus for an vector
#define VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_sum_float) ({ \
  union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_sum_float; \
  vec_mappedTo_function = VECTOR_FLOAT_SET(log_index_fast(v.buffer[0]), log_index_fast(v.buffer[1])); \
  vec_mappedTo_function; }) //! ie, @return the result.

#endif

//
#else //! then we use a differnt approxmiation for computing the log-values:
#include "fast_log_approxFloatValues.h"
#endif
/* #define log_range_0_one_abs(val) (static_listOf_values[(uint)(val*static_listOf_values_size)]) */
//#define log_range_0_one(val) (static_listOf_values[(uint)(val*static_listOf_values_size)]) //! ...  2.2 sec or approx. 1.84x time-improvement for -cnt-features=10000, which is in contrast to the possible "2.1x" time-imrpvoement (where the latter refers to the case of matrix-muliplicaiton). <-- FIXME[aritlce]: ... update our aritcle wrt. the fact that 'this approach' si faster that the use of a 'cotnext-siwthc' through an "if ... else" clause (to dientify the table to use).

//#define log_range_0_one_abs(val) ({static_listOf_values[(uint)(val*static_listOf_values_size)];}) //! ...  2.2 sec or approx. 1.84x time-improvement for -cnt-features=10000, which is in contrast to the possible "2.1x" time-imrpvoement (where the latter refers to the case of matrix-muliplicaiton). <-- FIXME[aritlce]: ... update our aritcle wrt. the fact that 'this approach' si faster that the use of a 'cotnext-siwthc' through an "if ... else" clause (to dientify the table to use).
// FIXME <-- for a list of "-cnt-features=10000" .. difference is "0m3.735s" VS "0m3.596s" ... ie, consider writing a new list/data-set.
//#define log_range_0_one_abs(val) ({val = fabs(val); static_listOf_values[(uint)(val*static_listOf_values_size)];}) //! ...  2.2 sec or approx. 1.84x time-improvement for -cnt-features=10000, which is in contrast to the possible "2.1x" time-imrpvoement (where the latter refers to the case of matrix-muliplicaiton). <-- FIXME[aritlce]: ... update our aritcle wrt. the fact that 'this approach' si faster that the use of a 'cotnext-siwthc' through an "if ... else" clause (to dientify the table to use).
//! Note: there is insignifcnat exeuciton-tiema ssicated to "log_index(..)"
//#define log_index_fast(val) ({printf("val=%d < |%d|, arr=%p, arr-midpoint=%p, at %s:%d\n", (int)val, static_listOf_values_forIndex_size, static_listOf_values_forIndex, static_listOf_values_forIndex_midPoint, __FILE__, __LINE__);if((int)val > static_listOf_values_forIndex_size) {printf("val=%d, at %s:%d\n", (int)val, __FILE__, __LINE__);} assert((int)val < static_listOf_values_forIndex_size); static_listOf_values_forIndex_midPoint[(int)val];})
//#define log_index_fast(val) ({assert(static_listOf_values_forIndex_midPoint); if((int)val > static_listOf_values_forIndex_size) {printf("val=%d, at %s:%d\n", (int)val, __FILE__, __LINE__);} assert((int)val < static_listOf_values_forIndex_size); static_listOf_values_forIndex_midPoint[(int)val];})
// assert((float)debug_get_floatValue_approx(static_listOf_values_forIndex_midPoint[(int)val]) == (float)debug_get_floatValue_approx(log(val)));
//#define log_index_fast(val) ({printf("val=%f, cmp: %.5f VS %.5f, at %s:%d\n", (float)val, (float)static_listOf_values_forIndex_midPoint[(int)val], (float)log(val), __FILE__, __LINE__); assert((int)val < static_listOf_values_forIndex_size); 
//									static_listOf_values_forIndex_midPoint[(int)val];})
// #define log_index_fast(val) ({assert((int)val < static_listOf_values_forIndex_size); assert(static_listOf_values_forIndex_midPoint); static_listOf_values_forIndex_midPoint[(int)val];})
#define log_index_fast(val) ({ static_listOf_values_forIndex_midPoint[(int)val];})
//#define log_index(val) ({assert((uint)val < static_listOf_values_forIndex_size); static_listOf_values_forIndex[(uint)val];})
//#define log_index_fast(val) (log(val))
#define log_index(val) (log(val))
#else
//! Then the 'real-life' version:
//#define threshold_logExact 0.1 //! 4.29 sec for -cnt-features=10000
//#define threshold_logExact 0.2 //! 3.81 sec for -cnt-features=10000
//#define threshold_logExact 0.3 //! 3.69 sec for -cnt-features=10000
//#define threshold_logExact 0.4 //! 2.9 sec for -cnt-features=10000
//#define threshold_logExact 0.5 //! 2.71 sec for -cnt-features=10000
//#define threshold_logExact 0.6 //! 2.5 sec for -cnt-features=10000
//#define threshold_logExact 0.99 //! 2.27 sec for -cnt-features=10000
//#define log_range_0_one(val) ({float ret_val = val*val; if(val > threshold_logExact){ret_val = log(val);} ret_val;})
//#define log_range_0_one(val) (val*val) //! 1.89 sec for -cnt-features=10000
#define log_range_vector_get_memoryLocation(vec) ({vec;})
#define log_range_vector_get_memoryLocation_abs(vec) ({vec;})


//! Note: there is insignifcnat exeuciton-tiema ssicated to "log_index(..)"
#if (mineAccuracy_computeLog_useAbs == 1) //! then we assume that 'taking' the "abs' is correct wrt. corrrectness of the resutls (ie, wrt. the applciaotn of the algorithm):

#define log_range_0_one_inputIsAdjusted(val) (fabs(val))
//! HAdnle special cases wrt. 'abs':
#if default_value_avoidErrors_nan == 1
#define log_range_0_one_abs(val) ({t_float result = 0; if(val != 0) {result = log(fabs(val));} result;}) 
#define log_range_0_one_inputIsAdjusted_abs(val) ({t_float result = 0; if(val != 0) {result = log(fabs(val));} result;}) 
#define log_index_fast(val) ({t_float result = 0; if(val != 0) {result = log(fabs(val));} result; })
#define log_range_0_one(val) ({t_float result = 0; if(val != 0) {result = log(fabs(val));} result; })
//#define log_range_0_one(val) (log(abs(val))) 
#define log_index(val) ({t_float result = 0; if(val != 0) {result = log(fabs(val));} result; })
//#define log_index(val) (log(abs(val)))
#else //! then we use the 'naive' approach found in the "mine" software:
#define log_index_fast(val) (log(fabs(val)))
#define log_range_0_one(val) (log(fabs(val))) 
#define log_index(val) (log(fabs(val)))
#define log_range_0_one_abs(val) (log(fabs(val)))
#define log_range_0_one_inputIsAdjusted_abs(val) (val)
#endif

//! -----------------------------------------------
#else //! then we dot 'translate' valeus into their 'asolute' value, ie, using the 'default' strategy in the "minervfa" Mine implemetnaiton:
//! Float-numbers:
#define log_index_fast(val) (log(val))
#define log_index(val) (log(val))
#define log_range_0_one(val) (log(val)) 
#define log_range_0_one_inputIsAdjusted(val) (val)
//! HAdnle special cases wrt. 'abs':
#if default_value_avoidErrors_nan == 1
#define log_range_0_one_abs(val) ({t_float result = 0; if(val > 0) {result = log(val);} result;}) 
#define log_range_0_one_inputIsAdjusted_abs(val) ({t_float result = 0; if(val > 0) {result = log(val);} result;}) 
#else //! then we use the 'naive' approach found in the "mine" software:
#define log_range_0_one_abs(val) (log(val)) 
#define log_range_0_one_inputIsAdjusted_abs(val) (val)
#endif
#endif

//! --------------------
#endif


			    //void allocate_optmize_usePReComptued_floats();

#if optmize_usePReComptued_floats == 1
static void allocate_optmize_usePReComptued_floats(void) {
  // FIXME: if ["elow] results in a performance-icnrease then remember to (a) de-allcoate ["elow] and (b) do not allcoat ethis 'ofr eahc' during the parallel copmtuation of columns

#if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values:

  { //! Update:
    static_listOf_values_forIndex = allocate_1d_list_float(/*size=*/static_listOf_values_forIndex_size*2, /*default-value=*/0); 
    static_listOf_values_forIndex_midPoint = static_listOf_values_forIndex + static_listOf_values_forIndex_size; //! ei, to 'handle' zero-valeus.

    // printf("allcoate for size=%d and arr=%p, add-midpoint=%p, at [%s]:%s:%d\n", (int)static_listOf_values_forIndex_size, static_listOf_values_forIndex, static_listOf_values_forIndex_midPoint, __FUNCTION__, __FILE__, __LINE__);

    for(int i = 1; i < static_listOf_values_forIndex_size; i++) {
      // printf("\t update-midPoint-index-log[%u], at %s:%d\n", (uint)i, __FILE__, __LINE__);
      static_listOf_values_forIndex_midPoint[i] = (t_float)log((float)i);
      assert(static_listOf_values_forIndex_midPoint[i] == static_listOf_values_forIndex[i + static_listOf_values_forIndex_size]);
#if (mineAccuracy_computeLog_useAbs == 1) //! then we assume that 'taking' the "abs' is correct wrt. corrrectness of the resutls (ie, wrt. the applciaotn of the algorithm):
      static_listOf_values_forIndex_midPoint[-1*i] = static_listOf_values_forIndex_midPoint[i]; //! ie, implicty stored the 'abs' value.
#endif
    }
  }

#if optmize_use_logValues_fromLocalTable != 1 //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]:

  const uint cnt_allocated = 2*(uint)(static_listOf_values_size+1); //! ie, to handle the value-range [-1, 1].
  vec_fast_log_defaultValue = VECTOR_FLOAT_SET1(/*static_listOf_values_size=*/static_listOf_values_size);
  static_listOf_values = allocate_1d_list_float(/*size=*/cnt_allocated, /*default-value=*/0); 
  static_listOf_values_shallowCopy_aboveZero =  static_listOf_values + static_listOf_values_size; //! ie, to reduce the number of artimethic operations to identify the index for valeus in range [0, 1]
  assert(static_listOf_values);

  assert(VECTOR_FLOAT_getAtIndex(vec_fast_log_defaultValue, /*index=*/0) == static_listOf_values_size);

  if(false) {printf("\t(info)\t allcoated \"static_listOf_values\" w/cnt_allocated=%u, at %s:%d\n", cnt_allocated, __FILE__, __LINE__);}



  for(int i = 1; i <= (int)static_listOf_values_size; i++) {
    // FIXME: make use of the log-valeus.
    const uint local_index = static_listOf_values_size - i;  
#if 1 == 2
    assert(i< cnt_allocated);
    const float scalar_value = (-1*(float)i)*((float)1/(float)(static_listOf_values_size)); //! ie, the negative value-range [0, -1] 
    assert(isnan(log(scalar_value))); //! ie, as we expect that negative nubmers will alwasy be compelx, ie, NAN
    static_listOf_values[local_index] = log(scalar_value);
    const float expected_index = __get_internalIndexOf_float(scalar_value);
    // printf("[(%u=%f) <- %f] = %f, at %s:%d\n", local_index, expected_index, scalar_value, static_listOf_values[local_index], __FILE__, __LINE__);
    assert(static_listOf_values[local_index] != NAN);
    assert(expected_index == local_index); //! ie, what for for cosnsitency expect.
#endif
    static_listOf_values[local_index] = 0; //! ie, as we expect that negative nubmers will alwasy be compelx, ie, NAN
  }

  for(uint i = 1; i < static_listOf_values_size; i++) {
    // FIXME: make use of the log-valeus.    
    const uint local_index = static_listOf_values_size + i;
    assert(local_index < cnt_allocated);
    const float scalar_value = (float)i/(float)(static_listOf_values_size);    
    static_listOf_values[local_index] = log(scalar_value);
    // printf("[%u=%f] = %f , at %s:%d\n", local_index, scalar_value, static_listOf_values[local_index], __FILE__, __LINE__);
    assert(static_listOf_values[local_index] != NAN); 
    assert(__get_internalIndexOf_float(scalar_value) == local_index); //! ie, what for for cosnsitency expect.    
    assert(static_listOf_values_shallowCopy_aboveZero[i] == static_listOf_values[local_index]); //! ie, givne the expectiaotn of a 'shallow copy'.
#if (mineAccuracy_computeLog_useAbs == 1) //! then we assume that 'taking' the "abs' is correct wrt. corrrectness of the resutls (ie, wrt. the applciaotn of the algorithm):
    assert(static_listOf_values_shallowCopy_aboveZero != NULL);
    static_listOf_values_shallowCopy_aboveZero[-1*(int)i] = static_listOf_values[local_index]; //! ie, implicty stored the 'abs' value.
#endif
  }
#endif
#endif //! whre 'thsi endif' refers to: #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values:
}
//! De-allcoat ehte reserved memory:
static void free_optmize_usePReComptued_floats(void) {
  //fprintf(stderr, "(info)\t de-allocates the set of allcoated floats, at %s:%d\n", __FILE__, __LINE__);
#if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values:
  free(static_listOf_values_forIndex); 
#if optmize_use_logValues_fromLocalTable != 1 //! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]:
  assert(static_listOf_values); assert(static_listOf_values_size);
  free(static_listOf_values);    
#endif //! whre 'thsi endif' refers to: #if optmize_usePReComptued_floats_accessMemory == 1 //! then we make use of the pre-copmtued log-values:
#endif
}
#else 
static void allocate_optmize_usePReComptued_floats(void) {
  // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

}
//! De-allcoat ehte reserved memory:
static void free_optmize_usePReComptued_floats(void) {}
#endif



//#define log_index(val) (val * val)


/* //! De-allcoat ehte reserved memory: */
/* void free_optmize_usePReComptued_floats(); */
//! Main-function for correctness-evlauation:
void fast_log_validateCorrectness(void);
  

#endif
