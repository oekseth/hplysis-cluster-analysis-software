 //!
//! Identify the thresholds:
#ifndef __macro__adjustValueInsteadOfMask
const t_float min_cnt_rows    = self->thresh_minMax__eitherOr_valueDiff_rows[0];  const t_float max_cnt_rows    = self->thresh_minMax__eitherOr_valueDiff_rows[1];
const t_float min_cnt_cols    = self->thresh_minMax__eitherOr_valueDiff_cols[0];  const t_float max_cnt_cols    = self->thresh_minMax__eitherOr_valueDiff_rows[1];
#endif
//! -----------------
//!
//! Apply the filters:
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
const bool isTo_evaluateFor_rows = ( (min_cnt_rows    != T_FLOAT_MIN_ABS) || (max_cnt_rows != T_FLOAT_MAX) ); //! ie, as least one of the valeus shoudl different from the 'extreme cases'.
#else
#ifdef __macro__computeFor__rows
const bool isTo_evaluateFor_rows = true;
#else
const bool isTo_evaluateFor_rows = false;
#endif
#endif //! end: --------------------
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
const bool isTo_evaluateFor_cols = ( (min_cnt_cols    != T_FLOAT_MIN_ABS) || (max_cnt_cols != T_FLOAT_MAX) ); //! ie, as least one of the valeus shoudl different from the 'extreme cases'.
#else
#ifdef __macro__computeFor__cols
const bool isTo_evaluateFor_cols = true;
#else
const bool isTo_evaluateFor_cols = false;
#endif
#endif //! end: --------------------

if( (isTo_evaluateFor_rows || isTo_evaluateFor_cols) ) {
  const uint default_value_float = 0;
  t_float *mapOf_minVal_rows = NULL; t_float *mapOf_minVal_cols = NULL;
  t_float *mapOf_maxVal_rows = NULL; t_float *mapOf_maxVal_cols = NULL;
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
  if(min_cnt_rows    != T_FLOAT_MIN_ABS) {mapOf_minVal_rows = allocate_1d_list_float(self->nrows, default_value_float);}
  if(min_cnt_cols    != T_FLOAT_MIN_ABS) {mapOf_minVal_cols = allocate_1d_list_float(self->ncols, default_value_float);}
  if(max_cnt_rows    != T_FLOAT_MAX) {mapOf_maxVal_rows = allocate_1d_list_float(self->nrows, default_value_float);}
  if(max_cnt_cols    != T_FLOAT_MAX) {mapOf_maxVal_cols = allocate_1d_list_float(self->ncols, default_value_float);}
#else
  if(isTo_evaluateFor_rows) {
    mapOf_minVal_rows = allocate_1d_list_float(self->nrows, default_value_float);
    mapOf_maxVal_rows = allocate_1d_list_float(self->nrows, default_value_float);
  }
  if(isTo_evaluateFor_cols) {
    mapOf_minVal_cols = allocate_1d_list_float(self->ncols, default_value_float);
    mapOf_maxVal_cols = allocate_1d_list_float(self->ncols, default_value_float);
  }
#endif //! end: --------------------

  if(isTo_evaluateFor_rows) { //! then itniate:
    for(uint i = 0; i < self->nrows; i++) {
      if(mapOf_minVal_rows != NULL) {mapOf_minVal_rows[i] = T_FLOAT_MAX;}
      if(mapOf_maxVal_rows != NULL) {mapOf_maxVal_rows[i] = T_FLOAT_MIN_ABS;}
    }
    for(uint i = 0; i < self->ncols; i++) {
      if(mapOf_minVal_cols != NULL) {mapOf_minVal_cols[i] = T_FLOAT_MAX;}
      if(mapOf_maxVal_cols != NULL) {mapOf_maxVal_cols[i] = T_FLOAT_MIN_ABS;}
    }
  }
  if(isTo_evaluateFor_cols) {mapOf_minVal_cols = allocate_1d_list_float(self->ncols, default_value_float);}
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    if(self->mapOf_interesting_rows[row_id]) { //! then we inspect valeus wrt. the column:
      const t_float *__restrict__ row = self->matrix[row_id];
      //! ---
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	if(isOf_interest(row[col_id])) {
	  if(mapOf_minVal_rows != NULL) { mapOf_minVal_rows[row_id] = macro_min(row[col_id], mapOf_minVal_rows[row_id]);}
	  if(mapOf_maxVal_rows != NULL) { mapOf_minVal_rows[row_id] = macro_max(row[col_id], mapOf_maxVal_rows[row_id]);}
	  //! --- 
	  if(mapOf_minVal_cols != NULL) { mapOf_minVal_cols[col_id] = macro_min(row[col_id], mapOf_minVal_cols[col_id]);}
	  if(mapOf_maxVal_cols != NULL) { mapOf_minVal_cols[col_id] = macro_max(row[col_id], mapOf_maxVal_cols[col_id]);}
	}
      }
    }
  }
  //! 
  //! Apply the thresholds:
  if(isTo_evaluateFor_rows) {	
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
      bool isOf_interest = (!mapOf_minVal_rows || (mapOf_minVal_rows[row_id] > min_cnt_rows) );
      isOf_interest = ( isOf_interest && (!mapOf_maxVal_rows || (mapOf_maxVal_rows[row_id] < max_cnt_rows) ) ); //! ie, where "found-max-val > max-threshold"      
      // printf("isOf_interest='%s' given mapOf_min-max_rows[%u]=[%f, %f], min-max=[%f, %f], at %s:%d\n", (isOf_interest) ? "true" : "false", row_id, mapOf_minVal_rows[row_id], mapOf_maxVal_rows[row_id], min_cnt_rows, max_cnt_rows, __FILE__, __LINE__);
#else
      const t_float deNumerator = mapOf_maxVal_rows[row_id] - mapOf_minVal_rows[row_id];
      const t_float deNumerator_inv = (deNumerator != 0) ? 1/deNumerator : 0;
#endif //! end: --------------------

      if(
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
	 isOf_interest == false
#else
	deNumerator_inv != 0
#endif
	 ) {
	if(self->mapOf_interesting_rows[row_id]) {
	  self->mapOf_interesting_rows[row_id] = false;
	  for(uint col_id = 0; col_id < self->ncols; col_id++) {
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
	    self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
#else
	    if(isOf_interest(self->matrix[row_id][col_id])) {
	      const t_float numerator = self->matrix[row_id][col_id] - mapOf_minVal_rows[row_id];
	      self->matrix[row_id][col_id] = (numerator != 0) ? numerator * deNumerator_inv : 0;
	    }
#endif  //! end: --------------------
	  }
	}
      }
    }
  }
  if(isTo_evaluateFor_cols) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
      bool isOf_interest = (!mapOf_minVal_cols || (mapOf_minVal_cols[col_id] > min_cnt_cols) );
      isOf_interest = ( isOf_interest && (!mapOf_maxVal_cols || (mapOf_maxVal_cols[col_id] < max_cnt_cols) ) ); //! ie, where "found-max-val > max-threshold"
      // printf("isOf_interest='%s' given mapOf_min-max_cols[%u]=[%f, %f], min-max=[%f, %f], at %s:%d\n", (isOf_interest) ? "true" : "false", col_id, mapOf_minVal_cols[col_id], mapOf_maxVal_cols[col_id], min_cnt_cols, max_cnt_cols, __FILE__, __LINE__);
#else
      const t_float deNumerator = mapOf_maxVal_cols[col_id] - mapOf_minVal_cols[col_id];
      const t_float deNumerator_inv = (deNumerator != 0) ? 1/deNumerator : 0;
#endif //! end: --------------------
      if(
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
	 isOf_interest == false
#else
	deNumerator_inv != 0
#endif
	 ) {
	//if(isOf_interest == false) {
	if(self->mapOf_interesting_cols[col_id]) {
	  self->mapOf_interesting_cols[col_id] = false;
	  //if( (mapOf_count_cols[col_id] <= min_cnt_columns)  || (mapOf_count_cols[col_id] >= max_cnt_columns) ) {
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {
#ifndef __macro__adjustValueInsteadOfMask //! start: --------------------
	    self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
#else
	    if(isOf_interest(self->matrix[row_id][col_id])) {
	      const t_float numerator = self->matrix[row_id][col_id] - mapOf_minVal_cols[col_id];
	      self->matrix[row_id][col_id] = (numerator != 0) ? numerator * deNumerator_inv : 0;
	    }
#endif  //! end: --------------------
	  }
	}
      }
    }
  }
  //! ----------------------------------------------------------------------------
  //! 
  //! De-allcoate locally reserved memory:
  if(mapOf_minVal_rows != NULL) {free_1d_list_float(&mapOf_minVal_rows);}
  if(mapOf_maxVal_rows != NULL) {free_1d_list_float(&mapOf_maxVal_rows);}
  if(mapOf_minVal_cols != NULL) {free_1d_list_float(&mapOf_minVal_cols);}
  if(mapOf_maxVal_cols != NULL) {free_1d_list_float(&mapOf_maxVal_cols);}
 }

#undef __macro__computeFor__rows
#undef __macro__computeFor__cols
#undef __macro__adjustValueInsteadOfMask
