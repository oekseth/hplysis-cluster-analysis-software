#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis

/**
   @brief examplify how our "hp_api_fileInputTight.h" may be used for: distance-based computations
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks provides an 'extract' of the lgocis examplfied in our "tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.c"
   -- input: in this example we (a) load an 'arbirary' input-file, 
   -- logic: compute a distance-matrix, which is then exproted to a result-file
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //e_kt_correlationFunction_t enum_id = e_kt_correlationFunction_groupOf_MINE_mic;
  const e_kt_correlationFunction_t enum_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  //!
  //! Define an input-file to use in analysis:
  const char *file_name = "data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv";
  //const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 

  //! Load the TSV-file:
  //! Note: advanced options in data-loading conserns noise-injection, both wrt. 'noise-functions' and 'merging of different data-sets'.
  s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(file_name, initAndReturn__s_kt_matrix_fileReadTuning_t()); //! where latter funciton is deifned in our "kt_matrix.h"
  //!
  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(1): Comptue simlairty-metric.
  //! Note: 'this operaiton' is required as the "file_name" is Not expected to describe/represent an adjcency-matrix
  s_kt_matrix_t mat_computedSim = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
  bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(/*metric=*/enum_id, &mat_input, /*mat_input_2=*/NULL, &mat_computedSim);
  assert(is_ok); //! ie, what is expected.
  //!
  //! Export simlairty-metric:
  //allocOnStack__char__sprintf(2000, resultPath, "%s_%s", , get_stringOf__short__e_hpLysis_clusterAlg_t(clustAlg));
  const char *result_file = "tut_tmp_tut_hp_api_fileInputTight_4_dist_1_Euclid.tsv";
  is_ok = export__singleCall__s_kt_matrix_t(&mat_computedSim, result_file, NULL);
  assert(is_ok); //! ie, what is expected.

  const char *stringOf_enum = get_stringOf_enum__e_kt_correlationFunction_t(enum_id);
  const char *stringOf_inputFile_1 = file_name;
  const char *stringOf_inputFile_2 = NULL; // random_file;
  const char *stringOf_exportFile = NULL; //! ie, to "stdout".
  const char *stringOf_exportFile__format = NULL;
  const uint ncluster = UINT_MAX; const char *stringOf_ncluster = NULL;
  const bool inputFile_isSparse = false; const char *stringOf_isSparse = NULL;
  //! 
  { //! Case(b): API-logis using strings:
    //! 
    //! Apply logics:
    uint arg_size = 0;
    char *listOf_args__2d[10];
    char *listOf_args = construct__terminalArgString__fromInput__hp_api_fileInputTight(/*debug_config__isToCall=*/true,  
										       stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse, &arg_size, listOf_args__2d);
    //const bool is_ok = readFromFile__exportResult__hp_api_fileInputTight(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, ncluster, inputFile_isSparse);
    assert(listOf_args); assert(strlen(listOf_args));
    MF__expand__bashInputArg__hp_api_fileInputTight("", "Use-case: distance between measurements", listOf_args__2d, arg_size);
    //printf("%s\t #! %s\n", listOf_args, "Use-case: CCM:matrix-based");
    //! ----------------------------------
    //! 
    //! Case(c): Bash-API using strings:
    //! 
    //! Apply logics:
    const bool is_ok = fromTerminal__hp_api_fileInputTight(listOf_args__2d, arg_size);
    assert(is_ok);
    //! De-allocate:
    if(listOf_args) {free_1d_list_char(&listOf_args); listOf_args = NULL;}
  }

  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  // free__s_kt_matrix(&mat_random);
  free__s_kt_matrix(&mat_computedSim);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

