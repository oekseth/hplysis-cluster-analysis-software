#ifndef db_ds_bTree_rel_h
#define db_ds_bTree_rel_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @file db_ds_bTree_rel
   @brief provide a B-tree-implementaiotn with different configurale fixed-sized children-couunts (oekseth, 06. mar. 2017)
 **/

#include "db_ds_bTree__aux.h"
#include "db_rel.h"

#include "db_ds_bTree_rel__internalWrappers.h" //! eg, for "s_db_node_typeOfKey_rel_case32"
#include "db_searchResult.h" //! which is used to simplify the spciciaotn hand 'handling' of 'nestedsed and basic sematnci-searches'.
#include "kt_set_1dsparse.h"

/**
   @struct s_db_ds_bTree_rel
   @brief provide a b-Tree interface for "s_db_rel_t" nodes (oekseth, 06. mar. 2017).
 **/
typedef struct s_db_ds_bTree_rel {
  s_db_node_typeOfKey_rel_case4_t *root_case4; //=NULL;
  s_db_node_typeOfKey_rel_case16_t *root_case16; //=NULL;
  s_db_node_typeOfKey_rel_case32_t *root_case32; //=NULL;
  e_db_ds_bTree_cntChildren_t typeOf_tree;
  //s_db_node_typeOfKey_int_case4_t *root; //=NULL;
} s_db_ds_bTree_rel_t;
//! @return an intlized 'verison' of our "s_db_ds_bTree_rel_t" struct.
s_db_ds_bTree_rel_t init__s_db_ds_bTree_rel_t(const e_db_ds_bTree_cntChildren_t typeOf_tree);
//! De-allcoates a given s_db_ds_bTree_rel_t tree.
void free__s_db_ds_bTree_rel_t(s_db_ds_bTree_rel_t *self);
/**
   @brief insert a given "key" object into our struct (oekseth, 06. mar. 2017)
   @param <self> is the object to insert in
   @param <key> is the key to insert
   @return true upon success.
 **/
bool insert__s_db_ds_bTree_rel_t(s_db_ds_bTree_rel_t *self, const s_db_rel_t key);
/**
   @brief idneitfy a "scalar_result" which 'corresponds' to "key" (oekseht, 06. mar. 2017)
   @param <self> is the object to search in
   @param <key> is the key to search for
   @param <scalar_result> is the idneitifed object
   @param <isTo_inpsectAll> which is to be set to tru if arbitrary or undefined 'searhc-kesy' are used.
   @param <result_set> which if Not set to null is 'used' to insert the idneitfed result-relationships.
   @return the number fo elments found.
 **/
uint find__s_db_ds_bTree_rel_t(const s_db_ds_bTree_rel_t *self, const s_db_rel_t key, s_db_rel_t *scalar_result, const bool isTo_inpsectAll,  s_kt_set_1dsparse_t *result_set);


#endif //! EOF
