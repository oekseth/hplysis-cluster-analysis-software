#ifndef s_allAgainstAll_config_h
#define s_allAgainstAll_config_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file s_allAgainstAll_config
   @brief configure the configurations wrt. application of all-against-all distance-correlation-computation.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/


#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "correlation_enums.h"
#include "kt_set_2dsparse.h"
#include "kt_list_2d.h"

//! A structure which is sued to generalize the cofnigurations when using the "__optimal_compute_allAgainstAll_SIMD(..)" function.
typedef struct s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights {
  t_float exponent; t_float cutoff;
  t_float *listOf_result_afterCorrelation; //! which is to hold the result.
} s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t;
//! A generic struct to store a t_float-value.
//! @remarks a structure which is sued to generalize the cofnigurations when using the "__optimal_compute_allAgainstAll_SIMD(..)" function.
typedef struct s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic {
  t_float return_distance;
  uint index2_best; //! which is used for the case of "e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min" and "e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max"
} s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t;

//! Intiates the object o a min-distance
static void init_ToMin__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *self) {
  assert(self);
  self->return_distance = T_FLOAT_MAX;
  self->index2_best = UINT_MAX;
}
//! Intiates the object to a max-distance
static void init_ToMax__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *self) {
  assert(self);
  self->return_distance = T_FLOAT_MIN_ABS;
  self->index2_best = UINT_MAX;
}
//! Intiates the object to a max-distance
static void init_ToSum__s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic(s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *self) {
  assert(self);
  self->return_distance = 0;
  self->index2_best = UINT_MAX;
}

//! An object to ofngiure the all-against-all construction of distnace-metrics (oekseth, 06. june 2016).
typedef struct s_allAgainstAll_config {

  //! 
  //! Configurations wrt. the distance-metric:
  t_float configMetric_power; //! whihc is the default power-valeu to be sued, ie, if any.
  t_float *weight;
  char** mask1; 
  char** mask2;
  uint transposed;
  uint nrows; uint ncols; //! which is sued druing emory-deallcoateion of the amsk-lists (oekseth, 06. jul. 2017).

  //! -----------------------------
  //!
  //! Heurtitics which are safe to apply wrt. given 'rpcoessed' input-data-set(s):
  e_cmp_masksAre_used_t  masksAre_used;  

  //! -----------------------------
  //!
  //! Advanced configurations wrt. application of a given distance-metric:
  enum e_typeOf_optimization_distance typeOf_optimization;
  bool isTo_invertMatrix_transposed;
  uint CLS;
  bool isTo_use_continousSTripsOf_memory;
  enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation;
  bool forNonTransposed_useFastImplementaiton;
  bool forNonTransposed_useFastImplementaiton_useSSE;



  //! -----------------------------
  //!
  //! Computation of sub-sets:
  uint iterationIndex_2;
  void *s_inlinePostProcess; //! eg, wrt. the "s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic" strcut.
  s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult; //! which is to be explctly set/used if we are to use/store for only a apsre data-set (oekseht, 06. aug. 2017).

  //! -----------------------------
  //!
  //! Parameters to seperate communicaiton-steps, eg, wwrt. MPI communication:
  bool inAllAgainstAll__inputIsAlreadySorted_forSpearman;
  //bool inAllAgainstAll__


  //! -----------------------------
  //!
  //! Knolwedge the type of memory which has been allcoated in this object:
  bool __internalMemoryAllocation__hasAllocated__masks;
  //! -----------------------------
  //!
  //! Paralle shcuedling-sets.
  bool para_isTo_usePara_ifAboveThreshold;
  loint para_cntThreshold_SM_squares; //! ie, the number of "(nrows_1/SM) * (nrows_2/SM) > threshold" blocks, eg for a symemtirc matrix with SM=64 
  //! -----------------------------
} s_allAgainstAll_config_t;


//! Set to empty the s_allAgainstAll_config structure-object.
void setTo_empty__s_allAgainstAll_config(s_allAgainstAll_config_t *self);


//! Initaites the s_allAgainstAll_config_t structure-object.
void init__s_allAgainstAll_config(s_allAgainstAll_config_t *self, const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2/* = UINT_MAX*/, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation/* = e_allAgainstAll_SIMD_inlinePostProcess_undef*/, void *s_inlinePostProcess/* = NULL*/, e_cmp_masksAre_used_t  masksAre_used/* = e_cmp_masksAre_used_undef*/, const bool isTo_use_SIMD/* = true*/, const enum e_typeOf_optimization_distance typeOf_optimization /* = e_allAgainstAll_SIMD_inlinePostProcess_undef*/);
//! @return false if we do Not need to use masks (oekseth, 06. sept. 2016).
bool s_allAgainstAll_config__needTo_useMask_evaluation(const s_allAgainstAll_config_t *self);

//! Use the proeprties in the s_allAgainstAll_config_t to 'idenitfy and set the correct empty values' in our resultMatrix (oekseth, 06. des. 2016)
//! @remarks resultMatrix is expected to be in diemsions [nrows_1, nrows_2].
void setMatrixTo_defaultEmptyValue__s_allAgainstAll_config_t(const s_allAgainstAll_config_t *self, t_float **resultMatrix, const uint nrows_1, const uint nrows_2);

//! An ANSI C constructor to itnaite the object:
s_allAgainstAll_config_t get_init_struct_s_allAgainstAll_config();

//! De-allcoates an object of type "s_allAgainstAll_config_t"
//s_allAgainstAll_config_t 
void free_memory__s_allAgainstAll_config(s_allAgainstAll_config_t *self);

/**
   @brief set the emta-pproeprties to be used in the 'buidling' of the distance-matrix
   @param<self> is the object to update
   @param<isTo_init>  which if set to true intiate the object: should be set to false if proeprteis (not incldued in 'this' function-call) have explcitly (ie, intaitonlally) been set/intiated at an earlier exeuction-point
   @param<mask1> the masks to be used for "data1" in the "compute_allAgainstAll_distanceMetric(..)" function-call
   @param<mask2> the masks to be used for "data2" in the "compute_allAgainstAll_distanceMetric(..)" function-call
   @param<weight> which if set is used to mulipl the column-value with the score in the "weight" table, ie, if not set we assume the value of 'each' is set to "1".
   @param<isTo_transpose>  set to true if the transposed of the correlation-matrix is to be computed
   @param <masksAre_used> described the types of makss to be applied: to be consistent with "mask1" and "mask2" paraemters.
   @remarks 
   -- if "mask1" or "mask2" is intiated (ie, Not set to NULL), then we expect both to be set (eg, 'set' to '1' for both cases);
   -- the "masksAre_used" parameter is used to ncrease the perofrmance of our correlation-comptuation: if the 'maks' paremters are not set while the masksAre_used parameter is set to "e_cmp_masksAre_used_true" or "e_cmp_masksAre_used_undef" we assume implicit masks are to be used whiel if "e_cmp_masksAre_used_false" is set we assuem there is no need for usign/applying masks.
   -- the use of the "weight" parameter results in a slight increase in the xeuction-time, ie, if all columsn have the same weight we reccoment setting the "weight" parameter to "NULL"
 **/
void set_metaMatrix(s_allAgainstAll_config_t *self, const bool isTo_init, char **mask1, char **mask2, float weight[], const bool isTo_transpose, const e_cmp_masksAre_used_t  masksAre_used, const uint nrows, const uint ncols);
//! Set masks of type uint, a data-type which we in this function converts to elements of 1-byte "char", ie, as an optmizaiton heuristic (oekseth, 06. setp. 2016).
void set_masks_typeOfMask_uint__s_allAgainstAll_config(s_allAgainstAll_config_t *self, const bool isTo_init, uint **mask1_uint, uint **mask2_uint, const bool isTo_translateMask_intoImplictDistanceMatrix, const uint nrows, const uint ncols, t_float **data1, t_float **data2, const bool isTo_use_continousSTripsOf_memory);

//! @return false if we do Not need to use masks (oekseth, 06. sept. 2016).
static bool needTo_useMask_evaluation__s_allAgainstAll_config(const s_allAgainstAll_config_t *self) {
  assert(self);
  return (self->masksAre_used != e_cmp_masksAre_used_false); //! ie, if the 'no-mask' proeprty is epxlcitly set, then we assume that masks are Not needed to be seud (oekseth, 06. sept. 2016).
}

//! Intiates the "s_allAgainstAll_config" object.
static void init__fast_s_allAgainstAll_config(s_allAgainstAll_config_t *self) {
  self->nrows = 0; self->ncols = 0;
  self->forNonTransposed_useFastImplementaiton = true;
  self->forNonTransposed_useFastImplementaiton_useSSE = true;
  self->masksAre_used = e_cmp_masksAre_used_undef; //! ie, the devfault assumption.
  self->configMetric_power = 3; //! ie, our default assumption for the "Minkowski" distance-metric
  // self-> = true;
}



//! Intiates the "s_allAgainstAll_config" object.
static void init_useSlow__s_allAgainstAll_config(s_allAgainstAll_config_t *self) {
  self->nrows = 0; self->ncols = 0;
  self->forNonTransposed_useFastImplementaiton = false;
  self->forNonTransposed_useFastImplementaiton_useSSE = false;
  self->masksAre_used = e_cmp_masksAre_used_true; //! ie, the devfault assumption.
  // self-> = true;
}
//! Intiates the "s_allAgainstAll_config" object.
static void init_useFast__s_allAgainstAll_config(s_allAgainstAll_config_t *self, const e_cmp_masksAre_used_t masksAre_used) {
  self->nrows = 0; self->ncols = 0;
  self->forNonTransposed_useFastImplementaiton = true;
  self->forNonTransposed_useFastImplementaiton_useSSE = true;
  self->masksAre_used = masksAre_used;
  // self-> = true;
}
//! Itnaites the object storing the result as a sparse data-et (oekseth, 06. aug. 2017).
void init__s_allAgainstAll_config__storeSubset(struct s_allAgainstAll_config *self, s_kt_list_2d_kvPair_filterConfig_t *sparseDataResult, const uint iterationIndex_2, const e_cmp_masksAre_used_t masksAre_used);

#endif //! EOF
