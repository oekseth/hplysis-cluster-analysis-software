#ifndef kt_terminal_parser_h
#define kt_terminal_parser_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_terminal_parser
   @brief a parser to the input-tmerinal-arguemtns, argumetns which may be sued to cofnigure this object (oekseth, 06. okt. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"

/* /\** */
/*    @struct s_kt_matrix */
/*    @brief a srapper-object used to add elements to the input-matrix. */
/*  **\/ */
/* typedef struct s_kt_matrix { */
/*   t_float **matrix; */
/*   t_float *weight; */
/*   uint ncols; uint nrows;   */
/* } s_kt_matrix_t; */


#endif // !EOF
