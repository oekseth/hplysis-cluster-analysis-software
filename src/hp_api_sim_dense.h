#ifndef hp_api_sim_dense_h
#define hp_api_sim_dense_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

//#include "kt_centrality.h"
#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "kt_list_2d.h" //! which among others define the "s_kt_list_2d_kvPair_t" struct.

//#include "hp_sim_dense.h"

/**
   @struct s_hp_api_sim_dense_config
   @brief provide a configuraiton interface for idnetifying interesting simalrity-paris during dense domptuation (oekseth, 06. otk. 2017).
 **/
typedef struct s_hp_api_sim_dense_config {
  uint config_cntMin_afterEach;
  uint config_cntMax_afterEach;
  t_float config_ballSize_max;
  bool isTo_ingore_zeroScores;
} s_hp_api_sim_dense_config_t;

//! @returm a default itniated vieorns of the s_hp_api_sim_dense_config_t cofnig object.
s_hp_api_sim_dense_config_t initAndReturn__s_hp_api_sim_dense_config_t();
//! Comptue sparse simalrity through applciaotn fo sense siamrlity-emtrics.
bool apply__simMetric_dense__extraArgs__hp_api_sim_dense(const s_kt_correlationMetric_t obj_metric__, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_2, s_kt_list_2d_kvPair_t *obj_result, s_hp_api_sim_dense_config_t config);
//! Applies a dense matrix-based simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017).
bool apply__simMetric_dense__hp_api_sim_dense(const e_kt_correlationFunction_t enum_id, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_2, s_kt_list_2d_kvPair_t *mat_result);
//! Read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
bool readFromFile__stroreInStruct__exportResult__hp_api_sim_dense(const char *stringOf_enum,
								  const char *stringOf_enumMerge_rows, const char *stringOf_enumMerge_rows_postAdjust,
								  s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_2, s_kt_list_2d_kvPair_t *obj_result, 
								  s_kt_list_1d_uint_t *map_clusters, 
								  const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
								  const char *stringOf_exportFile_clusters,  const char *stringOf_exportFile__format_clusters, 
								  s_hp_api_sim_dense_config_t config);

//! Provide a generic interface to the "readFromFile__stroreInStruct__exportResult__hp_api_sim_dense(..)" funciton.
bool readFromFile__exportResult__hp_api_sim_dense(const char *stringOf_enum,
						  const char *stringOf_enumMerge_rows, const char *stringOf_enumMerge_rows_postAdjust,
						  const char *stringOf_inputFile_1,  const char *stringOf_inputFile_2, const bool isTo_transpose,
						  const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
						  const char *stringOf_exportFile_clusters,  const char *stringOf_exportFile__format_clusters, 
						  // const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool inputFile_isSparse
						  s_hp_api_sim_dense_config_t config);

//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_sim_dense(const bool debug_config__isToCall, const char *stringOf_enum,
								const char *stringOf_enumMerge_rows, const char *stringOf_enumMerge_rows_postAdjust,
								const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, 
								const bool isTo_transpose,
								const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
								const char *stringOf_exportFileClust,  const char *stringOf_exportFileClust__format, 
								s_hp_api_sim_dense_config_t config,
								//const uint nclusters, const bool inputFile_isSparse, 
								uint *scalar_cntArgs, char **listOf_args__2d);
//bool readFromFile__exportResult__hp_api_sim_dense(const char *stringOf_enum, const char *stringOf_inputFile_1,  const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool inputFile_isSparse); 
//bool readFromFile__exportResult__hp_api_sim_dense(const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse);

/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_sim_dense(char **arg, const uint arg_size);
//static void printOptions____hp_api_sim_dense();




#endif //! EOF
