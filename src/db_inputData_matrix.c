#include "db_inputData_matrix.h"
#include "kt_forest_findDisjoint.h"
#include "kt_list_1d.h"
/* static void __load_relations__fromMatrix__s_db_ds_directMapping__db_inputData_matrix(s_db_ds_directMapping_t *self, uint **matrix_relations, const uint matrix_relations_rows, const uint matrix_relations_cols) { */

/* } */
/* static void __load_syns__fromMatrix__s_db_ds_directMapping__db_inputData_matrix(s_db_ds_directMapping_t *self, uint **matrix, const uint matrix_rows, const uint matrix_cols, const bool isTo__applyRecursiveCall) { */

/* } */

s_db_ds_directMapping_t loadData__s_db_ds_directMapping__db_inputData_matrix(const char *nameOfFile__relations, const char *nameOfFile__synonyms, const bool isTo_applyPreNormalizaiotnStep__onSynonyms, const e_db_ds_directMapping__initConfig_t objConfig) {
  //! ---------------------------------------------------------------------------
  //!
  //! Load data-set:
  assert(nameOfFile__relations); assert(strlen(nameOfFile__relations));
  s_kt_matrix_fileReadTuning_t conf_read = initAndReturn__s_kt_matrix_fileReadTuning_t();
  conf_read.isTo__copyNamesIfSet = false; // TODO: cosnider settting 'this' to true.
  s_kt_matrix_t mat_rel = readFromAndReturn__file__advanced__s_kt_matrix_t(nameOfFile__relations, conf_read);
  assert(mat_rel.nrows > 0);
  assert(mat_rel.ncols > 0);
  s_kt_matrix_t mat_syns = setToEmptyAndReturn__s_kt_matrix_t();
  if(nameOfFile__synonyms && strlen(nameOfFile__synonyms)) {
    mat_syns = readFromAndReturn__file__advanced__s_kt_matrix_t(nameOfFile__synonyms, conf_read);
    assert(mat_syns.nrows > 0);
    assert(mat_syns.ncols > 0);
  }
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  //! ---------------------------------------------------------------------------
  //!
  //!
  uint cnt_head = 0; //nameOfFile__relations;
  const uint cnt_relations = mat_rel.nrows;
  //! Fidn the biggest index usied in (hed, rt, tail);
  for(uint h = 0; h < mat_syns.nrows; h++) {
    const t_float *row = mat_syns.matrix[h]; assert(row);
    for(uint i = 0; i < mat_syns.ncols; i++) {
      const uint key = (uint)row[i];
      if(key != UINT_MAX) {
	cnt_head = macro_max(cnt_head, key);
      }
    }
  }
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  for(uint h = 0; h < mat_rel.nrows; h++) {
    const t_float *row = mat_rel.matrix[h]; assert(row);
    for(uint i = 0; i < mat_rel.ncols; i++) {
      const uint key = (uint)row[i];
      if(key != UINT_MAX) {
	cnt_head = macro_max(cnt_head, key);
      }
      //cnt_head = macro_max(cnt_head, (uint)row[i]);
    }
  }
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  cnt_head += 1; //! ie, a 'simpel offset'.

  //! --------------------------------
  //!
  //! Intniates:
  s_db_ds_directMapping_t self = init__enumConfig__s_db_ds_directMapping_t(cnt_head, cnt_relations, objConfig);

  // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //! --------------------------------
  //!
  //! Pre-processing: merge synonyms If requested:
  s_kt_list_1d_uint_t map_norm = init__s_kt_list_1d_uint_t(0);
  if(isTo_applyPreNormalizaiotnStep__onSynonyms && nameOfFile__synonyms && strlen(nameOfFile__synonyms)) {
    s_kt_forest_findDisjoint_t obj_forest = get_empty__s_kt_forest_findDisjoint();
    s_kt_set_2dsparse_t listOf_relations; setTo_Empty__s_kt_set_2dsparse_t(&listOf_relations); 
    allocate__s_kt_set_2dsparse_t(&listOf_relations, cnt_head, 100, NULL, NULL, /*isTo_useImplictMask=*/false, /*allcoateScores=*/false);
    //!
    //! Insert synonyms: 
    assert(mat_syns.ncols >= 2);
    for(uint h = 0; h < mat_syns.nrows; h++) {
      s_db_rel_t rel;
      const t_float *row = mat_syns.matrix[h]; assert(row);
      uint head_id = (uint)row[0];       uint tail_id = (uint)row[1];
      if(mat_syns.ncols > 2) {
	tail_id = (uint)row[2];
      }
      //!
      //! Add relation:
      assert(head_id < cnt_head);
      assert(tail_id < cnt_head);
      push__s_kt_set_2dsparse_t(&listOf_relations, head_id, tail_id);
    }
    //!
    //! Comptue central vertices:
    // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    allocate__extensive__s_kt_forest_findDisjoint(&obj_forest, cnt_head, cnt_head, &listOf_relations, /*init=*/true, /*useSeperateNameSpace_rows=*/false, /*useSeperateNameSpace_columns=*/false, /*totalCntUniqueVerticesToEvalauteForAllIterations=*/cnt_head);
    //! Apply logics:
    // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    graph_disjointForests__s_kt_forest_findDisjoint(&obj_forest, /*isTo_identifyCentrlaityVertex=*/true);

    // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    
    //!
    //! Fidnt eh 'normalizaiotn-nodes':
    map_norm = init__s_kt_list_1d_uint_t(cnt_head);
    for(uint h = 0; h < cnt_head; h++) {
      const uint norm_id = get_centralityVertex_inDisjointforest__s_kt_forest_findDisjoint(&obj_forest, h);
      if(norm_id != UINT_MAX) { //! then update the mappings:
	assert(norm_id < cnt_head);
	map_norm.list[h] = norm_id;
      }
    }
    //! De-allocate:
    free_s_kt_forest_findDisjoint(&obj_forest);
    free_s_kt_set_2dsparse_t(&listOf_relations);
  }
  // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  //! --------------------------------
  {
#define __normalizeName(vertex_id) ({assert(vertex_id < cnt_head); if(map_norm.list[vertex_id] != UINT_MAX) {vertex_id = map_norm.list[vertex_id];} vertex_id;})

    //!
    //! Insert synonyms: 
    
    if(mat_syns.nrows > 0) {
      assert(mat_syns.ncols >= 2);
      for(uint h = 0; h < mat_syns.nrows; h++) {
	s_db_rel_t rel;
	const t_float *row = mat_syns.matrix[h]; assert(row);
	uint head_id = (uint)row[0];       uint tail_id = (uint)row[1];
	if(mat_syns.ncols > 2) {
	  tail_id = (uint)row[2];
	}
	
	if(isTo_applyPreNormalizaiotnStep__onSynonyms) {
	  head_id = __normalizeName(head_id);	
	  tail_id = __normalizeName(tail_id);	
	} 
	if(head_id != tail_id) {
	  if( (head_id < cnt_head) && (tail_id < cnt_head) ) {
	    setMappingFor_head__s_db_ds_directMapping_t(&self, head_id, tail_id);
	    // TODO: vlaidate that [”elow] 'is needed'.
	    setMappingFor_tail__s_db_ds_directMapping_t(&self, head_id, tail_id);
	  } //! else we for simplicty ingore this relaitonship
	}
      }
    }
    // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //!
    //! Insert relations: 
    for(uint h = 0; h < mat_rel.nrows; h++) {
      const t_float *row = mat_rel.matrix[h]; assert(row);
      s_db_rel_t rel = __getRelation__fromRow__s_db_rel(row, mat_rel.ncols);
      if(mat_syns.nrows && isTo_applyPreNormalizaiotnStep__onSynonyms) {
	rel.head         = __normalizeName(rel.head);
	rel.tailRel.rt   = __normalizeName(rel.tailRel.rt);
	rel.tailRel.tail = __normalizeName(rel.tailRel.tail);
      }
      if( (rel.head < cnt_head) && (rel.tailRel.tail < cnt_head) ) {
	//! Insert the row:
	insertRelation__s_db_ds_directMapping_t(&self, rel);
      } //! else we for simplicty ingore this relaitonship
    }
  }
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  //! -------------------------
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_rel);
  free__s_kt_matrix(&mat_syns);
  free__s_kt_list_1d_uint_t(&map_norm);
  //! -------------------------
  //! @return
  return self;
}


//!  prodcue two data-files to examplify evlauation and applicaiotn of our ipnut-file-laoding proceudre (oekseth, 06. mar. 2017)
void generateSample__storeInFiles__db_inputData_matrix(const uint cnt_heads, const uint cnt_pred, const uint nrows_each, const t_float fraciton_syns, const char *fName__rel, const char *fName__syn) {
  const uint cnt_fraction = (uint)((t_float)(cnt_heads)*fraciton_syns);
  const uint cnt_syns = cnt_heads * cnt_fraction;
  //!
  //! Build the relation-set:
  const uint cnt_relations = nrows_each * cnt_heads; // * cnt_pred;
  /* const uint cnt_syns = (uint)(4.0*fraciton_syns * (t_float)cnt_relations); */
  /* const uint cnt_fraction = fraciton_syns/cnt_heads; */
  //assert(cnt_fraction > 0);
  /* assert(fraciton_syns <= 1); // TODO: consider remving this 'citeria'. */
  /* assert(cnt_syns > 0); */
  printf("Generate matrices for |relations|=%u and |synonyms|=%u; heads=%u, cols=%u,  at %s:%d\n", cnt_relations, cnt_syns, cnt_heads, nrows_each, __FILE__, __LINE__);
  assert(cnt_relations > 0);
  s_kt_matrix_t mat_rel = initAndReturn__s_kt_matrix(cnt_relations, /*cnt-fields=*/5);
  s_kt_matrix_t mat_syn = setToEmptyAndReturn__s_kt_matrix_t();
  if(cnt_syns > 0) {
    mat_syn = initAndReturn__s_kt_matrix(cnt_syns, /*cnt-fields=*/2);
    //! ----------------------------------------------------------------------------------
    //!
    //! Define synonyms:
    //assert(cnt_fraction > 0); 
    uint cnt_syn = 0;
    for(uint h = 0; h < cnt_heads; h++) {
      for(uint i = 0; i < cnt_fraction; i++) {
	if(cnt_syn >= mat_syn.nrows) {continue;}
	assert(cnt_syn < mat_syn.nrows);
	//! Identify the synonym: 
	//! Note: we use 'radnomenss' to cpåature the 'non-homeognee' properites of 'acutal relationshsip fodun in compelx data-setabase', ie, in cotnrast to the 'alternaitve' of suign a 'fixed numbe rof synonyms foir each vertex-case':
	uint tail = 0;
	uint cnt_while = 0;
	while(cnt_while++ < 20) {
	  tail = rand() % cnt_heads;
	  if(tail != h) {cnt_while = 20;} //! ie, break.
	}
	if(tail == h) {tail = h + 1; if(tail > cnt_heads) {tail = 0;}} //! ie, a fall-back.
	assert(cnt_syn < mat_syn.nrows);
	//! Set the relationship:
	mat_syn.matrix[cnt_syn][0] = h;
	mat_syn.matrix[cnt_syn][1] = tail;
	//! -----------------------------------------------  
	//! Increment:
	cnt_syn++;      
      }
    }
  }
  // printf("inserts relations, at %s:%d\n", __FILE__, __LINE__);
  //! ----------------------------------------------------------------------------------
  //!
  //! Define relations:
  uint cnt_rel = 0;     uint predicate = 0;
  for(uint h = 0; h < cnt_heads; h++) {
    for(uint t = h+1; t < (h+1+nrows_each); t++) {
      if(t < cnt_heads) {
	assert(cnt_rel < mat_rel.nrows);
	assert(mat_rel.matrix[cnt_rel]);
	//! Set the relationship:
	mat_rel.matrix[cnt_rel][0] = h;
	mat_rel.matrix[cnt_rel][1] = predicate;
	mat_rel.matrix[cnt_rel][2] = t;
	mat_rel.matrix[cnt_rel][3] = 1.0; //! ie, the 'default distance'.
	mat_rel.matrix[cnt_rel][4] = cnt_rel;
	//! -----------------------------------------------  
	//! Increment:
	cnt_rel++;
	if(predicate++ >= cnt_pred) {predicate = 0;}	  
      }
    }
  }
  //! ----------------------------------------------------------------------------------
  //!
  //! Export relationships:
  printf("export-matrix for rel/dims=[%u, %u], at %s:%d\n", mat_rel.nrows, mat_rel.ncols, __FILE__, __LINE__);
  bool is_ok = export__singleCall__s_kt_matrix_t(&mat_rel, fName__rel, NULL); assert(is_ok);
  if(cnt_syns > 0) {
    printf("export-matrix for syn/dims=[%u, %u], at %s:%d\n", mat_syn.nrows, mat_syn.ncols, __FILE__, __LINE__);
    is_ok = export__singleCall__s_kt_matrix_t(&mat_syn, fName__syn, NULL); assert(is_ok);
  }

  printf("produced-matrix, at %s:%d\n", __FILE__, __LINE__);
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_rel);
  free__s_kt_matrix(&mat_syn);
}
