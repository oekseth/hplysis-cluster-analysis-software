  { //! A set of lgoical valridations, which are used to calidate correctness of our optimization-stragegy.
    { //! Valdiate that memmove 'produce' teh correct result:
      const uint list_size = 5;
      uint list[list_size] = {4, 3, 2, 1, 0}; 
      memmove(list+1, list, sizeof(float)*(list_size-1));
      assert(list[0] == 4); //! ie, unchanged
      assert(list[1] == 4); //! ie, updated.
      assert(list[2] == 3); //! ie, updated.
    }
  }

  const bool isTo_evlauateOnlyMergeSort = false;
  
  const uint arrOf_chunkSizes_size = 5; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {kilo, 10*kilo, 100*kilo, kilo*kilo, 5*kilo*kilo};
  { //! Test the overhead of using implict memory-accesss for fixed lists, eg, wrt. memory-iteration:      
    // FIXME: ... seems like the "uint":"uint" combiantion perofmrs more than 2x faster thant eh otehr combinations .... ie, try to 'explain the reason for this' <-- ask Jan-Christian if he has any explanations.
    for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
      uint list_size = arrOf_chunkSizes[chunk_index];
      //! Set 'the same valeus' for muliple items:
      const uint bucket_size = 100;
      list_size = floor(list_size / bucket_size)*bucket_size; //! ie, to simplify our lgocis wrt. 'ensuring' that we wist all of the buckets.

      printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
      //! Construct a sample-list, where we have 'buckets' of equal values:
      float *list = new float[list_size];
      for(uint i = 0; i < list_size; i++) {list[i] = rand();}
      //! new test: ------------------------------------------------------------------------------------------

      //! new test: ------------------------------------------------------------------------------------------
      
      { //! comapre the time-complexity of while-loops VS for-loopps, eg, as seen in our graphAlgorithms_distance::insertionSort(..) routien.
	//! Note: we use an "n*n" insertion-sort algorithm to test/evaluate the signfiacne of the differnet approaches, where the latter is implemented in our "graphAlgorithms_distance::insertionSort(..)" routine.
	//! Note: we re-use (ie, make use of) 'this' procedure for mask-specirviciaont when testing for our "kt_forest_findDisjoint.c" (eg, in our "measure_kt_forest_findDisjoint.c") (oekseth, 06. otk. 2016).
	// FIXME[article]: ... update our artilce witht eh observaiton that 'ordered access in the inner forlopp reduces the time-cosnumption by approx. 33 per cent', ie, an approx. 33 per-cent speed-up for insertion-sort (for 'naive implementaiton' VS 'improved implemetnaiton')
	
	// FIXME: update our "insertionSort(..)" wrt. results of [below].
	
	//! Note: in order to evlauate the 'ability' to hide/camfoflate nosie (assicated to randomness) we in [”elow] use both 'random' and 'rverse' intiaition wrt. the 'data to sort':
	for(uint isTo_useRandom_init = 0; isTo_useRandom_init < 2; isTo_useRandom_init++) {

	  if(isTo_evlauateOnlyMergeSort == false) 
	    //if(false) // FIXME: remove
	  { //! Test the time using an 'inner' for-loop:
	    for(uint i = 0; i < list_size; i++) {list[i] = (isTo_useRandom_init) ? rand() : (float)(list_size - i);}
	    const char *idOf_experiement = "insertionSort-reverse";
	    start_time_measurement(); 
	    const uint max_pos_iteration = list_size - 1;
	    uint swapCount = 0; //! which is used to ifner the distance.
	    //uint debug_cnt = 0;
	    for(uint i = list_size - 2; i < list_size; i--) {
	      uint j = i;
	      const float val = list[i];
	      for(; (j < max_pos_iteration) && (list[j + 1] < val); j++) {
		list[j] = list[j + 1];
		//debug_cnt++;
	      }
	    
	      list[j] = val;
	      swapCount += (j - i); //! ie, the number of swaps at this disatnce-position: 'corresponds' to Kendalls tau at "index <= i".
	    }
	    // printf("\t\tdebug_cnt=%u, at %s:%d\n", debug_cnt, __FILE__, __LINE__);
	    //! Then generate the results:
	    char *string = allocate_1d_list_char(default_string_size, default_value_char);; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "articmetic-memory-for-loop-overhead-1d-%u w/traverse-test=%s and data-preOrdering=%s", list_size, idOf_experiement, (isTo_useRandom_init) ? "random" : "reverse"); 
	    //! Update the result-container:
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
	    free_1d_list_char(&string); string = NULL;	  
	  }
	  //! ----------------
	  if(isTo_evlauateOnlyMergeSort == false)  //	  if(false) // FIXME: remove
	  { //! Test the case wrt. an 'while-loop' of [ªbove].
	    for(uint i = 0; i < list_size; i++) {list[i] = (isTo_useRandom_init) ? rand() : (float)(list_size - i);}
	    const char *idOf_experiement = "insertionSort-while-forward";
	    start_time_measurement(); 
	    //uint debug_cnt = 0;
	    for(int j = 1; j < list_size; j++) {
	      const float key = list[j];
	      int i = j -1; // Task 
	      while( (i >= 0) && (list[i] > key) ) {
		//assert(i < (int)list_size); 	      assert(i >= 0); 
		list[i+1] = list[i]; i--; //debug_cnt++;
		//printf("i=%d, at %s:%d\n", i, __FILE__, __LINE__);
		//assert(i < (int)list_size); 	      assert(i >= -1); 
	      }// Moves key forwrad
	      //assert(i < (int)list_size);
	      list[i+1] = key; // Sets
	    }
	    //printf("\t\tdebug_cnt=%u, at %s:%d\n", debug_cnt, __FILE__, __LINE__);
	    //! Then generate the results:
	    char *string = allocate_1d_list_char(default_string_size, default_value_char);; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "articmetic-memory-for-loop-overhead-1d-%u w/traverse-test=%s and data-preOrdering=%s", list_size, idOf_experiement, (isTo_useRandom_init) ? "random" : "reverse"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
	    free_1d_list_char(&string); string = NULL;	  
	  }
	  //! ----------------
	  if(isTo_evlauateOnlyMergeSort == false) 
	  //if(false) // FIXME: remove
	  { //! Make use of intrisnitcts wrt. [ªbove].
	    // FIXME[article]: update the docuemntaiton with this result ... through the use of "memset(..)" we for a reverse-pre-ordered list set manage a perofrmance-increase of 2.2x  for the 'reverse inseriton-sort' and 1.6x increase for teh 'naive' insertion-sort. <-- try to 'correlate' thuis number with the average number of 'data-swappigns' whcih are required. <-- seems like "1.6x" and "2.2x" is often 're-occureing' as time-differences, ie, do you jan-christain have an expalantion/suggestioni 'for this possible correlation'?
	    // FIXME[ªrticle]: similar to [abov€] ntoe that we for a random list of 100,000 items resudes the time-cost from (reverse, ordered)=(tick=13.9100s, tick=10.4900s) to "tick=0.0100s": latter due to the "tick=0.0100s(..)" which impleis that there is no point in updating the list is the 'intermeidate valeus' are un-changed.
	    //! Note: if we include the "debug_cnt" varaible, in thhe wile-loop, the code goes notisbalbe slower: on average the performance-icnrease is reduced from 2.2x to 2.0x. The latter is an examplficiaotn of 'itnroducsing noise into the memory-ache', ie, the importance of 'clean and to-the-point soruce-code'.
	    for(uint i = 0; i < list_size; i++) {list[i] = (isTo_useRandom_init) ? rand() : (float)(list_size - i);}
	    const char *idOf_experiement = "insertionSort-while-forward and use-intrisnitcts=true";
	    start_time_measurement(); 
	    //uint debug_cnt = 0;
	    for(int j = 1; j < list_size; j++) {
	      const float key = list[j];
	      const uint end_pos = j -1; // Task 
	      int i = (int)end_pos; //j -1; // Task 
	      // int debug_cnt = 0;
	      while( (i >= 0) && (list[i] > key) ) {
		//list[i+1] = list[i]; 
		i--; 
		//debug_cnt++;
	      }// Moves key forwrad
	    
	      // FIXME: if this approach resutls in faster code, then (a) translate 'this' into a macro ... and write a similar udpate/comparison for the "mergesort(..)" ... and thereafter for the "quicksort(..)" ... 	    	    
	      if(i != end_pos) { //! then valeus are updated:
		const int length = (j - i) - 1;
		if(length > 0) {
		  memmove(/*dest=*/list + i + 1, list + i, sizeof(float)*length); //! ie, to make room for: list[i+1] = key; // Sets
		  list[i+1] = key; // ie, to 'place the old value in the 'free space' created [above].
		}
	      }
	    }
	    //! Then generate the results:
	    char *string = allocate_1d_list_char(default_string_size, default_value_char);; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "articmetic-memory-for-loop-overhead-1d-%u w/traverse-test=%s and data-preOrdering=%s", list_size, idOf_experiement, (isTo_useRandom_init) ? "random" : "reverse"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
	    free_1d_list_char(&string); string = NULL;	  
	  }

	  if(isTo_evlauateOnlyMergeSort == false) 
	    //if(false) // FIXME: remove
	  { //! Make use of intrisnitcts wrt. [ªbove].
	    // FIXME[article]: ... may you jan-chrsitan explain why [”elow] appraoch/strategy is in-efficent, ie, when compared to the [ªbove] "memmove(..)" call/operationi?
	    //! Note: if we include the "debug_cnt" varaible, in thhe wile-loop, the code goes notisbalbe slower: on average the performance-icnrease is reduced from 2.2x to 2.0x. The latter is an examplficiaotn of 'itnroducsing noise into the memory-ache', ie, the importance of 'clean and to-the-point soruce-code'.
	    for(uint i = 0; i < list_size; i++) {list[i] = (isTo_useRandom_init) ? rand() : (float)(list_size - i);}
	    const char *idOf_experiement = "insertionSort-while-forward and use-intrisnitcts=true alt2";
	    start_time_measurement(); 
	    //uint debug_cnt = 0;
	    for(int j = 1; j < list_size; j++) {
	      const float key = list[j];
	      const uint end_pos = j -1; // Task 
	      int i = (int)end_pos; //j -1; // Task 
	      // int debug_cnt = 0;
	      while( (i >= 0) && (list[i] > key) ) {
		//assert(i < (int)list_size); 	      assert(i >= 0); 
		//list[i+1] = list[i]; 
		i--; 
		//debug_cnt++;
		//printf("i=%d, at %s:%d\n", i, __FILE__, __LINE__);
		//assert(i < (int)list_size); 	      assert(i >= -1); 
	      }// Moves key forwrad
	    
	      // FIXME: if this approach resutls in faster code, then (a) translate 'this' into a macro ... and write a similar udpate/comparison for the "mergesort(..)" ... and thereafter for the "quicksort(..)" ... 	    	    
	      if(i != end_pos) { //! then valeus are updated:
		const int length = (j - i) - 1;
		if(length > 0) {
		  const uint SM = 4; //! ie, due to the 16 bytes in "_mm_storeu_ps(..)" and the sizeof(float)==4, ie, 4 'chunks'.
		  if(length > SM) {
		    uint numberOf_chunks = 0; uint chunkSize_row_last = length;
		    // FIXME: in [”elow] why do we need "length-1"?
		    __get_updatedNumberOF_lastSizeChunk_entry(/*nrows=*/(uint)(length-1), SM, &numberOf_chunks, &chunkSize_row_last);
		    assert(numberOf_chunks != UINT_MAX); assert(numberOf_chunks != 0);
		    const uint k_group_size = (chunkSize_row_last == SM) ? numberOf_chunks : numberOf_chunks - 1;	      
		    int k = (int)end_pos - 4;
		    if((k_group_size > 0) ) {
		      // printf("k=%u, k_group_size=%u, end_pos=%u, length=%d, at %s:%d\n", k, k_group_size, end_pos, length, __FILE__, __LINE__);
		      assert(k >= 0); assert(k < (int)end_pos);
		      for(uint k_group = 0; k_group < k_group_size; k -= 4, k_group++) {
			//! then we move the four elements 'forward' with step=1.
			assert(k >= 0); assert(k >=i);
			__m128 term1  = _mm_loadu_ps(&list[k]);		
			// FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
			__m128 arr_result_tmp = _mm_loadu_ps(&list[k+1]); //! ie, where "+1" is to move aa bload of foru floats forward.
			// fixme: valdiate orrectenss of [below].
			_mm_storeu_ps(&list[k], arr_result_tmp);
		      }
		    } //else {k = end_pos;}
		    //! move the reamdining 'items' forward:
		    memmove(/*dest=*/list + i + 1, list + i, sizeof(float)*/*length=*/chunkSize_row_last); //! ie, to make room for: list[i+1] = key; // sets
		  } else { //! then we have less than 4 elements to move, ie, use the ANSI optimized move-procedure:
		    memmove(/*dest=*/list + i + 1, list + i, sizeof(float)*length); //! ie, to make room for: list[i+1] = key; // Sets
		  }

		  
		  list[i+1] = key; // ie, to 'place the old value in the 'free space' created [above].
		}
	      }
	    }
	    //! then generate the results:
	    char *string = allocate_1d_list_char(default_string_size, default_value_char);; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "articmetic-memory-for-loop-overhead-1d-%u w/traverse-test=%s and data-preordering=%s", list_size, idOf_experiement, (isTo_useRandom_init) ? "random" : "reverse"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
	    free_1d_list_char(&string); string = NULL;	  
	  }
	  { //! Compare teh use of merge-sort with 'direct copying' and 'implcit copying'.
	    //! Note(1): we observve that the use of "memmove(..)" (and/or "memcpy(..)") results in a performance-ovrehad (Wrt. merge-sort when compared to a 'naive' approach where hte items are copied directly): wrt. "memmove(..)" we for a 'random access-pattern' gets a perforamnce-overehad of 1.20x while for a 'reverse' dat-data-roder gets a performance-overehad of 1.02x. From the latter we infer that the 'optimization' gained by using "memmove(..)" is severly overweighted/reduced by the cost (fo the latter fucniton-call combiend with the overhead in the artimetic operations). In this  ontext it is of itnerest to ntoe/observe that memmove(..) goes approx. 1.1x faster tahn "memcpy(..)" <-- FIXME: may you jan-crhstian explain the difference  of the latter?
	    //! Note(2): if we use the 'non-optimzilsed sorting' then the 'improved' merge-sort' goes 1.04x faster for 'reverse-rodered-datset', while 1.5x slower for a for 'random-ordered-datset'
	    // FIXME[article]: use [ªbove] "note(1)" and "note(2) in oru artilce.
	    { //! Test merge-sort, first without optimilaisoatn using "memset(..)":
	      float *arr_result = allocate_1d_list_float(list_size, default_value_float); memset(arr_result, 0, sizeof(float)*list_size);
	      for(uint i = 0; i < list_size; i++) {list[i] = (isTo_useRandom_init) ? rand() : (float)(list_size - i);}
	      const char *idOf_experiement = "mergeSort use-memset-in-sorting=false";
	      //! The call:
	      start_time_measurement();
	      sort_array(list, arr_result, list_size, e_typeOf_sortFunction_mergeSort_internal);
	      //! then generate the results:
	      char *string = allocate_1d_list_char(default_string_size, default_value_char);; assert(string); memset(string, '\0', 1000);
	      sprintf(string, "articmetic-memory-for-loop-overhead-1d-%u w/traverse-test=%s and data-preordering=%s", list_size, idOf_experiement, (isTo_useRandom_init) ? "random" : "reverse"); 
	      __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
	      free_1d_list_float(&arr_result);;
	    }
	    { //! Test merge-sort, first without optimilaisoatn using "memset(..)":
	      // FIXME: if [”elow] result in an optimization then add a new enum named e_typeOf_sortFunction_mergeSort_internal_naive ... and then update [ªbove]
	      float *arr_result = allocate_1d_list_float(list_size, default_value_float); memset(arr_result, 0, sizeof(float)*list_size);
	      for(uint i = 0; i < list_size; i++) {list[i] = (isTo_useRandom_init) ? rand() : (float)(list_size - i);}
	      const char *idOf_experiement = "mergeSort use-memset-in-sorting=true";
	      //! The call:
	      start_time_measurement();
	      sort_array(list, arr_result, list_size, e_typeOf_sortFunction_mergeSort_internal_memoryAcceesOptimized);
	      //! then generate the results:
	      char *string = allocate_1d_list_char(default_string_size, default_value_char);; assert(string); memset(string, '\0', 1000);
	      sprintf(string, "articmetic-memory-for-loop-overhead-1d-%u w/traverse-test=%s and data-preordering=%s", list_size, idOf_experiement, (isTo_useRandom_init) ? "random" : "reverse"); 
	      __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
	      free_1d_list_float(&arr_result);;
	    }
	  }
	}
      }
      //! De-allcoate:
      free_1d_list_float(&list);;
      //delete [] list;
    }
  }
