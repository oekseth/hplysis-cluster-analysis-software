#ifndef kt_clusterAlg_fixed__alg__miniBatch_h
#define kt_clusterAlg_fixed__alg__miniBatch_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_clusterAlg_fixed__alg__miniBatch.h
   @brief defined the mini-bathc algorithm (oekseth, 06. feb. 201y)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
**/

//#include "graphAlgorithms_distance.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_distance.h"
#include "math_generateDistribution.h"
//#include "math_generateDistribution.h"
#include "log_clusterC.h"
#include "kt_matrix.h"

/**
   @struct s_kt_clusterAlg_fixed__alg__miniBatch
   @brief specifies properties used comptuation of the 'minmibath' algorithm (oekseth, 06. jan. 2017).
   @remarks For detials wrt. the "miniibach-k-means" Algorithm see the 'original' article by D. Sculley at Google ["https://www.eecs.tufts.edu/~dsculley/papers/fastkmeans.pdf"]
   @remakrs this implemtnation is inpsired by the poor implementaions found in the work of ("https://algorithmicthoughts.wordpress.com/2013/07/26/machine-learning-mini-batch-k-means/" and 
   "https://github.com/siddharth-agrawal/Mini-Batch-K-Means")
   @remarks the algorithm consists of the following steps:
   step(1): setup: intlaizatin of the algorithm;
   step(1.a): choose/seelct the randomziaotn-type to 'be used' wrt. sleeciton of matrix-subset for: (a) 'start-rows' and (b) 'rows in each iteration';
   step(1.b): pre-configruation: idneitfy a 'mean' count/number of rows to use as a 'representive threshold';
   step(1.c): random-start-nodes: use the 'mean' count to 'at random' select 'mean feature-vectors';
   step(2): perform "self->cnt_iterations" where we for each iteration:
   step(2.a): choose/select/identify a "self->cnt_cluster" 'random set of rows':
   step(2.b): find vertices in the 'intal means-matrix-batch' which are closest to the 'randomly selected vertices': idneitfy the 'ranozmied clsuter-selection' which has the minimum-deivaiton when compared to the 'itnal randomzaiton-appraoch': chhose/idneitfy/select/find the min-distance between 'init-randomzied-sample' and 'current-randomzied-sample': $cluster[i<cnt_clusters] = min_{j}\{$\sum (d_{batchRandom}(i, k) - d_{initRandom}(j, k))^2 \}$; for the latter use the hpLysis "apply__extensive__hp_distance(..)" function, where simlarity-emtic is sedefiend by "self->objMetric__sim";
   step(2.c): Take gradient-step: get per-center learning-rate by 1/#(found); update the 'mean-matrix' to capture central vertices in the data-set: 
   step(2.c.1): remember the old seleciton: copy the 'means-matrix' into the 'centroids' matrix;
   step(2.c.2): Iterate through each of the "cnt_clusters":  update each of the features in the 'means' matrix with: $ p*row_{bestCenter}(j) + p_{not}*row_{random}(j) $; from 'adjustment-equation' we observe how the "k-means-miniBatch' algorithm 'reslts' in a high-score if a row is selected as a cluster-center 'by a factor greater than by randomness', ie, for which the 'random selection-phase' needs to capture the 'speciics' fo the input-data-set;
   step(2.d): 
   step(): 
   step(): 
   @remarks improvement-opportunity, ie, generic wekaness of the "minibatch" algorithm: selection of random start-feature-vectors: both wrt. 'the number of rows' to select and 'the rows which are sleected at random'. Possitble optmization-strateguies are/cosners use a 'a priory' knolwedge, such as: "k-means++ nitantion-procedure", "central vertices idneitfed in either iterativeeigenVector, HCA, or both hca and iterativeeigenVector", etc.; in hpLysis we have generalized the 'random sleeciton' through use/applciaiton of the/our "s_kt_randomGenerator_vector_t" data-structrea (and assicated functions).
   @remarks VS("k-means"::coverage): while "minibatch" choosses/used a pre-deifned subset of vertices 'in its evlauation' "k-means" evaluates all vertices
   --- note: ... iot. merge "minibatch" into "kmeans"  we need/require an approach 
   --- note: ... 
   @remarks VS("k-means"::randomSelection): while "minibatch" selects cenotrids at random, "k-means" selects cluster-mberships (for all vertices) at random
   @remarks VS("k-means"::clusterSelection): while "minibatch" comptus/adjsut the center based on the relative frequency of features-scores, "k-means" 'noramlizes' the feature-vectors across/for/along the mberships in a given cluster
   //! ---
   @remarks VS("k-means++(scikit)"::coverage): while "minibatch" choosses/used a pre-deifned subset of vertices 'in its evlauation', "k-means++(scikit)" selects a random-subset with a sigifncat smaller number of entites (when the defulat cofngiraution is used, ie, a smaller subset/sub-space is evlauated);
   @remarks VS("k-means++(scikit)"::randomSelection): while "minibatch" selects cenotrids at random, "k-means++(scikit)" selects the 'farthest-away-distances' as a selection-criteria;
   @remarks VS("k-means++(scikit)"::clusterSelection): while "minibatch" comptus/adjsut the center based on the relative frequency of features-scores, "k-means++(scikit)" ...
   //! ---
   @remarks VS(""::coverage): while "minibatch" choosses/used a pre-deifned subset of vertices 'in its evlauation' "" evaluates ... vertices
   @remarks VS(""::randomSelection): while "minibatch" selects cenotrids at random, "" selects cluster-mberships (for all vertices) ... ;
   @remarks VS(""::clusterSelection): while "minibatch" comptus/adjsut the center based on the relative frequency of features-scores, "" ... 
// assert(false) // FIXME[coceptual::kmeans]:: add supprot for 'only choosing the centorids' ... and then idneityf the vertices which are closest .... ie, to 'drop'/replac ethe 'k-means or k-median' step with a 'kmeans++' appraoch 'inside each' ... and add optional support for 'then callign the [medoid, means, median] centorid-appraoc, ie, before using/'contiuing' with the 'cluster-best-fit appraoch' (ie, where we in the latter to use the ordinary/default k-means centoid-seleciotn-appraoch) <--- try improving this algorithm-scaffold .... and 'figure out how' to simplify an evlauationi of different eprmtautions wr.t the latter.
 **/
typedef struct s_kt_clusterAlg_fixed__alg__miniBatch {
  t_float cnt_means_realative; //! ie, realtive number-of-random-rows-to-use: should be between [0.0001, 0.99999]
  uint cnt_iterations;
  e_kt_randomGenerator_type_t typeOf_randomNess; //! is the type of randomenss to be used in the idneitifciaotn of random vectors: for4 details see our "math_generateDistribution.h"
  bool isTo_initIntoIncrementalBlocks; //! ie, which if set to false re-uses ealrier randomziaotn.
  //e_kt_randomGenerator_type_t typeOf_randomNess;
  bool config__perf__useFastDistFunction; //! where for for "false" only supports the dist-metric: $cluster[i<BATCH_SIZE] = min_{j}\{\sum (d_{batchRandom}(i, k) - d_{initRandom}(j, k))^2 \}$
  s_kt_correlationMetric_t objMetric__sim; //! which is the corlreionat/silmiarty-emtric to be sued.
} s_kt_clusterAlg_fixed__alg__miniBatch_t;

//! Intiaite the "s_kt_clusterAlg_fixed__alg__miniBatch_t" object.
//! @return an itnaited object.
s_kt_clusterAlg_fixed__alg__miniBatch_t initAndReturn__s_kt_clusterAlg_fixed__alg__miniBatch_t();
//! Infers clusters uisng the mini-batch algorithm (oesketh, 06. feb. 2017).
const bool kt_clusterAlg_fixed__alg__miniBatch(const s_kt_clusterAlg_fixed__alg__miniBatch_t *self, const s_kt_matrix_t *matrix_input, const uint cnt_clusters, s_kt_clusterAlg_fixed_resultObject_t *obj_result);

#endif //! EOF
