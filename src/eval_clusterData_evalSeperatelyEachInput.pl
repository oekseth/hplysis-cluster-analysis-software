=head
@brief evaluates GOLD-CCM seperately for each dta-colleciotn (produced by "eval_clusterData.pl")
=cut
use strict;
my @clust_alg = (
	"algorithm::k-means::avg",
	"algorithm::k-means::rank",
	"algorithm::k-means::medoid",
	"algorithm::hpCluster",
	"algorithm::disjoint::kdTree", "algorithm::disjoint::kdTree::CCM",
	"algorithm::HCA::single", "algorithm::HCA::max",
	"algorithm::HCA::average", "algorithm::HCA::centroid", "algorithm::Kruskal::HCA",
	"algorithm::k-means::altAlg::miniBatch",
	"algorithm::random::best"
);

my @hashAlg = ();
my $metric_index = 0;
foreach my $metric_sim (
	"e_kt_correlationFunction_groupOf_minkowski_euclid",
  "e_kt_correlationFunction_groupOf_minkowski_euclid",
  "e_kt_correlationFunction_groupOf_minkowski_cityblock",
	"e_kt_correlationFunction_groupOf_minkowski_minkowski",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Gower",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Soergel",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Canberra",
  "e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian",
  "e_kt_correlationFunction_groupOf_intersection_WaveHedges",
  "e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt",
  "e_kt_correlationFunction_groupOf_intersection_Czekanowski",
  "e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef",
  "e_kt_correlationFunction_groupOf_innerProduct_innerProduct",
  "e_kt_correlationFunction_groupOf_innerProduct_harmonicMean",
  "e_kt_correlationFunction_groupOf_innerProduct_cosine",
  "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook",
  "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef",
  "e_kt_correlationFunction_groupOf_innerProduct_Dice",
  "e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef",
  "e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance",
  "e_kt_correlationFunction_groupOf_fidelity_fidelity",
  "e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya",
  "e_kt_correlationFunction_groupOf_fidelity_Hellinger",
  "e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef",
  "e_kt_correlationFunction_groupOf_fidelity_Matusita",
  "e_kt_correlationFunction_groupOf_squared_Euclid",
  "e_kt_correlationFunction_groupOf_squared_Pearson",
  "e_kt_correlationFunction_groupOf_squared_Neyman",
  "e_kt_correlationFunction_groupOf_squared_squaredChi",
  "e_kt_correlationFunction_groupOf_squared_probabilisticChi",
  "e_kt_correlationFunction_groupOf_squared_divergence",
  "e_kt_correlationFunction_groupOf_squared_Clark",
  "e_kt_correlationFunction_groupOf_shannon_KullbackLeibler",
  "e_kt_correlationFunction_groupOf_shannon_Jeffreys",
  "e_kt_correlationFunction_groupOf_shannon_kDivergence",
  "e_kt_correlationFunction_groupOf_shannon_Topsoee",
  "e_kt_correlationFunction_groupOf_shannon_JensenShannon",
  "e_kt_correlationFunction_groupOf_combinations_Taneja",
  "e_kt_correlationFunction_groupOf_combinations_KumarJohnson",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin",
  "e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric",	
	"e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine"
	) {
	foreach my $norm_metric (
	    "row-Relative-Avg-Abs",
	    "row-Relative-Rank",
	    "" #! ie, none.
	    ) {
	    foreach my $metric_clusterAlg (@clust_alg) {
		    # printf("[%d] = %s\n",  $metric_index, $metric_clusterAlg);
                    push(@hashAlg, $metric_clusterAlg); #! ie, maps the 'internal metirc-idnex' to the given clsutera-lgorithm
                    $metric_index += 1;
                }
           }
}

my @arr_files = (
"data/local_downloaded/56_msq.csv.hpLysis.tsv",
"data/local_downloaded/20_chorSub.csv.hpLysis.tsv",
"data/local_downloaded/13_pulpfiber.csv.hpLysis.tsv",
"data/local_downloaded/4_randu.csv.hpLysis.tsv",
"data/local_downloaded/32_cf.csv.hpLysis.tsv",
"data/local_downloaded/airquality.csv.hpLysis.tsv",
"data/local_downloaded/39_UScrime.csv.hpLysis.tsv",
"data/local_downloaded/46_pottery.csv.hpLysis.tsv",
"data/local_downloaded/40_Hedonic.csv.hpLysis.tsv",
"data/local_downloaded/38_Melanoma.csv.hpLysis.tsv",
"data/local_downloaded/49_affect.csv.hpLysis.tsv",
"data/local_downloaded/54_Holzinger.csv.hpLysis.tsv",
"data/local_downloaded/47_smoking.csv.hpLysis.tsv",
"data/local_downloaded/0_airquality.csv.hpLysis.tsv",
"data/local_downloaded/51_bfi.csv.hpLysis.tsv",
"data/local_downloaded/19_Hartnagel.csv.hpLysis.tsv",
"data/local_downloaded/1_attitude.csv.hpLysis.tsv",
"data/local_downloaded/37_gilgais.csv.hpLysis.tsv",
"data/local_downloaded/42_cancer.csv.hpLysis.tsv",
"data/local_downloaded/52_burt.csv.hpLysis.tsv",
"data/local_downloaded/21_votes.repub.csv.hpLysis.tsv",
"data/local_downloaded/attitude.csv.hpLysis.tsv",
"data/local_downloaded/57_msq.csv.hpLysis.tsv",
"data/local_downloaded/33_Arbuthnot.csv.hpLysis.tsv",
"data/local_downloaded/3_LifeCycleSavings.csv.hpLysis.tsv",
"data/local_downloaded/45_phosphate.csv.hpLysis.tsv",
"data/local_downloaded/8_alcohol.csv.hpLysis.tsv",
"data/local_downloaded/31_aldh2.csv.hpLysis.tsv",
"data/local_downloaded/43_pbc.csv.hpLysis.tsv",
"data/local_downloaded/23_Airline.csv.hpLysis.tsv",
"data/local_downloaded/l.hpLysis.tsv",
"data/local_downloaded/62_Nursing.csv.hpLysis.tsv",
"data/local_downloaded/16_wood.csv.hpLysis.tsv",
"data/local_downloaded/5_rock.csv.hpLysis.tsv",
"data/local_downloaded/50_affect.csv.hpLysis.tsv",
"data/local_downloaded/34_Cavendish.csv.hpLysis.tsv",
"data/local_downloaded/36_Boston.csv.hpLysis.tsv",
"data/local_downloaded/44_heptathlon.csv.hpLysis.tsv",
"data/local_downloaded/14_salinity.csv.hpLysis.tsv",
"data/local_downloaded/60_Fertility.csv.hpLysis.tsv",
"data/local_downloaded/41_bladder.csv.hpLysis.tsv",
"data/local_downloaded/59_barro.csv.hpLysis.tsv",
"data/local_downloaded/53_epi.bfi.csv.hpLysis.tsv",
"data/local_downloaded/iris.data.hpLysis.tsv",
"data/local_downloaded/35_environmental.csv.hpLysis.tsv",
"data/local_downloaded/17_neuro.csv.hpLysis.tsv",
"data/local_downloaded/12_NOxEmissions.csv.hpLysis.tsv",
"data/local_downloaded/2_crimtab.csv.hpLysis.tsv",
"data/local_downloaded/63_Pines.csv.hpLysis.tsv",
"data/local_downloaded/15_toxicity.csv.hpLysis.tsv",
"data/local_downloaded/27_Cigarette.csv.hpLysis.tsv",
"data/local_downloaded/9_ambientNOxCH.csv.hpLysis.tsv",
"data/local_downloaded/28_Crime.csv.hpLysis.tsv",
"data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv",
"data/local_downloaded/0_phosphate.csv.hpLysis.tsv",
"data/local_downloaded/24_BudgetItaly.csv.hpLysis.tsv",
"data/local_downloaded/7_USJudgeRatings.csv.hpLysis.tsv",
"data/local_downloaded/11_milk.csv.hpLysis.tsv",
"data/local_downloaded/6_swiss.csv.hpLysis.tsv",
"data/local_downloaded/26_Cigar.csv.hpLysis.tsv",
"data/local_downloaded/55_msq.csv.hpLysis.tsv",
"data/local_downloaded/29_Electricity.csv.hpLysis.tsv",
"data/local_downloaded/22_nuts.csv.hpLysis.tsv",
"data/local_downloaded/18_Ginzberg.csv.hpLysis.tsv",
"data/local_downloaded/48_affect.csv.hpLysis.tsv",
"data/local_downloaded/30_Forward.csv.hpLysis.tsv",
"data/local_downloaded/10_coleman.csv.hpLysis.tsv",
"data/local_downloaded/58_neo.csv.hpLysis.tsv",
"data/local_downloaded/25_BudgetUK.csv.hpLysis.tsv",
    );



sub get_files {
    my ($file_index) = @_;
    my $directory = '.';
    opendir (DIR, $directory) or die $!;
    my $file_name = $arr_files[$file_index];
    # my @arr_result = ();
    my %hashClusters;
    my $max_count = 0;
    while (my $file = readdir(DIR)) {
	# printf("eval: file: %s\n", $file);
	if($file =~/^eval_data_(\d+)_metric_(\d+).+\.tsv$/) {
	    if($1 == $file_index) {
		# printf("eval: file: %s\n", $file);
		my $metric_index = $2;
		my $clusterAlg = $hashAlg[$metric_index];
		if($clusterAlg =~ /^\s*$/) {
		    printf("!!\t ERROR:eval: file: %s, index=%d\n", $file, $metric_index);
		}
		# printf("- add-cols[|%s|]::\topens-file=%s\n", $clusterAlg, $file);		
		{ #! then fetch the cluster-memberships.
		    my $input_file = $file;
		    open(FILE_IN, "<$input_file") or die("!!\t input(file): An error in opning file=\"$input_file\"");
		    my $arr_clusters = $hashClusters{$clusterAlg};
		    # printf("opens-file=%s\n", $file);
		    if(!defined($arr_clusters)) {
			$hashClusters{$clusterAlg} = [];
			$arr_clusters = $hashClusters{$clusterAlg};
		    }
		    while (my $line = <FILE_IN>) {
			chomp($line); #! ie, remvoe the trail-newline.    while (my $line = <FILE_IN>) {
			if($line !~ /^\s*\#/) {
			    my @arrOf_cols = split("\t", $line);
			    #! FIX: isuses in roenrrosu cluste-re-mbershisp
			    my $cnt_cols = scalar(@arrOf_cols);
			    if($max_count < $cnt_cols) {
				$max_count = $cnt_cols;
			    }
			    for(my $i = 0; $i < $cnt_cols; $i++) {
				if($arrOf_cols[$i] > 100000) { #! ie, an 'empty' value:
				    $arrOf_cols[$i] = 0;
				}
			    }
			    # printf("add-cols[|%s|]::\topens-file=%s\n", $clusterAlg, $file);
			    push(@{$arr_clusters}, \@arrOf_cols); 
			}
		    }
		    close(FILE_IN)
		}
		# push(@arr_result, {"file" => $file, clustAlg => $clustAlg } );
	    }
	}
    }
    close(DIR);
    return ($file_name, \%hashClusters, $max_count);
}

my $tmpFile_rCluster         = "tmp_clusterRand_" . rand() . ".tsv";
my $tmpFile_ccm              = "tmp_ccm_" . rand() . ".tsv";
my $tmpFile_clusters         = "tmp_algClusterId_" . rand() . ".tsv";


my $gold_index = 0;
foreach my $ccm_gold ("Rands-index", "Rands-index (alternative-implementation)", "Adjusted Rand Index (ARI)", "Chi-squared") {
    my $resultFile = "eval_data_result_" . $gold_index . ".tsv";
    $gold_index += 1;

    open(FILE_RESULT, ">$resultFile") or die("!!\t input(file): An error in opning file=\"$resultFile\"");
    { #! Write out header:
	my @arr_result = ("#! file");
	foreach my $metric_clusterAlg (@clust_alg) {
	    push(@arr_result, $metric_clusterAlg);
	}
	print(FILE_RESULT join("\t", @arr_result) . "\n");
    }
    # exit;
    #my $file_index = 49; {
    for(my $file_index = 0; $file_index < scalar(@arr_files); $file_index += 1) {
	my ($file_name, $hashClusters, $max_count) = get_files($file_index);
	#! Step(): comptue a reference 'random' cluster:
	{
	    my $result_file = $tmpFile_rCluster;
	    open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");
	    #!
	    my @arr_clustMemerships = ("#! row-header"); 
	    for(my $i = 0; $i < ($max_count-1); $i++) {
		push(@arr_clustMemerships, "col-" . $i);
	    }
	    printf(FILE_OUT join("\t", @arr_clustMemerships) . "\n");
	    my $cnt_head = scalar(@arr_clustMemerships);
	    @arr_clustMemerships = ("head-rand"); 
	    for(my $i = 0; $i < ($max_count-1); $i++) {
		my @range = ( 0 .. $max_count );
		my $clust_id = $range[rand(@range)];
		#my $clust_id = rand() % ($max_count + 1);
		if(($i > 0) && ($clust_id == $arr_clustMemerships[$i-1])) {$clust_id = $i;} #! ie, to aovid all numbers from being the same.
		push(@arr_clustMemerships, $clust_id);
	    }
	    my $cnt_data = scalar(@arr_clustMemerships);
	    if($cnt_head != $cnt_data) {
		printf("!!\t Investigate why head:%d != data:%d\n", $cnt_head, $cnt_data);
	    }
	    printf(FILE_OUT join("\t", @arr_clustMemerships) . "\n");
	    close(FILE_OUT);
	}
	my @arr_result = ($file_name);
	foreach my $metric_clusterAlg (
	    #"algorithm::k-means::altAlg::miniBatch",
	    @clust_alg
	    ) {
	    my $mat_clusters = $hashClusters->{$metric_clusterAlg};
	    #printf("has-data? alg: " . $metric_clusterAlg . "\n");
	    if(defined($mat_clusters)) {
		#print("YES: has-data");
		#! Step(): Concatenate files:
		{
		    my $result_file = $tmpFile_clusters;
		    open(FILE_OUT, ">$result_file") or die("!!\t input(file): An error in opning file=\"$result_file\"");
		    my $head_isAdded = 0;
		    foreach my $arr (@{$mat_clusters}) {
			if(scalar(@{$arr}) > 0) {
			    if($head_isAdded == 0) {
				my @arr_h = ("#! matrix-head");
				for(my $i = 0; $i < (scalar(@{$arr})-1); $i += 1) {
				    push(@arr_h, "col-" . $i);
				}
				printf(FILE_OUT join("\t", @arr_h) . "\n");
				$head_isAdded = 1;			    
			    }			    
			    printf(FILE_OUT join("\t", @{$arr}) . "\n");
			}
			
		    }
		    close(FILE_OUT);		
		}
		#! Step(): Compute the clusters:
		my $result_file = $tmpFile_ccm;
		#my $cmd = "./x_hp -ops=$ccm_gold -file2=$tmpFile_rCluster -file1=$tmpFile_clusters -out=$result_file";
		my $cmd = "./x_hp -ops=$ccm_gold -file1=$tmpFile_rCluster -file2=$tmpFile_clusters -out=$result_file";
		print($cmd . "\n");
		my $result_str = `$cmd`;
		if(-f $result_file) {
		    #! Step(): Select extreme values
		    open(FILE_IN, "<$result_file") or die("Unable to open the input-file $result_file\n");
		    my $row_id = 0;
		    my $min_def = 100000; my $max_def = -100000;
		    my $min = $min_def;   my $max = $max_def;
		    while (my $line = <FILE_IN>) {
			chomp($line); #! ie, remvoe the trail-newline.
			#printf("$line\n");
			if( ($line !~ /^\#/) && ($row_id > 0) ) {
			    #if($row_id > 0) {
			    my @arrOf_cols = split("\t", $line);
			    shift(@arrOf_cols); #! ie, remove hte row-header.
			    foreach my $ccm_score (@arrOf_cols) {
				#my $ccm_score = @arrOf_cols[1];
				if($min > $ccm_score) {
				    $min = $ccm_score;
				}
				if($ccm_score < 10000) {
				    if($max < $ccm_score) {
					$max = $ccm_score;
				    }
				}
				#printf("%d\t%f\n", $file_id, $ccm_score);
			    }
			}
			$row_id++;
		    }
		    close(FILE_IN);
		    #! Step(): Write out
		    push(@arr_result, $min);
		    push(@arr_result, $max);		
		}
	    }
	    #! Step(): Write out
	    print(FILE_RESULT join("\t", @arr_result) . "\n");
	    # exit;
	}
	#my @arr_clustAlg = get_files($file_index);
	
    }

    close(FILE_RESULT);
}
