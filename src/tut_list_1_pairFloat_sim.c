#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_sparse_sim.h" //! which is used for sparse comptautions of simlairty-metircs.

/**
   @brief how to use a list of paris as input to a sparse data-set comptuation.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks extends our "tut_list_0_pairFloat.c" with a 'call' to oru "kt_sparse_sim.h"
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 5; const uint ncols = 3; 
  //!
  //! Specify the simlairty-metic: 
  s_kt_correlationMetric_t sim_metric = setTo_empty__andReturn__s_kt_correlationMetric_t();
  sim_metric.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  //! -----------------------------------------------------------------------------------
  //!
  //! Allocate:
  const uint list_size = nrows*ncols;
  s_kt_list_1d_pairFloat_t vec_1 = init__s_kt_list_1d_pairFloat_t(list_size);
  assert(vec_1.list_size == list_size);
  //!
  //! Set values:
  uint curr_pos = 0;
  const t_float distance_default = 1;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      assert(curr_pos < vec_1.list_size);
      vec_1.list[curr_pos] = MF__initVal__s_ktType_pairFloat(row_id, col_id, distance_default);
      //! What we expect:
      s_ktType_pairFloat_t obj = vec_1.list[curr_pos];
      assert(obj.head == row_id);
      assert(obj.tail == col_id);
      assert(obj.score == distance_default);
      //! Increment:
      curr_pos++;
    }
  }
  assert(curr_pos == list_size);
  //!
  //! Convert into a 2d-list:
  s_kt_set_2dsparse_t sparse_1 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(&vec_1);
  assert(sparse_1._nrows == nrows); //! ie, gvien our [ªbove] isneriotn-policy.
  //!
  //! Compute simliarty:
  s_kt_sparse_sim obj_sim = init__s_kt_sparse_sim_t(sim_metric);
  s_kt_set_2dsparse_t sparse_result = computeStoreIn__2dList__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/NULL, /*isTo_rememberScores=*/true, /*weight=*/NULL);
  //! De-allcoate the sparse cofnigruation-object:
  free__s_kt_sparse_sim_t(&obj_sim);
  //!
  //! WRite out the result: access the 2d-list and write out the results:
  for(uint row_id = 0; row_id < sparse_1._nrows; row_id++) {
    uint key_val_size = 0; uint list_val_size = 0;
    const uint *key_val = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&sparse_1, row_id, &key_val_size);
    const t_float *list_val = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&sparse_1, row_id, &list_val_size);
    /* //! What we expect: */
    /* assert(key_val_size == ncols); */
    /* assert(list_val_size == ncols); */
    assert(key_val);
    assert(list_val);
    //!
    //! Read through the 'sparse' data-set:
    for(uint col_id = 0; col_id < key_val_size; col_id++) {
      /* assert(key_val[col_id] == col_id); */
      /* assert(list_val[col_id] == distance_default); */
      printf("[%u][%u] = %f, at %s:%d\n", row_id, col_id, list_val[col_id], __FILE__, __LINE__);
    }
  }

  

  //!
  //! De-allocates:
  free__s_kt_list_1d_pairFloat_t(&vec_1);
  free_s_kt_set_2dsparse_t(&sparse_1);
  //free__s_kt_list_1d_pairFloat_t(&vec_1);
  free_s_kt_set_2dsparse_t(&sparse_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}


