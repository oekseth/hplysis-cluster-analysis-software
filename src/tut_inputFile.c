#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "aux_sysCalls.h"
/**
   @brief examplify the use of a 'advanced inptu-file specificaiton' through the "s_kt_matrix_fileReadTuning_t" into hpLysis-clustering.  (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks Demonstrates features wrt.:
   -- input: use of logics in "s_kt_matrix_t" to load input-files: examplfies both a 'direct' speicficaiton of attributes, and a geneirc stragty using lgoics and data-strucutres deifned in our "hp_clusterFileCollection.h"
   -- computation: use of different correlation/simliarty metrics
   -- result(a): how results from mmuliple clusterings may be exported into one single-unified java-script file.
   -- result(b): an iterative appraoch to print out cluster-conistnecy-scores when compating an input-amtrix to a godl-standard-set of clusters:
**/
int main() 
#endif
{
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
  //! Default configurations:
  //const bool config__typeOfOps__onlySimMetric = true;
  const bool config__isToPrintOut__iterativeStatusMessage = true;
  //! ----
  const char *pathTo_localFolder_storingEachSimMetric = "results/tutEx__difficultData/";
  const char *nameOf_resultFile__clusterMemberships = "tutResult_simMetrics.js";
  const char *nameOf_resultFile__clusterMemberships__deviation = "tutResult_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //const bool config_corrMetric_prior_use = true; //! which otherwise impleis that we do Not compute the correlaiton/simlairty emtric (Before clsutering).
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  const uint config_arg_npass = 10000; 
  //!
  //! Result data:
  const e_hpLysis_export_formatOf_t config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
  /* const bool isTo_transposeMatrix = false;   */
  /* const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0; */
  /* const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  0; */
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = 0;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  if(true) {
    fileRead_config__syn.imaginaryFileProp__nrows = 10;
    fileRead_config__syn.imaginaryFileProp__ncols = 10;
  } else { //! then we are interested in a more performacne-demanind approach: 
    fileRead_config__syn.imaginaryFileProp__nrows = 400;
    fileRead_config__syn.imaginaryFileProp__ncols = 400;
  }
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = 0;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.





  //!
   //! Specify a list of data to use: 
#if(1 == 1)
#define __Mi_c_mapOf_realLife__isStruct 1
  const uint mapOf_realLife_size = 6 + 11 + 3; //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    // FIXME[article]: update our aritlce wrt. [”elow] 'note' tags/descipritons.
    // FIXME[article]:  ... 
    // FIXME[article]:  ... 
    //! -------------------------------------------------- 
    //! Note[<syntetic> w/cnt=6]: .... 
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ---- 
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ---- 
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! -------------------------------------------------- 
    //! 
    //! 
    //! Note[<real-life-small-data-sets> w/cnt=11]: 
    //! ---- 
    //! cnt_clusters=2, size=15; seperation=ambiguous-overlapping; Trait="curved, intersecting"; Euclid oversimplifies/'extremifies' the speration, while canberra is uanble to capture the 'curved relationships': the other metrics are unable to capture the relationjships. 
    {/*tag=*/"birthWeight_chinese", /*file_name=*/"tests/data/kt_mine/birthWeight_chineseChildren.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ... cnt_clusters=1, size=; seperation=periodic; Trait="curved, intersecting"; Euclid 
    {/*tag=*/"butterFat_cows", /*file_name=*/"tests/data/kt_mine/butterFat_percentage_cows.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! cnt_clusters=1, size=11; seperation='well-defined'; Trait="curved, x-axis-start-point differs"; Euclid 
    {/*tag=*/"fishGrowth", /*file_name=*/"tests/data/kt_mine/fish_growth.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ... cnt_clusters=2, size=4; seperation=ambiguous; Trait="curved, fluctating, discrete-sinus"; Euclid 
    // FIXME: consider using [below] as an example of 'periodic clsuters which are diicuflt to clsuter'.
    // FIXME: consider to transpose [below] .... figure out how to 'hanlde' the case where 'a ranking' would result in isngificnat seperation 
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"gallsThorax", /*file_name=*/"tests/data/kt_mine/galls_thorax_length_transposed.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ------ 
    //! 
    //! "Spottinggrade": cnt_clusters=2, size=4; seperation='change VS not-change, ie, a non-linear comparison'; Trait="curved, intersecting"; ....??... <-- asusmes other metrics will find thios challenging
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"spottingGrade", /*file_name=*/"tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ---- 
  //! ... cnt_clusters=2, size=3; seperation='ambigious: silamirlty in curve-increase'; Trait="curved, simliar-peaks"; 
    {/*tag=*/"guinePigs", /*file_name=*/"tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_housed_pregnancy_first.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! "limesOxygen": if transposed nearly-all (ie, xmt. "species 2, 75 seawater") seems to have a simlar flucation
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"limesOxygen", /*file_name=*/"tests/data/kt_mine/limpes_oxygenConsumption.tsv", /*fileRead_config=*/fileRead_config__transpose, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! ------ 
    //! 
    //! "Weedlength": cnt_clusters=1, size=~20; seperation='clear: samve dsicrete sinus-vurve wrt. flcutaitons ... Metrics: none of the compared metric manages to idneitfy/describe this relationship
    {/*tag=*/"weedLength", /*file_name=*/"tests/data/kt_mine/weed_length.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //{/*tag=*/"", /*file_name=*/"", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //! -------------------------------------------------- 
    //! Note[]: 
    //! -------------------------------------------------- 
    //! Note[iris]: the results produced by/through MINE results in a considerably better/higher CCM-score (when comapred to Euclid), an observation which is consernsent between/across k-means-pmeruatiosn such as [mean, rank, medoid]. However, a challenge/sekenss wrt. the evlauatin of the IRIS data-set conserns the apriory-interpetaiton/expectaiton (where the researhcers states/assuems tha tehre are three clsuters i IIRS, while currnet cluster-metrics/CCMs indicates that the latter is Not supported), an observaiotn which may imply that (when considering real-lfie dat-asets) we ....??... <--- try to use the altter observiaotn as a jumping-store to ....??... 
    //! Note[iris]: k-means: "rank" VS "avg": while the use of a simlairty-pre-step results in the "avg" and "rank" to 'produce' exaclty the same results, the results (converted to sclar-numbers through CCMs) indicates that the algorithsm 'produce' a 2x-difference (when comptuign sepreately for "rank" and "avg" while Not using/appliying an Eculidcian sim-metric-pre-step). In brief, when comparing/evaluating the dfiferences between k-means with--withouth the 'correlation-pre-step', we clearly observe that the clsuter-algrotihms are stronlgy influence by any 'data-normalizaiton-steps' (illustrated in the latter through our applicaiton/use of Euclid as a simalrity-pre-step appraoch). 
    //! Note[iris]: ..     
    //! Note[iris, cluster="kMeans--[AVG, rank, emdoid]"]: metric="Mine": clusters are in either group(0) OR group(1); metric="Euclid": result simialr to "scikit-learn" (ie, where group(0) in MINE is exaclty simliar, while group(1 =~ 2) (a result which is simlair to the 'defualt answer'); <-- FIXME: try expalinign/arguing for why the MINE-impelmetnation may be correct.
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    {/*tag=*/"iris", /*file_name=*/"data/local_downloaded/iris.data.hpLysis.tsv", /*fileRead_config=*/fileRead_config, /*fileIsAdjecency=*/false, /*k_clusterCount=*/4, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans},
    //{/*tag=*/, /*file_name=*/, /*fileRead_config=*/fileRead_config,/*k_clusterCount=*/, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/, /*metric__insideClust=*/, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_},
    //! Note[iris, cluster="kMeans--medoid"]: metric="Mine": ... ; metric="Euclid": ... 
    //! Note[iris, cluster=""]: metric="Mine": ... ; metric="Euclid": ... 
    //! Note[iris, cluster=""]: metric="Mine": ... ; metric="Euclid": ... 
    //! -------------------------------------------------- 
  };
#else //! then we use an altenrativ edata-set-collecditon:
  //!
  //const e_hpLysis_clusterAlg_t config__clusterAlg = e_hpLysis_clusterAlg_kCluster__rank;
  const e_hpLysis_clusterAlg_t config__clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg_t config__clusterAlg = e_hpLysis_clusterAlg_kCluster__medoid;
  //const e_hpLysis_clusterAlg_t config__clusterAlg = e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG;
  //! Simlairty metric (before clustering):
  const e_kt_categoryOf_correaltionPreStep_t config_dist_metricCorrType = e_kt_categoryOf_correaltionPreStep_none;
  //const e_kt_correlationFunction_t config_dist_metric_id = e_kt_correlationFunction_groupOf_MINE_mic;
  const e_kt_correlationFunction_t config_dist_metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
  //const e_kt_correlationFunction_t config_dist_metric_id__insideClustering = e_kt_correlationFunction_groupOf_MINE_mic;
  const e_kt_correlationFunction_t config_dist_metric_id__insideClustering = e_kt_correlationFunction_groupOf_minkowski_euclid;
  const bool config__clusterConfig__inputMatrix__isAnAdjecencyMatrix__default = false;
  const bool config_corrMetric_prior_use = false;
  //const uint config_kMeans_nclusters = 3;
  const uint config_kMeans_nclusters__defaultAssumption = 2;
  const bool clusterConfig__rowNameIsSortedAndDescribesClusterId = false;
  const uint mapOf_realLife_size = 10;
#define __Mi_c_mapOf_realLife__isStruct 0
  const char *mapOf_realLife[mapOf_realLife_size] = {
    // FIXME[article]: consider using [”elow] to describe/suyggest cases where the 'correct clusteirng' is diffuclt to idneitfy using/through an 'eitehr-or' appraoch <-- cosndier in this cotnext to evlauate different aprpaoches wrt. HCA-based emthods ... ie, as the latter may be able to 'quantify' sigicnace fo different clsuger-meberships'.
    //! -----------------------------------
    //"0x7fff891eb880_1719693181.brief.tsv"
    //"data/FCPS/01FCPSdata/TwoDiamonds.lrn.sub"
    //"data/FCPS/01FCPSdata/TwoDiamonds.lrn"
    "data/FCPS/01FCPSdata/Atom.lrn",
    "data/FCPS/01FCPSdata/EngyTime.lrn",
    "data/FCPS/01FCPSdata/Hepta.lrn",
    "data/FCPS/01FCPSdata/Target.lrn",
    //! ---- 
    //"data/FCPS/01FCPSdata/tmp.TwoDiamonds.lrn",
    "data/FCPS/01FCPSdata/WingNut.lrn",
    "data/FCPS/01FCPSdata/Chainlink.lrn",
    "data/FCPS/01FCPSdata/GolfBall.lrn",
    "data/FCPS/01FCPSdata/Lsun.lrn",
    //! ---- 
    "data/FCPS/01FCPSdata/Tetra.lrn",
    "data/FCPS/01FCPSdata/TwoDiamonds.lrn"
  };
#endif
  FILE *fileDesriptorOut__clusterResults = NULL;    FILE *fileDesriptorOut__clusterResults__deviation = NULL; 
  //! finish: configuration ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! ------------------------------------------------------------------------------------
  //! Start: logics ------------------------------------------------------------------------------------
  //! 
  //! 
  { //! Open the file used for writing of 'named' cluster-emberships':      
    fileDesriptorOut__clusterResults = fopen(nameOf_resultFile__clusterMemberships, "wb");
    if(!fileDesriptorOut__clusterResults) {
      fprintf(stderr, "!!\t Unable to open the result-file=\"%s\": pelase vlaidate correctenss of both path, disk-space and dist-write-permissions. Observaiton at [%s]:%s:%d\n", nameOf_resultFile__clusterMemberships, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up. 
    }
    fileDesriptorOut__clusterResults__deviation = fopen(nameOf_resultFile__clusterMemberships__deviation, "wb");
    if(!fileDesriptorOut__clusterResults__deviation) {
      fprintf(stderr, "!!\t Unable to open the result-file=\"%s\": pelase vlaidate correctenss of both path, disk-space and dist-write-permissions. Observaiton at [%s]:%s:%d\n", nameOf_resultFile__clusterMemberships__deviation, __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up. 
    }
  }
  //! ----------------------------------------------------------------------------
  //!
  { //! Validate that the path(s) are correct:
    if(pathTo_localFolder_storingEachSimMetric != NULL) {
      const int result = access(pathTo_localFolder_storingEachSimMetric, W_OK);
      if (result != 0) {
	fprintf(stderr, "!!\t Unable to write to folder=\"%s\", ie, please vliadate the altter. Observaiton at [%s]:%s:%d\n", pathTo_localFolder_storingEachSimMetric, __FUNCTION__, __FILE__, __LINE__);
	assert(false);
      } else {
	//! 'Empty' the current oflder:      
	{const bool is_ok = removeFiles_inFolder__aux_sysCalls(pathTo_localFolder_storingEachSimMetric); assert(is_ok);}
	//{ char str[2000]; sprintf(str, "rm -f %s*", pathTo_localFolder_storingEachSimMetric);  system(str); }
      }
    }
  }
  //! --------------------------------------------------
  //!
  s_kt_matrix_t matResult__result_x_ccm; setTo_empty__s_kt_matrix_t(&matResult__result_x_ccm);
  s_kt_matrix_t matResult__data_x_simAndAlg__time; setTo_empty__s_kt_matrix_t(&matResult__data_x_simAndAlg__time); //! ie, 'hold' the exeuciton-time, 'buildng' a legend-row for each data-set: simplifes the comaprin of exeuction-time betweend fifernet algoritm-appraoches.
  //{ //! Allcoate space for the matrix:
#if( __Mi_c_mapOf_realLife__isStruct == 1)
    //! 
    //! Identify the matrix-dimensions:
    const s_hp_clusterFileCollection_localCounter_t obj_counter = applyLogics__insideChunk__s_hp_clusterFileCollection_t(NULL, mapOf_realLife, mapOf_realLife_size, e_hp_clusterFileCollection___internalTraverseLogic__updateCounters);
    assert(obj_counter.cnt_data > 0);
    assert(obj_counter.cnt__data_x_cluster_x_metPri__x__metInside > 0);
    assert(obj_counter.cnt__cluster_x_metPri__x__metInside > 0);
    const uint cnt_ccm = (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;     assert(cnt_ccm > 0);
    //!
    //! Allcoate space:
    matResult__result_x_ccm = initAndReturn__s_kt_matrix(obj_counter.cnt__data_x_cluster_x_metPri__x__metInside, cnt_ccm);
    matResult__data_x_simAndAlg__time = initAndReturn__s_kt_matrix(obj_counter.cnt_data, obj_counter.cnt__cluster_x_metPri__x__metInside);    
    assert(matResult__result_x_ccm.nrows > 0);     assert(matResult__data_x_simAndAlg__time.nrows > 0);
    //! 
    //! Specify strings for non-complex cases:
    for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
      set_stringConst__s_kt_matrix(&matResult__result_x_ccm, ccm_type_index, /*string=*/getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), /*addFor_column=*/true);
    }
#endif



    //  }


  uint currentCounter__data_x_cluster_x_metPri__x__metInside = 0;

  //! --------------------------------------------------
  //!
  //! Apply logics for each data-set:
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    uint currentCounter__cluster_x_metPri__x__metInside = 0;
    uint cnt_matrices_exported = 0;
    //!
    //! Load data:
#if( __Mi_c_mapOf_realLife__isStruct == 1)
    const char *stringOf_tagSample = mapOf_realLife[data_id].file_name;
    const s_hp_clusterFileCollection data_obj = mapOf_realLife[data_id];
    const uint config_kMeans_nclusters = data_obj.k_clusterCount;
    assert(config_kMeans_nclusters > 0);
    const bool config__clusterConfig__inputMatrix__isAnAdjecencyMatrix = data_obj.inputData__isAnAdjcencyMatrix;
    if(config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "\n\n# New file: \"%s\"\n", data_obj.file_name);}
    //!
    //! Set the simliarty/correlation matrix: "prior":
    s_hp_clusterFileCollection_simMetricSet_t objMetrics__prior = init__hp_clusterFileCollection_simMetricSet(data_obj.metric__beforeClust, /*includeEmptyCase=*/true);
    assert(objMetrics__prior.list_size > 0);     assert(objMetrics__prior.list != NULL);  //! whe expec thte object 'to hold content':
    //!
    //! Set the simliarty/correlation matrix: "inside-clustering":
    s_hp_clusterFileCollection_simMetricSet_t objMetrics__inside = init__hp_clusterFileCollection_simMetricSet(data_obj.metric__insideClust, /*includeEmptyCase=*/false);
    assert(objMetrics__inside.list_size > 0);     assert(objMetrics__inside.list != NULL);  //! whe expec thte object 'to hold content':


    //!
    //! Add string:
    set_stringConst__s_kt_matrix(&matResult__data_x_simAndAlg__time, data_id, /*string=*/(data_obj.tag) ? data_obj.tag : data_obj.file_name, /*addFor_column=*/false);

    //! 
    //! Specify the clustering-algorithm to use:
    //! Note: we have chosen to 'palce' the clustierng-algorithm 'outsid ethe otehr loops' as the clustering-algorithm is (in reseharc-ltiterature) oftne the mina-focus (ie, in cotnrast to our observaiotns indaiting that the silmairty-emtric is of even higher improtance wrt. the clsuter-reuslts).
    // FIXME[article]:  ... visually evlauate [ªbove] 'assumption' ... using oru web-itnefac e... and then update both [ªbove] aand oru aritlce-text.
    s_hp_clusterFileCollection__clusterAlg_t obj_clusterAlg = init__s_hp_clusterFileCollection__clusterAlg_t(data_obj.clusterAlg, /*includeEmptyCase=*/false); //! wher ehte latter is set to false as we asusme it is of itnerest to 'not only' compute wrt. the simalrity-amtrix.
    assert(obj_clusterAlg.list != NULL);     assert(obj_clusterAlg.list_size > 0); //! ie, as we expect the object to have been set.
    //! 
    //! Iterate throguh the clustering-algorithms:
    for(uint cluster_index = 0; cluster_index < obj_clusterAlg.list_size; cluster_index++) {
      const e_hpLysis_clusterAlg_t config__clusterAlg = obj_clusterAlg.list[cluster_index];
      if(config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "-- clusterAlg: \"%s\"\n", get_stringOf__e_hpLysis_clusterAlg_t(config__clusterAlg));}

      //assert(false); // FIXME: remove.
      //continue; // FIXME: remove.
      const bool config__typeOfOps__onlySimMetric = (config__clusterAlg == e_hpLysis_clusterAlg_undef);
      //!
      //! Handle different cases wrt. simlairty-metrics:
      assert(objMetrics__prior.list_size >= 0);
      for(uint met_prior = 0; met_prior < objMetrics__prior.list_size; met_prior++) {
	//printf("\t\t- config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__);
	//! Set the metric:
	//printf("met_prior=%u <%u, at %s:%d\n", met_prior, objMetrics__prior.list_size,  __FILE__, __LINE__);
	const s_kt_correlationMetric_t corrMetric_prior = objMetrics__prior.list[met_prior]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
	const bool config_corrMetric_prior_use = (corrMetric_prior.metric_id != e_kt_correlationFunction_undef);
	if(config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "\t-- metric-prior: \"%s\":\"%s\"\n",  get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id));}
	for(uint met_prior__inside = 0; met_prior__inside < objMetrics__inside.list_size; met_prior__inside++) {
	  //printf("\t\t-- config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__);
	  //! Spefiy the string-object used to idneity the metic: 	  
	  char str_suff[2000] = {'\0'}; 
	  //const char *str_suff = 
	  getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_id, cnt_matrices_exported++, data_obj, cluster_index, met_prior, met_prior__inside, str_suff);
	  //char *str_suff = getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_obj, config__clusterAlg, met_prior, met_prior__inside);
	  //char str_suff[2000] = {'\0'}; sprintf(str_suff, "%s.a_%u.c_%u_%u", data_obj.tag, cluster_index, met_prior, met_prior__inside);
	  //! Set the metric:
	  const s_kt_correlationMetric_t corrMetric_inside = objMetrics__inside.list[met_prior__inside]; //init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
	  //s_kt_correlationMetric_t corrMetric_inside; init__s_kt_correlationMetric_t(&corrMetric_inside, config_dist_metric_id__insideClustering, config_dist_metricCorrType);
	  if(config__isToPrintOut__iterativeStatusMessage) {fprintf(stdout, "\t\t-- metric-inside: \"%s\":\"%s\"\n",  get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_inside.typeOf_correlationPreStep), get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id));}
	  //!
	  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
#else
	  const char *stringOf_tagSample = mapOf_realLife[data_id];
	  const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name.
	  if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name
	  const uint config_kMeans_nclusters = config_kMeans_nclusters__defaultAssumption;
	  assert(config_kMeans_nclusters > 0);
	  const bool config__clusterConfig__inputMatrix__isAnAdjecencyMatrix = config__clusterConfig__inputMatrix__isAnAdjecencyMatrix__default;
	  // const bool config__typeOfOps__onlySimMetric = false;
	  //!
	  //! Set the simliarty/correlation matrix:
	  s_kt_correlationMetric_t corrMetric_prior; init__s_kt_correlationMetric_t(&corrMetric_prior, config_dist_metric_id, config_dist_metricCorrType);
	  s_kt_correlationMetric_t corrMetric_inside; init__s_kt_correlationMetric_t(&corrMetric_inside, config_dist_metric_id__insideClustering, config_dist_metricCorrType);
#endif
	  s_kt_matrix_t obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
	  if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) {
	    fprintf(stderr, "!!\t File=\"%s\" resulsts in emtpy matrix: pelase validate your input-file. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
	    continue;
	  }
	  if( (config__typeOfOps__onlySimMetric == false) && (obj_matrixInput.nrows < config_kMeans_nclusters) ) {
	    fprintf(stderr, "!!(odd-usage)\t For File=\"%s\" seems like you have requested an evlauation where nrows(%u) !> cnt_clusters(%u). If this is what you intended, then please ingore this observaiton. Oderwhise we suggesting invesitgiating the latter. OBservation at [%s]:%s:%d\n", stringOf_tagSample, obj_matrixInput.nrows, config_kMeans_nclusters, __FUNCTION__, __FILE__, __LINE__);
	    fprintf(stderr, "!!\t File=\"%s\" resulsts in emtpy matrix: pelase validate your input-file. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
	    continue;
	  }

	  assert(obj_matrixInput.matrix); assert(obj_matrixInput.matrix[0]);
	  //printf("val='%f', at %s:%d\n", obj_matrixInput.matrix[0][0], __FILE__, __LINE__);
	  //assert(obj_matrixInput.matrix[0][0] == 0); // FIXME: remvoe
	  //!
	  //! Configure:
	  s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
	  hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = config__clusterConfig__inputMatrix__isAnAdjecencyMatrix;
	  hp_config.config.corrMetric_prior_use = config_corrMetric_prior_use;
	  if(pathTo_localFolder_storingEachSimMetric) {
	    //! then we export the result-matrix:
	    char str_local[2000] = {'\0'}; sprintf(str_local, "%s%s.index_%u_%s.tsv", pathTo_localFolder_storingEachSimMetric, "simMatrix", data_id, str_suff);
	    hp_config.stringOfResultPrefix__exportCorr__prior = str_local;
	  }
	  s_kt_matrix_t *matrix_cmp = NULL;     if(config_corrMetric_prior_use == false) {matrix_cmp = &obj_matrixInput;} //! which is then used when evlauating the cluster-emmberships.
	  hp_config.config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:    
	  hp_config.config.corrMetric_insideClustering = corrMetric_inside;

	  // printf("at %s:%d\n",  __FILE__, __LINE__); continue; // FIXME: remove.

	  //!
	  //! Start the timer: 
	  start_time_measurement(); //! defined in our "measure_base.h"
	  //!
	  //! Call our hpLysis:	  
	  if(config__typeOfOps__onlySimMetric) { //! then we do Not apply clustering:
	    //!
	    //! Correlate: 
	    const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
	    assert(is_ok);
	    //!
	    //! Finalise the timing-oepraiton: 
	    const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	    // printf("simOnly[%u < %u==%u], at %s:%d\n", currentCounter__cluster_x_metPri__x__metInside, matResult__data_x_simAndAlg__time.ncols, obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__);
	    assert(currentCounter__cluster_x_metPri__x__metInside < matResult__data_x_simAndAlg__time.ncols);
	    matResult__data_x_simAndAlg__time.matrix[data_id][currentCounter__cluster_x_metPri__x__metInside] = exex_time; currentCounter__cluster_x_metPri__x__metInside++;
	    //!
	    //! Investgiate if the string is not set, and then update:
	    const char *str_curr = getAndReturn_string__s_kt_matrix(&matResult__data_x_simAndAlg__time, currentCounter__cluster_x_metPri__x__metInside, /*addFor_column=*/true);
	    if(!str_curr || !strlen(str_curr)) { 
	      char str__insert[2000] = {'\0'};
	      getStringOfMAppingToFile__s_hp_clusterFileCollection_t(data_id, cnt_matrices_exported++, data_obj, cluster_index, met_prior, met_prior__inside, str__insert);
	      set_string__s_kt_matrix(&matResult__data_x_simAndAlg__time, currentCounter__cluster_x_metPri__x__metInside, /*stringTo_add=*/str__insert, /*addFor_column=*/true);
	    }
#endif
	    //! 
	    //! -------------------------------------------------
	  } else { //! then clsuteirng is applied:
	    const bool is_ok = cluster__hpLysis_api(&hp_config, config__clusterAlg, &obj_matrixInput, config_kMeans_nclusters, config_arg_npass);
	    assert(is_ok);
	    //!
	    //! Finalise the timing-oepraiton: 
	    const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	    //printf("[%u < %u==%u], at %s:%d\n", currentCounter__cluster_x_metPri__x__metInside, matResult__data_x_simAndAlg__time.ncols, obj_counter.cnt__cluster_x_metPri__x__metInside, __FILE__, __LINE__);
	    assert(currentCounter__cluster_x_metPri__x__metInside < matResult__data_x_simAndAlg__time.ncols);
	    matResult__data_x_simAndAlg__time.matrix[data_id][currentCounter__cluster_x_metPri__x__metInside] = exex_time; currentCounter__cluster_x_metPri__x__metInside++;
#endif

	    //! 
	    //! -------------------------------------------------
	    { //! For the 'summary' of STD-variations
	      fprintf(fileDesriptorOut__clusterResults, "\n//! Export cluster-results for data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	      { //! Export: traverse teh generated result-matrix-object and write out the results:    
		const uint *vertex_clusterId = hp_config.obj_result_kMean.vertex_clusterId;
		assert(vertex_clusterId);
		const uint cnt_vertex = hp_config.obj_result_kMean.cnt_vertex;
		uint *ranks = get_rank__uint__correlation_rank(cnt_vertex, vertex_clusterId); //! found in ourr "correlation_rank.h"
		assert(ranks); //! ie, as we exepct at elast one cluster-id to have beens et
		assert(cnt_vertex > 0);
		fprintf(fileDesriptorOut__clusterResults, "//! clusterMemberships=[");
		uint max_cnt = 0;
		if(false) {
		  // fixme: figure out why [”elow] produce odd results.
		  for(uint i = 0; i < cnt_vertex; i++) { //! then we write out using the cluster-order:
		    const uint vertex_id = ranks[i]; 
		    assert(vertex_id < cnt_vertex);
		    fprintf(fileDesriptorOut__clusterResults, "%u->%u, ", vertex_id, vertex_clusterId[vertex_id]); max_cnt = macro_max(vertex_clusterId[vertex_id], max_cnt);
		  }
		} else {
		  for(uint i = 0; i < cnt_vertex; i++) {fprintf(fileDesriptorOut__clusterResults, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
		}
		if(ranks) {free_1d_list_uint(&ranks);}
		fprintf(fileDesriptorOut__clusterResults, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);

#if( __Mi_c_mapOf_realLife__isStruct == 1)
		//!
		//! Apply different matrix-based CCMs:
		assert(currentCounter__data_x_cluster_x_metPri__x__metInside < matResult__result_x_ccm.nrows);
		assert(e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef == matResult__result_x_ccm.ncols);
		//! Set the string:
		set_stringConst__s_kt_matrix(&matResult__result_x_ccm, currentCounter__data_x_cluster_x_metPri__x__metInside, /*string=*/str_suff, /*addFor_column=*/false);
#endif
		//!
		//! Iterate: 
		for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
		  const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
		  const t_float score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&hp_config, ccm_type, /*mapOf_clusterIds=*/NULL, matrix_cmp);
		  //!
		  //! Write out the score:
		  /* const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
		  /* if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name */	
		  if(score != T_FLOAT_MAX) {
		    fprintf(stdout, "#!\t\t ccm[\"%s\"]=%f \t %s\t data=\"%s\", sim-metric=\"%s\" and dist-enum-prestep='%u':\n", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), score, str_suff, stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
		  }
#if( __Mi_c_mapOf_realLife__isStruct == 1)
		  //!
		  //! Update the result-object: 
		  matResult__result_x_ccm.matrix[currentCounter__data_x_cluster_x_metPri__x__metInside][ccm_type_index] = score;
#endif
		}		
#if( __Mi_c_mapOf_realLife__isStruct == 1)
		//! Update counter:
		currentCounter__data_x_cluster_x_metPri__x__metInside++;
#endif
	      }	  
	      // export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults__deviation, e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS, matrix_cmp);
	    }
	  }
	  //!
	  //! Update the result-file:
	  { //! For the 'complete' simlairty-result-matrix:
	    fprintf(fileDesriptorOut__clusterResults, "\n//! Export data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	    export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults, config_exportFormat, &obj_matrixInput);
	  }
	  { //! For the 'summary' of STD-variations
	    fprintf(fileDesriptorOut__clusterResults, "\n//! Export data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	    export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults__deviation, e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_TSV, matrix_cmp);
	  }
	  if(
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	     (data_obj.alt_clusterSpec == e_hp_clusterFileCollection__goldClustersDefinedBy_rowString) || 
	     (data_obj.mapOf_vertexClusterId_size > 0)
#else
clusterConfig__rowNameIsSortedAndDescribesClusterId
#endif
) { //! then we evlauate the cosnsitency usign different CCM-metrics:
	    assert(obj_matrixInput.nameOf_rows != NULL);
	    const uint empty_0 = 0;
	    uint *mapOf_clusterIds = allocate_1d_list_uint(obj_matrixInput.nrows, empty_0);
	    uint cluster_pos = 0;
	  if(
#if( __Mi_c_mapOf_realLife__isStruct == 1)
(data_obj.alt_clusterSpec == e_hp_clusterFileCollection__goldClustersDefinedBy_rowString) 
#else
true
#endif
	     ) {

	    const char *str_curr = obj_matrixInput.nameOf_rows[0];
	    if(strstr(str_curr, "_BR_")) { //! then we asusem a pre-define d'order' is fiound in the clsuter-spec:
	      for(uint i = 0; i < obj_matrixInput.nrows; i++) {
		const char *str_curr = obj_matrixInput.nameOf_rows[i];
		assert(str_curr); assert(strlen(str_curr));
		const char *suff = strstr(str_curr, "_BR_");
		assert(suff);  assert(strlen(suff));
		suff += strlen("_BR_");
		assert(suff);  assert(strlen(suff));
		//! 
		//! Se thte clsuter-idnex:
		const int cluster_pos = atoi(suff);
		mapOf_clusterIds[i] = (uint)cluster_pos;		
	      }
	    } else 
	      //if(false)  //! which 'may' be set if we are intestedied in investgiating the case where 'all vertices are in the same clsuter'.
	      {
		mapOf_clusterIds[0] = cluster_pos;
		for(uint i = 1; i < obj_matrixInput.nrows; i++) {
		  const char *str_prev = obj_matrixInput.nameOf_rows[i-1];
		  const char *str_curr = obj_matrixInput.nameOf_rows[i];
		  assert(str_prev);	assert(str_curr);
		  assert(strlen(str_prev));	assert(strlen(str_curr));
		  //! Compare:
		  if( (strlen(str_prev) != strlen(str_curr)) || (0 != strcmp(str_prev, str_curr)) ) {
		    cluster_pos++;
		  }
		  //! Update:
		  //printf("vertex[%u] w/clusterId=%u, at %s:%d\n", i, cluster_pos, __FILE__, __LINE__);
		  mapOf_clusterIds[i] = cluster_pos;
		}
	      }
	  } else {
#if( __Mi_c_mapOf_realLife__isStruct == 1)
	    for(uint i = 0; i < data_obj.mapOf_vertexClusterId_size; i++) {
	      mapOf_clusterIds[i] = data_obj.mapOf_vertexClusterId[i];
	    }
#else
	    assert(false); //! ie, as we then need adding support f'ro this'.
#endif
	  }
	    //!
	    //! Apply different matrix-based CCMs:
	    for(uint ccm_type_index = 0; ccm_type_index < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_type_index++) {
	      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_type = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_type_index;
	      const t_float score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&hp_config, ccm_type, mapOf_clusterIds, matrix_cmp);
	      //!
	      //! Write out the score:
	      /* const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name. */
	      /* if(str_suff) {str_suff++;} //! ie, to avodi the '/' to be part of the file-name */	
	      if(score != T_FLOAT_MAX) {
		fprintf(stdout, "#!\t\t ccm[\"%s\"]=%f \t data=\"%s\", sim-metric=\"%s\" and dist-enum-prestep='%u':\n", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_type), score, stringOf_tagSample, get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_inside.metric_id), corrMetric_inside.typeOf_correlationPreStep);
	      }
	      //!
	      //! Update the result-object: 
	    }
      
	    //!
	    //! De-allcoate lcoally reserved memory:
	    free_1d_list_uint(&mapOf_clusterIds);
	  }

	  //!
	  //! De-allcoate lcoally reserved memory:
	  free__s_hpLysis_api_t(&hp_config);
	  free__s_kt_matrix(&obj_matrixInput);
	}
#if( __Mi_c_mapOf_realLife__isStruct == 1)
      }
      /* printf("- de-allcoates: objMetrics__prior, at %s:%d\n", __FILE__, __LINE__); */
      /* printf("config__clusterAlg=%u, at %s:%d\n", config__clusterAlg, __FILE__, __LINE__); */
    }
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&objMetrics__prior);
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&objMetrics__inside);
    //printf("- de-allcoates: objMetrics__inside, at %s:%d\n", __FILE__, __LINE__);
    //! De-allocate:
    free__s_hp_clusterFileCollection_simMetricSet_t(&obj_clusterAlg);
#endif
  }


  if(fileDesriptorOut__clusterResults) {
#if( __Mi_c_mapOf_realLife__isStruct == 1)    
    //!
    //! Write out a mapping-file descriibng the 'properties' of [ªbov€] data-set:
    writeOut__mappingFile__s_hp_clusterFileCollection_t(fileDesriptorOut__clusterResults, mapOf_realLife, mapOf_realLife_size);
#endif
  }

#if( __Mi_c_mapOf_realLife__isStruct == 1)
  {
    assert(matResult__result_x_ccm.nrows > 0);     assert(matResult__data_x_simAndAlg__time.nrows > 0);
    const char *input_file = nameOf_resultFile__clusterMemberships;
    { //! Then we erite out the result-files: for CCM "matResult__result_x_ccm"
      { //! JS: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.js", input_file, "dataSiAlg__ccm"); 
	export__singleCall__toFormat_javaScript__s_kt_matrix_t(&matResult__result_x_ccm, fileName_local, NULL, "dataSiAlg__ccm");
      }
      { //! TSV: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.tsv", input_file, "dataSiAlg__ccm"); 
	export__singleCall__s_kt_matrix_t(&matResult__result_x_ccm, fileName_local, NULL);
      }      
    }
    { //! Then we erite out the result-files: for "exeuction-time"
      { //! JS: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.js", input_file, "data_x_simAndAlg"); 
	export__singleCall__toFormat_javaScript__s_kt_matrix_t(&matResult__data_x_simAndAlg__time, fileName_local, NULL, "data_x_simAndAlg");
      }
      { //! TSV: 
	char fileName_local[2000] = {'\0'}; sprintf(fileName_local, "%s__%s_collection.tsv", input_file, "data_x_simAndAlg"); 
	export__singleCall__s_kt_matrix_t(&matResult__data_x_simAndAlg__time, fileName_local, NULL);
      }
    }
  }
#endif


  //!
  //! De-allcoate and clsoe file-desicprtors:
  assert(fileDesriptorOut__clusterResults);
  fclose(fileDesriptorOut__clusterResults);
  assert(fileDesriptorOut__clusterResults__deviation);
  fclose(fileDesriptorOut__clusterResults__deviation);

  // ---------------------------------------------------------------------------------------------------
    assert(false); // FIXME: seems like only the first clsuter-alg is evlauated ... trye figureing out why ... resovling tis issue ... updated the locaiton-plascement of the de-allcoation-calls ... a newg-fault now occures ... investgiates ... 

  assert(false); // FIXME[code]: 'in our defualt test-setting' use ofr Euclid-sim-metric an option to ewaluate the time-cost of a niave distnace-metric-implementaiotn .... which would imply using the cluster-c logics <-- instead update our 'config s_hp_clusterFileCollection object' wrt. the latter.

  // ---------------------------------------------------------------------------------------------------
  assert(false); // ok:FIXME: coneptually idneitfy challens to be resovled wrt. the current aritlce s... ie, describe and resovled any poential graveyard-cases.
  // ---------------------------------------------------------------------------------------------------

  assert(false); // FIXME: re-compute our results using a small data-set .... and vlaidate that none of the cases/errors ... eg, wrt 'compleness of string-dientifes in matrices ... 
  assert(false); // FIXME[code]: in the configuraiton add a for-loop hwere we 'copy' the 'default cofnigruations' ... and thereafter add/integrate different ranomdizaiotn-thresholds ... uisgn a for-loop.
  assert(false); // move our the 'core-conrtent' from our "tut_inputFile.c" into a to-be-added "hp_clusterFileCollection.c"
  // ---------------------------------------------------------------------------------------------------
  assert(false); // FIXME[code::bonus]:write a disjotin-forest-implemetnaiton .... iteratinig through random values/scores.
  assert(false); // FIXME: evlauate exec using valgridn 'locally' <-- write a sepreate tut-example for this
  // ---------------------------------------------------------------------------------------------------
  assert(false); // FIXME: re-compute our results using a small data-set .... and vlaidate that none of the cases/errors ... eg, wrt 'compleness of string-dientifes in matrices ... 
  assert(false); // FIXME: thereafter visualize the result .... evlauating the correctness/describiness of the CCMs
  // ---------------------------------------------------------------------------------------------------
  assert(false); // FIXME: re-start our euxciton of .... 'small-sytnetic and small-real-life' data-set-evaluation <-- write a sepreate tut-example for this
  assert(false); // FIXME: re-start our euxciton of .... 'large-sytnetic' data-set-evaluation <-- ensure that we have added (in our evlaution) differetn 'pre-consturcted' set sof random-values ... usign the æconcatena-tematrx-ootpion'. <-- write a sepreate tut-example for this
  assert(false); // FIXME: re-start our euxciton of .... vincentarelbundock---data-sets <--- where we 'also for these' apped with 'random noise' in our "hp_clusterFileCollection.c" file <-- write a sepreate tut-example for this
  assert(false); // FIXME: re-start our euxciton of .... vilje-data-set <--- where we 'also for these' apped with 'random noise' in our "hp_clusterFileCollection.c" file <-- write a sepreate tut-example for this

  // ---------------------------------------------------------------------------------------------------

  assert(false); // FIXME: update our [ªbove] "s_hp_clusterFileCollection" ... managge to 'get our update through ' ... ie, 'try runnign this' .... manully isnpect reuslts ... fxi errorrs .... perform a manula inspeciton of the generated simlamirty-matrix (and where we use the result of the latter to udpate our k-means-expectiaotn-count) 
  assert(false); // FIXME: use our ehatmap-generator to visually inspect the clsuter-results of the 'odl' data-set-colleciton ... and thereafter update our "init_radial.js" wrt. the data-set ... investigating/testing constiency wrt. CCMs

  assert(false); // FIXME: get our 'new-added data-set to compile' ... ie, incldue the altter 'directly' as a 'macor-spec-option in "tut_inputFile.c"'.

  //! -------------------------------------------------------------- 



  assert(false); // FIXME[concpetual::read::skim] "kmeans++" ("http://ilpubs.stanford.edu:8090/778/1/2006-13.pdf")
  assert(false); // FIXME[concpetual::read::skim] 'seleciton of k in k-means' ["http://www.ee.columbia.edu/~dpwe/papers/PhamDN05-kmeans.pdf"] ... cosnidering using "https://datasciencelab.wordpress.com/2014/01/21/selection-of-k-in-k-means-clustering-reloaded/" as an isnpraiton wrt. idneitficioant of 'correct k-in-clsuters'.

  assert(false); // FIXME[conceptual]: describe how to 'enable' the use of "kMEans++" in combaitnion with our otehr alteritms' ... eg, updating our "math_generateDistribution.c" .... and adding new enums to our "hpLysis_api"
  assert(false); // FIXME[code]: write an implemtnation of "/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/external_kMeans_+/baylorml-master/fast_kmeans/elkan_kmeans.cpp" ... <-- first implement "kemans++" by using the  "general_functions.h"  as a template.
  assert(false); // FIXME[code]: write impelmetnation--verisons of the code in "https://github.com/BaylorCS/baylorml/tree/master/fast_kmeans", eg, wrt. "Elkans" ... and comapre the authros library iwth our appraoch ... dowdnloaded partially at "external_kMeans_+"
  assert(false); // FIXME[concpetual::read] read Elkans aritlce "http://www.aaai.org/Papers/ICML/2003/ICML03-022.pdf"
  assert(false); // FIXME[article]: write a detailed descipriton of the algorithms used in the Sci-Py-code from "k_means_.py" ... goal is to relax while Not programming ... <-- the "sci-py-code' is a contactantion of external libraries .... only the "kmeans++' algorithm is of noivle interest/improtance.
  assert(false); // FIXME: update our "k-means-immeplemtantion" ... 'take' a cofnigruation-object as input ... where for for the latter incldue/inroporate the random-spec-object "s_kt_randomGenerator_vector_t" deifne doin our "math_generateDistribution.h" 
  /** FIXME: update our aritce wrt. [”elow]d eescipriton of algorithms:
      ---------------------------------------------------------------
      * "k-means++": an alterantive/defined strategy for idnetificiaotn/seleciton of intital cluster-centers (in/for k-means clsutering). Goal is to reduce the time to idneitfy algorithm-convergence: the latter is n contrast to the 'defualt' k-emans algorithm (where ranom-intalis<ions are used). 
      * "k-means--elkan": ... 
      * "MiniBatchKMeans":       
      * "":       
      ---------------------------------------------------------------
   **/
  assert(false); // FIXME[scipy--scikit::docu]: ... describe how the authros 'apply' a parallel evlauation <--- seems like the 'algorithm is called seprately for differnet chunks' ... and then the 'best-result' is selected ... which impleis that 'the best-case-exec-time' will result in 'extremly hiogh' exec-time-cost' (ie, where parallisaiton will not imrpove ehte exec-time as the k-emans-clsuter-alg will first stop afte rhteir completion ... eg, for the case with a 'random data-set without any clear/well-defined clusters)<-- fixme[aritcle]: udpate our docuemtantion with this (eg, after we have performed a eprforamcne-elvaution)
  assert(false); // FIXME[scipy--scikit::docu]: ... elvuaate tha following asseriton: "The worst case complexity is given by O(n^(k+2/p)) with n = n_samples, p = n_features. (D. Arthur and S. Vassilvitskii,  'How slow is the k-means method?' SoCG2006)".
  assert(false); // FIXME[code]: write a new function to supprot both 'binary search' and 'iterative searhc' in sorted lists ... both for an input of "search=[]" and "search=valueToSearch". .... where we use our curretn tempalte-impelmetantiosn ... and update our ....??... wrt. the latter. 
  assert(false); // FIXME[code]: implement the k-means-++ algorithm ... using "_k_init(...)" in "k_means_.py" as a tempalte .... and 'berify' using "https://en.wikipedia.org/wiki/K-means%2B%2B"
  assert(false); // FIXME[algorithm::concpetual]: describe an approach to merge HCA-results using a 'breath-first' appraoch, eg, always selecting the 'min-sum-node-to-merge' ... and then use 'this' as a strategy to merge different distjoint-forest-results ... where we comptue the weight of each cluster using $[\sum\sum d(x \in c_i, c(c(i)))$, $\sum\sum d(x \in c_i, y \in c_i), ...]$ <-- try to first evlauate/suggest different ccm-based appraoches 'for this'
  assert(false); // FIXME[concpetual--ccm]: describe a new ccm which use ... 'total-error divided by total-sum-of-scores' ... seperately for [avg, rank, medoid] (ie, simlar to the k-emasn-appraoches) ... eg, $\sum_{C_i \inC}\sum_{x \in c_i}d(x, c(c_i))$, ie, the "sum of distances to the centroid", where the 'sum of scores' (alt. "global impact of clusters") may be computed using 'appraoches' such as "[avg, median, medoid, ...]x[Euclid, Cityblock, ...]"
  assert(false); // FIXME[concpetual--ccm]: describe a new ccm which use ... 
  assert(false); // FIXME[concpetual--new-alg]: deescribe the k-emans-algorithmby/of ... Mount et al. ... Har Peled et al. ... Kumar et al. ... km-Hybrid 
  assert(false); // FIXME[scipy--scikit::perforamnce]: write a python-script to evlaute the exec-time-cost of differetn sim-metrics used in k-means
  assert(false); // FIXME[scipy--scikit::perforamnce]: evlauyate the 'parallel performance' of the latter ... setting the "n_jobs" 'parallel-config input-arpameter' 
  //! -------------------------------------------------------------- 


  assert(false); // FIXME[conceptual]: try to describe an appraoch for how we may relate the CCMS to the clsuter-results .... ieg, wrt. implciaiton low socres and high scores for each CCM <-- consider use outJC-visu-appraoch  after we have completed our curretnøngoing evlauation .
  assert(false); // FIXME["data/vincentarelbundock-Rdatasets-7133d7c/"::conceptual]: ... idneityf thge subset of data which we are to use ... describinb/evlautign each data-set ... itnrospecting upon proerpteis which may be extracted/described .... and how an 'autoamted analyses' may be constructed based on the latter.
  assert(false); // FIXME["data/vincentarelbundock-Rdatasets-7133d7c/"]: ... update our Perl "convert.pl" script ... get a sampel-dat-aset 'torugh' ... anc comapre results wrt. Euclid VS MINE
  assert(false); // FIXME: add support of differtent nosie-paremeters wrt. the 'sytentic data-cases' ... using different cofnigratiion-tile-tuning-objects.
  assert(false); // FIXME[tut_inputFile::data]: ... using "../janchris/vilje-matriser/displaymatrices.c" as a template ... write a new tut-example .... include JCs vilje-matrix into the fiel-colelciton-set ... using a new && 'specific' macro-alternative ... compute for the latter using all sim-metrcis ... upwate a WWW-accessible web-page ... give JC an heads-up.
  assert(false); // FIXME[alternative-libraries]: .... compare with different appraoches described in "https://en.wikipedia.org/wiki/K-means_clustering" ... and scaffold/suggest/hyptoese known wekaneeses wrt. k-means-clsutering.
    /**
       ok: // FIXME: write/update our perl-wrapper where the first column 'represents' the row-id-string, eg, ("9") in: "10",NA,194,8.6,69,5,10 .... and simliarty replace the "NA" with '-': copmpleted!
       ok:// FIXME: get a sample-dat-aset through ... udpating our "./data/local_downloaded/convert.pl" : completed!       
       ok:// FIXME[./data/local_downloaded/convert.pl]: .... update our "s_hp_clusterFileCollection_t" with a paremter 'stating' if first-cokumn 'hold' the cluster-id ... update our current config-objects ... get compling : completed!
       ok:// FIXME[./data/local_downloaded/convert.pl]: .... update the/our Perl-script to generate the "s_hp_clusterFileCollection_t" objects 'which we expect as input'.: completed!

       // FIXME: update out tut-file to generate a unifed file holdign the inptu-data-sets defined in "data/local_downloaded/metaObj.c" ... and then visually evlauate the latter suign a JS-lineplot-appraoch .... use the latter to arguing for why our MINE-based appraoch sigicnatly outperforms estalibshed appraoches wrt. an unsupervised-clsutering-appraoch.             
       // FIXME: in [”elow] 'when we have build a perl-parser for these' ... and updateed oiur repo ... 'set cluster-count'=0 ... ens rethat this results in only sim-amtrix being gneerated ... use MINe to visaully hyptoese clsuters ... then re-run using difnferent clsuter-algorithms ... 
     **/
  assert(false); // FIXME[tut_inputFile]: ...
  assert(false); // FIXME[tut_inputFile]: ... 
  assert(false); // FIXME[]: ... 




  assert(false); // FIXME[clustering]: ... use k-means ... compare clustering-results of MINE VS Euclid ... both wrt. ['use-before' 'not-use-any-before']x[MINE]x[Euclid] ... scaffold/buidl a tebale ... to bue sued as latex-input wrt. cluster-data-evlauation <-- seems like an isngiciant/unsseable/CCM-not-detactable difference (where Silhoutte teturns alisgly different score,s through were the difference may be expalined by different random-investgiaotn-points) .... 
  assert(false); // FIXME[conceptual]: idnetify a handful of data-sets ... write a conpeutal scaffold wrt. expectsiations.
  assert(false); // FIXME[conspetual]: evaluate/descrfibe the 'trend' assicated to CCMs hwne comparing k-means-MINE and k-measn-Euclid (ie, wrt. result-accuracy) ... therafter update our web-inteface witht he generated matrix ... and update our aritlce with sreen-shots of the altter ccm-evlauation. 
  assert(false); // FIXME[hpLysis]: write/include a CCM-based appraoch to 'get a measure of density' when comparedx ot a gold-standard named "scalar_CCMclusterSimilarity__s_hpLysis_api_t(..)"... ie, for a 'speicfic' data-set .... updating our hpLysis wrt. the latter ... and then comapre Euclid VS MINE
  assert(false); // FIXME: update our "g_tree_formatConversion.js" to 'take' a non-quadratic input-amtrix as input ... therafter vlaidate altter updaitng our "init_radial.js" ... sovle error wrt. 'non-vivile' legned-name (for large dat-ainputs) (fixed: udpated our "g_tree_legend.js"§) .... and then inspect/describe teh 'inptu-riris-data-set' VS our clsutering-results
  assert(false); // FIXME: wrt. the "data/FCPS/01FCPSdata/" ... visualize the inptu-matirx using our line-plot ... to try to hypotehse possible 'regions of simalrity' .... and consider udpating our ariltce-text witht eh latter.  <--- seems like our appraoch is 'unalbe to dientify patterns' <-- try to 'get' the IRIS-data-set through ... update our "export_config_extensive_s_kt_matrix_t(..)" to export the names: update our "measure_kt_matrix_cmpCluster__stub__loadSampleData.c" to correctly inlcude/parse the 'parsed' names from  ... 
  assert(false); // FIXME: evlauate the results wrt. the k-means-results .... and comapre 'pre-euclid' to 'none' and "MINE" ... 

  assert(false); // FIXME[alternative-libraries]: .... "http://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_iris.html" .... and "http://stamfordresearch.com/k-means-clustering-in-python/" .... which examplfies the IRIS-data-set ... where the authros seems to correclty 'manageæ to dientify the IRIS-clsuters. 
  assert(false); // FIXME[alternative-libraries]: .... 



  assert(false); // FIXME: try to gernalise lgocis in our "compute__VRC__s_kt_matrix_cmpCluster_clusterDistance_t(..)" .... ie, to 'mvoe this' to 'both an inptu-funciton' asn 'to make this accesisble for our other CCM-amtrix-based-functions'.
  assert(false); // FIXME[kt_matrix_cmpCluster.h]: ... implement (and udpate our aritlce-tempalte) for a new CCM-metric named .... PBM <-- what are the matrix 'to use'?


  assert(false); // FIXME[time=1.days]: write an impmentaiton of our 'disjoint-seperaiton' ... and validate that he result 'reflects oru asusmpitons' <-- preCondition: idneify a descriptive/accurayc CCM 'to be used as a reference'.
  assert(false); // FIXME[time=1.days]:  get a curretn 'version' of our HCA to wokr ... using the c_clsuter-naive-clsute-rsub-divison-appraoch .... and then evalaute/comapre the latter yo our disjoitn-algorithm.
  assert(false); // FIXME[time=2.days]: implmenet the Sci-Py-code from "k_means_.py", and compare the results <-- among others 'expierment' witht he time-cost of copmtuing Euclid VS the 'otpimum case using our 2d-appraoch' ... where we test for "cityblock" (as the numpy NP library is used 'through'/by usignt he "safe_sparse_dot" funciton to copmtue "Euclid").
  assert(false); // FIXME: write and get a 'basic' version of "tut_inputFile.c" 'through' ... write out a seperate file 'holidng' the sumamrty-attributes for the dfifernet rows in the reuslt-silmarity-file ... thereafter copmtue MINE for the different 'cases' in "data/FCPS/01FCPSdata/" ... validate the results.

  assert(false); // FIXME[automate::after-our-algorithm-approach-works]: 'adjsut' our inptu-fileName-strucutre to alsso cover/incldue the naems of the 'gold-standard-cases' .... and 'adjsut the latter memberships' with '-1' (to handle the cluster-enumeration stating at '1' instead of '0'). 

  assert(false); // FIXME[concept+code]: write permtaution of "kt_matrix__disjointClusterSeperation(..)" .... boefre starting with [”elow]: ... add supprot for our 'new disjtoin-rankb-ased algorithm' .... and write a new tut-eampel illsutrating/descriibng the latter .... where convergence-critieras as .....??.... 

  assert(false);     // FIXME[article]: update our aritlce with all "fixme[article]" 'in/from this coducment'



  assert(false); // FIXME[finishing-tuchc::coentpual]: cluster our result-data-matrices ... ie, describe why and 'how' such may be useful
  assert(false); // FIXME[finishing-tuchc]: update our "hp_clusterFileCollection.h" with 'call logics' to code-logics ... and then write a new example-stub 'examplifying the suage of the new hp_clusterFileCollection funciton'.
  assert(false); // FIXME[finishing]: try to dientify an example which may be used to illsutrate the "minibatch-aglrothm' .... and then write a enw "tut" file named "tut_clust_miniBatch.c".

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------


  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
