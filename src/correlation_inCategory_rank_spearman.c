#include "correlation_inCategory_rank_spearman.h"
#include "correlation_rank_rowPair.h"
#include "correlationType_spearman.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @brief compute all-against-all for spearman.
   @remarks in this procedure we have updated the spearman-algorithm with the following steps:
 # Step(1): first allocate tempraory matrices, and then seperately compute two sorted matrices.
 # Step(2): use a matrix-iteration to comptue the 'mulitplicaiton' of the sums:
 # Step(3): udse teh 'correlation-approach' to 'adjsut' the sum.
**/
void ktCorr_compute_allAgainstAll_distanceMetric_spearman(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input,  char** mask1, char** mask2, const t_float weight_[], t_float **resultMatrix, const e_typeOf_measure_spearman_masksAreAligned_t masksAre_aligned, bool transpose,  s_allAgainstAll_config_t *config_allAgainstAll) {
  assert(config_allAgainstAll->CLS > 0); //! ie, what we expect
  // FIXME: udpate our benchmark-tests wrt. this fucntion ... adn test specirifally the new-introduced "masksAre_aligned" 'heuristcs' ... which if set require at least an "n*n*n" increased time-cost.

  //printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

  const t_float *weight = NULL; //! ie, as the weight-order will nto reflect the sorted values.

  bool needTo_useMask_evaluation = (config_allAgainstAll->masksAre_used == e_cmp_masksAre_used_false); //! ie, investigate if the caller has declared that 'all valeus are of itnerest', eg, for the case where the "euclidc distance metric" is used in the k-means procedure, ie, where a acell-dsitance=0 is 'enough' to cpature/descirbe a non-set cell (ie, as the latter value will then be ingored form the comptaution).
  if(config_allAgainstAll->masksAre_used == e_cmp_masksAre_used_undef) {
    assert(data1_input); assert(data2_input);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncols, data1_input, data2_input, mask1, mask2, transpose, config_allAgainstAll->iterationIndex_2);
    config_allAgainstAll->masksAre_used = (needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  } 
  //! Test if we may use our otopmized strategy for copmuting "spearman":
  bool mayUse_optimized_computaiton = true;
  if( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none) ) {
    mayUse_optimized_computaiton = false;
  } else { //! then we 'investigate' proeprties assicated ot the matrix:
    if(needTo_useMask_evaluation == false) {mayUse_optimized_computaiton = true;} 
    else {
      mayUse_optimized_computaiton = (masksAre_aligned == e_typeOf_measure_spearman_masksAreAligned_always);
      
      if(!mayUse_optimized_computaiton) {
	if(masksAre_aligned == e_typeOf_measure_spearman_masksAreAligned_never) {mayUse_optimized_computaiton = false;}
	else if(masksAre_aligned == e_typeOf_measure_spearman_masksAreAligned_compare) {

	  // FXIME: cosnider to move "ktMask__vectorsAre_consistentlyInteresting(..)" to a new file named ....??... <-- may the 'caller' instead 'pad' the 'sorted set' with zeros ... and would 'such an approach' we 'as efficent'?
	  // FIXME: givent eh high time-cost of [”elow] is it possible to 'omit' [”elow] call?
	  mayUse_optimized_computaiton = ktMask__vectorsAre_consistentlyInteresting(nrows, ncols, data1_input, data2_input, mask1, mask2, transpose);
	}
      }
    }
  }

  s_allAgainstAll_config_t config; init_useFast__s_allAgainstAll_config(&config, config_allAgainstAll->masksAre_used); //! ie, default intaiton.
  //s_kt_correlationConfig config; s_kt_correlationConfig_init_useFast(&config, config_allAgainstAll->masksAre_used); //! ie, default intaiton.
  if(mayUse_optimized_computaiton == false) { //! then we use the slwo strategy: expects a time-overhead of "n" (ie, given teh extra/unneccesary sort-calls).
    //! Note: this 'approach' is needed as the pre-sorting 'has no point' if we first need to filter wrt. ithe indexes, ie, where 'mask' is set to different values
    // FIXME[article]: update our artilce-text wrt. [below].
    if(transpose == 0) {
      const uint nrows_2 = (config_allAgainstAll && (config_allAgainstAll->iterationIndex_2 != UINT_MAX)) ? config_allAgainstAll->iterationIndex_2 : nrows;
      for(uint index1 = 0; index1 < nrows; index1++) {
	for(uint index2 = 0; index2 < nrows_2; index2++) {
	  //! Then we 'make' a call whiche tiehr use an optmized OR slow impelemtantion, a choise dependent on the "config.forNonTransposed_useFastImplementaiton" configruation-attribute (oekseth, 06. sept. 2016).
	  const t_float distance = kt_spearman(ncols, data1_input, data2_input, mask1, mask2, weight, index1, index2, transpose, config);
	  if(resultMatrix) {  resultMatrix[index1][index2] = distance;}
	  else {ktCorr__postProcess_afterCorrelation_for_value(index1, index2, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1_input == data2_input));}
	}
      }
    } else {
      printf("(transpose)\tnrows=%u, ncols=%u, at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
      //assert(mask1);       assert(mask2);
      const uint ncols_2 = (config_allAgainstAll && (config_allAgainstAll->iterationIndex_2 != UINT_MAX)) ? config_allAgainstAll->iterationIndex_2 : ncols;
      for(uint index1 = 0; index1 < ncols; index1++) {
	//printf("\tcol_id=[...][%u], at %s:%d\n", index1, __FILE__, __LINE__);
	for(uint index2 = 0; index2 < ncols_2; index2++) {
	  //! Then we 'make' a call whiche tiehr use an optmized OR slow impelemtantion, a choise dependent on the "config.forNonTransposed_useFastImplementaiton" configruation-attribute (oekseth, 06. sept. 2016).
	  //printf("\tcol_id=[...][%u]---[...][%u], at %s:%d\n", index1, index2,  __FILE__, __LINE__);
	  const t_float distance = kt_spearman(nrows, data1_input, data2_input, mask1, mask2, weight, index1, index2, transpose, config);
	  if(resultMatrix) {  resultMatrix[index1][index2] = distance;}
	  else {
	    ktCorr__postProcess_afterCorrelation_for_value(index1, index2, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1_input == data2_input));
	  }
	}
      }
    }
    return;
  } 
  //! Note: at this exeuction-point we assume we are to compute for optimal matrices
  
  t_float **matrix_tmp_1 = NULL;  t_float **matrix_tmp_2 = NULL;
  //! Allocate memory and compute the rank for the matrices.
    //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
  ktCorrelation_compute_rankFor_matrices(nrows, ncols, data1_input, data2_input, mask1, mask2, &matrix_tmp_1, &matrix_tmp_2, transpose, needTo_useMask_evaluation, config_allAgainstAll->iterationIndex_2);
  

  //printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

  //! Step(2): matrix-computation: --------------------------------------------------------------------------------------
{
  t_float **data1 = matrix_tmp_1; t_float **data2 = matrix_tmp_1;
  if(data1_input != data2_input) {data2 = matrix_tmp_2;}
  mask1 = NULL; mask2 = NULL; //! ie, as the 'column-postion' refers to a differnet psotion in the sorted list.

  //! Then comptue the optmial distance-matrix:
  ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, 
						   /*type=*/e_kt_correlationFunction_groupOf_minkowski_euclid, 
						   //						   /*type=*/e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly, 
						   transpose, e_typeOf_metric_correlation_pearson_spearMan, config_allAgainstAll);
  // ->CLS, config_allAgainstAll->isTo_use_continousSTripsOf_memory, config_allAgainstAll->iterationIndex_2, /*isTo_use_SIMD=*/true, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, , config_allAgainstAll->masksAre_used);
  if(resultMatrix) {
    //printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    //! Comptue teh score:
    const uint iterationIndex_2 = (config_allAgainstAll) ? config_allAgainstAll->iterationIndex_2 : UINT_MAX;
    compute_score_correlation_forMatrix(nrows, ncols, data1, data2, mask1, mask2, resultMatrix, transpose, weight, e_typeOf_metric_correlation_pearson_spearMan, needTo_useMask_evaluation, iterationIndex_2);
  } //! else we asusem the correlations has been comptued seperately in the "ktCorr_matrixBase__optimal_compute_allAgainstAll(..)" call.


  //printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
 }



  //! De-allcoate the locally reserve3d memory:
 free_2d_list_float(&matrix_tmp_1, nrows);
  if(data1_input != data2_input) {
    free_2d_list_float(&matrix_tmp_2, nrows);
  }

}
