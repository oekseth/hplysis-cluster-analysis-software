
//!
//! Appraoch: primary: use the appraoch defined in the "cluster.c" library:
math_generateDistribution__randomassign(nclusters, nelements, tclusterid);

math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(nclusters, nelements, tclusterid);

/* //! */
/* //! Appraoch: secondary: handle cases when randomziaotn-function 'poitnlessly' results in all vertices being tin the same cluster: */
/* // FIXME[aritlce]: udpate our artilce wrt. [below]. */
/*       { //! Add a 'handler' for the case hwer ethe 'random allocaiton' reuslts in all veritces 'bieng in the same', a 'case' which may arise when input-data-sets are sufifcnetly small enough (oekseth, 06. jan. 2017): */
/* 	// FIXME[aritlce]: upate our aritlce with thsi observation .. ie, as 'this cas'e is Not handled by impelmtatinos such as "clsuter.c" k-means-clsuter-impemletnmatnhion */
/* 	uint clusterId_max = 0; uint clusterId_min = UINT_MAX; */
/* 	for(uint j = 0; j < nelements; j++) { */
/* 	  clusterId_max = macro_max(clusterId_max, tclusterid[j]); */
/* 	  clusterId_min = macro_min(clusterId_min, tclusterid[j]); */
/* 	} //! ie, update the container 'which is not reset in each while-loop'. */
/* 	if(clusterId_max == clusterId_min) { //! then our 'random fucntion' has suggested all vertices to be 'in the same', ie, then 'explcit update our allcoation': */
/* 	  /\* const uint cnt_min = macro_min(nclusters, nelements); *\/ */
/* 	  /\* for(uint j = 0; j < cnt_min; j++) { *\/ */
/* 	  /\*   //	  clusterid[j] =  *\/ */
/* 	  /\*   tclusterid[j] = /\\*cluster-id=*\\/j; *\/ */
/* 	  /\* } *\/ */
/* 	  { //! Note: in [”elow] we 'itrndocue' a handler to 'fix' cases wehere the random-suggest wrognly 'conrges': */
/* 	    // FIXME[aritlce]: udpate our artilce wrt. [below]. */
/* 	    // FIXME[aritlce]: evlautte the extera/adidtional cost 'of this appraoch'. */
/* 	    // TODO: evlauate/idneityf the 'need' fo this. */
/* 	    uint cnt_changes = 0; uint max_cntIter = 40;   uint cnt_iter_curr = 0; */
/* 	    const uint cnt_half = 0.5*nelements; */
/* 	    assert_possibleOverhead(cnt_half > 0); */
/* 	    while(cnt_changes == 0) { */
/* 	      for(uint j = 0; j < nelements; j++) {cnt_changes = (clusterid[j] == tclusterid[j]);} */
/* 	      //fprintf(stderr, "try[%u]\tcnt_changes=%u, at %s:%d\n", cnt_iter_curr, cnt_changes, __FILE__, __LINE__); */
/* 	      if(cnt_changes == 0) { //! then 'apply' an alternative 'random swap': */
/* 		//! First swap [”elow] and [ªbove]: */
/* 		if(true) { */
/* 		  const uint cnt_each = nelements/nclusters; */
/* 		  uint k = 0; uint row_id = 0; */
/* 		  for(; k < nclusters-1; k++) { */
/* 		    for(uint i = 0; i < cnt_each; i++) { */
/* 		      tclusterid[row_id++] = k; */
/* 		      assert_possibleOverhead(row_id < nelements); */
/* 		    } */
/* 		  } */
/* 		  //! Update for the 'trailing part': */
/* 		  assert_possibleOverhead(row_id < nelements); */
/* 		  for(; row_id < nelements; row_id++) { */
/* 		    tclusterid[row_id] = k; */
/* 		    assert_possibleOverhead(row_id < nelements); */
/* 		    assert_possibleOverhead(row_id < nelements); */
/* 		  } */
/* 		  //! Then re-apply the randomziaiton, ie, to avodi a 'local convregence wrt. this': */
/* 		  // FIXME[aricle] ivnesigtate the improatnce of [below] */
/* 		  //printf("nelements=%u, at %s:%d\n", nelements, __FILE__, __LINE__); */
/* 		  math_generateDistribution__randomPermutation__uint__sameArray(tclusterid, nelements, /\*list_size_cnttoswap=*\/0.5*nelements, /\*config_swapsMayOverlap=*\/true, /\*maxValue_toSetInRandomization=*\/nclusters-1); */
/* 		} else { */
/* 		  for(uint j = 0; j < cnt_half; j++) {const uint tmp = tclusterid[nelements-j-1]; */
/* 		    tclusterid[j] = tclusterid[nelements-j-1]; */
/* 		    tclusterid[nelements-j-1]  = tmp; */
/* 		  } */
/* 		  //! Then re-apply the randomziaiton, ie, to avodi a 'local convregence wrt. this': */
/* 		  // FIXME[aricle] ivnesigtate the improatnce of [below] */
/* 		  math_generateDistribution__randomassign(nclusters, nelements, tclusterid); */
/* 		} */
/* 	      } */
/* 	      if(cnt_iter_curr++ > max_cntIter) { */
/* #ifndef NDEBUG */
/* 		// fprintf(stderr, "!!(random-still-same)\t Investigate this case: for quesitons please cotnact the devleoper at [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
/* #endif */
/* 		break; */
/* 	      } */
/* 	    }	 */
/* 	  } */
/* 	} */
/*       } */

