/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @brie defines seperately each correlation-fucnton, a strategy used for opmtiziaotn.
   @remarks last generated at Fri Oct 13 17:49:55 2017 from "build_codeFor_tiling.pl"
 **/
//! ---------------------------------------------------------------------------------------------------------------------------------------------------------
//!
//! Define muliple functions:
//! Note: the distance-macros are expected to be found in our "correlation_macros__distanceMeasures.h"
//! 
#include "e_template_correlation_tile.h"

//!
//! Build for euclid:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__euclid__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__euclid__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__euclid__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__euclid__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__euclid

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__euclid__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__euclid__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__euclid__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__euclid__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__euclid__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__euclid__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__euclid__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__euclid__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__euclid__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__euclid__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__euclid__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__euclid__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__euclid__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__euclid__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__euclid__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__euclid__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__euclid__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__euclid__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__euclid__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__euclid__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__euclid //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__euclid__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__euclid__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for cityBlock:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__cityBlock__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__cityBlock__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__cityBlock__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__cityBlock__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__cityBlock

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__cityBlock__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__cityBlock__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__cityBlock__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__cityBlock__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__cityBlock__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__cityBlock__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__cityBlock__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__cityBlock__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__cityBlock__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__cityBlock__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__cityBlock__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__cityBlock__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__cityBlock__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__cityBlock__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__cityBlock__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__cityBlock__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__cityBlock__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__cityBlock__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__cityBlock__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__cityBlock__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__cityBlock //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__cityBlock__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for minkowski:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__minkowski__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__minkowski__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__minkowski__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__minkowski__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__minkowski__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__minkowski__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__minkowski__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__minkowski__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__minkowski__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__minkowski__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__minkowski__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__minkowski__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__minkowski__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__minkowski__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__minkowski__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__minkowski__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__minkowski__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__minkowski__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__minkowski__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__minkowski__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__minkowski__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__minkowski__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__minkowski__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__minkowski__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for minkowski__subCase__zeroOne:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__minkowski__subCase__zeroOne__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__minkowski__subCase__zeroOne__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__minkowski__subCase__zeroOne__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__minkowski__subCase__zeroOne__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__zeroOne

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__zeroOne__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__minkowski__subCase__zeroOne__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__minkowski__subCase__zeroOne__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__minkowski__subCase__zeroOne__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__zeroOne__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__minkowski__subCase__zeroOne__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__minkowski__subCase__zeroOne__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__minkowski__subCase__zeroOne__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__zeroOne__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__minkowski__subCase__zeroOne__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__zeroOne__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__zeroOne__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__zeroOne__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__minkowski__subCase__zeroOne__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__minkowski__subCase__zeroOne__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__minkowski__subCase__zeroOne__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__zeroOne__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__minkowski__subCase__zeroOne__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__zeroOne__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__zeroOne__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__zeroOne //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__zeroOne__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for minkowski__subCase__pow3:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow3__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__minkowski__subCase__pow3__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__minkowski__subCase__pow3__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__minkowski__subCase__pow3__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__pow3

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow3__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__minkowski__subCase__pow3__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow3__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow3__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow3__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__minkowski__subCase__pow3__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__minkowski__subCase__pow3__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__minkowski__subCase__pow3__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow3__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__minkowski__subCase__pow3__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow3__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow3__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow3__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__minkowski__subCase__pow3__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__minkowski__subCase__pow3__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__minkowski__subCase__pow3__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow3__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__minkowski__subCase__pow3__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow3__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow3__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__pow3 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__pow3__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski_pow3__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for minkowski__subCase__pow4:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow4__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__minkowski__subCase__pow4__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__minkowski__subCase__pow4__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__minkowski__subCase__pow4__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__pow4

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow4__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__minkowski__subCase__pow4__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow4__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow4__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow4__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__minkowski__subCase__pow4__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__minkowski__subCase__pow4__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__minkowski__subCase__pow4__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow4__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__minkowski__subCase__pow4__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow4__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow4__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow4__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__minkowski__subCase__pow4__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__minkowski__subCase__pow4__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__minkowski__subCase__pow4__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow4__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__minkowski__subCase__pow4__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow4__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow4__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__pow4 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__pow4__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski_pow4__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for minkowski__subCase__pow5:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow5__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__minkowski__subCase__pow5__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__minkowski__subCase__pow5__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__minkowski__subCase__pow5__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__minkowski__subCase__pow5

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow5__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__minkowski__subCase__pow5__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow5__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow5__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow5__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__minkowski__subCase__pow5__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__minkowski__subCase__pow5__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__minkowski__subCase__pow5__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow5__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__minkowski__subCase__pow5__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow5__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow5__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow5__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__minkowski__subCase__pow5__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__minkowski__subCase__pow5__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__minkowski__subCase__pow5__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__minkowski__subCase__pow5__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__minkowski__subCase__pow5__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__minkowski__subCase__pow5__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__minkowski__subCase__pow5__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__minkowski__pow5 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__minkowski__pow5__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__minkowski_pow5__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for chebychev:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__chebychev__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__chebychev__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__chebychev__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__chebychev__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__chebychev

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__chebychev__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__chebychev__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__chebychev__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__chebychev__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__chebychev__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__chebychev__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__chebychev__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__chebychev__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__chebychev__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__chebychev__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__chebychev__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__chebychev__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__chebychev__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__chebychev__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__chebychev__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__chebychev__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__chebychev__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__chebychev__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__chebychev__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__chebychev__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__chebychev //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__chebychev__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 1 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__chebychev__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for chebychev_minInsteadOf_max:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__chebychev_minInsteadOf_max__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__chebychev_minInsteadOf_max__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__chebychev_minInsteadOf_max__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__chebychev_minInsteadOf_max__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__chebychev_minInsteadOf_max

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__chebychev_minInsteadOf_max__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__chebychev_minInsteadOf_max__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__chebychev_minInsteadOf_max__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__chebychev_minInsteadOf_max__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__chebychev_minInsteadOf_max__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__chebychev_minInsteadOf_max__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__chebychev_minInsteadOf_max__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__chebychev_minInsteadOf_max__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__chebychev_minInsteadOf_max__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__chebychev_minInsteadOf_max__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__chebychev_minInsteadOf_max__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__chebychev_minInsteadOf_max__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__chebychev_minInsteadOf_max__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__chebychev_minInsteadOf_max__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__chebychev_minInsteadOf_max__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__chebychev_minInsteadOf_max__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__chebychev_minInsteadOf_max__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__chebychev_minInsteadOf_max__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__chebychev_minInsteadOf_max__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__chebychev_minInsteadOf_max__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__chebychev //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__chebychev__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 1 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__chebychev__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Sorensen:
//! @remarks the 'Sorensen' metric is widely used in ecology: a different name for the 'Sorensen' metric is the 'Bray-Curtis' metric. 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Sorensen__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Sorensen__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Sorensen__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Sorensen__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Sorensen

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Sorensen__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Sorensen__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Sorensen__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Sorensen__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Sorensen__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Sorensen__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Sorensen__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Sorensen__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Sorensen__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Sorensen__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Sorensen__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Sorensen__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Sorensen__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Sorensen__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Sorensen__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Sorensen__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Sorensen__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Sorensen__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Sorensen__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Sorensen__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Gower:
//! @remarks Gower differs from Chebychev with respect to the normalization of the entites signficance in the data-set, ie, (1/d). In the 'default' verison we assume that the error-rate is set to zero, ie, as explained in the work of Gower 	tep{http://cbio.mines-paristech.fr/~jvert/svn/bibli/local/Gower1971general.pdf}.  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Gower__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Gower__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Gower__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Gower__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Gower

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Gower__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Gower__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Gower__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Gower__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Gower__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Gower__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Gower__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Gower__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Gower__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Gower__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Gower__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Gower__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Gower__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Gower__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Gower__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Gower__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Gower__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Gower__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Gower__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Gower__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Gower //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Gower__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__absoluteDifferenc__Gower__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Soergel:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Soergel__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Soergel__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Soergel__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Soergel__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Soergel

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Soergel__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Soergel__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Soergel__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Soergel__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Soergel__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Soergel__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Soergel__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Soergel__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Soergel__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Soergel__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Soergel__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Soergel__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Soergel__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Soergel__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Soergel__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Soergel__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Soergel__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Soergel__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Soergel__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Soergel__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Soergel //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Soergel__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Kulczynski_absDiff:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Kulczynski_absDiff__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Kulczynski_absDiff__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Kulczynski_absDiff__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Kulczynski_absDiff__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Kulczynski_absDiff

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Kulczynski_absDiff__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Kulczynski_absDiff__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Kulczynski_absDiff__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Kulczynski_absDiff__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Kulczynski_absDiff__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Kulczynski_absDiff__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Kulczynski_absDiff__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Kulczynski_absDiff__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Kulczynski_absDiff__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Kulczynski_absDiff__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Kulczynski_absDiff__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Kulczynski_absDiff__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Kulczynski_absDiff__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Kulczynski_absDiff__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Kulczynski_absDiff__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Kulczynski_absDiff__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Kulczynski_absDiff__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Kulczynski_absDiff__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Kulczynski_absDiff__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Kulczynski_absDiff__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Kulczynski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Kulczynski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Canberra:
//! @remarks difference between 'Canberra' and 'Sorensen' conserns how 'Canberra' normalizes the absoltue difference at the individual level. 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Canberra__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Canberra__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Canberra__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Canberra__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Canberra

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Canberra__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Canberra__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Canberra__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Canberra__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Canberra__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Canberra__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Canberra__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Canberra__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Canberra__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Canberra__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Canberra__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Canberra__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Canberra__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Canberra__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Canberra__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Canberra__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Canberra__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Canberra__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Canberra__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Canberra__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Canberra //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Canberra__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Lorentzian:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Lorentzian__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Lorentzian__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Lorentzian__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Lorentzian__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Lorentzian

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Lorentzian__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Lorentzian__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Lorentzian__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Lorentzian__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Lorentzian__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Lorentzian__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Lorentzian__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Lorentzian__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Lorentzian__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Lorentzian__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Lorentzian__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Lorentzian__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Lorentzian__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Lorentzian__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Lorentzian__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Lorentzian__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Lorentzian__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Lorentzian__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Lorentzian__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Lorentzian__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Lorentzian //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__absoluteDifferenc__Lorentzian__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_mean //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_mean__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_mean__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean_standardDeviation_userDefinedPower:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_userDefinedPower

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPower__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_userDefinedPow_ZeroOne__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow_zeroOne //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow_zeroOne__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 1  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow2:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow2__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow2__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow2__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow2__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow2

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow2__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow2__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow2__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow2__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow2__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow3:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow3__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow3__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow3__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow3__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow3

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow3__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow3__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow3__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow3__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow3__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow4:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow4__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow4__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow4__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow4__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow4

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow4__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow4__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow4__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow4__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow4__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for oekseth_forMasks_mean_standardDeviation_pow5:
//! @remarks Identify the deviation between the mean of columns which are only partially described (ie, where cell-masks are used) 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow5__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow5__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow5__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow5__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__oekseth_forMasks_mean_standardDeviation_pow5

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow5__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow5__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow5__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow5__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__oekseth_forMasks_mean_standardDeviation_pow5__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5 //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for intersection:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__intersection__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__intersection__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__intersection__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__intersection__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__intersection

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__intersection__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__intersection__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__intersection__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__intersection__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__intersection__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__intersection__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__intersection__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__intersection__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__intersection__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__intersection__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__intersection__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__intersection__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__intersection__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__intersection__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__intersection__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__intersection__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__intersection__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__intersection__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__intersection__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__intersection__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__intersection //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__intersection__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for intersectionInverted:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__intersectionInverted__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__intersectionInverted__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__intersectionInverted__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__intersectionInverted__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__intersectionInverted

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__intersectionInverted__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__intersectionInverted__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__intersectionInverted__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__intersectionInverted__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__intersectionInverted__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__intersectionInverted__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__intersectionInverted__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__intersectionInverted__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__intersectionInverted__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__intersectionInverted__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__intersectionInverted__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__intersectionInverted__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__intersectionInverted__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__intersectionInverted__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__intersectionInverted__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__intersectionInverted__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__intersectionInverted__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__intersectionInverted__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__intersectionInverted__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__intersectionInverted__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__intersectionAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__intersectionAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__intersectionAlt__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for WaveHedges:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__WaveHedges__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__WaveHedges__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__WaveHedges__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__WaveHedges__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__WaveHedges

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__WaveHedges__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__WaveHedges__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__WaveHedges__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__WaveHedges__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__WaveHedges__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__WaveHedges__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__WaveHedges__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__WaveHedges__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__WaveHedges__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__WaveHedges__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__WaveHedges__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__WaveHedges__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__WaveHedges__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__WaveHedges__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__WaveHedges__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__WaveHedges__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__WaveHedges__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__WaveHedges__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__WaveHedges__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__WaveHedges__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for WaveHedges_alt:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__WaveHedges_alt__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__WaveHedges_alt__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__WaveHedges_alt__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__WaveHedges_alt__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__WaveHedges_alt

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__WaveHedges_alt__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__WaveHedges_alt__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__WaveHedges_alt__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__WaveHedges_alt__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__WaveHedges_alt__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__WaveHedges_alt__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__WaveHedges_alt__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__WaveHedges_alt__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__WaveHedges_alt__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__WaveHedges_alt__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__WaveHedges_alt__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__WaveHedges_alt__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__WaveHedges_alt__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__WaveHedges_alt__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__WaveHedges_alt__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__WaveHedges_alt__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__WaveHedges_alt__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__WaveHedges_alt__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__WaveHedges_alt__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__WaveHedges_alt__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges_alt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__WaveHedges_alt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Czekanowski:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Czekanowski__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Czekanowski__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Czekanowski__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Czekanowski__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Czekanowski

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Czekanowski__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Czekanowski__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Czekanowski__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Czekanowski__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Czekanowski__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Czekanowski__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Czekanowski__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Czekanowski__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Czekanowski__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Czekanowski__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Czekanowski__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Czekanowski__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Czekanowski__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Czekanowski__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Czekanowski__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Czekanowski__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Czekanowski__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Czekanowski__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Czekanowski__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Czekanowski__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Czekanowski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Czekanowski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Czekanowski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Czekanowski_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Czekanowski_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Czekanowski_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Czekanowski_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Czekanowski_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Czekanowski_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Czekanowski_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Czekanowski_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Czekanowski_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Czekanowski_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Czekanowski_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Czekanowski_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Czekanowski_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Czekanowski_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Czekanowski_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Czekanowski_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Czekanowski_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Czekanowski_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Czekanowski_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Czekanowski_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Czekanowski_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Czekanowski_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Czekanowski_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Czekanowski_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Czekanowski_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Czekanowski_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__CzekanowskiAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__CzekanowskiAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__CzekanowskiAlt__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Motyka:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Motyka__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Motyka__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Motyka__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Motyka__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Motyka

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Motyka__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Motyka__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Motyka__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Motyka__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Motyka__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Motyka__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Motyka__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Motyka__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Motyka__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Motyka__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Motyka__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Motyka__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Motyka__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Motyka__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Motyka__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Motyka__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Motyka__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Motyka__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Motyka__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Motyka__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Motyka //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Motyka__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Motyka__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Motyka_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Motyka_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Motyka_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Motyka_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Motyka_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Motyka_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Motyka_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Motyka_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Motyka_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Motyka_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Motyka_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Motyka_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Motyka_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Motyka_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Motyka_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Motyka_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Motyka_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Motyka_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Motyka_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Motyka_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Motyka_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Motyka_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Motyka_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Motyka_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Motyka_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Motyka_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__MotykaAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__MotykaAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Motyka__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Kulczynski:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Kulczynski__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Kulczynski__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Kulczynski__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Kulczynski__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Kulczynski

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Kulczynski__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Kulczynski__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Kulczynski__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Kulczynski__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Kulczynski__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Kulczynski__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Kulczynski__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Kulczynski__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Kulczynski__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Kulczynski__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Kulczynski__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Kulczynski__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Kulczynski__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Kulczynski__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Kulczynski__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Kulczynski__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Kulczynski__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Kulczynski__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Kulczynski__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Kulczynski__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Kulczynski //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Kulczynski__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Kulczynski__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Ruzicka:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Ruzicka__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Ruzicka__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Ruzicka__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Ruzicka__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Ruzicka

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Ruzicka__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Ruzicka__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Ruzicka__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Ruzicka__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Ruzicka__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Ruzicka__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Ruzicka__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Ruzicka__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Ruzicka__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Ruzicka__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Ruzicka__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Ruzicka__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Ruzicka__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Ruzicka__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Ruzicka__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Ruzicka__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Ruzicka__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Ruzicka__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Ruzicka__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Ruzicka__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Ruzicka //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Ruzicka__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Ruzicka__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Tanimoto:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Tanimoto__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Tanimoto__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Tanimoto__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Tanimoto__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Tanimoto

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Tanimoto__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Tanimoto__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Tanimoto__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Tanimoto__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Tanimoto__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Tanimoto__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Tanimoto__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Tanimoto__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Tanimoto__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Tanimoto__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Tanimoto__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Tanimoto__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Tanimoto__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Tanimoto__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Tanimoto__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Tanimoto__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Tanimoto__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Tanimoto__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Tanimoto__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Tanimoto__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__Tanimoto //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__Tanimoto__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__Tanimoto__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Tanimoto_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Tanimoto_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Tanimoto_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Tanimoto_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Tanimoto_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Tanimoto_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Tanimoto_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Tanimoto_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Tanimoto_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Tanimoto_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Tanimoto_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Tanimoto_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Tanimoto_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Tanimoto_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Tanimoto_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Tanimoto_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Tanimoto_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Tanimoto_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Tanimoto_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Tanimoto_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Tanimoto_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Tanimoto_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Tanimoto_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Tanimoto_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Tanimoto_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Tanimoto_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__intersection__TanimotoAlt //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__intersection__TanimotoAlt__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__intersection__TanimotoAlt__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for innerProduct:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__innerProduct__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__innerProduct__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__innerProduct__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__innerProduct__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__innerProduct

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__innerProduct__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__innerProduct__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__innerProduct__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__innerProduct__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__innerProduct__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__innerProduct__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__innerProduct__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__innerProduct__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__innerProduct__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__innerProduct__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__innerProduct__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__innerProduct__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__innerProduct__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__innerProduct__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__innerProduct__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__innerProduct__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__innerProduct__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__innerProduct__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__innerProduct__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__innerProduct__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__innerProduct //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__innerProduct__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for harmonicMean:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__harmonicMean__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__harmonicMean__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__harmonicMean__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__harmonicMean__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__harmonicMean

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__harmonicMean__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__harmonicMean__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__harmonicMean__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__harmonicMean__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__harmonicMean__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__harmonicMean__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__harmonicMean__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__harmonicMean__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__harmonicMean__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__harmonicMean__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__harmonicMean__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__harmonicMean__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__harmonicMean__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__harmonicMean__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__harmonicMean__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__harmonicMean__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__harmonicMean__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__harmonicMean__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__harmonicMean__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__harmonicMean__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__harmonicMean //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__harmonicMean__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__harmonicMean__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for cosine:
//! @remarks The 'cosine' metric (or: 'angular metric')  measure the angle between two vectors 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__cosine__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__cosine__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__cosine__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__cosine__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__cosine

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__cosine__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__cosine__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__cosine__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__cosine__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__cosine__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__cosine__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__cosine__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__cosine__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__cosine__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__cosine__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__cosine__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__cosine__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__cosine__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__cosine__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__cosine__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__cosine__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__cosine__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__cosine__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__cosine__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__cosine__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__cosine__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for HumarHasseBrook_PCA_or_Jaccard:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__HumarHasseBrook_PCA_or_Jaccard__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__HumarHasseBrook_PCA_or_Jaccard__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__HumarHasseBrook_PCA_or_Jaccard__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__HumarHasseBrook_PCA_or_Jaccard__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__HumarHasseBrook_PCA_or_Jaccard

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__HumarHasseBrook_PCA_or_Jaccard__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__HumarHasseBrook_PCA_or_Jaccard__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__HumarHasseBrook_PCA_or_Jaccard__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__HumarHasseBrook_PCA_or_Jaccard__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__HumarHasseBrook_PCA_or_Jaccard__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__HumarHasseBrook_PCA_or_Jaccard__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__HumarHasseBrook_PCA_or_Jaccard__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__HumarHasseBrook_PCA_or_Jaccard__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__HumarHasseBrook_PCA_or_Jaccard__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__HumarHasseBrook_PCA_or_Jaccard__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__HumarHasseBrook_PCA_or_Jaccard__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__HumarHasseBrook_PCA_or_Jaccard__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__HumarHasseBrook_PCA_or_Jaccard__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__HumarHasseBrook_PCA_or_Jaccard__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__HumarHasseBrook_PCA_or_Jaccard__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__HumarHasseBrook_PCA_or_Jaccard__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__HumarHasseBrook_PCA_or_Jaccard__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__HumarHasseBrook_PCA_or_Jaccard__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__HumarHasseBrook_PCA_or_Jaccard__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__HumarHasseBrook_PCA_or_Jaccard__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__KumarHassebrook_or_Jaccard__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Dice:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Dice__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Dice__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Dice__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Dice__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Dice

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Dice__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Dice__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Dice__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Dice__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Dice__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Dice__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Dice__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Dice__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Dice__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Dice__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Dice__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Dice__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Dice__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Dice__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Dice__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Dice__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Dice__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Dice__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Dice__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Dice__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__Dice__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Jaccard_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Jaccard_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Jaccard_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Jaccard_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Jaccard_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Jaccard_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Jaccard_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Jaccard_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Jaccard_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Jaccard_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Jaccard_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Jaccard_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Jaccard_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Jaccard_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Jaccard_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Jaccard_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Jaccard_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Jaccard_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Jaccard_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Jaccard_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Jaccard_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Jaccard_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Jaccard_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Jaccard_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Jaccard_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Jaccard_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__JaccardAltDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Dice_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Dice_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Dice_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Dice_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Dice_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Dice_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Dice_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Dice_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Dice_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Dice_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Dice_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Dice_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Dice_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Dice_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Dice_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Dice_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Dice_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Dice_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Dice_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Dice_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Dice_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Dice_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Dice_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Dice_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Dice_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Dice_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__DiceAltDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for sampleCoVariance:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__sampleCoVariance__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__sampleCoVariance__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__sampleCoVariance__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__sampleCoVariance__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__sampleCoVariance

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__sampleCoVariance__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__sampleCoVariance__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__sampleCoVariance__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__sampleCoVariance__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__sampleCoVariance__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__sampleCoVariance__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__sampleCoVariance__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__sampleCoVariance__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__sampleCoVariance__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__sampleCoVariance__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__sampleCoVariance__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__sampleCoVariance__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__sampleCoVariance__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__sampleCoVariance__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__sampleCoVariance__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__sampleCoVariance__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__sampleCoVariance__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__sampleCoVariance__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__sampleCoVariance__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__sampleCoVariance__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCoVariance //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Brownian:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Brownian__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Brownian__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Brownian__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Brownian__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Brownian

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Brownian__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Brownian__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Brownian__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Brownian__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Brownian__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Brownian__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Brownian__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Brownian__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Brownian__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Brownian__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Brownian__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Brownian__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Brownian__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Brownian__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Brownian__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Brownian__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Brownian__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Brownian__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Brownian__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Brownian__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Brownian_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Brownian_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Brownian_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Brownian_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Brownian_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Brownian_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Brownian_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Brownian_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Brownian_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Brownian_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Brownian_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Brownian_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Brownian_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Brownian_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Brownian_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Brownian_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Brownian_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Brownian_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Brownian_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Brownian_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Brownian_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Brownian_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Brownian_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Brownian_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Brownian_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Brownian_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__permutated_innerRootInDenumerator__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for fidelity:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__fidelity__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__fidelity__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__fidelity__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__fidelity__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__fidelity

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__fidelity__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__fidelity__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__fidelity__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__fidelity__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__fidelity__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__fidelity__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__fidelity__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__fidelity__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__fidelity__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__fidelity__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__fidelity__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__fidelity__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__fidelity__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__fidelity__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__fidelity__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__fidelity__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__fidelity__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__fidelity__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__fidelity__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__fidelity__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction metric_macro_defaultFunctions__postProcess__none //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Bhattacharyya:
//! @remarks the metric is adjusted in order to 'handle' empty valeus by taking the 'abs' of the v*u product, an appraoch also applifed for 'fidelity', 'Hellinger', 'Matusitat' and 'squared chord'; in order to simplify the indiciation of special cases 'using' 'abs(..)' in combiaton with the 'sqrt(..)' funciton please search for all applications of the 'mathLib_float_sqrt_abs(..)' call in the soruce-code of hpLysis. 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Bhattacharyya__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Bhattacharyya__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Bhattacharyya__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Bhattacharyya__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Bhattacharyya

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Bhattacharyya__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Bhattacharyya__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Bhattacharyya__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Bhattacharyya__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Bhattacharyya__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Bhattacharyya__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Bhattacharyya__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Bhattacharyya__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Bhattacharyya__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Bhattacharyya__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Bhattacharyya__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Bhattacharyya__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Bhattacharyya__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Bhattacharyya__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Bhattacharyya__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Bhattacharyya__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Bhattacharyya__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Bhattacharyya__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Bhattacharyya__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Bhattacharyya__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Bhattacharyya__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Hellinger:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Hellinger__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Hellinger__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Hellinger__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Hellinger__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Hellinger

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Hellinger__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Hellinger__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Hellinger__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Hellinger__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Hellinger__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Hellinger__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Hellinger__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Hellinger__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Hellinger__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Hellinger__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Hellinger__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Hellinger__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Hellinger__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Hellinger__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Hellinger__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Hellinger__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Hellinger__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Hellinger__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Hellinger__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Hellinger__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Hellinger__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Matusita:
//! @remarks Matusita 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Matusita__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Matusita__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Matusita__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Matusita__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Matusita

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Matusita__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Matusita__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Matusita__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Matusita__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Matusita__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Matusita__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Matusita__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Matusita__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Matusita__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Matusita__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Matusita__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Matusita__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Matusita__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Matusita__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Matusita__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Matusita__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Matusita__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Matusita__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Matusita__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Matusita__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Hellinger_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Hellinger_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Hellinger_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Hellinger_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Hellinger_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Hellinger_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Hellinger_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Hellinger_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Hellinger_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Hellinger_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Hellinger_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Hellinger_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Hellinger_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Hellinger_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Hellinger_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Hellinger_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Hellinger_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Hellinger_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Hellinger_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Hellinger_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Hellinger_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Hellinger_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Hellinger_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Hellinger_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Hellinger_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Hellinger_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Hellinger_altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Matusita_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Matusita_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Matusita_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Matusita_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Matusita_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Matusita_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Matusita_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Matusita_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Matusita_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Matusita_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Matusita_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Matusita_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Matusita_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Matusita_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Matusita_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Matusita_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Matusita_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Matusita_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Matusita_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Matusita_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Matusita_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Matusita_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Matusita_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Matusita_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Matusita_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Matusita_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for squaredChord:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__squaredChord__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__squaredChord__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__squaredChord__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__squaredChord__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__squaredChord

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__squaredChord__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__squaredChord__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__squaredChord__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__squaredChord__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__squaredChord__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__squaredChord__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__squaredChord__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__squaredChord__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__squaredChord__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__squaredChord__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__squaredChord__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__squaredChord__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__squaredChord__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__squaredChord__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__squaredChord__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__squaredChord__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__squaredChord__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__squaredChord__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__squaredChord__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__squaredChord__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__altDef__Hellinger_or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for squaredChord_altDef:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__squaredChord_altDef__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__squaredChord_altDef__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__squaredChord_altDef__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__squaredChord_altDef__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__squaredChord_altDef

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__squaredChord_altDef__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__squaredChord_altDef__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__squaredChord_altDef__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__squaredChord_altDef__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__squaredChord_altDef__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__squaredChord_altDef__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__squaredChord_altDef__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__squaredChord_altDef__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__squaredChord_altDef__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__squaredChord_altDef__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__squaredChord_altDef__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__squaredChord_altDef__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__squaredChord_altDef__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__squaredChord_altDef__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__squaredChord_altDef__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__squaredChord_altDef__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__squaredChord_altDef__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__squaredChord_altDef__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__squaredChord_altDef__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__squaredChord_altDef__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__fidelity__fidelity_or_Bhattacharyya_or_Hellinger__or_Matusita_or_squaredChord__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__fidelity__Matusita_or_squaredChord__altDef__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Euclid_squared:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Euclid_squared__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Euclid_squared__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Euclid_squared__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Euclid_squared__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Euclid_squared

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Euclid_squared__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Euclid_squared__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Euclid_squared__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Euclid_squared__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Euclid_squared__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Euclid_squared__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Euclid_squared__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Euclid_squared__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Euclid_squared__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Euclid_squared__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Euclid_squared__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Euclid_squared__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Euclid_squared__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Euclid_squared__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Euclid_squared__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Euclid_squared__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Euclid_squared__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Euclid_squared__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Euclid_squared__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Euclid_squared__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Euclid //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Euclid__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Euclid__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Pearson_squared:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Pearson_squared__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Pearson_squared__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Pearson_squared__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Pearson_squared__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_squared

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Pearson_squared__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Pearson_squared__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Pearson_squared__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Pearson_squared__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Pearson_squared__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Pearson_squared__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Pearson_squared__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Pearson_squared__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Pearson_squared__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Pearson_squared__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Pearson_squared__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Pearson_squared__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Pearson_squared__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Pearson_squared__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Pearson_squared__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Pearson_squared__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Pearson_squared__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Pearson_squared__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Pearson_squared__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Pearson_squared__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Pearson //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Pearson__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Pearson__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Neyman:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Neyman__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Neyman__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Neyman__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Neyman__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Neyman

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Neyman__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Neyman__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Neyman__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Neyman__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Neyman__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Neyman__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Neyman__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Neyman__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Neyman__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Neyman__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Neyman__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Neyman__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Neyman__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Neyman__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Neyman__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Neyman__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Neyman__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Neyman__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Neyman__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Neyman__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Neyman //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Neyman__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Neyman__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for squaredChi:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__squaredChi__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__squaredChi__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__squaredChi__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__squaredChi__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__squaredChi

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__squaredChi__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__squaredChi__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__squaredChi__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__squaredChi__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__squaredChi__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__squaredChi__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__squaredChi__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__squaredChi__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__squaredChi__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__squaredChi__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__squaredChi__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__squaredChi__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__squaredChi__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__squaredChi__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__squaredChi__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__squaredChi__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__squaredChi__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__squaredChi__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__squaredChi__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__squaredChi__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__squaredChi__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for probabilisticChi:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__probabilisticChi__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__probabilisticChi__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__probabilisticChi__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__probabilisticChi__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__probabilisticChi

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__probabilisticChi__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__probabilisticChi__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__probabilisticChi__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__probabilisticChi__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__probabilisticChi__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__probabilisticChi__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__probabilisticChi__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__probabilisticChi__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__probabilisticChi__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__probabilisticChi__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__probabilisticChi__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__probabilisticChi__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__probabilisticChi__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__probabilisticChi__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__probabilisticChi__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__probabilisticChi__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__probabilisticChi__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__probabilisticChi__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__probabilisticChi__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__probabilisticChi__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__probabilistic_or_divergence__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for squared_divergence:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__squared_divergence__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__squared_divergence__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__squared_divergence__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__squared_divergence__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__squared_divergence

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__squared_divergence__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__squared_divergence__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__squared_divergence__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__squared_divergence__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__squared_divergence__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__squared_divergence__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__squared_divergence__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__squared_divergence__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__squared_divergence__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__squared_divergence__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__squared_divergence__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__squared_divergence__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__squared_divergence__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__squared_divergence__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__squared_divergence__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__squared_divergence__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__squared_divergence__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__squared_divergence__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__squared_divergence__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__squared_divergence__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__divergence //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__divergence__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__probabilistic_or_divergence__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Clark:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Clark__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Clark__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Clark__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Clark__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Clark

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Clark__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Clark__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Clark__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Clark__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Clark__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Clark__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Clark__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Clark__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Clark__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Clark__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Clark__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Clark__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Clark__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Clark__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Clark__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Clark__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Clark__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Clark__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Clark__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Clark__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__Clark //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__Clark__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__Clark__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for addativeSymmetricSquaredChi:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__addativeSymmetricSquaredChi__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__addativeSymmetricSquaredChi__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__addativeSymmetricSquaredChi__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__addativeSymmetricSquaredChi__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__addativeSymmetricSquaredChi

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__addativeSymmetricSquaredChi__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__addativeSymmetricSquaredChi__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__addativeSymmetricSquaredChi__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__addativeSymmetricSquaredChi__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__addativeSymmetricSquaredChi__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__addativeSymmetricSquaredChi__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__addativeSymmetricSquaredChi__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__addativeSymmetricSquaredChi__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__addativeSymmetricSquaredChi__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__addativeSymmetricSquaredChi__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__addativeSymmetricSquaredChi__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__addativeSymmetricSquaredChi__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__addativeSymmetricSquaredChi__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__addativeSymmetricSquaredChi__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__addativeSymmetricSquaredChi__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__addativeSymmetricSquaredChi__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__addativeSymmetricSquaredChi__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__addativeSymmetricSquaredChi__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__addativeSymmetricSquaredChi__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__addativeSymmetricSquaredChi__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 1
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Pearson_productMoment_generic:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Pearson_productMoment_generic__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Pearson_productMoment_generic__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Pearson_productMoment_generic__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Pearson_productMoment_generic__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_generic

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_generic__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Pearson_productMoment_generic__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Pearson_productMoment_generic__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Pearson_productMoment_generic__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_generic__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Pearson_productMoment_generic__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Pearson_productMoment_generic__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Pearson_productMoment_generic__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_generic__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Pearson_productMoment_generic__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_generic__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_generic__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_generic__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Pearson_productMoment_generic__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Pearson_productMoment_generic__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Pearson_productMoment_generic__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_generic__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Pearson_productMoment_generic__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_generic__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_generic__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_generic__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Pearson_productMoment_absolute:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Pearson_productMoment_absolute__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Pearson_productMoment_absolute__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Pearson_productMoment_absolute__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Pearson_productMoment_absolute__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_absolute

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_absolute__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Pearson_productMoment_absolute__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Pearson_productMoment_absolute__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Pearson_productMoment_absolute__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_absolute__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Pearson_productMoment_absolute__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Pearson_productMoment_absolute__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Pearson_productMoment_absolute__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_absolute__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Pearson_productMoment_absolute__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_absolute__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_absolute__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_absolute__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Pearson_productMoment_absolute__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Pearson_productMoment_absolute__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Pearson_productMoment_absolute__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_absolute__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Pearson_productMoment_absolute__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_absolute__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_absolute__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_absolute__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Pearson_productMoment_uncentered:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncentered__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Pearson_productMoment_uncentered__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Pearson_productMoment_uncentered__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Pearson_productMoment_uncentered__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_uncentered

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncentered__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Pearson_productMoment_uncentered__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Pearson_productMoment_uncentered__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Pearson_productMoment_uncentered__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncentered__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Pearson_productMoment_uncentered__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Pearson_productMoment_uncentered__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Pearson_productMoment_uncentered__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncentered__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Pearson_productMoment_uncentered__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_uncentered__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_uncentered__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncentered__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Pearson_productMoment_uncentered__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Pearson_productMoment_uncentered__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Pearson_productMoment_uncentered__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncentered__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Pearson_productMoment_uncentered__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_uncentered__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_uncentered__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_uncentered__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 1 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Pearson_productMoment_uncenteredAbsolute:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncenteredAbsolute__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Pearson_productMoment_uncenteredAbsolute__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Pearson_productMoment_uncenteredAbsolute__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Pearson_productMoment_uncenteredAbsolute__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Pearson_productMoment_uncenteredAbsolute

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncenteredAbsolute__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Pearson_productMoment_uncenteredAbsolute__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Pearson_productMoment_uncenteredAbsolute__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Pearson_productMoment_uncenteredAbsolute__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncenteredAbsolute__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Pearson_productMoment_uncenteredAbsolute__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Pearson_productMoment_uncenteredAbsolute__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Pearson_productMoment_uncenteredAbsolute__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncenteredAbsolute__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Pearson_productMoment_uncenteredAbsolute__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_uncenteredAbsolute__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_uncenteredAbsolute__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncenteredAbsolute__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Pearson_productMoment_uncenteredAbsolute__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Pearson_productMoment_uncenteredAbsolute__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Pearson_productMoment_uncenteredAbsolute__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Pearson_productMoment_uncenteredAbsolute__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Pearson_productMoment_uncenteredAbsolute__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Pearson_productMoment_uncenteredAbsolute__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Pearson_productMoment_uncenteredAbsolute__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 2 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__squared__PearsonProductMoment_uncenteredAbsolute__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Kullback_Leibler:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Kullback_Leibler__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Kullback_Leibler__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Kullback_Leibler__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Kullback_Leibler__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Kullback_Leibler

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Kullback_Leibler__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Kullback_Leibler__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Kullback_Leibler__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Kullback_Leibler__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Kullback_Leibler__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Kullback_Leibler__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Kullback_Leibler__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Kullback_Leibler__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Kullback_Leibler__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Kullback_Leibler__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Kullback_Leibler__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Kullback_Leibler__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Kullback_Leibler__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Kullback_Leibler__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Kullback_Leibler__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Kullback_Leibler__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Kullback_Leibler__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Kullback_Leibler__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Kullback_Leibler__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Kullback_Leibler__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__KullbackLeibler //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__KullbackLeibler__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__KullbackLeibler__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Jeffreys:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Jeffreys__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Jeffreys__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Jeffreys__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Jeffreys__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Jeffreys

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Jeffreys__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Jeffreys__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Jeffreys__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Jeffreys__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Jeffreys__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Jeffreys__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Jeffreys__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Jeffreys__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Jeffreys__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Jeffreys__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Jeffreys__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Jeffreys__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Jeffreys__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Jeffreys__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Jeffreys__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Jeffreys__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Jeffreys__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Jeffreys__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Jeffreys__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Jeffreys__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__Jeffreys //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__Jeffreys__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__Jeffreys__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for kDivergence:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__kDivergence__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__kDivergence__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__kDivergence__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__kDivergence__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__kDivergence

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__kDivergence__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__kDivergence__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__kDivergence__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__kDivergence__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__kDivergence__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__kDivergence__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__kDivergence__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__kDivergence__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__kDivergence__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__kDivergence__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__kDivergence__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__kDivergence__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__kDivergence__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__kDivergence__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__kDivergence__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__kDivergence__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__kDivergence__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__kDivergence__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__kDivergence__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__kDivergence__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__kDivergence //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__kDivergence__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__kDivergence__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Topsoee:
//! @remarks A different name for the 'Topsoee' metric is 'information theory'. 

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Topsoee__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Topsoee__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Topsoee__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Topsoee__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Topsoee

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Topsoee__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Topsoee__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Topsoee__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Topsoee__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Topsoee__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Topsoee__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Topsoee__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Topsoee__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Topsoee__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Topsoee__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Topsoee__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Topsoee__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Topsoee__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Topsoee__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Topsoee__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Topsoee__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Topsoee__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Topsoee__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Topsoee__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Topsoee__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__Topsoee //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__Topsoee__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__Topsoee__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for JensenShannon:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__JensenShannon__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__JensenShannon__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__JensenShannon__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__JensenShannon__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__JensenShannon

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__JensenShannon__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__JensenShannon__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__JensenShannon__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__JensenShannon__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__JensenShannon__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__JensenShannon__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__JensenShannon__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__JensenShannon__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__JensenShannon__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__JensenShannon__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__JensenShannon__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__JensenShannon__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__JensenShannon__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__JensenShannon__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__JensenShannon__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__JensenShannon__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__JensenShannon__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__JensenShannon__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__JensenShannon__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__JensenShannon__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__JensenShannon //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__JensenShannon__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__JensenShannon__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for JensenDifference:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__JensenDifference__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__JensenDifference__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__JensenDifference__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__JensenDifference__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__JensenDifference

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__JensenDifference__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__JensenDifference__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__JensenDifference__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__JensenDifference__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__JensenDifference__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__JensenDifference__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__JensenDifference__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__JensenDifference__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__JensenDifference__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__JensenDifference__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__JensenDifference__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__JensenDifference__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__JensenDifference__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__JensenDifference__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__JensenDifference__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__JensenDifference__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__JensenDifference__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__JensenDifference__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__JensenDifference__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__JensenDifference__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__shannon__JensenDifference //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__shannon__JensenDifference__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__shannon__JensenDifference__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for Taneja:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__Taneja__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__Taneja__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__Taneja__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__Taneja__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__Taneja

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__Taneja__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__Taneja__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__Taneja__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__Taneja__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__Taneja__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__Taneja__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__Taneja__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__Taneja__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__Taneja__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__Taneja__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__Taneja__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__Taneja__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__Taneja__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__Taneja__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__Taneja__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__Taneja__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__Taneja__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__Taneja__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__Taneja__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__Taneja__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__combination__Taneja //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__combination__Taneja__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__combination__Taneja__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for KumarJohnson:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__KumarJohnson__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__KumarJohnson__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__KumarJohnson__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__KumarJohnson__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__KumarJohnson

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__KumarJohnson__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__KumarJohnson__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__KumarJohnson__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__KumarJohnson__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__KumarJohnson__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__KumarJohnson__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__KumarJohnson__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__KumarJohnson__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__KumarJohnson__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__KumarJohnson__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__KumarJohnson__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__KumarJohnson__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__KumarJohnson__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__KumarJohnson__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__KumarJohnson__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__KumarJohnson__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__KumarJohnson__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__KumarJohnson__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__KumarJohnson__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__KumarJohnson__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__combination__KumarJohnson //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__combination__KumarJohnson__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__combination__KumarJohnson__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for averageOfChebyshevAndCityblock:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__averageOfChebyshevAndCityblock__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__averageOfChebyshevAndCityblock__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__averageOfChebyshevAndCityblock__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__averageOfChebyshevAndCityblock__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__averageOfChebyshevAndCityblock

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__averageOfChebyshevAndCityblock__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__averageOfChebyshevAndCityblock__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__averageOfChebyshevAndCityblock__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__averageOfChebyshevAndCityblock__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__averageOfChebyshevAndCityblock__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__averageOfChebyshevAndCityblock__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__averageOfChebyshevAndCityblock__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__averageOfChebyshevAndCityblock__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__averageOfChebyshevAndCityblock__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__averageOfChebyshevAndCityblock__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__averageOfChebyshevAndCityblock__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__averageOfChebyshevAndCityblock__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__averageOfChebyshevAndCityblock__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__averageOfChebyshevAndCityblock__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__averageOfChebyshevAndCityblock__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__averageOfChebyshevAndCityblock__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__averageOfChebyshevAndCityblock__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__averageOfChebyshevAndCityblock__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__averageOfChebyshevAndCityblock__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__averageOfChebyshevAndCityblock__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for VicisWaveHedges:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__VicisWaveHedges__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__VicisWaveHedges__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__VicisWaveHedges__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__VicisWaveHedges__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__VicisWaveHedges

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedges__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__VicisWaveHedges__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__VicisWaveHedges__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__VicisWaveHedges__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__VicisWaveHedges__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__VicisWaveHedges__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__VicisWaveHedges__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__VicisWaveHedges__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedges__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__VicisWaveHedges__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__VicisWaveHedges__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__VicisWaveHedges__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__VicisWaveHedges__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__VicisWaveHedges__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__VicisWaveHedges__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__VicisWaveHedges__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedges__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__VicisWaveHedges__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__VicisWaveHedges__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__VicisWaveHedges__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for VicisWaveHedgesMax:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMax__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__VicisWaveHedgesMax__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__VicisWaveHedgesMax__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__VicisWaveHedgesMax__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__VicisWaveHedgesMax

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMax__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__VicisWaveHedgesMax__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__VicisWaveHedgesMax__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__VicisWaveHedgesMax__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMax__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__VicisWaveHedgesMax__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__VicisWaveHedgesMax__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__VicisWaveHedgesMax__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMax__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__VicisWaveHedgesMax__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__VicisWaveHedgesMax__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__VicisWaveHedgesMax__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMax__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__VicisWaveHedgesMax__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__VicisWaveHedgesMax__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__VicisWaveHedgesMax__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMax__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__VicisWaveHedgesMax__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__VicisWaveHedgesMax__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__VicisWaveHedgesMax__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for VicisWaveHedgesMin:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMin__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__VicisWaveHedgesMin__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__VicisWaveHedgesMin__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__VicisWaveHedgesMin__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__VicisWaveHedgesMin

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMin__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__VicisWaveHedgesMin__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__VicisWaveHedgesMin__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__VicisWaveHedgesMin__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMin__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__VicisWaveHedgesMin__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__VicisWaveHedgesMin__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__VicisWaveHedgesMin__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMin__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__VicisWaveHedgesMin__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__VicisWaveHedgesMin__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__VicisWaveHedgesMin__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMin__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__VicisWaveHedgesMin__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__VicisWaveHedgesMin__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__VicisWaveHedgesMin__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__VicisWaveHedgesMin__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__VicisWaveHedgesMin__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__VicisWaveHedgesMin__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__VicisWaveHedgesMin__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for VicisSymmetric:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__VicisSymmetric__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__VicisSymmetric__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__VicisSymmetric__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__VicisSymmetric__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__VicisSymmetric

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__VicisSymmetric__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__VicisSymmetric__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__VicisSymmetric__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__VicisSymmetric__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__VicisSymmetric__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__VicisSymmetric__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__VicisSymmetric__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__VicisSymmetric__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__VicisSymmetric__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__VicisSymmetric__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__VicisSymmetric__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__VicisSymmetric__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__VicisSymmetric__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__VicisSymmetric__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__VicisSymmetric__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__VicisSymmetric__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__VicisSymmetric__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__VicisSymmetric__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__VicisSymmetric__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__VicisSymmetric__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for VicisSymmetricMax:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__VicisSymmetricMax__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__VicisSymmetricMax__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__VicisSymmetricMax__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__VicisSymmetricMax__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__VicisSymmetricMax

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__VicisSymmetricMax__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__VicisSymmetricMax__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__VicisSymmetricMax__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__VicisSymmetricMax__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__VicisSymmetricMax__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__VicisSymmetricMax__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__VicisSymmetricMax__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__VicisSymmetricMax__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__VicisSymmetricMax__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__VicisSymmetricMax__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__VicisSymmetricMax__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__VicisSymmetricMax__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__VicisSymmetricMax__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__VicisSymmetricMax__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__VicisSymmetricMax__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__VicisSymmetricMax__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__VicisSymmetricMax__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__VicisSymmetricMax__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__VicisSymmetricMax__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__VicisSymmetricMax__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for downwardMutability_symmetricMax:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMax__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__downwardMutability_symmetricMax__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__downwardMutability_symmetricMax__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__downwardMutability_symmetricMax__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__downwardMutability_symmetricMax

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMax__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__downwardMutability_symmetricMax__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__downwardMutability_symmetricMax__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__downwardMutability_symmetricMax__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMax__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__downwardMutability_symmetricMax__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__downwardMutability_symmetricMax__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__downwardMutability_symmetricMax__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMax__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__downwardMutability_symmetricMax__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__downwardMutability_symmetricMax__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__downwardMutability_symmetricMax__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMax__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__downwardMutability_symmetricMax__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__downwardMutability_symmetricMax__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__downwardMutability_symmetricMax__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMax__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__downwardMutability_symmetricMax__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__downwardMutability_symmetricMax__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__downwardMutability_symmetricMax__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__symmetricMax__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------

//!
//! Build for downwardMutability_symmetricMin:
//! @remarks  

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMin__notMask
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_each__downwardMutability_symmetricMin__notMask 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_oneToMany__downwardMutability_symmetricMin__notMask 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask kt_computeForMetric_manyToMany__downwardMutability_symmetricMin__notMask
 #define TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC kt_computeForMetric_postProcessTemporalCorrelationScores__downwardMutability_symmetricMin

//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMin__notMask__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_each__downwardMutability_symmetricMin__notMask__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_oneToMany__downwardMutability_symmetricMin__notMask__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight kt_computeForMetric_manyToMany__downwardMutability_symmetricMin__notMask__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMin__mask_explicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_each__downwardMutability_symmetricMin__mask_explicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_oneToMany__downwardMutability_symmetricMin__mask_explicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit kt_computeForMetric_manyToMany__downwardMutability_symmetricMin__mask_explicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMin__mask_explicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_each__downwardMutability_symmetricMin__mask_explicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_oneToMany__downwardMutability_symmetricMin__mask_explicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight kt_computeForMetric_manyToMany__downwardMutability_symmetricMin__mask_explicit__weight
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMin__mask_implicit
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_each__downwardMutability_symmetricMin__mask_implicit 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_oneToMany__downwardMutability_symmetricMin__mask_implicit 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit kt_computeForMetric_manyToMany__downwardMutability_symmetricMin__mask_implicit
//! ---

//! ---
#define TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeTile_xmtPostPostCompute__downwardMutability_symmetricMin__mask_implicit__weight
#define TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_each__downwardMutability_symmetricMin__mask_implicit__weight 
#define TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_oneToMany__downwardMutability_symmetricMin__mask_implicit__weight 
#define TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight kt_computeForMetric_manyToMany__downwardMutability_symmetricMin__mask_implicit__weight
//! ---

#define TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax //! eg, "correlation_macros__distanceMeasures__euclid_getRetVal" for Euclid
//! ---
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax__SSE //! eg, "correlation_macros__distanceMeasures__euclid__SSE" for the SSE-version of the "Euclid" macro.
#define TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument 0  //! ie, as we do Not expect an explciti 'power' argument to be used in the distance-comptuation.
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin 0 //! ie, as we are itnerested in the sum of the resutls, eg, in contrast to "Chebychev"
#define TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate 1 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator */  //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction correlation_macros__distanceMeasures__downwardMutability__symmetricMin__postProcess //! eg, 0 /* ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar */ //! ie, update a result-able, and not a 'complex table with muliple lists/tables'.
#define TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices 0 //! which if set is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
#define TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores 0
//! Apply [above] specificaiotns:
#include "template_correlation_tile_wrapper.cxx"
//! -------------------------------------------------------------------------------------------------------------------
