printf(

"\n# Metrics in the correlation-group of \"shannon\":\n"
"\"e_kt_correlationFunction_groupOf_shannon_KullbackLeibler\"\t description: KullbackLeibler;\n"
"\"e_kt_correlationFunction_groupOf_shannon_Jeffreys\"\t description: Jeffreys;\n"
"\"e_kt_correlationFunction_groupOf_shannon_kDivergence\"\t description: kDivergence;\n"
"\"e_kt_correlationFunction_groupOf_shannon_Topsoee\"\t description: Topsoee;\n"
"\"e_kt_correlationFunction_groupOf_shannon_JensenShannon\"\t description: JensenShannon;\n"
"\"e_kt_correlationFunction_groupOf_shannon_JensenDifference\"\t description: JensenDifference;\n"


"\n# Metrics in the correlation-group of \"absoluteDifference\":\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen\"\t description: Sorensen;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_Gower\"\t description: Gower;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_Soergel\"\t description: Soergel;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski\"\t description: Kulczynski;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_Canberra\"\t description: Canberra;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian\"\t description: Lorentzian;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean\"\t description: oekseth forMasks mean;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable\"\t description: oekseth forMasks standardDeviation pow variable;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne\"\t description: oekseth forMasks standardDeviation pow variable zeroOne;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2\"\t description: oekseth forMasks standardDeviation pow2;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3\"\t description: oekseth forMasks standardDeviation pow3;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4\"\t description: oekseth forMasks standardDeviation pow4;\n"
"\"e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5\"\t description: oekseth forMasks standardDeviation pow5;\n"


"\n# Metrics in the correlation-group of \"combinations\":\n"
"\"e_kt_correlationFunction_groupOf_combinations_Taneja\"\t description: Taneja;\n"
"\"e_kt_correlationFunction_groupOf_combinations_KumarJohnson\"\t description: KumarJohnson;\n"
"\"e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock\"\t description: averageOfChebyshevAndCityblock;\n"


"\n# Metrics in the correlation-group of \"fidelity\":\n"
"\"e_kt_correlationFunction_groupOf_fidelity_fidelity\"\t description: fidelity;\n"
"\"e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya\"\t description: Bhattacharyya;\n"
"\"e_kt_correlationFunction_groupOf_fidelity_Hellinger\"\t description: Hellinger;\n"
"\"e_kt_correlationFunction_groupOf_fidelity_Matusita\"\t description: Matusita;\n"
"\"e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef\"\t description: Hellinger (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef\"\t description: Matusita (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_fidelity_squaredChord\"\t description: squaredChord;\n"
"\"e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef\"\t description: squaredChord (alternative formulation);\n"


"\n# Metrics in the correlation-group of \"rank_kendall\":\n"
"\"e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine\"\t description: kendall sampleCorrelation or BrownianCoVariance or Cosine;\n"
"\"e_kt_correlationFunction_groupOf_rank_kendall_coVariance\"\t description: kendall coVariance;\n"
"\"e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock\"\t description: kendall KumarHassebrock;\n"
"\"e_kt_correlationFunction_groupOf_rank_kendall_Dice\"\t description: kendall Dice;\n"
"\"e_kt_correlationFunction_groupOf_rank_kendall_Goodman\"\t description: kendall Goodman;\n"
"\"e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized\"\t description: kendall coVariance normalized;\n"


"\n# Metrics in the correlation-group of \"innerProduct\":\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_innerProduct\"\t description: innerProduct;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_harmonicMean\"\t description: harmonicMean;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_cosine\"\t description: cosine;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook\"\t description: Jaccard or KumarHassebrook;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_Dice\"\t description: Dice;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef\"\t description: Jaccard (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef\"\t description: Dice (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance\"\t description: sampleCoVariance;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian\"\t description: sampleCorrelation or brownian;\n"
"\"e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator\"\t description: sampleCorrelation or brownian--innerRootInDenumerator;\n"


"\n# Metrics in the correlation-group of \"squared\":\n"
"\"e_kt_correlationFunction_groupOf_squared_Euclid\"\t description: Euclid;\n"
"\"e_kt_correlationFunction_groupOf_squared_Pearson\"\t description: Pearson;\n"
"\"e_kt_correlationFunction_groupOf_squared_Neyman\"\t description: Neyman;\n"
"\"e_kt_correlationFunction_groupOf_squared_squaredChi\"\t description: squaredChi;\n"
"\"e_kt_correlationFunction_groupOf_squared_probabilisticChi\"\t description: probabilisticChi;\n"
"\"e_kt_correlationFunction_groupOf_squared_divergence\"\t description: divergence;\n"
"\"e_kt_correlationFunction_groupOf_squared_Clark\"\t description: Clark;\n"
"\"e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi\"\t description: addativeSymmetricSquaredChi;\n"
"\"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic\"\t description: Pearson productMoment generic;\n"
"\"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute\"\t description: Pearson productMoment absolute;\n"
"\"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered\"\t description: Pearson productMoment uncentered;\n"
"\"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute\"\t description: Pearson productMoment uncenteredAbsolute;\n"


"\n# Metrics in the correlation-group of \"directScore\":\n"
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
"\"e_kt_correlationFunction_groupOf_directScore_direct_avg\"\t description: direct avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_sum\"\t description: direct sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_minus\"\t description: direct minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_abs_minus\"\t description: direct abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_sq_sum\"\t description: direct sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_sq_minus\"\t description: direct sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus\"\t description: direct sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus\"\t description: direct divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus\"\t description: direct divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_mul\"\t description: direct mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator\"\t description: direct div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator\"\t description: direct div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator\"\t description: direct maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator\"\t description: direct minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_min\"\t description: direct min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_max\"\t description: direct max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_useScore_1\"\t description: direct useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_useScore_2\"\t description: direct useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_abs\"\t description: direct abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_log_abs\"\t description: direct log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_2log_abs\"\t description: direct 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs\"\t description: direct sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus\"\t description: direct sqrt abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg\"\t description: mergeMatrices--sum avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum\"\t description: mergeMatrices--sum sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus\"\t description: mergeMatrices--sum minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus\"\t description: mergeMatrices--sum abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum\"\t description: mergeMatrices--sum sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus\"\t description: mergeMatrices--sum sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus\"\t description: mergeMatrices--sum sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus\"\t description: mergeMatrices--sum divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus\"\t description: mergeMatrices--sum divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul\"\t description: mergeMatrices--sum mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator\"\t description: mergeMatrices--sum div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator\"\t description: mergeMatrices--sum div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator\"\t description: mergeMatrices--sum maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator\"\t description: mergeMatrices--sum minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min\"\t description: mergeMatrices--sum min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max\"\t description: mergeMatrices--sum max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1\"\t description: mergeMatrices--sum useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2\"\t description: mergeMatrices--sum useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs\"\t description: mergeMatrices--sum abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs\"\t description: mergeMatrices--sum log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs\"\t description: mergeMatrices--sum 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs\"\t description: mergeMatrices--sum sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus\"\t description: mergeMatrices--sum sqrt abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg\"\t description: mergeMatrices--mul avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum\"\t description: mergeMatrices--mul sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus\"\t description: mergeMatrices--mul minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus\"\t description: mergeMatrices--mul abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum\"\t description: mergeMatrices--mul sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus\"\t description: mergeMatrices--mul sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus\"\t description: mergeMatrices--mul sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus\"\t description: mergeMatrices--mul divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus\"\t description: mergeMatrices--mul divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul\"\t description: mergeMatrices--mul mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator\"\t description: mergeMatrices--mul div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator\"\t description: mergeMatrices--mul div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator\"\t description: mergeMatrices--mul maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator\"\t description: mergeMatrices--mul minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min\"\t description: mergeMatrices--mul min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max\"\t description: mergeMatrices--mul max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1\"\t description: mergeMatrices--mul useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2\"\t description: mergeMatrices--mul useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs\"\t description: mergeMatrices--mul abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs\"\t description: mergeMatrices--mul log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs\"\t description: mergeMatrices--mul 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs\"\t description: mergeMatrices--mul sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus\"\t description: mergeMatrices--mul sqrt abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg\"\t description: mergeMatrices--avg avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum\"\t description: mergeMatrices--avg sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus\"\t description: mergeMatrices--avg minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus\"\t description: mergeMatrices--avg abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum\"\t description: mergeMatrices--avg sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus\"\t description: mergeMatrices--avg sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus\"\t description: mergeMatrices--avg sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus\"\t description: mergeMatrices--avg divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus\"\t description: mergeMatrices--avg divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul\"\t description: mergeMatrices--avg mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator\"\t description: mergeMatrices--avg div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator\"\t description: mergeMatrices--avg div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator\"\t description: mergeMatrices--avg maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator\"\t description: mergeMatrices--avg minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min\"\t description: mergeMatrices--avg min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max\"\t description: mergeMatrices--avg max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1\"\t description: mergeMatrices--avg useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2\"\t description: mergeMatrices--avg useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs\"\t description: mergeMatrices--avg abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs\"\t description: mergeMatrices--avg log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs\"\t description: mergeMatrices--avg 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs\"\t description: mergeMatrices--avg sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus\"\t description: mergeMatrices--avg sqrt abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg\"\t description: symmetricProperty--sum avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum\"\t description: symmetricProperty--sum sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus\"\t description: symmetricProperty--sum minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus\"\t description: symmetricProperty--sum abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum\"\t description: symmetricProperty--sum sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus\"\t description: symmetricProperty--sum sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus\"\t description: symmetricProperty--sum sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus\"\t description: symmetricProperty--sum divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus\"\t description: symmetricProperty--sum divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul\"\t description: symmetricProperty--sum mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator\"\t description: symmetricProperty--sum div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator\"\t description: symmetricProperty--sum div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator\"\t description: symmetricProperty--sum maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator\"\t description: symmetricProperty--sum minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min\"\t description: symmetricProperty--sum min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max\"\t description: symmetricProperty--sum max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1\"\t description: symmetricProperty--sum useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2\"\t description: symmetricProperty--sum useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs\"\t description: symmetricProperty--sum abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs\"\t description: symmetricProperty--sum log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs\"\t description: symmetricProperty--sum 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs\"\t description: symmetricProperty--sum sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus\"\t description: symmetricProperty--sum sqrt abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg\"\t description: symmetricProperty--mul avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum\"\t description: symmetricProperty--mul sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus\"\t description: symmetricProperty--mul minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus\"\t description: symmetricProperty--mul abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum\"\t description: symmetricProperty--mul sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus\"\t description: symmetricProperty--mul sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus\"\t description: symmetricProperty--mul sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus\"\t description: symmetricProperty--mul divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus\"\t description: symmetricProperty--mul divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul\"\t description: symmetricProperty--mul mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator\"\t description: symmetricProperty--mul div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator\"\t description: symmetricProperty--mul div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator\"\t description: symmetricProperty--mul maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator\"\t description: symmetricProperty--mul minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min\"\t description: symmetricProperty--mul min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max\"\t description: symmetricProperty--mul max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1\"\t description: symmetricProperty--mul useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2\"\t description: symmetricProperty--mul useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs\"\t description: symmetricProperty--mul abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs\"\t description: symmetricProperty--mul log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs\"\t description: symmetricProperty--mul 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs\"\t description: symmetricProperty--mul sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus\"\t description: symmetricProperty--mul sqrt abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg\"\t description: symmetricProperty--avg avg;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum\"\t description: symmetricProperty--avg sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus\"\t description: symmetricProperty--avg minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus\"\t description: symmetricProperty--avg abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum\"\t description: symmetricProperty--avg sq sum;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus\"\t description: symmetricProperty--avg sq minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus\"\t description: symmetricProperty--avg sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus\"\t description: symmetricProperty--avg divMax sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus\"\t description: symmetricProperty--avg divAbsMinus sq abs minus;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul\"\t description: symmetricProperty--avg mul;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator\"\t description: symmetricProperty--avg div headIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator\"\t description: symmetricProperty--avg div tailIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator\"\t description: symmetricProperty--avg maxIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator\"\t description: symmetricProperty--avg minIsNumerator;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min\"\t description: symmetricProperty--avg min;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max\"\t description: symmetricProperty--avg max;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1\"\t description: symmetricProperty--avg useScore 1;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2\"\t description: symmetricProperty--avg useScore 2;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs\"\t description: symmetricProperty--avg abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs\"\t description: symmetricProperty--avg log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs\"\t description: symmetricProperty--avg 2log abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs\"\t description: symmetricProperty--avg sqrt abs;\n"
"\"e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus\"\t description: symmetricProperty--avg sqrt abs minus;\n"

#endif

"\n# Metrics in the correlation-group of \"minkowski\":\n"
"\"e_kt_correlationFunction_groupOf_minkowski_euclid\"\t description: euclid;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_cityblock\"\t description: cityblock;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_minkowski\"\t description: minkowski;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne\"\t description: minkowski--subCase--zeroOne;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3\"\t description: minkowski--subCase--pow3;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4\"\t description: minkowski--subCase--pow4;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5\"\t description: minkowski--subCase--pow5;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_chebychev\"\t description: chebychev;\n"
"\"e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max\"\t description: chebychev minInsteadOf max;\n"


"\n# Metrics in the correlation-group of \"MINE\":\n"
"\"e_kt_correlationFunction_groupOf_MINE_mic\"\t description: mic;\n"
"\"e_kt_correlationFunction_groupOf_MINE_mas\"\t description: mas;\n"
"\"e_kt_correlationFunction_groupOf_MINE_mev\"\t description: mev;\n"
"\"e_kt_correlationFunction_groupOf_MINE_mcn\"\t description: mcn;\n"
"\"e_kt_correlationFunction_groupOf_MINE_mcn_general\"\t description: mcn general;\n"
"\"e_kt_correlationFunction_groupOf_MINE_gmic\"\t description: gmic;\n"
"\"e_kt_correlationFunction_groupOf_MINE_tic\"\t description: tic;\n"


"\n# Metrics in the correlation-group of \"intersection\":\n"
"\"e_kt_correlationFunction_groupOf_intersection_intersection\"\t description: intersection;\n"
"\"e_kt_correlationFunction_groupOf_intersection_intersection_altDef\"\t description: intersection (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_intersection_WaveHedges\"\t description: WaveHedges;\n"
"\"e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt\"\t description: WaveHedges alt;\n"
"\"e_kt_correlationFunction_groupOf_intersection_Czekanowski\"\t description: Czekanowski;\n"
"\"e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef\"\t description: Czekanowski (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_intersection_Motyka\"\t description: Motyka;\n"
"\"e_kt_correlationFunction_groupOf_intersection_Motyka_altDef\"\t description: Motyka (alternative formulation);\n"
"\"e_kt_correlationFunction_groupOf_intersection_Kulczynski\"\t description: Kulczynski;\n"
"\"e_kt_correlationFunction_groupOf_intersection_Ruzicka\"\t description: Ruzicka;\n"
"\"e_kt_correlationFunction_groupOf_intersection_Tanimoto\"\t description: Tanimoto;\n"
"\"e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef\"\t description: Tanimoto (alternative formulation);\n"


"\n# Metrics in the correlation-group of \"downwardMutability\":\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges\"\t description: VicisWaveHedges;\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax\"\t description: VicisWaveHedgesMax;\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin\"\t description: VicisWaveHedgesMin;\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric\"\t description: VicisSymmetric;\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax\"\t description: VicisSymmetricMax;\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax\"\t description: symmetricMax;\n"
"\"e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin\"\t description: symmetricMin;\n"

);