from __future__ import print_function
import numpy as np
import time
from sklearn.metrics.pairwise import manhattan_distances
from sklearn.metrics import euclidean_distances #! which we use for simliarty-metrics
#from sklearn.metrics import cosine_similarity
from sklearn.metrics.pairwise import paired_distances
from scipy.spatial.distance import cosine, cityblock, minkowski, wminkowski
from sklearn.metrics.pairwise import paired_distances
from sklearn.metrics.pairwise import pairwise_distances
import sys

#! A genelrized funciton to symplify the 'dump' of values: 
def eprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    print(*args, file=sys.stderr, **kwargs)


#from sklearn.metrics.pairwise import manhattan_distances
#from sklearn.metrics import cosine_distances #! which we use for simliarty-metrics

#! Src: "http://stackoverflow.com/questions/10580676/comparing-two-numpy-arrays-for-equality-element-wise" combined with work by (oesketh, 06. feb. 2017).
exec_time0 = []
exec_time1 = []
exec_time2 = []
exec_time_pyIter_sum_plus = []
exec_time_pyIter_sum_mul = []
exec_time_pyIter_sum_mul_xmtMem = []
exec_time_pyIter_sum_plus_mask = []
exec_time_pyIter_sum_mul_mask = []
exec_time_pyIter_sum_mul_mask_andFuncCall = []
exec_time_pyIter_sum_mul_mask_andFuncCall_alsoInside = []
exec_time_pyIter_sim__Euclid = []
exec_time_pyIter_sim__Manhatten = []
exec_time_pyIter_sim__paired = []
exec_time_pyIter_sim__cosine = []



"""
        - from scikit-learn: ['cityblock', 'cosine', 'euclidean', 'l1', 'l2',
          'manhattan']

        - from scipy.spatial.distance: ['braycurtis', 'canberra', 'chebyshev',
          'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski',
          'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto',
          'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath',
          'sqeuclidean', 'yule']
"""
exec_time_pyIter_sim__sciPySpatial__cosine = []
exec_time_pyIter_sim__sciPySpatial__minkowski = []

currentPos = 0

def __getFor_pair(score1, score2):
    if( (score1 != None) and (score2 != None) ): #! then a 'mask' is Not set.
        return (score1 * score2)
    return 0

def __compute_row_seperateMetricCall(ncols, row_1, row_2):
    sum = 0
    for i in xrange(ncols):
        sum += __getFor_pair(row_1[i], row_2[i])
    return sum

def __compute_row(ncols, row_1, row_2):
    sum = 0
    for i in xrange(ncols):
        if( (row_1[i] != None) and (row_2[i] != None) ): #! then a 'mask' is Not set.
            sum += (row_1[i] * row_2[i])
    return sum


def compute_matrix(nrows, ncols):    
    global currentPos
#! -------------------------------------------------------
    sizeOfArray = nrows
#sizeOfArray = 340
#sizeOfArray = 128*70
#sizeOfArray = 5000
    sizeOfArray_out = ncols #2*128
    numOfIterations = 1
#numOfIterations = 200

    globalConfig__2dMatrix = False
    globalConfig__evaluyateNaive = True
    #!
    #! Iterate:
    for i in xrange(numOfIterations):
        #print("start radnom iteration at i=", i, " / ", numOfIterations, "\n")
    #A = np.random.uniform(0,255,[sizeOfArray,sizeOfArray_out])
    #B = np.random.uniform(0,255,[sizeOfArray,sizeOfArray_out])
        A = np.random.rand(nrows, ncols)
        # print("val=", A[0][0])
        #A = np.random.randint(0,255,(nrows, ncols))
        B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
        #B = np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
        
        a = time.clock() 
        res = (A==B).all()
        b = time.clock()
        exec_time0.append( b - a )

        a = time.clock() 
        res = np.array_equal(A,B)
        b = time.clock()
        exec_time1.append( b - a )

        a = time.clock() 
        res = np.array_equiv(A,B)
        b = time.clock()
        exec_time2.append( b - a )
    
    #! -------------------------- start: tests by (oesketh, 06. feb. 2017):
    #! 
    #! Convert to Python-list:
        A_pyStruct = A.tolist()
        B_pyStruct = B.tolist()
        if(globalConfig__evaluyateNaive == True):
    #! -------------------------
    #! 
    #! New test:
            a = time.clock() 
            sum = 0
    #print "len(A)=", len(A_pyStruct)
    #print "len(A[0])=", len(A_pyStruct[0])
            for k in xrange(nrows):
                for l in xrange(nrows):
                    for i in xrange(ncols):
                        sum = sum + A_pyStruct[l][i] + B_pyStruct[k][i]
            b = time.clock()
            exec_time_pyIter_sum_plus.append( b - a )
    #! -------------------------
    #! 
    #! New test:
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    for i in xrange(ncols):
                        sum += (A_pyStruct[l][i] * B_pyStruct[k][i])
            b = time.clock()
            exec_time_pyIter_sum_mul.append( b - a )
    #! -------------------------
    #! 
    #! New test:
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    for i in xrange(ncols):
                        sum += (k * l)
            b = time.clock()
            exec_time_pyIter_sum_mul_xmtMem.append( b - a )
    #! -------------------------
    #! 
    #! New test:
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    for i in xrange(ncols):
                        if( (A_pyStruct[k][i] != None) and (B_pyStruct[l][i] != None) ): #! then a 'mask' is Not set.
                            sum += (A_pyStruct[k][i] + B_pyStruct[l][i])
            b = time.clock()
            exec_time_pyIter_sum_plus_mask.append( b - a )
            # #! -------------------------
            # #! 
            #! New test:
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    for i in xrange(ncols):
                        if( (A_pyStruct[l][i] != None) and (B_pyStruct[k][i] != None) ): #! then a 'mask' is Not set.
                            sum += (A_pyStruct[l][i] * B_pyStruct[k][i])
            b = time.clock()
            exec_time_pyIter_sum_mul_mask.append( b - a )
            # #! -------------------------
            # #! 
            #! New test:
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    sum += __compute_row(ncols, A_pyStruct[k], B_pyStruct[l])
            b = time.clock()
            exec_time_pyIter_sum_mul_mask_andFuncCall.append( b - a )
            # #! -------------------------
            # #! 
            #! New test:
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    sum += __compute_row_seperateMetricCall(ncols, A_pyStruct[k], B_pyStruct[l])
            b = time.clock()
            exec_time_pyIter_sum_mul_mask_andFuncCall_alsoInside.append( b - a )
    
    #! -------------------------
    #! 
    #! New test: Eculid(itnerna):
        a = time.clock() 
        closest_dist_sq = euclidean_distances(A_pyStruct, B_pyStruct)
        #print("dim.rows=", len(A_pyStruct), len(A_pyStruct[0]))
    #closest_dist_sq = euclidean_distances(A_pyStruct[0], B_pyStruct[0])
        b = time.clock()
        exec_time_pyIter_sim__Euclid.append( b - a )
    #! -------------------------
    #! 
    #! New test: Cosine(itnerna):
        a = time.clock() 
    #sklearn.metrics.pairwise.cosine_distances(X, Y=None)[source]
    #closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric="cosine") #! ie, "scipy.spatial"
        closest_dist_sq = manhattan_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = sklearn.metrics.pairwise.cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = euclidean_distances(A_pyStruct[0], B_pyStruct[0])
        b = time.clock()
        exec_time_pyIter_sim__Manhatten.append( b - a )
    #! -------------------------
    #! 
    #! New test: Cosine(itnerna):
        a = time.clock() 
    #sklearn.metrics.pairwise.cosine_distances(X, Y=None)[source]
    #closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric=cosine) #! ie, "scipy.spatial"
#        closest_dist_sq = paired_distances(A_pyStruct, B_pyStruct, metric="euclidean", n_jobs=1)
        closest_dist_sq = paired_distances(A_pyStruct, B_pyStruct, metric="cityblock", n_jobs=1)
    #closest_dist_sq = sklearn.metrics.pairwise.cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = euclidean_distances(A_pyStruct[0], B_pyStruct[0])
        b = time.clock()
        exec_time_pyIter_sim__paired.append( b - a )


    #! -------------------------
    #! 
    #! New test: Eculid(itnerna):
        a = time.clock() 
        closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric="cosine", n_jobs=1) #! ie, "scipy.spatial"
        b = time.clock()
        exec_time_pyIter_sim__cosine.append( b - a )
    #! -------------------------
    #! 
    #! New test: Cosine(itnerna):
        a = time.clock() 
    #sklearn.metrics.pairwise.cosine_distances(X, Y=None)[source]
    #closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric="cosine") #! ie, "scipy.spatial"
        closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric=cosine) #! ie, "scipy.spatial"
    #closest_dist_sq = sklearn.metrics.pairwise.cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = euclidean_distances(A_pyStruct[0], B_pyStruct[0])
        b = time.clock()
        exec_time_pyIter_sim__sciPySpatial__cosine.append( b - a )
    #! -------------------------
    #! 
    #! New test: Cosine(itnerna):
        a = time.clock() 
    #sklearn.metrics.pairwise.cosine_distances(X, Y=None)[source]
    #closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric=cosine) #! ie, "scipy.spatial"
    #closest_dist_sq = paired_distances(A_pyStruct, B_pyStruct)
        kwds = {"p": 2.0}
        closest_dist_sq = pairwise_distances(A_pyStruct, B_pyStruct, metric=minkowski, **kwds) #! ie, "scipy.spatial"
    #closest_dist_sq = sklearn.metrics.pairwise.cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = cosine_distances(A_pyStruct, B_pyStruct)
    #closest_dist_sq = euclidean_distances(A_pyStruct[0], B_pyStruct[0])
        b = time.clock()
        exec_time_pyIter_sim__sciPySpatial__minkowski.append( b - a )


    if(True): #! then result is generated using a TAB-centered-matrix-output:
        #print("test-2") 
        if(currentPos == 0): #! then we write out the row-header:
            eprint("matrix[", nrows, ",", ncols, "]", sep="", end="\t")
            #! -------------
            if(globalConfig__2dMatrix == True):
                eprint("(A==B).all()", sep="\t", end="\t")
                eprint("np.array_equal(A,B)", sep="\t", end="\t")
                eprint("np.array_equiv(A,B)", sep="\t", end="\t")
            if(globalConfig__evaluyateNaive == True):
                eprint("python:iter:sum_plus", sep="\t", end="\t") #! 1
                eprint("python:iter:sum_mul", sep="\t", end="\t") #! 2
                eprint("python:iter:sum_mul_xmtMem", sep="\t", end="\t") #! 2
                eprint("python:iter:sum_plus_mask", sep="\t", end="\t") #! 3
                eprint("python:iter:sum_mul_mask", sep="\t", end="\t") #! 4
                eprint("python:iter:sum_mul_mask-feature-function", sep="\t", end="\t") #! 4
                eprint("python:iter:sum_mul_mask-feature-function-andPairFunction", sep="\t", end="\t") #! 4
    #! -------------------
            eprint("python:iter:sim__Euclid", sep="\t", end="\t") #! 5
            eprint("python:iter:sim__Manhatten", sep="\t", end="\t") #! 6
            eprint("python:iter:sim__paired", sep="\t", end="\t") #! 7
            eprint("python:iter:sim__cosine", sep="\t", end="\t") #! 8
            eprint("python:iter:sim__sciPySpatial__cosine", sep="\t", end="\t") #! 9
            eprint("python:iter:sim__sciPySpatial__minkowski", sep="\t", end="\t") #! 10
    #! -------------------
            eprint("") #! ie, anewline.
    #! ------------------------------------------------------------------------
    #!
    #! Write out the values:         
        eprint("matrix[", nrows, ",", ncols, "]", sep='', end="\t")
        #! -------------
        if(globalConfig__2dMatrix == True):
            eprint(np.mean(exec_time0), sep='\t', end="\t")
            eprint(np.mean(exec_time1), sep="\t", end="\t")
            eprint(np.mean(exec_time1), sep="\t", end="\t")
          #eprint("\t"
        if(globalConfig__evaluyateNaive == True):
            eprint(np.mean(exec_time_pyIter_sum_plus), sep="\t", end="\t") #! 1
            eprint(np.mean(exec_time_pyIter_sum_mul), sep="\t", end="\t") #! 2
            eprint(np.mean(exec_time_pyIter_sum_mul_xmtMem), sep="\t", end="\t") #! 2
            eprint(np.mean(exec_time_pyIter_sum_plus_mask), sep="\t", end="\t") #! 3
            eprint(np.mean(exec_time_pyIter_sum_mul_mask), sep="\t", end="\t") #! 4
            eprint(np.mean(exec_time_pyIter_sum_mul_mask_andFuncCall), sep="\t", end="\t") #! 4
            eprint(np.mean(exec_time_pyIter_sum_mul_mask_andFuncCall_alsoInside), sep="\t", end="\t") #! 4
    #! -------------------
        eprint(np.mean(exec_time_pyIter_sim__Euclid), sep="\t", end="\t") #! 5
        eprint(np.mean(exec_time_pyIter_sim__Manhatten), sep="\t", end="\t") #! 6
        eprint(np.mean(exec_time_pyIter_sim__paired), sep="\t", end="\t") #! 7
        eprint(np.mean(exec_time_pyIter_sim__cosine), sep="\t", end="\t") #! 8
        eprint(np.mean(exec_time_pyIter_sim__sciPySpatial__cosine), sep="\t", end="\t") #! 9
        eprint(np.mean(exec_time_pyIter_sim__sciPySpatial__minkowski), sep="\t", end="\t") #! 10
    #! -------------------
        eprint("") #! ie, anewline.
    else:
        eprint('Method: (A==B).all(),       ', np.mean(exec_time0))
        eprint('Method: np.array_equal(A,B),', np.mean(exec_time1))
        eprint('Method: np.array_equiv(A,B),', np.mean(exec_time2))
#! ------ test-cases by (oekseth, 06. feb. 2017)
        if(globalConfig__evaluyateNaive == True):
            eprint('Method: Python-default:(A + B),', np.mean(exec_time_pyIter_sum_plus)) #! 1
            eprint('Method: Python-default:(A * B),', np.mean(exec_time_pyIter_sum_mul)) #! 2
            eprint('Method: Python-default-xmtMem:(A * B),', np.mean(exec_time_pyIter_sum_mul_xmtMem)) #! 2
            eprint('Method: Python-default:(mask(A) + mask(B)),', np.mean(exec_time_pyIter_sum_plus_mask)) #! 3
            eprint('Method: Python-default:(mask(A) * mask(B))),', np.mean(exec_time_pyIter_sum_mul_mask)) #! 4
            eprint('Method: Python-default:(mask(A) * mask(B)))-funcCall,', np.mean(exec_time_pyIter_sum_mul_mask_andFuncCall)) #! 4
            eprint('Method: Python-default:(mask(A) * mask(B)))-funcCall-externalEuclidPairLib,', np.mean(exec_time_pyIter_sum_mul_mask_andFuncCall_alsoInside)) #! 4
    #! -------------------
        eprint('Method: SimMetric:Numpy:Euclid(A, B),', np.mean(exec_time_pyIter_sim__Euclid)) #! 5
        eprint('Method: SimMetric:Numpy:Manhatten(A, B),', np.mean(exec_time_pyIter_sim__Manhatten)) #! 6
        eprint('Method: SimMetric:Numpy:paired(A, B),', np.mean(exec_time_pyIter_sim__paired)) #! 7
        eprint('Method: SimMetric:Numpy:cosine(A, B),', np.mean(exec_time_pyIter_sim__cosine)) #! 8
        eprint('Method: SimMetric:Numpy:sciSpatial:cosine(A, B),', np.mean(exec_time_pyIter_sim__sciPySpatial__cosine)) #! 9
        eprint('Method: SimMetric:Numpy:sciSpatial:minkowski(A, B),', np.mean(exec_time_pyIter_sim__sciPySpatial__minkowski)) #y! 10
    currentPos += 1



#!
#! Deimsnions:
#cnt_cases = 40
cnt_cases = 10
minSize = 128
multFact__nrows = 1
multFact__ncols = 1
for i in xrange(cnt_cases):
    case_id = i
    nrows = minSize + 128*case_id*multFact__nrows
    ncols = nrows # minSize + 128*case_id*multFact__ncols
    compute_matrix(nrows, ncols)
