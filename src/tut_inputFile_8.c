#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.

/**
   @brief a permtaution ot "tut_inputFile_4.c" where we elvauat ethe "data/FCPS/01FCPSdata/" data-set (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a permtuation of "tut_inputFile_4.c".
   @remarks thsi exampel is indteded to increase a users famliarty with how data-sets may be applied iot apply large-scale clsuteirnga-anlsyssi.
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;

  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;

  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "binomial_p005";
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //obj_config.globalConfig__isToStore__inputMatrix__inFormat__csv = isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.


  assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  /* //! -------------------------------------------- */
  /* //! */
  /* //! File-specific cofnigurations:  */
  /* s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t(); */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = false; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;   */
  /* fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file. */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config; */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = true; */
  /* //! */
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //!
  //! We are interested in a more performacne-demanind approach: 
  const uint sizeOf__nrows = 500;
  const uint sizeOf__ncols = 500;
  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = true; //! ie, the 'important part' of this.
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat = fileRead_config__syn;
  s_kt_matrix_t matrix_concatToAll; setTo_empty__s_kt_matrix_t(&matrix_concatToAll);
  if(config__nameOfDefaultVariable && strlen(config__nameOfDefaultVariable)) {
    matrix_concatToAll = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable, fileRead_config__syn__concat);
    //! 
    //! Update the configuraiton-object:
    assert(matrix_concatToAll.ncols > 0);
    fileRead_config__syn.mat_concat = &matrix_concatToAll;
  }

//! -------------------------------------------------------------------------
//!
//!
//! Intiate for the real-lfie data-sets expected ... where files are expected to be found in "tests/data/kt_mine/"
const uint mapOf_functionStrings_base_size = 10;
const char *mapOf_functionStrings_base[mapOf_functionStrings_base_size] = {
    "data/FCPS/01FCPSdata/Atom.lrn",
    "data/FCPS/01FCPSdata/EngyTime.lrn",
    "data/FCPS/01FCPSdata/Hepta.lrn",
    "data/FCPS/01FCPSdata/Target.lrn",
    //! ---- 
    //"data/FCPS/01FCPSdata/tmp.TwoDiamonds.lrn",
    "data/FCPS/01FCPSdata/WingNut.lrn",
    "data/FCPS/01FCPSdata/Chainlink.lrn",
    "data/FCPS/01FCPSdata/GolfBall.lrn",
    "data/FCPS/01FCPSdata/Lsun.lrn",
    //! ---- 
    "data/FCPS/01FCPSdata/Tetra.lrn",
    "data/FCPS/01FCPSdata/TwoDiamonds.lrn"
};

  //! --------------------------
  //!
  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint mapOf_realLife_size = mapOf_functionStrings_base_size *  __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {      
    //! Budil the sample-string:
    const char *stringOf_sampleData_type = mapOf_functionStrings_base[base_id];
    assert(stringOf_sampleData_type);
    assert(strlen(stringOf_sampleData_type));
    //!
    //! ITerate through the different k-cluster-count-specifriciaotns:
    for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
      const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
      assert(current_pos < mapOf_realLife_size);
      //!
      //! Add the object:
      mapOf_realLife[current_pos].tag = stringOf_sampleData_type;
      mapOf_realLife[current_pos].file_name = stringOf_sampleData_type;
      mapOf_realLife[current_pos].fileRead_config = fileRead_config__syn;
      mapOf_realLife[current_pos].inputData__isAnAdjcencyMatrix = false;
      mapOf_realLife[current_pos].k_clusterCount = k_clusterCount;
      mapOf_realLife[current_pos].mapOf_vertexClusterId = NULL;
      mapOf_realLife[current_pos].mapOf_vertexClusterId_size = 0;
      mapOf_realLife[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
      mapOf_realLife[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      mapOf_realLife[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      mapOf_realLife[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
      //!
      current_pos++;
    }
  }

  //! ----------------------------------
  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_concatToAll);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
