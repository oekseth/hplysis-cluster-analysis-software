#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_sparse_sim.h" //! which is used for sparse comptautions of simlairty-metircs.
#include "hpLysis_api.h" //! ie, the aPI used for clsutering.
#include "aux_exportMeta.h" //! which is used to 'enable' the 'pre-construciton' of web-based visualziaiton-wrappers, eg, using the "Hiogcharts.js" java-scirpt-lbirary (oeskejth, 06. mar. 2017)
#include "hp_ccm.h"

/**
   @brief how to export (to a file) and import (from a file) a list of pairs/relations, and then use latter as input to a sparse simlairty-copmtaution.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks a permtuation of our "tut_sparseSim_4_fromFile_intersection__postOps_hca.c" where we:
   (a) input: use muliple input-files, input-files which 'orignate' from our an Apache-server-log.
   (b) result(data::each): both simliarty-matrix, HCA-tree and cluster-counts; export using a tree-based fromat supproted by the "highchart.js" java-script library.
   (b) result(data::summary): assess the 'cluster-simliarty' for each of the different input-data-combinations, eg, to evlauate if "software x software:robots" have a hgihger degree of cluste-consistency than "software x byFile"
   (c) result(format):  use the "aux_exportMeta.h" to construct 'viusaulziaiotn-wrappers' for our resuylt-data-sets: export result as a matrix (where 'rows' are the 'cluser-ids' while 'columns' are the 'clustered row-ids' ... export the result ... scaffold a visualizaiton (ie, to compare/evlaute simliarty in cluster-meberships). 
   @remarks in this use-case-tutotail we demonstrate the applcailibyt of our "kt_sparse_sim.h" simliarty-module. The input-fiels we use provides a use-case of 'how' to incldue/use mulitple sparse relations-files, eg, to  descirbe/represent different feature-evlauation-ctieria. To exmaplify the latter use-cas-asseriton, we in this use-case-evlauaiton evalaute different access-parameters of data-accesses: to relate "IP-address" to ["client-software", "day-of-week", "file-requested"]x["not-registered-as-a-robot", "a robot"]. 
   @remarks compairson-method: demosntrates a multi-step siliarty-based method/appraoch where we: step(a) load a set of apache-log-files into 2d-struct (consturcted using our "data/parse__apacheLog.pl"); step(b) comptue simliarty for [file(a) x file(b)]; step(c) apply HCA; step(d) load the 'string-to-integer' mapping file (into memory); step(d) generate results compatilbe to "http://www.highcharts.com/demo/treemap-with-levels"; step(e) construct a web-based visualizaiton (of the latter, using "http://www.highcharts.com/demo/treemap-large-dataset" as a template).
   @remarks useCase(concrete): to evaluate and describe patterns in Apache-log-files, eg, "[dayOfWeek]x[ipAddress]", "[accessed_webPage]x[ipAddress]", "[nameOf_lastVisistedPagePAge (eg, Baidu or GoogleBoots)]x[ipAddress]". In oru appraoch we have written a new Perl-parser ("data/parse__apacheLog.pl"): we consturct differnet dat-afiles (to descirb ediffernet  patterns/cases/features of Apache-log-files): each of Apache-subset-feature-files is read/parsed/'loaded' into memory, before analyssing the simlarity betwene the features. In oru simlairty-based apache-fatreu-simalrity-appraoch we comptue "ccm(hca(similarity(simliarty(data[m]xdata[x])=))). In In our hca-based appraoch we use the hpLysis appraoch for dynamic HCA-k-means cosntruction/identificaiton. The 'dynamic k-means-hca-based appraoch' use/apply 'an iterative HCA-based appraoch maximiszing the Silhoutte index', a result which is then elvuated usign the complete set of matrix-based CMC-amtrix (supported by hpLysis): we generate a CCM-based matrix (where each 'ccm-row' depscries/rpesent a given iniput-file/evlauation-case). The latter CCM-matrix is (in our viaul evlauation-appraoch) used to analyse/the 'dependency' between the evalauted/anlaysed data-sets (combining an evlauaiton of different heatmap-matrices with a CCM-line-plot).    
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const char *stringOf_resultPrefix = "sparseSim_apache_";
#endif 
  //!
  //! Specify the simlairty-metic: 
  s_kt_correlationMetric_t sim_metric = setTo_empty__andReturn__s_kt_correlationMetric_t();
  sim_metric.metric_id = e_kt_correlationFunction_groupOf_intersection_intersection;
  //! ------------------------------------------------------------------------------  
  //! Note: in [”elow] ex epct expect our "parse__apacheLog.pl" and our "tut_sparseSim_7_realLife_apacheLog_compareMultipleFiles.c" to be realted (oekseht, 06. mar. 2017)p
  const uint arrOf_inputFiles_size = 6;
  const char *arrOf_inputFiles[arrOf_inputFiles_size] = {
    "data/apache_ip_bySoftware.tsv", //! index: 0
    "data/apache_ip_bySoftware.robots.tsv",
    //! ---------------
    "data/apache_ip_by_dayOf_week.tsv", //! index: 2
    "data/apache_ip_by_dayOf_week.robots..tsv",
    //! ---------------
    "data/apache_ip_accessedFile.tsv", //! index: 4
    "data/apache_ip_accessedFile.robots.tsv",
    //! ---------------
  };
  const char *inputFile__stringToId = "data/apache_strToId.tsv"; //! a file which duie to safety-readosna re Not included in our repo.  
  s_kt_set_2dsparse_t arrOf_data[arrOf_inputFiles_size] = {
    setToEmptyAndReturn__s_kt_set_2dsparse_t(),
    setToEmptyAndReturn__s_kt_set_2dsparse_t(),
    //! ---------------
    setToEmptyAndReturn__s_kt_set_2dsparse_t(),
    setToEmptyAndReturn__s_kt_set_2dsparse_t(),
    //! ---------------
    setToEmptyAndReturn__s_kt_set_2dsparse_t(),
    setToEmptyAndReturn__s_kt_set_2dsparse_t(),
  };
  
  s_kt_matrix_t obj_inputMap = setToEmptyAndReturn__s_kt_matrix_t();
  if(inputFile__stringToId) { //  && (0 != access(inputFile__stringToId, W_OK))) {
    //! Then we read the input-file
    obj_inputMap = readFromAndReturn__file__advanced__s_kt_matrix_t(inputFile__stringToId, initAndReturn__s_kt_matrix_fileReadTuning_t());
    if(obj_inputMap.nrows == 0) {
      fprintf(stderr, "(file-not-found)\t A non-importnat observation: The apache-mappign-file \"%s\" was Not found: if the latter is of interest then (A) consturct your own or (b) request the latter from the hpLysis developer [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", inputFile__stringToId, __FUNCTION__, __FILE__, __LINE__);
    }
    // assert(obj_inputMap.nrows > 0); //! as the file was otherwise Not read, ie, could be an errro rin the file-format.
    //! 
    /* printf("cnt_rows=%u, at %s:%d\n", obj_inputMap.nrows, __FILE__, __LINE__); */
    /* assert(false); // FIXME: remove. */
  } else {
    //assert(false); // FIXME: remove.
  }
  //assert(false); // FIXME: remove.

  //!
  //! Load data:
  for(uint i = 0; i < arrOf_inputFiles_size; i++) {
    //! Read the 'sparse colleciton' from the [ªbove] generated file:
    const char *file_name = arrOf_inputFiles[i];
    assert(file_name);
    assert(strlen(file_name));
    s_kt_list_1d_pairFloat_t vec_1 = initFromFile__s_kt_list_1d_pairFloat_t(file_name);
    assert(vec_1.list_size > 0); //! as the file is otehrise Not read.
    //printf("cnt_listElem = %u, at %s:%d\n", vec_1.list_size, __FILE__, __LINE__);
    //!
    //! Convert into a 2d-list:
    s_kt_set_2dsparse_t sparse_1 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(&vec_1);
    assert(sparse_1._nrows > 0); //! ie, gvien our [ªbove] isneriotn-policy.
    //! De-allocate the 'local' data, and udpat ethe 'glboal' pointer:
    free__s_kt_list_1d_pairFloat_t(&vec_1);
    arrOf_data[i] = sparse_1; //! ie, as we 'use this' in [”elow]
  }
  //!
  //! Cosntruct an oipen the result-file:
  char nameOf_resultFile[2000] = {'\0'}; sprintf(nameOf_resultFile, "%s%s", stringOf_resultPrefix, "result_realLife_apache_hca.js");
  FILE *fileP = fopen(nameOf_resultFile, "wb");
  //! ...
  char nameOf_resultFile_meta_dom[2000] = {'\0'}; sprintf(nameOf_resultFile_meta_dom, "%s%s", stringOf_resultPrefix, "result_realLife_apache_hca__meta.html");
  FILE *fileP_meta_DOM = fopen(nameOf_resultFile_meta_dom, "wb");
  assert(fileP_meta_DOM);
  //! ...
  char nameOf_resultFile_meta[2000] = {'\0'}; sprintf(nameOf_resultFile_meta, "%s%s", stringOf_resultPrefix, "result_realLife_apache_hca__meta.js");
  FILE *fileP_meta = fopen(nameOf_resultFile_meta, "wb");
  assert(fileP_meta);

  //assert(false); // FIXME: remove

  //!
  //! Configure our HCA+CCM-based appraoch

  //!
  //! Allocate memory for our HCA+CCM-based appraoch:
  const uint cnt_data_cases = arrOf_inputFiles_size*arrOf_inputFiles_size;
  assert(cnt_data_cases > 0);
  s_kt_matrix_t mat_result_dataxCCm = initAndReturn__s_kt_matrix(cnt_data_cases, /*cnt-ccms=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  { //! the row-names and column-names for [ªbove]: 
    //! First add for: rows:
    uint row_id = 0;
    for(uint data_in = 0; data_in < arrOf_inputFiles_size; data_in++) {
      for(uint data_out = 0; data_out < arrOf_inputFiles_size; data_out++) {
	char string_description[2000] = {'\0'}; sprintf(string_description, "%s VS %s", arrOf_inputFiles[data_in], arrOf_inputFiles[data_out]);
	set_stringConst__s_kt_matrix(&mat_result_dataxCCm, row_id, string_description, /*addFor_column=*/false);	  
	//! Icnrement:
	row_id++;
      }
    }
    //! Add for: columns:
    for(uint enum_i = 0; enum_i < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; enum_i++) {
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)enum_i;
      const char *string_description = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_enum);
      set_stringConst__s_kt_matrix(&mat_result_dataxCCm, enum_i, string_description, /*addFor_column=*/true);	  
    }    
  }

  //!
  //! Comptue simliarty-metrics and generate result-objects:
  uint mat_result_dataxCCm__currentPos = 0;
  for(uint data_in = 0; data_in < arrOf_inputFiles_size; data_in++) {
    s_kt_set_2dsparse_t sparse_1 = arrOf_data[data_in];
    for(uint data_out = 0; data_out < arrOf_inputFiles_size; data_out++) {
      printf("(info)\t Compute::simMetric::sparse for \"%s\" x \"%s\", at %s:%d\n", arrOf_inputFiles[data_in], arrOf_inputFiles[data_out], __FILE__, __LINE__);
      s_kt_set_2dsparse_t sparse_2 = arrOf_data[data_in];
      //!
      //! Compute simliarty:
      s_kt_sparse_sim obj_sim = init__s_kt_sparse_sim_t(sim_metric);
      //!
      //! Apply logics:
      //! Note: the [”elow] "mat_result" fucntion-call is simliar to our 'sparse-fucntion-call', where latter may be 'called' using: s_kt_set_2dsparse_t sparse_result = computeStoreIn__2dList__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/&sparse_2, /*isTo_rememberScores=*/true, /*weight=*/NULL);
      s_kt_matrix_base_t mat_result = computeStoreIn__matrix__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/&sparse_2, /*weight=*/NULL);
      //s_kt_matrix_base_t mat_result = computeStoreIn__matrix__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/NULL, /*weight=*/NULL);
      assert(mat_result.nrows > 0);
      assert(mat_result.ncols > 0);
      //! De-allcoate the sparse cofnigruation-object:
      free__s_kt_sparse_sim_t(&obj_sim);
      //! -------------------------------------------------------------- 
      //!
      { //! Apply cluster-based logics, and then write out the result:
	//! Cosntruct a shallow copy of the amtirx, and then export:
	s_kt_matrix_t mat_shallow = setToEmptyAndReturn__s_kt_matrix_t();
	mat_shallow.matrix = mat_result.matrix;
	mat_shallow.nrows = mat_result.nrows;
	mat_shallow.ncols = mat_result.ncols;
	printf("(info)\t Compute::HCA::sparse for \"%s\" x \"%s\", at %s:%d\n", arrOf_inputFiles[data_in], arrOf_inputFiles[data_out], __FILE__, __LINE__);
	//! Initate object
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	//! Note: as we (fro most cases in this tut-example) comptue simalirty for dfferent emtrics, we need to (for some of the HCAs) first 'apply' a simlairty-metric-pre-step, oie, hence [”elow].
	//! Note(time): given the rekative large number of ros and columns (in our "mat_shallow") we expect [”elow] compouation to be a 'delaying factor', which explains the importance of our optmized hpLysis software (ie, as 'the effects of Not using our optmized software' woudl ahve resulted in a sigicnat exueicotn-tiem-increase.). 
	obj_hp.config.corrMetric_prior_use = true; 
	obj_hp.config.corrMetric_prior = sim_metric;
	if(true) { //! Then we use the CCM-based dynamic-k-clksuter-approach (oekseth, 06. mar. 2017):
	  obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true; //! ie, a 'sfeguard' to future-code-changes: whiel latter is by defualt set to true, explcitly calling latter will ensure that 'any future changes to API' will be 'detected' by 'this call'.
	  obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette; //! ie, ot use the Silhouette enum.
	}

	//! Apply cluster using an HCA-based algorithm:
	e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
	bool is_ok = cluster__hpLysis_api(&obj_hp, clustAlg, &mat_shallow, /*nclusters=*/UINT_MAX, /*npass=*/10); //! where "nclusters" and "npass" are ignored as we use an HCA-based lagorithm.
	assert(is_ok);
	//!
	{ //! Updateour CCM-based object with the [ªbove] results:
	  assert(mat_result_dataxCCm__currentPos < mat_result_dataxCCm.nrows);
	  //!
	  //! Apply the CCMs:
	  //! Initiate data-structre:
	  s_kt_matrix_base_t matSim__shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&(obj_hp.matrixResult__inputToClustering));
	  //! Itniate the input-data-set to the "hp_ccm.h" API: 
	  //! Note: simpler use-case-exmapel wrt. [below] is seen in our "tut_ccm_11_compareMatrixBased.c".
	  s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	  const uint *map_clusterMembers = obj_hp.obj_result_kMean.vertex_clusterId; //! where latter 'mapping' is defined in our "kt_clusterAlg_fixed_resultObject.h"
	  {
	    const uint map_clusterMembers_size = obj_hp.obj_result_kMean.cnt_vertex; //! where latter 'mapping' is defined in our "kt_clusterAlg_fixed_resultObject.h"
	    printf("map_clusterMembers_size=%u VS %u, at %s:%d\n", map_clusterMembers_size,  matSim__shallow.nrows, __FILE__, __LINE__);
	    assert(map_clusterMembers_size == matSim__shallow.nrows);
	  }
	  //! Apply lgoics:
	  is_ok = ccm__singleMatrix__completeSet__hp_ccm(&matSim__shallow, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
	  assert(is_ok);    
	  assert(mat_result_dataxCCm.ncols == obj_result.list_size);
	  //!
	  //! Update our result-object:
	  for(uint i = 0; i < obj_result.list_size; i++) {
	    const t_float ccm_score = obj_result.list[i];
	    mat_result_dataxCCm.matrix[mat_result_dataxCCm__currentPos][i] = ccm_score;
	  }
	  //! 
	  //! Increment:
	  mat_result_dataxCCm__currentPos++;
	  //!
	  //! De-allocate:
	  //! Note: would be wrong de-allcoating the "matSim__shallow" objec,t as the latter 'is' only a shallow copy of the matrix foudn in oru "obj_hp" "s_hpLysis_api_t" object.
	  free__s_kt_list_1d_float_t(&obj_result);
	} //! and at thsiu exec-point we ahve updated our CCM-based evlauaiton.

	//! 
	//! Export to a 'homegrown' "highcharts.js" format:
	char str_header[2000] = {'\0'}; sprintf(str_header, "case_%u_%u", data_in, data_out);
	fprintf(fileP, "\n var %s = [\n", str_header); //! ie, a new java-scirpt vairable.
	char **map_indexToName = obj_inputMap.nameOf_rows;
	const s_kt_clusterAlg_hca_resultObject_t obj_hca = obj_hp.obj_result_HCA; //! ie, the HCA-result-object.
	assert(obj_hca.nrows > 0);
	//! 
	if(map_indexToName) { //! Udpate the reuslt-matrix with names (if naems are set):
	  assert(obj_hp.matrixResult__inputToClustering.nrows > 0); //! ie, as we expect 'this' to have been set.
	  const uint nrows = obj_hp.matrixResult__inputToClustering.nrows;
	  const uint ncols = obj_hp.matrixResult__inputToClustering.ncols;
	  assert(ncols == nrows); //! ie, as we expect 'an adjcency-amtrix'
	  assert(obj_inputMap.nrows >= nrows); //! ie, as we expect the 'name-map' to be a superset of the input-data-sets.
	  //!
	  //! Set the names:
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    const char *stringTo_add = map_indexToName[row_id];
	    if(stringTo_add && strlen(stringTo_add)) {
	      set_stringConst__s_kt_matrix(&(obj_hp.matrixResult__inputToClustering), row_id, stringTo_add, /*addFor_column=*/true);
	      set_stringConst__s_kt_matrix(&(obj_hp.matrixResult__inputToClustering), row_id, stringTo_add, /*addFor_column=*/false);
	    }
	  }
	}
	//! 
	//! 
	{ //! Expoert the 'generated' simlairty-matrix:
	  char nameOf_resultFile[2000] = {'\0'}; sprintf(nameOf_resultFile, "%s%s_%u_%u.tsv", stringOf_resultPrefix, "result_simMetric", data_in, data_out);	  
	  const bool is_ok = export__hpLysis_api(&obj_hp, nameOf_resultFile, NULL, /*format=*/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, NULL);
	  assert(is_ok);
	  //! Update our Meta-file wrt. [ªbov€] cofniguraiton, a 'emta-configuraiotn' where we byd efualt use/suppor thte "higchartcs.js" java-scirpt-library:
	  if(true) {	    
	    assert(fileP_meta_DOM);	    
	    assert(fileP_meta);	    
	    const uint DOM_ID = (data_in+1)*data_out;
	    char string_description[2000] = {'\0'}; sprintf(string_description, "Apache-features: %s VS %s", arrOf_inputFiles[data_in], arrOf_inputFiles[data_out]);
	    linePlot__aux_exportMeta(DOM_ID, e_aux_exportMeta__exportFormat_highCharts, fileP_meta, fileP_meta_DOM, nameOf_resultFile, string_description);
	  }
	}
	//!
	//! Iterat ethrough the result-object:
	for(uint i = 0; i < obj_hca.nrows; i++) {
	  Node_t node = obj_hca.mapOf_nodes[i];
	  const bool has_parent = (node.right != node.left) && (node.right != INT_MAX) && (node.right != (int)UINT_MAX);
	  const t_float color = (t_float)node.left;
	  //const t_float color = (t_float)node.distance;
	  fputs("{", fileP);
	  if(map_indexToName == NULL) {
	    fprintf(fileP, "id:\"%d\", name:\"%d\", ", node.left, node.left);
	    if(has_parent) {
	      fprintf(fileP, "parent:\"%d\", value:%f, colorValue:%f",  node.right, node.distance, color);
	    }
	  } else {
	    //! Define a genrlaized funciotn to simplfiy our format-exort-routine:
#define __MiF__writeOutVar(tag, key) ({char str_local[2000] = {'\0'}; if(key >= 0) {sprintf(str_local, "%s:\"%s\",", tag, map_indexToName[key]);} else {sprintf(str_local, "%s:\"%d\", ", tag, key);;}fputs(str_local, fileP);;})
	    //#define __MiF__writeOutVar(tag, key) ({char str_local[2000] = {'\0'}; if(key >= 0) {sprintf(str_local, "%s:\"%s\",", tag, map_indexToName[key]);} else {sprintf(str_local, "%s:\"%d\", ", tag, key);} fputs(str_local, fileP);};})
	    __MiF__writeOutVar("id", node.left);
	    __MiF__writeOutVar("name", node.left);
	    if(has_parent) {
	      //if(node.left != node.right) {
	      __MiF__writeOutVar("parent", node.right);
	      fprintf(fileP, "value:%f, colorValue:%f",  node.distance, color);
	    }
	    //assert(false); // FIXME: add seomthing ... ie, when we 'know' correctness of [ªbove]
#undef __MiF__writeOutVar
	  }
	  fputs("},\n", fileP);
	}
	/* is_ok = export__hpLysis_api(&obj_hp, /\*resultFile=*\/"result_tut_hca.tsv", NULL, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, NULL); */
	/* assert(is_ok); */
	//! De-allocate:
	free__s_hpLysis_api_t(&obj_hp);
	//! 'Complete' the [ªbove] generated java-scirpt-list of variables:
	fprintf(fileP, "\n];\n");
      }
    }
  }
  { //! Write out the resutl-matrix:
    //!
    //! Write otu the matrix:
    char nameOf_resultFile_data[2000] = {'\0'}; sprintf(nameOf_resultFile_data, "%s_%s", stringOf_resultPrefix, "result_realLife_apache.tsv");
    const bool is_ok = export__singleCall__s_kt_matrix_t(&mat_result_dataxCCm, nameOf_resultFile_data, NULL);
    assert(is_ok);
    //!
    //! Write out the meta-object:
    char str_metaObj[2000] = {'\0'}; sprintf(str_metaObj, "%s_%s", stringOf_resultPrefix, "result_realLife_apache_hca");
    s_aux_exportMeta_t obj_meta = init__s_aux_exportMeta_t(str_metaObj);
    o_linePlot__s_aux_exportMeta(&obj_meta, /*DOM_ID=*/0, /*format=*/e_aux_exportMeta__exportFormat_highCharts, nameOf_resultFile_data, /*description=*/"CCM-scores of HCA-based data-construction");
    //! De-allcoate:
    free__s_aux_exportMeta_t(&obj_meta);
  }
  //!
  //! De-allocates:
  for(uint i = 0; i < arrOf_inputFiles_size; i++) {
    free_s_kt_set_2dsparse_t(&(arrOf_data[i]));
  }
  assert(fileP);   fclose(fileP); fileP = NULL;
  assert(fileP_meta);  fclose(fileP_meta); fileP_meta = NULL;
  assert(fileP_meta_DOM);  fclose(fileP_meta_DOM); fileP_meta_DOM = NULL;

  free__s_kt_matrix(&obj_inputMap);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

