	//! -------------------------------------------------------------
	//! 
	//! Allocate: 
	s_trans_global_list_t obj_tree = init__s_trans_global_list_t(cnt_vertices, (e_trans_global_traverseType_t)update_resultSet, 
								     /*isTo_update_resultSet=*/(resultAccess != e_trans_global_resultType_none),
								     (e_trans_global_resultType_t)resultAccess);
	
	//! Construct a sampled-ata-set for the oejbec tin qeustion.
	//	s_kt_list_1d_uint_t arrOf_startNodes = 
__sem__constructSampleDataAnd_call__andCall(&obj_tree, cnt_childrenEach, cnt_disjointTrees, is_req, use_dummy);

	{
	  const char *resultAccess_str = "direct";
	  if(resultAccess == e_trans_global_resultType_pointerList_apiWrapper) {resultAccess_str = "pointer-list";}
	  else if(resultAccess == e_trans_global_resultType_none) {resultAccess_str = "none";}
	  else if(resultAccess != e_trans_global_resultType_directList) {assert(false);} //! ie, as we then need adding support for this.
	  char str_local[2000] = {'\0'}; 
	  if(use_dummy == false) {
	    sprintf(str_local, "type=[timePointToUpdate='%s', access='%s', recursive='%s']", (update_resultSet==0) ? "naive" : "afterCover", 
		    //char str_local[2000] = {'\0'}; sprintf(str_local, "type=[timePointToUpdate='%s', access='%s', recursive='%s'], topology=[N=%u, tree-width-each-node=%u, |graphs|=%u]", (update_resultSet==0) ? "naive" : "afterCover", 
		    resultAccess_str, (is_req) ? "yes" : "no" 
		    // , cnt_vertices, cnt_childrenEach, cnt_disjointTrees
		    );
	  } else {
	    sprintf(str_local, "is-a-dummy");
	  }
	  float cmp_time_search = FLT_MAX;
	  const float endTime = end_time_measurement(str_local, cmp_time_search);	
	  {
	    const uint row_id = obj_timer.current_row;
	    const uint col_id = timer_col_id; timer_col_id++;
	    assert(row_id < obj_timer.mat_time.nrows);
	    assert(col_id < obj_timer.mat_time.ncols);
	    //! Set the timing-results:
	    //printf("end-time=%f, at %s:%d\n", endTime, __FILE__, __LINE__);
	    obj_timer.mat_time.matrix[row_id][col_id] = endTime;
	    if(col_id == 0) { //! then set the row-idnifier:
	      char str_local[2000] = {'\0'}; sprintf(str_local, "vertices=%u", cnt_vertices);
	      set_stringConst__s_kt_matrix(&(obj_timer.mat_time), row_id, str_local, /*addFor_column=*/false);
	    }
	    if(row_id == 0) { //! then set the column-idnifier:
	      set_stringConst__s_kt_matrix(&(obj_timer.mat_time), col_id, str_local, /*addFor_column=*/true);
	    }
	  }
	}
	
	//! 
	//! Free memory: 
	free__s_trans_global_list_t(&obj_tree);
