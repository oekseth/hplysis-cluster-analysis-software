#ifndef s_log_approx_h
#define s_log_approx_h

#include "def_intri.h"
#include "libs.h"


/**
   @enum e_s_log_approx
   @brief the type of look-up-tables to construct
   @author Ole Kristian Ekseth (oekseth).
**/
typedef enum e_s_log_approx {
  //e_s_log_approx_mantissa_float_inMacro, //! ie, store the result in a macro
  e_s_log_approx_log_mantissa_float_inStruct, //! ie, "log(..)" for the mantissa of a "float" data-type
  e_s_log_approx_log_uint, //! ie, for the complete range of a "uint" data-type, limited by the "uint n" parameter as the 'upper limit'.
  e_s_log_approx_log_float_range, //! which pre-comptues the float-valeus for a [min, max] range.
  e_s_log_approx_log_float_range_between_0_1, //! which pre-comptues the float-valeus for a [min, max] range.
  //e_s_log_approx_
  e_s_log_approx_undef
} e_s_log_approx_t;
/**
   @enum e_s_log_approx
   @brief the type of logaritm to use
   @author Ole Kristian Ekseth (oekseth).
**/
typedef enum e_s_log_approx_typeOf_log {
  e_s_log_approx_typeOf_log_log,
  e_s_log_approx_typeOf_log_log2,
  e_s_log_approx_typeOf_log_undef
} e_s_log_approx_typeOf_log_t;

/**
   @enum s_log_approx
   @brief provide fast access to the log(..) functions.
   @author Ole Kristian Ekseth (oekseth).
 **/
typedef struct s_log_approx {
  uint lookup_table_size; float *lookup_table;
  float (*log_function)(float); //! which is the uder-defined float-funciton to make use of, eg, "log(..)" and "log2(..)"
} s_log_approx_t;


//! Allocate the valeus in the object.
void allocate_obj(s_log_approx_t *obj, const uint n, const enum e_s_log_approx_typeOf_log typeOf_logFunction, const float range_minValue, const float range_maxValue);

//! De-allcoate the locally reserved memory for the s_log_approx_t object.
void free_obj(s_log_approx_t *obj);

#endif
