#ifndef func_hp3_h
#define func_hp3_h

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


 /**
   @file func_hp3
   @brief provide lgocis for computing hp3*...(...) functions
   @remarks include both the 'bnaive' "mine" implementaitons and KNitting-Tools improved algorithms
   @author Ole Kristian Ekseth (oekseth).
 **/


//! Simplify the configuration of our optmized "mine" software-implementaiton.
#include "configure_optimization.h" //! Whcih specifies the optmizaitons and macro-configurations

//! a lcoal debug-funciton to validate correctness of our appraoch.
// #define debug_local_log_aboveZero(val) ({t_float ret_val = 0; if(val > 0) {ret_val = (t_float)log(val);} ret_val;}) 

#define function_prefix_funcHP3 static
// #define function_prefix_funcHP3 

#define hp3_improved_alt2(c, s, t, total_inv) ({t_float H = 0.0;	\
  const t_float prob_1 = (t_float) c[s-1] * total_inv; \
  const t_float prob_log = log_range_0_one(prob_1); \
  H -= prob_1 * prob_log;   \
  const int sum = c[t-1] - c[s-1]; \
  t_float prob_2 = (t_float) sum * total_inv; \
  H -= prob_2 * log_range_0_one_abs(prob_2); 	\
  H;}) // ! ie, return


#if optmize_use_logValues_fromLocalTable == 1


//  const t_float prob_log = log_range_0_one(prob_1);  
  //const t_float prob_2_log = (sum > 0) ? log_index_fast(sum) + total_inv_log : 0; 
#define hp3_improved(c, c_log, s, t, total_inv, total_inv_log) ({t_float H = 0.0;	\
  assert((int)total_inv_log == (int)log(total_inv)); \
  const t_float prob_1 = (t_float) c[s-1] * total_inv; \
  const t_float prob_log = c_log[s-1] + total_inv_log;	\
  /*const t_float prob_log = log(c[s-1]*total_inv); */	\
  assert((int)c_log[s-1] == (int)log(c[s-1])); \
  H -= prob_1 * prob_log;	       \
  const int sum = c[t-1] - c[s-1]; \
  const t_float prob_2 = (t_float) sum * total_inv; \
  const t_float prob_2_log = log_index_fast(sum) + total_inv_log;	\
  /*const t_float prob_2_log = log(sum *total_inv); */			\
  H -= prob_2 * prob_2_log;					  \
  H;}) //! ie, return


#else //! ---------------------------
#define hp3_improved(c, s, t, total_inv) (hp3_improved_alt2(c, s, t, total_inv))
#endif

#if optmize_use_logValues_fromLocalTable == 1


	// FIXME: in ["elow] validate correctness of the new-included "VECTOR_FLOAT_TYPE vec_sum = VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET1(0), VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function));" iii ie, instead of the rpeviosu/old "VECTOR_FLOAT_TYPE vec_sum = VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function), VECTOR_FLOAT_SET1(-1));"
// VECTOR_FLOAT_TYPE vec_mappedTo_function = log_range_vector_get_memoryLocation(vec_prob); 
//    vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted); 
#define hp3_improved_intri(c, vec_c_forT, s, t, vec_total_inv) ({	\
    const SPARSEVEC_TYPE vec_s = local_SPARSEVEC_LOAD(&c[s-1]); \
    VECTOR_FLOAT_TYPE vec_s_float = SPARSEVEC_convertTo_float(vec_s); \
    VECTOR_FLOAT_TYPE vec_prob = VECTOR_FLOAT_MUL(vec_s_float, vec_total_inv); \
    VECTOR_FLOAT_TYPE vec_mappedTo_function = VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_s_float); \
    vec_mappedTo_function = VECTOR_FLOAT_ADD(vec_mappedTo_function, vec_total_inv_log); /*! ie, #log(sum / total) = log(sum) - log(total) = log(sum) + log(1/total)" */ \
    /* Combine [above]: Note: "-1" is used to comptue "-1" */	\
    VECTOR_FLOAT_TYPE vec_sum = VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET1(0), VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function)); \
    /* Next-sum: */	\
    SPARSEVEC_TYPE vec_t_s = SPARSEVEC_SUB(vec_s, vec_c_forT);  \
    VECTOR_FLOAT_TYPE vec_t_s_float = SPARSEVEC_convertTo_float(vec_t_s); \
    /*VECTOR_FLOAT_PRINT(vec_t_s_float);*/ \
    vec_prob = VECTOR_FLOAT_MUL(vec_t_s_float, vec_total_inv); \
    /*! Make use of the "fromLocalTable" specific table-lgoics: */ \
    vec_mappedTo_function = VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_t_s_float); \
    vec_mappedTo_function = VECTOR_FLOAT_ADD(vec_mappedTo_function, vec_total_inv_log); /*! ie, #log(sum / total) = log(sum) - log(total) = log(sum) + log(1/total)" */ \
    vec_sum = VECTOR_FLOAT_SUB(vec_sum, VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function));})



#else //! ---------------------------

//! Note: teh "hp3_improved_intri(..)" is an trinistincitc verison of "hp3_improved(..)"
// FIXME: validate correctness of using the 'absoltue value' "SPARSEVEC_ABS(..)" <-- otherise update our "log_range_0_one_abs(..)" to 'allow' for the use of negative valeus.
//vec_t_s = SPARSEVEC_ABS(vec_t_s);					
#define hp3_improved_intri(c, vec_c_forT, s, t, vec_total_inv) ({	\
    const SPARSEVEC_TYPE vec_s = local_SPARSEVEC_LOAD(&c[s-1]); \
    VECTOR_FLOAT_TYPE vec_prob = VECTOR_FLOAT_MUL(SPARSEVEC_convertTo_float(vec_s), vec_total_inv); \
    VECTOR_FLOAT_TYPE vec_mappedTo_function = log_range_vector_get_memoryLocation(vec_prob); \
    vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted); \
    /* Next-sum: */	\
    VECTOR_FLOAT_TYPE vec_sum = VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function), VECTOR_FLOAT_SET1(-1)); \
    SPARSEVEC_TYPE vec_t_s = SPARSEVEC_SUB(vec_s, vec_c_forT);  \
    vec_prob = VECTOR_FLOAT_MUL(SPARSEVEC_convertTo_float(vec_t_s), vec_total_inv); \
    vec_mappedTo_function = log_range_vector_get_memoryLocation_abs(vec_prob); \
    vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted_abs); \
    vec_sum = VECTOR_FLOAT_SUB(vec_sum, VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function));})


#endif


/* #if optmize_use_logValues_fromLocalTable == 1 */

/* #else //!************************************************* */
// const t_float prob_log = debug_local_log_aboveZero(prob);
//! Comptue the shannons entropy for a number-range.
#if optmize_use_logValues_fromLocalTable == 1 
//  printf("prb=%f, total_inv=%f, at %s:%d\n", prob, total_inv, __FILE__, __LINE__); 
//  
//  const t_float sum_log = cumhist_log[t-1][i] - cumhist_log[s-1][i];
#define hp2q_dataScheme_new_item(cumhist,  t, s, i, total_inv, total_log) ({ \
  const t_sparseVec sum = cumhist[t-1][i] - cumhist[s-1][i]; \
  const t_float prob = (t_float) sum * total_inv;	     \
  const t_float prob_log = log_index_fast(sum) - total_log;  \
  prob * prob_log; })

#else //! then we use 'lookup' for the ifnerred log-value, a float-vlaue expected to be in range [-1, 1],

#define hp2q_dataScheme_new_item(cumhist,  t, s, i, total_inv) ({ \
  const t_sparseVec sum = cumhist[t-1][i] - cumhist[s-1][i]; \
  t_float prob = (t_float) sum * total_inv;	     \
  prob * log_range_0_one_abs(prob); })

#define hp2q_dataScheme_new_item_div(cumhist,  t, s, i, total) ({ \
  const t_sparseVec sum = cumhist[t-1][i] - cumhist[s-1][i]; \
  t_float prob = (t_float) sum / total;	     \
  prob * log_range_0_one_abs(prob); })

#endif




//! ***************************************************************************************
//! ***************************************************************************************
//! ***************************************************************************************

//! Comptue the F value in the "mine" algorithm.
#define __compute_function_F_naive(c, I, HP2Q, s, t, ct, HQ) ({ \
  const t_float cs = (t_float) c[s-1]; \
  /* printf("cs=%f, ct=%f, at %s:%d\n", cs, ct, __FILE__, __LINE__); */ \
  const t_float left_side = ((cs/ct) * (I[s][l-1]-HQ)); \
  const t_float right_side = (((ct-cs)/ct) * HP2Q[s][t]); \
  /* printf("(left-right) = %f - %f, at %s:%d\n", left_side, right_side, __FILE__, __LINE__); */ \
  left_side - right_side;}) //! ie, return the result.

//!A function similar to above, with difference that we use muliplciaiton instead of divsion.
#define __compute_function_F(c, I, HP2Q, t, s, ct, ct_inv, HQ) ({	\
      const t_float cs = (t_float)c[s-1];		\
  /* printf("cs=%f, ct=%f, at %s:%d\n", cs, ct, __FILE__, __LINE__); */ \
      const t_float val_1  = I[l-1][s];			\
      const t_float val_2 = HP2Q[t][s];					\
      const t_float right_side = (((ct-cs)*ct_inv) * val_2); \
      const t_float left_side = ((cs*ct_inv) * (val_1 - HQ)); \
      /* printf("left-side=%f, right_side=%f, at %s:%d\n", left_side, right_side, __FILE__, __LINE__); */ \
      /* printf("(left-right) = %f - %f, at %s:%d\n", left_side, right_side, __FILE__, __LINE__); */ \
      const t_float F = left_side - right_side; \
      F; });  //! ie, return

//! Note: we expect the vectors to always be greater than "0":
//      const VECTOR_FLOAT_TYPE vec_right_div = VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_MUL(/*first-div=*/VECTOR_FLOAT_SUB(vec_ct, vec_cs), /*second-div=*/vec_ct_inv)); 
#define __compute_function_F_intr(c, I, HP2Q, t, s, vec_ct, vec_ct_inv, vec_HQ) ({ \
      const VECTOR_FLOAT_TYPE vec_cs = SPARSEVEC_convertTo_float(local_SPARSEVEC_LOAD(&c[s-1])); \
      const VECTOR_FLOAT_TYPE vec_val_1  = local_VECTOR_FLOAT_LOAD(&I[l-1][s]); \
      const VECTOR_FLOAT_TYPE vec_val_2  = local_VECTOR_FLOAT_LOAD(&HP2Q[t][s]); \
      /*FIXME: valdiate that it is always correct to call ["elow] VECTOR_FLOAT_revertOrder(..) function */ \
      /*Note[below]: right-sub1 = (ct-cs)*ct_inv */ \
      const VECTOR_FLOAT_TYPE vec_right_div = VECTOR_FLOAT_MUL(/*first-div=*/VECTOR_FLOAT_SUB(vec_ct, vec_cs), /*second-div=*/vec_ct_inv); \
      /*Note[below]: right = (ct-cs)*ct_inv*val_2 */ \
      const VECTOR_FLOAT_TYPE vec_right = /*secnd-sub=*/VECTOR_FLOAT_MUL(/*first-mul=*/vec_right_div, /*second-mul=*/vec_val_2); \
      /* printf("right: "); VECTOR_FLOAT_PRINT(vec_right);  */\
      /*Note[below]: left = (cs*ct_inv)*(val1 - HQ) */ \
      const VECTOR_FLOAT_TYPE vec_left = /*first-sub=*/VECTOR_FLOAT_MUL(/*first=*/VECTOR_FLOAT_MUL(vec_cs, vec_ct_inv), /*second=*/VECTOR_FLOAT_SUB(vec_val_1, vec_HQ) ); \
      /* printf("left: "); VECTOR_FLOAT_PRINT(vec_left);	 */		\
      /* printf("left-side="); VECTOR_FLOAT_PRINT(vec_left); */		\
    VECTOR_FLOAT_SUB(vec_left, vec_right);});  //! ie, return



/* Returns the entropy induced by the points on the partition Q.
 * See section 3.2.1, page 10, SOM.
 *
 * Parameters
 *   cumhist : cumulative histogram matrix along P_map
 *   q : number of rows of cumhist (number of partitions in Q_map)
 *   p : number of cols of cumhist (number of partitions in P_map)
 *   n : total number of points
 */
function_prefix_funcHP3 t_float hq_old(t_sparseVec **cumhist, int q, int p, int n) {
  int i;
  t_float H = 0.0;
  
  t_float total = (t_float) n;
#if optmize_use_multInsteadOf_div == 1
  const t_float total_inv = (total != 0) ? (1 / total) : 0;
#endif

  //! Note: time=|q|
  // printf("----------------------- at %s:%d\n", __FILE__, __LINE__);
  for (i=0; i<q; i++) {
    if (cumhist[i][p-1] != 0) {
      //! NoteS: comptues "Shannon entropy".
      const t_float sum = cumhist[i][p-1];
#if optmize_use_multInsteadOf_div == 1
      const t_float prob = (t_float) sum * total_inv;
#else
      const t_float prob = (t_float) sum / total;
#endif
      // printf("[%d]\t prob=%f log(prob=%f,  --> %f at %s:%d\n", i, prob, log_range_0_one(prob), H,  __FILE__, __LINE__);
#if optmize_use_logValues_fromLocalTable == 1 
      //const t_float prob_log = debug_local_log_aboveZero(prob);
      const t_float prob_log = log_index_fast(sum) - log_index_fast(total); //range_0_one(prob);     
#ifndef NDEBUG //! then we validate the correctness of this approach:
      const t_float exp_1 = debug_get_floatValue_approx(prob_log);
      const t_float exp_2 = debug_get_floatValue_approx(log(prob));
      if(exp_1 != exp_2) {
	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__);
      }
      assert(exp_1 == exp_2);
#endif
#else//! then we include lgocis and viaralbes to 'provide access' to pre-comptued lgocs for floating-point numbers in range [-1.0, 1.0]:
      const t_float prob_log = log_range_0_one_abs(prob);     
#endif
      H -= prob * prob_log;
    }
  }




  //! @return
  return H;
}



#if optmize_use_logValues_fromLocalTable == 1
function_prefix_funcHP3 t_float hq_improved(t_sparseVec **cumhist, t_float **cumhist_log, int q, int p, int n, const t_float n_inv_log)
#else
function_prefix_funcHP3 t_float hq_improved(t_sparseVec **cumhist, int q, int p, int n)
#endif
{
  int i;
  t_float H = 0.0;


  // assert(false); 
  
  t_float total = (t_float) n; 
#if optmize_use_multInsteadOf_div == 1
  const t_float total_inv = 1.0/total;
  assert(total != 0);
#endif
    //! Note[time] approx zero, ie, ingisficant.
  // printf("----------------------- at %s:%d\n", __FILE__, __LINE__);
  for (i=0; i<q; i++) {
    //if (cumhist[p-1][i] != 0) {
    //! NoteS: comptues "Shannon entropy".
#if optmize_use_multInsteadOf_div == 1
    const t_float prob = (t_float) cumhist[p-1][i] * total_inv;
#else
    const t_float prob = (t_float) cumhist[p-1][i] / total;
#endif
    //assert(prob <= 1);
#if optmize_use_logValues_fromLocalTable == 1
    //const t_float prob_log = debug_local_log_aboveZero(prob); 
    const t_float prob_log = cumhist_log[p-1][i] + n_inv_log;
    assert(!isnan(prob_log)); assert(!isinf(prob_log));
    //printf("validate:\t %f =? %f, at %s:%d\n", prob_log, log_range_0_one(prob), __FILE__, __LINE__);
    // assert((int)prob_log == (int)log_range_0_one(prob));
    H -= prob * prob_log;
#ifndef NDEBUG //! then we validate the correctness of this approach:
      const t_float exp_1 = debug_get_floatValue_approx(prob_log);
      const t_float exp_2 = debug_get_floatValue_approx(log(prob));
      if(exp_1 != exp_2) {
	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__);
      }
      assert(exp_1 == exp_2);
#endif
#else
    H -= prob * log_range_0_one(prob);
#endif
    // printf("[%d]\t prob=%f log(prob=%f,  --> %f at %s:%d\n", i, prob, log_range_0_one(prob), H,  __FILE__, __LINE__);
    //}
    assert(!isnan(H)); assert(!isinf(H));
  }
  assert(!isnan(H)); assert(!isinf(H));
  //! @return  
  return H;
}


//! ***************************************************************************************
//! ***************************************************************************************
//! ***************************************************************************************


/* Returns the entropy induced by the points on the partition 
 * <c_0, c_s, c_t>. See line 5 of Algorithm 2, SOM.
 *
 * Parameters
 *   c : c_1, ..., c_p
 *   s : s in c_s
 *   t : t in c_t
 */
function_prefix_funcHP3 t_float hp3_old(t_sparseVec *c, int s, int t)
{
  int sum;
  t_float prob, H = 0.0;  
  t_float total = (t_float) c[t-1];

  if (s == t) {
    return 0.0;
  }
#if optmize_use_multInsteadOf_div == 1
  const t_float total_inv = 1.0/total;
#endif
  
  if (c[s-1] != 0) {
    const t_float sum = c[s-1];
#if optmize_use_multInsteadOf_div == 1
    prob = (t_float) sum * total_inv;
#else
    prob = (t_float) sum / total;
#endif
#if optmize_use_logValues_fromLocalTable == 1
    //const t_float prob_log = debug_local_log_aboveZero(prob); 
    const t_float prob_log = log_index_fast(sum) - log_index_fast(total); 
/* #ifndef NDEBUG //! then we validate the correctness of this approach: */
/*       const t_float exp_1 = debug_get_floatValue_approx(prob_log); */
/*       const t_float exp_2 = debug_get_floatValue_approx(log(prob)); */
/*       if(exp_1 != exp_2) { */
/* 	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__); */
/*       } */
/*       assert(exp_1 == exp_2); */
/* #endif */
#else
    const t_float prob_log = log_range_0_one_abs(prob);
#endif
    H -= prob * prob_log;
  }
  
  sum = c[t-1] - c[s-1];
  if (sum != 0) {
#if optmize_use_multInsteadOf_div == 1
    prob = (t_float) sum * total_inv;
#else
    prob = (t_float) sum / total;
#endif
#if optmize_use_logValues_fromLocalTable == 1
    //const t_float prob_log = debug_local_log_aboveZero(prob); 
    const t_float prob_log = log_index_fast(sum) - log_index_fast(total); 
/* #ifndef NDEBUG //! then we validate the correctness of this approach: */
/*       const t_float exp_1 = debug_get_floatValue_approx(prob_log); */
/*       const t_float exp_2 = debug_get_floatValue_approx(log(prob)); */
/*       if(exp_1 != exp_2) { */
/* 	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__); */
/*       } */
/*       assert(exp_1 == exp_2); */
/* #endif */
#else
    const t_float prob_log = log_range_0_one_abs(prob);
#endif
    H -= prob * prob_log;
  }
  
  return H;
}

//! ***************************************************************************************
//! ***************************************************************************************
//! ***************************************************************************************


/* Returns the entropy induced by the points on the partition 
 * <c_0, c_s, c_t>, Q. See line 5 of Algorithm 2 in SOM.
 *
 * Parameters
 *   cumhist : cumulative histogram matrix along P_map
 *   c : c_1, ..., c_p
 *   q : number of rows of cumhist (number of partitions in Q_map)
 *   p : number of cols of cumhist (number of partitions in P_map)
 *   s : s in c_s
 *   t : t in c_t
 */
function_prefix_funcHP3 t_float hp3q_dataScheme_old(t_sparseVec **cumhist, t_sparseVec *c, int q, int p, int s, int t) {
  int i, sum;
  t_float prob, H = 0.0;
  

  t_float total = (t_float) c[t-1];    
#if optmize_use_multInsteadOf_div == 1
  const t_float total_inv = 1.0/total;
#endif

  /* printf("total_inv=%f, for q=%d,  at %s:%d\n", 1.0 /total, q, __FILE__, __LINE__); */

  for (i=0; i<q; i++)      {
    /* printf("# i=%d, at %s:%d\n", i, __FILE__, __LINE__); */
    t_float sum = cumhist[i][s-1];
    if (sum != 0) 	{
#if optmize_use_multInsteadOf_div == 1
      prob = (t_float) sum * total_inv;
      //printf("[%d]\tprob=%f, at %s:%d\n", i, prob, __FILE__, __LINE__);
#else
      prob = (t_float) sum / total;
#endif
#if optmize_use_logValues_fromLocalTable == 1 
      //const t_float prob_log = debug_local_log_aboveZero(prob); 
      const t_float prob_log = log_index_fast(sum) - log_index_fast(total);
/* #ifndef NDEBUG //! then we validate the correctness of this approach: */
/*       const t_float exp_1 = debug_get_floatValue_approx(prob_log); */
/*       const t_float exp_2 = debug_get_floatValue_approx(log(prob)); */
/*       if(exp_1 != exp_2) { */
/* 	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__); */
/*       } */
/*       assert(exp_1 == exp_2); */
/* #endif */
#else
      const t_float prob_log = log_range_0_one(prob);
#endif
      H -= prob * prob_log;
      /* printf("prob=%f, at %s:%d\n", prob, __FILE__, __LINE__); */
    }

    // FIXEM:[cricial]: include [below]

    sum = cumhist[i][t-1] - cumhist[i][s-1];
    if (sum != 0) 	{
#if optmize_use_multInsteadOf_div == 1
      prob = (t_float) sum * total_inv;
#else
      prob = (t_float) sum / total;
#endif
#if optmize_use_logValues_fromLocalTable == 1 
      //const t_float prob_log = debug_local_log_aboveZero(prob); 
      const t_float prob_log = log_index_fast(sum) - log_index_fast(total);
/* #ifndef NDEBUG //! then we validate the correctness of this approach: */
/*       const t_float exp_1 = debug_get_floatValue_approx(prob_log); */
/*       const t_float exp_2 = debug_get_floatValue_approx(log(prob)); */
/*       if(exp_1 != exp_2) { */
/* 	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__); */
/*       } */
/*       //printf("(cmp)\tvalue=%f, total=%f: %f VS %f w/rounded=(%f, %f), at %s:%d\n", (float)sum, (float)total, (float)prob_log, (float)log(prob), exp_1, exp_2, __FILE__, __LINE__); */
/*       assert(exp_1 == exp_2); */
/* #endif */
#else
      const t_float prob_log = log_range_0_one(prob);
#endif
      H -= prob * prob_log;
      //H -= prob * log_range_0_one_abs(prob);
    }
    //printf("prob=%f, at %s:%d\n", prob, __FILE__, __LINE__);
    //if(true) {printf("(info)\t Comptued: '%f' at %s:%d\n", (t_float)H,  __FILE__, __LINE__);}
  }
  ///if(true) {printf("(info)\t Comptued: '%f' at %s:%d\n", (t_float)H,  __FILE__, __LINE__);}
  
  return H;
}

// FIXME: write correctness-tests wrt. ["elow]
// FIXME: try to re-write ["elow] and then idnetify a use-case 'in our work'.
#if optmize_use_logValues_fromLocalTable == 1 
function_prefix_funcHP3 t_float hp3q_dataScheme_new(t_sparseVec **cumhist, t_float **cumhist_log, int q, int p, int s, int t, const t_float total, const t_float total_inv, const t_float total_inv_log) 
#else
  function_prefix_funcHP3 t_float hp3q_dataScheme_new(t_sparseVec **cumhist, int q, int p, int s, int t, const t_float total, const t_float total_inv) 
#endif
{
  t_float H = 0.0;   
  int i = 0;
/* #if optmize_use_multInsteadOf_div == 0 */
/*   const t_float total = 1/total_inv; */
/* #endif */
  //printf("total_inv=%f, for q=%d, at %s:%d\n", total_inv, q, __FILE__, __LINE__);
  // printf("investgiates cumhist-values in range[%d-1][0...%d], at %s:%d\n", s, q, __FILE__, __LINE__);
  for (; i<q; i++) {
    assert(!isnan(H));
    
    // TODO[log]: amek ["elow] function optional ... using a enw macro ... and then test correctness of 'differen strategies'. <-- the example is interesting  ... and wrt. exeuction-time
    //if(cumhist[s-1][i] > 0)
      {
#if optmize_use_multInsteadOf_div == 1
	float prob = (t_float) (cumhist[s-1][i] * total_inv); 
	//printf("[%d]\tprob=%f, at %s:%d\n", i, prob, __FILE__, __LINE__);
	assert(!isnan(cumhist[s-1][i]));
	assert(!isnan(prob));
#else
	float prob = (t_float) (cumhist[s-1][i] / total);
	assert(!isnan(prob)); 
#endif
#if optmize_use_logValues_fromLocalTable == 1 
	//const t_float prob_log = debug_local_log_aboveZero(prob); 
	const t_float prob_log = cumhist_log[s-1][i] + total_inv_log; //! ie, we then make use of pre-comptued log-values.
	// printf("[%d-1][%d]\t prob-log=%f, cumhist_log[][]=%f, total_inv_log=%f, at %s:%d\n", s, i, prob_log, cumhist_log[s-1][i], total_inv_log, __FILE__, __LINE__);
	assert(!isnan(prob_log));     assert(prob_log != INFINITY); assert(!isinf(prob_log));
#ifndef NDEBUG //! then we validate the correctness of this approach:
	if(cumhist[s-1][i] > 0)
	{ //! Valdiate that the "cumhist_log" is set correctly:
	  const t_float exp_1 = debug_get_floatValue_approx(cumhist_log[s-1][i]);
	  const t_float exp_2 = debug_get_floatValue_approx(log(cumhist[s-1][i]));
	  //printf("(cmp-[%u][%u])\t vlaue=%f:\t %f VS %f, at %s:%d\n", s-1, i, cumhist[s-1][i], cumhist_log[s-1][i], log(cumhist[s-1][i]), __FILE__, __LINE__);
	  if(exp_1 != exp_2) {
	    printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__);
	  }
	  assert(exp_1 == exp_2); 
	}
	/* { */
	/*   const t_float exp_1 = debug_get_floatValue_approx(prob_log); */
	/*   const t_float exp_2 = debug_get_floatValue_approx(log(prob)); */
	/*   if(prob_log != log(prob)) { */
	/*     printf("(cmp)\t (%d=%f VS %d=%f), for log(%f = %f*%f ) we have %f VS %f, givne 'imrpoved'=(%f + %f), at %s:%d\n", (int)prob_log, (float)prob_log, (int)log(prob), (float)log(prob), (float)prob, cumhist[s-1][i], total_inv_log, (float)exp_1, (float)exp_2,cumhist_log[s-1][i], total_inv_log, __FILE__, __LINE__); */
	/*   } */
	/*   assert((int)prob_log == (int)log(prob)); */
	/*   //assert(exp_1 == exp_2); */
	/* } */
#endif
#else
	const t_float prob_log = log_range_0_one_abs(prob);    
	assert(!isnan(prob_log));     assert(prob_log != INFINITY); assert(!isinf(prob_log));
#endif
	assert(!isnan(prob));     assert(!isnan(prob_log));     assert(!isnan(H)); assert(!isinf(prob_log));
    
	// printf("# [i=%d] = %f * %f, at %s:%d\n", i, prob, prob_log, __FILE__, __LINE__);
	H -= prob * prob_log;
	assert(!isnan(H));
      }
    // printf("[%d]\t prob=%f, curr=%f, log=%f, internal-log-index=%d, at %s:%d\n", i, prob, prob * log_range_0_one(prob), log_range_0_one(prob), __get_internalIndexOf_float(prob),  __FILE__, __LINE__);

    const int sum = cumhist[t-1][i] - cumhist[s-1][i];
    // TODO[log]: amek ["elow] function optional ... using a enw macro ... and then test correctness of 'differen strategies'. <-- the example is interesting 
    //if(sum > 0)
      {
#if optmize_use_multInsteadOf_div == 1
	const t_float prob = (t_float) sum * total_inv;
	assert(!isnan(prob));
#else
	const t_float prob = (t_float) sum / total;
	assert(!isnan(prob));
#endif


#if optmize_use_logValues_fromLocalTable == 1
	//prob = 0;
	const t_float prob_log = log_index_fast(sum) + total_inv_log; // log_index_fast(total);
	//    prob = log_index_fast(sum) - log_index_fast(total);
	assert(!isnan(prob_log));
#ifndef NDEBUG //! then we validate the correctness of this approach:
	if(sum > 0)
	{ //! Valdiate partialty in the result:
	  t_float exp_1 = debug_get_floatValue_approx(log_index_fast(sum));
	  const t_float exp_2 = debug_get_floatValue_approx(log(sum));
	  /* printf("\t(cmp) value=%f, (%f=%f) = (%f VS %f), at %s:%d\n", (float)sum,  */
	  /* 	 (float)log_index_fast(sum), (float)log(sum),  */
	  /* 	 exp_1, exp_2, __FILE__, __LINE__); */
	  if(exp_1 != exp_2) {
	    printf("%f VS %f, at %s:%d\n", log_index_fast(sum), (float)log(sum), __FILE__, __LINE__);
	  }
	  assert(exp_1 == exp_2);
	}
	if(cumhist[s-1][i] > 0)
	{  //! Valdiate correctness of teh "total_inv_log":
	  const t_float exp_1 = (t_float)debug_get_floatValue_approx(total_inv_log);
	  const t_float exp_2 = (t_float)debug_get_floatValue_approx(log(total_inv));
	  assert(exp_1 == exp_2);
	}
/* 	{ */
/* 	  t_float exp_1 = debug_get_floatValue_approx(prob); */
/* #if optmize_use_multInsteadOf_div == 1 */
/* 	  const t_float tmp_2 = (t_float)log(sum*total_inv); */
/* 	  const t_float tmp_2_alt = log(sum/total); */
/* 	  const t_float exp_2_alt = (t_float)debug_get_floatValue_approx(tmp_2_alt); */
/* 	  const t_float exp_2 = debug_get_floatValue_approx(tmp_2); */
/* 	  const t_float debug_part1 = log(sum); const t_float debug_part2 = log(total_inv); */
/* 	  const t_float tmp_3 = debug_part1 + debug_part2; */
/* 	  const t_float exp_3 = debug_get_floatValue_approx(tmp_3); */

/* 	  /\* // FIXME: .... seems like "exp_3 == exp_1" ... which is due to ....  *\/ */
/* 	  /\* printf("(cmp)\t for log(%f * %f) we have %f = (%f = %f + %f) VS %f (%f = %f = %f = %f + %f), at %s:%d\n", (float)sum, (float)total_inv,  *\/ */
/* 	  /\* 	 (float)exp_1, log_index_fast(sum) + total_inv_log, log_index_fast(sum), total_inv_log,  *\/ */
/* 	  /\* 	 (float)exp_2,  *\/ */
/* 	  /\* 	 exp_3, tmp_3, debug_part1 + debug_part2, debug_part1, debug_part2,  *\/ */
/* 	  /\* 	 __FILE__, __LINE__); *\/ */


/* 	  assert(exp_1 == exp_3);	   */
/* 	  assert( (int)(log_index_fast(sum) + total_inv_log) == (int)tmp_3); */
/* 	  /\* assert((int)_1 == (int)exp_2_alt);	   *\/ */
/* 	  /\* assert((int)exp_1 == (int)exp_2);	   *\/ */
/* #else */
/* 	  const t_float tmp_2_log = log(sum/total); */
/* 	  const t_float exp_2 = debug_get_floatValue_approx(tmp_2_log); */
/* 	  assert(exp_1 == exp_2); */
/* #endif */
/* 	} */
#endif
#else //! ----------------------------
	const t_float prob_log = log_range_0_one_abs(prob);	
	//! ---
#endif //! ----------------------------
	H -= prob * prob_log;

	assert(!isnan(H));
      }
    assert(!isnan(H));
  }
  // if(true) {printf("(info)\t Comptued: '%f' at %s:%d\n", (t_float)H,  __FILE__, __LINE__);} 
  assert(!isnan(H));
  return H;
}

//! Note: we expect the "q_intri" input-parameter to always be "q_intri > 0", ie, as the result 'of calling this fucntion' will othereisse be errnous.
#if optmize_use_logValues_fromLocalTable == 1 
function_prefix_funcHP3 t_float hp3q_dataScheme_new_intri(t_sparseVec **cumhist, t_float **cumhist_log, int q, int p, int s, int t, const t_float total_inv, const VECTOR_FLOAT_TYPE vec_total_inv, const VECTOR_FLOAT_TYPE vec_total_inv_log, const int q_intri)
#else
  function_prefix_funcHP3 t_float hp3q_dataScheme_new_intri(t_sparseVec **cumhist, int q, int p, int s, int t, const t_float total_inv, const VECTOR_FLOAT_TYPE vec_total_inv, const int q_intri) 
#endif
{  
  int i = 0;

#if optmize_use_multInsteadOf_div == 0
  const t_float total = (total_inv != 0) ? 1/total_inv : 0;
#endif

  // printf("q=%u, q_intri=%d, SPARSEVEC_ITER_SIZE=%d, macroConfig_use_posixMemAlign='%s', at %s:%d\n", q, q_intri, SPARSEVEC_ITER_SIZE, (macroConfig_use_posixMemAlign) ? "true" : "false", __FILE__, __LINE__);
  
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);;
  for (i=0; i < q_intri; i += SPARSEVEC_ITER_SIZE)  {
    const SPARSEVEC_TYPE vec_2 = local_SPARSEVEC_LOAD(&cumhist[s-1][i]);   
#if SPARSEVEC_CONFIG_DATATYPE == 0
    VECTOR_FLOAT_TYPE vec_prob = SPARSEVEC_convertTo_float(vec_2);
#else
    VECTOR_FLOAT_TYPE vec_prob = vec_2;
#endif
    vec_prob = VECTOR_FLOAT_MUL(vec_prob, vec_total_inv); //! ie, "t_float prob = (t_float) sum * total_inv;"
#if optmize_use_logValues_fromLocalTable == 1 
    VECTOR_FLOAT_TYPE vec_mappedTo_function = VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&cumhist_log[s-1][i]), vec_total_inv_log); //! ie, "log(cumhist[s-1][i]) + log(total_inv)"
#else
    VECTOR_FLOAT_TYPE vec_mappedTo_function = log_range_vector_get_memoryLocation(vec_prob); //! ie, get the 'adjusted' indexes, thereby makign use of our "log_range_0_one_inputIsAdjusted(..)"
    //VECTOR_FLOAT_PRINT(vec_mappedTo_function); //! ok <-- produce correct resutls
    vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted);
#endif
    //VECTOR_FLOAT_TYPE vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_prob, log_range_0_one);
    //! Update the result wrt. [ªbove] 'shannons entropy':
    // VECTOR_FLOAT_PRINT(vec_prob); //! ok <-- produce correct resutls
    //printf("mapped-function= "); VECTOR_FLOAT_PRINT(vec_mappedTo_function);
    // VECTOR_FLOAT_PRINT(VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function));

    vec_result = VECTOR_FLOAT_SUB(vec_result, VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function));


    // FIXEM:[cricial]: include [below]

    //! The second computation:
    // FIXME: wrt. ["elow] figure out why the 'input-memory' is not correctly allocated.
    const SPARSEVEC_TYPE vec_1 = local_SPARSEVEC_LOAD(&cumhist[t-1][i]); 
    SPARSEVEC_TYPE vec_sum = SPARSEVEC_SUB(vec_1, vec_2); //! ie, "const int sum = cumhist[t-1][i] - cumhist[s-1][i];"


    // FIXME: validate correctness of using the 'absoltue value' <-- otherise update our "log_range_0_one_abs(..)" to 'allow' for the use of negative valeus.
    // vec_sum = SPARSEVEC_ABS(vec_sum);
#if SPARSEVEC_CONFIG_DATATYPE == 0
    VECTOR_FLOAT_TYPE vec_sum_float = SPARSEVEC_convertTo_float(vec_sum);
#else
    VECTOR_FLOAT_TYPE vec_sum_float = vec_sum;
#endif
    

#if SPARSEVEC_CONFIG_DATATYPE == 0
    vec_prob = SPARSEVEC_convertTo_float(vec_sum);
#else
    vec_prob = vec_sum;
#endif
    vec_prob = VECTOR_FLOAT_MUL(vec_prob, vec_total_inv); //! ie, "t_float prob = (t_float) sum * total_inv;"

#if optmize_use_logValues_fromLocalTable == 1
	// FIXME: update ["elow] when we have written effeicnt code for " >= 0".
    // FIXME: update ["elow] ... 
    // assert(false); // FIXME: ... wrtie code to set to zero valeus <= 0
    //vec_sum_float; //, log_range_vector_get_memoryLocation_abs(vec_prob); //! ie, get the 'adjusted' indexes, thereby makign use of our "log_range_0_one_inputIsAdjusted(..)"
    
    vec_mappedTo_function = VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_sum_float);

    //assert(false); // FIXME: udpate this function ... include ["elow]
    vec_mappedTo_function = VECTOR_FLOAT_ADD(vec_mappedTo_function, vec_total_inv_log); //! ie, #log(sum / total) = log(sum) - log(total) = log(sum) + log(1/total)"


    //vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted_abs);
#else
    vec_mappedTo_function = log_range_vector_get_memoryLocation_abs(vec_prob); //! ie, get the 'adjusted' indexes, thereby makign use of our "log_range_0_one_inputIsAdjusted(..)"
    vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted_abs);
#endif
    //! Update the result wrt. [ªbove] 'shannons entropy':
    vec_result = VECTOR_FLOAT_SUB(vec_result, VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function));
  }

  // assert(false); // FIXME: include [above] when memory-error is resolved.

  t_float H = 0.0;   

  // FIXME: udpate ["elow] wrti the "optmize_use_logValues_fromLocalTable" 'case'.

  // printf("-------------\ni=%d, q=%d, q_intri=%d, at %s:%d\n", i, q, q_intri, __FILE__, __LINE__);
#if optmize_use_logValues_fromLocalTable == 1
  const t_float total_inv_log = VECTOR_FLOAT_getAtIndex(vec_total_inv_log, /*index=*/0);
#endif
  for (; i<q; i++) {
#if optmize_use_multInsteadOf_div == 1
    float prob = (t_float) (cumhist[s-1][i] * total_inv);
#else
    float prob = (t_float) (cumhist[s-1][i] / total);
#endif
#if optmize_use_logValues_fromLocalTable == 1 
    const t_float prob_log = cumhist_log[s-1][i] + total_inv_log; //! ie, we then make use of pre-comptued log-values.
#ifndef NDEBUG //! then we validate the correctness of this approach:
    if(prob > 0) {
      t_float exp_1 = debug_get_floatValue_approx(prob_log);
      const t_float exp_2 = debug_get_floatValue_approx(log(prob));
      if(exp_1 != exp_2) {
	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__);
      }
      assert(exp_1 == exp_2);
    }
#endif
#else
    const t_float prob_log = log_range_0_one(prob);    
#endif
    /* printf("# i=%d, at %s:%d\n", i, __FILE__, __LINE__); */
    H -= prob * prob_log;

    // printf("[%d]\t prob=%f, curr=%f, log=%f, internal-log-index=%d, at %s:%d\n", i, prob, prob * log_range_0_one(prob), log_range_0_one(prob), __get_internalIndexOf_float(prob),  __FILE__, __LINE__);

    const int sum = cumhist[t-1][i] - cumhist[s-1][i];
#if optmize_use_logValues_fromLocalTable == 1
    //prob = 0;
    //if(prob > 0) {
    prob = log_index_fast(sum) + total_inv_log; // log_index_fast(total);
#ifndef NDEBUG //! then we validate the correctness of this approach:
    if(prob > 0) {
      const t_float exp_1 = debug_get_floatValue_approx(prob);
#if optmize_use_multInsteadOf_div == 1
      const t_float exp_2 = debug_get_floatValue_approx(log(sum*total_inv));
#else
      const t_float exp_2 = debug_get_floatValue_approx(log(sum/total));
#endif
      if(exp_1 != exp_2) {
	printf("%f VS %f, at %s:%d\n", prob_log, (float)log(prob), __FILE__, __LINE__);
      }
      assert(exp_1 == exp_2);
    }
#endif
      //}
#else //! ----------------------------
#if optmize_use_multInsteadOf_div == 1
    prob = (t_float) sum * total_inv;
#else
    prob = (t_float) sum / total;
#endif
    H -= prob * log_range_0_one_abs(prob);
#endif //! ----------------------------
    /* float prob = (t_float) cumhist[s-1][i] * total_inv; */
    /* H -= prob * log_range_0_one(prob);     */
    /* // // FIXEM:[cricial]: include [below */
    /* const t_sparseVec sum = cumhist[t-1][i] - cumhist[s-1][i]; */
    /* prob = (t_float) sum * total_inv; */
    /* H -= prob * log_range_0_one_abs(prob); */
  }
  // FIXME: udpate ["elow] ... consider writing a new macro named VECTOR_FLOAT_SET1
  // FIXME: wrtie a correctness-test for ["elow] macro
  // printf("H=%f, at %s:%d\n", H, __FILE__, __LINE__);
  // printf("result=%f before merge, at %s:%d\n", VECTOR_FLOAT_storeAnd_horizontalSum(vec_result), __FILE__, __LINE__);   VECTOR_FLOAT_PRINT(vec_result);

  //vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_SET1_SINGLE(H)); //! ie, where "ADD" is due to the 'negative sums'.  <-- to understand the latter, note that: assert( (-1 + -1) == -2)

  //! @return
  const t_float scalar_result = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);  
  // FIXME: remvoe ["elow]
  // assert(scalar_result < H); //! ie, due to the 'negative sums'. 
  /* printf("scalar_result=%f, H=%f, at %s:%d\n", scalar_result, H, __FILE__, __LINE__); */
  return (scalar_result + H); //! where "H" is 'negative by default', 
}



//! ***************************************************************************************
//! ***************************************************************************************
//! ***************************************************************************************


/* Returns the entropy induced by the points on the partition <c_s, c_t>
 * and Q. See line 13 of Algorithm 2, SOM.
 *
 * Parameters
 *   cumhist : cumulative histogram matrix along P_map
 *   c : c_1, ..., c_p
 *   q : number of rows of cumhist (number of partitions
 *       in Q_map)
 *   p : number of cols of cumhist (number of partitions
 *       in P_map)
 *   s : s in c_s
 *   t : t in c_t
 */
function_prefix_funcHP3 t_float hp2q_dataScheme_old(t_sparseVec **cumhist, t_sparseVec *c, int q, int p, int s, int t) {
  int i, sum;
  t_float prob, H = 0.0 ;
  
  
  t_float total = (t_float) (c[t-1] - c[s-1]);
  
  if (s == t) {return 0.0;}
  
#if optmize_use_multInsteadOf_div == 1
  const t_float total_inv = 1.0/total;
#endif

  if(total == 0) {return 0.0;} //! which is new-added in our implemetnaiton in order to handle the INFINITY case (oekseth, 06. june 2016).
  // FIXME: try to re-write ["elow] and then idnetify a use-case 'in our work'.

  // FIXME[ªrticle] 

  for (i=0; i<q; i++)      {
    sum = cumhist[i][t-1] - cumhist[i][s-1];
    if (sum != 0) 	{
#if optmize_use_multInsteadOf_div == 1
      prob = (t_float) sum * total_inv;
#else
      prob = (t_float) sum / total;
#endif
#if optmize_use_logValues_fromLocalTable == 1 
      const t_float prob_log = log_index_fast(sum) - log_index_fast(total);
#else
      const t_float prob_log = log_range_0_one(prob);
#endif
      H -= prob * prob_log;
    }
  }  
  return H;
}




// FIXME: write a correctness-test comparing 'this' to "hp2q_dataScheme_old(..)"
//! @remarks parameter-range in ["elow] macro is: "t_float hp2q_dataScheme_new(const int start_pos, t_sparseVec **cumhist, t_sparseVec *c, int q, int p, int s, int t);"
//! @remarks the "start_pos" param is used to simplify 'integration' with our "hp2q_dataScheme_new_intri(..)"
//#define hp2q_dataScheme_new(start_pos, cumhist, c, q, p, s, t) ({	
#if optmize_use_logValues_fromLocalTable == 1 
function_prefix_funcHP3 t_float hp2q_dataScheme_new(const int start_pos, t_sparseVec **cumhist,  t_sparseVec *c, t_float *c_log, int q, int p, int s, int t) {
  t_float H = 0.0 ; const t_float total = (t_float) (c[t-1] - c[s-1]); 
  if(total == 0) {return 0.0;} //! ie, as the resutl will othersie always be "0", ie, due to ..
  const t_float total_inv = 1 / total; 
  const t_float total_log = log_index_fast(total);
  // printf("for total=%d, compare log-values %f VS %f, at %s:%d\n", (int)total, total_log, log(total), __FILE__, __LINE__);
  // assert((int)total_log == (int)log(total)); //! ie, what we expect.
  for (int i = start_pos; i<q; i++)  { 
    // printf("[%d/%d], given (s, t)=(%d, %d) w/score=(%f, %f), at %s:%d\n", i, q, s, t, cumhist[t-1][i], cumhist[s-1][i], __FILE__, __LINE__); 
    H -= hp2q_dataScheme_new_item(cumhist, t, s, i, total_inv, total_log);
  } 
  // H;})
  return H;
}
//! --------------------------------
#else //! then we use 'lookup' for the ifnerred log-value, a float-vlaue expected to be in range [-1, 1],
function_prefix_funcHP3 t_float hp2q_dataScheme_new(const int start_pos, t_sparseVec **cumhist, t_sparseVec *c, int q, int p, int s, int t) {
  t_float H = 0.0 ; const t_float total = (t_float) (c[t-1] - c[s-1]); const t_float total_inv = 1 / total; 
  if(total == 0) {return 0.0;} //! ie, as the resutl will othersie always be "0", ie, due to ..
  //printf("evalaute in range [%d, %d], at %s:%d\n", start_pos, q, __FILE__, __LINE__);
  for (int i = start_pos; i<q; i++)  { 
    // printf("[%d/%d], given (s, t)=(%d, %d) w/score=(%f, %f), at %s:%d\n", i, q, s, t, cumhist[t-1][i], cumhist[s-1][i], __FILE__, __LINE__); 
#if optmize_use_multInsteadOf_div == 1
    H -= hp2q_dataScheme_new_item(cumhist, t, s, i, total_inv);
#else //! ei, then we evlauate the time-cost of using "divison" instead of "muliplicaiton":
    H -= hp2q_dataScheme_new_item_div(cumhist, t, s, i, total);
#endif
  } 
  // H;})
  return H;
}
#endif
//#endif //!*************************************************


// FIXME: write a correctness-test comparing 'this' to "hp2q_dataScheme_old(..)"
#if optmize_use_logValues_fromLocalTable == 1 
function_prefix_funcHP3 t_float hp2q_dataScheme_new_intri(t_sparseVec **cumhist, t_sparseVec *c, t_float *c_log, int q, int p, int s, int t, const int q_intri) 
#else
function_prefix_funcHP3 t_float hp2q_dataScheme_new_intri(t_sparseVec **cumhist, t_sparseVec *c, int q, int p, int s, int t, const int q_intri) 
#endif
{    
  const t_float total = (t_float) (c[t-1] - c[s-1]); 
  if(total == 0) {return 0.0;} //! ie, as the resutl will othersie always be "0", ie, due to ..

  const t_float total_inv = 1 / total;
  const VECTOR_FLOAT_TYPE vec_totalInv = VECTOR_FLOAT_SET1(total_inv);

#if optmize_use_logValues_fromLocalTable == 1 
  const t_float total_log = log_index_fast(total);
  //assert((int)total_log == (int)log(total));
  const VECTOR_FLOAT_TYPE vec_total_log = VECTOR_FLOAT_SET1(total_log);
#endif

  //! Iterate, infer shabnnons entropy, and for each 'shannon' compute the sum of 'subtractions'
  VECTOR_FLOAT_TYPE vec_sum_global = VECTOR_FLOAT_SET1(0);
  int i = 0;  
  for (i=0; i < q_intri; i += SPARSEVEC_ITER_SIZE)  {
    // FIXME: wrt. ["elow] figure out why the 'input-memory' is not correctly allocated.
    const SPARSEVEC_TYPE vec_1 = local_SPARSEVEC_LOAD(&cumhist[t-1][i]);
    const SPARSEVEC_TYPE vec_2 = local_SPARSEVEC_LOAD(&cumhist[s-1][i]);
    SPARSEVEC_TYPE vec_sum = SPARSEVEC_SUB(vec_1, vec_2); //! ie, "const int sum = cumhist[t-1][i] - cumhist[s-1][i];"
    // FIXME: validate correctness of using the 'absoltue value' <-- otherise update our "log_range_0_one_abs(..)" to 'allow' for the use of negative valeus.
    // vec_sum = SPARSEVEC_ABS(vec_sum);
    
    // FIXME: in ["elow] how may we handle '16-bit' and '8-bit' integers?
#if SPARSEVEC_CONFIG_DATATYPE == 0
    VECTOR_FLOAT_TYPE vec_prob = SPARSEVEC_convertTo_float(vec_sum);
    VECTOR_FLOAT_TYPE vec_sum_float = vec_prob; //! ie, a deep copy similar to calling "SPARSEVEC_convertTo_float(vec_sum);"
#else
    VECTOR_FLOAT_TYPE vec_prob = vec_sum;
    VECTOR_FLOAT_TYPE vec_sum_float = vec_sum;
#endif



    vec_prob = VECTOR_FLOAT_MUL(vec_prob, vec_totalInv); //! ie, "t_float prob = (t_float) sum * total_inv;"

#if optmize_use_logValues_fromLocalTable == 1
    VECTOR_FLOAT_TYPE vec_mappedTo_function = VECTOR_FLOAT_fast_log_VECTOR_FLOAT_fast_get_logFor_vec(vec_sum_float);
    vec_mappedTo_function = VECTOR_FLOAT_SUB(vec_mappedTo_function, vec_total_log); //! ie, #log(sum / total) = log(sum) - log(total)"
#else
    VECTOR_FLOAT_TYPE vec_mappedTo_function = log_range_vector_get_memoryLocation_abs(vec_prob); //! ie, get the 'adjusted' indexes, thereby makign use of our "log_range_0_one_inputIsAdjusted(..)" 
    vec_mappedTo_function = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec_mappedTo_function, log_range_0_one_inputIsAdjusted_abs); 
#endif
    vec_prob = VECTOR_FLOAT_MUL(vec_prob, vec_mappedTo_function);
    //! Note: in ["elow] we use " += " instead of " -= " to reduce the number of macros in oru "def_intri.h", ie, to simplify maintainance and correctness-evaluation.
    vec_sum_global = VECTOR_FLOAT_SUB(vec_sum_global, vec_prob); //! ie, "H -= prob;" or "H -= prob * log_range_0_one_abs(prob);"
    //vec_sum_global = VECTOR_FLOAT_ADD(vec_sum_global, vec_prob); //! ie, "H -= prob;" or "H -= prob * log_range_0_one_abs(prob);"
    //vec_sum = VECTOR_FLOAT_ADD(vec_sum, vec_prob); //! ie, "H -= prob;" or "H -= prob * log_range_0_one_abs(prob);"
  }

  //! Update the values:
  t_float H = VECTOR_FLOAT_storeAnd_horizontalSum_subtract(vec_sum_global); 
  // printf("intri-end-pos=%d, q=%d, q_intri=%d, float=%f, at %s:%d\n", i, q, q_intri, H, __FILE__, __LINE__);
  //const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum_global);  H = sumOf_values * -1; //! ie, as the 'default veriso' use " -= " insteaf of our 'standarized' " += "

  //! Complete, ie, comptue the 'trail':
  for (; i<q; i++)  { 
    const t_sparseVec sum = cumhist[t-1][i] - cumhist[s-1][i]; 
    t_float prob = (t_float) sum * total_inv;    
#if optmize_use_logValues_fromLocalTable == 1
    const t_float prob_log = log_index_fast(sum) - total_log; //log_range_0_one_abs(prob); 
#else
    const t_float prob_log = log_range_0_one_abs(prob); 
#endif
    H -= prob * prob_log;
  } 
  return H; // + hp2q_dataScheme_new(/*start_pos=*/i, cumhist, c, q, p, s, t);
}




//! ***************************************************************************************
//! ***************************************************************************************
//! ***************************************************************************************

//! Validate consistency between "hq_old" and "hq_improved".
void assert_hq();
//! Validate consistency between "" and "".
void assert_hp3();
//! Validate consistency between "" and "".
void assert_hp3q();
//! Validate consistency between "" and "".
void assert_hp2q();
//! Validate consistency between "" and "".
void assert_F();


//! ******************************************************************************************
#endif //! EOF
