#ifndef hp_image_segment_h
#define hp_image_segment_h
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "kt_matrix.h"
#include "kt_aux_matrix_binning.h"
/**
   @struct s_hp_image_pixel
   @brief hold information for a given image pixel (oekseth, 06. feb. 2018).
 **/
typedef struct s_hp_image_pixel {
  uint pos_row;
  uint pos_col;
  unsigned char RGB[3]; //! ie, "Red", "Green", and "Blue".
} s_hp_image_pixel_t;

//! @returen an 'empty' veriosn of the "s_hp_image_pixel_t" object (oekseth, 06. feb. 2018).
s_hp_image_pixel_t init__s_hp_image_pixel_t();

//! Extract the RGB value from the text-string of: "9,0: (138,138,146)  #8A8A92  rgb(138,138,146)"
//! @return the identifed object.
s_hp_image_pixel_t extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(const char *row_input);
/**
   @brief export the s_hp_image_pixel_t object to a result file.
   @remarks correctness of this funcitonr equires cosnistency with the "extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(..)" function.,
 **/
void writeOut__toFile__format_imageMagicText_s_hp_image_pixel_t(s_hp_image_pixel_t self, FILE *file_out) ;

//! Idneitfy the Hue, SAturaion and Value of an RGB pixel (oekseht, 06. okt. 2017).
void RGBtoHSV__s_hp_image_pixel_t_( s_hp_image_pixel_t self, t_float *hue, t_float *saturation, t_float *value ) ;

/**
   @struct s_hp_image_segment
   @brief hold results of parsing an RGB file of data.
 **/
typedef struct s_hp_image_segment {
  s_kt_matrix_base_t mat_RGB[3];
} s_hp_image_segment_t;

//! @reutnr an intiatd verrison of the "s_hp_image_segment_t" object (oekseht, 06. feb . 2018).
s_hp_image_segment_t init__s_hp_image_segment_t();
//! @return a deep copy of the cpy object
s_hp_image_segment_t copy__s_hp_image_segment_t(const s_hp_image_segment_t *cpy);

//! DE-allocates the "s_hp_image_segment_t" object (oekseth, 06. feb. 2018).
void free__s_hp_image_segment_t(s_hp_image_segment_t *self);

/**
   @brief loads a PPM-file into memroy
   @remarks
   -- motivaiton: expeiremtns nidnciates that it takes considerably shroter time to generate 'ppm-files' than txt-fiels (when using ImageMaci's convert tool as a benchmark).
 */
s_hp_image_segment_t ppm_import__s_hp_image_segment_t(const char *input_file);
/**
   @brief loads a txt-file into memroy
 */
s_hp_image_segment_t txt_import__s_hp_image_segment_t(const char *input_file);
/**
   @brief Export the object to a given file (oekseth, 06. feb. 2018).
   @param <input_file> is the file name to exprot the data to.
   @remarks uses the "Imagemagic" software for the export routines.
   @return the object which holds the input data.
**/
s_hp_image_segment_t import__s_hp_image_segment_t(const char *input_file);

void ppm_export__s_hp_image_segment_t(s_hp_image_segment_t *self, const char *result_file);
//! Exprot image throguh a txt-intermidary
void txt_export__s_hp_image_segment_t(s_hp_image_segment_t *self, const char *result_file);
/**
   @brief Export the object to a given file (oekseth, 06. feb. 2018).
   @param <self> is the object which holds the input data.
   @param <result_file> is the file name to exprot the data to.
   @param <format_export> the file format to expoert data to.
   @remarks uses the "Imagemagic" software for the export routines.
**/
void export__s_hp_image_segment_t(s_hp_image_segment_t *self, const char *result_file); //, const char *format_export);

//! @return a matrix which hold the 'Hue' component of the input file.
s_kt_matrix_base_t fetch_Hue_s_hp_image_segment_t(s_hp_image_segment_t *self);

/**
   @enum
   @brief provides a genlairzed description of different strategies to construct features from a given matrix
 **/
typedef enum e_hp_image_segment {
  e_hp_image_segment_RGB,
  e_hp_image_segment_HSV,
  e_hp_image_segment_Hue,
  //! Note: the conceptual idea for the "hue_relative" is to use 'relative hue-coordinaes' ... eg, for "score(x, y) = score(y, y)/score(x, x)" .... where stnreht of the latter conserns .... 
  e_hp_image_segment_Hue_relative,
  //  e_hp_image_segment_,
  e_hp_image_segment_undef
} e_hp_image_segment_t;

//! @return a short (ie, huamnised) stringr-repserntiaoin of the enum
static const char *getStr_e_hp_image_segment(const e_hp_image_segment_t e) {
  if(e == e_hp_image_segment_RGB) {return "RGB";}
  else if(e == e_hp_image_segment_HSV) {return "HSV";}
  else if(e == e_hp_image_segment_Hue) {return "Hue";}
  else if(e == e_hp_image_segment_Hue_relative) {return "HueRelative";}
  //else if(e == ) {return "";}
  //else if(e == ) {return "";}
  else {return NULL;}
}

/**
   @brief construct a feature amtrix from an image (oekseth, 06. feb. 2018).
   @param <self> the object which hold the pixels
   @param <enum_id> the type of data to include
   @param <radius> the max distance to the neibhour pixels to be included: a 'radius' is used as a threshold wrt. which neighbour scores are used/incldued
   @return a new feature matrix where each row relates directly to a cell/pixel in the matrix, and where insertion order is [rows][columns].
   @remarks
   -- future: <scaling> the adjsutment factor to be applied.
 **/
s_kt_matrix_base_t fetch_features__s_hp_image_segment_t(s_hp_image_segment_t *self, const e_hp_image_segment_t enum_id, uint radius);
//s_kt_matrix_base_t fetch_features__s_hp_image_segment_t(s_hp_image_segment_t *self, const e_hp_image_segment_t enum_id, uint radius, t_float scaling);

/**
   @brief constructs a 'blurred' veriosn of a given matrix (oekseth, 0p6. feb. 2018).
   @param <self> the input matrix.
   @param <enum_id> the type of blur funciton to apply.
   @param <radius> the radius of the 'blur-field' to apply.
   @return a 'blurred' object.
 **/
s_hp_image_segment_t apply_blur__s_hp_image_segment_t(s_hp_image_segment_t *self, const e__blur enum_id, const t_float radius);

/**
   @brief constructs a 'normalied' veriosn of a given matrix (oekseth, 0p6. feb. 2018).
   @param <self> the input matrix.
   @param <enum_id> the type of normalization funciton to apply.
   @return a 'normalized' object.
 **/
s_hp_image_segment_t apply_blur__s_hp_image_segment_t(s_hp_image_segment_t *self, const e_kt_normType_t enum_id);

/**
   @brief constructs a 'normalied' veriosn of a given matrix (oekseth, 0p6. feb. 2018).
   @param <self> the input matrix.
   @param <cnt_bins> is the number of 'buckets' to be used wrt. the histogram-cosntruciton
   @param <isTo_forMinMax_useGlobal> which if set to false impleis taht the 'buckets' are ajduted locally for each row.
   @param <enum_id_scoring> the type of scores to be stored in the histogram.
   @return a 'binned' object.
 **/
s_hp_image_segment_t apply_binning__s_hp_image_segment_t(s_hp_image_segment_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring);

#endif //! EOF
