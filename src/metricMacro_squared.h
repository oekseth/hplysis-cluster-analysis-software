/* #ifndef metricMacro_squared_h */
/* #define metricMacro_squared_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_squared
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "squared".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics


//! @return the correlations-core for: squared euclid.
#define correlation_macros__distanceMeasures__squared__Euclid(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float mult = diff * diff; \
  diff; }) //! ie, return 
#define correlation_macros__distanceMeasures__squared__Euclid__postProcess(value) ({metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__squared__Euclid__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1 , term2);	\
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      diff; }) //! ie, return 
//! @return the correlations-core for:  squared Persons
#define correlation_macros__distanceMeasures__squared__Pearson(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float mult = diff * diff; \
  const t_float input_result = ( (diff != 0) & (term2 != 0)) ? diff / term2 : 0; \
  input_result; }) //! ie, return
#define correlation_macros__distanceMeasures__squared__Pearson__postProcess(value) ({metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__squared__Pearson__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1 , term2);	\
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(diff, term2);	\
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(input_result);	\
      input_result; }) //! ie, return

//! @return the correlations-core for:  Neyman's Chi
#define correlation_macros__distanceMeasures__squared__Neyman(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float mult = diff * diff; \
  const t_float input_result = ( (diff != 0) & (term1 != 0)) ? diff / term1 : 0; \
  input_result; }) //! ie, return
#define correlation_macros__distanceMeasures__squared__Neyman__postProcess(value) ({metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__squared__Neyman__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1 , term2);	\
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(diff, term1);	\
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(input_result);	\
      input_result; }) //! ie, return

//! @return the correlations-core for:  "Squared Chi" and "Probabilistic Chi"
#define correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float mult = diff * diff; \
  const t_float denumerator = term1 + term2; \
  const t_float input_result = (denumerator != 0) ? diff / denumerator : 0;	\
  input_result; }) //! ie, return
#define correlation_macros__distanceMeasures__squared__squaredChi_or_probabilisticChi__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1 , term2);	\
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1 , term2); \
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(diff , denumerator); \
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result; }) //! ie, return
#define correlation_macros__distanceMeasures__squared__squaredChi__postProcess(value) ({metric_macro_defaultFunctions__postProcess__none(value);})

//! @return the correlations-core for: divergence
#define correlation_macros__distanceMeasures__squared__divergence(term1, term2) ({ \
      const t_float diff = term1 - term2;				\
      const t_float mult = diff * diff;					\
      t_float denumerator = term1 + term2;				\
      denumerator = denumerator * denumerator;				\
      const t_float input_result = (denumerator != 0) ? diff / denumerator : 0;	\
      input_result; }) //! ie, return
#define correlation_macros__distanceMeasures__squared__divergence__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1 , term2);	\
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2);	\
      denumerator = VECTOR_FLOAT_MUL(denumerator, denumerator);	\
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(diff, denumerator); \
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result; }) //! ie, return

#define correlation_macros__distanceMeasures__squared__probabilistic_or_divergence__postProcess(value) ({metric_macro_defaultFunctions__postProcess__multiplyBy_constant(value, /*constant=*/2);})

//! @return the correlations-core for: "Clark": differs from "divergence" wrt. the use of "abs"
#define correlation_macros__distanceMeasures__squared__Clark(term1, term2) ({ \
      const t_float diff = mathLib_float_abs(term1 - term2);		\
      const t_float mult = diff * diff;					\
      t_float denumerator = term1 + term2;				\
      denumerator = denumerator * denumerator;				\
      const t_float input_result = ( (diff != 0) & (denumerator != 0) ) ? diff / denumerator : 0; \
      input_result; }) //! ie, return
#define correlation_macros__distanceMeasures__squared__Clark__postProcess(value) ({ \
  const t_float l_value = mathLib_float_abs(value); /*! Whcih is used/inlcudded to handle negative scores (oesketh, 06. june. 2017) */ \
  /*assert(l_value >= 0); // where latter will Not hold for Nan-valeus and inf-valeus, hence the latter is un-commend (oekseth, 06. june 2017) **/						\
  const t_float input_result = mathLib_float_sqrt(l_value); \
  metric_macro_defaultFunctions__postProcess__validateEmptyCase(input_result);}) //! ie, return.
#define correlation_macros__distanceMeasures__squared__Clark__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2);	\
      denumerator = VECTOR_FLOAT_MUL(denumerator, denumerator);	\
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(diff, denumerator); \
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(input_result);	\
      input_result; }) //! ie, return

//! @return the correlations-core for: Additative Symmetric squared Chi; 
#define correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi(term1, term2) ({ \
   const t_float diff = term1 - term2;				\
   const t_float mult_diff = diff * diff;				\
   const t_float sum  = term1 + term2; \
   const t_float numerator = mult_diff * sum; \
   const t_float denumerator = term1 * term2; \
   const t_float input_result = ( (numerator != 0) & (denumerator != 0) ) ? numerator / denumerator : 0; \
   input_result;}) //! ie, return.
#define correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi__postProcess(value) ({metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__squared__additativeSymmetriSquaredChi__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1, term2);	\
      const VECTOR_FLOAT_TYPE mult_diff = VECTOR_FLOAT_MUL(diff, diff);	\
      const VECTOR_FLOAT_TYPE sum  = VECTOR_FLOAT_ADD(term1, term2);	\
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MUL(mult_diff, sum); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MUL(term1, term2); \
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(input_result);	\
      input_result;}) //! ie, return.


//! A generic fucntion-wrapper for "Pearsons Product Moment" ("https://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient")
#define correlation_macros__distanceMeasures__squared__PearsonProductMoment(term1, term2, obj) ({ \
      obj.denumeratorLimitedTo_row1 = macro_pluss(obj.denumeratorLimitedTo_row1, term1); \
      obj.denumeratorLimitedTo_row2 = macro_pluss(obj.denumeratorLimitedTo_row2, term2); \
      obj.denumeratorLimitedTo_row_1and2 = macro_pluss(obj.denumeratorLimitedTo_row_1and2, macro_mul(term1, term2)); \
      /*---*/								\
      obj.numerator   = macro_pluss(obj.numerator, macro_mul(term1, term1)); \
      obj.denumerator = macro_pluss(obj.denumerator, macro_mul(term2, term2)); \
    })

#define correlation_macros__distanceMeasures__squared__PearsonProductMoment__SSE(term1, term2, obj) ({ \
      obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, term1); \
      obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, term2); \
      obj.denumeratorLimitedTo_row_1and2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row_1and2, VECTOR_FLOAT_ADD(term1, term2)); \
      /*---*/								\
      obj.numerator   = VECTOR_FLOAT_ADD(obj.numerator, VECTOR_FLOAT_ADD(term1, term1)); \
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, VECTOR_FLOAT_ADD(term2, term2)); \
    })


#define correlation_macros__distanceMeasures__squared__PearsonProductMoment_generic__postProcess(obj) ({ \
  const t_float sum1   = obj.denumeratorLimitedTo_row1;			\
  const t_float sum2   = obj.denumeratorLimitedTo_row2;			\
  t_float input_result  = obj.denumeratorLimitedTo_row_1and2;			\
  t_float tweight = (t_float)obj.cnt_interesting_cells;			\
  /*---*/					     \
  t_float denom1 = obj.numerator;		     \
  t_float denom2 = obj.denumerator;		     \
  /*---*/					     \
  if(tweight) { \
  input_result -= sum1 * sum2 / tweight; \
  denom1 -= sum1 * sum1 / tweight; \
  denom2 -= sum2 * sum2 / tweight; \
  if( (denom1 > 0) &&  (denom2 > 0) ) { \
  input_result = input_result / mathLib_float_sqrt(denom1*denom2); \
  input_result = 1. - input_result;				       \
  } else {input_result = metricMacro__constants__defaultValue__postProcess__allMatches;} \
  } else {input_result = metricMacro__constants__defaultValue__postProcess__allMatches;} \
  input_result; })


#define correlation_macros__distanceMeasures__squared__PearsonProductMoment_absolute__postProcess(obj) ({ \
  const t_float sum1   = obj.denumeratorLimitedTo_row1;			\
  const t_float sum2   = obj.denumeratorLimitedTo_row2;			\
  t_float input_result  = obj.denumeratorLimitedTo_row_1and2;			\
  t_float tweight = (t_float)obj.cnt_interesting_cells;			\
  /*---*/					     \
  t_float denom1 = obj.numerator;		     \
  t_float denom2 = obj.denumerator;		     \
  /*---*/					     \
  if(tweight) { \
  input_result -= sum1 * sum2 / tweight; \
  denom1 -= sum1 * sum1 / tweight; \
  denom2 -= sum2 * sum2 / tweight; \
  if( (denom1 > 0) &&  (denom2 > 0) ) { \
    input_result = mathLib_float_abs(input_result) / mathLib_float_sqrt(denom1*denom2); \
    input_result = 1. - input_result;						\
  } else {input_result = metricMacro__constants__defaultValue__postProcess__allMatches;} \
  } else {input_result = metricMacro__constants__defaultValue__postProcess__allMatches;} \
  input_result; })
#define correlation_macros__distanceMeasures__squared__PearsonProductMoment_uncentered__postProcess(obj) ({ \
  t_float input_result  = obj.denumeratorLimitedTo_row_1and2;			\
  t_float tweight = obj.cnt_interesting_cells;				\
  /*---*/								\
  t_float denom1 = obj.numerator;					\
  t_float denom2 = obj.denumerator;					\
  /*---*/								\
  if( (denom1 > 0) &&  (denom2 > 0) ) {					\
    input_result = input_result / mathLib_float_sqrt(denom1*denom2);		\
    input_result = 1. - input_result;						\
  } else {input_result = metricMacro__constants__defaultValue__postProcess__allMatches;} \
  input_result; })
#define correlation_macros__distanceMeasures__squared__PearsonProductMoment_uncenteredAbsolute__postProcess(obj) ({ \
  t_float input_result  = obj.denumeratorLimitedTo_row_1and2;			\
  /*---*/					     \
  t_float denom1 = obj.numerator;		     \
  t_float denom2 = obj.denumerator;		     \
  /*---*/					     \
  if( (denom1 > 0) &&  (denom2 > 0) ) { \
    input_result = mathLib_float_abs(input_result) / mathLib_float_sqrt(denom1*denom2); \
    input_result = 1. - input_result;						    \
  } else {input_result = metricMacro__constants__defaultValue__postProcess__allMatches;} \
  input_result; })
//#endif //! EOF
