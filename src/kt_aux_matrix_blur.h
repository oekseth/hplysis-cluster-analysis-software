#ifndef kt_aux_matrix_blur_h
#define kt_aux_matrix_blur_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_aux_matrix_blur.h
   @brief provide logics for blurring data (oekseth, 06. feb. 2018)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2018).
   @remarks among others used in algorithms and applications of image anlaysis. 
**/


//#include "type_2d_float_nonCmp_uint.h"
#include "kt_matrix_base.h"

/**
   @enum 
   @brief descirbes differnet types of "blurring" (oekseth, 06. feb. 2018).
 **/
typedef enum e__blur {
  e__blur_gaussian_1_generic,
  e__blur_gaussian_2_box,
  e__blur_gaussian_3_oneDim,
  e__blur_gaussian_4_accumulator,
  // e__blur_gaussian_,
  e__blur_undef
} e__blur;

//! @return an abbreivated (ie, shrot) striing-repsresniton of the enum
static const char *getStr_e__blur(const e__blur enum_id) {
  if(enum_id == e__blur_gaussian_1_generic) {return "generic";}
  else if(enum_id == e__blur_gaussian_2_box) {return "box";}
  else if(enum_id == e__blur_gaussian_3_oneDim) {return "oneDim";}
  else if(enum_id == e__blur_gaussian_4_accumulator) {return "accumulator";}
  //else if(enum_id == e__blur_gaussian_) {return "";}
  else {return NULL;}
}

/**
   @brief constructs a 'blurred' veriosn of a given matrix (oekseth, 0p6. feb. 2018).
   @param <self> the input matrix.
   @param <enum_id> the type of blur funciton to apply.
   @param <radius> the radius of the 'blur-field' to apply.
   @return a 'blurred' matrix.
 **/
s_kt_matrix_base_t apply_blur__kt_aux_matrix_blur(s_kt_matrix_base_t *self, const e__blur enum_id, const t_float radius);


#endif //! EOF
