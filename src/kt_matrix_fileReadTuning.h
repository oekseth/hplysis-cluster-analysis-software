#ifndef kt_matrix_fileReadTuning_h
#define kt_matrix_fileReadTuning_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_matrix_fileReadTuning
   @brief provide a gernalised itnerface for reading matrix-based input-files.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

//#include "kt_matrix_cmpCluster.h"
//#include "type_2d_float_nonCmp_uint.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_matrix_base.h"
#include "parse_main.h"

/**
   @struct s_kt_matrix_fileReadTuning_base   
   @brief a strucutre for adnvaced tuning/configuraitons of file-reading (oekseth, 06. feb. 2017).
 **/
typedef struct s_kt_matrix_fileReadTuning_base {
  bool fileIsRealLife; //! otherwise we assume the string describes one of the 'altenraitve math-functiosn' defined in our "constructSample_data_identifiedFromString" function, eg, ["lines-different-ax", "lines-curved", "lines-different-ax-and-axx", "lines-ax-inverse-x", "lines-circle", "lines-sinsoid", "lines-sinsoid-curved"], where the latter may have a 'noise-suffix of ["-medium", "large"].
  uint imaginaryFileProp__nrows;   uint imaginaryFileProp__ncols; //! which are used as the 'idmensions' if "fileIsRealLife" is specified: toerhwise ignored.
  s_kt_matrix_base_t *mat_concat; //! which if set is used to 'merge' muliple data-sets.
  bool isTo_transposeMatrix;  
  uint fractionOf_toAppendWith_sampleData_typeFor_rows; //! which if set for real-life data implies that we 'append' the data-set with a uniform nosie-distirbution, where a use-case is nto evlauat eht enosei-sensitivty 'be3tween' a data-set, a simlairty-metyric and a cluster-algorithm.
  uint fractionOf_toAppendWith_sampleData_typeFor_columns; //! (simliar as "fractionOf_toAppendWith_sampleData_typeFor_rows"). 
  char *isTo_exportInputFileAsIs__toFormat__js; //! which if set to true implies that we expert the input-file to a JS result-file; a use-case si to comapre a simalrity-amtrix-cosntructed dat-aset to the orignal input-file.
  bool isTo__copyNamesIfSet;
} s_kt_matrix_fileReadTuning_base_t;

//! Initates hte "s_kt_matrix_fileReadTuning_base_t" object and return (oesketh, 06. feb. 2017).
static s_kt_matrix_fileReadTuning_base_t initAndReturn__s_kt_matrix_fileReadTuning_base_t() {
  s_kt_matrix_fileReadTuning_base_t self;
  self.fileIsRealLife = true;
  self.mat_concat = NULL;
  self.isTo_transposeMatrix = false;
  self.fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  self.fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  self.imaginaryFileProp__nrows = 100;   self.imaginaryFileProp__ncols = 100; //! which are used as the 'idmensions' if "fileIsRealLife" is specified: toerhwise ignored.
  self.isTo_exportInputFileAsIs__toFormat__js = NULL; //! which if set to true implies that we expert the input-file to a JS result-file; a use-case si to comapre a simalrity-amtrix-cosntructed dat-aset to the orignal input-file.
  self.isTo__copyNamesIfSet = true;
  //! @return
  return self;
}


//! Parse a data-file suign the configurations from the "s_kt_matrix_fileReadTuning_base_t" object (oekseth, 06. feb. 2017).
static s_kt_matrix_base_t readFromAndReturn__file__advanced__s_kt_matrix_fileReadTuning_base_t(const char *input_file, s_kt_matrix_fileReadTuning_base_t config) {
  assert(input_file); 	    
  assert(strlen(input_file));
  //!
  //! Configure:
  const char *stringOf_sampleData_type = NULL;
  const char *stringOf_sampleData_type_realLife = input_file;
  if(config.fileIsRealLife == false) {stringOf_sampleData_type = input_file; stringOf_sampleData_type_realLife = NULL;}
  const uint fractionOf_toAppendWith_sampleData_typeFor_rows = config.fractionOf_toAppendWith_sampleData_typeFor_rows;
  const uint fractionOf_toAppendWith_sampleData_typeFor_columns = config.fractionOf_toAppendWith_sampleData_typeFor_columns;
  const bool isTo_transposeMatrix = config.isTo_transposeMatrix;
  //mapOf_realLife_classificaiton[data_id].isTo_transpose;
  //!
  //! Load data-set:
  s_kt_matrix_base_t obj_matrixInput = initAndReturn__empty__s_kt_matrix_base_t();
  uint nrows = config.imaginaryFileProp__ncols;     uint ncols = config.imaginaryFileProp__ncols;  //! which are used as the 'idmensions' if "fileIsRealLife" is specified: toerhwise ignored.
  // printf("generates a matrix with dims=[%u, %u], where synFile=\"%s\", realLifeFile=\"%s\", fileIsRealLife='%u', at %s:%d\n", nrows, ncols, stringOf_sampleData_type, stringOf_sampleData_type_realLife, config.fileIsRealLife, __FILE__, __LINE__);


  s_kt_matrix_base_t obj_baseToInclude = initAndReturn__empty__s_kt_matrix_base_t(); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
  if(config.mat_concat) {
    //printf("(concat)\t\t at %s:%d\n", __FILE__, __LINE__);
    assert(config.mat_concat->ncols > 0);
    assert(config.mat_concat->nrows > 0);
    if( (config.mat_concat->nrows > 0) && (config.mat_concat->ncols > 0) ) {
      obj_baseToInclude = *(config.mat_concat);
    }									    
  }
  //printf("(start)\t import-data \t at %s:%d\n", __FILE__, __LINE__);
  const bool isTo__copyNamesIfSet = config.isTo__copyNamesIfSet;
#define __MiC__inputType__matrixBase
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
#undef __MiC__inputType__matrixBase
  //printf("(cmpl)\t import-data \t at %s:%d\n", __FILE__, __LINE__);

  /* if(config.isTo_exportInputFileAsIs__toFormat__js) { */
  /*   char str_local[2000] = {'\0'}; sprintf(str_local, "%s__%s_locallYparsed.js", input_file, config.isTo_exportInputFileAsIs__toFormat__js);  */
  /*   char *stringOf_id = "parsedFile"; */
  /*   export__singleCall__toFormat_javaScript__s_kt_matrix_t(&obj_matrixInput, str_local, NULL, stringOf_id); */
  /* } */

  //! @return
  return obj_matrixInput;
}


#endif //! EOF
