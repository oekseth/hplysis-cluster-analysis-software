#ifndef kt_clusterAlg_SVD_h
#define kt_clusterAlg_SVD_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */



/**
   @file graphAlgorithms_svd
   @brief provide implementaitons of algorithms using the "Single VAlue Decomposition" (SVD) as a 'basis'.
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
 **/

#include "kt_distance_cluster.h"



/**
   @namespace graphAlgorithms_svd
   @brief provide implementaitons of algorithms using the "Single VAlue Decomposition" (SVD) as a 'basis'.
   @remarks the "c clustering library" is used as template.
   @author Ole Kristian Ekseth (oekseth).
 **/
//namespace graphAlgorithms_svd {

/**
   @brief Determine the singular value decomposition (SVD) by a real m by n rectangular matrix.
   @param <m> the number of rows of A (and u);
   @param <n> is the number of columns of A (and u) and the order of v;
   @param <u> contains the rectangular input matrix A to be decomposed: in the result represents/holds the orthogonal column vectors of the decomposition;
   @param <w> result: the non-negative diagonal singular values;
   @param <vt> result: the orthogonal column-matrix of the decomposition;
   @return true upon succsess.
**/
  int svd_slow(int m, int n, t_float** u, t_float w[], t_float** vt);
/**
   @brief use SVD to perform principal components analysis of a real nrows by ncolumns rectangular matrix.
   @param <nrows> The number of rows in the matrix u.
   @param <ncolumns> The number of columns in the matrix v.
   @param <u> is the distance-matricx: on input, the array containing the data to which the principal component analysis should be applied. The function assumes that the mean has already been subtracted of each column, and hence that the mean of each column is zero. On output, see below.
   @param <v> (not used in input)
   @param <mapOf_w> (not used in input)
   @return true upon success
   @remarks 
   On output:   
   # The eigenvalues of the covariance matrix are returned in w.
   # Ifnrows >= ncolumns, then   
   - u contains the coordinates with respect to the principal components;
   - v contains the principal component vectors.          
   # Ifnrows < ncolumns, then   
   - u contains the principal component vectors;
   - v contains the coordinates with respect to the principal components.   
   # For the above sets, the dot product v . u reproduces the data that were passed in u.
   # The arrays u, v, and w are sorted according to eigenvalue, with the largest eigenvalues appearing first.   
 **/
  const bool pca_slow(int nrows, int ncolumns, t_float** u, t_float** v, t_float* w);
  //const bool pca(uint nrows, uint ncolumns, float** u, float** v, list_generic<type_float> mapOf_w) ;
//};

#endif //! EOF
