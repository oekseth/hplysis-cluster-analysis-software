#include "kt_hash_string.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_sim_string.h"
#include "hp_exportMatrix.h"
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.



/**
   @brief examplfiy the writign and reading to/from a file using logics in our "kt_matrix.h" API.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- general: use logics in our "kt_matrix.h" 
   -- related: a permtuation of our "tut_matrix_1_toAndFromFile_values.c".
   @remarks in this procedure we demosntr/elvuaate the following use-case: 
   (1) read file into a string-array; (2) split words/strings using the user-porivded feild-delimoror (eg, '\t'); (3) insert into our "s_kt_list_2d_uint_t" [key1][key2] pairs; return botht he new-cosntructed hash-object and the 2d-sparse-list.
**/
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main() 
#else
  void tut_parse_3_fromSparseStrings_dbScan()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  //! Load result:
  //! grep -o -P '(?<=\# )(.+)$' result_tut_string_2_inpFile_aut_sparse_Levenshtein_norm_0.tsv > result_tut_string_2_inpFile_aut_sparse_Levenshtein_norm_0.tsv.local
  //  const char *file_inp = "result_tut_string_2_inpFile_aut_sparse_Levenshtein_norm_0.tsv";
    const char *file_inp = "result_tut_string_2_inpFile_aut_sparse_Levenshtein_norm_0.tsv.local";
  printf("#\t Parse-file: \"%s\", at %s:%d\n", file_inp, __FILE__, __LINE__);
  s_kt_list_1d_string_t obj_data = initFromFile__s_kt_list_1d_string_t(file_inp);
  printf("#\t Translate-file: \"%s\", at %s:%d\n", file_inp, __FILE__, __LINE__);
  s_kt_hash_string_2dSparse_t obj_str_2d = init__fromHash__s_kt_hash_string_2dSparse_t(&obj_data, 
										       /*word_seperator_keyValues=*/'\t',
										       /*word_seperator_valueValues=*/'\t', 
										       /*isTo_useDifferentMappingTables_forKeysIndexes=*/true); 
  printf("current_pos=%u, and [%u][%u], given nrows=%u, at %s:%d\n", obj_str_2d.obj_2d.current_pos, obj_str_2d.obj_dense.obj_strings_cntInserted, obj_str_2d.obj_sparse.obj_strings_cntInserted, obj_data.nrows, __FILE__, __LINE__);
  //    if(false) 
 
  const char *stringOf_resultFile_prefix = "tut_parse_3_";
  {
    printf("#\t Start-computing, at %s:%d\n", __FILE__, __LINE__);    
    //!
    //! Apply logics:
    //! ***********************************************************************************************
    //!
      { //! Idnetify disjtoint clsuters for the latter set (uisng our hpLysis-impemtantion of disjoitn-sets):
	char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000);
	sprintf(stringOf_resultFile, "%s.clusterPartitions_hpLsysisAlgVersion.tsv", stringOf_resultFile_prefix); //, getString__e_kt_sim_string_type_t(enum_id), isTo_normalize_result);
	if(true) {printf("(info): export to result-file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__); }

	//!
	//! Identify clsuters: 
	const s_kt_list_2d_uint_t obj_sparse_tmp = obj_str_2d.obj_2d;
	s_kt_list_2d_kvPair_t obj_sparse = init__s_kt_list_2d_kvPair_t(obj_sparse_tmp.list_size, 10);
	for(uint row_id = 0; row_id < obj_sparse_tmp.list_size; row_id++) {
	  const s_kt_list_1d_uint_t list = obj_sparse_tmp.list[row_id];
	  if(list.list_size > 0) {
	    for(uint k = 0; k < list.list_size; k++) {
	      const uint tail_id = list.list[k];
	      if(tail_id != UINT_MAX) {
		push__s_kt_list_2d_kvPair_t(&obj_sparse, row_id, MF__initVal__s_ktType_kvPair(tail_id, 1));
	      }
	    }
	  }
	}
	//!
	//! The DB-SCAN call:
	s_kt_list_1d_uint_t map_vertexToClusterId = search__relationsFromDataStruct__altVersion__kd_tree(&obj_sparse);
	/* //! Write out: */
	/* export__s_kt_list_2d_kvPair__hp_exportMatrixClusterMemberships(stringOf_resultFile, map_vertexToClusterId, strObj_1); */
	/* //	export__s_kt_list_2d_kvPair__hp_exportMatrixClusterMemberships(&map_vertexToClusterId); */
	/* //! */
	/* //! De-allcoate:  */
	free__s_kt_list_1d_uint_t(&map_vertexToClusterId);
	free__s_kt_list_2d_kvPair_t(&obj_sparse);
      }
    //    __eval__tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix(str_resultPrefix, enum_entropy, configHistogram_cntBins, obj_str_2d, /*isTo_computeClusters=*/false);
  }
  //!
  //! De-allocates:
  free__s_kt_list_1d_string(&obj_data);
  free__s_kt_hash_string_2dSparse_t(&obj_str_2d);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

