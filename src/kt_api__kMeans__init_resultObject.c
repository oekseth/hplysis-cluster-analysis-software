
  if(obj_result->vertex_clusterId == NULL) {
    init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, nclusters, size_inner);
  } else { //! then we investigate if we are to use a pre-defined itertion-start-point in our traverxsal (oekseth, 06. jan. 2017).
    if( (obj_result->cnt_vertex != size_inner) || (obj_result->clusterId_size != nclusters) ) {
      fprintf(stderr, "!!\t In-consistent memroy-allcoaiton for result-object: we expected both (size_inner=%u == %u) and (nclusters=%u == %u), a case which does Not hold: if reading the docuemtantion does Not help then please cotnact the senior delveoper at [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", obj_result->cnt_vertex, size_inner, obj_result->clusterId_size, nclusters, __FUNCTION__, __FILE__, __LINE__);
      assert(false);
      //! Then fix the sissue:
      free__s_kt_clusterAlg_fixed_resultObject_t(obj_result);
      //! Allocate:
      init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, nclusters, size_inner);      
    } else {
      uint min_val = 0; uint max_val = 0;
      for(uint i = 0; i < size_inner; i++) {
	const uint cluster_id = obj_result->vertex_clusterId[i];
	if(cluster_id != UINT_MAX) {
	  min_val = macro_min(min_val, cluster_id);
	  max_val = macro_max(max_val, cluster_id);
	}
      }
      if(min_val != max_val) { //! then we udpate the clsuter-ids:
	printf("min_val=%u, max_val=%u, at %s:%d\n", min_val, max_val, __FILE__, __LINE__);
	for(uint i = 0; i < size_inner; i++) {
	  const uint cluster_id = obj_result->vertex_clusterId[i];
	  if(cluster_id != UINT_MAX) {
	    obj_rand__init->mapOf_columnToRandomScore[i] = cluster_id;
	    //tclusterid[i] = cluster_id;
	  }
	} //! else we asusem a users has not a 'pre-deifned suggesiton of cluster-emmbersips', ie, a case which we expect to the the defualt-frequenct-use-case (oesketh, 06. jan. 2017).
      }
    }
  }
