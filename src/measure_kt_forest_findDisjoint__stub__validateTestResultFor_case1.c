    const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&obj_disjoint);
    const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
    printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
    //! Validate the result:
    //! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
    //assert(cnt_total_vertices == nrows); //! ie, the totla number of unique vertices inserted in [above]
    assert(cnt_interesting_disjointForests == 3); //! ie, what we expect.

    //!
    //! Valdiate memberships:    
    { //! First valdiate the [ªbove] explcitly inserted memberships:
      //! Forest(1):
      __inLove(0, 1);  __inLove(1, 0);  __inLove(1, 1); __inLove(1, 3); __inLove(3, 0); 
      __atWar(0, 4);  __atWar(0, 7); __atWar(0, 4);
      //! Forest(2):
      __atWar(3, 4);  __inLove(4, 5);    __inLove(5, 4);
      /* //! Forest(3): */
      __atWar(5, 8);  __inLove(7, 8);    __inLove(7, 9); __inLove(9, 8); __inLove(7, 8);
    }

    { //! Get the clsuter-id and 'the returned set of ityndieties' proudce the rorect reuslt:
      { //! Get the forest-id for one of the sets:
	const uint cnt_expected = 4; const uint arrOf_cmp[cnt_expected] = {0, 1, 2, 3};
	//! -----------------
	uint *arrOf_result = NULL; //allocate_1d_list_uint(nrows, default_value_float__); 
	uint arrOf_result_size = 0; //! where the latter is a temprao vriable:
	//! The test:
	const uint forest_id = get_foreestId_forVertex__s_kt_forest_findDisjoint(&obj_disjoint, /*vertex=*/arrOf_cmp[0]);
	//! Build a selit of the forest-ids:
	cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, &arrOf_result, &arrOf_result_size);
	assert(arrOf_result_size == cnt_expected); 
	assert(arrOf_result);
	// printf("arrOf_result_size=%u, cnt_expected=%u, at %s:%d\n", arrOf_result_size, cnt_expected, __FILE__, __LINE__);
	__assert_listsAreEqualTo_clusterMemberships(arrOf_cmp, cnt_expected); //! ie, vlaidate that all 'entites' are found.
	//! De-allcote:
	free_1d_list_uint(&arrOf_result); arrOf_result = NULL;
      }
      { //! Get the forest-id for one of the sets:
	const uint cnt_expected = 2; const uint arrOf_cmp[cnt_expected] = {4, 5};
	//! -----------------
	uint *arrOf_result = NULL; //allocate_1d_list_uint(nrows, default_value_float__); 
	uint arrOf_result_size = 0; //! where the latter is a temprao vriable:
	//! The test:
	const uint forest_id = get_foreestId_forVertex__s_kt_forest_findDisjoint(&obj_disjoint, /*vertex=*/arrOf_cmp[0]);
	//! Build a selit of the forest-ids:
	cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, &arrOf_result, &arrOf_result_size);
	assert(arrOf_result_size == cnt_expected); 
	assert(arrOf_result);
	// printf("arrOf_result_size=%u, cnt_expected=%u, at %s:%d\n", arrOf_result_size, cnt_expected, __FILE__, __LINE__);
	__assert_listsAreEqualTo_clusterMemberships(arrOf_cmp, cnt_expected); //! ie, vlaidate that all 'entites' are found.
	//! De-allcote:
	free_1d_list_uint(&arrOf_result); arrOf_result = NULL;
      }
      { //! Get the forest-id for one of the sets:
	const uint cnt_expected = 3; const uint arrOf_cmp[cnt_expected] = {7, 8, 9};
	//! -----------------
	uint *arrOf_result = NULL; //allocate_1d_list_uint(nrows, default_value_float__); 
	uint arrOf_result_size = 0; //! where the latter is a temprao vriable:
	//! The test:
	const uint forest_id = get_foreestId_forVertex__s_kt_forest_findDisjoint(&obj_disjoint, /*vertex=*/arrOf_cmp[0]);
	//! Build a selit of the forest-ids:
	cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, &arrOf_result, &arrOf_result_size);
	assert(arrOf_result_size == cnt_expected); 
	assert(arrOf_result);
	// printf("arrOf_result_size=%u, cnt_expected=%u, at %s:%d\n", arrOf_result_size, cnt_expected, __FILE__, __LINE__);
	__assert_listsAreEqualTo_clusterMemberships(arrOf_cmp, cnt_expected); //! ie, vlaidate that all 'entites' are found.
	//! De-allcote:
	free_1d_list_uint(&arrOf_result); arrOf_result = NULL;
      }
    }
