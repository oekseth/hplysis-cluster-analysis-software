#include "hpLysis_api.h"
#include "hp_clusterFileCollection.h"
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t __static__readDataFromObject__kd(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}

#endif //! #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

static void tut_norm_3__compute(s_kt_matrix_t *mat_list, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, e_kt_correlationFunction_t sim_pre, const e_hpLysis_clusterAlg clusterAlg, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm, const char *stringOf_resultFile) {
  assert(mat_list);
  assert(mat_input_size);
  assert(clusterAlg != e_hpLysis_clusterAlg_undef);
  //!
  //! Itniate result-matrix:
  s_kt_matrix_t mat_result_ccm = initAndReturn__s_kt_matrix(mat_input_size, (e_kt_normType_undef+1));
  for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {      
    const char *str = get_str__humanReadable__e_kt_normType_t((e_kt_normType_t)isTo_normalize);
    assert(str); assert(strlen(str));
    set_stringConst__s_kt_matrix(&mat_result_ccm, isTo_normalize, str, /*addFor_column=*/true);
  }
  //!
  //!
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    s_kt_matrix_t mat_input = mat_list[data_id];
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }    
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
#define __setRowName(obj) ({set_stringConst__s_kt_matrix(obj, data_id, stringOf_tagSample, /*addFor_column=*/false);})
    __setRowName(&(mat_result_ccm));
#undef __setRowName
    t_float *row_result = mat_result_ccm.matrix[data_id];
    assert(row_result);

    //!
    //! Apply logics:
    for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {      
      //! Normalize (or optionally copy the non-nroamized data-set):
      s_kt_matrix_t mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_input, (e_kt_normType_t)isTo_normalize);
      //      if(true) { //! then validate that there is at least one interesting cell:
	loint cnt_interesting = 0;
	const s_kt_matrix_t *matrix_input_1 = &mat_data;
	for(uint i = 0; i < matrix_input_1->nrows; i++) {
	  for(uint j = 0; j < matrix_input_1->ncols; j++) {
	    cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
	if(cnt_interesting == 0) {free__s_kt_matrix(&mat_data); continue;} //! ie, as the data-set does Not contain any intersting valeus.
	assert(cnt_interesting > 0);
      /* 	assert(cnt_interesting > 0); */
      /* } */
      if(false) { //! then we export the result-file to a tmeprary file:
	char fileName_local[1000]; memset(fileName_local, '\0', 1000);
	sprintf(fileName_local, "result_tut_norm1_useNorm%u.tsv", isTo_normalize);
	printf("export to file=\"%s\", at %s:%d\n", fileName_local, __FILE__, __LINE__);
	export__singleCall__s_kt_matrix_t(&mat_data, fileName_local, NULL);
      }
      // const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
      // const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree;
      //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = 
      //!
      //! Allocate object:
      s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
      obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
      obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
      obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
      //obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
      if(true) {
	obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
	assert(sim_pre != e_kt_correlationFunction_undef);
	obj_hp.config.corrMetric_prior_use = true;
      }
      
      //! 
      //! Apply logics:
      // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
      const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
      assert(is_ok);
      //! Get the CCM-score:
      const t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
      if(ccm_score != T_FLOAT_MAX) {
	/* assert(matResult_ccm_matrix); */
	/* assert(index_local < matResult_ccm_matrix->ncols); */
	/* assert(ccm_id < matResult_ccm_matrix->nrows); */
	//!
	//! Set the score:
	row_result[isTo_normalize] = ccm_score;
	//printf("[norm=%u]\t score(Silhouttte)=%f, at %s:%d\n", isTo_normalize, ccm_score, __FILE__, __LINE__); 
      }
      //! 
      //! De-allcoate object, and return:
      free__s_hpLysis_api_t(&obj_hp);      
      free__s_kt_matrix(&mat_data);
    }
  }
  //!
  //! Export and de-allcoate:
  printf("export to file=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
  export__singleCall__s_kt_matrix_t(&mat_result_ccm, stringOf_resultFile, NULL);
  free__s_kt_matrix(&mat_result_ccm);
}

static void tut_norm_3__compute_multConfig__ccmConvergence(s_kt_matrix_t *mat_input, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix_local, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm) {
  const uint arrOf_simAlgs_size = 6;
  static const e_kt_correlationFunction_t arrOf_simAlgs[arrOf_simAlgs_size] = {
    e_kt_correlationFunction_groupOf_minkowski_euclid,
    e_kt_correlationFunction_groupOf_minkowski_cityblock,
    e_kt_correlationFunction_groupOf_absoluteDifference_Canberra,
    e_kt_correlationFunction_groupOf_squared_Pearson,
    e_kt_correlationFunction_groupOf_rank_kendall_coVariance,
    e_kt_correlationFunction_groupOf_MINE_mic,
  };
  for(uint  sim_id = 0; sim_id < (uint)arrOf_simAlgs_size; sim_id++) {  
    const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
    //const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;
    e_kt_correlationFunction_t sim_pre = arrOf_simAlgs[sim_id];
    //! ---
    const char *str = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(sim_pre);
    assert(str); assert(strlen(str));
    //! ---- 
    char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); sprintf(stringOf_resultFile, "%s_kdDynamic_%s_%s.tsv", resultPrefix_local, getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm), str);
    assert(strlen(stringOf_resultFile));    
    //! ---- 
    //!
    tut_norm_3__compute(mat_input, mat_input_size, arrOf_stringNames, sim_pre, clusterAlg, enum_ccm, stringOf_resultFile);    
  }
}

static void tut_norm_3__compute_multConfig(s_kt_matrix_t *mat_input, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix_local) {
  //!
  //! Iterate: 
  { //! CCM-pertubrations:
    { //! Seperatley for each simliarty-metric:
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      /* char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); sprintf(stringOf_resultFile, "%s_kdDynamic_Silhoutette_%s.tsv", resultPrefix_local, str); */
      /* assert(strlen(stringOf_resultFile)); */
      //! Apply:
      tut_norm_3__compute_multConfig__ccmConvergence(mat_input, mat_input_size, arrOf_stringNames, resultPrefix_local, enum_ccm);
    }
    //    if(false)
    { //! Seperatley for each simliarty-metric:
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE;
      //! ---- 
      //!
      //! Apply:
      tut_norm_3__compute_multConfig__ccmConvergence(mat_input, mat_input_size, arrOf_stringNames, resultPrefix_local, enum_ccm);
    }
    // if(false)
    { //! Seperatley for each simliarty-metric:
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_VNND;
      /* char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); sprintf(stringOf_resultFile, "%s_kdDynamic_Silhoutette_%s.tsv", resultPrefix_local, str); */
      /* assert(strlen(stringOf_resultFile)); */
      //! Apply:
      tut_norm_3__compute_multConfig__ccmConvergence(mat_input, mat_input_size, arrOf_stringNames, resultPrefix_local, enum_ccm);
    }
    //if(false)
    { //! Seperatley for each simliarty-metric:
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC;
      //! ---- 
      //!
      //! Apply:
      tut_norm_3__compute_multConfig__ccmConvergence(mat_input, mat_input_size, arrOf_stringNames, resultPrefix_local, enum_ccm);
    }
    //if(false)
    { //! Seperatley for each simliarty-metric:
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin;
      /* char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); sprintf(stringOf_resultFile, "%s_kdDynamic_Silhoutette_%s.tsv", resultPrefix_local, str); */
      /* assert(strlen(stringOf_resultFile)); */
      //! Apply:
      tut_norm_3__compute_multConfig__ccmConvergence(mat_input, mat_input_size, arrOf_stringNames, resultPrefix_local, enum_ccm);
    }
    //    if(false)
    { //! Seperatley for each simliarty-metric:
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns;
      //! ---- 
      //!
      //! Apply:
      tut_norm_3__compute_multConfig__ccmConvergence(mat_input, mat_input_size, arrOf_stringNames, resultPrefix_local, enum_ccm);
    }
  }
  { //! Seperatley for each cluster-alg:
    const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
    // -----------
    static const uint setOfAlgsToEvaluate__size = 6;
    static const e_hpLysis_clusterAlg_t setOfAlgsToEvaluate[setOfAlgsToEvaluate__size] = {
      //! ------------------------------------------------------------
      e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, //! which is a permtuation of our "e_hpLysis_clusterAlg_disjoint" where 
      e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN,
      //! ------------------------------------------------------------
      e_hpLysis_clusterAlg_HCA_single,
      //! ------------------------------------------------------------
      e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, //! wherew we use a user-specifeid (or defualt) Cluster Comparison Metric (CCM) to idneityf the min-max-clusters thresholds.
      //! ------------------------------------------------------------
      //! Note: [below] is sued as reference-frames wrt. clustering-acccuracy, eg, to comapre/elvuate the correctness/prediocnt-acucryac of clsutierng (oesketh, 06. jul. 2017).
      e_hpLysis_clusterAlg_random_best,
      e_hpLysis_clusterAlg_random_worst,
    };    
    for(uint alg_id = 0; alg_id < setOfAlgsToEvaluate__size; alg_id++) {
      const e_hpLysis_clusterAlg_t clusterAlg = setOfAlgsToEvaluate[alg_id];
      const char *str = get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg);	  
      char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); sprintf(stringOf_resultFile, "%s_%s_Silhoutette_euclid.tsv", resultPrefix_local, str);
      assert(strlen(stringOf_resultFile));
      //! ---- 
      //!
      tut_norm_3__compute(mat_input, mat_input_size, arrOf_stringNames, sim_pre, clusterAlg, enum_ccm, stringOf_resultFile);    
    }
  }
  { //! Seperatley for each CCM:
    static const uint arrOf_CCMs_size = 6;
    static e_kt_matrix_cmpCluster_clusterDistance__cmpType_t arrOf_CCMs[arrOf_CCMs_size] = {
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin,
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns,
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE,
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM,
    };
    for(uint ccm_id = 0; ccm_id < arrOf_CCMs_size; ccm_id++) {
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_CCMs[ccm_id];
      const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
      const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;
      //const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
      //! ---------
      const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm);
      assert(str);
      char stringOf_resultFile[1000]; memset(stringOf_resultFile, '\0', 1000); sprintf(stringOf_resultFile, "%s_kdDynamic_%s_euclid.tsv", resultPrefix_local, str);
      assert(strlen(stringOf_resultFile));
      //! ---------
      //! ---- 
      //!
      tut_norm_3__compute(mat_input, mat_input_size, arrOf_stringNames, sim_pre, clusterAlg, enum_ccm, stringOf_resultFile);    
    }
  }
}

static void __eval__dataCollection__tut_norm_3(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = __static__readDataFromObject__kd(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
  }
  //!
  //! Apply logics: 
  tut_norm_3__compute_multConfig(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix);
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief cosntruct result-fiels which unify different normalziation-metrics  (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks produce three different reuslt-file-chunks:
   -- parameter="simliarty-metric": cosntruct chunks for differnet simlairty-emtircs;
   -- parameter="cluster-algorithm": cosntruct for different cluster-algorithms; 
   -- parameter="ccm-inner": use differnet 'inner' CCMs for data-clustering.
   @remarks computes for [data-id][normalizaiton-appraohces]=ccm for [kd-tree-dynamic, db-scan-dynamic]x[sim-metrics].
   @remarks related-tut-examples:
   -- "tut_norm_1_differentNormStrategies.c": a subset of this tut-example, ie, where latter is itnroducedused to avoid overhweilinming users with detials of clsuter-applicaiton. 
   -- "tut_kd_1_cluster_multiple_simMetrics.c": logics to evaluate implicaiton of different patterns. 
   -- "tut_norm_2_cluster_kd_dbScan.c": iteraiton-loops wrt. dynamic-CCM-evalaution
**/
int main(const int array_cnt, char **array) 
#else
  int tut_norm_3_cluster_kd_dbScan_80DataSets(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  const char *resultPrefix = "tut_80DataSets_";
  {
#define __MiCo__useLocalVariablesIn__dataRealFileLoading 1
#ifdef __MiCo__useLocalVariablesIn__dataRealFileLoading
 const uint sizeOf__nrows = 100;
 const uint sizeOf__ncols = 100;
    const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
    const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
    const bool isTo__testHCA_komb__inKmeans = false;
    const bool isTo__evaluate__simMetric__MINE__Euclid = true;
    const char *stringOf_resultDir = "";
// ---
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  // printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! We are interested in a more performacne-demanind approach: 

  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!

#endif

    { //const char *nameOf_experiment = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_norm_3(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset);
    }
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
  }

  /* //! Fris seperatley for a gvien input-file: */
  /* //! Note: for a list of files to be used see our cofniguraiton-fiels, eg, wrt. our "data/local_downloaded/metaObj.c" */
  /* //const char *file_name = "data/local_downloaded/22_nuts.csv.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set").  */
  /* //const char *file_name = "data/local_downloaded/56_msq.csv.hpLysis.tsv"; */
  /* const char *file_name = "data/local_downloaded/23_Airline.csv.hpLysis.tsv"; //! which examplfies chalelnges in scales (ie, different scalies used at differnt data-axis) ...  */
  /*   const uint index_pos = 3; */
  /*   bool fileIs_speificed = false; //! which is used to simplify error-generiaotn. */
  /*   if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) { */
  /*     //! then set the file-name: */
  /*     file_name = array[index_pos]; */
  /*     fileIs_speificed = true; */
  /*   } */
  /*   //! */
  /*   //! Load an arbitrary data-set: */
  /*   // const uint nrows = 1000; const uint ncols = 20; */
  /*   //const uint nrows = 1000; const uint ncols = 20; */
  /*   //  const uint nrows = 1000*10; const uint ncols = 5; */
  /*   //const uint nrows = 1000*1000; const uint ncols = 20; */
  /*   s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols); */
  /*   //! Then laod the file: */
  /*   assert(file_name); assert(strlen(file_name)); */
  /*   import__s_kt_matrix_t(&mat_input, file_name); */
  /*   if(mat_input.nrows == 0) { */
  /*     if(fileIs_speificed) { */
  /* 	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__); */
  /*     } else { //! then we asusmet eh file-name was spefieid by the user. */
  /* 	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__); */
  /*     } */
  /*     return false; */
  /*   } */
  /*   //! */
  /*   //! Apply logics: */
  /*   tut_norm_2_cluster_kd_dbScan__compute(mat_input); */
  /*   //! */
  /*   //! De-allocate: */
  /*   free__s_kt_matrix(&mat_input); */
    
    //!
    //! @return
#ifndef __M__calledInsideFunction
    //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    hpLysis__globalFree__kt_api();
    //! *************************************************************************  
#endif
    return true;
}
