#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_matrix.h"
#include "hp_ccm.h"
#include "measure_base.h"
#include "kt_api__centerPoints.h"
//#include "hp_distance.h"
#include "hp_distance_wrapper.h"
/**
   @brief demonstrates how idnetify the distance between cluster-centers and vertices
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks in order to whow the applcialbity of the "kt_api__centerPoints.h" functions we in this example we examplify/demonstrate a 'round-trip' where we evaluate 'goodness of in centers':
   -- input w/API="kt_matrix.h": specify different data-inputs, using both a 2d-float-matrix and a 1d-uint-list.
   -- centroid-identification w/API="kt_api__centerPoints.h": in the centorid-evalaution idneitfy the distance from each vertex to each of the 'center-points'
   -- input(similarity-matrix-computation): demosntrates how to access the centrodi-clsuter-result, and how a 'transpostion' of the altter facilates the comparison and gorup-idneitficoatn;
   -- simliarty-matrix w/API="hp_distance.h": identify simlairty between vertices based on the 'closeness' to different cenotrids.
   -- ccm w/API="hp_ccm.h": evaluate the 'goodness' between the new-constructed simliarty-matrix VS the intal cluster-specificoant.
   @remarks 
   -- inUse(algorithms): is incoprorated in bout our CCM-based approach (eg, "kt_matrix_cmpCluster.h") and in our cluster-algorithsm (eg, "kt_clusterAlg_fixed.h").
   -- application(hypothesis): evalaute the speration-ability between/for a given hypotheical assumption
   -- application(algorithm-construciton): to combine the 'closeness-step' (thoguh calls to logics in our "kt_api__centerPoints.h") with CCM-algorithms (eg, in oour "kt_matrix_cmpCluster.h"), where iteration-step may be the 'random seleciton of subset-vertices' (eg, as examplfied in our kmneas++ implementaiotn in our "kt_randomGenerator_vector.h"). 
   @todo consider to invesgtigate the applcialbity of our above "application(algorithm-construciton)" ... eg, in combainto/co-operation with the gorup of Hetland at NTNU.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const uint nclusters = 10; const uint nrows = 100; const uint ncols = 20;
  s_kt_correlationMetric_t obj_metric = setTo_empty__andReturn__s_kt_correlationMetric_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3); //! ie, the simlairty-metirc to be used.
#endif
  assert(nrows > nclusters); //! ie, as otheriwse would be an odd 'call'.
  s_kt_matrix_t obj_1  = initAndReturn__s_kt_matrix(nrows, ncols); 
  //! Set default values, both for the 'cluser-hypothesis' and wrt. the distance-matrix.
  //! Note: for 'real' use-cases a 'desciptive' input-matrix would be combined with values in "mapOf_vertexClusterIds" reflecting the investigated use-case. 
  const uint empty_0 = 0;
  uint *mapOf_vertexClusterIds = allocate_1d_list_uint(nrows, empty_0); assert(mapOf_vertexClusterIds);
  const uint cnt_each_cluster = nrows / nclusters; 
  assert(cnt_each_cluster != 0);
  uint clusterId_curr = 0;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    mapOf_vertexClusterIds[row_id] = clusterId_curr; //! ie, the null-hyptoehsis we investigate
    for(uint col_id = 0; col_id < ncols; col_id++) {
      obj_1.matrix[row_id][col_id] = (row_id + 1)*0.3*(col_id + 1);
    }
    if((clusterId_curr + 1) >= cnt_each_cluster) {clusterId_curr = 0;} else {clusterId_curr++;}
  }
  
  //!
  //! Investigate for different cluster-centrodi-cases:
  for(uint type_center = 0; type_center < e_kt_api_dynamicKMeans_undef; type_center++) {
    const e_kt_api_dynamicKMeans_t metric_centroid = (e_kt_api_dynamicKMeans_t)type_center; //! defined in: "e_kt_api_dynamicKMeans.h"
    s_kt_matrix_t obj_result_clusterToVertex; setTo_empty__s_kt_matrix_t(&obj_result_clusterToVertex);
    { //! Apply logics: 'each clsuter':
      assert(obj_1.nrows > 0);
      assert(obj_1.ncols > 0);
      assert(obj_1.nrows > nclusters);
      /* assert(self->input_matrix != NULL); */
      /* obj_1.nrows = self->nrows;  obj_1.ncols = self->nrows; obj_1.matrix = self->input_matrix; */
      
      assert(mapOf_vertexClusterIds);
      assert(obj_result_clusterToVertex.nrows == 0); //! ie, as we expect 'this object' to Not have been intalized.
      assert(obj_result_clusterToVertex.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized.
      const bool is_ok = kt_matrix__centroids(&obj_1, mapOf_vertexClusterIds, &obj_result_clusterToVertex, nclusters, /*typeOf_centroidAlg=*/metric_centroid); //! deifned in our "kt_api.h"
      assert(is_ok);
      assert(obj_result_clusterToVertex.matrix != NULL); //! ie, the result-matrix holding [clsuter-centers][nrows-vertices].
      assert(obj_result_clusterToVertex.nrows == nclusters);
    }
    //!
    //!
    //! -------------------------------------------------------------------------------------------------------    
    s_kt_matrix_t vec_1 = setToEmptyAndReturn__s_kt_matrix_t();
    { //! Compute the similarity-metric:
      //! 
      //! Itnaite a wrapper-object, ie, to satsify the input-expecaitosn to our funciton:
      s_kt_matrix_t mat_shallow_clusterWrapper = setToEmptyAndReturn__s_kt_matrix_t();
      mat_shallow_clusterWrapper.matrix = obj_result_clusterToVertex.matrix; //2d_vertexTo_clusterId;
      assert(obj_result_clusterToVertex.nrows == nclusters);
      mat_shallow_clusterWrapper.nrows = nclusters;
      mat_shallow_clusterWrapper.ncols = nrows;
      //! 
      //! Tranpsoe the matrix:
      //! Note: the background-reason for ”[elow] 'transpsotion' conserns the 'hyptoehsis' we are interested in investiationg: to test relatnedss between vertices and Not wrt. the clsuter-centers.
      s_kt_matrix_t mat_vertexToCentroids = setToEmptyAndReturn__s_kt_matrix_t();
      bool is_ok = init__copy_transposed__s_kt_matrix(&mat_vertexToCentroids, &mat_shallow_clusterWrapper, /*isTo_updateNames=*/false);
      assert(is_ok);
      //! 
      //! Apply the dsitance-computation
      /*result=*/vec_1 = applyAndReturn__altTypeAsInput__hp_distance_wrapper(obj_metric, &mat_vertexToCentroids, NULL, /*config=*/init__s_hp_distance__config_t());
      assert(vec_1.nrows);
      //! De-allocate:
      free__s_kt_matrix(&mat_vertexToCentroids);
      mat_shallow_clusterWrapper = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, 'clear ª[bov€]' as no mem-allcoatiosn are expecte dto be erpformed, ie, a 'safe-guard'.
    }
    //!
    //! -------------------------------------------------------------------------------------------------------
    { //! Comptue for all of the matrix-based CCMs:
      s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
      const uint *map_clusterMembers = mapOf_vertexClusterIds;
      s_kt_matrix_base_t mat_sim_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&vec_1);
      bool is_ok = ccm__singleMatrix__completeSet__hp_ccm(&mat_sim_shallow, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
      assert(is_ok);    
      //assert(mat_sim_shallow.nrows == obj_result.list_size);
      //!
      //! Write out the result of [above]:
      for(uint i = 0; i < obj_result.list_size; i++) {
	const t_float ccm_score = obj_result.list[i];
	if(ccm_score != T_FLOAT_MAX) { //! then the CCM-aglrotihm 'was albe' to comptue the CCM-score:
	  const char *str_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i); //! defined in our "kt_matrix_cmpCluster.h"
	  assert(str_enum);
	  if(true) {
	    printf("%s\t has-score\t %f\t # at [tut]:%d\n", str_enum, ccm_score, __LINE__);
	  } else {
	    printf("%s\t has-score\t %f\t # at %s:%d\n", str_enum, ccm_score, __FILE__, __LINE__);
	  }
	}
      }
      //!
      //! De-allocate:
      free__s_kt_list_1d_float_t(&obj_result);
      free__s_kt_matrix(&vec_1);
    }
    /* for(uint cluster_id_in = 0; cluster_id_in < nclusters; cluster_id_in++) { */
    /*   uint vertex_centroid_id = UINT_MAX; */
    /*   assert(obj_result_clusterToVertex.mapOf_rowIds); */
    /*   vertex_centroid_id = obj_result_clusterToVertex.mapOf_rowIds[cluster_id_in]; */
    /*   assert(vertex_centroid_id != UINT_MAX); */
    /*   assert(vertex_centroid_id < self->nrows); */
    /*   const t_float *row_centroid = obj_1.matrix[vertex_centroid_id]; */
    /*   assert(row_centroid); */
    /* } */
  }
  free__s_kt_matrix(&obj_1);
  free_1d_list_uint(&mapOf_vertexClusterIds);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
