#include "kt_clusterAlg_SOM_resultObject.h"
#include "kt_clusterAlg_fixed.h"

//! Intiate the s_kt_clusterAlg_SOM_resultObject_t object
s_kt_clusterAlg_SOM_resultObject_t setToEmptyAndreturn__s_kt_clusterAlg_SOM_resultObject_t() {
  s_kt_clusterAlg_SOM_resultObject_t self;
  //! ----
  self.celldata = NULL;
  self.vertex_clusterid = NULL;
  self.cluster_2d_to_centroidVertex = NULL;
  //! ----
  self.nxgrid = UINT_MAX;
  self.nygrid = UINT_MAX;
  self.nrows = 0;

  //! @return
  return self;
}

//! Intiate the s_kt_clusterAlg_SOM_resultObject_t object
void setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(s_kt_clusterAlg_SOM_resultObject_t *self, const uint nrows, const uint nxgrid, const uint nygrid) {
  assert(self);
  self->celldata = NULL;
  const uint cnt_dims = 2;
  const uint default_value_uint = 0;  
  if( nrows || nygrid || nxgrid) {
    assert(nrows > 0); //! ie, as the call is otherwise pointless
    if(nrows && nygrid && nxgrid) {
      assert(nrows != UINT_MAX); assert(nygrid != UINT_MAX); assert(nxgrid != UINT_MAX);
      //printf("allocates-SOM with SOM=[%u, %u], nrows=%u, at %s:%d\n", nxgrid, nygrid, nrows, __FILE__, __LINE__);
      self->celldata = allocate_3d_list_float(nxgrid, nygrid, nrows, default_value_uint);
    }
    self->vertex_clusterid = NULL;
    if(nrows && cnt_dims) {
      self->vertex_clusterid = allocate_2d_list_uint(nrows, cnt_dims, default_value_uint);
    }
    self->cluster_2d_to_centroidVertex = NULL; //! which hold the 'mapping' from a [grid_x][grid_y] to the centrodi of each vertex: of [nrows, nrows] dim;
    //self->internalcentroid_to_vertex = NULL; //! which 'maps' each centoid-vertex to the 'assicated score': of [nrows, nrows] dim;
    //self->centroid_to_vertex = NULL; //! which 'maps' each centoid-vertex to the 'assicated score': of [nrows, nrows] dim;
    self->nxgrid = nxgrid;
    self->nygrid = nygrid;
    self->nrows = nrows;
  } else {
    //! ----
    self->celldata = NULL;
    self->vertex_clusterid = NULL;
    self->cluster_2d_to_centroidVertex = NULL;
    //! ----
    self->nxgrid = UINT_MAX;
    self->nygrid = UINT_MAX;
    self->nrows = 0;
  }
}
//! De-allcoates the s_kt_clusterAlg_SOM_resultObject_t object
void free__s_kt_clusterAlg_SOM_resultObject_t(s_kt_clusterAlg_SOM_resultObject_t *self) {
  assert(self);
  if(self->celldata != NULL) {
    t_float ***celldata = self->celldata;    
    free_3d_list_float(&celldata);
    /* free_generic_type_1d(celldata[0][0]); */
    /* free_generic_type_2d(celldata[0]); */
    /* free_generic_type_2d(celldata); */
  }
  if(self->vertex_clusterid) {
    free_2d_list_uint(&(self->vertex_clusterid), self->nrows);
  }
  if(self->cluster_2d_to_centroidVertex) {
    free_2d_list_uint(&(self->cluster_2d_to_centroidVertex), self->nxgrid);
  }
  setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(self, 0, 0, 0);
}

/**
   @brief merge the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @param <previousCount__nxGrid> the number opf 'previous' nx-grids which have been added (eg, forest_id*nxgrid): used to provide a speration betwene the clsuter-results, ie, as we assume each clsuter-result to be 'dsijoint'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void mergeResult__s_kt_clusterAlg_SOM_resultObject_t(s_kt_clusterAlg_SOM_resultObject_t *self, s_kt_clusterAlg_SOM_resultObject_t *clusterResults, const uint *keyMap_localToGlobal, const uint previousCount__nxGrid) {
  assert(self); assert(clusterResults); 
  //assert(keyMap_localToGlobal);
  assert(self); assert(clusterResults); 
  assert(self->vertex_clusterid);
  //assert(self->cluster_2d_to_centroidVertex);
  assert(self->celldata);
  
  //! 
  { //! Idneitfy the max-diemsions wr.t the nx-grid, ie, to ivnestigat/idneityf the need to udpat the psoiton.
    uint max_x = 0;
    for(uint row_id = 0; row_id < clusterResults->nrows; row_id++) {
      const uint scalar_x_local = clusterResults->vertex_clusterid[row_id][0];    
      const uint scalar_x = scalar_x_local + previousCount__nxGrid;
      if(scalar_x_local != UINT_MAX)  {
	max_x = macro_max(max_x, scalar_x);
      }
    }
    if(max_x > self->nxgrid) {
      assert(max_x != UINT_MAX);
      const uint default_value_uint = 0;  
      t_float ***new_data = allocate_3d_list_float(max_x, self->nygrid, self->nrows, default_value_uint);
      //! 
      //! Copy:
      for(uint grid_x = 0; grid_x < self->nxgrid; grid_x++) {
	for(uint grid_y = 0; grid_y < self->nygrid; grid_y++) {
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {
	    new_data[grid_x][grid_y][row_id] = self->celldata[grid_x][grid_y][row_id];
	  }
	}
      }
      if(self->cluster_2d_to_centroidVertex) { //! then we udapte the latter. 
	uint **new_map = allocate_2d_list_uint(max_x, self->nygrid, default_value_uint);
	//! 
	//! Copy:
	for(uint grid_x = 0; grid_x < self->nxgrid; grid_x++) {
	  for(uint grid_y = 0; grid_y < self->nygrid; grid_y++) {
	    new_map[grid_x][grid_y] = self->cluster_2d_to_centroidVertex[grid_x][grid_y];
	  }
	}
	//! De-allcote and then udpate the pointer:
	free_2d_list_uint(&(self->cluster_2d_to_centroidVertex), (self->nxgrid));
	self->cluster_2d_to_centroidVertex = new_map;
      }
      //!
      //! De-allcoate and udpate:
      t_float ***celldata = self->celldata;    
      free_3d_list_float(&celldata);
      self->celldata = new_data;
      self->nxgrid = max_x; //! ie, the udpated diemsnion:
    }
  }
  //! 
  //! Iterate through the rows and idneitfy the values assicated to the data-sets:
  for(uint row_id = 0; row_id < clusterResults->nrows; row_id++) {
    const uint scalar_x_local = clusterResults->vertex_clusterid[row_id][0];    
    const uint scalar_x = scalar_x_local + previousCount__nxGrid;
    if(scalar_x_local != UINT_MAX)  {
      if( (scalar_x < self->nxgrid) ) { 
	const uint scalar_y = clusterResults->vertex_clusterid[row_id][1];
	assert(scalar_x_local < clusterResults->nxgrid);   
	assert(scalar_y < clusterResults->nygrid);
	//assert(scalar_x < self->nxgrid);  
	assert(scalar_y < self->nygrid);
	const uint centroid_id = (clusterResults->cluster_2d_to_centroidVertex) ? clusterResults->cluster_2d_to_centroidVertex[scalar_x_local][scalar_y] : UINT_MAX;
	//assert(centroid_id != UINT_MAX);
	const t_float scalar_score = clusterResults->celldata[scalar_x_local][scalar_y][row_id];    
	//!
	//! Update:
	const uint row_id_global = (keyMap_localToGlobal) ? keyMap_localToGlobal[row_id] : row_id;
	self->vertex_clusterid[row_id_global][0] = scalar_x;
	self->vertex_clusterid[row_id_global][1] = scalar_y;
	if(self->cluster_2d_to_centroidVertex) {self->cluster_2d_to_centroidVertex[scalar_x][scalar_y] = (keyMap_localToGlobal && (centroid_id != UINT_MAX) ) ? keyMap_localToGlobal[centroid_id] : centroid_id;}
	assert(self->celldata);
	assert(scalar_x < self->nxgrid);
	assert(self->celldata[scalar_x]);
	assert(self->celldata[scalar_x][scalar_y]);
	self->celldata[scalar_x][scalar_y][row_id_global] = scalar_score;
      } else {
	fprintf(stderr, "!!\t Seems like the local SOM-cluster-id=[%u >= |%u|], ie, please investigat this issue. (scalar_x_local=%u while previousCount__nxGrid=%u). In brief if latter is observed then please contact the sneior developer [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", scalar_x, self->nxgrid, scalar_x_local, previousCount__nxGrid, __FUNCTION__, __FILE__, __LINE__); //! added by (oekseth, 06. jul. 2017).
      }
    }
  }
}

//! Update the SOM-result object with the centroids (of eahc SOM-clsuter), thereby simplifying the reuslt-gernation (oekseth, 06. des. 2016)
void updateObj__afterSOM_computation__s_kt_clusterAlg_SOM_resultObject(s_kt_clusterAlg_SOM_resultObject_t *self, t_float **corrMatrix, const bool transpose) {
 { //! Construct the centroid-elements for each vertex:
   if(!self || !self->nrows || !self->nxgrid || !self->nygrid) {
     fprintf(stderr, "!!\t A pointless call: your object seems emptuy, ie, please validate your input-logics. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
     assert(false); //! ie, an heads-up.
     return;
   }
   assert(self->vertex_clusterid); //! ie, as we expect this to have been allocated.
   const long int cntuniqueGrids = self->nxgrid*self->nygrid + self->nygrid;
   //long int cntuniqueGrids = 0; //self->nxgrid*self->nygrid + self->nygrid;
   /* { */
   /*   for(uint grid_x = 0; grid_x < self->nxgrid; grid_x++) { */
   /*     for(uint grid_y = 0; grid_y < self->nygrid; grid_y++) { */
   /* 	 const long int cluster_id = (grid_x*self->nxgrid) + grid_y; */
   /* 	 cntuniqueGrids = macro_max(cluster_id, cntuniqueGrids); */
   /*     } */
   /*   } */
   /*   cntuniqueGrids++; */
   /* } */
    /* assert(cntuniqueGrids <= (long int)cntuniqueGrids); //! ie, as we otehrwise will have an erorr wrt. [below] 'procedure': */
    /* if(cntuniqueGrids >= (long int)cntuniqueGrids) { */
    /*   fprintf(stderr, "!!\t Seems like we have an overflow wrt. the cluster-centrodi-calls of the SOM-vertices, ie, pelase ask the devleoper to udpate the code wrt. your requestions, sending an emxial to [oesketh@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
    /* } */
    const uint default_value_uint = 0;
    uint *mapOf_verticesTo_centralityNode = allocate_1d_list_uint(self->nrows, default_value_uint);
    //uint *mapOf_verticesTo_centralityNode = allocate_1d_list_uint(cntuniqueGrids, default_value_uint);
    /* for(uint i = 0; i < cntuniqueGrids; i++) { */
    /* //for(uint i = 0; i < cntuniqueGrids; i++) { */
    /*   mapOf_verticesTo_centralityNode[i] = UINT_MAX; //! ie, set to  'empty'. */
    /* } */
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      const uint cluster_x = self->vertex_clusterid[row_id][0];
      const uint cluster_y = self->vertex_clusterid[row_id][1];
      assert(cluster_x < self->nxgrid);
      assert(cluster_y < self->nygrid);
      assert(cluster_x != UINT_MAX);
      assert(cluster_y != UINT_MAX);
      const long int new_value = (cluster_x*self->nygrid) + cluster_y;
      //const long int new_value = (cluster_x*self->nxgrid) + cluster_y;
      // printf("new_value=%u, cluster_x=%u cluster_y=%u, grid=[%u, %u], max=%u, at %s:%d\n", (uint)new_value, cluster_x, cluster_y, self->nxgrid, self->nygrid, (uint)cntuniqueGrids, __FILE__, __LINE__);
      assert(new_value < cntuniqueGrids);
      //! Set the cluster-id:
      assert((uint)new_value < UINT_MAX);
      mapOf_verticesTo_centralityNode[row_id] = (uint)new_value;
    }
    //! Then compute the cenotrids based on [above]:
    t_float **cmask_tmp = NULL;
    assert(cntuniqueGrids < UINT_MAX); //! ie, as we otheriwse 'should' use a different data-type in [below] "allocate_2d_list_uint(..)" call.
    t_float **cdata = allocate_2d_list_float((uint)cntuniqueGrids, self->nrows, default_value_uint);
    getclustercentroids__extensiveInputParams(/*nclusters=*/(uint)cntuniqueGrids, self->nrows, self->nrows, corrMatrix, 
					      /*mask=*/NULL, /*cluster-id=*/mapOf_verticesTo_centralityNode, cdata, /*cmask=*/NULL, 
					      cmask_tmp,
					      /*transpose=*/transpose, /*method=*/'m', /*isTo_copmuteResults_optimal=*/true, /*mayUse_zeroAs_emptySign_inComputations=*/true, /*needTo_useMask_evaluation=*/true);
    assert(self->cluster_2d_to_centroidVertex == NULL); //! ie, what we expect
    self->cluster_2d_to_centroidVertex = allocate_2d_list_uint(self->nxgrid, self->nygrid, default_value_uint);
							       //self->nrows, self->nrows, default_value_uint);
    for(uint grid_x = 0; grid_x < self->nxgrid; grid_x++) {
      for(uint grid_y = 0; grid_y < self->nygrid; grid_y++) {
	const uint cluster_id = (grid_x*self->nygrid) + grid_y;
	//const uint cluster_id = (grid_x*self->nxgrid) + grid_y;
	assert(cluster_id < (uint)cntuniqueGrids); //! ie, what we expect in [below]:
	//! Iterate through the clsuter and find the best centrality_score;
	uint best_index = UINT_MAX; t_float best_score = T_FLOAT_MAX;
	for(uint row_id = 0; row_id < self->nrows; row_id++) {
	  //printf("\t\t\tvertex[%u] in/cluster=%u w/score=%f, at %s:%d\n", row_id, cluster_id, cdata[cluster_id][row_id], __FILE__, __LINE__);
	  if(cluster_id == mapOf_verticesTo_centralityNode[row_id]) {
	    //printf("\tgrid[%u][%u]\t vertex[%u] w/score=%f, at %s:%d\n", grid_x, grid_y, row_id, cdata[cluster_id][row_id], __FILE__, __LINE__);
	    const t_float scalar_score = cdata[cluster_id][row_id];
	    self->celldata[grid_x][grid_y][row_id] = scalar_score;
	    if(
	       //cdata[cluster_id][row_id] &&
	       (scalar_score < best_score)) {
	      best_score = cdata[cluster_id][row_id];
	      best_index = row_id;
	    }
	  }
	}
	//! Then we update the centroid-vertex with the distance 'to the other vertices':
	self->cluster_2d_to_centroidVertex[grid_x][grid_y] = best_index;
	//printf("grid[%u][%u]=cluster(%u) has centralVertex=%u, at %s:%d\n", grid_x, grid_y, cluster_id, best_index, __FILE__, __LINE__);
      }
    }
    free_1d_list_uint(&mapOf_verticesTo_centralityNode);
    free_2d_list_float(&cdata, cntuniqueGrids);
  }
}


//! Print the resutls of the clsutering in a 'human-redable-format' to stream_out (oesketh, 06. jan. 2017).
bool printHumanReadableResult__extensive__s_kt_clusterAlg_SOM_resultObject_t(const s_kt_clusterAlg_SOM_resultObject_t *self, char **mapOf_names, FILE *stream_out, const char *seperator_cols, const char *seperator_newLine, const e_kt_clusterAlg_exportFormat_t enum_id) {
  if(!self || !self->nrows || !self->vertex_clusterid) {
    fprintf(stderr, "!!\t Seems like the object does not hold data, ie, please inveisgate your call. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(!stream_out) {stream_out = stdout;} //! ie, our defualt assumption.
  if(!seperator_cols) {seperator_cols = "\t";}
  if(!seperator_newLine) {seperator_newLine = "\n";}


  fprintf(stream_out, "# Write out the SOM hpLysis cluster-result for a data-set with |vertices|=%u and |clusters|=[x=%u][y=%u], at %s:%d\n", self->nrows, self->nxgrid, self->nygrid, __FILE__, __LINE__);

 
  //! -----------
  if( (enum_id == e_kt_clusterAlg_exportFormat_all) 
      || (enum_id == e_kt_clusterAlg_exportFormat_vertex_toCentroidIds)
      || (enum_id == e_kt_clusterAlg_exportFormat_vertex_toClusterIds)
      ) {
    fprintf(stream_out, "#! centroid-memberships: To being with we start priting out the centroid-memberships (which were identified during the clustering):\n");
    assert(self->vertex_clusterid);
    assert(self->cluster_2d_to_centroidVertex);
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      const uint cluster_x = self->vertex_clusterid[row_id][0];
      const uint cluster_y = self->vertex_clusterid[row_id][1];
      assert(cluster_x < self->nxgrid);
      assert(cluster_y < self->nygrid);
      uint centroidId = self->cluster_2d_to_centroidVertex[cluster_x][cluster_y];
      if(mapOf_names) {
	fprintf(stream_out, "%s%s" "in-cluster%s" "som[%u][%u]%s" "with-centroid%s" "%s%s\n", 
		mapOf_names[row_id], seperator_cols, seperator_cols, 
		cluster_x, cluster_y, seperator_cols, seperator_cols, 
		mapOf_names[centroidId], seperator_newLine
		);
      } else {
	fprintf(stream_out, "%u%s" "in-cluster%s" "som[%u][%u]%s" "with-centroid%s" "%u%s\n", 
		row_id, seperator_cols, seperator_cols, 
		cluster_x, cluster_y, seperator_cols, seperator_cols, 
		centroidId, seperator_newLine);
      }
    }
  }
  //! -----------
  if( (enum_id == e_kt_clusterAlg_exportFormat_all) || (enum_id == e_kt_clusterAlg_exportFormat_distanceToCentroids)) {
    fprintf(stream_out, "#! centroid-candidate-distances: In below we include the distances frome ach SOM-centroid to each vertex (in the given SOM cluster), using the syntax of <SOM-centroid><tab>{<vertex-score-at-incremental-cluster-index>} (where '0' indicats no-match):\n");
    assert(self->vertex_clusterid);
    for(uint grid_x = 0; grid_x < self->nxgrid; grid_x++) {
      for(uint grid_y = 0; grid_y < self->nygrid; grid_y++) {
	uint centroidId = self->cluster_2d_to_centroidVertex[grid_x][grid_y];
	if(centroidId != UINT_MAX) {
	  if(mapOf_names) {
	    fprintf(stream_out, "%s%s", mapOf_names[centroidId], seperator_cols);
	  } else {
	    fprintf(stream_out, "%u%s", centroidId, seperator_cols);
	  }
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {
	    t_float score = self->celldata[grid_x][grid_y][row_id];
	    if(score == T_FLOAT_MAX) {score = 0;}
	    fprintf(stream_out, "%f%s", score, seperator_cols);
	  }
	  fprintf(stream_out, "%s", seperator_newLine);
	}
      }
    }
  }  
  //! ----------------------------------------
  //! At this exeuciotn-poitn we asusme the oerpation was a success.
  return true;
}

//! Print the resutls of the clsutering in a 'human-redable-format' to STDOUt.
void printHumanReadableResult__toStdout__s_kt_clusterAlg_SOM_resultObject(const s_kt_clusterAlg_SOM_resultObject_t *self, char **mapOf_names) {
  assert(self);
  assert(self->celldata);
  assert(self->vertex_clusterid);
  FILE *stream_out = stdout;
  //! Apply:
  const char *seperator_cols = "\t";   const char *seperator_newLine = "\n";
  printHumanReadableResult__extensive__s_kt_clusterAlg_SOM_resultObject_t(self, mapOf_names, stream_out, seperator_cols, seperator_newLine, e_kt_clusterAlg_exportFormat_all);
}
