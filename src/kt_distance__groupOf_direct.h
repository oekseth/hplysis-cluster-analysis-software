#ifndef kt_distance__groupOf_direct_h
#define kt_distance__groupOf_direct_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */



/**
   @file kt_distance__groupOf_direct
   @brief provide support for difference "directScore" correlation-metrics, metrics described by oekseth (oekseth, 06. des. 2016).
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016). 
 **/

#include "e_kt_correlationFunction.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "correlation_enums.h"
#include "kt_distance__groupOf_direct__each.h"
#include "kt_distance__groupOf_direct__1ToMany.h"


//! Comptue for many-to-many
// @remarks Compute the correlation-score for the "directScore"-group of hpLysis-correlation-metrics.
void corr__computeDirectScore__manyToMany(const e_kt_correlationFunction_t metric_id, const uint nrows_1, const uint nrows_2, t_float **data_1, t_float **data_2, t_float *weight, char **mask1, char **mask2, t_float **resultMatrix, const e_cmp_masksAre_used_t  masksAre_used);


#endif //! EOF
