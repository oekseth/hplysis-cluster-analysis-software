#include "measure_kt_clusterAlg_svd.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure.h"


static const uint kilo = 1000;
static clock_t clock_time_start; static clock_t clock_time_end;
static struct tms tms_start;
static  struct tms tms_end;
/**
   @brief Start the measurement with regard to time.
   @remarks 
   - Uses the global vairalbe in this file for this prupose.
   - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
**/
static  void start_time_measurement() {
#ifndef NDEBUG
  //! Clears the cache by allocating 100MB of memory- and the de-allocating it:
  // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses.
  uint size = 1024*1024*100;
  char *tmp = new char[size];
  assert(tmp);
  memset(tmp, 7, size);
  delete [] tmp; tmp = NULL; size = 0;
#endif
  tms_start = tms();
  clock_time_start = times(&tms_start);
  // if(() == -1) // starting values
  //   err_sys("times error");
}
/**
   @brief Ends the measurement with regard to time.
   @param <msg> If string is given, print the status information to stdout.
   @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
   @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float end_time_measurement(const char *msg, float &user_time, float &system_time, const float prev_time_inSeconds) {
  clock_time_end = times(&tms_end); 
  long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
  const clock_t t_real = clock_time_end - clock_time_start;
  const float time_in_seconds = t_real/(float)clktck;
  const float diff_user = tms_end.tms_utime - tms_start.tms_utime;
  if(diff_user) user_time   = (diff_user)/((float)clktck);
  const float diff_system = tms_end.tms_stime - tms_start.tms_stime;
  system_time = diff_system/((float)clktck);
  if(msg) {
    // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
    if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) {
      printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
    } else {
      //float diff = prev_time_inSeconds / time_in_seconds;
      float diff = time_in_seconds / prev_time_inSeconds;
      // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
      printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg);
    }
  }
  //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
  return (float)time_in_seconds;
}


/**
   @brief Ends the measurement with regard to time.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float end_time_measurement(const char *msg, const float prev_time_inSeconds) {
  float user_time = 0, system_time=0;
  return end_time_measurement(msg, user_time, system_time, prev_time_inSeconds);
}



//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}

static void apply_tests() {
  const uint arrOf_cnt_buckets_size = 10; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40,
												  80, 160, 320, 740, 1000};
  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * kilo*1000;
    printf("----------\n\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    
    //! Tes tthe 'ideal' case:
    const char *stringOf_measureText = "Multiplication: ideal case";
    start_time_measurement();  //! ie, start measurement
    //! -------------
    t_float result = 0;
    for(uint i = 0; i < size_of_array; i++) {
      result = (i * i);
    }
    //! -------------        
    const float prev_time_inSeconds = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, FLT_MAX);
    printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    
    /* { //! Test wrt. the voaltiel key-word using a boolean operand-comparison: */
    /* //! -------------------------------- */
    /*   const double second_var = size_of_array*0.3; */
    /*   { const char *stringOf_measureText = "Boolean: ideal case"; */
    /* 	start_time_measurement();  //! ie, start measurement */
    /* 	t_float result = 0; */
    /* 	for(uint i = 0; i < size_of_array; i++) { */
    /* 	  const double x = (double)i; 	  const double y = second_var; */
    /* 	  result += (x >= y); */
    /* 	} */
    /* 	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);       */
    /* 	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__); */
    /*   } */
      //   // { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-code-file: inline";
    //   // 	start_time_measurement();  //! ie, start measurement
    //   // 	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*) = getFunction__largeOverhead__measure_codeStyle_functionRef(/*type=*/'b');
    //   // 	t_float result = 0;
    //   // 	for(uint i = 0; i < size_of_array; i++) {
    //   // 	  s_measure_codeStyle_functionRef_largeOverhead_t obj;
    //   // 	  func_ref(i, i, &obj); 	result += obj.sumOf;
    //   // 	}
    //   // 	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    //   // 	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    //   // }
    //   //! --------------------------------
    // }
  }
 
  assert(false); // FIXME: test the 'called' functions.
}


//! The main-function to test our "measure_codeStyle_functionRef_api"
void main__kt_clusterAlg_svd(int array_cnt, char **array) {


  // printf("-----, at %s:%d\n", __FILE__, __LINE__);
  //! Appply the performance-tests:
  apply_tests();

  assert(false); // FIXME: add soemthing ... use the 'sytntiec' asa tmplate.
  
}
