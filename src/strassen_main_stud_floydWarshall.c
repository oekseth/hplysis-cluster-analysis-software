#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_memAlloc.h"
#include "def_typeOf_float.h"
#include "def_intri.h"
#include "kt_mathMacros.h"
#include "types_base.h"
#include "types.h"

#ifndef macro_pluss
#define macro_pluss(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_add
#define macro_add(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_minus
#define macro_minus(term1, term2) ({term1 - term2;})
#endif
#ifndef macro_mul
#define macro_mul(term1, term2) ({term1 * term2;})
#endif
//! -------------------------------------------------------------------------------
//!
//! Type-independent speficiations of functions:
#define t_tileType t_float
#define t_tileTypeVec VECTOR_FLOAT_TYPE
#define vec_mul VECTOR_FLOAT_MUL
#define vec_load VECTOR_FLOAT_LOADU
#define vec_iter_size VECTOR_FLOAT_ITER_SIZE
#define vec_add VECTOR_FLOAT_ADD
#define vec_min VECTOR_FLOAT_MIN
#define vec_horizontalSum  VECTOR_FLOAT_storeAnd_horizontalSum
//! Function-names:
#define fast_floydWarshall fast_floydWarshall_float
#define fast_floydWarshall_xmtSSE fast_floydWarshall_xmtSSE_float
//! ------------------------------------------------------------------------


void fast_floydWarshall(t_tileType **matrix, t_tileType **matrix_2, const uint nrows, const uint ncols, t_tileType **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_tileType *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_tileType *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_tileType *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_tileType *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_tileType *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_tileType *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOADU(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; t_tileTypeVec vec_result = vec_add(vec_load(&row_1[int_j]), vec_load(&row_2[int_j])); 
	    //	    uint int_j = 0; t_tileTypeVec vec_result = vec_mul(vec_load(&row_1[int_j]), vec_load(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += vec_iter_size;
	    for(; int_j < SM; int_j += vec_iter_size) {
	      vec_result = vec_min(vec_result, vec_add(vec_load(&row_1[int_j]), vec_load(&row_2[int_j])));
	      //	      vec_result = vec_add(vec_result, vec_mul(vec_load(&row_1[int_j]), vec_load(&row_2[int_j])));
	      //	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] = macro_min(result[row_id+int_row_id][col_id+int_col_id], vec_horizontalSum(vec_result));
	    //	    result[row_id+int_row_id][col_id+int_col_id] += vec_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}


//! ------------------------------------------------------------------------
void fast_floydWarshall_xmtSSE(t_tileType **matrix, t_tileType **matrix_2, const uint nrows, const uint ncols, t_tileType **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_tileType *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_tileType *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_tileType *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_tileType *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_tileType *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_tileType *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; t_tileType vec_result = macro_pluss(row_1[int_j], row_2[int_j]);
	    int_j += 1;
	    for(; int_j < SM; int_j += 1) {
	      vec_result = macro_min(vec_result, macro_pluss(row_1[int_j], row_2[int_j]));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] = macro_min(result[row_id+int_row_id][col_id+int_col_id],  vec_result);
	  }
	}
      }
    }
  }
}

int main() {  }
//! ---------------------------------------------------------------------
//!
//! Reset:
#undef t_tileType
#undef t_tileTypeVec
#undef vec_mul
#undef vec_load
#undef vec_iter_size
#undef vec_add
#undef vec_horizontalSum
#undef vec_min
//! Function-names:
#undef fast_floydWarshall_xmtSSE
#undef fast_floydWarshall_xmtSSE
