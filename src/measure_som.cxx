#include "measure_som.h"
//#include "graphAlgorithms_som.h"
#include "kt_clusterAlg_SOM.h"
#include "measure_base.h"
#include "distance_2rows_slow.h"

static s_config_som_t som_configure; //! which is used at a 'lgobal scope' to simplify testinf of diffrent features.
//! ----
static uint nrows_fixed = UINT_MAX; static uint ncols_fixed = UINT_MAX;

static t_float prev_time_inSeconds = T_FLOAT_MAX;

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements__specify_SOM(const char *stringOf_measureText, const uint grid_x, const uint grid_y, const uint niter, const uint nrows, const uint ncols, const bool transpose,  const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    const char default_value_char = '\0'; const uint string_size = 2000;
    char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000);
    char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);

    sprintf(string, "\t nrows=%u ncols=%u transpose='%s' grid_x=%u grid_y=%u, number-of-iterations=%u, metrid-description=%s, typeOf_correlationPreStep='%s', tag:%s", 
	    nrows, ncols, 
	    (transpose) ? "yes" : "no", 
	    grid_x, grid_y, niter, 
	    stringOf_data_toAdd, get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(typeOf_correlationPreStep), 
	    stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    free_1d_list_char(&string); string = NULL; 
    //delete [] string; string = NULL; 
    
    prev_time_inSeconds = time_in_seconds;
    return time_in_seconds;
  }  
}


static void __compute_som(const uint grid_x, const uint grid_y, const uint nrows, const uint ncols, t_float** data, char** mask, const e_kt_correlationFunction metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {
  const int nxgrid = grid_x;
  const int nygrid = grid_y; /* Rectangular grid 2x2 */
  const double inittau = 0.02; /* Initial value of the neighborhood function */
  const int niter = 1000; /* Number of iterations */
  
  printf("\n----------------------------------------\n#\t Computes for a matrix=%u,%u, with SOM-feature-space=%u,%u, and distance-mtric='%d', at %s:%d\n", nrows, ncols, grid_x, grid_y, metric_id, __FILE__, __LINE__);

  //! Allocate memory:
  const t_float default_floatValue = 1;
  const uint default_intValue = 0;
  t_float *weight = allocate_1d_list_float(ncols, default_floatValue);
  const uint outerSize = 2;
  //void (*clusterid)[2] = malloc(nrows*sizeof(uint[2])); 
  //uint (*clusterid)[2] = NULL; //((*uint)[2])malloc(nrows*sizeof(uint[2])); 
  uint **clusterid = allocate_2d_list_uint(nrows, outerSize, default_intValue);
  //uint **clusterid = allocate_2d_list_uint(nrows, outerSize, default_intValue);
  t_float ***celldata = allocate_3d_list_float(nxgrid, nygrid, ncols, default_floatValue);

  //! -------------------------------
  //! Start the clock:
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'

  //! Compute the SOM-clusters:
  somcluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, data, mask, weight, /*trasnpose=*/0, nxgrid, nygrid, inittau,      niter, celldata, clusterid, &som_configure);  

  //! Write out the exeuciotn-time:
  __assertClass_generateResultsOf_timeMeasurements__specify_SOM("Computes SOM using optimal computations", grid_x, grid_y, niter, nrows, ncols, /*transpose=*/false, metric_id, typeOf_correlationPreStep);

  //free(celldata);
  free(weight);
  free_2d_list_uint(&clusterid, nrows);
  //free(clusterid);
  // FIXME: add [”elow]
  // free_2d_list_float(&celldata[0]);
  // free_generic_type_3d(celldata);
  free_3d_list_float(&celldata);

}


 static bool isTo_testMuliple = false;
 static bool isTo_reWriteSTD = true;
 static bool config_somWorker_useImproved_STD_useSSEIntrisinistics = true;
 static bool config_somWorker_useImproved___updateClosestPair = true;
 static bool config_somWorker_useImproved___updateClosestPair_useMemoryTiling = true;
 static bool config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = true;
 //static char config_distanceMetric_to_test = '\0'; //! which if set to a value in our "arrOf_distMetric" impleis that we only use one metric.
 static char config_distanceMetric_to_test = 'e'; 




static void call_optimized__fast() {

  //! Configure the 'SOM-configruation' using default configurations:
  s_config_som_init(&som_configure);


  // if(array && (array_cnt >= 3)) {
  //   if(0 == strcmp("-mulitiple", array[2])) {isTo_testMuliple = true;}
  //   else if(0 == strcmp("-notReWriteSTD", array[2])) {isTo_reWriteSTD = false;}
  // }

  const uint arrOf_distMetric_size_max = 8;
  uint arrOf_distMetric_size = arrOf_distMetric_size_max; //! ie, default assumption.
  char arrOf_distMetric[arrOf_distMetric_size_max] = {'e', 'b', 'c', 'a', 'u', 'x', 's', 'k'};
  if(config_distanceMetric_to_test != '\0') {
    arrOf_distMetric_size = 1;
    arrOf_distMetric[0] = config_distanceMetric_to_test;
  }

  //! Update the configuration-object:
  som_configure.config_somWorker_useImproved_STD = isTo_reWriteSTD;
  som_configure.config_somWorker_useImproved_STD_useSSEIntrisinistics = config_somWorker_useImproved_STD_useSSEIntrisinistics;
  som_configure.config_somWorker_useImproved___updateClosestPair = config_somWorker_useImproved___updateClosestPair;
  som_configure.config_somWorker_useImproved___updateClosestPair_useMemoryTiling = config_somWorker_useImproved___updateClosestPair_useMemoryTiling;
  som_configure.config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics;

  printf("# (info)\t isTo_testMuliple='%s'"
	 ", isTo_reWriteSTD='%s' (use SSE-intri='%s')"
	 ", isTo-iprove-closestPairUpdate='%s' (use SSE-intri='%s')"
	 ", at %s:%d\n", 
	 (isTo_testMuliple) ? "true" : "false",
	 (isTo_reWriteSTD)  ? "true" : "false",
	 (config_somWorker_useImproved_STD_useSSEIntrisinistics)  ? "true" : "false",
	 (config_somWorker_useImproved___updateClosestPair)  ? "true" : "false",
	 (config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics)  ? "true" : "false",
	 __FILE__, __LINE__);

  if(isTo_testMuliple) { //! Compute a small data-set: to evlauate the importance of the cell-grids we test for different SOM-sizes and diffnere tinptu-sizes:
    const t_float default_floatValue = 0;
    for(uint grid_x = 2; grid_x < 12; grid_x++) {
      for(uint grid_y = 2; grid_y < 12; grid_y++) {
	const uint nrows = (nrows_fixed != UINT_MAX) ? nrows_fixed : 500*16*2; const uint ncols = (ncols_fixed != UINT_MAX) ? ncols_fixed : 500*16*2;
	assert(nrows != UINT_MAX); assert(ncols != UINT_MAX);
	//const uint nrows = 15000, ncols = 6000;
	for(uint i = 1; i < 4; i++) {
	  for(uint out = 1; out < 4; out++) {

    // FIXME: remvoe [”elolow] and include [ªbove]:

    // { const uint grid_x = 2;
    //   { const uint grid_y = 2;
    // 	{const uint i = 1;
    // 	  {const uint out = 1;


	    const uint nrows = 500*i*4; const uint ncols = 200*out*4;
	    //! -------------------------------------------
	    const loint max_data_size = kilo * kilo * 100; //! ie, 100*1000*1000 elements to try using less than 1,000,000,000 GB of memory 
	    if((nrows*ncols) > max_data_size) {
	      fprintf(stderr, "The current row-size (%u)(%u) is outside the current max-member-threshold=%llu, ie, consider udpating the latter threadols (eg, if your a are running a large system than a latop with in-sufficient memoery, an observat ion at [%s]:%s:%d\n", nrows, ncols, max_data_size, __FUNCTION__, __FILE__, __LINE__);
	      continue;
	    }
	    //! -------------------------------------------

	    for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) {
	      //const e_kt_correlationFunction metric_id = e_kt_correlationFunction_groupOf_squared_Pearson;
	      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_index;
	      //e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	      if(false == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id))  {
		for(uint data_mod = 0; data_mod < 3; data_mod++) {
		  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)data_mod;
		  if( (typeOf_correlationPreStep != e_kt_categoryOf_correaltionPreStep_none) && (isTo_use_kendall__e_kt_correlationFunction(metric_id) ) ) {
		    continue; //! ie, as we then assume 'this case' is not of itnerest to evlauate.
		  }
		    //for(uint dist_index = 0; dist_index < arrOf_distMetric_size; dist_index++) {

		    // assert(false); // FIXME: udpate our procedure to iteratie throguh all of the correlation-emtrics and all of the post-rpcoessing-emtrics ... ie, to repalce tha t[above] "arrOf_distMetric" 'appraoch' (eosketh, 06. otk. 2016).



		  //const char dist = arrOf_distMetric[dist_index]; //'c'; /* Pearson correlation */
		  //const char dist = 'c'; /* Pearson correlation */
		  printf("nrows=%u, ncols=%u, at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
		  t_float **data = allocate_2d_list_float(nrows, ncols, default_floatValue);
		  for(uint i = 0; i < nrows; i++) {
		    for(uint out = 0; out < ncols; out++) {
		      t_float tmp_val = rand();
		      if(tmp_val != 0) {tmp_val = tmp_val / 10000;}
		      data[i][out] = tmp_val;
		      // data[i][out] = (t_float)&(data[i][out])/((i+1)*(out+1)); //! ie, use the memory-reference as a 'pasudo-random' number.
		    }
		  }
		  //! The call:	    
		  __compute_som(grid_x, grid_y, nrows, ncols, data, /*mask=*/NULL, metric_id, typeOf_correlationPreStep);
		  free_2d_list_float(&data, nrows);
		}
	      }
	    }
	  }
	}
      }
    }
  } else {
    //const uint nrows = 150; const uint ncols = 60;
    const uint nrows = (nrows_fixed != UINT_MAX) ? nrows_fixed : 500*16*2; const uint ncols = (ncols_fixed != UINT_MAX) ? ncols_fixed : 500*16*2;
    assert(nrows != UINT_MAX); assert(ncols != UINT_MAX);
    //const uint nrows = 500*16; const uint ncols = 500*16;
    //    const uint nrows = 500*16; const uint ncols = 200*16;
    //const uint nrows = 16; const uint ncols = 16;
    //const uint nrows = 3; const uint ncols = 5;
    //const uint nrows = 15000; const uint ncols = 3000;
    const uint grid_x = 4*4; const uint grid_y = 4*4;
    //const uint grid_x = 4*3; const uint grid_y = 4*3;
    //const uint grid_x = 4*2; const uint grid_y = 4*2;
    //for(uint dist_index = 0; dist_index < arrOf_distMetric_size; dist_index++) {
    // FIXME: remvoe [”elolow] and include [ªbove]:
    { const uint dist_index = 0;

      const char dist = arrOf_distMetric[dist_index]; //'c'; /* Pearson correlation */
    //! ----------------
      const t_float default_floatValue = 0;
      t_float **data = allocate_2d_list_float(nrows, ncols, default_floatValue);
      for(uint i = 0; i < nrows; i++) {
	for(uint out = 0; out < ncols; out++) {
	  const t_float tmp_val = rand();
	  if(tmp_val != 0) {data[i][out] = tmp_val / 10000;}
	  // data[i][out] = (t_float)&(data[i][out])/((i+1)*(out+1)); //! ie, use the memory-reference as a 'pasudo-random' number.
	}
      }
      //! The call:	    
      //const char dist = 'c'; /* Pearson correlation */

      // FIXME: udpate our procedure to iteratie throguh all of the correlation-emtrics and all of the post-rpcoessing-emtrics ... ie, to repalce tha t[above] "arrOf_distMetric" 'appraoch' (eosketh, 06. otk. 2016).
      const e_kt_correlationFunction metric_id = e_kt_correlationFunction_groupOf_squared_Pearson;
      const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;


      __compute_som(grid_x, grid_y, nrows, ncols, data, /*mask=*/NULL, metric_id, typeOf_correlationPreStep);
      free_2d_list_float(&data, nrows);
    }
  }
  // assert(false); // FIXME: add soemthing  
}



//! the main funciton for logic-texting
static void call_optimized__allDistanceMetrics() {
  for(uint test_multiple = 0; test_multiple < 2; test_multiple++) {
    isTo_testMuliple = test_multiple;
    for(uint rewrite_std = 0; rewrite_std < 2; rewrite_std++) {
      isTo_reWriteSTD = rewrite_std;
      for(uint useImproved_STD_SSE = 0; useImproved_STD_SSE < 2; useImproved_STD_SSE++) {
	config_somWorker_useImproved_STD_useSSEIntrisinistics = useImproved_STD_SSE;
	for(uint useImproved_closestPair = 0; useImproved_closestPair < 2; useImproved_closestPair++) {
	  config_somWorker_useImproved___updateClosestPair = useImproved_closestPair;
	  for(uint useImproved_closestPAir_memTiling = 0; useImproved_closestPAir_memTiling < 2; useImproved_closestPAir_memTiling++) {
	    config_somWorker_useImproved___updateClosestPair_useMemoryTiling = useImproved_closestPAir_memTiling;
	    for(uint useImproved_closestPAir_SSEandMemTiling = 0; useImproved_closestPAir_SSEandMemTiling < 2; useImproved_closestPAir_SSEandMemTiling++) {
	      config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = useImproved_closestPAir_SSEandMemTiling;
	      //!
	      //! Call:
	      call_optimized__fast();
	    }
	  }
	}
      }
    }
  }	
}

//! the main funciton for logic-texting
 static void call_optimized__singleMetricCase() {
   isTo_testMuliple = false;
   for(uint rewrite_std = 0; rewrite_std < 2; rewrite_std++) {
     isTo_reWriteSTD = rewrite_std;
     for(uint useImproved_STD_SSE = 0; useImproved_STD_SSE < 2; useImproved_STD_SSE++) {
       config_somWorker_useImproved_STD_useSSEIntrisinistics = useImproved_STD_SSE;
       for(uint useImproved_closestPair = 0; useImproved_closestPair < 2; useImproved_closestPair++) {
	 config_somWorker_useImproved___updateClosestPair = useImproved_closestPair;
	 for(uint useImproved_closestPAir_memTiling = 0; useImproved_closestPAir_memTiling < 2; useImproved_closestPAir_memTiling++) {
	   config_somWorker_useImproved___updateClosestPair_useMemoryTiling = useImproved_closestPAir_memTiling;
	   for(uint useImproved_closestPAir_SSEandMemTiling = 0; useImproved_closestPAir_SSEandMemTiling < 2; useImproved_closestPAir_SSEandMemTiling++) {
	     config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = useImproved_closestPAir_SSEandMemTiling;
	     //!
	     //! Call:
	     call_optimized__fast();
	   }
	 }
       }
     }
   }
}


static void call_subCase_timeCostMetricsIsolated__kendall() {
  
  for(uint multFact = 2; multFact < 8; multFact++) {
    const uint nrows = 100*16*multFact; const uint ncols = 100*16*multFact;
    printf("\n\n---- matrix-dims=[%u, %u], at [%s]:%s:%d\n----\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    //const uint nrows = 500*16; const uint ncols = 500*16;
    const loint max_data_size = kilo * kilo * 100; //! ie, 100*1000*1000 elements to try using less than 1,000,000,000 GB of memory 
    if((nrows*ncols) > max_data_size) {
      fprintf(stderr, "The current row-size (%u)(%u) is outside the current max-member-threshold=%llu, ie, consider udpating the latter threadols (eg, if your a are running a large system than a latop with in-sufficient memoery, an observat ion at [%s]:%s:%d\n", nrows, ncols, max_data_size, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
    const t_float default_value_float = 100;     const char default_value_char = 1;
    const uint size_of_array = ncols;
    t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
    t_float **matrix_transposed = allocate_2d_list_float(size_of_array, nrows, default_value_float);
    t_float **matrix_result = allocate_2d_list_float(nrows, nrows, default_value_float);
    int **mask1_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
    int **mask2_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
    char **mask1 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
    char **mask2 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
    t_float *weight = allocate_1d_list_float(size_of_array, default_value_float);
    int **mask1_int_transposed = allocate_2d_list_int(size_of_array, nrows, default_value_char);
    int **mask2_int_transposed = allocate_2d_list_int(size_of_array, nrows, default_value_char);
      
    { 
      // for(uint grid_x = 2; grid_x < 12; grid_x++) {
      // 	for(uint grid_y = 2; grid_y < 12; grid_y++) {	    
      for(uint grid_mult = 1; grid_mult < 12; grid_mult += 2) {
	{
	  const uint grid_x = 2*grid_mult; const uint grid_y = 2*grid_mult;
	  printf("---- SOM-dims=[%u, %u], at %s:%d\n", grid_x, grid_y, __FILE__, __LINE__);
	  for(uint case_slow = 0; case_slow < 2; case_slow++) {	      
	    const char *stringOf_measureText = "correlation-seperateStep(Kendall::slow::transpose::clusterC::dense-matrix::mask-int::transposed): idneitfy teh worst-case wrt. exeuction-time for SOM-correlation-step";
	    if(case_slow == false) {
	      prev_time_inSeconds = T_FLOAT_MAX; //! ie, reset.
	      stringOf_measureText = "correlation-seperateStep(Kendall::fast) test the case where we cobmine Knigths algorithm with pre-computation of scores";
	    }

	    struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
	      
	    //printf("at %s:%d\n", __FILE__, __LINE__);

	    setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);

	      
	    //! -------------------------------
	    //! Start the clock:
	    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'

	    bool transpose = true;
	    if(case_slow == false) {
	      transpose = false;
	      //printf("at %s:%d\n", __FILE__, __LINE__);
	      assert(isTo_use_kendall__e_kt_correlationFunction(/*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_coVariance)); //! ie, what we expect
	      init__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute, nrows, ncols, matrix, /*mask=*/NULL, /*needTo_useMask_evaluation=*/e_cmp_masksAre_used_false);
	      //printf("at %s:%d\n", __FILE__, __LINE__);
	    }
	    void (*metric_oneToMany) (config_nonRank_oneToMany_t) = setmetric__correlationComparison__oneToMany(e_kt_correlationFunction_groupOf_rank_kendall_coVariance, NULL, NULL, NULL, /*needTo_useMask_evaluation=*/e_cmp_masksAre_used_false);
	    s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, NULL, NULL, NULL, /*transpose=*/transpose, /*masksAre_used=*/e_cmp_masksAre_used_false, nrows, ncols);
	    t_float *arrOf_result_tmp  = allocate_1d_list_float(nrows, default_value_float);



	    //printf("at %s:%d\n", __FILE__, __LINE__);
	    t_float sumOf_values = 0;
	    for(uint ix = 0; ix < grid_x; ix++) {
	      const uint row_id = 5;
	      for(uint iy = 0; iy < grid_y; iy++) {
		if(case_slow == false) {
		  //printf("at %s:%d\n", __FILE__, __LINE__);
		  kt_compare__oneToMany(e_kt_correlationFunction_groupOf_rank_kendall_coVariance, e_kt_categoryOf_correaltionPreStep_none, 
					nrows, ncols, matrix, matrix, /*index1=*/row_id,  &config_metric, 

					&obj_kendallCompute,
					/*arrOf_result=*/arrOf_result_tmp,

					metric_oneToMany);

		  //printf("at %s:%d\n", __FILE__, __LINE__);		    
		    
		} else {
		  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
		    //! --------------
		    sumOf_values += kendall__slow_naiveImplementaiton(/*ncols=*/ncols, matrix_transposed, matrix_transposed, mask1_int_transposed, mask2_int_transposed, weight, row_id, row_id_out, /*transpose=*/transpose);
		  }
		}
	      }
	    }
	    //! --------------
	    __assertClass_generateResultsOf_timeMeasurements__specify_SOM(stringOf_measureText, grid_x, grid_y, /*niter=*/UINT_MAX, nrows, ncols, /*transpose=*/transpose, e_kt_correlationFunction_groupOf_rank_kendall_coVariance, e_kt_categoryOf_correaltionPreStep_none);
	    free_1d_list_float(&arrOf_result_tmp);
	  }
	}
      }
    }

    //! -------------------------------
    //! De-allcoate:
    free_2d_list_float(&matrix, nrows);
    free_2d_list_char(&mask1, nrows);
    free_2d_list_char(&mask2, nrows);
    free_2d_list_float(&matrix_transposed, size_of_array);
    free_2d_list_float(&matrix_result, nrows);
    free_2d_list_int(&mask1_int, nrows);
    free_2d_list_int(&mask2_int, nrows);
    free_2d_list_int(&mask1_int_transposed, size_of_array);
    free_2d_list_int(&mask2_int_transposed, size_of_array);
    free_1d_list_float(&weight);
  }
}

static void call_subCase_timeCostMetricsIsolated__euclid() {
  for(uint multFact = 2; multFact < 8; multFact++) {
    const uint nrows = 100*16*multFact; const uint ncols = 100*16*multFact;
    printf("\n\n---- matrix-dims=[%u, %u], at [%s]:%s:%d\n----\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    //const uint nrows = 500*16; const uint ncols = 500*16;
    const loint max_data_size = kilo * kilo * 100; //! ie, 100*1000*1000 elements to try using less than 1,000,000,000 GB of memory 
    if((nrows*ncols) > max_data_size) {
      fprintf(stderr, "The current row-size (%u)(%u) is outside the current max-member-threshold=%llu, ie, consider udpating the latter threadols (eg, if your a are running a large system than a latop with in-sufficient memoery, an observat ion at [%s]:%s:%d\n", nrows, ncols, max_data_size, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
    const t_float default_value_float = 100;     const char default_value_char = 1;
    const uint size_of_array = ncols;
    t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
    t_float **matrix_transposed = allocate_2d_list_float(size_of_array, nrows, default_value_float);
    t_float **matrix_result = allocate_2d_list_float(nrows, nrows, default_value_float);
    int **mask1_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
    int **mask2_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
    char **mask1 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
    char **mask2 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
    t_float *weight = allocate_1d_list_float(size_of_array, default_value_float);
    char **mask1_int_transposed = allocate_2d_list_char(size_of_array, nrows, default_value_char);
    char **mask2_int_transposed = allocate_2d_list_char(size_of_array, nrows, default_value_char);
      
    { 
      // for(uint grid_x = 2; grid_x < 12; grid_x++) {
      // 	for(uint grid_y = 2; grid_y < 12; grid_y++) {	    
      for(uint grid_mult = 1; grid_mult < 12; grid_mult += 2) {
	{
	  const uint grid_x = 2*grid_mult; const uint grid_y = 2*grid_mult;
	  printf("---- SOM-dims=[%u, %u], at %s:%d\n", grid_x, grid_y, __FILE__, __LINE__);
	  for(uint case_slow = 0; case_slow < 2; case_slow++) {	      
	    const char *stringOf_measureText = "correlation-seperateStep(Euclid::slow::transpose::clusterC::dense-matrix::mask-int::transposed): idneitfy teh worst-case wrt. exeuction-time for SOM-correlation-step";
	    if(case_slow == false) {
	      prev_time_inSeconds = T_FLOAT_MAX; //! ie, reset.
	      stringOf_measureText = "correlation-seperateStep(Euclid::fast) test the time-effoect of no-transpsoed data-access: we do not expect any large/singifance perofmrance-increase";
	    }

	    //struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
	      
	    //printf("at %s:%d\n", __FILE__, __LINE__);

	    //setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(obj_kendallCompute);

	      
	    //! -------------------------------
	    //! Start the clock:
	    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'

	    bool transpose = true;
	    if(case_slow == false) {
	      transpose = false;
	      //printf("at %s:%d\n", __FILE__, __LINE__);
	      //assert(isTo_use_kendall__e_kt_correlationFunction(/*metric_id=*/e_kt_correlationFunction_groupOf_rank_kendall_coVariance)); //! ie, what we expect
	      //init__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute, nrows, ncols, matrix, /*mask=*/NULL, /*needTo_useMask_evaluation=*/e_cmp_masksAre_used_false);
	      //printf("at %s:%d\n", __FILE__, __LINE__);
	    }
	    void (*metric_oneToMany) (config_nonRank_oneToMany_t) = setmetric__correlationComparison__oneToMany(e_kt_correlationFunction_groupOf_minkowski_euclid, NULL, NULL, NULL, /*needTo_useMask_evaluation=*/e_cmp_masksAre_used_false);
	    s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, NULL, NULL, NULL, /*transpose=*/transpose, /*masksAre_used=*/e_cmp_masksAre_used_false, nrows, ncols);
	    t_float *arrOf_result_tmp  = allocate_1d_list_float(nrows, default_value_float);



	    //printf("at %s:%d\n", __FILE__, __LINE__);
	    t_float sumOf_values = 0;
	    for(uint ix = 0; ix < grid_x; ix++) {
	      const uint row_id = 5;
	      for(uint iy = 0; iy < grid_y; iy++) {
		if(case_slow == false) {
		  //printf("at %s:%d\n", __FILE__, __LINE__);
		  kt_compare__oneToMany(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none, 
					nrows, ncols, matrix, matrix, /*index1=*/row_id,  &config_metric, 

					/*obj_kendallCompute=*/NULL,
					/*arrOf_result=*/arrOf_result_tmp,

					metric_oneToMany);

		  //printf("at %s:%d\n", __FILE__, __LINE__);		    
		    
		} else {
		  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
		    //! --------------
		    sumOf_values += __kt_euclid_slow(/*ncols=*/ncols, matrix_transposed, matrix_transposed, mask1_int_transposed, mask2_int_transposed, weight, row_id, row_id_out, /*transpose=*/transpose);
		  }
		}
	      }
	    }
	    //! --------------
	    __assertClass_generateResultsOf_timeMeasurements__specify_SOM(stringOf_measureText, grid_x, grid_y, /*niter=*/UINT_MAX, nrows, ncols, /*transpose=*/transpose, e_kt_correlationFunction_groupOf_rank_kendall_coVariance, e_kt_categoryOf_correaltionPreStep_none);
	    free_1d_list_float(&arrOf_result_tmp);
	  }
	}
      }
    }

    //! -------------------------------
    //! De-allcoate:
    free_2d_list_float(&matrix, nrows);
    free_2d_list_char(&mask1, nrows);
    free_2d_list_char(&mask2, nrows);
    free_2d_list_float(&matrix_transposed, size_of_array);
    free_2d_list_float(&matrix_result, nrows);
    free_2d_list_int(&mask1_int, nrows);
    free_2d_list_int(&mask2_int, nrows);
    free_2d_list_char(&mask1_int_transposed, size_of_array);
    free_2d_list_char(&mask2_int_transposed, size_of_array);
    free_1d_list_float(&weight);
  }
}

//! the main funciton for logic-texting
void measure_som_main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  
  //printf("cnt-args=%u, at %s:%d\n", array_cnt, __FILE__, __LINE__);
  
  if((array_cnt >= 3) && array[3] && strlen(array[3]))  {
    //printf("value=\"%s\", at %s:%d\n", array[3], __FILE__, __LINE__);
    if(0 == strncmp("tiny", array[3], strlen("tiny"))) {
      nrows_fixed = 20; ncols_fixed = 20;
    } else if(0 == strncmp("extraSmall", array[3], strlen("extraSmall"))) {
      nrows_fixed = 50; ncols_fixed = 50;
    } else if(0 == strncmp("small", array[3], strlen("small"))) {
      nrows_fixed = 16*20; ncols_fixed = 16*20;
    } else if(0 == strncmp("large", array[3], strlen("large"))) {
      nrows_fixed = 16*100 ; ncols_fixed = 16*100;
    } else if(0 == strncmp("extraLarge", array[3], strlen("extraLarge"))) {
      nrows_fixed = 16*400; ncols_fixed = 16*400;
    }
  }

  if(isTo_processAll || (0 == strncmp("call_optimized__fast", array[2], strlen("call_optimized__fast"))) ) {
    call_optimized__fast();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("call_optimized__allDistanceMetrics", array[2], strlen("call_optimized__allDistanceMetrics"))) ) {
    call_optimized__allDistanceMetrics();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("call_subCase_timeCostMetricsIsolated__kendall", array[2], strlen("call_subCase_timeCostMetricsIsolated__kendall"))) ) {
    call_subCase_timeCostMetricsIsolated__kendall();
    cnt_Tests_evalauted++;
  }


  if(isTo_processAll || (0 == strncmp("call_subCase_timeCostMetricsIsolated__euclid", array[2], strlen("call_subCase_timeCostMetricsIsolated__euclid"))) ) {
    call_subCase_timeCostMetricsIsolated__euclid();
    cnt_Tests_evalauted++;
  }





  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
