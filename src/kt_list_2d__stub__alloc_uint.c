  if(row_id >= self->list_size) {
    const uint list_size = (12 + row_id)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    assert(list_size < UINT_MAX);
    s_kt_list_1d_uint_t *new_list = MF__allocate__s_ktType_s_kt_list_1d_uint_t(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = setToEmpty__s_kt_list_1d_uint_t(); // MF__init__s_ktType_uint(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(self->list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      if(self->list != NULL) {
	MF__free__s_kt_list_1d_uint_t((self->list)); 
      }
    }
    //! Update pointer:
    self->list = new_list;    
  }
