/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @brief a wrapper-template to generate a terminal-bash-input-parser
   @remarks last generated at Fri Dec  2 15:20:45 2016 from "build_codeFor_tiling.pl"
 **/


         subString = get_subString_fromString_ifSet(array[i], "-input-file-1=");
         if(subString) {
            self.stringOf_input_file_1 = subString; cnt_updated++; ;
            //! Validate that the input-file is found:
            FILE *stream_tmp = fopen(subString, "rb");
            if (stream_tmp == NULL) {
               fprintf(stderr, "!!	 Unale to open the file \"%s\" for writing given input-parameter '-input-file-1=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);
            } else {fclose(stream_tmp);} //! ie, then close the file-descriptor.
         }
 
         subString = get_subString_fromString_ifSet(array[i], "-input-file-2=");
         if(subString) {
            self.stringOf_input_file_2 = subString; cnt_updated++; ;
            //! Validate that the input-file is found:
            FILE *stream_tmp = fopen(subString, "rb");
            if (stream_tmp == NULL) {
               fprintf(stderr, "!!	 Unale to open the file \"%s\" for writing given input-parameter '-input-file-2=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);
            } else {fclose(stream_tmp);} //! ie, then close the file-descriptor.
         }
 
         subString = get_subString_fromString_ifSet(array[i], "-input-file-1-mask=");
         if(subString) {
            self.stringOf_input_file_1_mask = subString; cnt_updated++; ;
            //! Validate that the input-file is found:
            FILE *stream_tmp = fopen(subString, "rb");
            if (stream_tmp == NULL) {
               fprintf(stderr, "!!	 Unale to open the file \"%s\" for writing given input-parameter '-input-file-1-mask=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);
            } else {fclose(stream_tmp);} //! ie, then close the file-descriptor.
         }
 
         subString = get_subString_fromString_ifSet(array[i], "-input-file-2-mask=");
         if(subString) {
            self.stringOf_input_file_2_mask = subString; cnt_updated++; ;
            //! Validate that the input-file is found:
            FILE *stream_tmp = fopen(subString, "rb");
            if (stream_tmp == NULL) {
               fprintf(stderr, "!!	 Unale to open the file \"%s\" for writing given input-parameter '-input-file-2-mask=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);
            } else {fclose(stream_tmp);} //! ie, then close the file-descriptor.
         }
 
         subString = get_subString_fromString_ifSet(array[i], "-input-file-1-columnWeights=");
         if(subString) {
            self.stringOf_input_file_1_weightList = subString; cnt_updated++; ;
            //! Validate that the input-file is found:
            FILE *stream_tmp = fopen(subString, "rb");
            if (stream_tmp == NULL) {
               fprintf(stderr, "!!	 Unale to open the file \"%s\" for writing given input-parameter '-input-file-1-columnWeights=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);
            } else {fclose(stream_tmp);} //! ie, then close the file-descriptor.
         }
 
         subString = get_subString_fromString_ifSet(array[i], "-input-file-2-columnWeights=");
         if(subString) {
            self.stringOf_input_file_2_weightList = subString; cnt_updated++; ;
            //! Validate that the input-file is found:
            FILE *stream_tmp = fopen(subString, "rb");
            if (stream_tmp == NULL) {
               fprintf(stderr, "!!	 Unale to open the file \"%s\" for writing given input-parameter '-input-file-2-columnWeights=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);
            } else {fclose(stream_tmp);} //! ie, then close the file-descriptor.
         }
 
         subString = get_subString_fromString_ifSet(array[i], "-result-file=");
         if(subString) {self.stringOf_resultFile = subString; cnt_updated++; } 
         subString = get_subString_fromString_ifSet(array[i], "-result-format=");
         if(subString) {self.result_format = subString; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-cnt-rows=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.nrows = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-cnt-rows=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
      updated_number = get_number_fromString(array[i], "-cnt-cols=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.ncols = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-cnt-cols=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
      updated_number = get_number_fromString(array[i], "-isTo_transposeMatrix=");
      if(updated_number != UINT_MAX) {self.isTo_transposeMatrix = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-input-use-stream-explicit-size=");
      if(updated_number != UINT_MAX) {self.isTo_readData_fromStream = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-include-stringIdentifers-in-matrix=");
      if(updated_number != UINT_MAX) {self.isTo_include_stringIdentifers_in_matrix = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-exportInto-javaScript-syntax=");
      if(updated_number != UINT_MAX) {self.isTo_inResult_isTo_useJavaScript_syntax = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-exportInto-JSON-syntax=");
      if(updated_number != UINT_MAX) {self.isTo_inResult_isTo_useJSON_syntax = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-result-isTo-exportInputFile=");
      if(updated_number != UINT_MAX) {self.isTo_inResult_isTo_exportInputData = updated_number; cnt_updated++; } 
          
         subString = get_subString_fromString_ifSet(array[i], "-help--printAll-correlationMetrics=");
         // fprintf(stderr, "!!	 test \"%s\" for writing given input-parameter '-help--printAll-correlationMetrics=': please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", subString, __FUNCTION__, __FILE__, __LINE__);         
         if(subString) {
              printAll_correlationMatric_enums__kt_distance();
         }
         subString = get_subString_fromString_ifSet(array[i], "-before-filter-correlation-type-id=");
         if(subString) {
            self.metric_beforeFilter.metric_id = get_enumBasedOn_configuration(subString);
            cnt_updated++; 
            
self.isToApply_correlation_beforeFilter = true;
;
         }
         updated_number = get_number_fromString(array[i], "-before-filter-correlation-type-isTo-export=");
         if(updated_number != UINT_MAX) {self.isToApply_correlation_beforeFilter_includeInResult = updated_number; cnt_updated++; } 
         subString = get_subString_fromString_ifSet(array[i], "-before-filter-correlation-preStep=");
         if(subString) {
            self.metric_beforeFilter.typeOf_correlationPreStep = get_typeOf_correlationPreStep_fromString(subString);
            cnt_updated++; 
            
self.isToApply_correlation_beforeFilter = true;
;
         }
         subString = get_subString_fromString_ifSet(array[i], "-after-filter-correlation-type-id=");
         if(subString) {
            self.metric_afterFilter.metric_id = get_enumBasedOn_configuration(subString);
            cnt_updated++; 
            
self.isToApply_correlation_afterFilter = true;
;
         }
         updated_number = get_number_fromString(array[i], "-after-filter-correlation-type-isTo-export=");
         if(updated_number != UINT_MAX) {self.isToApply_correlation_afterFilter_includeInResult = updated_number; cnt_updated++; } 
         subString = get_subString_fromString_ifSet(array[i], "-after-filter-correlation-preStep=");
         if(subString) {
            self.metric_afterFilter.typeOf_correlationPreStep = get_typeOf_correlationPreStep_fromString(subString);
            cnt_updated++; 
            
self.isToApply_correlation_afterFilter = true;
;
         }
         subString = get_subString_fromString_ifSet(array[i], "-in-clustering-correlation-type-id=");
         if(subString) {
            self.metric_insideClustering.metric_id = get_enumBasedOn_configuration(subString);
            cnt_updated++; 
            ;
         }
         subString = get_subString_fromString_ifSet(array[i], "-in-clustering-correlation-preStep=");
         if(subString) {
            self.metric_insideClustering.typeOf_correlationPreStep = get_typeOf_correlationPreStep_fromString(subString);
            cnt_updated++; 
            ;
         }

         subString = get_subString_fromString_ifSet(array[i], "-ifTwoMatrix-whenToMerge=");
         if(subString) {
            self.whenToMerge_differentMatrices  = get_enumFromString__e_kt_terminal_whenToMerge_t(subString);
            cnt_updated++; 
            ;
         }

         subString = get_subString_fromString_ifSet(array[i], "-hca_method=");
         if(subString) {
            self.clusterConfig.hca_method  = get_charConfigurationEnum_fromString__kt_clusterAlg_hca(subString);
            cnt_updated++; 
            
self.clusterConfig.isToCompute_hca = true;
;
         }
      updated_number = get_number_fromString(array[i], "-kmeans-number-of-clusters=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.clusterConfig.kmean_kmean_nclusters = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-kmeans-number-of-clusters=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
      updated_number = get_number_fromString(array[i], "-kmeans-number-of-iterations=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.clusterConfig.kmean_npass = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-kmeans-number-of-iterations=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
      updated_number = get_number_fromString(array[i], "-kmean-useMedian=");
      if(updated_number != UINT_MAX) {self.clusterConfig.kmean_isToUse_kMedian = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-kmean-alternativeAlgorithm-useMedoid=");
      if(updated_number != UINT_MAX) {self.clusterConfig.kmean_kmean_isToUse_medoidInComputation = updated_number; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-som-apply=");
      if(updated_number != UINT_MAX) {self.clusterConfig.isToCompute_som = updated_number; cnt_updated++; } 
      updated_float = get_float_fromString(array[i], "-som-Tau=");
      // fprintf(stdout, "(info) parameter='-som-Tau=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {
         self.clusterConfig.som_initTau = updated_float; cnt_updated++; ;
         //! Add value-range-test for float:
         if(updated_float < 0) {fprintf(stderr, "!!\t For parameter='-som-Tau=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
        }
      }
      updated_number = get_number_fromString(array[i], "-som-number-of-iterations=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.clusterConfig.som_niter = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-som-number-of-iterations=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
      updated_number = get_number_fromString(array[i], "-som_gridSize_x=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.clusterConfig.som_nxgrid = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-som_gridSize_x=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
      updated_number = get_number_fromString(array[i], "-som_gridSize_y=");
      if(updated_number != UINT_MAX) {
         if(updated_number > 1) {
           self.clusterConfig.som_nygrid = updated_number; cnt_updated++; 
         } else {
             fprintf(stderr, "!!	 For parameter='-som_gridSize_y=' your input-value '%u' is outside the expected range=[1, ....], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);
         }
;} 
         updated_number = get_number_fromString(array[i], "-pca-apply=");
         if(updated_number != UINT_MAX) {self.clusterConfig.isToCompute_pca = updated_number; cnt_updated++; } 
         subString = get_subString_fromString_ifSet(array[i], "-sample-data-distribution=");
         if(subString) {self.stringOf_sampleData_type = subString; cnt_updated++; } 
         subString = get_subString_fromString_ifSet(array[i], "-sample-data-distribution-secondMatrix=");
         if(subString) {self.stringOf_sampleData_type_secondMatrix = subString; cnt_updated++; } 
      updated_number = get_number_fromString(array[i], "-percent-toAppendWithSampleDistributionFor-rows=");
      if(updated_number != UINT_MAX) {self.fractionOf_toAppendWith_sampleData_type_rows = updated_number; cnt_updated++; ; 
      //! Add value-range-test:
      if( (updated_number > 100) || (updated_number < 0) ) {fprintf(stderr, "!!	 For parameter='-percent-toAppendWithSampleDistributionFor-rows=' your input-value '%u' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);}}
      updated_number = get_number_fromString(array[i], "-percent-toAppendWithSampleDistributionFor-columns=");
      if(updated_number != UINT_MAX) {self.fractionOf_toAppendWith_sampleData_type_columns = updated_number; cnt_updated++; ; 
      //! Add value-range-test:
      if( (updated_number > 100) || (updated_number < 0) ) {fprintf(stderr, "!!	 For parameter='-percent-toAppendWithSampleDistributionFor-columns=' your input-value '%u' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_number, __FUNCTION__, __FILE__, __LINE__);}}
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=");
      // fprintf(stdout, "(info) parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent); self.isTo_applyDataFilter = true; 
;
         //! Add value-range-test for flaot-aboveZero:
         if( (updated_float < 0) || (updated_float > 100) ){fprintf(stderr, "!!	 For parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);}
     }
    
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-percentValuesAboveCountForScalarValue-row-max=");
      // fprintf(stdout, "(info) parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-row-max=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent); self.isTo_applyDataFilter = true; 
;
         //! Add value-range-test for flaot-aboveZero:
         if( (updated_float < 0) || (updated_float > 100) ){fprintf(stderr, "!!	 For parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-row-max=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);}
     }
    
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-min=");
      // fprintf(stdout, "(info) parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-min=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent); self.isTo_applyDataFilter = true; 
;
         //! Add value-range-test for flaot-aboveZero:
         if( (updated_float < 0) || (updated_float > 100) ){fprintf(stderr, "!!	 For parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-min=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);}
     }
    
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-max=");
      // fprintf(stdout, "(info) parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-max=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent); self.isTo_applyDataFilter = true; 
;
         //! Add value-range-test for flaot-aboveZero:
         if( (updated_float < 0) || (updated_float > 100) ){fprintf(stderr, "!!	 For parameter='-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-max=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);}
     }
    
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-absoluteValueMinValue=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent_scalarAbsMin = updated_float; cnt_updated++; } 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-absoluteValueMinValue-row=");
      // fprintf(stdout, "(info) parameter='-filter-eitherOr-absoluteValueMinValue-row=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_absoluteValueAboveThreshold); self.isTo_applyDataFilter = true; 
;
         //! Add value-range-test for flaot-aboveZero:
         if( (updated_float < 0) || (updated_float > 100) ){fprintf(stderr, "!!	 For parameter='-filter-eitherOr-absoluteValueMinValue-row=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);}
     }
    
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-absoluteValueMinValue-column=");
      // fprintf(stdout, "(info) parameter='-filter-eitherOr-absoluteValueMinValue-column=' your input-value '%f' ... given  expected range=[0, 100]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);
      if(updated_float != T_FLOAT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_absoluteValueAboveThreshold); self.isTo_applyDataFilter = true; 
;
         //! Add value-range-test for flaot-aboveZero:
         if( (updated_float < 0) || (updated_float > 100) ){fprintf(stderr, "!!	 For parameter='-filter-eitherOr-absoluteValueMinValue-column=' your input-value '%f' is outside the expected range=[0, 100], ie, please update your input-parameters: if the latter error-message seems confusing then pelase contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", updated_float, __FUNCTION__, __FILE__, __LINE__);}
     }
    
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-valueDiff-rows-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_valueDiff_rows[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-valueDiff-rows-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_valueDiff_rows[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-valueDiff-cols-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_valueDiff_cols[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-valueDiff-cols-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_valueDiff_cols[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-STD-rows-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_STD_rows[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_STD); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-STD-rows-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_STD_rows[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_STD); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-STD-cols-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_STD_cols[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_STD); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-STD-cols-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_STD_cols[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_STD); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-kurtosis-rows-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_kurtosis_rows[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-kurtosis-rows-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_kurtosis_rows[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-kurtosis-cols-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_kurtosis_cols[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-kurtosis-cols-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_kurtosis_cols[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-skewness-rows-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_skewness_rows[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_skewness); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-skewness-rows-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_skewness_rows[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_skewness); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-skewness-cols-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_skewness_cols[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_skewness); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-eitherOr-skewness-cols-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__eitherOr_skewness_cols[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__eitherOr__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_eitherOr_skewness); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-valueUpperLower-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_valueUpperLower_matrix[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_minMax); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-valueUpperLower-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_valueUpperLower_matrix[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_minMax); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-meanAbsDiff-row-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_meanAbsDiff_row[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-meanAbsDiff-row-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_meanAbsDiff_row[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-meanAbsDiff-column-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_meanAbsDiff_column[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-meanAbsDiff-column-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_meanAbsDiff_column[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-medianAbsDiff-row-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_medianAbsDiff_row[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-medianAbsDiff-row-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_medianAbsDiff_row[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-medianAbsDiff-column-min=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_medianAbsDiff_column[0] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent); self.isTo_applyDataFilter = true; 
} 
      updated_float = get_float_fromString(array[i], "-filter-cellMask-medianAbsDiff-column-max=");
      if(updated_float != UINT_MAX) {self.filter_middleOf_correlationApplication.thresh_minMax__cellMask_medianAbsDiff_column[1] = updated_float; cnt_updated++; 
updateOrderOf_filter__cellMask__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent); self.isTo_applyDataFilter = true; 
} 
      updated_number = get_number_fromString(array[i], "-adjustValues-log=");
      if(updated_number != UINT_MAX) {cnt_updated++; 
updateOrderOf_adjust__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_adjustValues_log); self.isTo_applyDataAdjust = true; 
;} 

         subString = get_subString_fromString_ifSet(array[i], "-adjustValues-subract-from-row=");
         if(subString) {
            self.filter_middleOf_correlationApplication.typeOf_adjust_subtractOrAdd_rows  = get_enum_fromString__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(subString);
            cnt_updated++; 
            
updateOrderOf_adjust__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows);
;
         }

         subString = get_subString_fromString_ifSet(array[i], "-adjustValues-subract-from-columns=");
         if(subString) {
            self.filter_middleOf_correlationApplication.typeOf_adjust_subtractOrAdd_cols  = get_enum_fromString__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(subString);
            cnt_updated++; 
            
updateOrderOf_adjust__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows);
;
         }
      updated_number = get_number_fromString(array[i], "-adjustValues-sum-of-squares-rows=");
      if(updated_number != UINT_MAX) {cnt_updated++; 
updateOrderOf_adjust__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_adjustValues_center_normalization_rows);
;} 
      updated_number = get_number_fromString(array[i], "-adjustValues-sum-of-squares-columns=");
      if(updated_number != UINT_MAX) {cnt_updated++; 
updateOrderOf_adjust__kt_matrix_filter_t(&(self.filter_middleOf_correlationApplication), e_kt_matrix_filter_adjustValues_center_normalization_cols);
;} 