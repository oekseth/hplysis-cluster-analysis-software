#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_ccm.h"
#include "export_ppm.c" //! which is used for *.ppm file-export.

static void __tut_sim_16_manyMany_singleInput_exportTo_ppm(s_kt_matrix_t mat_result, const char *file_pref) {
	  //! Copy:
	  s_kt_matrix_t mat_cpy = initAndReturn__copy__s_kt_matrix(&mat_result, true);
	  //! Adjust(1): get max.
	  t_float score_max = T_FLOAT_MIN_ABS;
	  for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
	    for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
	      const t_float score = mat_cpy.matrix[row_id][col_id];
	      if(isOf_interest(score)) {
		score_max = macro_max(score_max, score);
	      }
	    }
	  }
	  if( (score_max != T_FLOAT_MIN_ABS) && (score_max != 0) ) {
	    //! Adjust(1):
	    t_float score_max_inv = 1/score_max;
	    for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
	      for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
		const t_float score = mat_cpy.matrix[row_id][col_id];
		if(isOf_interest(score)) {
		  mat_cpy.matrix[row_id][col_id] *= score_max_inv;
		}
	      }
	    }
	    //! Export:
	    char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_img.ppm", file_pref);
	    //	    const char *file_name_result_img = "tmp_img.ppm";
	    fprintf(stderr, "(info:export)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
	    ppm_image_t *picture = exportTo_image ( mat_cpy.matrix, mat_cpy.nrows, mat_cpy.ncols, mat_cpy.nameOf_rows, mat_cpy.nameOf_columns);
	    ppm_image_write ( file_name_result, picture );
	    ppm_image_finalize ( picture );
	    /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL); */
	    /* assert(is_ok); */
	    //! De-allocate:
	  }
	  free__s_kt_matrix(&mat_cpy);
}

//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
static void __tut_sim_16_manyMany_singleInput_allMetrics_ccmEval__computeForData(s_kt_matrix_base_t vec_1, s_kt_list_1d_uint_t list_hypo, e_hpLysis_clusterAlg_t clusterAlg, const char *prefix) {
  //!
  //! Open result-file-pointer: null-hypothesis: 
  char result_file[1000]; memset(result_file, '\0', 1000); sprintf(result_file, "%s_%s", prefix, "result_tut_sim_16_nullHyp.tsv");
  FILE *fPointer = fopen(result_file, "wb");
  assert(fPointer);
  printf("(info)\t Export results file=\"%s\", at %s:%d\n", result_file, __FILE__, __LINE__);
  //! Open result-file-pointer: clusterresults:
  const char *result_file_clust_ = "result_tut_sim_16_clust.tsv";
   char result_file_clust[1000]; memset(result_file_clust, '\0', 1000); sprintf(result_file_clust, "%s_%s", prefix, result_file_clust_);
   printf("(info)\t Export results file=\"%s\", at %s:%d\n", result_file_clust, __FILE__, __LINE__);
  FILE *fPointer_clusterAlg = fopen(result_file_clust, "wb");
  assert(fPointer_clusterAlg);
  //! Open result-file-pointer: clusterresults--gold:
  const char *result_file_clust_vsHyp_ = "result_tut_sim_16_clust_vs_hyp.tsv";
  char result_file_clust_vsHyp[1000]; memset(result_file_clust_vsHyp, '\0', 1000); sprintf(result_file_clust_vsHyp, "%s_%s", prefix, result_file_clust_vsHyp_);
  printf("(info)\t Export results file=\"%s\", at %s:%d\n", result_file_clust_vsHyp, __FILE__, __LINE__);
  FILE *fPointer_clusterAlg_vsHyp = fopen(result_file_clust_vsHyp, "wb");
  assert(fPointer_clusterAlg_vsHyp);
  //! 
  //! 
  //! Compute simlairty and CCM-score: 
  for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
    const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
    //! 
    printf("(sim-metric) \"%s\", at %s:%d\n", 
	   get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), //! where latter is defined in "kt_metric_aux.h"
	   __FILE__, __LINE__);
    //! 
    //! Specify/define the simlairty-metric:
    s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/metric_id, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
    //  s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_fidelity_Matusita, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
    //s_kt_matrix_base vec_2 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows_2, ncols);
    s_kt_matrix_base vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.
    //! 
    //! 
    //! Apply logics:
    s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
    const bool is_ok = apply__hp_distance(obj_metric, &vec_1, NULL,  &vec_result, /*config=*/hp_config);
    assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
    assert(vec_result.nrows == vec_1.nrows);   assert(vec_result.ncols == vec_1.nrows);
    if(true)  { //! then write out an eps-verison of the image:
      s_kt_matrix_t mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
      char result_file[1000]; memset(result_file, '\0', 1000); sprintf(result_file, "%s_%s", prefix, get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id));
      __tut_sim_16_manyMany_singleInput_exportTo_ppm(mat_result, /*file_pref=*/result_file);
    }
    //!
    //! 
    for(uint case_id = 0; case_id < 2; case_id++) {
      FILE *fPointer_local = fPointer;
      //! Compute the matrix-based CCMs:
      s_kt_list_1d_float_t list_ccm = setToEmpty__s_kt_list_1d_float_t();
      if(case_id == 0) {
	const bool is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_result, list_hypo.list, &list_ccm);
	assert(is_ok);
	assert(list_ccm.list_size == (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef); //! ie, what we expect.
      } else { //! then we compute the cluster-simliarty, and therafter infer CCM-score:
	fPointer_local = fPointer_clusterAlg;
	//!
	//! Infer clusters: 
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	//! Configure:
	obj_hp.config.corrMetric_prior_use = false; //! ie, as we have alredy comptued the simlairty-matrix. 	
	//! Get shallow copy, ie, to satisify the inptu-expecaitosn of our "hpLysis_api.h":
	s_kt_matrix_t mat_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	//! 
	bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_shallow, /*nclusters=*/UINT_MAX, /*npass=*/UINT_MAX);
	assert(is_ok);
	//! Fetch the clsuter-identiigfges:
	//! Note: we expecta taht the k-means-object is set through use of CCMs in parition-idnetificoants of the HCA-tree.
	const s_kt_clusterAlg_fixed_resultObject_t obj_result_kMean = obj_hp.obj_result_kMean;
	uint *vertex_clusterId = obj_result_kMean.vertex_clusterId;
	if(vertex_clusterId != NULL) { //! then clusters were inferred.
	  assert(obj_result_kMean.cnt_vertex <= vec_result.nrows); //! ie, as it may be that Not all vertices are member of mroe clsuters 'than itself'.
	  //!
	  //! Compute CCM-score: 
	  is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_result, vertex_clusterId, &list_ccm);
	  assert(is_ok);
	  if(fPointer_clusterAlg_vsHyp) { //! then comptue [gold x cluster-result]: 
	    s_kt_list_1d_float_t list_ccm_gold = setToEmpty__s_kt_list_1d_float_t();
	    const bool is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(vertex_clusterId, list_hypo.list, /*cnt_vertices=*/macro_min(obj_result_kMean.cnt_vertex, vec_result.nrows), 
									/*matrix-1=*/vec_result.matrix, 
									/*matrix-2=*/vec_result.matrix, 
									/*result=*/&list_ccm_gold);
	    assert(is_ok);
	    //! 
	    //! Write out the results: 
	    { //! Explcitly write out the result, where where we ahve sued a CCM-gold to comptue the latter:
	      //! Note: in [”elow] we examplfiy how lgocis in our "kt_matrix_cmpCluster.h" may be used wrt. 'understading' the meaning/stirng-name of the CCM-matrix-enums.
	      for(uint i = 0; i < list_ccm_gold.list_size; i++) {
		const t_float score = list_ccm_gold.list[i];
		if(isOf_interest(score)) { //! then we asusem the results is set:	    
		  if(false) {
		    printf("%s\t%s\t%f\n",
			   //! Sim-Metric:
			   get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), //! where latter is defined in "kt_metric_aux.h"
			   //! CCM-Metric:
			   getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i), //! where latter is defined in "kt_matrix_cmpCluster.h"
			   //! Score:
			   score);
		  }
		  assert(fPointer_clusterAlg_vsHyp);
		  if(fPointer_clusterAlg_vsHyp) {
		    fprintf(fPointer_clusterAlg_vsHyp, "%s\t%s\t%f\n",
			    //! Sim-Metric:
			    get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), //! where latter is defined in "kt_metric_aux.h"
			    //! CCM-Metric:
			    getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i), //! where latter is defined in "kt_matrix_cmpCluster.h"
			    //! Score:
			    score);
		  }
		}
	      }
	    }
	    //! De-allocate:
	    free__s_kt_list_1d_float_t(&list_ccm_gold);
	  }
	  //!
	  //! De-allocate: 
	  free__s_hpLysis_api_t(&obj_hp);
	}
      }
      //! 
      //! 
      { //! Explcitly write out the result:
	//! Note: in [”elow] we examplfiy how lgocis in our "kt_matrix_cmpCluster.h" may be used wrt. 'understading' the meaning/stirng-name of the CCM-matrix-enums.
	for(uint i = 0; i < list_ccm.list_size; i++) {
	  const t_float score = list_ccm.list[i];
	  if(isOf_interest(score)) { //! then we asusem the results is set:	    
	    if(false) {
	      printf("%s\t%s\t%f\n",
		     //! Sim-Metric:
		     get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), //! where latter is defined in "kt_metric_aux.h"
		     //! CCM-Metric:
		     getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i), //! where latter is defined in "kt_matrix_cmpCluster.h"
		     //! Score:
		     score);
	    }
	    assert(fPointer);
	    if(fPointer_local) {
	      fprintf(fPointer_local, "%s\t%s\t%f\n",
		     //! Sim-Metric:
		     get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), //! where latter is defined in "kt_metric_aux.h"
		     //! CCM-Metric:
		     getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i), //! where latter is defined in "kt_matrix_cmpCluster.h"
		     //! Score:
		     score);
	    }
	  }
	}
      }
      //!
      //! De-allocate:
      free__s_kt_list_1d_float_t(&list_ccm); 
    }
    if(false) { //! then explcitly write out the comptued simalrity-metrics: 
      //!
      //! 'Fetch' out the result:
      for(uint row_id = 0; row_id < vec_result.nrows; row_id++) {
	for(uint col_id = 0; col_id < vec_result.ncols; col_id++) {
	  //!
	  //! Write out the result:
	  fprintf(stdout, "# Result: \t\t sim-score[%u][%u]=\"%f\", at [%s]:%s:%d\n", row_id, col_id, vec_result.matrix[row_id][col_id], __FUNCTION__, __FILE__, __LINE__);
	  //fprintf(stdout, "# Result: \t\t sim-score[%u][%u]=\"%f\", at [%s]:%s:%d\n", vec_result.matrix[row_id][col_id], __FUNCTION__, __FILE__, __LINE__);
	}
      }
    }
    //!
    //! De-allocate:    
    free__s_kt_matrix_base_t(&vec_result);
  }
  if(fPointer) {
    fclose(fPointer); fPointer = NULL;
  }
  if(fPointer_clusterAlg) {
    fclose(fPointer_clusterAlg); fPointer_clusterAlg = NULL;
  }
  if(fPointer_clusterAlg_vsHyp) {
    fclose(fPointer_clusterAlg_vsHyp); fPointer_clusterAlg_vsHyp = NULL;
  }
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief demonstrates how similartiy-metrics combiend with a CCM may be used to describe the simlairty between two matrices.
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks in this example we use (a) one singular matrix, and (b) examplifes how 'this matrix' may be set explcitly, examplfied through/by iteraitng through a for-loop.
   @remarks many-to-many: a simple permtaution of "tut_sim_5_oneToMany_Gower.c" where we comptue for "row(...) x row(...)" wrt. the two input-matrices.
   @remarks general: use logics in our "hp_distance" to comptue the simalrit between two vectors.
   @remarks an example-applicaiton is seen in (one of) our implementaiotns of the "kmeans++" algorithm ("____init_centers_pySciKitLearn__kmeanspp__s_kt_matrix_base_t(..)"). 
   @remarks simliartiy: comptue the sim-matrix="innerproduct--coVariance" between two matrices using our "hp_distance.h" API 
   @remarks "hp_distance": we call the "apply__hp_distance(..)" function;
   @remarks generic: a permtaution of "tut_sim_14_manyMany_Matusita__setInputInLoop.c" and "tut_ccm_2_matrixVSgold_Silhouette.c" where we use CCM-evaluation for clsuter-anlaysis and evaluates the complete set of 'base' simliarty-metrics. 
   @remarks generic: we use the "tut_ccm_13_categorizeData.c" wrt. the iteraiton of simalirty-metrics.
   @remarks generalizaiton(example): to simplify the understanidng and re-use of our appraoch we have strucutred our "tut_sim_*" examples to use the same 'example-seutp', ie, where differneces is found wrt. the API-calls in to our "hp_distance", and wrt. different "s_kt_correlationMetric" calls.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#else
  int tut_sim_16_manyMany_singleInput_allMetrics_ccmEval()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  const uint nrows = 200; const uint ncols = 1024; 
  e_hpLysis_clusterAlg_t clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //  const uint nrows = 200; const uint ncols = 100; 
  //  const uint nrows = 20; const uint ncols = 100; 
  //  const uint nrows_2 = 6; 
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/ /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"

  //! Allocate two vectors, ie, matrices with demsinos [1, ncols]:
  //! Note: for simplicty we use [”elow] fucntion, defined in our "kt_matrix_base.h", ie, to set the vectors to default valeus, therby reducing the nubmer of code-lines in this use-case-example; in the latter randomenss is used, ie, for which we expect the vectors to be different.
  assert(nrows > 2);   assert(ncols > 0); //! ie, as we otherise have a poinbtelss' call
  s_kt_matrix_base vec_1 = initAndReturn__s_kt_matrix_base_t(nrows, ncols);
  assert(vec_1.matrix);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      //! Note: in [”elow] we describe/iniclude the 'nvoel part' of this "hp_distance" use-case:
      vec_1.matrix[row_id][col_id] = (1+row_id)*(0.5*col_id); //! ie, set the matrix-value to an 'explcitli/arbitrary/wild-case score' 
    }
  }
  //!
  //! Consturct a hypotehtical clsuter-result-set.
  //! Note: [below] examplfies the use of a hard-coded data-set to evlauate/cosnider the accuracy of pariwise simliarty-metrics:
  assert(vec_1.nrows > 0);
  s_kt_list_1d_uint_t list_hypo = init__s_kt_list_1d_uint_t(vec_1.nrows); //! where latter is deifne din our "kt_list_1d.h".
  { //! Set the scores: for simplicty split the data-set intow two equal parts:
    uint row_id = 0;
    uint cnt_half = (uint)((t_float)vec_1.nrows * 0.5);
    for(row_id = 0; row_id < cnt_half; row_id++) {
      list_hypo.list[row_id] = /*cluster-id=*/0;
    }
    for(; row_id < vec_1.nrows; row_id++) {
      list_hypo.list[row_id] = /*cluster-id=*/1;
    }
  }
  if(false) {
    //!
    //! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
    __tut_sim_16_manyMany_singleInput_allMetrics_ccmEval__computeForData(vec_1, list_hypo, clusterAlg, "random");
  }

  //!
  //! De-allocate:
  free__s_kt_matrix_base_t(&vec_1);
  free__s_kt_list_1d_uint_t(&list_hypo);
  { //! Then an altetntive file-data-load-strategy:
    const char *config__fileName__binaryMatrix = "data/vilje-cost-rack1-half.dat";
    mdata_t dat;   mdata_initf ( &dat, config__fileName__binaryMatrix );
    if(!dat.n) {
      fprintf(stderr, "(vilje-not-at-path)\t Seems like the cosntructed matrix was empty when laoding from \"%s\", ie, please invesetivgate. OBservaiton at [%s]:%s:%d\n", config__fileName__binaryMatrix, __FUNCTION__, __FILE__, __LINE__);
      return true;      
    }
    //! Itniate:
    s_kt_matrix_t matrix_concatToAll__type__overhead = initAndReturn__s_kt_matrix(dat.n, dat.n);
    s_kt_matrix_t matrix_concatToAll__type__latency = initAndReturn__s_kt_matrix(dat.n, dat.n);
    //!
    //! Load the Vilje super-comptuer matrix (into our two data-strucutres), where logics are foudn in  the "matrixdata.h" written by "jancrhis@ntnu.no":
    for(uint i=0; i<(uint)dat.n; i++ ) {
      for(uint j=0; j<(uint)dat.n; j++ ) {
	matrix_concatToAll__type__overhead.matrix[i][j] = TS(dat,i,j);
	matrix_concatToAll__type__latency.matrix[i][j]  = TL(dat,i,j);
      }
    }
    //! De-allcoate the 'parser':
    mdata_finalize ( &dat );
    const uint nrows = matrix_concatToAll__type__overhead.nrows;
    assert(nrows > 0);

    s_kt_list_1d_uint_t list_hypo = init__s_kt_list_1d_uint_t(nrows); //! where latter is deifne din our "kt_list_1d.h".
    { //! Set the scores: for simplicty split the data-set intow two equal parts:
      uint row_id = 0;
      uint cnt_half = (uint)((t_float)nrows * 0.5);
      for(row_id = 0; row_id < cnt_half; row_id++) {
	list_hypo.list[row_id] = /*cluster-id=*/0;
      }
      for(; row_id < nrows; row_id++) {
	list_hypo.list[row_id] = /*cluster-id=*/1;
      }
    }
    if(true) { //! then compute Euclid and write out the results to a matrix:
      {
	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__overhead);
	//	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__latency);
	{
	  bool is_ok = export__singleCall__s_kt_matrix_t(&matrix_concatToAll__type__overhead, "tmp_input.tsv", NULL);
	  assert(is_ok);
	}
	//	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__overhead);
	//	const char *file_name_result = "vilje_result_metric_Euclid_latency.tsv";
	const char *file_name_result = "vilje_result_metric_Euclid_overhead.tsv";
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	//	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/metric_id, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"      
	s_hp_distance__config_t hp_config = init__s_hp_distance__config_t(); 
	//!
	//! Compute similairty: 
	s_kt_matrix_base_t vec_result = initAndReturn__empty__s_kt_matrix_base_t();
	bool is_ok = apply__hp_distance(obj_metric, &vec_1, NULL,  &vec_result, /*config=*/hp_config);      
	assert(is_ok);
	//!
	//! Export result: 
	s_kt_matrix_t mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	if(true) {
	  __tut_sim_16_manyMany_singleInput_exportTo_ppm(mat_result, /*file_pref=*/"vilje_overhead");
	} else {
	  fprintf(stderr, "(info)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
	  is_ok = export__singleCall__s_kt_matrix_t(&mat_result, file_name_result, NULL);
	  assert(is_ok);
	}
	//!
	//! De-allocate:
	free__s_kt_matrix_base_t(&vec_result);
      }
    }
    //s_kt_matrix_fileReadTuning_t config = initAndReturn__s_kt_matrix_fileReadTuning_t();
    {
      //!
      //! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__overhead);
      __tut_sim_16_manyMany_singleInput_allMetrics_ccmEval__computeForData(vec_1, list_hypo, clusterAlg, "vilje_overhead");
    }
    {
      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__latency);
      __tut_sim_16_manyMany_singleInput_allMetrics_ccmEval__computeForData(vec_1, list_hypo, clusterAlg, "vilje_latency");
    }
    
    //!
    //! De-allocate:
    free__s_kt_matrix(&matrix_concatToAll__type__overhead);
    free__s_kt_matrix(&matrix_concatToAll__type__latency);
  free__s_kt_list_1d_uint_t(&list_hypo);    
    //    s_kt_matrix_t mat_real = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file=*/"data/vilje-cost-rack1-half.dat", config);
  }

  //free__s_kt_matrix_base_t(&vec_2);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}
 

