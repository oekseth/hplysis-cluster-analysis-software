    //! 
    //! Local configurations:
#define __MiV__matrixDims 1 //! ie, to 'enalbe' lcoal speicficoant of [”elow] varialbes:
    const uint sizeOf__nrows = 10;   const uint sizeOf__ncols = 10;
    //  const uint sizeOf__nrows = 50;   const uint sizeOf__ncols = 50;
    const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
    const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
    const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------
    // const e_kt_correlationFunction_t metric__beforeClust = e_kt_correlationFunction_groupOf_minkowski_euclid;
    // const e_kt_correlationFunction_t metric__insideClust = e_kt_correlationFunction_groupOf_minkowski_euclid;
    // //const bool fileRead_config__transpose = false;
    // const e_hpLysis_clusterAlg_t clusterAlg = e_hpLysis_clusterAlg_kCluster__rank;
#include "tut__stub__config__inputData.c" //! which is used as 'cefualt cofnigruations' for [”elow]
    //! --------------------------------------------
    //!
    //! File-specific cofnigurations: 
    s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
    //fileRead_config.isTo_transposeMatrix = true;
    fileRead_config.isTo_transposeMatrix = false;
    fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
    fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
    fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
    s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
    //fileRead_config.isTo_transposeMatrix = true;
    fileRead_config.isTo_transposeMatrix = true;
    //!
    //s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
#undef __MiV__matrixDims
    //! -----------------------------------------------------------------------------------------
    //!
    //!
