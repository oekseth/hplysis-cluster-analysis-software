s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
//!
{ //! Load the data-set:
  const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  20*fraction_colNoise;
#ifndef NDEBUG //! then we are interestied in validating the correctness of the "obj_baseToInclude", ie, as we have 'earlier' observed errors wrt. the matrix-allcoation-routine (oekseht, 06. jan. 2017).
      if(obj_baseToInclude.nrows) {
	for(uint row_id = 0; row_id < obj_baseToInclude.nrows; row_id++) {
	  //assert(obj_baseToInclude.matrix[row_id]);
	  assert(row_id < obj_baseToInclude.nrows);
	  assert_possibleOverhead(row_id < obj_baseToInclude.nrows);
	  const t_float *__restrict__ row = obj_baseToInclude.matrix[row_id];
	  assert(row);
	}
      }
#endif

 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
}
if(stringOf_sampleData_type_realLife == NULL) {
  assert(obj_matrixInput.nrows > 10); //! ie, to 'have a meaningful comparison'.
 }
//! -----------------------------------
//!
    { //! Compute the correlation-matrix and exprot the heat-map:
      //! Note: we use [below] string in our "heatMap.pl", ie, if [below] is udpated then remember updating oru regex wrt. "heatMapsBase" in our "__buildHeatMapsFor__dir(...)" (oekseth, 06. des. 2016)
      assert(mult_factor >= 1);
      char string_local__corr_prior[2000];
      assert(strlen(stringOf_tagSample) < 1500); //! ie, what we expect
      //fprintf(stderr, "stringOf_tagSample=\"%s\", at %s:%d\n", stringOf_tagSample, __FILE__, __LINE__);
      /* if(stringOf_prefix == NULL) { */
      /* 	if(stringOf_sampleData_type_realLife == NULL) { */
      /* 	  sprintf(string_local__corr_prior, "tagLocalIndex_%u.tagNrows_%u.tagExperiment_%s.tagSample_%s.tsv", (mult_factor-1), nrows, stringOf_experimentDescription, stringOf_tagSample); */
      /* 	} else { //! then we do Not set the nrows-count (ie, as we expect each file-input to have different input-sizes): */
      /* 	  sprintf(string_local__corr_prior, "tagLocalIndex_%u.tagNrows_fraction_colNoise_%u.tagExperiment_%s.tagSample_%s.tsv", (mult_factor-1), fraction_colNoise, stringOf_experimentDescription, stringOf_tagSample); */
      /* 	} */
      /* } else { //! then we asusme a specific correlation-type is used to genrate the results */
      if(stringOf_sampleData_type_realLife == NULL) {
	sprintf(string_local__corr_prior, "groupId_%u.tagLocalIndex_%u.tagNrows_%u.tagExperiment_%s.tagSample_%s.correlationSampleCategory_%s.tsv", groupId, local_col_index, obj_matrixInput.nrows, stringOf_experimentDescription, stringOf_tagSample, stringOf_prefix);
      } else { //! then we do Not set the nrows-count (ie, as we expect each file-input to have different input-sizes):
	sprintf(string_local__corr_prior, "groupId_%u.tagLocalIndex_%u.tagNrows_fraction_colNoise_%u_%u.tagExperiment_%s.tagSample_%s.correlationSampleCategory_%s.tsv", groupId, local_col_index, obj_matrixInput.nrows, fraction_colNoise, stringOf_experimentDescription, stringOf_tagSample, stringOf_prefix);
      }
      { //! 'clean' the string:
	const uint cnt_chars = strlen(string_local__corr_prior); assert(cnt_chars > 0);
	for(uint i = 0; i < cnt_chars; i++) {
	  if(string_local__corr_prior[i] == '/') {string_local__corr_prior[i] = '_';} //! ie, to 'remove' a directoyr-prefix-symbol.
	}
      }
	
      //fprintf(stderr, "stringOf_tagSample=\"%s\", string_local__corr_prior=\"%s\", at %s:%d\n", stringOf_tagSample, string_local__corr_prior, __FILE__, __LINE__);

      //}
      //char string_local__corr[2000]; sprintf(string_local, "%s.subset_naiveMetrics.afterClust.tsv", base_string);
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, &fileHandler__localCorrMetrics);
      hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
      hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
      hp_config.config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //!
      //! Correlate and export:
      const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
      assert(is_ok);
      //! De-allocates the "s_hpLysis_api_t" object.
      free__s_hpLysis_api_t(&hp_config);	    
    }
//! -----------------------------------
//!
free__s_kt_matrix(&obj_matrixInput);
