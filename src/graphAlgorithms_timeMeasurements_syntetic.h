#ifndef graphAlgorithms_timeMeasurementsExternalTools_syntetic_h
#define graphAlgorithms_timeMeasurementsExternalTools_syntetic_h

/**
   @file 
   @brief provide implementaitons of algorithms for computation of distance, rank and correlation
**/

#include "types.h"
#include "kt_clusterAlg_fixed.h"

/**
   @namespace graphAlgorithms_timeMeasurementsExternalTools_syntetic
   @brief evaluate/benchmark different sections of systetic examples extracted/found in/from different software-lbiraries.
   @author Ole Kristian Ekseth (oekseth).
**/
namespace graphAlgorithms_timeMeasurementsExternalTools_syntetic {
  void main();
}


#endif
