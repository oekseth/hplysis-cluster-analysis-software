// TODO: validate correnctess of teh 'new'inferred'/set "weight_index" ... ie, that the latter 'refers' to teh column-pos


// TODO[JC]: may you JC figure out why the VECTOR_FLOAT_LOAD(..) calls results in a memory-error when using the kt_compare__oneToMany(..) funciton-calls 'as a call'er (while the work 'wokrs-as-it-shoudl' for all-agaisnt-all)? (oekseth, 06. nov. 2016)


#if( (configure_SSE_useFor_mask_explicit != 0) )
if(chunkSize_cnt_i_intris > (VECTOR_FLOAT_ITER_SIZE*4)) {
#if(TEMPLATE_distanceConfiguration_useWeights == 1) 	    //! Load the weigth-macros:
  const uint weight_index = i + j2; VECTOR_FLOAT_TYPE vec_weight = VECTOR_FLOAT_LOADU(&weight[/*index=*/weight_index]);
#endif
  VECTOR_FLOAT_TYPE term1  = VECTOR_FLOAT_LOADU(&rmul1[j2]); VECTOR_FLOAT_TYPE term2  = VECTOR_FLOAT_LOADU(&rmul2[j2]);

  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros( (i + j2 + VECTOR_FLOAT_ITER_SIZE) <= ncols); //! ie, validate that we are 'isndie' the memory-boundary (oekseth, 06. mar. 2017)
  //! Apply the mask-filters:
  VECTOR_FLOAT_TYPE vec_maskCount; internalTemplateMacro__applyMasks(vec_maskCount);

  //! Call a generlized funciton to include the weights:
  internalTemplateMacro__includeWeights__eachTerm__SSE();	    

  //! Compute the distance:
  VECTOR_FLOAT_TYPE mul; internalTemplateMacro__computeDistance__SSE(mul);
  //! ......................................................................


  //! start: update the distance-counts:
#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_distanceConfiguration_useWeights == 1) )//! then we are to update the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice: then we adjust the score through weights:
  vec_maskCount = VECTOR_FLOAT_MUL(vec_maskCount, vec_weight); //! ie, then muliply the weight with the mask-count, eg, as seen wrt. comptuation of Perasosn correlation-coeffeicnet in the "cluster.c" naive-software-impelemtantion
#endif
#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) )
  objLocalComplex_nonSSE.cnt_interesting_cells = VECTOR_FLOAT_storeAnd_horizontalSum(vec_maskCount); //! ie, intialise the temproal result-vector.
#elif( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) )
  objLocalComplex_sse.cnt_interesting_cells = vec_maskCount; //! ie, intialise the temproal result-vector.
#endif
  //! end: update the distance-counts:


#if((TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) )
  VECTOR_FLOAT_TYPE vec_result = mul; //! ie, initaite.
#endif


  //! Call a generlized funciton to include the weights wr.t the post-processing:
  internalTemplateMacro__includeWeights__forResult__SSE();
  
  //printf("(info)\t Comptue non-ranked correlation-matrix, given chunkSize_cnt_i_intris=%u, at %s:%d\n", chunkSize_cnt_i_intris, __FILE__, __LINE__);
	    
  /*! --------*/
  j2 += VECTOR_FLOAT_ITER_SIZE; //! increment the intiation-count:

  //printf("FIXME: inlucde [”elow] whent eh memroy-error-issue is resovled, at %s:%d\n", __FILE__, __LINE__);

  if( (i + VECTOR_FLOAT_ITER_SIZE + chunkSize_cnt_i_intris) > ncols) {chunkSize_cnt_i_intris = ncols - (i + VECTOR_FLOAT_ITER_SIZE);}
  assert( (i + chunkSize_cnt_i_intris + VECTOR_FLOAT_ITER_SIZE) <= ncols); // ie, what we expect wrt. [”elow]

  /*! -------- */
  for (; j2 < chunkSize_cnt_i_intris; j2 += VECTOR_FLOAT_ITER_SIZE) {
    /*! Get the values: */
#if(TEMPLATE_distanceConfiguration_useWeights == 1) 	    //! Load the weigth-macros:
    const uint weight_index = i + j2; VECTOR_FLOAT_TYPE vec_weight = VECTOR_FLOAT_LOADU(&weight[/*index=*/weight_index]);
#endif
    VECTOR_FLOAT_TYPE term1  = VECTOR_FLOAT_LOAD(&rmul1[j2]); VECTOR_FLOAT_TYPE term2  = VECTOR_FLOAT_LOAD(&rmul2[j2]);
    //    VECTOR_FLOAT_TYPE term1  = VECTOR_FLOAT_LOADU(&rmul1[j2]); VECTOR_FLOAT_TYPE term2  = VECTOR_FLOAT_LOADU(&rmul2[j2]);

    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros( (i + j2 + VECTOR_FLOAT_ITER_SIZE) <= ncols); //! ie, validate that we are 'isndie' the memory-boundary (oekseth, 06. mar. 2017)
    //! Apply the mask-filters:
    VECTOR_FLOAT_TYPE vec_maskCount;  internalTemplateMacro__applyMasks(vec_maskCount);

    //! Call a generlized funciton to include the weights:
    internalTemplateMacro__includeWeights__eachTerm__SSE();
	      
    //! Compute the distance:
    VECTOR_FLOAT_TYPE mul; internalTemplateMacro__computeDistance__SSE(mul);

    //! Call a generlized funciton to include the weights wr.t the post-processing:
    internalTemplateMacro__includeWeights__forResult__SSE();

    //! start: update the distance-counts:
#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_distanceConfiguration_useWeights == 1) )//! then we are to update the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice: then we adjust the score through weights:
    vec_maskCount = VECTOR_FLOAT_MUL(vec_maskCount, vec_weight); //! ie, then muliply the weight with the mask-count, eg, as seen wrt. comptuation of Perasosn correlation-coeffeicnet in the "cluster.c" naive-software-impelemtantion
#endif
#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) )
    objLocalComplex_nonSSE.cnt_interesting_cells = VECTOR_FLOAT_storeAnd_horizontalSum(vec_maskCount); //! ie, intialise the temproal result-vector.
#elif( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) )
    objLocalComplex_sse.cnt_interesting_cells = VECTOR_FLOAT_ADD(objLocalComplex_sse.cnt_interesting_cells, vec_maskCount); //! ie, intialise the temproal result-vector.
#endif
    //! end: update the distance-counts:

    /*! Load the result-vector and add the values, and 'store' tehr result: */
#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) )
    /* printf("result_tmp[%u]=%f -->sum=%f, given input=(%f, %f) at %s:%d\n", j2, VECTOR_FLOAT_storeAnd_horizontalSum(vec_result),  */
    /* 	     VECTOR_FLOAT_storeAnd_horizontalSum(mul), VECTOR_FLOAT_storeAnd_horizontalSum(term1), VECTOR_FLOAT_storeAnd_horizontalSum(term2), __FILE__, __LINE__); */
    vec_result = VECTOR_FLOAT_ADD(mul, vec_result);
#elif(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) //! tehnw e are itnerested in the max-value
#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
    //printf("\t\t\t (cmp-val)\t %f VS %f, at %s:%d\n", result, result_tmp, __FILE__, __LINE__);
    vec_result = VECTOR_FLOAT_MIN(vec_result, mul);
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
    //printf("\t\t\t (cmp-val)\t %f VS %f, at %s:%d\n", result, result_tmp, __FILE__, __LINE__);
    vec_result = VECTOR_FLOAT_MAX(vec_result, mul);
    //result = macro_max(result, result_tmp);
#else
#warning "Investigate this issue, ie, are we to suem 'um of vlaues'?"
#endif //! endif(type-of-sum)
    
    /* printf("result_tmp[%u]=%f -->sum=%f, given input=(%f, %f) at %s:%d\n", j2, VECTOR_FLOAT_storeAnd_horizontalSum(vec_result),  */
    /* 	     VECTOR_FLOAT_storeAnd_horizontalSum(mul), VECTOR_FLOAT_storeAnd_horizontalSum(term1), VECTOR_FLOAT_storeAnd_horizontalSum(term2), __FILE__, __LINE__); */
#endif
  }

  /*! Combine teh vectors: */
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
  const t_float sum_tmp = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); //! ie, return
#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
  result = macro_min(result, sum_tmp);
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
  result = macro_max(result, sum_tmp);
#else
  result += sum_tmp;
#endif
#else //! then we udpate mulipel result-data-matrices:
#define __template_correlation_tile_innerMostColumn_mergeComplex__config__useSSE 1
#include "template_correlation_tile_innerMostColumn_mergeComplex.c" //! ie, then merge compelx result-objects
#endif

  //!
  //! Ivnestigate if we are to update the count of distances:
/* #if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /\*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*\/0)  ) //! then we are to update the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice: */
/*   objLocalComplex_SSE.cnt_interesting_cells = VECTOR_FLOAT_ADD(objLocalComplex_SSE.cnt_interesting_cells, vec_maskCount); //! ie, update the temproal result-vector. */
/* #elif( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /\*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*\/0)  ) */
#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) )
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
  objLocalComplex_nonSSE.cnt_interesting_cells += (uint)VECTOR_FLOAT_storeAnd_horizontalSum(vec_maskCount); //! ie, update the temproal result-vector wrt. the counts
#elif(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
  objLocalComplex_nonSSE.cnt_interesting_cells += (uint)VECTOR_FLOAT_storeAnd_horizontalSum(vec_maskCount); //! ie, update the temproal result-vector wrt. the counts
#endif
#endif
}
#endif //! wrt. "configure_SSE_useFor_mask_explicit"
