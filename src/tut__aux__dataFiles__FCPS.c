#ifndef __MiCo__useLocalVariablesIn__varName_useLocal
#define __MiV__exper nameOf_experiment
#define __MiV__reaLife_list mapOf_realLife
#define __MiV__reaLife_list_size mapOf_realLife_size
#endif

const char *__MiV__exper = "FCPS";  //! 
      //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_8.c":
      const uint config__kMeans__defValue__k__min = 2;
      const uint config__kMeans__defValue__k__max = 4;
      //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
      // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
      const char *config__nameOfDefaultVariable = "binomial_p005";
      assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
      //const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 

const uint mapOf_functionStrings_base_size = 10;
const char *mapOf_functionStrings_base[mapOf_functionStrings_base_size] = {
    "data/FCPS/01FCPSdata/Atom.lrn",
    "data/FCPS/01FCPSdata/EngyTime.lrn",
    "data/FCPS/01FCPSdata/Hepta.lrn",
    "data/FCPS/01FCPSdata/Target.lrn",
    //! ---- 
    //"data/FCPS/01FCPSdata/tmp.TwoDiamonds.lrn",
    "data/FCPS/01FCPSdata/WingNut.lrn",
    "data/FCPS/01FCPSdata/Chainlink.lrn",
    "data/FCPS/01FCPSdata/GolfBall.lrn",
    "data/FCPS/01FCPSdata/Lsun.lrn",
    //! ---- 
    "data/FCPS/01FCPSdata/Tetra.lrn",
    "data/FCPS/01FCPSdata/TwoDiamonds.lrn"
};

  //! --------------------------
  //!
  const uint __config__kMeans__defValue__k____cntIterations = 10;
  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint __MiV__reaLife_list_size = mapOf_functionStrings_base_size *  __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t __MiV__reaLife_list[__MiV__reaLife_list_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {      
    //! Budil the sample-string:
    const char *stringOf_sampleData_type = mapOf_functionStrings_base[base_id];
    assert(stringOf_sampleData_type);
    assert(strlen(stringOf_sampleData_type));
    //!
    //! ITerate through the different k-cluster-count-specifriciaotns:
    for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
      const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
      assert(current_pos < __MiV__reaLife_list_size);
      //!
      //! Add the object:
      __MiV__reaLife_list[current_pos].tag = stringOf_sampleData_type;
      __MiV__reaLife_list[current_pos].file_name = stringOf_sampleData_type;
      __MiV__reaLife_list[current_pos].fileRead_config = fileRead_config__syn;
      __MiV__reaLife_list[current_pos].inputData__isAnAdjcencyMatrix = false;
      __MiV__reaLife_list[current_pos].k_clusterCount = k_clusterCount;
      __MiV__reaLife_list[current_pos].mapOf_vertexClusterId = NULL;
      __MiV__reaLife_list[current_pos].mapOf_vertexClusterId_size = 0;
      __MiV__reaLife_list[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
      __MiV__reaLife_list[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      __MiV__reaLife_list[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      __MiV__reaLife_list[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
      //!
      current_pos++;
    }
  }

#undef __MiV__exper
#undef __MiV__reaLife_list
#undef __MiV__reaLife_list_size
