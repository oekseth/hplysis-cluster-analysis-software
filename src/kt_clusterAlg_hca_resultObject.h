#ifndef kt_clusterAlg_hca_resultObject_h
#define kt_clusterAlg_hca_resultObject_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_clusterAlg_hca__node.h"
#include "e_kt_clusterAlg_exportFormat.h" //! which is used to enumerate diffferent cluster-export-foramts (oekseth, 06. jan. 2016).
#include "kt_set_2dsparse.h"
#include "kt_set_1dsparse.h"

/**
   @file kt_clusterAlg_fixed_resultObject
   @brief a result-object for the hca clustering (oesketh, 06. nov. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
Node_t *allocate_1d_list_Node(const uint size, const Node_t default_value) ;
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
void free_1d_list_Node(Node_t **list);

/**
   @struct s_kt_clusterAlg_hca_resultObject
   @brief provide a structured access to the result-objects generated in our k-means clustering-procedure (oekseth, 06. nov. 2016).
 **/
typedef struct s_kt_clusterAlg_hca_resultObject {
  Node_t *mapOf_nodes; uint nrows;
} s_kt_clusterAlg_hca_resultObject_t;

//! Intiate the s_kt_clusterAlg_hca_resultObject_t object
s_kt_clusterAlg_hca_resultObject_t setToEmptyAndreturn__s_kt_clusterAlg_hca_resultObject_t();
//! Intiate the s_kt_clusterAlg_hca_resultObject_t object
void setTo_empty__s_kt_clusterAlg_hca_resultObject_t(s_kt_clusterAlg_hca_resultObject_t *self, const uint nrows);
//! De-allcoates the s_kt_clusterAlg_hca_resultObject_t object
void free__s_kt_clusterAlg_hca_resultObject_t(s_kt_clusterAlg_hca_resultObject_t *self);

/**
   @struct s_kt_clusterAlg_hca_resultObject_2d
   @brief a wrapper-object which translate a 1d-list of relationships into a dense 2d-list of relationshisp (oekseth, 06. feb. 2017)
 **/
typedef struct s_kt_clusterAlg_hca_resultObject_2d {  
  s_kt_set_2dsparse_t stack2d;
  uint offsetToRemove__internalToRealLife; //! ie, as 'itneral vertices' have navgative indxes.
  uint *mapOf_internalStartNodes;
  uint mapOf_internalStartNodes_size;
} s_kt_clusterAlg_hca_resultObject_2d_t;

//! @return an itniated version of the "s_kt_clusterAlg_hca_resultObject_2d_t" object (oeksethj, 06. feb. 2017).
static s_kt_clusterAlg_hca_resultObject_2d_t initEmpty__s_kt_clusterAlg_hca_resultObject_2d_t() {
  s_kt_clusterAlg_hca_resultObject_2d_t self;
  setTo_Empty__s_kt_set_2dsparse_t(&(self.stack2d));
  self.offsetToRemove__internalToRealLife = 0;
  self.mapOf_internalStartNodes = NULL;
  self.mapOf_internalStartNodes_size = 0;
  
  //! @return
  return self;
}

static void free__s_kt_clusterAlg_hca_resultObject_2d_t(s_kt_clusterAlg_hca_resultObject_2d_t *self) {
  assert(self);
  if(self->mapOf_internalStartNodes) {free_1d_list_uint(&(self->mapOf_internalStartNodes)); self->mapOf_internalStartNodes = NULL;}
  free_s_kt_set_2dsparse_t(&(self->stack2d));
}

/**
   @brief translates a 1d-list into a dense 2d-list of relationships (oekseth, 06. feb. 2017).
   @param <obj_hca> is the object with the HCA-result.
   @return a new-intiated object 
 **/
s_kt_clusterAlg_hca_resultObject_2d_t build_2dStruct__s_kt_clusterAlg_hca_resultObject_2d_t(const s_kt_clusterAlg_hca_resultObject_t *obj_hca);
//! @return a new-intalized stack of start-nodes wrt. teh self object (oekseth, 06. feb. 2017).
s_kt_set_1dsparse_t constructStackOf_startNodes__s_kt_clusterAlg_hca_resultObject_2d_t(const s_kt_clusterAlg_hca_resultObject_2d_t *self);

/**
   @brief merge the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void mergeResult__s_kt_clusterAlg_hca_resultObject_t(s_kt_clusterAlg_hca_resultObject_t *self, const s_kt_clusterAlg_hca_resultObject_t *clusterResults, const uint *keyMap_localToGlobal);


/**
   @brief construct an adjcency-matrix based on the Node_t arrOf_nodes table (oekseth, 06. nov. 2016).
   @param <self> the object which 'hold the data' to sue
   @param <matrix_result> an itnaited adjcency-matrix: if the matrix is first used in this call then we expec all cells to have been set to '0'; expected to be an djcency-matrix of diemsions "|nrows|x|nrows|".
   @param <mapOf_1d_distanceToRoot_forMetric__nodeCount> which we update to 'hold' the number of 'node-traversals' from teh root-node; simliar to "matrix_result" the objhect is expected to be pre-allocated to the nrwos size;
   @param <mapOf_1d_distanceToRoot_forMetric__linkScoreSum> which we update to 'hold' the 'sum of link-scores from the root-node' from teh root-node; simliar to "matrix_result" the objhect is expected to be pre-allocated to the nrwos size
   @param <nrows> which is expected to be equal or greater than the arrOf_nodes_size: if the valeus differet we expect the optional mapOf_localIdTo_globalId to be used.
   @param <mapOf_localIdTo_globalId> optional:  if (mapOf_localIdTo_globalId != NULL) we assume mapOf_localIdTo_globalId[node_id] = global_index, where global_index < nrows, and where we update matrix_result[global_index_head][global_index_tail] = simliarty.
   @param <isTo_addInternalNodesIn__mapAttributeSet> which fi set to "true" impleis taht we udpate the 'dsitance-structkrue' with knowledge of the iamginary ndoes (where the 'iamgiarny nodes' are then 'indxed to a psotion after' the itnernla memory
   @return true upon success.
   @remarks the "self" object is expected to be the result of a call to an HCA algorithm. The object is (among others) expected to hold arrOf_nodes (ie, the tree of nodes, a tree which may be constructed from a call to our "treecluster__extensiveInputParams(..)" function) and the arrOf_nodes_size (ie, the number of nodes in arrOf_nodes).
 **/
//->mapOf_nodes, clusterResults->nrows
//const Node_t *arrOf_nodes, const uint arrOf_nodes_size
bool constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca(const s_kt_clusterAlg_hca_resultObject_t *self, t_float **matrix_result, t_float  *mapOf_1d_distanceToRoot_forMetric__nodeCount, t_float *mapOf_1d_distanceToRoot_forMetric__linkScoreSum, const uint nrows, const uint *mapOf_localIdTo_globalId, const bool isTo_addInternalNodesIn__mapAttributeSet);

/**
   @brief Print the resutls of the clsutering in a 'human-redable-format' to stream_out (oesketh, 06. jan. 2017).
   @param <mapOf_names>  which if set is used to 'name' the indeanl indices.
   @param <stream_out> sit he file-desctiopror to be used in exprot.
   @param <seperator_cols>  is the seperator to be used to 'dsintish each column, eg, "\t"
   @param <seperator_newLine> is the seperator between each line, eg "\n"
   @param <enum_id> identifies the data-sets which are to be epxortedb
   @return true upon scucess.
**/
bool printHumanReadableResult__extensive__s_kt_clusterAlg_hca_resultObject_t(const s_kt_clusterAlg_hca_resultObject_t *self, char **mapOf_names, FILE *stream_out, const char *seperator_cols, const char *seperator_newLine, const e_kt_clusterAlg_exportFormat_t enum_id);


//! Print the resutls of the clsutering in a 'human-redable-format' to STDOUt.
void printHumanReadableResult__toStdout__s_kt_clusterAlg_hca_resultObject_t(const s_kt_clusterAlg_hca_resultObject_t *self, char **mapOf_names);

#endif //! EOF
