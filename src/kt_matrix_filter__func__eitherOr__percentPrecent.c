
//!
//! Identify the thresholds:
t_float min_cnt_rows = self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[0];
//min_cnt_rows *= self->nrows;
t_float max_cnt_rows = self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[0];
if(max_cnt_rows != T_FLOAT_MIN_ABS) {
  assert(max_cnt_rows >= min_cnt_rows);
  max_cnt_rows *= 0.01 * self->nrows;
 }
//! ------------
t_float min_cnt_columns = self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[0];
//assert(min_cnt_columns != UINT_MAX);
//min_cnt_columns *= self->ncols;
t_float max_cnt_columns = self->thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[0];
if(max_cnt_columns != T_FLOAT_MIN_ABS) {
  assert(max_cnt_columns >= min_cnt_columns);
  max_cnt_columns *= 0.01 * self->ncols;
 }
//! -----------------
//!
//! Apply the filters:
//! Note: we choose to adjsut wrt. the column-size and row-size BEFORE evlauation of the distjoint-fitlers, ie, as the row--column size may implify that (for some thresholds) all of the valeus are ignroed, ie, a case/ciatuions which we asusme is Not indeted by the caller.
const bool isTo_evaluateFor_rows = (min_cnt_rows < max_cnt_rows);
const bool isTo_evaluateFor_cols = (min_cnt_columns < max_cnt_columns);
if( (isTo_evaluateFor_rows || isTo_evaluateFor_cols) && (min_cnt_rows <= max_cnt_rows) && (min_cnt_columns <= max_cnt_columns) ) {
  const uint default_value_uint = 0;
  uint *mapOf_count_rows = NULL; uint *mapOf_count_cols = NULL;
  if(isTo_evaluateFor_rows) {mapOf_count_rows = allocate_1d_list_uint(self->nrows, default_value_uint);}
  if(isTo_evaluateFor_cols) {mapOf_count_cols = allocate_1d_list_uint(self->ncols, default_value_uint);}

  //! Update the count:
  // uint cntOf_interest_cols = 0;
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    if(self->mapOf_interesting_rows[row_id]) {
      const t_float *__restrict__ row = self->matrix[row_id];
      //! ---
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	if(isOf_interest(row[col_id])) {
	  if(isTo_evaluateFor_rows) {mapOf_count_rows[row_id]++;}
	  if(isTo_evaluateFor_cols) {mapOf_count_cols[col_id]++;}
	}
      }
    }
  }
  if(isTo_evaluateFor_rows) {	
    //! 
    //! Apply the thresholds:
    for(uint row_id = 0; row_id < self->nrows; row_id++)  {
      // printf("row[%u].cnt=%u is inside=[%f, %f], at %s:%d\n", row_id, mapOf_count_rows[row_id], min_cnt_rows, max_cnt_rows, __FILE__, __LINE__);
      if( (mapOf_count_rows[row_id] <= min_cnt_rows)  || (mapOf_count_rows[row_id] >= max_cnt_rows) ) {
	if(self->mapOf_interesting_rows[row_id]) {
	  self->mapOf_interesting_rows[row_id] = false;
	  for(uint col_id = 0; col_id < self->ncols; col_id++) {
	    self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	  }
	}
      }
    }
  }
  if(isTo_evaluateFor_cols) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      // printf("col[%u].cnt=%u is inside=[%f, %f], at %s:%d\n", col_id, mapOf_count_cols[col_id], min_cnt_columns, max_cnt_columns, __FILE__, __LINE__);
      if(self->mapOf_interesting_cols[col_id]) {
	self->mapOf_interesting_cols[col_id] = false;
	if( (mapOf_count_cols[col_id] <= min_cnt_columns)  || (mapOf_count_cols[col_id] >= max_cnt_columns) ) {
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {
	    self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	  }
	}
      }
    }
  }

  //! De-allcoate locally reserved memory:
  if(isTo_evaluateFor_rows) {free_1d_list_uint(&mapOf_count_rows);}
  if(isTo_evaluateFor_cols) {free_1d_list_uint(&mapOf_count_cols);}
 }
