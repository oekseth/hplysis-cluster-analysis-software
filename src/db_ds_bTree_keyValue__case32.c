

static uint __MiF__search__slowEvaluateAll(const __Mi__localType *self, const __Mi__localType__wrapper key, __Mi__localType__wrapper *scalar_result, s_kt_set_1dsparse_t *result_set) {
#include "db_ds_bTree__stubSearch__evaluateAll.c"
}/*End of search()*/

//! De-allocate the 'allcoated children' in each of the "pointes" references (oekseth, mar. 2017).
static void __MiF__free(__Mi__localType *self) {
  //! The call:
#include "db_ds_bTree__free.c"
}

static int __MiF__search(const __Mi__localType *root, const __Mi__localType__wrapper key, __Mi__localType__wrapper *scalar_result, s_kt_set_1dsparse_t *result_set) {
#include "db_ds_bTree__stubSearch.c"
}/*End of search()*/


static e_db_cmpResult_t __MiF__search__recursive(__Mi__localType *ptr, const __Mi__localType__wrapper key, __Mi__localType__wrapper *upKey, __Mi__localType **newnode, s_kt_set_1dsparse_t *result_set) {
#include "db_ds_bTree__stubrecursiveIns.c"
}



static void __MiF__insert(__Mi__localType **root, const __Mi__localType__wrapper key, s_kt_set_1dsparse_t *result_set) {
#include "db_ds_bTree__stubAlloc.c"
}/*End of insert()*/



#undef __MiF__search__recursive
#undef __Mi__localType
#undef __M__constant
#undef __MiF__search__slowEvaluateAll
#undef __MiF__free
//#undef __MiF__free_mem
#undef __MiF__search
#undef __MiF__insert
