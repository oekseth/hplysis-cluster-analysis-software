    if( 
       (head_1 < self->obj_traverse.cnt_vertices)
       && (head_2 < self->obj_traverse.cnt_vertices)
	) {
      // --- 
#define t_type uint
#define t_type_max UINT_MAX
#define t_type_min_abs  0
      //! Fetch the data-containers: 
      const uint *__restrict__ row_1 = self->obj_traverse.map_result[head_1].list;
      const uint *__restrict__ row_2 = self->obj_traverse.map_result[head_2].list;
      const uint *__restrict__ rowOf_scores_1 = self->obj_traverse.map_result_distance[head_1].list;
      const uint *__restrict__ rowOf_scores_2 = self->obj_traverse.map_result_distance[head_2].list;
      const uint row_size   = self->obj_traverse.map_result[head_1].list_size;
      const uint row_size_2 = self->obj_traverse.map_result[head_2].list_size;
#if(1 == 2)
      const uint alt_row_size   = self->obj_traverse.map_result_distance[head_1].list_size;
      assert(alt_row_size == row_size);
      const uint alt_row_size_2 = self->obj_traverse.map_result_distance[head_2].list_size;
      assert(alt_row_size_2 == row_size_2);
      /* assert(row_size > 0); */
      /* assert(row_size_2 > 0); */
#endif
      if(row_size && row_size_2) { //! then both hav esocres:
	//!
	//! Apply logics:
#include "kt_semanticSim__stub__cmpWrapper.c"
	//!
	// --- 
	ret_val_local  = retVal_result; //! ie, hte reuslt comptued in [ªbove].
      } /* else { */
      /* 	obj_pairs->list[pair_id].score = T_FLOAT_MAX; */
      /* } */
      //      const uint retVal_result;
    }
