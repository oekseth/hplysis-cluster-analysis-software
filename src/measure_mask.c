#include "measure_mask.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
// #include "correlation_macros__distanceMeasures.h"

#include "mask_base.h"
#include "kt_mathMacros.h"
#include "matrix_transpose.h" //! which we evlauate 'while' evlauating the 'default' transpsoe-operaitons.
#include "mask_api.h"
#include "correlation_base.h"
#include "correlationType_delta.h"
#include "correlation_inCategory_delta.h"
#include "graphAlgorithms_aux.h"
#include "kt_clusterAlg_fixed.h"

static t_float prev_result_ofTimeMEasurement = 0;


static t_float prev_timeOf_measurement = 0;


//! Ends the time measurement for the array:
static void local__assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const class measure *measure_linear, const uint isTo_use_continousSTripsOf_memory = UINT_MAX, const uint isTo_use_transpose = UINT_MAX, const uint isTo_sumDifferentArrays = UINT_MAX, const uint CLS = UINT_MAX ) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  const char *stringOf_isTo_use_continousSTripsOf_memory = "";
  if(isTo_use_continousSTripsOf_memory == 1) {stringOf_isTo_use_continousSTripsOf_memory = ", memoryAllocatedIdeal=yes";}
  else if(isTo_use_continousSTripsOf_memory == 0) {stringOf_isTo_use_continousSTripsOf_memory = ", memoryAllocatedIdeal=no";}
  //! Get knowledge of the memory-access-pattern:
  const char *stringOf_isTo_use_transpose = "";
  if(isTo_use_transpose == 1) {stringOf_isTo_use_transpose = ", memoryAccessedTransposely=yes";}
  else if(isTo_use_transpose == 0) {stringOf_isTo_use_transpose = ", memoryAccessedTransposely=no";}
  //! Get knowledge of the memory-access-pattern:
  const char *stringOf_isTo_sumDifferentArrays = "";
  if(isTo_sumDifferentArrays == 1) {stringOf_isTo_sumDifferentArrays = ", compareTwoMatrices=yes";}
  else if(isTo_sumDifferentArrays == 0) {stringOf_isTo_sumDifferentArrays = ", compareTwoMatrices=no";}
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s%s%s%s, and CLS=%u)", size_of_array, stringOf_measureText, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    __assertClass_generateResultsOf_timeMeasurements(string, size_of_array, size_of_array); //prev_timeOf_measurement);

    delete [] string; string = NULL; 
  } 
}


static void memoryLookUp_clusterMedian() {
  const uint arrOf_chunkSizes_size = 3; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*kilo, 20*kilo, 30*kilo};
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    uint list_size = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
    //! Construct a sample-list, where we have 'buckets' of equal values:
    float **distmatrix = NULL; char **mask = NULL;
    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
      const bool isTo_use_continousSTripsOf_memory = true;
      // FIXME[article]: update our artilce wrt. [ªbove] observation ... for cnt-buckets=[10, 20, 40, 80, 180] there is a perofrmance-increase=[3.44, 5.41, 9.6, 19.5, 37.63] through our approach: in the latter we tested for list_size=10,000, and where cnt_buckets=180 resulted in an exeuction-time of 128.2800 seconds.
      const uint size_of_array = list_size;
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;
      assert(factor_rows*2 < size_of_array); //! ie, to avoid 'size so f 1'
      maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  

      uint *clusterid = new uint[max(nrows, ncols)];

      const uint arrOf_cnt_buckets_size = 10; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40,
												      80, 160, 320, 740, 1000};
      for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {
	const uint cnt_buckets = arrOf_cnt_buckets[cnt_buckets_index];
      //for(uint cnt_buckets = 1; cnt_buckets < 100; cnt_buckets++) {
	for(uint i = 0; i < max(nrows, ncols); i++) {clusterid[i] = rand() % cnt_buckets;}
	printf("--------------- cnt_buckets=%u\n", cnt_buckets);
	
	{ //! Test a 2d-traversaion:
	  for(uint i = 0; i < nrows; i++) {clusterid[i] = rand() % cnt_buckets;}
	  { //! Non-intrisitntcs:
	    start_time_measurement(); 
	    //! Iterate:
	    float sum = 0;
	    for(uint k = 0; k < nrows; k++) {
	      const uint i = clusterid[k];
	      assert(i <= ncols);
	      for(uint j = 0; j < ncols; j++) {
		sum += distmatrix[i][j];
	      }
	    }
	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "memoryAccess::traverse-2d-[%u][%u] w/cnt-buckets=%u, and memory-allocation-%s", nrows, ncols, cnt_buckets, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	    delete [] string; string = NULL;
	  }
	}
	{ //! Test a 3d-traversaion:
	  start_time_measurement(); 
	  //! Iterate:
	  float sum = 0;
	  for(uint i = 0; i < cnt_buckets; i++) {
	    for(uint j = 0; j < ncols; j++) {
	      for(uint k = 0; k < nrows; k++) {
		if(i == clusterid[k]) {
		  assert(i <= ncols);
		  sum += distmatrix[i][j];
		}
	      }
	    }
	  }
	  //! Then generate the results:
	  char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	  sprintf(string, "memoryAccess::traverse-3d-[%u][%u] w/cnt-buckets=%u, and memory-allocation-%s", nrows, ncols, cnt_buckets, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	  __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	  delete [] string; string = NULL;
	}
	//! ---------------------------------------------------------
	const uint max_cnt = max(nrows, ncols);
	float **arrOf_valuesFor_clusterId_global = allocate_2d_list_float(cnt_buckets, max_cnt, 0);
	float **distmatrix_2 = NULL; char **mask_2 = NULL;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix_2, &mask_2, isTo_use_continousSTripsOf_memory);	  
	float **mask_tmp = allocate_2d_list_float(max(nrows, ncols), max(nrows, ncols), 0);
	{ //! Test the exeuction-time of the different "getclustermedians(..)" implemetnaiton-stratgeis:
	  {
	    uint transpose = 0;
	    start_time_measurement(); 
	    const bool isTo_copmuteResults_optimal = false; //! ie, use the 'naive' strategy:
	    const bool mayUse_zeroAs_emptySign_inComputations = true; 	    const bool needTo_useMask_evaluation = true;
	    getclustermedians__extensiveInputParams(cnt_buckets, nrows, ncols, distmatrix, mask, clusterid, distmatrix_2, mask_2, mask_tmp, transpose, isTo_copmuteResults_optimal, arrOf_valuesFor_clusterId_global, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "memoryAccess::traverse-3d-[%u][%u] w/cnt-buckets=%u, test-real=true, and memory-allocation-%s", nrows, ncols, cnt_buckets, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	    delete [] string; string = NULL;
	  }
	  {
	    uint transpose = 1;
	    start_time_measurement(); 
	    const bool isTo_copmuteResults_optimal = false; //! ie, use the 'naive' strategy:
	    const bool mayUse_zeroAs_emptySign_inComputations = true; 	    const bool needTo_useMask_evaluation = true;
	    getclustermedians__extensiveInputParams(cnt_buckets, nrows, ncols, distmatrix, mask, clusterid, distmatrix_2, mask_2, mask_tmp, transpose, isTo_copmuteResults_optimal, arrOf_valuesFor_clusterId_global, isTo_copmuteResults_optimal, mayUse_zeroAs_emptySign_inComputations);
	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "memoryAccess::traverse-3d-[%u][%u] w/cnt-buckets=%u, test-real=true, transpose=true, and memory-allocation-%s", nrows, ncols, cnt_buckets, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	    delete [] string; string = NULL;
	  }
	  {
	    uint transpose = 0;
	    start_time_measurement(); 
	    const bool isTo_copmuteResults_optimal = true; //! ie, use the 'naive' strategy:
	    const bool mayUse_zeroAs_emptySign_inComputations = true; 	    const bool needTo_useMask_evaluation = true;
	    getclustermedians__extensiveInputParams(cnt_buckets, nrows, ncols, distmatrix, mask, clusterid, distmatrix_2, mask_2, mask_tmp, transpose, isTo_copmuteResults_optimal, arrOf_valuesFor_clusterId_global, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "memoryAccess::traverse-3d-[%u][%u] optimal ...  w/cnt-buckets=%u, test-real=true, and memory-allocation-%s", nrows, ncols, cnt_buckets, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	    delete [] string; string = NULL;
	  }
	  {
	    uint transpose = 1;
	    start_time_measurement(); 
	    const bool isTo_copmuteResults_optimal = true; //! ie, use the 'naive' strategy:
	    const bool mayUse_zeroAs_emptySign_inComputations = true; 	    const bool needTo_useMask_evaluation = true;
	    getclustermedians__extensiveInputParams(cnt_buckets, nrows, ncols, distmatrix, mask, clusterid, distmatrix_2, mask_2, mask_tmp, transpose, isTo_copmuteResults_optimal, arrOf_valuesFor_clusterId_global, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "memoryAccess::traverse-3d-[%u][%u] optimal ... w/cnt-buckets=%u, test-real=true, transpose=true, and memory-allocation-%s", nrows, ncols, cnt_buckets, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	    delete [] string; string = NULL;
	  }
	}
	//! De-allocate lcoally reserved memory:
	free_2d_list_float(&arrOf_valuesFor_clusterId_global, cnt_buckets_index);
	free_2d_list_float(&mask_tmp, max(nrows, ncols));
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix_2, mask_2, isTo_use_continousSTripsOf_memory);
      }
      //! De-allocate lcoally reserved memory:
      maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);
      delete [] clusterid;	
    }
  }
}



//! ***********************************************************************************************************
//! ***********************************************************************************************************
//! ***********************************************************************************************************

//! Define a sampel-structure to use in [below] testing:
typedef class list_wrapper {
public:
  float *list;
  uint n;
  //! A function which 'emulates' a list[index] direct acces.
  float get_value(const uint index) const {return list[index];}
  //! A function which 'emulates' data-access-lsits which 'we expcts'.
  float get_value_w_rangeTest(const uint index) const {if(index < n) {return list[index];} else return T_FLOAT_MAX;}
} list_wrapper_t;
//! A c-style list-access, 
float get_value(const list_wrapper_t *obj, const uint index) {return obj->list[index];}
//! A c-style list-access with index-validaiton
float get_value_w_rangeTest(const list_wrapper_t *obj, const uint index) {if(index < obj->n) {return obj->list[index];} else return T_FLOAT_MAX;}


static void evaluate_listAccess_and_assicated_structureOVerehead() {
  const uint arrOf_chunkSizes_size = 5; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {kilo, 10*kilo, 100*kilo, kilo*kilo, 100*kilo*kilo};
  { //! Test the overhead of using implict memory-accesss for fixed lists, eg, wrt. memory-iteration:      
    for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
      const uint list_size = arrOf_chunkSizes[chunk_index];

      printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
	  
      //! new test: ------------------------------------------------------------------------------------------
      { //! Test the overhead of template-lists:	    
	//! Note: in [”elow] expereiemtnal setup we investigate a data-set of [list_size][list_size]

	//! Investigate if the artimetic operations may 'hide' the cost of memory-allcoation, ie, through the machine-hardwares implicat 'underlying' parallisation:	
	const uint arrOf_innerChunkSizes_size = 5; const uint arrOf_innerChunkSizes[arrOf_innerChunkSizes_size] = {0, kilo, 10*kilo, 100*kilo, kilo*kilo};
	for(uint inner_chunkSize_index = 0; inner_chunkSize_index < arrOf_innerChunkSizes_size; inner_chunkSize_index++) {	  
	  const uint list_size_artimetics = arrOf_innerChunkSizes[inner_chunkSize_index];
	  { //! Test the 'simplies' access-pattersn:
	    //! --------------
	    //! Define the cases which we evaluate:
	    enum {
	      access_direct, access_wrapper_inside_direct, access_wrapper_outside_direct, 
	      access_wrapper_inside_validation, access_wrapper_outside_validation, 
	      access_undef
	    };
	    //! Allocate:
	    //! Note: we use the same memorya-llcoations in order to simplify comparison the different acces-patterns:
	    float *list = new float[list_size];
	    list_wrapper obj; obj.list = list; obj.n = list_size;
	    for(uint access_id = 0; access_id < access_undef; access_id++) {
	      const char *idOf_experiement = NULL;
	      //! Start the time-measurement:
	      start_time_measurement(); 

	      if(access_id == access_direct) {
		idOf_experiement = macro_getVarName(access_direct);
		//! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton:
		float sum = 0;
		for(uint k = 0; k < list_size; k++) {
		  sum += list[k];
		}
	      } else if(access_id == access_wrapper_inside_direct) {
		idOf_experiement = macro_getVarName(access_wrapper_inside_direct);
		//! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton:
		float sum = 0;
		for(uint k = 0; k < list_size; k++) {
		  sum += obj.get_value(k);
		}
	      } else if(access_id == access_wrapper_outside_direct) {
		idOf_experiement = macro_getVarName(access_wrapper_outside_direct);
		//! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton:
		float sum = 0;
		for(uint k = 0; k < list_size; k++) {
		  sum += get_value(&obj, k);
		}
	      } else if(access_id == access_wrapper_inside_validation) {
		idOf_experiement = macro_getVarName(access_wrapper_inside_validation);
		//! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton:
		float sum = 0;
		for(uint k = 0; k < list_size; k++) {
		  sum += obj.get_value_w_rangeTest(k);
		}
	      } else if(access_id == access_wrapper_outside_validation) {
		idOf_experiement = macro_getVarName(access_wrapper_outside_validation);
		//! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton:
		float sum = 0;
		for(uint k = 0; k < list_size; k++) {
		  sum += get_value_w_rangeTest(&obj, k);
		}
	      }

	      //! Then generate the results:
	      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	      sprintf(string, "memoryAccess::structure-overhead-2d-%u w/artimetic-cnt=%u and allocation-type=%s", list_size, list_size, idOf_experiement); 
	      __assertClass_generateResultsOf_timeMeasurements(string, list_size, list_size); //prev_timeOf_measurement);
	      delete [] string; string = NULL;
	    }	      
	    delete [] list;
	  }
	  /* { //! Test for "list_generic" */
	  /*   list_generic<type_float> arrOf_tmp; */
	  /*   for(uint k = 0; k < list_size; k++) { arrOf_tmp.push(k);} */
	  /*   //! Start the time-measurement: */
	  /*   start_time_measurement();  */
	  /*   const char *idOf_experiement = "list_generic_float"; */
	  /*   //! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton: */
	  /*   float sum = 0; */
	  /*   for(uint k = 0; k < list_size; k++) { */
	  /*     sum += arrOf_tmp.template_get_value(k).get_key(); */
	  /*   } */
		
	  /*   //! Then generate the results: */
	  /*   char *string = new char[1000]; assert(string); memset(string, '\0', 1000); */
	  /*   sprintf(string, "memoryAccess::structure-overhead-2d-%u w/artimetic-cnt=%u and allocation-type=%s", list_size, list_size, idOf_experiement);  */
	  /*   __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement); */
	  /*   delete [] string; string = NULL; */
	  /*   arrOf_tmp.free_memory(); */
	  /* }	       */
	}
	/* { //! Test for an array of type_float, ie, the use of 'implcit declearationi-classes' to access the data, ie, test the overhead of 'class-wrappers': */
	/*   type_float *list = new type_float[list_size]; */
	/*   //! Start the time-measurement: */
	/*   start_time_measurement();  */
	/*   const char *idOf_experiement = "type_float-implicit-variable-access"; */
	/*   //! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton: */
	/*   float sum = 0; */
	/*   for(uint k = 0; k < list_size; k++) { */
	/*     sum += list[k].get_key(); */
	/*   } */
		
	/*   //! Then generate the results: */
	/*   char *string = new char[1000]; assert(string); memset(string, '\0', 1000); */
	/*   sprintf(string, "memoryAccess::structure-overhead-2d-%u w/artimetic-cnt=%u and allocation-type=%s", list_size, list_size, idOf_experiement);  */
	/*   __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement); */
	/*   delete [] string; string = NULL; */
	/*   delete [] list; */
	/* }	       */
      }
    }
  }
}



/* #include "bm_vertex.h" */
/* #include "list_for_bm.h" */
/* static void evaluate_memoryAccessOrder_generalized() { */
/*   // FIXME: mmake use of this funciton. */
/*   const uint kilo = 1000;  */
/*   const uint arrOf_matrixSizes_size = 1;   const uint arrOf_matrixSizes[arrOf_matrixSizes_size] = { */
/*     10000, */
/*   }; */
/*   const bool measure_time = true; */
/*   const bool isTo_testFixedOverhead = false; const bool isTo_test_randomAccess = false;  const bool isTo_compareWith_idea_accessPAttern = false; */
/*   { //! An intial set of comparisons: goal is to identify the 'space' between ideal/optimal and 'non-ideal' memory-access-patterns: */
/*     const uint size_of_array = 1000;  */

/*     for(uint i = 0; i < arrOf_matrixSizes_size; i++) { */
/*       const uint size_of_array = arrOf_matrixSizes[i]; */
/*       printf("# Array-size: %u\n", size_of_array); */
/*       const uint numberOfTimes_dataIs_accessed = size_of_array; */
/*       //! ----------------------------------------------------------- */
/*       class measure measure_linear = measure(); */
/*       bm_vertex<4> temp; list_for_bm::benchmark_arithmetic_operation_only(temp, size_of_array, numberOfTimes_dataIs_accessed, measure_time, &measure_linear); */
/*       list_for_bm::benchmark_random_accessed_list(temp, size_of_array, UINT_MAX, numberOfTimes_dataIs_accessed, measure_time); */
/*       //! ----------------------------------------------------------- */
/*       { */
/* 	bm_vertex<8> temp; list_for_bm::benchmark_arithmetic_operation_only(temp, size_of_array, numberOfTimes_dataIs_accessed, measure_time); */
/* 	list_for_bm::benchmark_random_accessed_list(temp, size_of_array, UINT_MAX, numberOfTimes_dataIs_accessed, measure_time); */
/*       } */
/*       //! ----------------------------------------------------------- */
/*       { */
/* 	bm_vertex<12> temp; list_for_bm::benchmark_arithmetic_operation_only(temp, size_of_array, numberOfTimes_dataIs_accessed, measure_time);         */
/* 	list_for_bm::benchmark_random_accessed_list(temp, size_of_array, UINT_MAX, numberOfTimes_dataIs_accessed, measure_time);         */
/*       } */
/*       //! ----------------------------------------------------------- */
/*       { */
/* 	bm_vertex<32768> temp; list_for_bm::benchmark_arithmetic_operation_only(temp, size_of_array, numberOfTimes_dataIs_accessed, measure_time); */
/* 	list_for_bm::benchmark_random_accessed_list(temp, size_of_array, UINT_MAX, numberOfTimes_dataIs_accessed, measure_time); */
/*       } */
/*       //! ----------------------------------------------------------- */
/*       measure_linear.free_memory(); */
/*     } */
/*   } */
/* } */

//! Test/evaluate the time-difference between different memory-access-patterns:
//! Note: compare the importance of 'non-transposed' memory-access, eg, as seen in the "hq(..)" funciton in the "mime.c" software-library.
static void evaluate_memoryAccessOrder_capture_2d_matrixAccess()  { 
  // FIXME: mmake use of this funciton.
    const uint kilo=1000; const uint arrOf_matrixColumnSizes_size = 8+4+4; const uint arrOf_matrixColumnSizes[arrOf_matrixColumnSizes_size] = {64,   800, 1200,2400, 
																	     3600, 4800, 6000, 7000,
																	     8000,    9000,    10*kilo, 11*kilo, 
																	       
																	     12*kilo, 13*kilo, 14*kilo, 15*kilo};
    for(uint i = 0; i < arrOf_matrixColumnSizes_size; i++) {
      const uint size_of_array = arrOf_matrixColumnSizes[i];
      //const uint size_of_array = 20000;
      {  //! The time of linear access in a linear list of items:
	//float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	start_time_measurement();	    
	//float *list = new float[size_of_array*size_of_array];
	float sum = 0;
	uint counter = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  sum += i;
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:artimetic", size_of_array, prev_timeOf_measurement);
	//delete [] list;
      }
      {  //! The time of linear access in a linear list of items:
	//float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	float *list = new float[size_of_array*size_of_array];
	start_time_measurement();	    
	float sum = 0;
	uint counter = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  sum += list[counter];
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:idea-same-memory-chunk", size_of_array, prev_timeOf_measurement);
	delete [] list;
      }
      {  //! The time of linear access in a linear list of items:
	const bool isTo_use_continousSTripsOf_memory = true;
	//float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	start_time_measurement();	    
	float *list = new float[size_of_array*size_of_array];
	float sum = 0;
	uint counter = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  sum += list[counter++];
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt1-sequential", size_of_array, prev_timeOf_measurement);
	delete [] list;
      }
      {  //! The time of linear access in a linear list of items, where we use 'memory-jumps' similar to transposed access in 'continuous allcoated' memory-lists:
	const bool isTo_use_continousSTripsOf_memory = true;
	//float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	float *list = new float[size_of_array*size_of_array];
	start_time_measurement();	    
	float sum = 0;
	uint counter = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  sum += list[counter]; counter += size_of_array; //! ie, to test the effect of 'memory-jumps'.
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt1-jump", size_of_array, prev_timeOf_measurement);
	delete [] list;
      }
      {  //! The time of linear access in a linear list of items:
	const bool isTo_use_continousSTripsOf_memory = true;
	float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	start_time_measurement();	    
	float sum = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  //sum += 1;
	  sum += distmatrix[0][i];
	}     
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
	if(true) {printf("------------------------------, at %s:%d\n", __FILE__, __LINE__);}
      }
      {  //! The time of linear access in a linear list of items, where we use 'memory-jumps' similar to transposed access in 'continuous allcoated' memory-lists:
	const bool isTo_use_continousSTripsOf_memory = true;
	float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	start_time_measurement();	    
	float sum = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  sum += distmatrix[i][size_of_array/2];
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2-jump", size_of_array, prev_timeOf_measurement);
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
      }
      {  //! The time of linear access in a non-linear list of items:
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	const bool isTo_use_continousSTripsOf_memory = false; 	float **distmatrix = NULL; char **mask = NULL;
	maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	start_time_measurement();	    
	float sum = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  sum += distmatrix[i][size_of_array/2];
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:old-jump", size_of_array, prev_timeOf_measurement);
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
      }
      {  //! The time of linear access in a non-linear list of items:
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	const bool isTo_use_continousSTripsOf_memory = true; 	float **distmatrix = NULL; char **mask = NULL;
	maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float *list = new float[size_of_array*size_of_array]; uint coutner = 0;
	start_time_measurement();	    
	float sum = 0;
	const uint size_of_array_outer = 8000;
	for(uint i = 0; i < size_of_array; i++) {
	  for(uint k = 0; k < size_of_array_outer; k++) {
	    //sum += 1;
	    //sum += list[0];
	    //sum += list[coutner++];
	    //sum += list[i*size_of_array + k];
	    // sum += list[k*size_of_array + i];
	    //sum += distmatrix[0][0];
	    //sum += distmatrix[0][k];
	    //sum += distmatrix[i][k];
	    sum += distmatrix[k][i]; 
	    //sum += distmatrix[i][k];
	  }
	}
	__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:old-jump", size_of_array, prev_timeOf_measurement);
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
	delete [] list;
      }
    }
    // { //! A rectangular matrix:
    //   const uint size_of_array = 4000000;
    //   const uint size_y = size_of_array; const uint size_x = 50;
    //   float **distmatrix = NULL; char **mask = NULL;
    //   const bool isTo_use_continousSTripsOf_memory = true;
    //   maskAllocate__makedatamask(/*nrows=*/size_y, /*ncols=*/size_x, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
    //   start_time_measurement();	    
    //   float sum = 0;
    //   for(uint x = 0; x < size_x; x++) {
    // 	for(uint y = 0; y < size_y; y++) {
    // 	  // for(uint x = 0; x < size_x; x++) {
    // 	  // sum += 1; //! 0.69s
    // 	  //sum += list[0][0];
    // 	  //sum += list[0][coutner++];
    // 	  //sum += distmatrix[0][y*size_x + x];
    // 	  //sum += distmatrix[0][x*size_y + y]; //! 0.77s
    // 	  //sum += distmatrix[0][0];
    // 	  //sum += distmatrix[0][x];
    // 	  //sum += distmatrix[y][x]; //! 4.33s, ie, slower: approx. 5.6x ... 6.2x
    // 	  // sum += distmatrix[x][y]; //! 0.75s
    // 	}

    //   }
    //   __assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:old-jump", size_of_array, prev_timeOf_measurement);
    //   maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
    // }
}


//! For large lists indviestigate if a sorting of "index1" and "index2" may reduce the memory-cache-misss-rate
static void evaluate_memoryAccessOrder_effectOf_sortingToReduceMemoryCacheMisses() {
  //! Note: to sort a list has higher cost than the assicated costs assicated to random memory-accesses:

  // FIXME: mmake use of this funciton.
  
  // FIXME: update [below] to cover/describe different amtrix-topologies and different lsits-sizes ... using our "Spearman(..)" as template.
  const uint size_of_array = 20000;     const uint n_clusters_1 = 10000;
  assert(n_clusters_1 < size_of_array);
  uint index1[n_clusters_1]; for(uint i = 0; i < n_clusters_1; i++) {
    index1[i] = (uint)(rand() % size_of_array) ;
    //index1[i] = n_clusters_1-1;
  }
  const bool isTo_use_continousSTripsOf_memory = true;
  float **distmatrix = NULL; char **mask = NULL;
  //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
  maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  


  //! Start measurement:
  start_time_measurement();	    
  /* if(false) { */
  /*   list_uint tmp; for(uint i = 0; i < n_clusters_1; i++) {tmp.push(i);} tmp.apply_merge_sort(/\*remove-overlaps=*\/false); */
  /*   for(uint i = 0; i < n_clusters_1; i++) {index1[i] = tmp[i].get_key();} //! ie, update. */
  /*   tmp.free_memory(); */
  /* } */


  float sum = 0;
  for(uint j = 0; j < size_of_array; j++) {
    uint count = 0;
    for(uint k = 0; k < n_clusters_1; k++) {
      const uint i = index1[k];
      assert(i < size_of_array);
      sum += distmatrix[i][j];	
    }
  }
  __assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2-jump", size_of_array, prev_timeOf_measurement);
  maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
}


//! Identify/evalaute the time-overhead assicated to "isTo_use_continousSTripsOf_memory": difference = 1.4x, given 2-calls="tick=1.1700s" VS linear="tick=1.6900s"
static void evaluate_costOf_memoryAllocations()  { 
  // FIXME: mmake use of this funciton.
  // FIXME: update this function with for-loops testing matrix-topologies.
    // FIXME: updarte doccuemtnation wrt. resutls of this evaluation.
    { //! Test the effect of using two memory-allocation-calls:
      //! Start measurement:
      start_time_measurement();	    
      const bool isTo_use_continousSTripsOf_memory = true;
      float **distmatrix = NULL; char **mask = NULL;
      //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
      const uint size_of_array = 20000;    
      maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
      maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
      __assertClass_generateResultsOf_timeMeasurements("memory-allocation-procedure::2-calls", size_of_array, prev_timeOf_measurement);
    }
    { //! Test the effect of using "n" memory-allocation-calls:
      //! Start measurement:
      start_time_measurement();	    
      const bool isTo_use_continousSTripsOf_memory = false;
      float **distmatrix = NULL; char **mask = NULL;
      //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
      const uint size_of_array = 20000;    
      maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
      maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
      __assertClass_generateResultsOf_timeMeasurements("memory-allocation-procedure::linear", size_of_array, prev_timeOf_measurement);
    }
  }


#include "measure.h"
/* #include "bm_vertex.h" */
//#include "list_for_bm.h"



static void addindex(float *x, int n, const bool isTo_useIntrisinistic) {
  // FIXME: if this reulsts in a performance-increase, then mvoe this to the "libmine" software.
  if(isTo_useIntrisinistic == false) {
    for (int i = 0; i < n; i++)
      x[i] = x[i] + i;
  } else {
    // n a multiple of 4, x is 16-byte aligned
    __m128 index, incr, x_vec;
    // FIXME: why [below] "index"?
    index = _mm_set_ps(0, 1, 2, 3);
    incr = _mm_set1_ps(4);
    for(int i = 0; i < n/4; i++) {
      __m128 x_vec = _mm_load_ps(x + i*4);       // load 4 floats 
      x_vec = _mm_add_ps(x_vec, index);          // add index
      _mm_store_ps(x+i*4, x_vec);                // store back
      index = _mm_add_ps(index, incr);           // increment index
    }
  }
}

//! Store a value without loading a cache line, ie, use intrinsic functions:
static inline void StoreNTD(float* dest, __m128 const & source) {
  // FIXME: ask jc to validate [below]
  _mm_stream_ps((float*)dest, *(__m128*)&source); // MOVNTQ
  _mm_empty(); // EMMS
}


//! Test the impact of using knowledge of a comptuers emmory-ache to improve perofrmance of an algorithm.
//! Note: CLS described the level1-memory-cache, which may either be laoded 'in compile-time', or hard-coded: "gcc -DCLS=$(getconf LEVEL1_DCACHE_LINESIZE)", or "getconf LEVEL1_DCACHE_LINESIZE" from the terminal-line.
// FXIME: set [below] to "64" and test differnet cache-combinations of pow(2)     // #define CLS 64 
static void testEffectOf_exactInstructionsInMemoryAccess(const uint size_of_array, const bool isTo_use_continousSTripsOf_memory, const uint transpose, const bool isTo_sumDifferentArrays, const uint CLS = 64) {
  // FIXME: update our distance-matrix-code to 'handle' [below] cae, ie, pritn a warning and then use a non-cahce-effective strategy for comptuation.
  assert(CLS <= size_of_array);
  assert( (CLS % (uint)sizeof(float)) == 0); //! ie, as the 'chunks' need to be divided into 'feasible parts' for the memory-float-cahce-loading.
  const uint N = size_of_array;
  // TODO: allocate [”elow] using "posix_memalign"
  float **distmatrix = NULL; char **mask = NULL;
  maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
  float **distmatrix_result = NULL; char **mask_result = NULL;
  maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix_result, &mask_result, isTo_use_continousSTripsOf_memory);	  



  // FIXME: if this code resutls in a ntoeworthy perofmrance-inmpreovemetns, then udpate our distance-comptaution-aglortihms .... eg, to write a new procedure to comptue teh distance-metrics for all cases ... ie, as a 'pre-step' to "kemans" clsutering.

  // FIXME: validate that the sum is the same.
 
  float **res = distmatrix_result; float **mul1 = distmatrix; float **mul2 = distmatrix;

  char **mask_data_2 = NULL;
  if(isTo_sumDifferentArrays) {
    maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &mul2, &mask_data_2, isTo_use_continousSTripsOf_memory);	  
  }

  //! Initiate:
  for(uint i = 0; i < N; i++) {
    for(uint j = 0; j < N; j++) {
      res[i][j] = 0; 
      mul1[i][j] = 0;       
      mul2[i][j] = 0;
    }
  }

  { //! Test a 'default' appraoch, ie, the appraoch which we have observed in the diffrent code chunks (in other libraries) which we have read/studied:
    //! Start measurement:
    start_time_measurement();	    
    const uint nrows = N; const uint ncols = N;
    uint cnt_iterations = 0;
    for(uint i = 0; i < nrows; i++) {
      for(uint j = 0; j < ncols; j++) {
	for(uint k = 0; k < nrows; k++) {
	  cnt_iterations++;
	  if(transpose) {
	    res[i][j] += mul1[i][k] * mul2[k][j];
	  } else {
	    res[i][j] += mul1[i][k] * mul2[j][k]; //! ie, then similar to euclidian distance.
	  }
	}
      }
    }
    local__assertClass_generateResultsOf_timeMeasurements("matrix-multiplication-pattern:default", size_of_array, NULL, isTo_use_continousSTripsOf_memory, transpose, isTo_sumDifferentArrays, CLS);
    if(false) {printf("cnt_iterations=%u, expected-iterations=%u, at %s:%d\n", cnt_iterations, N*N*N, __FILE__, __LINE__);}
    assert(cnt_iterations == (N * N * N));
  }

  { //! Test an 'intristinstcs' appraoch:
    // assert(false); // FIXME: complete [below].
    //! Start measurement:
    start_time_measurement();	    
    const uint nrows = N; const uint ncols = N;
    const uint SM = 4; //! which is due to our "_mm_* calls [below]
    uint cnt_iterations = 0; const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM; 
    uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);
    const uint last_index2 = numberOf_chunks_rows - 1;
    for(uint i = 0; i < nrows; i++) {      
      for(uint j = 0; j < ncols; j++) {
	// assert(false); // FIXME: divide [below] into chunks of 4
	float sum = 0;
	//! Load one x coefficient into all the 4 vector elements
	for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < last_index2; cnt_index2++, index2 += SM) {
	  const uint k = index2;
	  //for(uint k = 0; k < nrows; k++) {
	  cnt_iterations++;
	  if(transpose == 0) { //! then: res[i][j] += mul1[i][k] * mul2[j][k]; //! ie, then similar to euclidian distance.
	    __m128 term1  = _mm_loadu_ps(&mul1[i][k]);
	    // FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
	    __m128 term2  = _mm_loadu_ps(&mul2[j][k]);  
	    __m128 mul = _mm_mul_ps(term1, term2);
	    //! Load the result-vector
	    //__m128 arr_result_tmp = _mm_loadu_ps(&res[i][j]);
	    //! Add the values:
	    //arr_result_tmp = _mm_add_ps(mul, arr_result_tmp);
	    //! Store the result:
	    //_mm_storeu_ps(&arr_result[i][j], arr_result_tmp);
	    // FIXME: validate correctness of [”elow] ... ie, that res[i][j ...] += mul1[i][k ... ] * mul2[j][k ... ];
	    float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, mul);
	    for(uint i = 0; i < 4; i++) {sum += result[i];}
	  } else { //! then: 
	    res[i][j] += mul1[i][k] * mul2[k][j]; //! ie, as the elemnts are nto continus, then we may not use 'undelryign itnrisnitstics'.
	  }
	}
    	//! Then iterate through the 'tail':
	for(uint k = last_index2; k < chunkSize_row_last; k++) {
	  if(transpose == 0) {
	    sum += mul1[i][k] * mul2[j][k]; //! ie, then similar to euclidian distance.
	  } else {
	    res[i][j] += mul1[i][k] * mul2[k][j];
	  }
	}
	if(transpose == 0) {
	  res[i][j] = sum;
	}
      }
    }
    local__assertClass_generateResultsOf_timeMeasurements("matrix-multiplication-pattern:intristinstics-alt1", size_of_array, NULL, isTo_use_continousSTripsOf_memory, transpose, isTo_sumDifferentArrays, CLS);
    if(false) {printf("cnt_iterations=%u, expected-iterations=%u, at %s:%d\n", cnt_iterations, N*N*N, __FILE__, __LINE__);}
    //assert(cnt_iterations == (N * N * N));
  }



  //! Compute: res(i,j) = a(i,k)*b(k,j) = a(i,1)*b(1,j) + a(i,2)*b(2,j) + ... + a(i,k)*b(k,j) 
  //! Nteo(1): In [”elow] "there are six nested loops. The outer loops iterate with intervals of SM (the cache line size divided by sizeof(float)). This divides the multiplication in several smaller problems which can be handled with more cache locality. The inner loops iterate over the missing indexes of the outer loops. There are, once again, three loops. The only tricky part here is that the k2 and j2  loops are in a different order. This is done since, in the actual computation, only one expression depends in k2 but two depend on j2". ["https://www.akkadia.org/drepper/cpumemory.pdf"]
  //! Note(2): [below] code is inspired by "https://www.akkadia.org/drepper/cpumemory.pdf" page 97: in contrast we for Euclid compute: res(i,j) = sum of{ a(i,k)*b(j,k) };  
  {
    float *__restrict__ rres;  float *__restrict__ rmul1; float *__restrict__  rmul2; const uint SM = (uint)(CLS / (uint)sizeof (float)); uint i2, k2; //! ie, the 'default' line.
    const uint nrows = N; const uint ncols = N; float **data1 = mul1; float **data2 = mul2; float **resultMatrix = res;
    //! Note: in [”elow] we use "__restrict__", which is used to avoid aliasing, ie, to try to ge thte same speed for numeric comptuation as 'reached' by Fortran programs.
    uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);
  uint cnt_iterations = 0; const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM; 
    const uint iterationIndex_2 = nrows;     uint numberOf_chunks_rows_2 = iterationIndex_2 / SM;  
    uint chunkSize_row_last_2 = SM; __get_updatedNumberOF_lastSizeChunk_entry(iterationIndex_2, SM, &numberOf_chunks_rows_2, &chunkSize_row_last_2);

    assert(SM % 4 == 0); //! ie, given our calls to _mm_store_ps(..) 

    // FIXME: try to include [below]
    { //! Test segmented iteration, though without optimized 'funciton-calls' for artimentic operations and memory-access, nor the 'utlizaitonon' of 'inner' arrays.
      //! Start measurement:
      start_time_measurement();	    
      //! Identify the last chunk-size for datasets where nrows and ncols are not divisible by SM.
      const uint nrows = N; const uint ncols = N;
      uint chunkSize_row_last = SM; uint chunkSize_col_last = SM;
      assert(nrows > 0); assert(ncols > 0); assert(SM > 0);
      __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);
      uint index1 = 0;
      //for(uint cnt_index1 = 0; cnt_index1 < numberOf_chunks_rows; cnt_index1++) {
      for(uint cnt_index1 = 0; cnt_index1 < numberOf_chunks_rows; cnt_index1++, index1 += SM) {
	const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_rows) ? SM : chunkSize_row_last;
	//! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
	uint numberOf_chunks_rows_local = 0; uint chunkSize_row_last_local = 0;  
	if(data1 == data2) {
	  __get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, &numberOf_chunks_rows_local, &chunkSize_row_last_local);
	} else { //! else the matrix is assumed not to be symmetric.
	  numberOf_chunks_rows_local = numberOf_chunks_rows_2; chunkSize_row_last_local = chunkSize_row_last_2;
	}
	for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_rows_local; cnt_index2++, index2 += SM) {
	  const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_rows_local) ? SM : chunkSize_row_last_local;
	  uint i = 0;
	  for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
	    const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; 
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    //assert(resultMatrix[index1]);
	    //const uint debug_startPos_rres = (index1 * ncols) + index2;
	    // assert(resultMatrix + debug_startPos_rres);
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[index1][i];
		 i2 < chunkSize_index1;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		   rres += ncols,  rmul1 += ncols
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {
	      // assert(rres <= pointer_resultLastPos1);
	      // assert( ((rres + ncols*chunkSize_index1) -1) <= pointer_resultLastPos1);

	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(float) ??
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      
	      uint k2 = 0; 
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[index2][i];  
		   k2 < chunkSize_index2; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmul2 += ncols
		   ) {				
		//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		// _mm_prefetch (&data2[8], _MM_HINT_NTA);

		if(true) { //! Note: for a dataset of 400 relations this approach reduces the time-consumption from 0.20 seconds to 0.14 seconds, ie, a speed-up-improvement of 1.25x: main reason for this speed-up is the 'dealy' in writing of data, a 'dealy' handled/'fxied' by the "_mm_storeu_ps(..)" routine/funciton-call.
		  float result[4] = {0, 0, 0, 0};
		  for (uint j2 = 0; j2 < chunkSize_cnt_i; j2 += 4) {
		    __m128 term1  = _mm_loadu_ps(&rmul1[j2]);
			// FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
		    __m128 term2  = _mm_loadu_ps(&rmul2[j2]);  
		    __m128 mul = _mm_mul_ps(term1, term2);
		    //! Load the result-vector
		    //		    __m128 arr_result_tmp = _mm_loadu_ps(&res[i][j]);
		    __m128 arr_result_tmp = _mm_loadu_ps(&result[0]);
		    //! Add the values:
		    arr_result_tmp = _mm_add_ps(mul, arr_result_tmp);
		    //! Store the result:
		    // FIXME: validate corrrectness of [”elow] call.
		    _mm_storeu_ps(&result[0], arr_result_tmp);
		    //_mm_storeu_ps(&arr_result[i][j], arr_result_tmp);
		    // FIXME: validate correctness of [”elow] ... ie, that res[i][j ...] += mul1[i][k ... ] * mul2[j][k ... ];
		    //_mm_add_ps(result, mul);
		    //_mm_store_ps(result, mul);
		    //for(uint i = 0; i < 4; i++) {rres[k2] += result[i];}
		  }
		  //! Combine teh vectors:
		  for(uint i = 0; i < 4; i++) {rres[k2] += result[i];}
		} else {
		  //! The operation:		
		  for (uint j2 = 0; j2 < chunkSize_cnt_i; ++j2) {
		    rres[k2] += rmul1[j2] * rmul2[j2];
		  }
		}
	      }
	    }	    
	  }
	}
      }
      local__assertClass_generateResultsOf_timeMeasurements("matrix-multiplication-pattern:cache-segmented-accessMemoryHihghLevel", size_of_array, NULL, isTo_use_continousSTripsOf_memory, transpose, isTo_sumDifferentArrays, CLS);


      /* // FIXME: try to include [below] */
      /* { //! Test segmented iteration, though without optimized 'funciton-calls' for artimentic operations and memory-access: */

      /* 	assert(false); // FIXME: include a permtuation of [ªbvoe] in [”elow] ... using intristinstics */
      /* } */
      /* { */
      /* 	assert(false); // FIXME: include a permtuation of [ªbvoe] in [”elow] ... using intristinstics */
      /* } */
      /* { */
      /* 	assert(false); // FIXME: include a permtuation of [ªbvoe] in [”elow] ... using intristinstics */
      /* } */

      /* assert(false); // FIXME: merge [”elow] with [ªbove] to test the use or aritmetic itnrisitnstcs. */
    
      /* // FIXME: try to include [below] <-- first need to resolve/identify reasons for current problems wrt. memory-access as compleained by valgrindf */
      /* if(false) { //! Test segmented iteration, including optimized 'funciton-calls' to test the effect of increased specifity for artimentic operations and memory-access: */
      /* 	//! Note: we observe that this procedure goes apporx 3x slower than the [ªbov€] ode, ie, the compiler maanges to identify better itnructions than our suggestions, whihc may be explained by compilers being updated since the work of "https://www.akkadia.org/drepper/cpumemory.pdf"  */
      /* 	//! Identify the last chunk-size for datasets where nrows and ncols are not divisible by SM. */
      /* 	const uint nrows = N; const uint ncols = N; */
      /* 	uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; */
      /* 	assert(nrows > 0); assert(ncols > 0); assert(SM > 0); */
      /* 	__get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last); */

      /* 	//! Start measurement: */
      /* 	start_time_measurement();	     */
      /* 	//! Iterate: */
      /* 	uint cnt_iterations = 0; uint cnt_i = 0;  const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM; */
      /* 	for (uint i = 0; i < nrows; i += SM) { */
      /* 	  assert(i < nrows); */
      /* 	  const uint chunkSize_row = (cnt_i++ < numberOf_chunks_rows) ? SM : chunkSize_row_last; */
      /* 	  uint cnt_j = 0;  for (uint j = 0; j < ncols; j += SM) { */
      /* 	    assert(j < ncols); */
      /* 	    const uint chunkSize_col_j = (cnt_j++ < numberOf_chunks_cols) ? SM : chunkSize_col_last; */
      /* 	    uint cnt_k = 0; for (uint k = 0; k < ncols; k += SM) { */
      /* 	      assert(k < ncols); */
      /* 	      const uint chunkSize_col_k = (cnt_k++ < numberOf_chunks_cols) ? SM : chunkSize_col_last; */
	    
      /* 	      //! Compute: res(i,j) = a(i,k)*b(k,j) = a(i,1)*b(1,j) + a(i,2)*b(2,j) + ... + a(i,k)*b(k,j)  */
      /* 	      for (i2 = 0,  */
      /* 		     rres = (transpose)  ? &res[j][i]  : &res[i][j],  */
      /* 		     rmul1 = (transpose) ? &mul1[k][i] : &mul1[i][k]; */
      /* 		   i2 < chunkSize_row; */
      /* 		   ++i2, rres += N, rmul1 += N) { */
      /* 		assert(i2 < chunkSize_row); */
      /* 		//! Prefetch the data: */
      /* 		// FIXME: in [”elow] is "8" duet ot hte sizeof(float) ?? */
      /* 		// FIXME: wrt. [below] ... try calling _mm_prefetch(&rres[8], _MM_HINT_NTA); */
      /* 		_mm_prefetch (&rmul1[8], _MM_HINT_NTA); */
	  
      /* 		for (k2 = 0, rmul2 =  */
      /* 		       (transpose) ? &mul2[k][j] : &mul2[j][k]; k2 < chunkSize_col_k; ++k2, rmul2 += N) { */
      /* 		  assert(k2 < chunkSize_col_k); */
      /* 		  //! The operation: */
      /* 		  __m128 m1d = _mm_load_ss (&rmul1[k2]); */
      /* 		  m1d = _mm_unpacklo_ps (m1d, m1d); */
      /* 		  //! The operation: */
      /* 		  for (uint j2 = 0; j2 < chunkSize_col_j; ++j2) { */
      /* 		    // for (uint i = 0; i < N; i += SM) { */
      /* 		    // 	for (uint j = 0; j < N; j += SM) { */
      /* 		    // 	  for (uint k = 0; k < N; k += SM) { */
      /* 		    // 	    for (i2 = 0, rres = &res[i][j], rmul1 = &mul1[i][k]; i2 < SM; */
      /* 		    // 		 ++i2, rres += N, rmul1 += N) { */
      /* 		    // 	      //! Prefetch the data: */
      /* 		    // 	      // FIXME: in [”elow] is "8" duet ot hte sizeof(float) ?? */
      /* 		    // 	      _mm_prefetch (&rmul1[8], _MM_HINT_NTA); */
	  
      /* 		    // 	      for (k2 = 0, rmul2 = (transpose) ? &mul2[k][j] : &mul2[j][k]; k2 < SM; ++k2, rmul2 += N) { */
      /* 		    // //! The operation: */
      /* 		    // __m128 m1d = _mm_load_ss (&rmul1[k2]); */
      /* 		    // m1d = _mm_unpacklo_ps (m1d, m1d); */
      /* 		    // //! The operation: */
      /* 		    // for (uint j2 = 0; j2 < SM; j2++) { */
      /* 		    __m128 m2 = _mm_load_ss (&rmul2[j2]); */
      /* 		    __m128 r2 = _mm_load_ss (&rres[j2]); */
      /* 		    _mm_store_ss (&rres[j2], _mm_add_ss (_mm_mul_ss (m2, m1d), r2));		   */
      /* 		  } */
      /* 		} */
      /* 	      } */
      /* 	    } */
      /* 	  } */
      /* 	} */
      /* 	__assertClass_generateResultsOf_timeMeasurements("matrix-multiplication-pattern:cache-segmented-andOptimizedMemoryCalls", size_of_array, NULL, isTo_use_continousSTripsOf_memory, transpose, isTo_sumDifferentArrays, CLS); */
  /*   } */
    }
  }
  //! De-allocate locally reserved memory:
  maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
  maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix_result, mask_result, isTo_use_continousSTripsOf_memory);    
  if(isTo_sumDifferentArrays) {
    maskAllocate__freedatamask(/*nelements=*/size_of_array, mul2, mask_data_2, isTo_use_continousSTripsOf_memory);    
  }
}


//! Test the impact of using knowledge of a comptuers emmory-ache to improve perofrmance of an algorithm.
static void evaluate_effectsOf_tiling() {
  /**
     @remarks in this comparison we compare a 'default'/'rodinary' matrix-multiplication with cache-senstive matrix-multiplication. Results are:
     #! For a [400][400]: difference= : "" VS ""
     #! For a [800][800]: difference= 2.6: "tick=7.1000s" VS "tick=2.8100s"
     #! For a [1000][1000]: difference= : ""
     #! For a [1200][1200]: difference=3.2x  :  "tick=29.6100s"  VS "tick=9.2800s"
     #! For a [2400][2400]: difference=3.7x  :  "tick=276.2600s" VS "tick=74.9200s"
     #! For a [][]: difference=x : "" VS ""
     @remarks we observe that in C++ machine-optimized aritmetic operations does not decrease the exeuction-ie, memory-optmization (of soruce-code) is (assumed to be) computer-machine-inpdendendet.

     tick=7.1000s cpu=7.0800s system=0.0000s (finding 800 relations with matrix-multiplication-pattern:default, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=yes)
     tick=2.7100s cpu=2.7000s system=0.0000s (finding 800 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=yes, memoryAccessedTransposely=yes, compareTwoMatrices=no)
     tick=2.8100s cpu=2.8000s system=0.0000s (finding 800 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=yes)

     FRom above we observe that our policy works considerably better irrespective of the data 'orignating' from different matrices: our 'senstivity-programming' wrt. memory-cache reduce time-overhead assicated to loading memory from different data-chunks.

     ... to compare differences in 'naive' VS 'optimized, the following bash-call is used: "grep -P "(default.+memoryAllocatedIdeal\=no,)|(cache\-segmented,)" out.txt"

     tick=14.2100s cpu=14.1600s system=0.0000s (finding 1200 relations with matrix-multiplication-pattern:default, memoryAllocatedIdeal=no, memoryAccessedTransposely=no, compareTwoMatrices=yes, and CLS=64)
     tick=29.9900s cpu=29.8800s system=0.0000s (finding 1200 relations with matrix-multiplication-pattern:default, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=no, and CLS=8)
     tick=30.0500s cpu=29.9500s system=0.0000s (finding 1200 relations with matrix-multiplication-pattern:default, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=yes, and CLS=64)       
     tick=9.3700s cpu=9.3300s system=0.0100s (finding 1200 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=no, memoryAccessedTransposely=no, compareTwoMatrices=yes, and CLS=64)
     tick=9.1300s cpu=9.1000s system=0.0000s (finding 1200 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=yes, memoryAccessedTransposely=no, compareTwoMatrices=yes, and CLS=64)
     tick=14.2700s cpu=14.2000s system=0.0100s (finding 1200 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=yes, memoryAccessedTransposely=no, compareTwoMatrices=no, and CLS=8)
     tick=18.4000s cpu=18.3400s system=0.0000s (finding 1200 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=no, and CLS=8)
     tick=9.4500s cpu=9.4200s system=0.0000s (finding 1200 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=no, and CLS=64)
 

     //! Note: in [below] we have a difference of approx. 3.7x
     tick=276.2600s cpu=274.4100s system=0.7900s (finding 2400 relations with matrix-multiplication-pattern:default, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=no, and CLS=8)
     tick=160.8200s cpu=159.8300s system=0.3900s (finding 2400 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=no, memoryAccessedTransposely=yes, compareTwoMatrices=no, and CLS=8)
     tick=113.7400s cpu=113.0600s system=0.2300s (finding 2400 relations with matrix-multiplication-pattern:default, memoryAllocatedIdeal=no, memoryAccessedTransposely=no, compareTwoMatrices=no, and CLS=8)
     tick=76.9500s cpu=76.7000s system=0.0100s (finding 2400 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=no, memoryAccessedTransposely=no, compareTwoMatrices=no, and CLS=32)
     tick=78.5900s cpu=78.0300s system=0.2500s (finding 2400 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=yes, memoryAccessedTransposely=no, compareTwoMatrices=yes, and CLS=32)
     tick=74.9200s cpu=74.6200s system=0.0500s (finding 2400 relations with matrix-multiplication-pattern:cache-segmented, memoryAllocatedIdeal=yes, memoryAccessedTransposely=no, compareTwoMatrices=yes, and CLS=64)
  **/


  // FIXME: seems like "testEffectOf_exactInstructionsInMemoryAccess_2d_loop(..)" perofrm better with CLS=32 (ie, using a smaller chace-size than the actual L1 memory-ache-size) ... validate this assumption of larger matrices.

  // FIXME: test the cost of 'transposed euclidian' VS non-transposed euclidian' ... ie, write a new test-case for the latter.   

  // FIXME: mmake use of this funciton.
  const uint kilo=1000; const uint arrOf_matrixColumnSizes_size = 8+4+4; const uint arrOf_matrixColumnSizes[arrOf_matrixColumnSizes_size] = {
    400,   800, 1200,2400, 
    3600, 4800, 6000, 7000,
    8000,    9000,    10*kilo, 11*kilo, 
    12*kilo, 13*kilo, 14*kilo, 15*kilo};
  const uint arrOf_cacheChunkSize_size = 6; const uint arrOf_cacheChunkSize[arrOf_cacheChunkSize_size] = {
    64, 64, 64, // FIXME: remvoe this and include [below]
    //8, 32, 64, 
													  128, 256, 512};
  //! Note: we use the array-size as the first loop given the strong correlation between matrix-size and exueciton-time.
  for(uint i = 0; i < arrOf_matrixColumnSizes_size; i++) {
    if(true) {printf("\n----\n");}
    const uint size_of_array = arrOf_matrixColumnSizes[i];
    for(uint transpose = 0; transpose < 2; transpose++) {
      for(uint isTo_use_continousSTripsOf_memory = 0; isTo_use_continousSTripsOf_memory < 2; isTo_use_continousSTripsOf_memory++) {
	for(uint isTo_sumDifferentArrays = 0; isTo_sumDifferentArrays < 2; isTo_sumDifferentArrays++) {
	  //! Then test the effect of changes to L1 memory-cache assumptions.
	  //! Note: these resutls eare expected to vary between differne3t memory-hardware-architectures.
	  for(uint CLS_index = 0; CLS_index < arrOf_cacheChunkSize_size; CLS_index++) {
	    // FIXME: for the [”elow] functions ask jan-Christan to validate correctness ... and to consider new improvements wrt. optimizaiton.
	    const uint CLS = arrOf_cacheChunkSize[CLS_index];
	    if(CLS <= size_of_array) {
	      //! First test for an 'approach' which use amtrix-muliplicaiton, ie, a 3d-loop:
	      testEffectOf_exactInstructionsInMemoryAccess(size_of_array, (bool)isTo_use_continousSTripsOf_memory, transpose, (bool)isTo_sumDifferentArrays, CLS);
	      //! Tehreafter test the effect on 2d-loops:
	      // testEffectOf_exactInstructionsInMemoryAccess_2d_loop(size_of_array, (bool)isTo_use_continousSTripsOf_memory, transpose, (bool)isTo_sumDifferentArrays, CLS);
	      // FIXME: remove the "** mask" attributes ... and 'replac'e teh latter with "T_FLOAT_MAX" ... and then validate that the 'code goes 2x faster'
	    } //! else we drop the test.
	  }
	}
      }
    }
  }
}

//! @return teh sum of values in the list.
static float __get_sum(const float *arr, const uint arr_size) {
  assert(arr); assert(arr_size);
  float sum = 0;
  for(uint i = 0; i < arr_size; i++) {sum += arr[i];}
  return sum;
}

// uint32_t sum_array(const uint8_t a[], int n)
// {
//   const __m128i vk0 = _mm_set1_epi8(0);       // constant vector of all 0s for use with _mm_unpacklo_epi8/_mm_unpackhi_epi8
//   const __m128i vk1 = _mm_set1_epi16(1);      // constant vector of all 1s for use with _mm_madd_epi16
//   __m128i vsum = _mm_set1_epi32(0);           // initialise vector of four partial 32 bit sums
//   uint32_t sum;
//   int i;

//   for (i = 0; i < n; i += 16)
//     {
//       __m128i v = _mm_load_si128(&a[i]);      // load vector of 8 bit values
//       __m128i vl = _mm_unpacklo_epi8(v, vk0); // unpack to two vectors of 16 bit values
//       __m128i vh = _mm_unpackhi_epi8(v, vk0);
//       vsum = _mm_add_epi32(vsum, _mm_madd_epi16(vl, vk1));
//       vsum = _mm_add_epi32(vsum, _mm_madd_epi16(vh, vk1));
//       // unpack and accumulate 16 bit values to
//       // 32 bit partial sum vector

//     }
//   // horizontal add of four 32 bit partial sums and return result
//   vsum = _mm_add_epi32(vsum, _mm_srli_si128(vsum, 8));
//   vsum = _mm_add_epi32(vsum, _mm_srli_si128(vsum, 4));
//   sum = _mm_cvtsi128_si32(vsum);
//   return sum;
// }

#include "immintrin.h"

//! Evlauate the perfomrance-difference when using intristinstics to compare two vectors.
static void evaluate_cmpOf_two_vectors_wrt_intrisinitstics()  {

  const uint list_size = 1000;   //const uint list_size = 12000;
  //const uint nrows = list_size; const uint ncols = list_size;
  //! Note: the exeuction-time of our comparted/evlauated appraoches are indepednent of the matrix-size <-- FIXME: update our aritlces wrt. the latter.  
  const uint nrows = list_size*1000; const uint ncols = 64;

  float *arr_1 = allocate_1d_list_float(nrows, 1); 
  float *arr_2 = allocate_1d_list_float(ncols, 1); float *arr_result = allocate_1d_list_float(ncols, 0);
  
  const bool isTo_print_sum = false; //! Add random numbers ot the lists, ie, to 'ensure' that the compielr does not 'infer from the default vlaues the result'.

  // assert(false); // FIXME: remvoe

  // FIXME: extend this comparison to also perofrm an alla-gainst-all-matrix-comparison.

  for(uint isTo_store_result = 0; isTo_store_result < 2; isTo_store_result++) {
    //! Reset, ie, to avoid the first comptuation 'to' only be performed in optimized mode'
    for(uint i = 0; i < nrows; i++) {arr_1[i] =  rand();}
    for(uint i = 0; i < ncols; i++) {arr_2[i] = arr_result[i] =rand();}

    //! ----------------------------------------------------------------
    { //! Naive:
      start_time_measurement(); 
      float sum = 0;
      for(uint i = 0; i < nrows; i++) {
	for(uint j = 0; j < ncols; j++) {
	  if(isTo_store_result) {
	    arr_result[j] += arr_1[i] * arr_2[j];
	  } else {
	    sum += arr_1[i] * arr_2[j];
	  }
	}
      }
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "memoryAccess::traverse-2d-[%u][%u] use-intrisntistcs=false, isTo_store_result=%s", nrows, ncols, (isTo_store_result) ? "true" : "false"); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;
      if(isTo_print_sum) {printf("sum=%f | %f, at %s:%d\n", sum, __get_sum(arr_result, ncols), __FILE__, __LINE__);}	
    }
    //! ----------------------------------------------------------------
    { //! Intrisntistcs, alt1:
      //! Reset, ie, to avoid the first comptuation 'to' only be performed in optimized mode'
      for(uint i = 0; i < nrows; i++) {arr_1[i] =  rand();}
      for(uint i = 0; i < ncols; i++) {arr_2[i] = arr_result[i] = rand();}
      float sum = 0;
      start_time_measurement(); 
      assert(ncols == 64); //! ie, a simplicitc assumption.
      __m128 xv;
      __m128 Av0, mul0, yv0;
      __m128 Av1, mul1, yv1;
      __m128 Av2, mul2, yv2;
      __m128 Av3, mul3, yv3;
      __m128 Av4, mul4, yv4;
      __m128 Av5, mul5, yv5;
      __m128 Av6, mul6, yv6;
      __m128 Av7, mul7, yv7;


      /* Pre-load the entire y vector into registers */
      yv0  = _mm_loadu_ps(&arr_result[0 ]);
      yv1  = _mm_loadu_ps(&arr_result[4 ]);
      yv2  = _mm_loadu_ps(&arr_result[8 ]);
      yv3  = _mm_loadu_ps(&arr_result[12]);
      yv4  = _mm_loadu_ps(&arr_result[16]);
      yv5  = _mm_loadu_ps(&arr_result[20]);
      yv6  = _mm_loadu_ps(&arr_result[24]);
      yv7  = _mm_loadu_ps(&arr_result[28]);

      // /* pre-fetch the first column of A into cache */
      // _mm_prefetch((void *)&arr_2[0 ], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[4 ], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[8 ], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[12], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[16], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[20], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[24], _MM_HINT_T0);
      // _mm_prefetch((void *)&arr_2[28], _MM_HINT_T0);


      const uint base_size = 32; const uint cnt_sections = 2;
      assert(base_size * cnt_sections == ncols); //! ie, wr.t correctness of [above]
      for(uint i = 0; i < nrows; i++) {

	for(uint sections_cols = 0; sections_cols < cnt_sections; sections_cols++) {
	  /* pre-fetch (j+1)-th column of A into cache */
	  const uint offsetRows = base_size * cnt_sections; const uint offsetRows_plussOne = 0*(offsetRows + 1);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 0 ], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 4 ], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 8 ], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 12], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 16], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 20], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 24], _MM_HINT_T0);
	  // _mm_prefetch((void *)&arr_2[offsetRows_plussOne + 28], _MM_HINT_T0);


	  /* Load one x coefficient into all the 4 vector elements */
	  xv = _mm_load1_ps(&arr_1[i]);

	  /* Load an entire column j of A into vector registers*/
	  Av0  = _mm_loadu_ps(&arr_2[offsetRows + 0 ]);
	  Av1  = _mm_loadu_ps(&arr_2[offsetRows + 4 ]);
	  Av2  = _mm_loadu_ps(&arr_2[offsetRows + 8 ]);
	  Av3  = _mm_loadu_ps(&arr_2[offsetRows + 12]);
	  Av4  = _mm_loadu_ps(&arr_2[offsetRows + 16]);
	  Av5  = _mm_loadu_ps(&arr_2[offsetRows + 20]);
	  Av6  = _mm_loadu_ps(&arr_2[offsetRows + 24]);
	  Av7  = _mm_loadu_ps(&arr_2[offsetRows + 28]);

	  /* multiply */
	  mul0 = _mm_mul_ps(Av0, xv);
	  mul1 = _mm_mul_ps(Av1, xv);
	  mul2 = _mm_mul_ps(Av2, xv);
	  mul3 = _mm_mul_ps(Av3, xv);
	  mul4 = _mm_mul_ps(Av4, xv);
	  mul5 = _mm_mul_ps(Av5, xv);
	  mul6 = _mm_mul_ps(Av6, xv);
	  mul7 = _mm_mul_ps(Av7, xv);
	  mul0 = _mm_mul_ps(Av0, xv);

	  /* add */
	  yv0  = _mm_add_ps(mul0, yv0);
	  yv1  = _mm_add_ps(mul1, yv1);
	  yv2  = _mm_add_ps(mul2, yv2);
	  yv3  = _mm_add_ps(mul3, yv3);
	  yv4  = _mm_add_ps(mul4, yv4);
	  yv5  = _mm_add_ps(mul5, yv5);
	  yv6  = _mm_add_ps(mul6, yv6);
	  yv7  = _mm_add_ps(mul7, yv7);
	}
      }

      /* store y back to memory */
      _mm_storeu_ps(&arr_result[0 ], yv0);
      _mm_storeu_ps(&arr_result[4 ], yv1);
      _mm_storeu_ps(&arr_result[8 ], yv2);
      _mm_storeu_ps(&arr_result[12], yv3);
      _mm_storeu_ps(&arr_result[16], yv4);
      _mm_storeu_ps(&arr_result[20], yv5);
      _mm_storeu_ps(&arr_result[24], yv6);
      _mm_storeu_ps(&arr_result[28], yv7);
      
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "memoryAccess::traverse-2d-[%u][%u] use-intrisntistcs=true, for-loop-optimization=true, isTo_store_result=%s", nrows, ncols, (isTo_store_result) ? "true" : "false"); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;
      if(isTo_print_sum) {printf("sum=%f | %f, at %s:%d\n", sum, __get_sum(arr_result, ncols), __FILE__, __LINE__);}	
    }

    { //! Intrisntistcs, alt2:
      //! Reset, ie, to avoid the first comptuation 'to' only be performed in optimized mode'
      for(uint i = 0; i < nrows; i++) {arr_1[i] =  rand();}
      for(uint i = 0; i < ncols; i++) {arr_2[i] = arr_result[i] = rand();}

      // FIXME[article]: update our artilce wrt. [below] ... seems like we get a 38x performancre-improvement 'though [”elow] appraoch': while approx. 3x with "o3" compilation we 'reach' 3.8x 2ith "-O3" optimized compilation .... and in non-otpimzied mode for ncols=64 we get a 1.5 x optimizaiton-degree
      start_time_measurement(); 
      //! Iterate:
      __m128 Av, mul;
      float sum = 0;
    
      // FIXME: test different variations of CLS
      // const uint CLS = 64;     const uint SM = (uint)(CLS / (uint)sizeof (float)); uint i2, k2; //! ie, the 'default' line
      const uint SM = 4; //! which is due to our "_mm_* calls [below]
      uint cnt_iterations = 0; const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM; 
      uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);

      // FIXME: if [below] increases the performance ... then cosndier including 'this' into our __optimal_compute_allAgainstAll(..) funciton

      for(uint i = 0; i < nrows; i++) {
	// xv = _mm_load1_ps(&x[j]);
	
	//! Ntoe: explict pre-fetching seems not to be of improtacne wrt. the exeuction-time
	//_mm_prefetch((void *)&arr_1[i + 0 ], _MM_HINT_T0);
	//_mm_prefetch((void *)&arr_1[i + 0 ], _MM_HINT_NTA);
	//! Load the first vector:
	__m128 xv = _mm_load1_ps(&arr_1[i]);
	const uint last_index2 = numberOf_chunks_cols - 1;
	//! Load one x coefficient into all the 4 vector elements
	for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < last_index2; cnt_index2++, index2 += SM) {
	  const uint j = index2;
	  // _mm_prefetch((char*)&data[4 * N], _MM_HINT_T0);
	  //for(uint j = 0; j < list_size; j+= 4) {
	  //! Load the second vector
	  //_mm_prefetch((void *)&arr_2[j + 0 ], _MM_HINT_T0);
	  __m128 yv  = _mm_loadu_ps(&arr_2[j]);      __m128 mul = _mm_mul_ps(yv, xv);
	  if(isTo_store_result) {
	    //! Load the result-vector
			// FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
	    __m128 arr_result_tmp = _mm_loadu_ps(&arr_result[j]);
	    //! Add the values:
	    arr_result_tmp = _mm_add_ps(mul, arr_result_tmp);
	    //! Store the result:
	    //_mm_stream_ps(&arr_result[j], arr_result_tmp);
	    //! Note: when "_mm_stream_ps(..)" is sued instead of "_mm_store_ss(..)" the exeuction-time increases by a factor of more than 6x <-- FIXME: may you jan-christain explain thsi difference?
	    _mm_storeu_ps(&arr_result[j], arr_result_tmp);
	  } else {
	    //! Copy the result to the local variable:
	    float result[4] = {0, 0, 0, 0}; _mm_store_ps (result, mul);
	    for(uint m = 0; m < 4; m++) {sum += result[m];}
	  }
	}
	//! Then iterate through the 'tail':
	for(uint j = last_index2; j < chunkSize_col_last; j++) {
	  if(isTo_store_result) {
	    arr_result[j] += arr_1[i]*arr_2[j];
	  } else {
	    sum += arr_1[i]*arr_2[j];
	  }
	}
      }
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "memoryAccess::traverse-2d-[%u][%u] use-intrisntistcs=true, isTo_store_result=%s", nrows, ncols, (isTo_store_result) ? "true" : "false"); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;
      if(isTo_print_sum) {printf("sum=%f | %f, at %s:%d\n", sum, __get_sum(arr_result, ncols), __FILE__, __LINE__);}	
    }

    { //! Intrisntistcs: compare "_mm_storeu_ps(..)" VS "_mm_store_ps(..)"
      //! Note: seems like the difference between "_mm_storeu_ps(..)" VS "_mm_store_ps(..)" is insigifnt: at most we 'get' a differnece of 1.04x.
      // FIXME[ªrticle]: wrt. wrt. [ªbove] "note"
      //! Reset, ie, to avoid the first comptuation 'to' only be performed in optimized mode'
      for(uint i = 0; i < nrows; i++) {arr_1[i] =  rand();}
      for(uint i = 0; i < ncols; i++) {arr_2[i] = arr_result[i] = rand();}

      // FIXME[article]: update our artilce wrt. [below] ... seems like we get a 38x performancre-improvement 'though [”elow] appraoch': while approx. 3x with "o3" compilation we 'reach' 3.8x 2ith "-O3" optimized compilation .... and in non-otpimzied mode for ncols=64 we get a 1.5 x optimizaiton-degree
      start_time_measurement(); 
      //! Iterate:
      __m128 Av, mul;
      float sum = 0;
    
      // FIXME: test different variations of CLS
      // const uint CLS = 64;     const uint SM = (uint)(CLS / (uint)sizeof (float)); uint i2, k2; //! ie, the 'default' line
      const uint SM = 4; //! which is due to our "_mm_* calls [below]
      uint cnt_iterations = 0; const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM; 
      uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);

      // FIXME: if [below] increases the performance ... then cosndier including 'this' into our __optimal_compute_allAgainstAll(..) funciton

      for(uint i = 0; i < nrows; i++) {
	// xv = _mm_load1_ps(&x[j]);
	
	//! Ntoe: explict pre-fetching seems not to be of improtacne wrt. the exeuction-time
	//_mm_prefetch((void *)&arr_1[i + 0 ], _MM_HINT_T0);
	//_mm_prefetch((void *)&arr_1[i + 0 ], _MM_HINT_NTA);
	//! Load the first vector:
	__m128 xv = _mm_load1_ps(&arr_1[i]);
	const uint last_index2 = numberOf_chunks_cols - 1;
	//! Load one x coefficient into all the 4 vector elements
	for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < last_index2; cnt_index2++, index2 += SM) {
	  const uint j = index2;
	  // _mm_prefetch((char*)&data[4 * N], _MM_HINT_T0);
	  //for(uint j = 0; j < list_size; j+= 4) {
	  //! Load the second vector
	  //_mm_prefetch((void *)&arr_2[j + 0 ], _MM_HINT_T0);
	  __m128 yv  = _mm_loadu_ps(&arr_2[j]);      __m128 mul = _mm_mul_ps(yv, xv);
	  if(isTo_store_result) {
	    //! Load the result-vector
			// FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
	    __m128 arr_result_tmp = _mm_loadu_ps(&arr_result[j]);
	    //! Add the values:
	    arr_result_tmp = _mm_add_ps(mul, arr_result_tmp);
	    //! Store the result:
	    //_mm_stream_ps(&arr_result[j], arr_result_tmp);
	    //! Note: when "_mm_stream_ps(..)" is sued instead of "_mm_store_ss(..)" the exeuction-time increases by a factor of more than 6x <-- FIXME: may you jan-christain explain thsi difference?
	    _mm_store_ps(&arr_result[j], arr_result_tmp);
	  } else {
	    //! Copy the result to the local variable:
	    float result[4] = {0, 0, 0, 0}; _mm_store_ps (result, mul);
	    for(uint m = 0; m < 4; m++) {sum += result[m];}
	  }
	}
	//! Then iterate through the 'tail':
	for(uint j = last_index2; j < chunkSize_col_last; j++) {
	  if(isTo_store_result) {
	    arr_result[j] += arr_1[i]*arr_2[j];
	  } else {
	    sum += arr_1[i]*arr_2[j];
	  }
	}
      }
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "memoryAccess::traverse-2d-[%u][%u] use-intrisntistcs=true, isTo_store_result=%s, instead of _mm_storeu_ps(..) use _mm_store_ps(..)", nrows, ncols, (isTo_store_result) ? "true" : "false"); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;
      if(isTo_print_sum) {printf("sum=%f | %f, at %s:%d\n", sum, __get_sum(arr_result, ncols), __FILE__, __LINE__);}	
    }
  }


  // FIXME: compare loop-unrolling for different row-sizes: in order to 'get' a fixed 'row-size' anfd 'fixed column-size' we sue 'tiling as an inner chunk'.
  // FIXME: generalize [ªbove] using loop-unrolloing ... http://stackoverflow.com/questions/28231743/self-unrolling-macro-loop-in-c-c
  free_1d_list_float(&arr_1);   free_1d_list_float(&arr_2);   free_1d_list_float(&arr_result);
}


//! Adjust the score by the sum of weights for the 'variable' step:
//! Note: this funciton is used in our "cluster.c" and our "graphAlgorithms_distance" to post-process the result of 3d-for-loop to investigate/compute a matrix from two other matrices.
static void __adjustMatrix_byWeight(const uint sizeOf_i, const float weight[], float **resultMatrix, const uint nrows, const uint ncols) {
  assert(sizeOf_i > 0);
  //assert(weight);
  assert(resultMatrix);
  float sumOf_weights = 0;
  if(weight) {
    for(uint i = 0; i < sizeOf_i; i++) {
      const float weight_local = (weight) ? weight[i] : 1;
      sumOf_weights += weight_local;	
    }
  } else {sumOf_weights =  sizeOf_i;}
  // TODO: validate correctness of [below] calsue.
  if(sumOf_weights != 0) {
    //! Update 
    //printf("nrows=%u, ncols=%u, at %s:%d\n", nrows, ncols,__FILE__, __LINE__);
    for(uint index1 = 0; index1 < nrows; index1++) {
      assert(index1 < nrows);
      //printf("[%u/%u], at %s:%d\n", index1, nrows, __FILE__, __LINE__);
      assert(resultMatrix[index1]);
      assert(resultMatrix[index1][0] != T_FLOAT_MAX); // FIXME: remove
      //}
      for(uint index2 = 0; index2 < ncols; index2++) {
      	// FIXME: instead of [”elow] use _mm_storeu_ps .... ite, to dealy the "resultMatrix[index1][index2]" update
      	float result = resultMatrix[index1][index2];
      	if(result != 0) {result /= sumOf_weights;} //! else the alternative case may be explained by an empty cluster.
      	resultMatrix[index1][index2] = result;
      }
    }
  }
  // assert(false); // FIXME: include [ªbov€].
}



// //! Indeitfy when the vlaeus have a 'max-valeui':
// inline __m128 MaximumAbsoluteComponent (__m128 const v) {
//   __m128 SIGN = _mm_set1_ps(0x80000000u);
//   __m128 vAbs = _mm_andnot_ps(SIGN, v);
//   __m128 max0 = _mm_shuffle_ps(vAbs, vAbs, _MM_SHUFFLE(0,0,0,0));
//   __m128 max1 = _mm_shuffle_ps(vAbs, vAbs, _MM_SHUFFLE(1,1,1,1));
//   __m128 max2 = _mm_shuffle_ps(vAbs, vAbs, _MM_SHUFFLE(2,2,2,2));
//   __m128 max3 = _mm_shuffle_ps(vAbs, vAbs, _MM_SHUFFLE(3,3,3,3));
//   max0 = _mm_max_ps(max0, max1);
//   max2 = _mm_max_ps(max2, max3);
//   max0 = _mm_max_ps(max0, max2);
//   return max0;
// }


static void __adjustMatrix_byWeight_optimized(const uint sizeOf_i, const float weight[], float **resultMatrix, const uint nrows, const uint ncols, const bool isTo_useDebugComptuation = false) {
  assert(sizeOf_i > 0);
  //assert(weight);
  
  // { // FIXME: remvoe this block
  // 	      __m128 tmp = _mm_log_ps(_mm_set1_ps(1));
  // }

  assert(resultMatrix);
  const uint SM = 4; //! ie, due to the 16 bytes in "_mm_storeu_ps(..)" and the sizeof(float)==4, ie, 4 'chunks'.
  if(sizeOf_i > SM) {
    float sumOf_weights = sizeOf_i;
    if(weight) {
      uint k = 0; uint numberOf_chunks = 0; uint chunkSize_row_last = sizeOf_i;
      if(sizeOf_i >= SM) {
	// FIXME: in [”elow] why do we need "length-1"?
	__get_updatedNumberOF_lastSizeChunk_entry(/*nrows=*/(uint)sizeOf_i, SM, &numberOf_chunks, &chunkSize_row_last);
	assert(numberOf_chunks != UINT_MAX); assert(numberOf_chunks != 0);
	const uint k_group_size = (chunkSize_row_last == SM) ? numberOf_chunks : numberOf_chunks - 1;	          
	assert(k_group_size > 0); //! ie, what we expect.
	// printf("k=%u, k_group_size=%u, end_pos=%u, length=%d, at %s:%d\n", k, k_group_size, end_pos, length, __FILE__, __LINE__);
	float result[4] = {0, 0, 0, 0}; 
	for(uint k_group = 0; k_group < k_group_size; k += 4, k_group++) {
	  //! then we move the four elements 'forward' with step=1.
	  __m128 term1  = _mm_loadu_ps(&weight[k]);		
	  //__m128 arr_result_tmp = _mm_loadu_ps(&list[k+1]); //! ie, where "+1" is to move aa bload of foru floats forward.
	  //! Load the result-vector
			// FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
	  __m128 arr_result_tmp = _mm_loadu_ps(&result[0]);
	  //! Add the values:
	  arr_result_tmp = _mm_add_ps(term1, arr_result_tmp);
	  // fixme: valdiate orrectenss of [below].
	  //! Store the result:
	  _mm_store_ps(result, arr_result_tmp);
	  //_mm_storeu_ps(&list[k], arr_result_tmp);
	}
	//! Merge the values:
	// FIXME[jc]: do you have a 'scalar operation' for this?
	for(uint i = 0; i < 4; i++) {sumOf_weights += result[i];}
      }
      for(; k < sizeOf_i; k++) {
	const float weight_local = (weight) ? weight[k] : 1;
	sumOf_weights += weight_local;	
      }     
    }
    if(sumOf_weights != 0) { //! Then update the result:
      if(ncols >= SM) {

	// Set to zero when the original length is zero.
	__m128 zero = _mm_setzero_ps();

	// Compute the maximum absolute value component.
	// __m128 maxComponent = MaximumAbsoluteComponent(v);
	// __m128 empty_values = _mm_cmpneq_ps(zero, maxComponent);


	uint numberOf_chunks = 0; uint chunkSize_row_last = ncols;
	__get_updatedNumberOF_lastSizeChunk_entry(/*nrows=*/(uint)ncols, SM, &numberOf_chunks, &chunkSize_row_last);
	const uint k_group_size = (chunkSize_row_last == SM) ? numberOf_chunks : numberOf_chunks - 1;	          
	//! Start the iteraiton:
	float arr_length[4] = {sumOf_weights, sumOf_weights, sumOf_weights, sumOf_weights};
	for(uint index1 = 0; index1 < nrows; index1++) {
	  uint index2 = 0; 


	  //normalized = _mm_and_ps(mask, normalized);
	  for(uint k_group = 0; k_group < k_group_size; index2 += 4, k_group++) {
	    //! Load the data:
	    const __m128 vec_data1 = _mm_loadu_ps(&resultMatrix[index1][index2]);		
	    if(isTo_useDebugComptuation == false) {
	      // FIXME: ask jan-crhistain to update this code-chunk ... and then update the comments in our tetsts.
	      const __m128 vec_empty_1 = _mm_set1_ps(T_FLOAT_MAX);
	      //! Idenitfyt he cases where the values is set to T_FLOAT_MAX:
	      __m128 vec_cmp = _mm_cmpneq_ps(vec_empty_1, vec_data1);              // identify the cases where the vlaeus is not T_FLOAT_MAX    
	      // FIXME: figure out why [”elow] 'caseus' a reverse-order of the array ... and the drop the use of the 'hsuffle' calls.
	      __m128 vec_masked1 = _mm_and_ps(vec_cmp, vec_data1); //! where T_FLOAT_MAX => '0'
	      __m128 vec_masked1_shuffled = _mm_shuffle_ps(vec_masked1, vec_masked1, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
	      //! Intialise the vector to be used dividing the values:
	      const __m128 vec_div = _mm_set1_ps(sumOf_weights);
	      //! Comptue the result:
	      __m128 vec_result = _mm_div_ps(vec_masked1_shuffled, vec_div); //! where we sue a 'reverse-ordered' access.
	      // // FIXME: is [below] correct?
	      // __m128 vec_result_shuffled = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
	      
	      //! FIXME: Manage to 'revert back' T_FLOAT_MAX values. <-- until then we assuem that '0' is an 'accpetable' valeu to use.
	      
	      //! Store the result:
	      _mm_storeu_ps(&resultMatrix[index1][index2], vec_result);
	    } else { //! then we test the effect of storing the values directly
	      //! Note: this 'chunk' is used to test/evlaute the cost of [ªbove] aritcmethics
	      //__m128 vec_data1_reversed = _mm_shuffle_ps(vec_data1, vec_data1, 0x1B);
	      if(true) {
		_mm_storeu_ps(&resultMatrix[index1][index2], vec_data1);
	      } else { //! then we evaluate/test the cost of 'revere sorting'
		//! Note: applicaiton of [”elow] logics increases the time-cost (wrt. above non-reversed option) with a factor of 1.2x.
		__m128 vec_data1_reversed = _mm_shuffle_ps(vec_data1, vec_data1, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
		_mm_storeu_ps(&resultMatrix[index1][index2], vec_data1_reversed);
	      }
	    }
	    //_mm_storeu_ps(&resultMatrix[index1][index2], vec_result_shuffled);
	  }
	  //! Then complete the oepraiton, ie, for those valeus which is not the mulitpler of '4':
	  for(; index2 < ncols; index2++) {
	    float result = resultMatrix[index1][index2];
	    if(result != 0) {result /= sumOf_weights;} //! else the alternative case may be explained by an empty cluster.
	    resultMatrix[index1][index2] = result;
	  }
	}
      } else {
	for(uint index1 = 0; index1 < nrows; index1++) {
	  for(uint index2 = 0; index2 < ncols; index2++) {
	    // FIXME: instead of [”elow] use _mm_storeu_ps .... ite, to dealy the "resultMatrix[index1][index2]" update
	    float result = resultMatrix[index1][index2];
	    if(result != 0) {result /= sumOf_weights;} //! else the alternative case may be explained by an empty cluster.
	    resultMatrix[index1][index2] = result;
	  }
	}
      }
    }
  } else { //! Then we use the 'fallback' prcoedure:
    __adjustMatrix_byWeight_optimized(sizeOf_i, weight, resultMatrix, nrows, ncols);
  }
}

//! Evlauate the perfomrance-difference when using intristinstics to compare two vectors.
static void evaluate_cmpOf_matrix2d_wrt_intrisinitstics()  {
    
  { //! A test-case where we rplace all T_FLOAT_MAX with 0:
    const __m128 vec_empty_1 = _mm_set1_ps(T_FLOAT_MAX);
    const __m128 vec_data1 = _mm_set_ps(T_FLOAT_MAX, 1, T_FLOAT_MAX, 3);                   // d0 = T_FLOAT_MAX;    

    //! Idenitfyt he cases where the values is set to T_FLOAT_MAX:
    __m128 vec_cmp = _mm_cmpneq_ps(vec_empty_1, vec_data1);              // identify the cases where the vlaeus is not T_FLOAT_MAX    
    // FIXME: figure out why [”elow] 'caseus' a reverse-order of the array ... and the drop the use of the 'hsuffle' calls.
    __m128 vec_masked1 = _mm_and_ps(vec_cmp, vec_data1); //! where T_FLOAT_MAX => '0'
    __m128 vec_masked1_shuffled = _mm_shuffle_ps(vec_masked1, vec_masked1, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
    const __m128 vec_empty_2 = _mm_set1_ps(2);
    __m128 vec_result = _mm_div_ps(vec_masked1_shuffled, vec_empty_2); //! where we sue a 'reverse-ordered' access.

    //! 'Revert back' the T_FLOAT_MAX values:
    // __m128 tmp_1 = _mm_andnot_ps(vec_result, vec_empty_1);
    // __m128 tmp_2 = _mm_and_ps(vec_result, d1);
//     __m128 d = _mm_or_ps(d0_masked, d1_masked);       // d = wZ == -1.0 ? 1.0 / (1.0 - wZ) : 1.0 / (1.0 + wZ)
    //vec_result = _mm_blendv_ps(vec_result, vec_data1, vec_empty_1);
    // vec_result = _mm_andnot_ps(vec_result, vec_data1);
    //vec_result = _mm_andnot_ps(vec_data1, vec_result);

    if(true) {
      float result[4] = {0, 0, 0, 0}; 
      //_mm_store_ps(result, vec_cmp); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);}
      _mm_store_ps(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);}
      printf("-----------------***\n\n");
      //_mm_store_ps(result, vec_result_shuffled); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);}
      //_mm_store_ps(result, vec_masked1); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);}
      printf("-----------------***\n\n");
      //printf("d = %f\n", d[0]);
    }    
  }
  
  // { //! Manage to 'revert back' T_FLOAT_MAX values.
  //   assert(false); // FIXME: add support for this.
  // }

  if(false) { //! Test the sue of the "max" calls
    const __m128 vec_data1 = _mm_set_ps(2, 1, 0, 5);                   // d0 = T_FLOAT_MAX;    
    __m128 vec_result = _mm_set_ps(1, 3, 3, 2);                   // d0 = T_FLOAT_MAX;    
    //! ---
    const __m128 vec_max = _mm_max_ps(vec_data1, vec_result); 
    //! Update, ie, revert the result-order:
    vec_result = _mm_shuffle_ps(vec_max, vec_max, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
    //! --
    float result[4] = {0, 0, 0, 0}; 
    //_mm_store_ps(result, vec_cmp); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);}
    _mm_store_ps(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec_max[%u] = %f, ", i, result[i]);}
    printf("-----------------***\n\n");
  }


  { //! Test the signficance of using intristinstics wrt. max-value-computation.
    //! Note: what we test, is the effect of a simple/stratigth-forward vecterization-task, a vectorizaiton which is mainly bound wrt. the the FLOPS.
    //! Result: approx. 3x optimization when using intristinstics: we observe that the sigfiance (of the intristinstics-optimizaiton) incresse with increasing matrix-size, an boservation which may be explained by ...??... <-- FIXME[JC]: have you any expalnations/ideas?
    const uint arrOf_chunkSizes_size = 3; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*kilo, 20*kilo, 30*kilo};
    for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
      uint list_size = arrOf_chunkSizes[chunk_index];
      printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
      //! Construct a sample-list, where we have 'buckets' of equal values:
      float **distmatrix = NULL; char **mask = NULL;
      for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
	//{ const uint factor_rows = 50;
	const uint size_of_array = list_size;
	const uint factor_cols = factor_rows;
	const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;
	printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
	float **distmatrix = NULL; char **mask = NULL;
	const bool isTo_use_continousSTripsOf_memory = true;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	{ //! Use the naive non-otpimized approach:
	  const char *idOf_experiement = "use-intrisnitistcs=false";
	  start_time_measurement();	    
	  const float max_value = get_maxValue__float(nrows, ncols, distmatrix, /*isTo_useIntrisinistic=*/false);
	  //! then generate the results:
	  char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	  sprintf(string, "matrix-get-max-value for [%u][%u] w/traverse-test=%s and max-value=%f", nrows, ncols, idOf_experiement, max_value); 
	  __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	  delete [] string;
	  //__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	}
	{ //! Use the naive non-otpimized approach:
	  const char *idOf_experiement = "use-intrisnitistcs=true";
	  start_time_measurement();	    
	  const float max_value = get_maxValue__float(nrows, ncols, distmatrix, /*isTo_useIntrisinistic=*/true);
	  //! then generate the results:
	  char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	  sprintf(string, "matrix-get-max-value for [%u][%u] w/traverse-test=%s and max-value=%f", nrows, ncols, idOf_experiement, max_value); 
	  __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	  delete [] string;
	  //__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	}

	//! De-allcoate the locally reserved memory:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);	
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);	
      }
    }

  }

  //assert(false); // FIXME: remvoe

  { //! Test the signficance of using intristinstics wrt. transpsoed comptuation.
    //! Note: what we test, is the effect of using 'cosneuqative stores' on the left-side 'operation' while non-cosneuqative 'loads' on the right-side 'memory-fetch'.
    //! Result: 1.09x optimization when using intristinstics.
    const uint arrOf_chunkSizes_size = 3; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*kilo, 20*kilo, 30*kilo};
    for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
      uint list_size = arrOf_chunkSizes[chunk_index];
      printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
      //! Construct a sample-list, where we have 'buckets' of equal values:
      float **distmatrix = NULL; char **mask = NULL;
      for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
	//{ const uint factor_rows = 50;
	const uint size_of_array = list_size;
	const uint factor_cols = factor_rows;
	const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;
	printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
	float **distmatrix = NULL; char **mask = NULL;
	const bool isTo_use_continousSTripsOf_memory = true;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	{ //! Use the naive non-otpimized approach:
	  const char *idOf_experiement = "use-intrisnitistcs=false";
	  start_time_measurement();	    
	  matrix__transpose__compute_transposedMatrix_float(nrows, ncols, distmatrix, result, /*isTo_useIntrisinistic=*/false);
	  //! then generate the results:
	  char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	  sprintf(string, "matrix-transpose for [%u][%u] w/traverse-test=%s", nrows, ncols, idOf_experiement); 
	  __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	  delete [] string;
	  //__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	}
	{ //! Use the naive non-otpimized approach:
	  const char *idOf_experiement = "use-intrisnitistcs=true";
	  start_time_measurement();	    
	  matrix__transpose__compute_transposedMatrix_float(nrows, ncols, distmatrix, result, /*isTo_useIntrisinistic=*/true);
	  //! then generate the results:
	  char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	  sprintf(string, "matrix-transpose for [%u][%u] w/traverse-test=%s", nrows, ncols, idOf_experiement); 
	  __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement); delete [] string;
	}

	//! De-allcoate the locally reserved memory:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);	
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);	
      }
    }

  }

  //assert(false); // FIXME: remvoe


  const uint arrOf_chunkSizes_size = 3; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*kilo, 20*kilo, 30*kilo};
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    uint list_size = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
    //! Construct a sample-list, where we have 'buckets' of equal values:
    float **distmatrix = NULL; char **mask = NULL;
    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
      //{ const uint factor_rows = 50;
      const uint size_of_array = list_size;
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;
      printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
      float **distmatrix = NULL; char **mask = NULL;
      const bool isTo_use_continousSTripsOf_memory = true;
      maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  



      for(uint isTo_useWeight = 0; isTo_useWeight < 2; isTo_useWeight++) {
	float *arrOf_weight = NULL;
	if(isTo_useWeight) {arrOf_weight = new float[ncols]; for(uint i = 0; i < ncols; i++) {arrOf_weight[i] = rand();}}
	{ //! Test the case where weighting is not used.
	  //! Note(1): the optimized funciton produces results aprpx. 1.4x faster than the 'naive' implementation.
	  //! Note(2): the cost of using the 'iline' aritmetic oeprations increases the time-cost by a factor of 2.2x <-- FIXME: ask jhan-christain to try to optimize this code.
	  //! Note(3): the 'intristinstics optimizations' which we describe/evlautet eh effects of (in thsi cotnext) may be seen as an advanced application (of itnrisntitcs), ie, where we use a set of 'complex' vector-oeprations to increase the performance of the operation.
	  // FIXME[article]: .. . udpate wrt. [ªbove] "note(1)" and "note(2)" and "note(3)"
	  { //! Test for 'naive':
	    const char *idOf_experiement = "use-intrisnitistcs=false";
	    start_time_measurement();	    
	    //! The call:
	    __adjustMatrix_byWeight(ncols, arrOf_weight, distmatrix, nrows, ncols);
	    //! then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "matrix-intrisiinistcs wrt. adjustByWeight for [%u][%u] w/traverse-test=%s and use-weight=%s", nrows, ncols, idOf_experiement, (isTo_useWeight) ? "yes" : "no"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	    delete [] string;
	    //__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	  }
	  { //! Test for 'optimized':
	    const char *idOf_experiement = "use-intrisnitistcs=true";
	    start_time_measurement();	    
	    __adjustMatrix_byWeight_optimized(ncols, arrOf_weight, distmatrix, nrows, ncols, /*isTo_useDebugComptuation=*/false);
	    //! then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "matrix-intrisiinistcs wrt. adjustByWeight for [%u][%u] w/traverse-test=%s and use-weight=%s", nrows, ncols, idOf_experiement, (isTo_useWeight) ? "yes" : "no"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	    delete [] string;
	    //__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	  }
	  { //! Evaluate the [ªbove] cost assicated to teh vector-comptuation:
	    const char *idOf_experiement = "use-intrisnitistcs=true discard-artimetic-complexitites=true";
	    start_time_measurement();	    
	    __adjustMatrix_byWeight_optimized(ncols, arrOf_weight, distmatrix, nrows, ncols, /*isTo_useDebugComptuation=*/true);
	    //! then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "matrix-intrisiinistcs wrt. adjustByWeight for [%u][%u] w/traverse-test=%s and use-weight=%s", nrows, ncols, idOf_experiement, (isTo_useWeight) ? "yes" : "no"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	    delete [] string;
	    //__assertClass_generateResultsOf_timeMeasurements("test-data-structure-linear:ideal-alt2", size_of_array, prev_timeOf_measurement);
	  }
	  printf("------------------\n");
	}
	if(arrOf_weight) {delete [] arrOf_weight;}
      }      
     
      printf("# De-allcoates reserved memory, at %s:%d\n", __FILE__, __LINE__);
      maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);
    }
  }
  
  // assert(false); // FIXME: add something.
}



//! Evlauate the perofrmance-cost of calling "log(..)" and "log(2)" functions.
//! Note: results indicates that performance-cost of the log-aritmetics increases the exeuction-tiem-cost by a factor in range [7.3 ... 10.5]. Of interest in this context si to observew that for increasing memory-access-chunks the relative cost of the "log(..)" call is decreasing, ie, 7.3x perofmrance-optim for 40,000,000 sequential emmorya-ccdcesses, while <-- FIXME: the latter could be due to in-accuracies in our tiem-measurements ... of importance is to note/rmemeber the high cost of the "log(..)" arimetic operations.
static void evalaute_timeOf_log_implementiaotns() {

  const uint arrOf_chunkSizes_size = 6; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {100*kilo, kilo*kilo, 5*kilo*kilo, 10*kilo*kilo, 20*kilo*kilo, 40*kilo*kilo};
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    uint list_size = arrOf_chunkSizes[chunk_index];
    float *list = allocate_1d_list_float(list_size, 0);
    for(uint i = 0; i < list_size; i++) {list[i] = rand();}
    { //! A 'base' aritmetic comparison:
      const char *idOf_experiement = "simple-memory-access";
      start_time_measurement(); 
      uint sum = 0; 
      for(uint i = 0; i < list_size; i++) {
	sum += list[i];
      }
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "log-comparison-evaluation-%u w/traverse-test=%s", list_size, idOf_experiement); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;	  
    }
    { //! A 'Default' comparison wrt. applicaiton of "log(..)"
      const char *idOf_experiement = "log-1x";
      start_time_measurement(); 
      uint sum = 0; 
      for(uint i = 0; i < list_size; i++) {
	sum += log(list[i]);
      }
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "log-comparison-evaluation-%u w/traverse-test=%s", list_size, idOf_experiement); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;	  
    }
    { //! A 'Default' comparison wrt. applicaiton of "log(..) & branch"
      const char *idOf_experiement = "log-2x";
      start_time_measurement(); 
      uint sum = 0; 
      for(uint i = 0; i < list_size; i++) {
	sum += log(list[i]) /log(2);
      }
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "log-comparison-evaluation-%u w/traverse-test=%s", list_size, idOf_experiement); 
      __assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
      delete [] string; string = NULL;	  
    }

    { //! Investigate the perofmrance-ebenefit of suing 'intermediate buckets':
      //! Note: seems like this approach produces results approx. 7x faster than the 'log(..)' call, though could be due to parital sequentiality in memory-accesses, ie, as the 'modulo' ("%") used below is the best of many poor strategies'. However, for index-based pre-comptuation of logaritms we assume [”elow] is represnsetive.
      const uint arrOf_precomputedValues_size = 4;  const uint max_cnt_elements = (uint)pow((float)10, (float)arrOf_precomputedValues_size); 
      float *arrOf_precomputedValues = allocate_1d_list_float(max_cnt_elements, 0);
      printf("--------- \n max-allocation=%.2f K, at %s:%d\n", (float)(max_cnt_elements)/kilo*kilo, __FILE__, __LINE__);
      for(uint i = 1; i < max_cnt_elements; i++) {arrOf_precomputedValues[i] = log(i);} //! ie, pre-computes the set of elements.

      for(uint bucket_index = 0; bucket_index < arrOf_precomputedValues_size; bucket_index++) {
	const uint local_max_cnt_elements = (uint)pow((float)10, (float)bucket_index); 

	const char *idOf_experiement = "pre-computed buckets";
	start_time_measurement(); 
	//! -------------------------------
	uint sum = 0; 
	for(uint i = 0; i < list_size; i++) {
	  uint test_index = i % local_max_cnt_elements;
	  sum += arrOf_precomputedValues[test_index];
	}
	//! Then generate the results:
	char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	sprintf(string, "log-comparison-evaluation-%u w/traverse-test=%s and bucket_index=%u", list_size, idOf_experiement, bucket_index); 
	__assertClass_generateResultsOf_timeMeasurements(string, list_size, prev_timeOf_measurement);
	delete [] string; string = NULL;	  
      }
      free_1d_list_float(&arrOf_precomputedValues);
    }



    { //! A sytnatcic example:
      const float min_range = 10; const float max_range = 100; const float cnt_buckets = 10; const float cnt_buckets_inverted = 1/cnt_buckets;
      //! ---- 
      float val1 = 55.5;
      //! ----
      const float val1_adjusted = val1 - min_range; //! ie, "45.5".
      //! Remove the mantissa:
      const uint val1_bucket_index = (uint)(val1_adjusted * cnt_buckets_inverted); //! ie, "4".
      //! Then access the pre-comptued structure: ... 


      // FIXME: write an itnri-funciton of ª[bvoe] ... where we support .... 0.001 ... 0.100 ... ie, to 'get the remainder' <-- "VECTOR_FLOAT_MUL(vec_data1, vec_cntZerosMinVal_sq)" , eg, "*100,00" for "[0.01, 0]" and "*0.1" for "[10, 100]" <-- seems wrong, ie, as ...??...
    }



    // FIXME: write an itnri-funciton of ª[bvoe] ... where we ... 


    // FIXME: simialr to ª[bove] ... though ... 'before measuremnet starts' (eg, generated using a macro at compile-time)
    // FIXME: simialr to ª[bove] ... though ... and this time for indexes ... eg, "for (i=2; i<=x; i++) { ... log(i) .. } "


    // FIXME: write intrisntictc cases for [ªbove] ... 



    

    free_1d_list_float(&list);
  }
  
  // assert(false); // FIXME: move [ªbove] into a to-be-wrttien "assert_s_log_approx.cxx"

}








//! end-new

static void __measure_2d_maskComparison() {
  const uint arrOf_chunkSizes_size = 6; 
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint base_size =  VECTOR_INT_ITER_SIZE_short;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      100*base_size,     
							kilo*base_size,    5*kilo*base_size, 
							10*kilo*base_size, 50*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };


  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    
    const t_float default_value_float = 0;     const char default_value_char = 0;
    t_float **matrix   = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
    char **matrix_char = allocate_2d_list_char(size_of_array, size_of_array, default_value_char);
    { const char *stringOf_measureText = "2d-matrix-loop::implict-masks";
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The operation to measure:
      const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, NULL, NULL, /*transpose=*/0);
      //! Complete:
      t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
      prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference.
    }
/*     { const char *stringOf_measureText = "2d-matrix-loop::implict-masks::slow"; */
/*       //! Start tiem-emasuremetns: */
/*       start_time_measurement(); */
/*       //! The operation to measure: */
/* #define __localConfig_use_slow */
/*       const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, NULL, NULL, /\*transpose=*\/0); */
/* #undef __localConfig_use_slow */
/*       //! Complete: */
/*       t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement); */
/*       prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference. */
/*     } */
    { const char *stringOf_measureText = "2d-matrix-loop::implict-masks::transposed";
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The operation to measure:
      const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, NULL, NULL, /*transpose=*/0);
      //! Complete:
      t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
      prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference.
    }
/*     { const char *stringOf_measureText = "2d-matrix-loop::implict-masks::slow::transposed"; */
/*       //! Start tiem-emasuremetns: */
/*       start_time_measurement(); */
/*       //! The operation to measure: */
/* #define __localConfig_use_slow */
/*       const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, NULL, NULL, /\*transpose=*\/1); */
/* #undef __localConfig_use_slow */
/*       //! Complete: */
/*       t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement); */
/*       prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference. */
/*     } */
    //! ---------------------------------: char-mask:
    { char *stringOf_measureText = "2d-matrix-loop::explicit-masks";
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The operation to measure:
      const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, matrix_char, matrix_char, /*transpose=*/0);
      //! Complete:
      t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
      prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference.
    }
/*     { const char *stringOf_measureText = "2d-matrix-loop::explicit-masks::slow"; */
/*       //! Start tiem-emasuremetns: */
/*       start_time_measurement(); */
/*       //! The operation to measure: */
/* #define __localConfig_use_slow */
/*       const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, matrix_char, matrix_char, /\*transpose=*\/0); */
/* #undef __localConfig_use_slow */
/*       //! Complete: */
/*       t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement); */
/*       prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference. */
/*     } */
    { const char *stringOf_measureText = "2d-matrix-loop::explicit-masks::transposed";
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The operation to measure:
      const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, matrix_char, matrix_char, /*transpose=*/0);
      //! Complete:
      t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
      prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference.
    }
/*     { const char *stringOf_measureText = "2d-matrix-loop::explicit-masks::slow::transposed"; */
/*       //! Start tiem-emasuremetns: */
/*       start_time_measurement(); */
/*       //! The operation to measure: */
/* #define __localConfig_use_slow */
/*       const bool result = ktMask__vectorsAre_consistentlyInteresting(size_of_array, size_of_array, matrix, matrix, matrix_char, matrix_char, /\*transpose=*\/1); */
/* #undef __localConfig_use_slow */
/*       //! Complete: */
/*       t_float relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement); */
/*       prev_result_ofTimeMEasurement = relative_tickTime; //! ie, a reference. */
/*     } */

    //! ---------------------------------
    //!
    //! De-allocate lcoally reserved meory:
    free_2d_list_float(&matrix, size_of_array);
    free_2d_list_char(&matrix_char, size_of_array);
  }
}

/* //! Ends the time measurement for the array: */
/* static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) { */
/*   assert(stringOf_measureText); */
/*   assert(strlen(stringOf_measureText)); */
/*   //! Get knowledge of the memory-intiaition/allocation pattern: */
/*   { */
/*     //! Provides an identficator for the string: */
/*     char *string = new char[1000]; assert(string); memset(string, '\0', 1000); */
/*     sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);   */
/*     //! Prints the data: */
/*     //! Complte the measurement: */
/*     const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds); */
/*     //time.print_formatted_result(string);   */
/*     delete [] string; string = NULL;  */
    
/*     return time_in_seconds; */
/*   }  */
/* } */


static void test_imapctOf_transpose() {
  //! Speicy the matrix-sizes:
  const uint arrOf_cnt_buckets_size = 10; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 
												 2, 3, 4, 5, 5, 7, 8, 9, 10,
												  //11, 12, 13, 14, 15
												  // 80, 160
												  //, 320, 740, 1000
  };
  const uint size_each_bucket = VECTOR_CHAR_ITER_SIZE * 100;
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_cnt_buckets_size, default_value_float);


  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
    const uint ncols_adjusted = size_each_bucket;   const uint nrows_adjusted = size_of_array;
    const t_float default_value_float = 50; const uint default_value_uint = 50; const char default_value_char = 50;
    t_float **matrix_float = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
    t_float **matrix_float_2 = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
    uint **matrix_uint = allocate_2d_list_uint(nrows_adjusted, ncols_adjusted, default_value_uint);
    char **matrix_char = allocate_2d_list_char(nrows_adjusted, ncols_adjusted, default_value_char);
    char **matrix_char_2 = allocate_2d_list_char(nrows_adjusted, ncols_adjusted, default_value_char);
    t_float **matrix_result_float = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float);
    uint **matrix_result_uint = allocate_2d_list_uint(nrows_adjusted, ncols_adjusted, default_value_uint);
    char **matrix_result_char = allocate_2d_list_char(nrows_adjusted, ncols_adjusted, default_value_char);
    t_float *arrOf_weight = allocate_1d_list_float(ncols_adjusted, default_value_float);
    //! --------------------------------------------------------------------------------------------------

#include "measure_externalCxx__nipals__measure__tanspose.c" //! ie, perofmrn perofmrance-tests for 'transpose'.
#include "measure_externalCxx__nipals__measure__iterationOverheadDifferentIterationTypes.c"
#include "measure_externalCxx__nipals__measure__maskConversion.c" //! ie, perofmrn perofmrance-tests for "mask_api.c"
#include "measure_externalCxx__nipals__measure__minMaxLoops.c"
#include "measure_externalCxx__nipals__measure__overheadIfClauses.c"
    

    // { const char *stringOf_measureText = "";
    //   //! -------------------------------
    //   //! Start the clock:
    //   start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //   //!
    //   // 
    //   assert(false); // FIXME: complete [ªbove]
    //   //! Update the result-container:
    //   __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    // }
    // { const char *stringOf_measureText = "";
    //   //! -------------------------------
    //   //! Start the clock:
    //   start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //   //!
    //   // 
    //   assert(false); // FIXME: complete [ªbove]
    //   //! Update the result-container:
    //   __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
    // }
    //! ---------



    //...

    //! --------- new measurement:


    //! --------------------------------------------------------------------------------------------------
    //! De-allcoate:
    free_2d_list_float(&matrix_float, nrows_adjusted);
    free_2d_list_float(&matrix_float_2, nrows_adjusted);
    free_2d_list_uint(&matrix_uint, nrows_adjusted);
    free_2d_list_char(&matrix_char, nrows_adjusted);
    free_2d_list_char(&matrix_char_2, nrows_adjusted);
    free_2d_list_float(&matrix_result_float, nrows_adjusted);
    free_2d_list_uint(&matrix_result_uint, nrows_adjusted);
    free_2d_list_char(&matrix_result_char, nrows_adjusted);
    free_1d_list_float(&arrOf_weight);
  }
  //!
  //!  De-allocate locally reserved memory
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}

//! Investigate the time-effects of buffer-overhead assicated to "uint" VS "char".
static void test_imapctOf_transpose__uintVSChar() {
  // FIXME: updarte doccuemtnation wrt. resutls of this evaluation.
  const uint size_of_array = 10000;
  const uint nrows = size_of_array; const uint ncolumns = size_of_array;
  { //! Evaluate time-overhead assicated to uint w/size = 4 Bytes
    char **mask = new char*[nrows]; mask[0] = new char[nrows * ncolumns]; 
    for(uint transpose = 0; transpose < 2; transpose++) {
      class measure time = measure(); time.start_time_measurement();	    
      float sum = 0;
      for(uint i = 0; i < nrows; i++) {
	for(uint k = 0; k < ncolumns; k++) {
	  if(transpose) {sum += mask[k][i];}
	  else {sum += mask[i][k];}
	}
      }
      if(transpose == false) {
	__assertClass_generateResultsOf_timeMeasurements("transpose-matrix::input-matrix-transposed=false, element-overhead::Bytes=1", nrows, ncolumns); //UINT_MAX, transpose, time, NULL);
      } else {
	__assertClass_generateResultsOf_timeMeasurements("transpose-matrix::input-matrix-transposed=true, element-overhead::Bytes=1", nrows, ncolumns); //UINT_MAX, transpose, time, NULL);
      }
    }
    //! De-allocate locally reseerved memory:
    delete [] mask[0]; delete [] mask;
  }
  { //! Evaluate time-overhead assicated to uint w/size = 4 Bytes
    uint **mask = new uint*[nrows]; mask[0] = new uint[nrows * ncolumns]; 
    for(uint transpose = 0; transpose < 2; transpose++) {
      class measure time = measure(); time.start_time_measurement();	    
      float sum = 0;
      for(uint i = 0; i < nrows; i++) {
	for(uint k = 0; k < ncolumns; k++) {
	  if(transpose) {sum += mask[k][i];}
	  else {sum += mask[i][k];}
	}
      }
      if(transpose == false) {
	__assertClass_generateResultsOf_timeMeasurements("element-overhead::Bytes=4", nrows, ncolumns); //UINT_MAX, transpose, time, NULL);
      } else {
	__assertClass_generateResultsOf_timeMeasurements("transpoe-matrix for tranpose-input-matrix=true, element-overhead::Bytes=4", nrows, ncolumns); //UINT_MAX, transpose, time, NULL);
      }
    }
    //! De-allocate locally reseerved memory:
    delete [] mask[0]; delete [] mask;
  }
  { //! Evaluate time-overhead assicated to uint w/size = 8 Bytes: describes the 'effect' of using "float" instead of "double" wrt. exeuciton-time-difference.
    double **mask = new double*[nrows]; mask[0] = new double[nrows * ncolumns]; 
    for(uint transpose = 0; transpose < 2; transpose++) {
      class measure time = measure(); time.start_time_measurement();	    
      float sum = 0;
      for(uint i = 0; i < nrows; i++) {
	for(uint k = 0; k < ncolumns; k++) {
	  if(transpose) {sum += mask[k][i];}
	  else {sum += mask[i][k];}
	}
      }
      if(transpose == false) {
	__assertClass_generateResultsOf_timeMeasurements("transpose-matrix: element-overhead::Bytes=8", ncolumns, nrows); //UINT_MAX, transpose, time, NULL);
      } else {
	__assertClass_generateResultsOf_timeMeasurements("transpose-matrix transpose-input-matrix=true: element-overhead::Bytes=8", ncolumns, nrows); //UINT_MAX, transpose, time, NULL);
      }
    }
    //! De-allocate locally reseerved memory:
    delete [] mask[0]; delete [] mask;
  }
}

/**
   @remarks in this function we evaluate/compare/assses the following applications:
   case(a): complex-input AND explict-mask: "VECTOR_FLOAT_AND(vec_mask_1, vec_mask_2);" <-- move this into a new macro ... and 'similariy' wrtite/update a new 'count' macro-option.
   case(b): complex-input AND implicit-mask: ... consider to use ....  
   case(c): filtering of NAN-values: VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_result)
   case(d): ... support counting for the 'mask-vectors' ... and thereafter add a enw macro-arpemter for thsi.
   ---------
   case(e)  ... write performance-comparisosn for different maskign-strategies ... inlcude 'this' into a new 'compiled file' ... compare differnet SSE maksign-strategies and different non-SSE maksing stratgeis ... inlcuding the 'no-mask' case.
... : in below we use CMPEQ with a letency of "3" instead of AND with a tlency of "1" this in order to get the NAN-vlaeus, NAN-valeus which are of itnerest wrt. merging of markss and vectors: the use of "CMP" (instead of AND) 'enalbes' the use of the 'AND' operator 8instead of the muliplcaiton-operator) for which we observe a perfomrance-differnce of ....??... 
.... .... in comparsion of mask-comptuation wrt. explicti masks .... wrt. our "VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(..)" macro (found in our "def_typeOf_float.h" header-file) ... which we in our peformacne-emasuremetns compare with our "VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(..)"  to demonstrate th improatnce of 'atomic' optmizaiton of operaitons: the 'slow-perofmrance-implenmtnation' demonstrate ana latenrive (and ion many cotnects mroe obvious/intutive) appraoch for application of mask-proeprties. 

 **/
void measure_mask(const int array_cnt, char **array) {


  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, 


  if(isTo_processAll || (0 == strncmp("2d-mask-comparison", array[2], strlen("2d-mask-comparison"))) ) {
    __measure_2d_maskComparison();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("transpose", array[2], strlen("transpose"))) ) {
    test_imapctOf_transpose();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("transpose_uintVSChar", array[2], strlen("transpose_uintVSChar"))) ) {
    test_imapctOf_transpose__uintVSChar();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_clusterMedian", array[2], strlen("memoryLookUp_clusterMedian"))) ) {
    memoryLookUp_clusterMedian();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_bytesOverhead", array[2], strlen("memoryLookUp_bytesOverhead"))) ) {
    evaluate_listAccess_and_assicated_structureOVerehead();
    cnt_Tests_evalauted++;
  /* } else if(isTo_processAll || (0 == strncmp("memoryLookUp_accessOrder", array[2], strlen("memoryLookUp_accessOrder"))) ) { */
    //evaluate_memoryAccessOrder_generalized();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_overhead2dMatrixOverhead", array[2], strlen("memoryLookUp_overhead2dMatrixOverhead"))) ) {
    evaluate_memoryAccessOrder_capture_2d_matrixAccess();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_evaluateIf_preSortingMayReduceExecTimeByFewerMemoryCacheMisses", array[2], strlen("memoryLookUp_evaluateIf_preSortingMayReduceExecTimeByFewerMemoryCacheMisses"))) ) {
    evaluate_memoryAccessOrder_effectOf_sortingToReduceMemoryCacheMisses();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_costOf_recurringMemoryAllocations", array[2], strlen("memoryLookUp_costOf_recurringMemoryAllocations"))) ) {
    evaluate_costOf_memoryAllocations();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_tilingEffects_intro", array[2], strlen("memoryLookUp_tilingEffects_intro"))) ) {
    evaluate_effectsOf_tiling();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_intro_SSE_twoVectors", array[2], strlen("memoryLookUp_intro_SSE_twoVectors"))) ) {
    evaluate_cmpOf_two_vectors_wrt_intrisinitstics();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("memoryLookUp_intro_SSE_2dMatrix", array[2], strlen("memoryLookUp_intro_SSE_2dMatrix"))) ) {
    evaluate_cmpOf_matrix2d_wrt_intrisinitstics();
    cnt_Tests_evalauted++;
  /* } else if(isTo_processAll || (0 == strncmp("memoryLookUp_semantic_costOf_randomSetAccess", array[2], strlen("memoryLookUp_semantic_costOf_randomSetAccess"))) ) { */
  /*   evaluate_graphIteration_semantics_costOf_randomSetAccess(); */
  /*   cnt_Tests_evalauted++; */
  } else if(isTo_processAll || (0 == strncmp("airthmeticOverhead_costOf_logTransformation", array[2], strlen("airthmeticOverhead_costOf_logTransformation"))) ) {
    evalaute_timeOf_log_implementiaotns();
    cnt_Tests_evalauted++;
  /* } else if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) { */

  /*   cnt_Tests_evalauted++; */
  /* } else if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) { */

  /*   cnt_Tests_evalauted++; */
  }





  if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }


  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }

  assert(false); // FIXME: ask JC to describe ... "_MM_SET_FLUSH_ZERO_MODE" 
  assert(false); // FIXME: add seomthing.


  assert(false); // FIXME: udpate our "clusterC_cmp.html" based on [abov€] results ... and include 'the java-doc.-section-dsiucsison in this'.

}
