#include "matrix_transpose.h"


// //! Store a value without loading a cache line, ie, use intrinsic functions:
// static inline void StoreNTD(t_float* dest, __m128 const & source) {
//   // FIXME: ask jc to validate [below]
//   _mm_stream_ps((t_float*)dest, *(__m128*)&source); // MOVNTQ
//   _mm_empty(); // EMMS
// }

void matrix__transpose__compute_transposedMatrix_float(const uint nrows, const uint ncolumns, t_float **mul1, t_float **res, const bool isTo_useIntrisinistic) {
  // FIXME: write benchmark-tests for this funciotn
  assert(mul1);
  assert(res);
  // FIXME: consider to add an option to use the same amtrix as inptu as output.  
  //printf("(transpose-matrix::float)\t at %s:%d\n", __FILE__, __LINE__);

  //! Iterate:
  // Note: in [”elow] we assume continus memory-allcoation-rotuiens, ie, to 'increment' in the memory.
  // printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
  const uint size_innerOptimized = (isTo_useIntrisinistic && (nrows > VECTOR_FLOAT_ITER_SIZE) && (ncolumns > VECTOR_FLOAT_ITER_SIZE) ) ? nrows - VECTOR_FLOAT_ITER_SIZE : 0;
  
  for (uint i = 0; i < ncolumns; i += 1) { //SM) {
    //assert(mul1[i]);
    // const uint chunkSize_row = (cnt_i++ < numberOf_chunks_rows) ? SM : chunkSize_row_last;
    // uint cnt_j = 0; 
    uint j = 0;
    t_float *__restrict__ row = res[i];
    assert(res);
    if(size_innerOptimized > 0) {
      for (; j < size_innerOptimized; j += VECTOR_FLOAT_ITER_SIZE) {
	//! Store a value without loading a cache line, ie, use intrinsic functions:
	const VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_setFrom_columns(mul1, j, i);
	//assert(j < nrows);
	VECTOR_FLOAT_STOREU(&row[j], vec_value);	
	//VECTOR_FLOAT_STORE(&row[j], vec_value);	
      }
    } 
    //! Compelte the update:
    for (; j < nrows; j += 1) { // SM) {
      assert_possibleOverhead(mul1[j]);
      res[i][j] = mul1[j][i];
      //      res[j][i] = mul1[i][j];
    }
  }
}

void matrix__transpose__compute_transposedMatrix_uint(const uint nrows, const uint ncolumns, uint **mul1, uint **res, const bool isTo_useIntrisinistic) {
  const uint size_innerOptimized = (isTo_useIntrisinistic) ? VECTOR_UINT_maxLenght_forLoop(nrows) : 0;  
  for (uint i = 0; i < ncolumns; i += 1) { //SM) {
    uint j = 0;
    if(size_innerOptimized > 0) {
      for (; j < size_innerOptimized; j += VECTOR_UINT_ITER_SIZE) { // SM) {
	VECTOR_UINT_STOREU(&res[i][j], VECTOR_UINT_setFrom_columns(mul1, j, i)); //(mul1[j+0][i], mul1[j+1][i], mul1[j+2][i], mul1[j+3][i]);
      }
    } 
    //! Compelte the update:
    for (; j < nrows; j += 1) { // SM) {
      res[i][j] = mul1[j][i];
      //      res[j][i] = mul1[i][j];
    }
  }
}


void matrix__transpose__compute_transposedMatrix_char(const uint nrows, const uint ncolumns, char **mul1, char **res, const bool isTo_useIntrisinistic) {
  // FIXME: write benchmark-tests for this funciotn

  // FIXME: consider to add an option to use the same amtrix as inptu as output.  

  //! Iterate:
  // Note: in [”elow] we assume continus memory-allcoation-rotuiens, ie, to 'increment' in the memory.
  // printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
  const uint size_innerOptimized = (isTo_useIntrisinistic && (nrows > VECTOR_CHAR_ITER_SIZE)) ? nrows - VECTOR_CHAR_ITER_SIZE : 0;
  
  for (uint i = 0; i < ncolumns; i += 1) { //SM) {
    uint j = 0;
    if(size_innerOptimized > 0) {
      for (; j < size_innerOptimized; j += VECTOR_CHAR_ITER_SIZE) { // SM) {
	// assert(j < nrows); 	assert((j+4) < nrows);
	//! Store a value without loading a cache line, ie, use intrinsic functions:
	// FIXME[jc]: would it go faster if we used _mm_load1_ps(..) .... and is there any other magics/trickery? .... and what is the difference wrt. _mm_stream_ps(..) and "_mm_store_ss(..)" ??
	__m128i vec_tmp = VECTOR_CHAR_setFrom_rows(mul1, /*dynamic=*/j, /*fixed=*/i);
	//_mm_store_ps(vec_tmp, vec_tmp);
	// FIXME: include [”elow]       
	// FIXME[jc]: ask jan-chrsitan wrt. the difference between "_mm_store_ps(..)" and "_mm_storeu_ps(..)" .... ie, the implciaton/'requiremnt' of "mem_addr must be aligned on a 16-byte boundary or a general-protection exception may be generated" ["https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=10,13,3152,583,0,0,5126,5174&text=_mm_store"] ... and why the latter error is thrown in valgrind ... <--- any stratgies to 'oversecome this'?

	// assert(false); // FIXME[jc]: may you identify which 'fucntion-call' to use  ... and then incldue the proerp one [below]?
	_mm_storeu_si128((__m128i*)&(res[i][j]), vec_tmp);

      }
    } 
    //! Compelte the update:
    for (; j < nrows; j += 1) { // SM) {
      res[i][j] = mul1[j][i];
      //      res[j][i] = mul1[i][j];
    }
  }
}


/** 
    @brief Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    @param <data> is the input-meatrix to transpose.
    @param <nrows> is the number of rows in data, ie, the number of columns in resultMatrix.
    @param <ncolumns> is the number of columns in data, ie, the number of rows in resultMatrix
    @param <resultMatrix> is the matrix which 'contain' the inverse data-set.
    @param <resultMatrix_mask> is the char-matrix which 'contain' the inverse data-set: only updated if data is set.
    @param <isTo_useIntrisinistic> which if set to false implies that we use default C lanauge-specific operations: mainly included for benchmarking.
**/
void matrix__transpose__compute_transposedMatrix(const uint nrows, const uint ncolumns, t_float **mul1, char **mask1, t_float **res, char **res_mask, const bool isTo_useIntrisinistic) {
  //! What we expect:
    assert(nrows > 0);
  assert(ncolumns > 0);
  assert(mul1);
  assert(res);
  
  //printf("(transpose-matrix)\t at %s:%d\n", __FILE__, __LINE__);

  if(mul1) {
    matrix__transpose__compute_transposedMatrix_float(nrows, ncolumns, mul1, res, isTo_useIntrisinistic);
  }
  if(mask1) {
    matrix__transpose__compute_transposedMatrix_char(nrows, ncolumns, mask1, res_mask, isTo_useIntrisinistic);
  }
}

//! @return a enw-allcoated transpoed row from an input-matrix (oekseth, 06. setp. 2016)
t_float *get_transposed_row_fromMatrixFloat__matrix_transpose(const uint index1, const uint nrows, const uint ncols, t_float **data) {
  assert(data); assert(index1 < ncols); assert(nrows > 0);
  const t_float default_value_float = 0;
  t_float *row = allocate_1d_list_float(nrows, default_value_float);
  //printf("(info)\t allocates a new row w/size=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
  for(uint i = 0; i < nrows; i++) {
    row[i] = data[i][index1];
  }
  //! @return:
  return row;
}
