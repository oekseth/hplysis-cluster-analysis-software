#include "tut_wrapper_emd.h"
#include "kt_aux_matrix_blur.h"
#include "external_emd.h"

static t_float func_external_dist(feature_t *F1, feature_t *F2) { return *F1 - *F2;} //_COST[*F1][*F2]; }

//! The main function for logics associated to "image segmentation" and the "earth mover distance" (EMD) (oekseth, 06. feb. 2018).
void tut_wrapper_emd(const int array_cnt, char **array) {
  { //! Test an impmetanion ofsen used by otehrs when applying/using the "EMD":
    {
      feature_t   f1[5] = { 0, 1, 2, 3, 4 };
      feature_t   f2[3] = { 0, 1, 2 };
      float       w1[5] = { 0.4, 0.2, 0.2, 0.1, 0.1 };
      float       w2[3] = { 0.6, 0.2, 0.1 };
      //! --------------- 
      signature_t s1 = { 5, f1, w1};
      signature_t s2 = { 3, f2, w2};
      float       e;
      flow_t      flow[7];
      int         i, flowSize;
      
      e = external_emd(&s1, &s2, func_external_dist, flow, &flowSize);
      
      printf("emd=%f\n", e);
      printf("\nflow:\n");
      if(false) {
	printf("from\tto\tamount\n");
	for (i=0; i < 7; i++)
	  if (flow[i].amount > 0)
	    printf("%d\t%d\t%f\t at %s:%d\n\n", flow[i].from, flow[i].to, flow[i].amount, __FILE__, __LINE__);
      }
    }
    {
      feature_t   f1[5] = { 0, 1, 2, 3, 4 };
      feature_t   f2[5] = { 0, 1, 2, 3, 4 };
      float       w1[5] = { 0.4, 0.2, 0.2, 0.1, 0.1 };
      float       w2[5] = { 0.6, 0.2, 0.1, -0.3, 0.6 };
      //! --------------- 
      signature_t s1 = { 5, f1, w1};
      signature_t s2 = { 5, f2, w2};
      float       e;
      flow_t      flow[7];
      int         i, flowSize;
      
      e = external_emd(&s1, &s2, func_external_dist, flow, &flowSize);
      
      printf("emd=%f\n", e);
      printf("\nflow:\n");
      if(false) {
	printf("from\tto\tamount\n");
	for (i=0; i < 7; i++)
	  if (flow[i].amount > 0)
	    printf("%d\t%d\t%f\t at %s:%d\n\n", flow[i].from, flow[i].to, flow[i].amount, __FILE__, __LINE__);
      }
    }
    { //! Fixed: "feature":
      feature_t   f1[5] = { 0, 1, 1, 1, 1 };
      feature_t   f2[5] = { 0, 1, 1, 1, 1 };
      float       w1[5] = { 0.4, 0.2, 0.2, 0.1, 0.1 };
      float       w2[5] = { 0.6, 0.2, 0.1, 0.2, 0.1};
      //! --------------- 
      signature_t s1 = { 5, f1, w1};
      signature_t s2 = { 5, f2, w2};
      float       e;
      flow_t      flow[7];
      int         i, flowSize;
      
      e = external_emd(&s1, &s2, func_external_dist, flow, &flowSize);
      
      printf("emd=%f\n", e);
      printf("\nflow:\n");
      printf("(fixed=feature):\tfrom\tto\tamount\n");
      if(false) {
	for (i=0; i < 7; i++)
	  if (flow[i].amount > 0)
	    printf("%d\t%d\t%f\t at %s:%d\n\n", flow[i].from, flow[i].to, flow[i].amount, __FILE__, __LINE__);
      }
    }

    { //! Fixed: "weight", increment features
      feature_t   f1[5] = { 0, 1, 2, 3, 4 };
      feature_t   f2[5] = { 0, 1, 2, 3, 4 };
      float       w1[5] = { 0.4, 0.2, 0.2, 0.1, 0.1 };
      float       w2[5] = { 0.6, 0.2, 0.1, 0.2, 0.1};
      //! --------------- 
      signature_t s1 = { 5, f1, w1};
      signature_t s2 = { 5, f2, w2};
      float       e;
      flow_t      flow[7];
      int         i, flowSize;
      
      e = external_emd(&s1, &s2, func_external_dist, flow, &flowSize);
      
      printf("emd=%f\n", e);
      printf("\nflow:\n");
      printf("(fixed=weight, increment=feature):\tfrom\tto\tamount\n");
      if(false) {
	for (i=0; i < 7; i++)
	  if (flow[i].amount > 0)
	    printf("%d\t%d\t%f\t at %s:%d\n\n", flow[i].from, flow[i].to, flow[i].amount, __FILE__, __LINE__);
      }
    }
    { //! Fixed: "features", increment features
      feature_t   f1[5] = { 100, 12, 42, 13, 24 };
      feature_t   f2[5] = { 10, 1, 2, 3, 4 };
      float       w1[5] = { 1, 1, 1, 1, 1};
      float       w2[5] = { 1, 1, 1, 1, 1};
      //! --------------- 
      signature_t s1 = { 5, f1, w1};
      signature_t s2 = { 5, f2, w2};
      float       e;
      flow_t      flow[7];
      int         i, flowSize;
      
      e = external_emd(&s1, &s2, func_external_dist, flow, &flowSize);
      
      printf("emd=%f\n", e);
      printf("\nflow:\n");
      printf("(incrementing=weights):\tfrom\tto\tamount\n");
      if(false) {
	for (i=0; i < 7; i++)
	  if (flow[i].amount > 0)
	    printf("%d\t%d\t%f\t at %s:%d\n\n", flow[i].from, flow[i].to, flow[i].amount, __FILE__, __LINE__);
      }
    }
  }
  assert(false); // FIXME: complete   

}
