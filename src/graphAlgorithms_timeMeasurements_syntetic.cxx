#include "graphAlgorithms_timeMeasurements_syntetic.h"

#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"
#ifdef __SSE4_1__
#include <smmintrin.h>
#endif



#include "measure.h"
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#include <omp.h> // for parallisation using OpenMP
#endif
#include "graphAlgorithms_aux.h"
#include "stdint.h" //! provides support for "uint_fast_64_t"

// //! Note: this funciton is used to 'only' iterate throug half of the items in a [][] matrix.
// static void __get_updatedNumberOF_lastSizeChunk_entry(const uint nrows, const uint SM, uint &numberOf_chunks, uint &chunkSize_row_last) {
//   assert(nrows >= SM);
//   //! Avoid overflwo in the last chunks of cahce-buffers:
//   numberOf_chunks = nrows / SM;
//   // FIXME: vliadte correctness of [”elow] new-added 'fix'.
//   //if(numberOf_chunks == 0) {numberOf_chunks = 1;}
//   //assert(numberOf_chunks != 0); // FIXME: valdiate this new-added assertion.
//   // 
//   chunkSize_row_last = nrows - (SM*numberOf_chunks);
//   assert( (chunkSize_row_last + SM*numberOf_chunks) == nrows );
//   if(chunkSize_row_last == 0) {chunkSize_row_last = SM;}
//   // printf("nrows=%u, numberOf_chunks=%u=%.4f, SM=%u, at %s:%d\n", nrows, numberOf_chunks, (float)nrows/(float)SM, SM, __FILE__, __LINE__);
//   assert(chunkSize_row_last <= SM);
//   // printf("chunkSize_row_last=%u, sum-prev=%u, at %s:%d\n", chunkSize_row_last, SM*numberOf_chunks, __FILE__, __LINE__);
// }

// //! Identify the last chunk-size for datasets where nrows and ncols are not divisible by SM.
// static void __get_updatedNumberOF_lastSizeChunk(const uint nrows, const uint ncols, const uint SM, uint &chunkSize_row_last, uint &chunkSize_col_last) {
//   //! Avoid overflwo in the last chunks of cahce-buffers:
//   { //! First for the rows:
//     assert(SM > 0); assert(nrows > 0);
//     const uint numberOf_chunks = nrows / SM;
//     // 
//     chunkSize_row_last = nrows - (SM*numberOf_chunks);
//     assert( (chunkSize_row_last + SM*numberOf_chunks) == nrows );
//     if(chunkSize_row_last == 0) {chunkSize_row_last = SM;}
//     assert(chunkSize_row_last <= SM);
//     // printf("nrows=%u, numberOf_chunks=%u=%.4f, SM=%u, at %s:%d\n", nrows, numberOf_chunks, (float)nrows/(float)SM, SM, __FILE__, __LINE__);
//     // printf("chunkSize_row_last=%u, sum-prev=%u, at %s:%d\n", chunkSize_row_last, SM*numberOf_chunks, __FILE__, __LINE__);
//   }
//   { //! Thereafter for the columns:
//     const uint numberOf_chunks = ncols / SM;
//     // const int diff = ncols - (((int)(ncols-1) - (int)numberOf_chunks)); // * (int)SM; //! ie, "(ncols * SM) - (numberOf_chunks * SM)"
//     // assert(diff >= 0);
//     // assert((uint)diff <= SM);
//     chunkSize_col_last = ncols - (SM*numberOf_chunks);
//     if(chunkSize_col_last == 0) {chunkSize_col_last = SM;}
//     // printf("ncols=%u, numberOf_chunks=%u=%.4f, SM=%u, at %s:%d\n", ncols, numberOf_chunks, (float)ncols/(float)SM, SM, __FILE__, __LINE__);
//     assert(chunkSize_col_last <= SM);
//   }	
//   if(false) { //! Validate [abov€]:
//     const uint cnt_elementInMatrix = nrows * ncols * ncols;
//     const uint numberOf_chunks_rows = nrows / SM;
//     const uint numberOf_chunks_cols = ncols / SM;
//     const uint sumOf_beforeLast = ( numberOf_chunks_rows*numberOf_chunks_cols*numberOf_chunks_cols*SM*SM*SM);
//     printf("\t sumOf_beforeLast=%u, cnt_elementInMatrix=%u, chunkSize_row_last=%u, chunkSize_col_last=%u, at %s:%d\n", sumOf_beforeLast, cnt_elementInMatrix, chunkSize_row_last, chunkSize_col_last, __FILE__, __LINE__);
//     assert(cnt_elementInMatrix >= sumOf_beforeLast);
//     const uint remaining = cnt_elementInMatrix - sumOf_beforeLast;
	  
//   }
// }


// static const uint kilo = 1000;
// static clock_t clock_time_start; static clock_t clock_time_end;
// static struct tms tms_start;
// static  struct tms tms_end;
// /**
//    @brief Start the measurement with regard to time.
//    @remarks 
//    - Uses the global vairalbe in this file for this prupose.
//    - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
// **/
// static  void start_time_measurement() {
// #ifndef NDEBUG
//   //! Clears the cache by allocating 100MB of memory- and the de-allocating it:
//   // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses.
//   uint size = 1024*1024*100;
//   char *tmp = new char[size];
//   assert(tmp);
//   memset(tmp, 7, size);
//   delete [] tmp; tmp = NULL; size = 0;
// #endif
//   tms_start = tms();
//   clock_time_start = times(&tms_start);
//   // if(() == -1) // starting values
//   //   err_sys("times error");
// }
// /**
//    @brief Ends the measurement with regard to time.
//    @param <msg> If string is given, print the status information to stdout.
//    @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
//    @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
//    @return the clock tics on the system since the 'start_time_measurement()' method was called.
// **/
// static double end_time_measurement(const char *msg, double &user_time, double &system_time) {
//   clock_time_end = times(&tms_end); 
//   long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
//   const clock_t t_real = clock_time_end - clock_time_start;
//   const double time_in_seconds = t_real/(double)clktck;
//   const double diff_user = tms_end.tms_utime - tms_start.tms_utime;
//   if(diff_user) user_time   = (diff_user)/((double)clktck);
//   const double diff_system = tms_end.tms_stime - tms_start.tms_stime;
//   system_time = diff_system/((double)clktck);
//   if(msg) printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
//   //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
//   return time_in_seconds;
// }


// /**
//    @brief Ends the measurement with regard to time.
//    @return the clock tics on the system since the 'start_time_measurement()' method was called.
// **/
// static double end_time_measurement(const char *msg = NULL) {
//   double user_time = 0, system_time=0;
//   return end_time_measurement(msg, user_time, system_time);
// }





//! ***********************************************************************************************************
//! ***********************************************************************************************************
//! ***********************************************************************************************************

static void evaluate_artimetic_2d_memoryLoopUp() {

}




void graphAlgorithms_timeMeasurementsExternalTools_syntetic::main() {

  // FIXME: udnerstna the following code and applciation:     if (_mm_movemask_epi8 (_mm_castps_si128(mask)) == 0xfff0)

  // FIXME: test perofmranc eof "https://github.com/zerovm/zerovm-samples/blob/master/disort/src/bitonic_sort.c"

  // FIXME: update our "mine.c" wrt. [above] intrisntistcs code/resutls ... and thereafter update 'base iteration-fucntions' in our "cluster.c"
  // FIXME: try to identify a usage wrt. [”elow] 'memset' routine.
  /**
void* memset(void *ptr, int value, size_t num)
{
const char *cur = ptr;
... align pointer on 16 bytes ...
__m128i mm_value = _mm_set1_epi8(value);
for (size_t i = 0; i < num; i += 16) {
_mm_stream_si128((__m128i*)cur, mm_value);    
cur += 16;
};
_mm_sfence();
... process the remainder ...
}


   **/
  // FIXME: ... try to make use of the inistintics in the staticstical lbirary ... testing the latter ...  ... and then update functiosn such as "StoreNTD(..)" and "addindex(..)" and "testEffectOf_exactInstructionsInMemoryAccess_2d_loop(..)" ... in 'this' and "graphAlgorithms_distance.cxx"


  //   //! Evlauate the perofrmance-cost of calling "log(..)" and "log(2)" functions.
  //   evalaute_timeOf_log_implementiaotns();
  // assert(false); // FIXME: remvoe

  // //! Approximate the time-costs assicated to graph-iterations.
  // evaluate_graphIteration_semantics_costOf_randomSetAccess();

  // assert(false); // FIXME: remvoe



  //   //! Evalaute/assess teh signficnace/improtance of using intrisitnstic optimzilation in matrix-access.
  // evaluate_cmpOf_matrix2d_wrt_intrisinitstics();
  // assert(false); // FIXME: remove


  // //! Evlauate the perfomrance-difference when using intristinstics to compare two vectors.
  // evaluate_cmpOf_two_vectors_wrt_intrisinitstics();
  // assert(false); // FIXME: remove




  // evaluate_effectsOf_tiling();
  // assert(false);


  // //! Compare the signfiance/improtance of using 3-for-loops instead of 2 for-loops:
  // evaluate_artimetic_2d_memoryLoopUp();

  // assert(false);


  // { //! Test time-effect of parallism, among other identifying the minimum number of elements/'calculations' in for-loops, eg, wrt. "1d-loops", "2d-loops" and "3d-loops".
  //   // FIXME: update our ciode wrt. [”elow] ... (a) complet ethe benchmarks (nlduign the comptue-matrix-before task); (b) manage to compile our software using "OpenMP" eg, "/usr/local/bin/g++ -fopenmp mxm.cpp -lm" (using our 'approach' in our "server/" directory) ... (and in this cotnext write a new "graphAlgorithms_entropy" to be 'filled' with algorithms used in the "libmine" software); (c) start parallising; (d) update our benchmark-tests with the udpated parallel functions, eg, to identify the 'minimum work-size needed for each thread'.
  //   // FIXME: when this evaluation/benchmark is completed, then update our code in 'this' and in "mime.c"
  //   // FIXME: if [ªbove] is 'stasifiable' ... then consider to  compute the distance-matrix using parallel tbb computation ... ie, to get a higher speed-up.
  //   // FIXME: add tests for "1d" loops.
  //   // FIXME: add tests for "2d" loops.
  //   // FIXME: add tests for "3d" loops.
    
  //   { //! Test the effect on sorting:
  //     // FIXME: use the different values/combination in the "e_distance_rank_typeOf_computation" foudn in our "graphAlgorithms_distance.h"
  //     // FIXME: based on [below] resutls ... udpate our  "maskAllocate__sort_indexes(..)" and the quicksort(..) call in our udpated "mine.c"
  //     { //! Use our rank-implemetnaitons to test the effect/importance of 'list-wrappers'.
  // 	// FIXME: add something
  // 	// FIXME: call graphAlgorithms_distance::get_rank_wCppOverhead(..)", which is used to test the effect/improtance of using tempalte-listwrappers in function-calls.
  // 	// FIXME: call graphAlgorithms_distance::get_rank(..)"
  //     }
  //     { //! Dvide the wquicksort-algorithm into k chunks, and then 'merge' the chunks after the operation.
	
  //     }
  //     { //! Test the performacne-optimization of "c_qsort.c"

  //     }
  //     { //! Update the quicksort algorithm itself
  // 	// FIXME: consider to inlcude "http://stackoverflow.com/questions/16007640/openmp-parallel-quicksort"
  //     }
  //     { //! Update our merge-sort algorithm

  //     }

  //   }
  // }



  // // FIXME: test the buffer-sizes wrt. parallisaiton of 'cases where the inner chunks are equal in complexity'.  


  // // FIXME: ... in 2d-lists ... test the signfiance of intrisitntcs ...  wrt. memory-access ... 

  // //! new test: ------------------------------------------------------------------------------------------      




  //! new test: ------------------------------------------------------------------------------------------      

  //! Evaluate assicated strucutre-overhead assicated to list-accesses:
  //  evaluate_listAccess_and_assicated_structureOVerehead();

  {
    // FIXME: include .. test ... and udpate [”elow] funciton wr.t their results.
    // evaluate_memoryAccessOrder_generalized();
    // evaluate_memoryAccessOrder_capture_2d_matrixAccess();
    // evaluate_memoryAccessOrder_effectOf_sortingToReduceMemoryCacheMisses();
    // evaluate_costOf_memoryAllocations();
  }

  //! new test: ------------------------------------------------------------------------------------------

#if 0 == 1
  { //! Test the importanct/'impact' of memory-re-use.    
    { //! Test the overhead in time-cost of 'allcoating new memory' for N indices VS 'only to reset the memory'.
      //! Note: is an 'occuring'/'important' part of both the "mime" library and the "C cluster library" 
      //! Note: we compare the access of 'atomic' types VS access of 'safe wrappers': 'access to a list of floats' VS 'access to a list offloats warpped/accessed in a C++ object'.
      // FIXME: for [”elow] update teh 'resutl-test' with 'what we observe/lkearn'.
      const uint arrOf_chunkSizes_size = 5; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {kilo, 10*kilo, 100*kilo, kilo*kilo, 100*kilo*kilo};
  
      

      for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
	const uint list_size = arrOf_chunkSizes[chunk_index];

	//! Investigate if the artimetic operations may 'hide' the cost of memory-allcoation, ie, through the machine-hardwares implicat 'underlying' parallisation:	
	const uint arrOf_innerChunkSizes_size = 5; const uint arrOf_innerChunkSizes[arrOf_innerChunkSizes_size] = {0, kilo, 10*kilo, 100*kilo, kilo*kilo};
	for(uint inner_chunkSize_index = 0; inner_chunkSize_index < arrOf_innerChunkSizes_size; inner_chunkSize_index++) {	  
	  const uint list_size_artimetics = arrOf_innerChunkSizes[inner_chunkSize_index];

	  //! new test: ------------------------------------------------------------------------------------------
      

	  //! new test: ------------------------------------------------------------------------------------------
	  { //! Test the overehead of using muliple calls to allcoate one chunk of memory:
	    // FIXME: for [”elow] update teh 'resutl-test' with 'what we observe/lkearn'.
	    const uint total_size = list_size * list_size_artimetics;	    
	    if(total_size < (2*kilo*kilo*100)) {
	      //! Evalaute cases both wrt. 'ideal' memory-allcoation and 'non-ideal' memory-allocation:
	      for(uint isTo_use_continousSTripsOf_memory = 0; isTo_use_continousSTripsOf_memory < 2; isTo_use_continousSTripsOf_memory++) {
		//! Allocate memory:
		float **distmatrix = NULL; char **mask = NULL;
		//! Start the time-measurement:
		start_time_measurement(); 
		//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
		maskAllocate__makedatamask(/*nrows=*/list_size, /*ncols=*/list_size_artimetics, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
		//! Then de-allocate:
		maskAllocate__freedatamask(/*nelements=*/list_size, distmatrix, mask, isTo_use_continousSTripsOf_memory);

		//! Then generate the results:
		char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
		sprintf(string, "memoryAccess::allocate-deAllocate-2d-%u=[%u][%u]:%s", total_size, list_size, list_size_artimetics, (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
		__assertClass_generateResultsOf_timeMeasurements(string, list_size, /*measure_linear=*/NULL);
		delete [] string; string = NULL;
	      }

	    } //! else we assume taht the 'memory-overehad' will be 'too much'
	  } 
	  //! new test: ------------------------------------------------------------------------------------------

	  //! Test different memory-allcoation schemes:
	  // FIXME: for [”elow] update teh 'resutl-test' with 'what we observe/lkearn'.
	  enum {
	    allocation_cpp, allocation_cpp_andMemset, allocation_cpp_tryCatch_andMemset, allocation_c, allocation_c_andMemset, 
	    //! and then the counts of the [ªbove] enum-alternatives:
	    allocation_undef
	  };
	  for(uint allocation_id = 0; allocation_id < allocation_undef; allocation_id++) {	    
	    //! Start the time-measurement:
	    start_time_measurement(); 
	    //! Allocate:
	    float *data = NULL;
	    if(allocation_id == allocation_cpp) {data = new float[list_size]; }
	    else if(allocation_id == allocation_cpp_andMemset) {data = new float[list_size]; memset(data, 0, list_size*sizeof(float));}
	    else if(allocation_id == allocation_cpp_tryCatch_andMemset) {data = list_generic<float>::allocate_list(list_size, 0, __FILE__, __LINE__);}
	    else if(allocation_id == allocation_c) {data = (float*)malloc(list_size * sizeof(float));}
	    else if(allocation_id == allocation_cpp_tryCatch_andMemset) {data = (float*)malloc(list_size * sizeof(float)); memset(data, 0, list_size*sizeof(float));}
	    else {assert(false);} //! ie, as we then need to add support for this case.

	    //! ITerations to evaluate the 'effect' of 'underlyiong' hardware-appalcaiton:
	    float sum = 0;
	    for(uint k = 0; k < list_size_artimetics; k++) {
	      sum += k;
	    }

	    const char *idOf_experiement= NULL;
	    //! de-allocate:
	    if(allocation_id == allocation_cpp) {delete [] data; idOf_experiement = macro_getVarName(allocation_cpp);}
	    else if(allocation_id == allocation_cpp_andMemset) {delete [] data; idOf_experiement = macro_getVarName(allocation_cpp_andMemset);}
	    else if(allocation_id == allocation_cpp_tryCatch_andMemset) {delete [] data; idOf_experiement = macro_getVarName(allocation_cpp_tryCatch_andMemset);}
	    else if(allocation_id == allocation_c) {free(data); idOf_experiement = macro_getVarName(allocation_c);}
	    else if(allocation_id == allocation_cpp_tryCatch_andMemset) {free(data); idOf_experiement = macro_getVarName(allocation_c_andMemset);}
	    else {assert(false);} //! ie, as we then need to add support for this case.


	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    sprintf(string, "memoryAccess::allocate-deAllocate-2d-%u w/artimetic-cnt=%u and allocation-type=%s", list_size, list_size_artimetics, idOf_experiement); 
	    __assertClass_generateResultsOf_timeMeasurements(string, list_size, /*measure_linear=*/NULL);
	    delete [] string; string = NULL;
	  }
	}
      }
    }
  }
#endif
  


  if(true) {
    
    // FIXME: move [below] to our new test-class .. and then comapre 'this' with Perl and other progamming-alnagues ... eg, "performance_time_synteticExamples.pl"
    uint size = 1000*1000*1000;
    uint sum = 0;
    for(uint i = 0; i < size; i++) {
      sum += i;
    }
    printf("sum=%u, at %s:%d\n", sum, __FILE__, __LINE__);
    // assert(false); //! ie, to stop the time-emasurement
  }

}
