#include "measure_kt_matrix_filter.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "kt_forest_findDisjoint.h"
#include "kt_matrix_filter.h"
#include "math_generateDistribution.h"
#include "correlation_macros__distanceMeasures.h"
static  const uint arrOf_chunkSizes_size = 6;
//! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
static  const uint base_size =  VECTOR_INT_ITER_SIZE_short;
static  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      
							      100*base_size,
							      kilo*base_size, 
							      2*kilo*base_size,
							      3*kilo*base_size, 4*kilo*base_size,
							      //80*kilo*base_size, 160*kilo*base_size,
							      //300*kilo*base_size, 600*kilo*base_size,
};
static const uint cnt_row_fractions = 10;
static const t_float mapOf_fraction[cnt_row_fractions] = {
  0.125, 0.25, 0.5, 0.75, 1.0, 
  1.125, 1.25, 1.5, 1.75, 2.0, 
};

//! Idneitfy the perofmrance-penelaties wrt. our 'clases' when comapred to (a) linked-lsits and (b) correlation-comptuation from dense matrices.
static void filter_adjustValue() {
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_chunkSizes_size, default_value_float);

  //! -------------------------------
  //! Perform muliple iteraitons 
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint ncols = arrOf_chunkSizes[chunk_index];
    for(uint row_fraction = 0; row_fraction < cnt_row_fractions; row_fraction++) {
      const uint nrows = macro_max((uint)(t_float)(VECTOR_FLOAT_ITER_SIZE*((ncols * mapOf_fraction[row_fraction]/VECTOR_FLOAT_ITER_SIZE))), VECTOR_FLOAT_ITER_SIZE*4);
      //const uint nrows = ncols;
      const uint size_of_array = ncols;
      t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
      //! Generate the valeus using a unifrom fistrubiton:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  matrix[row_id][col_id] = math_generateDistribution__uniform();
	}
      }
      //! ------------------------------------- 
      { //! Test for 'cell-mask' row-filtering:
	//! ------------------------------------- 
	{ //! Test for 'all-out' row-filtering:
	  const uint stringOf_enums_size = 5;
	  assert(stringOf_enums_size == e_kt_matrix_filter_adjustValues_undef);
	  const char *stringOf_enums[stringOf_enums_size] = {
	    "logarithm(..)",
	    "subtract-abs(rows)",
	    "subtract-abs(cols)",
	    "feature-scaling(rows)",
	    "feature-scaling(cols)",
	  };
	  const e_kt_matrix_filter_adjustValues_t mapOf_enums[stringOf_enums_size] = {
	    e_kt_matrix_filter_adjustValues_log,
	    e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows,
	    e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_cols,
	    e_kt_matrix_filter_adjustValues_center_normalization_rows,
	    e_kt_matrix_filter_adjustValues_center_normalization_cols
	  };
	  const uint stringOf_typeOf_adjust_size = 4;
	  assert( (stringOf_typeOf_adjust_size-1) == (uint)e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none);
	  const char *stringOf_typeOf_adjust[stringOf_typeOf_adjust_size] = {
	    "median",
	    "mean",
	    "STD",
	    "none",
	  };
	  const e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t mapOf_typeOf_adjust[stringOf_typeOf_adjust_size] = {
	    e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median,
	    e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean,
	    e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD,
	    e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none
	  };
	  for(uint enum_id = 0; enum_id < stringOf_enums_size; enum_id++) {
	    for(uint adjust_id = 0; adjust_id < stringOf_typeOf_adjust_size; adjust_id++) {
	      const e_kt_matrix_filter_adjustValues_t enum_type = mapOf_enums[enum_id];
	      const e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t typeOf_adjustment = mapOf_typeOf_adjust[adjust_id];
	      if( (typeOf_adjustment != e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none) &&
		  ( (enum_type == e_kt_matrix_filter_adjustValues_log) || (enum_type == e_kt_matrix_filter_adjustValues_center_normalization_rows) || (enum_type == e_kt_matrix_filter_adjustValues_center_normalization_cols) ) ) {continue;} //! ie, as we expect ath 'our' functions will not make use of the 'extrac' ocnfiguraiton-otpiostns assciated to "adjust_id"
	      char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
	      sprintf(stringOf_measureText, "matrix-adjustment-filter \"%s\" and adjustment-type=\"%s\" for a data-set with a distribution=\"uniform\"", stringOf_enums[enum_id], stringOf_typeOf_adjust[adjust_id]);
	      const char *stringOf_measureText_base = stringOf_measureText;
	      //const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_type = (e_kt_matrix_filter_orderOfFilters_eitherOr_t)enum_id;
	      //! Allocate the object:
	      s_kt_matrix_filter self = init_setDefaultValues__s_kt_matrix_filter(matrix, nrows, ncols);
	      self.typeOf_adjust_subtractOrAdd_rows = typeOf_adjustment; 
	      self.typeOf_adjust_subtractOrAdd_cols = typeOf_adjustment;
	      //! -------------------------------
	      //! Start the clock:
	      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	      //! The operation:
	      adjustValue__s_kt_matrix_filter_t(&self, enum_type);

	      //! Write out the exeuciotn-time:
	      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	
	      free__s_kt_matrix_filter_t(&self);
	      /* 	    { //! Identify disjtoint-networks, both wrt. exeuciton-time and dsitjoint-forest-count: */
	      /* 	      uint cntOf_interest = 0; uint cnt_notOfItnerest = 0; */
	      /* 	      { //! First vlaidate taht tehre exists valeus which are of interest and not-of-interest: */
	      /* 		for(uint row_id = 0; row_id < nrows; row_id++) { for(uint col_id = 0; col_id < ncols; col_id++) { if(isOf_interest(matrix[row_id][col_id])) {cntOf_interest++;} else {cnt_notOfItnerest++;} } } */
	      /* 	      } */
	      /* 	      if(!cnt_notOfItnerest || !cntOf_interest) { */
	      /* 		printf("(info)\t the filter-ops of %s did not result in any 'hidden' regions, ie, cntOf_interest=%u, and cnt_notOfItnerest=%u, ie, no point in disjoint-forest-computation: for details see [%s]:%s:%d\n", stringOf_enums[enum_id], cntOf_interest, cnt_notOfItnerest, __FUNCTION__, __FILE__, __LINE__); */
	      /* 		// assert(false); // FIXME: remove */
	      /* 	      } else { */
	      /* #include "measure_func__disjointForest.c" */
	      /* 	      } */
	      /* 	      //assert(false); // FIXME: .... add tests for different implemetnaiton-strategies ...  */

	      /* 	    } */
	    }
	  }
	}
      }
      //! ----------------------------
      //! De-allcoate meory:
      free_2d_list_float(&matrix, nrows);
    }
  }
  //! ----------------------------
  //! De-allcoate meory:
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}


//! Idneitfy the perofmrance-penelaties wrt. our 'clases' when comapred to (a) linked-lsits and (b) correlation-comptuation from dense matrices.
static void filter_cellMask() {
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_chunkSizes_size, default_value_float);

  //! -------------------------------
  //! Perform muliple iteraitons 
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint ncols = arrOf_chunkSizes[chunk_index];
    for(uint row_fraction = 0; row_fraction < cnt_row_fractions; row_fraction++) {
      const uint nrows = macro_max((uint)(t_float)(VECTOR_FLOAT_ITER_SIZE*((ncols * mapOf_fraction[row_fraction]/VECTOR_FLOAT_ITER_SIZE))), VECTOR_FLOAT_ITER_SIZE*4);
      const uint size_of_array = ncols;
      t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
      //! Generate the valeus using a unifrom fistrubiton:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  matrix[row_id][col_id] = math_generateDistribution__uniform();
	}
      }
      //! ------------------------------------- 
      { //! Test for 'cell-mask' row-filtering:
	//! ------------------------------------- 
	{ //! Test for 'all-out' row-filtering:
	  const uint stringOf_enums_size = 3;
	  assert(stringOf_enums_size == e_kt_matrix_filter_orderOfFilters_cellMask_undef);
	  const char *stringOf_enums[stringOf_enums_size] = {
	    "min-max",
	    "in-range-mean-percent",
	    "in-range-median-percent",
	  };
	  const e_kt_matrix_filter_orderOfFilters_cellMask_t mapOf_enums[stringOf_enums_size] = {
	    e_kt_matrix_filter_orderOfFilters_cellMask_minMax,
	    e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent,
	    e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent,
	  };
	  for(uint enum_id = 0; enum_id < stringOf_enums_size; enum_id++) {
	    char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
	    sprintf(stringOf_measureText, "cell-mask matrix-filter \"%s\" for a data-set with a distribution=\"uniform\"", stringOf_enums[enum_id]);
	    const char *stringOf_measureText_base = stringOf_measureText;
	    const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_type = mapOf_enums[enum_id];
	    //const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_type = (e_kt_matrix_filter_orderOfFilters_eitherOr_t)enum_id;
	    //! Allocate the object:
	    s_kt_matrix_filter self = init_setDefaultValues__s_kt_matrix_filter(matrix, nrows, ncols);
	    //! -------------------------------
	    //! Start the clock:
	    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	    //! The operation:
	    applyFilter__cellMask__s_kt_matrix_filter_t(&self, enum_type);

	    //! Write out the exeuciotn-time:
	    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	
	    free__s_kt_matrix_filter_t(&self);
	    { //! Identify disjtoint-networks, both wrt. exeuciton-time and dsitjoint-forest-count:
	      uint cntOf_interest = 0; uint cnt_notOfItnerest = 0;
	      { //! First vlaidate taht tehre exists valeus which are of interest and not-of-interest:
		for(uint row_id = 0; row_id < nrows; row_id++) { for(uint col_id = 0; col_id < ncols; col_id++) { if(isOf_interest(matrix[row_id][col_id])) {cntOf_interest++;} else {cnt_notOfItnerest++;} } }
	      }
	      if(!cnt_notOfItnerest || !cntOf_interest) {
		printf("(info)\t the filter-ops of %s did not result in any 'hidden' regions, ie, cntOf_interest=%u, and cnt_notOfItnerest=%u, ie, no point in disjoint-forest-computation: for details see [%s]:%s:%d\n", stringOf_enums[enum_id], cntOf_interest, cnt_notOfItnerest, __FUNCTION__, __FILE__, __LINE__);
		// assert(false); // FIXME: remove
	      } else {
#include "measure_func__disjointForest.c"
	      }
	      //assert(false); // FIXME: .... add tests for different implemetnaiton-strategies ... 

	    }
	  }
	}
      }
      //! ----------------------------
      //! De-allcoate meory:
      free_2d_list_float(&matrix, nrows);
    }
  }
  //! ----------------------------
  //! De-allcoate meory:
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}

//! Idneitfy the perofmrance-penelaties wrt. our 'clases' when comapred to (a) linked-lsits and (b) correlation-comptuation from dense matrices.
static void filter_eitherOr() {
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_chunkSizes_size, default_value_float);

  //! -------------------------------
  //! Perform muliple iteraitons 
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint ncols = arrOf_chunkSizes[chunk_index];
    for(uint row_fraction = 0; row_fraction < cnt_row_fractions; row_fraction++) {
      const uint nrows = macro_max((uint)(t_float)(VECTOR_FLOAT_ITER_SIZE*((ncols * mapOf_fraction[row_fraction]/VECTOR_FLOAT_ITER_SIZE))), VECTOR_FLOAT_ITER_SIZE*4);
      //const uint nrows = ncols;
      const uint size_of_array = ncols;
      t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
      //! Generate the valeus using a unifrom fistrubiton:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  matrix[row_id][col_id] = math_generateDistribution__uniform();
	}
      }
      //! ------------------------------------- 
      { //! Test for 'all-out' row-filtering:
	const uint stringOf_enums_size = 6;
	assert(stringOf_enums_size == e_kt_matrix_filter_orderOfFilters_eitherOr_undef);
	const char *stringOf_enums[stringOf_enums_size] = {
	  "absolute-value above-threshold",
	  "percent present",
	  "min-max diff",
	  "Gaussian STD",
	  "kurtosis",
	  "skewness"
	};
	const e_kt_matrix_filter_orderOfFilters_eitherOr_t mapOf_enums[stringOf_enums_size] = {
	  e_kt_matrix_filter_orderOfFilters_eitherOr_absoluteValueAboveThreshold,
	  e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent,
	  e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff,
	  e_kt_matrix_filter_orderOfFilters_eitherOr_STD,
	  e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis,
	  e_kt_matrix_filter_orderOfFilters_eitherOr_skewness,
	};
	for(uint enum_id = 0; enum_id < stringOf_enums_size; enum_id++) {
	  char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
	  sprintf(stringOf_measureText, "either-or matrix-filter \"%s\" for a data-set with a distribution=\"uniform\"", stringOf_enums[enum_id]);
	  const char *stringOf_measureText_base = stringOf_measureText;
	  const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_type = mapOf_enums[enum_id];
	  //const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_type = (e_kt_matrix_filter_orderOfFilters_eitherOr_t)enum_id;
	  //! Allocate the object:
	  s_kt_matrix_filter self = init_setDefaultValues__s_kt_matrix_filter(matrix, nrows, ncols);
	  //! -------------------------------
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  //! The operation:
	  applyFilter__eitherOr__s_kt_matrix_filter_t(&self, enum_type);

	  //! Write out the exeuciotn-time:
	  __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	
	  free__s_kt_matrix_filter_t(&self);
	  { //! Identify disjtoint-networks, both wrt. exeuciton-time and dsitjoint-forest-count:
	    uint cntOf_interest = 0; uint cnt_notOfItnerest = 0;
	    { //! First vlaidate taht tehre exists valeus which are of interest and not-of-interest:
	      for(uint row_id = 0; row_id < nrows; row_id++) { for(uint col_id = 0; col_id < ncols; col_id++) { if(isOf_interest(matrix[row_id][col_id])) {cntOf_interest++;} else {cnt_notOfItnerest++;} } }
	    }
	    if(!cnt_notOfItnerest || !cntOf_interest) {
	      printf("(info)\t the filter-ops of %s did not result in any 'hidden' regions, ie, cntOf_interest=%u, and cnt_notOfItnerest=%u, ie, no point in disjoint-forest-computation: for details see [%s]:%s:%d\n", stringOf_enums[enum_id], cntOf_interest, cnt_notOfItnerest, __FUNCTION__, __FILE__, __LINE__);
	      // assert(false); // FIXME: remove
	    } else {
#include "measure_func__disjointForest.c"
	    }
	    //assert(false); // FIXME: .... add tests for different implemetnaiton-strategies ... 
	  }
	}
      }
      //! ----------------------------
      //! De-allcoate meory:
      free_2d_list_float(&matrix, nrows);
    }
  }


  //! ----------------------------
  //! De-allcoate meory:
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}


//! The main assert function.
void measure_kt_matrix_filter__main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  if(isTo_processAll || (0 == strncmp("test_correctness", array[2], strlen("test_correctness"))) ) {
    assert(false);
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("filter_adjustValue", array[2], strlen("filter_adjustValue"))) ) {
    filter_adjustValue();
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("filter_cellMask", array[2], strlen("filter_cellMask"))) ) {
    filter_cellMask();
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("filter_eitherOr", array[2], strlen("filter_eitherOr"))) ) {
    filter_eitherOr();
    cnt_Tests_evalauted++;
  }

  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}


