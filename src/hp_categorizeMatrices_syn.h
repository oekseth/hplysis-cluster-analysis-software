#ifndef kt_categorizeMatrices_syn_h
#define hp_categorizeMatrices_syn_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_categorizeMatrices_syn.h
   @brief logics to clasisfy data-sets using syntietc funcitons and data-distruibutions (ie, thence the "syn" suffix) (oekseth, 06. arp. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
   -- Idea. Evlautes closenss to data-matrices using sytneitc funcitosn and dat-adistrubitosn as 'point-of-reference'.
**/

#include "kt_list_1d_string.h"
#include "kt_list_1d.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "parse_main.h"
//#include "c_parse/parse_main.h"
#include "kt_longFilesHandler.h" //!" which is only used implicty, eg, by 'callers which are in need of lon file-anems' (eosketh, 06. des. 2016).
#include "math_generateDistribution.h" //! for ranomness-support (oekseth, 06. jan. 2017)
#include "matrix_deviation.h"
#include "kt_matrix_base.h"
#include "kt_matrix.h"
#include "kt_list_1d.h"
#include "hp_ccm.h"
#include "hp_distance.h"
#include "s_dataStruct_matrix_dense.h"
#include "e_kt_correlationFunction.h"
/**
   @enum e_kt_categorizeMatrices_subCases__cmpFunction
   @brief different fucntiosn to comapre data-set with  (oekseth, 06. arp. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarsk in its 'base-cosntruction' this enum is deidned to be a wrapepr aroudn the hpLysis "s_dataStruct_matrix_dense.h" fucntion-focnsturctor.
 **/
typedef enum e_kt_categorizeMatrices_subCases__cmpFunction {
  e_kt_categorizeMatrices_subCases__cmpFunction_flat,
  e_kt_categorizeMatrices_subCases__cmpFunction_flat_noise_medium,
  e_kt_categorizeMatrices_subCases__cmpFunction_flat_noise_high,
  //! --- 
  e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal,
  e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal_noise_medium,
  e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal_noise_high,
  //! --- 
  e_kt_categorizeMatrices_subCases__cmpFunction_linear_differentCoeff_b,
  e_kt_categorizeMatrices_subCases__cmpFunction_linear_differentCoeff_a,
  e_kt_categorizeMatrices_subCases__cmpFunction_sinus,
  e_kt_categorizeMatrices_subCases__cmpFunction_sinus_curved,
  //e_kt_categorizeMatrices_subCases__cmpFunction_
  e_kt_categorizeMatrices_subCases__cmpFunction_random_1, //! where latter is asusmed to bet the 'legtnh' of the non-random enums.
  e_kt_categorizeMatrices_subCases__cmpFunction_random_2,
  e_kt_categorizeMatrices_subCases__cmpFunction_random_3,
  e_kt_categorizeMatrices_subCases__cmpFunction_uniform_1,
  e_kt_categorizeMatrices_subCases__cmpFunction_uniform_2,
  e_kt_categorizeMatrices_subCases__cmpFunction_uniform_3,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_1,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_2,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_3,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_1,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_2,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_3,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_1,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_2,
  e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_3,
  //e_kt_categorizeMatrices_subCases__cmpFunction_,
  e_kt_categorizeMatrices_subCases__cmpFunction_undef
} e_kt_categorizeMatrices_subCases__cmpFunction_t;

//! @return a shirt 'human-rdable' string-repsritoant of the enum-id in queisont (oekseth, 06. arp. 2017)
static const char *get_shortString__e_kt_categorizeMatrices_subCases__cmpFunction_t(const e_kt_categorizeMatrices_subCases__cmpFunction_t  enum_id) {
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_flat) {return "flat";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_flat_noise_medium)  {return "flat-noise-medium";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_flat_noise_high) {return "flat-noise-high";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal) {return "equal";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal_noise_medium) {return "equal-noise-medium";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal_noise_high) {return "equal-noise-high";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_differentCoeff_b) {return "linear-diffCoeff-b";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_differentCoeff_a) {return "linear-diffCoeff-a";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_sinus) {return "sinus";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_sinus_curved) {return "sinus-curved";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_random_1) {return "random-1";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_random_2) {return "random-2";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_random_3) {return "random-3";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_uniform_1) {return "uniform-1";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_uniform_2) {return "uniform-2";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_uniform_3) {return "uniform-3";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_1) {return "bionomial-05pst-1";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_2) {return "bionomial-05pst-2";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_3) {return "bionomial-05pst-3";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_1) {return "bionomial-5pst-1";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_2) {return "bionomial-5pst-2";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_3) {return "bionomial-5pst-3";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_1) {return "bionomial-20pst-1";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_2) {return "bionomial-20pst-2";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_3) {return "bionomial-20pst-3";}
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_undef) {return "not-defined";}
  { //! then we have Not yet ahdnled the enum, ie, an error 'from oru side':
    fprintf(stderr, "!!\t Enum Not supported, i,e inviestigate, givne enum=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false); // TOD: add supprot for this.
    return "not-defined";
  }
  //if(enum_id == ) {return "";}
}

/**
   @enum e_kt_categorizeMatrices_subCases__hypo 
   @brief   (oekseth, 06. arp. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
 **/
typedef enum e_kt_categorizeMatrices_subCases__hypo {
  e_kt_categorizeMatrices_subCases__hypo_simMetric,
  //e_kt_categorizeMatrices_subCases__hypo_simMetric_andPreStep,
  e_kt_categorizeMatrices_subCases__hypo_simMetric_inCategory_small,
  e_kt_categorizeMatrices_subCases__hypo_simMetric_inCategory_medium,
  e_kt_categorizeMatrices_subCases__hypo_dataId,
  e_kt_categorizeMatrices_subCases__hypo_dataSet,
  //e_kt_categorizeMatrices_subCases__hypo_
  e_kt_categorizeMatrices_subCases__hypo_undef
} e_kt_categorizeMatrices_subCases__hypo_t;



//! @return a shirt 'human-rdable' string-repsritoant of the enum-id in queisont (oekseth, 06. arp. 2017)
static const char *get_shortString__e_kt_categorizeMatrices_subCases__hypo_t(const e_kt_categorizeMatrices_subCases__hypo_t enum_id) {
  if(enum_id == e_kt_categorizeMatrices_subCases__hypo_simMetric) {return "simMetric";}
  if(enum_id == e_kt_categorizeMatrices_subCases__hypo_simMetric_inCategory_small) {return "simMetric-category-small";}
  if(enum_id == e_kt_categorizeMatrices_subCases__hypo_simMetric_inCategory_medium) {return "simMetric-category-medium";}
  if(enum_id == e_kt_categorizeMatrices_subCases__hypo_dataId) {return "data-id";}
  if(enum_id == e_kt_categorizeMatrices_subCases__hypo_dataSet) {return "data-ensamble";}
  if(enum_id == e_kt_categorizeMatrices_subCases__hypo_undef) {return "not-defined";}
  { //! then we have Not yet ahdnled the enum, ie, an error 'from oru side':
    fprintf(stderr, "!!\t Enum Not supported, ie, inviestigate, givne enum=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false); // TOD: add supprot for this.
    return "not-defined";
  }
}

/**
   @enum e_kt_categorizeMatrices_typeOfAdjust_synAndInput
   @brief  configures the type of adjustments to be made to increase the 'fit' between the evlauted data-sets VS our colelciotn of math-fucntiosn and data-distributions (oekseth, 06. arp. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
 **/
typedef enum e_kt_categorizeMatrices_typeOfAdjust_synAndInput {
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_none,
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization,
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_min,
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_avg,
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_max,
  /* e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization_andMulBy_min, */
  /* e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization_andMulBy_max, */
  /* e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization_andMulBy_avg, */
  //e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization_and_,
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_undef //! where use of the latter results in oru appraoch using the expemreintally best-performing cofniguraiton.
} e_kt_categorizeMatrices_typeOfAdjust_synAndInput_t;

/**
   @struct s_hp_categorizeMatrices_syn
   @brief  provides lgocis to catefgorize data-sets based on simliarty to a 'sytnetic' reference-data-set (oekseth, 06. arp. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
 **/
typedef struct s_hp_categorizeMatrices_syn {
  //s_kt_matrix_t features_cmp
  s_kt_list_1d_uint_t listOf_hyp[e_kt_categorizeMatrices_subCases__hypo_undef]; //! which hold diffneret hyptoeses/calddifcioatins asiscated toe ach vetex: used to ivnestigate/assert the best-expalantion/hyptoesis wrt. the dfifneret assumtpsion, eg, to answer the case "are the data-set or algroithm mreot improtynat than simlairty-emtric when comptuing ....??... 
  s_kt_list_1d_float_t listOf_scores__max[e_kt_categorizeMatrices_subCases__cmpFunction_undef];
  //s_kt_list_1d_float_t listOf_scores__simMe[e_kt_categorizeMatrices_subCases__cmpFunction_undef];
  s_kt_list_1d_string_t listOf_names; //! which hold the string-identifes fo the inserted entries
  //  s_kt_list_1d_string_t arrOf_vertex_to_hypothesis
  s_kt_correlationMetric_t sim_toCompare;
  uint current_pos;
  const char *stringOf_resultPath;
  const char *stringOf_resultTag;
  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_t defaultAdjustment_ofSyn;
  t_float defaultAdjustment_ofSyn__mul_even; //! eg, "*1"
  t_float defaultAdjustment_ofSyn__mul_odd; //! eg, *-10"
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function;
  //setToEmpty__s_kt_list_1d_float_t
} s_hp_categorizeMatrices_syn_t;

//! Cosntruct a file holding the data-set which we evlauate/comapre each data-matrix to. 
void static__exportSynteticDAtaMatrix(const uint ncols, const char *fileName_toExportTo, const e_kt_categorizeMatrices_typeOfAdjust_synAndInput defaultAdjustment_ofSyn);

/**
   @brief Itniatelises and returns the "s_hp_categorizeMatrices_syn_t" object.
   @param <sim_toCompare> is the simlairty-emtric to be used as a 'backbone' wrt. our data-comparison rotuines.
   @param <stringOf_resultPath> is the repfix to be seud when cosntructin ghte file-naems wrt. data-export.
   @param <stringOf_resultTag> is the suffix to be appended after the "stringOf_resultPath" to unqiuely idneitfyl fiels generated from this object.
**/
static s_hp_categorizeMatrices_syn_t init__s_hp_categorizeMatrices_syn(s_kt_correlationMetric_t sim_toCompare, const char *stringOf_resultPath, const char *stringOf_resultTag) {
  //const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function) {
  assert(stringOf_resultPath); assert(strlen(stringOf_resultPath));
  s_hp_categorizeMatrices_syn_t self;
  self.current_pos = 0;
  self.stringOf_resultPath = stringOf_resultPath;
  self.stringOf_resultTag = stringOf_resultTag;
  self.defaultAdjustment_ofSyn =  e_kt_categorizeMatrices_typeOfAdjust_synAndInput_undef; //! where use of the latter results in oru appraoch using the expemreintally best-performing cofniguraiton.
  self.defaultAdjustment_ofSyn__mul_even = 1.0; //! eg, "*1"
  self.defaultAdjustment_ofSyn__mul_odd = -20.0; //! eg, *-10"
  //  self.defaultAdjustment_ofSyn__mul_odd = -2.0; //! eg, *-10"
  //printf("stringOf_resultPath_raw=%s\n", stringOf_resultPath);
  self.listOf_names = setToEmpty_andReturn__s_kt_list_1d_string_t();
  //self.typeOf_function = typeOf_function;
  for(uint i = 0; i < e_kt_categorizeMatrices_subCases__cmpFunction_undef; i++) {
    self.listOf_scores__max[i] = setToEmpty__s_kt_list_1d_float_t();
    //self.listOf_scores__simMe[i] = setToEmpty__s_kt_list_1d_float_t();
  }
  for(uint i = 0; i < e_kt_categorizeMatrices_subCases__hypo_undef; i++) {
    self.listOf_hyp[i] = setToEmpty__s_kt_list_1d_uint_t();
  }
  self.sim_toCompare = sim_toCompare; //! ie, the simlairty-emtic to be sued during comptautions.
  //!
  //! @return self:
  return self;
}

//! Trnalstes a collection of 1d-lists into a matrix, and then generate the result-output-file (oekseht, 0+06. apr. 2017).
//! @reutrn the new-cosntructed feautre-matrix. 
s_kt_matrix_t __private__exportMatrix(const s_kt_list_1d_float_t *listOf_scores, const uint listOf_scores_size, const s_kt_list_1d_string_t listOf_names, const char *fileName_toExportTo);

  /**
     @return the local index where the result was indested at.
     @param <self> is the object ot de-allocate.
     @param <isTo_export> optional: if set to true, then we exprot the itnernal set of data.
     @param <arrOf_sim_postProcess> optional if used, then we apply the differnet hyptoesis in our data-set.
     @param <arrOf_sim_postProcess_size> optional: should be set if "arrOf_sim_postProcess" is used; facialtes thte use/elvuaiotn of diffneret sim-emtics wrt. hypoteiss-evlauation.
     @remarks if the arrOf_sim_postProcess option is used:
     -- [simMetric x vertex x hypothesis][CCM-metrics] ... ie, simplifying the comparison for/acorss different hyptoehsis. 
     -- useCase. To grasp/capture difnferet hyptoeshsi, eg, wrt. [sim-metric, sim-metric-group, data-group, random-group, algorithm, algoriuthm-group ]
   **/
void free__s_hp_categorizeMatrices_syn_t(s_hp_categorizeMatrices_syn_t *self, const bool isTo_export, s_kt_correlationMetric_t *arrOf_sim_postProcess, const uint arrOf_sim_postProcess_size);

/**
   @struct s_hp_categorizeMatrices_syn_config_dataObj
   @brief describes the data-set, a descirption used to enumerate an inserted data-set  (oekseth, 06. arp. 2017)
   @remarks 
   -- Idea. To 'add' proeprties wrt. the vertex, proerpteis which are used during the de-allcoaiton-call ("free__s_hp_categorizeMatrices_syn_t(..)") to evlaute different hyptohesis wrt. diferences in data-set-variations.
 **/
typedef struct s_hp_categorizeMatrices_syn_config_dataObj {
  s_kt_correlationMetric_t sim_whichIsApplied;
  //bool sim_whichIsApplied__isToUseInHypEval;
  uint data_id; //! which we use to vinestigate if simlairty-metrics have lower varition wrt. 'ohter emtics in the same data-id/file'.
  uint data_id_category; //! which we use to vinestigate if simlairty-metrics have lower varition wrt. 'ohter emtics in the same data-set'.
} s_hp_categorizeMatrices_syn_config_dataObj_t;

s_hp_categorizeMatrices_syn_config_dataObj_t setToEmtyAndReturn__s_hp_categorizeMatrices_syn_config_dataObj_t() {
 s_hp_categorizeMatrices_syn_config_dataObj_t self;
  self.sim_whichIsApplied = setTo_empty__andReturn__s_kt_correlationMetric_t();
  self.data_id = UINT_MAX;
  self.data_id_category = UINT_MAX;
  //! @return
  return self;
}

//! @return an itnaited verison of the "s_hp_categorizeMatrices_syn_config_dataObj_t" object (oekseth, 06. arp. 2017).
static s_hp_categorizeMatrices_syn_config_dataObj_t init__s_hp_categorizeMatrices_syn_config_dataObj_t(s_kt_correlationMetric_t sim_whichIsApplied, uint data_id, uint data_id_category) {
  s_hp_categorizeMatrices_syn_config_dataObj_t self;
  self.sim_whichIsApplied = sim_whichIsApplied;
  self.data_id = data_id;
  self.data_id_category = data_id_category;
  //! @return
  return self;
}


/**
   @brief add a vertex-data-set to the collection (of catrozied data-sets) (oekseth, 06. arp. 2017).
   @param <self>  is the object to update
   @param <obj_matrix> is the matrix to compare with sytnetaic dta-distributions
   @param <stringOf_entry> is the string-desicptor to assicated with 'this entry' (eg, a composion of cluster-algorithm and data-set-id);
   @param <vertexProp> optional: may be used to 'add' proeprties wrt. the vertex, proerpteis which are used during the de-allcoaiton-call ("free__s_hp_categorizeMatrices_syn_t(..)") to evlaute different hyptohesis wrt. diferences in data-set-variations.
   @return the index where the entity was insrted at.
   @remarks the 'lcoal return-index' may amogn others be used to udpate wrt. differtn 'hyptoeshsi-classies' which the data-entry/vertex may be mmeber of
 **/
uint add_dataSet__s_hp_categorizeMatrices_syn_t(s_hp_categorizeMatrices_syn_t *self, const s_kt_matrix_t *obj_matrix, const char *stringOf_entry, const s_hp_categorizeMatrices_syn_config_dataObj_t vertexProp);


/**
   @struct s_hp_categorizeMatrices_syn__setOf
   @brief  a wrapper to genrlaised input-logics wrt. our "s_hp_categorizeMatrices_syn_t" (oekseth, 06. arp. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
 **/
typedef struct s_hp_categorizeMatrices_syn__setOf {
  s_hp_categorizeMatrices_syn_t obj_rawData;
  s_hp_categorizeMatrices_syn_t obj_afterSim;
} s_hp_categorizeMatrices_syn__setOf_t;

/**
   @brief Itniatelises and returns the "s_hp_categorizeMatrices_syn_t" object.
   @param <stringOf_resultPath> is the repfix to be seud when cosntructin ghte file-naems wrt. data-export.
   @return an intalied object.
**/
static s_hp_categorizeMatrices_syn__setOf_t init__s_hp_categorizeMatrices_syn__setOf(const char *stringOf_resultPath) {
  //! -------------
  s_hp_categorizeMatrices_syn__setOf_t self;
  //! Note[sim-metric--synCompare=MINE::MIC]: in our 'hard-code' soruce-tree an exmapel-resutl wrt. latter is seen for the 'base-systnatic data-set' "results/backup_tut_ccm_13_categorizeData/metric_MINE/_simToSynt__fromDataInput.tsv_xmtRandom.tsv". From the latter we observe that our 'base-sytantic-cofnigruations' resutls in inseeable variabnce betwene the diffenret metrics: in brief our MINE+syn-feature-appraoch are unable to seperate/disntuigsh betwene features.
  //s_kt_correlationMetric_t sim_toCompare = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none);
  //! Note[sim-metric--synCompare=Kendall::Dice]: ... simliar to MINE Kendall does Not result in any sigcinat deivaiton wrt. our 'base-simliarty-emtrics': from our "results/backup_tut_ccm_13_categorizeData/metric_Kendall_Dice/_simToSynt__fromDataInput.tsv_xmtRandom_deviation.tsv"
  s_kt_correlationMetric_t sim_toCompare = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none);
  //! Note[sim-metric--synCompare=Minkowski::Euclid]: .. wrt. the non-random scores, the Euclid was less senstive than both MINE::MIC and Kendall::Dice, an observaiotn which correspons/relfects to our understnaidng: while MIEN is hgily sentive to variatons in data-sets, Euclid (for many cases) are unable to idneitfy minor co-variace (while Kendall-permatusioin are foudn in the 'misslde' wr.t sensitivty when comapred ot siliarty-emtircs in oru evlauaiton-space). 
  //  s_kt_correlationMetric_t sim_toCompare = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none);
  //! Note[sim-metric--synCompare=]: ..
  //s_kt_correlationMetric_t sim_toCompare = initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none);
  //! -------------
  
  //printf("stringOf_resultPath_raw=%s\n", stringOf_resultPath_raw);
  self.obj_rawData = init__s_hp_categorizeMatrices_syn(sim_toCompare, stringOf_resultPath, "raw/");
  //! -------------
  //allocOnStack__char__sprintf(8000, stringOf_resultPath_afterSimMetric, "%s_%s", stringOf_resultPath, "afterSimMetric");
  self.obj_afterSim = init__s_hp_categorizeMatrices_syn(sim_toCompare, stringOf_resultPath, "afterSimMetric/");
  //! -------------
  return self;
}

//! Copmtue results, export, and then de-allocate
static void free__s_hp_categorizeMatrices_syn__setOf_t(s_hp_categorizeMatrices_syn__setOf *self) {
  assert(self);
  const uint arrOf_sim_postProcess_size = 6;
  s_kt_correlationMetric_t arrOf_sim_postProcess[arrOf_sim_postProcess_size] = {
    //! --- 
    initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
    //! --- 
    initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
    initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
    //! --- 
    //initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
  };
  free__s_hp_categorizeMatrices_syn_t(&(self->obj_rawData), /*isTo_export=*/true, arrOf_sim_postProcess, arrOf_sim_postProcess_size);
  const bool isTo_export = true;
  free__s_hp_categorizeMatrices_syn_t(&(self->obj_afterSim), /*isTo_export=*/isTo_export, arrOf_sim_postProcess, arrOf_sim_postProcess_size);
}

/**
   @brief add a vertex-data-set to the collection (of catrozied data-sets) (oekseth, 06. arp. 2017).
   @param <self>  is the object to update
   @param <obj_rawData> is the matrix used as input to the simlarity-emtirc
   @param <obj_afterSim> optioanl: if set is the matrix which si the reuslt of applying a simalirty-emtirc.
   @param <stringOf_entry> is the string-desicptor to assicated with 'this entry' (eg, a composion of cluster-algorithm and data-set-id);
   @param <vertexProp> optional: may be used to 'add' proeprties wrt. the vertex, proerpteis which are used during the de-allcoaiton-call ("free__s_hp_categorizeMatrices_syn_t(..)") to evlaute different hyptohesis wrt. diferences in data-set-variations.
 **/
static void add_dataSet__s_hp_categorizeMatrices_syn__setOf_t(s_hp_categorizeMatrices_syn__setOf_t *self, const s_kt_matrix_t *obj_rawData, const s_kt_matrix_t *obj_afterSim, const char *stringOf_entry, const s_hp_categorizeMatrices_syn_config_dataObj_t vertexProp) {
  assert(self);
  if(obj_rawData) {
    add_dataSet__s_hp_categorizeMatrices_syn_t(&(self->obj_rawData), obj_rawData, stringOf_entry, vertexProp);
  }


  if(obj_afterSim != NULL) {
    add_dataSet__s_hp_categorizeMatrices_syn_t(&(self->obj_afterSim), obj_afterSim, stringOf_entry, vertexProp);
  }
}


#endif //! EOF
