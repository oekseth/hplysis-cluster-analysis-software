  //! ----------------------
  for(char config_weight = 0; config_weight < 2; config_weight++) {
    for(char config_mask = 0; config_mask < 2; config_mask++) {
      for(char config_useTwoMatricesAsInput = 0; config_useTwoMatricesAsInput < 2; config_useTwoMatricesAsInput++) {
	for(char config_transpose = 0; config_transpose < 2; config_transpose++) {
	  const bool transpose = config_transpose;
	  //! -------------------------------------------
	  //! Generate the measurement-test:
	  char stringOf_measureText[2000]; memset(stringOf_measureText, '\0', 2000);
	  sprintf(stringOf_measureText, "%s: %s; transpose='%s',  use-weight='%s', mask-type='%s', use-two-differentMatrices-as-inpnut='%s'", 
		  stringOf_measureText_base, stringOf_proxType,
		  (config_transpose) ? "true" : "false",
		  (config_weight) ? "true" : "false",
		  (config_mask == 1) ? "maskExplicit" :  "mask-none",
		  (config_useTwoMatricesAsInput) ? "true" : "false"
		  );

	  t_float **local_matrix_1 = matrix;   t_float **local_matrix_2 = matrix;
	  if(transpose == true) {
	    local_matrix_1 = matrix_transposed; local_matrix_2 = matrix_transposed;
	  }
	  t_float *local_weight = NULL;
	  if(config_weight) {local_weight = weight;}
	  
	  if(config_useTwoMatricesAsInput) {
	    if(transpose == false) {
	      local_matrix_2 = allocate_2d_list_float(nrows, size_of_array, default_value_float);
	    } else {
	      local_matrix_2 = allocate_2d_list_float(size_of_array, nrows, default_value_float);
	    }
	  }
	  char **local_mask1 = NULL; 	      char **local_mask2 = NULL;
	  bool masks_isAllocated = false;
	  if(config_mask == 1) {
	    if(transpose == false) {
	      local_mask1 = mask1; local_mask2 = mask2;
	    } else {
	      masks_isAllocated = true;
	      local_mask1 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
	      local_mask2 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
	    }
	  }
	  //! 
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  //!
	  //!
	  //! The experiemnt:
	  t_float sumOf_values = 0;
	  // printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
	  for(uint row_id = 0; row_id < nrows; row_id++) {
	    for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	      //! --------------
	      if(local_mask1) {
		assert(local_mask2);
		assert(local_mask1[row_id]); 	      assert(local_mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	      }
	      //! --------------
	      sumOf_values += __localConfig__funcName(/*ncols=*/size_of_array, local_matrix_1, local_matrix_2, /*mask1=*/local_mask1, /*mask2=*/local_mask2, local_weight, row_id, row_id_out, /*transpose=*/transpose);
	    }
	  }
	  //! --------------
	  __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
	  if(config_useTwoMatricesAsInput) {
	    if(transpose == false) { 
	      free_2d_list_float(&local_matrix_2, nrows);
	    } else {free_2d_list_float(&local_matrix_2, size_of_array);}
	  }
	  if(masks_isAllocated) {
	    if(transpose == false) { 
	      free_2d_list_char(&local_mask1, nrows);
	      free_2d_list_char(&local_mask2, nrows);
	    } else {
	      free_2d_list_char(&local_mask1, size_of_array);
	      free_2d_list_char(&local_mask2, size_of_array);
	    }
	  }
	}
      }
    }
  }
#undef __localConfig__funcName
