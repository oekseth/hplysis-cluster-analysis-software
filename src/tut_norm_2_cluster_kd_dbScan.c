#include "hpLysis_api.h"

static void tut_norm_2_cluster_kd_dbScan__compute(s_kt_matrix_t mat_input) {
   //!
    //! Apply logics:
    for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {      
      //! Normalize (or optionally copy the non-nroamized data-set):
      s_kt_matrix_t mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_input, (e_kt_normType_t)isTo_normalize);
      if(false) { //! then we export the result-file to a tmeprary file:
	char fileName_local[1000]; memset(fileName_local, '\0', 1000);
	sprintf(fileName_local, "result_tut_norm1_useNorm%u.tsv", isTo_normalize);
	printf("export to file=\"%s\", at %s:%d\n", fileName_local, __FILE__, __LINE__);
	export__singleCall__s_kt_matrix_t(&mat_data, fileName_local, NULL);
      }
      //const e_kt_correlationFunction_t sim_pre = (e_kt_correlationFunction_t)42;
      const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
      const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree;
      //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
      //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = 
      //!
      //! Allocate object:
      s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
      obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
      //obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
      obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
      //obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
      if(true) {
	obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
	obj_hp.config.corrMetric_prior_use = true;
      }
      
      //! 
      //! Apply logics:
      // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
      if(true) {
	const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
	//	const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
	assert(is_ok);
      } else { //! then we use a HCA-pre-step ot idneitfy cstart-centrodi-vertices in k-means-clustering
	const uint nclusters = UINT_MAX; 
	const e_hpLysis_clusterAlg_t clusterAlg__firstPass = e_hpLysis_clusterAlg_HCA_single;	
	const bool is_ok = cluster__kMeansTwoPass__hpLysis_api(&obj_hp, clusterAlg, &mat_data, nclusters, 100, /*cofnig-firstPass*/obj_hp.config, clusterAlg__firstPass);
	assert(is_ok);
      }
      //! Get the CCM-score:
      const t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
      if(ccm_score != T_FLOAT_MAX) {
	/* assert(matResult_ccm_matrix); */
	/* assert(index_local < matResult_ccm_matrix->ncols); */
	/* assert(ccm_id < matResult_ccm_matrix->nrows); */
	//!
	//! Set the score:
	printf("[norm=%u]\t score(Silhouttte)=%f, at %s:%d\n", isTo_normalize, ccm_score, __FILE__, __LINE__); 
      }
      //! 
      //! De-allcoate object, and return:
      free__s_hpLysis_api_t(&obj_hp);      
      free__s_kt_matrix(&mat_data);
    }
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief   (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks 
   @remarks related-tut-examples:
   -- "tut_norm_1_differentNormStrategies.c": a subset of this tut-example, ie, where latter is itnroducedused to avoid overhweilinming users with detials of clsuter-applicaiton. 
   -- "tut_kd_1_cluster_multiple_simMetrics.c": logics to evaluate implicaiton of different patterns. 
**/
int main(const int array_cnt, char **array) 
#else
  int tut_norm_2_cluster_kd_dbScan(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  const char *resultPrefix = "tut_evalKd_";

  //! Fris seperatley for a gvien input-file:
  //! Note: for a list of files to be used see our cofniguraiton-fiels, eg, wrt. our "data/local_downloaded/metaObj.c"
  //const char *file_name = "data/local_downloaded/22_nuts.csv.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
  //const char *file_name = "data/local_downloaded/56_msq.csv.hpLysis.tsv";
  //const char *file_name = "data/local_downloaded/23_Airline.csv.hpLysis.tsv"; //! which examplfies chalelnges in scales (ie, different scalies used at differnt data-axis) ... 
    const char *file_name = "tests/data/kt_mine/birthWeight_chineseChildren.tsv";
    const uint index_pos = 3;
    bool fileIs_speificed = false; //! which is used to simplify error-generiaotn.
    if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) {
      //! then set the file-name:
      file_name = array[index_pos];
      fileIs_speificed = true;
    }
    //!
    //! Load an arbitrary data-set:
    // const uint nrows = 1000; const uint ncols = 20;
    //const uint nrows = 1000; const uint ncols = 20;
    //  const uint nrows = 1000*10; const uint ncols = 5;
    //const uint nrows = 1000*1000; const uint ncols = 20;
    s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols);
    //! Then laod the file:
    assert(file_name); assert(strlen(file_name));
    import__s_kt_matrix_t(&mat_input, file_name);
    if(mat_input.nrows == 0) {
      if(fileIs_speificed) {
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      } else { //! then we asusmet eh file-name was spefieid by the user.
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      }
      return false;
    }
    //!
    //! Apply logics:
    tut_norm_2_cluster_kd_dbScan__compute(mat_input);
    //!
    //! De-allocate:
    free__s_kt_matrix(&mat_input);
    
    //!
    //! @return
#ifndef __M__calledInsideFunction
    //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    hpLysis__globalFree__kt_api();
    //! *************************************************************************  
#endif
    return true;
}
