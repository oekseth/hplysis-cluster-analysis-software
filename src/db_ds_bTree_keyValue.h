#ifndef db_ds_bTree_keyValue_h
#define db_ds_bTree_keyValue_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file db_ds_bTree_keyValue
   @brief provide a B-tree-implementaiotn with different configurale fixed-sized children-couunts (oekseth, 06. mar. 2017)
 **/

#include "db_ds_bTree__aux.h"
#include "kt_set_1dsparse.h"
#include "db_rel.h"
/**
   @struct s_db_baseNodeId_typeOfKey_int
   @brief defines a "key, value" object, an object used in our B-tree (oekseth, 06. feb. 2017).
 **/
typedef struct s_db_baseNodeId_typeOfKey_int  {
  //! -----------------------------
  //! Key-parts: [”elow] used to idneityf 'uqniuness' wr.t the node.
  uint head;
  //! -----------------------------
  //! Non-key-parts: [”elow] Not used in uniquness-comparison between nodes.
  long long unsigned int value; //! eg, to 'represent' an 'internal' relation-id.
} s_db_baseNodeId_typeOfKey_int_t;
#define __M__cmp__typeOf_node__int(obj1, obj2) ({ int score = 0; if(obj1.head < obj2.head) {score = -1;} else if(obj1.head > obj2.head) {score = 1;} score;})
//#define __M__cmp__typeOf_node__int(obj1, obj2) ({ int score = 0; if(obj1 < obj1) {score = -1;} else if(obj1 > obj2) {score = 1;} score;})
#define __M__isEqual__typeOf_node__int(obj1, obj2) ({ const int cmp_val = __M__cmp__typeOf_node__int(obj1, obj2); /*printf("compare (%d == %d) --> cmp_val=%d, at %s:%d\n", obj1.head, obj2.head, cmp_val, __FILE__, __LINE__);*/ const int ret_val = (0 == cmp_val); ret_val;})
//#define __M__isEqual__typeOf_node__int(obj1, obj2) ({ (0 == __M__cmp__typeOf_node__int(obj1, obj2));})
#define __M__searchPos__typeOf_node__int(key, key_arr, n) ({uint pos=0; while ( (pos < (uint)n) && (__M__cmp__typeOf_node__int(/*obj1=*/key, /*obj2=*/key_arr[pos]) == 1) ) { pos++; }; pos;})
#define __M__getKeyFroObj__node__int(obj) ({obj.head;})
#define MF__resultsToUSe__afterSearch__tyoeOf_node__int(rel) ({searchId_head = UINT_MAX; if(__M__isWildCart__s_db_rel_t(rel.head) == false) {searchId_head = rel.head;}})

#include "db_ds_bTree_keyValue__internalWrappers.h" //! eg, for "s_db_node_typeOfKey_int_case32"
/**
   @struct s_db_ds_bTree_keyInt
   @brief provide a b-Tree interface for "s_db_baseNodeId_typeOfKey_int_t" nodes (oekseth, 06. mar. 2017).
 **/
typedef struct s_db_ds_bTree_keyInt {
  s_db_node_typeOfKey_int_case4_t *root_case4; //=NULL;
  s_db_node_typeOfKey_int_case16_t *root_case16; //=NULL;
  s_db_node_typeOfKey_int_case32_t *root_case32; //=NULL;
  e_db_ds_bTree_cntChildren_t typeOf_tree;
  //s_db_node_typeOfKey_int_case4_t *root; //=NULL;
} s_db_ds_bTree_keyInt_t;
//! @return an intlized 'verison' of our "s_db_ds_bTree_keyInt_t" struct.
s_db_ds_bTree_keyInt_t init__s_db_ds_bTree_keyInt_t(const e_db_ds_bTree_cntChildren_t typeOf_tree);
//! De-allcoates a given s_db_ds_bTree_keyInt_t tree.
void free__s_db_ds_bTree_keyInt_t(s_db_ds_bTree_keyInt_t *self);
/**
   @brief insert a given "key" object into our struct (oekseth, 06. mar. 2017)
   @param <self> is the object to insert in
   @param <key> is the key to insert
   @return true upon success.
 **/
bool insert__s_db_ds_bTree_keyInt_t(s_db_ds_bTree_keyInt_t *self, const s_db_baseNodeId_typeOfKey_int_t key);
/**
   @brief idneitfy a "scalar_result" which 'corresponds' to "key" (oekseht, 06. mar. 2017)
   @param <self> is the object to search in
   @param <key> is the key to search for
   @param <scalar_result> is the idneitifed object
   @param <isTo_inpsectAll> which is to be set to tru if arbitrary or undefined 'searhc-kesy' are used.
   @param <result_set> which if Not set to null is 'used' to insert the idneitfed result-relationships.
   @return true if the key was found.
 **/
bool find__s_db_ds_bTree_keyInt_t(s_db_ds_bTree_keyInt_t *self, const s_db_baseNodeId_typeOfKey_int_t key, s_db_baseNodeId_typeOfKey_int_t *scalar_result, const bool isTo_inpsectAll, s_kt_set_1dsparse_t *result_set);


#endif //! EOF
