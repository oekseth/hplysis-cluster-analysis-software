#include "hp_distance_wrapper.h"

//! Compute the simlairty-metric and return the result (oekseth, 06. mar. 2017).
//#include "kt_matrix.h" 
//static
s_kt_matrix_t applyAndReturn__altTypeAsInput__hp_distance_wrapper(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2, const s_hp_distance__config_t local_config) {
  assert(obj_1);

  s_kt_matrix_base_t result_mat = initAndReturn__empty__s_kt_matrix_base_t();
  s_kt_matrix_base_t cpy_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(obj_1);
  if(obj_2) {
    s_kt_matrix_base_t cpy_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(obj_2);
  //! The call:
    const bool is_ok = apply__hp_distance(obj_metric, &cpy_1, &cpy_2, &result_mat, local_config); 
    assert(is_ok);    
    //! 'Reset' internals:
    cpy_2 = initAndReturn__empty__s_kt_matrix_base_t();
  } else {
    const bool is_ok = apply__hp_distance(obj_metric, &cpy_1, NULL, &result_mat, local_config); 
    assert(is_ok);
  }
  //!
  //! Wrap the 'reslt-object' into an s_kt_matrix_t object:
  s_kt_matrix_t mat_return = setToEmptyAndReturn__s_kt_matrix_t();
  mat_return.nrows = result_mat.nrows;
  mat_return.ncols = result_mat.ncols;
  mat_return.matrix = result_mat.matrix;
  { //! Add the string-idnetifers:
    if(obj_1->nameOf_rows) {
      //! Add names for: rows:
      // printf("set row-names, at %s:%d\n", __FILE__, __LINE__);      
      // assert(obj_1->nameOf_rows);
      for(uint i = 0; i < mat_return.nrows; i++) {
	const char *name = obj_1->nameOf_rows[i];
	if(name && strlen(name)) {
	  set_stringConst__s_kt_matrix(&mat_return, i, name, /*addFor_column=*/false);
	}
      }
    }  //! else we assuem no\ string-dinfiers are asiscted ot the matrix.
    if(obj_2 == NULL) { //! then we asusem asme row-naems are set for both rows and columns:
      //! Add names for: columns:
      if(obj_1->nameOf_rows  != NULL) {
	// printf("set col-names, at %s:%d\n", __FILE__, __LINE__);      
	for(uint i = 0; i < mat_return.ncols; i++) {
	  const char *name = obj_1->nameOf_rows[i];
	  if(name && strlen(name)) {
	    set_stringConst__s_kt_matrix(&mat_return, i, name, /*addFor_column=*/true);
	  }
	}
      } //! else we assuem no\ string-dinfiers are asiscted ot the matrix.
    } else { 
      if(obj_1->nameOf_rows  != NULL) {
	// printf("set col-names, at %s:%d\n", __FILE__, __LINE__);      
	//! Add names for: columns:
	for(uint i = 0; i < mat_return.ncols; i++) {
	  const char *name = obj_2->nameOf_rows[i];
	  if(name && strlen(name)) {
	    set_stringConst__s_kt_matrix(&mat_return, i, name, /*addFor_column=*/true);
	  }
	}
      } //! else we assuem no\ string-dinfiers are asiscted ot the matrix.
    }
  }
  //! 'Reset' internals:
  result_mat = initAndReturn__empty__s_kt_matrix_base_t();
  cpy_1 = initAndReturn__empty__s_kt_matrix_base_t();
  //! @return
  return mat_return;
}

