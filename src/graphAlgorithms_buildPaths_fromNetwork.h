#ifndef graphAlgorithms_buildPaths_fromNetwork_h
#define graphAlgorithms_buildPaths_fromNetwork_h

#include "type_uint_n.h"
#include "sp_johnson.h"

/**
   @class graph_vertex_index
   @ingroup shortestPaths
   @brief relates a vertex-id to a local index.
   @author Ole Kristian Ekseth (oekseth)
 **/
class graph_vertex_index : public type_uint_n<2> {
 public:
  //! @return the tail-id in the local name-scope.
  const uint get_vertex_id() const {
    return get_key(0);
  }
  //! @return the rt-id in the local name-scope.
  const uint get_tableIndex() const {
    return get_key(1);
  }

  //! The constructor:
 graph_vertex_index(const uint vertex_id = UINT_MAX, const uint tableIndex = UINT_MAX) :
  type_uint_n<2>(/*defalut-value=*/vertex_id) {
    //! Set the values:
    set_key(1, tableIndex);
    //! What we expect:
    assert(vertex_id == get_vertex_id());    
    assert(tableIndex == get_tableIndex());
  }
};



/**
   @blass graphAlgorithms_buildPaths_fromNetwork
   @brief construct/infer paths from a network, eg, as basis for betweenness-centrlaity using a shortest-path-graph as input.
   @author Ole Kristian Ekseth (oekseth)
**/
class graphAlgorithms_buildPaths_fromNetwork {



  //! Identify th start-vertices, ie, the vertice wiothout 'depdendencies'.
  void infer_startVertices(const sp_compressed *compressed, list_generic<graph_vertex_index> &arrOf_stack, const uint biggest_vertex_id) const {
    assert(compressed);  
    assert(arrOf_stack.get_current_pos() == 0);
    assert(arrOf_stack.is_empty());
    //! Identify the start-vertices, ie, the vertices 'which are not themself part of another path':
    //! Note: we use the 'comrepssed name-sapce' to reduce the number of memory-cache-misses
    const uint cnt_vertices_compressed = compressed->get_cnt_vertices();
    assert(cnt_vertices_compressed > 0);
    list_uint arrOf_cntParents;
    for(uint head_id = 0; head_id < cnt_vertices_compressed; head_id++) {
      //! Find/idneitfy the vertices 'which are the start of the paths':
      // FIXME: ... add/write [”elow] funciton. 
      const uint cnt_parents = compressed->get_cnt_parents_forLocalVertex(head_id);
      if(cnt_parents > 0) {
	arrOf_cntParents.increment(head_id, cnt_parents);
      }
    }
    //! Inser the 'start-vertices' into teh stack:
    for(uint head_id = 0; head_id < cnt_vertices_compressed; head_id++) {
      if( (arrOf_cntParents[head_id].is_empty() == true) || (arrOf_cntParents[head_id].get_key() == UINT_MAX) ) {
	arrOf_stack.push(graph_vertex_index(head_id));
      } 
    }

    arrOf_cntParents.free_memory();
  }

  /**

   **/
  void init(const sp_compressed *compressed, const uint biggest_vertex_id, graphAlgorithms_centrality &objOf_lib_centrality) {
    assert(compressed);
    assert(biggest_vertex_id > 0);
    const uint matrix_3d_ofPaths_size = compressed->get_cnt_vertices(); 
    assert(matrix_3d_ofPaths_size > 0);
    list_generic<graph_vertex_index> arrOf_stack;
    //! Identify th start-vertices, ie, the vertice wiothout 'depdendencies'.
    infer_startVertices(compressed, arrOf_stack, biggest_vertex_id);
  
    list_2d_generic<type_2d_uint_nonCmp_float>  *matrix_3d_ofPaths = new list_2d_generic*[matrix_3d_ofPaths_size]; 
  
    list_2d_generic<type_2d_uint_nonCmp_float> setOf_local_paths; //! hold the set of paths

    // FIXME: validate concpetually [”elow] alrogirhm ... and then describe a new use-case.

    
    assert(false); // FIXME: describe correctness of our [below] approach
    assert(false); // FIXME: for Directe NEtworks how does 'this approach' ''diverge' from the 'path-remembering-appraoch' (descirbed&implemented in oekseths master-thesis)?
    assert(false); // FIXME: describe possible use-case-applicaitons of [below].

  
    //! Start the path-investigation:
    while(arrOf_stack.get_current_pos() > 0) {
      const graph_vertex_index current_obj = arrOf_stack.pop();
      assert(current_obj.is_empty() == false);
      const uint head_id = current_obj.get_vertex_id();
      /* const uint head_id_realWorld = compressed->get_globalId_forLocallyScoped_id(head_id); */
      /* assert(head_id_realWorld != UINT_MAX); */
      /* assert(head_id_realWorld <= biggest_vertex_id); //! ie, as otherwise would be strange. */
      uint current_pathIndex = current_pathIndex.get_tableIndex();
      if(current_pathIndex == UINT_MAX) {current_pathIndex = head_id;} //! ie, as we then assuem that 'this' is a 'root'.
      else {assert(current_pathIndex == head_id);}

    

      /* list_2d_generic<sp_compressed_tailRelation> objOf_vertex; */
      /* // FIXME: figure out how we may update our "sp_compressed" to 'store' the set of paths.  <-- .... */
      /* //! Note: the set of paths are constructed 'dynamically' during this traversal-procedure. */
      /* compressed->get_arrOf_relations_for_globalVertex(head_id, objOf_vertex); */

      bool is_cyclic = false;
      //console.log("objOf_vertex=", objOf_vertex);
      if(setOf_local_paths[current_pathIndex] && setOf_local_paths[current_pathIndex]->get_current_pos()) { //! then we update the result "matrix_3d_ofPaths".
	const uint cnt_paths = setOf_local_paths[current_pathIndex]->get_current_pos();
	for(var k = 0; k < cnt_paths; k++) {
	  const uint parent_id = setOf_local_paths[current_pathIndex]->template_get_value(k).get_key();
	  assert(parent_id != UINT_MAX);
	  // var distance_parent = objOf_vertex.setOf_local_paths[k].distance;
	  // if(distance_parent == undefined) {console.log(dummyTo_break);} //! ie, as we then need to investigte this case.
	
	  //! Add 'this' to the parent set of paths:
	  const uint parent_index = matrix_3d_ofPaths[parent_id].get_current_pos() - 1;
	  if(parent_index == 0) {
	    fprintf(stderr, "!!\t Validate that we have iterated in/throught he path 'at an ealrier point', given parent_id=%u, at %s:%d\n", parent_id, __FILE__, __LINE__);
	  }
	  { //! Investigate if 'this' is alreayd found, ie, 'if there is a cyclic DFS:
	    if(parent_id == head_id) {
	      fprintf(stderr, "!!\t(is-cyclic)\t vertex=%u, at [%s]:%s:%d\n", head_id, __FUNCTION__, __FILE__, __LINE__);
	      //! Note: the [”elow] could also imply that 'the paths has not bee correctly cleared for a new path-case' (eg, when there exists more than one path between two vertices).
	      is_cyclic = true; //! ie, as 'this' is then found.
	    }
	  }
	  // FIXME: ... add/write [”elow] funciton. 
	  const uint distance_parent = compressed->get_minDistance_forGlobalVertexPair(head_id, parent);
	  type_2d_uint_nonCmp_float local_obj(/*index=*/head_id, distance_parent);	
	  matrix_3d_ofPaths[parent_id].setOf_local_paths[parent_index].push(local_obj);
	}
      }

      { //! Intiate a new 'path' for 'this', ie, 'givent eh proeprties of the Depth-First-Search', ie, wehre we 'assume' that a vertex is always along a 'new path' when 'entered':
	// TODO: validate correctness of [”elow] 'distance' attribute:
	const uint new_row_index = matrix_3d_ofPaths[head_id].get_unsafe_list_object_and_insertNew_ifNotSet(matrix_3d_ofPaths[head_id].get_current_pos());
	matrix_3d_ofPaths[head_id].get_unsafe_list_object(new_row_index)->push(type_2d_uint_nonCmp_float(head_id)); //! ie, add a enw 'path-set'.
      }
    
    

      // for(uint head_id = 0; head_id < matrix_3d_ofPaths_size; head_id++) {
      //   list_generic<sp_compressed_tailRelation> arrOf_tmp;
      //   compressed->get_arrOf_relations_for_localVertex(head_id, arrOf_tmp);
      //   for(uint k = 0; k < arrOf_tmp.get_current_pos(); k++) {
      // 	updateCentrality_infer_setOf_paths(matrix_3d_ofPaths, matrix_3d_ofPaths_size, head_id, arrOf_tmp[k].get_local_tail_id(), arrOf_tmp[k].get_distance());
      //   }
      //   arrOf_tmp.free_memory();
      // }

      // FIXME: include [ªbove] in [below]. <--- first describe 'hwy and how we aredoinf [”elow]'.

      if(is_cyclic == false) { //! then we add teh chidlren 'to the set':
	
	var setOf_local_paths_updated = [];
	var arrOf = objOf_vertex.setOf_local_paths;
	for(var k = 0; k < arrOf.length; k++) {
	  setOf_local_paths_updated.push(arrOf[k]); //! ie, a 'deep copy' of the strucutre.
	}
	//! Add 'this' to the path-set, ie, 'includign the 'distance-measurement'/'estimation':
	var idOf_head = objOf_vertex.idOf_vertex;
	setOf_local_paths_updated.push({idOf_vertex:idOf_head, distance:self.statusOf_vertex[idOf_head].distance});
	//! Iterate 'through the set of chidlren along the min-path':
	var arrOf = self.statusOf_vertex[idOf_head].arrOf_prevHeads;
	for(var i = 0; i < arrOf.length; i++) {
	  var vertex = arrOf[i];
	  if(vertex != objOf_vertex.idOf_vertex) {
	    var local_obj = {idOf_vertex:vertex, setOf_local_paths:setOf_local_paths_updated, parent_debug:idOf_head /*where the latter is only used for deubgging*/};
	    if(false) {console.log("pair", idOf_head, vertex, "w/obj=", local_obj);}
	    arrOf_stack.push(local_obj);
	  } //! else we asusme it is an 'reflexive' relation.
	}
      } else {
	console.log("-t\t the follwoing vertex was cyclci, ie, consider udpatingt he prcoedure", objOf_vertex);
      }
    }
    //! Update the centaltiry-measures, ie, based on 'computations' for the idOf_source vertex (oekseth, 06. des. 201%).
    if(false) {console.log("objOf_lib_centrality=", objOf_lib_centrality);}



    // FIXME: in [ªbov€] validate that have used the 'compressed' idneities'.
    objOf_lib_centrality.compute_betweenness_centrality(matrix_3d_ofPaths,  matrix_3d_ofPaths_size, /*map-to-realWorld-vertices=*/compressed->get_mapOf_compressedKeys_toGlobalScope());


    //! De-allcoate the lcoally reserved memory:
    for(uint head_id = 0; head_id < matrix_3d_ofPaths_size; head_id++) {
      matrix_3d_ofPaths_size[head_id].free_memory();
    }
    delete [] matrix_3d_ofPaths; 
    arrOf_stack.free_memory();
    setOf_local_paths.free_memory();

    //arrOf_stack = arrOf_stack_2;
    //console.log("stack=", arrOf_stack, arrOf_stack.length);
    //if(true) {console.log("................. returns"); return;}
    { //! Start the depth-first-search:



      // FIXME: remember to 'call the inseriton-procedure for the udpated shortest-path-matrix' in/from our 'johnson'.
      //if(true) {console.log("................. returns"); return;}
    }
  }
  
  static void assert_class(const bool print_info);
}

#endif
