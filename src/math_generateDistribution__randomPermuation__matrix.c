  
  uint **matrix_cntUpdates_from = NULL;   uint **matrix_cntUpdates_to = NULL;
  if(config_swapsMayOverlap == false) {
    matrix_cntUpdates_from = allocate_2d_list_uint(nrows, ncols, empty_0);
    assert(matrix_cntUpdates_from);
    //! ----
    matrix_cntUpdates_to = allocate_2d_list_uint(nrows, ncols, empty_0);
    assert(matrix_cntUpdates_to);
  }

const bool localConifg__isToUSePRev_index_inMove = true;//! ie, a simplfed/genralited assumption
uint prev_oldIndex_row = UINT_MAX; uint prev_oldIndex_col = UINT_MAX;


const loint possible_cells = (loint)nrows * (loint)ncols;
const loint swap_count_max = (config_swapsMayOverlap) ? list_size_cntToSwap__total : macro_min(list_size_cntToSwap__total, possible_cells); //! ie, to ato 'aovid' an infiten swapping-operaiton
//printf("swap_count_max=%lld given list_size_cntToSwap__total=%lld and matrix=[%u, %u], at %s:%d\n", swap_count_max, list_size_cntToSwap__total, nrows, ncols, __FILE__, __LINE__);
  for(loint swap_count = 0; swap_count < swap_count_max; swap_count++) {
    bool is_updated = false;
    // TODO: is [”elow] correct? wrt. teh "p"?
    //const t_float p = 1.0/(list_size_cntToSwap__total - (loint)swap_count);
    const t_float p = 0.4;

    //! 
    //! Idneitfy the 'old' index
    uint old_index_head = UINT_MAX;     uint old_index_tail = UINT_MAX;
    if( (prev_oldIndex_row == UINT_MAX) || !localConifg__isToUSePRev_index_inMove ) 
    { loint cnt_in_while = 0;
      do {
	old_index_head = math_generateDistribution__binomial(/*n*/nrows-1, p);
	old_index_tail = math_generateDistribution__binomial(/*n*/ncols-1, p);
	
	//! Test for convergence and update:
	is_updated = (matrix_cntUpdates_from) ? (matrix_cntUpdates_from[old_index_head][old_index_tail] == 0) : true;
	assert(cnt_in_while < 5*(loint)(nrows * ncols)); //! ie, as we otherwise have a cirical bug.
	assert(old_index_head < nrows);
	assert(old_index_tail < ncols);

      } while(is_updated == false);
    } else {old_index_head = prev_oldIndex_row; old_index_tail = prev_oldIndex_col;}
    if(matrix_cntUpdates_from) {matrix_cntUpdates_from[old_index_head][old_index_tail]++;}
    assert(old_index_head != UINT_MAX);
    assert(old_index_tail != UINT_MAX);
    assert(old_index_head < nrows);
    assert(old_index_tail < ncols);

    //!
    //! Idneitfy the 'new' index
    uint new_index_head = UINT_MAX;     uint new_index_tail = UINT_MAX;
    { loint cnt_in_while = 0;
      do {
	new_index_head = math_generateDistribution__binomial(/*n*/nrows-1, p);
	new_index_tail = math_generateDistribution__binomial(/*n*/ncols-1, p);
	assert(new_index_head < nrows);
	assert(new_index_tail < ncols);
	
	//! Test for convergence and update:
	is_updated = (matrix_cntUpdates_to) ? (matrix_cntUpdates_to[new_index_head][new_index_tail] == 0) : true;
	if( (new_index_head == old_index_head) && (new_index_tail == old_index_tail) ) {is_updated = false;} //! ie, to avoid updating reflexive relationships.
	assert(cnt_in_while < 5*(loint)(nrows * ncols)); //! ie, as we otherwise have a cirical bug.
      } while(is_updated == false);
    }
    if(matrix_cntUpdates_to) {matrix_cntUpdates_to[new_index_head][new_index_tail]++;}
    assert(new_index_head != UINT_MAX);
    assert(new_index_tail != UINT_MAX);
    assert(new_index_head < nrows);
    assert(new_index_tail < ncols);

    //!
    //! Update:
    result_matrix[new_index_head][new_index_tail] = result_matrix[old_index_head][old_index_tail];
    //!
    //! Update
    prev_oldIndex_row = new_index_head;
    prev_oldIndex_col = new_index_tail;
  }

  //! De-allocate:
if(matrix_cntUpdates_from) { free_2d_list_uint(&matrix_cntUpdates_from, nrows); matrix_cntUpdates_from = NULL;}
if(matrix_cntUpdates_to) { free_2d_list_uint(&matrix_cntUpdates_to, nrows); matrix_cntUpdates_to = NULL;}
