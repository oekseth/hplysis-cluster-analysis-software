#include "distance_2rows_slow.h"

/* ********************************************************************* */

/**
   @brief compute the weighted Pearson distance between two rows or columns in a matrix. 
   @remarks We define the Pearson distance as one minus the Pearson correlation. This definition yields a semi-metric: 
   # d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float correlation(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  t_float result = 0.;
  t_float sum1 = 0.;
  t_float sum2 = 0.;
  t_float denom1 = 0.;
  t_float denom2 = 0.;
  t_float tweight = 0.;
  if(transpose == 0) { /* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	const t_float term1 = data1[index1][i];
        const t_float term2 = data2[index2][i];
	//if(isOf_interest(term1) && isOf_interest(term2) ) 
	  {
	  const t_float w = (weight) ? weight[i] : 1;
	  sum1 += w*term1;
	  sum2 += w*term2;
	  result += w*term1*term2;
	  denom1 += w*term1*term1;
	  denom2 += w*term2*term2;
	  tweight += w;
	}
      }
    }
  } else {
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	const t_float term1 = data1[i][index1];
        const t_float term2 = data2[i][index2];
	//if(isOf_interest(term1) && isOf_interest(term2) ) 
	  {
	  const t_float w = (weight) ? weight[i] : 1;
	  //const t_float w = weight[i];

	  sum1 += w*term1;
	  sum2 += w*term2;
	  result += w*term1*term2;
	  denom1 += w*term1*term1;
	  denom2 += w*term2*term2;
	  tweight += w;
	}
      }
    }
  }
  if(!tweight) return 0; /* usually due to empty clusters */
  result -= sum1 * sum2 / tweight;
  denom1 -= sum1 * sum1 / tweight;
  denom2 -= sum2 * sum2 / tweight;
  if(denom1 <= 0) return 1; /* include '<' to deal with roundoff errors */
  if(denom2 <= 0) return 1; /* include '<' to deal with roundoff errors */
  result = result / sqrt(denom1*denom2);
  result = 1. - result;
  return result;
}

/* ********************************************************************* */

/**
   @brief compute the weighted Pearson distance between two rows or columns, using the absolute value of the correlation.
   @return the Pearson distance.
   @remarks This definition (ie, in above) yields a semi-metric: d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is: (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float acorrelation(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  t_float result = 0.;
  t_float sum1 = 0.;   t_float sum2 = 0.;
  t_float denom1 = 0.;   t_float denom2 = 0.;
  t_float tweight = 0.;
  // Compute: sum of tails, where we for each tail multily the head-tail distance with the weight of each tail:
  if(transpose == 0) { /* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	const t_float term1 = data1[index1][i];
        const t_float term2 = data2[index2][i];
	const t_float w = (weight) ? weight[i] : 1; //        const t_float w = weight[i];
        sum1 += w*term1;
        sum2 += w*term2;
        result += w*term1*term2;
        denom1 += w*term1*term1;
        denom2 += w*term2*term2;
        tweight += w;
      }
    }
  } else {
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	const t_float term1 = data1[i][index1];
	const t_float term2 = data2[i][index2];
	const t_float w = (weight) ? weight[i] : 1; //const t_float w = weight[i];
        sum1 += w*term1;
        sum2 += w*term2;
        result += w*term1*term2;
        denom1 += w*term1*term1;
        denom2 += w*term2*term2;
        tweight += w;
      }
    }
  }
  if(!tweight) return 0; /* usually due to empty clusters */
  //! Result-base = abs( sum( w(tail)*w(head_1,tail)*w(head_2,tail) ) - ( sum( w(tail)*w(head_1,tail) ) * (sum (w(tail)*w(head_2,tail) ) ) / sum(w(tail)) ) ) 
  result -= sum1 * sum2 / tweight;
  denom1 -= sum1 * sum1 / tweight;
  denom2 -= sum2 * sum2 / tweight;
  if(denom1 <= 0) return 1; /* include '<' to deal with roundoff errors */
  if(denom2 <= 0) return 1; /* include '<' to deal with roundoff errors */
  result = mathLib_float_abs(result) / sqrt(denom1*denom2);
  result = 1. - result;
  return result;
}

/* ********************************************************************* */

/**
   @brief compute the weighted Pearson distance between two rows or columns, using the uncentered version of the Pearson correlation. 
   @remarks In the uncentered Pearson correlation, a zero mean is used for both vectors even if the actual mean is nonzero. This definition yields a semi-metric: d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float ucorrelation(const uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  t_float result = 0.;
  t_float denom1 = 0.;
  t_float denom2 = 0.;
  uint flag = 0;
  /* flag will remain zero ifno nonzero combinations of mask1 and mask2 are
   * found.
   */
  if(transpose == 0) { /* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	const t_float term1 = data1[index1][i];
        const t_float term2 = data2[index2][i];
	const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
        result += w*term1*term2;
        denom1 += w*term1*term1;
        denom2 += w*term2*term2;
        flag = 1;
      }
    }
  } else {
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	const t_float term1 = data1[i][index1];
        const t_float term2 = data2[i][index2];
	const t_float w = (weight) ? weight[i] : 1; //const t_float w = weight[i];
        result += w*term1*term2;
        denom1 += w*term1*term1;
        denom2 += w*term2*term2;
        flag = 1;
      }
    }
  }
  if(!flag) return 0.;
  if(denom1==0.) return 1.;
  if(denom2==0.) return 1.;
  result = result / sqrt(denom1*denom2);
  result = 1. - result;
  return result;
}

/* ********************************************************************* */

/**
   @brief compute the weighted Pearson distance between two rows or columns, using the absolute value of the uncentered version of the Pearson correlation. 
   @remarks The uacorrelation routine calculates In the uncentered Pearson correlation, a zero mean is used for both vectors even ifthe actual mean is nonzero. This definition yields a semi-metric: d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is: (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float uacorrelation(const uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  t_float result = 0.;
  t_float denom1 = 0.;
  t_float denom2 = 0.;
  uint flag = 0;
  /* flag will remain zero ifno nonzero combinations of mask1 and mask2 are
   * found.
   */
  if(transpose == 0) { /* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	const t_float term1 = data1[index1][i];
        const t_float term2 = data2[index2][i];
	const t_float w = (weight) ? weight[i] : 1; //const t_float w = weight[i];
        result += w*term1*term2;
        denom1 += w*term1*term1;
        denom2 += w*term2*term2;
        flag = 1;
      }
    }
  } else {
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	const t_float term1 = data1[i][index1];
        const t_float term2 = data2[i][index2];
	const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
        result += w*term1*term2;
        denom1 += w*term1*term1;
        denom2 += w*term2*term2;
        flag = 1;
      }
    }
  }
  if(!flag) return 0.;
  if(denom1==0.) return 1.;
  if(denom2==0.) return 1.;
  result = mathLib_float_abs(result) / sqrt(denom1*denom2);
  result = 1. - result;
  return result;
}

