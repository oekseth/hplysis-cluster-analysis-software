{
  //!
  //! Validate input:
  if(!obj_1 || !obj_1->nrows || !obj_1->ncols) {
    fprintf(stderr, "!!\t Error in size: the input-matrix seems empty. In brief update your input-ojbects. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, abort.
  } else if(isTo_transposeMatrix == false) {
    if(obj_result_clusterId->nrows != obj_1->nrows) {
      fprintf(stderr, "!!\t Error in size: we expected the 'nrows'=%u to equal 'cnt-cluster-counts'=%u, an asseriton which we (from the latter) observe does Not hold (for a data-set with a non-transposed input-matrix). In brief update your input-ojbectws. Observation at [%s]:%s:%d\n", obj_1->nrows, obj_result_clusterId->nrows, __FUNCTION__, __FILE__, __LINE__);
      return false; //! ie, abort.
    }
  } else {
    if(obj_result_clusterId->nrows != obj_1->ncols) {
      fprintf(stderr, "!!\t Error in size: we expected the 'ncols'=%u to equal 'cnt-cluster-counts'=%u, an asseriton which we (from the latter) observe does Not hold (for a data-set with a Transposed input-matrix). In brief update your input-ojbectws. Observation at [%s]:%s:%d\n", obj_1->ncols, obj_result_clusterId->nrows, __FUNCTION__, __FILE__, __LINE__);
      return false; //! ie, abort.
    }
  }
  //!
  //! Get the cluster-ids:
  uint *clusterid = obj_result_clusterId->mapOf_rowIds;
  assert(clusterid);
  assert(obj_result_clusterId->nrows);
  {
    //uint *clusterid = allocate_1d_list_uint(clusterid_size, default_value_uint);
    uint cnt_above = 0;
    for(uint i = 0; i < obj_result_clusterId->nrows; i++) {
      if(clusterid[i] != UINT_MAX) {      
	cnt_above += (clusterid[i] >= nclusters);
      }/*  else { */

      /* } */
      //clusterid[i] = UINT_MAX;
    }
    if(cnt_above > 0) {
      fprintf(stderr, "!!\t Error in cluster-configuration: %u vertices were givne a cluster-id >= |clusters|=%u, ie, for which you should update your input. In brief update your input-ojbects. Observation at [%s]:%s:%d\n", cnt_above, nclusters, __FUNCTION__, __FILE__, __LINE__);
      //assert(false); //! ie, an heads-up
      //return false; //! ie, abort.
    }
  }

  assert(obj_1); assert(obj_1->nrows); assert(obj_1->matrix); assert(obj_1->ncols); 
  assert(obj_result_clusterId || obj_result_clusterId_columnValue); assert(nclusters > 0);
  const uint default_value_uint = 0;
  const uint clusterid_size = (isTo_transposeMatrix) ? obj_1->nrows : obj_1->ncols;
  

  char **mask = NULL;
  char **cmask = NULL;
  t_float **cmask_tmp = NULL; 
  const char empty_1 = 1;
  if(isTo_transposeMatrix == false) {    
    cmask = allocate_2d_list_char(nclusters, obj_1->ncols, empty_1);
    cmask_tmp = allocate_2d_list_float(nclusters, obj_1->ncols, empty_1);
  } else {
    cmask = allocate_2d_list_char(obj_1->nrows, nclusters, empty_1);
    cmask_tmp = allocate_2d_list_float(obj_1->nrows, nclusters, empty_1);
  }
  assert(cmask_tmp);
  // assert(false); // FIXME: is it neccesary to update/allocte the "cmask" object ... and if so how is 'it' to be used?

  const uint size_outer_cdata = (isTo_transposeMatrix == false) ? obj_1->ncols : obj_1->nrows;
  const t_float default_value_float = 0;
  t_float **cdata = allocate_2d_list_float(nclusters, size_outer_cdata, default_value_float);
  for(uint i = 0; i < nclusters; i++) {for(uint k = 0; k < size_outer_cdata; k++) {cdata[i][k] = T_FLOAT_MAX;}}

  const uint nrows = obj_1->nrows; t_float **data = obj_1->matrix; const uint ncolumns = obj_1->ncols;
  //
  //! Call the function:
  const int ret_val = __internalMacro__funcCall();
  //!
  //! Handle the return-values:
  assert(ret_val <= 1);   assert(ret_val >= 0);
  //assert(cmask_tmp == NULL); //! ie, as we expect 'this' to have been reset
  
  //! Update the result-object:
  assert(clusterid);
  /* if(obj_result_clusterId) { */
  /*   assert(obj_result_clusterId->mapOf_rowIds == NULL); */
  /*   obj_result_clusterId->mapOf_rowIds = clusterid; //! ie, 'allocate' */
  /*   obj_result_clusterId->nrows = clusterid_size; */
  /* } else {free_1d_list_uint(&clusterid); clusterid = NULL;} */

  assert(cdata);
  if(obj_result_clusterId_columnValue) {
    assert(obj_result_clusterId_columnValue->matrix == NULL);
    obj_result_clusterId_columnValue->matrix = cdata;
    obj_result_clusterId_columnValue->nrows = nclusters;
    obj_result_clusterId_columnValue->ncols = size_outer_cdata;
  } else {free_2d_list_float(&cdata, nclusters); cdata = NULL;}
  //! --------------------
  if(cmask) {free_2d_list_char(&cmask, nclusters); cmask = NULL;}
  if(cmask_tmp) {free_2d_list_float(&cmask_tmp, nclusters); cmask_tmp = NULL;}
  
  assert(cmask_tmp == NULL); //! ie, as we expect 'this' to have been reset
  
  //! @return the status-value wrt. the comptuation:
  return (bool)ret_val;
}
#undef __internalMacro__funcCall 
