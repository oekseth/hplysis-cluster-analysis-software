uint cnt_found = 0; //! where Idea is to 'Not stop at first match' ... and 'return the total number of idneitfed matcfhes'.
if(self) {
  //! Find the 'correct fork' to search in (ie, if any):
  for(int pos = 0; pos < self->n; pos++) {
    if(1 == __MiF__cmp__equal(key, self->key[pos]) ) {       
      //printf("\tKey %d found in position %d of last dispalyed node (given search=%d)\n", __M__getKeyFroObj__node__int(key), pos, __M__getKeyFroObj__node__int(ptr->key[pos]));
      // assert(self->key[pos].head == key.head); // TODO: cosnider removing this ... ie, as 'latter' does Not hold for wild-cards.
      *scalar_result = self->key[pos]; 
      __MF__updateResultStack((self->key[pos])); //! ie, update the result-set if "result_set" is 'used'
      //return 1;
      cnt_found++;
    }
  } 
  //!
  //! Inveigate children, ie, as 'this' did not explcitly hold any childrne witht eh key in question.
  for(int i = 0; i <= self->n; i++) {
    if(self->pointers[i]) {
      //!  Resursive search:
      const uint ret_val = __MiF__search__slowEvaluateAll(self->pointers[i], key, scalar_result, result_set);
     cnt_found += ret_val;
     //if(ret_val) {return 1;}
    }
  }
 }
 //printf("Key %d is not available\n", __M__getKeyFroObj__node__int(key));
 return cnt_found; //! ie, Not found.
