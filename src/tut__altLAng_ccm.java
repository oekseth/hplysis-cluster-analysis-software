import java.util.Random;



/**
   @brief a stand-alone app to idneitfy time consumpion for silhouette Cluster Comparison MEtric (CCM) copmtaution.
   @remarks inispriation: "https://docs.oracle.com/javase/tutorial/getStarted/cupojava/unix.html". 
   @remarks build-process:
   -- compile: javac tut__altLAng_ccm.java
   -- run: java -classpath . altLAng_ccm
   -- combined(call): javac tut__altLAng_ccm.java; java -classpath . altLAng_ccm 1>result.txt
 */
class altLAng_ccm { //! where latter is the naem of the exeutable
    public static float value_result = 0;

    public static float __getForCol(int ncols, float matrix[][], int row_id, int row_id_out) {
	float sum = 0F;
	for(int i = 0; i < ncols; i++) {
	    float s_1 = matrix[row_id][i];
	    float s_2 = matrix[row_id_out][i];
	    if( (s_1 != Float.MAX_VALUE) && (s_2 != Float.MAX_VALUE) ) {
		sum += s_1 * s_2;
	    }
	}
	return sum;
    }

    //! Idea: comptue Euclid xmt if-calsue-testign and xmt memory-fetching.:
    public static float get_timeOf__Silhouette(int nrows_1, int ncols) {

	int cnt_clusters = 5;

      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      Random rand = new Random();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = rand.nextFloat() + 1;
	      // matrix[row_id][i] = i;
	  }
      }
      //! Set cluster-memberships: 
      int [] map_clusters = new int[nrows_1];
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  int random = (int )(Math.random() * cnt_clusters);
	  map_clusters[row_id] = random;
	  if(map_clusters[row_id] > cnt_clusters) {
	      map_clusters[row_id]  = 0;
	  }
      }

      //! ------------------------------------------------ START: 
      //! 
      //! 
      long startTime = System.currentTimeMillis();
      //! 
      //! 
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  //! 
	  //! Comptue distance to all clusters: 
	  double silhCoeff = 0.0;
	  int totalCount = 0;
	  int[] countsByClusters = new int[cnt_clusters];
	  double[] distanceByClusters = new double[cnt_clusters];
	  //! 
	  //! 
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	  // int numFCluster = clustering.size();
	      //                        for (int fc = 0; fc < numFCluster; fc++) {
	      //if(pointInclusionProbFC[p1][fc] > pointInclusionProbThreshold){
	      double distance =  __getForCol(ncols, matrix, row_id, row_id_out);


	      distance = Math.sqrt(distance);
	      //double distance = distance(matrix[row_id], matrix[row_id_out]);
	      int fc = map_clusters[row_id_out];
	      distanceByClusters[fc] += distance;
	      countsByClusters[fc]++;
	  }
	  
	  //find closest OWN cluster as clusters might overlap
	  double minAvgDistanceOwn = Double.MAX_VALUE;
	  int minOwnIndex = -1;
	  for (int fc = 0; fc < cnt_clusters; fc++) {
	      double normDist = distanceByClusters[fc]/(double)countsByClusters[fc];
	      if(normDist < minAvgDistanceOwn){// && pointInclusionProbFC[p][fc] > pointInclusionProbThreshold){
		  minAvgDistanceOwn = normDist;
		  minOwnIndex = fc;
	      }
	  }

	  
	  //find closest other (or other own) cluster
	  double minAvgDistanceOther = Double.MAX_VALUE;
	  for (int fc = 0; fc < cnt_clusters; fc++) {
	      if(fc != minOwnIndex){
		  double normDist = distanceByClusters[fc]/(double)countsByClusters[fc];
		  if(normDist < minAvgDistanceOther){
		      minAvgDistanceOther = normDist;
		  }
	      }
	  }
	  
	  double silhP = (minAvgDistanceOther-minAvgDistanceOwn)/Math.max(minAvgDistanceOther, minAvgDistanceOwn);
	  // point.setMeasureValue("SC - own", minAvgDistanceOwn);
	  // point.setMeasureValue("SC - other", minAvgDistanceOther);
	  // point.setMeasureValue("SC", silhP);
	  
	  silhCoeff+=silhP;
	  totalCount++;
        // double [][] pointInclusionProbFC = new double[points.size()][numFCluster];
        // for (int p = 0; p < points.size(); p++) {
        //      DataPoint point = points.get(p);
        //     for (int fc = 0; fc < numFCluster; fc++) {
        //         Cluster cl = clustering.get(fc);
        //         pointInclusionProbFC[p][fc] = cl.getInclusionProbability(point);
        //     }
        // }


	// //! Construct clusters: 
        // for (int p = 0; p < points.size(); p++) {

	    //! Add all points to a cluster which is above a certain size: 
            // DataPoint point = points.get(p);
            // ArrayList<Integer> ownClusters = new ArrayList<Integer>();
            // for (int fc = 0; fc < numFCluster; fc++) {
            //     if(pointInclusionProbFC[p][fc] > pointInclusionProbThreshold){
            //         ownClusters.add(fc);
            //     }
            // }

	   
                //System.out.println(point.getTimestamp()+" Silh "+silhP+" / "+avgDistanceOwn+" "+minAvgDistanceOther+" (C"+minIndex+")");
      }
      // if(totalCount>0)
      // 	  silhCoeff/=(double)totalCount;
      // //normalize from -1, 1 to 0,1
      // silhCoeff = (silhCoeff+1)/2.0;
      // addValue(0,silhCoeff);

      // //! Allcoate resutl-atmrix and compute:
      // float matrix_result[][] = new float[nrows_1][nrows_1];
      // l
      // for(int row_id = 0; row_id < nrows_1; row_id++) {
      // 	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
      // 	      for(int i = 0; i < ncols; i++) {
      // 		  sum += (row_id * row_id_out);
      // 	      }
      // 	      matrix_result[row_id][row_id_out] = sum;
      // 	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
      // 	  }
      // }      
      long estimatedTime = System.currentTimeMillis() - startTime;      
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }
    //! Idea: comptue Euclid xmt if-calsue-testign:
    public static float get_timeOf__MatrixComp(int nrows_1, int ncols) {
      float [] outputs = new float[nrows_1];
      float sum = 0F;
      float matrix[][] = new float[nrows_1][ncols];      
      //! Itnaite:
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int i = 0; i < ncols; i++) {
	      matrix[row_id][i] = i;
	  }
      }
      //! Allcoate resutl-atmrix and compute:
      float matrix_result[][] = new float[nrows_1][nrows_1];
      long startTime = System.currentTimeMillis();
      for(int row_id = 0; row_id < nrows_1; row_id++) {
	  for(int row_id_out = 0; row_id_out < nrows_1; row_id_out++) {
	      for(int i = 0; i < ncols; i++) {
		  sum += matrix[row_id][i] * matrix[row_id_out][i];
	      }
	      matrix_result[row_id][row_id_out] = sum;
	      value_result += sum; //! ie, to 'avpod the compielr from optmziedinzg avway' this logic
	  }
      }      
      long estimatedTime = System.currentTimeMillis() - startTime;
      return (float)estimatedTime*(float)0.001; //! ie, convert to 'seconds'.
    }

    public static void main(String[] args) {
	System.out.println("row_name\tSilouette\tEuclid"); //! ie, write out a header
	System.err.println("row_name\tSilouette\tEuclid"); //! ie, write out a header
	//for(int case_id = 0; case_id < 200; case_id++) {
	int cnt_cases = 10;
	// int cnt_cases = 20;
	for(int case_id = 1; case_id <= cnt_cases; case_id++) {
	    //int nrows = 128*(1 + case_id); 	    int ncols = 128*(1 + case_id);      
	    int nrows = 1000*10; int ncols = 50*case_id;
	    //	    int nrows = 1000*10; int ncols = 5;
	    //int nrows = 1000*5; int ncols = 5;
	    //	    int nrows = 1000*2; int ncols = 5;
	    // int nrows = 10; int ncols = 20;
	    double time_1 = get_timeOf__Silhouette(nrows, ncols);
	    double time_2 = get_timeOf__MatrixComp(nrows, ncols);
	    //float time = 
	    System.out.println("matrix[" + nrows + "," + ncols + "]\t" 
			       + time_1
			       + "\t"
			       + time_2
			       );
	    System.err.println("matrix[" + nrows + "," + ncols + "]\t" 
			       + time_1
			       + "\t"
			       + time_2
			       );
	    //+ "\n"); // Display the string.
	}
	System.err.println("sum of score=" + value_result);
    }
}
