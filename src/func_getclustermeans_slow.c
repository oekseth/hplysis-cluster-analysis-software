{  //! What we expect:
  assert(nclusters > 0);
  assert(nrows > 0);
  assert(data);
  //assert(mask);
  assert(clusterid);
  assert(cdata);
  //assert(cmask);
  assert(cmask_tmp);
  assert( (transpose == 0) || (transpose == 1) );

  if(transpose == 0) {
    //! Intiate the dummy/temproary list:
    for(uint i = 0; i < nclusters; i++) {
      for(uint j = 0; j < ncolumns; j++) {
	cmask_tmp[i][j] = 0;
      }
    }

    //! Intialise:
    if(!cmask) {
      memset(cdata[0], 0, sizeof(float)*nclusters*ncolumns);
    } else {
      for(uint i = 0; i < nclusters; i++) {
	for(uint j = 0; j < ncolumns; j++) {
	  if(cmask) {cmask[i][j] = 0;}
	  cdata[i][j] = 0.;
	}
      }
    }
    //! Update the sum of weights assicated to each cluster wrt. a cells (head, tail) weights.

    // printf("mask=%p, nrows=%u, ncols=%u, at %s:%d\n", mask, nrows, ncolumns, __FILE__, __LINE__);
   
    for(uint k = 0; k < nrows; k++) {
      const uint i = clusterid[k];
      for(uint j = 0; j < ncolumns; j++) {
	// printf("tests for mask[%u<%u][%u<%u], at %s:%d\n", k, nrows, j, ncolumns, __FILE__, __LINE__);
	if(!mask || (mask[k][j] != 0) ) {
	  const float value = data[k][j];
	  if(isOf_interest(value)) {
	    assert(value != T_FLOAT_MIN_ABS);
	    cdata[i][j] += value;
	    cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'.
	  }
        } 
      }
    }
    //! Infer the 'midpoints' for each centroid:
    for(uint i = 0; i < nclusters; i++) {
      for(uint j = 0; j < ncolumns; j++) {
	if(cmask_tmp[i][j] > 0) {
	  assert_possibleOverhead(cdata[i][j] != T_FLOAT_MAX);
	  cdata[i][j] /= cmask_tmp[i][j]; //! ie, infer the average-value/'center' of each centroid.
          if(cmask) {cmask[i][j] = 1;}
        } else {
	  cdata[i][j] = T_FLOAT_MAX;
	  // FIXME: validate [”elow] ... and if [”elow] does not hold .. then update our itanlizaiton-procedure.
          if(cmask) {cmask[i][j] = 0;}
	  //if(mask) {assert_possibleOverhead(!mask[i][j]);}
	  assert(isOf_interest(cdata[i][j]) == false);
	}
      }
    }
  } else { //! then teh data-set is 'transposed':
    assert(cmask_tmp);
    //printf("\t\t accesses cmask_tmp w/dim=%u, %u, at %s:%d\n", nclusters, nrows, __FILE__, __LINE__);
    //! Intiate the dummy/temproary list:
    for(uint j = 0; j < nrows; j++) {
      for(uint i = 0; i < nclusters; i++) {
	cmask_tmp[j][i] = 0;
      }
    }
    assert_possibleOverhead(cdata);
    //! Intialise:
    for(uint i = 0; i < nrows; i++) {
      assert_possibleOverhead(cdata[i]);
      for(uint j = 0; j < nclusters; j++) {
	cdata[i][j] = 0.;
        if(cmask) {cmask[i][j] = 0;}
      }
    }
    //! Update the sum of weights assicated to each cluster wrt. a cells (head, tail) weights.
    for(uint k = 0; k < ncolumns; k++) {
      const uint i = clusterid[k];
      for(uint j = 0; j < nrows; j++) {
	if(!mask || (mask[j][k] != 0) ) {
	  const float value = data[j][k];
	  if(value != T_FLOAT_MAX) {
	    assert(value != T_FLOAT_MIN_ABS);
	    cdata[j][i]+= value;
	    cmask_tmp[j][i]++;
	  }
        }
      }
    }
    //! Infer the 'midpoints' for each centroid:
    for(uint i = 0; i < nrows; i++) {
      for(uint j = 0; j < nclusters; j++) {
	if(cmask_tmp[i][j] > 0) {
	  cdata[i][j] /= cmask[i][j];
          if(cmask) {cmask[i][j] = 1;}
        } else {
	  cdata[i][j] = T_FLOAT_MAX;
	  // FIXME: validate [”elow] ... and if [”elow] does not hold .. then update our itanlizaiton-procedure.
	  if(mask) {assert(!mask[i][j]);}
	  assert(isOf_interest(cdata[i][j]) == false);
        }
      }
    }
  }  
}
