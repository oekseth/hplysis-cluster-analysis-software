#ifndef kt_earth_moving_distance_emd_h
#define kt_earth_moving_distance_emd_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_earth_moving_distance_emd.h
   @brief provide an optimied implementation of the "Earth Moving Distance (EMD)" (oekseth, 06. feb. 2018)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2018).
   @remarks among others used in algorithms and applications of image anlaysis. 
**/


//#include "type_2d_float_nonCmp_uint.h"
#include "kt_matrix_base.h"


#endif //! EOF
