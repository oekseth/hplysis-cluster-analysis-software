#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
//#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction

//! --------------------------------------------------------
//! Start: not-adjsut-wrt.-sum:
#define __templateInternalVariable__isTo_updateCountOf_vertices 0
//!
//! Apply logics:
#define TEMPLATE_distanceConfiguration_useWeights 0
#define TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany functName__xmtWeight
#include "template_sparseSimMetric__nonOptimized.c"
//!
//! Apply logics:
#undef TEMPLATE_distanceConfiguration_useWeights
#undef TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany
#define TEMPLATE_distanceConfiguration_useWeights 1
#define TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany functName__weight
#include "template_sparseSimMetric__nonOptimized.c"
//! --------------------------------------------------------
//! Start: Adjsut-wrt.-sum:
#undef TEMPLATE_distanceConfiguration_useWeights
#undef TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany
#undef __templateInternalVariable__isTo_updateCountOf_vertices
#define __templateInternalVariable__isTo_updateCountOf_vertices 1
//!
//! Apply logics:
#define TEMPLATE_distanceConfiguration_useWeights 0
#define TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany functName__adjust_xmtWeight
#include "template_sparseSimMetric__nonOptimized.c"
//!
//! Apply logics:
#undef TEMPLATE_distanceConfiguration_useWeights
#undef TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany
#define TEMPLATE_distanceConfiguration_useWeights 1
#define TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany functName__adjust_weight
#include "template_sparseSimMetric__nonOptimized.c"
#undef TEMPLATE_distanceConfiguration_useWeights
#undef TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany
//! --------------------------------------------------------

#undef __templateInternalVariable__isTo_updateCountOf_vertices

//! Reset for 'oder cofniguraitons':
#undef TEMPLATE_distanceConfiguration_useWeights
#undef TEMPLATE_distanceConfiguration_macroFor_metric
#undef TEMPLATE_distanceConfiguration_macroFor_metric_SSE
#undef TEMPLATE_distanceConfiguration_macroFor_metric_includePower 
#undef TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax
#undef TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin
#undef TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate
#undef TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices
#undef TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#undef TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction
#undef TEMPLATE_NAMEOF_MAIN_FUNC__sparse__manyMany
// --- 
#undef functName__xmtWeight
#undef functName__weight
#undef functName__adjust_xmtWeight
#undef functName__adjust_weight
#undef TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#undef TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
#undef TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#undef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#undef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#undef TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#undef TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#undef TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
