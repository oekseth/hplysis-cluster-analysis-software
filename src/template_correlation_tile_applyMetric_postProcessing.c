  /**
   @brief apply post-processing metrics (oekseth, 06. sept. 2016).
   @remarks in below we assuem for 'scalar non-compelx distance-emtics' that a T_FLOAT_MAX value indicatees a 'mask', ie, where we for T_FLOAT_MAX cases does Not comptue the post-procesisng-funciotn (oekseth, 06. des. 2016)
**/


#if(__templateInternalVariable__isTo_updateCountOf_vertices == 1)
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 1) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
const uint ncolsAndWeight = objLocalComplex_nonSSE.cnt_interesting_cells;
#else
const uint ncolsAndWeight = ncols; 
#endif
#else
const uint ncolsAndWeight = ncols; 
#endif
const t_float ncolsAndWeight_inverse = (ncolsAndWeight != 0) ? 1 / (t_float)ncolsAndWeight : 0; 
{  
  {
    //t_float result = 8; printf("FIXME: remove this at %s:%d\n", __FILE__, __LINE__);  
    //! ----------------------------
/* #if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar) */
/*     printf("result-before-adjustment=%f, at %s:%d\n", result, __FILE__, __LINE__); //! ie, to investigate the 'complaint' about the valeu being used 'un-intalized'. */
/* #endif */


    //! start: distance-computation: ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 0) ) //! postProcessCase(1.a) non-complex, not-count, not-power 
    //! Compute:
    //printf("FIXME: include [below], at %s:%d\n", __FILE__, __LINE__);  
    if( (result != T_FLOAT_MAX) & (result != T_FLOAT_MIN) ) {
      //assert_possibleOverhead(isinf(result)  == false); //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
      //! Validate: 
      //assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result) == false); //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
      //assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result) == false); //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
      //! Apply logics:
      result_adjusted = TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction(result); //! ie, udpat he result-varialbe witht he updated results.
      //! Validate: 
      // assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_adjusted) == false); //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017) 
      // assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_adjusted) == false); //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
      //printf("\tresult_adjusted=%f, given result=%f, at %s:%d\n", result_adjusted, result, __FILE__, __LINE__);
    } else {result_adjusted = T_FLOAT_MAX;} //! ie, then set the latter 'to an empty value'.
/* #if(configureDebug_useExntensiveTestsIn__tiling == 1)  //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017) */
/*  if(isinf(result_adjusted)) { */
/*    fprintf(stderr, "!!\t The 'produced' result was initinte given input-value=%f, ie, please investgigate. Observation at [%s]:%s:%d\n", result, __FUNCTION__, __FILE__, __LINE__); */
/*    assert(false); */
/*  } */
/* #endif */

    //    printf("result=%f, result_adjusted=%f, at %s:%d\n", result, result_adjusted, __FILE__, __LINE__);
#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 1) ) //! postProcessCase(1.b) non-complex, count, not-power 
    //! Compute:
//printf("FIXME: include [below], at %s:%d\n", __FILE__, __LINE__);  
 //if(result != T_FLOAT_MAX) {
 if( (result != T_FLOAT_MAX) & (result != T_FLOAT_MIN) ) {
      result_adjusted = TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction(result, ncolsAndWeight, ncolsAndWeight_inverse); //! ie, udpat he result-varialbe witht he updated results.
      //! Validate: 
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_adjusted) == false);
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_adjusted) == false);
      // printf("\tresult_adjusted=%f, given result=%f, ncolsAndWeight=%u, ncolsAndWeight_inverse=%f, at %s:%d\n", result_adjusted, result, ncolsAndWeight, ncolsAndWeight_inverse, __FILE__, __LINE__);
    } else {result_adjusted = T_FLOAT_MAX;} //! ie, then set the latter 'to an empty value'.
#if(configureDebug_useExntensiveTestsIn__tiling == 1) 
 if(isinf(result_adjusted)) {
   fprintf(stderr, "!!\t The 'produced' result was initinte given input-value=%f, ie, please investgigate. Observation at [%s]:%s:%d\n", result, __FUNCTION__, __FILE__, __LINE__);
   assert(false);
 }
#endif

    //printf("result=%f, result_adjusted=%f, at %s:%d\n", result, result_adjusted, __FILE__, __LINE__);
#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 1) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 0) ) //! postProcessCase(1.d) non-complex, not-count, power 
    //! Compute:
//printf("result=%f, pwo=%f, tmp-result=%f, at [%s]:%s:%d\n", (t_float)result, self.configMetric_power, mathLib_float_pow(result, self.configMetric_power), __FUNCTION__, __FILE__, __LINE__);
 if( (result != T_FLOAT_MAX) & (result != T_FLOAT_MIN) ) {
 //if(result != T_FLOAT_MAX) {
   //const t_float configMetric_power = self.configMetric_power;
      result_adjusted = TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction(result, configMetric_power); //! ie, udpat he result-varialbe witht he updated results.
      //! Validate: 
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_adjusted) == false);
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_adjusted) == false);
      //printf("\tresult_adjusted=%f, given result=%f, power=%f, at %s:%d\n", result_adjusted, result, self.configMetric_power, __FILE__, __LINE__);
 } else {result_adjusted = T_FLOAT_MAX;} //! ie, then set the latter 'to an empty value'.
#if(configureDebug_useExntensiveTestsIn__tiling == 1) 
 if(isinf(result_adjusted)) {
   fprintf(stderr, "!!\t The 'produced' result was initinte given input-value=%f, ie, please investgigate. Observation at [%s]:%s:%d\n", result, __FUNCTION__, __FILE__, __LINE__);
   assert(false);
 }
#endif

    // printf("result=%f, result_adjusted=%f, at %s:%d\n", result, result_adjusted, __FILE__, __LINE__);
#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) ) //! ie, case (2.a):
    //printf("FIXME: include [below], at %s:%d\n", __FILE__, __LINE__);  
    if( (objLocalComplex_nonSSE.numerator != T_FLOAT_MAX) & (objLocalComplex_nonSSE.numerator != T_FLOAT_MIN) ) {
      result_adjusted = TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction(objLocalComplex_nonSSE); //! ie, udpat he result-varialbe witht he updated results.
      //! Validate: 
      /* assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_adjusted) == false); */
      /* assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_adjusted) == false); */
    } else {result_adjusted = T_FLOAT_MAX;} //! ie, then set the latter 'to an empty value'.
#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 1) ) //! ie, case (2.b):
//printf("FIXME: include [below], at %s:%d\n", __FILE__, __LINE__);  
    if( (objLocalComplex_nonSSE.numerator != T_FLOAT_MAX) & (objLocalComplex_nonSSE.numerator != T_FLOAT_MIN) ) {
      //const t_float configMetric_power = self.configMetric_power;
      result_adjusted = TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction(objLocalComplex_nonSSE, configMetric_power); //! ie, udpat he result-varialbe witht he updated results.
      //! Validate: 
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_adjusted) == false);
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_adjusted) == false);
    } else {result_adjusted = T_FLOAT_MAX;} //! ie, then set the latter 'to an empty value'.
#else //! thenw e pritn a warning, ie, to hopfuylly identify any bugs (oekseth, 06. sept. 2016)
#error "Error: seems like your compilation-configuraiton-parameters were in-consistent, ie, please investigate the documetnaiton or give the developer an heads-up at [oekseth@gmail.com]"
#endif


 //printf("# \t result: '%f', at %s:%d\n", result_adjusted, __FILE__, __LINE__);
    //! end: distance-computation: ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
  }
}
