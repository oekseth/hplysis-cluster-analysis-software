#ifndef hp_distanceCluster_h
#define hp_distanceCluster_h

#include "e_kt_api_dynamicKMeans.h"
#include "kt_api__centerPoints.h"
#include "def_memAlloc.h"
#include "hp_distance.h"

/**
   @struct s_hp_distanceCluster_t 
   @brief provide wrapper-logics to find the closest distance between non-assigned vertices versus assigned vertices (oekseth, 06. jul. 2017).
   @remarks compelxity conserns how to compare vertex-sets, ie, through use of max-scores, min-scores, average-scores, etc. 
 **/
typedef struct s_hp_distanceCluster {
  e_kt_api_dynamicKMeans_t metric_centroid;
  s_kt_correlationMetric_t objMetric__postMerge;
  uint minCntVertex_inEachCluster;
} s_hp_distanceCluster_t;

/**
   @brief itnalize a configruation-object for membership-associations (oekseth, 06. jul. 2017).
   @retunr an intlized object
 **/
static s_hp_distanceCluster_t init__s_hp_distanceCluster_t() {
  s_hp_distanceCluster_t self;
  //  e_kt_api_dynamicKMeans_t 
  // FIXME[tut+eval]: consider to elvauate differnet eprmtautison wrt. this.
  self.minCntVertex_inEachCluster = 2;
  self.metric_centroid = e_kt_api_dynamicKMeans_AVG; //self.config.centroidMetric__vertexVertex__center;
  self.objMetric__postMerge = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1);
  //! @return
  return self;
}

/**
   @brief idnietfy memberships between non-assigned vertices versus vertices which are part of explcitly deifned clsutes (oekseth, 06. jul. 2017).
   @param <self> si the cofniguraiotn-object which hold the deitlas/cofniguraitons to apply.
   @param <obj_mapOf_disjtointForestIDs> is the clsuter-dienties of the veriteces: use UINT_MAX to makr a vertex as 'not-asisnged'.
   @param <mat_input> is the simlairty-matirx to be used when idneitfying/evlauating vertex-simalrity
   @param <retList> is the new-constructed list of vertex-simliarites
   @return true if the operaion was assuemd being a success.
 **/
bool apply__s_hp_distanceCluster_t(s_hp_distanceCluster_t *self, s_kt_list_1d_uint_t *obj_mapOf_disjtointForestIDs, const s_kt_matrix_t *mat_input, s_kt_list_1d_uint_t *retList);

#endif //! EOF
