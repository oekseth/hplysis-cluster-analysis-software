#ifndef correlation_macros__distanceMeasures_h
#define correlation_macros__distanceMeasures_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlation_macros__distanceMeasures
   @brief provide functiosn for comptuation and evaluation of different distance-measures
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


#ifndef macro_min
#define macro_min(x, y)	((x) < (y) ? (x) : (y))
#endif
#ifndef macro_max
#define	macro_max(x, y)	((x) > (y) ? (x) : (y))
#endif
#ifndef macro_minus
#define macro_minus(term1, term2) ({(t_float)term1 - (t_float)term2;})
#endif
#ifndef macro_pluss
#define macro_pluss(term1, term2) ({(t_float)term1 + (t_float)term2;})
#endif
#ifndef macro_mul
#define macro_mul(term1, term2) ({(t_float)term1 * (t_float)term2;})
#endif
#ifndef macro_div
#define macro_div(term1, term2) ({(t_float)term1 / (t_float)term2;})
#endif
#ifndef macro_div_handleZeroInDeNominator
#define macro_div_handleZeroInDeNominator(term1, term2) ({ (term2 != 0) ? term1 / term2 : 0;})
#endif
#ifndef macro_div_handleZeroInNumeratorAndDeNominator
#define macro_div_handleZeroInNumeratorAndDeNominator(term1, term2) ({ ( (term1 != 0) & (term2 != 0) ) ? term1 / term2 : 0;})
#endif


// FIXME: wrtie a perofrmance-comparison of [”elow] impelemtantion-strategies.

// FIXME: wrt. "Brownian correlation" ... write a use-case application and update both our API and our web-itnerface <-- first try to mathemically express/rexpalin the nce in corrleation-results ... ieg, to arpametriz ethe differnet corrleation-emasrues 'described in thsi file (oesketh, 06. sept. 2016)
// FXIME: write an example case/applciaton where we use "brownian" correlation-measure ... and then update both oru API-itnerface and our web-page ... and test that the latte rprodcue/rsult in ifnroamtive results

// FIXME: wrt. a rpesetnaiton consider to use a comparison between "appleis and pears" as a correlation-example ... ask people for why the fruit "organige" 'recfived' its name (true asnwer: from Sanscrit, ie, Not from the color "orange": the color-name was conied based on the fruits color, ie, not the often-assumed assumption of 'an inverse relatonsp') ... and then sue the latter as a methapor wrt. comparison of diffneret  "Pearson's correlation-metric-appraoches" ... ie, whtat differnces in corrlation-scores may indicate.

//! Adjust the a term (comptued based on a correlation-measure) wieth the assicted weight:
#define correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index) ({ \
  const t_float weight_local = weight[weight_index];			\
  const t_float input_result_local = weight_local*term;			\
  input_result_local;}); //! ie, returnt he input_result.


//! Compute a subset of the Spearmns and 'genriec' correlations, focusing only on the sum of both (and not comptue the 'sum of each').
/* #define correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly(value1, value2, term) ({term = value1 * value2;}) */
#define correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly(value1, value2) ({t_float term = 0; term = value1 * value2; term;})
/* //! @return an assicated correlation-score, wehre teh valeus are adjsuted wrt. the weights: */
/* #define correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly_weight(value1, value2, weight, weight_index) ({ \ */
/*   t_float term = 0; correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly(value1, value2, term); \ */
/*   const t_float ret_val = correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index);\ */
/*   ret_val;}) //! ie, return the adjusted weight */

#define correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly__SSE(value1, value2) ({VECTOR_FLOAT_MUL(value1, value2);}) //! ie, treun.

#define metricMacro__constants__defaultValue__postProcess__noMatch T_FLOAT_MAX
#define metricMacro__constants__defaultValue__postProcess__allMatches 0 //! ie, 'zero distance' (though Not any declared negative distnace) (oekseth, 06. des. 2016).

#define metric_macro_defaultFunctions__postProcess__validateEmptyCase(input_result) ({(input_result != 0) ? input_result : metricMacro__constants__defaultValue__postProcess__allMatches ;}) //! which is a wrapepr-fucntion to simplify our code-writing-clogics wrt. generalised functiosn (oekseth, 06. des. 2016)
#define metric_macro_defaultFunctions__postProcess__none(input_result) ({(input_result != 0) ? input_result : metricMacro__constants__defaultValue__postProcess__allMatches ;}) //! which is a wrapepr-fucntion to simplify our code-writing-clogics wrt. generalised functiosn (oekseth, 06. des. 2016)
#define metric_macro_defaultFunctions__postProcess__allMatches(input_result) ({(input_result != 0) ? input_result : metricMacro__constants__defaultValue__postProcess__allMatches ;}) //! which is a wrapepr-fucntion to simplify our code-writing-clogics wrt. generalised functiosn (oekseth, 06. des. 2016)

//! @return "(1 - input_result)", ie, to 'stanrize' different euqaitons such as alternative formuatlions of equaitons descirbed in ["http://users.uom.gr/~kouiruki/sung.pdf"]
//! @remakrs the [below] macor uis used in order to simpliy 'classificoant'/'identificaiotn' of our distance-macors.
#define metric_macro_defaultFunctions__postProcess__1_minusValue(input_result) ({ \
      /* assert(input_result <= 1); assert(input_result >= -1); */ \
  t_float __result = 0; if(input_result != 0) {__result = 1/input_result;} /*! ie, to ahndle/address the cases where valeu is not a gemotric dsitrubion, ie, orutisde range [0, 1]*/ \
  __result;})
//(1 - input_result);}) //! which is a wrapepr-fucntion to simplify our code-writing-clogics wrt. generalised functiosn (oekseth, 06. des. 2016)
      //(input_result != 0) ? (1 - input_result) : metricMacro__constants__defaultValue__postProcess__noMatch;}) //! which is a wrapepr-fucntion to simplify our code-writing-clogics wrt. generalised functiosn (oekseth, 06. des. 2016)
#define metric_macro_defaultFunctions__postProcess__1_minusValue_xmtDefaultAdjustment(input_result) ({ \
      metric_macro_defaultFunctions__postProcess__1_minusValue(input_result);})
  //assert(input_result <= 1); assert(input_result >= -1); 1 - input_result;}) //! which is a wrapepr-fucntion to simplify our code-writing-clogics wrt. generalised functiosn (oekseth, 06. des. 2016)
#define metric_macro_defaultFunctions__postProcess__multiplyBy_constant(input_result, constant) ({ (input_result * constant);})


// metric_macro_defaultFunctions__postProcess__scalar_numeratorDenumerator_divide__subCase__input_resultMayNeverBeInfininty


// FIXME[perofrmance]: write a perofmrance-comparsion wrt. [”elow]
//! @return the input_result of a division-operation.
#define metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator) ({ \
    /*! Compute: */ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (numerator != 0) && (denumerator != 0) ) {input_result = numerator / denumerator;} \
    /*! Return: */ \
      input_result;})
//! @return the input_result of a division-operation.
#define metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__old(numerator, denumerator) ({ \
    /*! What we expect (oekseth, 06. mar. 2017) */ \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(numerator)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(denumerator) == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(numerator)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(denumerator) == false); \
    /*! Compute: */ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (numerator != 0) && (denumerator != 0) ) {input_result = numerator / denumerator;} \
    /*! What we expect (oekseth, 06. mar. 2017) */ \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(input_result)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(input_result) == false); \
    /*! Return: */ \
      input_result;})
#define metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator) ({ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (denumerator != 0) ) {input_result = numerator / denumerator;} input_result;})

//! @return the input_result of a division-operation.
#define metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(obj) ({ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (obj.numerator != 0) && (obj.denumerator != 0) ) {input_result = obj.numerator / obj.denumerator;} input_result;})
#define metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(obj) ({ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (obj.denumerator != 0) ) {input_result = obj.numerator / obj.denumerator;} input_result;})
#define metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty_multiplyBy_constant(obj, constant) ({ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (obj.denumerator != 0) ) {input_result = constant * obj.numerator / obj.denumerator;} input_result;})

#define metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__divideByWeight(obj, ncolsAndWeight, ncolsAndWeight_inverse) ({ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if__divideByWeight( __divideByWeight(obj.numerator != 0) && __divideByWeight(obj.denumerator != 0) ) {input_result = obj.numerator / obj.denumerator;} input_result;})
#define metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty__divideByWeight(obj, ncolsAndWeight, ncolsAndWeight_inverse) ({ \
      t_float input_result = metricMacro__constants__defaultValue__postProcess__allMatches; if( (obj.denumerator != 0) ) {input_result = obj.numerator / obj.denumerator;} input_result;})




//! ****************************************************
//! --          Minkowski                  --
#include "metricMacro_minkowski.h"


//! ****************************************************
//! --          Absolute differences                  --
#include "metricMacro_absoluteDifference.h"


//! ****************************************************
//! --          Intersection-metrics                  --
#include "metricMacro_intersection.h"

//! ****************************************************
//! --          Inner Product Metrics                  --
#include "metricMacro_innerProduct.h"


//! ****************************************************
//! --           Fidelity Metrics (or: Squared-chord metrics)                  --
#include "metricMacro_fidelity.h"



//! ****************************************************
//! --          Squared metrics                  --
#include "metricMacro_squared.h"



//! ****************************************************
//! --          Shannon's metrics                  --
#include "metricMacro_shannon.h"

/* //! @return the correlations-core for: */
/* #define correlation_macros__distanceMeasures__(term1, term2) ({}) */
/* //! @return the correlations-core for: */
/* #define correlation_macros__distanceMeasures____SSE(term1, term2) ({}) */


//! ****************************************************
//! --             Combinations of metrics               --
#include "metricMacro_combination.h"


/* //! @return the correlations-core for: */
/* #define correlation_macros__distanceMeasures__(term1, term2) ({}) */
/* //! @return the correlations-core for: */
/* #define correlation_macros__distanceMeasures____SSE(term1, term2) ({}) */



//! ****************************************************
//! --           Vicissitude metrics (or: downward mutability metrics (where latter is conined/used by oekseth))                 --
#include "metricMacro_downwardMutability.h"

/* //! @return the correlations-core for: */
/* #define correlation_macros__distanceMeasures__(term1, term2) ({}) */
/* //! @return the correlations-core for: */
/* #define correlation_macros__distanceMeasures____SSE(term1, term2) ({}) */


//! Compute the Spearmans rank-correlation (oekseth, 06. sept. 2016)
//! @remarks the expected input is expted to be two the anrks of two vertices, eg, when comapring two rows
#define correlation_macros__distanceMeasures__spearman(value1, value2, result_sum, result_denom1, result_denom2) ({ \
  assert(value1 != T_FLOAT_MAX); assert(value2 != T_FLOAT_MAX); \
  /*! Update the result:*/					\
  result_sum += value1 * value2;					\
  result_denom1 += value1 * value1; result_denom2 += value2 * value2;	\
    })
//! Comptue the separmans
//! @remarks simialr to "correlation_macros__distanceMeasuresupdateDistance__spearman(..)" with difference that thsi funciton use SSE intristnicts to copmtue the results
#define correlation_macros__distanceMeasures__spearman_SSE(vec_1, vec_2, vec_result, vec_denom1, vec_denom2) ({ \
  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(vec_1, vec_2)); \
  vec_denom_1 = VECTOR_FLOAT_ADD(vec_denom_1, VECTOR_FLOAT_MUL(vec_1, vec_1)); \
  vec_denom_2 = VECTOR_FLOAT_ADD(vec_denom_2, VECTOR_FLOAT_MUL(vec_2, vec_2)); \
    })


//! Compute the post-process value for Spearman: to be used after two feature vectors/rows are compared.
#define correlation_macros__distanceMeasures__postProcessCorrelation__spearman(result, denom1, demon2, ncols, ncols_inv) ({ \
  t_float scalar_result = 1; /*ie, a 'no-match'*/			\
  if( (result !=0.) && (denom1 != 0.) && (denom2 !=0.) ) {		\
  scalar_result = result * ncols_inv; denom1 *= ncols_inv; denom2 *= ncols_inv; \
  const t_float avgrank = 0.5*(ncols - 1); /* Average rank */ \
  scalar_result -= avgrank * avgrank;					 \
  denom1 -= avgrank * avgrank;					 \
  denom2 -= avgrank * avgrank;						\
  if( (denom1 > 0) && (denom2 > 0) ) { \
  assert((denom1*denom2) != 0);						\
  scalar_result = scalar_result / sqrt(denom1*denom2);				\
  scalar_result = 1. - scalar_result;							\
  } else {scalar_result = 1.0;}  \
  } else {scalar_result = 1.0;}  \
  scalar_result;}) //! ie, return the correlation-result:

//! Compute the post-process value for Spearman: to be used after two feature vectors/rows are compared.
#define correlation_macros__distanceMeasures__postProcessCorrelation__spearman__slow(result, denom1, demon2, ncols) ({ \
  t_float scalar_result = 1; /*ie, a 'no-match'*/			\
  if( (result !=0.) && (denom1 != 0.) && (denom2 !=0.) ) {		\
  scalar_result = result / ncols; denom1 /= ncols; denom2 /= ncols; \
  const t_float avgrank = 0.5*(ncols - 1); /* Average rank */ \
  scalar_result -= avgrank * avgrank;					 \
  denom1 -= avgrank * avgrank;					 \
  denom2 -= avgrank * avgrank;						\
  if( (denom1 > 0) && (denom2 > 0) ) { \
  assert((denom1*denom2) != 0);						\
  scalar_result = scalar_result / sqrt(denom1*denom2);				\
  scalar_result = 1. - scalar_result;							\
  } else {scalar_result = 1.0;}  \
  } else {scalar_result = 1.0;}  \
  scalar_result;}) //! ie, return the correlation-result:


//! Compute the post-process value for Pearson (basic): to be used after two feature vectors/rows are compared.
#define correlation_macros__distanceMeasures__postProcessCorrelation__pearson_basic(result, sum1, sum2, denom1, demon2, ncols, ncols_inv) ({ \
  t_float scalar_result = result; \
  scalar_result -= sum1 * sum2 * ncols_inv; \
  denom1 -= sum1 * sum1 * ncols_inv; \
  denom2 -= sum2 * sum2 * ncols_inv;		\
  if( (denom1 > 0) && (denom2 > 0) ) { \
    scalar_result = scalar_result / sqrt(denom1*denom2);  \
    scalar_result = 1. - scalar_result;			  \
  } else {scalar_result = 1.0;} 				  \
  scalar_result;}) //! ie, return the correlation-result:

//! Compute the post-process value for Pearson (basic): to be used after two feature vectors/rows are compared; slow version.
#define correlation_macros__distanceMeasures__postProcessCorrelation__pearson_basic__slow(result, sum1, sum2, denom1, demon2, ncols) ({ \
  t_float scalar_result = result; \
  scalar_result -= sum1 * sum2 / ncols; \
  denom1 -= sum1 * sum1 / ncols; \
  denom2 -= sum2 * sum2 / ncols;		\
  if( (denom1 > 0) && (denom2 > 0) ) { \
    scalar_result = scalar_result / sqrt(denom1*denom2);  \
    scalar_result = 1. - scalar_result;			  \
  } else {scalar_result = 1.0;} 				  \
  scalar_result;}) //! ie, return the correlation-result:


//! Compute the post-process value for Pearson (absolute): to be used after two feature vectors/rows are compared.
//! @remarks Result-base = abs( sum( w(tail)*w(head_1,tail)*w(head_2,tail) ) - ( sum( w(tail)*w(head_1,tail) ) * (sum (w(tail)*w(head_2,tail) ) ) / sum(w(tail)) ) ) 
#define correlation_macros__distanceMeasures__postProcessCorrelation__pearson_absolute(result, sum1, sum2, denom1, demon2, ncols, ncols_inv) ({ \
  t_float scalar_result = result; \
  scalar_result -= sum1 * sum2 * ncols_inv; \
  denom1 -= sum1 * sum1 * ncols_inv; \
  denom2 -= sum2 * sum2 * ncols_inv; \
  if( (denom1 > 0) && (denom2 > 0) ) { \
  scalar_result = mathLib_float_abs(scalar_result) / sqrt(denom1*denom2); \
  scalar_result = 1. - scalar_result; \
  } else {scalar_result = 1.0;} 				  \
  scalar_result;}) //! ie, return the correlation-result:


//! Compute the post-process value for Pearson (absolute): to be used after two feature vectors/rows are compared; slow version.
#define correlation_macros__distanceMeasures__postProcessCorrelation__pearson_absolute__slow(result, sum1, sum2, denom1, demon2, ncols) ({ \
  t_float scalar_result = result; \
  scalar_result -= sum1 * sum2 / ncols; \
  denom1 -= sum1 * sum1 / ncols; \
  denom2 -= sum2 * sum2 / ncols; \
  if( (denom1 > 0) && (denom2 > 0) ) { \
  scalar_result = mathLib_float_abs(scalar_result) / sqrt(denom1*denom2); \
  scalar_result = 1. - scalar_result; \
  } else {scalar_result = 1.0;} 				  \
  scalar_result;}) //! ie, return the correlation-result:

//! Compute the post-process value for Pearson (un-centered): to be used after two feature vectors/rows are compared.
#define correlation_macros__distanceMeasures__postProcessCorrelation__pearson_unCentered(result, sum1, sum2, denom1, demon2) ({ \
  t_float scalar_result = result; \
  /* FIXME: validate that a cell with value=0 is not of interest. **/ \
  const char flag = (sum1 || sum2); \
  if(flag) {\
    if( (denom1 > 0) && (denom2 > 0) ) {			  \
    scalar_result = scalar_result / sqrt(denom1*denom2); \
    scalar_result = 1. - scalar_result;			 \
    }								  \
  } else {scalar_result = 1.0;} 				  \
  scalar_result;}) //! ie, return the correlation-result:


//! Compute the post-process value for Pearson (un-centered absolute): to be used after two feature vectors/rows are compared.
#define correlation_macros__distanceMeasures__postProcessCorrelation__pearson_unCentered_absolute(result, sum1, sum2, denom1, demon2) ({ \
  t_float scalar_result = result; \
  /* FIXME: validate that a cell with value=0 is not of interest. **/ \
  const char flag = (sum1 || sum2);				      \
  if(flag) {\
    if( (denom1 > 0) && (denom2 > 0) ) {			  \
      scalar_result = mathLib_float_abs(scalar_result) / sqrt(denom1*denom2); \
      scalar_result = 1. - scalar_result;				\
    }								  \
  } else {scalar_result = 1.0;} 				  \
  scalar_result;}) //! ie, return the correlation-result:


#endif //! EOF
