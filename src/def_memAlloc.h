#ifndef def_memAlloc_h
#define def_memAlloc_h

/**
   @file def_memAlloc
   @brief describes and generalises a set of memory-allocations.
   @remarks used to simplify siwthcing between different 'states'.

   @remarks easy-to-do-bugs which we have observed is/conersn:
   - using an aritmentic expression (when calling the macrocs), eg, "allocate_1d_list_float(x+1, 0" instead of "const uitn size  = x+1; allocate_1d_list_float(size, 0);". Result is a 'vilent' crash of the softare
   @remarks usage: generic memory-allocatation-rotuines (eg, when a user is interested in allocating lists of their own data-strucutres):
   alloc_generic_type_1d(...); free_generic_type_1d(...);
   alloc_generic_type_2d(...); free_generic_type_2d(...);
   alloc_generic_type_3d(...); free_generic_type_3d(...);
   @remarks usage: type-specific memory-allocatation-rotuines: to simplify we only list a subset of the 'supported rotuines' in below:
   allocate_1d_list_float(...); free_1d_list_float(...);
   allocate_2d_list_float(...); free_2d_list_float(...);
   allocate_1d_list_uint(...); free_1d_list_uint(...);
   allocate_2d_list_uint(...); free_2d_list_uint(...);
 **/

#ifndef SWIG
#if !defined(__APPLE__) && !defined(__sun) 
#include <malloc.h>
#endif
#include <limits.h>
#endif //! #ifndef SWIG
#include "types_base.h"

//! Allcoate a new char-variable (oekseth, 06. mar. 2017)
#define allocOnStack__char(size, name) char name[size]; memset(name, '\0', size); //! ie, where a call to "allocOnStack__char(2000, str_local) 'translates' into "char str_local[2000]; memset(str_local, '\0', size);, and wehre a 'specific' strneght of latter cosnerns the 'densntiy' of the syntax (oekseth, 06. mar. 2017).
//! Allcoate and 'intaite using sprintf' a new char-variable (oekseth, 06. mar. 2017)
//! Not: i the "allocOnStack__char__sprintf(..)" we make use of a "variadic macro" (ie, vairable number of arguments).
#define allocOnStack__char__sprintf(size, name, str_pattern, ...) char name[size]; memset(name, '\0', size); sprintf(name, str_pattern, __VA_ARGS__); //! ie, where a call to "allocOnStack__char(2000, str_local) 'translates' into "char str_local[2000]; memset(str_local, '\0', size);, and wehre a 'specific' strneght of latter cosnerns the 'densntiy' of the syntax (oekseth, 06. mar. 2017).

// FIXME[jc]: <-- we have itnroduced 'this' given in-correct answeres when suign "memset(..)" to integer-pointers' .. do you have a work-arodunw rt. 'this'?
// FIXME: write an itnrii-verison of 'this' ... and evaluate the eprformacne-ebenft
//#define memset_local(arr, default_value, arr_size) ({printf("for a list of %u elements sets default-value=%f, at %s:%d\n", arr_size, (float)default_value, __FILE__, __LINE__); for(uint i = 0; i < (uint)arr_size; i++) {arr[i] = default_value;}})
#define memset_local(arr, default_value, arr_size) ({for(uint i = 0; i < (uint)arr_size; i++) {arr[i] = default_value;}})
//! A safe appraoch to reset memory for a 2d-pointer:
#define memset_local_2d(arr_2d,  cnt_rows, cnt_columns, default_value) ({		\
  assert(arr_2d); assert(cnt_rows); assert(cnt_columns); assert(arr_2d[0]); \
  const long long unsigned int arr_size = cnt_rows * cnt_columns; \
  for(loint row_id = 0; row_id < (loint)cnt_rows; row_id++) {for(loint col_id = 0; col_id < (loint)cnt_columns; col_id++) {arr_2d[row_id][col_id] = default_value;}}})
//  for(long long unsigned int i = 0; i < arr_size; i++) {arr_2d[0][i] = default_value;}})


// FIXME[JC]: do you belive there is a differnece in exueciton-time (of memory-accesses) assicated to "new [] " and "malloc(..)" if we muse "memset(..)" (or for-loops) .... ie, wrt. 'locality in time of requested/required memory-access'? <-- if the latter is the case, would we then expect this 'issue'/'case' to be mroe signfinat if paralll/muliple thresas are allocating meory? <-- if so, are there anby known low-level approach to effciently allocate memory (eg, to avoid 'low-level mamory-locks', ie, if such exist)

//! A standarized approach to ensure that memory is correctly allocated into chunks:
// FIXME[jc]: what are the 'correct values' to be used in the "aligned(..)" call?
#define memAlign_preAllocated  __attribute__ ((aligned (32))) 

#ifndef macroConfig_use_posixMemAlign
//#define macroConfig_use_posixMemAlign 0 //! ie, our default assumption
#define macroConfig_use_posixMemAlign 1 //! ie, our default assumption
#endif


//! *********************************************************
//! Provide lgocis for correctly idetnifying the size of 2d-memory allocatiosn.
//! Note: if this funciton is nto called for 2d-memory-allcoations then each "2d_data[i][0]" may not be aligned in memory-chunks, ie, which ...??... 
// FIXME[jc]: ... may you itnrospect upon why this proceudre-udpate is neccesary ... ie, why the 'default memalign-offset' does not provide the correct results?
//! ********
#if macroConfig_use_posixMemAlign == 1 //! then we assume that Intel's intrisntics (eg, SSE code-standards) is ot be used/applied.
    //! @return the updated size wrt. alignment of memory for 2d-lists.
#define __macroInline_getSizeOut_AlignedMemory(size_outer, block_size) ({ \
      const float factor = (float)size_outer / (float)block_size; const float factor_ceil = (float)ceil(factor); \
      const float delta = factor_ceil - factor; \
      const uint offset = (uint)(delta*block_size); \
      const uint expected_new_size_out = (uint)factor_ceil * (uint)block_size; \
      const uint new_size_out = size_outer + offset; \
      assert(expected_new_size_out == new_size_out); \
      /**! // Compute the inner size, where "total_size" is expected to be implcitly rpovided by the caller, ie, through the 'inlining' of this macro. **/ \
      /* total_size = new_size_out * size_in;*/				\
      /*! @return the updated 'outer size':*/ \
      new_size_out; }) //! ie, the return-statemetn usign the "ANSI C macro" syntax.

#else
    //! @return the updated size wrt. alignment of memory for 2d-lists.
#define __macroInline_getSizeOut_AlignedMemory(size_outer, block_size) ({ size_outer;}) //! ie, a 'dummy' wrapper.
#endif

//! *********************************************************
//!
//! 
//! Masking an HCA: Allcoate a tempraory lost to hold the memory-pointers:
#define MF__getMemRef_data(data, nrows) ({t_float **mem_ref = NULL; alloc_generic_type_2d(t_float, mem_ref, nrows); for(uint i = 0; i < nrows; i++) {mem_ref[i] = data[i];} mem_ref;})
#define MF__getMemRef_mask(mask, nrows) ({uint **mem_ref = NULL; alloc_generic_type_2d(uint, mem_ref, nrows); for(uint i = 0; i < nrows; i++) {mem_ref[i] = mask[i];} mem_ref;})
//! De-allcote for both the masks and for the data.
#define MF__free__memRef(mem_ref_data, mem_ref_mask) ({if(mem_ref_data) {free_generic_type_2d_wrapper(mem_ref_data, nrows); mem_ref_data = NULL;} if(mem_ref_mask) {free_generic_type_2d_wrapper(mem_ref_mask, nrows); mem_ref_mask = NULL;}})


//! *********************************************************
//! Tailor the memory-allocaitons for the system and usage:
//! ********
#if macroConfig_use_posixMemAlign == 1 //! then we assume that Intel's intrisntics (eg, SSE code-standards) is ot be used/applied.
//! Start: "Posix memalgin" ----------------------------------------------------------

// FIXME[JC]: may you suggest different valeus/combinatiosn wrt. [”elow] "SIZE_INTRI_ALIGNMENT_CHUNKS" ... eg, using system-levle-macros to ''cofnigure' [”elow]? <-- should we use "64" for __m256 systems?
#define SIZE_INTRI_ALIGNMENT_CHUNKS 32
//#define SIZE_INTRI_ALIGNMENT_CHUNKS 64

//! Allocate a 1d-list of data:
//#define alloc_generic_type_1d(type_t, ptr, size) (ptr = (type_t*)malloc(size * sizeof(type_t)))    
#define alloc_generic_type_1d_xmtMemset(type_t, ptr, size) ({const int result_data = (int)posix_memalign((void**)&ptr, SIZE_INTRI_ALIGNMENT_CHUNKS, (int)size * sizeof(type_t)); assert((int)result_data == 0); ptr;})
#define alloc_generic_type_1d(type_t, ptr, size, default_value) ({const int result_data = (int)posix_memalign((void**)&ptr, SIZE_INTRI_ALIGNMENT_CHUNKS, (int)size * sizeof(type_t)); assert((int)result_data == 0); memset_local(/*arr=*/ptr, (type_t)default_value, /*arr-size=*/size); ptr;})
//! Allocate a 2d-list of data:
//#define alloc_generic_type_2d__test(type_t, ptr, size) ({ptr = (char**)malloc(5 * sizeof(char*)); ptr;})
#define alloc_generic_type_2d(type_t, ptr, size) ({ptr = (type_t**)malloc(size * sizeof(type_t*)); ptr;})

#define alloc_generic_type_3d(type_t, ptr, size) ({ptr = (type_t***)malloc(size * sizeof(type_t**)); ptr;})
//! De-allocate a 1d-list of data:
//#define free_generic_type_1d(ptr) ({printf("free-pointer, at %s:%d\n", __FILE__, __LINE__);free(ptr);})    
#define free_generic_type_1d(ptr) (free(ptr))    
//! De-allocate a 2d-list of data:
#define free_generic_type_2d(ptr) (free(ptr))    
#define free_generic_type_2d_wrapper(ptr, nrows) ({for(loint row_id = 0; row_id < (loint)nrows; row_id++) {if(ptr[row_id]) {free(ptr[row_id]); ptr[row_id] = NULL;}} free(ptr);})
#define free_generic_type_3d(ptr) (free(ptr))    

//! complete: "Posix memalgin" ----------------------------------------------------------
#elif macroConfig_use_posixMemAlign == 0
//! Then we tailor the memory-allcoations for teither the stnadards of "ANSI C" or "C++":
#ifdef __cplusplus
//! Start: "C++" ----------------------------------------------------------

//! Allocate a 1d-list of data:
#define alloc_generic_type_1d_xmtMemset(type_t, ptr, size) ({ptr = new type_t[size]; ptr;})    
#define alloc_generic_type_1d(type_t, ptr, size, default_value) ({ptr = new type_t[size]; memset_local(/*arr=*/ptr, (type_t)default_value, /*arr-size=*/size); ptr;})    
//! Allocate a 2d-list of data:
#define alloc_generic_type_2d(type_t, ptr, size) ({ptr = new type_t*[size]; ptr;})    
#define alloc_generic_type_3d(type_t, ptr, size) ({ptr = new type_t**[size]; ptr;})
//! De-allocate a 1d-list of data:
#define free_generic_type_1d(ptr) (delete [] ptr)    
//! De-allocate a 2d-list of data:
#define free_generic_type_2d(ptr) (delete [] ptr)    
#define free_generic_type_2d_wrapper(ptr, nrows) ({for(loint row_id = 0; row_id < (loint)nrows; row_id++) {if(ptr[row_id]) {delete [] ptr[row_id]; ptr[row_id] = NULL;}} delete [] ptr;})
#define free_generic_type_3d(ptr) (delete [] ptr)    

//! complete: "C++" ----------------------------------------------------------
#else
//! Start: "ANSI C" ----------------------------------------------------------

//! Allocate a 1d-list of data:
				      // 
				      //#define alloc_generic_type_1d(type_t, ptr, size) ({printf("size=%d, at %s:%d\n", (int)size, __FILE__, __LINE__); ptr = (type_t*)malloc(size * sizeof(type_t)); ptr;})    
//#define alloc_generic_type_1d(type_t, ptr, size, default_size) ({printf("allocates a list of %u elements and size=%d, at %s:%d\n", size, (int)sizeof(type_t), __FILE__, __LINE__); ptr = (type_t*)malloc(size * sizeof(type_t)); memset_local(/*arr=*/ptr, default_value, /*arr-size=*/size); ptr;})    
#define alloc_generic_type_1d_xmtMemset(type_t, ptr, size) ({ptr = (type_t*)malloc(size * sizeof(type_t)); ptr;})    
#define alloc_generic_type_1d(type_t, ptr, size, default_value) ({ptr = (type_t*)malloc(size * sizeof(type_t)); memset_local(/*arr=*/ptr, (type_t)default_value, /*arr-size=*/size); ptr;})    
//! Allocate a 2d-list of data:
#define alloc_generic_type_2d(type_t, ptr, size) ({ptr = (type_t**)malloc(size * sizeof(type_t*)); ptr;})    
#define alloc_generic_type_3d(type_t, ptr, size) ({ptr = (type_t***)malloc(size * sizeof(type_t**)); ptr;})
//! De-allocate a 1d-list of data:
#define free_generic_type_1d(ptr) (free(ptr))    
//! De-allocate a 2d-list of data:
#define free_generic_type_2d(ptr) (free(ptr))    
#define free_generic_type_2d_wrapper(ptr, nrows) ({for(loint row_id = 0; row_id < (loint)nrows; row_id++) {if(ptr[row_id]) {free(ptr[row_id]); ptr[row_id] = NULL;}} free(ptr);})
#define free_generic_type_3d(ptr) (free(ptr))


//! complete: "ANSI C" ----------------------------------------------------------

#endif


//! Then we 'contniue' our elvuvation of the macroConfig_use_posixMemAlign parameer
#else 
#error "Add support for this case" //! ie, as we then need to add soemthing (oekseth, 06. june 2016).
assert(false) //! ie, to syntax-error to 'ensure' a compilation-abort-case.
#endif
//! Complete: *********************************************************

//#define alloc_generic_type_2d__wrapper(type_t, ptr, cnt_rows, cnt_columns, default_value) ({type_t **tmp_loc; assert(false);})
#define alloc_generic_type_2d__wrapper(type_t, ptr, cnt_rows, cnt_columns, default_value) ({type_t **tmp_loc; assert(cnt_rows > 0); assert(cnt_columns > 0); alloc_generic_type_2d(type_t, tmp_loc, cnt_rows); for(loint row_id = 0; row_id < (loint)cnt_rows; row_id++) {tmp_loc[row_id] = alloc_generic_type_1d(type_t, tmp_loc[row_id], cnt_columns, default_value);} tmp_loc;})


//! A generic proceudre to merge two 1d-lists (oekseth, 06. feb. 2017).
#define __KT__mergeGeneric_1d(type_t, ptr1, ptr1_size, ptr2, ptr2_size) ({	\
  assert(ptr1);   assert(ptr1_size > 0); \
  assert(ptr2);   assert(ptr2_size > 0); \
  const long long int __listCpy__size = ptr1_size + ptr2_size; const type_t __empty_0 = 0; \
  type_t *__listCpy = NULL; __listCpy = alloc_generic_type_1d(type_t, __listCpy, __listCpy__size, __empty_0); assert(__listCpy); \
  /*! Copy the data into the enw pointer:*/				\
  memcpy(__listCpy, ptr1, sizeof(type_t)*ptr1_size); \
  memcpy(__listCpy + ptr1_size, ptr2, sizeof(type_t)*ptr2_size); \
  __listCpy;}) //! ie, return

#ifndef NDEBUG
#define KT__mergeGeneric_1d(type_t, ptr1, ptr1_size, ptr2, ptr2_size) ({	\
  type_t *__listCpy = __KT__mergeGeneric_1d(type_t, ptr1, ptr1_size, ptr2, ptr2_size); assert(__listCpy); \
  /*! Validate correctness:: */					 \
  long long int m1 = 0; for(; m1 < (long long int)ptr1_size; m1++) {assert(__listCpy[m1] == ptr1[m1]);} assert(ptr1_size == m1); \
  for(; m1 < (long long int)ptr2_size; m1++) {assert(__listCpy[m1] == ptr1[m1-ptr1_size]);} \
  __listCpy;}) //! ie, return
#else
#define KT__mergeGeneric_1d(type_t, ptr1, ptr1_size, ptr2, ptr2_size) ({	\
  type_t *__listCpy = __KT__mergeGeneric_1d(type_t, ptr1, ptr1_size, ptr2, ptr2_size); assert(__listCpy); \
  __listCpy;}) //! ie, return
#endif

//! A proceudre to merge two 1d-lists (oekseth, 06. feb. 2017).
#define KT__merge_1d_list_uint(ptr1, ptr1_size, ptr2, ptr2_size) ({ KT__mergeGeneric_1d(uint, ptr1, ptr1_size, ptr2, ptr2_size);}) //! ie, return
#define KT__merge_1d_list_float(ptr1, ptr1_size, ptr2, ptr2_size) ({ KT__mergeGeneric_1d(t_float, ptr1, ptr1_size, ptr2, ptr2_size);}) //! ie, retunr
#define KT__merge_1d_list_char(ptr1, ptr1_size, ptr2, ptr2_size) ({ KT__mergeGeneric_1d(char, ptr1, ptr1_size, ptr2, ptr2_size);}) //! ie, return

#define KT__memset_1d_list(list, size, value) ({assert(list); assert(size != UINT_MAX); assert(size != 0); for(uint i = 0; i < size; i++) {list[i] = value;}})
#define KT__memset_2d_list(list_2d, size_in, size_out, value) ({assert(list_2d); assert(size_in != UINT_MAX); assert(size_in != 0); assert(size_out != 0); \
  for(uint in = 0; in < size_in; in++) {  \
  assert(list_2d[in]); \
  KT__memset_1d_list(list_2d[in], size_out, value);}})

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_float(size, default_value) ({ assert((char)default_value < CHAR_MAX); t_float *tmp = NULL; alloc_generic_type_1d(t_float, tmp, size); printf("sizeof=%d, size=%d, at %s:%d\n", (int)sizeof(t_float), (int)size, __FILE__, __LINE__); memset(tmp, (char)default_value, size*sizeof(t_float)); tmp = NULL; })

//#define allocate_1d_list_float(size, default_value) ({t_float *tmp = (t_float*)malloc(size*sizeof(t_float))})
//#define allocate_1d_list_float(size, default_value) ({ t_float *tmp = (t_float*)malloc(size*sizeof(t_float)); memset(tmp, (char)default_value, size*sizeof(t_float)); tmp = NULL; })
// FIXME: use [”elow] and remove [above]
#define allocate_1d_list_float(size, default_value) ({ assert((char)default_value < CHAR_MAX); t_float *tmp = NULL; alloc_generic_type_1d(t_float, tmp, size, default_value);  tmp; })
#define allocate_1d_list_float_explicitType_double(size, default_value) ({ assert((char)default_value < CHAR_MAX); double *tmp = NULL; alloc_generic_type_1d(double, tmp, size, default_value);  tmp; })
#define allocate_1d_list_sparseVec(size, default_value) ({ assert((char)default_value < CHAR_MAX); t_sparseVec *tmp = NULL; alloc_generic_type_1d(t_sparseVec, tmp, size, default_value); tmp; })
// FIXME[article]: consider to include [”elow] code as an examplficiaiton of possible 'traps' when convering from functiosn to macros
//#define allocate_1d_list_float(size, default_value) ({ float *tmp = new float[size]; memset(tmp, default_value, size*sizeof(float)); return tmp = NULL; })


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_float(list) ({assert(list); t_float *tmp = *list; free_generic_type_1d(tmp);})
#define free_1d_list_float_explicitType_double(list) ({assert(list); double *tmp = *list; free_generic_type_1d(tmp);})
#define free_1d_list_sparseVec(list) ({assert(list); t_sparseVec *tmp = *list; free_generic_type_1d(tmp);})

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_uint(size, default_value) ({uint *tmp = (uint*)malloc(size * sizeof(uint));memset(tmp, default_value, size*sizeof(uint)); tmp = NULL;})
// FIXME: use [”elow] and remove [above]
#define allocate_1d_list_uint(size, default_value) ({uint *tmp = NULL; alloc_generic_type_1d(uint, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_uint(list) ({assert(list); uint *tmp = *list;  free_generic_type_1d(tmp); })


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_int(size, default_value) ({int *tmp = (int*)malloc(size*sizeof(int)); memset(tmp, default_value, size*sizeof(int)); tmp = NULL;})
// FIXME: use [”elow] and remove [above]
#define allocate_1d_list_int(size, default_value) ({int *tmp = NULL; alloc_generic_type_1d(int, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_int(list) ({assert(list); int *tmp = *list;  free_generic_type_1d(tmp); })
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define allocate_1d_list_int(size, default_value) ({int *tmp = (int*)malloc(size*sizeof(int)); memset(tmp, default_value, size*sizeof(int)); tmp = NULL;})
// FIXME: use [”elow] and remove [above]
// printf("default-value=%d, at %s:%d\n", default_value, __FILE__, __LINE__); 
#define allocate_1d_list_int_short_unsigned(size, default_value) ({unsigned short int *tmp = NULL; alloc_generic_type_1d(unsigned short int, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_int_short_unsigned(list) ({assert(list); unsigned short int *tmp = *list;  free_generic_type_1d(tmp); })

#define allocate_1d_list_int_short(size, default_value) ({short int *tmp = NULL; alloc_generic_type_1d(short int, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_int_short(list) ({assert(list); short int *tmp = *list;  free_generic_type_1d(tmp); })


//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_char(size, default_value) ({ char *tmp = NULL; alloc_generic_type_1d(char, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_char(list) ({assert(list); char *tmp = *list;  free_generic_type_1d(tmp); })

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_uchar(size, default_value) ({ uchar *tmp = NULL; alloc_generic_type_1d(uchar, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_uchar(list) ({assert(list); uchar *tmp = *list;  free_generic_type_1d(tmp); })

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_lowint(size, default_value) ({ lowint *tmp = NULL; alloc_generic_type_1d(lowint, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_lowint(list) ({assert(list); lowint *tmp = *list;  free_generic_type_1d(tmp); })
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_loint(size, default_value) ({ loint *tmp = NULL; alloc_generic_type_1d(loint, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_loint(list) ({assert(list); loint *tmp = *list;  free_generic_type_1d(tmp); })
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define allocate_1d_list_lowint_s(size, default_value) ({ lowint_s *tmp = NULL; alloc_generic_type_1d(lowint_s, tmp, size, default_value);  tmp;})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_1d_list_lowint_s(list) ({assert(list); lowint_s *tmp = *list;  free_generic_type_1d(tmp); })


//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      /* memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(uint)); 
#define allocate_2d_list_uint(size_x, size_y, default_value) ({ uint **tmp = NULL; tmp = alloc_generic_type_2d__wrapper(uint, tmp, size_x, size_y, default_value); tmp;})
#define allocate_2d_list_uint_old(size_x, size_y, default_value) ({ \
      uint **tmp = NULL; alloc_generic_type_2d(uint, tmp, size_x);		\
      tmp[0]; alloc_generic_type_1d(uint, tmp[0], size_x * size_y, default_value);	\
      assert((char)default_value < CHAR_MAX);				\
      /*uint offset = size_y;*/ uint current = size_y;			\
      for(uint i = 1; i < size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_uint(list, nrows) ({assert(list);  uint **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_uint_old(list) ({assert(list);  uint **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      /* memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(int16_t)); 
#define allocate_2d_list_int16_t(size_x, size_y, default_value) ({      int16_t **tmp = NULL; tmp = alloc_generic_type_2d__wrapper(int16_t, tmp, size_x, size_y, default_value); tmp;})
#define allocate_2d_list_int16_t__old(size_x, size_y, default_value) ({ \
      int16_t **tmp = NULL; alloc_generic_type_2d(int16_t, tmp, size_x); \
      tmp[0]; alloc_generic_type_1d(int16_t, tmp[0], size_x * size_y, default_value);	\
      assert((char)default_value < CHAR_MAX);				\
      /*int16_t offset = size_y;*/ int16_t current = (int16_t)size_y;	\
      for(int16_t i = 1; i < (int16_t)size_x; i++) {			\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_int16_t(list, nrows) ({assert(list);  int16_t **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_int16_t__old(list) ({assert(list);  int16_t **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      /* memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(int)); 
#define allocate_2d_list_int(size_x, size_y, default_value) ({ int **tmp = NULL; tmp = alloc_generic_type_2d__wrapper(int, tmp, size_x, size_y, default_value); tmp;})
#define allocate_2d_list_int__old(size_x, size_y, default_value) ({ \
      int **tmp = NULL; alloc_generic_type_2d(int, tmp, size_x); \
      tmp[0]; alloc_generic_type_1d(int, tmp[0], size_x * size_y, default_value);	\
      assert((char)default_value < CHAR_MAX);				\
      /*int offset = size_y;*/ int current = (int)size_y;		\
      for(uint i = 1; i < (uint)size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_int(list, nrows) ({assert(list);  int **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_int__old(list) ({assert(list);  int **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      memset(tmp[0], (char)default_value, /*size=*/nrows*ncols*sizeof(char)); 
#define allocate_2d_list_char(size_x, size_y, default_value) ({ char **tmp = NULL; tmp = alloc_generic_type_2d__wrapper(char, tmp, size_x, size_y, default_value); tmp;})
#define allocate_2d_list_char__old(nrows, ncols, default_value) ({ \
      char **tmp = alloc_generic_type_2d(char, tmp, nrows);   \
      assert((char)default_value < CHAR_MAX);				\
      tmp[0]; alloc_generic_type_1d(char, tmp[0], ncols*nrows, default_value);	\
      for(uint i = 0; i < nrows; i++) {					\
	tmp[i] = tmp[0] + i*ncols;					\
      }									\
      const uint cnt_cells = nrows * ncols;				\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_char(list, nrows) ({assert(list);  char **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_char__old(list) ({assert(list);  char **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})

#define allocate_2d_list_uchar(size_x, size_y, default_value) ({ uchar **tmp = NULL; tmp = alloc_generic_type_2d__wrapper(uchar, tmp, size_x, size_y, default_value); tmp;})
#define allocate_2d_list_uchar__old(nrows, ncols, default_value) ({ \
      uchar **tmp = alloc_generic_type_2d(uchar, tmp, nrows);   \
      assert((uchar)default_value < UCHAR_MAX);				\
      tmp[0]; alloc_generic_type_1d(uchar, tmp[0], ncols*nrows, default_value);	\
      for(uint i = 0; i < nrows; i++) {					\
	tmp[i] = tmp[0] + i*ncols;					\
      }									\
      const uint cnt_cells = nrows * ncols;				\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_uchar(list, nrows) ({assert(list);  uchar **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_uchar__old(list) ({assert(list);  uchar **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})
#define allocate_2d_list_lowint(size_x, size_y, default_value) ({ lowint **tmp = NULL; tmp = alloc_generic_type_2d__wrapper(lowint, tmp, size_x, size_y, default_value); tmp;})
#define allocate_2d_list_lowint__old(nrows, ncols, default_value) ({ \
      lowint **tmp = alloc_generic_type_2d(lowint, tmp, nrows);   \
      assert((lowint)default_value < LOWINT_MAX);				\
      tmp[0]; alloc_generic_type_1d(lowint, tmp[0], ncols*nrows, default_value);	\
      for(uint i = 0; i < nrows; i++) {					\
	tmp[i] = tmp[0] + i*ncols;					\
      }									\
      const uint cnt_cells = nrows * ncols;				\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_lowint(list, nrows) ({assert(list);  lowint **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_lowint__old(list) ({assert(list);  lowint **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})
#define allocate_2d_list_lowint_s(nrows, ncols, default_value) ({ \
      lowint_s **tmp = alloc_generic_type_2d(lowint_s, tmp, nrows);   \
      assert((lowint_s)default_value < SHRT_MAX);				\
      tmp[0]; alloc_generic_type_1d(lowint_s, tmp[0], ncols*nrows, default_value);	\
      for(uint i = 0; i < nrows; i++) {					\
	tmp[i] = tmp[0] + i*ncols;					\
      }									\
      const uint cnt_cells = nrows * ncols;				\
      tmp;}) //! ie, return.

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_lowint_s(list, nrows) ({assert(list);  lowint_s **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_lowint_s__old(list) ({assert(list);  lowint_s **tmp = *list;  free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);})


//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(t_float)); 
#define allocate_2d_list_float(size_x, size_y, default_value) ({ t_float **tmp_wrap = NULL; tmp_wrap = alloc_generic_type_2d__wrapper(t_float, tmp_wrap, size_x, size_y, default_value); tmp_wrap;})
#define allocate_2d_list_float__old(size_x, size_y, default_value) ({ \
      t_float **tmp = alloc_generic_type_2d(t_float, tmp, size_x);		\
      tmp[0] = alloc_generic_type_1d(t_float, tmp[0], size_x * size_y, default_value);	\
      assert((char)default_value < CHAR_MAX);				\
      /*uint offset = size_y;*/ uint current = size_y;			\
      for(uint i = 1; i < size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y;			\
      }									\
      tmp;}) //! ie, return
//! Copy a 2d-float-list into "new" from "old", assuming that both 2d-slits have already been allcoated using a continous-memry-allcoaiton-scheme (oekseth, 06. jan. 2017)
#define copy_2d_list_float__old(new, old, size_x, size_y) ({ \
						       const long int size_total = size_x * size_y; assert(size_total > 0); \
						       assert(new); assert(old); memcpy(&(new[0][0]), &(old[0][0]), sizeof(t_float)*size_total);})
#define copy_2d_list_float(new, old, size_x, size_y) ({			\
      assert(new); assert(old);  for(uint row_id = 0; row_id < size_x; row_id++) { memcpy(&(new[row_id][0]), &(old[row_id][0]), sizeof(t_float)*size_y);}})
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_float(list, nrows) ({assert(list);  t_float **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_float__old(list) ({ assert(list);  t_float **tmp = *list; free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);  })
/* //! --------------------------- */
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(t_float));
/* #define allocate_3d_list_float(size_inner, size_x, size_y, default_value) ({ \ */
/*       t_float ***tmp = alloc_generic_type_3d(t_float, tmp, size_inner);		\ */
/*       assert((char)default_value < CHAR_MAX);				\ */
/*       for(uint inner = 0; inner < size_inner; inner++) {				\ */
/* 	tmp[inner] = allocate_2d_list_float(size_x, size_y, default_value); \ */
/*       }									\ */
/*       tmp;}) //! ie, return */

//! ---------------------------
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//      memset(tmp[0], (char)default_value, /*size=*/size_x*size_y*sizeof(t_float)); 
#define allocate_3d_list_float(size_inner, size_x, size_y, default_value) ({ \
      t_float ***tmp = alloc_generic_type_3d(t_float, tmp, size_inner);		\
      assert((char)default_value < CHAR_MAX);				\
      uint inner = 0;							\
      const uint size_each = size_x * size_y; \
      const uint inner_and_x_size = size_inner * size_x; \
      /*const uint matrix_size = size_inner * size_each;	*/	\
      tmp[0] = allocate_2d_list_float(inner_and_x_size, size_y, default_value); \
      /*tmp[0][0] = alloc_generic_type_1d(t_float, tmp[0][0], matrix_size, default_value);*/ \
      /*uint current = size_each;*/					\
      const uint size_outer = size_x * size_y; uint current = size_outer; \
      for(uint matrix_id = 1; matrix_id < size_inner; matrix_id++) {	\
      tmp[matrix_id] = tmp[0] + size_x; current += size_x; \
	/*tmp[inner] = tmp[0] + current; current += size_each;*/	\
      }									\
      tmp;}) //! ie, return

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
//#define free_3d_list_float(size_inner, list) ({ assert(list);  t_float ***tmp = *list; {free_generic_type_1d(tmp[0][0]);} free(tmp);  })
//#define free_3d_list_float(size_inner, list) ({ assert(list);  t_float ***tmp = *list; 
#define free_3d_list_float(list) ({ assert(list);  t_float ***tmp = *list; \
  /*free_2d_list_float(&tmp[0]); */ \
  free_generic_type_1d(tmp[0][0]); free_generic_type_2d(tmp[0]); \
  free_generic_type_3d(tmp); })
//#define free_3d_list_float(size_inner, list) ({ assert(list);  t_float ***tmp = *list; free_2d_list_float(&tmp[0]); free_generic_type_3d(tmp); })
//#define free_3d_list_float(size_inner, list) ({ assert(list);  t_float ***tmp = *list; for(uint i = 0; i < size_inner; i++) {free_generic_type_1d(tmp[i][0]); free_generic_type_2d(tmp[i]);} free(tmp);  })

//      memset(tmp[0], (char)default_value, /*size=*/size_x*size_y_mod*sizeof(t_sparseVec)); 
#define allocate_2d_list_sparseVec(size_x, size_y, default_value) ({ t_sparseVec **tmp_wrap; tmp_wrap = alloc_generic_type_2d__wrapper(t_sparseVec, tmp_wrap, size_x, size_y, default_value); tmp_wrap;})
#define allocate_2d_list_sparseVec__old(size_x, size_y, default_value) ({ \
      t_sparseVec **tmp = alloc_generic_type_2d(t_sparseVec, tmp, size_x);		\
      const uint size_y_mod = __macroInline_getSizeOut_AlignedMemory(size_y, /*block_size=*/SPARSEVEC_ITER_SIZE); \
      tmp[0]; alloc_generic_type_1d(t_sparseVec, tmp[0], size_x * size_y_mod, default_value);	\
      assert((char)default_value < CHAR_MAX);				\
      /*uint offset = size_y;*/ uint current = size_y_mod;			\
      for(uint i = 1; i < size_x; i++) {				\
	tmp[i] = tmp[0] + current; current += size_y_mod;			\
      }									\
      tmp;}) //! ie, return
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
#define free_2d_list_sparseVec(list, nrows) ({assert(list);  t_sparseVec **tmp = *list; free_generic_type_2d_wrapper(tmp, nrows); tmp;})
#define free_2d_list_sparseVec__old(list) ({ assert(list);  t_sparseVec **tmp = *list; free_generic_type_1d(tmp[0]); free_generic_type_2d(tmp);  })







//! complete: <file> wrt. "def_memAlloc_h" ----------------------------------------------------------
#endif //! ie, EOF
