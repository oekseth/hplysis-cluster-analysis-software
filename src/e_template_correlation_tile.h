#ifndef e_template_correlation_tile_h
#define e_template_correlation_tile_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file e_template_correlation_tile.h
   @brief provide logics for computation of ranks, lgoics to be used/integrated in all-against-all-compairosn
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/

#include "e_template_correlation_tile_maskType.h"

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "parse_main.h"
#include "e_kt_correlationFunction.h"
/**
   @enum e_template_correlation_tile_typeOf_distanceUpdate
   @brief used to simplify the comptaution of partial sets wrt. SSE-computaiton: relates to "s_template_correlation_tile_temporaryResult_SSE", "s_template_correlation_tile_temporaryResult" and "s_kt_computeTile_subResults_t"
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/
typedef enum e_template_correlation_tile_typeOf_distanceUpdate {
  e_template_correlation_tile_typeOf_distanceUpdate_scalar,
  e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator, //! which is expected to 'invovle' two variables
  e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex, //! which is expected to 'invovle' four variables
  //e_template_correlation_tile_typeOf_distanceUpdate_undef,
} e_template_correlation_tile_typeOf_distanceUpdate_t;

/**
   @struct s_template_correlation_tile_temporaryResult_SSE
   @brief hold an intermediate SSE data-struct to be used for comptautioni of compelx distance-metrics.
   @rremarks examples of use include the distance-metrics of "Kulczynski" and "Soerensen"
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/
typedef struct s_template_correlation_tile_temporaryResult_SSE {
  VECTOR_FLOAT_TYPE cnt_interesting_cells; //! which is sued/icnmreneted if (a) mask-proerpties are set and (b) 'such' is recquested by/through the macro-parameter-call to our "template_correlation_tile.cxx" (oekseth, 06. setp. 2016).
  VECTOR_FLOAT_TYPE numerator;   VECTOR_FLOAT_TYPE denumerator;
  
  //! More elabourate data-tonaienrs.
  /* VECTOR_FLOAT_TYPE numeratorLimitedTo_row1; //! ie, the value 'below an equation-line' for: row1 */
  VECTOR_FLOAT_TYPE numeratorLimitedTo_row_1and2; //! ie, the value 'below an equation-line' for: row1 && row1, eg, in the "Tanimoto" distance-metric
  /* VECTOR_FLOAT_TYPE numeratorLimitedTo_row2; //! ie, the value 'below an equation-line' for: row2 */
  VECTOR_FLOAT_TYPE denumeratorLimitedTo_row1; //! ie, the value 'below an equation-line' for: row1
  VECTOR_FLOAT_TYPE denumeratorLimitedTo_row_1and2; //! ie, the value 'below an equation-line' for: row1 && row1, eg, in the "Jaccard" distance-metric
  VECTOR_FLOAT_TYPE denumeratorLimitedTo_row2; //! ie, the value 'below an equation-line' for: row2
} s_template_correlation_tile_temporaryResult_SSE_t;

//! Set the "s_template_correlation_tile_temporaryResult_SSE" object to empty.
void setTo_empty__s_template_correlation_tile_temporaryResult_SSE(s_template_correlation_tile_temporaryResult_SSE_t *self, const bool isTo_findMaxValue, const bool isTo_findMinValue);

/**
   @struct s_template_correlation_tile_temporaryResult
   @brief hold an intermediate non-SSE data-struct to be used for comptautioni of compelx distance-metrics.
   @rremarks examples of use include the distance-metrics of "Kulczynski" and "Soerensen"
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/
typedef struct s_template_correlation_tile_temporaryResult {
  uint cnt_interesting_cells; //! which is sued/icnmreneted if (a) mask-proerpties are set and (b) 'such' is recquested by/through the macro-parameter-call to our "template_correlation_tile.cxx" (oekseth, 06. setp. 2016).
  t_float numerator;   t_float denumerator;
  
  //! More elabourate data-tonaienrs.
  /* t_float numeratorLimitedTo_row1; //! ie, the value 'below an equation-line' for: row1 */
  t_float numeratorLimitedTo_row_1and2; //! ie, the value 'below an equation-line' for: row1 && row1, eg, in the "Tanimoto" distance-metric
  /* t_float numeratorLimitedTo_row2; //! ie, the value 'below an equation-line' for: row2 */
  t_float denumeratorLimitedTo_row1; //! ie, the value 'below an equation-line' for: row1
  t_float denumeratorLimitedTo_row_1and2; //! ie, the value 'below an equation-line' for: row1 && row1, eg, in the "Jaccard" distance-metric
  t_float denumeratorLimitedTo_row2; //! ie, the value 'below an equation-line' for: row2
} s_template_correlation_tile_temporaryResult_t;

//! Set the "s_template_correlation_tile_temporaryResult" object to empty.
void setTo_empty__s_template_correlation_tile_temporaryResult(s_template_correlation_tile_temporaryResult_t *self, const bool isTo_findMaxValue, const bool isTo_findMinValue);

/* /\** */
/*    @struct s_template_correlation_tile_temporaryResult_listOf */
/*    @brief wraps a list of s_template_correlation_tile_temporaryResult_t fucntionis, used for storing 'intermeidate' results (oekseth, 06. mar. 2017). */
/*  **\/ */
/* typedef struct s_template_correlation_tile_temporaryResult_listOf { */
/*   s_template_correlation_tile_temporaryResult_t *list; */
/*   long unsigned int list_size; */
/* } s_template_correlation_tile_temporaryResult_listOf_t; */

/* //! @return a poitner to our new-allcoated data-structre (oesketh, 06. mar. 2017). */
/* static s_template_correlation_tile_temporaryResult_listOf_t setToEmpty__s_template_correlation_tile_temporaryResult_listOf_t() { */
/*   s_template_correlation_tile_temporaryResult_listOf_t self; */
/*   self.list = NULL; self.list_size = 0; */
/*   //! @return */
/*   return self; */
/* } */

/* static s_template_correlation_tile_temporaryResult_listOf_t init__s_template_correlation_tile_temporaryResult_listOf_t(const list_size, const bool isTo_findMaxValue, const bool isTo_findMinValue) { */

/* } */



/* //! Set the "s_template_correlation_tile_temporaryResult" object to empty. */
/* static void setTo_empty__s_template_correlation_tile_temporaryResult(s_template_correlation_tile_temporaryResult_t *self, const bool isTo_findMaxValue) { */
/*   assert(self); */
/*   t_float default_value_float = 0; if(isTo_findMaxValue) {default_value_float = T_FLOAT_MIN_ABS;} */
/*   self->cnt_interesting_cells = 0; */
/*   self->numerator = default_value_float; */
/*   self->denumerator = default_value_float; */
/*   /\* self->numeratorLimitedTo_row1 = default_value_float; *\/ */
/*   /\* self->numeratorLimitedTo_row_1and2 = default_value_float; *\/ */
/*   /\* self->numeratorLimitedTo_row2 = default_value_float; *\/ */
/*   self->denumeratorLimitedTo_row1 = default_value_float; */
/*   self->denumeratorLimitedTo_row_1and2 = default_value_float; */
/*   self->denumeratorLimitedTo_row2 = default_value_float; */
/* } */


/* /\** */
/*    @brief update the global object at the  */
/*  **\/ */
/* static void update_globalObject_for_sse_andNotSSE(s_kt_computeTile_subResults_t *global_object, const uint local_index1, const uint local_index2, const s_template_correlation_tile_temporaryResult_SSE_t obj_sse, const s_template_correlation_tile_temporaryResult_t obj_notSSe) { */

/* } */


/**
   @struct s_template_correlation_tile_classification
   @brief used to hold specific properties of each classifciaotn-matrix (oekseth, 06. nvo. 2016).
   @remarks expected to be itnatied from our "build_codeFor_tiling.pl" Perl-script.
 **/
typedef struct s_template_correlation_tile_classification {
  e_kt_correlationFunction_t metric_id; //! ie, the name of teh enum in qeustion.
  const char *humanReadable_name; //! eg, "euclid"
  const char *humanReadable_description; //! which is optional, ie, Not alwyas set.
  //! ----
  bool use_powerInArgument;
  bool use_max;
  bool use_countOf_vertices;
  bool use_weightMuliplicationAfter_computationOfScores;
  //! ----
  // FIXME: add support for [”elow]:
  bool use_abs; //! ie, if the 'sign of difference' is of interest <-- write a use-case wrt. 'this'.
  bool use_log; //! ie, if the 'relationship between vlaeus' are normalized usign a lgoraithm-transform.
  bool use_div; //! ie, if the valeus are compared, such as wrt. "|a-b|/(a+b)"
  bool use_mul; //! ie, if signficant valeus are given an increased weight, for which we expect the 'inverse' of a log-transform-operation
	  //! --------
  bool exception_each_divByZero; //! which wrt. SSE is handled using our "VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(...)"
  bool exception_each_zeroByDiv; //! which wrt. SSE is handled using our "VECTOR_FLOAT_dataMask_replaceINF_by_Zero(..)" (and "macro_div_handleZeroInNumeratorAndDeNominator(..)" fro some of of our distnace-metrics (though not cosnistenly used in/across all of them)), eg, wrt. Squared-Perason and squared-Neyman 
  bool exception_postProcess__needToBeLessThan1; //! ie, for cases where the 'som of vlaues' should be 'less than 1æ, eg, for comptatuiosn using "1 - sum(...)" as a post-procesisng option: e_kt_correlationFunction_groupOf_intersection_intersection_altDef, e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_correlationFunction_groupOf_fidelity_Matusita, 
  //! --------
  bool use_inPostProcessing_div; //! ie, where we 'divsion' is used beteween seperate sets.
  bool use_inPostProcessing_log; 
  bool use_inPostProcessing_pow;  //! both wrt. "sqrt(..)" and wrt. "pow(..)"

  //! ----
  e_template_correlation_tile_typeOf_distanceUpdate_t typeOf_distanceUpdate;
  //! ------------------------------
} s_template_correlation_tile_classification_t;

//! Intiate the s_template_correlation_tile_classification_t object
s_template_correlation_tile_classification_t init__s_template_correlation_tile_classification_t(e_kt_correlationFunction_t metric_id, const char *humanReadable_name, const char *humanReadable_description, const bool use_powerInArgument, const bool use_max, const bool use_countOf_vertices, const bool use_weightMuliplicationAfter_computationOfScores, const e_template_correlation_tile_typeOf_distanceUpdate_t typeOf_distanceUpdate, const bool use_abs, const bool use_log, const bool use_div, const bool use_mul, const bool exception_each_divByZero, const bool exception_each_zeroByDiv, const bool exception_postProcess__needToBeLessThan1, const bool use_inPostProcessing_div, const bool use_inPostProcessing_log,  const bool use_inPostProcessing_pow);

//! @return the s_template_correlation_tile_classification_t object assicated to metric_id
s_template_correlation_tile_classification_t get_forEnum_structOf_s_template_correlation_tile_classification_t(const e_kt_correlationFunction_t metric_id);

/**
   @brief update the stringOf_data_toAdd with a descrption of metric_id
   @param <metric_id> is the enum to update with a 'strineidfied' description of the metric_id
   @param <stringOf_data_toAdd> is the string to udpate with a descirption: expects at least 1000 chars to have been memory-allcoated wrt. this pointer.
 **/
void include_stringDescription_forEnum_s_template_correlation_tile_classification_t(const e_kt_correlationFunction_t metric_id, char *stringOf_data_toAdd);
#endif //! EOF
