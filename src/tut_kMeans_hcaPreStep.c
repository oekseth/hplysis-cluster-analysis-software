#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks examplifeis how HCA may be used as a pre-step to irmpvoe the resutl-accuracy. 
   @remarks Demonstrates features wrt.:
   -- input: merge two 'matemcialcal' fucntiosn into one matrix
   -- computation: use of two differnet permtautiosn for the same 'main algorithm' call, wher ehte latter is described by "clustAlg"
   -- result(a): how results from mmuliple clusterings may be exported into one single-unified java-script file.
   -- result(b): write out the CCM-score for a givne comptaution.
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  /* s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t(); */
  /* obj_config.config__isToPrintOut__iterativeStatusMessage = true; */
  /* obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons */
  /* //! ---- */
  /* obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/"; */
  /* obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js"; */
  /* obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix */
  /* //! */
  /* //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"): */
  /* obj_config.config_arg_npass = 10000; */
  /* //! */
  /* //! Result data: */
  /* obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS; */
  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  //const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //! ----------------------------------- 
  //! 
  //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
  /* const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess;  */
  /* const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess;  */
  /* const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------    */
  const char *config__nameOfDefaultVariable_1 = "binomial_p005";
  const char *config__nameOfDefaultVariable_2 = "linear-differentCoeff-b";
  const uint cnt_Calls_max = 3; const uint arg_npass = 1000;
  const char *result_file = "test_tut_kCluster.tsv";
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  const e_hpLysis_clusterAlg clusterAlg__firstPass = e_hpLysis_clusterAlg_HCA_centroid;
  //! ----------------------------------- 
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  //! Valdiate that the algorithsm ahs the expected 'proerpties'w rt. our test-setup:
  assert(isOf_type_HCA__e_hpLysis_clusterAlg_t(clusterAlg__firstPass));
  assert(isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(clusterAlg));

  //!
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  if(     true ) {
    fileRead_config__syn.imaginaryFileProp__nrows = 20;
    fileRead_config__syn.imaginaryFileProp__ncols = 20;
  } else { //! then we are interested in a more performacne-demanind approach: 
    fileRead_config__syn.imaginaryFileProp__nrows = 400;
    fileRead_config__syn.imaginaryFileProp__ncols = 400;
  }
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.


  //!
  //! Laod and concatenate matrices:
  s_kt_matrix_t matrix_1 = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable_1, fileRead_config__syn);
  fileRead_config__syn.mat_concat = &matrix_1;
  s_kt_matrix_t matrix_merged = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable_2, fileRead_config__syn);

  const uint nclusters = 2;
  //! Open teh file :
  FILE *file_out = fopen(result_file, "wb");
  //FILE *file_out = stdout;
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\": please investigate. Observiaotn at [%s]:%s:%d\n", result_file, __FUNCTION__, __FILE__, __LINE__); assert(false);}

  //!
  //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:
  for(uint cnt_Calls = 0; cnt_Calls < cnt_Calls_max; cnt_Calls++) {
    for(uint case_id = 0; case_id < 2; case_id++) {
      s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
      // (Symmetric) adjacency matrix                                                                                                                            
      obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
      obj_hp.config.corrMetric_prior_use = true; //! ie, applky a 'defualt simalrity-emtric'
      if(case_id == 0) {
	fprintf(stdout, "\t Calls algorithm usign the 'nromal appraoch', ie, withotu a pre-algorithm-stpe (to identify start-nodes),  at %s:%d\n", __FILE__, __LINE__);
	fprintf(file_out, "\t Calls algorithm usign the 'nromal appraoch', ie, withotu a pre-algorithm-stpe (to identify start-nodes),  at %s:%d\n", __FILE__, __LINE__);
	const bool is_also_ok = cluster__hpLysis_api (
						      &obj_hp, clusterAlg, &matrix_merged,
						      /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						      );
	assert(is_also_ok);
      } else if(case_id == 1) {
	fprintf(stdout, "\n#\t Calls algorithm usign the 'oekseth-implciit-appraoch': apply a pre-algorithm-stpe to dineitfy k-means-start-nodes, and thereafter sue a 'nroaml algorithm-call-exeuction',  at %s:%d\n", __FILE__, __LINE__);
	fprintf(file_out, "\n#\t Calls algorithm usign the 'oekseth-implciit-appraoch': apply a pre-algorithm-stpe to dineitfy k-means-start-nodes, and thereafter sue a 'nroaml algorithm-call-exeuction',  at %s:%d\n", __FILE__, __LINE__);
	//! Then we apply the 'nvoel call' examplifed in this example-case:
	const bool is_ok = cluster__kMeansTwoPass__hpLysis_api(&obj_hp, clusterAlg, &matrix_merged, nclusters, arg_npass, /*cofnig-firstPass*/obj_hp.config, clusterAlg__firstPass);
	assert(is_ok);
      } else {assert(false);} //! ie, as we then need adding support f'or this'.

      //! 
      //! Export results to "file_out":
      assert(file_out);
      const s_kt_correlationMetric_t corrMetric_prior = obj_hp.config.corrMetric_prior;
      fprintf(stdout, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
      fprintf(file_out, "#! ----------------------------------------\n#! Exports the similairty-matrix for corrMetric_prior=\"%s\"::\"%s\", at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(corrMetric_prior.metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(corrMetric_prior.typeOf_correlationPreStep), __FILE__, __LINE__);
      /* bool is_ok_e = export__hpLysis_api(&obj_hp, /\*stringOf_file=*\/NULL, /\*file_out=*\/file_out, /\*exportFormat=*\/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV); */
      /* assert(is_ok_e); */
      //! Export the vertex-clsuterId-memberships:
      {
	const bool is_ok_e = export__hpLysis_api(&obj_hp, /*stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &matrix_merged);
	assert(is_ok_e);
      }

      { //! 
	const t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
	if(CCM_score != T_FLOAT_MAX) {
	  fprintf(stdout, "\t CCM-score=%f for CCM=\"Silhuette\", at %s:%d\n", CCM_score, __FILE__, __LINE__);
	  fprintf(file_out, "\t CCM-score=%f for CCM=\"Silhuette\", at %s:%d\n", CCM_score, __FILE__, __LINE__);
	} else {
	  fprintf(stdout, "\t CCM-score=undef for CCM=\"Silhuette\", at %s:%d\n", __FILE__, __LINE__);
	  fprintf(file_out, "\t CCM-score=undef for CCM=\"Silhuette\", at %s:%d\n", __FILE__, __LINE__);
	}
      }

      //! 
      //! 
      { //! Export: traverse teh generated result-matrix-object and write out the results:    
	//! Note: the [”elow] calls implictly 'test' the/our "cuttree(..)" proceudre (first described in the work of "clsuter.c"):
	const uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
	assert(vertex_clusterId);
	const uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;
	assert(cnt_vertex > 0);
	fprintf(file_out, "clusterMemberships=[");
	uint max_cnt = 0;
	for(uint i = 0; i < cnt_vertex; i++) {fprintf(file_out, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
	fprintf(file_out, "], w/biggestClusterId=%u, at %s:%d\n", max_cnt, __FILE__, __LINE__);
      }    
      //! De-allocates the "s_hpLysis_api_t" object.
      free__s_hpLysis_api_t(&obj_hp);	    
    }
  }


  //!
  //! Close the file and the amtrix:
  assert(file_out); 
  if(file_out != stdout) {fclose(file_out); file_out = NULL;}
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_1);
  free__s_kt_matrix(&matrix_merged);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
