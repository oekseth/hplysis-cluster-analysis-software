    { //! test correctness:
      const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&obj_disjoint);
      const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
      printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
      //! Validate the result:
      //! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
      //assert(cnt_total_vertices == nrows); //! ie, the totla number of unique vertices inserted in [above]
      assert(cnt_interesting_disjointForests == 3); //! ie, what we expect.

      //! Validate uniuqneess of forests:
      { //! First for the columns:
	const uint forest_id_1 = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest1_col[0], /*datasetId=*/1, /*vertex_describesRow=*/false);
	assert(forest_id_1 != UINT_MAX);
	const uint forest_id_2 = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest2_col[0], /*datasetId=*/1, /*vertex_describesRow=*/false);
	assert(forest_id_2 != UINT_MAX);
	const uint forest_id_3 = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest3_col[0], /*datasetId=*/1, /*vertex_describesRow=*/false);
	assert(forest_id_3 != UINT_MAX);
	//! ----------	
	assert(forest_id_1 != UINT_MAX);
	assert(forest_id_1 != forest_id_2);
	assert(forest_id_1 != forest_id_3);
	//! ----------	
      }
      { //! Thereafter for the rows:
	const uint forest_id_1a = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest1_row_1[0], /*datasetId=*/1, /*vertex_describesRow=*/true);
	const uint forest_id_1b = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest1_row_2[0], /*datasetId=*/2, /*vertex_describesRow=*/true);
	//! --
	const uint forest_id_2a = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest2_row_1[0], /*datasetId=*/1, /*vertex_describesRow=*/true);
	const uint forest_id_2b = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest2_row_2[0], /*datasetId=*/2, /*vertex_describesRow=*/true);
	//! --
	const uint forest_id_3a = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest3_row_1[0], /*datasetId=*/1, /*vertex_describesRow=*/true);
	const uint forest_id_3b = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest3_row_2[0], /*datasetId=*/2, /*vertex_describesRow=*/true);
	//! --
	assert(forest_id_1a != UINT_MAX);
	assert(forest_id_1a != forest_id_2a);
	assert(forest_id_1a != forest_id_3a);
	//! --
	assert(forest_id_1b != UINT_MAX);
	assert(forest_id_1b != forest_id_2a);
	assert(forest_id_1b != forest_id_3a);
	//! --
	//printf("f=1: %u VS %u, at %s:%d\n", forest_id_1a, forest_id_1b, __FILE__, __LINE__);
	assert(forest_id_1a == forest_id_1b);
	//printf("f=2: %u VS %u, at %s:%d\n", forest_id_2a, forest_id_2b, __FILE__, __LINE__);
	assert(forest_id_2a == forest_id_2b);
	//printf("f=3: %u VS %u, where m_forest3_row_2[0]=%u, at %s:%d\n", forest_id_3a, forest_id_3b, m_forest3_row_2[0], __FILE__, __LINE__);
	assert(forest_id_3b != UINT_MAX);
	assert(forest_id_3a == forest_id_3b);
      }    
    }
    //! --------------------------------
    { //! Fetch a chunk of disjtoinf-reost-emmbershisp and 'correctly map them into the different data-chunk-categories:
      const uint globalStartPos_offset1 = get_internalStartPos_of_verticesFrom_dataSetId__s_kt_forest_findDisjoint_t(&obj_disjoint, /*datasetId=*/1, /*for_rows=*/false);
      const uint globalStartPos_offset2 = get_internalStartPos_of_verticesFrom_dataSetId__s_kt_forest_findDisjoint_t(&obj_disjoint, /*datasetId=*/2, /*for_rows=*/true);
      assert(globalStartPos_offset1 == nrows_1);
      assert(globalStartPos_offset2 == (nrows_1 + ncols)); //! ie, as we expect 'global column-numbers' to be 'named' after the rows 'have been named':
      uint test_key = 0;
      assert(0 == __assert__adjustInternalDisjointIndex__toLocalIndex(test_key));
      test_key = nrows_1;
      assert(0 == __assert__adjustInternalDisjointIndex__toLocalIndex(test_key));
      test_key = nrows_1 + 1;
      assert(1 == __assert__adjustInternalDisjointIndex__toLocalIndex(test_key));
      test_key = nrows_1 + ncols + 1;
      assert(1 == __assert__adjustInternalDisjointIndex__toLocalIndex(test_key));
      //!
      //! Validate the different relationships:
      for(uint i = 0; i < m_forest1_row_size_1; i++) {
	for(uint k = 0; k < m_forest1_row_size_1; k++) {
	  __inLove(m_forest1_row_1[i], m_forest1_row_1[k]);
	}
	for(uint k = 0; k < m_forest1_row_size_2; k++) {
	  __inLove__girlsAllToBeautiful(m_forest1_row_1[i], m_forest1_row_2[k]);
	}
	for(uint k = 0; k < m_forest1_col_size; k++) {
	  __inLove__girlsBoys(m_forest1_row_1[i], m_forest1_col[k]);
	}
	for(uint k = 0; k < m_forest2_row_size_1; k++) {
	  __atWar(m_forest1_row_1[i], m_forest2_row_1[k]);
	}
	for(uint k = 0; k < m_forest2_row_size_2; k++) {
	  __atWar__girlsAllToBeautiful(m_forest1_row_1[i], m_forest2_row_2[k]);
	}
	for(uint k = 0; k < m_forest2_col_size; k++) {
	  __atWar__girlsBoys(m_forest1_row_1[i], m_forest2_col[k]);
	}
	for(uint k = 0; k < m_forest3_row_size_1; k++) {
	  __atWar(m_forest1_row_1[i], m_forest3_row_1[k]);
	}
	for(uint k = 0; k < m_forest3_row_size_2; k++) {
	  __atWar__girlsAllToBeautiful(m_forest1_row_1[i], m_forest3_row_2[k]);
	}
      }	
    }
    //! ----------------------
    { //! Get the clsuter-id and 'the returned set of ityndieties' proudce the rorect reuslt:
      { //! Get the forest-id for one of the sets:
	const uint cnt_expected_row_1 = m_forest1_row_size_1; const uint *arrOf_cmp_row_1 = m_forest1_row_1;
	const uint cnt_expected_row_2 = m_forest1_row_size_2; const uint *arrOf_cmp_row_2 = m_forest1_row_2;
	//const uint cnt_expected_row = m_forest1_row_size; const uint *arrOf_cmp_row = m_forest1_row;
	const uint cnt_expected_col = m_forest1_col_size; const uint *arrOf_cmp_col = m_forest1_col;
	{ //! ----------------- the copy-paste-test -----
	  uint *arrOf_result_row_1 = NULL;  uint arrOf_result_row_size_1 = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_row_2 = NULL;  uint arrOf_result_row_size_2 = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_col = NULL;  uint arrOf_result_col_size = 0; //! where the latter is a temprao vriable:
	  //! The test:
	  const uint forest_id = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest1_row_1[0], /*datasetId=*/1, /*vertex_describesRow=*/true);
	  //! Build a selit of the forest-ids:
	  cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, 
														 &arrOf_result_row_1, &arrOf_result_row_size_1,
														 &arrOf_result_row_2, &arrOf_result_row_size_2,
														 &arrOf_result_col, &arrOf_result_col_size);
	  //!
	  //! Valdiate that the 'fetched results from our disjoint-forst-comptuation is correct':
	  assert(arrOf_result_row_size_1 == cnt_expected_row_1); 
	  assert(arrOf_result_row_size_2 == cnt_expected_row_2); 
	  assert(arrOf_result_col_size == cnt_expected_col); 
	  // printf("arrOf_result_size=%u, cnt_expected=%u, at %s:%d\n", arrOf_result_size, cnt_expected, __FILE__, __LINE__);


	  uint *arrOf_result = arrOf_result_row_1; uint arrOf_result_size = arrOf_result_row_size_1;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_row_1, cnt_expected_row_1); //! ie, vlaidate that all 'entites' are found.
	  //! --
	  arrOf_result = arrOf_result_row_2; arrOf_result_size = arrOf_result_row_size_2;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_row_2, cnt_expected_row_2); //! ie, vlaidate that all 'entites' are found.
	  //! --
	  arrOf_result = arrOf_result_col; arrOf_result_size = arrOf_result_col_size;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_col, cnt_expected_col); //! ie, vlaidate that all 'entites' are found.
	  //! De-allcote:
	  free_1d_list_uint(&arrOf_result_row_1); arrOf_result_row_1 = NULL;
	  free_1d_list_uint(&arrOf_result_row_2); arrOf_result_row_2 = NULL;
	  free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;
	}
      }
      //! --------------- forest-2:
      { //! Get the forest-id for one of the sets:
	const uint cnt_expected_row_1 = m_forest2_row_size_1; const uint *arrOf_cmp_row_1 = m_forest2_row_1;
	const uint cnt_expected_row_2 = m_forest2_row_size_2; const uint *arrOf_cmp_row_2 = m_forest2_row_2;
	//const uint cnt_expected_row = m_forest2_row_size; const uint *arrOf_cmp_row = m_forest2_row;
	const uint cnt_expected_col = m_forest2_col_size; const uint *arrOf_cmp_col = m_forest2_col;
	{ //! ----------------- the copy-paste-test -----
	  uint *arrOf_result_row_1 = NULL;  uint arrOf_result_row_size_1 = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_row_2 = NULL;  uint arrOf_result_row_size_2 = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_col = NULL;  uint arrOf_result_col_size = 0; //! where the latter is a temprao vriable:
	  //! The test:
	  const uint forest_id = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest2_row_1[0], /*datasetId=*/1, /*vertex_describesRow=*/true);
	  //! Build a selit of the forest-ids:
	  cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, 
														 &arrOf_result_row_1, &arrOf_result_row_size_1,
														 &arrOf_result_row_2, &arrOf_result_row_size_2,
														 &arrOf_result_col, &arrOf_result_col_size);
	  //!
	  //! Valdiate that the 'fetched results from our disjoint-forst-comptuation is correct':
	  assert(arrOf_result_row_size_1 == cnt_expected_row_1); 
	  //printf("arrOf_result_size_2=%u, cnt_expected_2=%u, at %s:%d\n", arrOf_result_row_size_2, cnt_expected_row_2, __FILE__, __LINE__);
	  assert(arrOf_result_row_size_2 == cnt_expected_row_2); 
	  assert(arrOf_result_col_size == cnt_expected_col); 


	  uint *arrOf_result = arrOf_result_row_1; uint arrOf_result_size = arrOf_result_row_size_1;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_row_1, cnt_expected_row_1); //! ie, vlaidate that all 'entites' are found.
	  //! --
	  arrOf_result = arrOf_result_row_2; arrOf_result_size = arrOf_result_row_size_2;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_row_2, cnt_expected_row_2); //! ie, vlaidate that all 'entites' are found.
	  //! --
	  arrOf_result = arrOf_result_col; arrOf_result_size = arrOf_result_col_size;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_col, cnt_expected_col); //! ie, vlaidate that all 'entites' are found.
	  //! De-allcote:
	  free_1d_list_uint(&arrOf_result_row_1); arrOf_result_row_1 = NULL;
	  free_1d_list_uint(&arrOf_result_row_2); arrOf_result_row_2 = NULL;
	  free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;	  
	  //assert(false); // FIXME: update [below]
	}
      }
      //! --------------- Forest-3:
      { //! Get the forest-id for one of the sets:
	const uint cnt_expected_row_1 = m_forest3_row_size_1; const uint *arrOf_cmp_row_1 = m_forest3_row_1;
	const uint cnt_expected_row_2 = m_forest3_row_size_2; const uint *arrOf_cmp_row_2 = m_forest3_row_2;
	const uint cnt_expected_col = m_forest3_col_size; const uint *arrOf_cmp_col = m_forest3_col;
	{ //! ----------------- the copy-paste-test -----
	  uint *arrOf_result_row_1 = NULL;  uint arrOf_result_row_size_1 = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_row_2 = NULL;  uint arrOf_result_row_size_2 = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_col = NULL;  uint arrOf_result_col_size = 0; //! where the latter is a temprao vriable:
	  //! The test:
	  const uint forest_id = get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(&obj_disjoint, m_forest3_row_1[0], /*datasetId=*/1, /*vertex_describesRow=*/true);
	  //! Build a selit of the forest-ids:
	  cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, 
														 &arrOf_result_row_1, &arrOf_result_row_size_1,
														 &arrOf_result_row_2, &arrOf_result_row_size_2,
														 &arrOf_result_col, &arrOf_result_col_size);
	  //!
	  //! Valdiate that the 'fetched results from our disjoint-forst-comptuation is correct':
	  assert(arrOf_result_row_size_1 == cnt_expected_row_1); 
	  assert(arrOf_result_row_size_2 == cnt_expected_row_2); 
	  assert(arrOf_result_col_size == cnt_expected_col); 
	  // printf("arrOf_result_size=%u, cnt_expected=%u, at %s:%d\n", arrOf_result_size, cnt_expected, __FILE__, __LINE__);


	  uint *arrOf_result = arrOf_result_row_1; uint arrOf_result_size = arrOf_result_row_size_1;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_row_1, cnt_expected_row_1); //! ie, vlaidate that all 'entites' are found.
	  //! --
	  arrOf_result = arrOf_result_row_2; arrOf_result_size = arrOf_result_row_size_2;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_row_2, cnt_expected_row_2); //! ie, vlaidate that all 'entites' are found.
	  //! --
	  arrOf_result = arrOf_result_col; arrOf_result_size = arrOf_result_col_size;
	  __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp_col, cnt_expected_col); //! ie, vlaidate that all 'entites' are found.
	  //! De-allcote:
	  free_1d_list_uint(&arrOf_result_row_1); arrOf_result_row_1 = NULL;
	  free_1d_list_uint(&arrOf_result_row_2); arrOf_result_row_2 = NULL;
	  free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;
	  //assert(false); // FIXME: update [below]
	}
      }
      //! ---------------
    }
