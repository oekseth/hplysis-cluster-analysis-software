#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
//#include "db_ds_directMapping.h"
#include "db_ds_bTree_keyValue.h"
//#include "db_ds_bTree_rel.h"
#include "kt_matrix.h"
#include "measure_base.h"
/**
   @brief demonstrates how a bo build and access a b-tree w/elements = 'key-->value' access-pattern
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks from the teresults we observe that:
   -- result(a): there is an in-sigicnat time-cost-difference between different 'fixed' 'chunk-sizes' in B-tres:
   -- result(b): the exeuciton-time is 'strongly' simliar to a 'simplifed direct list-acces': difference may be explained by API-overhead in funciotn-call.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const uint nrows_base = 1000*100; 
  //const uint cnt_nrowsCases = 50; //! ie, the number of 'measurement-points'.
  const uint cnt_searches = 1000*1000*100; //! where we for the altter use 'psaudo-random numbers' wrt. the searches (through the 'use' of the modulo ("%")  operator).
  const bool isTo_useRandomAccess = true;
  const bool isTo_useKeyBasedSearchPattern = true; // which is set ot false impleis that we 'use' a 'complete traversal' to idneityf/elvuate the cases, ie, for the case where 'abitrary keys' are used.
  uint current_search = 0;
#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} else {if(current_search++ > nrows) {current_search = 0;} val_ret = current_search;} val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = search_id; if(isTo_useRandomAccess) {val_ret = rand() % nrows;} val_ret;})
#endif
  //!
  //! Iterate through the differnet 'row-size-cases':

  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id < nrows) {val_ret = (nrows-1) % search_id;} else {val_ret = search_id % (nrows-1);} val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id > 0) {if(search_id < nrows) {val_ret = nrows % search_id;} else {val_ret = search_id % (nrows-1);}}  val_ret;})
  //long long initseed = (configure_function_uniform_dataType)time(0);
  long long initseed = 100; //! ie, to ensure that the randomoized funciton r'eturns the same result for all our cese', ie, in cotnrast to a 'nromaø' "(configure_function_uniform_dataType)time(0);" call.
  srand(initseed);  
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = 0; if(search_id > 0) {val_ret = nrows % search_id;}  val_ret;})
  //#define __Mi__getRandVal(search_id) ({ uint val_ret = rand() % nrows; val_ret;})
  t_float result = 0;
  {
    const uint nrows =  nrows_base;
    float cmp_time_insert = FLT_MAX; float cmp_time_search = FLT_MAX;
    uint *arrOf_uints;
    uint col_cnt_insert = 0;     uint col_cnt_search = 0;
    { //! Construct a 'reference-set' where we use a 1d-list:
      start_time_measurement();
      arrOf_uints = (uint*)malloc(sizeof(uint)*nrows);
      assert(arrOf_uints);
      for(uint row_id = 0; row_id < nrows; row_id++) {
	arrOf_uints[row_id] = row_id;
      }      
      cmp_time_insert = end_time_measurement("Simple list-access(insert)", 0);
      //! 
      //! Search:
      start_time_measurement();
      for(uint search_id = 0; search_id < cnt_searches; search_id++) {
	const uint row_id = __Mi__getRandVal(search_id);
	//const uint row_id = search_id % nrows;
	result += (t_float)arrOf_uints[row_id];
      }
      cmp_time_search = end_time_measurement("Simple list-access(search)", 0);
    }

    //!
    //! Insert: 
    { //! Case: B-tree w/"key-->value" mapping: 
      for(uint blockCase = 0; blockCase < e_db_ds_bTree_cntChildren_undef; blockCase++) {
	start_time_measurement();
	s_db_ds_bTree_keyInt_t obj = init__s_db_ds_bTree_keyInt_t((e_db_ds_bTree_cntChildren_t)blockCase);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  s_db_baseNodeId_typeOfKey_int_t key; key.head = row_id;
	  insert__s_db_ds_bTree_keyInt_t(&obj, key);
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "B-tree(bCase:%u):insert", blockCase);
	  const float endTime = end_time_measurement(str_local, cmp_time_insert);
	}
	//! 
	//! Search:
	start_time_measurement();
	for(uint search_id = 0; search_id < cnt_searches; search_id++) {
	  const uint row_id = __Mi__getRandVal(search_id);
	  s_db_baseNodeId_typeOfKey_int_t key; key.head = row_id;
	  s_db_baseNodeId_typeOfKey_int_t result_obj;
	  //result += 
	  //printf("key = %u, at %s:%d\n", (uint)key.head, __FILE__, __LINE__);
	  find__s_db_ds_bTree_keyInt_t(&obj, key, &result_obj, (isTo_useKeyBasedSearchPattern == false), NULL);
	  result += (t_float)result_obj.head;
	}
	{
	  char str_local[2000] = {'\0'}; sprintf(str_local, "B-tree(bCase:%u):serach", blockCase);
	  const float endTime = end_time_measurement(str_local, cmp_time_search);
	  //! Update oru result-set:
	}
	//!
	//! De-allocate:
	free__s_db_ds_bTree_keyInt_t(&obj);
      }
    }
    //!
    //! De-allocate:
    assert(arrOf_uints);  free(arrOf_uints); arrOf_uints = NULL; //! ie, de-allocate.
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
