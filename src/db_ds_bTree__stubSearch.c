  const __Mi__localType *ptr = root;
bool isFound = false;
  while (ptr) {
    //! Find the 'correct fork' to search in (ie, if any):
    const int pos = __MiF__search__direct(key, ptr->key, ptr->n);
    if( (pos < ptr->n) &&  (1 == __MiF__cmp__equal(key, ptr->key[pos]) ) ) { 
      //printf("\tKey %d found in position %d of last dispalyed node (given search=%d)\n", __M__getKeyFroObj__node__int(key), pos, __M__getKeyFroObj__node__int(ptr->key[pos]));
      assert(ptr->key[pos].head == key.head); // TODO: cosnider removing this
      *scalar_result = ptr->key[pos];   
      __MF__updateResultStack((ptr->key[pos])); //! ie, update the result-set if "result_set" is 'used'
      isFound = true;
      // return 1; // TODO: cosnider including 'this' ... which is curretnly' omitted givne the psosiblity of 'arbiitrayr valeus' in the search-key'.
    }
    ptr = ptr->pointers[pos];
  }
  //printf("Key %d is not available\n", __M__getKeyFroObj__node__int(key));
  return isFound; //! ie, Not found.
