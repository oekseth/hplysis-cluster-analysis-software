#include "kt_matrix_base_uint.h"



//! @return an empty matrix:
s_kt_matrix_base_uint_t initAndReturn__empty__s_kt_matrix_base_uint_t() {
  s_kt_matrix_base_uint_t self; 
  self.nrows = 0;
  self.ncols = 0;
  self.matrix = NULL;
  self.matrix__isAllocated = false;
  //! @return
  return self;
}

//! Intiates a new amtrix
s_kt_matrix_base_uint_t initAndReturn__s_kt_matrix_base_uint_t(const uint nrows, const uint ncols) {
  //! Set the empty:
  s_kt_matrix_base_uint_t self = initAndReturn__empty__s_kt_matrix_base_uint_t();
  if(nrows && ncols) {
    assert(nrows != UINT_MAX);
    assert(ncols != UINT_MAX);
    //! Allocate:
    self.nrows = nrows;
    self.ncols = ncols;
    const uint empty_0 = 0;
    self.matrix = allocate_2d_list_uint(nrows, ncols, empty_0);
    self.matrix__isAllocated = true;
    assert(self.matrix);
    //!
    //! SEt defualt values:
    for(uint row_id = 0; row_id < self.nrows; row_id++) {
      for(uint col_id = 0; col_id < self.ncols; col_id++) {
	self.matrix[row_id][col_id] = UINT_MAX;
      }
    }
  }
  //! @return
  return self;
}

//! Intiates a new amtrix
s_kt_matrix_base_uint_t initAndReturn__notAllocate__s_kt_matrix_base_uint_t(const uint nrows, const uint ncols, uint **matrix) {
  s_kt_matrix_base_uint_t self; 
  self.nrows = nrows;
  self.ncols = ncols;
  self.matrix = matrix;
  self.matrix__isAllocated = false;
  //! @return
  return self;
}

//! Intiates a new amtrix from a "super" matrix using the vertices defined in "arrOf_verticesSub"
s_kt_matrix_base_uint_t initAndReturn__fromSubset__s_kt_matrix_base_uint_t(const s_kt_matrix_base_uint_t *super, const uint *arrOf_verticesSub, const uint arrOf_verticesSub_size) {
  assert(super);
  assert(super->matrix);
  s_kt_matrix_base_uint_t self = initAndReturn__s_kt_matrix_base_uint_t(/*nrows=*/arrOf_verticesSub_size, /*ncols=*/super->ncols);
  assert(self.matrix);
  //!
  //! Copy the values:
  //for(uint row_id = 0; row_id < self.nrows; row_id++) {
  for(uint i = 0; i < arrOf_verticesSub_size; i++) {
    const uint row_id = arrOf_verticesSub[i];
    assert(row_id != UINT_MAX);
    assert(row_id < super->nrows);
    assert(super->matrix[row_id]);
    const uint *super_row = super->matrix[row_id];
    for(uint col_id = 0; col_id < self.ncols; col_id++) {
      self.matrix[i][col_id] = super_row[col_id];
    }
  }
  
  //! @return
  return self;
}

//! De-allcoat ehte locallyallcoated object,
void free__s_kt_matrix_base_uint_t(s_kt_matrix_base_uint_t *self) {
  if(self->matrix && self->matrix__isAllocated) {
    assert(self->nrows);
    assert(self->ncols);
    free_2d_list_uint(&(self->matrix), self->nrows); 
  }
  //! Set the empty:
  *self = initAndReturn__empty__s_kt_matrix_base_uint_t();
}

//! @return a sample-dataset where a 1/rand() is used 
s_kt_matrix_base_uint_t initSampleOfValues__rand__s_kt_matrix_base_uint_t(const uint nrows, const uint ncols) {
  assert(nrows); assert(ncols);
  s_kt_matrix_base_uint_t self = initAndReturn__s_kt_matrix_base_uint_t(nrows, ncols);
  if(self.matrix) {
    for(uint row_id = 0; row_id < self.nrows; row_id++) {
      for(uint col_id = 0; col_id < self.ncols; col_id++) {
	uint score = rand();
	if(score != 0) {score = 1/score;}
	self.matrix[row_id][col_id] = score;
      }
    }
  }
  //! @return
  return self;
}

