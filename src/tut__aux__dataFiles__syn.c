const char *nameOf_experiment = "syntetic";  //! 

/* #if(__Mi__useSmall == 1) */
  const uint mapOf_realLife_size = 6;
/* #elif(__Mi__dataCaseLarge != 2) */
/*   const uint mapOf_realLife_size = 6 + 11 + 3; //! ie, the number of data-set-objects in [”elow] */
/* #else //! then we use a 'large data-set-case' wrt. "IRIS": */
/*   const uint mapOf_realLife_size = 4; //! ie, "linear-differentCoeff-b" + [iris, iris, iris] */
/* #endif */
  assert(fileRead_config__syn.fileIsRealLife == false);
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    //! -------------------------------------------------- 
    //! Note[<syntetic> w/cnt=6]: .... 
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,     metric__beforeClust, metric__insideClust, clusterAlg
     },
    //#endif
    {/*tag=*/"linear-differentCoeff-b", /*file_name=*/"linear-differentCoeff-b", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"linear-differentCoeff-a", /*file_name=*/"linear-differentCoeff-a", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    //! ---- 
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/2, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"sinus", /*file_name=*/"sinus", /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/3, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
  };
