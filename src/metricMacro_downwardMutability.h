/* #ifndef metricMacro_downwardMutability_h */
/* #define metricMacro_downwardMutability_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_downwardMutability
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "downwardMutability".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics

//! @return the correlations-core for: Vicis Wave Hedges
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = mathLib_float_abs(diff); \
  const t_float denumerator = macro_min(term1, term2); \
  const t_float result = macro_div_handleZeroInNumeratorAndDeNominator(numerator, denumerator); \
  result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedges__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MIN(term1, term2);	\
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = mathLib_float_abs(diff); \
  const t_float denumerator = macro_max(term1, term2); \
  const t_float result = macro_div_handleZeroInNumeratorAndDeNominator(numerator, denumerator); \
  result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMax__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2);	\
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = mathLib_float_abs(diff); \
  const t_float denumerator = macro_min(term1, term2); \
  const t_float result = macro_div_handleZeroInNumeratorAndDeNominator(numerator, denumerator); \
  result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__downwardMutability__VicisWaveHedgesMin__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MIN(term1, term2);	\
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; }) //! ie, return.


//! @return the correlations-core for: Vivis Symmetric
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = diff * diff; \
  const t_float denumerator = macro_min(term1, term2); \
  const t_float result = macro_div_handleZeroInNumeratorAndDeNominator(numerator, denumerator); \
  result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetric__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1, term2);		\
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MUL(diff, diff);		\
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MIN(term1, term2);		\
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; }) //! ie, return.
//! @return the correlations-core for: Vivis Symmetric
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = diff * diff; \
  const t_float denumerator = macro_max(term1, term2); \
  const t_float result = macro_div_handleZeroInNumeratorAndDeNominator(numerator, denumerator); \
  result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMax__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1, term2);		\
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MUL(diff, diff);		\
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2);		\
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; }) //! ie, return.
//! @return the correlations-core for: Vivis Symmetric
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMin(term1, term2) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = diff * diff; \
  const t_float denumerator = macro_min(term1, term2); \
  const t_float result = macro_div_handleZeroInNumeratorAndDeNominator(numerator, denumerator); \
  result; }) //! ie, return.
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMin__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__downwardMutability__VicisSymmetricMin__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1, term2);		\
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MUL(diff, diff);		\
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MIN(term1, term2);		\
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; }) //! ie, return.


//! @return the correlations-core for: symmetric min-max
#define correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax(term1, term2, obj) ({ \
  const t_float diff = term1 - term2; \
  const t_float numerator = diff * diff; \
  const t_float result_left = macro_div_handleZeroInNumeratorAndDeNominator(numerator, term1); \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_left) == false); \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_left) == false); \
  const t_float result_right = macro_div_handleZeroInNumeratorAndDeNominator(numerator, term2); \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_right) == false); \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_right) == false); \
  /*! Update the result-objet*/						\
  obj.numerator += result_left; obj.denumerator += result_right; })
#define correlation_macros__distanceMeasures__downwardMutability__symmetricMinMax__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1, term2);		\
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MUL(diff, diff);		\
      VECTOR_FLOAT_TYPE result_left = VECTOR_FLOAT_DIV(numerator, term1); \
      /*Handle dividye-by-zero-cases: */				\
      result_left = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result_left);	\
      result_left = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result_left);	\
      VECTOR_FLOAT_TYPE result_right = VECTOR_FLOAT_DIV(numerator, term2); \
      /*Handle dividye-by-zero-cases: */				\
      result_right = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result_right);	\
      result_right = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result_right);	\
      /*! Update the result-objet*/					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, result_left); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, result_right); })


#define correlation_macros__distanceMeasures__downwardMutability__symmetricMin__postProcess(obj) ({ \
  const t_float value = macro_min(obj.numerator, obj.denumerator); \
  metric_macro_defaultFunctions__postProcess__validateEmptyCase(value);})
#define correlation_macros__distanceMeasures__downwardMutability__symmetricMax__postProcess(obj) ({ \
  const t_float value = macro_max(obj.numerator, obj.denumerator); \
  metric_macro_defaultFunctions__postProcess__validateEmptyCase(value);})

/* #endif //! EOF */
