#include "kt_clusterAlg_kruskal.h"
#include "kt_forest_findDisjoint.h"


//! compute Kruskal MST using a sparse lsit-object obj_sparse as input (oekseth, 06. feb. 2017).
bool cluster_inputSparse__kt_clusterAlg_kruskal(s_kt_clusterAlg_hca_resultObject_t *obj_sparse, s_kt_clusterAlg_hca_resultObject_t *obj_result_hca, s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMeans, const s_kt_clusterAlg_config__ccm *config, const bool isAn__adjcencyMatrix, const uint unique_vertices) {
  if(!obj_result_hca && !obj_result_kMeans) {
    fprintf(stderr, "!!\t No result-objects are specified, ie, please udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  } else if(!obj_sparse || !obj_sparse->nrows || !obj_sparse->mapOf_nodes) {
    fprintf(stderr, "!!\t No input-data is specified, ie, please udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  } else if(isAn__adjcencyMatrix == false) {
    fprintf(stderr, "!!\t Your input-relationships does Not reflect an adjecency-data. In contrast we expected the data-input to be representing adjcency-data, ie, where relation[i][k] describes the dsitance between vertex(i)--vertex(k) (ie, in cotrnast to ffeature-matrix where the namespace of the rows and columns differs). To resovle the latter (for the use-case where your data-input representa a set of vertex--featre relationships) we suggest to comptue a simalirty-matrix using our high-eprformance distance-comptautison, eg, wrt. teh API foudn in our \"hp_distance.h\". To summarize, please udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  Node_t *arrOf_nodes = obj_sparse->mapOf_nodes; 
  const uint arrOf_nodes_size = obj_sparse->nrows;

  //!
  //! Identify the min-max-node:
  int min_node_id = INT_MAX;       int max_node_id = 0;
  int min_node_id_realWorld = 0; 
  {
    for(uint node_id_local = 0; node_id_local < arrOf_nodes_size; node_id_local++) {
      const Node_t node = arrOf_nodes[node_id_local];
      min_node_id = macro_min(min_node_id, node.right);
      min_node_id = macro_min(min_node_id, node.left);
      if(node.left != INT_MAX) {
	max_node_id = macro_max(max_node_id, node.left);	  
      }
      if(node.right != INT_MAX) {
	max_node_id = macro_max(max_node_id, node.right);	  
      }
    }
    assert(min_node_id != INT_MAX);
    assert(max_node_id > 0);
    //assert(max_node_id <= arrOf_nodes_size); //! ie, as we otherwise need updating [below]:
    /* if(max_node_id > arrOf_nodes_size) { */
    /*   arrOf_nodes_size = (uint)max_node_id; */
    /* } */
  }
  //!
  //! Allocate a temoprary list of vertices:
  //!
  //! Allocate space
  s_kt_clusterAlg_hca_resultObject_t _local__result_mst = setToEmptyAndreturn__s_kt_clusterAlg_hca_resultObject_t();
  const uint _local__result_mst__maxNrows = (uint)arrOf_nodes_size; // TODO: validate correctness of latter.
  //const uint _local__result_mst__maxNrows = (uint)arrOf_nodes_size; // TODO: validate correctness of latter.
  //const uint _local__result_mst__maxNrows = (uint)max_node_id; 
  uint inserted_edge = 0;
  if(obj_result_hca) {
    setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&_local__result_mst, _local__result_mst__maxNrows);
  } //! else we assume 'only the clusters' are of interest wr.t the reuslt.

  //!
  //! Sort, ie, to 'awlasy choose' teh 'best edges first':
  //! Order the nodes based on the assicated distance (to each node):
  qsort(arrOf_nodes, /*result-size=*/arrOf_nodes_size, sizeof(Node_t), nodecompare__Node_t); //! an appraoch/pre-step simlairy to: single-linkage hierarchical clustering (eg, SLINK).

  const bool __config__inUnifion_use2dSparse = true;

    

  //!
  //! Loop: intiate default cluste-rembershisp: each vertex 'by deifniont' is an 'isolated iseland':
  const uint cnt_vertices = _local__result_mst__maxNrows;
  // fprintf(stderr, "cnt_vertices=%u, max_node_id=%d, nrows=%u, at %s:%d\n", cnt_vertices, max_node_id, obj_sparse->nrows, __FILE__, __LINE__);
  const uint empty_0 = 0;
  uint *mapOf_vertexBelongsToCluster = allocate_1d_list_uint(cnt_vertices, empty_0);
  //if(__config__inUnifion_use2dSparse) {
  /*   for(uint i=0; i<cnt_vertices; i++) {     */
  /*     mapOf_vertexBelongsToCluster[i] = UINT_MAX; //! ie, as each vertex is 'by def' an 'siolated iseland'. */
  /*   } */
  /* } else { */
    for(uint i=0; i<cnt_vertices; i++) {    
      mapOf_vertexBelongsToCluster[i] = i; //! ie, as each vertex is 'by def' an 'siolated iseland'.
    }

#define __MiF__get_clusterId(nodex_index)({ uint clusterId = UINT_MAX; if( (nodex_index >= 0) && ((int)nodex_index < (int)cnt_vertices) ) {clusterId = (uint)mapOf_vertexBelongsToCluster[nodex_index];} else {clusterId_isOf_interest = false;} clusterId;})

    //  }
  s_kt_set_2dsparse_t listOf_membership_vertexCollection;
  if(__config__inUnifion_use2dSparse) {
    allocate__s_kt_set_2dsparse_t((&listOf_membership_vertexCollection), /*max-cnt-disjoitn-forestS=*/cnt_vertices, /*default_cnt_elements=*/cnt_vertices/10, /*matrix=*/NULL, /*mask=*/NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/false);
    for(uint i=0; i<cnt_vertices; i++) {    
      if(mapOf_vertexBelongsToCluster[i] == UINT_MAX) {
	assert(false); // TODO: validat corretness of this.
	//mapOf_vertexBelongsToCluster[i] = i; //! ie, as each vertex is 'by def' an 'siolated iseland'.
      }
    }
  }
  //! ----------------------------------------------------------------------
  //!
  //! Loop: Cosntruct sub-sets:
  uint cnt_clusters_current = cnt_vertices;
  const uint minClustersInSet__toBeEvaluated__wrtTrhesholds = (config) ? cnt_vertices * config->ccmToApply__minVertexCount__each__fraction : UINT_MAX;
  const t_float ccm_worst_caseValue = (config) ? get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(config) : T_FLOAT_MAX; //! ie, to get a 'tailedored' min-max-score' (oekseth, 06. mar. 2017).
  t_float ccm__global = ccm_worst_caseValue;
  for(uint edge_index = 0; edge_index < arrOf_nodes_size; edge_index++) {
    const Node_t node = arrOf_nodes[edge_index];
    if(node.left < 0) {continue;} // TODO: valdiate correncess of this assumption.
    if(node.right < 0) {continue;} // TODO: valdiate correncess of this assumption.
    /* if(mapOf_vertexBelongsToCluster[node.left] == UINT_MAX) {continue;} */
    /* if(mapOf_vertexBelongsToCluster[node.right] == UINT_MAX) {continue;} */
    bool clusterId_isOf_interest = true;
    const uint clusterId_left  = __MiF__get_clusterId(node.left); //! a macor-fucniton which also udpates our "clusterId_isOf_interest".
    const uint clusterId_right  = __MiF__get_clusterId(node.right); //! a macor-fucniton which also udpates our "clusterId_isOf_interest".
    if(clusterId_isOf_interest == false) {continue;}
       /* const uint clusterId_right = (uint)mapOf_vertexBelongsToCluster[node.right]; */
    /* const uint clusterId_left  = (uint)mapOf_vertexBelongsToCluster[node.left]; */
    /* const uint clusterId_right = (uint)mapOf_vertexBelongsToCluster[node.right]; */
    assert_possibleOverhead(clusterId_left != UINT_MAX);
    assert_possibleOverhead(clusterId_right != UINT_MAX);
    uint cnt_NotSet = 0;
    if(clusterId_left != clusterId_right) { //! then we merge the sets:
      bool isTo__merge_clusters = true;
      if( config && config->isTo_pply__CCM && (clusterId_right != UINT_MAX) && (clusterId_left != UINT_MAX)) {
	//! Investigate if described at least a trheshold chunk of vetices 'as part of clusters':	
	uint max_cluster_id__notSet = 0;
	{ //! First find the max-cluster-size, and then set to the 'not-set-cases' to "max_cluster_id__notSet++", ie, to 'isolated clusters
	  for(uint i=0; i<cnt_vertices; i++) {    
	    if(mapOf_vertexBelongsToCluster[i] != UINT_MAX) {
	      max_cluster_id__notSet = macro_max(max_cluster_id__notSet, mapOf_vertexBelongsToCluster[i]);
	    } else if( (mapOf_vertexBelongsToCluster[i] == UINT_MAX) ) {cnt_NotSet++;}
	  }
	}
	assert(cnt_NotSet <= cnt_vertices);
	//const uint cnt_set = cnt_vertices - cnt_NotSet;
	//const t_float fraq__set = (cnt_NotSet != 0) ? (t_float)cnt_NotSet/(t_float)cnt_vertices : 1;
	// FIXME: consider using a differnet strategy than [”elow] ... ie, intrsopect upon differnet staegies 'for such':
	if(cnt_NotSet < minClustersInSet__toBeEvaluated__wrtTrhesholds) { //! then we assume 'enough clusters' are set iof. the clustering-set to be intersting wrt. CCM-evaluation:
	  if(__config__inUnifion_use2dSparse) {

	    uint arrOf_clust_1_size = 0;
	    const uint *arrOf_clust_1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&listOf_membership_vertexCollection, clusterId_left, &arrOf_clust_1_size);
	    if(arrOf_clust_1 && (arrOf_clust_1_size > minClustersInSet__toBeEvaluated__wrtTrhesholds)) {	  
	      assert(arrOf_clust_1_size != UINT_MAX); assert(arrOf_clust_1_size > 0);
	      uint arrOf_clust_2_size = 0;
	      const uint *arrOf_clust_2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&listOf_membership_vertexCollection, clusterId_right, &arrOf_clust_2_size);
	      if(arrOf_clust_2 && (arrOf_clust_2_size > minClustersInSet__toBeEvaluated__wrtTrhesholds)) {	  
		assert(arrOf_clust_2_size != UINT_MAX); assert(arrOf_clust_2_size > 0);
		//!
		//! Then we evlauate the CCM-score of 'this appraoch', comparing to the 'previous score':
		//! Note: Allocate a new mapping-list, and update the mapping-scores:
		uint *__mapOf_vertexBelongsToCluster = allocate_1d_list_uint(cnt_vertices, empty_0);

		for(uint i=0; i<cnt_vertices; i++) {    
		  if(mapOf_vertexBelongsToCluster[i] == clusterId_left) {
		    assert(clusterId_right != UINT_MAX);
		    __mapOf_vertexBelongsToCluster[i] = clusterId_right;
		  } else if(mapOf_vertexBelongsToCluster[i] == UINT_MAX) {
		    __mapOf_vertexBelongsToCluster[i] = max_cluster_id__notSet++; //! ie, as each vertex is 'by def' an 'siolated iseland'.
		  }
		}
	      
		//! Apply CCM update if the result 'is improved'
		assert(config);
		const t_float ccm__local = apply__CCM__s_kt_clusterAlg_config__ccm_t(config, __mapOf_vertexBelongsToCluster);
		if(ccm__local != T_FLOAT_MAX) {
		  // FIXME: condier to investigate a parameter to 'know' if 'low' OR 'high' cmc-score idnicates 'good clsuters'.
		  if(ccm__global != ccm_worst_caseValue) {
		    if(ccm_worst_caseValue == T_FLOAT_MAX) {
		      if(ccm__local > ccm__global) {
			ccm__global = ccm__local;
		      } else {
			isTo__merge_clusters = false;
		      }
		    } else if(ccm_worst_caseValue == T_FLOAT_MIN_ABS) {
		      if(ccm__local < ccm__global) {
			ccm__global = ccm__local;
		      } else {
			isTo__merge_clusters = false;
		      }
		    } else {assert(false);} //! ie, as we then need adding suppoirt or this 'use-case'.
		  } else {ccm__global = ccm__local;}
		}		
		//!
		//! De-allocate:
		free_1d_list_uint(&__mapOf_vertexBelongsToCluster); __mapOf_vertexBelongsToCluster = NULL;
	      }
	    }
	  } else {
	    fprintf(stderr, "!!\t option Not supported: please contact the senior developer [eosketh@gmail.com] for this feature. OBservaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	    assert(false); // TODO: add support for this.
	  }
	} //! else we assume that 'all to little is known' wr.t the vertices 'in the set', ie, for a CCM-evlauation 'to have a point'.
      }

      if(isTo__merge_clusters) {
	//! Then we 'by definiotn: we have merged/unfied two different vertex-clsuters:
	//! Note: as we assuem 'the two smallest clsuters' have now been merged, we assuem ti is 'safe to cut off the count' if the covnergence-critera has been bet:
	cnt_clusters_current--; //! ie, as two vertices 'have been merged'.
	//!
	if(obj_result_hca) { //! add the cluster to the MST: 
	  assert(inserted_edge < _local__result_mst__maxNrows);
	  _local__result_mst.mapOf_nodes[inserted_edge++] = node; ///next_edge;
	}
	//! Then merg ehte lcusters:
	if(__config__inUnifion_use2dSparse) {
	  //! Then iterate through a sparse list .... using logics in our disjtoint-forest-procedure
	  listOf_membership_vertexCollection = static__merge_sets__kt_forest_findDisjoint(/*this_key=*/(uint)node.left, /*parent_key=*/(uint)node.right, /*mapOf_vertexMembershipId=*/mapOf_vertexBelongsToCluster, &listOf_membership_vertexCollection);
	} else { //! then we 'unify the relationships/entities':
	  for(uint i=0; i<cnt_vertices; i++) {
	    if(mapOf_vertexBelongsToCluster[i] == clusterId_left) {
	      assert(clusterId_right != UINT_MAX);
	      mapOf_vertexBelongsToCluster[i] = clusterId_right;
	    }
	  }
	}
      }
      /* spanlist.data[spanlist.n] = elist.data[i]; */
      /* spanlist.n = spanlist.n+1; */
      /* alt2_union1(belongs,cno1,cno2);     */
    }
    //! ----------------------------------------------------
    //! 
    //! Investigate if we have 'readched the limit' wrt. our evlauation:
    /* if( */
    /*    (cnt_vertices - cnt_clusters_current)  */
    /*    //cnt_NotSet < minClustersInSet__toBeEvaluated__wrtTrhesholds */
    /*    )  */
    { //! then we assume 'enough clusters' are set iof. the clustering-set to be intersting wrt. k-means-max-evaluation:

      //! NotE: idea in [”elow] is to stop the merging when there are less than 'k' clusters 'remaining' to be merged: as "cnt_clusters_current" are the number of clusters which have been merged, "cnt_vertices - cnt_clusters_current" ....??...  ... a 'count' which may be idenifed through ....??... 
      if(config && __config__inUnifion_use2dSparse) {
	// FIXME: validate correctness of [below]
	const uint cnt_clusters = (cnt_vertices - cnt_clusters_current) ;
	// FIXME: 'figure out' hwo to use the 'min-clsuter-count' .... eg, by 'using' a min-number-of-vertcies-in-each-clsuter as a 'coutn-trehshold'
	/* uint cnt_clusters = 0; */
	/* for(uint disjointForest_id = 0; disjointForest_id < (listOf_membership_vertexCollection.biggestInserted_row_id+1); disjointForest_id++) { */
	/*   const uint forest_id = disjointForest_id; */
	/*   uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection), forest_id, &list_size); */
	/*   if(list_size > 0) {cnt_clusters++;} */
	/* } */
	if(config->k_clusterCount__min != UINT_MAX) {
	  if(cnt_clusters_current < config->k_clusterCount__min) { //! then we 'break':
	    edge_index = arrOf_nodes_size; //! ie, 'top the elvuaiaotn'.
	  }
	}
	/* if(config->k_clusterCount__max != UINT_MAX) {	 */
	/*   if(cnt_clusters > config->k_clusterCount__max) { //! then we 'break': */
	/*     edge_index = arrOf_nodes_size; //! ie, 'top the elvuaiaotn'. */
	/*   } */
	/* } */
      }
    }
  }
  if(obj_result_hca && false) {
    printf("Following are the edges in the constructed MST\n");
    for(uint i = 0; i < inserted_edge; ++i) {
      const Node_t node = _local__result_mst.mapOf_nodes[i];
      printf("%d -- %d == %f\n", node.left, node.right, node.distance);
    }
  }
  //! ----------------------------------------------------------------------
  //!
  //! Budid the result-objects:
  //! Note: store teh result-object "result" in a "kt_clusterAlg_hca_resultObject.h" ... OR a "kt_clusterAlg_fixed_resultObject.h" .... ie, 'depending' upon configuraitons in the user-object: if a user has Not requedsted any k-means-trehshold' then the 'hca' result-object is used.2
  if(obj_result_kMeans) {
    if(cnt_clusters_current == 0) {cnt_clusters_current = 1;}
    init__s_kt_clusterAlg_fixed_resultObject_t(obj_result_kMeans, cnt_clusters_current, unique_vertices);
    assert(obj_result_kMeans->vertex_clusterId);
    for(uint i=0; i < unique_vertices; i++) { //! ie, the n copy the mappings:
    //for(uint i=0; i<cnt_vertices; i++) { //! ie, the n copy the mappings:
      obj_result_kMeans->vertex_clusterId[i] = mapOf_vertexBelongsToCluster[i];      
    }
  }
  if(obj_result_hca) {
    /* setTo_empty__s_kt_clusterAlg_hca_resultObject_t(obj_result_hca, inserted_edge); */
    /* for(uint i = 0; i < inserted_edge; ++i) { */
    setTo_empty__s_kt_clusterAlg_hca_resultObject_t(obj_result_hca, unique_vertices);
    // fprintf(stderr, "cnt_vertices=%u, at %s:%d\n", cnt_vertices, __FILE__, __LINE__);
    const uint cnt_ele = macro_min((uint)unique_vertices, (uint)_local__result_mst.nrows);
    for(uint i = 0; i < cnt_ele; ++i) {
      const Node_t node = _local__result_mst.mapOf_nodes[i];    
      obj_result_hca->mapOf_nodes[i] = node;
    }
  }

  //! ----------------------------------------------------------------------
  //!
  //! De-allocate:
  free_1d_list_uint(&mapOf_vertexBelongsToCluster); mapOf_vertexBelongsToCluster = NULL;
  if(obj_result_hca) {
    free__s_kt_clusterAlg_hca_resultObject_t(&_local__result_mst);
  }
  if(__config__inUnifion_use2dSparse) {
    free_s_kt_set_2dsparse_t(&(listOf_membership_vertexCollection));
  }
  //!
  //! @return success
  return true;
}

//! compute Kruskal MST using a matrix obj_1 as input (oekseth, 06. feb. 2017).
bool cluster_inputMatrix__kt_clusterAlg_kruskal(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_hca_resultObject_t *obj_result_hca, s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMeans, const s_kt_clusterAlg_config__ccm *config, const bool isAn__adjcencyMatrix) {
  if(!obj_result_hca && !obj_result_kMeans) {
    fprintf(stderr, "!!\t No result-objects are specified, ie, please udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  } else if(!obj_1 || !obj_1->nrows || !obj_1->ncols || !obj_1->matrix) {
    fprintf(stderr, "!!\t No input-data is specified, ie, please udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  } else if(isAn__adjcencyMatrix == false) {
    fprintf(stderr, "!!\t Your input-relationships does Not reflect an adjecency-data. In contrast we expected the data-input to be representing adjcency-data, ie, where relation[i][k] describes the dsitance between vertex(i)--vertex(k) (ie, in cotrnast to ffeature-matrix where the namespace of the rows and columns differs). To resovle the latter (for the use-case where your data-input representa a set of vertex--featre relationships) we suggest to comptue a simalirty-matrix using our high-eprformance distance-comptautison, eg, wrt. teh API foudn in our \"hp_distance.h\". To summarize, please udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  //!
  //! Identify number of edges
  long long int cnt_interestingVertices = 0;
  for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
    assert(obj_1->matrix[row_id]);
    for(uint col_id = 0; col_id < obj_1->ncols; col_id++) {
      if(isOf_interest(obj_1->matrix[row_id][col_id])) { //! ie, inveestigate "T_FLOAT_MAX":
	cnt_interestingVertices++;
      }
    }
  }
  if(cnt_interestingVertices == 0) {
    fprintf(stderr, "!!\t No input-data is specified: all vertices in a matrix[%u, %u] where set to T_FLOAT_MAX, ie, either drop calling this funciton or alternativly we suggest to udpate your API: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", obj_1->nrows, obj_1->ncols, __FUNCTION__, __FILE__, __LINE__);
    return false;
  } else if(cnt_interestingVertices >= (long long int)UINT_MAX) {
    fprintf(stderr, "!!\t Your inptu-data exceesed the expected threshold of %lld elements (a threshold specified by the 'uint' data-type with cnt-bytes='%u', ie, for an input with matrix-dims[%u, %u]. In brief, please apply a simalrity-appraoch + maksing before calling this function: for quesitons pelase cotnac tthe senior devleoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_interestingVertices, (uint)sizeof(uint),  obj_1->nrows, obj_1->ncols, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }

  //fprintf(stderr, "nrows=%u, at %s:%d\n", obj_1->nrows, __FILE__, __LINE__);

  //!
  //! Allocate space
  s_kt_clusterAlg_hca_resultObject_t obj_local = setToEmptyAndreturn__s_kt_clusterAlg_hca_resultObject_t();
  //setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_local, obj_1->nrows);
  setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_local, cnt_interestingVertices);
  //assert(obj_local.nrows >= cnt_interestingVertices);
  //!
  {//! Insert relationships:
    long long int cnt_curr = 0;
    for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
      assert(obj_1->matrix[row_id]);
      for(uint col_id = 0; col_id < obj_1->ncols; col_id++) {
	if(isOf_interest(obj_1->matrix[row_id][col_id])) { //! ie, inveestigate "T_FLOAT_MAX":
	  assert(cnt_curr < cnt_interestingVertices);
	  Node_t loca_node = initAndReturn_Node_t(/*left=*/(int)row_id,/*right=*/(int)col_id,/*distance=*/obj_1->matrix[row_id][col_id]);
	  assert(cnt_curr < obj_local.nrows);
	  obj_local.mapOf_nodes[cnt_curr] = loca_node;
	  cnt_curr++;
	}
      }
    }
  }
  // fprintf(stderr, "nrows=%u, at %s:%d\n", obj_1->nrows, __FILE__, __LINE__);
  //!
  //! The call:
  const bool is_ok = cluster_inputSparse__kt_clusterAlg_kruskal(&obj_local, obj_result_hca, obj_result_kMeans, config, isAn__adjcencyMatrix, obj_1->nrows);

  //!
  //! De-allcoate lcoal varibles:
  free__s_kt_clusterAlg_hca_resultObject_t(&obj_local);
  
  //!
  //! @return the success-flag:
  return is_ok;
}
