#include "kt_distance__groupOf_direct__1ToMany.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "correlation_macros__distanceMeasures.h" //! which 'provide' a bunch of auxialiary-functions used in "metricMacro_directScore.h"
#include "metricMacro_directScore.h" //! which is a set of non-tiled-correlation-metrics (oekseth, 06. des. 2016).

//! Comptue for one-to-many
// @remarks Compute the correlation-score for the "directScore"-group of hpLysis-correlation-metrics.
void corr__computeDirectScore__oneToMany(const e_kt_correlationFunction_t metric_id, const uint row_id_1, const uint nrows_2, t_float **data_1, t_float **data_2, char **mask1, char **mask2, t_float *weight, t_float *list_result, const e_cmp_masksAre_used_t  masksAre_used) {
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1) //! then we apply logics:
  assert(list_result); //! ie, as we expec the result-matrix to 'have been allcoated'.
  assert(data_1);
  assert(data_2);
  const uint row_id = row_id_1;

#define __stub_metricGroup__allAgainstAll 0
#define __stub_metricGroup__oneToMany 1
#include "kt_distance__stub__metricGroup__directScore__main.c"
#else //! then generate a warning:
  globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct__generateWarning();
  return; // T_FLOAT_MAX;
#endif
}
