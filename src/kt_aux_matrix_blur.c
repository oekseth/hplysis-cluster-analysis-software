#include "kt_aux_matrix_blur.h"

#ifndef PI
#define PI 3.141592653589793
#endif

//! Idneitfy standard deviation for the Blur
//! Src: "http://blog.ivank.net/fastest-gaussian-blur.html"
static t_float *__aux__matrix__blur__STD(const t_float sigma, const uint n) {
  assert(sigma != 0);
  assert(n != 0);
  assert(n != UINT_MAX);
  const t_float wIdeal = sqrt((12*sigma*sigma/n)+1);  // Ideal averaging filter width 
  int wl = floor(wIdeal);  if(wl%2==0) wl -= 1;
  int wu = wl+2;
  
  t_float mIdeal = (12*sigma*sigma - n*wl*(t_float)wl - 4*n*(t_float)wl - 3*n)/(-4*(t_float)wl - 4);
  t_float m = roundf(mIdeal);
  // var sigmaActual = Math.sqrt( (m*wl*wl + (n-m)*wu*wu - n)/12 );
  
  t_float empty_0 = 0;
  t_float *arr_sizes = allocate_1d_list_float(n, empty_0);  for(uint i=0; i<n; i++) {arr_sizes[i] = i<m?wl:wu;}
  return arr_sizes;
}
//! Compute "Gaussian Blur": 
//! Note: Overehad cosnerns ....
//! Src: "http://blog.ivank.net/fastest-gaussian-blur.html"
static s_kt_matrix_base_t __blur__Gaussian_4_accumulator(s_kt_matrix_base_t *self, const t_float r) {
  assert(self);
  assert(self->nrows);
  assert(self->ncols);
  s_kt_matrix_base_t obj_new = initAndReturn__s_kt_matrix_base_t(self->nrows, self->ncols); //, false);
  assert(obj_new.nrows == self->nrows);
  assert(obj_new.ncols == self->ncols);
  //!
  //! Apply logics: 
  //!
  //! Apply logics: pre-step: 
  const uint box_count = 3;
  t_float *arr_sizes = __aux__matrix__blur__STD(r, /*count=*/box_count);
  assert(arr_sizes);
  const uint nrows = self->nrows;
  const uint ncols = self->ncols;
  //!
  //! Apply logics: 
  s_kt_matrix_base_t *box_self = self;  s_kt_matrix_base_t *box_new = &obj_new;
  for(uint box_id = 0; box_id < box_count; box_id++) {
    //assert(arr_sizes[box_id] != 1);  // TODO: cvliadate correcnteess of dorpping this (which occure for cases where raidus is amlm) (oekseth, 06.11.2020).
    assert(obj_new.matrix == box_new->matrix); //! ie, based on the 'swapping' squenece of the result matrix.
    //! 
    const t_float r_local = 0.5*(arr_sizes[box_id]-1);
    //!
    //!
    for(uint for_rows = 0; for_rows < 2; for_rows++) { //! ie, evaluate two different cases.
      //!
      //!
      if(for_rows == 0) {
	t_float iarr = 1.0 / (r_local+r_local+1);
	for(uint i=0; i<nrows; i++) {
	  const uint row_id = i;
	  // uint li = row_id;
	  //! Note: the idea of [”elow] is to 'drag' the valeus alongside each diemsnion, hnencre creating a 'blur' effect. The axxumulator which is used is the "first value" (fv). 
	  uint col_id = (uint)r_local ;
	    //uint row_id = i*w, li = row_id, ri = row_id+r;
	  t_float fv = box_self->matrix[row_id][0]; //! ie, "first value".
	  uint fv_pos = 1;
	  while(!isOf_interest(fv) && (fv_pos < ncols) ) {fv = box_self->matrix[row_id][fv_pos++]; } //! ie, icnrement until we have foudn an itnresting value.
	  t_float lv = box_self->matrix[row_id][ncols - 1]; //! ie, "last value".
	  uint lv_pos = ncols - 2;
	  while(!isOf_interest(lv) && (lv_pos > fv_pos) ) {fv = box_self->matrix[row_id][lv_pos--]; } //! ie, icnrement until we have foudn an itnresting value.
	  assert(lv_pos >= fv_pos);
	  if(!isOf_interest(lv) || !isOf_interest(fv) || ( ((t_float)lv_pos - (t_float)fv_pos) <= r_local) ) {continue;} //! ie, then drop evlauating this case. 
	  //! 
	  //! 
	  t_float val = (r_local + 1)*fv; 
	  for(uint j=0; j<(uint)r_local; j++) { 
	    const t_float score = box_self->matrix[row_id][j]; 
	    if(isOf_interest(score)) {
	      val += score;
	    }
	  }
	  //! 
	  //! 
	  uint li = 0, ti = 0;
	  for(uint j=0  ; j<=(uint)r_local ; j++) { 
	    const t_float score = box_self->matrix[row_id][col_id++];
	    if(isOf_interest(score)) {
	      val += score - fv;   
	      box_new->matrix[row_id][j] = round(val*iarr); 
	    }
	    ti++;
	  }	  
	  //! 
	  //! 
	  for(uint j=(uint)r_local+1; j<ncols - (uint)r_local; j++) { 
	    const t_float score_1 = box_self->matrix[row_id][col_id++];
	    const t_float score_2 = box_self->matrix[row_id][li++];
	    if(isOf_interest(score_1) && isOf_interest(score_2)) {
	      val += score_1 - score_2;   
	      box_new->matrix[row_id][j]  = round(val*iarr); 
	    }
	  }
	  //! 
	  //! 
	  for(uint j=ncols- r_local ; j<ncols  ; j++) { 
	    const t_float score = box_self->matrix[row_id][li];
	    if(isOf_interest(score)) {
	      val += lv        - score;
	      box_new->matrix[row_id][ti] = round(val*iarr); 
	    }
	    ti++; li++;
	  }
	}
      } else {
	t_float iarr = 1.0 / (r_local+r_local+1);
	//!
	//!
	for(uint col_id=0; col_id < ncols; col_id++) {
	  //uint ti = col_id, li = 0; // ri = ti+r*w;
	  t_float fv = box_new->matrix[0][col_id], lv = box_new->matrix[nrows-1][col_id]; 
	  //!
	  //!
	  uint fv_pos = 1;
	  while(!isOf_interest(fv) && (fv_pos < nrows) ) {fv = box_self->matrix[fv_pos++][col_id]; } //! ie, icnrement until we have foudn an itnresting value.
	  assert(nrows > 2); //! as we otherwise have a bug. 
	  //assert(ncols > 2); //! as we otherwise have a bug. 
	  uint lv_pos = nrows - 2;
	  //uint lv_pos = ncols - 2;
	  while(!isOf_interest(lv) && (lv_pos > fv_pos) ) {
	    fv = box_self->matrix[lv_pos][col_id];  //! ie, icnrement until we have foudn an itnresting value.
	    assert(lv_pos > 0); //! ie, as we otherwise have a bug.
	    lv_pos--;
	  }
	  // assert(lv_pos >= fv_pos);
	  if(!isOf_interest(lv) || !isOf_interest(fv) || ( ((t_float)lv_pos - (t_float)fv_pos) <= r_local) ) {continue;} //! ie, then drop evlauating this case. 
	  //! 
	  //! 


	  //ti+w*(h-1)], 
	  t_float val = (r_local+1)*fv;
	  for(uint j=0; j<r_local; j++) {
	    const t_float score = box_new->matrix[j][col_id];
	    if(isOf_interest(score)) {
	      val += box_new->matrix[j][col_id];
	    }
	  }
	  // uint row_id = (uint)r_local;
	  for(uint j=0; j <= (uint)r_local ; j++) { 
	    //assert(ti < ncols);
	    const t_float score = box_new->matrix[j][col_id];
	    if(isOf_interest(score)) {
	      val += score - fv; 
	      box_self->matrix[/*ti=*/j][col_id] = round(val*iarr);  // ri+=w; ti+=w; 
	    }
	    //row_id++; 
	  }
	  uint li = 0, ti = 0;
	  for(uint j=(uint)r_local+1; j<nrows-(uint)r_local; j++) { 
	    const t_float score_1 = box_new->matrix[j][col_id];
	    const t_float score_2 = box_new->matrix[li][col_id];
	    if(isOf_interest(score_1) && isOf_interest(score_2)) {
	      val +=  score_1 - score_2; 
	      box_self->matrix[j][col_id] = round(val*iarr);  
	    }
	    li++; 
	    // ri++;
	    //ti++; 
	  }
	  for(uint j=nrows-(uint)r_local; j< nrows; j++) { 
	    const t_float score = box_new->matrix[j][col_id];
	    if(isOf_interest(score)) {
	      val += lv      - score; 
	      box_self->matrix[j][col_id] = round(val*iarr);  
	    }
	    //li++; // ti++;
	  }
	}
	
      }
      //!
      //!
      //!
      //! Swap boxes:
      s_kt_matrix_base_t *tmp = box_self; box_self = box_new; box_self = tmp;
      assert(box_self != box_new);
    }
  }

  //!
  //! De-allocate:
  free_1d_list_float(&arr_sizes);
  //!
  //! @return
  return obj_new;
}


//! Compute "Gaussian Blur": 
//! Note: Overehad cosnerns ....
//! Src: "http://blog.ivank.net/fastest-gaussian-blur.html"
static s_kt_matrix_base_t __blur__Gaussian_3_oneDim(s_kt_matrix_base_t *self, const t_float r) {
  assert(self);
  assert(self->nrows);
  assert(self->ncols);
  s_kt_matrix_base_t obj_new = initAndReturn__s_kt_matrix_base_t(self->nrows, self->ncols); //, false);
  assert(obj_new.nrows == self->nrows);
  assert(obj_new.ncols == self->ncols);
  //!
  //! Apply logics: 
  //!
  //! Apply logics: pre-step: 
  const uint box_count = 3;
  t_float *arr_sizes = __aux__matrix__blur__STD(r, /*count=*/box_count);
  assert(arr_sizes);
  //!
  //! Apply logics: 
  s_kt_matrix_base_t *box_self = self;  s_kt_matrix_base_t *box_new = &obj_new;
  for(uint box_id = 0; box_id < box_count; box_id++) {
    // assert(arr_sizes[box_id] != 1);  // TODO: cvliadate correcnteess of dorpping this (which occure for cases where raidus is amlm) (oekseth, 06.11.2020).
    const t_float r_local = 0.5*(arr_sizes[box_id]-1);
    //!
    //!
    for(uint for_rows = 0; for_rows < 2; for_rows++) { //! ie, evaluate two different cases.
      //!
      //!
      for(int i=0; i < (uint)self->nrows; i++) {
	if(for_rows == 0) {
	  for(int j=0; j < (uint)self->ncols; j++) {
	    t_float val = 0;
	    for(int ix = j-r_local; ix<j+r_local+1; ix++) {
	      //! Identify valid coordiantes: 
	      int x = macro_min((int)self->ncols -1, macro_max(0, ix));
	      const t_float score = box_self->matrix[i][x];
	      if(isOf_interest(score)) {
		val += score;
	      }
	    }
	    if( (val != 0) && isOf_interest(val))  {
	      box_new->matrix[i][j] = val/(r_local+r_local+1);
	    }
	  }
	} else {
	  for(int j=0; j < (uint)self->ncols; j++) {
	    t_float val = 0;
	    for(int iy = i-r_local; iy<i+r_local+1; iy++) {
	      //! Identify valid coordiantes: 
	      int x = macro_min((int)self->nrows -1, macro_max(0, iy));
	      const t_float score = box_self->matrix[x][j];
	      if(isOf_interest(score)) {
		val += score;
	      }
	    }
	    if(val != 0) {
	      box_new->matrix[i][j] = val/(r_local+r_local+1);
	    }
	  }
	}
      }
      //!
      //!
      //!
      //! Swap boxes:
      s_kt_matrix_base_t *tmp = box_self; box_self = box_new; box_self = tmp;
      assert(box_self != box_new);
    }
  }
  //!
  //! Validate cosnistnecy in result:
  assert(obj_new.matrix == box_new->matrix);
  assert(self->matrix   == box_self->matrix);
  //!
  //! De-allocate:
  free_1d_list_float(&arr_sizes);
  //!
  //! @return
  return obj_new;
}


//! Compute "Gaussian Blur": 
//! Note: Overehad cosnerns ....
//! Src: "http://blog.ivank.net/fastest-gaussian-blur.html"
static s_kt_matrix_base_t __blur__Gaussian_2_box(s_kt_matrix_base_t *self, const t_float r) {
  assert(self);
  assert(self->nrows);
  assert(self->ncols);
  s_kt_matrix_base_t obj_new = initAndReturn__s_kt_matrix_base_t(self->nrows, self->ncols); //, false);
  assert(obj_new.nrows == self->nrows);
  assert(obj_new.ncols == self->ncols);
  //!
  //! Apply logics: pre-step: 
  const uint box_count = 3;
  t_float *arr_sizes = __aux__matrix__blur__STD(r, /*count=*/box_count);
  assert(arr_sizes);
  //!
  //! Apply logics: 
  s_kt_matrix_base_t *box_self = self;  s_kt_matrix_base_t *box_new = &obj_new;
  for(uint box_id = 0; box_id < box_count; box_id++) {
    //if(arr_sizes[box_id] == 1);
    //assert(arr_sizes[box_id] != 1); // TODO: cvliadate correcnteess of dorpping this (which occure for cases where raidus is amlm) (oekseth, 06.11.2020).
    const t_float r_local = 0.5*(arr_sizes[box_id]-1);
    //!
    //!
    for(int i=0; i < (uint)self->nrows; i++) {
      for(int j=0; j < (uint)self->ncols; j++) {
	t_float val = 0;
	for(int iy = i-r_local; iy<i+r_local+1; iy++) {
	  for(int ix = j-r_local; ix<j+r_local+1; ix++) {
	    //! Identify valid coordiantes: 
	    int x= macro_min((int)self->nrows -1, macro_max(0, iy));
	    int y = macro_min((int)self->ncols -1, macro_max(0, ix));
	    //! Increment the weight: 
	    const t_float score = box_self->matrix[x][y];
	    if(isOf_interest(score)) {
	      val += score;
	    }
	  }
	}
	if(val != 0) {
	  box_new->matrix[i][j] = val /( (r_local+r_local+1)*(r_local+r_local+1));
	}
      }
    }
    //!
    //! Swap boxes:
    s_kt_matrix_base_t *tmp = box_self; box_self = box_new; box_self = tmp;
    assert(box_self != box_new);
  }

  //!
  //! @return
  free_1d_list_float(&arr_sizes);
  return obj_new;
}

//! Compute "Gaussian Blur": 
//! Note: Overehad cosnerns ....
//! Src: "http://blog.ivank.net/fastest-gaussian-blur.html"
static s_kt_matrix_base_t __blur__Gaussian_1_generic(s_kt_matrix_base_t *self, const t_float r) {
  assert(self);
  assert(self->nrows);
  assert(self->ncols);
  s_kt_matrix_base_t obj_new = initAndReturn__s_kt_matrix_base_t(self->nrows, self->ncols); //, false);
  assert(obj_new.nrows == self->nrows);
  assert(obj_new.ncols == self->ncols);
  //!
  //! Apply logics: 

  t_float r_local = ceilf(r * 2.57);     // significant radius
  for(int i=0; i < (uint)self->nrows; i++) {
   for(int j=0; j < (uint)self->ncols; j++) {
      t_float val = 0, wsum = 0;
      for(int iy = i-r_local; iy<i+r_local+1; iy++) {
	for(int ix = j-r_local; ix<j+r_local+1; ix++) {
	  //! Identify valid coordiantes: 
	  int x = macro_min((int)self->ncols -1, macro_max(0, ix));
	  int y = macro_min((int)self->nrows -1, macro_max(0, iy));
	  const t_float score = self->matrix[y][x];
	  if(isOf_interest(score)) {
	    //! Idnetify the size of the boudning area: 
	    int dsq = (ix-j)*(ix-j)+(iy-i)*(iy-i);
	    //! Adjust the area:
	    t_float wght = expf( -dsq / (2*r*r) ) / (PI*2*r*r);
	    //! Adjsut + increment the weight: 
	    val += score * wght;  wsum += wght;
	  }
	}
      }
      obj_new.matrix[i][j] = roundf(val/wsum);            
    }
  }
  //!
  //! @return
  return obj_new;
}

s_kt_matrix_base_t apply_blur__kt_aux_matrix_blur(s_kt_matrix_base_t *self, const e__blur enum_id, const t_float radius) {
  if( (enum_id == e__blur_undef) || (enum_id == e__blur_gaussian_4_accumulator) ) {
    return __blur__Gaussian_4_accumulator(self, radius);
  } else if(enum_id == e__blur_gaussian_1_generic) {
    return __blur__Gaussian_1_generic(self, radius);
  } else if(enum_id == e__blur_gaussian_2_box) {
    return __blur__Gaussian_2_box(self, radius);
  } else if(enum_id == e__blur_gaussian_3_oneDim) {
    return __blur__Gaussian_3_oneDim(self, radius);
    //  } else if(enum_id == ) {
  } else {
    fprintf(stderr, "!!\t (not-supported)\t Add support for enum_id=%u, ie, requsting a code udpate to the snior developer [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
  }
  //! A fall-back: 
  return initAndReturn__empty__s_kt_matrix_base_t();
}
