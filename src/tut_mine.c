#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"

/**
   @brief a simplitic example to how corrleating amtrices using hpLyssi impelmantion fot eh MINE correlation-metric  (oekseth, 06. jan. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. jan. 2017).
   @remarks Demonstrates features wrt.:
   -- input: different appraoches to speciify d aata-matrix:
   -- computation: how to use the MINE correlation-metic (which is the defualt non-specified metric).
   -- result: examplies the use of three differnet strategies for evlauating the results: 
   (a) input-matrix: the call to "export__singleCall__s_kt_matrix_t(..)" in order to expoert a data-set of type "kt_matrix_t".
   (b) generate results using 'direct' expor-fucnitonaltiy (specified in the 'costnrcuytor'): included to simplify the number of cofniguraiotn-calls.
   (c) call an exprot-rotuine in hpLysis: provide a rich cofniguraiton-space wrt. result-generaiotn.
   (d) direct access the exprot-object, eg, to be sued as inptu for further analsyssi.
**/

int main() 
#endif
{
  //! Test the MINE-correlaiton-emtric for a sytnetic data-set:
  const uint nrows = 5;     const uint ncols = 5; 
  const char *stringOf_sampleData_type = "lines-different-ax";
  const char *stringOf_sampleData_type_realLife = NULL;
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(false) {
    //!
    { //! Load the data-set:
      const bool isTo_transposeMatrix = false;
      const char *stringOf_sampleData_type_realLife = NULL;
      const char *stringOf_sampleData_type = "flat";
      const uint fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const uint fractionOf_toAppendWith_sampleData_typeFor_columns =  0;
      s_kt_matrix_t obj_baseToInclude; setTo_empty__s_kt_matrix_t(&obj_baseToInclude); //! ie, intiate an object 'which we do Not use' (though may be used to merge different dat-asets).
 const bool isTo__copyNamesIfSet = true;
#include "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
    }
  } else { //! then we build using Unix-time, thereby demostnrategin the use of user-explcit valeus (rather than file-loading):
    init__s_kt_matrix(&obj_matrixInput, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
    assert(obj_matrixInput.nrows == nrows);
    assert(obj_matrixInput.ncols == ncols);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	if(true) { 
	  obj_matrixInput.matrix[row_id][col_id] = row_id; //! which for rank-based metics uch as "spearman" will 'produce' a matrix with "-" (ie, as all rows will then 'have the same ransk wrt. their columns').
	} else {
	  obj_matrixInput.matrix[row_id][col_id] = rand();
	}
      }
    }
  }
  //!
  //! Buidl a subset:
  const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&obj_matrixInput, "testData_beforeCorr.tsv", NULL);
  assert(is_ok_ex);

  //!
  //! Correlate:
  s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, NULL); //&fileHandler__localCorrMetrics);
  hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
  hp_config.stringOfResultPrefix__exportCorr__prior = "testData.tsv";
  if(true) { //! then ovverride the default correlation-metric:
    //! Set the corrleation-metric:
    //! Ntoe: for dfiferent corr-emtrics see our "e_kt_correlationFunction.h"
    s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(e_kt_correlationMetric_initCategory_timeComplexitySet__small_O1_minkowski_euclid);
    hp_config.config.corrMetric_prior = corrMetric_prior;
  } //! else we use the 'default metic' specified in our "hpLysis_api.c"
    // hp_config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
  fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  //!
  //! Correlate and export:
  const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
  assert(is_ok);
  //! 
  //! Export results to "STDOUT":
  printf("# Exports the similairty-matrix to standard-out (stdout), at %s:%d\n", __FILE__, __LINE__);
  const bool is_ok_e = export__hpLysis_api(&hp_config, /*stringOf_file=*/NULL, /*file_out=*/stdout, /*exportFormat=*/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, NULL);
  assert(is_ok_e);

  //! 
  //! Export: traverse teh generated result-matrix-object and write out the results:
  //! Note: this object may be sued as input to a new hpLyssi-fucntion, eg, to comapre the simlairty betweend fifferetn simliarity-metrics.
  const s_kt_matrix_t *matrix = &(hp_config.matrixResult__inputToClustering);
  for(uint row_id = 0; row_id < matrix->nrows; row_id++) {
    printf("row[%u] = [", row_id);
    for(uint col_id = 0; col_id < matrix->ncols; col_id++) {
      printf("%f, ", matrix->matrix[row_id][col_id]);
    }
    printf("], at %s:%d\n", __FILE__, __LINE__);
  }

  //! De-allocates the "s_hpLysis_api_t" object.
  free__s_hpLysis_api_t(&hp_config);	    
  free__s_kt_matrix(&obj_matrixInput);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
