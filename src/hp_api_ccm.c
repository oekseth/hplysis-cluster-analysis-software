#include "hp_api_norm.h"
#include "hp_api_fileInputTight.h"
#include "kd_tree.h" //! which is used for the DB-SCAN clustering.

//! Print the comprensive list of enum-options supported by our "hp_api_fileInputTight.h" API (oekseth, 06. mar. 2017)
void printOptions__hp_api_norm() {
  printOptions__simMetric__hp_api_fileInputTight();  
}

//! @returm a default itniated vieorns of the s_hp_api_ccm_config_t cofnig object.
s_hp_api_ccm_config_t initAndReturn__s_hp_api_ccm_config_t() {
  s_hp_api_ccm_config_t self;
  /* self.config_cntMin_afterEach = 1; */
  /* self.config_cntMax_afterEach = 10; */
  /* self.config_ballSize_max = T_FLOAT_MAX; */
  self.isTo_ingore_zeroScores = false;
  return self;
}

//! Comptue sparse simalrity through applciaotn fo sense siamrlity-emtrics.
bool apply__simMetric_dense__extraArgs__hp_api_norm(const e_kt_normType_t obj_metric__, s_kt_matrix_t *mat_1,
						    //s_kt_matrix_t *mat_2,
						    s_kt_matrix_t *mat_result, s_hp_api_ccm_config_t config) {
  
  e_kt_normType_t obj_metric = obj_metric__;
  //if(obj_metric.metric_id == e_kt_correlationFunction_undef) {obj_metric.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;} //! ie, wher elatter is the deuflat pparoach in a alrge nubmer fo reseharc-ojurnals (oekseth, 06. arp. 2017).
  //!
  //! What we expect:
  if(!mat_1 || !mat_1->nrows || !mat_1->ncols || !mat_result) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;    
  } /* else if(mat_2 && (mat_2->ncols != mat_1->ncols) ) { */
  /*   if( (mat_2->ncols == 0) && (mat_2->nrows == 0) ) { */
  /*     mat_2 = mat_1; //! ie, as we then assuemt atht he caller has explctilyset  "mat_2" to empty. */
  /*   } else { */
  /*     allocOnStack__char__sprintf(2000, str_local, "We expected the mat_1.ncols == mat_2.ncols, which is Not the case: ncols(1)=%u while ncols(2)=%u. ", mat_1->ncols, mat_2->ncols); */
  /*     MF__inputError_generic(str_local); */
  /*     assert(false); */
  /*     return false;  */
  /*   }      */
  /* }  */

  //! De-allcoate hte result-matrix if the reuslt-amtrix is already allcoated:
  //free__s_kt_list_2d_kvPair_t(obj_result);
  assert(mat_result); free__s_kt_matrix(mat_result);
  assert(mat_1->matrix != mat_result->matrix);
  //!
  //! Apply logics:
  //s_kt_list_2d_kvPair_t   
  *mat_result = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(mat_1, obj_metric);
  assert(mat_1->matrix != mat_result->matrix);
  //s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(mat_1);
  /* s_kt_matrix_base_t vec_2 = initAndReturn__empty__s_kt_matrix_base_t(); */
  /* if(mat_2 && mat_2->nrows) { */
  /*   vec_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(mat_2); */
  /* } */
  //!


  /* assert(vec_1.nrows > 0); */
  /* assert(vec_1.ncols > 0); */
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return true;
}

/* //! Applies a dense matrix-based simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017). */
/* bool apply__simMetric_dense__hp_api_norm(const e_kt_normType_t enum_id, s_kt_matrix_t *mat_1,  s_kt_list_2d_kvPair_t *mat_result) { */
/*   s_hp_api_ccm_config_t config = initAndReturn__s_hp_api_ccm_config_t(); */
/*   //! Set the metric: */
/*   e_kt_normType_t obj_metric = e_kt_normType_undef;  // obj_metric.metric_id = enum_id; */
/*   //! Apply logics: */
/*   return apply__simMetric_dense__extraArgs__hp_api_norm(obj_metric, mat_1,  mat_result, config); */
/* } */


/* //! Compute clusters based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017). */
/* //bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const e_kt_normType_t obj_metric, const uint nclusters, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep) { */
/* bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t enum_id, const e_kt_normType_t obj_metric_pre, const e_kt_normType_t obj_metric_inside, const uint cnt_clusters_x, const uint cnt_clusters_y, t_float iterative_SOM_tauInit, uint iterative_maxIterCount, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, s_kt_clusterResult_condensed_t *result_clusterMemberships, const bool isTo_inMatrixResult_storeSimMetricResult, const bool isTo_useSimMetricInPreStep) { */
/*   if(!mat_1 || !mat_1->nrows || !mat_1->ncols || !mat_result) { */
/*     MF__inputError_generic("The input-parameters were Not set"); */
/*     assert(false); */
/*     return false; */
/*   }      */

/* } */


//! Read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
bool readFromFile__stroreInStruct__exportResult__hp_api_norm(const char *stringOf_enum, s_kt_matrix_t *mat_1, s_kt_matrix_t *mat_result, 
							     //s_kt_list_1d_uint_t *map_clusters, 
							     const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
							     //const char *stringOf_exportFile_clusters,  const char *stringOf_exportFile__format_clusters, 
							     const bool isTo_transpose,
							     s_hp_api_ccm_config_t config) {  
  assert(mat_1->matrix != mat_result->matrix);
  //! ----------------------------------
  //!  
  //! What we expect:
  if(!mat_1 || !mat_1->nrows || !mat_1->ncols 
     //|| !stringOf_enum || !strlen(stringOf_enum) 
     ) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     



  //  if(config.config_cntMax_afterEach > 1) {config.config_cntMax_afterEach--;} //! which is used to reflec tthe attributes of teh inptud-ata

  //! --------------------------------------------------------------
  //! 
  //! 
  e_kt_normType_t enum_id = e_kt_normType_avgRelative; //e_kt_correlationFunction_groupOf_minkowski_euclid;
  if(stringOf_enum && strlen(stringOf_enum)) {
    enum_id = get_enum_e_kt_normType_t(stringOf_enum);
    // get_enumBasedOn_configuration__kt_metric_aux(stringOf_enum, /*printAn_errorIf_notFound=*/false); //! deifned in our "kt_metric_aux.h".
  }
  /* if(isTo_use_MINE__e_kt_correlationFunction(enum_id)) { */
  /*   fprintf(stderr, "!!(requested-future-option)\t Your requredted the comptuation of the MINE metric, which is currently not supported, ie, pealse requrest support for this feature, cotnasting [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
  /* } */
  e_kt_normType_t obj_metric = e_kt_normType_undef;  // obj_metric.metric_id = enum_id;
  {
    const bool is_ok = apply__simMetric_dense__extraArgs__hp_api_norm(obj_metric, mat_1, mat_result, config);
    //    const bool is_ok = apply__simMetric_dense__hp_api_norm(enum_id, mat_1, NULL, obj_result);
    assert(is_ok);
    assert(mat_1->matrix != mat_result->matrix);
  }
  //! --------------------------------------------------------------
  //! 
  //! 
  //! 

  //! --------------------------------------------------------------
  //! 
  //! 
  if(
     (mat_result->nrows > 0) &&
     (
      (stringOf_exportFile && strlen(stringOf_exportFile)) ||
      (stringOf_exportFile__format && strlen(stringOf_exportFile__format))
      )
     ) {
    //!
      //! Provide support both for different 'export' formats, and for the 'use' of "stdout" file-descriptor:
      bool is_epxorted = false;
      bool isTo_useMatrixFormat = true;
      bool isTo_inResult_isTo_useJavaScript_syntax = false;
      bool isTo_inResult_isTo_useJSON_syntax = false;
      const char *suffix = "tsv";
      if(stringOf_exportFile__format && strlen(stringOf_exportFile__format)) {
#define __MiF__cmp(string) ({bool is_equal = false; if(strlen(string) == strlen(stringOf_exportFile__format)) {if(0 == strncasecmp(string, stringOf_exportFile__format, strlen(string))) {is_equal = true;}} is_equal;}) //! ie, a wrapper to return true if the format equals:
	//!
	//! Investigate different format-cases:
	if(__MiF__cmp("json")) {
	  is_epxorted = true;
	  isTo_inResult_isTo_useJavaScript_syntax = true;
	  isTo_inResult_isTo_useJSON_syntax = true;
	  suffix = "json";
	} else if(__MiF__cmp("js")) {
	  isTo_inResult_isTo_useJavaScript_syntax = true;
	  is_epxorted = true;
	  suffix = "js";
	}      
#undef __MiF__cmp
      }

      s_kt_matrix_t mat_export = *mat_result;
      if(isTo_transpose) { //! then we need to 'back-transpose':	
	printf("transpose, at %s:%d\n", __FILE__, __LINE__);
	mat_export = setToEmptyAndReturn__s_kt_matrix_t();
	const bool is_ok = init__copy_transposed__s_kt_matrix(&mat_export, mat_result, true);
	assert(is_ok);
      }
      
      
      //!
      //! Then we export the result: 
      FILE *file_out = NULL;
      if(stringOf_exportFile && strlen(stringOf_exportFile)) {
	export_config__s_kt_matrix_t(&mat_export, stringOf_exportFile);
	assert(mat_export.stream_out != NULL); //! ie, as we expect [ªbov€] to have been impluct set.
      } else { //! then we use the 'stadnard-out' file-dsescriptor:
	file_out = stdout;
	mat_export.stream_out = file_out;
      }
      //!
      //! Compelte configuraiton, and then exprot result:
      assert(mat_export.stream_out != NULL); //! ie, as we expect [ªbov€] to have been impluct set.
      export_config_extensive_s_kt_matrix_t(&mat_export, NULL, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, 
					    /*matrix-variable-suffix=*/suffix, 
					    false, //(bool)(stringOf_id != NULL), 
					    /*cnt_dataObjects_toGenerate=*/1); //cnt_objs_toExport); 
      export__s_kt_matrix_t(&mat_export);
      //! --------------------
      //!
      //! Close:
      if(!file_out) {
	closeFileHandler__s_kt_matrix_t(&mat_export);
	assert(mat_result->matrix != NULL);
      }
      if(isTo_transpose) { //! then we need to 'back-transpose':
	assert(mat_export.matrix != mat_result->matrix); //! ie, as we expect the 'mat_export' to be a different object.
	free__s_kt_matrix(&mat_export);	
      }
      assert(mat_result->matrix != NULL);
  } else {
    if(mat_result->nrows > 0) {
      fprintf(stderr, "!!\t Unable to write the result to file=\"%s\": please investigate both the file-path, access-permissions on the result-directory, and available disk-space. If latter does not help, then please contact the senior developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_exportFile, __FUNCTION__, __FILE__, __LINE__);
    }
  }
  assert(mat_1->matrix != mat_result->matrix);

  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return true;
}



bool readFromFile__exportResult__hp_api_norm(const char *stringOf_enum, const char *stringOf_inputFile_1,   const bool isTo_transpose,
					     const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
					     //const char *stringOf_exportFile_clusters,  const char *stringOf_exportFile__format_clusters, 
						  // const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool inputFile_isSparse
						  s_hp_api_ccm_config_t config) {
  //! ----------------------------------
  //!  
  //! What we expect:
  if(!stringOf_enum || !strlen(stringOf_enum) || !stringOf_inputFile_1 || !strlen(stringOf_inputFile_1)) {
    MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  //! ----------------------------------
  s_kt_matrix_fileReadTuning_t config_read = initAndReturn__s_kt_matrix_fileReadTuning_t();
  config_read.isTo_transposeMatrix = isTo_transpose;
  //!
  //! Read the input-file(s) and validate results: 
  s_kt_matrix_t mat_1 = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_inputFile_1, config_read);
  if(!mat_1.nrows || !mat_1.ncols) {
    allocOnStack__char__sprintf(2000, str_local, "Unable to read input-file=\"%s\". ", stringOf_inputFile_1);
    MF__inputError_generic(str_local);
    //MF__inputError_generic("The input-parameters were Not set");
    assert(false);
    return false;
  }     
  // printf("\n\n file_1=[%u, %u], at %s:%d\n", mat_1.nrows, mat_1.ncols, __FILE__, __LINE__);
  //! Read the second input-file
  /* s_kt_matrix_t mat_2 = setToEmptyAndReturn__s_kt_matrix_t(); */
  /* if(stringOf_inputFile_2 && strlen(stringOf_inputFile_2)) { */
  /*   mat_2 = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_inputFile_2, config_read); //initAndReturn__s_kt_matrix_fileReadTuning_t()); */
  /*   if(!mat_2.nrows || !mat_2.ncols) { */
  /*     allocOnStack__char__sprintf(2000, str_local, "Unable to read input-file_2=\"%s\". ", stringOf_inputFile_2); //! ie, intate a new variable "str_local".  */
  /*     MF__inputError_generic(str_local); */
  /*     //MF__inputError_generic("The input-parameters were Not set"); */
  /*     assert(false); */
  /*     return false; */
  /*   }      */
  /* } */
  //! Intiate result-matrix:
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  //s_kt_list_2d_kvPair_t obj_result = setToEmpty__s_kt_list_2d_kvPair_t();

  /* s_kt_list_1d_uint_t map_clusters = setToEmpty__s_kt_list_1d_uint_t(); bool isTo_apply_DBSCAN = false; */
  /* if(stringOf_exportFile_clusters && strlen(stringOf_exportFile_clusters)) { isTo_apply_DBSCAN = true;} */
  //! ----------------------------------
  //!
  if(!stringOf_exportFile__format || !strlen(stringOf_exportFile__format)) {stringOf_exportFile__format = "sparse_rows";}
  //! ----------------------------------
  //!
  //! Apply Logics:
  const bool is_ok = readFromFile__stroreInStruct__exportResult__hp_api_norm
    (stringOf_enum, &mat_1,  &mat_result, 
     //(isTo_apply_DBSCAN) ? &map_clusters : NULL,      
     stringOf_exportFile, stringOf_exportFile__format,
     isTo_transpose,
     //stringOf_exportFile_clusters,  stringOf_exportFile__format_clusters,
     config);
  assert(is_ok);
  //!
  //! De-allcoate:
  assert(mat_1.matrix != mat_result.matrix);
  free__s_kt_matrix(&mat_1);
  free__s_kt_matrix(&mat_result);
  //free__s_kt_matrix(&mat_2);
  //free__s_kt_list_2d_kvPair_t(&obj_result);
  //free__s_kt_list_1d_uint_t(&map_clusters);
  //! @return the status-code.
  return is_ok;
}


static const char *stringOf_enum__par = "-ops=";
static const char *stringOf_inputFile_1__par = "-file1=";
//static const char *stringOf_inputFile_2__par = "-file2=";
static const char *stringOf_inputFile__transpose = "-transpose=";
static const char *stringOf_exportFile__par = "-out=";
static const char *stringOf_exportFile__format__par = "-outFormat=";
/* static const char *stringOf_exportFileClust__par = "-outCluster="; */
/* static const char *stringOf_exportFileClust__format__par = "-outClusterFormat="; */
// ----------------------------
/* static const char *stringOf_filter_rank_min        = "-filterRankMin="; */
/* static const char *stringOf_filter_rank_max        = "-filterRankMax="; */
//static const char *stringOf_filter_score_max       = "-filterScoreMax=";
static const char *stringOf_filter_isTo_ignoreZero = "-filterIgnoreZero=";
//static const char *nclusters__par = "-nclusters=";
//static const char *inputFile_isSparse__par = "-inputIsSparse=";


//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_norm(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_1, //const char *stringOf_inputFile_2, 
								const bool isTo_transpose,
								const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
							   //const char *stringOf_exportFileClust,  const char *stringOf_exportFileClust__format, 
								s_hp_api_ccm_config_t config,
								//const uint nclusters, const bool inputFile_isSparse, 
								uint *scalar_cntArgs, char **listOf_args__2d) {  
  assert(scalar_cntArgs);
  assert(listOf_args__2d);
  uint cnt_chars = 10000;
  const char def_0 = 0;
  //! Allocate:
  char *str_cmd = allocate_1d_list_char(cnt_chars, def_0);
  //! ----------------------------
  //!
  //! Define function
  char *curr = str_cmd;
  uint cnt = 0;
#define __MiF__insertIfValue(val, param) ({if(val && strlen(val)) {listOf_args__2d[cnt] = curr; cnt++; sprintf(curr, "%s%s", param, val); curr += strlen(curr); *curr = '\0'; curr++;}}) //! ie, 'insert value' if the inptu-value is set.
  //!
  //! Add values:
  __MiF__insertIfValue("x_hp_norm ", ""); //! ie, as the first arguemtn is always the name of the progrma.
  __MiF__insertIfValue(stringOf_enum , stringOf_enum__par);
  __MiF__insertIfValue(stringOf_inputFile_1 , stringOf_inputFile_1__par);
  //__MiF__insertIfValue(stringOf_inputFile_2 , stringOf_inputFile_2__par);
  __MiF__insertIfValue(stringOf_exportFile , stringOf_exportFile__par);
  /* __MiF__insertIfValue(stringOf_exportFileClust__format , stringOf_exportFileClust__format__par); */
  /* __MiF__insertIfValue(stringOf_exportFileClust , stringOf_exportFileClust__par); */
  /* __MiF__insertIfValue(stringOf_exportFileClust__format , stringOf_exportFileClust__format__par); */
  if( isTo_transpose == true ) {
    __MiF__insertIfValue("1", stringOf_inputFile__transpose);
  }
  /* if( config.config_cntMin_afterEach != UINT_MAX )    { */
  /*   allocOnStack__char__sprintf(2000, str_local, "%u", config.config_cntMin_afterEach); //! ie, intate a new variable "str_local". */
  /*   __MiF__insertIfValue(str_local , stringOf_filter_rank_min); */
  /* } */
  /* if( config.config_cntMax_afterEach != UINT_MAX ) { */
  /*   allocOnStack__char__sprintf(2000, str_local, "%u", config.config_cntMax_afterEach); //! ie, intate a new variable "str_local". */
  /*   __MiF__insertIfValue(str_local , stringOf_filter_rank_max); */
  /* } */
  /* if( config.config_ballSize_max != T_FLOAT_MAX ) { */
  /*   allocOnStack__char__sprintf(2000, str_local, "%f", config.config_ballSize_max); //! ie, intate a new variable "str_local". */
  /*   __MiF__insertIfValue(str_local , stringOf_filter_score_max); */
  /* }   */
  if(config.isTo_ingore_zeroScores) {
    __MiF__insertIfValue("1", stringOf_filter_isTo_ignoreZero);
  }
#undef __MiF__insertIfValue
  *scalar_cntArgs = cnt;
  //! ----------------------------
  //!
  if(debug_config__isToCall) { //! then apply logic:
    const bool is_ok = readFromFile__exportResult__hp_api_norm(stringOf_enum, stringOf_inputFile_1,  isTo_transpose,
							       stringOf_exportFile, stringOf_exportFile__format, 
							       //stringOf_exportFileClust, 	    stringOf_exportFileClust__format,
								    config);
    assert(is_ok);
  }
  //! ----------------------------
  //!
  //! @return 
  return str_cmd;
}

/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_norm(char **arg, const uint arg_size) {
  assert(arg);
  const char *stringOf_enum = NULL;
  //const char *stringOf_enum__nclusters = NULL;
  const char *stringOf_inputFile_1 = NULL;
  //const char *stringOf_inputFile_2 = NULL;
  bool isTo_transpose_input = false;
  const char *stringOf_exportFile = NULL;
  const char *stringOf_exportFile__format = NULL;
  /* const char *stringOf_exportFileClust = NULL; */
  /* const char *stringOf_exportFileClust__format = NULL;  */
  s_hp_api_ccm_config_t config = initAndReturn__s_hp_api_ccm_config_t();
  //! ----------------------------------
  uint __cntParams_set = 0;

  if(arg_size == 2) {
#define __MiF__cmp(string) ({bool is_equal = false; if(strlen(string) == strlen(arg[1])) {if(0 == strncasecmp(string, arg[1], strlen(string))) {is_equal = true;}} is_equal;}) //! ie, a wrapper to return true if the format equals:
    //    printf("arg[1]=\"%s\", at %s:%d\n", arg[1], __FILE__, __LINE__);
    /* if(__MiF__cmp("-ops=ccm_matrix")) { */
    /*   printOptions__ccm_matrixBased__hp_api_fileInputTight(); */
    /* } */
    /* if(__MiF__cmp("-ops=ccm_goldXgold")) { */
    /*   printOptions__ccm_twoClusterResults__hp_api_fileInputTight();  */
    /* } */
    if(__MiF__cmp("-ops")) {
      FILE *fileP = stdout;
      fprintf(fileP, "#\tOptions in hpLysis: [");
      for(uint enum_id = 0; enum_id < e_kt_normType_undef; enum_id++) {
	const e_kt_normType_t enum_type = (e_kt_normType_t)enum_id;
	const char *str = get_str__humanReadable__e_kt_normType_t(enum_type);
	if(str && strlen(str)) {
	  fprintf(fileP, "\"%s\"", str); 
	  if((enum_id +1) != e_kt_normType_undef) {
	    fprintf(fileP, ", ");
	  }
	}
      }
      fprintf(fileP, "], at %s:%d\n", __FILE__, __LINE__);
    }
    /* if(__MiF__cmp("-ops=clustAlg")) { */
    /*   printOptions__clustAlg__hp_api_fileInputTight(); */
    /* } */
    if(__MiF__cmp("-help")) {
      for(uint enum_id = 0; enum_id < e_kt_normType_undef; enum_id++) {
	const e_kt_normType_t enum_type = (e_kt_normType_t)enum_id;
	const char *str = get_description__humanReadable__e_kt_normType_t(enum_type);
	printf("\t%s\n", str);
      }
    }
    if(__MiF__cmp("-ex")) {
      FILE *fileP = stdout;
      fprintf(fileP, "#! Exemple calls are:\n");
      // -----------------------------------------------------------------------
      // -----------------------------------------------------------------------
#define __MiF__constructCMD() ({ \
	uint arg_size = 0; char *listOf_args__2d[10]; char *listOf_args = construct__terminalArgString__fromInput__hp_api_norm(/*debug_config__isToCall=*/false,  stringOf_enum, stringOf_inputFile_1,  isTo_transpose_input, stringOf_exportFile, stringOf_exportFile__format, config, &arg_size, listOf_args__2d);						  \
	assert(listOf_args); assert(strlen(listOf_args));		\
	MF__expand__bashInputArg__hp_api_fileInputTight("", string_desc, listOf_args__2d, arg_size); \
	/*! De-allocate:*/ \
	if(listOf_args) {free_1d_list_char(&listOf_args); listOf_args = NULL;} })
      // -----------------------------------------------------------------------
      // -----------------------------------------------------------------------
      //! -------------------------------------------------------------------------------------------
      //! 
      //! 
      {
	//! Note: [below] example-chunk is manually validted in our "tut_hp_api_fileInputTight_4_dist_1_Euclid.c":
	const char *file_name = "data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv";
	// ------------------
	const char *stringOf_inputFile_1 = file_name;
	//const char *stringOf_inputFile_2 = NULL; // random_file;
	const char *stringOf_exportFile = NULL; //! ie, to "stdout".
	const char *stringOf_exportFile__format = NULL;
	/* const char *stringOf_exportFileClust = NULL; */
	/* const char *stringOf_exportFileClust__format = NULL; */
	bool isTo_transpose_input = false;
	s_hp_api_ccm_config_t config = initAndReturn__s_hp_api_ccm_config_t();
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_normType_t enum_id = e_kt_normType_avgRelative;
	  //const e_kt_normType_t enum_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	  const char *string_desc = "#! Use-case: similarity of features for metric='row-RelativeAvg'";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_str__humanReadable__e_kt_normType_t(enum_id);
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_normType_t enum_id = e_kt_normType_avgRelative;
	  //const e_kt_normType_t enum_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	  isTo_transpose_input = true;
	  const char *string_desc = "#! Use-case: write out results into 'triplets'.";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_str__humanReadable__e_kt_normType_t(enum_id);
	  //const char *stringOf_enum = get_str__humanReadable__e_kt_normType_t(enum_id);
	  //const char *stringOf_enum = get_stringOf_enum__e_kt_normType_t(enum_id);
	  const char *stringOf_exportFile__format = "sparse_rows";
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_normType_t enum_id = e_kt_normType_STD_row;
	  //const e_kt_normType_t enum_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	  isTo_transpose_input = true;
	  const char *string_desc = "#! Use-case: transpose the data, and write out results to a seperate file";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_str__humanReadable__e_kt_normType_t(enum_id);
	  //const char *stringOf_enum = get_stringOf_enum__e_kt_normType_t(enum_id);
	  const char *stringOf_exportFile = "result.tsv";
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	{
	  // const uint ncluster = UINT_MAX; 
	  const e_kt_normType_t enum_id = e_kt_normType_rankRelative;
	  //const e_kt_normType_t enum_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
	  const char *string_desc = "#! Use-case: similarity of features for metric=rank-relative";
	  //!
	  //! Define an input-file to use in analysis:
	  // ------------------
	  const char *stringOf_enum = get_str__humanReadable__e_kt_normType_t(enum_id);
	  //const char *stringOf_enum = get_stringOf_enum__e_kt_normType_t(enum_id);
	  //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id);
	  //! 
	  //! Apply logics:
	  __MiF__constructCMD();
	}
	/* { */
	/*   // const uint ncluster = UINT_MAX;  */
	/*   const e_kt_normType_t enum_id = e_kt_correlationFunction_groupOf_MINE_mic; */
	/*   const char *string_desc = "#! Use-case: similarity of features for metric=MINE-mic"; */
	/*   //! */
	/*   //! Define an input-file to use in analysis: */
	/*   // ------------------ */
	/*   const char *stringOf_enum = get_stringOf_enum__e_kt_normType_t(enum_id); */
	/*   //const char *stringOf_enum = getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id); */
	/*   //!  */
	/*   //! Apply logics: */
	/*   __MiF__constructCMD(); */
	/* } */
      }
#undef __MiF__constructCMD
    }
#undef __MiF__cmp
    //!
    //! At this exueicotn-point we asusme the oerpation was a success:
    return true;    
  } else if(arg_size > 2) {
    //! ----------------------------------
    //!
    //! Then identify and evluate the input-parameters: 
#define __MiF__setIfFound(result, para) ({const char *val = strstr(arg[i], para); if(val) {val += strlen(para); result = val;}})
    for(uint i = 1; i < arg_size; i++) {
      // printf("eval: \"%s\", at %s:%d\n", arg[i], __FILE__, __LINE__);
      __MiF__setIfFound(stringOf_enum, stringOf_enum__par);
      //! -------------- 
      __MiF__setIfFound(stringOf_inputFile_1, stringOf_inputFile_1__par);
      //__MiF__setIfFound(stringOf_inputFile_2, stringOf_inputFile_2__par);
      //! -------------- 
      __MiF__setIfFound(stringOf_exportFile, stringOf_exportFile__par);
      __MiF__setIfFound(stringOf_exportFile__format, stringOf_exportFile__format__par);
      // ----- 
      /* __MiF__setIfFound(stringOf_exportFileClust, stringOf_exportFileClust__par); */
      /* __MiF__setIfFound(stringOf_exportFileClust__format, stringOf_exportFileClust__format__par); */
      //! -------------- 
      //! -------------- 
      //!
      //! Add for 'numeric' input-data-cases:
      //! -------------- 
      const char *result = NULL;
#if(false)
      __MiF__setIfFound(result, stringOf_filter_rank_min);
      if(result) {config.config_cntMin_afterEach = (uint)atoi(result);} //! ie, icnrement 'past' the operation.
      // ----------------------- 
      result = NULL;
      __MiF__setIfFound(result, stringOf_filter_rank_max);
      if(result) {config.config_cntMax_afterEach = (uint)atoi(result);} //! ie, icnrement 'past' the operation.
      // ----------------------- 
      result = NULL;
      __MiF__setIfFound(result, stringOf_filter_score_max);
      if(result) {config.config_ballSize_max = (t_float)atof(result);} //! ie, icnrement 'past' the operation.
#endif
      // ----------------------- 
      result = NULL;
      __MiF__setIfFound(result, stringOf_filter_isTo_ignoreZero);
      if(result) {config.isTo_ingore_zeroScores = (bool)atoi(result);} //! ie, icnrement 'past' the operation.
      // ----------------------- 
      result = NULL;
      __MiF__setIfFound(result, stringOf_inputFile__transpose);
      if(result) {isTo_transpose_input = (bool)atoi(result);} //! ie, icnrement 'past' the operation.
      /* //! --------------  */
      /* result = NULL; __MiF__setIfFound(result, inputFile_isSparse__par);        */
      /* if(result) {inputFile_isSparse = (bool)atoi(result);} //! ie, icnrement 'past' the operation.       */
    }
#undef __MiF__setIfFound
  }
  if( //(__cntParams_set == 0) || 
      (stringOf_inputFile_1 == NULL) || (stringOf_enum == NULL) ) {
    //!
    //! Then we write out the different optiosn described in [ªbove].
    printf("#! The simplified hpLysis interface provides support for:\n");
#define __MiF__printAg(opt, desc) ({if( (opt != NULL) && strlen(opt))  {printf("\t \"%s\": %s\n", opt, desc);} else {printf("%s\n", desc);}})
    //! Write out arguments:
    __MiF__printAg(stringOf_enum__par, "The type of algorithm of metric to apply: write [program-name] and either \"ccm_matrix\", \"ccm_goldXgold\", \"distance\", or \"clustAlg\" to get a list of the correct enum-alternatives");
    __MiF__printAg(stringOf_inputFile_1__par, "The first input-file: the dataset (eg, feature matris) to normalize");
    // __MiF__printAg(stringOf_inputFile_2__par, "The second input-matrix: (a) matrix-based CCMs: the hypothesized data-set; (b) goldXgold-CCM: an alternative set of hypothesis to compare the first input-data-set with; (c) distance: an alternative distance-matrix to be used as input: (d) clustAlg: ignored");
    __MiF__printAg(stringOf_inputFile__transpose, "Transpose: If set, then the rows are treated as features, ie, for which the matrix(es) are tilted before the analysis");
    //__MiF__printAg(stringOf_exportFile__par, "The format-type. We expect values to be separated by 'tabs'. Default format a 'dense matrix' (where each index describes the position in the matrix). If 'sparse' is used, we assume a row consists of 'three' (3) columns, where column[0]='row_id', column[1]='col_id', column[2]='distance'");
    __MiF__printAg(stringOf_exportFile__par, "If set, denotes the file to used to export the result. Otherwise the \"stdout\" is used for file-export");
    __MiF__printAg(stringOf_exportFile__format__par, "the format of the exported data-set: either \"tsv\", \"js\" or \"json\"");
    /* __MiF__printAg(stringOf_exportFileClust__par, "Cluster-result: If set denotes the file to used to export the result. Otherwise the \"stdout\" is used for file-export"); */
    /* __MiF__printAg(stringOf_exportFileClust__format__par, "Cluster-result: the format of the exported data-set: either \"triplets\" or \"sparse_rows\""); //, \"js\" or \"json\""); */
    // -----------------
    //    __MiF__printAg(stringOf_filter_score_max, "Value-threshold-max: ignore the cells with a score above the given similarity score threshold");
    __MiF__printAg(stringOf_filter_isTo_ignoreZero, "Value-filter-zero: If set, then all scores inferred to be '0' are discarded");
    /* __MiF__printAg(stringOf_filter_rank_min, "Rank-filter-min: minimum number of cells below the 'value-threshold' for the relationship to be of interest"); */
    /* __MiF__printAg(stringOf_filter_rank_max, "Rank-filter-max: maximum number of cells below the 'value-threshold' for the relationship to be of interest"); */
      //__MiF__printAg(, "")
    /* __MiF__printAg(nclusters__par, "if set is used to infer the number of clusters. Alternatively the hpLysis 'dynamic k-means' is used to infer clusters (where a CCM is used to identify the optimal nclusters count of clusters)"); */
    /* __MiF__printAg(inputFile_isSparse__par, "The format-type. We expect values to be separated by 'tabs'. Default format a 'dense matrix' (where each index describes the position in the matrix). If 'sparse' is used, we assume a row consists of 'three' (3) columns, where column[0]='row_id', column[1]='col_id', column[2]='distance'"); */
    /* __MiF__printAg("", "--------------------------------------------"); */
    __MiF__printAg("-ex", "print example calls to demonstrate use.");
    __MiF__printAg("-help", "explain the parameters.");
#undef __MiF__printAg
    printf("\n For questions wrt. the hpLysis interface, please contact the senior developer [oekseth@gmail.com].\n");
    //!
    //! At this exueicotn-point we asusme the oerpation was a success:
    return true;
  }
  //! ----------------------------------
  //!
  //! Apply the speicifed logics:
  //  printf("stringOf_inputFile_1=\"%s\", stringOf_inputFile_2=\"%s\", inputFile_isSparse=%d, at %s:%d\n", stringOf_inputFile_1, stringOf_inputFile_2, inputFile_isSparse, __FILE__, __LINE__);
  const bool is_ok = readFromFile__exportResult__hp_api_norm(stringOf_enum, stringOf_inputFile_1,  isTo_transpose_input,
								  stringOf_exportFile, stringOf_exportFile__format, 
							     //stringOf_exportFileClust, 	    stringOf_exportFileClust__format,
								  config);
  //const bool is_ok = readFromFile__exportResult__hp_api_norm(stringOf_enum, stringOf_inputFile_1, stringOf_inputFile_2, stringOf_exportFile, stringOf_exportFile__format, nclusters, inputFile_isSparse);
  assert(is_ok);
  //! ----------------------------------
  //!
  //! At this exueicotn-point we asusme the oerpation was a success:
  return is_ok;
}


