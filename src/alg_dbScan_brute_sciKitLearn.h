#ifndef alg_dbScan_brute_sciKitLearn_h
#define alg_dbScan_brute_sciKitLearn_h
/**
   @file 
   @brief an implemtnation of DBSCAN using the sci-kit-learn-approach.
   @remarks 
   -- generic: to ensure that the permtuation reflects the best-performing strategy, the impmetantion reflects teh implemtantioin found in the "sci-kit learn" machine learning library.
   -- permtuation: assumes a correlation matrix is provided, hence avoids the needs for pre-comptuing pariwise simlairties.
 **/
#include "kt_matrix_base.h"
#include "kt_list_2d.h"

/**
   @brief comptues DBSCAN.
   @param <obj_1> is the data of relationships
   @return the result of the clustering
 **/
s_kt_list_1d_uint_t computeFor_filteredMatrix__alg_dbScan_brute_sciKitLearn(const s_kt_matrix_base_t *obj_1);
/**
   @brief comptues DBSCAN for a pre-aprttioing set of relationships.
   @param <obj_1> is the data of relationships
   @return the result of the clustering
 **/
s_kt_list_1d_uint_t  computeFor_relations__alg_dbScan_brute_sciKitLearn(const s_kt_list_2d_uint_t *obj_1);

#endif //! EOF: #define alg_dbScan_brute_sciKitLearn_h
