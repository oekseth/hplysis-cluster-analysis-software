#include "correlation_rank.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


//! @return the rank of the assicated data data-poiner.
void getrank_memoryRe_use(const uint n, const t_float *arr_input, t_float *arr_result_columnRanks, uint *tmp_index, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, t_float *optional_arrOf_sorted)  {
  // FIXME: remember to benchmark the 'effect' of [”elow] when comapred to "graphAlgorithms_distance::getrank(..)"
  // FIXME: update wrt. typeOf_performance_sortAppraoch_toUse
  // FIXME: use a different call tahn [”elow]  

  /* Call sort to get an index table */
  // sort_merge_sort<type_2d_float_nonCmp_uint>::sort_array(arrOf_new.get_unsafe_list(), arrOf_new.get_current_pos());

  if(n <= 1) {return;} //! ie, as the 'ranks' tehn does Not change  (oekseth, 06. jan. 2017).

  { //! Firsta a simple test to investigate if there 'exists' values which has an empty value-score (or any other cases hwer ehte same valeu si consitently used)  (oekseth, 06. jan. 2017):
    uint cnt_startValue = 0;
    const t_float start_value = arr_input[0];
    for(uint i = 1; i < n; i++) {
      cnt_startValue += (arr_input[i] == start_value);
    }    
    if(cnt_startValue == n) { //! then all values are 'sggined' to the same score.
      const t_float default_rank = (start_value == T_FLOAT_MAX) ? T_FLOAT_MAX : 1; //! ie, where we assume that "T_FLOAT_MAX" indicates 'an empty value'
      for(uint i = 0; i < n; i++) {
	arr_result_columnRanks[i] = default_rank;
      }
      return; //! ie, as all entites has the same value.
    }    
    /*  else { //! then 'test' if the values are 'contulys icnreasing': */
    /*   uint cnt_prop_increase = 0; */
    /*   for(uint i = 1; i < n; i++) { */
    /* 	cnt_prop = (arr_input[i] < arr_input[i-1]); */
    /*   } */
    /*   if(cnt_prop_increase == n) {  //! then all values are sorted: */
    /* 	for(uint i = 0; i < n; i++) { */
    /* 	  arr_result_columnRanks[i] = i; //! ie, the 'rank'  */
    /* 	} */
    /*   return; //! ie, as all entites has the same value. */
    /* } */
  }

  if( (typeOf_performance_sortAppraoch_toUse == e_distance_rank_typeOf_computation_ideal) || (typeOf_performance_sortAppraoch_toUse == e_distance_rank_typeOf_computation_undef) ) {
    // FIXME: update [below] caluse after our time-benchmark fo teh sroting-prcoeudres.
    quicksort(n, arr_input, tmp_index, arr_result_columnRanks);
  } else if(typeOf_performance_sortAppraoch_toUse == e_distance_rank_typeOf_computation_quickSort) {
    quicksort(n, arr_input, /*arr_index=*/tmp_index, /*arr_result=*/arr_result_columnRanks);
  } else if(typeOf_performance_sortAppraoch_toUse == e_distance_rank_typeOf_computation_mergeSort) {
    // FIXME: validate correctness of using arr_result_columnRanks also as the arr_tmp parameter.
    assert(false); // FIXME: update the [”elow] routien wrt. update of the tmp_index <-- until now omitted, ie, for simplicaiton of cod
    // memcpy(arr_result_columnRanks, arr_input, sizeof(t_float)*n);
    // if(tmp_index != NULL) {
    //   //! Set default values:
    //   for(uint i=0; i<n; i++) {tmp_index[i] = i;}
    // }

    // mergeSort(arr_result_columnRanks, arr_result_columnRanks, /*size=*/n, tmp_index);
  } else {
    assert(false); //! ie, as we then need to add support for this option.
  }
 
  if(optional_arrOf_sorted != NULL) {
    memcpy(optional_arrOf_sorted, arr_result_columnRanks, sizeof(t_float)*/*size=*/n); //! then copy the result-set into the pointer.
  }

  /* Build a rank table */
  for (uint i = 0; i < n; i++) {
    assert(tmp_index[i] != T_FLOAT_MAX);
    assert(tmp_index[i] != T_FLOAT_MIN);
    const uint col_id = tmp_index[i]; //! ie, the column-index of the sorted entry.
    arr_result_columnRanks[col_id] = i; //! ie, the 'sorted rank' for a given vertex.
  }

  //! Handle the case where the ranks are equal:  
  uint i = 0;
  // FIXME: write a 'syntetic' time-benchmark where we comapre teh tiem-cost of [below] VS 'a for-loop' with eqvivalent 'base' lgoics ... as 'seen' in our "graphAlgorithms_distance::get_rank_wCppOverhead(..)"

  // FIXME: validate taht we in [”elow] manage to correctly hadnle the '0' values <-- are we always 'to ingore these'? <-- expect s the latter to be 'proeprtly' handlded in a pre-fitlering rotuine.

  //printf("list_size=%u, at [%s]:%s:%d\n", n, __FUNCTION__, __FILE__, __LINE__);

  while (i < n) {
    double value = arr_input[tmp_index[i]]; 
    uint j = i + 1;
    if(value != T_FLOAT_MAX) {
      while (j < n && arr_input[tmp_index[j]] == value) j++;
      const uint m = j - i; /* number of equal ranks found */
      const uint col_id = tmp_index[i];
      value = arr_result_columnRanks[col_id] + (m-1)*.5;
      for (uint j = i; j < i + m; j++) arr_result_columnRanks[tmp_index[j]] = value;
      i += m;
    } else {
      // TODO[jc]: may you vlaidate correcness 'of thios'? (oekseth, 06. des. 2016)
      arr_result_columnRanks[tmp_index[i]] = T_FLOAT_MAX;
      i++;
    }
  }
}

//! @return the rank of the assicated data data-poiner.
t_float* getrank(const uint n, const t_float *data, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse)  {
  t_float* rank = allocate_1d_list_float(n, /*empty-value=*/0);
  if (!rank) return NULL;
  uint *index = allocate_1d_list_uint(n, /*empty-value=*/0);
  if (!index) {
    free_1d_list_float(&rank);
    return NULL;
  }
  getrank_memoryRe_use(n, data, rank, index, typeOf_performance_sortAppraoch_toUse, NULL);
  free (index);
  return rank;
}

//! Identify the rank for a given vector, using a matrix as 'input'.
void ktCorrelation_compute_rank_forVector(const uint index1, const uint size, t_float **data, char **mask, t_float *tmp_array, uint *tmp_array_index,  t_float *result_array, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation, t_float *optional_arrOf_sorted) {
  assert(size); assert(result_array); assert(tmp_array); assert(data);
  assert(data[index1]);
  //! Idnetify the interesting vertices:  
  if(needTo_useMask_evaluation == false) {
    // FIXME: update our time-benchmarks to evlauate the time-signifance of this 'optionn'.
    memcpy(tmp_array, data[index1], sizeof(t_float)*size);
  } else {
    if(mask) {
      for(uint i = 0; i < size; i++) {
	if(mask[index1][i]) {
	  tmp_array[i] = data[index1][i];
	} else {tmp_array[i] = T_FLOAT_MAX;}
      }
    } else {
      for(uint i = 0; i < size; i++) {
	if(isOf_interest(data[index1][i])) {
	  tmp_array[i] = data[index1][i];
	} else {tmp_array[i] = T_FLOAT_MAX;}
      }
    }
  }  
  //! Compute the rank:
  getrank_memoryRe_use(size, tmp_array, result_array,  tmp_array_index, typeOf_performance_sortAppraoch_toUse, optional_arrOf_sorted);
}


//! Identify the rank for a given vector, using a transposed matrix as 'input'.
void ktCorrelation_compute_rank_forVector_firstTranspose(const uint index1, const uint size, t_float **data, char **mask, t_float *tmp_array, uint *tmp_array_index, t_float *result_array, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation, t_float *optional_arrOf_sorted) {
  if(needTo_useMask_evaluation == false) {
    // FIXME: update our time-benchmarks to evlauate the time-signifance of this 'optionn'.
    //! Note: from perofmrance-evlaution of oru "graphAlgorithms_distance::compute_transposedMatrix(..)" we expect a perormacne-increase of 1.2x wrt. use of this fucniton-option.
    const uint size_innerOptimized = (size > 16) ? size - 4 : 0;
    uint i = 0;
    if(size_innerOptimized > 0) {
      for(; i < size_innerOptimized; i += 4) {
	__m128 vec_tmp = _mm_set_ps(data[i+0][index1], data[i+0][index1], data[i+2][index1], data[i+3][index1]);
	_mm_storeu_ps(&tmp_array[i], vec_tmp);
      }
    }
    for(; i < size; i++) {
      tmp_array[i] = data[i][index1];
    }
  } else {
    //! Idnetify the interesting vertices:
    if(mask) {
      for(uint i = 0; i < size; i++) {
	if(mask[i][index1]) {
	  tmp_array[i] = data[i][index1];
	} else {tmp_array[i] = T_FLOAT_MAX;}
      }
    } else {
      for(uint i = 0; i < size; i++) {
	if(isOf_interest(data[i][index1])) {
	  tmp_array[i] = data[i][index1];
	} else {tmp_array[i] = T_FLOAT_MAX;}
      }
    }
  }

  //printf("list_size=%u, at [%s]:%s:%d\n", size, __FUNCTION__, __FILE__, __LINE__);
  //! Compute the rank:
  getrank_memoryRe_use(size, tmp_array, result_array,  tmp_array_index, typeOf_performance_sortAppraoch_toUse, optional_arrOf_sorted);
}


//! Extends "ktCorrelation_compute_rankFor_matrices(..)" with an option "onlyCompute_data1_index" to only comptue for a particular index in "data1_input": for the latter iniput we expect that the data is transpsoed (oekseth, 06. setp. 2016).
//! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
void ktCorrelation_compute_rankFor_matrices_andVector(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char** mask1, char** mask2, t_float ***matrix_tmp_1_, t_float ***matrix_tmp_2_, const uint transpose, const bool needTo_useMask_evaluation, const uint onlyCompute_data1_index, const uint iterationIndex_2) {
  
  //printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

  //! Allocate the conaitners to be used for the sorted lists:
  const uint size_x = (transpose == 0) ? nrows : ncols;   const uint size_y = (transpose == 0) ? ncols : nrows;
  assert(*matrix_tmp_1_ == NULL);
  assert(*matrix_tmp_2_ == NULL);
  const t_float default_value_float = 0;
  if(onlyCompute_data1_index == UINT_MAX) {
    *matrix_tmp_1_ = allocate_2d_list_float(size_x, size_y, default_value_float);
  } else {
    if(data1_input != data2_input) { //! then we may safely only allate oen row-index (oekseth, 06. otk. 2016).
      const uint size_x = 1; //(onlyCompute_data1_index + 1);
      assert(size_x <= (nrows+1));
      //printf("allocates for size_x=%u, at %s:%d\n", size_x, __FILE__, __LINE__);
      *matrix_tmp_1_ = allocate_2d_list_float(size_x, size_y, default_value_float);
    } else {
      const uint size_x = nrows; //(onlyCompute_data1_index + 1);
      assert(size_x <= (nrows+1));
      //printf("allocates for size_x=%u, at %s:%d\n", size_x, __FILE__, __LINE__);
      *matrix_tmp_1_ = allocate_2d_list_float(size_x, size_y, default_value_float);
    }
  }
  *matrix_tmp_2_ = *matrix_tmp_1_;

  if(data2_input && (!data1_input || (data1_input != data2_input) )) {
    if(transpose == 0) {
      const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
      *matrix_tmp_2_ = allocate_2d_list_float(nrows_2, ncols, default_value_float);
    } else {
      const uint ncols_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : ncols;	  
      *matrix_tmp_2_ = allocate_2d_list_float(nrows, ncols_2, default_value_float);
    }
  }

  t_float **matrix_tmp_1 = *matrix_tmp_1_; t_float **matrix_tmp_2 = *matrix_tmp_2_;

  //! Step(1): sort: --------------------------------------------------------------------------------------
  //! Then we allocate a temporary list, ie, to 'avopid' the empty values:
  assert(ncols > 0);
  t_float *list_column = NULL; uint *list_column_index = NULL;
  if(transpose == 0) {
    list_column = allocate_1d_list_float(ncols, default_value_float); 
    list_column_index = allocate_1d_list_uint(ncols, default_value_float);
    //printf("list_size=%u, at [%s]:%s:%d\n", ncols, __FUNCTION__, __FILE__, __LINE__);
  } else {
    list_column = allocate_1d_list_float(nrows, default_value_float); 
    list_column_index = allocate_1d_list_uint(nrows, default_value_float);
    //printf("list_size=%u, at [%s]:%s:%d\n", nrows, __FUNCTION__, __FILE__, __LINE__);
  }

  //! Comptue teh sorted lists:

  if(onlyCompute_data1_index != UINT_MAX) {assert(transpose == 0);} //! ie, as we otherwise may an an incosnstency.

  if(transpose == 0) {
    assert(matrix_tmp_1_ != NULL); //! ie, what we expect
    if(onlyCompute_data1_index != UINT_MAX) {
      const uint index1 = onlyCompute_data1_index;
      ktCorrelation_compute_rank_forVector(index1, /*size=*/ncols, /*data=*/data1_input, /*mask=*/mask1, /*temp-array=*/list_column, list_column_index, /*result-array=*/matrix_tmp_1[0], e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, NULL);
    } else {
      assert(matrix_tmp_1);
      //printf("nrows=%u, at %s:%d\n", nrows, __FILE__, __LINE__);
      for(uint index1 = 0; index1 < nrows; index1++) {
	assert(matrix_tmp_1[index1]);
	ktCorrelation_compute_rank_forVector(index1, /*size=*/ncols, /*data=*/data1_input, /*mask=*/mask1, /*temp-array=*/list_column, list_column_index, /*result-array=*/matrix_tmp_1[index1], e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, NULL);
      } 
    }
    if(data2_input && (!data1_input || (data1_input != data2_input) )) {
    //if(data1_input != data2_input) {
      assert(matrix_tmp_2);
      assert(data2_input);
      const uint nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : nrows;	  
      for(uint index1 = 0; index1 < nrows_2; index1++) {
	assert(matrix_tmp_2[index1]);
	assert(data2_input[index1]);
	ktCorrelation_compute_rank_forVector(index1, /*size=*/ncols, /*data=*/data2_input, /*mask=*/mask2, /*temp-array=*/list_column, list_column_index, /*result-array=*/matrix_tmp_2[index1], e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, NULL);
      } 
    }
  } else {
    for(uint index1 = 0; index1 < ncols; index1++) {
      ktCorrelation_compute_rank_forVector_firstTranspose(index1, /*size=*/nrows, /*data=*/data1_input, /*mask=*/mask1, /*temp-array=*/list_column, list_column_index, /*result-array=*/matrix_tmp_1[index1], e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, NULL);
    } 
    if(data2_input && (!data1_input || (data1_input != data2_input) )) {
    //if(data1_input != data2_input) {
      const uint ncols_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : ncols;	  
      for(uint index1 = 0; index1 < ncols_2; index1++) {
	ktCorrelation_compute_rank_forVector_firstTranspose(index1, /*size=*/nrows, /*data=*/data2_input, /*mask=*/mask2, /*temp-array=*/list_column, list_column_index, /*result-array=*/matrix_tmp_2[index1], e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, NULL);
      } 
    }
  }
  //! De-allcoate the temproary list:
  free_1d_list_float(&list_column);   free_1d_list_uint(&list_column_index);
}


//! Allocate memory and compute the rank for the matrices.
//! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
void ktCorrelation_compute_rankFor_matrices(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char** mask1, char** mask2, t_float ***matrix_tmp_1_, t_float ***matrix_tmp_2_, const uint transpose, const bool needTo_useMask_evaluation, const uint iterationIndex_2) {
  ktCorrelation_compute_rankFor_matrices_andVector(nrows, ncols, data1_input, data2_input, mask1, mask2, matrix_tmp_1_, matrix_tmp_2_, transpose, needTo_useMask_evaluation, /*onlyCompute_data1_index=*/UINT_MAX, iterationIndex_2);
}
