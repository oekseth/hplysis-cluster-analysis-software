#ifndef s_kt_computeTile_subResults_h
#define s_kt_computeTile_subResults_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_template_correlation_tile.h"
/* #include "log_clusterC.h" */
/* #include "e_kt_correlationFunction.h" */



/**
   @struct s_kt_computeTile_subResults
   @brief provide access to intermeidatre results (oekseth, 06. setp. 2016)
   @remarks is of improtance wrt. optmizaiotn of equations where the result cosnistes of muliple sub-sets which may first be joined when all valeus (betweenn/in tow rows) are evlauated.
   @auhtor Ole Kristine Ekseth (oekseth)
 **/
typedef struct s_kt_computeTile_subResults {
  //! Lcoal information wrt. the 'space' of allcoated data:
  uint _cnt_rows;  uint _cnt_cols;
  //! --------
  uint **matrixOf_result_cnt_interesting_cells; //! which is sued/icnmreneted if (a) mask-proerpties are set and (b) 'such' is recquested by/through the macro-parameter-call to our "template_correlation_tile.cxx" (oekseth, 06. setp. 2016).
  t_float **matrixOf_result_numerator; //! ie, the value 'above an equation-line'.
  t_float **matrixOf_result_denumerator; //! ie, the value 'below an equation-line'.

  //! More elabourate data-tonaienrs.
  /* t_float **matrixOf_result_numeratorLimitedTo_row1; //! ie, the value 'below an equation-line' for: row1 */
  
  //! Note: the "matrixOf_result_numeratorLimitedTo_row_1and2" is only used by "e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef"
  t_float **matrixOf_result_numeratorLimitedTo_row_1and2; //! ie, the value 'below an equation-line' for: row1 && row1, eg, in the "Tanimoto" distance-metric
  /* t_float **matrixOf_result_numeratorLimitedTo_row2; //! ie, the value 'below an equation-line' for: row2 */
  t_float **matrixOf_result_denumeratorLimitedTo_row1; //! ie, the value 'below an equation-line' for: row1
  t_float **matrixOf_result_denumeratorLimitedTo_row_1and2; //! ie, the value 'below an equation-line' for: row1 && row1, eg, in the "Jaccard" distance-metric
  t_float **matrixOf_result_denumeratorLimitedTo_row2; //! ie, the value 'below an equation-line' for: row2

  //! 
  //! Local mapping-information:
  uint globalResult_index_row; //! which is used as 'a point of reference' wrt. index-updates: used to simplify correctness-testing of our appraoch.
  uint globalResult_index_col; //! which is used as 'a point of reference' wrt. index-updates: used to simplify correctness-testing of our appraoch.
} s_kt_computeTile_subResults_t;

//! Initiate the "s_kt_computeTile_subResults_t" object.
void setTo_empty__s_kt_computeTile_subResults(s_kt_computeTile_subResults_t *self);

//! @return tru if this object has bexplcitly been set to 'empty' (oekseth, 06. otk. 2016).
bool isEmpty__s_kt_computeTile_subResults(const s_kt_computeTile_subResults_t *self);
/**
   @param <self> is the s_kt_computeTile_subResults objhect to update (oekseth, 06. setp. 2016)
   @param <max_chunkSize_rows> is the max number of row to be evlaauted seperately in the all-against-all memory-tile-comptauion.
   @param <max_chunkSize_columns> is the max number of columns to be evlaauted seperately in the all-against-all memory-tile-comptauion.
   @param <typeOf_distanceFormula> describes the degree of 'arptuiality' in the distance-corrlation-equations: used to avodi allcoating memory which will anyhow nto be seud, ie, to 'move compelxtiy fromt eh caller ot this fucniton'.
   @param <isTo_updateCountVariable> which is to be set to tru if (a) eitehr implcit or epxlcit masks are used && the distance-metric (which is being comptued) need this knowledge for correct comptuation of a given metric.
   @param <isTo_findMaxValue> which if to be set to "true" if we are to itnate the result-value with "T_FLOAT_MIN_ABS", ie, to avodi intaiting with "0" to resutl in errnrous results
   @param <isTo_findMinValue> which if to be set to "true" if we are to itnate the result-value with "T_FLOAT_MAX", ie, to avodi intaiting with "0" to resutl in errnrous results
 **/
void init__s_kt_computeTile_subResults(s_kt_computeTile_subResults_t *self, const uint max_chunkSize_rows, const uint max_chunkSize_columns, const enum e_template_correlation_tile_typeOf_distanceUpdate typeOf_distanceFormula, const bool isTo_updateCountVariable, const bool isTo_findMaxValue, const bool isTo_findMinValue);

//! Clear old data
//! @remarks is used to avoid unneccesary re-allcoatesions for each [index1...][index2...] tile which is evlauated/computed.
void clearOldData(s_kt_computeTile_subResults_t *self, const enum e_template_correlation_tile_typeOf_distanceUpdate typeOf_distanceFormula, const bool isTo_updateCountVariable, const bool isTo_findMaxValue, const bool isTo_findMinValue);


//! A warpper-macro-funtion to ge tthe local row-index:
#define getRowIndex__s_kt_computeTile_subResults_t(self, offset_row) ({self->globalResult_index_row + offset_row;})
//! A warpper-macro-funtion to ge tthe local column-index:
#define getColumnIndex__s_kt_computeTile_subResults_t(self, offset_column) ({self->globalResult_index_col + offset_column;})

//! De-allcoate a given object of type "s_kt_computeTile_subResults_t"
void free__s_kt_computeTile_subResults(s_kt_computeTile_subResults_t *self);


#endif //! EOF
