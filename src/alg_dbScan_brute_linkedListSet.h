#ifndef alg_dbScan_brute_linkedListSet_h
#define alg_dbScan_brute_linkedListSet_h
/**
   @file 
   @brief an implemtnation of DBSCAN using linked lists. 
   @remarks 
   -- generic: to ensure that the permtuation reflects the best-performing strategy, the impmetantion given the highest-scoring rank at "google.com" (date: 01.01.2019) is chosen. For details of the tempalte, see "https://github.com/gyaikhom/dbscan/blob/master/dbscan.c"
   -- permtuation: assumes a correlation matrix is provided, hence avoids the needs for pre-comptuing pariwise simlairties.
 **/
#include "kt_matrix.h"
#include "kt_clusterAlg_fixed_resultObject.h"

/**
   @brief comptues DBSCAN.
   @param <obj_1> is the covariance simalrity matrix.
   @param <obj_result> holds the resutls of the clustering
   @param <epsilon> where scores less than eplsion is used.
   @param <minpts> the maximum number of poitns which needs to be related to the given vertex 'for it to be sued as a straverse starting point'.
   @return true upon success.
 **/
bool compute__alg_dbScan_brute_linkedListSet(const s_kt_matrix_base_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const t_float epsilon, const uint minpts);

#endif //! EOF: #define alg_dbScan_brute_linkedListSet_h
