### How do I get set up? ###



```
#!bash

#!
#! install? call "bash install.bash" in your terminal
bash install.bash


#!
#! Compile a simple tutorial (tut):
g++ -I../src/  -O2  -mssse3   -L ../src/  tut_helloWorld.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_tut

#! Execute the above tutorial:
./x_tut

```

