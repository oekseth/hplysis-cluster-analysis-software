/**
   @brief comptue score for each column-index (oekseth, 06. sept. 2016)
   @remarks may be sued both for comptaution of correlation-scores between two rows and wrt. tilign-operaitons: for details of use pelase see our "template_correlation_tile.c"
   
   @remarks in [”elow] we include a minor subset of the iniptu-params we execpect to get set: for a comrpehensive lsit please see docuemtantion in our "template_correlation_tile.cxx"
   @param<__config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject> which is used to turn on and off the 'configuraiton'wrt. mergign of itnernal data-objects
**/


//! ......................................................................
//! *** start: apply Masks:
//! ---------:
#if( (TEMPLATE_iterationConfiguration_typeOf_mask == /*e_template_correlation_tile_maskType_mask_implicit=*/2) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 0) ) //! ------------------------------
#define internalTemplateMacro__applyMasks(vec_maskCount) ({/*printf("(c1) at %s:%d\n", __FILE__, __LINE__);*/ VECTOR_FLOAT_dataMask_implicit_updatePair(term1, term2);})
#elif( (TEMPLATE_iterationConfiguration_typeOf_mask == /*e_template_correlation_tile_maskType_mask_implicit=*/2) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 1) ) //! ------------------------------
	    //! then we include a slgiht overehad wrt. the comoptuation of the amsk-matric, ie, to get a 'unified' count-varialbe from the mask-proeprties
#define internalTemplateMacro__applyMasks(vec_maskCount) ({/*printf("(c2) at %s:%d\n", __FILE__, __LINE__);*/ vec_maskCount = VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(term1, term2);})
	    // mul = VECTOR_FLOAT_dataMask_pair(mul, term1, term2);
	    //! ----------------------------------------------------------------------------------------------------------
#elif( (TEMPLATE_iterationConfiguration_typeOf_mask == /*e_template_correlation_tile_maskType_mask_explicit=*/1) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 0) )
	    /*! Make use of the implict FLT_MAX mask-lists, ie, muliply the mask with the copmuted distance: */ 
#define internalTemplateMacro__applyMasks__dummy(vec_maskCount) ({ \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(mask1); \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(mask2); \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros((i + j2 + VECTOR_FLOAT_ITER_SIZE) <= ncols); /*ie, validate the 'memroy-boundayr' (oesketh, 06. mar. 2017) */ \
      /*printf("(c3) at %s:%d\n", __FILE__, __LINE__);*/ ;}) // FIXME: incldue [”elow]
#define internalTemplateMacro__applyMasks(vec_maskCount) ({ \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros((i + j2 + VECTOR_FLOAT_ITER_SIZE) <= ncols); /*ie, validate the 'memroy-boundayr' (oesketh, 06. mar. 2017) */ \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(mask1); \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(mask2); \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(&rmask1[j2]); \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(&rmask2[j2]); \
      VECTOR_FLOAT_TYPE vec_mask_1 = VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(rmask1, j2); \
      VECTOR_FLOAT_TYPE vec_mask_2 = VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(rmask2, j2); \
	VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(term1, term2, vec_mask_1, vec_mask_2, vec_mask_combined);})
//      /*printf("(c3) at %s:%d\n", __FILE__, __LINE__);*/ VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef_atIndex(term1, term2, rmask1, rmask2, j2);})
#elif( (TEMPLATE_iterationConfiguration_typeOf_mask == /*e_template_correlation_tile_maskType_mask_explicit=*/1) && (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 1) )
//! then we include a slgiht overehad wrt. the comoptuation of the amsk-matric, ie, to get a 'unified' count-varialbe from the mask-proeprties
#define internalTemplateMacro__applyMasks(vec_maskCount) ({/*printf("(c4) at %s:%d\n", __FILE__, __LINE__);*/ vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef_atIndex(term1, term2, rmask1, rmask2, j2);})
	    //! -------------------------------------------------------
#else
#define internalTemplateMacro__applyMasks(vec_maskCount)({}) //! ie, a dummy wrapper to simplify the code-reading.
#endif
	    
//! ......................................................................
//! *** end: apply Masks.
//! ---------:

//! ----------------------------
//! start: distance-computation: ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) )
#define internalTemplateMacro__computeDistance(result_tmp) ({result_tmp = TEMPLATE_distanceConfiguration_macroFor_metric(term1, term2);})
#define internalTemplateMacro__computeDistance__SSE(mul) ({mul = TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2);})

#elif( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 1) )
	    //! then we include knowledge of the power to 'raise' a number 'into':
#define internalTemplateMacro__computeDistance(result_tmp) ({result_tmp = TEMPLATE_distanceConfiguration_macroFor_metric(term1, term2, self.configMetric_power);})
#define internalTemplateMacro__computeDistance__SSE(mul) ({mul = TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2, self.configMetric_power);})

#else //! then we 'provide' a complex resutl-object intot eh caller, eg, to handle mulipel result-set which may first be merged 'when all valeus are comptued':
	    //#elif(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumerator)
	    //! then we asusem that the macro-fucntion called in below udpated both a "nuermator" and a "denumerator" field:
#if( (TEMPLATE_distanceConfiguration_macroFor_metric_includePower == 0) )
#define internalTemplateMacro__computeDistance(result_tmp) ({TEMPLATE_distanceConfiguration_macroFor_metric(term1, term2, objLocalComplex_nonSSE);})
#define internalTemplateMacro__computeDistance__SSE(mul) ({; TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2, objLocalComplex_sse);})
#else //! then the object is both compelx and is 'adjusted' by a user-defined power-scalar-value
#define internalTemplateMacro__computeDistance(result_tmp) ({TEMPLATE_distanceConfiguration_macroFor_metric(term1, term2, objLocalComplex_nonSSE, self.configMetric_power);})
#define internalTemplateMacro__computeDistance__SSE(mul) ({; TEMPLATE_distanceConfiguration_macroFor_metric_SSE(term1, term2, objLocalComplex_sse, self.configMetric_power);})
#endif
#endif //! endif("TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate")
//! end: distance-computation: ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo



	    // FIXME[performance]: write a perofmrance-comparison-test where we compare teh time-effect of 'before applciaton of the distnace-formula' and 'after the applicaiton of the distance-formula' ... ie, for the "TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores" parameter

//! start: weight-ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#if( (TEMPLATE_distanceConfiguration_useWeights == 1) && (TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 0) )
//! start: --------
#define internalTemplateMacro__includeWeights__eachTerm() ({\
      term1 = term1 * weight[weight_index]; \
      term2 = term2 * weight[weight_index]; \
    })
#define internalTemplateMacro__includeWeights__eachTerm__SSE() ({ \
  term1 = VECTOR_FLOAT_MUL(term1, vec_weight); \
  term2 = VECTOR_FLOAT_MUL(term2, vec_weight); \
})
#define internalTemplateMacro__includeWeights__forResult() ({}) //! ie, a dummy wrapper to simplify the code-reading.
#define internalTemplateMacro__includeWeights__forResult__SSE() ({}) //! ie, a dummy wrapper to simplify the code-reading.
//! end: --------
#elif( (TEMPLATE_distanceConfiguration_useWeights == 1) && (TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 1) )
//! start: --------
#define internalTemplateMacro__includeWeights__eachTerm() ({}) //! ie, a dummy wrapper to simplify the code-reading.
#define internalTemplateMacro__includeWeights__eachTerm__SSE() ({}) //! ie, a dummy wrapper to simplify the code-reading.
//assert(&weight[weight_index]); /* FIXME: remove */     
/*const uint weight_index = i + j2;*/			      
#define internalTemplateMacro__includeWeights__forResult() ({ result_tmp = result_tmp * weight[weight_index]; }) 
#define internalTemplateMacro__includeWeights__forResult__SSE() ({ \
      vec_result = VECTOR_FLOAT_MUL(vec_result, vec_weight);  \
}) 
//! end: --------
#else //! then we asusem that tehre are no distance-metrics to be applied.
//! start: --------
#define internalTemplateMacro__includeWeights__eachTerm() ({}) //! ie, a dummy wrapper to simplify the code-reading.
#define internalTemplateMacro__includeWeights__eachTerm__SSE() ({}) //! ie, a dummy wrapper to simplify the code-reading.
#define internalTemplateMacro__includeWeights__forResult() ({}) //! ie, a dummy wrapper to simplify the code-reading.
#define internalTemplateMacro__includeWeights__forResult__SSE() ({}) //! ie, a dummy wrapper to simplify the code-reading.
//! end: --------
#endif
//! end: weight-ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit)
assert(rmask1); assert(rmask2);
#endif




	  uint j2 = 0; // which is used in [”elow]:


//printf("(inner::start)\t at %s:%d\n", __FILE__, __LINE__);

//printf("FIXME: include [”elow], at %s:%d\n", __FILE__, __LINE__);
#include "template_correlation_tile_innerMostColumn__SSE.c" //! ie, apply SSE-inner-logics (oekseth, 06. nov. 2016).


//correlation_delta_tileMacros__mask_implicit(chunkSize_cnt_i, chunkSize_cnt_i_intris, rmul1, rmul2, weight, /*index=*/1 + i2, typeOf_metric);	  
#if (configure_SSE_useFor_mask_explicit == 0) ||  (configure_SSE_alwasyPad_arraySize_with_SSE_blockSize == 0)	  
/*Thereafter comptue for the non--itnristnicts 'post-processing:*/
/*! Then 'handle' for the 'trail':*/				
	  {
	    //assert(&rmul1[j2]); 	    assert(&rmul2[j2]); // FIXME: remove
	    //__correlation_delta_tileMacros__mask_implicit_serial(chunkSize_cnt_i, rmul1, rmul2, weight, weight_index, typeOf_metric); 
	    //printf("(info)\t Comptue non-ranked correlation-matrix, given j2=%u and chunkSize_cnt_i=%u, at %s:%d\n", j2, chunkSize_cnt_i, __FILE__, __LINE__);
	    
	    if( (i + chunkSize_cnt_i) > ncols) {chunkSize_cnt_i = ncols - i;}
	    
	    assert_possibleOverhead( (i + chunkSize_cnt_i) <= ncols); // ie, what we expect wrt. [”elow]


	    for (; j2 < chunkSize_cnt_i; j2 ++) {			
	      
	      //assert(rmul1); assert(rmul2); assert(&rmul1[j2]); 	    assert(&rmul2[j2]); assert(j2 < ncols); // FIXME: remove
	      const uint weight_index = i + j2;
	      if(
#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_implicit)
		 //! Then we evlaute wrt. T_FLOAT_MAX :
		 isOf_interest(rmul1[j2]) && isOf_interest(rmul2[j2])
#elif(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit)
		 //! Then we sue the mask-valeus as an explcit valuse.
		 rmask1[j2] && rmask2[j2]
#else
		 true //! ie, as the valeu is epxected always being of itnerest.
#endif
		 ) { 


		//assert(j2 < ncols); // FIXME: remvoe.
		t_float term1 = rmul1[j2], term2 = rmul2[j2];
		// printf("\t\t(%f x %f), at %s:%d\n", term1, term2, __FILE__, __LINE__);
		//! where [”elow] si out-commeted as we observe for some metrics that the vlaue increase to inf for large number, eg, wrt. the '*pow5* minkoski-emtrics (oekseth, 06. arp. 2017) */
		//assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(term1) == false); assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(term2) == false); 

#ifndef NDEBUG //! then we validate that our maks-appraoch is correctly applied in [above].
 //! where [”elow] si out-commeted as we observe for some metrics that the vlaue increase to inf for large number, eg, wrt. the '*pow5* minkoski-emtrics (oekseth, 06. arp. 2017) */
/* #if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_implicit) */
/* 		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term1 != T_FLOAT_MAX); */
/* 		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term2 != T_FLOAT_MAX); */
/* #elif(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) */
/* 		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term1 != T_FLOAT_MAX); */
/* 		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term2 != T_FLOAT_MAX); */
/* #else */
/* 		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term1 != T_FLOAT_MAX); */
/* 		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(term2 != T_FLOAT_MAX); */
/* #endif */
#endif

		//! Call a generlized funciton to include the weights:
		internalTemplateMacro__includeWeights__eachTerm();
	      
		//! Compute the distance:
		//printf("Compute for value-pair(%f, %f), at %s:%d\n", term1, term2, __FILE__, __LINE__);
	      	t_float result_tmp; internalTemplateMacro__computeDistance(result_tmp);

		
		//! Call a generlized funciton to include the weights wr.t the post-processing:
		internalTemplateMacro__includeWeights__forResult();
		
		
#if( (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
		//printf("result_tmp[%u]=%f -->sum=%f, at %s:%d\n", j2, result_tmp, result + result_tmp, __FILE__, __LINE__);
		//printf("\t\t\t[%u]\t (sum-values)\t %f VS %f, at %s:%d\n", j2, result, result_tmp, __FILE__, __LINE__);
		result  += result_tmp;
		// printf("\t\t %f += %f = (%f x %f), at %s:%d\n", result, result_tmp, term1, term2, __FILE__, __LINE__);
#elif(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) //! tehnw e are itnerested in the max-value
		//! ----
#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
		//printf("\t\t\t [%u]\t (cmp-val)\t %f VS %f, at %s:%d\n", j2, result, result_tmp, __FILE__, __LINE__);
		result = macro_min(result, result_tmp);
		//printf("\t\t\t\t (result)\t %f at %s:%d\n", result, __FILE__, __LINE__);
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
		//printf("\t\t\t[%u]\t (cmp-val)\t %f VS %f, at %s:%d\n", j2, result, result_tmp, __FILE__, __LINE__);
		result = macro_max(result, result_tmp);
#else
#warning "Investigate this issue, ie, are we to suem 'um of vlaues'?"
		//! ----
#endif //! endif(type-of-sum)
#else
		//printf("(assumes-corrMetric-is-complex), at %s:%d\n", __FILE__, __LINE__);
#endif


#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0)  ) //! then we are to update the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice:
		//printf("\t\t\t\t(updated-the-weight\t at %s:%d\n", __FILE__, __LINE__);
#if(TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores == 1)
		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(weight);
		assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(weight_index < ncols);
		objLocalComplex_nonSSE.cnt_interesting_cells += weight[weight_index]; //! ie, update the temproal result-vector wrt. the counts
#else //! then we assume the weight-table is Not set (oekseth, 06. des. 2016)
		objLocalComplex_nonSSE.cnt_interesting_cells += 1; //! ie, update the temproal result-vector wrt. the counts: "+1" is used as we at this exueciton-point assuems that the cell is not 'remvoed by/through maksing'.
#endif
#elif((TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*/0) && (__templateInternalVariable__isTo_updateCountOf_vertices == 1) ) //! tehnw e are itnerested in the max-value
		//printf("\t\t\t\t(updated-only-the-coutns-wrt.the-weight\t at %s:%d\n", __FILE__, __LINE__);
		objLocalComplex_nonSSE.cnt_interesting_cells += 1; //! ie, update the temproal result-vector wrt. the counts: "+1" is used as we at this exueciton-point assuems that the cell is not 'remvoed by/through maksing'.
#endif
		
	      }
	    }
	  }
#endif //! wrt. the non-SSE step.

#if(__config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject == 1)
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar) //! if:start(non-scalar-updated).
#define __template_correlation_tile_innerMostColumn_mergeComplex__config__useSSE 0
//printf("\t\t(merge-results-associated-to-vertex, at %s:%d\n", __FILE__, __LINE__);
	    #include "template_correlation_tile_innerMostColumn_mergeComplex.c" //! ie, then merge compelx result-objects
#endif //! if:end(non-scalar-updated).
#endif


          //! 
	  //! 
	  //! Update the counts wrt. the inferred results:

#if(__config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject == 1)
#if(__templateInternalVariable__isTo_updateCountOf_vertices == 1)
complexResult->matrixOf_result_cnt_interesting_cells[global_index1][global_index2] += (uint)objLocalComplex_nonSSE.cnt_interesting_cells;
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
complexResult->matrixOf_result_cnt_interesting_cells[global_index1][global_index2] += (uint)objLocalComplex_nonSSE.cnt_interesting_cells;
#elif(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
complexResult->matrixOf_result_cnt_interesting_cells[global_index1][global_index2] += (uint)objLocalComplex_nonSSE.cnt_interesting_cells;          
#endif
#endif
#endif //! where we do not udpate the complex object if a user expects 'usign the "objLocalComplex_nonSSE" object 'ciretly'.

/* #if(__config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject == 1) */
/* #if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == /\*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*\/0) ) //! then we are to update the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice: */
/* 	  complexResult->matrixOf_result_cnt_interesting_cells[global_index1][global_index2] += (uint)objLocalComplex_nonSSE.cnt_interesting_cells; */
/* #elif( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != /\*e_template_correlation_tile_typeOf_distanceUpdate_scalar=*\/0) ) //! then we are to update the count of itnereesting cells, ie, we then udpat ehte count of itneresting vertice: */
/* 	  complexResult->matrixOf_result_cnt_interesting_cells[global_index1][global_index2] += (uint)VECTOR_FLOAT_storeAnd_horizontalSum(objLocalComplex_SSE.cnt_interesting_cells); */
/* 	  complexResult->matrixOf_result_cnt_interesting_cells[global_index1][global_index2] += (uint)objLocalComplex_nonSSE.cnt_interesting_cells;           */
/* #endif  */
/* #endif //! where we do not udpate the complex object if a user expects 'usign the "objLocalComplex_nonSSE" object 'ciretly'. */


//printf("(inner::completed)\t at %s:%d\n", __FILE__, __LINE__);

//! -------------------------------------------------------------
//! Clear macro-functions:
#undef internalTemplateMacro__includeWeights__eachTerm
#undef internalTemplateMacro__includeWeights__eachTerm__SSE
#undef internalTemplateMacro__includeWeights__forResult
#undef internalTemplateMacro__includeWeights__forResult__SSE
#undef internalTemplateMacro__applyMasks
#undef internalTemplateMacro__computeDistance
#undef internalTemplateMacro__computeDistance__SSE
#undef internalTemplateMacro__includeWeights__forResult__SSE
#undef __config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject


//printf("sum=%f, at %s:%d\n", result, __FILE__, __LINE__);
