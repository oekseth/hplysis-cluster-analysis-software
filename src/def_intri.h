#ifndef def_intri_h
#define def_intri_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */



/**
   @file def_intri
   @brief describes and generalises a set of intrisnitistc SIMD macros.
   @author Ole Kristian Ekseth (oekseth)
   @remarks in the set of macro-fucntiosn which we use, we oberve the following categories/clases (of tasks):
   - ...?? 
   - masks: use either implict masks (eg, FLT_MAX) or 'direct' masks (eg, "0" stored in a spserte mask-array, or "0" in the dta-array itself);
   - ...?? 

   ... a challenge in rpgoramming wrt. intrisnitistc is/cosnerns our goal to construct programs which may be used on semi-new devices, ie, to avoid the pre-codniton/requriement of ...??...

   ... wrt. 'the goal to learn intrisnitistc' ... it seems like a mjaor chalelnge is to correctly itnerpret the warnigns given by teh compiler, eg, wrt: float data1[4]  = {0, 1, 2, 3};; __m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);; VECTOR_FLOAT_TYPE mask = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float(vec_term1), VECTOR_FLOAT_SET1(0)); 
   ... a different challegne (in learning to efficently program/apply/use intrisnitistc) is the 'reversion' of the storage-order. An exmaple of the latter is seen wrt. "vec_result = VECTOR_FLOAT_revertOrder(vec_result);" ub our "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1, VECTOR_FLOAT_LOG)" VS the lgocis in our "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(..)" (wrt. the "VECTOR_FLOAT_revertOrder(..)" call).

   .. conceptual challenge when using intel SSE itnruscriotns is/conserns the diffnere ce in programming from 'explict if-claues' to 'no-branch-statements' (a case which si explcit true for marchine-hardware which only support versions less than SSE3): instead of computing if(...) { ... } else { ...}, both resutls are comptued, and then 'merged togheter'. The conspetual complexity (wrt. the latter) is foudn/seen wrt. ...??.. 


   .... we have observed that the use of SSE intrisnticts may improve the efficency/perofmrance of non-SSE code ... and exmaple is the use of muliplciaotns instead of divisons ... where the diccutlg/challenging handling of "NAN" values in SSE encoruage the use of muliplcatiosn: an improtant side-effect is that muliplciatiosn has a higher perofmrnace than divison, from which a 'similar-written' non-SSE code results in a better/faster performance.

 **/


#define DO_PRAGMA(x) _Pragma (#x)
#define COMPILESTATUS(x) DO_PRAGMA(message ("COMPILE-STATUS - " #x))

//! start: file def_intri ---------------------------------------------------------


// FIXME: cosnider moving [”elow] "isOf_interest(..)" to a different funciton.
#define isOf_interest(value) ( (value != FLT_MAX) && (value != FLT_MIN) )
#define get_implicitMask() ({T_FLOAT_MAX;}) //! ie, returnt he implicti-mask-value

//! Pre-comptue the 'divsor' used to handle NAN-covnersions from flaot to char.
// FIXME[JC]: ... may you try to idneitfy a funciton which 'aovids' the ned/suage of [”elow] macro?
#define __constant_float_charMin  (float)((float)1.0 / SCHAR_MIN)
//#define __constant_float_charMin_negative  (float)((float)-1.0 / SCHAR_MIN)


#ifndef SWIG
#include <math.h>
#include <xmmintrin.h>
#include <immintrin.h>
#include <xmmintrin.h> 
#include <pmmintrin.h>
#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"
#ifdef __SSE4_1__
#include <smmintrin.h>
#endif
#endif // #ifndef SWIG


#ifndef VECTOR_CONFIG_USE_FLOAT
//! Then we use a default configuration wrt. the float-data-type to use (oekseth, 06. june 2016)
// FIXME: if we choose to use oterh 'valeus' than 0|1 for "VECTOR_CONFIG_USE_FLOAT" ... then update the macros which use the "VECTOR_CONFIG_USE_FLOAT" .. to an if-else macro-clause
#define VECTOR_CONFIG_USE_FLOAT 1
#endif




// FIXME[jc]: consider to include the 'defts' from  "/home/klatremus/poset_src/externalLibs/libpll-1.0.11/src/parsimony.c" in 'this' <-- may you describe how the different compiel-specs are set (indcued [”elow])?

/* #define ULINT_SIZE 64 */
/* #define INTS_PER_VECTOR 8 */
/* #define LONG_INTS_PER_VECTOR 4 */
/* #define INT_TYPE __m256d */
/* #define CAST double* */
/* #define SET_ALL_BITS_ONE (__m256d)_mm256_set_epi32(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF) */
/* #define SET_ALL_BITS_ZERO (__m256d)_mm256_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000) */
/* #define VECTOR_LOAD _mm256_load_pd */
/* #define VECTOR_BIT_AND _mm256_and_pd */
/* #define VECTOR_BIT_OR  _mm256_or_pd */
/* #define VECTOR_STORE  _mm256_store_pd */
/* #define VECTOR_AND_NOT _mm256_andnot_pd */
 
/* #elif (defined(__SSE3)) */

/* #include <xmmintrin.h>  */
/* #include <pmmintrin.h> */
  
/* #define INTS_PER_VECTOR 4 */
/* #ifdef __i386__ */
/* #define ULINT_SIZE 32 */
/* #define LONG_INTS_PER_VECTOR 4 */
/* #else */
/* #define ULINT_SIZE 64 */
/* #define LONG_INTS_PER_VECTOR 2 */
/* #endif */
/* #define INT_TYPE __m128i */
/* #define CAST __m128i* */
/* #define SET_ALL_BITS_ONE _mm_set_epi32(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF) */
/* #define SET_ALL_BITS_ZERO _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000) */
/* #define VECTOR_LOAD _mm_load_si128 */
/* #define VECTOR_BIT_AND _mm_and_si128 */
/* #define VECTOR_BIT_OR  _mm_or_si128 */
/* #define VECTOR_STORE  _mm_store_si128 */
/* #define VECTOR_AND_NOT _mm_andnot_si128 */

/* #endif */
 

/**
   FIXME[jc]: ... wrt. coding-schemed ... may you itrnospect upon .... how we may beautfiy the vector-code through void-pointers ... 
   ... in the grammer we often observe a 'pattern': 
   <code-block> ::= <for-loop-wrapper> + <logical-block> + <tail-operations>
   <logical-block> ::= "expansion of variables into 'load', 'set', 'aritmetics' and 'store'"
   <tail-operations> ::= "scalar-max" | "scalar-min" | ...       
 **/
 
#define VECTOR_CHAR_ITER_SIZE 16
// FIXME: may we use 'the fact' that VECTOR_CHAR_TYPE and VECTOR_INT_TYPE is the 'same'? ... eg, when 'covnerting' from differnet vector-formats??
//! FIXME[JC]: what is the differnece between "__m64" and "__m128i" ... ... why is there 'such a difnferec'e? .... and how may this difference be tuilized/ and voercomed? ... and how may the "_mm_max_epu8(..)" be combined with "_mm_sub_epi8(..)"? (oekseth, 06.. otk. 2016).
#define VECTOR_CHAR_TYPE __m64
#define VECTOR_CHAR_TYPE_alt __m128i

#define VECTOR_CHAR_SET1 _mm_set1_pi8
#define VECTOR_CHAR_MIN _mm_min_pu8
#define VECTOR_CHAR_MAX _mm_max_pu8
#define VECTOR_CHAR_SUB _mm_sub_epi8

//! @retunr the absoltue value of the flaoting-poitn vector.
#define VECTOR_CHARU_abs(vec) ({VECTOR_CHARU_MAX_alt(VECTOR__SUB(VECTOR_INT16_SET_zero(), vec), vec);})
// FIXME[JC: may you write a macor which use [”elow] and de-acitiveates [”elow] ... and similar for other/similar cases ... and thereafter thest the performance-differnecE? (oekseth, 06. okt. 2016).
									      //#define VECTOR_INT16_abs(vec) ({_mm_abs_epi8(vec);})

//#define VECTOR_CHAR_TYPE __m128i
// FIXME[JC]: is it a better stratgy/approach to laod a 'transoposed' row than [”elow]?
/* char, char, char, char,  */
/* char, char, char, char,  */
/* char, char, char, char,  */
/* char, char, char, char)’ */
// , matrix[rowPos_dynamic+16][columnPos_fixed], matrix[rowPos_dynamic+17][columnPos_fixed], matrix[rowPos_dynamic+18][columnPos_fixed], matrix[rowPos_dynamic+19][columnPos_fixed], matrix[rowPos_dynamic+20][columnPos_fixed], matrix[rowPos_dynamic+21][columnPos_fixed], matrix[rowPos_dynamic+22][columnPos_fixed], matrix[rowPos_dynamic+23][columnPos_fixed], matrix[rowPos_dynamic+24][columnPos_fixed], matrix[rowPos_dynamic+25][columnPos_fixed], matrix[rowPos_dynamic+26][columnPos_fixed], matrix[rowPos_dynamic+27][columnPos_fixed], matrix[rowPos_dynamic+28][columnPos_fixed], matrix[rowPos_dynamic+29][columnPos_fixed], matrix[rowPos_dynamic+30][columnPos_fixed], matrix[rowPos_dynamic+31][columnPos_fixed]
#define VECTOR_CHAR_setFrom_rows(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_epi8(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed], matrix[rowPos_dynamic+4][columnPos_fixed], matrix[rowPos_dynamic+5][columnPos_fixed], matrix[rowPos_dynamic+6][columnPos_fixed], matrix[rowPos_dynamic+7][columnPos_fixed], matrix[rowPos_dynamic+8][columnPos_fixed], matrix[rowPos_dynamic+9][columnPos_fixed], matrix[rowPos_dynamic+10][columnPos_fixed], matrix[rowPos_dynamic+11][columnPos_fixed], matrix[rowPos_dynamic+12][columnPos_fixed], matrix[rowPos_dynamic+13][columnPos_fixed], matrix[rowPos_dynamic+14][columnPos_fixed], matrix[rowPos_dynamic+15][columnPos_fixed]))

#define VECTOR_CHAR_setFrom_columns VECTOR_CHAR_setFrom_rows //! ie, a wrapper

//! Load a chunk of 16 (ie, "4 * 4Byte") 8-bit organised memory:
#define VECTOR_CHAR_set_data(arrOf_values, rowPos_dynamic) ({*(__m64*)&arrOf_values[rowPos_dynamic]})
#define VECTOR_CHAR_LOAD(arr) (*(__m64*)arr)
#define VECTOR_CHAR_LOAD_alt(mem_ref)  ( _mm_load_si128((__m128i*)mem_ref) )
#define VECTOR_CHAR_STORE(result, vec) (_mm_store_si128((__m128i*)result, vec))
#define VECTOR_CHAR_STOREU(result, vec) (_mm_storeu_si128((__m128i*)result, vec))

//! Note an example-applciaiton is for: "mask[i] = (array[i] > 0) ? 1 : 0;"
// FIXME[jc]: is there a better 'way' to convert a 'float' to a char'?
// FIXME: compare [”elow] "VECTOR_CHAR_convertFrom_float_old(..)" to [ªbove] "VECTOR_CHAR_convertFrom_float(..)"
#define VECTOR_CHAR_convertFrom_float_old(arrOf_values, rowPos_dynamic) ( _mm_set_epi8((char)arrOf_values[rowPos_dynamic+0], (char)arrOf_values[rowPos_dynamic+1], (char)arrOf_values[rowPos_dynamic+2], (char)arrOf_values[rowPos_dynamic+3], (char)arrOf_values[rowPos_dynamic+4], (char)arrOf_values[rowPos_dynamic+5], (char)arrOf_values[rowPos_dynamic+6], (char)arrOf_values[rowPos_dynamic+7], (char)arrOf_values[rowPos_dynamic+8], (char)arrOf_values[rowPos_dynamic+9], (char)arrOf_values[rowPos_dynamic+10], (char)arrOf_values[rowPos_dynamic+11], (char)arrOf_values[rowPos_dynamic+12], (char)arrOf_values[rowPos_dynamic+13], (char)arrOf_values[rowPos_dynamic+14], (char)arrOf_values[rowPos_dynamic+15]))
// FIXME[jc]: is there a better 'way' to convert a 'float' to a char'?
#define VECTOR_CHAR_convertFrom_uint(arrOf_values, rowPos_dynamic) ( _mm_set_epi8((char)arrOf_values[rowPos_dynamic+0], (char)arrOf_values[rowPos_dynamic+1], (char)arrOf_values[rowPos_dynamic+2], (char)arrOf_values[rowPos_dynamic+3], (char)arrOf_values[rowPos_dynamic+4], (char)arrOf_values[rowPos_dynamic+5], (char)arrOf_values[rowPos_dynamic+6], (char)arrOf_values[rowPos_dynamic+7], (char)arrOf_values[rowPos_dynamic+8], (char)arrOf_values[rowPos_dynamic+9], (char)arrOf_values[rowPos_dynamic+10], (char)arrOf_values[rowPos_dynamic+11], (char)arrOf_values[rowPos_dynamic+12], (char)arrOf_values[rowPos_dynamic+13], (char)arrOf_values[rowPos_dynamic+14], (char)arrOf_values[rowPos_dynamic+15]))

/* #define VECTOR_CHAR_convertFrom_float_toBoolean(arrOf_values, rowPos_dynamic) ({ \ */
/*   __m128 vec_float = VECTOR_FLOAT_getBoolean_aboveZero_vec(VECTOR_FLOAT_setFrom_columns_array(arrOf_values, rowPos_dynamic */
/* }) */

// FIXME[jc]: validae correctness of [”elow] _mm_loadu_si128(..) and _mm_storeu_si128(..)
// #define VECTOR_CHAR_storeAnd_add(result, vec_add1) (_mm_storeu_si128((__m128i*)result, _mm_add_epi8(_mm_loadu_si128((__m128i*)result), vec_add1)))


#define VECTOR_CHAR_storeAnd_add(result, vec_add1) (_mm_storeu_si128((__m128i*)result, _mm_add_epi16(_mm_loadu_si128((__m128i*)result), vec_add1)))
#define VECTOR_CHAR_storeAnd_horizontalSum(vec_add1) ({char result[VECTOR_CHAR_ITER_SIZE] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; VECTOR_CHAR_STORE(result, vec_add1); \
  char scalar_result = 0; for(uint m = 0; m < VECTOR_CHAR_ITER_SIZE; m++) { {scalar_result += result[m];} } \

#include "def_typeOf_int.h" //! which includes defintions for "int" and "short int"
#include "def_typeOf_float.h" //! which includes defintions for "int" and "short int"

//! --------------------------------------------------------------------------------------------------
//! ---------------------------------------- int16_t ----------------------------------------------------------
//!
//! Handle different options wrt. INT16:
#define VECTOR_INT16_ITER_SIZE 8
// FIXME[jc]: may you suggest a better type?
#define VECTOR_INT16_TYPE __m128i
//! Specify a vector wrt. the index-range. 
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]? <-- may we use "_mm_loadu_si32(..)" ??
#define VECTOR_INT16_setFrom_indexRange(index) (_mm_set_epi16((int16_t)(index+0), (int16_t)(index+1), (int16_t)(index+2), (int16_t)(index+3), (int16_t)(index+4), (int16_t)(index+5), (int16_t)(index+6), (int16_t)(index+7)))
//! Load a chunk of memory:
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_INT16_set_data(arrOf, index) (_mm_set_epi16((int16_t)(arrOf[index+0]), (int16_t)(arrOf[index+1]), (int16_t)(arrOf[index+2]), (int16_t)(arrOf[index+3]), (int16_t)(arrOf[index+4]), (int16_t)(arrOf[index+5]), (int16_t)(arrOf[index+6]), (int16_t)(arrOf[index+7])))
//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
#define VECTOR_INT16_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_epi16(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed], matrix[rowPos_dynamic+4][columnPos_fixed], matrix[rowPos_dynamic+5][columnPos_fixed], matrix[rowPos_dynamic+6][columnPos_fixed], matrix[rowPos_dynamic+7][columnPos_fixed]))


#define VECTOR_INT16_SET_zero() ({_mm_set_epi16(0, 0, 0, 0, 0, 0, 0, 0);})
//! --- simple aritmetics
// FXIME[JC]: mauy you validate correctness of [”elow]?
//#define VECTOR_INT16_MUL _mm_mul_epu16
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_INT16_ADD _mm_add_epi16
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_INT16_SUB _mm_sub_epi16
// FXIME[JC]: mauy you validate correctness of [”elow]?
//#define VECTOR_INT16_DIV _mm_div_epi16
#define VECTOR_INT16_MUL _mm_mullo_epi16
#define VECTOR_INT16_MAX _mm_max_epi16
#define VECTOR_INT16_MIN _mm_min_epi16


// FIXME[jc]: validate correctness of [below]
#define VECTOR_INT16_LOAD(mem_ref)  ( _mm_load_si128((__m128i*)mem_ref) )
#define VECTOR_INT16_LOADU(mem_ref) ( _mm_loadu_si128((__m128i*)mem_ref) )
// FIXME[jc]: validate correctness of [below]
#define VECTOR_INT16_STORE(mem_ref, vector) (_mm_store_si128((__m128i*)mem_ref, vector) )
#define VECTOR_INT16_STOREU(mem_ref, vector) (_mm_storeu_si128((__m128i*)mem_ref, vector) )

//! Print the cotnents of a vector to stdout
// FIXME[jc]: wrt. [”elow] ... what is the difference when compared to "float *p = (float *)&vec_result";
#define VECTOR_INT16_PRINT(vec_result) ({int16_t result[8] = {0, 0, 0, 0, 0, 0, 0, 0}; VECTOR_INT16_STORE(result, vec_result); for(uint i = 0; i < 8; i++) {printf("vec[%u] = %u, ", i, result[i]);} fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); })

// FIXME[jc]: validae correctness of [”elow] _mm_loadu_si128(..) and _mm_storeu_si128(..)
#define VECTOR_INT16_storeAnd_add(result, vec_add1) (_mm_storeu_si128((__m128i*)result, _mm_add_epi16(_mm_loadu_si128((__m128i*)result), vec_add1)))
#define VECTOR_INT16_storeAnd_horizontalSum(vec_add1) ({int16_t result[VECTOR_INT16_ITER_SIZE] = {0, 0, 0, 0, 0, 0, 0, 0}; VECTOR_INT16_STOREU(result, vec_add1); \
      int16_t  scalar_result = 0; for(uint m = 0; m < VECTOR_INT16_ITER_SIZE; m++) { {scalar_result += result[m];} } \
      scalar_result; }) //! ie, return the result.

//! @retunr the absoltue value of the flaoting-poitn vector.
#define VECTOR_INT16_abs(vec) ({VECTOR_INT16_MAX(VECTOR_INT16_SUB(VECTOR_INT16_SET_zero(), vec), vec);})
// FIXME[JC: may you write a macor which use [”elow] and de-acitiveates [”elow] ... and similar for other/similar cases ... and thereafter thest the performance-differnecE? (oekseth, 06. okt. 2016).
									      //#define VECTOR_INT16_abs(vec) ({_mm_abs_epi16(vec);})


//! --------------------------------------------------------------------------------------------------
//! -------------------------------------  UINT   -------------------------------------------------------------
//!
//! Handle different options wrt. UINT:
#define VECTOR_UINT_ITER_SIZE 4
// FIXME[jc]: may you suggest a better type?
#define VECTOR_UINT_TYPE __m128i
//! Specify a vector wrt. the index-range. 
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]? <-- may we use "_mm_loadu_si32(..)" ??
#define VECTOR_UINT_setFrom_indexRange(index) (_mm_set_epi32((uint)(index+0), (uint)(index+1), (uint)(index+2), (uint)(index+3)))
//! Load a chunk of memory:
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_UINT_set_data(arrOf, index) (_mm_set_epi32((uint)(arrOf[index+0]), (uint)(arrOf[index+1]), (uint)(arrOf[index+2]), (uint)(arrOf[index+3])))
//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
#define VECTOR_UINT_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_epi32(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed]))

//! --- simple aritmetics
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_MUL _mm_mul_epu32
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_ADD _mm_add_epi32
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_SUB _mm_sub_epi32
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_DIV _mm_div_epi32

// FIXME[jc]: validate correctness of [below]
#define VECTOR_UINT_LOAD(mem_ref) ( _mm_loadu_si128((__m128i*)mem_ref) )
// FIXME[jc]: validate correctness of [below]
#define VECTOR_UINT_STORE(mem_ref, vector) (_mm_store_si128((__m128i*)mem_ref, vector) )
#define VECTOR_UINT_STOREU(mem_ref, vector) (_mm_storeu_si128((__m128i*)mem_ref, vector) )

//! Print the cotnents of a vector to stdout
// FIXME[jc]: wrt. [”elow] ... what is the difference when compared to "float *p = (float *)&vec_result";
#define VECTOR_UINT_PRINT(vec_result) ({uint result[4] = {0, 0, 0, 0}; VECTOR_UINT_STORE(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec[%u] = %u, ", i, result[i]);} fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); })



//! @return the interation-size with offset:
#define macro__get_maxIntriLength_uint(length) ( (length > VECTOR_UINT_ITER_SIZE) ? (length - VECTOR_UINT_ITER_SIZE) : 0  )

//! @return the for-loop-size to be sued.
#define VECTOR_CHAR_maxLenght_forLoop(size) ( (size >= VECTOR_CHAR_ITER_SIZE) ? size - VECTOR_CHAR_ITER_SIZE : 0 )
//! @return the for-loop-size to be sued.
#define VECTOR_UINT_maxLenght_forLoop(size) ( (size >= VECTOR_UINT_ITER_SIZE) ? size - VECTOR_UINT_ITER_SIZE : 0 )

/* // FIXME[jc]: may you imrpove speed of [”elow] funciton? */
/* #define VECTOR_FLOAT_MEMSET(arr, size, value) ({ */

/* }) */

/** // FIXME[jc] may you support [”elow] new-added 'operations'?
    

 **/


//! Print the cotnents of a vector to stdout
// FIXME[jc]: may you try to idnetify a mroe effective method/approach than [”elow] instruciton/funciton?
#define VECTOR_CHAR_PRINT(vec_result) (VECTOR_FLOAT_PRINT(VECTOR_FLOAT_convertFrom_CHAR(vec_result)))

//! Convert a float to an 8-bit intrisinistic char-vector.
//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array
#define VECTOR_CHAR_convertFrom_float(arrOf_values, rowPos_dynamic) (VECTOR_CHAR_convertFrom_float_vector(VECTOR_FLOAT_LOAD(&arrOf_values[rowPos_dynamic])))



#if 0 == 1
/* definition to expand macro then apply to pragma message */
#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var "="  VALUE(var)


/* Some example here */
#pragma message(VAR_NAME_VALUE(VECTOR_CONFIG_USE_FLOAT))
#pragma message(VAR_NAME_VALUE(macroConfig_use_posixMemAlign)) 
#endif



// TODO: cosnider to implement for rpef-"http://stackoverflow.com/questions/19494114/parallel-prefix-cumulative-sum-with-sse": xisum

// FIXME[JC]: may to propose/describe macros to merge/unify procedusres in opur "def_memAlloc.h" with 'those in this "def_intri.h" file, eg, wrt. "_mm_store_ps(..)" VS "_mm_storeu_ps(..)" ... ??
#include "def_sparseInt.h"
#include "def_memAlloc.h" //! which need to be included after the defniont of "t_float", ie, to aovid incosnsitencies between the memory-allcoations and the 'defualt cofniguraitons'.

//! end: file def_intri ---------------------------------------------------------
#endif
