#ifndef __MiCo__useLocalVariablesIn__varName_useLocal
#define __MiV__exper nameOf_experiment
#define __MiV__reaLife_list mapOf_realLife
#define __MiV__reaLife_list_size mapOf_realLife_size
#endif

const char *__MiV__exper = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
  //! -------------------------------------------------------------------------
  //!
  //!
  //! Intiate the colleciton of real-life data-sets:
  const uint __MiV__reaLife_list_size = 66; //! ie, "wc -l ./data/local_downloaded/metaObj.c"
    s_hp_clusterFileCollection_t __MiV__reaLife_list[__MiV__reaLife_list_size] = {
#include "data/local_downloaded/metaObj.c" //! ie, the colleciton of data-files, a colleciton where we asusme 'the caller mof this srcirpt' is situtated in the "src/" folder of the hpLysis software.
  };
#undef __MiV__exper
#undef __MiV__reaLife_list
#undef __MiV__reaLife_list_size
