#define __score mat->matrix[row_id][col_id]
#if(1 == 1)
#define __MiF__printScore(tag) ({;}) //! ie, a 'empty' wrapper.
#else
#define __MiF__printScore(tag) ({fprintf(stderr, "(change::%s)\t[%u][%u]=%f, at %s:%d\n", tag, row_id, col_id, __score, __FILE__, __LINE__);})
#endif

  for(uint row_id = 0; row_id < mat->nrows; row_id++) {
    //   for my $n (@nodes) { #! ie, for each row: 
    t_float sum = 0;
    t_float sumsq = 0;
    t_float max = T_FLOAT_MIN_ABS;
    /* my $sumsq = 0; */
    /* my $max = 0; */
    for(uint col_id = 0; col_id < mat->ncols; col_id++) {
      //      for my $nb (keys %{$mx->{$n}}) {
      //# FIXME: move [below] to inseriton-rpcoedure.
      const t_float score = __score;
      if(
	 !__MiF__isOf_interest(score) || 
	 (score < conf_cellThreshold) ) { //! remove/prune elemnts below a certain threshold.
	//         if ($mx->{$n}{$nb} < conf_cellThreshold) { #
	__MiF__printScore("empty");
	__score = 0; //T_FLOAT_MAX; //! ie,     delete($mx->{$n}{$nb});
      } else {
	//! Comptue "pow(...)": 
	__MiF__printScore("power");
	assert(isOf_interest(__score)); 
	__score = __MiF__pow(__score); //$mx->{$n}{$nb} **= $I;
	assert(isOf_interest(__score));
	sum += __score; //! ie, the sum-of-features.
      }
    }
    if(sum != 0) { //! then we adjsut by the sum and copmtue the global-squared-sum.
      const t_float sum_inv = 1.0/sum;
      for(uint col_id = 0; col_id < mat->ncols; col_id++) {
	//      for my $nb (keys %{$mx->{$n}}) {
	//# FIXME: move [below] to inseriton-rpcoedure.
	const t_float score = __score;
	if(__MiF__isOf_interest(score) ) {
	  __score = macro_mul(__score, sum_inv); //! ie, $mx->{$n}{$nb} /= $sum;
	  assert(isOf_interest(__score));
	  __MiF__printScore("average");
	  sumsq = macro_pluss(sumsq, __score * __score);  //! sum x_i^2 over stochastic vector x
	  max = macro_max(max, __score);
	}
	//$max = $mx->{$n}{$nb} if $max < $mx->{$n}{$nb};
      }
    }
    const t_float diff_local = max - sumsq;
    chaos = macro_max(chaos, diff_local);
  }
#undef __score
#undef __MiF__printScore
