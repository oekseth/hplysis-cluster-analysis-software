/******************************************************************************/
/* The C Clustering Library.
 * Copyright (C) 2002 Michiel Jan Laurens de Hoon.
 *
 * This library was written at the Laboratory of DNA Information Analysis,
 * Human Genome Center, Institute of Medical Science, University of Tokyo,
 * 4-6-1 Shirokanedai, Minato-ku, Tokyo 108-8639, Japan.
 * Contact: mdehoon 'AT' gsc.riken.jp
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation with or without modifications and for any purpose and
 * without fee is hereby granted, provided that any copyright notices
 * appear in all copies and that both those copyright notices and this
 * permission notice appear in supporting documentation, and that the
 * names of the contributors or copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific prior permission.
 * 
 * THE CONTRIBUTORS AND COPYRIGHT HOLDERS OF THIS SOFTWARE DISCLAIM ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 * 
 */

#ifndef C_CLUSTER_LIBRARY
#define C_CLUSTER_LIBRARY
//! The __cplusplus macro and C_CLUSTER_LIBRARY macro was included in this document by oekseth to make it possible to link to KnittingTools C++-library.
#ifdef __cplusplus
extern "C"{
#endif 

#ifndef min
#define min(x, y)	((x) < (y) ? (x) : (y))
#endif
#ifndef max
#define	max(x, y)	((x) > (y) ? (x) : (y))
#endif

#ifdef WINDOWS
#  include <windows.h>
#endif

#include "configure_hcaMem.h" //! Whcih specifies the optmizaitons and macro-configurations

#define CLUSTERVERSION "1.52"

#define __clusterC__standAlone 0

/* Chapter 2 */
t_float clusterdistance (int nrows, int ncolumns, t_float** data, int** mask,
  t_float weight[], int n1, int n2, int index1[], int index2[], char dist,
  char method, int transpose);
t_float** distancematrix (int ngenes, int ndata, t_float** data,
  int** mask, t_float* weight, char dist, int transpose);

/* Chapter 3 */
int getclustercentroids(int nclusters, int nrows, int ncolumns,
  t_float** data, int** mask, int clusterid[], t_float** cdata, int** cmask,
  int transpose, char method);
void getclustermedoids(int nclusters, int nelements, t_float** distance,
  int clusterid[], int centroids[], t_float errors[]);
void kcluster (int nclusters, int ngenes, int ndata, t_float** data,
  int** mask, t_float weight[], int transpose, int npass, char method, char dist,
  int clusterid[], t_float* error, int* ifound);
void kmedoids (int nclusters, int nelements, t_float** distance,
  int npass, int clusterid[], t_float* error, int* ifound);

/* Chapter 4 */
#if(__clusterC__standAlone == 1)
typedef struct {int left; int right; t_float distance;} Node;
#endif
/*
 * A Node struct describes a single node in a tree created by hierarchical
 * clustering. The tree can be represented by an array of n Node structs,
 * where n is the number of elements minus one. The integers left and right
 * in each Node struct refer to the two elements or subnodes that are joined
 * in this node. The original elements are numbered 0..nelements-1, and the
 * nodes -1..-(nelements-1). For each node, distance contains the distance
 * between the two subnodes that were joined.
 */

Node* treecluster (int nrows, int ncolumns, t_float** data, int** mask,
  t_float weight[], int transpose, char dist, char method, t_float** distmatrix);
void cuttree (int nelements, Node* tree, int nclusters, int clusterid[]);

/* Chapter 5 */
void somcluster (int nrows, int ncolumns, t_float** data, int** mask,
  const t_float weight[], int transpose, int nxnodes, int nynodes,
  t_float inittau, int niter, char dist, t_float*** celldata,
  int clusterid[][2]);

#if(__clusterC__standAlone == 1)
/* Chapter 6 */
int pca(int m, int n, t_float** u, t_float** v, t_float* w);
#endif

/* Utility routines, currently undocumented */
void sort(int n, const t_float data[], int index[]);
t_float mean(int n, t_float x[]);
t_float median (int n, t_float x[]);

t_float* calculate_weights(int nrows, int ncolumns, t_float** data, int** mask,
  t_float weights[], int transpose, char dist, t_float cutoff, t_float exponent);

  //! Locally added 'number-value-range' functions (oekseth, 06. july 2016)
  t_float uniform(void);
  int binomial(int n, t_float p);

  //! Locally added correlation function (oekseth, 06. july 2016)
  t_float euclid (int n, t_float** data1, t_float** data2, int** mask1, int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float cityblock (int n, t_float** data1, t_float** data2, int** mask1, int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float correlation (int n, t_float** data1, t_float** data2, int** mask1, int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float acorrelation (int n, t_float** data1, t_float** data2, int** mask1,  int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float ucorrelation (int n, t_float** data1, t_float** data2, int** mask1, int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float uacorrelation (int n, t_float** data1, t_float** data2, int** mask1, int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float c_spearman (int n, t_float** data1, t_float** data2, int** mask1, int** mask2, const t_float weight[], int index1, int index2, int transpose);
  t_float c_kendall (int n, t_float** data1, t_float** data2, int** mask1, int** mask2,  const t_float weight[], int index1, int index2, int transpose);
#ifdef __cplusplus
}
#endif

#endif
