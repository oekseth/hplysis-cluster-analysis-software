#include "maskAllocate.h"

//! Allcoate a distance-matrix and an assicated mask-set.
uint maskAllocate__makedatamask__uint(const uint nrows, const uint ncols, float*** data, uint*** mask, const bool isTo_use_continousSTripsOf_memory) { 
  assert(nrows != 0);
  assert(ncols != 0);
  const uint total_lenght = nrows * ncols;
  
  float **pdata = NULL; uint **pmask = NULL;

  assert(nrows < 100000000); //! else a try-catch-block may be of itnerest.b


  if(isTo_use_continousSTripsOf_memory) {
    pdata = allocate_2d_list_float(nrows, ncols, /*empty-value=*/0);
    pmask = allocate_2d_list_uint(nrows, ncols, /*empty-value=*/0);
  } else { // Then we use a sub-optimal memory-allocaiton-appraoch:
    pdata = alloc_generic_type_2d(t_float, pdata, nrows);
    pmask = alloc_generic_type_2d(uint, pmask, nrows);
/* #ifdef __cplusplus */
/*     pdata = new float*[nrows]; pmask = new uint*[nrows]; //pmask = new char*[nrows]; */
/* #else  */
/*     pdata = (float*)malloc(sizeof(float*)*nrows); */
/*     pmask = (uint*)malloc(sizeof(uint*)*nrows); */
/* #endif */
    for(uint i = 0; i < nrows; i++) {      
      //printf("\t i=%u, given n=%u, at %s:%d\n", i, nrows, __FILE__, __LINE__);
      pdata[i] = allocate_1d_list_float(ncols, /*empty-value=*/0);
      pmask[i] = allocate_1d_list_uint(ncols, /*empty-value=*/0);
    }
  }

  //! Update:
  *data = pdata; *mask = pmask;

  //! @return true upon success.
  return 1;
}

/* ---------------------------------------------------------------------- */

//! De-allocate the data allcoated in "makedatamask(..)"
void maskAllocate__freedatamask__uint(const uint n, float** data, uint** mask, const bool isTo_use_continousSTripsOf_memory) { 
  assert(data);
  //assert(mask);  
  if(isTo_use_continousSTripsOf_memory) {
    free_2d_list_float(&data, n);
    if(mask) {
      free_2d_list_uint(&mask, n);
    }
  } else { // Then we use a sub-optimal memory-allocaiton-appraoch:
    for(uint i = 0; i < n; i++) {
      free_1d_list_float(&data[i]);
      if(mask) {free_1d_list_uint(&mask[i]);}
    }
#ifdef __cplusplus
    delete [] data; if(mask) {delete [] mask;}
#else
    free(data); if(mask) free(mask);
#endif
  }
}

//! Allcoate a distance-matrix and an assicated mask-set.
uint maskAllocate__makedatamask(const uint nrows, const uint ncols, float*** data, char*** mask, const bool isTo_use_continousSTripsOf_memory) { 
  assert(nrows != 0);
  assert(ncols != 0);
  const uint total_lenght = nrows * ncols;
  
  float **pdata = NULL; char **pmask = NULL;

  assert(nrows < 100000000); //! else a try-catch-block may be of itnerest.b


  if(isTo_use_continousSTripsOf_memory) {
    pdata = allocate_2d_list_float(nrows, ncols, /*empty-value=*/0);
    // FIXME: consider to incldue [”elow].
    pmask = allocate_2d_list_char(nrows, ncols, /*empty-value=*/0);    
  } else { // Then we use a sub-optimal memory-allocaiton-appraoch:
    pdata = alloc_generic_type_2d(t_float, pdata, nrows);
    pmask = alloc_generic_type_2d(char, pmask, nrows);
/* #ifdef __cplusplus */
/*     pdata = new float*[nrows]; // pmask = new char*[nrows]; //pmask = new char*[nrows]; */
/* #else  */
/*     pdata = (float*)malloc(sizeof(float*)*nrows); */
/*     // FIXME: consider to incldue [”elow]. */
/*     pmask = (uint*)malloc(sizeof(uint*)*nrows); */
/* #endif */
    for(uint i = 0; i < nrows; i++) {      
      //printf("\t i=%u, given n=%u, at %s:%d\n", i, nrows, __FILE__, __LINE__);
      pdata[i] = allocate_1d_list_float(ncols, /*empty-value=*/0);
      pmask[i] = allocate_1d_list_char(ncols, /*empty-value=*/0);
      // FIXME: consider to incldue [”elow].
    }
  }

  //! Update:
  *data = pdata; *mask = pmask;

  //! @return true upon success.
  return 1;
}

/* ---------------------------------------------------------------------- */

//! De-allocate the data allcoated in "makedatamask(..)"
void maskAllocate__freedatamask(const uint n, float** data, char** mask, const bool isTo_use_continousSTripsOf_memory) { 
  assert(data);
  //assert(mask);  
  if(isTo_use_continousSTripsOf_memory) {
    free_2d_list_float(&data, n);
    if(mask) {
      free_2d_list_char(&mask, n);
    }
  } else { // Then we use a sub-optimal memory-allocaiton-appraoch:
    for(uint i = 0; i < n; i++) {
      free_1d_list_float(&data[i]);
      if(mask) {free_1d_list_char(&mask[i]);}
    }
#ifdef __cplusplus
    delete [] data; if(mask) {delete [] mask;}
#else
    free(data); if(mask) free(mask);
#endif
  }
}

