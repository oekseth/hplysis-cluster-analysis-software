#ifndef correlation_inCategory_matrix_base_h
#define correlation_inCategory_matrix_base_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlation_inCategory_matrix_base
   @brief provide base-logics for all-against all computations of correlation-scores.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/



//#include "correlationFunc_optimal_compute_allAgainstAll_SIMD.h"
#include "s_allAgainstAll_config.h"
#include "correlation_inCategory_delta.h"


//! Copmute the optmial distance-matrix:
void ktCorr_matrixBase__optimal_compute_allAgainstAll_nonSIMD(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, s_allAgainstAll_config_t *config_allAgainstAll); //, const uint CLS, const bool isTo_use_continousSTripsOf_memory, uint iterationIndex_2 = UINT_MAX);
//! Copmute the optmial distance-matrix:
void ktCorr_matrixBase__optimal_compute_allAgainstAll(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config_t *config_allAgainstAll);
//void ktCorr_matrixBase__optimal_compute_allAgainstAll(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const uint CLS, const bool isTo_use_continousSTripsOf_memory, uint iterationIndex_2, const bool isTo_use_SIMD, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_undef );

//! @brief comptue the correlations in a matrix.
//! @remarks in this function we test the effect of computing 'sums seperately' VS 'the naive/old/previous method', eg, wrt. "sum1 += w*term1;" sum2 += w*term2;", "denom1 += w*term1*term1;", "denom2 += w*term2*term2;"  and "tweight += weight[i];" in the "graphAlgorithms_distance::correlation(..)" function.  Differently tols, we test the effect of both using temporary data-structures 'an not using temproary data-structures', ie, as correlation may be comptued using either a 'muliple mulitciaotn of the same values' or 'moving out the duplciatioe copmtautiosn into seperate lists/data-structures'.
void ktCorr_compute_allAgainstAll_distanceMetric_correlation(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, bool transpose,  s_allAgainstAll_config_t *config_allAgainstAll);
//void ktCorr_compute_allAgainstAll_distanceMetric_correlation(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, bool transpose, const enum e_typeOf_optimization_distance typeOf_optimization ,  const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, e_cmp_masksAre_used_t  masksAre_used );

#endif //! EOF
