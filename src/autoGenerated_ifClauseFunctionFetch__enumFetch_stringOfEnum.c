 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_euclid", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_euclid")))) { return e_kt_correlationFunction_groupOf_minkowski_euclid;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_cityblock", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_cityblock")))) { return e_kt_correlationFunction_groupOf_minkowski_cityblock;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_minkowski", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_minkowski")))) { return e_kt_correlationFunction_groupOf_minkowski_minkowski;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne")))) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3")))) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4")))) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5")))) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_chebychev", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_chebychev")))) { return e_kt_correlationFunction_groupOf_minkowski_chebychev;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max")))) { return e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_Gower", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_Gower")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_Gower;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_Soergel", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_Soergel")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_Soergel;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_Canberra", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_Canberra")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_Canberra;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5")))) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_intersection", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_intersection")))) { return e_kt_correlationFunction_groupOf_intersection_intersection;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_intersection_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_intersection_altDef")))) { return e_kt_correlationFunction_groupOf_intersection_intersection_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_WaveHedges", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_WaveHedges")))) { return e_kt_correlationFunction_groupOf_intersection_WaveHedges;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt")))) { return e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Czekanowski", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Czekanowski")))) { return e_kt_correlationFunction_groupOf_intersection_Czekanowski;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef")))) { return e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Motyka", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Motyka")))) { return e_kt_correlationFunction_groupOf_intersection_Motyka;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Motyka_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Motyka_altDef")))) { return e_kt_correlationFunction_groupOf_intersection_Motyka_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Kulczynski", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Kulczynski")))) { return e_kt_correlationFunction_groupOf_intersection_Kulczynski;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Ruzicka", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Ruzicka")))) { return e_kt_correlationFunction_groupOf_intersection_Ruzicka;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Tanimoto", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Tanimoto")))) { return e_kt_correlationFunction_groupOf_intersection_Tanimoto;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef")))) { return e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_innerProduct", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_innerProduct")))) { return e_kt_correlationFunction_groupOf_innerProduct_innerProduct;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_harmonicMean", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_harmonicMean")))) { return e_kt_correlationFunction_groupOf_innerProduct_harmonicMean;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_cosine", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_cosine")))) { return e_kt_correlationFunction_groupOf_innerProduct_cosine;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook")))) { return e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_Dice", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_Dice")))) { return e_kt_correlationFunction_groupOf_innerProduct_Dice;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef")))) { return e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef")))) { return e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance")))) { return e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian")))) { return e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator")))) { return e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_fidelity", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_fidelity")))) { return e_kt_correlationFunction_groupOf_fidelity_fidelity;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya")))) { return e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_Hellinger", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_Hellinger")))) { return e_kt_correlationFunction_groupOf_fidelity_Hellinger;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_Matusita", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_Matusita")))) { return e_kt_correlationFunction_groupOf_fidelity_Matusita;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef")))) { return e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef")))) { return e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_squaredChord", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_squaredChord")))) { return e_kt_correlationFunction_groupOf_fidelity_squaredChord;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef")))) { return e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Euclid", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Euclid")))) { return e_kt_correlationFunction_groupOf_squared_Euclid;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Pearson", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Pearson")))) { return e_kt_correlationFunction_groupOf_squared_Pearson;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Neyman", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Neyman")))) { return e_kt_correlationFunction_groupOf_squared_Neyman;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_squaredChi", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_squaredChi")))) { return e_kt_correlationFunction_groupOf_squared_squaredChi;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_probabilisticChi", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_probabilisticChi")))) { return e_kt_correlationFunction_groupOf_squared_probabilisticChi;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_divergence", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_divergence")))) { return e_kt_correlationFunction_groupOf_squared_divergence;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Clark", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Clark")))) { return e_kt_correlationFunction_groupOf_squared_Clark;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi")))) { return e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic")))) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute")))) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered")))) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute")))) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_shannon_KullbackLeibler", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_shannon_KullbackLeibler")))) { return e_kt_correlationFunction_groupOf_shannon_KullbackLeibler;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_shannon_Jeffreys", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_shannon_Jeffreys")))) { return e_kt_correlationFunction_groupOf_shannon_Jeffreys;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_shannon_kDivergence", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_shannon_kDivergence")))) { return e_kt_correlationFunction_groupOf_shannon_kDivergence;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_shannon_Topsoee", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_shannon_Topsoee")))) { return e_kt_correlationFunction_groupOf_shannon_Topsoee;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_shannon_JensenShannon", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_shannon_JensenShannon")))) { return e_kt_correlationFunction_groupOf_shannon_JensenShannon;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_shannon_JensenDifference", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_shannon_JensenDifference")))) { return e_kt_correlationFunction_groupOf_shannon_JensenDifference;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_combinations_Taneja", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_combinations_Taneja")))) { return e_kt_correlationFunction_groupOf_combinations_Taneja;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_combinations_KumarJohnson", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_combinations_KumarJohnson")))) { return e_kt_correlationFunction_groupOf_combinations_KumarJohnson;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock")))) { return e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges")))) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax")))) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin")))) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric")))) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax")))) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax")))) { return e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin")))) { return e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine")))) { return e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_rank_kendall_coVariance", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_rank_kendall_coVariance")))) { return e_kt_correlationFunction_groupOf_rank_kendall_coVariance;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock")))) { return e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_rank_kendall_Dice", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_rank_kendall_Dice")))) { return e_kt_correlationFunction_groupOf_rank_kendall_Dice;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_rank_kendall_Goodman", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_rank_kendall_Goodman")))) { return e_kt_correlationFunction_groupOf_rank_kendall_Goodman;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized")))) { return e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_mic", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_mic")))) { return e_kt_correlationFunction_groupOf_MINE_mic;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_mas", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_mas")))) { return e_kt_correlationFunction_groupOf_MINE_mas;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_mev", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_mev")))) { return e_kt_correlationFunction_groupOf_MINE_mev;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_mcn", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_mcn")))) { return e_kt_correlationFunction_groupOf_MINE_mcn;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_mcn_general", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_mcn_general")))) { return e_kt_correlationFunction_groupOf_MINE_mcn_general;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_gmic", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_gmic")))) { return e_kt_correlationFunction_groupOf_MINE_gmic;}
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_MINE_tic", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_MINE_tic")))) { return e_kt_correlationFunction_groupOf_MINE_tic;}
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_avg")))) { return e_kt_correlationFunction_groupOf_directScore_direct_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_sum")))) { return e_kt_correlationFunction_groupOf_directScore_direct_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_direct_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_mul")))) { return e_kt_correlationFunction_groupOf_directScore_direct_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_min")))) { return e_kt_correlationFunction_groupOf_directScore_direct_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_max")))) { return e_kt_correlationFunction_groupOf_directScore_direct_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_direct_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_direct_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_abs")))) { return e_kt_correlationFunction_groupOf_directScore_direct_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_direct_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_direct_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( 0 == strncmp(stringOf_enum, "e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus", macro_min(strlen(stringOf_enum), strlen("e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus")))) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus;}
#endif
