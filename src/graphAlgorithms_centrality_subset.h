#ifndef graphAlgorithms_centrality_subset_h
#define graphAlgorithms_centrality_subset_h


/**
   @class graphAlgorithms_centrality
   @brief provide logics to compute centralites from datsets of values
   @author Ole Kristian Ekseth (oekseth)
 **/
class graphAlgorithms_centrality_subset {
  // FIXME: move contents from our "graphAlgorithms_centrality" to thsi class .. and then update the latter writh 'wrappers' and enums for "in"", "out", and "bi" ... and incldue/write a 'compression-scheme' ... moving parts of "sp_compressed" to a new class named "sp_compressed_base"
};

#endif
