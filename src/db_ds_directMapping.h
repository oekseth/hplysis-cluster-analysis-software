#ifndef db_ds_directMapping_h
#define db_ds_directMapping_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "kt_set_2dsparse.h"
#include "kt_set_1dsparse.h"
#include "correlation_sort.h"
#include "db_rel.h"
#include "db_ds_bTree_rel.h" //! ie, where we 'integrate' a B-tree to simplify our comparison with different database-appraoches (oekseth, 06. mar. 2017).

/**
   @struct s_db_ds_directMapping
   @brief a strucutre desgiend to evlauate the time-cost of different data-base data-access appraoches (oekseth, 60. mar. 2017).
   @remarks provides an alternative database-interface for a "direct list-access as alternaitve to be-tree": the interface is (among others) used to provide an altenraitve data-structure to the 'naive' "B-tree" data-strucutre: in our appraoch we faciliate the use of a 'direct list-access'. To increase searhc-fperofmrance we provide options to 'normalise' wrt. names: we include 'into the struct' optiol lists for "map_syn_head, map_syn_rt, map_syn_tail". When searching for relations we have used/added indexes for both 'tail-vertices' and 'head-vertices: the latter is implied using our "kt_set_2dsparse.h" (where latter hold index-referances to a to-be-written struct "s_db_tailRel_t").
   @remarks "b-tree": in order to investigate the importance of our relation-based aprpaoch we integrate our "s_db_ds_bTree_rel_t" (into our  "db_ds_directMapping.h") (ie, as an alternative to accessing our "s_db_rel *listOf_rel").
   @remarks a slow-down wrt. our "b-tree" appraoch cosnerns the 'implcit support' for wild-cards ... ie, as the latter does 'not allow' us from breakign/sotppign the search 'if a amtch is ofund' (eg, as seen in our "db_ds_bTree__stubSearch.c"). The latter 'contraint' implies that we 'for each time we find a relation' ... update search-resutl ... and then 'continue recusive B-tre-traversal' .... ie, to 'handle' the 'match-muliple' case.
 **/
typedef struct s_db_ds_directMapping {
  //s_db_tailRel_t *listOf_tailRel;    uint listOf_tailRel_size;
  s_db_rel *listOf_rel; 
  //! ----------------------
  s_db_ds_bTree_rel_t treeOf_relations;
  bool treeOf_relations__isUsed;
  //! ----------------------
  uint cnt_head;
  uint listOf_rel_size;
  uint listOf_rel_size__currPos;
  s_kt_set_2dsparse_t mapOf_vertexTo_tailRel; //! where each "mapOf_vertexTo_tailRel[head][i] => listOf_tailRel[ index=[head][i] ]"
  s_kt_set_2dsparse_t mapOf_vertexFrom_tail; //! where each "mapOf_vertexFrom_tail[tail][i] => listOf_tailRel[ index=[tail][i] ]"
  //! ----------------------
  uint map__size;
  uint *map_syn_head;
  uint *map_syn_rt;
  uint *map_syn_tail;
  //! ----------------------
  struct s_db_ds_directMapping *objInverse;
  bool objInverse__isAllocated;
  /* //! ---------------------- */
  /* bool config__supportSynMapping; */
  //! ----------------------
} s_db_ds_directMapping_t;
//! Set the object to empty:
s_db_ds_directMapping_t setToEmpty__s_db_ds_directMapping_t();
//! Intiate the s_db_ds_directMapping_t object
s_db_ds_directMapping_t init__s_db_ds_directMapping_t(const uint cnt_head, const uint cnt_relations, const bool config__use2dList__toImproveAccessSpeed, const bool config__use2dList__toImproveAccessSpeed__alsoForTailToHead, const bool config__useExplcitSynbMappingTable, const bool config__allcoateInverse, const bool config__useBTree__forRelations);
/**
   @enum e_db_ds_directMapping__initConfig
   @brief generlaies our init-structre (oekseth, 06. mar. 2017).
 **/
typedef enum e_db_ds_directMapping__initConfig {
  e_db_ds_directMapping__initConfig__synMappings,
  e_db_ds_directMapping__initConfig__synMappings__2dList,
  e_db_ds_directMapping__initConfig__synMappings__2dList__mapInverse, //! where we 'inser thte relation-ids' into a sperate list: while appraoch results in a higher miss-rate compared to "e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse" 'beneift' is a reuce exueciotn-tiem when 'budlign' the data-sets.
  e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse,
  e_db_ds_directMapping__initConfig__2dList,
  e_db_ds_directMapping__initConfig__bTree_forRelations__syn,
  e_db_ds_directMapping__initConfig__bTree_forRelations,
  //e_db_ds_directMapping__initConfig__,
  //! -----------------------------------------
  e_db_ds_directMapping__initConfig_undef
} e_db_ds_directMapping__initConfig_t;

//! @return true if we are to sue an 'topmized syn-mapping-strategy'.
static const char *getString__e_db_ds_directMapping__initConfig(const e_db_ds_directMapping__initConfig_t enum_id) {
  if(enum_id == e_db_ds_directMapping__initConfig__synMappings) {
    return "synMap";
  } else if(enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList) {
    return "synMap_2dList";
  } else if(enum_id == e_db_ds_directMapping__initConfig__2dList) {
    return "2dList";
  } else if(enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList__mapInverse) {
    return "2dList_mapInv";
  } else if(enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse) {
    return "2dList_andInv";
  } else if(enum_id == e_db_ds_directMapping__initConfig__bTree_forRelations__syn) {
    return "synMap_bTree:rel";
  } else if(enum_id == e_db_ds_directMapping__initConfig__bTree_forRelations) {
    return "bTree:rel";
  } else {assert(false);}
  return "";
}
//! @return true if we are to sue an 'topmized syn-mapping-strategy'.
static const bool useOptimizedSynMappings__e_db_ds_directMapping__initConfig(const e_db_ds_directMapping__initConfig_t enum_id) {
  return (
	  (enum_id == e_db_ds_directMapping__initConfig__synMappings) ||
	  (enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList) || 
	  (enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse)
	  );
}
//! @returns an itnlized object using our "e_db_ds_directMapping__initConfig" init-fucniton
static s_db_ds_directMapping_t init__enumConfig__s_db_ds_directMapping_t(const uint cnt_head, const uint cnt_relations, const e_db_ds_directMapping__initConfig_t enum_id) {
  bool config__use2dList__toImproveAccessSpeed = false;
  bool config__use2dList__toImproveAccessSpeed__alsoForTailToHead = false;
  bool config__useExplcitSynbMappingTable = false;
  bool config__allcoateInverse = false;
  bool config__useBTree__forRelations = false;
  if(enum_id == e_db_ds_directMapping__initConfig__synMappings) {
    config__useExplcitSynbMappingTable = true;
  } else if(enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList) {
    config__use2dList__toImproveAccessSpeed = true;
    config__useExplcitSynbMappingTable = true;
  } else if(enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList__mapInverse) {
    config__use2dList__toImproveAccessSpeed = true;
    config__useExplcitSynbMappingTable = true;
    config__use2dList__toImproveAccessSpeed__alsoForTailToHead = true;
  } else if(enum_id == e_db_ds_directMapping__initConfig__2dList) {
    config__useExplcitSynbMappingTable = true;
  } else if(enum_id == e_db_ds_directMapping__initConfig__synMappings__2dList__andInverse) {
    config__useExplcitSynbMappingTable = true;    
    config__use2dList__toImproveAccessSpeed = true;
    config__allcoateInverse = true;
  } else if(enum_id == e_db_ds_directMapping__initConfig__bTree_forRelations__syn) {
    config__useBTree__forRelations = true;
    config__useExplcitSynbMappingTable = true;    
  } else if(enum_id == e_db_ds_directMapping__initConfig__bTree_forRelations) {
    config__useBTree__forRelations = true;
  } else {assert(false);}
  //! Constuct and return:
  return init__s_db_ds_directMapping_t(cnt_head, cnt_relations, config__use2dList__toImproveAccessSpeed, config__use2dList__toImproveAccessSpeed__alsoForTailToHead, config__useExplcitSynbMappingTable, config__allcoateInverse, config__useBTree__forRelations);
}
//! De-allcoate the object.
void free__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self);
//! Insert a relationship, using lgocis configured in the init-fucntion of our s_db_ds_directMapping_t self object (oekseth, 06. mar. 2017).
bool insertRelation__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const s_db_rel_t relation_inp);

//typedef enum e_db_ds_directMapping__inResultStack_score 

//! identify a list of internal indxes to interesting relations.
//! @remarks our "isToStore__verticesInResultStack" param 'solved'/hanldes hte cse/issue how are we to to 'correctly' 'extract' the cotnnet from 'relation-ids in our 'inverse object': in our apporahc we use hte latter boolean parameter to store the 'cocnrete vertices' and (not the relation-dis if set to true)
bool findRelation__cntInteresting__s_db_ds_directMapping_t(const s_db_ds_directMapping_t *self, const s_db_rel_t relation_inp, uint *scalar_cntInteresting, const bool namesAlreadyMappedIntoSynonyms, s_kt_set_1dsparse_t *result_set, const bool isToStore__verticesInResultStack);
//! Update the amping between a 'genrlaized vertex-name' vertex_norm and the 'sepcific' vertex_syn 'searchalbeæ' vertex-name
bool setMappingFor_head__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const uint vertex_norm, const uint vertex_syn);
//! Update the amping between a 'genrlaized vertex-name' vertex_norm and the 'sepcific' vertex_syn 'searchalbeæ' vertex-name
bool setMappingFor_rt__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const uint vertex_norm, const uint vertex_syn);
//! Update the amping between a 'genrlaized vertex-name' vertex_norm and the 'sepcific' vertex_syn 'searchalbeæ' vertex-name
bool setMappingFor_tail__s_db_ds_directMapping_t(s_db_ds_directMapping_t *self, const uint vertex_norm, const uint vertex_syn);

//! Search for a set of nodes using a tree-traversal procedure (oekseth, 06. mar. 2017).
bool searchFor__nestedSet__s_db_ds_directMapping_t(const s_db_ds_directMapping_t *self, s_db_searchResult_rel_t *searchResult, const bool config__isTo_preCompute_preSearchConditions);

//! @return a 'randomly seløected' node (oekseth, 06. mar. 2017)
static s_db_rel_t findRelAtRandom__s_db_ds_directMapping(const s_db_ds_directMapping_t *self) {
  assert(self);
  const uint cnt_thresh = 100;
  for(uint i = 0; i < cnt_thresh; i++) {
    s_db_rel_t relation_search = __M__initWildCard__s_db_rel_t();
    //! Idenitfy the complete set of relationshisp matching a search-citeria:  
    const uint head_id = ((i+1) != cnt_thresh) ? rand() % self->cnt_head : 0; //! ie, by 'defualt' evlauate the first investigated vertex.
    relation_search.head = head_id;
    assert(head_id < self->cnt_head);
    s_kt_set_1dsparse_t result_set; allocate__simple__s_kt_set_1dsparse_t(&result_set, /*ncols=*/1000); 
    uint cnt_interesting = 0;
    findRelation__cntInteresting__s_db_ds_directMapping_t(self, relation_search, &cnt_interesting, /*namesAlreadyMappedIntoSynonyms=*/true, &result_set, /*isToStore__verticesInResultStack=*/true);
    uint arrOf_relationIds_size = 0;
    const uint *arrOf_relationIds = get_sparseRow_ofIds__upTo_currentPos__kt_set_1dsparse(&result_set, &arrOf_relationIds_size);
    //printf("\t\t [recusriveStep]\t cntMatches=%u, given our cntSearchTripletes=%u, at %s:%d\n", arrOf_relationIds_size, arrOf_relationIds_size, __FILE__, __LINE__);
    s_db_rel_t relation_result = __M__initWildCard__s_db_rel_t();
    if(arrOf_relationIds_size > 0) {
      const uint pos = rand() % arrOf_relationIds_size;
      assert(pos < arrOf_relationIds_size);
      const uint tail_id = arrOf_relationIds[pos];
      assert(tail_id < self->cnt_head);
      //!
      //! Update the relation:
      relation_result.tailRel.tail = tail_id;
      relation_result.head = tail_id;
      
    }
    //! De-allocate:
    free_s_kt_set_1dsparse_t(&result_set);
    
    if(arrOf_relationIds_size > 0) {
      return relation_result;
    }
  }
  assert(false); // TODO: cosnider removing 'this'.
  return __M__initWildCard__s_db_rel_t(); //! ie, a 'fall-back'
}

#endif //! EOF
