#ifndef kt_distance_h
#define kt_distance_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_distance
   @brief the main access point for the distnac-e-comptautions
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/


#include "e_kt_correlationFunction.h"
#include "config_nonRank_each.h"
#include "config_nonRank_oneToMany.h"
#include "config_nonRank_manyToMany.h"
#include "correlationType_kendall_partialPreCompute_kendall.h"
#include "parse_main.h"
#include "kt_metric_aux.h"
#include "kt_list_1d.h"

/** 
    @brief A function to map the old lgoics of naive cluster-anlasysis-implemtnation in the "cluster.c" library with Knitting-Tools HPOC software for cluster-analysis (oekseth, 06. okt. 2016).
    @param<dist> is the char-idnetifer to be used, ie, as specified in the cluster.c software
    @param<scalar_metric_id> is the metirc to be used in speidficaiton of the distance-matrix.
    @param<scalar_typeOf_correlationPreStep> is the pre-prcssing step to be used wrt. comptuation of distnace-metrics.
**/
void get_enumParams_fromBackCompaitbleConfigFor_clusterC_distanceSpec(const char dist, e_kt_correlationFunction_t *scalar_metric_id,  e_kt_categoryOf_correaltionPreStep_t *scalar_typeOf_correlationPreStep);



#include "correlationFunc_optimal_compute_allAgainstAll_SIMD.h"

//! @return a poiner to an optimized two-row- dstaicne-comparison-function.
t_float(*setmetric__correlationComparison__each__1d(const e_kt_correlationFunction_t typeOf_metric, const t_float *weight, char* mask1, char* mask2, const bool needTo_useMask_evaluation)) 
(const config_nonRank_each_t row_config);
t_float(*setmetric__correlationComparison__each(const e_kt_correlationFunction_t typeOf_metric, const t_float *weight, char **mask1, char **mask2, const bool needTo_useMask_evaluation)) 
(const config_nonRank_each_t row_config);
//(const uint, t_float *__restrict__, t_float *__restrict__, const t_float *__restrict__, char *__restrict__, char *__restrict__, const s_allAgainstAll_config_t);
//! @return a poiner to an optimized one-to-all dstaicne-comparison-function.
void(*setmetric__correlationComparison__oneToMany(const e_kt_correlationFunction_t typeOf_metric, const t_float *weight, char **mask1, char **mask2, const bool needTo_useMask_evaluation)) 
(const config_nonRank_oneToMany_t obj_config);
//(const uint, const uint, const uint, t_float **, t_float **,  char** , char** , const t_float [], const e_kt_correlationFunction_t , const e_typeOf_metric_correlation_pearson_t, s_allAgainstAll_config *, t_float *);

//! @return a poiner to an optimized one-to-all dstaicne-comparison-function.
void(*setmetric__correlationComparison__oneToMany_maskType__uint(const e_kt_correlationFunction_t typeOf_metric, const t_float *weight, uint **mask1, uint **mask2, const bool needTo_useMask_evaluation))  
(const config_nonRank_oneToMany_t obj_config);
//(const uint, const uint, const uint, t_float **, t_float **,  char** , char** , const t_float [], const e_kt_correlationFunction_t , const e_typeOf_metric_correlation_pearson_t, s_allAgainstAll_config *, t_float *);

//! @return a poiner to an optimized all-against-all dstaicne-comparison-function.
void(*setmetric__correlationComparison__manyToMany(const e_kt_correlationFunction_t typeOf_metric, const t_float *weight, char **mask1, char **mask2, const bool needTo_useMask_evaluation)) 
(const config_nonRank_manyToMany_t obj_config);
//(const uint nrows, const uint ncols, t_float **, t_float **,  char**, char**, const t_float[], t_float **, const e_kt_correlationFunction_t, const e_typeOf_metric_correlation_pearson_t, s_allAgainstAll_config *);

//#include "s_kt_correlationConfig_allAgainstAll.h"

/* /\** */
/*    @struct s_ktDistanceConfig */
/*    @brief the cofniguration for the all-against-all correlation-matrix-construction (oekseth, 06. setp. 2016). */
/*  **\/ */
/* typedef struct s_ktDistanceConfig_allAgainstAll { */
/*   t_float *weight; */
/*   char** mask1;  */
/*   char** mask2; */
/*   uint transposed; */
/*   // uint iterationIndex_2; */
/*   s_allAgainstAll_config_t config; */
/* } s_ktDistanceConfig_allAgainstAll_t; */

/* //! SEt the "s_ktDistanceConfig_allAgainstAll_t" object to empty, ie, intiaite. */
/* static void setTo_empty__s_ktDistanceConfig_allAgainstAll(s_ktDistanceConfig_allAgainstAll_t *self) { */
/*   assert(self); */
/*   self->weight = NULL; */
/*   //self->resultMatrix = NULL; */

/*   //self->iterationIndex_2 = UINT_MAX; */
/*   self->config = get_init_struct_s_allAgainstAll_config(); //! ie, initatie. */
/* } */

/**
   @brief compute the distance-matrix usign our optmized distance-comptuation routines (oekseth, 06. sept. 2016).
   @param <metric_id> is the correlation-metric to compute for.
   @param <isTo_useSpearman_rankPreComputation> which if set to true implies that we use Spearmans rank-correlation before computing the non-ranked correlation-metrics
   @param <nrows> is the number of rows in both matrices
   @param <ncols> is the number of columns in both matrices
   @param <data1> is the amtrix to comarpe with the second matrix
   @param <data2> is the secodn matrix to compare with the first matrix: may be the same as the first matrix
   @param <resultMatrix> is the default result-container to be used.
   @param <self> configues the comparison-operation.
   @param <obj_resultsFromTiling> which is to be set if MPI-processing si to be sued: for this case the correlation-emtrics are Not applied/adjsuted wrt. their 'partial sums'.
 **/
void kt_compute_allAgainstAll_distanceMetric(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1, t_float **data2, t_float **resultMatrix, s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *obj_resultsFromTiling);

//! A wrapper to the "kt_compute_allAgainstAll_distanceMetric(..)" used to avodi the ned to firs tiniate the configuration-strucutre 'before making the call' (oekseth, 06. sept. 2016).
void kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1, t_float **data2, t_float *weight, t_float **resultMatrix, char** mask1, char** mask2, uint transposed, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_cmp_masksAre_used_t  masksAre_used, s_kt_computeTile_subResults_t *obj_resultsFromTiling, s_kt_list_2d_kvPair_filterConfig_t *optional_sparseDataStorage);
//! A modifciaotn of the "kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(..)" which use "uint" data-type instead of "char" data-type for copmtuation of mask-porperties.
void kt_compute_allAgainstAll_distanceMetric__extensiveInputParams_maskType__uint(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1, t_float **data2, t_float *weight, t_float **resultMatrix, uint** mask1, uint** mask2, uint transposed, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_cmp_masksAre_used_t  masksAre_used, s_kt_computeTile_subResults_t *obj_resultsFromTiling, s_kt_list_2d_kvPair_filterConfig_t *optional_sparseDataStorage);
//void kt_compute_allAgainstAll_distanceMetric__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1, t_float **data2, t_float *weight, t_float **resultMatrix, char** mask1, char** mask2, uint transposed, const bool isTo_invertMatrix_transposed = true, const uint CLS = 64, const bool isTo_use_continousSTripsOf_memory = true, const uint iterationIndex_2 = UINT_MAX, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef, void *s_inlinePostProcess = NULL, const e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_undef);

/**
   @brief apply accorelation-metrics to tow rows
   @param <metric_id> is the correlation-metric to compute for.
   @param <typeOf_correlationPreStep> which if set to true implies that we use Spearmans rank-correlation before computing the non-ranked correlation-metrics
   @param <nrows> is the number of rows in the input-matrices.
   @param <ncols> is the number of columns (opr rows if "transposed == 1" is used.
   @param <data11> is the first row to evalaute
   @param <data22> is the second row to evaluate
   @param <index1> refers to the first row to evaluate
   @param <self> configures the exeuction.
 **/
void kt_compare__oneToMany(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, const uint index1, s_allAgainstAll_config_t *self, s_correlationType_kendall_partialPreCompute_kendall_t *optionalStore_kendall, t_float *arrOf_result, void (*metric_oneToMany_nonRank) (config_nonRank_oneToMany_t) );

/**
   @brief apply accorelation-metrics to tow rows
   @param <metric_id> is the correlation-metric to compute for.
   @param <isTo_useSpearman_rankPreComputation> which if set to true implies that we use Spearmans rank-correlation before computing the non-ranked correlation-metrics
   @param <ncols> is the number of columns (opr rows if "transposed == 1" is used.
   @param <data11> is the first row to evalaute
   @param <data22> is the second row to evaluate
   @param <index1> refers to the first row to evaluate
   @param <index2> refers to the second row to evaluate
   @param <self> configures the exeuction.
   @param <metric_each_nonRank> which if set refers to the non-ranked metric to be used, eg, in combiation for a separmn-rank distance-pcrorrelation metric.
 **/
t_float kt_compare__each(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1, t_float **data2, const uint index1, const uint index2,  s_allAgainstAll_config_t self, t_float (*metric_each_nonRank)(const config_nonRank_each_t), s_correlationType_kendall_partialPreCompute_kendall_t *optionalStore_kendall) ;
//! A simple permtuation of "kt_compare__each(..)" (oekseth, 06. des. 2016).
t_float kt_compare__each__maskImplicit(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1, t_float **data2, const uint index1, const uint index2) ;
//! A simple permtuation of "kt_compare__each__maskImplicit(..)" where we 'allow' a suer to use a 'row pointer' as input (ie, instead of a matrix) (oekseth, 06. des. 2016).
t_float kt_compare__each__maskImplicit__useRowPointersAsInput(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint ncols, t_float *row_1, t_float *row_2);
// void compute_allAgainstAll_distanceMetric(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_kt_correlationConfig_allAgainstAll *config_allAgainstAll);

//! A warpper call to compute correlation-metrics for extensive valeu-pareamters:
//! @return teh computed distance.
t_float kt_compare__each__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char **mask1, char **mask2, t_float *weight, const uint index1, const uint index2, const bool transpose, const bool isTo_useImplictMask, t_float (*metric_each_nonRank)(const config_nonRank_each_t), s_correlationType_kendall_partialPreCompute_kendall_t *optionalStore_kendall);


//! Post-process the reuslts of unfieid set of paritally evlauted chunks wrt. correlation.
void apply_correlationMPI_postProcess(const e_kt_correlationFunction_t typeOf_metric, const uint nrows_to_evaluate, const uint nrows_outer_to_evaluate, const t_float *rres_startPos, s_kt_computeTile_subResults_t *complexResult, t_float **resultMatrix, s_allAgainstAll_config_t self);

#endif //! EOF
