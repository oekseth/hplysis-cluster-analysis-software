#ifndef db_searchNode_rel_h
#define db_searchNode_rel_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file 
   @brief provide lgoics for 'basic' semantic searche s(oekseth, 06. mar. 2017).
 **/

#include "db_rel.h"
#include "kt_list_1d.h"

//! Identify the vertices to be sued 'in the next search':
#define MF__resultsToUSe__afterSearch__s_db_searchNode_rel(rel) ({searchId_head = UINT_MAX; searchId_tail = UINT_MAX; const bool isW_h = __M__isWildCart__s_db_rel_t(rel.head); const bool isW_t = __M__isWildCart__s_db_rel_t(rel.tailRel.tail); if( (isW_h && isW_t) || (!isW_h && !isW_t)) {searchId_head = rel.head; searchId_tail = rel.tailRel.tail;} else if(isW_h) {searchId_head = rel.head; } else {searchId_tail = rel.tailRel.tail;}})

/**
   @struct s_db_searchNode_rel
   @brief a strucutre to provide lgocis for semantic data-searches (oekseth, 06. mar. 2017)
   @remarks why 'our search-based appraoch' provides higher perofrmance than a b-tree-appraoch for 'vertex-based searches'. 
   -- Result: a new database-approach for high-perofmrance exueciton of semantic data-searches, an appraoch which is stragith-forward to implement/understand: a proof-of-concpet (of our appraoch) is found/incldued in our "db_ds_directMapping.h".  Brief: in our appraoch we manage to achive a sigciant higher utilizaiotn of memory-cache (ie, reduced number fo cache-misses) when compared to ordianry/defulat database-appraoches (scuh as B-trees, ie, where our appraoch results in improved/hibher utalizaiton of memory-cache). To understand/realize why our approach outperforms established appraoches, it is of imnprotance of idneitfy the reasons for why semantic searches becomes un-acceptable slo (to return a 'completed' answer). 
   -- In brief, the main-performanc-ebottlenecsk cosenrsn cases where database is: (a) complex and large (eg, a number of predicates and nodes), (b) 'furcating' snames' (ie, where muliple naems needs to be inveeistgated ue to synonym-relationships), (c) 'implict' graph-based queries (eg, query=(head, rt, (.... (rt, tail))), where a large search-space need to be traversed iot. idnetify the search-results). In order to overcome the latter issues we have focused on improved utlizaiton of comptuer-hardware: thoguh list-based data-access and data-normalization to redcue/compress the memory-cache-search-space. The implicaiotn (of our appraoch) is a sigicant peformance-boost, a perofmrance-boost which is both due to 're-indexing' of data' and the 'data-access' itself. The 're-indexing' of data (applied in our data-loading-phase, and then used as pre-step to each data-search) results in a sigicncant 'performance-contributor': in the latter we use distjoint-forests among synonym-relationships to comptue a 'normilized fomr' of the input-data. A different aspect consersn our list-based data-access: to understand why a 'list-based data-access' manages to imrpove search-performance, it is of itnerst to study/examplify important/common search-pattersn, eg, wrt. wild-card-searches: to search for relationships/triplets where more than 'one uniuqe' sollution exists, a use-case which (for several cases) results in muliple matches (ie, muliple meory-accesses). To improve memory-cache-utilizaiton we (instead of a tree-based data-traversal) iterate through a 'single list of values' (ie, a simple 'for-lopp'). When comparing the time-cost of a tree-based 'data-comparison' (to a 'list-based data-comparison') the tree-based iteraiton clearly results in higher instruction-cache-miss-rate and memory-cache-miss-rate. 
   -- Analysis(overall). Our latter list-based appraoch provides muliple perofmrance-benefits: benefit(a) simplcitly in code-fetch-instructiosn (where a recusive loop or a stack-based appraoch provides higher isntruction-compelxity when compared to a simple traversla through a list); benefit(b) predicable memory-access (ie, where a compariler may correctly 'designate' memory-prefetch-instructions); benefit(c) 're-use' of earlier emmory-accesses (ie, where 'earlier computations' will be found in memroy-cache, eg, for the use-case where a user search for a (head, [rt1, rt2, ...], tail) relationship). To summarize we (in our appraoch) have replaced 'implcit synony-handling' and 'tree-traversxsal' with 'explcit synonym-handling' and 'list-iteration', ie, to use/apply common software-appraoches as the 'core' of a sematnic database-engine. 
   -- Conclusion. From the latter we observe how our approach may be widely used in the research-community:     our appraoch is both stragith-forward to implement (eg, as illsutrated/examplfied in our repostiry), and provide aisgicnat performance-boosts (eg, when compraed to "B-tree" data-base-strucutre-appraoches). 
   @remarks how our performance-setup is desgined to capture muliple/different cominbaitons of 'implcit data-searches'. 
   -- Overall: provide a 'deifntion' to 'what' and 'how' to search through a network (eg, to 'know' if we are to 'call' a recursive seearch for a nodes children). In the 'basic interpretaiton' of thsi strucutre we observe a list of heads, precicates/RTs and tails 'to search'. To 'know' ow the search si to change 'for each new relation-case we ivnestgiate' we incldue/incorpraote cofniguraiton-'knowlede' of 'the sbutitation-strateg for each recusrive fuctnion-search-call' (eg, as seen wrt. our "isTo__ifMatch__preSelectionCriteria" and our "isTo__replace__head").
   -- Idea(idneitfy children): descirbe a search-appraoch to find diameter for a vertex (eg, to use a recursive search): to simplify our search-logics we 'by default' selects the max-distance based on the return-value. To avoid muliple 'investigations for the same node' we use/add a mapping-table (to 'avoid' 'multiple calls to expand a given vertex'). In our searhc-logic we suppor the use of 'wildcard-node' (iue, to 'match all'). In cocnrete use-case-scantiors 'one is only interestied in expenading nodes matchign certian cirtieras' (eg, 'for nodes classifeid as proteisn', ie, wher ehte latter examplifes/implies the need for 'semantic searhc-lgocis'), ie, hence the itnerest/need for different 'search-cofnigruatiosn' wrt. data-searches. To examplify the latter, a use-case is to 'support'/enable a 'fixed' search-codniton (eg, to 'only include for head-->tail' if there exists a predicate=search 'connecting' head--->tail), ie, as exmpalfied in/through our "isTo__ifMatch__preSelectionCriteria" boolean search-critiera'.
   -- Use-case(search-diamter): to idneitfy the max-path-length/distance between nodes. To understand the improtance/applcialbity of the latter, note that the 'search-dieamter' rpvodie indicaitons/clues of 'number of searches which will be perofrmed for real-life queries'. A straight-forard search-based use-case is to find all cases which match ehter (a) "(head, [rt], .... *)" and "(*...., [rt], tail)" (ie, to search for muliple 'implicit' triplets, eg, to idneityfin the cdadate-genes based on a max-cuasaiton-legnth distance). A more complex/elaborate example-use-case conserns the search for the max-reuglation-distance between [proteins, genes, enyzmes]: for the latter use-case we first perform a non-recursive invesgtitate/search for relationships assicating 'either the head or tail' to one of the latter codntions/requirements, and then 'if the latter matches' we perofrm a 'recursive investgiation/search' using the 'inspect-in-dpeth' search-clause
   @remarks implict logics through our "db_inputData_matrix.h":
   -- overall(applicaiton and time-cost): our "db_inputData_matrix.h" provide supprot for data-normalizaiton of synonyms (ie, if the synonym-fiel is defined): we observe that oru data-pre-normaliziaotn-stpe has an ingisifncat/unseeable exueciton-time-overhead, ie, as seen/examplfied through different congiratuions 'made' when calling our "tut_db_useCase__1_normalizeData.c". 
   -- API(usage): 'call' a 'constructor' using "db_inputData_matrix" as 'input': among others provide supprot for data-normalizaiton of synonyms (ie, if the synonym-fiel is defined). 
   -- use-case(usage): provides/examplifes a normlization-strategy for data-sets: produce a mapping-table based on a forest of synonyms, and then (a) write out this mapping-table, (b) noramlizes an input-matrix, and (c) examplfies how latter may be used to update+access our "db_ds_directMapping.h" (eg, wrt. b-tree-insertiosn) .... this mapping-table  ... Take 'as input' two different matrices: (a) 'synonym-relations' and (b) 'ordinary' SKB-relations ... ... API: we use our "get_centralityVertex_inDisjointforest__s_kt_forest_findDisjoint(..)" ("kt_forest_findDisjoint.h") to 'build' a mapping-table (or: a synonym-normalizaiton-list), ie, for which our nromalizaton-appraoch becomes stragith-forward. In order to convert Knitting-Toosl datatabase into a lsit of indceses we use the "data/parse__skbTriplets__intoTsvMatrix.pl" Perl xscript.
   @remarks Seleciton-strategy (when searching for 'amtching'v ertices):
   -- overall: we 'know' if we are to update wrt. the head-vertex or tail-vertex by inveseitgating the 'wild-card': selectCase(a) if a 'wild-card' is used for the 'tail' then we assume that tail-id is of 'specific' interst/use. A more comples isuse/case consens selectCase(b), where 'wild-card' is set for both (for which we update both vertices), and simliarlty for selectCase(c) where 'no wild-card' is set' (for which we update both vertices). 
   -- implementaiton: among others seen in our "db_ds_directMapping.c", where 'interesting lgocis' is found wrt. our "MF__resultsToUSe__afterSearch__s_db_searchNode_rel(..)"
 **/
typedef struct s_db_searchNode_rel {
  //! Generic search-containers:
  s_kt_list_1d_uint_t listOf_heads;
  s_kt_list_1d_uint_t listOf_predicates;
  s_kt_list_1d_uint_t listOf_tails;
  //! -------------------------------
  //! Search-expansion-logics:
  bool isTo__ifMatch__preSelectionCriteria; //! which is used to 'descide' if 'this node' is of interest: if false then: (a) 'the node is Not expanded wrt. its children' and (b) distance=T_FLOAT_MAX is returned; else 'the seleciotn-ctitiera' is used to expand the given node.
  /* bool isTo__ifMatch__; */
  //! -------------------------------
  //! 'Descides' how to cosntruct the search-terms to investigate/search
  bool isTo__replace__head;
  bool isTo__replace__rt;
  bool isTo__replace__tail;
  // bool isTo__;
  //! -------------------------------
  //! 
} s_db_searchNode_rel_t;

//! Inititates the "s_db_searchNode_rel_t" object.
s_db_searchNode_rel_t init__s_db_searchNode_rel_t(uint searchCnt__heads, uint searchCnt__predicates, uint searchCnt__tails, const bool isTo__ifMatch__preSelectionCriteria, const bool isTo_allocteMem);
//! @return an 'empty verisonæ fo the "s_db_searchNode_rel_t" object
static s_db_searchNode_rel_t setToEmpty__s_db_searchNode_rel_t() {
  return init__s_db_searchNode_rel_t(0, 0, 0, true, /*isTo_allocteMem=*/false);
}
//! @return a search-config-struct which 'matches all cases'.
static s_db_searchNode_rel_t init__matchAll__s_db_searchNode_rel_t(uint searchCnt__heads, uint searchCnt__predicates, uint searchCnt__tails, const bool isTo__ifMatch__preSelectionCriteria) {
  return init__s_db_searchNode_rel_t(0, 0, 0, false, /*isTo_allocteMem=*/true);
}
//! Inititates the "s_db_searchNode_rel_t" object.
s_db_searchNode_rel_t init__mulitpleVertices__s_db_searchNode_rel_t(const s_kt_list_1d_uint_t *listOf_heads, const s_kt_list_1d_uint_t *listOf_predicates, const s_kt_list_1d_uint_t *listOf_tails, const bool isTo__ifMatch__preSelectionCriteria);
//! De-allocates the "s_db_searchNode_rel_t" object.
void free__s_db_searchNode_rel_t(s_db_searchNode_rel_t *self);
/**
   @brief consturct a set of searhc-triplets based/for the 'current' searhc-vertex (oesketh, 06. mar. 2017).
   @return a lsit of triplets 'to be searched for'.
 **/
s_db_rel_t *getSearchNodes__s_db_searchNode_rel_t(const s_db_searchNode_rel_t *self, const uint vertex_id, uint *scalar_retSize, const bool isTo__applyRecursiveCall);

/**
   @enum e_db_searchNode_rel__sampleUseCases
   @brief provide a set of sytneitc use-cases (oekseth, 06. mar. 2017)
   @remarks used to simplify inveistgaiton of time-cost for different data-seach-patterns.
 **/
typedef enum e_db_searchNode_rel__sampleUseCases {
  //! --------------------------------------
  //! Use recursive-search-strategies:
  e_db_searchNode_rel__sampleUseCases_wildCard_isTail,
  e_db_searchNode_rel__sampleUseCases_wildCard_isHead,
  //! --------------------------------------
  //! Combine 'resuviese search' with pre-search-strategies:
  e_db_searchNode_rel__sampleUseCases_wildCard_isTail_preSearch_rt,
  e_db_searchNode_rel__sampleUseCases_wildCard_isTail_preSearch_rtAndTail,
  /* e_db_searchNode_rel__sampleUseCases_wildCard_isTail_preSearch_tail, */
  //! --------------------------------------
  //! Use pre-search-strategies:
  //e_db_searchNode_rel__sampleUseCases_preSearch_rt,
  e_db_searchNode_rel__sampleUseCases_preSearch_rtAndTail,
  /* e_db_searchNode_rel__sampleUseCases_wildCard_isHead, */
  /* e_db_searchNode_rel__sampleUseCases_, */
  /* e_db_searchNode_rel__sampleUseCases_, */
  //! --------------------------------------------------
  e_db_searchNode_rel__sampleUseCases_undef
} e_db_searchNode_rel__sampleUseCases_t;
/**
   @brief construct to new search-objects for different evlauation-test-cases (oekseth, 06. mar. 2017). 
 **/
void buildSampleCases__s_db_searchNode_rel_t(const e_db_searchNode_rel__sampleUseCases_t enum_id, const s_kt_list_1d_uint_t *listOf_heads, const s_kt_list_1d_uint_t *listOf_predicates, const s_kt_list_1d_uint_t *listOf_tails,  const s_kt_list_1d_uint_t *listOf_preSelection_predictes,  const s_kt_list_1d_uint_t *listOf_preSelection_tails, s_db_searchNode_rel_t *retObj_filterBeforeRecursion, bool *scalar_retObj_filterBeforeRecursion_isToUse, s_db_searchNode_rel_t *retObj_inRecursionTraversal, bool *scalar_retObj_inRecursionTraversal_isToUse);

#endif //! EOF
