#include "assert_intri.h"

//! test simple/basis operations:
//! @remarks examplify and tests the correctness and 'implications' of using "_mm_and_ps(..)" to write "if ... else ... " clauses.
static void  __assert_simpleOperations() {
  // FIXME[article]: include 'this' in a future articel describing intristinc templates .... and 'make' this example a 'core' in an intrisntictc itneouction to NAN and INFINITY learning cases/issues, eg, wrt "INFINITY + NAN = NAN"
  //! --------------------------------------------------
  //! --------------------------------------------------
  { //! 'Atomic' examples:
    { //! A simple example to test "f = _mm_cvtss_f32(a);"
      VECTOR_FLOAT_TYPE vec_term1 = VECTOR_FLOAT_SET(0, 1, 2, 3);
      // FIXME: try to describe a cocnrete application of [below] function.
      const t_float result_scalar = _mm_cvtss_f32(vec_term1);
      //printf("%f\n", result_scalar);
      assert(result_scalar == 3); //! ie, as we expect the last tiem 'to be fetched'.
    }
    { //! Examplify: "_mm_max_ss(..)"
      VECTOR_FLOAT_TYPE vec_term1 = VECTOR_FLOAT_SET(0, 1, 2, 3); 
      VECTOR_FLOAT_TYPE vec_term2 = VECTOR_FLOAT_SET(1, 2, 3, 4);
      // FIXME: try to describe a cocnrete application of [below] function.
      VECTOR_FLOAT_TYPE vec_result = _mm_max_ss(vec_term1, vec_term2); //! ie, use the frist three values from vec_term1, and then identify the max-values between "vec_term2, vec_term1"
      VECTOR_FLOAT_PRINT(vec_result); //! where result is: "[vec_term1[0..3), max(vec_term1[3], vec_term2[3])]"
    }
  }
  //! --------------------------------------------------
  { //! Test effects of 'divide by zero' cases
    __m128 vec_result  = _mm_div_ps(_mm_set_ps(1, 2, 0, 0), _mm_set_ps(1, 0, 3, 0)); 
    VECTOR_FLOAT_PRINT(vec_result); //! ie, print out the vector, where result in inverse order is: 0/0=-nan, 0/3=0.0, 2/0=inf, 1/1=1.0
    vec_result = _mm_div_ps(vec_result, _mm_set_ps(2, 2, 2, 2));
    VECTOR_FLOAT_PRINT(vec_result); //! ie, print out the vector, where result in inverse order is: ...
    
    
    
    if(true) {printf("default constants: nan=%f, infintite=%f, (nan+1)=%f, (infinity+nan)=%f, (nan*nan)=%f, at %s:%d\n--\n", NAN, INFINITY, NAN+1, INFINITY+NAN, NAN*NAN, __FILE__, __LINE__);} //! a print-out which examplieis the reulst of divindign by and on "0".


    if(false) {//! then we examplify how if-sentences may be computed through the "and" logical operaiton, an 'operand' which may be sued in a "vec_result = _mm_add_ps(_mm_mul_ps(vec_input, _mm_and_ps(..)), _mm_mul_ps(vec_input, _mm_andnot_ps(..)))" 'tempalte-call'.
      VECTOR_FLOAT_PRINT(_mm_and_ps(_mm_set_ps(0, 1, 1, 1), _mm_set_ps(1, 0, 1, 1))); //! result-inverse-order: 1, 1, 0, 0
      VECTOR_FLOAT_PRINT(_mm_and_ps(_mm_set_ps(3, 1, 1, 1), _mm_set_ps(1, 0, 1, 1))); //! result-inverse-order: 1, 1, 0, 0
    }


    if(true) { //! Test conversion:


      { //! Convert from int to float:
	VECTOR_INT_TYPE vec_term1 = VECTOR_INT_SET(-1, 0, 2, 20);
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_convertFrom_INT(vec_term1);
	t_float result[VECTOR_FLOAT_ITER_SIZE]; VECTOR_FLOAT_STORE(result, vec_result);
	VECTOR_FLOAT_PRINT(vec_result);	
	assert(result[0] == -1);
	assert(result[1] == 0);
	assert(result[2] == 2);
	assert(result[3] == 20);
      }
      { //! Convert from uint to float:
	uint data1[32] = {0, 20, 50, 60};
	VECTOR_UINT_TYPE vec_term1 = VECTOR_UINT_LOAD(data1);
	VECTOR_UINT_PRINT(vec_term1);
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_convertFrom_UINT(vec_term1);
	t_float result[VECTOR_FLOAT_ITER_SIZE]; VECTOR_FLOAT_STORE(result, VECTOR_FLOAT_revertOrder(vec_result));
	VECTOR_FLOAT_PRINT(vec_result);	
	for(uint i = 0; i < 4; i++) {
	  printf("compare[%u]: %u VS %u, at %s:%d\n", i, (uint)result[i], data1[i], __FILE__, __LINE__);
	  assert((uint)result[i] == data1[i]);
	}
      }

      { //! Convert from float to char
	//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array

	__m128 vec_cmp_1 = _mm_set_ps(0, 1, 2, 3);
	__m64 vec_tmp_1 = _mm_cvtps_pi8(vec_cmp_1);	
	//__m128 tmp = VECTOR_FLOAT_convertFrom_CHAR(vec_tmp_1); VECTOR_FLOAT_PRINT(tmp);
	VECTOR_CHAR_PRINT(vec_tmp_1); //! result: [3, 2, 1, 0], ie, result in reverse order.
	
	//! And then use the macro-call, ie, to 're-produce' [ªbov€]:
	VECTOR_CHAR_PRINT(VECTOR_CHAR_convertFrom_float_vector(vec_cmp_1));
      }


      { //! Convert from "char" to "float"
	//! Note: demonstrate how an 8-bit mask-arary may be loaded into a flatoign-potin-array.
	const uint i = 0; //! which in pracise would be an index inside a for-loop.

	//__m128 a = _mm_set1_ps(2);

	//! Step(1): examplify how a char-vector may be converted into a float-vector:
	__m128 vec_cmp_1 = _mm_set_ps(0, 1, 2, 3);
	__m64 vec_tmp_1 = _mm_cvtps_pi8(vec_cmp_1);	
	VECTOR_FLOAT_PRINT(_mm_cvtpi8_ps(vec_tmp_1)); //! result: vec[0] = -nan, vec[1] = -nan, vec[2] = 0.000000, vec[3] = 0.000000, 
	//! -------------------------------------------------------------------------
	//! Step(2): examplify how a char-array may be converted into a float-vector:
	char buf[100] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 15};
	__m64 vec_char = *(__m64*)&buf[0]; // &buf[i]; // assume 8-byte aligned
	VECTOR_FLOAT_PRINT(_mm_cvtpi8_ps(vec_char)); //! result: vec[0] = -nan, vec[1] = -nan, vec[2] = 0.000000, vec[3] = 0.000000, 
      }

    }

    //! --------------------------------------------------

    { //! Validate "VECTOR_FLOAT_dataMask_pair(..)":
      //! ------------
      float data1[100] = {FLT_MAX, 2, 50, 40};       float data2[100] = {FLT_MAX, FLT_MAX, 1, 2};
      __m128 vec_mul = _mm_set_ps(1, 2, 3, 4); __m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);  __m128 vec_term2 = VECTOR_FLOAT_LOAD(data2);

      const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1); 
      __m128 vec_cmp_1 = _mm_cmplt_ps(vec_term1, vec_empty_empty);        vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones);	
      // VECTOR_FLOAT_PRINT(vec_cmp_1);
      __m128 vec_cmp_2 = _mm_cmplt_ps(vec_term2, vec_empty_empty);        vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones);		
      // TODO[article]: cosnider as an exierside to examplify the 'effect' of using "_mm_or_ps(..)" instead of "_mm_and_ps(..)"
      __m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2);  
      //! 'Clear' the NAN values:
      //mask = _mm_and_ps(mask, vec_empty_ones); 
      //mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); 
      //mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); 
      //
      // //! --------
      // FIXME: update our assicated macro-function wrt. [”elow] order of the "_mm_shuffle_ps(..)"
      __m128 vec_result = _mm_mul_ps(mask, vec_mul);
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); 
      VECTOR_FLOAT_PRINT(vec_result);
      { //! Compare the results of this with the macro-varialbe:
	__m128 vec_result_cmp = VECTOR_FLOAT_dataMask_pair(vec_mul, vec_term1, vec_term2);
	float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result);
	float result_cmp[4] = {0, 0, 0, 0}; _mm_store_ps(result_cmp, vec_result_cmp);
	for(uint i = 0; i < 4; i++) {
	  assert(result[i] == result_cmp[i]);
	}
      }
    }
    
    if(true) { //! Logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
      char data1[100] = {1, 2, 30, 40};       char data2[100] = {0, 1, 1, 2};
      __m128 vec_mul = _mm_set_ps(1, 2, 3000, 400);  __m128 vec_term1 = VECTOR_FLOAT_convertFrom_CHAR_buffer(data1);  __m128 vec_term2 = VECTOR_FLOAT_convertFrom_CHAR_buffer(data2);
      //! ------------
      const __m128 vec_empty_empty = _mm_set1_ps(0); 		  const __m128 vec_empty_ones = _mm_set1_ps(1); 
      if(true) {
	//! Note: Examplifeis how we for "SIMD3" (which does nto supprot "_mm_blendv(..)") may handle/support the 'divide-by-zero' cases using a 'conceptual' if(a[i] && b[i]) { /* then some action, ie, a set of instructions */ }
	//! ---------------


	__m128 mask;


	// FIXME: try out: __m64 _m_pcmpgtb (__m64 a, __m64 b)

	//! Note: when "_mm_cmpgt_pi8(..)" is compared to "_mm_cmpgt_ps(..)" the first 'gives' a '-1' while the latter a "-nan". In our appraoch we 'handle' the "-1" by muliplying the result with tiself, ie, "mask = _mm_mul_ps(mask, mask);"
	__m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_cvtps_pi8(vec_term1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_cvtps_pi8(vec_term2), _mm_set1_pi8(0));
	// VECTOR_CHAR_PRINT(vec_cmp_1);  VECTOR_CHAR_PRINT(vec_cmp_2); printf("--\n");

	mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));
	// FIXME[jc]: si there a better approach/strategy than [below]?
	mask = _mm_mul_ps(mask, mask); //! ie, to 'handle' the "-1" signs.
      } else {
	// float data1[100] = {FLT_MAX, 2, 50, 40};       float data2[100] = {FLT_MAX, FLT_MAX, 1, 2};
	// __m128 vec_mul = _mm_set_ps(1, 2, 3, 4); __m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);  __m128 vec_term2 = VECTOR_FLOAT_LOAD(data2);

	// const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1); 
	__m128 vec_cmp_1 = _mm_cmpgt_ps(vec_term1, vec_empty_empty); 
	__m128 vec_cmp_2 = _mm_cmpgt_ps(vec_term2, vec_empty_empty); 

	vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones);	
	// VECTOR_FLOAT_PRINT(vec_cmp_1);
	vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones);		
	
	// TODO[article]: cosnider as an exierside to examplify the 'effect' of using "_mm_or_ps(..)" instead of "_mm_and_ps(..)"
	__m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2);  
	vec_result = _mm_mul_ps(mask, vec_mul);
	//! 'Clear' the NAN values:
	//mask = _mm_and_ps(mask, vec_empty_ones); 
	//mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); 
	//mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); 
      }
      //
      // //! --------
      // FIXME: update our assicated macro-function wrt. [”elow] order of the "_mm_shuffle_ps(..)"

      //      __m128 vec_result = _mm_div_ps(mask, vec_mul);
      // FIXME: use [below] to write a new itnrsitnictc fucniton wrt. "div" for valeus which may be '0' (and where the latter is to be ingored).
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); 
      VECTOR_FLOAT_PRINT(vec_result);


      {
	//float mul[4] = {1, 2, 3000, 400}; __m128 vec_mul = VECTOR_FLOAT_LOAD(mul); 
	//__m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);  __m128 vec_term2 = VECTOR_FLOAT_LOAD(data2);
	__m128 vec_mul = _mm_set_ps(1, 2, 3000, 400); 
	float result_mul[4] = {0, 0, 0, 0}; _mm_store_ps(result_mul, vec_mul);
	//! ---
	float data1[100] = {1, 2, 30, 40};       float data2[100] = {0, 1, 1, 2}; 
	__m128 vec_term1 = _mm_set_ps(1, 2, 30, 40);  __m128 vec_term2 = _mm_set_ps(0, 1, 1, 2);
	VECTOR_FLOAT_STORE(data1, vec_term1); VECTOR_FLOAT_STORE(data2, vec_term2);
	{ //! Compare the results of this with the macro-varialbe:
	  __m128 vec_result_cmp = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0(vec_mul, vec_term1, vec_term2);
	  VECTOR_FLOAT_PRINT(vec_result_cmp);
	  float result_cmp[4] = {0, 0, 0, 0}; _mm_store_ps(result_cmp, vec_result_cmp);
	  float result_mul[4] = {0, 0, 0, 0}; _mm_store_ps(result_mul, vec_mul);
	  for(uint i = 0; i < 4; i++) {
	    if(!data1[i] || !data2[i]) {assert(result_cmp[i] == 0);}
	    else {assert(result_cmp[i] == result_mul[i]);}
	  }
	}
      }      
    }
  }

  { //! Valdiate basisc 'uint-macros' in our "def_intri.h":
    {
      uint data1[100] = {0, 1, 2, 3}; uint data_result[100] = {0, 0, 0, 0};
      VECTOR_UINT_TYPE vec_tmp = VECTOR_UINT_LOAD(data1);
      //! Print the result:
      if(false) {VECTOR_UINT_PRINT(vec_tmp);}
      //! Valdiate correctness of the load-store operation:
      VECTOR_UINT_STORE(data_result, vec_tmp);
      for(uint i = 0; i < 4; i++) {
	assert(data_result[i] == data1[i]);
      }
	
      //! Valdiate the add-opertion:
      vec_tmp = VECTOR_UINT_ADD(vec_tmp, vec_tmp);
      if(false) {VECTOR_UINT_PRINT(vec_tmp);}
      VECTOR_UINT_STORE(data_result, vec_tmp);
      for(uint i = 0; i < 4; i++) {
	assert((data_result[i]) == 2*data1[i]); //! ie, an 'add by itself' implies a "2*" logicla operaiton.
      }
    }
  }
    

  if(true) { //! Logics for: "if( (a[i] != FLT_MIN) && (b[i] != FLT_MAX) ) { /* then do somethign*/ }":
    //! ---------------
    float data1[100] = {FLT_MAX, 2, 50, 40};       float data2[100] = {FLT_MAX, FLT_MAX, 1, 2};
    __m128 vec_mul = _mm_set_ps(1, 2, 3, 4); __m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);  __m128 vec_term2 = VECTOR_FLOAT_LOAD(data2);

    { //! Validate computation of "VECTOR_FLOAT_dataMask_data1(..)"
      float data1[100] = {FLT_MAX, 2, 50, 40}; 
      __m128 mask_ref; __m128 vec_data = VECTOR_FLOAT_dataMask_data1(data1, mask_ref);
      if(false) {VECTOR_FLOAT_PRINT(mask_ref); 	  VECTOR_FLOAT_PRINT(vec_data);}
      float data_result[100] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(data_result, vec_data);
      float mask_result[100] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(mask_result, mask_ref);
      //! Valdiate correctness of the load-store operation:	
      assert(VECTOR_INT_ITER_SIZE == 4);
      for(uint i = 0; i < 4; i++) {
	//printf("index=%u, data1=%f, data_result=%f \n", i, data1[i], data_result[i]);
	if(data1[i] == FLT_MAX) {assert(data_result[i] == 0); assert(mask_result[i] == 0);}
	else {assert(data_result[i] == data1[i]); assert(mask_result[i] == 1);}	  
      }
    }
    { //! Validate computation of "VECTOR_FLOAT_dataMask_data1_maskImplicit(..)":
      float data1[100] = {0, 2, 50, 40}; 
      __m128 vec_result = VECTOR_FLOAT_dataMask_data1_maskImplicit(data1);
      float data_result[100] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(data_result, vec_result);
      //! Valdiate correctness of the load-store operation:	
      assert(VECTOR_INT_ITER_SIZE == 4);
      for(uint i = 0; i < 4; i++) {
	if(data1[i] == FLT_MAX) {assert(data_result[i] == 0); }
	else {assert(data_result[i] == data1[i]);}	  
      }
    }
    { //! Validate computation of "VECTOR_CHAR_getBoolean_aboveZero(..)":
      float data1[100] = {0, 2, 50, 40}; 
      __m128 vec_result = VECTOR_FLOAT_getBoolean_aboveZero(data1);
      float data_result[100] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(data_result, vec_result);
      if(false) {VECTOR_FLOAT_PRINT(vec_result);}
      uint i = 0;
      assert(data_result[i] == 0); i++;
      for(; i < 4; i++) {
	assert(data_result[i] == 1);
      }
    }
    { //! Validate computation of "VECTOR_FLOAT_storeAnd_horizontalMin(...)"
      float data1[100] = {10, 1, 2, 3}; 
      const float ret_val = VECTOR_FLOAT_storeAnd_horizontalMin(VECTOR_FLOAT_LOAD(data1));
      float min_val = FLT_MAX;
      for(uint i = 0; i < 4; i++) { if(min_val > data1[i]) {min_val = data1[i]; } }
      //printf("min_val=%f, ret_val=%f, at %s:%d\n", min_val, ret_val, __FILE__, __LINE__);
      assert(min_val == ret_val);
    }
    { //! Validate computation of "VECTOR_FLOAT_storeAnd_horizontalMin(...)"
      float data1[100] = {10, 1, 2, 3}; 
      const float ret_val = VECTOR_FLOAT_storeAnd_horizontalMin_alt1(VECTOR_FLOAT_LOAD(data1));
      float min_val = FLT_MAX;
      for(uint i = 0; i < 4; i++) { if(min_val > data1[i]) {min_val = data1[i]; } }
      //printf("min_val=%f, ret_val=%f, at %s:%d\n", min_val, ret_val, __FILE__, __LINE__);
      assert(min_val == ret_val);
    }
    { //! Validate computation of "VECTOR_FLOAT_storeAnd_horizontalSum(...)"
      float data1[100] = {0, 1, 2, 3}; 
      const float ret_val = VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_LOAD(data1));
      float sum_expected = 0;
      for(uint i = 0; i < 4; i++) { sum_expected += data1[i]; }
      //printf("sum_expected=%f, ret_val=%f, at %s:%d\n", sum_expected, ret_val, __FILE__, __LINE__);
      assert(sum_expected == ret_val);
    }
    { //! Validate computation of "VECTOR_FLOAT_storeAnd_horizontalSum(...)"
      float data1[100] = {0, 1, 2, 3}; 
      const float ret_val = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(VECTOR_FLOAT_LOAD(data1));
      float sum_expected = 0;
      for(uint i = 0; i < 4; i++) { sum_expected += data1[i]; }
      //printf("sum_expected=%f, ret_val=%f, at %s:%d\n", sum_expected, ret_val, __FILE__, __LINE__);
      assert(sum_expected == ret_val);
    }
    //! -------------------------------------------------------------------------------------------------
    { //! Validate computation of "VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(..)"
      const uint size = 20 * VECTOR_FLOAT_ITER_SIZE;
      float data1[size] = {0, 1, 2, 3}; 
      for(uint i = 0; i < size; i++) {data1[i] = i;} //! ie, intalise.
      const float ret_val = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(data1, /*start-pos=*/0, /*endPosPlussOne=*/size);
      float min_val = FLT_MAX;
      for(uint i = 0; i < size; i++) { if(min_val > data1[i]) {min_val = data1[i]; } }
      //printf("min_val=%f, ret_val=%f, at %s:%d\n", min_val, ret_val, __FILE__, __LINE__);
      assert(min_val == ret_val);
    }

    { //! Validate computation of "VECTOR_FLOAT_MATH_find_sumDistance_ofArray_alignedInPerfectChunks(..)"
      const uint size = 20 * VECTOR_FLOAT_ITER_SIZE;
      float data1[size] = {0, 1, 2, 3}; 
      for(uint i = 0; i < size; i++) {data1[i] = i;} //! ie, intalise.
      const float ret_val = VECTOR_FLOAT_MATH_find_sumOfDistance_ofArray_alignedInPerfectChunks(data1, /*start-pos=*/0, /*endPosPlussOne=*/size);
      float sum_expected = 0;
      for(uint i = 0; i < size; i++) { sum_expected += data1[i]; }
      //printf("sum_expected=%f, ret_val=%f, at %s:%d\n", sum_expected, ret_val, __FILE__, __LINE__);
      assert(sum_expected == ret_val);
    }
    assert(VECTOR_FLOAT_ITER_SIZE == 4); //! ie, to simplify our asserts.
    { //! Identify the min-value for each column-range: test correctness of our new-written "VECTOR_UNROLL_minDistance(..)":
      
      const uint tile_size = 5; 
      const uint size = tile_size * VECTOR_FLOAT_ITER_SIZE;
      float data1[size] memAlign_preAllocated = {0, 1, 2, 3}; 
      //! Intiate: to simplify our tests the first index in each  'block' is always the min-value.
      for(uint chunk_id = 0, i=0; chunk_id < (uint)VECTOR_FLOAT_ITER_SIZE; chunk_id++) {
	printf("chunk_id[%u] has min-value=%f at %s:%d\n", chunk_id, (float)i, __FILE__, __LINE__);
	for(uint innerTile_index = 0; innerTile_index < tile_size; innerTile_index++) {
	  data1[i] = (float)i;	  
	  assert(i < size); i++;	  
	}
      }

      {
	uint arr_input_pos_start_memRef = 0;
	const float scalar_1 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(/*arr_input=*/data1, arr_input_pos_start_memRef + 1*tile_size, arr_input_pos_start_memRef + 2*tile_size); 
	printf("scalar_1=%f, at %s:%d\n", scalar_1, __FILE__, __LINE__); \
	assert(scalar_1 == (1*tile_size));
      }
      
      //assert(false); // FIXME: set tile_size to a different valeu ... adn then validat ehte reuslt
      
      // FIXEM: include [”elow] 'later on' ... when we have figure out why [”elow] results in erronous results .. <-- could be due to 'minor memory-errors' in the memory-allocations
      if(false) {
	//! Then compute the score:
	float result[VECTOR_FLOAT_ITER_SIZE] memAlign_preAllocated = {0, 1, 2, 3}; 
	uint arr_input_pos_start_memRef = 0;
	VECTOR_FLOAT_minDistance_eachTile(result, /*row_index=*/0, /*arr_input=*/data1, &arr_input_pos_start_memRef, tile_size);
	assert(arr_input_pos_start_memRef == (tile_size*VECTOR_FLOAT_ITER_SIZE)); //! ie, as we expect every tile to have been validated.      
	
	
	for(uint chunk_id = 0, i=0; chunk_id < (uint)VECTOR_FLOAT_ITER_SIZE; chunk_id++) {
	  assert(result[i] == i); //! ie, givne the [ªbove] 'intaiton-phase'.
	  i += tile_size;
	}      
      }
    }



    { //! Validate computation of "VECTOR_FLOAT_transposeMatrix_forColumn(..)"
      const uint size = 31; //! ie, to not have the row 'divisible' by "VECTOR_FLOAT_ITER_SIZE", ie, to test mvoe advanced logics/ufnciontlaity:
      float **matrix = allocate_2d_list_float(size, size, /*default-value=*/0);
      //! Specify values, ie, to test correctness:
      for(uint i = 0; i < size; i++) {
	for(uint out = 0; out < size; out++) {
	  matrix[i][out] = out;
	  //printf("matrix[%u][%u] = %f, at %s:%d\n", i, out, matrix[i][out], __FILE__, __LINE__);
	}
      }

      //! The call:
      const uint index_row = 0;
      VECTOR_FLOAT_transposeMatrix_forColumn(/*result=*/matrix, /*input=*/matrix, /*index_row=*/index_row, /*col_size=*/size);
	
      //! Validate:
      //for(uint i = 0; i < size; i++) 
      {
	//printf("------------  at %s:%d\n",  __FILE__, __LINE__);
	for(uint out = 0; out < size; out++) {
	  //printf("matrix[%u][%u] = %f, at %s:%d\n", index_row, out, matrix[index_row][out], __FILE__, __LINE__);
	  assert(matrix[index_row][out] == index_row); //! ie, as we have transposed the matrix
	}
      }
	
      //! Complete, ie, de-allcoate:
      free_2d_list_float(&matrix, size);
    }


    //! --------------------------------------------------
    { //! Validate macro "VECTOR_FLOAT_DIV_omitZeros_fromComputation(..)":
      { //! Valdiate correctness of our "VECTOR_FLOAT_dataMask_pair_NAN(..)" macro:
	__m128 vec_term1 = _mm_set_ps(2, 1, 0, 0); __m128 vec_term2 = _mm_set_ps(4, 0, 0, 0);

	{
	  const __m64 vec_cmp_1 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), _mm_set1_pi8(0)); 
	  const __m64 vec_cmp_2 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term2), _mm_set1_pi8(0)); 	  
	  VECTOR_FLOAT_TYPE mask = _mm_cvtpi8_ps(_mm_or_si64(vec_cmp_1, vec_cmp_2)); mask = _mm_mul_ps(mask, mask); 
	  //VECTOR_FLOAT_TYPE mask_inverted = mask; //_mm_mul_ps(mask, _mm_set1_ps(1));
	  //	  VECTOR_FLOAT_TYPE mask_inverted = VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(1));
	  if(false) {printf("--mask:\n"); VECTOR_FLOAT_PRINT(mask); }

	  //VECTOR_FLOAT_TYPE vec_altValue_1 = _mm_add_ps(vec_term1, mask); 
	  VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); 
	  if(false) {printf("--div-data:\n"); VECTOR_FLOAT_PRINT(vec_term1); VECTOR_FLOAT_PRINT(vec_altValue_1); }

	  VECTOR_FLOAT_TYPE vec_altValue_2 = VECTOR_FLOAT_ADD(vec_term2, mask);
	  if(false) {VECTOR_FLOAT_PRINT(vec_altValue_1); 	  VECTOR_FLOAT_PRINT(vec_altValue_2);}
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_DIV(vec_altValue_1, vec_altValue_2); 
	  VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); 
	  if(false) {VECTOR_FLOAT_PRINT(vec_result); 	  VECTOR_FLOAT_PRINT(vec_subtract);}
	  vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract);	
	}

	VECTOR_FLOAT_TYPE vec_divResult = VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_term1, vec_term2);
	VECTOR_FLOAT_PRINT(vec_divResult);
	//! Validate result:
	t_float buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_FLOAT_STORE(buffer, vec_divResult);
	assert(VECTOR_FLOAT_ITER_SIZE == 4); //! ie, what we expect.
	if(false) {printf("buffer[0] = %f =? %f, at %s:%d\n", buffer[0], (float)2/4, __FILE__, __LINE__);}
	assert(buffer[0] == (float)(2.0 / 4));
	for(uint i = 1; i <  4; i++) {assert(buffer[i] == 0);}
	//! --------------
      }          
    }

    
    
    { //! Valdiate correctness of "VECTOR_MATH_shannonEntropy_float(..)"
      assert(VECTOR_FLOAT_ITER_SIZE == 4);
      
      { //! Validate the log-function:
	float data1[4] memAlign_preAllocated = {2, 1, 2, 3};
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOG(VECTOR_FLOAT_LOAD(data1));
	vec_result = VECTOR_FLOAT_revertOrder(vec_result);
	float result_cmp[4] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result_cmp, vec_result);
	if(false) {VECTOR_FLOAT_PRINT(vec_result);}
	for(uint i = 0; i < 4; i++) {
	  if(data1[i]) {
	    const float expected_value = log(data1[i]);
	    if(false) {printf("buffer[%u] = %f =? %f, at %s:%d\n", i, expected_value, result_cmp[i], __FILE__, __LINE__);}
	    assert(result_cmp[i] == expected_value);
	  } else {assert(result_cmp[i] == 0);}
	}
      }
      { //! Then comapre for cases hwer ehte value is "0":
	//! Note: as an 'intro' we first validate correctness of the "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(..)" function:
	//float data1[4]  = {0, 1, 2, 3};
	float data1[4] memAlign_preAllocated = {0, 1, 2, 3};
	//	VECTOR_FLOAT_TYPE vec_term1 = VECTOR_FLOAT_LOAD(data1);
	__m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);
	{ //! An 'annoated' extract from our "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(..)":
	  // FIXME[jc]: is there a better approach to avodi the "NAN" value (ie, than converting to-and-from "char")? <-- if so, then update functions which makes use of "_mm_cmpeq_pi8(..)".
	  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), VECTOR_CHAR_SET1(0))); 
	  mask = VECTOR_FLOAT_MUL(mask, mask); //! ie, to remove the 'negative signs'.
	  VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask);
	  if(true) {VECTOR_FLOAT_PRINT(vec_altValue_1); }
	  
	  VECTOR_FLOAT_TYPE vec_result    = /*funcRef_float_1d=*/VECTOR_FLOAT_LOG(vec_altValue_1);	

	  vec_result = VECTOR_FLOAT_revertOrder(vec_result);

	  if(true) {printf("--\n"); VECTOR_FLOAT_PRINT(vec_result); VECTOR_FLOAT_PRINT(mask); }
	  VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); 
	  vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract);	

	  if(true) {printf("--\n"); VECTOR_FLOAT_PRINT(vec_result); VECTOR_FLOAT_PRINT(vec_subtract); printf("-----\n");}
	 



	  if(true) {VECTOR_FLOAT_PRINT(vec_result);}
	  //! Validate:
	  float result_cmp[4] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result_cmp, vec_result);	    
	  assert(result_cmp[0] == 0); //! ie, as we expect "data1[0] == 0"
	  for(uint i = 0; i < 4; i++) {
	    if(data1[i]) {
	      const float expected_value = log(data1[i]);
	      if(true) {printf("buffer[%u] = %f =? %f, at %s:%d\n", i, expected_value, result_cmp[i], __FILE__, __LINE__);}
	      //assert(result_cmp[i] == expected_value);
	    } else {assert(result_cmp[i] == 0);}	    
	  }

	  {
	    //VECTOR_CHAR_TYPE vec_result = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), _mm_set1_pi8(0)); 
	    VECTOR_FLOAT_TYPE vec_result_alt2 = VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1, VECTOR_FLOAT_LOG);
	    if(true) {VECTOR_FLOAT_PRINT(vec_result_alt2);}
	    float result_cmp_alt2[4] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result_cmp_alt2, vec_result_alt2);	    
	    for(uint i = 0; i < 4; i++) {
	      assert(result_cmp[i] == result_cmp_alt2[i]);
	    }
	  }
	}
      }
    }



    {
      assert(VECTOR_FLOAT_ITER_SIZE == 4);
      { //! Valdiate our expctation that 'nan' are covnerted to 'valeus different from 0' when using an 'impliict char-covnerison':
	// FIXME[jc]: is there a better approach to avodi the "NAN" value (ie, than converting to-and-from "char")? <-- if so, then update functions which makes use of "_mm_cmpeq_pi8(..)". 
	__m128 vec_term1 = VECTOR_FLOAT_SET(NAN, 0, 0, 0);
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_convertFrom_CHAR(VECTOR_CHAR_convertFrom_float_vector(vec_term1)));
	float result[4] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_result);
	assert(result[0] == -128);
	for(uint i = 1; i < 4; i++) {assert(result[i] == 0);} 	
      }
      
      { //! Validate correctness of our VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean:
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(VECTOR_FLOAT_SET(NAN, 0, 0, 0));
	float result[4] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_result);
	assert(result[0] == 1);
	for(uint i = 1; i < 4; i++) {assert(result[i] == 0);} 	
      }

      { //! Test the following logics: if(arr_current[bucket_id] < arr_best_value(bucket_id)) {arr_best_value[bucket_id] = score; arr_best_index[bucket_id] = row_id;}
	const uint row_id = 10; float local_score[4] memAlign_preAllocated = {0, -10, 2, 3}; float global_score[4] memAlign_preAllocated = {3, 2, 1, -500};
	float global_index[4] memAlign_preAllocated = {100, 200, 300, 400};
	const VECTOR_FLOAT_TYPE vec_input  = VECTOR_FLOAT_LOAD(local_score); const VECTOR_FLOAT_TYPE vec_global = VECTOR_FLOAT_LOAD(global_score);
	//! Identify the masks wrt. the 'local' and the 'global' data-set:
	__m128 local_mask = _mm_cmpgt_ps(vec_input, vec_global);  __m128 global_mask = _mm_cmplt_ps(vec_input, vec_global);
	//! 'Handle' the NAN values:
	local_mask = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(local_mask);
	global_mask = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(global_mask);

	if(true) {printf("--\n"); VECTOR_FLOAT_PRINT(local_mask); VECTOR_FLOAT_PRINT(global_mask);}

	// FIXME: validate correnctess of [”elow] wrt. the funcitons usage.
	VECTOR_FLOAT_TYPE vec_input_index = VECTOR_FLOAT_SET1(row_id); //VECTOR_FLOAT_setFrom_indexRange(row_id);	
	vec_input_index = VECTOR_FLOAT_MUL(vec_input_index, local_mask);	
	//! Set to '1' the global indexes which are not of interest:
	VECTOR_FLOAT_TYPE vec_global_index = VECTOR_FLOAT_LOAD(global_index);
	vec_global_index = VECTOR_FLOAT_MUL(vec_global_index, global_mask);

	if(true) {printf("--\n"); VECTOR_FLOAT_PRINT(vec_input_index); VECTOR_FLOAT_PRINT(vec_global_index);}

	vec_global_index = VECTOR_FLOAT_ADD(vec_input_index, vec_global_index); //! ie, as the empty values are set to '0'.
	VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_MIN(vec_input, vec_global);

	if(true) {printf("--\n"); VECTOR_FLOAT_PRINT(vec_global_index); VECTOR_FLOAT_PRINT(vec_min);}

	{ //! Test correctness:
	  const float result_correct_score[4] memAlign_preAllocated = {0, -10, 1, -500};  
	  const float result_correct_index[4] memAlign_preAllocated = {row_id, row_id, 300, 400};
	  float local_index[4] memAlign_preAllocated = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(local_index, vec_input_index);
	  { //! Valdiate that our inptu is as expected:
	    uint i = 0;
	    for(; i < 2; i++) {
	      assert(local_score[i]  == result_correct_score[i]);
	      assert(global_score[i] >= result_correct_score[i]);
	      //! ---
	      assert(local_index[i]  == result_correct_index[i]);
	      assert(global_index[i] != result_correct_index[i]);
	    }
	    for(; i < 4; i++) {
	      assert(local_score[i]  >= result_correct_score[i]);
	      assert(global_score[i] == result_correct_score[i]);
	      //! ---
	      assert(local_index[i]  != result_correct_index[i]);
	      assert(global_index[i] == result_correct_index[i]);
	    }
	  }
	  //! Then validate the result:
	  float result_cmp_score[4] memAlign_preAllocated = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result_cmp_score, vec_min);
	  float result_cmp_index[4] memAlign_preAllocated = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result_cmp_index, vec_global_index);
	  for(uint i = 0; i < 4; i++) {
	    assert(result_correct_score[i] == result_cmp_score[i]);
	    assert(result_correct_index[i] == result_cmp_index[i]);
	  }

	  
	  { //! Compare with the macro:
	    float memRef_result_score[4] memAlign_preAllocated = {0, 0, 0, 0}; float memRef_result_index[4] memAlign_preAllocated = {0, 0, 0, 0};
	    //! -------------
	    const VECTOR_FLOAT_TYPE vec_input  = VECTOR_FLOAT_LOAD(local_score); const VECTOR_FLOAT_TYPE vec_global = VECTOR_FLOAT_LOAD(global_score);
	    VECTOR_FLOAT_TYPE vec_global_index = VECTOR_FLOAT_LOAD(global_index); 
	    VECTOR_FLOAT_TYPE vec_result_min = VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(vec_input, vec_global, vec_global_index,  /*idOf_input=*/row_id);
	    VECTOR_FLOAT_STORE(memRef_result_score, vec_result_min); VECTOR_FLOAT_STORE(memRef_result_index, vec_global_index);
	    //! -------------
	    
	    //! Validate correctness:
	    for(uint i = 0; i < 4; i++) {
	      assert(result_correct_score[i] == memRef_result_score[i]);
	      assert(result_correct_index[i] == memRef_result_index[i]);
	    }
	  }
	  { //! Compare with a macro which tinegrates [ªbvove], ie, our "VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input_data(..)":
	    float memRef_result_score[4] memAlign_preAllocated = {0, 0, 0, 0}; float memRef_result_index[4] memAlign_preAllocated = {0, 0, 0, 0};
	    //! -------------
	    VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input_data(memRef_result_score, memRef_result_index, local_score, global_score, global_index, /*idOf_input=*/row_id);
	    //! -------------
	    
	    //! Validate correctness:
	    for(uint i = 0; i < 4; i++) {
	      assert(result_correct_score[i] == memRef_result_score[i]);
	      assert(result_correct_index[i] == memRef_result_index[i]);
	    }
	  }
	}
      }
    }


    { //! Validate correctness of the "VECTOR_INT_STORE(..)" function.
      const int size_of_array = 10; const int default_value = 5;
      int *listOf_values_32Bit_int = allocate_1d_list_int(size_of_array, default_value);
      VECTOR_INT_TYPE vec_data = _mm_load_si128((__m128i const*)&listOf_values_32Bit_int[0]);
      VECTOR_INT_PRINT(vec_data);
      int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_data);
      for(uint i = 0; i < VECTOR_INT_ITER_SIZE; i++) {
	assert(buffer[i] == default_value);
      }
      free_1d_list_int(&listOf_values_32Bit_int);
      //#define VECTOR_INT_LOAD(mem_ref) (_mm_load_si128((__m128i const*)mem_ref))
      //#define VECTOR_INT_LOAD(mem_ref) (VECTOR_INT_SET(*(mem_ref+0), *(mem_ref+1), *(mem_ref+2), *(mem_ref+3)))
    }
  { //! Validate correctness of the "VECTOR_INT_STORE(..)" function for 'short int'
    const short int size_of_array = 10; const int default_value = 5;
    short int *listOf_values_32Bit_int = allocate_1d_list_int_short(size_of_array, default_value);
    VECTOR_INT_TYPE vec_data = _mm_load_si128((__m128i const*)&listOf_values_32Bit_int[0]);
    //VECTOR_INT_PRINT(vec_data);
    short int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_data);
    for(uint i = 0; i < VECTOR_INT_ITER_SIZE_short; i++) {
      printf("buffer[%u]=%d, at %s:%d\n", i, (int)buffer[i], __FILE__, __LINE__);
      assert(buffer[i] == default_value);
    }

    { //! Now test for the assicated macro-function:
      VECTOR_INT_TYPE vec_data = VECTOR_INT_LOAD(&listOf_values_32Bit_int[0]);
      //VECTOR_INT_PRINT(vec_data);
      short int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_data);
      for(uint i = 0; i < VECTOR_INT_ITER_SIZE_short; i++) {
	printf("buffer[%u]=%d, at %s:%d\n", i, (int)buffer[i], __FILE__, __LINE__);
	assert(buffer[i] == default_value);
      }      
    }
    //! De-allcoate locally reserved memory:
    free_1d_list_int_short(&listOf_values_32Bit_int);
//#define VECTOR_INT_LOAD(mem_ref) (VECTOR_INT_SET(*(mem_ref+0), *(mem_ref+1), *(mem_ref+2), *(mem_ref+3)))
  }
  { //! Test a new-written "VECTOR_INT_MAX(..)" function:
      VECTOR_INT_TYPE vec_cmp = VECTOR_INT_SET(1, 2, 30, 40); VECTOR_INT_TYPE vec_input = VECTOR_INT_SET(24, 23, 5, 6);

    //! @return the valeu at the assicated index
    // FIXME: try to update our examples and oru macros to use [beloł] .. ie, to not alwyas make use of the buffer[4] 'code-strategy'

        
    //VECTOR_INT_TYPE vec_result = vec_cmp; union UNION_VECTOR_INT_TYPE d_result; d_result.sse = vec_result; 
    
    VECTOR_INT_TYPE vec_result = VECTOR_INT_MAX(vec_input, vec_cmp);
    int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_result);
    // VECTOR_INT_PRINT(vec_result); //! ie, [24, 23, 30, 40]
    assert(buffer[0] == 24);     assert(buffer[1] == 23);
    assert(buffer[2] == 30);     assert(buffer[3] == 40);
  }
  //! --------------------------------------------------


    { //! A simple exampel to test the "c = _mm_hadd_ps(c, c);" lgoics <-- does 'this' get the 'horizontal sum' <-- if so, then udpate our macro
      
      // __m128 temp = _mm_add_ps(_mm_movehl_ps(foo128, foo128), foo128);
      // float x;
      // _mm_store_ss(&x, _mm_add_ss(temp, _mm_shuffle_ps(temp, 1)));

      assert(false); // FIXME: add something.
    }

    assert(false); // FIXME: update our memory-allcoation-procedrues and 'static procredusre's to use ... 
    assert(false); // FIXME: update our memory-allcoation-procedrues and 'static procredusre's to use ... "posiz_memalign" ... __attribute((aligned(32)) ... "float* a; posix_memalign((void**)&a, 16,  N * sizeof(float));                                                                                                                                                            

    


    //! --------------------------------------------------



    // //! --------------------------------------------------
    // __m128i t0 = _mm_and_si128(_mm_castps_si128(V), g_XMQNaNTest);
    // __m128i t1 = _mm_and_si128(_mm_castps_si128(V), g_XMInfinity);
    // t0 = _mm_cmpeq_epi32(t0, g_XMZero);
    // t1 = _mm_cmpeq_epi32(t1, g_XMInfinity);
    // __m128i isNaN = _mm_andnot_si128(t0, t1);
 
    // select0 = _mm_and_si128(isNaN, g_XMQNaN);
    // select1 = _mm_andnot_si128(isNaN, result5);
    // __m128i vResult = _mm_or_si128(select0, select1);
 
      assert(false); // FIXME: try making use of: ... void _MM_SET_EXCEPTION_MASK (unsigned int a)

    fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); assert(false); // FIXME: validate [ªbove] ... adn then test for FLT_MAX and FLT_MIN


    assert(false); // FIXEM: trye teh foolowing code: if(_mm_movemask_epi8(_mm_castps_si128(vec_needToIterate)) == 0xfff0) { }

  }
  //! --------------------------------------------------
  { //! Test 'generic' strategies wrt. the 'mask-property' using the char-data-table
    
    // FIXME: test the correctness of 'conversion'.
    
  }


  { //! Validate correctness of each macro-function in our "def_intri.h"

  }




}


void testPerformance_intri_base() {
  
  // FIXME: move this funciton into a new 'itnri-base-perofrmance-test' class/file.

  { //! test wwrt. "__m128 _mm_setzero_ps (void)" VS "_mm_set1_ps(0)"

  }

  { //! Evlaute the time of 'fetching' data, ie, 
    //! "float *p = (float *)&vec_result"; 
    //! VS 
    //! {float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec_masked1[%u] = %f, ", i, result[i]);} fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); }
    //! --------------------------


  }

  // FIXME: test wrt. 'log(..)' for cases where value=[0, 1]. ... eg, test the logics found in the "esl_sse.cpp" file
  // FIXME: also compare with: "http://jrfonseca.blogspot.no/2008/09/fast-sse2-pow-tables-or-polynomials.html"
}


void suffle_example() {

    printf("shuff-eincreasing=%d, at %s:%d\n\n\n", _MM_SHUFFLE(0, 1, 2, 3), __FILE__, __LINE__);
    if(false) { //! then we write out an explcit set of combinaitons:
      for(int i1 = 0; i1 < 10; i1++) {
	for(int i2 = 0; i2 < 10; i2++) {
	  for(int i3 = 0; i3 < 10; i3++) {
	    for(int i4 = 0; i4 < 10; i4++) {
	      //VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(i1, 0, 0, 0))); //! result: 
	      //VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(i1, i2, i2, i4))); //! result: 
	      printf("VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(%d, %d, %d, %d))); //! result: \n", i1, i2, i3, i4);
	    }
	  }
	}
      }
    }

    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(0, 0, 0, 0))); //! result: 4, 4, 8, 8
    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(1, 1, 0, 0))); //! result: 4, 4, 7, 7
    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(7, 7, 7, 7))); //! result: 1, 1, 5, 5
    VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(5, 5, 5, 5))); //! result: 
    //    VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), _MM_SHUFFLE(7, 7, 7, 7))); //! result: 

    //VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), 2)); //! result: 2, 4, 8, 8
    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), 3)); //! result: 1, 4, 8, 8
    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), 4)); //! result: 4, 3, 8, 8
    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), 5)); //! result: 3, 3, 8, 8
    // VECTOR_FLOAT_PRINT(_mm_shuffle_ps(_mm_set_ps(1, 2, 3, 4), _mm_set_ps(5, 6, 7, 8), 6)); //! result: 2, 3, 8, 8
}

  //! Evaluate different appraoches for basic artimetic operations:
static void assert_basic_artimetics() {

  { //! Examplify how we may move/slide a set of valeus: 
    // TODO: try to describe a use-case 'for this'.
    {
      VECTOR_FLOAT_TYPE vec_data = VECTOR_FLOAT_SET(1, 2, 3, 4);
      VECTOR_FLOAT_TYPE shift1_SSE = _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(vec_data), /*cnt-bytes*/4));  
      VECTOR_FLOAT_PRINT(shift1_SSE); //! which results in [0, 4, 3, 2]
      VECTOR_FLOAT_TYPE shift2_SSE = _mm_shuffle_ps(_mm_setzero_ps(), vec_data, 0x40); 
      VECTOR_FLOAT_PRINT(shift2_SSE); //! which results in [0, 0, 4, 3]
    }
    {
      VECTOR_FLOAT_TYPE vec_data = VECTOR_FLOAT_SET(1, 2, 3, 4);
      VECTOR_FLOAT_TYPE shift1_SSE = _mm_castsi128_ps(_mm_slli_si128(_mm_castps_si128(vec_data), /*cnt-bytes*/12));  
      VECTOR_FLOAT_PRINT(shift1_SSE); //! which results in [0, 4, 3, 2]
      VECTOR_FLOAT_TYPE shift2_SSE = _mm_shuffle_ps(_mm_setzero_ps(), vec_data, 0x40); 
      VECTOR_FLOAT_PRINT(shift2_SSE); //! which results in [0, 0, 4, 3]
    }
    {
      VECTOR_FLOAT_TYPE vec_data = VECTOR_FLOAT_SET(1000, 2, 3, 4);
      VECTOR_FLOAT_TYPE shift1_SSE = _mm_castsi128_ps(_mm_srai_epi32(_mm_castps_si128(vec_data), /*cnt-bytes*/1));  
      VECTOR_FLOAT_PRINT(shift1_SSE); //! which results in []
      VECTOR_FLOAT_TYPE shift2_SSE = _mm_shuffle_ps(_mm_setzero_ps(), vec_data, 0x40); 
      VECTOR_FLOAT_PRINT(shift2_SSE); //! which results in []
    }
  }

  assert(false); // FIXME:w rite correctneee-test for "VECTOR_INT_MAX(..)"

}

//! Evaluate different appraoches for applicaiton/inference of masks:
static void assert_mask_operations() {


  {
    const uint char_size = 100; const char default_value_char = 0;
    char *char_term1 = allocate_1d_list_char(char_size, default_value_char);
    char *char_term2 = allocate_1d_list_char(char_size, default_value_char);
    char_term1[0] = 1; char_term1[1] = 1; char_term1[2] = 1;
    char_term2[0] = 1; char_term2[1] = 0; char_term2[3] = 1;
    VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_SET(3, 2, 1, -2);
    { //! -----------------------------------------------------------------------
      //! Apply the mask-filters:
      __m64 vec_1 = VECTOR_CHAR_LOAD(char_term1); __m64 vec_2 = VECTOR_CHAR_LOAD(char_term2);
      VECTOR_FLOAT_PRINT_wMessage("vec_1: ", VECTOR_FLOAT_convertFrom_CHAR(vec_1));
      VECTOR_FLOAT_PRINT_wMessage("vec_2: ", VECTOR_FLOAT_convertFrom_CHAR(vec_2));

      VECTOR_CHAR_TYPE mask_char = _mm_and_si64(vec_1, vec_2);
      VECTOR_FLOAT_TYPE mask =     VECTOR_FLOAT_convertFrom_CHAR(mask_char);
      mask = VECTOR_FLOAT_revertOrder(mask);
      VECTOR_FLOAT_PRINT_wMessage("mask: ", mask);

      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_mul, mask);
      vec_result = VECTOR_FLOAT_revertOrder(vec_result);
      VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);
    }
    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2);
    VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);
    float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result);
    float result_cmp[4] = {3.0, 0, 0, 0}; //! ie, the input-data in reverse order provided [ªbov€], where we expec tht 'combiend mask' to be set to "[1, 0, 0, 0]"
    for(uint i = 0; i < 4; i++) {
      assert(result[i] == result_cmp[i]);
    }

    // __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_mul_su32(vec_1, vec_1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_mul_su32(vec_2, vec_2), _mm_set1_pi8(0));
    // __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	
    // mask = _mm_mul_ps(mask, mask); 
    // mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3));
    // __m128 vec_result = _mm_mul_ps(mask, vec_mul);
    // vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3));

    //! -------------
    //! De-allcoate locally reserved mmoery:
    free_1d_list_char(&char_term1); free_1d_list_char(&char_term2);
  }
  // FIXME: try to include [below] tests when we have fiogured out why [”elow] does not work  ... eg, for result_cmp[1]=1 VS result_mul[1]=400
  //if(false)
  

  float scalar = NAN;
  printf("hex is %x\n", *(unsigned int*)&scalar);
  const t_float log_scalar = NAN; //log_range_0_one_abs(prob);
  assert(sizeof(t_float) == sizeof(int));  
  printf("NAN is %x and log-hex=%x, at %s:%d\n\n", *(unsigned int*)&scalar, *(unsigned int*)&log_scalar, __FILE__, __LINE__);	
  // printf("internal index=%u for prob=%f w/log %f =?= %f,  at %s:%d\n", (uint)__get_internalIndexOf_float(prob), prob, log_scalar, log(prob), __FILE__, __LINE__);

  { //! Translate "[nan, 0, nan, nan]" into "[1, 0, 1, 1]"
    //! Note: this strategy may be used to hadnle nan-values
    if(false) { //! then we incotrudce a set fo sampel-comparisons to illustrate 'key-concpets'w r.t NAN-values:
      VECTOR_FLOAT_PRINT(VECTOR_FLOAT_SET1(NAN));
      VECTOR_FLOAT_PRINT(VECTOR_FLOAT_AND(VECTOR_FLOAT_SET1(NAN), VECTOR_FLOAT_SET1(NAN))); //! ie, "(NAN && NAN) = NAN"
      VECTOR_FLOAT_PRINT(VECTOR_FLOAT_ANDNOT(VECTOR_FLOAT_SET(0, 1, 2, NAN), VECTOR_FLOAT_SET1(NAN))); //! ie, "(NAN && NAN) = NAN"
      VECTOR_FLOAT_PRINT(VECTOR_FLOAT_XOR(VECTOR_FLOAT_SET1(NAN), VECTOR_FLOAT_SET1(NAN))); //! ie, "(NAN xor NAN) = 0"
      VECTOR_FLOAT_TYPE vec1 = VECTOR_FLOAT_SET(NAN, 0, NAN, NAN);
      VECTOR_FLOAT_PRINT(VECTOR_FLOAT_XOR(vec1, vec1));  //! ie, "(NAN xor 0) = 0"
      vec1 = VECTOR_FLOAT_SET(NAN, 4, 1, 0);
      VECTOR_FLOAT_PRINT_wMessage("vec xor vec: ", VECTOR_FLOAT_XOR(vec1, vec1));  //! ie, "(NAN xor 1) = 0"
      VECTOR_FLOAT_PRINT_wMessage("vec xor nan: ", VECTOR_FLOAT_XOR(vec1, VECTOR_FLOAT_SET1(NAN)));  //! ie, "(NAN xor 1) = 0"
      VECTOR_FLOAT_PRINT_wMessage("vec and nan: ", VECTOR_FLOAT_AND(vec1, VECTOR_FLOAT_SET1(NAN)));  //! ie, "(NAN and 1) = 1" and "(NAN and 0) = 0" <-- if this proeprty is combined with "!= 0", then NAN-valeus may 'easily' be clreared, ie, where "vec1" is the result os such a comparison.
      VECTOR_FLOAT_PRINT_wMessage("vec and 1: ", VECTOR_FLOAT_AND(vec1, VECTOR_FLOAT_SET1(1)));  //! ie, "(NAN and 1) = 1" and "(NAN and 0) = 0" <-- if this proeprty is combined with "!= 0", then NAN-valeus may 'easily' be clreared, ie, where "vec1" is the result os such a comparison.
    }
    if(false) { //! A sampel-data-set: steps(1-2): find 'non-zero' valeus; step(3): use and 'and' to idnetify all values which are zero; step(4): use an 'and' to intersect NAN values.
      //! Step(0): intliase:
      VECTOR_FLOAT_TYPE vec1 = VECTOR_FLOAT_SET(0, 1, 2, 3);     VECTOR_FLOAT_TYPE vec2 = VECTOR_FLOAT_SET(4, 0, 0, 5);
      /*! Step(1-2): find non-zero valeus:*/
      VECTOR_FLOAT_TYPE vec_cmpNonZero_1 = VECTOR_FLOAT_CMPEQ(vec1, VECTOR_FLOAT_SET1(0));
      VECTOR_FLOAT_PRINT_wMessage("vec non-zero-1", vec_cmpNonZero_1);
      VECTOR_FLOAT_TYPE vec_cmpNonZero_2 = VECTOR_FLOAT_CMPEQ(vec2, VECTOR_FLOAT_SET1(0));
      VECTOR_FLOAT_PRINT_wMessage("vec non-zero-2", vec_cmpNonZero_1);
      //VECTOR_FLOAT_PRINT_wMessage("vec non-zero-and", VECTOR_FLOAT_AND(vec_cmpNonZero_1, vec_cmpNonZero_2));
      /*! Step(3): Find all values which are zero: */
      VECTOR_FLOAT_TYPE vec_notZero = VECTOR_FLOAT_OR(vec_cmpNonZero_1, vec_cmpNonZero_2);    
      //VECTOR_FLOAT_TYPE vec_notZero = VECTOR_FLOAT_OR(vec_cmpNonZero_1, vec_cmpNonZero_2);    
      VECTOR_FLOAT_PRINT_wMessage("vec-notZero", vec_notZero); //!  [-nan, 0.000000, 0.000000, 0.000000, ],
      /*! Note: we need to use the 'ant-not' operatnd to 'clear' the NAN-valeus: if the latter had nott bene the case/issue, then we instead coudl have 'ivnerted' above lgocis.*/
      vec_notZero = VECTOR_FLOAT_ANDNOT(vec_notZero, VECTOR_FLOAT_SET1(1));
      /*! Step(4): 'revert' the zeros: */
      VECTOR_FLOAT_PRINT_wMessage("vec-notZero and 1: ", vec_notZero);  //! ie, "(NAN and 1) = 1" and "(NAN and 0) = 0" <-- if this proeprty is combined with "!= 0", then NAN-valeus may 'easily' be clreared, ie, where "vec1" is the result os such a comparison.
    }

    if(false) { //! test subtraction wrt. NAN-values:
      VECTOR_FLOAT_TYPE vec1 = VECTOR_FLOAT_SET(NAN, 6, NAN, NAN);
      VECTOR_FLOAT_PRINT_wMessage("vec and 1: ", VECTOR_FLOAT_AND(vec1, VECTOR_FLOAT_SET1(1))); 
      VECTOR_FLOAT_PRINT_wMessage("vec and 1: ", VECTOR_FLOAT_SUB(vec1, vec1));  //! ... 
      VECTOR_FLOAT_PRINT_wMessage("vec and 1: ", VECTOR_FLOAT_SUB(vec1, VECTOR_FLOAT_SET1(NAN)));  //! ... 
      VECTOR_FLOAT_PRINT_wMessage("vec and 1: ", VECTOR_FLOAT_MUL(vec1, vec1));  //! ... 

      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_DIV(VECTOR_FLOAT_SET(1, 0, 0, 2.1), VECTOR_FLOAT_SET(0, 0, 1, 3)); //! ie, a division which results in NAN
      VECTOR_FLOAT_PRINT_wMessage("div-by-zero=", vec_result);
    }

    { //! Test cases. wrt. div-by-zero-results, eg, " [0.000000, -nan, -nan, -nan, ]":
      //! Ntoe(1): base don the observation that "0/0 = 0" (ie, not NAN nor INFINITY), we set "0/.." and "../0" to "0", ie, wher ethe latter describes our 'idea':
      //! Note(2): the [”elow] 'case' examplfieis how NAN-valeus may be derived 'driectly' from a 'dvide-by-zero' case:

      // VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(1, NAN, INFINITY, 2.1);
      //VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_DIV(VECTOR_FLOAT_SET(1, 0, 0, 2.1), VECTOR_FLOAT_SET(0, 0, 1, 3)); //! ie, a division which results in NAN
      
      VECTOR_FLOAT_TYPE vec_term1 = VECTOR_FLOAT_SET(1, 0, 0, 2.1); VECTOR_FLOAT_TYPE vec_term2 = VECTOR_FLOAT_SET(0, 0, 1, 3); 

      /*! Merge the input-terms: */
      /*VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_AND(vec_term1, vec_term2);*/
      VECTOR_FLOAT_TYPE mask = vec_term2; 
      VECTOR_FLOAT_PRINT_wMessage("mask: ", mask);
      mask = VECTOR_FLOAT_CMPNEQ(mask, VECTOR_FLOAT_SET1(0));
      /*! Clear the NAN signs: */
      mask = VECTOR_FLOAT_AND(mask, VECTOR_FLOAT_SET1(1));
      VECTOR_FLOAT_PRINT_wMessage("vec-sample: ", mask);

      //! In order to avoid nan-cases each '0' valeu need to be set to 1:
      VECTOR_FLOAT_TYPE mask_inv = VECTOR_FLOAT_ANDNOT(mask, VECTOR_FLOAT_SET1(1));
      VECTOR_FLOAT_PRINT_wMessage("mask_inv: ", mask_inv);
      /*! Increment 'zero-valeus': */
      VECTOR_FLOAT_TYPE vec_term2_wMask = VECTOR_FLOAT_ADD(vec_term2, mask_inv); 
      VECTOR_FLOAT_PRINT_wMessage("term2 xmt mask: ", vec_term2_wMask);

      /*! Set to 'zero' the valeus which are to be empty: */
      VECTOR_FLOAT_TYPE vec_1_mask = VECTOR_FLOAT_AND(VECTOR_FLOAT_SET1(1), mask);     
      VECTOR_FLOAT_PRINT_wMessage("'1' xmt mask: ", vec_1_mask);

      /*! Apply the mask to term2: */
      VECTOR_FLOAT_TYPE vec_divBy_term2 = VECTOR_FLOAT_DIV(vec_1_mask, vec_term2_wMask);
      VECTOR_FLOAT_PRINT_wMessage("1/term2: ", vec_divBy_term2);
      /*! Multiply the tersult with term1: */
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_term1, vec_divBy_term2);
      VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);
    }


    

    { //! Validate 'results' of/from above using our new-written macro "VECTOR_FLOAT_omitZeros_fromComputation_DIV(..)":
      VECTOR_FLOAT_TYPE vec_term1 = VECTOR_FLOAT_SET(1, 0, 0, 2.1); VECTOR_FLOAT_TYPE vec_term2 = VECTOR_FLOAT_SET(0, 0, 1, 3); 
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_term1, vec_term2);
      VECTOR_FLOAT_PRINT(vec_result);
      float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result);
      float result_cmp[4] = {2.1/3.0, 0, 0, 0}; //! ie, the input-data in reverse order provided [ªbov€].
      for(uint i = 0; i < 4; i++) {
	assert(result[i] == result_cmp[i]);
      }

    }

    assert(false); // FIXME: write a macro wrt. 'this' ... and then test the erpforamnce in our "measure_intri.cxx"
  }



  { //! Logic: "c = 0; if (a < b) {c = d;}"
    __m128 a = _mm_setr_ps(1,-1,0,2); //! ie, set the valeus in reverse order.
    __m128 b = _mm_setr_ps(1,1,1,1);
    __m128 d = _mm_setr_ps(1,2,1,5);

    __m128 c = _mm_cmplt_ps(a,b);
    c = _mm_and_ps(c,d);

    VECTOR_FLOAT_PRINT(c);    
  }
  { //! Comapre the bit-sign in SSE
    //! Inspiration: "http://stackoverflow.com/questions/8440764/compare-the-sign-bit-in-sse-intrinsics"
    //! Note: [1.0 -1.0 0.0 2.0] and b is [1.0 1.0 1.0 1.0] the desired mask we would get is [true false true true].
    const __m128i MASK = _mm_set1_epi32(0xffffffff);

    __m128 a = _mm_setr_ps(1,-1,0,2); //! ie, set the valeus in reverse order.
    __m128 b = _mm_setr_ps(1,1,1,1);

    __m128  vec_f = _mm_xor_ps(a,b);
    __m128i vec_i = _mm_castps_si128(vec_f);

    vec_i = _mm_srai_epi32(vec_i,31);
    vec_i = _mm_xor_si128(vec_i, MASK);

    vec_f = _mm_castsi128_ps(vec_i);
    
    VECTOR_FLOAT_PRINT(vec_f);
    VECTOR_INT_PRINT(vec_i); 
    //  i = (0xffffffff, 0, 0xffffffff, 0xffffffff)
    //  f = (0xffffffff, 0, 0xffffffff, 0xffffffff)
  }
  { //! Valdiate the new-written "VECTOR_INT_MUL_short(..)" macro
    // VECTOR_FLOAT_PRINT(VECTOR_FLOAT_MUL(VECTOR_FLOAT_SET(0, -1, 2, 3), VECTOR_FLOAT_SET1(2)))
    const short int test_mul_value = 2; const short int counter_start_pos = 0;
    short int counter = counter_start_pos; 
    VECTOR_INT_TYPE_short vec_1 = VECTOR_INT_SET_short(counter++, counter++, counter++, counter++, counter++, counter++, counter++, counter++);  VECTOR_INT_TYPE_short vec_2 = VECTOR_INT_SET1_short(test_mul_value);; 
    //! --- 
    VECTOR_INT_TYPE_short vec_result = VECTOR_INT_MUL_short(vec_1, vec_2);
    VECTOR_INT_PRINT_short(vec_result);
    //! ---
    //! Revert the buffer, ie, to simplify testing:
    //VECTOR_INT_PRINT_short(vec_result);
    //vec_result = _mm_bswap_epi16(vec_result);
    vec_result = VECTOR_INT_revertOrder_short(vec_result);
    VECTOR_INT_PRINT_short(vec_result);
    // __m128i _mm_shufflelo_epi16 (__m128i a, int imm8)
    short int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_result);
    //! Validate correcgness:
    counter = counter_start_pos; //! ie, reset.
    for(short int i = 0; i < VECTOR_INT_ITER_SIZE_short; i++) {
      const short int expected_result = (test_mul_value*counter);
      printf("buffer[%u]=%d =? %d , at %s:%d\n", i, (int)buffer[i], (int)expected_result,__FILE__, __LINE__);
      assert(buffer[i] == expected_result);
      counter++;
    }      
  }
  { //! Valdiate the new-written "VECTOR_INT_DIV_short(..)" macro


    const short int test_mul_value = 2; const short int counter_start_pos = 8;
    short int counter = counter_start_pos; 
    VECTOR_INT_TYPE_short vec_1 = VECTOR_INT_SET_short(counter++, counter++, counter++, counter++, counter++, counter++, counter++, counter++);  VECTOR_INT_TYPE_short vec_2 = VECTOR_INT_SET1_short(test_mul_value);; 
    //! --- 

    printf("vec1="); VECTOR_INT_PRINT_short(vec_1);    printf("vec2=");  VECTOR_INT_PRINT_short(vec_2);

    VECTOR_INT_TYPE_short vec_result = VECTOR_INT_DIV_short_notInverseResult(vec_1, vec_2);
    VECTOR_INT_PRINT_short(vec_result);

    //vec_result = _mm_bswap_epi16(vec_result);
    //vec_result = VECTOR_INT_revertOrder_short(vec_result);
    VECTOR_INT_PRINT_short(vec_result);
    // __m128i _mm_shufflelo_epi16 (__m128i a, int imm8)
    short int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_result);
    //! Validate correcgness:
    counter = counter_start_pos; //! ie, reset.
    for(short int i = 0; i < VECTOR_INT_ITER_SIZE_short; i++) {
      const short int expected_result = (counter / test_mul_value);
      printf("buffer[%u]=%d =? %d , at %s:%d\n", i, (int)buffer[i], (int)expected_result,__FILE__, __LINE__);
      assert(buffer[i] == expected_result);
      counter++;
    }      
  }
  { //! Valdiate the new-written "VECTOR_INT_MUL(..)" macro
    // VECTOR_FLOAT_PRINT(VECTOR_FLOAT_MUL(VECTOR_FLOAT_SET(0, -1, 2, 3), VECTOR_FLOAT_SET1(2)))
    const int test_mul_value = 2; const int counter_start_pos = -1;
    int counter = counter_start_pos; 
    VECTOR_INT_TYPE vec_1 = VECTOR_INT_SET(counter++, counter++, counter++, counter++);  VECTOR_INT_TYPE vec_2 = VECTOR_INT_SET1(test_mul_value);; 
    //! --- 
    VECTOR_INT_TYPE vec_result = VECTOR_INT_MUL(vec_1, vec_2);
    VECTOR_INT_PRINT(vec_result);
    //! ---
    //! Revert the buffer, ie, to simplify testing:
    //VECTOR_INT_PRINT(vec_result);
    //vec_result = _mm_bswap_epi16(vec_result);
    vec_result = VECTOR_INT_revertOrder(vec_result);
    VECTOR_INT_PRINT(vec_result);
    // __m128i _mm_shufflelo_epi16 (__m128i a, int imm8)
    int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_result);
    //! Validate correcgness:
    counter = counter_start_pos; //! ie, reset.
    for(int i = 0; i < VECTOR_INT_ITER_SIZE; i++) {
      const int expected_result = (test_mul_value*counter);
      printf("buffer[%u]=%d =? %d , at %s:%d\n", i, (int)buffer[i], (int)expected_result,__FILE__, __LINE__);
      assert(buffer[i] == expected_result);
      counter++;
    }      
  }


  { //! Valdiate the new-written "VECTOR_INT_DIV(..)" macro


    const int test_mul_value = 2; const int counter_start_pos = 8;
    int counter = counter_start_pos; 
    VECTOR_INT_TYPE vec_1 = VECTOR_INT_SET(counter++, counter++, counter++, counter++);  VECTOR_INT_TYPE vec_2 = VECTOR_INT_SET1(test_mul_value);; 
    //! --- 

    printf("vec1="); VECTOR_INT_PRINT(vec_1);    printf("vec2=");  VECTOR_INT_PRINT(vec_2);

    VECTOR_INT_TYPE vec_result = VECTOR_INT_DIV(vec_1, vec_2);
    VECTOR_INT_PRINT(vec_result);

    //vec_result = _mm_bswap_epi16(vec_result);
    vec_result = VECTOR_INT_revertOrder(vec_result);
    VECTOR_INT_PRINT(vec_result);
    // __m128i _mm_shufflelo_epi16 (__m128i a, int imm8)
    int buffer[VECTOR_INT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec_result);
    //! Validate correcgness:
    counter = counter_start_pos; //! ie, reset.
    for(int i = 0; i < VECTOR_INT_ITER_SIZE; i++) {
      const int expected_result = (counter / test_mul_value);
      printf("buffer[%u]=%d =? %d , at %s:%d\n", i, (int)buffer[i], (int)expected_result,__FILE__, __LINE__);
      assert(buffer[i] == expected_result);
      counter++;
    }      
  }


  { //! Validate correctness of the "VECTOR_INT_STORE(..)" function for 'short int'    
    const char size_of_array = 10; const int default_value = 5;
    { //! For: "int" 
      int *listOf_values_32Bit_int = allocate_1d_list_int(size_of_array, default_value);
      VECTOR_INT_TYPE vec_data = VECTOR_INT_SET1(default_value);
      VECTOR_INT_STORE(listOf_values_32Bit_int, vec_data);
      //! What we expect:
      assert(listOf_values_32Bit_int[0] == default_value);
      free_1d_list_int(&listOf_values_32Bit_int);
    }
    { //! For: "short int" 
      short int *listOf_values_32Bit_int = allocate_1d_list_int_short(size_of_array, default_value);
      VECTOR_INT_TYPE_short vec_data = VECTOR_INT_SET1_short(default_value);
      VECTOR_INT_STORE_short(listOf_values_32Bit_int, vec_data);
      //! What we expect:
      assert(listOf_values_32Bit_int[0] == default_value);
      free_1d_list_int_short(&listOf_values_32Bit_int);
    }
  }
}




// static void  assert_closestPair_code() {

//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
//! @remarks Logics: if(arr_current[bucket_id] < arr_best_value(bucket_id)) {arr_best_value[bucket_id] = score; arr_best_index[bucket_id] = row_id;}
#define __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs, bucket_id, row_id) ({ \
  const VECTOR_FLOAT_TYPE vec_input  = VECTOR_FLOAT_LOAD(&matrixOf_locallyTiled_minPairs[bucket_id]); \
  const VECTOR_FLOAT_TYPE vec_global = VECTOR_FLOAT_LOAD(&mapOf_minTiles_value[bucket_id]); \
  VECTOR_FLOAT_TYPE vec_global_index = VECTOR_FLOAT_LOAD(&mapOf_minTiles_value_atRowIndex[bucket_id]); \
  VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(vec_input, vec_global, vec_global_index,  /*idOf_input=*/row_id); \
  VECTOR_FLOAT_STORE(&mapOf_minTiles_value[bucket_id], vec_min); \
  VECTOR_FLOAT_STORE(&mapOf_minTiles_value_atRowIndex[bucket_id], vec_global_index); \
    })


//   //! --------------------------------------------------
//   //! --------------------------------------------------
//   { //! test sub-parts of the lgocis in our "__intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(..)", and then update our "__forAll_update_globalMin_atBucketIndex(..)".:

//     // FIXME: add something.

//   }

//   assert(false); // FIXME: incldue [ªbove] in our "def_intri.h" ... and then update our "measure_intri.cxx" wrt correctneee-stests.

//   assert(false); // FIXME: move [ªbove] code which is assicated to our "def_intri.h" into a new file named "assert_def_intri.cxx" .. and then call teh latter.
// }


//! @return the 2d-cotneitnecy-table for vertices:
//! @remarks the cotneitngency-table which is used in below is base don the inspraiton of "http://www.ibm.com/support/knowledgecenter/SSLVMB_20.0.0/com.ibm.spss.statistics.help/alg_proximities_binary.htm" and used (amon others) in our "correlationType_proximity.cxx"
#define VECTOR_FLOAT_insideRange_forPair_construct2dContingencyTable(vec_1, vec_2, vec_min, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b) ({ \
      VECTOR_FLOAT_TYPE vec_cmp_1 = VECTOR_FLOAT_insideRange_notRevertOrder_notInvertNan(vec_1, vec_min, vec_max); \
      VECTOR_FLOAT_TYPE vec_cmp_2 = VECTOR_FLOAT_insideRange_notRevertOrder_notInvertNan(vec_2, vec_min, vec_max); \
      sumOf_case_1a = VECTOR_FLOAT_AND(vec_cmp_1, vec_cmp_2);		\
      sumOf_case_1b = VECTOR_FLOAT_ANDNOT(vec_cmp_1, vec_cmp_2); \
      sumOf_case_2a = VECTOR_FLOAT_ANDNOT(vec_cmp_2, vec_cmp_1); \
      sumOf_case_2b = VECTOR_FLOAT_AND(sumOf_case_1a, VECTOR_FLOAT_SET1(0)); \
    })


//! The main assert function.
void assert_intri_main() {

  { //! A set of itnroduction-tests:
    assert(mathLib_float_sqrt(0) == 0);
  }

  { //! TEst a simple "0/value" case (oekseth, 06. nov. 2016):
    const t_float value_inf = INFINITY;
    //    const t_float value_inf = 0/10.0;
    //printf("value_inf=%f, at %s:%d\n", value_inf, __FILE__, __LINE__);
    VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_SET(value_inf, 0, 5.0, -1);
    VECTOR_FLOAT_TYPE vec_result = vec_input;
    //VECTOR_FLOAT_PRINT_wMessage("input: ", vec_result);
    VECTOR_FLOAT_TYPE vec_cmpMIN = VECTOR_FLOAT_CMPLT(vec_result, VECTOR_FLOAT_SET1(T_FLOAT_MAX)); 
    //VECTOR_FLOAT_PRINT_wMessage("test-inf-handling-beforeMerge: ", vec_cmpMIN);
    vec_result = VECTOR_FLOAT_AND(vec_result, vec_cmpMIN);
    //VECTOR_FLOAT_PRINT_wMessage("test-inf-result: ", vec_result);
    for(uint index = 0; index < 4; index++) {
      const t_float input_value = VECTOR_FLOAT_getAtIndex(vec_input, /*index=*/index);
      const t_float local_value = VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index);
      //printf("[%u] = %f (vs. orig=%f), at %s:%d\n", index, local_value, buffer[index], __FILE__, __LINE__);
      if(isinf(input_value)) { assert(local_value == 0);}
      else {
	if(!isnan(local_value)) {
	  assert(local_value == input_value);
	}
      }
    }
  }

  { //! Test the diffnere tmulliplcaion-fucntison wrt. the "int-16" SSE-intrisinittics (oekseth, 06. otk. 2016):
    const uint local_index = -1; //! ie, as we expec that 'negavie numbers' are to be correctly/proeprly handlec
    VECTOR_INT16_TYPE vec_mul = VECTOR_INT16_setFrom_indexRange(local_index);
    
    VECTOR_INT16_PRINT(VECTOR_INT16_MUL(vec_mul, vec_mul)); //! and from 'this' we observe that the result correpons to an 'ordinary' muliplicaiton.

    /* vec_mul = VECTOR_INT16_setFrom_indexRange(local_index); */
    /* VECTOR_INT16_PRINT(_mm_mullo_epi16(vec_mul, vec_mul)); */
    /* VECTOR_INT16_PRINT(_mm_mulhi_epi16(vec_mul, vec_mul)); */
    
    // assert(false); // FIXEM: remove.
  }

  { //! Test the application/use of explict masks:
    { //! Test wrt. char-masks:
      // VECTOR_FLOAT_convertFrom_CHAR
	const char default_value_char = 10; const uint size = 4;
	char *result = allocate_1d_list_char(size, default_value_char);
	uint cnt = 0; result[cnt] = cnt++; result[cnt] = cnt++; result[cnt] = cnt++; result[cnt] = cnt++; 
	VECTOR_FLOAT_TYPE vec_result =  VECTOR_FLOAT_convertFrom_CHAR_buffer(result);
	for(uint index = 0; index < 4; index++) {
	  assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == (t_float)result[index]);
	}

	//! De-allocate:
	free_1d_list_char(&result);
    }
    { //! 
      //! Note: this test is written as we in [”elow]/other tests observe that the "VECTOR_FLOAT_SET" resutls in an 'inversion' when pritned out
      { //! A simplictic case for: "VECTOR_FLOAT_LOAD(..)":
	const t_float default_value_float = 10; const uint size = 4;
	t_float *result = allocate_1d_list_float(size, default_value_float);
	uint cnt = 0; result[cnt] = cnt++; result[cnt] = cnt++; result[cnt] = cnt++; result[cnt] = cnt++; 
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOAD(result);
	for(uint index = 0; index < 4; index++) {
	  assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == result[index]);
	}

	//! De-allocate:
	free_1d_list_float(&result);
      }
      { //! A simplictic case for: "VECTOR_FLOAT_SET(..)":
	const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {10, 1, 2, NAN};
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[3], buffer[2], buffer[1], buffer[0]);
	//VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
	//VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[3], buffer[2], buffer[1], buffer[0]);
	for(uint index = 0; index < 4; index++) {
	  const t_float local_value = VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index);
	  printf("[%u] = %f (vs. orig=%f), at %s:%d\n", index, local_value, buffer[index], __FILE__, __LINE__);
	  if(!isnan(local_value)) {
	    assert(local_value == buffer[index]);
	  }
	}
      }
    }
    { //! Test the filtering of NAN-values
      const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {10, 1, 2, NAN};
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[3], buffer[2], buffer[1], buffer[0]);
      //VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
      //! --------
      VECTOR_FLOAT_PRINT_wMessage("vec_result:", vec_result);

      //printf("T_FLOAT_MIN_ABS
      VECTOR_FLOAT_TYPE vec_result_nanFiltered = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_result);

      VECTOR_FLOAT_PRINT_wMessage("vec_result:", vec_result);
      VECTOR_FLOAT_PRINT_wMessage("vec_cmpMIN:", vec_result_nanFiltered);

      for(uint index = 0; index < 4; index++) {
      	//if(mask[index]) {
      	// printf("index=%u, at %s:%d\n", index, __FILE__, __LINE__);
      	if(!isnan(buffer[index])) { //! then we asusem the valeus i of itnerest:
	  assert(VECTOR_FLOAT_getAtIndex(vec_result_nanFiltered, /*index=*/index) == buffer[index]);
      	} else {
      	  assert(VECTOR_FLOAT_getAtIndex(vec_result_nanFiltered, /*index=*/index) == 0);
      	}
      }
    }
    { //! Test the filtering of NAN-values
      const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {10, 1, 2, NAN};
      const t_float default_value_float = 10; const uint size = 4;
      t_float *result = allocate_1d_list_float(size, default_value_float);
      uint cnt = 0; result[cnt] = cnt++; result[cnt] = cnt++; result[cnt] = cnt++; result[cnt] = cnt++; 
      //uint cnt = 0; result[cnt] = buffer[cnt++]; result[cnt] = buffer[cnt++]; result[cnt] = buffer[cnt++]; result[cnt] = buffer[cnt++]; 
      
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[3], buffer[2], buffer[1], buffer[0]);
      //VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
      //! --------
      VECTOR_FLOAT_PRINT_wMessage("vec_result:", vec_result);
      
      //printf("T_FLOAT_MIN_ABS
      VECTOR_FLOAT_TYPE vec_result_nanFiltered = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_result);

      VECTOR_FLOAT_PRINT_wMessage("vec_result:", vec_result);
      VECTOR_FLOAT_PRINT_wMessage("vec_cmpMIN:", vec_result_nanFiltered);

      for(uint index = 0; index < 4; index++) {
      	//if(mask[index]) {
      	// printf("index=%u, at %s:%d\n", index, __FILE__, __LINE__);
      	if(!isnan(buffer[index])) { //! then we asusem the valeus i of itnerest:
	  assert(VECTOR_FLOAT_getAtIndex(vec_result_nanFiltered, /*index=*/index) == buffer[index]);
	  assert(VECTOR_FLOAT_getAtIndex(vec_result_nanFiltered, /*index=*/index) == result[index]);
      	} else {
      	  assert(VECTOR_FLOAT_getAtIndex(vec_result_nanFiltered, /*index=*/index) == 0);
      	}
      }
      //! De-allocate:
      free_1d_list_float(&result);      
    }
    { //! A 'case' wherew we evlaaute the correctness wrt. our 'slow explcit amsk-implemntaiton':
      //! ------------------------------ start: input:
      const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 2, 3};
      const char mask1[VECTOR_FLOAT_ITER_SIZE] = {1, 0, 0, 1};
      const char mask2[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 0, 1}; 
      const t_float buffer1[VECTOR_FLOAT_ITER_SIZE] = {0, 3, 9, 24};
      const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 6, 6, 10};
      { //! Test when the masks are set from a pointer
	//const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0], buffer1[1], buffer1[2], buffer1[3]);
	VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0], buffer2[1], buffer2[2], buffer2[3]);     
	VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	//! ------------------------------ end: input.
	VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(mask1), VECTOR_FLOAT_convertFrom_CHAR_buffer(mask2), vec_mask_combined);
	//!
	//! Validate corre ctness of [ªbove]
	VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	assert(4 == VECTOR_FLOAT_ITER_SIZE);
	const t_float emptyValue_toSet = 0;
	for(uint index = 0; index < 4; index++) {
	  //if(mask[index]) {
	  // printf("index=%u, at %s:%d\n", index, __FILE__, __LINE__);
	  if(mask1[index] && mask2[index]) { //! then we asusem the valeus i of itnerest:
	    assert(VECTOR_FLOAT_getAtIndex(vec_input_1, /*index=*/index) == buffer1[index]);
	    assert(VECTOR_FLOAT_getAtIndex(vec_input_2, /*index=*/index) == buffer2[index]);
	    //assert(VECTOR_FLOAT_getAtIndex(vec_maskCount, /*index=*/index) == 1);	  
	  } else {
	    assert(VECTOR_FLOAT_getAtIndex(vec_input_1, /*index=*/index) == emptyValue_toSet);
	    assert(VECTOR_FLOAT_getAtIndex(vec_input_2, /*index=*/index) == emptyValue_toSet);
	    // assert(VECTOR_FLOAT_getAtIndex(vec_maskCount, /*index=*/index) == 0);	  
	  }
	}
      }

    }
    {   /*! Case: an explicit maks (with valeus set to eitehr "0" or "1"): include the count of itneresting vertices/valeus  */
      //! ------------------------------ start: input:
      const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 2, 3};
      const char mask1[VECTOR_FLOAT_ITER_SIZE] = {1, 0, 0, 1};
      const char mask2[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 0, 1}; 
      const t_float buffer1[VECTOR_FLOAT_ITER_SIZE] = {0, 3, 9, 24};
      const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 6, 6, 10};
      //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
      VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0], buffer1[1], buffer1[2], buffer1[3]);
      VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0], buffer2[1], buffer2[2], buffer2[3]);     
      //! ------------------------------ end: input.

      //!
      //! The Call:
      VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);
      // VECTOR_FLOAT_PRINT_wMessage("vec_maskCount=", vec_maskCount);
      //vec_maskCount = VECTOR_FLOAT_revertOrder(vec_maskCount);


      //!
      //! Validate corre ctness of [ªbove]
      // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);
      // VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
      assert(4 == VECTOR_FLOAT_ITER_SIZE);
      const t_float emptyValue_toSet = 0;
      for(uint index = 0; index < 4; index++) {
	//if(mask[index]) {
	// printf("index=%u, at %s:%d\n", index, __FILE__, __LINE__);
	if(mask1[index] && mask2[index]) { //! then we asusem the valeus i of itnerest:
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_1, /*index=*/index) == buffer1[index]);
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_2, /*index=*/index) == buffer2[index]);
	  assert(VECTOR_FLOAT_getAtIndex(vec_maskCount, /*index=*/index) == 1);	  
	} else {
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_1, /*index=*/index) == emptyValue_toSet);
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_2, /*index=*/index) == emptyValue_toSet);
	  assert(VECTOR_FLOAT_getAtIndex(vec_maskCount, /*index=*/index) == 0);	  
	}
      }      
    }
    {   /*! Case: an explicit maks (with valeus set to eitehr "0" or "1")  */
      //! ------------------------------ start: input:
      const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 2, 3};
      const char mask1[VECTOR_FLOAT_ITER_SIZE] = {1, 0, 0, 1};
      const char mask2[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 0, 1}; 
      const t_float buffer1[VECTOR_FLOAT_ITER_SIZE] = {0, 3, 9, 24};
      const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 6, 6, 10};
      //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
      VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0], buffer1[1], buffer1[2], buffer1[3]);
      VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0], buffer2[1], buffer2[2], buffer2[3]);     
      // VECTOR_FLOAT_TYPE vec_mask_1 = VECTOR_FLOAT_convertFrom_CHAR_buffer(mask1); //VECTOR_FLOAT_SET(mask1[0], mask1[1], mask1[2], mask1[3]);
      // VECTOR_FLOAT_TYPE vec_mask_2 = VECTOR_FLOAT_convertFrom_CHAR_buffer(mask2); //VECTOR_FLOAT_SET(mask2[0], mask2[1], mask2[2], mask2[3]);
      //! ------------------------------ end: input.

      //!
      //! The Call:
      VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);

      //!
      //! Validate corre ctness of [ªbove]
      // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);
      // VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
      assert(4 == VECTOR_FLOAT_ITER_SIZE);
      const t_float emptyValue_toSet = 0;
      for(uint index = 0; index < 4; index++) {
	//if(mask[index]) {
	// printf("index=%u, at %s:%d\n", index, __FILE__, __LINE__);
	if(mask1[index] && mask2[index]) { //! then we asusem the valeus i of itnerest:
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_1, /*index=*/index) == buffer1[index]);
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_2, /*index=*/index) == buffer2[index]);
	} else {
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_1, /*index=*/index) == emptyValue_toSet);
	  assert(VECTOR_FLOAT_getAtIndex(vec_input_2, /*index=*/index) == emptyValue_toSet);
	}
      }
      
      // // FIXME: if [”elow] works whten write diffenre tmacros '... where [ªbove] is used inc obmaitnion with muliplciaton.


      //assert(false);
    }
  }

  { //! Test the application/use of implciit masks:
    
    { /*! Case: an implicit maks (with valeus set to T_FLOAT_MAX) and where we are interested in 'getting' the 'vertex-count' */
      //! ------------------------------ start: input:
      const t_float buffer1[VECTOR_FLOAT_ITER_SIZE] = {0, 3, T_FLOAT_MAX, 6};
      const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, T_FLOAT_MAX};
      VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0], buffer1[1], buffer1[2], buffer1[3]);
      VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0], buffer2[1], buffer2[2], buffer2[3]);
      //! ------------------------------ end: input.
      /*! First combine the masks: */
      VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2);
      VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1); 
      VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2); 
      VECTOR_FLOAT_PRINT_wMessage("vec_maskCount=", vec_maskCount); 
      assert(false);
    }
    { /*! Case: an implciti maks (with valeus set to T_FLOAT_MAX)  */
      //! ------------------------------ start: input:
      const t_float buffer1[VECTOR_FLOAT_ITER_SIZE] = {0, 3, T_FLOAT_MAX, 6};
      const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, T_FLOAT_MAX};
      VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0], buffer1[1], buffer1[2], buffer1[3]);
      VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0], buffer2[1], buffer2[2], buffer2[3]);
      //! ------------------------------ end: input.
      /*! First combine the masks: */
      VECTOR_FLOAT_dataMask_implicit_updatePair(vec_input_1, vec_input_2);
      VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1); 
      VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2); 
      assert(false);
    }
  }

  { //! Test the use of "and" to filter wrt. masks:
    VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_SET(NAN, 1, 0, 1);
    //const VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_SET(1, NAN, 1, 1);
    VECTOR_FLOAT_PRINT_wMessage("cmpord-and=", VECTOR_FLOAT_AND(vec_1, VECTOR_FLOAT_SET1(6)));
    vec_1 = VECTOR_FLOAT_SET(1, 1, 0, 1);
    //const VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_SET(1, NAN, 1, 1);
    VECTOR_FLOAT_PRINT_wMessage("cmpord-and=", VECTOR_FLOAT_AND(vec_1, VECTOR_FLOAT_SET1(6)));

    //assert(false); // FIXME: remvoe    
  }
  { //! Test applications of the isntruction: "VECTOR_FLOAT_CMPORD(..)"
    const VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_SET(NAN, 1, 0, 1);
    const VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_SET(1, NAN, 1, 1);
    const VECTOR_FLOAT_TYPE vec_r = VECTOR_FLOAT_CMPORD(vec_1, vec_2);
    VECTOR_FLOAT_PRINT_wMessage("cmpord=", vec_r);
    VECTOR_FLOAT_PRINT_wMessage("cmpord-or=", VECTOR_FLOAT_OR(vec_r, VECTOR_FLOAT_SET1(1)));
    VECTOR_FLOAT_PRINT_wMessage("cmpord-xor=", VECTOR_FLOAT_XOR(vec_r, VECTOR_FLOAT_SET1(1)));
    VECTOR_FLOAT_PRINT_wMessage("cmpord-and=", VECTOR_FLOAT_AND(vec_r, VECTOR_FLOAT_SET1(6)));
    VECTOR_FLOAT_PRINT_wMessage("cmpord-sum=", VECTOR_FLOAT_ADD(vec_r, VECTOR_FLOAT_SET1(1)));
    assert(false); // FIXME: remvoe    
  }
  { //! Test applications of the isntruction: "VECTOR_FLOAT_CMPORD(..)"
    const VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_SET(0, 1, 0, 1);
    const VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_SET(1, 0, 1, 1);
    const VECTOR_FLOAT_TYPE vec_r = VECTOR_FLOAT_CMPORD(vec_1, vec_2);
    VECTOR_FLOAT_PRINT_wMessage("cmpord=", vec_r);
    assert(false); // FIXME: remvoe
    
  }

  { //! Test simplicit cases (oekseth, 06. setp. 2106):
    //! Note: written as a 'base' for our "correlationType_proximity.cxx":

    // { //! Extend the masking to test for valeu-range wrt.: exlcit masks:
    //   {
    // 	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_SET(1, 2, 3, 4);
    // 	VECTOR_FLOAT_PRINT_wMessage("----\ninput=", vec_value);
    // 	VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_SET1(1);   VECTOR_FLOAT_TYPE vec_max = VECTOR_FLOAT_SET1(5);
    // 	VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_insideRange(vec_value, vec_min, vec_max);
    // 	//VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_CMPEQ(vec_gt, vec_lt);
    // 	VECTOR_FLOAT_PRINT_wMessage("result-eq-inversed", vec_eq);
    //   }      
    // }
    // assert(false);


    // { //! Extend the masking to test for valeu-range wrt.: implicit masks:

    // }
    // assert(false);


    { //! Test masking wrt. implict mask-valeus.
      //! Status: works (oekseth, 06. sept. 2016)
      { //! A test-case:
	const t_float emptyValue_toSet = T_FLOAT_MAX;
	VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
	const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 2, T_FLOAT_MAX};
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
	

	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_implicitMask(vec_input, VECTOR_FLOAT_SET1(T_FLOAT_MAX), vec_spec_emptyValue);
	
	//vec_mask = VECTOR_FLOAT_AND(vec_mask, VECTOR_FLOAT_SET1(1));
	// vec_mask = VECTOR_FLOAT_ANDNOT(vec_mask, VECTOR_FLOAT_SET1(INFINITY));	
	
	//! 
	//! What we expect:	
	if(false) {VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);}
	assert(4 == VECTOR_FLOAT_ITER_SIZE);
	for(uint index = 0; index < 4; index++) {
	  //if(mask[index]) {
	  if(buffer[index] != T_FLOAT_MAX) { //! then we asusem the valeus i of itnerest:
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == buffer[index]);
	  } else {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == emptyValue_toSet);
	  }
	}
      }
      { //! A test-case:
	const t_float emptyValue_toSet = T_FLOAT_MAX;
	VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
	const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, T_FLOAT_MAX, T_FLOAT_MAX};
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
	

	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_implicitMask(vec_input, VECTOR_FLOAT_SET1(T_FLOAT_MAX), vec_spec_emptyValue);
	
	//vec_mask = VECTOR_FLOAT_AND(vec_mask, VECTOR_FLOAT_SET1(1));
	// vec_mask = VECTOR_FLOAT_ANDNOT(vec_mask, VECTOR_FLOAT_SET1(INFINITY));	
	
	//! 
	//! What we expect:	
	if(false) {VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);}
	for(uint index = 0; index < 4; index++) {
	  //if(mask[index]) {
	  if(buffer[index] != T_FLOAT_MAX) { //! then we asusem the valeus i of itnerest:
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == buffer[index]);
	  } else {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == emptyValue_toSet);
	  }
	}
      }
    }






    { //! test: "VECTOR_FLOAT_func_maskTransform_explcitToImplicit(..)"
      //! Status: works (oekseth, 06. sept. 2016)
      { //! Test explcit maksing:
	const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 2, 3};
	const char mask[VECTOR_FLOAT_ITER_SIZE] = {1, 0, 0, 1};
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
	VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_SET(mask[0], mask[1], mask[2], mask[3]);
	const t_float emptyValue_toSet = T_FLOAT_MAX;
	VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
	//! The operation:
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, vec_mask, vec_spec_emptyValue);

	//! 
	//! What we expect:
	if(false) {VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);}
	for(uint index = 0; index < 4; index++) {
	  if(mask[index]) {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == buffer[index]);
	  } else {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == emptyValue_toSet);
	  }
	}
      }
      { //! Test explcit maksing:
	const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, 1, 2, 3};
	const char mask[VECTOR_FLOAT_ITER_SIZE] = {1, 0, 1, 1};
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
	VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_SET(mask[0], mask[1], mask[2], mask[3]);
	const t_float emptyValue_toSet = T_FLOAT_MAX;
	VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
	//! The operation:
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, vec_mask, vec_spec_emptyValue);

	//! 
	//! What we expec:
	if(false) {VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);}
	for(uint index = 0; index < 4; index++) {
	  if(mask[index]) {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == buffer[index]);
	  } else {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == emptyValue_toSet);
	  }
	}
      }
      { //! Test explcit maksing:
	const t_float buffer[VECTOR_FLOAT_ITER_SIZE] = {0, -21, 2, 300};
	const char mask[VECTOR_FLOAT_ITER_SIZE] = {1, 0, 1, 1};
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_SET(buffer[0], buffer[1], buffer[2], buffer[3]);
	VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_SET(mask[0], mask[1], mask[2], mask[3]);
	const t_float emptyValue_toSet = T_FLOAT_MAX;
	VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
	//! The operation:
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, vec_mask, vec_spec_emptyValue);

	//! 
	//! What we expec:
	if(false) {VECTOR_FLOAT_PRINT_wMessage("result: ", vec_result);}
	for(uint index = 0; index < 4; index++) {
	  if(mask[index]) {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == buffer[index]);
	  } else {
	    assert(VECTOR_FLOAT_getAtIndex(vec_result, /*index=*/index) == emptyValue_toSet);
	  }
	}
      }
    }


    { //! Comarpe strategies for implict mask-filtering:

    }
    assert(false);

    { //! STrategies for merging of enties, eg, after mask-application:
      //! Note: Comarpe strategies for explicit mask-filtering:
      VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_SET(1, 2, 3, 4);
      VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_SET(1,  0, 1, 0);	
      VECTOR_FLOAT_TYPE vec_mask_2 = VECTOR_FLOAT_SET(1,  1, 0, 0);	
      VECTOR_FLOAT_PRINT_wMessage("masks-combined=", VECTOR_FLOAT_AND(vec_mask, vec_mask_2));
      VECTOR_FLOAT_PRINT_wMessage("value: ", vec_value);
      VECTOR_FLOAT_PRINT_wMessage("mask: GR", VECTOR_FLOAT_CMPGT(vec_value, vec_mask));
      VECTOR_FLOAT_PRINT_wMessage("mask: OR", VECTOR_FLOAT_OR(vec_value, vec_mask));
      VECTOR_FLOAT_PRINT_wMessage("mask: XOR", VECTOR_FLOAT_XOR(vec_value, vec_mask));
      VECTOR_FLOAT_PRINT_wMessage("mask: and", VECTOR_FLOAT_AND(vec_value, vec_mask));
      VECTOR_FLOAT_PRINT_wMessage("mask: andNot", VECTOR_FLOAT_ANDNOT(vec_value, vec_mask));
    }

    {
      //! Comapre with NAN, usign min and max to filter wrt. entites:
      //! Note[observe]: "T_FLOAT_MAX < INFINITY"
      //! Note[observe]: ... 
      //! Note[observe]: ... 
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_SET(INFINITY, 2, 3, NAN);
	VECTOR_FLOAT_PRINT_wMessage("\n\ninp: ", vec_value);
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-min", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("min: for 'input' VS float-min", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("--\nmin: for 'input' VS float-max", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-max", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'INFINITY'", VECTOR_FLOAT_AND(vec_value, VECTOR_FLOAT_SET1(INFINITY)));
	VECTOR_FLOAT_TYPE vec_andNot_inf = VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(INFINITY));
	VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'INFINITY'", vec_andNot_inf);
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'NAN'", VECTOR_FLOAT_AND(vec_value, VECTOR_FLOAT_SET1(NAN)));
	VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'NAN'", VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(NAN)));
	//! Try to set the non-values to "0":
	VECTOR_FLOAT_TYPE vec_andNot_inf_inv = VECTOR_FLOAT_AND(vec_andNot_inf, vec_andNot_inf);
	//VECTOR_FLOAT_TYPE vec_andNot_inf_inv = VECTOR_FLOAT_AND(vec_andNot_inf, VECTOR_FLOAT_SET1(0));
	VECTOR_FLOAT_PRINT_wMessage("vec_andNot_inf_inv=", vec_andNot_inf_inv);
	VECTOR_FLOAT_PRINT_wMessage("vec_andNot_inf_inv and 'input'", VECTOR_FLOAT_AND(vec_andNot_inf_inv, vec_value));
	VECTOR_FLOAT_PRINT_wMessage("vec_andNot_inf_inv andNot 'input'", VECTOR_FLOAT_ANDNOT(vec_andNot_inf_inv, vec_value));
	VECTOR_FLOAT_PRINT_wMessage("vec_andNot_inf_inv or 'input'", VECTOR_FLOAT_OR(vec_andNot_inf_inv, vec_value));
	VECTOR_FLOAT_PRINT_wMessage("vec_andNot_inf_inv xor 'input'", VECTOR_FLOAT_XOR(vec_andNot_inf_inv, vec_value));

      }
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_SET(INFINITY, -1, 0.3, NAN);
	VECTOR_FLOAT_PRINT_wMessage("\n\ninp: ", vec_value);
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-min", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("min: for 'input' VS float-min", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("--\nmin: for 'input' VS float-max", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-max", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'INFINITY'", VECTOR_FLOAT_AND(vec_value, VECTOR_FLOAT_SET1(INFINITY)));
	VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'INFINITY'", VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(INFINITY)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'NAN'", VECTOR_FLOAT_AND(vec_value, VECTOR_FLOAT_SET1(NAN)));
	VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'NAN'", VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(NAN)));
      }
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_DIV(VECTOR_FLOAT_SET1(0), VECTOR_FLOAT_SET1(1));
	VECTOR_FLOAT_PRINT_wMessage("\n\ninp div-on-zero: ", vec_value);
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-min", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("min: for 'input' VS float-min", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("--\nmin: for 'input' VS float-max", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-max", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'input'", VECTOR_FLOAT_AND(vec_value, vec_value));
      }
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_DIV(VECTOR_FLOAT_SET1(0), VECTOR_FLOAT_SET1(0));
	VECTOR_FLOAT_PRINT_wMessage("\n\ninp zero-on-zero: ", vec_value);
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-min", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("min: for 'input' VS float-min", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("--\nmin: for 'input' VS float-max", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-max", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'input'", VECTOR_FLOAT_AND(vec_value, vec_value));
      }
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_DIV(VECTOR_FLOAT_SET1(1), VECTOR_FLOAT_SET1(0));
	VECTOR_FLOAT_PRINT_wMessage("\n\ninp div-by-zero: ", vec_value);
	VECTOR_FLOAT_PRINT_wMessage("max: for 'input' VS float-min", VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("min: for 'input' VS float-min", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));
	VECTOR_FLOAT_PRINT_wMessage("--\nmin: for 'input' VS float-max", VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MAX)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'input'", VECTOR_FLOAT_AND(vec_value, vec_value));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'INFINITY'", VECTOR_FLOAT_AND(vec_value, VECTOR_FLOAT_SET1(INFINITY)));
	VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'INFINITY'", VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(INFINITY)));
	VECTOR_FLOAT_PRINT_wMessage("and: for 'input' VS 'NAN'", VECTOR_FLOAT_AND(vec_value, VECTOR_FLOAT_SET1(NAN)));
	VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'NAN'", VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(NAN)));

	
      }
      printf("-------------------------\n");


      //! Describe how: NAN may be 'removed'/'cleaned' from a data-set. <-- for NAN both "VECTOR_FLOAT_MAX(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN))" and "VECTOR_FLOAT_MIN(vec_value, VECTOR_FLOAT_SET1(T_FLOAT_MIN)));" works. Hoever, for INFINITY we need to ...??... <-- goal is to avodi using 'muliplicaiton' ... adn ot try minizmins 'the cost of this' (When comapred to the 'sourring operations which we anyhow need to do').
      assert(false); // FIXME [ªbove] ... eg, 'after' use of 'driect' MASK-list-attributes. 
      //! Describe how: INFINITY may be 'removed'/'cleaned' from a data-set.
      assert(false); // FIXME [ªbove]:
      //! Describe how: NAN may be coverted to '0' ... eg, for "and" when suing a direct/explict mask-list.
      assert(false); // FIXME [ªbove]:

      //! Describe how ... try using the followign example-strategy to clear INFINITY and NAN as a strategy wrt. ...??... VECTOR_FLOAT_PRINT_wMessage("and-not: for 'input' VS 'INFINITY'", VECTOR_FLOAT_ANDNOT(vec_value, VECTOR_FLOAT_SET1(INFINITY))); <--- consider using the latter as a 'mask' .... update our 'euclid' SSE-code wrt. the latter ... .and thereafter consider updaing our 'internal' SSE-matro-functions
      assert(false); // FIXME [ªbove]:
      //! Describe how ... 
      assert(false); // FIXME [ªbove]:
	assert(false);
    }

    { //! Validate correctness of oru new-added "VECTOR_FLOAT_insideRange(..)" macro:
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_SET(1, 2, 3, 4);
	VECTOR_FLOAT_PRINT_wMessage("----\ninput=", vec_value);
	VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_SET1(1);   VECTOR_FLOAT_TYPE vec_max = VECTOR_FLOAT_SET1(3);
	VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_insideRange(vec_value, vec_min, vec_max);
	//VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_CMPEQ(vec_gt, vec_lt);
	VECTOR_FLOAT_PRINT_wMessage("result-eq-inversed", vec_eq);
      }      
      {
	VECTOR_FLOAT_TYPE vec_value = VECTOR_FLOAT_SET(1, 2, 3, 4);
	VECTOR_FLOAT_PRINT_wMessage("----\ninput=", vec_value);
	VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_SET1(1);   VECTOR_FLOAT_TYPE vec_max = VECTOR_FLOAT_SET1(5);
	VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_insideRange(vec_value, vec_min, vec_max);
	//VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_CMPEQ(vec_gt, vec_lt);
	VECTOR_FLOAT_PRINT_wMessage("result-eq-inversed", vec_eq);
      }      
    }
    { //! Validate correctness of oru new-added "VECTOR_FLOAT_insideRange_forPair_construct2dContingencyTable(..)" macro:

      //! Note: need to validate that ....
      assert(false); // [ªbove];:

      { //! Validate 
	
	
      }
    }
    
  }

  // //! Evalute intri-operaitons used in our "s_dense_closestPair" impelmetnation.
  // assert_closestPair_code();

  assert(false); 

  //! Evaluate different appraoches for applicaiton/inference of masks:
  assert_mask_operations();


  //! Evaluate different appraoches for basic artimetic operations:
  assert_basic_artimetics();



  //! test simple/basis operations:
  __assert_simpleOperations();


  
  // FIXME: call thsi funciton

  if(true) {
#define test_sum(value) ({value * 2;})
    const t_float test_value = 1;
    const VECTOR_FLOAT_TYPE vec_test = VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(VECTOR_FLOAT_SET1(test_value), test_sum);
    
    const t_float value_atFirstIndex = VECTOR_FLOAT_getAtIndex(vec_test, /*index=*/0);
    assert(value_atFirstIndex == test_sum(test_value));
  }

  // FIXME: remove [”elow] block: <-- include [”elow] into our ...?..
  if(false) {
    const uint size=20000;
    t_float *test = allocate_1d_list_float(/*size=*/size, /*empty-value=*/0); 
    //t_float *test = (t_float *) malloc ((size) * sizeof(t_float));
    for(uint i = 0; i < size; i++) {test[i] = i;}
    t_float sum = 0;  for(uint i = 0; i < size; i++) {sum += test[i];}
    fprintf(stderr, "sum =%f, <-- remove this line, at %s:%d\n", sum, __FILE__, __LINE__); 
  }

  if(false) {
    { //! Valdiate that min-funciton
      //! Some basic tests:
      assert(-1 < 0);
      assert(-.00011 < 0);
      assert(-1 < FLT_MIN);
      assert(-1 > (-1.0*FLT_MAX));
      assert(-1 > (T_FLOAT_MIN_ABS));
      assert(-.00011 < 0);

      //! The the min-function:
      assert(2 == VECTOR_FLOAT_storeAnd_horizontalMax(VECTOR_FLOAT_SET1(2.0)));
    }
    union converter{
      float f_val;
      unsigned int u_val;
    };
    
    union converter a;
    a.f_val = 123.1443f;
    printf("my hex value %x , at %s:%d\n\n", a.u_val, __FILE__, __LINE__);


    // FIXME: write 'specpic case' wrt. 't_float = double'
    assert(sizeof(t_float) == sizeof(int)); 

    //float scalar = NAN;
    //printf("hex is %x\n", *(unsigned int*)&scalar);
  
    /* const t_float log_scalar = NAN; //log_range_0_one_abs(prob); */
    /* assert(sizeof(t_float) == sizeof(int));  /\* FIXME: remove this to ...??...  *\/ */
    // printf("NAN is %x and log-hex=%x, at %s:%d\n\n", *(unsigned int*)&scalar, *(unsigned int*)&log_scalar, __FILE__, __LINE__);	
    //printf("internal index=%u for prob=%f w/log %f =?= %f,  at %s:%d\n", (uint)__get_internalIndexOf_float(prob), prob, log_scalar, log(prob), __FILE__, __LINE__);
    
    
    /* const int is_nan = (*(unsigned int*)&scalar == *(unsigned int*)&log_scalar); */
    /* const int is_nan_macro = VECTOR_FLOAT_boolean_scalar_isNAN(log_scalar); */
    /* assert(is_nan == is_nan_macro); */
  }

  if(false) { //! validate correctness of allcoating 2d-lists of memory wrt. SSE intrinsics.     
    {
      /* const uint size_in = 10;  */const uint size_outer = 23; const float block_size = 4;
      //uint total_size = 0; /*! which is 'set' int the "__macroInline_getOffset_AlignedMemory(..)" macro */
      //! Note: in [”elow] expected: "delta" to be "(6.0 - 5.75) = 0.25"
      //! Note: in [”elow] expected: "offset" to be "0.25 * 4 = 1"
      uint size_out_upd = __macroInline_getSizeOut_AlignedMemory(size_outer, block_size);
      //const float factor_floor = (float)floor(factor);

      // printf("delta=%f, offset=%u, at %s:%d\n", delta, offset, __FILE__, __LINE__);
      //      const uint offset = (uint)(ceil( (factor - factor_floor)*block_size));
      assert(size_out_upd == 24); //! ie, to get the result of "21 + offset = 24"
      //assert(total_size == ( (size_outer + offset)*size_in) );
    }
    {
      const uint size_outer = 19; const float block_size = 8;
      /* const float factor = (float)size_outer / (float)block_size; const float factor_floor = (float)floor(factor); */
      /* const uint offset = (uint)(ceil( (factor - factor_floor)*block_size));
      **/
      uint size_out_upd = __macroInline_getSizeOut_AlignedMemory(size_outer, block_size); 
      assert( ((uint)size_out_upd % (uint)block_size) == 0);
      assert(size_out_upd == 24); 
    }
    assert(false); // FIXME: mvoe [above] to our "def_memAlloc.h" ... and then thereafter update (a) our "assert_intri.cxx" and our "kt_graphAlgorithms/" directory.
  }





  assert(false); // FIXME: add soemthing
}

