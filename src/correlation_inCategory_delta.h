#ifndef correlation_inCategory_delta_h
#define correlation_inCategory_delta_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlation_inCategory_delta
   @brief provide logics for computation of ranks, lgoics to be used/integrated in all-against-all-compairosn
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/


#include "correlation_rank.h"
#include "correlation_inCategory_delta_tileMacros.h"
#include "correlation_s_allAgainstAll_SIMD_inlinePostProcess.h"
#include "s_kt_correlationConfig.h"
#include "e_template_correlation_tile.h"
#include "s_allAgainstAll_config.h"
#include "s_kt_computeTile_subResults.h"


#if(VECTOR_FLOAT_ITER_SIZE == 4)  //! which we 'use' in order to simplify our testing of non-optmal-SSE-impelmetnations:
//! A slow 'version' of our "get_sumOf_values_denom_maskNotSet(..)"
t_float get_sumOf_values_denom_maskNotSet_slow_SSE(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1);
#endif
//! @return the sum of values for index1
t_float get_sumOf_values_denom_maskNotSet(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1);
//! @return the sum of values for index1
t_float get_sumOf_values_denom_mask_implicit(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1);
//! @return the sum of values for index1
t_float get_sumOf_values_denom_mask_explicit(const uint index1, const uint ncols, const uint ncols_intri, const t_float *row, const t_float weight[], t_float *scalar_denom1, const char *row_mask);


//! Then compute an 'inner square' of sums, where we expect all values to be of interest:
void kt_computeTile_xmtPostPostCompute_notMask(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const uint SM, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult);
//! Then compute an 'inner square' of sums, where we expect all values to be of interest:
void kt_computeTile_xmtPostPostCompute_maskImplicit(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const uint SM, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult);
//! Then compute an 'inner square' of sums, where we expect all values to be of interest:
void kt_computeTile_xmtPostPostCompute_maskExplicit(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const uint SM, char** mask1, char** mask2, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult);

//! @return the assicated and computed correlation-score.
t_float compute_score_correlation(t_float result, const t_float sum1, const t_float sum2, const t_float tweight, t_float denom1, t_float denom2, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const uint sizeOf_i);

//! Update, ie, adjust the score by the weight
void compute_score_correlation_forMatrix(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, t_float **resultMatrix, const bool transpose,  const t_float weight[], const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const bool needTo_useMask_evaluation, const uint iterationIndex_2);

//! Udate the result, ie, as we only computed [above] for 'half of the entries':
void update_cells_above_firstFoorLoop(const uint size_ncols_nrows,const uint size_ncols_nrows_2, t_float **resultMatrix, const bool transpose, const bool isTo_use_speedTest_updateOrder_in_out, const bool isTo_use_speedTest_inrisnistics);




#endif //! EOF
