const uint CLS = 64; 	

{ const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-none::weight-none): idneitfy the time-cost of the 2d-sorting, ie, for which the difference will rpovide3 incdicaitons of the maximum-perofmrance-optismiation which may ge gained from an SSE-memory-tilintg-approach;";
  t_float **matrix_tmp_1 = NULL;  t_float **matrix_tmp_2 = NULL;
  //! Allocate memory and compute the rank for the matrices.
  //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  ktCorrelation_compute_rankFor_matrices(nrows, ncols, matrix, matrix, mask1, mask2, &matrix_tmp_1, &matrix_tmp_2, /*transpose=*/false, /*needTo_useMask_evaluation=*/false, UINT_MAX);  
  assert(matrix_tmp_2 == matrix_tmp_1); //! ie,a swe we provdie the same matrix as input
  assert(matrix_tmp_1); free_2d_list_float(&matrix_tmp_1, nrows);
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  //if(matrix_tmp_2) {assert(matrix_tmp_2); free_2d_list_float(&matrix_tmp_2);}
}
{ const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-none::weight-none::transposed): idneitfy the time-cost of the 2d-sorting, ie, for which the difference will rpovide3 incdicaitons of the maximum-perofmrance-optismiation which may ge gained from an SSE-memory-tilintg-approach;";
  t_float **matrix_tmp_1 = NULL;  t_float **matrix_tmp_2 = NULL;
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  //! Allocate memory and compute the rank for the matrices.
  //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
  ktCorrelation_compute_rankFor_matrices(nrows, ncols, matrix, matrix, mask1, mask2, &matrix_tmp_1, &matrix_tmp_2, /*transpose=*/true, /*needTo_useMask_evaluation=*/false, UINT_MAX);  
  assert(matrix_tmp_2 == matrix_tmp_1); //! ie,a swe we provdie the same matrix as input
  assert(matrix_tmp_1); free_2d_list_float(&matrix_tmp_1, nrows);
  //if(matrix_tmp_2) {assert(matrix_tmp_2); free_2d_list_float(&matrix_tmp_2);}
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
}

if( (nrows > CLS) && (ncols > CLS)) { //! ------------------------------------------
  const char *stringOf_measureText_base = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-none::weight-none) time-cost assicated to the correlation-2d-iteration-step: evlauated the isoclated time-cost ingoring the time assicated to the sort-rank procedure";
  //! Generate the measurement-test:
  char stringOf_measureText[2000]; memset(stringOf_measureText, '\0', 2000);
  const char *centrality_metric_type = "float";
  const bool config_useTwoMatricesAsInput = false;
  sprintf(stringOf_measureText, "%s for correlation-metric: %s and type=%s; use-two-differentMatrices-as-inpnut='%s'", 
	  stringOf_measureText_base, "Euclid", centrality_metric_type,
	  (config_useTwoMatricesAsInput) ? "true" : "false");
	  
  //! Start the clock:
  t_float **matrix_result = allocate_2d_list_float(nrows, size_of_array, default_value_float);
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  //!
  //!
  //! The experiemnt:    
  //printf("---- start euclid-float, at %s:%d\n", __FILE__, __LINE__);
  const uint SM = CLS;
  kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, matrix_result, SM);
  //printf("ok:---- start euclid-float, at %s:%d\n", __FILE__, __LINE__);

  //! --------------
  __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(stringOf_measureText, nrows, size_of_array, SM, mapOf_timeCmp_forEachBucket[chunk_index]); 	  	  
  free_2d_list_float(&matrix_result, nrows);
}
if( (nrows > CLS) && (ncols > CLS)) { const char *stringOf_measureText_base = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-none::weight-none): comptue Spearman using an optmila appraoch where pre-sorting is combined with memory-tiling: for sinmplcity we use  Euclids correlation-emtric in as a mathemaitcla funciton (in the memory-tiling);";
  //! Idneitfy the time-cost assicated to an optmial Separman-impelmetnation, ie, where 

  t_float **matrix_result = allocate_2d_list_float(nrows, size_of_array, default_value_float);
  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
  t_float **matrix_tmp_1 = NULL;  t_float **matrix_tmp_2 = NULL;
  //! Allocate memory and compute the rank for the matrices.
  //! Note: if (data1_input == data2_input) then we only copmtue the rank for data1_input
  ktCorrelation_compute_rankFor_matrices(nrows, ncols, matrix, matrix, mask1, mask2, &matrix_tmp_1, &matrix_tmp_2, /*transpose=*/false, /*needTo_useMask_evaluation=*/false, UINT_MAX);  
  assert(matrix_tmp_2 == matrix_tmp_1); //! ie,a swe we provdie the same matrix as input
  assert(matrix_tmp_1); free_2d_list_float(&matrix_tmp_1, nrows);
  { //! ------------------------------------------
    //! Generate the measurement-test:
    char stringOf_measureText[2000]; memset(stringOf_measureText, '\0', 2000);
    const char *centrality_metric_type = "float";
  const bool config_useTwoMatricesAsInput = false;
    sprintf(stringOf_measureText, "%s for correlation-metric: %s and type=%s; use-two-differentMatrices-as-inpnut='%s'", 
	    stringOf_measureText_base, "Euclid", centrality_metric_type,
	    (config_useTwoMatricesAsInput) ? "true" : "false");
	  
    //! Start the clock:

    //!
    //!
    //! The experiemnt:    
    //printf("---- start euclid-float, at %s:%d\n", __FILE__, __LINE__);
  const uint SM = CLS;
    kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, matrix_result, SM);
    //printf("ok:---- start euclid-float, at %s:%d\n", __FILE__, __LINE__);

    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(stringOf_measureText, nrows, size_of_array, SM, mapOf_timeCmp_forEachBucket[chunk_index]); 	  	  
  }
  free_2d_list_float(&matrix_result, nrows);
}

if( (nrows > CLS) && (ncols > CLS)) 
{ //! Fast implementaiotns of Spearman using KLnight's algorithm:
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-none::weight-none): comptue Spearman's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, /*weight=*/NULL, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----
  if( (nrows > CLS) && (ncols > CLS)) 
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-none): comptue Spearman's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";

    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----
  if( (nrows > CLS) && (ncols > CLS)) 
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-explicit): comptue Spearman's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----
  if( (nrows > CLS) && (ncols > CLS)) 
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::fast::mask-implicit): comptue Spearman's Tau using Knight's algorithm (for which the time-complexity changes from |n*n| to |n*log(n)|);";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
	self.forNonTransposed_useFastImplementaiton = true;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
}
//! ---------------------------------------------- Slow implemetnaiotns using Knedalls 'default' algorithm:
{
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::mask-implicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, weight, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::mask-explicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	assert(mask1[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::mask-none): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, weight, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::dense-matrix::mask-none::weights-none): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
	s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
	self.forNonTransposed_useFastImplementaiton = false;

	//! --------------
	sumOf_values += kt_spearman(/*ncols=*/size_of_array, matrix, matrix, NULL, NULL, NULL, row_id, row_id_out, /*transpose=*/false,  self);

	free_memory__s_allAgainstAll_config(&self);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }  
}
//! ***********************************************************************************************************************
//! --------------------------- Comapre implementations using the implemetnaiton foudn in the "cluster.c" -----------------
{
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::slow::clusterC::dense-matrix::mask-explicit): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//! --------------
	assert(mask1_int[row_id]); 	      assert(mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
	sumOf_values += __kt_spearman_slow(/*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, row_id, row_id_out, /*transpose=*/false);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
#if(__localConfig__in2dSpearman__evalauteTransposedCase  == 1) 
  //! ----------------
  { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::slow::clusterC::dense-matrix::mask-none::transposed): identify the signficance of sorting for non-optimized rank-based correaltion-metrics";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
  	//! --------------
  	sumOf_values += __kt_spearman_slow(/*ncols=*/nrows, matrix_transposed, matrix_transposed, mask1_transposed, mask2_transposed, weight, row_id, row_id_out, /*transpose=*/true);
	//  	sumOf_values += __kt_spearman_slow(/*ncols=*/size_of_array, matrix_transposed, matrix_transposed, mask1_transposed, mask2_transposed, weight, row_id, row_id_out, /*transpose=*/true);
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]);
  }
#endif //! endif("__localConfig__in2dSpearman__evalauteTransposedCase == 1")
}
//! ***********************************************************************************************************************
//! --------------------------- An improvement fo wrt. the if-clause-sentences in the "clsuter.c"

{ //! ***************************************************** test for a 'deidated' all-againsta-ll-function:
  t_float **matrix_result = allocate_2d_list_float(nrows, size_of_array, default_value_float);
  if( (nrows > CLS) && (ncols > CLS)) 
  { //! ------------------- Fast implementaitons:
    { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::fast::maskNone::weightNone): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = true;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, /*weight=*/NULL, matrix_result, e_typeOf_measure_spearman_masksAreAligned_always, /*transpose=*/false, /*metric_id=*/ &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
  if( (nrows > CLS) && (ncols > CLS)) 
    { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::fast::maskNone): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = true;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, matrix_result, e_typeOf_measure_spearman_masksAreAligned_always, /*transpose=*/false, /*metric_id=*/ &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
  if( (nrows > CLS) && (ncols > CLS)) 
    { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::fast::maskImplicit): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = true;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, /*ncols=*/size_of_array, matrix, matrix, /*mask1=*/NULL, /*mask2=*/NULL, weight, matrix_result, e_typeOf_measure_spearman_masksAreAligned_always, /*transpose=*/false, /*metric_id=*/ &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
    { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::fast::maskExplicit): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_true, /*isTo_use_SIMD=*/true, /*typeOf_optimization=*/e_typeOf_optimization_distance_asFastAsPossible);
      self.forNonTransposed_useFastImplementaiton = false;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, /*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, matrix_result, e_typeOf_measure_spearman_masksAreAligned_always, /*transpose=*/false, /*metric_id=*/ &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    }      
  }
  { //! ------------------- Slow implementaitons:
    { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::slow): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
      self.forNonTransposed_useFastImplementaiton = false;
      //! Start the computatiosn:
      ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, /*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, matrix_result,e_typeOf_measure_spearman_masksAreAligned_always,  /*transpose=*/false, /*metric_id=*/ &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
    } 
    { const char *stringOf_measureText = "sort-and-rank-2d(Spearman::slow::transposed): call-3d-func-computations(..): identifies the time-minimization-gains wrt. suing a 'deidcated' function, ie, to minimize the time-overhead wrt. computations";
      //! -------------------------------
      //! Start the clock:
      t_float **matrix_result_transposed = allocate_2d_list_float(ncols, ncols, default_value_float);
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef; 
      s_allAgainstAll_config_t self; init__s_allAgainstAll_config(&self, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, NULL, e_cmp_masksAre_used_false, /*isTo_use_SIMD=*/false, /*typeOf_optimization=*/e_typeOf_optimization_distance_none);
      self.forNonTransposed_useFastImplementaiton = false;
      //! Start the computatiosn:
      printf("(transpose)\tnrows=%u, ncols=%u, at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
      //ktCorr_compute_allAgainstAll_distanceMetric_spearman(size_of_array, /*ncols=*/nrows, matrix, matrix, mask1, mask2, weight, matrix_result, e_typeOf_measure_spearman_masksAreAligned_always, /*transpose=*/true, /*metric_id=*/ &self);
      ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, /*ncols=*/size_of_array, matrix, matrix, mask1, mask2, weight, matrix_result_transposed, e_typeOf_measure_spearman_masksAreAligned_always, /*transpose=*/true, /*metric_id=*/ &self);

      //! --------------
      __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
      free_memory__s_allAgainstAll_config(&self);
      free_2d_list_float(&matrix_result_transposed, nrows);
    } 
  }     
  //! -------------------------------------
  //! De-allcoate locally reserved memory:
  free_2d_list_float(&matrix_result, nrows);
}

//! *****************************************************
#undef __localConfig__in2dSpearman__evalauteTransposedCase
