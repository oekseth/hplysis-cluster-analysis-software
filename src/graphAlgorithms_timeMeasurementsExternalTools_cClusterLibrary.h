#ifndef graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary_h
#define graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary_h

/**
   @file 
   @brief  evaluate/benchmark different sections of the C clsutering library through a combination of different parameters.
**/

#include "graphAlgorithms_aux.h"
//#include "graphAlgorithms_distance.h"
#include "kt_clusterAlg_fixed.h"

/**
   @namespace graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary
   @brief evaluate/benchmark different sections of the C clsutering library through a combination of different parameters.
   @author Ole Kristian Ekseth (oekseth).
**/
namespace graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary {
  void main();
}


#endif
