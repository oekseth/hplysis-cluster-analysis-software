#define __MiC__onlyInterestingcCasesAreOfInterest 1
//!
//! Apply logics:
#include "kt_semanticSim__stub__cmpWrapper__base.c"
t_float retVal_result = T_FLOAT_MAX;
#define __MiF__case(enum_id) obj_metric.metric_id == enum_id
// FIXME[coneptaul]: vlaidte correnctess of [”elow] 'depth(..') itneprtaiotn/view .... consider instead using the 'amx(..)' ... 
//const uint d1 = row_size;     const uint d2 = row_size_2;
#if(__MiB__supportTopologicalMetrics == 1)
const t_float d1 = self->mapVertex_sum[head_1];
const t_float d2 = self->mapVertex_sum[head_1];
const t_float D = self->networkProp_maxDiameter;
#endif
//!
//! Compute: 
if( isOf_interest(result__baseCmp) && (result__baseCmp != 0) ) {
  retVal_result = T_FLOAT_MAX;    
  if( __MiF__case(e_kt_semanticSim_Res)) {    
    // FIXME: update our inti-rotuien to ensure that the 'correct valeus' are set for [below] 
    retVal_result = result__baseCmp; //! ie, as we asusemt aht the distance has been 'cinroapted' into the transitve-clsoreud-sitance-tables.
#if(__MiB__supportTopologicalMetrics == 1)
  } else if(__MiF__case(e_kt_semanticSim_WuP) ||  __MiF__case(e_kt_semanticSim_Lin) || __MiF__case(e_kt_semanticSim_Chen) ) {
    //! Eq:Wup: $2*depth(lcs(c1, c2))/(depth(c1) + depth(c2))$; Cite: Wu Z, Palmer M. Verbs semantics and lexical selection. Proceedings of the 32nd Meeting of Association of Computational Linguistics; 1994; pp. 33–138. [Ref list]
    //! Eq:Lin:  $(2*IC(lcs(c1, c2)))/(IC(c1)+IC(c2))$; Cite: 24. Lin D. An information-theoretic definition of similarity. Proceedings of the International Conference on Machine Learning; 1998; pp. 296–304. URL citeseer.ist.psu.edu/95071.html. [Ref list]
    e_kt_semanticSim_Chen, //! Eq:Chen: $\sum(A_i \cap B_i)/(A + B)$ ["https://academic.oup.com/bioinformatics/article/23/10/1274/197095/A-new-method-to-measure-the-semantic-similarity-of"]
    retVal_result = 2*result__baseCmp/(d1 + d2);
  } else if( __MiF__case(e_kt_semanticSim_LCh)) {
    retVal_result = -1*mathLib_float_log_abs(result__baseCmp/(2*D));
  } else if( __MiF__case(e_kt_semanticSim_NAm)) {    
    //! Eq: $log(2 + (minpath(c1, c2)-1))*(D-d)$, where $d$ is "the depth of the concepts' LCS (d)" ; Cite: Nguyen H, Al-Mubaid H. New ontology-based semantic similarity measure for the biomedical domain. Proceedings of the IEEE International Conference on Granular Computing; 2006; pp. 623–628. [Ref list]    
    // FIXME: figure out how to correctly set the [below] "d" value/parameter ... inveistgate the research-aritlce ... consider seperatley comptuing "lcs(...)"
    const t_float d = 0;
    retVal_result = -1*mathLib_float_log_abs(2 + (result__baseCmp-1)*(D-d) );
    //! Eq: $IC(lcs(c1, c2) = -log(P(lcs(c1, c2)))$; Cite: 22. Resnik P. Using information content to evaluate semantic similarity in a taxonomy. Proceedings of the 14th International Joint Conference on Artificial Intelligence; 1995; pp. 448–453. 
  } else if( __MiF__case(e_kt_semanticSim_JcN)) {    
    //! Eq:Jcn: $1/(IC(c1) + IC(c2) - 2*IC(lcs(c1, c2)))$; Cite: Cite(1): 23. Jiang J, Conrath D. Semantic similarity based on corpus statistics and lexical taxonomy. Proceedings on International Conference on Research in Computational Linguistics; 1997; pp. 19–33. [Ref list ... 
    //! --- 
    t_float de_numerator = d1 + d2 - 2*result__baseCmp;
    if(de_numerator != 0) {
      retVal_result = 1/de_numerator;
    }
    //  } else if( __MiF__case(e_kt_semanticSim_)) {    
#endif
  } else {
    fprintf(stderr, "!!\t Option Not supported: pelase add supprot for thsi option=%u, at %s:%d\n", obj_metric.metric_id, __FILE__, __LINE__);
    assert(false); // FIXME: compelte [”elow] .... "tmp.c" ... add a new enum 'for such'.
  }
 }
//return t_type_max;
  //! Reset macro-varialbes.
#undef __MiF__case
#undef t_type
#undef t_type_max
#undef t_type_min_abs
#undef __MiC__onlyInterestingcCasesAreOfInterest
#undef __MiB__supportTopologicalMetrics
