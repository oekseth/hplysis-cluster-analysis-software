#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_ccm.h"
//#include "export_ppm.c" //! which is used for *.ppm file-export.


//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
static void __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(s_kt_matrix_base_t vec_1, s_kt_list_2d_uint_t list_2d_hypo, e_hpLysis_clusterAlg_t clusterAlg, const char *prefix, const e_kt_correlationFunction_t metric_id) {
  assert(list_2d_hypo.current_pos > 0);
  //! 
  //! Allocate result-matrices:
  s_kt_matrix_t matResult_goldXgold       = initAndReturn__s_kt_matrix(list_2d_hypo.list_size, e_kt_matrix_cmpCluster_metric_undef);
  s_kt_matrix_t matResult_matCCM_hyp      = initAndReturn__s_kt_matrix(list_2d_hypo.list_size, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  s_kt_matrix_t matResult_matCCM_clustRes = initAndReturn__s_kt_matrix(list_2d_hypo.list_size, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  for(uint hypo_id = 0; hypo_id < list_2d_hypo.current_pos; hypo_id++) { //! ie, then evaluate each hypotehsis. 
    assert(vec_1.nrows > 0);
    { //! Set hypotehsis-string:
      char str_local[1000]; memset(str_local, '\0', 1000); sprintf(str_local, "hypo(%u)", hypo_id); 
      set_stringConst__s_kt_matrix(&matResult_goldXgold, hypo_id, str_local, /*addFor_column=*/false);
      set_stringConst__s_kt_matrix(&matResult_matCCM_hyp, hypo_id, str_local, /*addFor_column=*/false);
      set_stringConst__s_kt_matrix(&matResult_matCCM_clustRes, hypo_id, str_local, /*addFor_column=*/false);
    }
    if(hypo_id == 0) { //! then add the column-strings: 
      //! For: gold-CCM:
      for(uint i = 0; i < e_kt_matrix_cmpCluster_metric_undef; i++) {
	const char *str_local = getStringOf__e_kt_matrix_cmpCluster_metric_t__short((e_kt_matrix_cmpCluster_metric_t)i); //! where latter is defined in "kt_matrix_cmpCluster.h"
	set_stringConst__s_kt_matrix(&matResult_goldXgold, i, str_local, /*addFor_column=*/true);
      }
      //! For: matrix-CCM:
      for(uint i = 0; i < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
	const char *str_local = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i); //! where latter is defined in "kt_matrix_cmpCluster.h"
	assert(i < matResult_matCCM_hyp.ncols);
	set_stringConst__s_kt_matrix(&matResult_matCCM_hyp,      i, str_local, /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&matResult_matCCM_clustRes, i, str_local, /*addFor_column=*/true);
      }
    }
    //! 
    //! Compute and add scores: 
    s_kt_matrix_base_t vec_result = vec_1;
    if(metric_id != e_kt_correlationFunction_undef) { //! then we comptue the simalrity: 
      vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.
      s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/metric_id, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
      //! 
      //! 
      //! Apply logics:
      s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
      const bool is_ok = apply__hp_distance(obj_metric, &vec_1, NULL,  &vec_result, /*config=*/hp_config);
      assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
      assert(vec_result.nrows == vec_1.nrows);   assert(vec_result.ncols == vec_1.nrows);
    } else {
      assert(vec_1.nrows == vec_1.ncols); //! ie, as we then assume the inptu-atmrix is an adjcency-matrix.
    }
    //!
    //! Simplify the access.
    s_kt_list_1d_uint_t list_hypo = list_2d_hypo.list[hypo_id];

    //!
    //! 
    for(uint case_id = 0; case_id < 2; case_id++) {
      //! Compute the matrix-based CCMs:
      s_kt_list_1d_float_t list_ccm = setToEmpty__s_kt_list_1d_float_t();
      if(case_id == 0) {
	assert(vec_result.nrows > 0);
	assert(list_hypo.list_size > 0);
	const bool is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_result, list_hypo.list, &list_ccm);
	assert(is_ok);
	assert(list_ccm.list_size == (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef); //! ie, what we expect.
      } else { //! then we compute the cluster-simliarty, and therafter infer CCM-score:
	//!
	//! Infer clusters: 
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	//! Configure:
	obj_hp.config.corrMetric_prior_use = false; //! ie, as we have alredy comptued the simlairty-matrix. 	
	//! Get shallow copy, ie, to satisify the inptu-expecaitosn of our "hpLysis_api.h":
	s_kt_matrix_t mat_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	//! 
	bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_shallow, /*nclusters=*/UINT_MAX, /*npass=*/UINT_MAX);
	assert(is_ok);
	//! Fetch the clsuter-identiigfges:
	//! Note: we expecta taht the k-means-object is set through use of CCMs in parition-idnetificoants of the HCA-tree.
	const s_kt_clusterAlg_fixed_resultObject_t obj_result_kMean = obj_hp.obj_result_kMean;
	uint *vertex_clusterId = obj_result_kMean.vertex_clusterId;
	if(vertex_clusterId != NULL) { //! then clusters were inferred.
	  assert(obj_result_kMean.cnt_vertex <= vec_result.nrows); //! ie, as it may be that Not all vertices are member of mroe clsuters 'than itself'.
	  //!
	  //! Compute CCM-score: 
	  assert(vec_result.nrows > 0);
	  is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_result, vertex_clusterId, &list_ccm);
	  assert(is_ok);
	  { //! then comptue [gold x cluster-result]: 
	    s_kt_list_1d_float_t list_ccm_gold = setToEmpty__s_kt_list_1d_float_t();
	    const uint cnt_vertices = macro_min(obj_result_kMean.cnt_vertex, vec_result.nrows);
	    assert(cnt_vertices > 0);
	    const bool is_ok = ccm__twoHhypoThesis__completeSet__hp_ccm(vertex_clusterId, list_hypo.list, /*cnt_vertices=*/cnt_vertices, 
									/*matrix-1=*/vec_result.matrix, 
									/*matrix-2=*/vec_result.matrix, 
									/*result=*/&list_ccm_gold);
	    assert(is_ok);
	    //! 
	    //! Write out the results: 
	    { //! Explcitly write out the result, where where we ahve sued a CCM-gold to comptue the latter:
	      //! Note: in [”elow] we examplfiy how lgocis in our "kt_matrix_cmpCluster.h" may be used wrt. 'understading' the meaning/stirng-name of the CCM-matrix-enums.
	      for(uint i = 0; i < list_ccm_gold.list_size; i++) {
		const t_float score = list_ccm_gold.list[i];
		if(isOf_interest(score)) { //! then we asusem the results is set:	    
		  assert(hypo_id < matResult_goldXgold.nrows);
		  assert(i < matResult_goldXgold.ncols);
		  //! Set score:
		  matResult_goldXgold.matrix[hypo_id][i] = score;
		}
	      }
	    }
	    //! De-allocate:
	    free__s_kt_list_1d_float_t(&list_ccm_gold);
	  }
	  //!
	  //! De-allocate: 
	  free__s_hpLysis_api_t(&obj_hp);
	}
      }
      //! 
      //! 
      { //! Explcitly write out the result:
	//! Note: in [”elow] we examplfiy how lgocis in our "kt_matrix_cmpCluster.h" may be used wrt. 'understading' the meaning/stirng-name of the CCM-matrix-enums.
	for(uint i = 0; i < list_ccm.list_size; i++) {
	  const t_float score = list_ccm.list[i];
	  if(isOf_interest(score)) { //! then we asusem the results is set:	    
	    if(case_id == 0) {
	      assert(hypo_id < matResult_matCCM_hyp.nrows);
	      assert(i < matResult_matCCM_hyp.ncols);
	      //! Set score:
	      matResult_matCCM_hyp.matrix[hypo_id][i] = score;
	    } else if(case_id == 1) {
	      assert(hypo_id < matResult_matCCM_clustRes.nrows);
	      assert(i < matResult_matCCM_clustRes.ncols);
	      //! Set score:
	      matResult_matCCM_clustRes.matrix[hypo_id][i] = score;
	    } else {assert(false);} //! ie, as we then need adding support for this.
	  }
	}
      }
      //!
      //! De-allocate:
      free__s_kt_list_1d_float_t(&list_ccm); 
    }
    //!
    //! De-allcoate:
    if(vec_1.matrix != vec_result.matrix) { //! then we de-allcoat ehte reuslt-mtairx:
      free__s_kt_matrix_base_t(&vec_result);
    }
    assert(vec_1.nrows > 0);
  }
  //!
  //! Export reulsts:
  {
    char str_local[1000]; memset(str_local, '\0', 1000); sprintf(str_local, "%s_goldXgold.tsv", prefix);
    bool is_ok = export__singleCall__s_kt_matrix_t(&matResult_goldXgold, str_local, NULL);
    assert(is_ok);
  }
  {
    char str_local[1000]; memset(str_local, '\0', 1000); sprintf(str_local, "%s_ccmMatrix_hyp.tsv", prefix);
    bool is_ok = export__singleCall__s_kt_matrix_t(&matResult_matCCM_hyp, str_local, NULL);
    assert(is_ok);
  }
  {
    char str_local[1000]; memset(str_local, '\0', 1000); sprintf(str_local, "%s_ccmMatrix_clust.tsv", prefix);
    bool is_ok = export__singleCall__s_kt_matrix_t(&matResult_matCCM_clustRes, str_local, NULL);
    assert(is_ok);
  }
  //!
  //! De-allocate:
  free__s_kt_matrix(&matResult_goldXgold);
  free__s_kt_matrix(&matResult_matCCM_hyp);
  free__s_kt_matrix(&matResult_matCCM_clustRes);
}


static void __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(s_kt_list_2d_uint_t *self, const uint hypo_id, const uint nrows, const uint block_size, const uint random_cntClusters) {
  assert(hypo_id < self->list_size);
  assert(nrows <= self->list[hypo_id].list_size);
  uint cluster_id = 0; uint cnt_ele = 0;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    //! 
    //! Set cluster-id:
    self->list[hypo_id].list[row_id] = cluster_id;
    //! 
    //! Update cluster-id: 
    if(cnt_ele > block_size) {
      cluster_id++;
      cnt_ele = 0; 
    } else if(block_size == UINT_MAX) {cluster_id = rand() % random_cntClusters;}
    cnt_ele++;
  }
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief demonstrates how different gold-hypotehsis in combiantion with with different CCMs provide new insight into data-sets
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2017).
   @remarks in this example we use (a) one singular matrix, and (b) examplifes how 'this matrix' may be set explcitly, examplfied through/by iteraitng through a for-loop.
   @remarks many-to-many: a simple permtaution of "tut_sim_5_oneToMany_Gower.c" where we comptue for "row(...) x row(...)" wrt. the two input-matrices.
   @remarks general: use logics in our "hp_distance" to comptue the simalrit between two vectors.
   @remarks an example-applicaiton is seen in (one of) our implementaiotns of the "kmeans++" algorithm ("____init_centers_pySciKitLearn__kmeanspp__s_kt_matrix_base_t(..)"). 
   @remarks simliartiy: comptue the sim-matrix="innerproduct--coVariance" between two matrices using our "hp_distance.h" API 
   @remarks "hp_distance": we call the "apply__hp_distance(..)" function;
   @remarks generic: a permtaution of "tut_sim_14_manyMany_Matusita__setInputInLoop.c" and "tut_ccm_2_matrixVSgold_Silhouette.c" where we use CCM-evaluation for clsuter-anlaysis and evaluates the complete set of 'base' simliarty-metrics. 
   @remarks generic: we use the "tut_ccm_13_categorizeData.c" wrt. the iteraiton of simalirty-metrics.
   @remarks generalizaiton(example): to simplify the understanidng and re-use of our appraoch we have strucutred our "tut_sim_*" examples to use the same 'example-seutp', ie, where differneces is found wrt. the API-calls in to our "hp_distance", and wrt. different "s_kt_correlationMetric" calls.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#else
  int tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  e_hpLysis_clusterAlg_t clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  const uint cnt_hypo = 2;
  //const uint nrows = 200; const uint ncols = 1024; 
  //  const uint nrows = 200; const uint ncols = 100; 
  const uint nrows = 20; const uint ncols = 100; 
  //  const uint nrows_2 = 6; 
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/ /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"

  //! Allocate two vectors, ie, matrices with demsinos [1, ncols]:
  //! Note: for simplicty we use [”elow] fucntion, defined in our "kt_matrix_base.h", ie, to set the vectors to default valeus, therby reducing the nubmer of code-lines in this use-case-example; in the latter randomenss is used, ie, for which we expect the vectors to be different.
  assert(nrows > 2);   assert(ncols > 0); //! ie, as we otherise have a poinbtelss' call
  s_kt_matrix_base vec_1 = initAndReturn__s_kt_matrix_base_t(nrows, ncols);
  assert(vec_1.matrix);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      //! Note: in [”elow] we describe/iniclude the 'nvoel part' of this "hp_distance" use-case:
      vec_1.matrix[row_id][col_id] = (1+row_id)*(0.5*col_id); //! ie, set the matrix-value to an 'explcitli/arbitrary/wild-case score' 
    }
  }
  //!
  //! Consturct a hypotehtical clsuter-result-set.
  //! Note: [below] examplfies the use of a hard-coded data-set to evlauate/cosnider the accuracy of pariwise simliarty-metrics:
  assert(vec_1.nrows > 0);
  s_kt_list_2d_uint_t list_hypo = init__s_kt_list_2d_uint_t(cnt_hypo, nrows); //! where latter is deifne din our "kt_list_1d.h".
  { //! Set the scores: for simplicty split the data-set intow two equal parts:
    uint hypo_id = 0;
    {
      assert(hypo_id < list_hypo.list_size);
      uint row_id = 0;
      uint cnt_half = (uint)((t_float)vec_1.nrows * 0.5);
      for(row_id = 0; row_id < cnt_half; row_id++) {
	list_hypo.list[hypo_id].list[row_id] = /*cluster-id=*/0;
      }
      for(; row_id < vec_1.nrows; row_id++) {
	list_hypo.list[hypo_id].list[row_id] = /*cluster-id=*/1;
      }
    }
    hypo_id++;
    {
      assert(hypo_id < list_hypo.list_size);
      uint row_id = 0;
      uint cnt_half = vec_1.nrows; 
      for(row_id = 0; row_id < cnt_half; row_id++) {
	list_hypo.list[hypo_id].list[row_id] = /*cluster-id=*/0;
      }
      /* for(; row_id < vec_1.nrows; row_id++) { */
      /* 	list_hypo.list[hypo_id].list[row_id] = /\*cluster-id=*\/1; */
      /* } */
    }
    hypo_id++;
    list_hypo.current_pos = hypo_id;
  }
  if(true) { 
    //!
    //! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid; //e_kt_correlationFunction_groupOf_;
    //    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
    __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"random", metric_id);
  }

  //!
  //! De-allocate:
  free__s_kt_matrix_base_t(&vec_1);
  free__s_kt_list_2d_uint_t(&list_hypo);
  { //! Then an altetntive file-data-load-strategy:
    const char *config__fileName__binaryMatrix = "data/vilje-cost-rack1-half.dat";
    mdata_t dat;   mdata_initf ( &dat, config__fileName__binaryMatrix );
    if(!dat.n) {
      fprintf(stderr, "(vilje-not-at-path)\t Seems like the cosntructed matrix was empty when laoding from \"%s\", ie, please invesetivgate. OBservaiton at [%s]:%s:%d\n", config__fileName__binaryMatrix, __FUNCTION__, __FILE__, __LINE__);
      return true;      
    }
    //! Itniate:
    s_kt_matrix_t matrix_concatToAll__type__overhead = initAndReturn__s_kt_matrix(dat.n, dat.n);
    s_kt_matrix_t matrix_concatToAll__type__latency = initAndReturn__s_kt_matrix(dat.n, dat.n);
    //!
    //! Load the Vilje super-comptuer matrix (into our two data-strucutres), where logics are foudn in  the "matrixdata.h" written by "jancrhis@ntnu.no":
    for(uint i=0; i<(uint)dat.n; i++ ) {
      for(uint j=0; j<(uint)dat.n; j++ ) {
	matrix_concatToAll__type__overhead.matrix[i][j] = TS(dat,i,j);
	matrix_concatToAll__type__latency.matrix[i][j]  = TL(dat,i,j);
      }
    }
    //! De-allcoate the 'parser':
    mdata_finalize ( &dat );
    const uint nrows = matrix_concatToAll__type__overhead.nrows;
    assert(nrows > 0);
    //    const uint nrows = matrix_concatToAll__type__overhead.nrows;
    const uint cnt_hypo = 9;
    s_kt_list_2d_uint_t list_hypo = init__s_kt_list_2d_uint_t(cnt_hypo, nrows); //! where latter is deifne din our "kt_list_1d.h".
    { //! Set the scores: for simplicty split the data-set intow two equal parts:
      /**
	 @remarks the hypothsis is based on an exmail converiosntion with Dr. Meyer at NTNU/Vilje, a conveirosnationw hich for cliadity is included, though in Norwegian:	 
	 - Når sammenhengende grupper av 8 rader (0-7, 8-15, 16-23, ...) ligner hverandre, fanger vi grupperingen av cores pr. CPU-socket, (k=144 for algoritmer som trenger clusterantall)	 
	 - Når sammenhengende grupper av 16 rader (0-15, 16-31, ... ) ligner, fanger vi cores pr. node (k=72)	 
	 - Sammenhengende grupper av 288 rader er flettet sammen i to 144-grupper; de første 80 skal ligne hverandre, de neste 144 skal ligne hverandre, og de siste 64 skal ligne de første 80 (og hverandre). (k=8). Dette svarer til cores i halve av rack-enheter ("IRU" i artikkelen).	 
	 - Sammenhengende grupper av 288 rader skal ligne hverandre når k=4, dette er cores i hele IRU-er	 
	 - Første og siste halvpart av radene skal ligne hverandre (576 rader hver, k=2)
	 ------------------------------------------
       **/
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/0, nrows, /*block_size=*/8, /*random_cntClusters=*/2);
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/1, nrows, /*block_size=*/16, /*random_cntClusters=*/2);
      //!
      //!
      //__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/2, nrows, /*block_size=*/nrows/2, /*random_cntClusters=*/nrows/2);
      { //! Set the clsuter-dienties into blocks of 'swapping' '80' and '144' block-sizes.
	// ---
	const uint blockSize_1 = 80; const uint blockSize_2 = 144;
	// ---
	s_kt_list_2d_uint_t *self = &list_hypo; const uint hypo_id = 2;
	assert(hypo_id < self->list_size);
	assert(nrows <= self->list[hypo_id].list_size);
	uint cluster_id = 0; uint cnt_ele = 0;
	uint block_size_curr = blockSize_1;
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  //! 
	  //! Set cluster-id:
	  self->list[hypo_id].list[row_id] = cluster_id;
	  //! 
	  //! Update cluster-id: 
	  if(cnt_ele > block_size_curr) {
	    if(block_size_curr == blockSize_1) {block_size_curr = blockSize_2;} else {block_size_curr = blockSize_1;} //! ie, then swap the block-sizes.
	    //! Update teh counter.
	    cluster_id++;
	    cnt_ele = 0; 
	  } //else if(block_size == UINT_MAX) {cluster_id = rand() % random_cntClusters;}
	  cnt_ele++;
	}
      }
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/3, nrows, /*block_size=*/288, /*random_cntClusters=*/2);
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/4, nrows, /*block_size=*/(uint)(t_float)nrows*0.5, /*random_cntClusters=*/2); //! ie, 'split in two.
      //! 
      //! PErmtautisonof randomness: 
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/5, nrows, /*block_size=*/UINT_MAX, /*random_cntClusters=*/nrows/2);
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/6, nrows, /*block_size=*/UINT_MAX, /*random_cntClusters=*/nrows/2);
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/7, nrows, /*block_size=*/UINT_MAX, /*random_cntClusters=*/2);
      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__constructHypo_forBlcokSize(&list_hypo, /*hypo_id=*/8, nrows, /*block_size=*/UINT_MAX, /*random_cntClusters=*/2);
      //! 
      //! 
      list_hypo.current_pos = 9;
    }
    if(false) { //! then compute Euclid and write out the results to a matrix:
      {
	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__overhead);
	//	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__latency);
	{
	  bool is_ok = export__singleCall__s_kt_matrix_t(&matrix_concatToAll__type__overhead, "tmp_input.tsv", NULL);
	  assert(is_ok);
	}
	//	s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__overhead);
	//	const char *file_name_result = "vilje_result_metric_Euclid_latency.tsv";
	const char *file_name_result = "vilje_result_metric_Euclid_overhead.tsv";
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	//	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/metric_id, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"      
	s_hp_distance__config_t hp_config = init__s_hp_distance__config_t(); 
	//!
	//! Compute similairty: 
	s_kt_matrix_base_t vec_result = initAndReturn__empty__s_kt_matrix_base_t();
	bool is_ok = apply__hp_distance(obj_metric, &vec_1, NULL,  &vec_result, /*config=*/hp_config);      
	assert(is_ok);
	//!
	//! Export result: 
	s_kt_matrix_t mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	if(true) {
	  __tut_sim_16_manyMany_singleInput_exportTo_ppm(mat_result, /*file_pref=*/"vilje_overhead");
	} else {
	  fprintf(stderr, "(info)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
	  is_ok = export__singleCall__s_kt_matrix_t(&mat_result, file_name_result, NULL);
	  assert(is_ok);
	}
	//!
	//! De-allocate:
	free__s_kt_matrix_base_t(&vec_result);
      }
    }
    //s_kt_matrix_fileReadTuning_t config = initAndReturn__s_kt_matrix_fileReadTuning_t();    
    //! ----------------------------------------
    //!
    {
      //!
      //! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__overhead);
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_undef; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_overhead_raw", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_overhead_Euclid", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	//const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid; //e_kt_correlationFunction_groupOf_;
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_overhead_Kendall_coVariance", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_overhead_absDiff_Sorensen", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_intersection_WaveHedges; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_overhead_intersection_waveHedges", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_squared_Euclid; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_overhead_squared_Euclid", metric_id);
      }
    //      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg, "vilje_overhead");
    }
    //! ----------------------------------------
    //!
    {
      //!
      //! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__latency);
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_undef; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_latency_raw", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_latency_Euclid", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	//const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid; //e_kt_correlationFunction_groupOf_;
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_latency_Kendall_coVariance", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_latency_absDiff_Sorensen", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_intersection_WaveHedges; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_latency_intersection_waveHedges", metric_id);
      }
      {
	//!
	//! Compute simliarty for the data-set, and therafter apply both matrix-CCM and gold-CCM-evalautions:
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_squared_Euclid; //e_kt_correlationFunction_groupOf_;
	//    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_coVariance; //e_kt_correlationFunction_groupOf_;
	__tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg,"vilje_latency_squared_Euclid", metric_id);
      }
    //      __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg, "vilje_latency");
    }
    /* { */
    /*   s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&matrix_concatToAll__type__latency); */
    /*   __tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs__computeForData(vec_1, list_hypo, clusterAlg, "vilje_latency"); */
    /* } */
    //!
    //! De-allocate:
    free__s_kt_matrix(&matrix_concatToAll__type__overhead);
    free__s_kt_matrix(&matrix_concatToAll__type__latency);
    free__s_kt_list_2d_uint_t(&list_hypo);    
    //    s_kt_matrix_t mat_real = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file=*/"data/vilje-cost-rack1-half.dat", config);
  }
  //free__s_kt_matrix_base_t(&vec_2);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}
 

