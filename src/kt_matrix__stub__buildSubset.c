
#if(__internalMacro__rowsSameAsColumns == 1)
const uint cnt_rows = arrOf_local_vertexMembers_size; const uint cnt_tails = arrOf_local_vertexMembers_size;
#define __local__getHead(local_id) ({__internalMacro__getGlobalVertexFromLocal(local_id);})
#define __local__getTail(local_id) ({__internalMacro__getGlobalVertexFromLocal(local_id);})
#else //! then we explcitly update for the 'heads and tails':
const uint cnt_rows = arrOf_local_vertexMembers_head_size;  const uint cnt_tails = arrOf_local_vertexMembers_tail_size;
#define __local__getHead(local_id) ({__internalMacro__getGlobalVertexFromLocal_head(local_id);})
#define __local__getTail(local_id) ({__internalMacro__getGlobalVertexFromLocal_tail(local_id);})
  //! Note: wrt. the "mapOf_globalTo_local" we only update "mapOf_globalTo_local" for the 'row' case (ie, as we asusem only the 'rows' are used in the result-table, while ie, as the columsn are expected to describe/present the feautres)
#endif

if(mapOf_globalTo_local != NULL) {    
  printf("cnt_rows=%u, at %s:%d\n", cnt_rows, __FILE__, __LINE__);
  for(uint local_id = 0; local_id < cnt_rows; local_id++) {      
    const uint global_vertex = __local__getHead(local_id);
    assert(global_vertex < superset->nrows);
    //printf("global_vertex=%u, at %s:%d\n", global_vertex, __FILE__, __LINE__);
    mapOf_globalTo_local[global_vertex] = local_id;
  }
 }
//! Initaite:
init__s_kt_matrix(self, /*nrows=*/cnt_rows, /*ncols=*/cnt_tails, /*isTo_allocateWeightColumns=*/(superset->weight != NULL));

if(superset->weight != NULL) { //! then copy the weights:
  for(uint local_id = 0; local_id < cnt_tails; local_id++) {      
    const uint global_vertex = __local__getTail(local_id);  
    t_float score = 0;
    //! Udpate the 'new' object with the 'knowledge' from the superset-object.
    get_weight__s_kt_matrix(superset, global_vertex, &score);
    set_weight__s_kt_matrix(self, local_id, score);
  }    
 }
//!
//! Update hte scores:
for(uint local_id = 0; local_id < cnt_rows; local_id++) {      
  const uint global_vertex_head = __local__getHead(local_id);  
  for(uint local_id_tail = 0; local_id_tail < cnt_tails; local_id_tail++) {      
    const uint global_vertex_tail = __local__getTail(local_id_tail);  
    t_float score = 0;
    //! Udpate the 'new' object with the 'knowledge' from the superset-object.	
    get_cell__s_kt_matrix(superset, global_vertex_head, global_vertex_tail, &score);
    set_cell__s_kt_matrix(self, local_id, local_id_tail, score);
  }
 }

//!
//! Udpate the name-mappigns:
if(isTo_updateNames) {
  if(superset->nameOf_rows != NULL) {
    bool addFor_column = false;
    for(uint local_id = 0; local_id < cnt_rows; local_id++) {      
      const uint global_vertex = __local__getHead(local_id);  
      char *stringTo_add = NULL;
      get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
      if(stringTo_add) {
	set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
      }
    }
  }
  //! ---------
  if(superset->nameOf_columns != NULL) {
    bool addFor_column = true;
    for(uint local_id = 0; local_id < cnt_tails; local_id++) {      
      const uint global_vertex = __local__getTail(local_id);  
      char *stringTo_add = NULL;
      get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
      if(stringTo_add) {
	set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
      }
    }
  }
 }

//! Undef-ien the lcoally defined macros:
#undef __local__getHead
#undef __local__getTail
