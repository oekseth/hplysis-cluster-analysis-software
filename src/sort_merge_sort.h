/* #ifndef sort_merge_sort_h */
/* #define sort_merge_sort_h */
/* /\\** */
/*    @file */
/*    @brief Provide a merge-sort implementation, using the statarized enum_compare comparison-runtines. */
/*    @ingroup ontology */
/*    @author Ole Kristian Ekseth (oekseth) */
/* **\\/ */
/* /\\* */
/*  * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com) */
/*  * */
/*  * This file is part of the hpLysis machine learning software. */
/*  * */
/*  * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify */
/*  * it under the terms of the hpLysis documentation  */
/*  *  */
/*  *  */
/*  * */
/*  * the hpLysis machine learning software is distributed in the hope that it will be useful, */
/*  * but WITHOUT ANY WARRANTY; without even the implied warranty of */
/*  v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/*  * hpLysis documentation for more details. */
/*  * */
/*  * You should have received a copy of the hpLysis documentation */
/*  * along with the hpLysis machine learning software.  */
/*  *\\/ */
/* #include "libs.h" */
/* #include "enum_compare.h" */
/* #include "types.h" */

/* /\\** */
/*    @class sort_merge_sort */
/*    @brief Provide a merge-sort implementation, using the statarized enum_compare comparison-runtines.  */
/*    @remarks  */
/*    - Used among others in the list_generic template class. */
/*    - An important property of the merge_sort, regards its low memory-footprint, given its inline procedure of sleecting elemetns, resulting (by convention) into a lwoer cache-miss-rate. */
/*    @ingroup sort */
/*    @author Ole Kristian Ekseth (oekseth) */
/*  *\\/ */
/* template<class type_t> class sort_merge_sort { */
/*  private: */
/*   /\\**! Swaps the elements *\\/ */
/*   inline static void swap(type_t &a, type_t &b) {const type_t temp = b; b = a; a = temp;} */

/*   /\\** */
/*      @brief Divides an array into two parts, and merges these parts */
/*      @param <array> is an array where the sub-parts [p, q] and [q,r] is sorted. */
/*      @param <start_pos> identifies the first element in the first sorted sub-list to merge. */
/*      @param <merge_pos> is partition between the two sublists. */
/*      @param <end_pos> identifies the last element in the second sorted sub-list to merge. */
/*   *\\/ */
/*   static void merge(type_t *array, const uint start_pos, const uint merge_pos, const uint end_pos) { */
/*     const uint size[2] = {merge_pos - start_pos, end_pos - merge_pos }; */
/*     type_t *list[2]; */
/*     // TODO: consider adding/using a try-catch block for [below] memory-allocations: */
/*     list[0] = new type_t[size[0]+1]; for(uint i = 0; i < size[0]; i++) list[0][i] = array[start_pos + i]; */
/*     list[1] = new type_t[size[1]+1]; for(uint i = 0; i < size[1]; i++) list[1][i] = array[merge_pos + i]; */
/*     list[0][size[0]] = type_t(); list[1][size[1]] = type_t(); */
/*     //    printf("\t\t("); for(uint i = 0;i < size[0]+1; i++)printf("%d, ", list[0][i]); printf(")\n"); */
/*     //printf("\t\t("); for(uint i = 0;i < size[1]+1; i++)printf("%d, ", list[1][i]); printf(")\n"); */
/*     uint i = 0, j = 0; */
/*     for(uint k = start_pos; k < end_pos; k++) { */
/*       if(i > size[0]) {array[k] = list[1][j++];} */
/*       else if(j > size[1]) {array[k] = list[0][i++];} */
/*       else {       */
/* 	assert(i <= size[0]);    assert(j <= size[1]); */
/* 	enum compare result_of_comparison = list[0][i].is_bigger_than(list[1][j]);       */
/* 	if(cmp_less_than == result_of_comparison) {array[k] = list[0][i++];} //       if(list[0][i] < list[1][j]) array[k] = list[0][i++]; */
/* 	else {array[k] = list[1][j++];} */
/*       } */
/*     } */
/*     delete [] list[0], delete [] list[1]; */
/*   }  */
/*   /\\** */
/*      @brief Sort the array by using a divide- and conquer-approach. */
/*      @param <array> is an array where the sub-parts [p, q] and [q,r] is sorted. */
/*      @param <start_pos> identifies the first element in the first sorted sub-list to merge. */
/*      @param <merge_pos> is partition between the two sublists. */
/*   *\\/ */
/*   static void merge_procedure(type_t *array, const uint start_pos, const uint end_pos) { */
/*     assert(array); */
/*     if(start_pos < (end_pos-1)) { */
/*       //      printf("p = %u, r = %u\n", p, r);      printf("\n"); */
/*       // FIXME: given our usage, is it possible to utliize the fact that the list is (in several cases) already partially sorted? */
/*       // <-- seems like it should be simple, ie, to investigate the sorted property of [q - <some elemtents>, q + <some elements> */
/*       //     <-- what is the offset (ie, the 'some-elements' in above) we are to use? */
/*       // <-- to validate such an operation, then we'll need to instrospect upon well-working tests. A motivation could here be to observe that the speed-of-sorting is considerably improved when we take this into account, ie to use our log-file as starting-poitn for such-a-task. */
/*       const uint merge_pos = (start_pos + end_pos)/2; */
/*       //      printf("#\tDivides into [%u, %u] and [%u, %u]\n", p, q, q, r); */
/*       merge_procedure(array, start_pos, merge_pos); */
/*       merge_procedure(array, merge_pos, end_pos); */
/*       //printf("(merge procdedure: before- and after for [%u, %u, %u].)\n", p, q, r); */
/*       //printf("\t\t("); for(uint i = p;i < q; i++) printf("%d, ", array[i]); printf(")\n"); */
/*       //printf("\t\t("); for(uint i = q;i < r; i++) printf("%d, ", array[i]); printf(")\n"); */
/*       merge(array, start_pos, merge_pos, end_pos); */
/*       //printf("\t\t("); for(uint i = p;i < r; i++) printf("%d, ", array[i]); printf(")\n"); */
/*       //printf("-----\n"); */
/*     } //else  */
/*   } */
/*  public: */
/*   /\\** */
/*      @brief Sort the array by using a divide- and conquer-approach. */
/*      @param <array> is an array to sort. */
/*      @param <size> is the number of consequative elements in the array (to sort). */
/*   *\/ */
/*   static void sort_array(type_t *array, const uint size) { */
/*     assert(array); */
/*     merge_procedure(array, 0, size); */
/*   } */
/*   /\* void delete_class() {}//{array.delete_class(); } *\/ */
/*   /\* merge_sort() {};//array = list_t(0);} *\/ */
/*  //! The main test function for this class   */
/*  static void assert_class(const bool print_info) { */
/*    if(false && print_info)  {;} */
/*    printf("--\tAsserts class 'merge_sort'.\n"); */
/*    //list_merge_sort<type_uint> temp = merge_sort<type_uint>(); */
/*    const uint array_size = 10; */
/*    type_uint sorted_array_tmp[array_size] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1}; */
/*    type_uint sorted_array[array_size]; */
/*    for(uint i = 0; i < array_size; i++) {sorted_array[i] = type_uint(sorted_array_tmp[i]);} */
/*    int a = 2, b = 4; const int old_a = a, old_b = b; */
/*    type_uint obj_1(a); type_uint obj_2(b); */
/*    sort_merge_sort<type_uint>::swap(obj_1, obj_2);  */
/*    assert(a == old_b && old_a == b); */
/*    sort_merge_sort<type_uint>::sort_array(sorted_array, array_size);     */
/*    //printf("array[%u] = %d\n", 0, sorted_array[0]);  */
/*    for(uint i = 1; i < array_size; i++) { */
/*      //      printf("array[%u] = %d\n", i, sorted_array[i]);  */
/*      enum compare result_of_comparison = sorted_array[i].is_bigger_than(sorted_array[i-1]); */
/*      assert(result_of_comparison != cmp_less_than); */
/*      //if(cmp_greater_than == result_of_comparison) {array[k] = list[0][i++];} //       if(list[0][i] < list[1][j]) array[k] = list[0][i++]; */
/*      //assert(sorted_array[i] >=sorted_array[i-1]); */
/*    } */
/*    //delete [] sorted_array; */
/*    printf("ok\tAssert of class 'merge_sort' complete.\n"); */
/*    return true; */
/*  } */
/* }; */
/* #endif */
