#include "hpLysis_api.h"

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief  examplify different data-nroamzioant-strategies and its implciaotn on kd-tree-clsutering  (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks 
   @remarks related-tut-examples:
   -- "tut_kd_1_cluster_multiple_simMetrics.c": logics to evaluate implicaiton of different patterns. 
**/
int main(const int array_cnt, char **array) 
#else
  int tut_norm_1_differentNormStrategies(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  const char *resultPrefix = "tut_evalKd_";

  //! Fris seperatley for a gvien input-file:
  //! Note: for a list of files to be used see our cofniguraiton-fiels, eg, wrt. our "data/local_downloaded/metaObj.c"
  //const char *file_name = "data/local_downloaded/22_nuts.csv.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
    // const char *file_name = "data/local_downloaded/56_msq.csv.hpLysis.tsv";
    // const char *file_name = "data/local_downloaded/23_Airline.csv.hpLysis.tsv"; //! which examplfies chalelnges in scales (ie, different scalies used at differnt data-axis) ... 
    const char *file_name = "tests/data/kt_mine/birthWeight_chineseChildren.tsv";
    //const char *file_name = "";
    const uint index_pos = 3;
    bool fileIs_speificed = false; //! which is used to simplify error-generiaotn.
    if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) {
      //! then set the file-name:
      file_name = array[index_pos];
      fileIs_speificed = true;
    }
    //!
    //! Load an arbitrary data-set:
    // const uint nrows = 1000; const uint ncols = 20;
    //const uint nrows = 1000; const uint ncols = 20;
    //  const uint nrows = 1000*10; const uint ncols = 5;
    //const uint nrows = 1000*1000; const uint ncols = 20;
    s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols);
    //! Then laod the file:
    assert(file_name); assert(strlen(file_name));
    import__s_kt_matrix_t(&mat_input, file_name);
    if(mat_input.nrows == 0) {
      if(fileIs_speificed) {
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      } else { //! then we asusmet eh file-name was spefieid by the user.
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      }
      return false;
    } else {
      fprintf(stdout, "(info) Opens  input-file=\"%s\". Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
    }

    //!
    //! Apply logics:
    for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {      
      //! Normalize (or optionally copy the non-nroamized data-set):
      s_kt_matrix_t mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_input, (e_kt_normType_t)isTo_normalize);
      assert(mat_data.nrows > 0);
      if(true) { //! then we export the result-file to a tmeprary file:
	char fileName_local[1000]; memset(fileName_local, '\0', 1000);
	sprintf(fileName_local, "result_tut_norm1_useNorm%u.tsv", isTo_normalize);
	printf("export to file=\"%s\", at %s:%d\n", fileName_local, __FILE__, __LINE__);
	export__singleCall__s_kt_matrix_t(&mat_data, fileName_local, NULL);
      }
      //! De-allcoate:
      free__s_kt_matrix(&mat_data);
    }
    //!
    //! De-allocate:
    free__s_kt_matrix(&mat_input);
    
    //!
    //! @return
#ifndef __M__calledInsideFunction
    //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    hpLysis__globalFree__kt_api();
    //! *************************************************************************  
#endif
    return true;
}
