#ifndef kt_clusterAlg_fixed_h
#define kt_clusterAlg_fixed_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file graphAlgorithms_kCluster
   @brief provide implementaitons of algorithms assicated/focused on a fixed number of clusters.
**/

#include "kt_distance_cluster.h"
//#include "graphAlgorithms_distance.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_randomGenerator_vector.h"
#include "log_clusterC.h"
/**
   @namespace graphAlgorithms_kCluster
   @brief provide implementaitons of algorithms assicated/focused on a fixed number of clusters.
   @remarks the "c clustering library" is used as template.
   @author Ole Kristian Ekseth (oekseth).
**/

//namespace graphAlgorithms_kCluster {


  /**
     @brief identify the center-vertex fo each cluster: calculates the cluster centroids, given to which cluster each element belongs.
     @remarks The  The centroid is defined as the mean over all elements for each dimension.
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
     @remarks a subset of the parameters are described below:
     # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
     # cdata: used 'in funciton-return': updated with the cluster centroids.
     # cmask: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid i fall corresponding data values of the cluster members are missing.
   @return true upon success
  **/
bool  getclustermeans__extensiveInputParams(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp,  uint transpose, const bool isTo_copmuteResults_optimal, const bool mayUse_zeroAs_emptySign_in, const bool needTo_useMask_evaluation);
  /**
     @brief idetnfy the median scores/weights of each cluster
     @remarks Calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the median over all elements for each dimension.
     @remarks a subset of the parameters are described below:
     # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
     # cdata: used 'in funciton-return': updated with the cluster centroids.
     # cmask: an array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
     @return true upon success
  **/
bool getclustermedians__extensiveInputParams(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, uint transpose, const bool isTo_copmuteResults_optimal, t_float **arrOf_valuesFor_clusterId_global/* = NULL*/, const bool mayUse_zeroAs_emptySign_inComputations/* = true*/, const bool needTo_useMask_evaluation/* = true*/);
  /**
     @remarks a wrapper-method to compute/calculcate the cluster centroids, given to which cluster each element belongs. Depending on the argument method, the centroid is defined as either the mean or the median for each dimension over all elements belonging to a cluster.
     @remarks a subset of the parameters are described below:
     # "cdata":  Result: this array contains the cluster centroids.
     # "cmask": Result: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
     # "method": Input: 
     - For method=='a', the centroid is defined as the mean over all elements belonging to a cluster for each dimension.
     - For method=='m', the centroid is defined as the median over all elements belonging to a cluster for each dimension.
  **/
int getclustercentroids__extensiveInputParams(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, uint transpose, char method, const bool isTo_copmuteResults_optimal, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation/*= true*/);
  /**
     @brief idnetify the centroids of each cluster.
     @param <nclusters> The number of clusters.
     @param <nelements> The total number of elements.
     @param <distance> Number of rows is nelements, number of columns is equal to the row number: the distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
     @param <clusterid> The cluster number to which each element belongs.
     @param <centroids> The index of the element that functions as the centroid for each cluster.
     @param <errors> The within-cluster sum of distances between the items and the cluster centroid.
     @param <isTo_useOptimizedVersion> which is included for debug-purposes to investigate time-effects.
     @remarks 
     - idea: identify the sum of distances to all members in a given cluster: for each clsuter 'allcoate' the centorid which has the minimum/lowest 'sum of distance to other members' (in the same cluster).
     - this function calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the element with the smallest sum of distances to the other elements.
  **/
  void getclustermedoids__extensiveInputParams(const uint nclusters, const uint nelements, t_float** distance, uint clusterid[], uint centroids[], t_float errors[], const bool isTo_useOptimizedVersion, uint **arrOf_clusterBuckets_tmp);
  /**
     @brief compute the kmeans clustering of the vertices.
     @remarks ...
     @remarks a subset of the parameters are described below:
     # "dist": specifies/identifies the distance-metric to be used (in the clustering). Defines which distance measure is used, as given by the table:
     - dist=='e': Euclidean distance
     - dist=='b': City-block distance
     - dist=='c': correlation
     - dist=='a': absolute value of the correlation
     - dist=='u': uncentered correlation
     - dist=='x': absolute uncentered correlation
     - dist=='s': Spearman's rank correlation
     - dist=='k': Kendall's tau
     - For other values of dist, the default (Euclidean distance) is used.
     # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
     # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
     # "":
  **/
int kmeans_or_kmedian__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weight[], const uint npass, t_float** cdata, char **cmask, uint clusterid[], t_float* error, s_kt_randomGenerator_vector_t *obj_rand__init, uint counts[], uint mapping[], const bool isTo_use_mean, const bool isTo_preComputeDistanceMatrix_ifSpeedIncreases/* = true*/, t_float **matrix2d_vertexTo_clusterId);
  /**
     @brief compute the clustering of the vertices, using a a 'fixed number of clsuters'.
     @remarks The kcluster routine performs k-means or k-median clustering on a given set of elements, using the specified distance measure. The number of clusters is given by the user. Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
     @remarks a subset of the parameters are described below:
     # "dist": specifies/identifies the distance-metric to be used (in the clustering). Defines which distance measure is used, as given by the table:
     - dist=='e': Euclidean distance
     - dist=='b': City-block distance
     - dist=='c': correlation
     - dist=='a': absolute value of the correlation
     - dist=='u': uncentered correlation
     - dist=='x': absolute uncentered correlation
     - dist=='s': Spearman's rank correlation
     - dist=='k': Kendall's tau
     - For other values of dist, the default (Euclidean distance) is used.
     # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
     # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
     # "iffound": The number of times the optimal clustering solution was found. The value of ifound is at least 1; its maximum value is npass. If the number of clusters is larger than the number of elements being clustered, *ifound is set to 0 as an error code. Ifa memory allocation error occurs, *ifound is set to -1.
     # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
     # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
  **/
void kcluster__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nclusters, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], uint transpose, uint npass, char method, uint clusterid[], t_float* error, uint* ifound, const bool isTo_use_continousSTripsOf_memor/* = true*/, const bool isTo_invertMatrix_transposed/* = true*/, const bool isTo_useImplictMask/* = false*/, t_float **matrix2d_vertexTo_clusterId, s_kt_randomGenerator_vector_t *obj_rand__init);
  /**
     @brief ...
     @remarks "For some data sets there may be more than one medoid, as with medians. A common application of the medoid is the k-medoids clustering algorithm, which is similar to the k-means algorithm but works when a mean or centroid is not definable. This algorithm basically works as follows. First, a set of medoids is chosen at random. Second, the distances to the other points are computed. Third, data are clustered according to the medoid they are most similar to. Fourth, the medoid set is optimized via an iterative process" ["https://en.wikipedia.org/wiki/Medoid"]. Key-poitns:
     - the algorithm "is more robust to noise and outliers as compared to k-means because it minimizes a sum of pairwise dissimilarities instead of a sum of squared Euclidean distances" ["https://en.wikipedia.org/wiki/K-medoids"]. The latter is due to the training-phase in the SOM: "because in the training phase weights of the whole neighborhood are moved in the same direction, similar items tend to excite adjacent neurons" ["https://en.wikipedia.org/wiki/Self-organizing_map"]. In breif, "SOM may be considered a nonlinear generalization of Principal components analysis (PCA)" ["https://en.wikipedia.org/wiki/Self-organizing_map"].
     - 
     @remarks from the documetnaiton:
     - The kmedoids routine performs k-medoids clustering on a given set of elements, using the distance matrix and the number of clusters passed by the user. 
     - Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
     @remarks a subset of the parameters are described below:
     # "nelements": The number of elements to be clustered.
     # "distmatrix":  The distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
     - number of rows is nelements, number of columns is equal to the row number
     # "npass": The number of times clustering is performed. 
     - Clustering is performed npass  times, each time starting from a different (random) initial assignment of genes to clusters. 
     - The clustering solution with the lowest within-cluster sum of distances is chosen.
     - Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
     # "clusterid": the cluster-identites assicated to each vertex
     - On input, ifnpass==0, then clusterid contains the initial clustering assignment from which the clustering algorithm starts; all numbers in clusterid should be between zero and nelements-1 inclusive. Ifnpass!=0, clusterid is ignored on input.
     - On output, clusterid contains the clustering solution that was found: clusterid contains the number of the cluster to which each item was assigned. On output, the number of a cluster is defined as the item number of the centroid of the cluster.
     # "error": The sum of distances to the cluster center of each item in the optimal k-medoids clustering solution that was found.
     # "iffound": If kmedoids is successful: the number of times the optimal clustering solution was found. 
     - The value of ifound is at least 1; its maximum value is npass. 
     - If the user requested more clusters than elements available, ifound is set to 0. If kmedoids fails due to a memory allocation error, ifound is set to -1.
  **/
void kmedoids__extensiveInputParams(uint nclusters, uint nelements, t_float** distmatrix, uint npass, uint clusterid[], uint *centroids__global, t_float* error, uint* ifound, t_float **matrix2d_vertexTo_clusterId, s_kt_randomGenerator_vector_t *obj_rand__init);
  /**
     @brief the system test for class rule_set.
     @param <print_info> if set a limited status information is printed to STDOUT.
     @return true if test completed.
  **/
  bool assert_class__kt__clusterAlg_fixed(const bool print_info);



#endif //! EOF
