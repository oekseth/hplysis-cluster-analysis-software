#include "ex_kMeans.h"
#include "kt_matrix.h"
#include "kt_api.h" //! which is sued to access the input-matrix.
#include "hp_clusterShapes.h"

/**
   @brief provide an appraoch to evalaution of topolocial features in complex data-sets (oekseth, 06. des. 201&).
   @remarks in below we list a cronological reasoning-appraoch wrt. our method:
   # (0) start: (a) descirbe core-proeprteis of Knitting-Tools itnegrated data-bases and realize/idneitfy  ambugities which need to be addressed (iot. address the signivcnace of Knitting-Tools Klaremus-appraoch); (b) scaffold/describe different clsuter-comparison-appraoches iot. assess/identify accuracy and desc iptivness of different data-bases (eg, PathwaCommons and KnittingTools); (c) figure otu why PCA is used; (d) compare PCA to both SVD-theortiecal-persepctives and PCA-applications; (e) 'unify' PCA with clsutering-appraoches throguh scaffolding/descirption of visaulziation-strategies, and realize that dsijoitn-forests may be sued as an 'effctive mean' to increase the performance (of both PCA and clsutering-algorithms, ie, based on the observaiton taht 'by definiton far-fetched lwo-scoring cells/interactions my be filtered'); (f) manage to idneitfy/find a researhc-arilttce which address the issues/use-cases/applciaitosn (which we have observed); 
   # (1) intro: find pros and cons of teh PCA-artice [ref]: (a) read and be confused; (b) suggest appraoches to vercome wekenesses of the PCA-artilce; (c) get conused (by the number of different dimesions/approaches to evlauate); (d) 
   # (2) method-construction::intro: (a) scaffold overall 'to-be-icnldued' consepts, perspectives and metrics; (b) suggest different clustering-cases to evaluate; (c) discuss/suggest diffenret cluster-comparison-metrics to use (eg, Rands index); (d) realize taht a clsustering-comparison-appraoch relates strongly/directly to Z-distributions (eg, hypthesis-testing) and "Bayesian conditioanlity theory"; (e) outline appraoches where "Bayesian codnitonality-theory" intersects with clsuter-comparison-metrics; (f) .... 
   # 

   @remarks an application of our approach is to idnetify the different perspectives which may be inferred/gained from the inclusion of muliple measures. A gneralized example conserns the use-case where a data-set may be described by the features=[feature1, feature2, ....], and where feature1=[cluster1=[metric(a), metric(b), .....], cluster2=[metric(c), metric(c), ....], cluster3[....], ...], etc.. From the latter we observe how a few metrics may be used to idneify/describe the parameter-space (of a givne data-set). The challenge conserns how to idneitfy such a 'parameter-space', an approach which we outline/describe in below test="multipleCorrelationMetrics-obvious-disjointCases". To understand the conceptual correctness (of our appraoch) we suggest comparing/using the following use-case-example: "when describing a given weahter-season in Bergen (the worlds famous hotspot for rainy days) it does not suffice to use a signle measure (eg, 'peoples happiness', 'the texture of a cats coat', 'number of umrellas', etc.). In contrast we would argue that muliple measures are needed: in Bergen we expect every day to bring some drops of rain (ie, as it is othewise not counted as a day, hence only days with rains are inlcuded in our sample-data-set). Compelxity therefore conserns the 'relative importance' of measurements (ie, similar to the 'data-ambigutiy' parameter in our data-evalaution, a data-ambugity which may arise from non-specifity in input-data, noise introduced to make the data-sets closer to real-world scenarious, non-global-optmality in k-means selectiosn, etc.). The 'relative importance' wrt. wheater in Bergen conserns each individuals contitious/sujective assertions/hyptohesis of 'how wet they may become in order to claim having a rainy day': as we expect the summers to be warm (ie, apporx. 15 Degrees) and the winters to be chilly (ie, approx 5 Degrees), ambigious results (wrt. the parameter-seleciotn we choose) will indicate that we are in the spring-season (as the autom-season does not exist in Bergen)". From the latter have illustrated how inconclusive results may be used to predict an outcome (ie, wrt. the latter 'spring-season'). Simliariy we may use our data-set-classificaiton (in test="multipleCorrelationMetrics-obvious-disjointCases") to provide a classification of data-sets.  <-- TODO: try to write a use-case where we illustrate how real-life data-sets may be classified wrt. the latter.


   //! ------------------------------------------------------------------
   //! @Subject: In below we describe how each test-case relate to each other, a description which provide a brief and simplified version/permutation of our appraoch. To summarise we in our approaches 'seeks a conceptual increase' to extract parameters from a multi-dimensional data-algorithm space. In below we describe how our different test-cases is used to stepwise increase the result-evaluation-complexity (ie, how infernces/asserts are 'made' in each our our test-cases):
   #case("obvious-disjointCases"): comparison-metric="1-dimensional rank-based": compare the 'ranks of clusters  Rand-score based/generated from the 250+ correlation-metrics' to Rand-scores genrated/clustered based on [Pearson, Euclid]; demonstrate that the latter [Pearson, Euclid] metrics 'are un-represnetive for the metrics which may be used in clsuteirng-algrotihms'; 
   #case("multipleCorrelationMetrics-obvious-disjointCases"): extend [above] through use of combinatorics: idneitfy the best-descritve metrics to combine in order idnetify specific/parituclar traits (eg, variability in cluster-predictions from data-perturbations). An example-application is to describe/understand 'for what data-permutation-cases the different correlation-metrics will result in the same cluster'. Fro the latter use-case we (for different data-permtautions) compare the singficnace of different cluster-membership-categories, eg, to answer the qeustison of: "are there any correlation-categories which: always overlaps? never overlaps?". From the latter we observe how 'this comparison-strategy' 'translates' into a 2d-comparison-strategy, ie, to idneitfy complex relationships through an investigation of 'all possible combinations of features'; 
.. idnetify the how a parametrizaiton of 'free variables' may be used to roughly/apprxoiatmemly describe/scaffold 'clsuter-staiblity'.
   #case("cluster-comparison-metrics"): an application-focused test-case which describes/idneitifes the applicability of cluster-comparison-metrics in data-compiarson (eg, wrt. how different cluster-outcomes are classified). The genericity (in this test-case) conserns our emthdology-focus on comparisng/relating different out-comes of [data-set]x[parameter=cluster-comparison-metric]. In this test-case we make use of [above] idneified/described test-meothdology to 'figure out what an agreemen/overlap in simliarty-score (between clsuter-comparison-metrics) says about the data-set, ie, where we demonstra a multi-parameter strategy to overcome in-sensistivties in 'the metics we evlauate'. Result is expected to be 'an appraoch to parametrize a data-set based on the different correlation-emtric-cateogires. 
   #case("clusterAlg-internalSkew"): [simliar to [above], with difference: we focus on the relationship between result-accruacy of different cluster-algorithm-permtautiosn].
   #case("clusterAlg-multipleCorrelationMetrics"): 
   #case("RandsIndex--seperationStrenght"): [simliar to [above], with difference: we focus on the ability of cluster-comparison-metrcis to provie a correct seperation between differnet data-set-value-cases]
   #case("")
   #case("")
   //! -----------------------------------------------------

   //! ------------------------------------------------------------------
   //! @Subject: "how real-life KnittingTools data-sets may be analyzed":
   #case("obvious-disjointCases"): "how differnet synonym-mapping-cases feares/compares with a set of reference-data-sets", ... 
   #case("multipleCorrelationMetrics-obvious-disjointCases"): "the genes which are most simliar for different combiantiosn/permtuations of sub-groups, eg, ato answer the question: what are the predicates which maximizes the head--tail simliarty seperately for the cases of [disease][annotation], [gene][annotation], and [gene--disease][annotation] data-sets?"
   #case("cluster-comparison-metrics"): 
   #case("clusterAlg-internalSkew"): 
   #case("clusterAlg-multipleCorrelationMetrics"): 
   #case("RandsIndex--seperationStrenght"): 
   #case("")
   #case("")
   //! -----------------------------------------------------


   //! ------------------------------------------------------------------
   //! @Subject: ""
   #case("obvious-disjointCases"): 
   #case("multipleCorrelationMetrics-obvious-disjointCases"): 
   #case("cluster-comparison-metrics"): 
   #case("clusterAlg-internalSkew"): 
   #case("clusterAlg-multipleCorrelationMetrics"): 
   #case("RandsIndex--seperationStrenght"): 
   #case("")
   #case("")
   //! -----------------------------------------------------
   // FIXME: cosndier 'translating' [ªbove] desciprtion-abstrat intoa  ewn aresearhc-aritlce where we ....??....


   
   //! ..................................................................
   //! ------------------------------------------------------------------
   //! @task how to handle the sad issues conserinign our MINE-app-status.
   @remarks we hypothese that a main-logical mis-concpetion wrt. our 'original' MINE-aritlce-sbumission consernd both (a) the 'mismatch between suggested/porpsoed reviwers VS the aritlce-format we choosed' (eg, where the minpy-author preferred an 'hidden atthcment-elaboriation-strategy' simliar to his own work) and (b) 'the need to be eplxit wrt. trendyness of software' (ie, to 'bring the sfotware to an applicaiton-filed which have gaiend poopularity').   
   @remarks an itneresting observation in [below] is that 'all searhces for k-means in BMC Bioinformatics' evlauates the use of an SVD-pre-step, for whcih we may infer that ....??...
   <-- consider to compare our MINE-based appraoch with CPA wrt. feature-dientificaotn ... and tehrafter submit seperate aritlces to both BMC and PNAS (where we for the latter may use ["http://www.pnas.org/content/111/14/5117.full.pdf"] as a tempalte ... an artilce which 'assert's that 'it' works better than a PCA-based eismionality-appraoch (eosketh, 06. jan. 2017)).
   <-- test the HCA-algorithm of ["http://bioinformatics.oxfordjournals.org/content/30/6/896.full"] which 'meres' a number of clustering-results 'into one unified result using/elvauting appraox. 20 corr-emtics (listed in "https://cran.r-project.org/web/packages/fastcluster/vignettes/fastcluster.pdf") .... <--- consider using 'this' in a new ox-app-ntoe-aritlce where we ....??... <-- todo: discuss this with JC ... dienitfying 'low-hadning-to-publsijh-artilces'.
   @remarks as a 'criteria for trendyness in researhc' may the following critera suffice:?
   trendyness(1): "outperforms other software" <-- seems like the latter does not held, ie, ref the ox-MINE-feedback
   trendyness(2): "higher accurayc than established/published appraoches": <-- is my current motviation, ie, as current results indidate that MINE our-perofmrs other software-appraoches: what do you JC think?
   trendyness(3): ...
   trendyness(4): ...
   trendyness(5): ...
   -------------
   --"MINE app-note": 
   @methodUpdate: "change focus from a pure software-udpate to an algorithm-update instead: read through all BMC-artilces which 'includes both a data-set and a k-means or SOM cluster-algorithm and demosntrate that our new cluster-method outperofmrs the latter'.": (a) evalaute our itnro-PCA-anlasysis-appraoch (to valdiate that the MIEN-metric outperofrms others for importnat cases); (b)    
   @relatedWork "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1184-5": ..... in their work the authros claim/assertk-means does not wrok proerptly on biolocial data either due to low predcition-calassifciaotn-accuarcy of high comptuational exeuciton-time ... in their work the authros asserts taht the 'k-emans' eitehr-or appraoch prevents/prohibits the 'detection' of overlapping/ambigius cases, an assert which (from our point of view) is wrong:: if we run k-means clsuteirng muliple times (with slightly differnet settings) and thereafter 'itnersect'/merge the k-means-clsuter-rpedicont-results the prediciton-result is a set of overlappign clusters ... "Stepwise iterative maximum likelihood clustering approach"; authros compare (among oothers) with k-means clsutering and assert that their algorithm-appraoch out-performs the latter. However 'how the correlation-emtrics are inferred/comapred' (ie, their correlation-emtric of choise) is nto stated, ie, 'is left to speculation' .... to validae their algorithm the authors "carried out experiments on normal Gaussian data as well as on biological data". 
   <-- correlation-metric: the while the authros (wrongly assumes/assertyehs tath HCA algorithms use Eucliddian corrleation-metrics, the correlaiton-emtrics whcih is sued in 'their tested k-means algorithm' is enver evlauated/asserted .... from our work wrt. ....??... we observ ethat the choise of a proper/descirptive k-means-clsuter-algrotihm si of profound importance. 
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="sharma2016stepwise"]
   <-- consider contatinting the author(s) to 'get data-sets to re-produce their resulsts' (ie, their 'parsed7colelcted veriosn of the data-sets').
   <-- data-set: ....??...
   <-- exeuctionTime: seems like they for a datga-set with n="102,000" and |cores|=6 had time='2,000 seconds' ... ie, approx. 12,000 seconds (ie,t he apralosiation was ideal) ... seems rather fast ... could be that they 'do Not have a squared matrix' <-- as the authros assume a Guassian dsitribution ... could we use/apply our 'dsijoitn-amsking-strategy in a meaningful manner wrt. the latter'?
   <-- implmentationLanguage: Matlab <-- may you JC 'try this out' (ie, if you have experience with Matlab). <-- the article used only 6 mosh of review, ie, assems like the reviser (oddly) liked this aritlce, ie, might be an interesting/good template <--- a 'wekaness' wrt. the aricle (from our point of view) is 'their' large numer of matehmcial equaitons (when exprlainign their algorithm) ... ie, which is in cosntrast to 'our preferred way of writing'.
   @relatedWork "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-0984-y": "pcaReduce: hierarchical clustering of single cell transcriptional profiles": ... "we compared our approach to other commonly used statistical techniques, such as K-means and hierarchical clustering. We found that pcaReduce was able to give more consistent clustering structures when compared to broad and detailed cell type labels." <-- Note: in the comparison the authros use "Adjusted Rand index" to compare a 'goal-standard-clsutering' to (a) 'teh authros algorithm', 'two permtuations of k-means', a specific HCA-algorithm 'and a number of other algorithms, ie, seems like the latter may 'serve as a benchmark wrt. teh aurraciy of our dataset-comparison-appraoches.
   <-- consider contatinting the author(s) to 'get data-sets to re-produce their resulsts' (ie, their 'parsed7colelcted veriosn of the data-sets').
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="yau2016pcareduce"]
   <-- data-set: ....??... <-- seems like the data-sets seems 'well-explaind' in the appendix ... ie, consider to 'use these data-sets as a benchmark'.
   @relatedWork "http://bioinformatics.oxfordjournals.org/content/31/12/1974.full" ... "It has been claimed that for a broad range of data distributions, the conventional similarities (such as Euclidean norm or Cosine measure) become less reliable as the dimensionality increases (Beyer et al., 1999). The reason is that all data become sparse in high-dimensional space and therefore the similarities measured by these metrics are generally low between objects (Beyer et al., 1999)" <-- seems like tha ltter is an arugment for the maksing-appraoch in our hpLysis
   <-- todo: consider to use 'the authrso arguemtnation' as an arugmetnaiton for hpLyssi opmtized maksing-aprpaoch
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="xu2015identification"]   
   @relatedWork "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0694-x" 
   ... "SVD ... has a complexity of O(m n 2) for m data points and n ambient dimensions (genes markers in our case). "
   <-- example-data-test: "We retrieved all breast cancer (BRCA) RNAseq samples from the RNASeqv2 section of the TCGA data portal (http://cancergenome.nih.gov/), selecting normalized data. The data matrix consists of 1100 tumors profiled at 20,531 expression levels. We then preprocessed data to convert expression values for each gene to Z scores by subtracting the mean expression level of each gene from all observed values for that gene and then dividing the zero-centered expression levels by the standard devation across values for the gene. We then applied our analysis pipeline to the resulting data matrix."
   <-- contact authors, where goal is to (a) get their code compiling and thereafter (b) test the 'effect' of using MINE instead of SVD <-- 'read though all MINE and "Total Information Coefficeint (TIC)" articles to test/fidn any appraoches which 'are the same'".
   <-- data-set: ....??...
   @relatedWork "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0755-1": "Interlog protein network: an evolutionary benchmark of protein interaction networks for the evaluation of clustering algorithms": .... 
   <-- compare our MINE-based clsutering-appraoch wrt. the authros data, eg, wrt. their MCL-based clstuerign-appraoch .... "Meanwhile, the superiority of the MCL is compatible with the earlier results [32]. By distinct approach, they presented a comparative assessment of clustering algorithms and showed that the MCL was remarkably robust in graphing alterations and capable of the extraction of the complexes from the PPINs."
   <-- data-set: ....??...
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="jafari2015interlog"]   
   <-- data-set: if [ªbove] 'results' in 'superiorty' for our MINE-based clsutering-appraoch: consider to use the 'gold-standard-datasets- from ["https://academic.oup.com/bioinformatics/article-lookup/doi/10.1093/bioinformatics/btu034"]
   <-- cosnider to read "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-7-488" ... and extract/'idnetify' the data-sets.
   <-- conside rto download "https://raw.githubusercontent.com/lmweber/benchmark-data-Levine-13-dim/master/data/Levine_13dim.txt" ... and use the latter to evaluate ...??... <-- how 'do we know the gold-standard wr.t the latter'?
   @relatedWork "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0614-0" "UNCLES: method for the identification of genes differentially consistently co-expressed in a specific subset of datasets"
   <-- consider to analsyse the different 'sysntietc data-fiels they provide', eg, 'the one' downloaded+covnerted at "/home/klatremus/Skrivebord/uncles_12859_2015_614_MOESM1_ESM.tsv.csv" (which for each row hold 'row-name'+'cluster1-membership' + 'cluster2-embmership' + [feature-scores]).
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="abu2015uncles"]   
   @relatedWork "http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0090801" w/title "Biclustering Methods: Biological Relevance and Application in Gene Expression Analysis" ... focus on comparison of differen "bi clsutering algorithms", ie, a 'permtuation' of our work  <-- consider implementing different bi-clsutering-algorithms into "hpLysis" ... and demonstrate that the MINE-correlation-appraoch 'results in a signifcant improvement wrt. result-accuracy'
   <-- for an introduction to "biclsutering" see "http://www.cs.princeton.edu/courses/archive/spr05/cos598E/Biclustering.pdf"
   @relatedWork "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-16-S4-S7" w/title "Analysis of miRNA expression profiles in breast cancer using biclustering" ... discusses how "bi-clsutering" may be applied/used
   @relatedWork ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"] w/title: "Interpolation based consensus clustering for gene expression time series"  .... "Early methods such as k-means, hierarchical clustering, and self-organizing maps are popular for their simplicity. However, because of noise and uncertainty of measurement, these common algorithms have low accuracy".  ... "The performance of our algorithm is superior to k-means algorithm which needs the number of clusters as parameter for input" <-- todo: download the 'dataset-sets used in the latter' and comapre the results 'wiht our MINE-based correlation-metric-pre-process-appraoch'.
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="chiu2015interpolation"]   
   @relatedWork ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0508-1" w/title "Classification of bioinformatics workflows using weighted versions of partitioning and hierarchical clustering algorithms"] <-- todo: consider contacting the authors iot. 'get access to both their data-sets and their timing-reuslsts' ... and then evlauate the atuhros 220 different data-sets using our HPC-JC-Vilje-pproach.
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="lord2015classification"]   
   @relatedWork ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1245-9" w/title "EnsCat: clustering of categorical data via ensembling"] ... "Ensemble clustering, as implemented in R and called EnsCat, gives more clearly separated clusters than other clustering techniques for categorical data" <--- investigte what "cateogircal data" actually 'are' (and assess how sparse-ontology-comparison combiend with 'makss' may be sued to support/faicliatet ehta letter) ...    "Since techniques based on feature selection are even harder to devise and compute for discrete data, feature selection does not seem a promising approach to high dimensional clustering of categorical data. Otherwise put, generic techniques such as ours that do not rely on extensive subject-matter knowledge are often the only available techniques." <-- what does the latter mean, ie, are the uathors 'saying' that an all-agaisnt-all-evlauation of matrices are unfedaible? <-- if my itnerpretioant is correct, the authors seems to expect that there is an either-or-density wrt. knowledge (an arugment which does not hold) ....

   -------------
   --"hpLysis": 
   <--- todo[new-aritlce]: would it be possible/itneresting to 'realise' a new aritcle which 'sperately' focus on "cluster Comaprsin Metrics (CCMs)" ... ie, as it seems like hpLyssi 'provides a comprehensive colelciton of emtrics Not supported by opterh/alteirtnvie packages'? ..... eg, when compared tot he "https://se.mathworks.com/help/stats/classeslist.html" package.
   @relatedWork "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" ... provides a parallle R-framework for computing correlation-matrices.
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   @relatedWork "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1356-3#Sec14" w/title "A new method of finding groups of coexpressed genes and conditions of coexpression": describes a new algorithm for "bi-clsutering": <-- could be a 'candiate' for our MINE algorithm.
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   -------------
   --"PCA-article": 
   -------------
   --"KnittingTools---dataBase": 
   -------------
   --"sematnic-simliarites": 
   @relatedWork "http://nar.oxfordjournals.org/content/44/W1/W154.full.pdf" <-- a web-tinerface ... first-imrepssion sit aht 'this' is a subset of the hpLyssi-web-itnerface ... ie, cosndier 'wrtiitng such an app-note'.
   @relatedWork "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1160-0" "TopoICSim: a new semantic similarity measure based on gene ontology" by Finn Drabløs
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   @relatedWork "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1294-0" "SGFSC: speeding the gene functional similarity calculation based on hash tables"
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   @relatedWork 
   @relatedWork    
   -------------
   --"search-engine using profiles":
   @relatedWork "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-0934-8": "GEMINI: a computationally-efficient search engine for large gene expression datasets" ... "To improve access to massive genomic data resources, we have developed a fast search engine, GEMINI, that uses a genomic profile as a query to search for similar genomic profiles. GEMINI implements a nearest-neighbor search algorithm using a vantage-point tree to store a database of n profiles and in certain circumstances achieves an O(logn)".
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   -------------------   
   --"Applications of different clsuter-compiarson-metrics":  .... idea: describing diffenret 'combaitions' of how the cluster-comparison-emtrics 'have been used and mis-used' ... writing a test-valaution-method wrt. the 'latter' <-- when we are satsified 'with our to-be-completed test-method' then 'dowload the ddiffernet sata-set in [”elow] cited aritlces' and find/idneityf cases where 'the use of cluster-comparison-metrics are miselading/errnous' 
   ***["Rands index", "Jaccard", "Rands adjusted index"]:  ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1245-9" w/title "EnsCat: clustering of categorical data via ensembling"] ... the authors asserts that the ̈́results produced by the latter metrics are approximate the same ... and therefore idnciate that the clsuter-repdicotn-result-difference is sigifcatin' <--- todo: try addressig 'this issue' ie, by evlauating/isdniefyign 'the data-set-cases where the latter emtrics are overlapping' (eg, by 'budling' a tmerinal-itnerface where a user may set 'some threshodls and thereafter idenitfy the degree-of-interseicont' wrt. the didfferent emtics').
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   ***["Normalized mutual information (NMI)", "Adjusted Rand Index"]: used in ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1075-9"] w/title "A novel procedure on next generation sequencing data analysis using text mining algorithm" ... "The clustering results were evaluated by Normalized mutual information (NMI) [34] and Adjusted Rand Index (ARI) [35]. NMI and ARI are two external validation metrics to evaluate the quality of clustering results with respect to the given true labels of datasets. The range of NMI and ARI values is 0–1. In general, the larger the value is, the better the clustering quality is." <-- todo: ivniestge the correctenss of the uathors asusmption ... and find cases 'ehere the authors conlsuion/metric-deivaiton is misleading'
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite="zhao2016novel"]   
   ***["Adjusted Rands index"]: used in ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0605-1" w/title "Topological characterization of neuronal arbor morphology via sequence representation: II - global alignment"] and ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0474-7" w/title "Measuring semantic similarities by combining gene ontology annotations and gene co-function networks"] <-- seems like the authors 'belive that a non-gemotreic-random-score indicates that the clusters are higly clsuter-rpedicoitns are higly acucrate <-- TODO: try to investigate this asusmption ... and find/idneify cases 'where the authros assumption is wrong/misleiading' <-
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   ***["Rands index"]: used in ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0508-1" w/title "Classification of bioinformatics workflows using weighted versions of partitioning and hierarchical clustering algorithms"] to examine diffneret lcuster-rpediction-results "using the weighted versions of k-means and k-medoids partitioning algorithms. The Calinski-Harabasz, Silhouette and logSS clustering indices were considered. Hierarchical classification methods, including the UPGMA, Neighbor Joining, Fitch and Kitsch algorithms, were also applied to classify bioinformatics workflows. ... Our findings based on the analysis of 220 real-life bioinformatics workflows suggest that the weighted clustering models based on keywords information or tasks execution times provide the most appropriate clustering solutions.". <-- what seems odd wrt. the authors applicaiton/use of the "RAnds index" si taht tehy do not evalaute/assess the signficance-thresholds wrt. Rands-index-difference (ie, they asusme that a difference in Rand-index-score implies a ntoeworthy/sigifcant difference in clsuter-rpedicotn-accuracy <-- TODO: try to address the latter isuse/observiaotn, ie, by 'fidning the threshodls for when Rand-idnex-difference implies a signifant deviation/difference in clsuter-rpedicotn-reuslts'.
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   ***["Rands adjusted index", "Silhouette index", "Minkowski", "Dunn’s Index", "Davis-Bouldin index", ]: used in the work of 
   ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"] to "judge the clustering accuracy of" the author s proposed/described algorithm. ... 
   <-- authorsAlgorithmComparisonStrategy: [see our "acm__clustMetricCmp.tex" wrt. cite=""]   
   -------------   -------------   -------------   -------------

      ---------------------------------------------------------------------------
      ---------------------------------------------------------------------------
      //! @task an appraoch to refute or accept the applicaiblity of our MINE correlaiton-emtric as an alternative to "Principal Component Analsysis" (PCA)
      <-- try to 'conceptually relate this appraoch' to our 'main work on building the optimized MINE correlation-analsysis-software': challenge cosenrnst eh 'large conpcetual space covered by such an approach' ... and where we 'hardly do any work wrt. PCA ... ie, feel unconvinced that we may argue for an improved software wrt. the MINE-correlation-measure': <-- TODO[JC]: may you 'concpetually evalaute' how 'an appraoch wher e we integrate MINE into a k-means-clsuter-algorithm differneitas/permtuates from other clsutering-algorithms'?
      @motivation get comfortable that our MINE-correlation-metric 'has a point', ie, that we may publish 'the MINE-app-note' in "BMC Bioinformatics"
      @reviewerAssignment to 'dedicate' our work to non-MINE authors, ie, to 'use researhc-templates of cluster-specific appraoches'. Hope is avoiding 'the issues observed wrt. the depressed minerva-author'.
      @motivation a simplicitc method: does not make use of any complex underlyign assumptions, ie, method-correctness is easy to validate. 
      @motivation provides a 'pre-study' to demosntrate/examplify the applicaiblity of hpLysis large-scale support of differnet correlation-metrics': what we hope is that there is a large differen/variance in the accuracy of different corlreaiton-emtrics (wrt. data-sets and pre-data-filtering-appraoches). 
      @result success-criteria: the MINE-correlation-metric outperforms other/existing correlation-measures (or: 'different permuations of Spearman, Euclid and Pearson').
      @result result-signficance: if our MINE-based appraoch 'improved clsuter-prediciton-qaulity' then a 'core-strneght' is that we 'did not modify our appraoch to faciltiate/imrpove the clstuer-results' ... ie, were 'our appraoch is an un-sperevised procedure with results which singivantly outoperforms existing appraoches'.
      @step (0) pre-steps:
      (0.a) 'get convienced that we may both manage to evlauate the netowrks and that our approach may be successuful'.
      <-- challenge: "representative data-sets", ie, .....??....
      (0.b) idneitfy and collect establihsed gold-standards for data-set-comparisons.
      (0.c) download 'gold-datasets' used to proove efficnecy/correnctess of 'new-described' algorithms (ie, which we are to compare with).
      <-- 'syntietic' and rela-world data-sets listed at ("https://clusteval.sdu.dk/1/datasets/3350") 
      <-- GEO: the data-sets listed in ["http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0091315"]
perf-compiarson: ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9"], which lists a numbe rof large files: <-- call 'these' from terminal-API
      (0.d) implmeent the different cluster-comparison-emtrics in "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0755-1" and test wrt. out 'sytnetic clsuter-dataset': step(a) implement the metrics=[Jaccard, Fowles-Malkow, Minkowski, Rand]; step(b) downdlaod+parse the 'describe dataset' and comapre wr.t the latter; step(c) implement/test wr.t the 'described algorithms to generate/buidl the clsuters'.
      (0.e) write a function to generate our 'sample-clsuter-distributiosn' ... and find the 'extreme ranks' for each cluster-comparison-metric
      <-- use the discussion 'of extreme cases' by ["http://homes.mpimf-heidelberg.mpg.de/~mhelmsta/pdf/1971%20Rand%20JASA.pdf"]
      (0.f) scaffold+write for: 'distance between clusters' (eg, as described at "http://www.cs.princeton.edu/courses/archive/spr11/cos435/Notes/clustering_general_topost.pdf" and "http://www.ims.uni-stuttgart.de/institut/mitarbeiter/schulte/theses/phd/algorithm.pdf")      
      <-- task: write an 'appraoch' to 'build a confusion-matrix from k-means result-emmberships' ... and get "Rand index" implemented.
      <---- (.) write a new 'class' named "kt_matrix_cmpCluster" and "measure_kt_matrix_cmpCluster"; add to repo and get compling; update our test-based structure ;
      <---- (a): 'map' a cluster-to-centrodi-mappings into a [cluster-id][vertex-id]='0|1'.
      <---- (b): 'call our many:many' wrt. the dot-prodcut and validate the results.
      <---- (c): implment "Rands index" and vlaidate results" ["http://homes.mpimf-heidelberg.mpg.de/~mhelmsta/pdf/1971%20Rand%20JASA.pdf"]
      <---- (d): [simliar for "Rands Adjsuted index"].
      <-- template: "http://www.otlet-institute.org/wikics/Clustering_Problems.html#toc-Subsection-4.1" <-- for: "Rands index" and "Rands average index"
      <-- template: "http://i11www.iti.uni-karlsruhe.de/extra/publications/ww-cco-06.pdf"
      <-- ok: ... read through the different articles at "https://scholar.google.no/scholar?as_q=rand+index+clustering&as_epq=&as_oq=&as_eq=&as_occt=any&as_sauthors=&as_publication=bmc+bioinformatics&as_ylo=2015&as_yhi=&btnG=&hl=no&as_sdt=0%2C5" and cosnider to updat eour 'list of possible researhces to cite': updated [ªbove] and our "measure_kt_matrix_cmpCluster.c"
      <---- try using [above] descirption to generate+test diffenre tdata-sets ... validating the differnet asusmptison (eg, wrt. 'increasiing number of clsuters where Rands index is epxected to prodcue emnaingless reuslts'.      
      <---- motivation: try describing an 'alterantive appraoch' which is 'focused on thc cluster-vertex-vertex-memberships (rahter than the cluster-pair-mbemberships).
      <---- 
      <-- template: "http://www.ims.uni-stuttgart.de/institut/mitarbeiter/schulte/theses/phd/algorithm.pdf"
      <-- try to scaffold how 'this' may be used to 'weight' differences in clusters (eg, in comnbination witha "Confusion matrix").
      <-- task: 'translate' all metrics into our distancembased-metric-spec.
      <-- try to 'figure out how' the [ªbove] may be related to "Confusion matrix-centered metrics".
      <-- consider to discuss/evlauate ehe 'mnetrics' listed in "https://cran.r-project.org/web/packages/NbClust/NbClust.pdf"

      @breakPoint: we assume 'at this step-point' that "measure_kt_matrix_cmpCluster.c" is completed.
      @breakPoint: update our correlaiton-metrics with a new 'class' named 'directScore' (where we do Not apply a loop-traversal while assuming tha that the matrix is an ajdcency-matrix), eg, for the 'metrics' of [headToTail, tailToHead, avg, sum, mult, div, max, min]
      @breakPoint: complete our "kt_assessPredictions.c"

      @step (1): Rand-index: use a 'simple rank-sort-appracoh' to idneitfy (eg, list) the metrics which best describe the 'actual sollution': <-- for simplict we select the 'best-scoring clsuter-rpediction among t tries when runngint he k-means clsutering-appraoch'.  <-- update+call our "kt_assessPredictions.c"
      (1.a) use our 'data-set-generaiton with a well-defined cluster-identity-seperation', where we apply/use the complete set of correlation-metrics and cluster-comparison-metrics:
      (1.a.1): correlation-metric-evaluation: 'project' the computed correlation-distance-matrics on the 'gold standard' (eg, using "Dunns index"), insert the results into a 1d-list, and identify the best-scoring (and worst-scoring) "data-set---correlation-metric" pairs. Result is used to indicate the applicability of different correlation-metrics. Threafter cluster the results iot. idneitfy ....??....  <-- Novelty: in the assessment of correlation-metircs I have never seen 'such an appraoch' (ie, to merge two different classifciaotn-schemes by combining cluster-membership-list with a not-directly-related correlation-matrix).
      (1.a.2): ["filter", "PCA"]x[none, "correlation-metric"]: test different filters (both masks and no-masks, eg, wrt. 'ranks') and identify .....??...
      (1.a.3): disjoint-rank-thresholds: extend/improve [above], where we try to idneitfy groups of data-sets which 'yealds' the same accruacy-degree (for a given disjoint-threshold).
      (1.a.4): k-means: test different permtuations (of k-means-clustering) and for each k-means-cluster-centroid-spec build a classifciation-matrix of [clusterStability=[low, medium, high]][resultAccuracy=[low, medium, high]] (ie, where the latter classifieds the different "data-set---correlation-metric" pairs), and use the latter result to hyptoehse/assert the best "data-set---correlation-metric" configurations/settings to be sed. An example-use-case of the latter is to ....??.... 
      (1.b) syntetic-data-sets: [ie, similar to above <-- for ech try to scaffold/hyptoehse/describe a 'gold standard']
      (1.c) 'data-sets used in vlaidation of clsuter-algorithms': download+parse+include different 'datasets listed in BMC aritlces', and idneitfy the 'impact' of our MINE appraoch.
      (1.d) 'small-sized': evalaute/use our 'existing' small-sized real-life data-set to vlaidate that MINE outperforms the acucracy 'of all other correlatiobn-emtrics'. 
      (1.e) "real life data-set (extensive)": for each 'real-life-dataset' apply an extensive cluster-comparison involving the parameters/factors of [corr-metric, 'randomness-permtautions with [none, low, medium, high] random-permtuation-factor', 'cluster-center-metric (eg, AVG VS medoid)', 'same result in each fixed-parameter-call'] and idneitfy the syntatic-data-set(s) which best 'capture teh latter relationship': use this 'relationship between real-life and syntetic data-sets' to ....??.... <-- try to introspect wrt. the latter (eg, to figure out 'how we may use a dataset-classification-strategy to evlauate/assert a cluster-classifciaiotns accuracy').
      @step (2): popular clsuter-comparison-metrics: repeat [above] and generate a list of the 'top-m-scoring metrics' for each clsuter-comparison-metric (eg, 'producing' a compareison-appraoch simliar to ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0755-1"]). <-- try to relate the "correlationMetric--goldStandard" relationship to the "kMeansClusterResult--goldStandard" relationship, eg, to assess/idneitfy/describe both "how the clsuter-comparison-metric relate to each other (eg, Rand VS Dunn)" and .....??....
      @step (3): k-means-permtuations: evalaute different 'permtuations' of k-means: ["AVG", "median", "medoid"] and different centrality-intaiton-strategies such as ['ideal collution (to test the improtance of accurate guesses)', 'rank-based selection (where we merge the topology of clusters until k-clsuters are idneifed)', HCA-cluster-selections (where we 'merge' both based on 'link-distance' and 'node-distance')]
      @step (4): all Knitting-Tools sample-data-sets: evalaute for both real-life and sample-data-sets: validate that the MINE correlation-emtric still 'outperofrm the others'.
      (4.a) syntetic-sample-data-sets: construct different sample-for-loop-data-set-budling-cases with a well-known-defined clsuter-seperation, where parameters are [dimesniontality=[low, heigh], cluster-seperation=[low, heigh], valueDensityt-Spread=[<where we invesitgate the effect of using different non-normalized valeu-spreads, eg, where we for the Minkowski-corr-metric-groups expects taht the 'small valeus' will be ingored, ie, where a non-normalized-use wi 'forget' the possilbe correlation between cosnstely small-scale correlation, ie, where dsijoitns-eperation may be seud for teh latter>].... ]
      <-- update our corr-metrics wrt. "chord distance" ["http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0144059"] <-- first valdiate 'taht we Not alredy supports the latter', ... 
      <-- consider to: comptue eigen-vector-centrlaity for each correlation-metric and then compare/evlauate how teh 'ranks' changes (between different vertices in the same input-data-set) for diffent correlation-metrics
      <-- consider to apply "Rand-index to assess stability in each clustering, and then use the STD-value (between different Rand-scores) to idneitfy the 'rank' of each data-set ... and thereafter evlauate using muliple cluster-ocmparison-meitrcs (where we are intersted in idniefying cases where the different cluster-comparison-metrics results in sigivntatly differetn ranks wrt. cluster-accuracy ... and use the ltter results (ie, if our hypotehss hold) to classify the data-sets and corr-metrics which signifcantly 'alters' the seperation-agreements of the cluster-comparison-metrics)
      (4.b) 'synetitc distributions': evaluate for [weight=none, preProcess=none, mask=none]: ...  [use [above] method].
      (4.c) 'real-life-small-data-sets': evaluate for [weight=none, preProcess=none, mask=none]: ... 
      (4.d) preProcess=[none, binary, ranked]:  .... 
      (4.e) weight=[none, {centrlaity-metrics}, STD]: ... 
      (4.f) masks=[none, low, medium, high]: ... 
      @step (5): different corr-metrics inside k-means VS outside: [<corr-metrics-before>]x[<corr-metrics-in-k-means>]: rank/sort the 'top-m-scoring metrics', and idneitfy the X percent of the 'corr-metrics-before' cases (ie, the metircs where "'cnt-in-best'/'total cluster-comparison' > P"). Validate our assumption that MINE cosnistenly outperforms the other metrics. Threafter copmmtue Z-scores to validate the signficance (of the latter results). 
      (5.a) "implementaiton-correctness and idneitifcaiton of value-space for each metric": (i) for "MINE" validate that the resutls produce expected results; (ii) for "none-rank" write test-cases 'using' the proeprties of the equations; (iii) Kendall-permutations: write test-cases where we investgiate the 'effect' of the different Kendall-post-processing-strategies <-- application: consider 'using these results' to 'first classify metrics by-hand using core/undleryign proerpties of each corr-metric' and thereafter 'build/generati semi-manual data-set for classificaitons' (where the latter is among others used to vlaidte our 'manual classifciaotn of corr-metrics').
      (5.b) disjoint-seperation: (i) apply each correlation-metric to our 'well-defined sytnetic data-set' and thereafter (ii) apply different filter-tenqieunes to investigate the 'match' between the 'ideal case' and 'the case inferred from the correlations' (eg, by selecting 'the best filter-permtuation (for each filter-type) for each metric'). <-- application: to describe the effects/importance/applcialbity of different filter-combiantions: ....??...
      (5.c) k-means-seperation: [simliar to [above]]  
      <-- application: 'identify the effect/accuracy-enhancement of k-means-cluster-categorization when compared to a pure/simplifed filter--disjoint-cluster-approach (eg, to assess the degree of result-acccruacy-increase when using the high-peformance-cost-strategy of k-means-clustering as a post-clustering-step' and 'to group the correlation-metrics based on their implicit-simliarty in k-means-clsuter-catergizations' 
      <-- try to find the 'best correlation-metrics for a given dataset-type' ... and manage to idneitfy different 'best-case-data-sets' for each of our approx 1,000 corr-metrics (and then compare the result to ["http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0144059" w/title "A Comparison Study on Similarity and Dissimilarity Measures in Clustering Continuous Data"]).
      <-- try to 'identify the cases where a rank-based disjoint-sub-divison appraoch' results in 'a result which is inside the AVG +- STD threshold of a non-disjoint-based k-means appraoch' (ie, for different catweogries of data-sets), and use the latter to suggest a MPI-based parallel algorithm 'for importnat sub-cases of use-case-data-set-cluster-applications' (ie, as popular algorithms are tailored towards specific use-case-applications).
      @step (6): data-filtering: compare the 'effects' of [PCA, mean-row-filtering, column-row-filtering, ... <-- consider writing/adding suppoprt for a "Laplacian transform"] VS "MINE"
      @step (7): algortihm-comparison: the 'scary part', ie, to compare the resutls to external-data-sets (eg, used by authros to validate the accuracy of their won algorithm)
      @step (8): random-isntances-in-same data-set: write a fucntion to 'extend a given input-dataset with random permtuations of random-vectors (eg, where a fraction 'r' random-permtautions of [m][n] implies [m*(1+f)][n] matrix), and use 'this' ato assess the staiblity of a clsutering-algorithm <-- frist try 'elaborating' this.
      @step (9): clsuter-result-accruacy: compare 'our appraoch' to the 'PCA mapReduce appraoch' and MCL-clustering-appraoch
      @step (10): ["data-set"]x["data-filtering"]x["corr-metric"]: describe an appraoch where we 'suggest a descriptive correlation-metric based on the results from ["data-set"]x["data-filtering"]' <-- consider to relate/invetigate ["STD in disjoitn-groups", "rank-based disjoint-seperation"] to the accuracy/correctness of the cluster-prediction
      @step (11): 'experimental centering': .... if we 'choose differnt intial-centering-strategies in k-means and then identify the clsutering with best seperation then we may use the training-data-sets as a classificoant-appraoch' <-- how may the 'latter' be used in practice (ie, as we asusme 'unkown' data-sets are used as input? An example could be 'using the latter proceudre' to compare the simliarty of X different data-sets, eg, by first copmuting for matrix1[..][...], matrix2[..][...], matrix1[..][...]xmatrix2[..][...], and thereafter to use the 'result of each clsuteirng as the seed/start nodes in the clustering' <-- try describing 'an experiemental setup wrt. the latter'
      @step (12): "combining mulitple corr-types in one distance-matrix", eg, ['+', '-', '*', 'max(..)', 'min(..)', abs(..), ...]. Motivation is to try 'overcoming valeus which are in different scales in the same feature' <-- given the 'possible ambiguity of the latter' are there any other/better appraoches 'than our latter approach'? 
      @step (13): "find the correlation-metrics which maximizes the PCA-cluster-seperation (eg, wrt. the sum-of-10-best-PCs)" ... an inspriation for the latter is found in the work of ["http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0144059"] which states that "A study by Perlibakas demonstrated that a modified version of this distance [ie, the Manhattan distance-metric] measure is among the best distance measures for PCA-based face recognition [34]".
      @step (14): "iterativly remove eitehr the most-central-vertices, leas-central-vertices and a combiatnion of the latter both wrt. using/including the 'in-betweeen-verticers' and 'below--above-vertices'". Use 'this approach' to test/evlauate the clsutering wrt. ....??... <-- try to both describe 'an applciaiotn of the latter' and a 'min-max-selection-strategy' to 'find the best-desciprtive cases' (ie, based on 'what we are interested in finding').
      @step (15): "Test if MINE-cluster-appraoch manages to produce clsuter-rstuls which are outside the predction-in-accrauceis assicated to Silhouette-index" ... eg, to compare with the algorithm produced/designed by ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"] ... where our Null-hypotehss is that (a) the authros algorithm 'comarped' to atlerantve/other algorithms 'inside the signficance-threshold' ... and (b) our MINE-based appraoch considerably boosts the result-accruacy. .... <--   som en speremientell idea: [mulig ny aritkkel]: å bruke correlasjons-metrikker (eks. Hamming-dsitance) for å finne ut 'lovlige misforståelser' (eks. i epsoter mtp. skriveleifer), dvs. å 'bruke varians i tilsvarende data-sett for å bestemme signrfiancs-threshold på både cluster-comparsion-metrikker, cluster-algorithms og correlation-metrics <-- for et kontkre teskmepl (mtp. cluster-algoritherm) kan du se ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"], dvs. der sistnevte velger å si at 'noen få desimlar forteller at forfatternes algortihme er mye bedre' <-- merk i denne sammenheng at sitnevnte aritkkel burke 1.5 år på å'komme igjennom', dvs. nok en svak kandidat.
      @step (16): "test diffenret algortihm-permtuations to combine HCA with k-means" ... eg, wrt. the work of ["http://www.cs.cmu.edu/~kmuruges/Home_files/hybridbisect.pdf"]
      @step ():
      @step ():



      // FIXME[JC]: may you 'itnrospect' upon .....??....
 **/


/**
   @enum e_typeOf_clusterComparison_evaluationMetrics_fixed
   @brief enumeratres the different clsuter-evluation-strategies which we 'apply' for a 'fxied' kmeans-permtated comparsion (oekseth, 06. des. 2016).
 **/
typedef enum e_typeOf_clusterComparison_evaluationMetrics_fixed {
  e_typeOf_clusterComparison_evaluationMetrics_fixed_kMeans_avg,
  e_typeOf_clusterComparison_evaluationMetrics_fixed_kMeans_median,
  e_typeOf_clusterComparison_evaluationMetrics_fixed_kMeans_medoid,
  e_typeOf_clusterComparison_evaluationMetrics_fixed_SOM,
  //! --------------------------------------------------------------------------------------------
  e_typeOf_clusterComparison_evaluationMetrics_fixed_undef //! ie, the 'cunt' of enums in [ªbove].
} e_typeOf_clusterComparison_evaluationMetrics_fixed_t;

/**
   @brief build a sample-amtrix with 'defined' seperation between each cluster
   @param <nrows> is the [n, n] matrix-diemsinos to use
   @param <cnt_clusters> is the number of clsuters to evlauate/cmoptue for.
   @param <score_weak> is the score to be asisnged to no-members
   @param <score_strong> is the score to be asisgned to members
   @param <obj_clusterMembers> hold the cluster-assignmetns [cluster-id][vertex-id] for the vertices.
   @return  a enw-allcoated object.
   @remarks 
   - is sued to investigate 'extemly obvious' cases wrt. clustering: to redcue the 'oviousness' choose "score_weak =~score_strong"
   - the obj_clusterMembers is used to 'speicfy the dieal case wrt. tlcuster-asisgments', ie, where "score_weak =~ score_strong" would indiate that there is 'no clear-case wrt. clsuter-assignmetns.
 **/
static s_kt_matrix_t kt_buildSampleSet__disjointSquares(const uint nrows, const uint cnt_clusters, const t_float score_weak, const t_float score_strong, s_kt_matrix_t *obj_clusterMembers) {
  assert(nrows <= cnt_clusters); assert(obj_clusterMembers);
  //! Intaite and set the default value:
  s_kt_matrix matrix; init__s_kt_matrix(&matrix, nrows, nrows, /*isTo_allocateWeightColumns=*/false);
  setDefaultValueTo__allCells__kt_matrix(&matrix, score_weak);
  

  // TODO: consider writign a new fucntion where we ... 'allow' for 'a grudal increase in difference between clusters', eg, to use a "percentReduction*(score_strong - score_weak)/cnt_in_each" to 'decrease the clear-cut-ness wrt. the clsuters'.
  // TODO: consider writign a new fucntion where we ... 
  // TODO: consider writign a new fucntion where we ... 

  uint row_id = 0;
  const uint cnt_in_each = (uint)((t_float)nrows/(t_float)cnt_clusters);
  //const uint nrows_floor = cnt_in_each * cnt_clusters;
  for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
    const uint start_pos = row_id; assert(start_pos < nrows);
    uint clusterSize_local = ((cluster_id +1) != cnt_clusters) ? cnt_in_each : (start_pos - row_id); //! ie, if in the last cluster then 'update the remaining set of vertices'.
    const uint last_pos = row_id + clusterSize_local;       assert(last_pos <= nrows);
    for(; row_id < last_pos; row_id++) {
      for(uint col_id = start_pos; col_id < last_pos; col_id++) {
	set_cell__s_kt_matrix(&matrix, row_id, col_id, score_strong);
	//! Update the clsuter-mberships:
	set_cell__s_kt_matrix(obj_clusterMembers, cluster_id, row_id, /*score=*/1);
      }
    }
  }
  assert(row_id == nrows); //! ie, as we expect all trwos to have been set

  //!
  //! @return:
  return matrix;
}

/**
   @brief apply example-cases where the 'human eye' may clearly see the 'regions of seperation', ie, where the disjointness 'between teh clsuters' is obvious (oekseth, 06. des. 2016).
   @param <nrows> is the [n, n] matrix-diemsinos to use
   @param <cnt_clusters> is the number of clsuters to evlauate/cmoptue for.
   @param <obj_clusterAssignments> which if set is used in combiatnion with obj_clusterAssignments_currentRow to insert the 'current devaition from the ideal clsuter-assignment', using a [cluster-id][vertex-id]
   @param <obj_clusterAssignments_currentRow> describes the 'order' which this function is called in, ie, the row_id to udpate in obj_clusterAssignments
   @param <obj_metric> is the distance-object to be used.
   @param <score_strong> is the 'score' to be used for 'vertices int he same clsuter'
   @param <score_weak> is the 'score' to be used for no-match clusters
   @remarks 
   -- application: among others we in this fucntion demosntrates the 'direct use' of the k-means and disjoint-result-objects    
 **/
static void  obvious_disjointCases(const uint nrows, const uint cnt_clusters, s_kt_matrix *obj_clusterAssignments, const uint obj_clusterAssignments_currentRow, s_kt_correlationMetric_t obj_metric, const t_float score_strong, const t_float score_weak) {
  assert(nrows != UINT_MAX);
  assert(nrows <= cnt_clusters);  


  { //! Test an explict case where we expect the the dsijoint-network to be 'exremely obvious':
    //! -----------------
    //!
    //!
    //const t_float empty_value = 0; 
    printf("#\t From an [%u, %u] matrix compute cnt-clusters=%u, where values=[weak, strong]=[%f, %f]. Observation at [%s]:%s:%d\n", nrows, nrows, cnt_clusters, score_weak, score_strong, __FUNCTION__, __FILE__, __LINE__);
    //! Initaite the lcoal object to hold the 'ideal clusters':
    s_kt_matrix_t obj_clusterMembers;
    assert(false); // FIXME: figure out 'hop' above may be intalised .... and write a new function in our "kt_api.c" to construct/compute the "Rand index" score.
    assert(false); // FIXME: when [ªbove] issue is fixed then remember de-allcoating [above] "obj_clusterMembers"

    //!
    s_kt_matrix_t matrix = kt_buildSampleSet__disjointSquares(nrows, cnt_clusters, score_weak, score_strong, &obj_clusterMembers);
    
    //! -----------------
    //!
    //! Validate the properties of the input-matrix:
    assert(matrix.nrows == nrows);     assert(matrix.ncols == nrows); //! ie, as we expect a symmetic matrix.
    //! Export the reuslt, an 'export' which is called iot. manually verify tha tthe reulst correponds to our exepctiaotns'
    const char *stringOf_resultFile = "ex_kMeans.1.tsv";
    export__singleCall__s_kt_matrix_t(&matrix, stringOf_resultFile, NULL);
    assert(false); // FXIEM: laod [ªbove] into our kt_matrix.c and thereafter expor tthe reuslt ... ie, to manually verify tha tthe reulst correponds to our exepctiaotns'


    assert(false); // FIXME: write an itnroduciton describing ... the usefullness of 'the results prodcued by this funciton' ... 
    assert(false); // FIXME: write an itnroduciton describing ... 
    assert(false); // FIXME: write an itnroduciton describing ... 


    

    //! -----------------
    //!
    //! Apply clustering:
    { //! Cluster using: disjoint-forests with rank-trehshold=20%

      assert(false); // FIXME: write a wrapper-function in our "kt_api.c" which
      assert(false); // FIXME: add seomthing

      assert(false); // FIXME: update our "obj_clusterMembers" wrt. [ªbove] "obj_clusterMembers" ... write a new function in our "kt_api.c" ... and then call 'this'.
    }
    //! ------------
    { //! Cluster using: disjoint-forests with rank-trehshold=30%


      assert(false); // FIXME: add seomthing
    }
    //! ------------
    { //! Cluster using: disjoitn-forests w/'dynamic rank-trehsholds' where we 'iterate' from rank-trehsholds[10%, 20%, 30%] until [cnt_clusters/2 ... cnt_clusters] disjotin-sileandsa re found.


      assert(false); // FIXME: add seomthing
    }





    //! ------------
    { //! Cluster using: k-means w/center-prop="mean":
      assert(matrix.nrows == matrix.ncols); //! ie, to simplify our evaluation.
      s_kt_clusterAlg_fixed_resultObject_t obj_result; init__s_kt_clusterAlg_fixed_resultObject_t(&obj_result, cnt_clusters, matrix.nrows);
      s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, NULL); //! ie, as the 'correlaiton-call' should result in an adjcency-matrix.
      const bool is_ok = kt_matrix__kluster(obj_metric, &matrix, &obj_result, cnt_clusters, /*npass=*/1000, /*isTo_useMean=*/true, ktApi__afterCorrelation, NULL);
      assert(is_ok); //! ie, as otheriwse would inciate in-correct results.

      
      assert(false); // FIXME: add a call to our to-be-written "Rands idnex" comptatuion

      e_typeOf_clusterComparison_evaluationMetrics_fixed_t typeOf_cmp = e_typeOf_clusterComparison_evaluationMetrics_fixed_kMeans_avg;
      assert(false); // FIXME: use [ªbove] to update the obj_clusterAssignments based on the [above] comptued 'clsuter-comparison-score'

      if(true) { //! then we manully investigat ehe results:
	printHumanReadableResult__toStdout__s_kt_clusterAlg_fixed_resultObject_t(&obj_result, /*mapOf_names=*/NULL);
      }
      free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
    }
    //! ------------
    { //! Cluster using: k-means w/center-prop="median":
      assert(matrix.nrows == matrix.ncols); //! ie, to simplify our evaluation.
      s_kt_clusterAlg_fixed_resultObject_t obj_result; init__s_kt_clusterAlg_fixed_resultObject_t(&obj_result, cnt_clusters, matrix.nrows);
      s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, NULL); //! ie, as the 'correlaiton-call' should result in an adjcency-matrix.
      const bool is_ok = kt_matrix__kluster(obj_metric, &matrix, &obj_result, cnt_clusters,  /*npass=*/1000, /*isTo_useMean=*/false, ktApi__afterCorrelation, NULL);
      assert(is_ok); //! ie, as otheriwse would inciate in-correct results.

      
      assert(false); // FIXME: add a call to our to-be-written "Rands idnex" comptatuion

      if(obj_clusterAssignments) {
	e_typeOf_clusterComparison_evaluationMetrics_fixed_t typeOf_cmp = e_typeOf_clusterComparison_evaluationMetrics_fixed_kMeans_median;
	assert(false); // FIXME: use [ªbove] to update the obj_clusterAssignments based on the [above] comptued 'clsuter-comparison-score'
      }
      if(true) { //! then we manully investigat ehe results:
	printHumanReadableResult__toStdout__s_kt_clusterAlg_fixed_resultObject_t(&obj_result, /*mapOf_names=*/NULL);
      }
      free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
    }
    //! ------------
    { //! Cluster using: k-means w/center-prop="medoid":
      assert(matrix.nrows == matrix.ncols); //! ie, to simplify our evaluation.
      s_kt_clusterAlg_fixed_resultObject_t obj_result; init__s_kt_clusterAlg_fixed_resultObject_t(&obj_result, cnt_clusters, matrix.nrows);
      s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, NULL); //! ie, as the 'correlaiton-call' should result in an adjcency-matrix.
      //s_kt_clusterAlg_fixed_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
      const bool is_ok = kt_matrix__kmedoids(obj_metric, &matrix, &obj_result, cnt_clusters, /*npass=*/1000, ktApi__afterCorrelation, NULL);
      assert(is_ok); //! ie, as otheriwse would inciate in-correct results.

      assert(false); // FIXME: add a call to our to-be-written "Rands idnex" comptatuion

      if(obj_clusterAssignments) {
	e_typeOf_clusterComparison_evaluationMetrics_fixed_t typeOf_cmp = e_typeOf_clusterComparison_evaluationMetrics_fixed_kMeans_medoid;
	assert(false); // FIXME: use [ªbove] to update the obj_clusterAssignments based on the [above] comptued 'clsuter-comparison-score'
      }


      if(true) { //! then we manully investigat ehe results:
	printHumanReadableResult__toStdout__s_kt_clusterAlg_fixed_resultObject_t(&obj_result, /*mapOf_names=*/NULL);
      }
      free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
    }


    //! ------------
    { //! Cluster using: SOM:
      assert(matrix.nrows == matrix.ncols); //! ie, to simplify our evaluation.
      const uint nxgrid = cnt_clusters; const uint nygrid = 1; //! ie, to 'ensure' that we comotpeut eh same number of clsuters as in k-means.
      s_kt_clusterAlg_SOM_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(&obj_result, matrix.nrows, nxgrid, nygrid);
      // FXIEM:try/epxierement with differne 'inti-tau' valeus --- and compare the smialirty in the results.
      s_kt_api_config_t ktApi__afterCorrelation = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, NULL); //! ie, as the 'correlaiton-call' should result in an adjcency-matrix.
      const bool is_ok = kt_matrix__som(obj_metric, &matrix, &obj_result, nxgrid, nygrid, /*init-Tay=*/0.1, /*npass=*/1000, ktApi__afterCorrelation);
      assert(is_ok); //! ie, as otheriwse would inciate in-correct results.
      
      // FIXME: try to describe an appraoch where we use the SOM-topology 'as basis for the clsuter-comparison' .... eg, remninismenct/similar to an HCA-clsuter-comaprsion-appraoch.
      
      
      assert(false); // FIXME: add a call to our to-be-written "Rands idnex" comptatuion

      if(obj_clusterAssignments) {
	e_typeOf_clusterComparison_evaluationMetrics_fixed_t typeOf_cmp = e_typeOf_clusterComparison_evaluationMetrics_fixed_SOM;
	assert(false); // FIXME: use [ªbove] to update the obj_clusterAssignments based on the [above] comptued 'clsuter-comparison-score'
      }

      if(true) { //! then we manully investigat ehe results:
	printHumanReadableResult__toStdout__s_kt_clusterAlg_SOM_resultObject(&obj_result, /*mapOf_names=*/NULL);
      }
      free__s_kt_clusterAlg_SOM_resultObject_t(&obj_result);
    }
    
    //! -----------------
    //!
    //! De-allocate:
    free__s_kt_matrix(&matrix);
  }


  assert(false); // FIXME: updat ehte fucntion-input param to inlcude a matrix ... and udpate this matrix wrt. the Rand-index-scores between the 'ideal assigment' and the 'different cluster-assigment-strategies'.
  assert(false); // FIXME: add seomthing

}




//! the main funciton for logic-texting
void ex_kMeans_main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  
  //printf("cnt-args=%u, at %s:%d\n", array_cnt, __FILE__, __LINE__);

  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  if(isTo_processAll || (0 == strncmp("obvious-disjointCases", array[2], strlen("obvious-disjointCases"))) ) {
    //! @brief valdiate that the clsuter-results is as we would expect, ie, a 'manual control-comparison' both wrt. teh clsuter-asisgnments and wrt. the result of the cluster-comparison-metrics (eg, "Rands index").
    //! Note[ref]: from a breif introductioni to hyptoetis-testing through z-valeus and lower-tail, upper-tail and both-tails see "http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/BS704_HypothesisTest-Means-Proportions/BS704_HypothesisTest-Means-Proportions3.html"
    /**
       @key "1dCmp::simpleRank"
       @idea evaluate accuracy of cluster-predictions using/through established 1d-vector comparison-stratgies. The accuracy is investigated using a multi-dimensional appraoch, ie, where the result-accraucy-vector has a large number of 'hidden dimensionalities'. The 'hidden dimensionalities' (in the result-comparison-vector) is the main/maijor complexity in accuracy-estiamtions of clsuter-results: to utlize the variability in cluster-predictions to hyptohese/predict how differnet cluster-algorithm-permtautions may imrpvoe a given data-set ..... In this 'simpklict evlauation-strategy' we make use of cosnitinal comparign (eg, Baysian condtionalty-therory when assessing/comparing the accuracy of different grouyp--group relationships). ... 
       @remarks in this example we strive/seek to perofmr a 1-dimeionsla comarpsion-appraoch: to provide an intutive itnroduction to the complexities (which we both examplfieis and demostnrates the applcialibty of), compelxities which we (in this example) realize/infer/demosntrate are of critical importance (in the understanding of cluster-accuracy through different value-adjustment-appraoches). The results (of this test-example-appraoch-case) is the 'reasoning-base of our appraoch', iw, erhew we demonstrate/examplify of how 'a 1-idmesional comparison-appraoch clearly indicates that there are un-answered questions which needs to be investigatied'. <-- try to improve the latter, focusing on .....??...
       @remarks in our appraoch (as described below) we outline/describe a strategy to evlauate multi-diemsinonal simliarities through 1d-vector-comparison, eg, wrt. the cluster-result-influcence of the 250+ correlation-metrics described in the hpLyssis software. In this test-appraoch we have chosen to simplify our evaluation by 'limting our evlauation-scope' to only apply/evaluate cluster-resutls wrt. the "Rand index" cluster-comparison-score. (In our test="cluster-comparison-metrics" we evlauate/assess the impact/improtance of the latter simplficiaton, and from the test="cluster-comparison-metrics" we infer/onclude that the cluster-preidtion-resutls are misleading/erronrous/non-descipve for data-sets wehre ....??...). In this 1d-comparison-approach we seeks to minimize the error-rate conserned with 'the use of non-comparable observations in the same comparison'. 
       An example of a 'naivistic approach' is to group the cluster-accuracy-predictions into K groups by first comptuign the rank, and thereafter compare the 'group-membership-rank' to set of reference-cases (eg, "Pearson" and "Euclid") iot. hyptoese singificance of cluster-appraoches. An example (fo the latter) conserns a sample-result-vector=[2.0, 1.0, 3.8, ...., 0.4, ...], where we for the latter sample-vector might consider to use the computed STD('sample-result-vector') as a '+- each value in sample-result-vector'. To examplify the latter it would be both misealding and wrong to "assert that a given correlation-metric X outperforms 'all other corrleation-emtrics Y' if X only occurs for one specific data-set-pertubation at one specific exeuction-point" (ie, to avoid/prevent random non-stable results to be used as best-practice-guidelines). However, as the altter value-trheshold describes the 'complete vector' (and Not the variance assicate to each observatino) we belive that 'the latter describe approach would be wrong/potinelss/meaningless'. 
       From the latter example-case we observe the need to unify/categorize the observations (eg, for 'the cluster-accuracy assicated to the same correlation-emtric in differnet data-set'). An example (of the latter) is to compute the STD 'for each class' and then use this 'knowldge of variance in each class/group to serve as a max-boundary wrt. interesting elements':  ....??...  ... The latter 'proposed appraoch' is an extension of the 'naivistict appraoch' (described prior to the latter example). 
       @testComplexityReductionStrategy in this test-valaution we restrict/simplify our evalaution to: (a) data-sets (where we use data-set-permtuations described in "kt_buildSampleSet__disjointSquares(..)") and (b) cluster-comparison-metric (limited to "Rands adjusted index", a 'limititation' used iot. map/bridge our results to the <PCA-aritcle>). 
       ***************************
       @method idea is to idneitfy accuracy of cluster-predictions to the 'ideal case', and thereafter use the 'hidden/underlying properties' to 'extract the sensitvity from eahc measurement-point/observation'. In this test-appraoch we first apply a basisc/naive comparison-appraoch before increasing the test-evaluation-complexity through a comparison of/with 'mutual support based on the hidden/undlerying proerty/proerpteis' (of each measurement-point).
       *******
       --step(1) comapre clusters-comparison-strategies to an 'ideal' cluster and assume that high-accuracy-classifications should be preferred by users:
       step(1.a) compute Rand-index for [data-set]x[value-adjustment, eg, 'none', PCA, 'Z-based filtering seprately for each column' (eg, where we asusme that each feature represents a time-point)', mean-adjustment, rank-adjustment and log-transform]x[cluster-alg]x[correlation-metric]x[muliple-calls-same-algorithm] (where we compare to an ideal cluster-classificaiton); 
       step(1.b) compute the rank of each computation; 
       step(1.c) find the percent of metric-cluster-algs which results in a better performance than [Euclid, squared-Pearson]x[PCA, 'none']; 
       *******
       --step(2) simple-comparison, where our heuristic(s) is the assumption that the vertex-classes are well-defined/consistent in/across muliple groups/value-domain-axis. An example of a psosbile group-group relationship conserns the evlauation-use-case to "find the correlation-metrics in k-means clustering which are least sensitive to internal-k-means-cluster-viaraiblity and random-noise". To comptue the latter we: 
       step(2.a) build/construct feature-vectors for each group (eg, group-catery1=["Euclid", "Pearson", "MINE-TIC", "Shannon", "Wavge-Hedges", etc.] in the group-category2=[<random-noise:0....1>, <algortihm-call:1....M>], eg, for [Euclid][random-noise:0.5]='score=<Rands-index|global-rank|etc.>'; 
       step(2.b) seperately for each group-category1 (eg, for Euclid) compute the AVG and STD (and other sensitive-deivaiton measures); 
       step(2.c) rank the correlation-metrics, and from the 'ranking' idneitfy/'suggest' the 'most accurate correlation-emtrics' (eg, into accuracy-categories=[low, medium, high]);
       step(2.d) permutate-step(2.b): reduce the sensitvity of 'outliers' by removing (from the comparison) entites which has a rank-score (in each feature-vector) less than a threshold=T=[0.1...0.8], and perofrm an-all-agaisnt-all-comparison' (using the latter results) where we predict/hyptohese ....??...
       step(2.remarks) an 'alternative appraoch' (when compared to above) is use k-means clsutering (as an alternative to a rank-based cateroization-appraoch), a case which we investigate in test="multipleCorrelationMetrics-obvious-disjointCases". From the latter test we observe ...??... for which we assert/dervie taht ....??...
       ....  ie, for which it then may be of interest to evalaute  the singifcnace/ability of 'using the group--class relationship to identiy super-groups of group-memberships, eg, to infer/assess ....??... a use-case conserns/is to describe/idneitfy 'how a staiblity-measure may be comptued for k-means clustering'. ... In this 'iterative appraoch' we: step(2.a) divide the 'ranked list' into [low, medium, high] and (from the latter) compute "percent(class(m)) = 'sum percentages for class(m) in each cluster(i) where percent-value > rank-threshold'" or alatneriavly "percent(class(m)) = 'number of class(m) in all clsuters(i) with a number>rank-thresohld'/'total number of instances from class(m)'; step(2.b) 
       *******
       --step(3) ... to overcome the latter (where wekaness cosnerns the arbitrayrness/randomness/unpredictability wrt. the rank-threshold) we ....??... <-- try suggestign/descrinb 'such a proceudre' ... 
       ... extend [above] to .... assess/evlauate the 'signficance of the results' using 'sinficance-tests' such as "Spearmans Student-t singficance for both both-tails, left-tail and -right-tailed' dsitributiosn  ... where the the *tails* are used in hypotehssi-evalaution (eg, wrt. data-evaluation) <-- 'prior to this' write a sample-test-case wherew we investigate some 'extreme hyptetical cases iot. validate our methdo to refute and accept hypothesis', where an example (of the latter) consens the case "where we expect/require t(0)='null-hyphothesis'='an algorithm to have at least 80 percent of its memberships in the same cluster'" .....??....
       step(3.a) for each of the input-dimension-axis sepeately compute both AVG, Median, STD, Kurstosis, Skewness, "Student-t (Pearson)", "Student-t (Spearman)", and "Jarque-Bera" (both based/for the 'values' and the 'ranks' in the 'global value-result-comparison') <-- TODO: consider to use "SES" and "SEF" in komparison with "skewness" to anser the question: "is the 'skewness' (for a given data-set-evluation) 'too much for random chance to be the explanation' ("http://brownmath.com/stat/shape.htm#Eq4")?" <-- in this context how 'may we know that our data-set has a normal-dsitribution?' ... and simliarlty 'how may we adjust our data-set-evalaution to assure a normal distribuiton'? ..... <-- wrt. "kurtosis" use 'kurtosis' to explain/assess 'the importance of varaince in a data-set' (eg, to descide 'the impoatcncde of STD in data-set-accuracy') ... ie, try to test out different 'kurtosis-thresholds' wrt. 'weighting of variance between differnet samples'. <-- wrt. [STD, skewness, kurtosis] write a 'test-data-set and test-code with distribitions simliar to ("http://brownmath.com/stat/shape.htm#Eq4"), eg, for frequecnce=f for the folloing data-distributions: [f=4, f=4, f=4, f=4], [f=0, f=2, f=6, f=8], [f=8, f=6, f=2, f=0], [f=8, f=0, f=8, f=0], [f=0, f=8, f=0, f=8] .... and use 'tehse tests' to asssess/evlaute .....??...   <-- compare 'this' with ("https://www.uky.edu/Centers/HIV/cjt765/9.Skewness%20and%20Kurtosis.pdf" [whcih states that Kustosis inidicates noramlity, ie, Kustosis may be used as an idnciator of a Gaussian-normality-STD-distribution], "https://www.spcforexcel.com/knowledge/basic-statistics/are-skewness-and-kurtosis-useful-statistics" [which porovides an elaborate itnroduction to Skewness and Kurstosis], .... ) we observe/assert that a 'clear seperation' would/will indicate .....??.... 
       step(3.b) perform an all-against-all-group-prediction-comparison iot. relate the groups (ie, seprately for each group, eg, wrt. 'rank +- STD') and from the latter find disjoint regions; 
       step(3.c) ....??...  (eg, 'for a skewness-threshold T(s) idneitfy a linear-consequent-increase in data-accruacy, and where we assume that each dsijoint-group describes diffferent algorithm-data-set-relationships' (where the skewness-threshold is used as a 'noise-filter wrt. data-set-prediction-accuracy').
       --step(4.a) .... 
       --step(4.b) .... 
       --step(4.c) .... 
       <-- 
       <-- consider to use percent-chance to be in ....??... to .....??....
       ************************************
       @methodSimliarTo
       @apiRequirements 
       @useCases 
       --"stability and convergence": variable-axis=[data-set, randomness, correlation-metrics, cluster-algorithms]. ...  eg, to test/evlauate the variablitliy of "Euclid and k-means-clustering-prediction" VS "Pearson and k-means-clustering-prediction": our assumption/idea/hyptoesis is that a 'good/descriptive metric amanges to provide the same classification-accruacy-degree in simliar data-sets and in conquecntive runs/calls (for the same data-set), ie, where a low spread in STD and high avg-rank-score indiciates a describe/accurate correlation-metric (for when applied to a fixed/speicifed/given clsuter-algorithm)' ....  <-- consider to 'extend the latter' and then 'use this in our 2d-k-means elvuation for clsutering-results'
       --"the accuracy of correlation-metrics in cluster-prediction": to generalize [ªbove] to describe/idneitfy/evaluate the 'spread' between differnet predicitosn. Examples of groups conserns "correlation-metric" .. where each 'group' is denoted/speicifed by the assicated 'correlation-emtic' (eg, Euclid and Pearson). <--- try to imrpvoe the latter by descirbing diffenret categories which we evlauate/clasisfy ... and possible inferences/evlauation-resutls, such as wrt. ....??... ..... we expect to observe/idneitfy/conslude that neither "Euclid" nor "Perasons" provides the best 'cosnistency in muliple calls to the same data-set' ... ie, that there are 'better corrleation-metrics wr.t the provided/(given data-set)' .... where we 'adjust the data-set until such a data-set si foudn' ... where our null-hyptoesiss is that .....??....  	 <--- could we extend/use teh latter in our '2d-k-means-comaprsion-strategy', where we 'relate the different measures' (eg, by 'comptuing a correlation-emtric of the ranks assicated to each vector', eg, wrt. [class="..."][group="...."] <-- try completing 'this'.)
       --"data-set-best-fit":  ... find the data-sets which providess teh best support/proof/backing for the a given 'hypothesis wrt. clsuter-relationship' .... 'manulaly' idneityf the data-sets which 'produce' the best-fit for the PCA-authors hyptoeiss-description wrt. 'PCA and k-means'.
       --"cluster-accuracy-impact wrt. value-mask-filtering": ... [mask-->disjoint-count]: evaluate/test an analsysi-appraoch where we test 'the simlairty of value-range-fildered matrix' (prior to clsutering) and thd clsutering-result (when compared to the 'ideal case') ..... eg, using the funciton "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(..)" to get the nubmer of disjoint-comptued-clsuters ... a result which may be sued to ....??.... 
       --"result-impact of filtering-techniques (eg, PCA)": ... find the min-max-extreme-deviation-cases (using the "Rand Index" clsuter-comparison-metric as a comparison-framework-score ... use the latter in a modifeid 'iterative algorithm' (to maximize the extreme cases), eg, where we 'select the different parameter-combinatiosn' based on .....??.... 'repeat above comparison-step' ... where our null-hyptoesiss is that .....??...
       --"sub-sampling and data-permutation": .... of interst is to apply data-permtautions and use the results (of the latter) to idneitfy the msot stable clusters .... in cotnrast to our 'work conserning the statbility of the clsutering-algorithms, as described by A. Rakhlin et al., focus/consentstes/dsicussses the selection of k-clsuters in k-means clustering: "... a stability-based solution has been used for at least a few decades by practitioners.  The approach stipulates that, for each K in some range, several clustering solutions should be computed by sub-sampling or perturbing the data. The best value of K is that for which the clustering solution are most “similar”. This rule of thumb is used in practice, although, to our knowledge, there is very little theoretical justification in the literature" ["http://papers.nips.cc/paper/3116-stability-of-k-means-clustering.pdf"] <-- FIXME: consider to write a funciton/method based 'on the latter described appraoch', ie, to 'apply data-permtautiosn, disjoitn-fitlering and k-means-algorithms in combiatnion with a ....??.... clsuter-score-scucecess-critiera\' <-- try to describe the altter, ie, a 'measure for how to known taht we have found a global (rather than a local) minima'.
       --"": 
       --"": 
       --"Klatremus": ...
       --"real-life": ... 
    **/

    const uint nrows = 6; const uint cnt_clusters = 2; 
    //! Intaite and set the default value:
    //s_kt_matrix obj_clusterAssignments; init__s_kt_matrix(&obj_clusterAssignments, cnt_clusters, nrows, /*isTo_allocateWeightColumns=*/false);
    //setDefaultValueTo__allCells__kt_matrix(&obj_clusterAssignments, /*score-no-cluster-match=*/0);
    uint obj_clusterAssignments_currentRow = 0; //! ie, as this is the 'first call we make for th e ojbect'.
    //! The call:
    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
    const t_float score_weak = 1;     const t_float score_strong = 10; //! where we use a 'linear step'fucntion' between these 'using' the "cnt_randomPermtuationsToUse" proeprty    
    obvious_disjointCases(nrows, cnt_clusters, /*obj_clusterAssignments=*/NULL, obj_clusterAssignments_currentRow, obj_metric, score_strong, score_weak);
    //!
    //! De-allocate:
    //free__s_kt_matrix(&obj_clusterAssignments);
    { //! The evalaution-method
      
      assert(false); // FIXME[programming]: implemnet the "Rand index" and "Rand adjusted index".
      assert(false); // FIXME[programming]: above update our variance-comptuation in our "matrix_deviation.c" to support the 'one-pass' STD-comptuation described in "http://www.lboro.ac.uk/media/wwwlboroacuk/content/mlsc/downloads/var_stand_deviat_group.pdf"
      assert(false); // FIXME[programming]: implemnet "standard error of skewness (SES)" and "standard error of kurtosis (SEK)" ("http://brownmath.com/stat/shape.htm#Eq4") in our "matrix_deviation.c", where to use "SES" in komparison with "skewness" to anser the question: "is the 'skewness' (for a given data-set-evluation) 'too much for random chance to be the explanation'?"
      assert(false); // FIXME: add somethign .-.. 
    }


    //! -------------------
    assert(false); // FIXME[real-life-data-sets--example]: for a data-set consisnteitn of ....??... from KnittingTools we assess/infer/relate ....??..
    assert(false); // FIXME[signi-test time=1-day]: try to 'translate' eahc of the signi-example-test in the 'downlaoded presentiaton' into tests reflecting [above] .... and thereafter 'apply these tests'.

    //!
    //! Increment the counter-variable:
    cnt_Tests_evalauted++;
  }


  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  if(isTo_processAll || (0 == strncmp("multipleCorrelationMetrics-obvious-disjointCases", array[2], strlen("multipleCorrelationMetrics-obvious-disjointCases"))) ) {
    //! @brief for all of the correlation-metrics in hpLysis comptue clsuters, and comapre the cluster-predictions 'to the ideal cluster-case'. 
    /**
       @key "2dCmp::combinatorics"
       @idea figure out the core-parameters which 'causes' varibility in different 'cluster paramters': (result of the latter is) to help researchers to buidl their expermental setup; to suggest a parametrization-strategy which may be used to increase the 'indedence-rate' between 'user-generated hypothesis' and 'the results from a cluster-prediction-analsysis'. 
       @remarks in this test-section we apply a combinatorical appraoch to find the min--max seperation-cases between groups, ie, to 'idneityf teh combiatnos which provides the best expalantion for different test-cases'. Our work is thereafore an extension of our test="obvious-disjointCases": in order to compare the multi-dimensional space (where each 'space' reflects to the scores assicated to one partulcar parameter-axiss, eg, "randomness-fraciton-1" VS "randomness-fraciton-2") k-means clustering is used. From the k-means clsutering-appraoch we are 'able' to handle the multi-dimesnioanl proerpteis (of our comparison, ie, in cotrnast to a 1d-comparison-appraoch uisng the sorted rank-proerpty, ie, in contrast to test="obvious-disjointCases").
       @remarks in this section we seek/try to relate data-sets with similar agreements in Rand-index-score (both seprately for a given k-means algorithm and across/between diffenret k-means-algoritm-pertubrations). Core parameter-axiss are ["data-set", "correlation-metric", "cluster-algoritm"]. In this work our null-hyptoesis is that "there will be an increased seperation in the algorithm-results for a corresponding increase in data-ambugiity", ie, for which we apriory expects to observe 'cluster-categories of data-sets which are simliar to their idnetiites'. To address the latter we categorize the 'different axiss of data-variability' into genearlized groups, and investigate how these grousp relate: to investgiate all possiblie combiantiosn defined 'by the combination-space of the groups', and from teh latter idnetiyf the 'extreme cases of influnece', thereby provding an expalaniton for 'what cases a correlation-metric is sutiable to use (eg, when cobmined to a partiuclar cluster-algorithm)
.       @remarks the results (assicated to this test) may (among others) be used to categorize the different distance-metrics, a cateorization/classification which (among others) seeks to idneityf the influence-effect of correlation-metrics, eg, to answer the question: "if we choose a data-type and a correlation-metric (to what extend) have we then pre-defined the result (of our evaluation)?". Wrt. the PCA-article we seek to estalish the correctness of limiting the reseharc-scope to the correlation-metrics of "Pearson" and "Euclid", ie, to asses/idneitfy/describe 'to what extend such a limtiiaon in focus may lead to errnous/misleading data inferences/conslusions/assertiosn. From the latter we observe how 'this test-case extends test="obvious-disjointCases" wrt. ....??...
       @remarks [Null-hypothesis]: in below we describe the hypothes which our method (in this test-case) is desinged to answer:
       assertion(a) there is a signicant prediction-resutl-difference between correlation-metrics, a result-difference which is provfudn/marked/evident in all of our evaluated/applied cluster-algorithms. (<-- update our test="obvious-disjointCases" to evalaute this using a student-t-dsitribution-correlation-test, and thereafter use 'this tets cobmiantorical approach to assess/find the best/max/min  combiatniosn which miaximises/miniizes the signifance of the clsuter-predictions'). Evaluation-strategy: use the cluster-correctness-degree-rank from muliple 'perspectives' (eg, 'randomness' and 'algirtihm-stailbityf-or-same-data-set') to find the correlation-metrics which 'reacts' simliary to a set/combination of 'stimuli' (eg, wrt. different noise-fraction-rates and different degree of 'oviousness between/in clsuter-seperaitoni');
       assertion(b) there are certain correlation-metrics which provide strong sensitvity to data-noise (ie, should Not be used for 'driect' k-means clsutering gvien their prediciton-unstaiblity, though may be higliy descirptive wrt. annotaiton/idneitficiaotn of cluster-sub-proerpteis); <-- extend our 'earlier' 1d-rank-based appraoch with muliple features (ie, to build and clsuter-analyse a feature-matrix, eg, wrt. 'skewness', 'kurtosis' and 'AVG') and use this relationship to find correlation-metrics which 'exhibtis a simliar reaction/affinitiy to dfiferent data-set-perturbations'.
       assertion(c) the clsuter-simlarity between the different corlreaiton-emtrics are stronly dpeendnet on the data-input, ie, similiarites in cluster-prediction maps (for a number of corrleaiton-emtics) directly to the cluster-algorithsm: hence a selective chose of cluster-algortihsm and data-sets may (even for singivcant clsuter-predictions) reflect the assumptison (of/for the researchs rather than the the un-supervised proerpteis which we would expect a cluster-user to idneitfy/investgiate) <--- try to figure out how we may address 'assertion(c)' (ie, if the latter tunrs out being the case)  <--- we inspect/evlauate 'assertion(c)' by/through a ... by first finding the data-sets which causes/results in 'extreme min--max predictions' (eg, seperately for each correlation-metric) and thereafter investigate 'to what extent the ranks assicated to each data-set-perturbation are shared across different data-sets' (which we outline in [”elow] "step(2)"). The result of the latter "step(2)" evaluation may be used to hyptohese 'classes of correlation-metrics which are simlarly related to changes in data' <-- TODO[JC]: may you JC evlaute the latter assumption ... and 'if the alter is correct' then try fidnign broader implcaitons/applicaitons 'of the comparisonst-rategy described in "step(2)"?'
       @testComplexityReductionStrategy in this evlauation/test we use a 'fixed k-count of clusters' and a 'fixed cluster-comparisn-metric'. This in order to 'icnrease our focus' on the effects/impacts of the corrleaiton-meitrcs, eg, answering the quesiton(s): "if we assume it is correct to use both the corrleaiton-emtrics of Pearson adn Euclid, what are the correlation-metrics which provide/results-in similar predictosn (ie, which then should also be correct to use, ie, based on teh cluster-rpediotns in this evaluation)?"
       @method extend test="obvious-disjointCases" to use k-means clsutering (as an alternative to a rank-based cateroization-appraoch). Difference wrt. the results are belivived/hypothesed to be wrt. ....??.... .... to ivnestigate the latter we .....??... .... a challenge conserns the large number of possible 'group-cases', eg, for the group-setset described by the following 'tree of categories': groups=["numeric-measures"=["AVG", "STD", "Skewness", "Kurtosis", "Student-t Z-score"], "data-matrix-input"=["cluster-seperation-degree", "randomness-factor", "methods for setting data-masks", "methods for data-filtering"=["logaritm", "STD", "ranks", "mean-adjustment", ...]], "algorithms"=["correlation-algorithms"=["specific-metric", "post-processing-type"=["none", "rank", "binary"], "correlation-category"], "cluster-algorithms"=["k-means"=["AVG", "median", "medoid"], "SOM"] .... 	 
       ***************************
       step(1) an evalaution where we use combiantorics to findt eh value-groups which 'in combination provides the best-desciprtive power for the dsitributioni in signifance-results': <-- try examplifying 'how this approach may be useful'.
       step(1.a) build feature-approximations from results in test="obvious-disjointCases", eg, AVG("euclid" | randomnessFraction="0.3")
       step(1.b) combinatoric::each: compute correlations for each 'combinatoric case', where we combine the [ªbove] feature-approximations for different categories (eg, for "randomnessFraction=[0....0.9]" and "clusterAmbigutiy=[0.1 ... 0.9]"). Select the 'combinatorical appraoches to evlauate' based on ....??... <-- try suggesting/scaffolding 'such an appraoch'.
       step(1.c) combinatoric::each: comptue clsuters (for each of the 'combiantoric cases'). To handle/investigate clsuter-rpediciton-variableitliy we compute 'for the same data-set a cluster-prediction muliple times'. In the 'fidnign fot eh best-cases' we seperately store/fidn knowledge/data-predictiosn wrt.: 
       step(1.c.1) error-approximatation: sum of distance between different clsuters
       step(1.c.2) "prediction-variabltiliy": compute Rand-index for each algorithm-call, and thereafter unify the Rand-index-socres to compute the student-t-score (for the clsuters), a student-t--score which is used to assess the stability-agreements in a gvien predcition
       step(1.d) evaluate: seperately (for [ªbove] results) rank the results, and thereafter find the min-and-max cases: use the min-max-cases to hypthese the 'data-perburation-groups which ahve msot in common', eg, wrt. .....??....
       step(1.e) evaluate: test the signifance of above results (eg, trhoguh "studnet-t test"): ....??...  ... 
       *******
       step(2) ... seperately find/compute the 'extrem cases' for different groups (eg, seperately for different correlation-metrics) .... and combine the results through .....??...  ... An example of the latter is to: ...??... .... :
       step(2.a) for group X (eg, where X="all correlaiton-metrics supported by hpLysis") find the data-set-pertubrations which results in extreme-seperations wrt. cluster-predictions: enumerate each 'data-set-pertubation' and 'store the complete set of predictions for the data-set-enumerations' (where we use the comparison-strategies listed in [ªbove] "step(1)" to find the min--max extreme-prediction-cases); 
       step(2.b) compare the predictions: use the [ªbove] result as a 'feature-vector in a feature-matrix', and thereafter compute clsuters 'where seperately use all of the hpLysis correlation-atmrix in the clsuter-analsysis';
       step(2.c) find extreme cluster-prediction-cases: 	 
       step(2.c.1) feature-matrix: copmute "Rand index" between the cluster-predictions: result is a feature-matrix 'of Rand index scores';
       step(2.c.2) k-means-clustering: use 'the comptued feature-matrix' to infer/assess 'the clusters of metrics' <--- descirbe/suggest 'what these clsuters describe' (eg, how they may be used in "describing the extreme cases for data-sets assicated to correlation-metrics").
       *******
       step(3) ... a wekaness of [above] cosnerns the few number of groups which may be evalauted (approx. 20 group to insitstate 'at full') .... to overcome this we suggest a search-herutistic where we explroe a larger search-space (in the same exec-time): iterativ algorithm: ... a greedy algorithm: always 'expand' a 'best-scocing cobmiatnion' until 'the signifance of the repdiction-socre is no longer the best score' 
       *******
       step(4) ...
       ***************************
       @methodSimliarTo 
       @apiRequirements 
       @useCases 
       --"best-fit cluster-algorithms for differnet correlation-metrics": to investigate the best clsuter-algorithms to use for differnet classes of correlation-emtrics we ....??... <-- try desciribn 'such' ased on ª[bvoe] emthod-descipriton.
       --"relationship between random-permutations and correlation-metrics cluster-prediction-capabilities": 	 ... how the accuracy of correlation-emtics in clsuter-predictions are depdnent upon the data-set: assess affinity/sensitivty to randomness ... apply a multi-step-analsysi where we evaluate/ivnestgiate for: case(a) "the same data-set and zero randomness (eg, testing/evlauating the veiation between different correlation-metrics and their clsuter-meberships-imsiliarty to the correlation-metric-group (assicated to each corrleation-emtric))"; case(b) update latter: "different data-sets, where difference is found wrt. the value-sepration-score-difference between similar-regions and -non-similair regions"; case(c) update latter: "itnroduce randomness, and assess/evlauate the simlairity/inflcuence between the metrics"; case(d) update latter: "extend the number of test-metrics using: sub-step(a) muliple calls (for the same data-set- adn same cluster-algoirithnm and same correlation-metric); sub-step(b) mulipel evalaution-stratgies (eg, 'clsuter wrt. internal vairance for each: [correlation-metric, cluster-algorithm, correlation-metric-cluster-algorithm]')";
       --"how k-means-prediction-deviations may be quantified by randomness-fraction": ... find the corrleaiton-emtrics which have the strongest relationship to a data-randomizaiton-fraction: ..... Idea is to use 'variance in K-means clsutering to asses the stability of k-means clstuering at random peratiitons: what we assume is that the idea-k-solution will start moving/shifting when the data-set is permtuated: of interest is to evaluate to what degee/extend this shfiting-change takes place: if there is a singivant shiting (and high stability) of a clsuter-algorithm then we asusme the clustering-algortihm to be both highly stable and higly accurate (in its own contentct) for a particular data-set. If we assume that each k-means-configuraiton-algortihm is idnepdnet of each other (<--TDOO[JC]: is this correct?) then we may 'muliply the effects of each clsutering-stability-measure, eg, product=\Pi(prob(class(m), k-cluster=[2....])), where the proability (for each cluster-membership) is comptued in order to reflect the clsuter-staiblity, a clsuter-staiblity which we compute using ....??.... <-- update 'this' wehn we ha ve completed 'the descirpiton of the staiblity-measure in [ªbove] FIXME'
       --"cluster-prediction-stability": evaluate the stailbity of k-means clustierng if we ... intiate teh k-emans clusteirng with diffenret permtautions of the 'ideal sollution' (ie, testing the effects of using a 'training set wrt. the rpediciton-otucomes'). Our "null hyptoehsis is that the staitlbiyt of algorithms will be depednetn on both the clsuter-algorithm, data-set and correaltion-emtric (which is evalauted)". To test this assumption we: approach(a) fix the data-set and identify the min-max-seperations (sepeately for each correlation-metric), and use this min-max-seperation to find the correlation-metrics (and clsuter-algorithms) which resutls in 'the most extreme cases'; appraoch(b) build a feature-matrix cosnsitencing of ["Rands-index(avg) (for 'muliple calls to the same data-algorithm-permtaution')", AVG, STD], and thereafter apply clustering. Use the clstuering-result to approximate/describe/assess the 'stability of clustering-algorithms', eg, where we 'compare the clsuter-agreements between/for different cluster--datasSet cobminations'
       --"application of mask-disjoint exec-time-optimization": to answer the question of "to what extend mask-filtering-and-disjointSeperation may be used/utiliized". To answer the latter question we relate the 'cluster-stability-degree' to differnet data-sets (eg, where data-set are cateorized along the parameter-axis of [cluster-value-seperation][datapoint-random-factor]) <-- figure-ot: how may we 'compare the results of real-life data-sets to such a cotnrolel evalaution?'" .... .... The latter use-case is an example of where/how 'knowledge of a k-means algorithms sensitvity may be used' ... 
       --"": 
       --"": 
       --"Klatremus": ...
       --"real-life": ... 
    **/
    assert(false); // FIXME: update our Perl-script with [above].
    assert(false); // FIXME: add something:    
    const uint nrows = 6; const uint cnt_clusters = 2; 
    //! Intaite and set the default value:
    //s_kt_matrix obj_clusterAssignments; init__s_kt_matrix(&obj_clusterAssignments, cnt_clusters, nrows, /*isTo_allocateWeightColumns=*/false);
    //setDefaultValueTo__allCells__kt_matrix(&obj_clusterAssignments, /*score-no-cluster-match=*/0);
    uint obj_clusterAssignments_currentRow = 0; //! ie, as this is the 'first call we make for th e ojbect'.
    //! The call:
    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
    const t_float score_weak = 1;     const t_float score_strong = 10; //! where we use a 'linear step'fucntion' between these 'using' the "cnt_randomPermtuationsToUse" proeprty    

    assert(false); // FIXME: in [”elow] tierate throguh 'the combined set of correlation-metrics and adjustment=[none, rank, binary]
    /* obvious_disjointCases(nrows, cnt_clusters, /\*obj_clusterAssignments=*\/NULL, obj_clusterAssignments_currentRow, obj_metric, score_strong, score_weak); */


    { //! Apply an analysis-step:
      //! Note: ... 
      //! Note[idea]:

      assert(false); // FIXME[implementaiton]: update our k-menans algorithm 'to use the distance-scores directly' (rather than evalatuiign the simliary between the neightobus of each candiate-verex-pair) ... ie, 'add a new inptu-arpameter to "kt_clusterAlg_fixed.c" ... and investigtate/test hte 'sepration between cluster-algorithms ... eg, to use 'the later as a comparison-case for all cases' (eg, as the 'mid-point used to answer/test if a corrleation-emtric manages to increase the result-clstuer-rpediction-accuracy') <-- instead consider to udpate our 'current distance-fucnitosn with this' to specify new correlation-metircs wrt. [psaudoCompareDirectSimliarty__min, psaudoCompareDirectSimliarty__avg, psaudoCompareDirectSimliarty__max] .... Use the latter to illsutrate our appraoch we might consider 'an alternative appraoch' where we makes 'directly use the [vertex-i][vertex-k] score (ie, instead of computing 'their simlairty though a distance-correlation-metric').  <-- udpate use-case ....??... wrt. the latter ... wherew we 'use the syntetic clsuter-data-set to ivnestigate th impact/improtance of differnet value-spread-stategies'
      assert(false); // FIXME[implementaiton]: write code: (a) find the sum-of-distances between 'members in each clsuter' (ie, intra-clsuter-distance); (b) the "prediction-variabltiliy" (as descritbed in above method-descirption).
      assert(false); // FIXME: evaluate the improtance/sensitvity of the diffneret clsuter-gorups (ie, as described/iotuliend in oru [”elow] test-function-cases.)
      assert(false); // FIXME: add somethin
    }


    //! -----------------------------------------
    assert(false); // FIXME[real-life-data-sets--example]: for a data-set consisnteitn of ....??... from KnittingTools we assess/infer/relate ....??..
    //! -----------------------------------------
    
    //!
    //! De-allocate:
    //free__s_kt_matrix(&obj_clusterAssignments);
    //!
    //! Increment the counter-variable:
    cnt_Tests_evalauted++;
  }


  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  if(isTo_processAll || (0 == strncmp("cluster-comparison-metrics", array[2], strlen("cluster-comparison-metrics"))) ) {
    //! @brief ... 
      /**
	 @key "2dCmp::" 
	 @idea an application-focused test-case which describes/idneitifes the applicability of cluster-comparison-metrics in data-compiarson (eg, wrt. how different cluster-outcomes are classified).
	 @remarks an 'application-focused' test-case where we focus on relating different cluster-comparison-metrics (rahter then idneitfying new metohd/algorithms for data-compiarosn). In this test-section we .....??....
	 @remarks [Null-hypothesis]: "different cluster-comparison-metrics relates differently to different value-spread-properties in different data-set permtuations", a hypotsis which implies (ie, if correct) that each/all/most cluster-comparison-metircs are weakly desicptive for a large number of ddata-distributions/network-topologies, ie, where the implication of our latter hyptoesis is a prediction-skewness/data-specialisation (wrt. the diffnere tcorrelationc-omarpsion-metrics we evlauate/test). If the latter assumptioni hold then we infer that the PCA-artilce [ref] is non-descrptive (for signifcnat/large number of life-science cases), ie, as their instrumetnation impleis a 'bias' which may not be rperesentaitve for the broad range of PCA-applicaitons (eg, in life-science researhc). 
	 @remarks "test-design": in our experimental setup we focus on the folloiwng "evaluation-axis": axis(1) "metrics" (eg, Euclid VS MINE-TIC VS 'Rands index'); axis(2) "analsysis-input-data" (eg, 'value-difference between strongly and weakly correlated regions', 'randomness-permtaution-factor', ...); axis(3) "result-comparison" (eg, k-means, 'Bayesian conditional proability' (<-- try to concretis--exmaplify the latter), ..)
	 @testComplexityReductionStrategy 
	 @method compare ["cluster-comparison-metric"]x[confusion-matrix-data-set]x[value-spread=[low, medium, high]. Compute metric-scores for a set of confusion-matrix-data-sets.  Goal is to provide/gain insight into the simliarty of the cluster-comparison-metrics/algortihms: if we are able to observe a consistent change/increase/permtuation in cluster-agreement-descriptions between different categories of datasets then the 'combined descirption of different metric-categories is assumed/hyptosied to provide a perspective into differnet aspects of the clsuter-simlairity' (ie, to use a 'feature of metric-scores' rahter than a 'simple scalar valeu from one of the metrics'). 
	 ***************************
	 step(1) .... In order to evaluate/assess the latter we apply a three-step approach: 
	 step(1.a) seperately for each cateogry of datasets idneitfy category-groups (ie, of metrics which provide the same predictions); 
	 step(1.b) conceptually describe/ascribe the simlairty between the groups,  ..... ;  <-- outline/descrive the latter.
	 step(1.c) compare the different clusters (seperately using each of the cluster-comparsion-metrics, thereby prodcuing [clsuter-comparison-metric]=[set-of-clsuter-predicitons]). 
	 step(1.result) From the above/latter 'automated comparsion' we idneifying cluster-categories describign 'the agreement in clsuter-similiarty between/indepdent-of different valeu-distributions'. Our expetation/assertion is that clusters with 'low accuracy in cluster-predictions' will prodcue/result-in 'stronlgy overlapping clusters', a case inspected through .....??.... <-- cosndier instead to provide different 'cobmaitnso' of the "value-spread=[..]" case/option and then compare/asses the clsuter-simlairty wrt. the latter. .The result is a new-described appraoch to assess the accurayc of clsuter-comparison-metrics:
	 step(1.result.a) "different data-distributions/data-types results in different prediction-signfiicance-agreements": we have seperately classified cluster-comparsion-metrics based on their prediction-silmialrty to different systnitc data-sets: from the comparison we observe how ....??...; 
	 step(1.result.c) result-accraucy: from a transposition of the 'cluster-input-matrix' we have idneitfied a realtionships between data-sets (using the correlation-emtrics as a 'glue') which differes from the underlying/obvious topology-proerpties (of the data-sets). Hence we have valdiated/examplified how the correlation-matrics provides different acucrayc for different result-cases. In order to idneityf the cluster-comparison-metrics (evalauted wrt. teh clsuter-clasisficaiton using the simlairty between the data-sets) which best 'maps' to the simliarty-provifle (of the data-sets) we 'clasify the clsuter-correlation-metics based on ....??... and then idnetify the correlation-comparsion-metircs which prodcue the best-desicptitve data-set-clsuters' <--- try to improv ethe latter desciption (ie, as the latter in practise would imply a combaitnionr appraoch, ie, unfeasible)   .... and from the altter assess the accuracy of ... a synteitc appraoch to evaluate the accuracy of cluster-metrics ... 
	 *******
	 step(2) comptue the 'scores of accuracy' seprately for differnet axis in the data-set': ...??...
	 *******
	 step(3) .. establish/attribute/relate the cluster-comparison-metircs to 'stadnard measurements of deviation' (eg, wrt. the API-support found in hpLysiss "matrix_deviation.h") .... where we idneitfy/establish a 'well-defined fixed' relationship between STD, 'skewness', kurstosis, STD and 'thd differnet cluster-comparison-metrics ..... a relationship used to .....??.... <-- try to 'explain why a well-defined relationship between estalished measures of deviaiton-in-values and clsuter-comparison-metics may provide both an inner-boudnary and an outer-boudnary wrt. the signifcnace of differences in clsuter-meberships between different feature-groups .... and use the latter to esbalsihed signifciance-thresohlds for cluster-predictions  <--  from the result 'of this' update our "clusterAlg-internalSkew" 
	 *******
	 step(4) .. idenityf the complexitty of ....??...  
	 step(4.a) cluster-algorithms-and-data: comparison of different cluster-comparison-metrics at different degrees of cluster-data-ambigity <-- ...??..
	 step(4.b): .... 
	 step(4.b.1) data-sensitivty(a): find the cluster-comparison-algorithms which are over-represented in different data-set-ambugity-case: result used to hypotese the 'sensivity of different metrics for different data-types'; if there is a 'singifcant spread' this is assumed to indicate that cluster-comparison-metrics are skwed towards different data-sets <-- TODO[JC]: may you JC validate this hyposis-assumption? 
	 step(4.b.2) data-sensitivty(b): to investigate agreements in metrics when simliar data-set-entries are merged, ie, [cluster-comparison-metric][data-ambigutity---randomness]='score (eg, skewness)', and where we 'for each data-subset' for 'each of the latter cases find the min-path-distance and max-path-distance between the metrics, a min-max-proerpty which is used to ....??.... .... 
	 step(4.c) 
	 step(4.d) 
	 step(4.e) 
	 step(4.f) test applicalbity of the later on syentic data-sets (with a leess known explcit-degree-of-nosie-value), where we .....??... 
	 step(4.g) test on real-life-data-sets ....??....
	 *******
	 step(5) .... a method which describes ....??.... result is expected to be 'an appraoch to parametrize a data-set based on the different correlation-emtric-cateogires .... where a success-critier conserns 'an biltiy to provide a better cluster-speration in/through less exeuction-time' ... ie, a minimziaotn-problem wehre we seeks to ....??... <-- consider comapring 'this result' with the MINE correlaiton-emtric wr.t accuracy .... 
	 *******
	 step(6) ..
	 ***************************
	 @methodSimliarTo 
	 @apiRequirements 
	 @useCases 
	 --"sensitivty to small perturbations": ... "are there any cluster-comparison-emtrics which are more senistive to small clsuter-membership-permtuations (eg, when compared to both 'itself' and 'to optehr classes')?"; "are there any clsuter-data-distributiosn which are central for all/most of the cluster-comparison-metrics?"; "to what extent does the relationship between the different clsuter-comparison-metrics change when we narrow-down the scope-of-variation (ie, are there any well-deifned claseses of clsuter-comparison-simlairty-emtrics which areise for differentc cases)?". In order to answer the latter questions we ....??...
	 --"": 
	 --"": 
	 --"": 
       --"how data-sets becomes simliar though differnet cluster-comparison-metrics": .... to apply the test-methdology described in test="multipleCorrelationMetrics-obvious-disjointCases" to assses the simliarty in data-sets (when using the cluster-comparison-emtics as features) ... for each 'overlapping chunk of data-set entries which are approximate the same ... construct relatsionship using the "AVG +- STD" attribute (ie, where we 'assume that instances with a small variation in clsuter-mebership represents the same vertex') and then infer 'disjoint sets of regions between the cluster-comparison-metrics' .... use the 'ideintifed clusters' to assert/hyptoese ......??.... and therafter use/apply a clsuter-comparisn-appraoch (of the latter clusters) to infer/describe .....??.... .... an example is to answer: "For what network-cases are clsuter-comparison-metrics=[x, Y, Z] overlapping?" and similarily "if the cluster-comparison-metrics=[x, Y, Z] are overlapping then what are the core-attributes of the input-data-set?" (where we in the latter seeks/tries to 'transfer' knowldge dervied from clustering into/to a deiscrption of the correlation-input-matrix). <--- try to improve the 'cocneptual ucnderstanding of this appraoch'.
       --"Rand index: specificty": To answer the quesiton "to what extend does the cluster-comparison-metric cpature intenral variability?" we compare the Rand-index-score with the intra-clsuter-simlairty (betwen each clsuter-rpedicotn). Use the result(s) as an assment/indication of "how the Rands index amanges to explain differnet data-evlauations".
	 --"Klatremus": ...
	 --"real-life": ... 
       **/
    assert(false); // FIXME: update our [”elow] proceudre based on [ªbove] "researhc-questions"?

    //! -----------------------------------------------------------------------
    //!
    //! Experimental configuration:
    assert(false); // FIXME: update our Perl-script with [above].
    assert(false); // FIXME: add something:

    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
    //! ---------------------------------------------
    //! -----------------------------------------------------------------------
    //!
    //! Comptue the clsuters:
    assert(false); // FIXME: add something:
    assert(false); // FIXME: add a multi-step appraoch where we: (a) evaluate using the "cofnusion-matrix" clsuter-comaprsuion-emtrics described in .....??...; (b) incldue the 'default' cluster-c-cluster-comaprsion-emtics; (c) incldue the 'compelte set of hpLysiss 250+ corrleation-metrics



    { //! Apply an analysis-step:

      assert(false); // FIXME[programming] implement the "Rand index" correlation-score in our "kt_api" and our "e_kt_correlationFunction.h" ... and the otehr cluster-correlation-metrics: ["Rands index", "Rands adjsuted index", "value thresholding", "Wallace", "Fowlkes–Mallows", "Mirkin", "variation of information", "correlation-min-max-sum-appraoches (ie, the clsuter-c-appraoch)" .... <and the data-comparison-metrics listed in our "matrix_deviation.h">]
    }
  }


  //!*****************************************************************************
  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  if(isTo_processAll || (0 == strncmp("clusterAlg-internalSkew", array[2], strlen("clusterAlg-internalSkew"))) ) {
    //! @brief 
    //! -----------------------------------------------------------------------
      /**
	 @key "2dCmp::" 
	 @idea  an applicaiton-centered appraoch where we seek to idneityf the accuracy-thresholds of the cluster-algorithms, eg, to idnetify the 'largest mask-valeu we may use, ie, minimize exueciton-time and maixmize accuracy'. 
	 @remarks from/based-on the cluster-classifciation-results we make use of hpLysis "matrix_deviation.h" API for evalaution of basis/default singifcance, eg, through our "get_forRow_sampleDeviation__matrix_deviation(..)" function-call. An example-input is a list of feature-counts in each of the clusters, eg, "[count=4, count=0, count=0, count=2] for kmeans=4 and number-of-samples-each-combination=6", where the lattter list may describe the 'number of times a non-masked-raw-matrix-input combined with a k-means-median algorithm results in the same classification'. From a combination of multiple 'feature-count-evaluations' (ie, as examplifed in the latter example-list-case) it is possible to assess/infer/assert/hypothese the variability of cluster-comparison-algorithms (an assertion which we elaborate in below use-case/example/test descriptiosn). From the/this comparison we establish a 'boundary wrt. prediction-accuracy', a 'predction-accurayc' which we use in our "clusterAlg-multipleCorrelationMetrics" to evalaute the acucrayc of muliple correlation-metrics.  ..... 
	 @remarks .... the importance of 'combining feature-measurement-vectors' into 'one data-set before clusterinig' is due to .....??.... .... to assess this we 'comapre with a seperate use-case where we idneitfy the variance in the deviation in clsuter-rpedictiosn, and from teh deviaiton ....??... <--- consider adding support 'for this' (ie, after we have explained/described the importance of 'combining the data-set-feature-vector Before clustering').
	 @remarks "disjoint--small-value-sensivitity": to assess the improtance of including valeus below a noise-trehshold in data-filteirng (ie, the use of 'masks') we inlcude seperate/distinct algorithm-calls where we apply a threshold-based data-filtering write a new evaluation wrt. data-evaluation: what we are itnersted in is to establish the deviaiton-in-accruacy if a disjoint-forest-optionzation-strategy is used (ie, wherew we 'filter away' (or: mask) insigifcnt 'blank' value-cell-scores. From the latter we investigate the 'maximum number of disjoitn-forests which may be a-priory idneitfied using an increased mask-threshold', ie, with goal of answering the performacne-optiosnation-use-case: "does there acutally exist any large/signifcnat disjoitn regions of no-match cases after correlation-matriax are applied, ie, waht is the expected perofrmacne-icnrease of/for 'such'?". In order to test/evaluate the latter we include/write a use-case-test evaluating the cluster-membership-deviation for 'algorithm--minValueThreshold--dataId'. What we expect is that there will be dsitinct signifcance-cases for different cateogries of data-sets, ie, where an otpsinzation-strategy is to 'allow' users to specify/hyptoese the 'core properties' of their data-set, a 'user-specifcioant' which is based on our idneitficiaton of 'the singifcaint data-sets-groups where all/most of a given/paritcular--mvalueTreshold-pertmuation-algrotihm is found in?' .... To summarize we expect/hope the latter evluation to 'establish that the use of disjoitn-rank-masks does Not reduce the overall accurayc wr.t cluster-rpedictiosn' <--- when the results of the latter 'is compelted' then udpate our test="clusterAlg-multipleCorrelationMetrics", ie, in order to 'idneify the correlation-metic---dataset-combinations which are least sensitvite to noise-disturbance (ie, where all/most of the correlation-emtrics are 'placed' in the same cluster)'.
	 @testComplexityReductionStrategy we expect there to be a well-defined relationship between cluster-comparison-metrics and 'stadnard measurements of deviation', an appraoch (expected to be) undertaken in our "cluster-comparison-metrics". The improtance of the latter 'relationship' conserns the inferences which may be drwawn for a comparison with differnet clsuter-membershisp for idneitites/veritces with the same 'nderlyign core proeprties'. To illsutrate/examplify the latter, teh "Rands index" is oftne used as a metric ofr data-comparison. However it is not nromal/frequent to observe appraoches where Rands index is used to measure/attribute/indicate the noise-uncertainty in predictions (<-- JC: validate this assertion): an intersting proerpty of heristic algorithsm (such as k-means and k-medians) are thier use/utlizaiton of randomenss to predict results, ie, where large resutl-deviations provides indicaitons of the 'solution-space' for a given data-set. Therefore, the use of 'muliple isntances' for the same data-row may/will indicate the possible variance in expalantion-strength (for a given data-set and assicated features). From the latter we observe how differneces in clsuter-mebershisp may be used to assess/evlauate in-singfinficanct differences (in clsuter-rpedictions). To examplify the applciability of the latter: "if an algorithm X with exeuction-time X(t) has a prediction-quality inside the preidciton-quality-borders of an alrotihm Y with exuecitoin-time Y(t) and X(t) is singifcnatly smaller than Y(t) then there is a strong argument for uitlizing/using algorithm X. A similiar relationhips/use-case may be seen for orthology-inclusion into existing data-sets: "if a data-set X (which is enirched with uncertain/ptuative ontology-encirchment-annotations) has an annotation-prediction-quality inside the prediction-quality-borders of a data-set Y, then we may assume that the users/reseahrc-aritlces cititing/using Y may safely use hte ontolgoy-enriched data-set X". In the latter examples we have demosntrated how Bayesian conditionality-theroy may be used in combination with k-means clsutering to predict/infer singifcance of relationships. From the latter examples we observe how we have made use of our 'idneitifed relationship between clsuter-comparison-metrics and estibasliehd metrics/meausres of value-deviation to hyptoese conidtinal relationships/memberships: from the results in our use-case="cluster-comparison-metrics" we established how 'well-defined relationship between estalished measures of deviaiton-in-values and clsuter-comparison-metics may provide both an inner-boudnary and an outer-boudnary wrt. the signifcnace of differences in clsuter-meberships between different feature-groups', and where the latter results have been used (in thsi evlauation <--- TODO: udpate our text-test-description after 'this' is ompcleted) to esbalsihed signifciance-thresohlds for cluster-predictions  <-- how may be construct differnet 'samples' for the different data-sets ... and how may the 'anntoation-quality' be evaluated?   
	 @method to modify our "RandsIndex--seperationStrenght" to inlcude different 'min-rank' thresohlds for cases=["rows", "columns", "rows AND columns"] ... and then (in the clsuter-evlauation-seciton) evaluate how these 'cluster-algorithm-permtautions' divges from the 'non-random-filtered data-cluster-predictiosn':
	 ***************************
	 step(1) "simple inferences based on cluster-count", where goalsi to assess the 'radius of predictions':
	 step(1.a) to iterate through the 'same loop as used in the vlaue-inseriton';  <-- try explaining the latter, ie, 'add mroe detials'.
	 step(1.b) count the number of 'occurences' in each cluster, infer core-properpteis (eg, through "get_forRow_sampleDeviation__matrix_deviation(..)") and comptue the lower-and-upper-tail, where the result is stored for use in 'step(c)'; 
	 step(1.c) compare the value-distributions using 'both the upper and lower tail' and wrt. the 'value/propability boundary defined by AVG +- STD'. 
	 step(1.result) idneitfy/evaluate the "singi--clusterCmp-radius": comapre the results from [ªbove] "signi-radius" with different cluster-comparison-metrics, ie, where we 'combine the feature-vector-count for each clsuter' with an ideal case where 'all vertices are in the same cluster', and then compare/combine the latter results with [ªbov€] vairance-evaluation through a visual inspection (using hpLysis web-itnerface) <-- try to extend/idneitfy metrics which may be used to intersect/merge 'metrics of skewness' with clsuter-comparison-metrics  <--- in 'this result-evalaution' we make use of the results in our "cluster-comparison-metrics" to ....??... <--- update 'the latter' when we have compelted the result-evlauation wrt. our "cluster-comparison-metrics"
	 *******
	 step(2) ... an appraoch wehre we try to maximize the 'use of masks in order to reduce the exuection-time' .... where we ini this evlauation-strategy use a method where we ....??...
	 *******
	 step(3) ..
	 *******
	 step(4) ..
	 *******
	 step(5) ..
	 ***************************
	 @methodSimliarTo 
	 @apiRequirements 
	 @useCases 
	 --"differences in ambigutiteis": ... evlauate/inspect 'different measurements for relating uncertainty/in-accracuceis/aibities between differnet cluster-corrleaiton-calls'. Exampels of the latter conserns: (a) HCA (where we compare the k-means clusters by .....??...); (b) 'sum-of-intra-cluster-distances' (where we 'weight' the sum-of-lnks between two clusters VS the 'internal weight' in a givne clsuter, and use the latter to assess the cluster-predciotn-stialbity by ....??...); (c) .....??.... .... To compare the [ªbove] clsuter-results with our "syntetic dsibituiotns of data-clusters" .... and try cateogrizing the diffneret 'cluster-rpedictiosn' based on the results in our [ªbove] "cluster-comparison-metrics"
	 --"": 
	 --"": 
	 --"": 
	 --"Klatremus": ...
	 --"real-life": ... 
       **/
    //!
    //! Experimental configuration:
    assert(false); // FIXME: update our Perl-script with [above].
    assert(false); // FIXME: add something:
    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
    //! ---------------------------------------------
    assert(false); // FIXME: do not 'start exeuction on this' until we have completed reuslt-evaluation of our "cluster-comparison-metrics"
    //! -----------------------------------------------------------------------
    //!
    //! Comptue the clsuters:
    assert(false); // FIXME: add something: 
    assert(false); // FIXME: apply a 'multi-step' appraoch in [ªbove] ... testing different data-set-combiantions ... and incrementally increasing the data-input-compelxity: step(a) ; step(b) test/evlauate wrt

    { //! Apply an analysis-step:
      //! Note: ... 

      assert(false); // FXIME: add a call to our function-test-case-spec.
    }



    //! -----------------------------------------
    assert(false); // FIXME[real-life-data-sets--example]: for a data-set consisnteitn of ....??... from KnittingTools we assess/infer/relate ....??..
    //! -----------------------------------------


    //! Increment the counter-variable:
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("clusterAlg-multipleCorrelationMetrics", array[2], strlen("clusterAlg-multipleCorrelationMetrics"))) ) {
    //! @brief 
      /**
	 @key "2dCmp::" 
	 @idea an 'applicaiton-section' where we identify the 'impact' of corlreaiotn-metics .... eg, to 'find the correlation-emtrics which are least sensitive to random permtautions' and 'the different grousp/Categoreis of correlation-metrics wrt. the differennces in cateorrizzed/elvauated data-sets.
	 @remarks ... 
	 @testComplexityReductionStrategy as an 'axiom' we assume that there is a well-defined relationship between 'internal vairance in predictions' and 'skewness wrt. the cluster-algorithm-rpedictions' .... ie, that we have established procedures for comparison and assement of singifcnat membership wrt. groups.
	 @method 
	 ***************************
	 step(1) ..
	 *******
	 step(2) ..
	 *******
	 step(3) ..
	 ***************************
	 @methodSimliarTo 
	 @apiRequirements 
	 @useCases 
	 --"": 
	 --"": 
	 --"": 
	 --"": 
	 --"Klatremus": ...
	 --"real-life": ... 
       **/
    //! -----------------------------------------------------------------------
    //!
    //! Experimental configuration:
    assert(false); // FIXME: update our Perl-script with [above].
    assert(false); // FIXME: add something:

    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
    //! ---------------------------------------------
    assert(false); // FIXME: do not 'start exeuction on this' until we have completed reuslt-evaluation of our "cluster-comparison-metrics"
    //! -----------------------------------------------------------------------
    //!
    //! Comptue the clsuters:
    assert(false); // FIXME: add something:
    assert(false); 


    { //! Apply an analysis-step:
      //! Note: ... 

      assert(false); // FIXME: .... 

    }
  }


  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  //! **********************************************************************************************************************************
  if(isTo_processAll || (0 == strncmp("RandsIndex--seperationStrenght", array[2], strlen("RandsIndex--seperationStrenght"))) ) {
    //! -----------------------------------------------------------------------
    //!
      /**
	 @key "2dCmp::seperation" 
	 @idea investigate the seperation-strenght of the cluster-comparison-metrics (eg, "Rands index") wrt. different 'classes' of data-sets:
	 @remarks in this section we demosntrate how the reliability of cluster-memberships (eg, the variance between different 'clsuter-calls' for the same data-set) may be used as an idnciation of the clsuter-stailbity (ofr a particular cluster-algorithm). The result (of this clsuter-evaluation-study) may therefore be used to .....??....
	 @remarks in this comparsion-strategy we assume that the cluster-algortihsm (which we evlauate) share a simliar variance/unstaiblity wrt. cluster-predictison for simliar data-sets, an assumption which is based on the poerpteis/data-distributions of the input-dataset (eg, wrt. ...??... <-- try to relate the 'preorpteis of our sampl-datyas-et' to the 'proerpties of the clsuter-algoritms we evlauate'). However, as each algorihm is ....??... ...       
	 @testComplexityReductionStrategy we assume that the variance between each of the cluster-algorithsm (which we invetigate) is insignicant when compared to the internal error in each cluster-algorithm (ie, where we evaluate heuirstic clsuter-algorithms which makes use of seleciton-choises which are Not gurnateed to be globally optimal). For details of the latter please see our "clusterAlg-internalSkew" , where we ....??... <-- update the latter when we have completed the 'latter' clsuter-result-evaluation' ... and then update [below].
	 @method 
	 ***************************
	 step(1) ..
	 *******
	 step(2) .. wehre we ... 'use one cluster-algorithm and one clsuter-comparison-algorithm' to relate the different dats-sets, ie, where the input-data-matrix consists of feautre-rows genrated 'calling' the same algorithm mulipel times (for the same data-set) ... ie, where the 'stailbity is implictly evaluated throug the simliarty of the features and the data-sets' .... and where  we comapre the 'signficance of the clsuter-emberships wrt. increased varaince of simliar-indexes datasets' through .....??.... <--- consider using/updating our web-interface for a 'manual comaprsion of simliarty between between predictions geernated for different 'cluster-comparison-metrics' and different 'cluster-algorithms', and then 'use a screen-short/URL from the latter to assert (ie, provide indicatiosn) of .....??.... ... and where we in the web-interface compare 'the seperate clsuter-resutls produced tby/through the use of (a) different correlation-clsuter-metrics and (b) different cluster-algorithms, and use the result to assess/hyptoese .....??....
	 *******
	 step(3) .. test the 'stailibty of difnfere tlcuster-algorithms' ... answering/testinf if "are there any clsuter-aglrotihsm which are more stable at particular data-set-cluster-seperiaotn-granarulairites (than other clsuter-algorithms)?, ie, is there an over-representaiotn for some of teh alrotihsm wrt. simliarty"... by computing [dataset][muliple-runs] ... and then test the 'number of times the same-algortihm-is-found-in-the-same-clsuter' <-- try improvign teh latter description by .....??.... <-- consider to 'permutate/mutate/change this sue-case to instead evlauate the resutls of [ªbove] comparison ... where were:
	 step(3.a) comptue clusters for seprate cases of 'randomness-fractions';
	 step(3.b) perform an all-agaisnt-all-comparison of 'this' (ie, to construct m*m confusion-amtrices, where each cofnusion-matrix has diemsoins=[k, k]), 
	 step(3.c) cluster the latter results iot. idneitfy categgories of  .....??....  <-- the latter may be visualize using a 'forest' (where the clusters in/fromt eh first 'evlauation' are 'glued togheter in the second cluster-evaluation-phase, ie, muliple layers to infer/assess connecitvity') <-- what are the comaprsions-trategies which may be used to idneitfy/categorze "the stability of algorithms across different data-sets"? To answer the latter question we observe the relationships between ....??... and ...??..., which we use to infer/relate/hyptoehse that  .....??...
	 *******
	 step(4) .. compute a 'large confusion-matrix' (<-- ....??...) for all clusters and thereafter in a 'post-analsys-step' seperately apply each cluster-comparison-metric. In this approach we: 
	 step(4.a) prediction-accuracy(manual): perform a manual comparsion of clusters genreated using two different cluster-comparison-emtrics. An example (of the latter) is to assess both "the simlairty in clsuter-rpedcitison between diffenret cluster-repdictoin-algorithms" and "the importance of different clustter-comparison-emtrics"; 
	 step(4.b) synetitc data-set: compare the predion-capbilites of cluster-comparsion-metrics using a sytneitc data-set of clsuter-cistributiosn; 
	 step(4.c) real-life data-set: compute cluster-comparison-clusters from an all-agaisnt-all comparison of the cluster-simliarties for the different data-sets, and then maunually ivnestigate/asees the comaprsin-resutls of the latter;
	 *******
	 step(5) ..
	 *******
	 step(6) ..
	 *******
	 step(7) ..
	 ***************************
	 @methodSimliarTo 
	 @apiRequirements 
	 @useCases 
	 --"": 
	 --"": 
	 --"": 
	 --"": 
	 --"Klatremus": ...
	 --"real-life": ... 
       **/
    //!
    //! Experimental configuration:
    const uint nrows = 200;     const uint cnt_clusters = 2; 
    const uint cnt_dataSets_toConstruct = 40; const uint cnt_samplesEach_dataSet = 10; 
    const uint cnt_randomPermtuationsToUse = 1; 
    // FIXME: write differnet tests for the "attenuationIncreaseFactor" .... and comapre the result-difference <-- describe a 'test' wrt. this.
    const t_float attenuationIncreaseFactor = 1; //! which is used to 'set' the degree of 'gradualr change between intiially disjoint-regions'.
    const t_float score_weak_base = 1;     const t_float score_strong_base = 10; //! where we use a 'linear step'fucntion' between these 'using' the "cnt_randomPermtuationsToUse" proeprty
    //const uint cnt_clusterRegions = 10;   
    //! The distance-metric to use:
    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    s_kt_correlationMetric_t obj_metric; init__s_kt_correlationMetric_t(&obj_metric, metric_id, e_kt_categoryOf_correaltionPreStep_none);
    //! ---------------------------------------------
    //! -----------------------------------------------------------------------
    //!
    //! 
    const uint cnt_total = cnt_dataSets_toConstruct*cnt_samplesEach_dataSet*cnt_randomPermtuationsToUse;
    if( (score_strong_base - score_weak_base) <= 0) {
      fprintf(stderr, "!!\t Your score-values=[%f <= %f] did nto follwo the expectedd proeprty, ie, please investigate your configurations. %s Observation at [%s]:%s:%d\n", score_strong_base, score_weak_base, __get_defaultErrorMessage());
    }
    if( (attenuationIncreaseFactor < 0) || (attenuationIncreaseFactor > 1) )  {
      fprintf(stderr, "!!\t Your attenuationIncreaseFactor=%f were Not in the space [0 <= 1], ie, the latter parameter did not follwo the expectedd proeprty, ie, please investigate your configurations. %s Observation at [%s]:%s:%d\n", attenuationIncreaseFactor, __get_defaultErrorMessage());
    }  

    //! Intaite and set the default value:
    s_kt_matrix obj_clusterAssignments; init__s_kt_matrix(&obj_clusterAssignments, cnt_total, e_typeOf_clusterComparison_evaluationMetrics_fixed_undef, /*isTo_allocateWeightColumns=*/false);
    setDefaultValueTo__allCells__kt_matrix(&obj_clusterAssignments, /*score-no-cluster-match=*/0);
    uint obj_clusterAssignments_currentRow = 0; //! ie, as this is the 'first call we make for th e ojbect'.
    //! The call:

    assert( (score_strong_base - score_weak_base) > 0);
    const t_float increment_step = attenuationIncreaseFactor * ( (score_strong_base - score_weak_base)/(t_float)cnt_dataSets_toConstruct);
    assert(increment_step != 0);

    for(uint data_id = 0; data_id < cnt_dataSets_toConstruct; data_id++) {
      //!
      //! Update the 'simlairty-score' between teh different data-sets:
      const t_float score_weak = score_weak_base*(1 + increment_step);       const t_float score_strong = score_strong_base; 

      //!
      //! Apply the dfifernet 'levels' of requested randomness:
      for(uint random_id = 0; random_id < cnt_randomPermtuationsToUse; random_id++) {

	assert(cnt_randomPermtuationsToUse == 1); // FIXME: add support for "cnt_randomPermtuationsToUse > 1" ... ie to ivnestigate the clsuter-membership-permtuation if 'random swapping' is 'itnroduced' iot. evlauate the clsuter-rpediiocnt-results for 'noise-cases'.

	//!
	//! Then we use the 'same data-set' for all of [”elow] samples, ie, to investigate the different clsuter-selecitosn made by 'each call to a hueritsic clsuter-alorithm', terheby 'gaining' isngith into the staiblity of the predicted clsuters:
	for(uint sample_id = 0; sample_id < cnt_samplesEach_dataSet; sample_id++) {
	  //!
	  //! Compute the clusters given the setting in question:
	  obvious_disjointCases(nrows, cnt_clusters, &obj_clusterAssignments, obj_clusterAssignments_currentRow, obj_metric, score_strong, score_weak);
	}
      }
    }

    //! ---------------------------------------------
    //! 
    //! Apply the clustering:      
    { //! Apply an anslsys-step:

      assert(false); // FIXME: add something

      //! ****** Result-analysis *********
      { //! 
	//! ---------------------------------------------
	//! 
	{ //! Question: "is the cluster-predictions reliable (ie, when we use the variance in clsuter-results as a comaprsion-basis)?". 
	  //! Note: For the same "data_id" compare/idneityf the 'chance' of a data-set 'to have all its cluster-comparison' in the same cluster:
	  assert(cnt_samplesEach_dataSet > 1); //! ie, as this comparison would otehrwise be pointless.
	  assert(false); // FIXME: add something
	  
	  assert(false); // FIXME: describe ... how ... to 'get meaningful resutls from scuh a comparison'
	  assert(false); // FIXME: describe ... how ... to assess the simliarty in cluster-results without a cluster-comparison <-- consider to 'write a seperate test-evlauation-case' for this ... where we ....??... ... and thereafter compare the latter results to 'this': the 'intersection wrt. predicontiso' are found wrt. .....??...
	  assert(false); // FIXME: describe ... how ...
	  assert(false); // FIXME: describe ... consider to ... evlauate seperately for differnet 'blocks' of "increment_step"
	  assert(false); // FIXME: describe ... consider to ... 
	}
	//! ---------------------------------------------
	//! 
	{ //! Question: "what is the noise-sensitivty (of our 'combined appraoch')?"
	  //! Note: we investgiate how does the combined accuracy of the clsuter-algorithms relates to increased noise in the data-sets: through a comaprsion with ....??... we ....??...
	  //! Note: the 'basis' (for this comparison) is the assumption that the 'simplictly in clustering' will be reflected in the cluster-memberships: for an increased 'intra-cluster-simliarty' the cluster-memberships are expected to be more diffuse (or: confused). 
	  assert(increment_step != 0); //! ie, as this comparison would otehrwise be pointless.
	  assert(false); // FIXME: add something

	  assert(false); // FIXME: describe a comparison-algorithm to hyptoese teh 'ideal valeu-srepad' wrt. the 'increased dis-simlairty relating to icnreased index-differences wrt. data_id indexes'  ... and then use 'this' to assess teh 'importance/impact of differences in clsuter-embershisp'
	  assert(false); // FIXME: describe ... how to 'get meaningful resutls from scuh a comparison'
	  assert(false); // FIXME: describe ... how to 'get emningful resutls from scuh a comparison' ... ie, where we have a linear perturbation/confusion increase in the values: the 'chance of vertices to be in the same cluster should therefore be increasing with decreased difference in index-score'. The latter assumption/assertion/observation may be represented using a matrix[delta(index=abs(data_id_1-data_id_2))][ random_perturbation_id ] = 'count of vertices in the same cluster', a result which we use to ...??...
	  assert(false); // FIXME: write a 'stand-alone use-case-example' wrt. [ªbove] 'value-range-comparsion-strategy' ... where we ....??... ... and where novelty (wr.t reseahrc-findings) conserns 'to relate two orthogonal groups/clusters/value-seperations' to .....??.. 
	  assert(false); // FIXME: describe ... 
	}
	//! ---------------------------------------------
	//! 
	{ //! Question: ""
	  //! Note: ... randomness ...
	  assert(cnt_samplesEach_dataSet > 1); //! ie, as this comparison would otehrwise be pointless.
	  // -----
	  assert(false); // FIXME: describe ... 
	  assert(false); // FIXME: try to scaffold a new evaluation-appraoch wehere we 'increase the k-number-of-clusters' ... and in the evlauation assess/evlauate the signficnace of .....??..
	  // -----
	  assert(false); // FIXME: add something	  
	}
	//! ---------------------------------------------
	//! 
	{ //! Question: ""
	  //! Note: 
	  assert(cnt_samplesEach_dataSet > 1); //! ie, as this comparison would otehrwise be pointless.
	  assert(false); // FIXME: add something
	  
	  assert(false); // FIXME: describe ... how to 'get emningful resutls from scuh a comparison'
	  assert(false); // FIXME: describe ... 
	}
      }
    }



    //! -----------------------------------------
    assert(false); // FIXME[real-life-data-sets--example]: for a data-set consisnteitn of ....??... from KnittingTools we assess/infer/relate ....??..
    //! -----------------------------------------


    //!
    //! De-allocate:
    free__s_kt_matrix(&obj_clusterAssignments);
    //!
    //! Increment the counter-variable:
    cnt_Tests_evalauted++;
    }


  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
