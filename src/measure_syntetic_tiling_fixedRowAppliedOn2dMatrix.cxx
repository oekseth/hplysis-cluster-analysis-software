#include "measure_syntetic_tiling_fixedRowAppliedOn2dMatrix.h"
#include "graphAlgorithms_som.h"
#include "log_clusterC.h"

// //! Note: this funciton is used to 'only' iterate throug half of the items in a [][] matrix.
// static void __get_updatedNumberOF_lastSizeChunk_entry(const uint nrows, const uint SM, uint *numberOf_chunks, uint *chunkSize_row_last) {
//   assert(nrows >= SM);
//   *numberOf_chunks = nrows / SM;
//   *chunkSize_row_last = nrows - (SM* *numberOf_chunks);
//   assert( (*chunkSize_row_last + SM* *numberOf_chunks) == nrows );
//   if(*chunkSize_row_last == 0) {*chunkSize_row_last = SM;}
//   assert(*chunkSize_row_last <= SM);
// }

//! A non-otpmized trategy to compute difference between a given row and its correcponande in muliple matrices.
static void __test_3dLoop_nonTiled(const uint nxgrid, const uint nygrid, const uint nrows, const uint ncols, t_float *row, t_float ***celldata) {
  for(uint ix = 0; ix < nxgrid; ix++) {
    for(uint iy = 0; iy < nygrid; iy++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	celldata[ix][iy][col_id] = (row[col_id] - celldata[ix][iy][col_id]);
      }
    }
  }
}

//! A non-otpmized trategy to compute difference between a given row and its correcponande in muliple matrices.
static void __test_3dLoop_nonTiled_condensed2dLoop(const uint nxgrid, const uint nygrid, const uint nrows, const uint ncols, t_float *row, t_float ***celldata) {
  const uint celldata_cnt_rows = nxgrid * nygrid;
  for(uint global_row_id = 0; global_row_id < celldata_cnt_rows; global_row_id++) {
  // for(uint ix = 0; ix < nxgrid; ix++) {
  //   for(uint iy = 0; iy < nygrid; iy++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      celldata[0][global_row_id][col_id] = (row[col_id] - celldata[0][global_row_id][col_id]);
      //celldata[ix][iy][col_id] = (row[col_id] - celldata[ix][iy][col_id]);
    }
      //}
  }
}


//! A memory-tiled 'verison' of "__test_3dLoop_nonTiled(..)"
static void __test_3dLoop_deltaOf_row(const uint nxgrid, const uint nygrid, const uint nrows, const uint ncols, t_float *row, t_float ***celldata, const uint CLS, const bool isTo_useSSE = false) { //, const uint cntBlocks_grid = 2) {
  assert( (ncols % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, to simplify our loops wrt. SSE intrisntictics  
  // assert( (nxgrid % cntBlocks_grid) == 0);
  // assert( (nygrid % cntBlocks_grid) == 0);
  assert( (ncols % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, to simplify our loops wrt. SSE intrisntictics
  // assert(nxgrid < 300); //! ie, to simplify our code-writing.
  // assert(nygrid < 300); //! ie, to simplify our code-writing.
  assert(CLS > 0); 
  //printf("CLS=%u, at %s:%d\n", CLS, __FILE__, __LINE__);



  //! ----------------------------------------------
  // uint numberOf_chunks_grid_x = 0, chunkSize_grid_x_last = 0;
  // __get_updatedNumberOF_lastSizeChunk_entry(nrows, cntBlocks_grid, &numberOf_chunks_grid_x, &chunkSize_grid_x_last);
  // uint numberOf_chunks_grid_y = 0, chunkSize_grid_y_last = 0;
  // __get_updatedNumberOF_lastSizeChunk_entry(nrows, cntBlocks_grid, &numberOf_chunks_grid_y, &chunkSize_grid_y_last);
  //! ----------------------------------------------
  const uint SM = (uint)(CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  assert(nrows > 0); assert(ncols > 0); assert(SM > 0);
  assert( (ncols % SM) == 0); //! ie, what we expect [”elow]
  const uint numberOf_chunks_column = ncols / SM; const uint chunkSize_colum_last = SM;
  //! ----------------------------------------------

  const uint matrix_size = nrows * ncols;


  
  // for(uint cnt_index_x = 0, index_x = 0; cnt_index_x < numberOf_chunks_grid_x; cnt_index_x++, index_x += cntBlocks_grid) {
  //   const uint chunkSize_index_x = (cnt_index_x < numberOf_chunks_grid_x) ? cntBlocks_grid : chunkSize_grid_x_last;
  //   //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
  //   //! Note(2): we assume the matrix is assumed not to be symmetric:
  //   // const uint numberOf_chunks_grid_x_local = numberOf_chunks_grid_x; const uint chunkSize_grid_x_last_local = chunkSize_grid_x_last;
  //   for(uint cnt_index_y = 0, index_y = 0; cnt_index_y < numberOf_chunks_grid_y; cnt_index_y++, index_y += cntBlocks_grid) {
  //     const uint chunkSize_index_y = (cnt_index_y < numberOf_chunks_grid_y) ? cntBlocks_grid : chunkSize_grid_y_last;
  //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
  //! Note(2): we assume the matrix is assumed not to be symmetric:
  // const uint numberOf_chunks_grid_y_local = numberOf_chunks_grid_y; const uint chunkSize_grid_y_last_local = chunkSize_grid_y_last;      

  // for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
  //   const uint chunkSize_cpø = (cnt_i < numberOf_chunks_cols) ? SM : chunkSize_col_last;
  //! --------------------------------------------------------------------------------------


  t_float *__restrict__ rres;  const t_float *__restrict__ rmul1; 
  //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
  if(isTo_useSSE == false) {
    //uint debug_cntIterated = 0;
    for(uint cnt_index_column = 0, index_row = 0; cnt_index_column < numberOf_chunks_column; cnt_index_column++, index_row += SM) {
      rmul1  = &row[index_row]; //! ie, the 'upper square' of the matrix we update.		   		   
      for(uint ix = 0; ix < nxgrid; ix++) {
	for(uint iy = 0; iy < nygrid; iy++) {
	  //uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i;
	  uint i2 = 0;
	  for (i2 = 0, rres = &celldata[ix][iy][index_row]
		 ;
	       i2 < SM;
	       i2++ 
		 //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		 //rres += ncols,  rmul1 += ncols
		 //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
	       ) {
	    rres[i2] = ( rmul1[i2] - rres[i2] );
	    //debug_cntIterated++;
	  }
	}
      }
    }
    // printf("cnt=%.3E, at %s:%d\n", (float)debug_cntIterated, __FILE__, __LINE__);
  } else {
    // uint debug_cntIterated = 0;
    for(uint cnt_index_column = 0, index_row = 0; cnt_index_column < numberOf_chunks_column; cnt_index_column++, index_row += SM) {
      rmul1  = &row[index_row]; //! ie, the 'upper square' of the matrix we update.		   		   
      for(uint ix = 0; ix < nxgrid; ix++) {
	for(uint iy = 0; iy < nygrid; iy++) {
	  //uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i;
	  uint i2 = 0;
	  for (i2 = 0, rres = &celldata[ix][iy][index_row]
		 ;
	       i2 < SM;
	       i2 += VECTOR_FLOAT_ITER_SIZE
		 //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		 //rres += ncols,  rmul1 += ncols
		 //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
	       ) {
	    const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOAD(&rres[i2]);
	    const VECTOR_FLOAT_TYPE vec_inp = VECTOR_FLOAT_LOAD(&rmul1[i2]);
	    VECTOR_FLOAT_STORE(&rres[i2], VECTOR_FLOAT_SUB(vec_inp, vec_old));
	    // debug_cntIterated += VECTOR_FLOAT_ITER_SIZE;
	    //rres[i2] = ( rmul1[i2] - rres[i2] );
	  }
	}
      }
    }
    // printf("cnt=%.3E, at %s:%d\n", (float)debug_cntIterated, __FILE__, __LINE__);
  }
 

}
//! A memory-tiled 'verison' of "__test_3dLoop_nonTiled(..)"
static void __test_3dLoop_deltaOf_row_condensed2dLoop(const uint nxgrid, const uint nygrid, const uint nrows, const uint ncols, t_float *row, t_float ***celldata, const uint CLS, const bool isTo_useSSE = false) { //, const uint cntBlocks_grid = 2) {
  // assert( (ncols % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, to simplify our loops wrt. SSE intrisntictics  
  // // assert( (nxgrid % cntBlocks_grid) == 0);
  // // assert( (nygrid % cntBlocks_grid) == 0);
  // assert( (ncols % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, to simplify our loops wrt. SSE intrisntictics
  // // assert(nxgrid < 300); //! ie, to simplify our code-writing.
  // // assert(nygrid < 300); //! ie, to simplify our code-writing.
  // assert(CLS > 0); 
  //printf("CLS=%u, at %s:%d\n", CLS, __FILE__, __LINE__);



  //! ----------------------------------------------
  // uint numberOf_chunks_grid_x = 0, chunkSize_grid_x_last = 0;
  // __get_updatedNumberOF_lastSizeChunk_entry(nrows, cntBlocks_grid, &numberOf_chunks_grid_x, &chunkSize_grid_x_last);
  // uint numberOf_chunks_grid_y = 0, chunkSize_grid_y_last = 0;
  // __get_updatedNumberOF_lastSizeChunk_entry(nrows, cntBlocks_grid, &numberOf_chunks_grid_y, &chunkSize_grid_y_last);
  //! ----------------------------------------------
  const uint SM = (uint)(CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  // assert(nrows > 0); assert(ncols > 0); assert(SM > 0);
  // assert( (ncols % SM) == 0); //! ie, what we expect [”elow]
  const uint numberOf_chunks_column = ncols / SM; const uint chunkSize_colum_last = SM;
  //! ----------------------------------------------

  const uint matrix_size = nrows * ncols;

  const uint celldata_cnt_rows = nxgrid * nygrid;

  
  // for(uint cnt_index_x = 0, index_x = 0; cnt_index_x < numberOf_chunks_grid_x; cnt_index_x++, index_x += cntBlocks_grid) {
  //   const uint chunkSize_index_x = (cnt_index_x < numberOf_chunks_grid_x) ? cntBlocks_grid : chunkSize_grid_x_last;
  //   //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
  //   //! Note(2): we assume the matrix is assumed not to be symmetric:
  //   // const uint numberOf_chunks_grid_x_local = numberOf_chunks_grid_x; const uint chunkSize_grid_x_last_local = chunkSize_grid_x_last;
  //   for(uint cnt_index_y = 0, index_y = 0; cnt_index_y < numberOf_chunks_grid_y; cnt_index_y++, index_y += cntBlocks_grid) {
  //     const uint chunkSize_index_y = (cnt_index_y < numberOf_chunks_grid_y) ? cntBlocks_grid : chunkSize_grid_y_last;
  //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
  //! Note(2): we assume the matrix is assumed not to be symmetric:
  // const uint numberOf_chunks_grid_y_local = numberOf_chunks_grid_y; const uint chunkSize_grid_y_last_local = chunkSize_grid_y_last;      

  // for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
  //   const uint chunkSize_cpø = (cnt_i < numberOf_chunks_cols) ? SM : chunkSize_col_last;
  //! --------------------------------------------------------------------------------------


  t_float *__restrict__ rres;  const t_float *__restrict__ rmul1; 
  //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
  if(isTo_useSSE == false) {
    //uint debug_cntIterated = 0;
    for(uint cnt_index_column = 0, index_row = 0; cnt_index_column < numberOf_chunks_column; cnt_index_column++, index_row += SM) {
      rmul1  = &row[index_row]; //! ie, the 'upper square' of the matrix we update.		   		   
      for(uint global_row_id = 0; global_row_id < celldata_cnt_rows; global_row_id++) {
      // for(uint ix = 0; ix < nxgrid; ix++) {
      // 	for(uint iy = 0; iy < nygrid; iy++) 
	{
	  //uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i;
	  uint i2 = 0;
	  for (i2 = 0, rres = &celldata[0][global_row_id][index_row]
	  //	  for (i2 = 0, rres = &celldata[ix][iy][index_row]
		 ;
	       i2 < SM;
	       i2++ 
		 //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		 //rres += ncols,  rmul1 += ncols
		 //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
	       ) {
	    rres[i2] = ( rmul1[i2] - rres[i2] );
	    //debug_cntIterated++;
	  }
	}
      }
    }
    // printf("cnt=%.3E, at %s:%d\n", (float)debug_cntIterated, __FILE__, __LINE__);
  } else {
    // uint debug_cntIterated = 0;
    for(uint cnt_index_column = 0, index_row = 0; cnt_index_column < numberOf_chunks_column; cnt_index_column++, index_row += SM) {
      rmul1  = &row[index_row]; //! ie, the 'upper square' of the matrix we update.		   		   
      for(uint global_row_id = 0; global_row_id < celldata_cnt_rows; global_row_id++) {
      // for(uint ix = 0; ix < nxgrid; ix++) {
      // 	for(uint iy = 0; iy < nygrid; iy++) 
	{
	  //uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i;
	  uint i2 = 0;
	  for (i2 = 0, rres = &celldata[0][global_row_id][index_row]
	  //for (i2 = 0, rres = &celldata[ix][iy][index_row]
		 ;
	       i2 < SM;
	       i2 += VECTOR_FLOAT_ITER_SIZE
		 //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		 //rres += ncols,  rmul1 += ncols
		 //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
	       ) {
	    const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOAD(&rres[i2]);
	    const VECTOR_FLOAT_TYPE vec_inp = VECTOR_FLOAT_LOAD(&rmul1[i2]);
	    VECTOR_FLOAT_STORE(&rres[i2], VECTOR_FLOAT_SUB(vec_inp, vec_old));
	    // debug_cntIterated += VECTOR_FLOAT_ITER_SIZE;
	    //rres[i2] = ( rmul1[i2] - rres[i2] );
	  }
	}
      }
    }
    // printf("cnt=%.3E, at %s:%d\n", (float)debug_cntIterated, __FILE__, __LINE__);
  }



}



//! The main funciton for logic-texting
void measure_syntetic_tiling_fixedRowAppliedOn2dMatrix_main(const int array_cnt, char **array) {
  s_log_clusterC_t logFor_synteticTest;
  s_log_clusterC_init(&logFor_synteticTest); //! ie, intiate the object.


  if(false)
  {
    const uint size_inner = 100;
    t_float ***tmp = NULL; tmp = alloc_generic_type_3d(t_float, tmp, size_inner);
    const uint size_x = 350, size_y = 350;
    //const uint matrix_size = size_x * size_y;
    const uint inner_and_x_size = size_inner * size_x;
    //const uint inner_and_x_size = size_inner * size_x;
    const t_float default_floatValue = 2;
    tmp[0] = allocate_2d_list_float(inner_and_x_size, size_y, default_floatValue);  //! ie, to allocate for all of the row-pointers in "tmp".    
    const t_float *start_pos = tmp[0][0];
    const uint size_outer = size_x * size_y; uint current = size_outer;
    for(uint matrix_id = 1; matrix_id < size_inner; matrix_id++) {
      tmp[matrix_id] = tmp[0] + size_x; current += size_x;
      //tmp[matrix_id] = tmp[0] + size_outer; current += size_outer;
      //tmp[matrix_id][0] = tmp[matrix_id-1][0] + matrix_size;
      // for(uint ix = 1; ix < size_x; ix++) {
  	// tmp[matrix_id][ix] = tmp[matrix_id][ix-1] + size_y;
  	// for(uint col_id = 0; col_id < size_y; col_id++) {
  	//   const t_float score = tmp[matrix_id][ix][col_id];
  	//   if(score != default_floatValue) {
  	//     printf("[%u][%u][%u]=%f, at %s:%d\n", matrix_id, ix, col_id, score, __FILE__, __LINE__);
  	//   }
  	//   assert(tmp[matrix_id][ix][col_id] == default_floatValue);
  	//   tmp[matrix_id][ix][col_id] = col_id;
  	// }
      // }
    }
    { const uint matrix_id = 0;
      assert(tmp[0][0] == start_pos);
      for(uint ix = 1; ix < size_x; ix++) {
  	tmp[matrix_id][ix] = tmp[matrix_id][ix-1] + size_y; 
  	for(uint col_id = 0; col_id < size_y; col_id++) {
  	  tmp[matrix_id][ix][col_id] = col_id;
  	}
      }
    }
    for(uint matrix_id = 0; matrix_id < size_inner; matrix_id++) 
    //const uint matrix_id = 0;
    {
      for(uint iy = 0; iy < size_x; iy++) {
  	for(uint col_id = 0; col_id < size_y; col_id++) {
  	  tmp[matrix_id][iy][col_id] = col_id;
  	}
      }
    }
    //! Test correctness of thsi, ie, that we do nto get any memory-corrptions:
    for(uint ix = 1; ix < size_inner; ix++) {
      for(uint iy = 0; iy < size_x; iy++) {
  	for(uint col_id = 0; col_id < size_y; col_id++) {
  	  const t_float expected_value = col_id;
  	  const t_float score = tmp[ix][iy][col_id];
  	  if(score != expected_value) {
  	    printf("[%u][%u][%u]=%f, at %s:%d\n", ix, iy, col_id, score, __FILE__, __LINE__);
  	  }
  	  assert(tmp[ix][iy][col_id] == expected_value);
  	}
      }
    }
  }


  const uint grid_x = 16; const uint grid_y = 16; 
  const uint ncols = 16*1000*14; const uint nrows = ncols*5;
  const t_float default_floatValue = 1;
  t_float *row = allocate_1d_list_float(ncols, default_floatValue);
  t_float ***celldata = allocate_3d_list_float(grid_x, grid_y, ncols, default_floatValue);
  for(uint ix = 0; ix < grid_x; ix++) {
    for(uint iy = 0; iy < grid_y; iy++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	celldata[ix][iy][col_id] = ix*col_id;
      }
    }
  }
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_1); //! ie, intiate the object.
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_1, "non-tiled");
  __test_3dLoop_nonTiled(grid_x, grid_y, nrows, ncols, row, celldata);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_1); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_2); //! ie, intiate the object.
  uint CLS = 32;
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_2, "CLS=32 w/tiling=true");
  __test_3dLoop_deltaOf_row(grid_x, grid_y, nrows, ncols, row, celldata, CLS);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_2); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_3); //! ie, intiate the object.
  CLS = 64;
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_3, "CLS=64 w/tiling=true");
  __test_3dLoop_deltaOf_row(grid_x, grid_y, nrows, ncols, row, celldata, CLS);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_3); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_4); //! ie, intiate the object.
  CLS = 128;
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_4, "CLS=128 w/tiling=true");
  __test_3dLoop_deltaOf_row(grid_x, grid_y, nrows, ncols, row, celldata, CLS);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_4); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_5); //! ie, intiate the object.
  CLS = 64;
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_5, "CLS=64 w/tiling=true and SSE-intri=true");
  __test_3dLoop_deltaOf_row(grid_x, grid_y, nrows, ncols, row, celldata, CLS, /*isTo_useSSE=*/true);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_5); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_6); //! ie, intiate the object.
  CLS = 128;
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_6, "CLS=128 w/tiling=true and SSE-intri=true");
  __test_3dLoop_deltaOf_row(grid_x, grid_y, nrows, ncols, row, celldata, CLS, /*isTo_useSSE=*/true);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_6); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_7); //! ie, intiate the object.
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_7, "non-tiled-2Loops-insteadOf-3Loops");
  __test_3dLoop_nonTiled_condensed2dLoop(grid_x, grid_y, nrows, ncols, row, celldata);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_7); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_8); //! ie, intiate the object.
  CLS = 64;
  // FIXME[JC]: seems like the impelmetnion/algorithm erpfroms slwoer if we use 'one condenced loop' instead of two loops: any reasons for this (whn compiled in non-opmtized mode)? <-- had expected the oppostive, ie, would be itneresting to known 'if the resuls is due to otehr 'elemtns' than noise in the time-emasuremnets': the time-differnece seems to be cosnistnet (betwene diffenre tmeasusremetns), ie, which explains 'the reaosn for thios queyr/quesiton').
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_8, "CLS=64 2Loops-insteadOf-3Loops");
  __test_3dLoop_deltaOf_row_condensed2dLoop(grid_x, grid_y, nrows, ncols, row, celldata, CLS, /*isTo_useSSE=*/false);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_8); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------
  start_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_9); //! ie, intiate the object.
  s_log_clusterC_setDescription(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_9, "CLS=64 2Loops-insteadOf-3Loops and SSE-intri=true");
  __test_3dLoop_deltaOf_row_condensed2dLoop(grid_x, grid_y, nrows, ncols, row, celldata, CLS, /*isTo_useSSE=*/true);
  end_time_measurement_clusterC(&logFor_synteticTest, e_log_clusterC_typeOf_caseEnumeration_9); //! ie, intiate the object.
  //! ---------------------------------------------------------------------------


  writeOut_object_clusterC(&logFor_synteticTest, stdout, e_logCluster_typeOf_writeOut_cmpCases_12x, "tilingOf-fixed-columns"); //! ie, intiate the object.
}

