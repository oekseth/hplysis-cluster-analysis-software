#include "hpLysis_api.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "fast_log.h"
#include "kt_clusterAlg_hca.h" //! which is used for "cuttree__kt_clusterAlg_hca(..)" (oekseth, 06.f eb. 2017)
#include "alg_dbScan_brute.h" //! olek: DBSCAN impelmetantions ()oekseth, 01.01.2019).
/* void test_func_2(test_enum_t test) { */
/*   printf("test=%d\n", test); */
/* } */

//! Global variables:
//int hpLysis_api_version = 1; //!< which is used to simplify the detection of future sigicant API-changes.  

//! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
void hpLysis__globalInit__kt_api() {
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 1) //! ie, as we then expec thte 'data-strucutre' to be loaded at 'run-time'.
  allocate_optmize_usePReComptued_floats();
#endif  
}


//! A 'glboal de-allocation-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
void hpLysis__globalFree__kt_api() {
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 1) //! ie, as we then expec thte 'data-strucutre' to be loaded at 'run-time'.
  free_optmize_usePReComptued_floats();
#endif  
}

//! @return an itnalized verison of our "s_hpLysis_api__config_t" object (oekseth, 06. feb. 2017)
s_hpLysis_api__config_t init__s_hpLysis_api__config_t(const char *stringOf_exportResult__heatMapOf_clusters, s_kt_longFilesHandler *fileHandler) {
  s_hpLysis_api__config_t self;
  //! ------------------
  self.corrMetric_prior = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3); 
  self.corrMetric_prior_use = true;
  self.corrMetric_insideClustering = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1);
  self.corrMetric_insideClustering__mergeConf = init__s_hp_distanceCluster_t();
  self.clusterConfig_mcl = setToEmpty__s_kt_clusterAlg_mcl_t();
  //!
  //! By default: asusme the matrix is (a) Not transposed and (b) contain some 'empty maksed vlaeus'' (ie, where using disjotinf-reost-speeraiton may boost eec-time without eprfomrance-loss): 
  self.clusterConfig = init__s_kt_api_config_t(/*isTo_transposeMatrix=*/false, /*isTo_applyDisjointSeperation=*/true, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, fileHandler);
  self.clusterConfig.stringOf_exportResult__heatMapOf_clusters = stringOf_exportResult__heatMapOf_clusters;

  //!
  //! Configurations when "nclsuters" is not specified though 'kmeans' is requested', ie, where we 'are then intersted in inferrign the k-count of clsuters based on a valeu-trehshold'.
  //self.opt_kCount__disjointThresholdFilter__scoreRelative_min = T_FLOAT_MAX; //! ie, do Not sue thresholds by default
  /* self.opt_kCount__disjointThresholdFilter__scoreRelative_min = 0.7; //! ie, the 'upper 30 per-cent' */
  /* self.opt_kCount__disjointThresholdFilter__scoreRelative_max = T_FLOAT_MAX; //! 'infitie', ie, Not used. */
  self.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__min = 0.1; //! ie, the max-count in a clsuter which we 'evaluate'
  self.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__max = UINT_MAX; //! ie, the max-count in a clsuter which we 'evaluate'

  //! By default: do Not use thresholding:
  self.config__valueMask = init__s_hpLysis_api__config__valueMask_t();
  self.config__valueMask__afterSimMatrix = init__s_hpLysis_api__config__valueMask_t();
  //!
  //! Algorithm-cofngiruation: "Self Orgnaising Maps" (SOM)  
  self.algConfig_SOM__nxgrid = UINT_MAX; //! ie, use 'spec' in the 'calling fucntion'.
  self.algConfig_SOM__nygrid = UINT_MAX;//! ie, use 'spec' in the 'calling fucntion'.
  // TODO[docu[ ivnestgiate the imporatnce of [”elow] ... and udpate our code+algorithm.-evlauation.
  self.algConfig_SOM__initTau = 0.3;
  //! ----
  //!
  self.config__CCM = initAndReturn__s_kt_clusterAlg_config__ccm_t(NULL); //! which is used for metircs which 'makes use of' CCM-based and dynamic k-means covnergence-criteira, eg, wrt our "kruskal" MST implementaiton (oekseth, 06. fe.b 2017)
  self.config__CCM__dynamicKMeans = initAndReturn__s_kt_clusterAlg_config__ccm_t(NULL); 
  self.config__CCM__dynamicKMeans.k_clusterCount__max = 30; //100; //! ie, as we have r'early seen' any clsuter-esults (in researhc) with a 'meaningful' intreptation of mroe than 100 unique clusters, ie, (hopefuuly) a 'defualt' over-estimate. 
  self.config__CCM__dynamicKMeans.isTo_pply__CCM = true; //! ie, as we by 'defualt' assume that the performance-penalty of CCM-clcuaions is isngicitncat 'when compared to the clsutering-exueciotn-timne' itself (and if latter is Not the case then please give oekseth an ehads-up, ie, to improive the cCM-implementaiotns (oekseth, 06. mar. 2017)
  self.config__CCM__dynamicKMeans__useHCAPre_step_forKMeans_ncluster = true; //! ie, to 'bound' the exeuciotin-time for k-means with 'undefined' n-cluster-count.
  //! ------------------------
  //! 
  //! Configurations of random-cluster-computaitons (oesketh, 06. jul. 2017):
  self.config__randomClusterIndetificaiton__cntRandomIterations = 100; //*1000; //! which is the number of random-iteraiotns to be used in the clsutering (oekseth, 06. jul. 2017).
  //! ---------------------------------
  //!
  self.kdConfig = init__s_kd_searchConfig_conf(); //! ie, itniate the cofnigruation-object, ie, define din our "kd_tree.h"
  //! ---------------------------------
  //!
  //! @return
  return self;
}

/* static bool applyCCMFiltering__kMeans(s_hpLysis_api_t *self) { */
/*   assert(self); */
/*   s_kt_clusterAlg_config__ccm config__CCM = self->config.config__CCM__dynamicKMeans; */
/*   if(config__CCM.isTo_pply__CCM == false) {return true;} */

/*   assert(false); // FIXME: complete */
/*   //! At this exec-piint we asusme the oepraiton was a success. */
/*   return true; */
/* } */

//! A wrapper to apply HCA-based ydnaimic k-means-logics (oekseth, 06. mar. 2017).
static bool applyCCMFiltering__hca(s_hpLysis_api_t *self) {
  //!
  //! HCA-specifc 'constrait';
  if(self->obj_result_HCA.nrows == 0) {
#ifndef NDEBUG
    fprintf(stderr, "!!\t An inconsistnecy in your funciton-call: the HCA-object did Not hold any data, ie, please udpate by reading the docuemtaiton and tut-exampesl. However, if latter does Not help, then contact senior devleoper [oesketh@gmail.com]. Observiaont at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, a compilatior-optmaizaiton
    return false;
#else
    return true; //! as we then assuem the HCA clsutering was Not able to infer any itneresting reuslts, ie, a pefepctly nroaml case (oekseth, 906. okt. 2017).
#endif
  }
  { //! Valditet ehtat the HCA-data-sets are comptued:
    const uint nrows = self->obj_result_HCA.nrows;
    assert(nrows > 0);
  }
  //!
  //! Define our function:
#define __MiF__getKClusterObject() ({s_kt_clusterAlg_fixed_resultObject_t obj_tmp = cuttree__kt_clusterAlg_hca(&(self->obj_result_HCA), nclusters); assert(obj_tmp.cnt_vertex > 0); obj_tmp;})
  //!
  //! Incldue assicated lgocis:
#include "hpLysis_api__stub__dynamicC.c"
  //!
  //! Reset internal vrialbes:
#undef __MiF__getKClusterObject
  //! 
  //! At this exec-piint we asusme the oepraiton was a success.
  return true;
}

/* //! De-allcoates the s_hpLysis_api__config_t object (oekseth, 06. feb. 2017). */
/* void free__s_hpLysis_api__config_t(s_hpLysis_api__config_t *self) { */

/* } */

//! Intalize the s_hpLysis_api_t to default values:
//! @return an intli<ed object.
s_hpLysis_api_t setToEmpty__s_hpLysis_api_t(const char *stringOf_exportResult__heatMapOf_clusters, s_kt_longFilesHandler *fileHandler) {
  s_hpLysis_api_t self;
  self.config = init__s_hpLysis_api__config_t(stringOf_exportResult__heatMapOf_clusters, fileHandler);


  //!
  //! Export:
  self.stringOfResultPrefix__exportCorr__prior = NULL; //! which if set to a string' (which shoudl be both unique and at wa writeable lcoation) is 'used' to export the correlation-data-set gernated 'before we apply the clsutering'.

  //!
  //! Result-objects: the type of result-object dpeends uppn the "e_hpLysis_clusterAlg_t" aprameter which is used.
  setTo_empty__s_kt_matrix_t(&(self.matrixResult__inputToClustering));
  setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&(self.obj_result_kMean));
  setTo_empty__s_kt_matrix_t(&(self.obj_result_kMean_clusterId_columnValue));
  self.obj_result_SOM = setToEmptyAndreturn__s_kt_clusterAlg_SOM_resultObject_t();
  self.obj_result_HCA = setToEmptyAndreturn__s_kt_clusterAlg_hca_resultObject_t();  
  //! ---------------------------------------
  self.objectHistory__prev__alg_type = e_hpLysis_clusterAlg_undef;
  //self.objectHistory__prev__inputMatrix = NULL;
  //! ---------------------------------------
  self.randomConfig = initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t();
  //! ---------------------------------
  //!
  //! Set hte reuslt-varialbes wrt. oru dynamic-k-means-applciaiton:
  self.dynamicKMeans__best__nClusters = UINT_MAX;
  self.dynamicKMeans__best__score = T_FLOAT_MAX;
  //! ---------------------------------
  //!
  //! 
  //! @return
  return self;
}


//! De-allocates the "s_hpLysis_api_t" object.
void free__s_hpLysis_api_t(s_hpLysis_api_t *self) {
  //!
  //! Free the memory:
  free__s_kt_matrix(&(self->matrixResult__inputToClustering));
  //! ---
  free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean));
  free__s_kt_matrix(&(self->obj_result_kMean_clusterId_columnValue));
  //! ---
  free__s_kt_clusterAlg_SOM_resultObject_t(&(self->obj_result_SOM));
  //! ---
  free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA));
  //! ---
  free__s_kt_randomGenerator_vector_t(&(self->randomConfig));    
  //!
  //! Reset:
  *self = setToEmpty__s_hpLysis_api_t(NULL, NULL);
}

static void __applyPosteriori_filters__hpLysis_api(s_hpLysis_api_t *self, const s_kt_matrix_t **matrix_input__, s_kt_matrix_t *matrix_result, const bool isTo_transposeMatrix, const s_hpLysis_api__config__valueMask_t __config__valueMask) {
  //!
  //! What we expect:
  assert(matrix_input__);
  const s_kt_matrix_t *matrix_input = *matrix_input__;
  assert(self); assert(matrix_input); assert(matrix_result);
  assert(matrix_input->nrows > 0);
  assert(matrix_input->ncols > 0);
  assert(matrix_result->nrows == 0);
  assert(matrix_result->ncols == 0);
  
  //printf("at %s:%d\n", __FILE__, __LINE__);
  if(__config__valueMask.isTo_applyValueThresholds) { //! then we adjsut the data-set wrt. the ranks:
    //printf("(apply-filters)\tat %s:%d\n", __FILE__, __LINE__);
    //! 
    //! Construct a 'ranked version' of [ªbove]:
    if(__config__valueMask.isTo_adjustToRanks_beforeTrehsholds_forRows) { //! then we adjsut the data-set wrt. the ranks:
      //printf("(adjust-to-ranks)\tat %s:%d\n", __FILE__, __LINE__);
      const bool isOk_rankCpy = init__copy__useRanksEachRow__s_kt_matrix(matrix_result, matrix_input, /*isTo_updateNames=*/true);
      assert(isOk_rankCpy);     assert(matrix_input->nrows == matrix_result->nrows);
    } else {
      const bool isOk_rankCpy = init__copy__s_kt_matrix(matrix_result, matrix_input, /*isTo_updateNames=*/true);
      assert(isOk_rankCpy);     assert(matrix_input->nrows == matrix_result->nrows);
    }
    //!
    //! Apply the ranksthresholds:
    const t_float threshold_min = (__config__valueMask.threshold_min != T_FLOAT_MAX) ? __config__valueMask.threshold_min : T_FLOAT_MIN_ABS;
    const t_float threshold_max = (__config__valueMask.threshold_max != T_FLOAT_MAX) ? __config__valueMask.threshold_max : T_FLOAT_MAX;
    if( (threshold_min != T_FLOAT_MIN_ABS) || (threshold_max != T_FLOAT_MAX) ) {
      for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
	for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	  const t_float score = matrix_result->matrix[row_id][col_id];
	  if( (score <= threshold_min) || (score >= threshold_max) ) {
	    //printf("mask[%u][%u], at %s:%d\n", row_id, col_id, __FILE__, __LINE__);
	    //! Then 'apply' the mask:
	    matrix_result->matrix[row_id][col_id]  = T_FLOAT_MAX;
	  }
	}
      }
    }
    if(isTo_transposeMatrix == true) {
      s_kt_matrix_t tmp;       const bool is_ok = init__copy_transposed__s_kt_matrix(&tmp, matrix_result, /*isTo_updateNames=*/true);
      assert(is_ok);
      //!
      //! De-allocate
      free__s_kt_matrix(matrix_result);
      //! Then  and 'steal' the poitners:
      *matrix_result = tmp;
      self->config.clusterConfig.isTo_transposeMatrix = false;

    }
    //!
    //! Udpate the 'matrix to use':
    *matrix_input__ = matrix_result;    
  } else if(isTo_transposeMatrix == true) {
    s_kt_matrix_t tmp;       const bool is_ok = init__copy_transposed__s_kt_matrix(&tmp, matrix_input, /*isTo_updateNames=*/true);
    assert(is_ok);
    //!
    //! De-allocate
    //if(matrix_input != obj_1__) {
    if(matrix_input->matrix == matrix_result->matrix) {
      assert(matrix_input->matrix == matrix_result->matrix); //! ie, as we then assume 'they are the same object':
      free__s_kt_matrix(matrix_result);
      self->config.clusterConfig.isTo_transposeMatrix = false;
    }
    //! Then  and 'steal' the poitners:
    *matrix_result = tmp;
    //!
    //! Udpate the 'matrix to use':
    *matrix_input__ = matrix_result;
  }
}



//! Compute DB-SCAN using a dynamic CCM-thresholds-appraoch (oekseth, 06. jul. 2017). 
static void  __ccmDynamic_disjointForest_dbScan__hpLysis_api(s_hpLysis_api_t *self, const s_kt_matrix_t *matrix_input, s_hp_distanceCluster_t *objMetric__postMerge, const bool use_kdTreeApproach, const e_hpLysis_clusterAlg_t alg_type) {
  //! ---------------------------- 
  //!
  //! Inlitaze the vlaue using an 'CCM-specfic' worst-case-vlaue':
  //  s_kt_matrix_base_t mat_localSim = initAndReturn__empty__s_kt_matrix_base_t();
  const bool beforeCall__inputMatrix__isAnAdjecencyMatrix = self->config.clusterConfig.inputMatrix__isAnAdjecencyMatrix;
  s_kt_matrix_base_t mat_localSim = initAndReturn__empty__s_kt_matrix_base_t();
  s_kt_clusterAlg_config__ccm config__CCM = self->config.config__CCM__dynamicKMeans;
  s_kt_matrix_base_t matrixResult__inputToClustering_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(matrix_input);
  if(beforeCall__inputMatrix__isAnAdjecencyMatrix == true) {
    assert(matrixResult__inputToClustering_shallow.nrows == matrixResult__inputToClustering_shallow.ncols); // TOOD: vlaidate this asusmption (oekseth, 06. okt. 2017).
  }
  if(!self->config.corrMetric_prior_use  && (beforeCall__inputMatrix__isAnAdjecencyMatrix == false) ) { //! then we comptue a lcoal verison of simlairty-matrix, ie, to be used wrt. the CCM-covnergence-emausremt-comtpatuions.
    assert(self->matrixResult__inputToClustering.nrows == 0); //! ie, as we then expect latter is Not allcoated.
    //! Then we need an ajducentcy-matrix, ie, for CCM-comptatuions wrt. cluster-convergence: 
    //! Compute:
    s_kt_matrix_base_t matrix_input_local = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(matrix_input);
    const bool is_ok = apply__hp_distance(self->config.corrMetric_prior, &matrix_input_local, NULL, &mat_localSim, init__s_hp_distance__config_t());
    assert(is_ok);
    // FIXME[ocneptaul]: try repalcing [”elow] with a dk-tree (or: sparse-aprpaoch) .... ie, after we ahve first udpated our "kt_matrix_cmpCluster.c" (oekseth, 06. jul. 2017).
    self->config.config__CCM__dynamicKMeans.matrix_unFiltered = &mat_localSim; //.matrix;
    config__CCM.matrix_unFiltered = &mat_localSim;
    /* self->config.config__CCM__dynamicKMeans.matrix_unFiltered.nrows = mat_localSim.nrows; */
    /* self->config.config__CCM__dynamicKMeans.matrix_unFiltered.ncol = mat_localSim.ncols; */
    // ----
    self->matrixResult__inputToClustering.matrix = mat_localSim.matrix;
    self->matrixResult__inputToClustering.nrows = mat_localSim.nrows;
    self->matrixResult__inputToClustering.ncols = mat_localSim.ncols;
    /* self->config.config__CCM__dynamicKMeans.matrix_unFiltered.nrows = mat_localSim.nrows; */
    /* self->config.config__CCM__dynamicKMeans.matrix_unFiltered.ncol = mat_localSim.ncols; */
    
    assert(config__CCM.matrix_unFiltered->nrows == config__CCM.matrix_unFiltered->ncols); //! ie, as we expect an ajdcneyc-amtrix as input.
  } else {
    self->config.config__CCM__dynamicKMeans.matrix_unFiltered = &matrixResult__inputToClustering_shallow;
    config__CCM.matrix_unFiltered = &matrixResult__inputToClustering_shallow;
    // printf("corrMetric_prior_use=%d, beforeCall__inputMatrix__isAnAdjecencyMatrix=%u, at  %s:%d\n", self->config.corrMetric_prior_use, beforeCall__inputMatrix__isAnAdjecencyMatrix, __FILE__, __LINE__);
    assert(config__CCM.matrix_unFiltered->nrows == config__CCM.matrix_unFiltered->ncols); //! ie, as we expect an ajdcneyc-amtrix as input.
  }
  t_float currentResult = get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(&config__CCM);
  //!
  //! Generla cofnigurations:
  const bool __isTo_selectBest = true; const bool __isTo_useAverage = false;

  // s_kt_matrix_t obj_cpy_ranksEachRow_transp; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);
  
  if(use_kdTreeApproach == false) { //! then a new/other modivioant ot the CCM-iterative-aprpaoch (oekseth, 60. jul. 2017).
    s_kt_matrix_t obj_cpy_ranksEachRow; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);
    t_float max_rank = T_FLOAT_MIN_ABS;     t_float min_rank = T_FLOAT_MAX; // t_float relativeWeight_eachPercent = 0;
    //! 
    //! Construct a 'ranked version' of [ªbove]:
    bool isOk_rankCpy = init__copy__useRanksEachRow__s_kt_matrix(&obj_cpy_ranksEachRow, matrix_input, /*isTo_updateNames=*/false);
    assert(isOk_rankCpy);     assert(matrix_input->nrows == obj_cpy_ranksEachRow.nrows);
    //! Adjust the data to be iin range [0, 100].
    for(uint row_id = 0; row_id < obj_cpy_ranksEachRow.nrows; row_id++) {
      uint cnt_interesting = 0;
      for(uint col_id = 0; col_id < obj_cpy_ranksEachRow.ncols; col_id++) {      
	if(isOf_interest(obj_cpy_ranksEachRow.matrix[row_id][col_id])) {
	  //  printf("%f\t", obj_cpy_ranksEachRow.matrix[row_id][col_id]);
	  max_rank = macro_max(max_rank, obj_cpy_ranksEachRow.matrix[row_id][col_id]);
	  min_rank = macro_min(min_rank, obj_cpy_ranksEachRow.matrix[row_id][col_id]);
	}
      }
      //printf("\n");
    }
    assert(max_rank != T_FLOAT_MIN_ABS); assert(max_rank != T_FLOAT_MAX);
    assert(min_rank != T_FLOAT_MAX);     assert(min_rank != T_FLOAT_MIN_ABS);    
    s_kt_matrix_t mat_tmp = initAndReturn__s_kt_matrix(obj_cpy_ranksEachRow.nrows, obj_cpy_ranksEachRow.ncols);
    const t_float diff_relative = (max_rank - min_rank)*0.01;
    const uint thres_col_start = 5;
    //! 
    //!  Rank the rows: 
    const t_float empty_0 = 0;    t_float *arrOf_counts = allocate_1d_list_float(obj_cpy_ranksEachRow.nrows, empty_0); assert(arrOf_counts);
    loint iter_index = 0;
    loint cnt_interesting_total_prev = (loint)UINT_MAX;
    for(uint thres_col = thres_col_start; thres_col < 101; thres_col++) {
      if( (thres_col_start != thres_col) && (diff_relative == 0) ) {continue;} //! ie, as there is then only one case to evaluate.
      const t_float max_val = min_rank + diff_relative*thres_col;
      //! 
      //!  Construct a copy:
      loint cnt_interesting_total = 0;
      for(uint row_id = 0; row_id < obj_cpy_ranksEachRow.nrows; row_id++) {
	uint cnt_interesting = 0;
	for(uint col_id = 0; col_id < obj_cpy_ranksEachRow.ncols; col_id++) {            
	  const t_float score = obj_cpy_ranksEachRow.matrix[row_id][col_id];
	  if(isOf_interest(score)) {
	    if( (score <= max_val) || (thres_col == 100) ) { //! ie, where we 'frot eh alst option' include alls cells (whiel use row-threshodls on the rows).
	      mat_tmp.matrix[row_id][col_id] = score; //! ie, copy.
	      cnt_interesting++;
	    } else {
	      mat_tmp.matrix[row_id][col_id] = T_FLOAT_MAX;
	    }
	  }
	}
	//! Update; 
	arrOf_counts[row_id] = cnt_interesting;
	cnt_interesting_total += cnt_interesting;
      }
      if(cnt_interesting_total == cnt_interesting_total_prev) {continue;} //! ie, to aovid reapeaded calls.
      //printf("#\tthresh col(max)=%f/%f, at %s:%d\n", max_val, max_rank, __FILE__, __LINE__);
      cnt_interesting_total_prev = cnt_interesting_total;
      if(cnt_interesting_total > 0) {
	//! 
	//!  Rank the rows: 
	t_float *arrOf_ranks = get_rank__correlation_rank(obj_cpy_ranksEachRow.nrows, arrOf_counts);
	t_float arrOf_ranks_min = T_FLOAT_MAX; t_float arrOf_ranks_max = T_FLOAT_MIN_ABS;
	for(uint row_id = 0; row_id < obj_cpy_ranksEachRow.nrows; row_id++) {	
	  const t_float score = arrOf_ranks[row_id];
	  if(isOf_interest(score)) {	  
	    arrOf_ranks_min = macro_min(arrOf_ranks_min, score);
	    arrOf_ranks_max = macro_max(arrOf_ranks_max, score);
	  }
	}
	const t_float rows__diff_relative = (arrOf_ranks_max - arrOf_ranks_min)*0.01;
	//! 
	//! Construct a new matrix and idneityf the ranks of each row:
	const uint thres_row_end = 100;
	uint cnt_rowsOf_interest_prev = UINT_MAX; //! which is sued to aovid unneccsary calls wrt. overalppign calls.
	for(uint thres_row = 5; thres_row < thres_row_end; thres_row++) { //! then 	  
	  //!
	  //! Apply the rank-trehsholds:
	  const t_float rows__max_val = arrOf_ranks_min + (rows__diff_relative*thres_col);
	  s_kt_list_1d_uint_t map_interestingRows = init__s_kt_list_1d_uint_t(obj_cpy_ranksEachRow.nrows);
	  loint cnt_rowsOf_interest = 0;
	  if(thres_row != thres_row_end) {
	    for(uint row_id = 0; row_id < obj_cpy_ranksEachRow.nrows; row_id++) {	
	      const t_float score = arrOf_ranks[row_id];
	      map_interestingRows.list[row_id] = false;
	      if(isOf_interest(score)) {	  
		if(score <= rows__max_val) {
		  map_interestingRows.list[row_id] = true;
		  cnt_rowsOf_interest++;
		}
	      }
	    }
	  } else { //! then we evlauate for all rows.
	    for(uint row_id = 0; row_id < obj_cpy_ranksEachRow.nrows; row_id++) {	
	      map_interestingRows.list[row_id] = false;
	    }
	  }
	  if(cnt_rowsOf_interest == cnt_rowsOf_interest_prev) {free__s_kt_list_1d_uint_t(&map_interestingRows); continue;} //! ie, no point in repeaing calls if alreayd evaluated.
	  cnt_rowsOf_interest_prev = cnt_rowsOf_interest;
	  // printf("#\t\tthresh row(max)=%f/%f, at %s:%d\n", rows__max_val, arrOf_ranks_max, __FILE__, __LINE__);
	  if( (cnt_rowsOf_interest != 0)) {  //! then we compute clusters:
	    // printf("\t\t\t[%u/%u][%u/%u], at %s:%d\n", (uint)cnt_rowsOf_interest, obj_cpy_ranksEachRow.nrows,  (uint)cnt_interesting_total, obj_cpy_ranksEachRow.ncols*obj_cpy_ranksEachRow.nrows,  __FILE__, __LINE__);
	    //!
	    //!
	    s_kt_clusterAlg_fixed_resultObject_t obj_tmp = initAndReturn__s_kt_clusterAlg_fixed_resultObject_t(); // init__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp, cnt_cluster, nrows);
	    //!
	    //! Compute disjoint-clusters: 
	    //const e_hpLysis_clusterAlg_t alg_type = (alg_type__ != e_hpLysis_clusterAlg_undef) ? alg_type__ : e_hpLysis_clusterAlg_kCluster__AVG;
	    if(alg_type == e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds) {
	      const bool is_ok = compute__maskedMatrix__kt_clusterAlg_dbScan(&mat_tmp, 
									     &map_interestingRows,
									     /*obj_result*/&obj_tmp,
									     objMetric__postMerge,
									     /*config=*/self->config.clusterConfig);
	      assert(is_ok);
	    } else { //! then we apply the appraoch which does Not manage to dientify disjoint-regions (oekseth, 06. jan. 2019).
	      // TODO: cosnider provdiing support which does Not require the copy of 'odl data' (oesketh, 06. jan. 2019).
	      s_kt_matrix_base_t obj_copy = initAndReturn__allocate__s_kt_matrix_base_t(mat_tmp.nrows, mat_tmp.ncols, mat_tmp.matrix);
	      t_float score_max = T_FLOAT_MIN_ABS;
	      for(uint row_id = 0; row_id < mat_tmp.nrows; row_id++) {
		if(map_interestingRows.list[row_id] == false) {
		  for(uint col_id = 0; col_id < mat_tmp.ncols; col_id++) {
		    mat_tmp.matrix[row_id][col_id] = T_FLOAT_MAX; //! ie, ignore.
		  }
		} else {
		  for(uint col_id = 0; col_id < mat_tmp.ncols; col_id++) {
		    if(mat_tmp.matrix[row_id][col_id] != T_FLOAT_MAX) {
		      score_max = macro_max(score_max, mat_tmp.matrix[row_id][col_id]);
		    }
		  }
		}
	      }
	      if(score_max != T_FLOAT_MIN_ABS) { //! then values were found:
		//get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_tmp);
		const t_float epsilon = score_max + 1; // self->config.kdConfig.ballsize; // FIXME: olek: validate this.
		const uint minpts  = 0; // self->config.kdConfig.nn; // FIXME: olek: validate this.
		const bool is_ok = compute__alg_dbScan_brute(e_alg_dbScan_brute_sciKitLearn, &obj_copy, &(obj_tmp), epsilon, minpts);
		assert(is_ok);
	      }
	      free__s_kt_matrix_base_t(&obj_copy);
	    }
	    const uint cnt_cluster = obj_tmp.clusterId_size;
	    if(cnt_cluster > 0) {
	      //!
	      //! Comptue the CCM-score: 
	      const bool isImproved = apply__CCM__updateBestScore__s_kt_clusterAlg_config__ccm_t(&config__CCM, &obj_tmp, currentResult, &(self->dynamicKMeans__best__score)); //! where latter funciton is define din our "kt_clusterAlg_config__ccm.c" and where the matrix is set wrt. the config__CCM.matrix_unFiltered attribute/proeprty	    
#define __print     
	      //#define __print      printf("[%u][%u]\t ccm=%f, cnt_cluster=%u, at %s:%d\n", thres_row, thres_col, (self->dynamicKMeans__best__score), cnt_cluster, __FILE__, __LINE__);
	      //!
	      //! 
	      //!
	      //! Update the CCM-scores:	    
	      if(__isTo_useAverage) {
		if(iter_index > 0) { //! then we merge: 
		  if( (self->dynamicKMeans__best__score != T_FLOAT_MAX) && (self->dynamicKMeans__best__score != T_FLOAT_MIN_ABS) ) {
		    currentResult = self->dynamicKMeans__best__score = (currentResult + self->dynamicKMeans__best__score)*0.5; //! ie, the average-score.
		  } else {
		    currentResult = self->dynamicKMeans__best__score;
		  }
		}
		//! For simplficyt slect the alst-clsuter-socre, ie, givent he abmitgity i merigng 'on-average'.
		__print
		  free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
		self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
		self->dynamicKMeans__best__nClusters = cnt_cluster;
	      } else if((self->obj_result_kMean.vertex_clusterId == NULL) || (isImproved && __isTo_selectBest) || (!isImproved && !__isTo_selectBest) ) {
		__print
		  free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
		self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
		self->dynamicKMeans__best__nClusters = cnt_cluster;
		currentResult = self->dynamicKMeans__best__score;
		if(false) { //! Export: traverse teh generated result-matrix-object and write out the results:    
		  //! Note: the [”elow] calls implictly 'test' the/our "cuttree(..)" proceudre (first described in the work of "clsuter.c"):	  
		  const uint *vertex_clusterId = self->obj_result_kMean.vertex_clusterId;
		  if(vertex_clusterId) {
		    const uint cnt_vertex = self->obj_result_kMean.cnt_vertex;
		    assert(cnt_vertex > 0);
		    FILE *file_out = stdout;
		    fprintf(file_out, "clusterMemberships=[");
		    uint max_cnt = 0;
		    for(uint i = 0; i < cnt_vertex; i++) {fprintf(file_out, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
		    fprintf(file_out, "], w/biggestClusterId=%u, n=%u, at %s:%d\n", max_cnt, cnt_vertex, __FILE__, __LINE__);
		    assert(vertex_clusterId != NULL);
		  }
		}
		//  = ccm__local;
	      } else { //! then the 'previosu result' 'had' a better score
		//	__print
		const uint *vertex_clusterId = self->obj_result_kMean.vertex_clusterId;
		if(vertex_clusterId) {
		  assert(vertex_clusterId != NULL);
		  assert(obj_tmp.vertex_clusterId != vertex_clusterId); //! ie, to avoid cealring our own object.
		}
		free__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp);
		assert(self->obj_result_kMean.vertex_clusterId != NULL);
	      }      
	      //!
	      //! 
	      iter_index++;
#undef __print
	      //	    .obj_mapOf_rootVertices_pointer = &map_interestingRows;
	    } //! else no idneifed clsuter, ie, no point investiioantg
	  } 
	  //!
	  //! De-allocate:
	    free__s_kt_list_1d_uint_t(&map_interestingRows);
	}
	//!
	//! De-allcoate:
	assert(arrOf_ranks); free_1d_list_float(&arrOf_ranks);
      } //! else there are Not any interesting cells, ie, dorps operaiton.
    }
    assert(arrOf_counts);
    free_1d_list_float(&arrOf_counts);
    
    //! 
    // relativeWeight_eachPercent = (max_rank - min_rank);
    /* //!  */
    /* //! Then transpose the matrix:  */
    /* isOk_rankCpy = init__copy_transposed__thereafterRank__s_kt_matrix(&obj_cpy_ranksEachRow_transp, matrix_input, /\*isTo_updateNames=*\/false);  */
    /* assert(isOk_rankCpy);     assert(matrix_input->nrows == obj_cpy_ranksEachRow.ncols); */
    free__s_kt_matrix(&obj_cpy_ranksEachRow);
    free__s_kt_matrix(&mat_tmp);
    //    free__s_kt_matrix_base_t(&mat_localSim);
    return; //! ie,a s we at this ecution-point asusmes we have complted the evlauaiton-appraoch.
  } //! else we use a KD-tree appraoch


    if(false) {
      printf("## prints inptu-amtrix w/dims=[%u, %u], at %s:%d\n", matrix_input->nrows, matrix_input->ncols, __FILE__, __LINE__);
      const s_kt_matrix_t *obj_1 = matrix_input;
      for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
	uint cnt_interesting = 0;
	for(uint col_id = 0; col_id < obj_1->ncols; col_id++) {      
	  if(isOf_interest(obj_1->matrix[row_id][col_id])) {
	    printf("%f\t", obj_1->matrix[row_id][col_id]); //cpy_ranksEachRow.matrix[row_id][col_id]);
	  }
	}
	printf("\n");
      }
    }
  //! 
  //! 
  loint iter_index = 0;
  const uint nodeThresh_startPos = 5;
  for(uint nodeThresh = nodeThresh_startPos; nodeThresh < 95; nodeThresh++) {
    t_float threshold_min = 0; //T_FLOAT_MAX; 
    t_float threshold_max = (t_float)nodeThresh; //! ie, to have a valeu in range [0, 1], ie, as we use the "useRelativeScore_betweenMinMax" prorpety.
    //    t_float threshold_max = (t_float)nodeThresh*0.01; //! ie, to have a valeu in range [0, 1], ie, as we use the "useRelativeScore_betweenMinMax" prorpety.
    //!
    //! For simplcity evalate all cases wrt. value-trehshodls:
    uint row_thresh_start = 3; uint row_thresh_end = matrix_input->nrows+1;
    if(row_thresh_start >= row_thresh_end) {row_thresh_start = row_thresh_end; row_thresh_end++;} //! ie, to evaluate at least one combiantion-case.
    if(row_thresh_end > 100) {row_thresh_end = 100;} // FIXME[new-artilce]: vlidate this assumptioni ... which is used for perfmrance-tuning (oesketh, 06. otk. 2017).
    for(uint row_thresh = row_thresh_start; row_thresh < row_thresh_end; row_thresh++) {
      t_float nodeThres_min = T_FLOAT_MAX; t_float nodeThres_max = (t_float)row_thresh;
      bool useRelativeScore_betweenMinMax = true; 
      bool isTo_adjustToRanks_beforeTrehsholds_forRows = true;
      //!
      //!
      s_kt_clusterAlg_fixed_resultObject_t obj_tmp = initAndReturn__s_kt_clusterAlg_fixed_resultObject_t(); // init__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp, cnt_cluster, nrows);
      if(use_kdTreeApproach == false) {
	printf("\t thresh(node)=[%f, %f/%u], thresh(row)=[%f, %f], at %s:%d\n", nodeThres_min, nodeThres_max, matrix_input->ncols, threshold_min, threshold_max, __FILE__, __LINE__);
	{ 
	  const bool is_ok = compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(matrix_input,
										       /*obj_result*/&obj_tmp,
										       ///*obj_result*/&(self->obj_result_kMean), 
										       threshold_min, threshold_max,
										       nodeThres_min, nodeThres_max,
										       /*nodeThresh__useDirectSCoreInteadOfCount=*/false,
										       useRelativeScore_betweenMinMax, 
										       /*isTo_adjustToRanks_beforeTrehsholds_forRows=*/true,
										       /*forRankAdjustment_adjsutTo_0_100=*/true,
										       /*isTo_insertIntoCompressedMatrix_beforeComptautiosn=*/false, //! ie, as we assuemt hat node-thresohlds are Not applied
										       objMetric__postMerge,
										       /*config=*/self->config.clusterConfig,
										       /*use_bruteDisjointAlg*/(alg_type != e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds) );
	  assert(is_ok);

	} //else {
	if(nodeThresh != nodeThresh_startPos) {continue;} // FIXME: removing this when we have figured out how to use the "nodeThresh" option in our kd-tree-implemtantion.
	s_kd_searchConfig_config_t conf_local = self->config.kdConfig;
	conf_local.nn = row_thresh;
	conf_local.ballsize = (t_float)row_thresh; // FIXME: validte this ... ie, to 'esnrue' that we ahve e gneierc support for other 'case's than rank-ids ... eg, by the 'sum-of-neibhours-inisde' to idneitfy/descirbe centrliaty of vertices ... then selecitnt the otpmost/best-scoring/best-rankining vertices.


	//! Build the kd-tree:
	s_kd_tree_t obj_kd = build_tree__s_kd_tree(matrix_input);     
	s_kd_searchConfig_t obj_search = init__fromConf__s_kd_searchConfig_t(//e_kd_tree_searchStrategy_r_nearest, 
									     &obj_kd, 
									     conf_local,
									     ///*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, 									     
									     &(self->config.corrMetric_insideClustering)
									     //&obj_metric
									     );
	//! Compute the clusters:
	s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
	// free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean));
	//! Insert into the result-object:
	obj_tmp = init_fromListOfClusterMemberships__s_kt_clusterAlg_fixed_resultObject_t(&map_clusterId, /*default-vertex-count=*/map_clusterId.list_size);
	//self->obj_result_kMean = init_fromListOfClusterMemberships__s_kt_clusterAlg_fixed_resultObject_t(&map_clusterId);
	//! 
	//! De-allocate:
	free__s_kt_list_1d_uint_t(&map_clusterId);
	free__s_kd_searchConfig_t(&obj_search);
	free__s_kd_tree(&obj_kd);
      }
      const uint cnt_cluster = obj_tmp.clusterId_size;
      //!
      //! Comptue the CCM-score: 
      const bool isImproved = apply__CCM__updateBestScore__s_kt_clusterAlg_config__ccm_t(&config__CCM, &obj_tmp, currentResult, &(self->dynamicKMeans__best__score)); //! where latter funciton is define din our "kt_clusterAlg_config__ccm.c" and where the matrix is set wrt. the config__CCM.matrix_unFiltered attribute/proeprty
       #define __print     
      //#define __print      printf("[%u]\t ccm=%f, cnt_cluster=%u, at %s:%d\n", row_thresh, (self->dynamicKMeans__best__score), cnt_cluster, __FILE__, __LINE__);
      //!
      //! Update the CCM-scores:
      if(__isTo_useAverage) {
	if(iter_index > 0) { //! then we merge: 
	  if( (self->dynamicKMeans__best__score != T_FLOAT_MAX) && (self->dynamicKMeans__best__score != T_FLOAT_MIN_ABS) ) {
	    currentResult = self->dynamicKMeans__best__score = (currentResult + self->dynamicKMeans__best__score)*0.5; //! ie, the average-score.
	  } else {
	    currentResult = self->dynamicKMeans__best__score;
	  }
	}
	//! For simplficyt slect the alst-clsuter-socre, ie, givent he abmitgity i merigng 'on-average'.
	__print
	free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
	self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
	self->dynamicKMeans__best__nClusters = cnt_cluster;
      } else if((self->obj_result_kMean.vertex_clusterId == NULL) || (isImproved && __isTo_selectBest) || (!isImproved && !__isTo_selectBest) ) {
	__print
	free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
	self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
	self->dynamicKMeans__best__nClusters = cnt_cluster;
	currentResult = self->dynamicKMeans__best__score;
	if(false) { //! Export: traverse teh generated result-matrix-object and write out the results:    
	  //! Note: the [”elow] calls implictly 'test' the/our "cuttree(..)" proceudre (first described in the work of "clsuter.c"):	  
	  const uint *vertex_clusterId = self->obj_result_kMean.vertex_clusterId;
	  assert(vertex_clusterId);
	  const uint cnt_vertex = self->obj_result_kMean.cnt_vertex;
	  assert(cnt_vertex > 0);
	  FILE *file_out = stdout;
	  fprintf(file_out, "clusterMemberships=[");
	  uint max_cnt = 0;
	  for(uint i = 0; i < cnt_vertex; i++) {fprintf(file_out, "%u->%u, ", i, vertex_clusterId[i]); max_cnt = macro_max(vertex_clusterId[i], max_cnt);}
	  fprintf(file_out, "], w/biggestClusterId=%u, n=%u, at %s:%d\n", max_cnt, cnt_vertex, __FILE__, __LINE__);
	  assert(vertex_clusterId != NULL);
	}
	//  = ccm__local;
      } else { //! then the 'previosu result' 'had' a better score
	//	__print
	const uint *vertex_clusterId = self->obj_result_kMean.vertex_clusterId;
	assert(vertex_clusterId != NULL);
	assert(obj_tmp.vertex_clusterId != vertex_clusterId); //! ie, to avoid cealring our own object.
	free__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp);
	assert(self->obj_result_kMean.vertex_clusterId != NULL);
      }      
      //!
      //! 
      iter_index++;
#undef __print
    }
  }
  //  free__s_kt_matrix(&obj_cpy_ranksEachRow);
  //  free__s_kt_matrix_base_t(&mat_localSim);
}

//! Comptue the random-clusters (oekseth, 06. jul. 2017)
static void   __randomClustering__hpLysis_api(s_hpLysis_api_t *self, const s_kt_matrix_t *matrix_input, const bool __isTo_selectBest, const bool __isTo_useAverage) {
  //! ---------------------------- 
  //!
  //! Inlitaze the vlaue using an 'CCM-specfic' worst-case-vlaue':
  s_kt_clusterAlg_config__ccm config__CCM = self->config.config__CCM__dynamicKMeans;
  s_kt_matrix_base_t matrixResult__inputToClustering_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(matrix_input);
  self->config.config__CCM__dynamicKMeans.matrix_unFiltered = &matrixResult__inputToClustering_shallow;
  config__CCM.matrix_unFiltered = &matrixResult__inputToClustering_shallow;
  t_float currentResult = get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(&config__CCM);
  //!
  //! Local logics: 
  uint niter_random = self->config.config__randomClusterIndetificaiton__cntRandomIterations;
  if( (niter_random == 0) || (niter_random == UINT_MAX) ) { niter_random = 10*1000;} //! ie,, hte fall-back.
  //! 
  //! Fidnt eh best CCM-score.     
  for(uint rand_id = 0; rand_id < niter_random; rand_id++) {
    //!
    //! Construct a randome data-sample: 
    // FIXME: why is "nrows == cnt_cluster"? (oekseth, 06. des. 2020)?
    const uint cnt_cluster = matrix_input->nrows;
    const uint nrows = cnt_cluster;
    s_kt_clusterAlg_fixed_resultObject_t obj_tmp; init__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp, cnt_cluster, nrows);
    for(uint i = 0; i < obj_tmp.cnt_vertex; i++) {
      obj_tmp.vertex_clusterId[i] = rand() % cnt_cluster; 
    }
    //self.randomConfig = initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t();
	 //       s_kt_clusterAlg_fixed_resultObject_t obj_tmp = __MiF__getKClusterObject();
    //!
    //! Comptue the CCM-score: 
    const bool isImproved = apply__CCM__updateBestScore__s_kt_clusterAlg_config__ccm_t(&config__CCM, &obj_tmp, currentResult, &(self->dynamicKMeans__best__score)); //! where latter funciton is define din our "kt_clusterAlg_config__ccm.c" and where the matrix is set wrt. the config__CCM.matrix_unFiltered attribute/proeprty
    if(__isTo_useAverage) {
      if(rand_id > 0) { //! then we merge: 
	if( (self->dynamicKMeans__best__score != T_FLOAT_MAX) && (self->dynamicKMeans__best__score != T_FLOAT_MIN_ABS) ) {
	  // FIXME: should we add a cluster-merge-step? <-- how? <-- assing vertices to both?
	  currentResult = self->dynamicKMeans__best__score = (currentResult + self->dynamicKMeans__best__score)*0.5; //! ie, the average-score.
	} else {
	  currentResult = self->dynamicKMeans__best__score;
	}
      }
      //! For simplficyt slect the alst-clsuter-socre, ie, givent he abmitgity i merigng 'on-average'.
      free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
      self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
      // self->dynamicKMeans__best__nClusters = nclusters;
    } else if( (isImproved && __isTo_selectBest) || (!isImproved && !__isTo_selectBest) ) {
      free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean)); 
      self->obj_result_kMean = obj_tmp; //! ie, 'grab' the memory-allocations of teh latter.
      //      self->dynamicKMeans__best__nClusters = cnt_c;
      currentResult = self->dynamicKMeans__best__score;
      //  = ccm__local;
    } else { //! then the 'previosu result' 'had' a better score
      free__s_kt_clusterAlg_fixed_resultObject_t(&obj_tmp);
    }
    //!
    //!
  }
}

//! Apply the clsutering, where the result is stored in the 'itnernal' "self->matrixResult__inputToClustering" "s_hpLysis_api_t" variable (ie, from which the latter result may be accessed).
bool filterAmdCorrelate__hpLysis_api(s_hpLysis_api_t *self, const s_kt_matrix_t *obj_1__, const s_kt_matrix_t *obj_2__, const bool isTo_applyPreFilters) {
  //!
  //! 'Consturct' matrices and Apply filters:
#include "hpLysis_api__stub__applyFilters.c"
  //!
  { //! Apply clustering, and 'write out' teh result:
    if(self->config.corrMetric_prior.metric_id == e_kt_correlationFunction_undef) { //! ie, ahandle a 'pointelss and in-cosnistent call':
      fprintf(stderr, "!!\t Seems like you have set the \"self->corrMetric_prior.metric_id = e_kt_correlationFunction_undef\", ie, a pointless call. We will now updatete the latter using the default silmiartly-metric in hpLysis_api.h. If the latter soudns odd then it might be an inconsticny in your configuraiton: if evlauating your latter API does not helkp, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up.
      //! Apply a fall-back:
      self->config.corrMetric_prior = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3);
    }
    const bool is_ok = kt_matrix__distancematrix(self->config.corrMetric_prior, matrix_input_1, matrix_input_2,  obj_result, self->config.clusterConfig);
    assert(is_ok); //! ie, what we expect.
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    if(self->stringOfResultPrefix__exportCorr__prior && strlen(self->stringOfResultPrefix__exportCorr__prior)) {
      //printf("\t# Export correlationi-matrix, at %s:%d\n", __FILE__, __LINE__);
      //! Then write out the correlation-matirx:      
      assert(strlen(self->stringOfResultPrefix__exportCorr__prior) < 2000); //! ie, asw e otherwise need updating [”elow]
      char string_local[2500] = {'\0'}; sprintf(string_local, "%s.corrMetric_prior.tsv", self->stringOfResultPrefix__exportCorr__prior);

      /*   //! The export-call: */
      /*   const bool is_ok = export__singleCall__s_kt_matrix_t(obj_result, string_local__stub); */
      /*   assert(is_ok); //! ie, what we expect. */
      /*   free_1d_list_char(&string_local__stub); string_local__stub = NULL; */
      /* } else { */
      //! The export-call:
      const bool is_ok = export__singleCall__s_kt_matrix_t(obj_result, string_local, self->config.clusterConfig.fileHandler);
      assert(is_ok); //! ie, what we expect.
      //}
    }
  }
  //! --------------------------------------------
  //! 
  //! De-allocate: 
#include "hpLysis_api__stub__applyFilters__free.c"
  /* //! */
  /* //! 'Set back' the information 'wchih we used' wrt. the weight-table: */
  /* if(matrixInput__weight_1) {matrix_input_1->weigth = matrixInput__weight_1;} */
  /* if(matrixInput__weight_2) {matrix_input_2->weigth = matrixInput__weight_2;} */

  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}



//! !pply filtering and comptue a scalar (based on a spcifid correlation-matrix) using Knitting-Tools approx. 1,000,000 different correlationi-metrics (oekseth, 06. jan. 2017).
bool scalar__filterAmdCorrelate__hpLysis_api(s_hpLysis_api_t *self, const e_kt_clusterComparison_t clusterCmp_method_id, const s_kt_matrix_t *obj_1__, const s_kt_matrix_t *obj_2__, const bool isTo_applyPreFilters, t_float *scalar_result) {
  //!
  //! 'Consturct' matrices and Apply filters:
#include "hpLysis_api__stub__applyFilters.c"
  //! --------------------------------------------
  //! 
  //! Apply filter-identificaiton:
  const bool is_ok = scalar__getSimliarty__betwenMatrices__kt_distance_cluster(clusterCmp_method_id, self->config.corrMetric_prior, matrix_input_1, matrix_input_2, scalar_result);  
  assert(is_ok);
  //! --------------------------------------------
  //! 
  //! De-allocate: 
#include "hpLysis_api__stub__applyFilters__free.c"
  /* //! */
  /* //! 'Set back' the information 'wchih we used' wrt. the weight-table: */
  /* if(matrixInput__weight_1) {matrix_input_1->weigth = matrixInput__weight_1;} */
  /* if(matrixInput__weight_2) {matrix_input_2->weigth = matrixInput__weight_2;} */

  //! ---------------------------------------------
  return is_ok; //! ie, as we asusme the oerpaiton was au success.
}

//! Apply the clsutering:
bool cluster__hpLysis_api(s_hpLysis_api_t *self, const e_hpLysis_clusterAlg_t alg_type__, const s_kt_matrix_t *obj_1__, const uint nclusters__inp, const uint npass) {
  assert(self); assert(obj_1__);
  uint nclusters = nclusters__inp;
  if(!self || !obj_1__ || !obj_1__->nrows || !obj_1__->ncols) {
    fprintf(stderr, "!!\t Your configuraiton does not follow the minimum-requirements: we expect botht eh input-object and input-matrix to contain data; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
    return false;
  }
  assert(alg_type__ <= e_hpLysis_clusterAlg_undef); //! ie, as the caller may then ahve provided a wrong 'uint'.
  //! ---------------------------------------  
  const s_kt_matrix_t *matrix_input = obj_1__;
  s_kt_matrix_t obj_cpy_ranksEachRow; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  bool isTo_transposeMatrix = self->config.clusterConfig.isTo_transposeMatrix;
  
#ifndef NDEBUG
/* #if(configureDebug_useExntensiveTestsIn__tiling == 1)  */
/*     { //! then we vlaidate wrt. 'specific inptu-values': */
/*       assert(matrix_input); */
/*       t_float **data = matrix_input->matrix; */
/*       const uint nrows = matrix_input->nrows; */
/*       const uint ncols = matrix_input->ncols; */
/*       assert(data); */
/*       for(uint i = 0; i < nrows; i++) { */
/* 	for(uint j = 0; j < ncols; j++) { */
/* 	  const t_float score = data[i][j]; */
/* 	  assert(isinf(score) == false); */
/* 	  assert(isnan(score) == false); */
/* 	} */
/*       } */
/*     } */
/* #endif */
#endif  
  {
    const uint nelements = (isTo_transposeMatrix == false) ? obj_1__->nrows : obj_1__->ncols;
    if(nclusters >= nelements) {
      // TODO: cosndier making [”elow] optioanl wrt. k-means clsuteirng
      if(nclusters != UINT_MAX) {
	nclusters = 0.5 * nclusters; //! ie, as the error-message may be due to an autoamtic-gnerrated-aprpaoch (oekseth, 06. aug. 2017).
	assert(nclusters > 0);
	fprintf(stderr, "!!(configuration-error:continues-after-update)\t We change the number-of-clusters from '%u' to '%u': Seems like you have requested %u clusters for a data-set with %u elements (given an input-matrix with dimensionis=[%u, %u]), ie, a pointless call. We will now updatete the latter using the default silmiartly-metric in hpLysis_api.h. If the latter sounds odd then it might be an inconsistency in your configuraiton: if evaluation your API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", nclusters, nclusters__inp, nclusters, nelements, obj_1__->nrows, obj_1__->ncols, __FUNCTION__, __FILE__, __LINE__);
	//return false;
      }
    }
  }
  //#ifndef NDEBUG
  { //! Validate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
    const s_kt_matrix_t *matrix_input_1 = matrix_input;
    assert(matrix_input_1);
    assert(matrix_input_1->nrows);
    assert(matrix_input_1->ncols);
    long long int cnt_interesting = 0;
    for(uint i = 0; i < matrix_input_1->nrows; i++) {
      for(uint j = 0; j < matrix_input_1->ncols; j++) {
	cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
    // assert(cnt_interesting > 0); //! ie, as we otherwse have a poitnless input to the clsutering
    if( (cnt_interesting == 0) ) {
      //*scalar_result = T_FLOAT_MAX;
      return true;
    }
  }
  //#endif

  //printf("## npass=%u, nclusters=%u, at %s:%d\n", npass, nclusters, __FILE__, __LINE__);

  const e_hpLysis_clusterAlg_t alg_type = (alg_type__ != e_hpLysis_clusterAlg_undef) ? alg_type__ : e_hpLysis_clusterAlg_kCluster__AVG;

  assert(alg_type <= e_hpLysis_clusterAlg_undef); //! ie, as the caller may then ahve provided a wrong 'uint'.

  //! --------------------------------------------
  //! 
  //! Apply the filtering:
  __applyPosteriori_filters__hpLysis_api(self, &matrix_input, &obj_cpy_ranksEachRow, isTo_transposeMatrix, self->config.config__valueMask);
  isTo_transposeMatrix = false; //! ie, as we expect htis to have been 'handled' in [above]
#ifndef NDEBUG
  { //! Validate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
    const s_kt_matrix_t *matrix_input_1 = matrix_input;
    assert(matrix_input_1);
    assert(matrix_input_1->nrows);
    assert(matrix_input_1->ncols);
    long long int cnt_interesting = 0;
    for(uint i = 0; i < matrix_input_1->nrows; i++) {
      for(uint j = 0; j < matrix_input_1->ncols; j++) {
	cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
    assert(cnt_interesting > 0); //! ie, as we otherwse have a poitnless input to the clsutering
  }
#endif
  //! --------------------------------------------
  //! 
  const bool beforeCall__inputMatrix__isAnAdjecencyMatrix = self->config.clusterConfig.inputMatrix__isAnAdjecencyMatrix;
  bool __local__corrMetric__prior_use = self->config.corrMetric_prior_use;
  const s_kt_correlationMetric beforeCall__corrMetric_prior = self->config.corrMetric_prior;
  //!
  //! Hadnle cases cprfiic for some of the algorithms we support (oekseth, 06. feb. 2017):
  if(self->config.corrMetric_prior_use == false) {
    //    printf("at %s:%d\n", __FILE__, __LINE__);
    if(
       (alg_type == e_hpLysis_clusterAlg_kCluster__medoid)
       //|| (alg_type == e_hpLysis_clusterAlg_disjoint_mclPostStrategy) 
       || (alg_type == e_hpLysis_clusterAlg_kruskal_hca) 
       || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed) 
       || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) 
       || ( (nclusters <= 1) || (nclusters == UINT_MAX) ) //! then 'an adjcency-amtrix' is needed as input for our clsuter-algorithms, ie, due to the 'expected' CCM-step to apply
       ) {
      if(
	 (alg_type != e_hpLysis_clusterAlg_disjoint) 
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_DBSCAN) 
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_mclPostStrategy) 
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds)
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds) 
	 // FIXME: validte wrt. the MCL-algorithm-impemtantion .... 
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_MCL) 
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_kdTree) 
	 && (alg_type != e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN) 
	 //	 && (alg_type != ) 
	 ) {
	
	//	printf("at %s:%d\n", __FILE__, __LINE__);
	__local__corrMetric__prior_use = true; //! ie, as we for medodi 'need' a correlation-emtric as input: for 'this task' we we use the 'inside-clsuteirng' simlairty-metric, ie, as we asusem 'this appraoch' correclty captures the users itneitnos. 
	self->config.corrMetric_prior = self->config.corrMetric_insideClustering;
	if(self->config.corrMetric_prior.metric_id == e_kt_correlationFunction_undef) {
	  self->config.corrMetric_prior = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1); //! ie, set the default assumption.
	}
      }
    }
  }
  //!
  s_kt_matrix_base_t matrixResult__inputToClustering_shallow = initAndReturn__empty__s_kt_matrix_base_t(); //! which is used when CCM-based 'applicaotn' is 'to be used in conjuciton with clsuteirng' (oekseth, 06. mar. 2017).
  if(__local__corrMetric__prior_use == true) { //! then we apply the correlation-metrics to the inptu-data-set:
    // printf("at %s:%d\n", __FILE__, __LINE__);
    const bool is_ok_corr = filterAmdCorrelate__hpLysis_api(self, matrix_input, NULL, /*isTo_applyPreFilters=*/false);
    assert(is_ok_corr);
    self->config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //! ie, givent eh result of [above].
    assert(self->matrixResult__inputToClustering.matrix);
    assert(self->matrixResult__inputToClustering.nrows > 0);
    // assert(self->matrixResult__inputToClustering.nrows == matrix_input->nrows); //! which should hold for a 'non-transposed' case.
    //!
    //! Update the reference (and first de-allcoate if we allcoated  a 'sperapte' matrix as input):
    if(matrix_input->matrix != obj_1__->matrix) {
      free__s_kt_matrix(&obj_cpy_ranksEachRow);          
    }
    obj_cpy_ranksEachRow = self->matrixResult__inputToClustering;
    matrix_input = &(self->matrixResult__inputToClustering);
    //! Update the CCM-matrix to be used (oekseth, 06. mar. 2017):
    matrixResult__inputToClustering_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&(self->matrixResult__inputToClustering));
    self->config.config__CCM__dynamicKMeans.matrix_unFiltered = &matrixResult__inputToClustering_shallow;
  } else { //! then we investigate the inptu-object wrt. the "weight" option:
    // printf("at %s:%d\n", __FILE__, __LINE__);
    const s_kt_matrix_t *matrix_input_1 = matrix_input;
    if(matrix_input_1 && matrix_input_1->weight) { 
      assert(matrix_input_1->ncols);
      uint cnt_zero = 0; uint cnt_extreme_min = 0; uint cnt_extreme_max = 0; uint cnt_1 = 0;
      for(uint col_id = 0; col_id < matrix_input_1->ncols; col_id++) {
	const t_float score = matrix_input_1->weight[col_id];
	if(score == 0) {cnt_zero++;}
	else if(score == T_FLOAT_MIN_ABS) {cnt_extreme_min++;}
	else if(score == T_FLOAT_MAX) {cnt_extreme_max++;}
	else if(score == 1) {cnt_1++;}
      }
      if(cnt_1 == matrix_input_1->ncols) { //! then we assuem the 'weight is Not needed'.
	fprintf(stderr, "(get-performance-increase)\t In your configuration we observe you have set all weights to '1', ie, a pointless call, ie, a call whcih introduce an unnceccessary complexity in your call). What we reccoment is to Not use a weight-table (in your input), thereby reducing the exueciotn-tiem of your operation (while keeping your rsults higly accurate). To summarsie please update your API call. For quesiton please cotnact the seionor developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	//matrixInput__weight_1 = matrix_input_1->weight;
	//matrix_input_1 = matrix_input_1->weight;
	//matrix_input_1->weight = NULL; 
      } else {
	const uint sum_empty = (cnt_zero + cnt_1 + cnt_extreme_min + cnt_extreme_max);
	if(sum_empty == matrix_input_1->ncols) {
	  fprintf(stderr, "!!\t Seems like all weigths are set to 'not-to-use' values, ie, please validate correctness of your call: indicates errornous use of your configuration-API. For quesiton please cotnact the seionor developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	  assert(false);
	  return false;
	}
      }
    }    
  }
  //#ifndef NDEBUG
  { //! Validate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
    const s_kt_matrix_t *matrix_input_1 = matrix_input;
    assert(matrix_input_1);
    assert(matrix_input_1->nrows);
    assert(matrix_input_1->ncols);
    long long int cnt_interesting = 0;
    for(uint i = 0; i < matrix_input_1->nrows; i++) {
      for(uint j = 0; j < matrix_input_1->ncols; j++) {
	cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
    if(cnt_interesting == 0) {
      if(false) {
	printf("(info)\t after application of your request simliarty-metric, it seems like no specific relationship seeprates your varialbes (or relatibvely brings them togheter). We therefore stop exueciotn, as no clsuteing-algorithm will be able to find any patterns in your data: for some cases the latter observation is an important feature, ie, this observaiton is (ofr some cases) Not an indication of an eror. However, if the latter observiaotn sounds odd, then please cotnact senior delvoeper [oekseth@gamil.com]. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      }
      /* if(true) { */
      /* 	for(uint i = 0; i < matrix_input_1->nrows; i++) { */
      /* 	  printf("row[%u]: ", i); */
      /* 	  for(uint j = 0; j < matrix_input_1->ncols; j++) { */
      /* 	    if(is */
      //}
      // assert(false); // FIXME: remove.
      return true; //! ie, as we have observed 'this case' to be a vlaidd case for some data-sets.
    }
    //assert(cnt_interesting > 0); //! ie, as we otherwse have a poitnless input to the clsutering
  }
  //#endif
#ifndef NDEBUG
/* #if(configureDebug_useExntensiveTestsIn__tiling == 1)  //! where [”elow] si out-commeted as we observe for some metrics that the vlaue increase to inf for large number, eg, wrt. the '*pow5* minkoski-emtrics (oekseth, 06. arp. 2017) */
/*     { //! then we vlaidate wrt. 'specific inptu-values': */
/*       assert(matrix_input); */
/*       t_float **data = matrix_input->matrix; */
/*       const uint nrows = matrix_input->nrows; */
/*       const uint ncols = matrix_input->ncols; */
/*       assert(data); */
/*       for(uint i = 0; i < nrows; i++) { */
/* 	for(uint j = 0; j < ncols; j++) { */
/* 	  const t_float score = data[i][j]; */
/* 	  assert(isinf(score) == false); */
/* 	  assert(isnan(score) == false); */
/* 	} */
/*       } */
/*     } */
/* #endif */
#endif
  //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    assert(matrix_input);
  if(
     (
      isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(alg_type) 
      || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed) 
      || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) 
      )
     && ( (nclusters <= 1) || (nclusters == UINT_MAX) || (nclusters > matrix_input->nrows) ) //! then 'an adjcency-amtrix' is needed as input for our clsuter-algorithms, ie, due to the 'expected' CCM-step to apply
     ) { //! then we dienitfy the number fo clusters to use:
    if(self->config.config__CCM__dynamicKMeans__useHCAPre_step_forKMeans_ncluster) {
      t_float ccm_resultScore = 0;
      const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
      const bool is_ok = standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(matrix_input, 
										  /*isAn__adjcencyMatrix=*/((obj_1__ == matrix_input) || self->config.clusterConfig.inputMatrix__isAnAdjecencyMatrix),
										  // TODO: consider 'addinb [”elow] as an optional cofngi-param.
										  /*simMetric=*/metric_id,
										  /*ccm=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
										  // TODO: consider 'addinb [”elow] as an optional cofngi-param.
										  /*clster-alg=*/e_hpLysis_clusterAlg_HCA_centroid,
										  &ccm_resultScore,
										  &nclusters);
      assert(is_ok); //! ei, what we expect
      if(nclusters <= 1) {
	fprintf(stderr, "!!\t For dynamic clsutering no clsutrers were found. If the latter is Not as expected W(rt. the input-data), please invseiggate your cofnigruations. For quesitons, please contact senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	return true; //! ie,a s the 'case may be valid'.
      }
    } //! else we assume a user/caller is explcty interested in using k-means 'in an tierative CCM-fashion. 
    // printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);
    //nclusters = standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(matrix, 
  } 


  //! --------------------------------------------
  //! 
  //! Investigate if it is of interest to construct two seperate matrices: one 'maksed' and one 'no-masked', ie, Afte rthe applciaton fot he [ªbove[] simalrity-metric:
  s_kt_matrix_t matrix__simAndfiltered = setToEmptyAndReturn__s_kt_matrix_t(); bool matrix__simAndfiltered__isAllocated = false;
  if(self->config.config__valueMask__afterSimMatrix.isTo_applyValueThresholds) {
    if(
       (alg_type == e_hpLysis_clusterAlg_kruskal_hca) 
       || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed) 
       || (alg_type == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) 
       //! --- 
       || (alg_type == e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG)
       || (alg_type == e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank)
       || (alg_type == e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid)
       //! --- 
       ) {
      assert(self->config.clusterConfig.isTo_transposeMatrix == false); //! ie, aswwe expect the latter 'isuse' will behave been resolved/handled in [ªbov€], ie, if the user requested a 'trasnpseo operaiton' wr.t the input-matrix.
      ///self->config__valueMask__afterSimMatrix.isTo_a
      //! Apply the filtering:
      __applyPosteriori_filters__hpLysis_api(self, &matrix_input, &matrix__simAndfiltered, /*self->isTo_transposeMatrix=*/false, self->config.config__valueMask__afterSimMatrix);
      matrix__simAndfiltered__isAllocated = true;
    } else { //! then we 'merge the results' into the "matrix_input"
      //! Apply the filtering:
      __applyPosteriori_filters__hpLysis_api(self, &matrix_input, &matrix__simAndfiltered, /*self->isTo_transposeMatrix=*/false, self->config.config__valueMask__afterSimMatrix);
      if(matrix_input->matrix != self->matrixResult__inputToClustering.matrix) {
	free__s_kt_matrix(&obj_cpy_ranksEachRow);
      } //! else we asusem 'the latter is to be used by the caller'.
      matrix_input = &matrix__simAndfiltered;    
      obj_cpy_ranksEachRow = matrix__simAndfiltered; matrix__simAndfiltered__isAllocated = false; //! ie, as we 'update' the "obj_cpy_ranksEachRow" wrt. the latter.  
    }
  } 
  //! --------------------------------------------
  //! 
  //! Handle the case where the user is intersted 'in useding the defualt type':
//const e_hpLysis_clusterAlg_t alg_type = alg_type__; //(alg_type__ != e_hpLysis_clusterAlg_undef) ? alg_type__ : e_hpLysis_clusterAlg_kCluster__AVG;
// if(alg_type__ != e_hpLysis_clusterAlg_undef) {
 if(self->config.corrMetric_insideClustering.metric_id == e_kt_correlationFunction_undef) { //! ie, ahandle a 'pointelss and in-cosnistent call':
   fprintf(stderr, "!!\t Seems like you have set the \"self->corrMetric_insideClustering.metric_id = e_kt_correlationFunction_undef\", ie, a pointless call. We will now updatete the latter using the default silmiartly-metric in hpLysis_api.h. If the latter soudns odd then it might be an inconsticny in your configuraiton: if evlauating your latter API does not helkp, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
   assert(false); //! ie, an heads-up.
   //! Apply a fall-back:
   self->config.corrMetric_insideClustering = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O1);
 } 
 bool __isTo__useDynamicK = false;
  if(isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(alg_type)) {
    if(self->randomConfig.nelements == 0) { //! then we intiate the strucutre:
      const uint nrows = matrix_input->nrows;
      if( (nclusters > 1) && (nclusters != UINT_MAX) ) {
	if(nrows <= nclusters) {
	  fprintf(stderr, "!!\t (error:kMeans)\t You have set |rows|=%u <= |clusters|=%u, ie, for whcih the result will result in a pointless cluster-executioni. In brief we suggest you update the input-cofnigirautions, eg, by using the \"tut_*.c\" tutorials as a use-case. However, if the latter does not solve your issue, please concat senior developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nrows, nclusters, __FUNCTION__, __FILE__, __LINE__); //! (eosketh, 06. mar. 2017)
	}
	updateSizeProp__s_kt_randomGenerator_vector_t(&(self->randomConfig), nclusters, nrows);
      } else {
	if(self->config.config__CCM__dynamicKMeans.isTo_pply__CCM == false) {
	  //! Then we expec tthe clsuters 'to ahve been defined', ie, an erorr in the user-stup:
	  fprintf(stderr, "!!\t (error:kMeans)\t You have Not set the |clusters| paremter, and neither the \"self->config.config__CCM__dynamicKMeans.isTo_pply__CCM\" parameter. Therefore weregard your API-usage as pointless (which is by no means negative, ie, as the latter may provide you with novel insight). In brief we suggest you update the input-cofnigirautions, eg, by using the \"tut_*.c\" tutorials as a use-case. However, if the latter does not solve your issue, please concat senior developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); //! (eosketh, 06. mar. 2017)
	  assert(false);
	} else {
	  __isTo__useDynamicK = true;
	}
      }
    }
  }
  
  //! @retunr a new-itnaited 'ranom object', usign the aprent-copfnngurations.
#define __MiF__iterativeRandom__init() ({s_kt_randomGenerator_vector_t random_each = copy__sameMatrix__s_kt_randomGenerator_vector_t((&self->randomConfig), nclusters); assert(random_each.nclusters == nclusters); /*printf("nclusters=%u, at %s:%d\n", nclusters, __FILE__, __LINE__);*/  random_each;})
  //#define __MiF__iterativeRandom__initLocalConfig() ({s_kt_randomGenerator_vector_t random_each = initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t(); /*set-matrix:*/random_each.matrix_input = self->randomConfig.matrix_input; /*set-configs:*/random_each.config_init = s

  //printf("(start-clustering)\t at %s:%d\n", __FILE__, __LINE__);

   //!
   //! Evflauat ehte differnet clsuter-cases:
 bool __haveApplied__hca = false;
   if(alg_type == e_hpLysis_clusterAlg_kCluster__AVG) {
     /* printf("## Comptue k-avg-clsuter, at %s:%d\n", __FILE__, __LINE__); */
     assert(matrix_input);
     //printf("## npass=%u, nclusters=%u, matrix-dims=[%u, %u], at %s:%d\n", npass, nclusters, matrix_input->nrows, matrix_input->ncols, __FILE__, __LINE__);
#define __MiF__cluster() ({const bool is_ok = kt_matrix__kmeans_or_kmedian(self->config.corrMetric_insideClustering, matrix_input, &result_clust, &(self->obj_result_kMean_clusterId_columnValue), nclusters, npass, /*isTo_useMean=*/true, self->config.clusterConfig, &rand_each); assert(is_ok); })
     //!
     //! Include logics:
     #include "hpLysis_api__stub__dynamicC__kPReStep.c"
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster__rank) {
#define __MiF__cluster() ({const bool is_ok = kt_matrix__kmeans_or_kmedian(self->config.corrMetric_insideClustering, matrix_input, &result_clust, &(self->obj_result_kMean_clusterId_columnValue), nclusters, npass, /*isTo_useMean=*/false, self->config.clusterConfig, &rand_each);     assert(is_ok);})
     //!
     //! Include logics:
#include "hpLysis_api__stub__dynamicC__kPReStep.c"
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster__medoid) {
#define __MiF__cluster() ({const bool is_ok = kt_matrix__kmedoids(self->config.corrMetric_insideClustering, matrix_input, &result_clust, nclusters, npass, self->config.clusterConfig, &rand_each); assert(is_ok);}) //, &(self->randomConfig));
      //! ie, what we expect
     //!
     //! Include logics:
#include "hpLysis_api__stub__dynamicC__kPReStep.c"
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch) {
     // TODO: consider 'mkaing' [”elow] aprt of the "hpLysis_api" cofnig-objecgt "self"
     s_kt_clusterAlg_fixed__alg__miniBatch_t config_kMeans = initAndReturn__s_kt_clusterAlg_fixed__alg__miniBatch_t(); //! ie, use defualt settings.
     config_kMeans.objMetric__sim = self->config.corrMetric_insideClustering;
#define __MiF__cluster() ({const bool is_ok = kt_matrix__kmeans_nonDefaultAlg__miniBatch(config_kMeans, matrix_input, &result_clust, nclusters, self->config.clusterConfig, &rand_each);    assert(is_ok); })
     //!
     //! Include logics:
     #include "hpLysis_api__stub__dynamicC__kPReStep.c"
   } else if( (alg_type == e_hpLysis_clusterAlg_disjoint)
	      || (alg_type == e_hpLysis_clusterAlg_disjoint_mclPostStrategy)
	      || (alg_type == e_hpLysis_clusterAlg_disjoint_DBSCAN) 	      
) {
     __haveApplied__hca == false;
     //#ifndef NDEBUG
     uint cnt_empty = UINT_MAX;
     //if(self->isTo_applyValueThresholds == false) {
     cnt_empty = 0;
     //! Inveistate if there are any non-mepty-cells:
     for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
       for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	 if(!isOf_interest(matrix_input->matrix[row_id][col_id])) {cnt_empty++;}
	 //else {printf("%u --> %u, at %s:%d\n", row_id, col_id, __FILE__, __LINE__);}
       }
     }
     //     }
     if( (cnt_empty != UINT_MAX) && (cnt_empty != 0) ) { //! then there might be mroe than one 'empty' cluster
       //#ednif
       //printf("cnt_empty=%u, at %s:%d\n", cnt_empty, __FILE__, __LINE__);
       s_hp_distanceCluster_t obj_conf = init__s_hp_distanceCluster_t();
       if(alg_type == e_hpLysis_clusterAlg_disjoint_DBSCAN) {
	 s_kt_matrix_base_t obj_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(matrix_input);
	 const t_float epsilon = self->config.kdConfig.ballsize; // FIXME: olek: validate this.
	 const uint minpts  = self->config.kdConfig.nn; // FIXME: olek: validate this.
	 const bool is_ok = compute__alg_dbScan_brute(e_alg_dbScan_brute_sciKitLearn, &obj_shallow, &(self->obj_result_kMean), epsilon, minpts);
	 assert(is_ok);
       } else {
	 const bool is_ok = kt_matrix__disjointClusterSeperation(matrix_input, &(self->obj_result_kMean), T_FLOAT_MAX, T_FLOAT_MAX, /*! Note: we use [ªbove] instead of [”elow] given the new-added itrnodcution of "config__valueMask__afterSimMatrix" parameter (oekseth, 06. feb. 2017) **/ /*useRelativeScore_betweenMinMax=*/true, /*isTo_adjustToRanks_beforeTrehsholds_forRows*/false, 
								 (alg_type == e_hpLysis_clusterAlg_disjoint_mclPostStrategy) ? &(self->config.corrMetric_insideClustering__mergeConf) : NULL,
								 /*! ie, as we 'did this' in the pre-step.*/self->config.clusterConfig); 
       assert(is_ok);
     }
     } else {
       if(false) {
	 fprintf(stderr, "!!\t We observe that your input-matirx does not hold any 'empty cells'. The latter implies that your data-set describes one big cluster, ie, with clsuterc-ount=1. If this is Not what you expected we would reccomend updating the filter-criterias, eg, wrt. cofnigruation-parameters such as  \"isTo_applyValueThresholds\", \"isTo_adjustToRanks_beforeTrehsholds_forRows\", \"threshold_min\", and \"threshold_max\". IF the latter pramters douns unfaimliar we suggest reading the documetnation. However if your cluster-resutls 'stillø' does not reflect your prior asusmptions, please contact the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
       }
     }
   } else if(
	     (alg_type == e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds)
	     ||
	     (alg_type == e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds)
	     ){
     //!
     //! Compute DB-SCAN usign a dynamic CCM-lsiding-thresholds-appraoch (oekseth, 06. jul. 2017):
     __ccmDynamic_disjointForest_dbScan__hpLysis_api(self, matrix_input, NULL, /*use_kdTreeApproach=*/false, alg_type); //, __isTo_selectBest, __isTo_useAverage);
   } else if(alg_type == e_hpLysis_clusterAlg_disjoint_MCL) {
     self->config.clusterConfig_mcl.obj_metric = self->config.corrMetric_insideClustering; //! ie, set the simlairty-metric to be used.
     const bool is_ok = compute__kt_clusterAlg_mcl(&(self->config.clusterConfig_mcl), matrix_input, &(self->obj_result_kMean), self->config.clusterConfig);
     assert(is_ok); 
   } else if(alg_type == e_hpLysis_clusterAlg_disjoint_kdTree) {
      //! Build the kd-tree:
      s_kd_tree_t obj_kd = build_tree__s_kd_tree(matrix_input);     
      // printf("##(init), at %s:%d\n", __FILE__, __LINE__);
      s_kd_searchConfig_t obj_search = init__fromConf__s_kd_searchConfig_t(//e_kd_tree_searchStrategy_r_nearest, 
									   &obj_kd, 
									   self->config.kdConfig,
									   ///*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, 
									   &(self->config.corrMetric_insideClustering)
									   //&obj_metric
									   );
      //! Compute the clusters:
      s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
      free__s_kt_clusterAlg_fixed_resultObject_t(&(self->obj_result_kMean));
      //! Insert into the result-object:
      self->obj_result_kMean = init_fromListOfClusterMemberships__s_kt_clusterAlg_fixed_resultObject_t(&map_clusterId, /*default-vertex-count=*/map_clusterId.list_size);
      //! 
      //! De-allocate:
      free__s_kt_list_1d_uint_t(&map_clusterId);
      free__s_kd_searchConfig_t(&obj_search);
      free__s_kd_tree(&obj_kd);
   } else if(alg_type == e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN) {
     //!
     //! Compute DB-SCAN usign a dynamic CCM-lsiding-thresholds-appraoch (oekseth, 06. jul. 2017):
     __ccmDynamic_disjointForest_dbScan__hpLysis_api(self, matrix_input, NULL, /*use_kdTreeApproach=*/true, alg_type); //, __isTo_selectBest, __isTo_useAverage);     
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG) {
     //const s_kt_matrix_t *__matrix_masked = (matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : matrix_input;
     const bool is_ok = kt_matrix__kCluster__dynamicDisjoint(/*type=*/e_kt_api_dynamicKMeans_AVG,
							     self->config.corrMetric_insideClustering,
							     //! ---
							     matrix_input,
							     (matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : NULL,
							     &(self->obj_result_kMean), 
							     &(self->obj_result_kMean_clusterId_columnValue), 
							     //! ----
							     npass,
							     //! ----
							     T_FLOAT_MAX, T_FLOAT_MAX,
							     //! Note: we use [ªbove] instead of [”elow] given the new-added itrnodcution of "config__valueMask__afterSimMatrix" parameter (oekseth, 06. feb. 2017)
							     /* self->opt_kCount__disjointThresholdFilter__scoreRelative_min, */
							     /* self->opt_kCount__disjointThresholdFilter__scoreRelative_max, */
							     /*useRelativeScore_betweenMinMax=*/true,
							     //! ----
							     self->config.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__min,
							     self->config.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__max,
							     //! ----
							     self->config.clusterConfig);
     assert(is_ok); //! ie, what we expect
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank) {
     const bool is_ok = kt_matrix__kCluster__dynamicDisjoint(/*type=*/e_kt_api_dynamicKMeans_rank,
							     self->config.corrMetric_insideClustering,
							     //! ---
							     matrix_input,
							     (matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : NULL,
							     &(self->obj_result_kMean), 
							     &(self->obj_result_kMean_clusterId_columnValue), 
							     //! ----
							     npass,
							     //! ----
							     T_FLOAT_MAX, T_FLOAT_MAX,
							     //! Note: we use [ªbove] instead of [”elow] given the new-added itrnodcution of "config__valueMask__afterSimMatrix" parameter (oekseth, 06. feb. 2017)
							     /* self->opt_kCount__disjointThresholdFilter__scoreRelative_min, */
							     /* self->opt_kCount__disjointThresholdFilter__scoreRelative_max, */
							     /*useRelativeScore_betweenMinMax=*/true,
							     //! ----
							     self->config.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__min,
							     self->config.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__max,
							     //! ----
							     self->config.clusterConfig);
     assert(is_ok); //! ie, what we expect
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid) {
     const bool is_ok = kt_matrix__kCluster__dynamicDisjoint(/*type=*/e_kt_api_dynamicKMeans_medoid,
							     self->config.corrMetric_insideClustering,
							     //! ---
							     matrix_input,
							     (matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : NULL,
							     &(self->obj_result_kMean), 
							     &(self->obj_result_kMean_clusterId_columnValue), 
							     //! ----
							     npass,
							     //! ----
							     T_FLOAT_MAX, T_FLOAT_MAX,
							     //! Note: we use [ªbove] instead of [”elow] given the new-added itrnodcution of "config__valueMask__afterSimMatrix" parameter (oekseth, 06. feb. 2017)
							     /* self->opt_kCount__disjointThresholdFilter__scoreRelative_min, */
							     /* self->opt_kCount__disjointThresholdFilter__scoreRelative_max, */
							     /*useRelativeScore_betweenMinMax=*/true,
							     //! ----
							     self->config.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__min,
							     self->config.opt_kCount__disjointThresholdFilter__cntInDisjointClusterCountRelative__max,
							     //! ----
							     self->config.clusterConfig);
     assert(is_ok); //! ie, what we expect
   } else if(alg_type == e_hpLysis_clusterAlg_kCluster__SOM) {
     uint nclusters_local = (nclusters == UINT_MAX) ? (uint)((t_float)0.5 * (t_float)matrix_input->nrows) : nclusters;
     const uint nxgrid = (self->config.algConfig_SOM__nxgrid != UINT_MAX) ? self->config.algConfig_SOM__nxgrid : macro_max(10*nclusters_local, 0.2 * matrix_input->nrows);
     const uint nygrid = (self->config.algConfig_SOM__nygrid != UINT_MAX) ? self->config.algConfig_SOM__nygrid : nclusters;
     //printf("allocates-SOM with SOM=[%u, %u], at %s:%d\n", nxgrid, nygrid,  __FILE__, __LINE__);
     const bool is_ok = kt_matrix__som(self->config.corrMetric_insideClustering, matrix_input, &(self->obj_result_SOM), nxgrid, nygrid, self->config.algConfig_SOM__initTau, npass, self->config.clusterConfig);
     assert(is_ok); //! ie, what we expect
   } else if(alg_type == e_hpLysis_clusterAlg_HCA_single) {
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     const bool is_ok = kt_matrix__hca(self->config.corrMetric_insideClustering, matrix_input, &(self->obj_result_HCA), e_kt_clusterAlg_hca_type_single, self->config.clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
     __haveApplied__hca = true;
   } else if(alg_type == e_hpLysis_clusterAlg_HCA_max) {
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     const bool is_ok = kt_matrix__hca(self->config.corrMetric_insideClustering, matrix_input, &(self->obj_result_HCA), e_kt_clusterAlg_hca_type_max, self->config.clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
     __haveApplied__hca = true;
   } else if(alg_type == e_hpLysis_clusterAlg_HCA_average) {
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     const bool is_ok = kt_matrix__hca(self->config.corrMetric_insideClustering, matrix_input, &(self->obj_result_HCA), e_kt_clusterAlg_hca_type_average, self->config.clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
     __haveApplied__hca = true;
   } else if(alg_type == e_hpLysis_clusterAlg_HCA_centroid) {
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     const bool is_ok = kt_matrix__hca(self->config.corrMetric_insideClustering, matrix_input, &(self->obj_result_HCA), e_kt_clusterAlg_hca_type_centroid, self->config.clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
   /* } else if(alg_type == e_hpLysis_clusterAlg_HCA_) { */
   /*   const bool is_ok = kt_matrix__hca(self->config.corrMetric_insideClustering, matrix_input, &(self->obj_result_HCA), e_kt_clusterAlg_hca_type_, /\*isTo_transposeMatrix=*\/false); */
   /*   assert(is_ok); //! ie, what we expect */
     __haveApplied__hca = true;
   } else if(alg_type == e_hpLysis_clusterAlg_kruskal_hca) {
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     const s_kt_matrix_t *__matrix_masked = matrix_input; //(matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : matrix_input;
     assert(__matrix_masked); assert(__matrix_masked->nrows); assert(__matrix_masked->ncols);
     //self.config__CCM
     // fprintf(stderr, "nrows=%u, at %s:%d\n", matrix_input->nrows, __FILE__, __LINE__);
     const bool is_ok = kt_matrix__cluster_inputMatrix__kt_clusterAlg_kruskal(__matrix_masked, &(self->obj_result_HCA), NULL, /*ccm-config=*/NULL, /*isAn__adjcencyMatrix=*/true); //e_kt_clusterAlg_hca_type_centroid, self->clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
     __haveApplied__hca = true; //! ie, as we have Not prodived the 'k-means-result-obejct' as inptu to [above].
   } else if(alg_type == e_hpLysis_clusterAlg_kruskal_fixed) {
     //self.config__CCM
     const s_kt_matrix_t *__matrix_masked = (matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : matrix_input;
     assert(__matrix_masked); assert(__matrix_masked->nrows); assert(__matrix_masked->ncols);
     //!
     //! Use a simpler object-format to 'ohld' the matrix:
     s_kt_matrix_base_t matrix_local = initAndReturn__notAllocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix);
     //s_kt_clusterAlg_config__ccm_t obj_local = copyObj__doNotUseCCM__s_kt_clusterAlg_config__ccm_t(&(self->config__CCM), &matrix_local);
     s_kt_clusterAlg_config__ccm_t obj_local = self->config.config__CCM; //copyObj__doNotUseCCM__s_kt_clusterAlg_config__ccm_t(&(self->config__CCM), &matrix_local);
     obj_local.matrix_unFiltered = &matrix_local;
     obj_local.isTo_pply__CCM = false; //! ie, do Not apply the CCM.   
     uint k_clusterCount__min = nclusters;
     if(nclusters == UINT_MAX) {nclusters = 0.5*matrix_input->nrows;}
     else if(nclusters >= matrix_input->nrows) {
       fprintf(stderr, "!!\t Manuyally set the cluster-count = 'nrows/2': Your configuraiton does not follow the minimum-requirements: we the 'nclusters'=%u attribute to be set to less than %u vertices, a case which does Not hold; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  nclusters, matrix_input->nrows, __FUNCTION__, __FILE__, __LINE__);
       k_clusterCount__min = matrix_input->nrows / 2;
     }
     obj_local.k_clusterCount__min = k_clusterCount__min;
     obj_local.k_clusterCount__max = k_clusterCount__min;
     //s_kt_matrix_base_t shallow_matrix = initAndReturn__notAllocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix);
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     const bool is_ok = kt_matrix__cluster_inputMatrix__kt_clusterAlg_kruskal(__matrix_masked, &(self->obj_result_HCA), &(self->obj_result_kMean), /*ccm-config=*/&(obj_local), /*isAn__adjcencyMatrix=*/true); //e_kt_clusterAlg_hca_type_centroid, self->clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
   } else if(alg_type == e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) {     
     //self.config__CCM
     const s_kt_matrix_t *__matrix_masked = (matrix__simAndfiltered.nrows > 0) ? &matrix__simAndfiltered : matrix_input;
     assert(__matrix_masked); assert(__matrix_masked->nrows); assert(__matrix_masked->ncols);
     //!
     //! Use a simpler object-format to 'ohld' the matrix:
     s_kt_matrix_base_t matrix_local = initAndReturn__notAllocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix);
     //s_kt_clusterAlg_config__ccm_t obj_local = copyObj__doNotUseCCM__s_kt_clusterAlg_config__ccm_t(&(self->config__CCM), &matrix_local);
     s_kt_clusterAlg_config__ccm_t obj_local = self->config.config__CCM; //copyObj__doNotUseCCM__s_kt_clusterAlg_config__ccm_t(&(self->config__CCM), &matrix_local);
     obj_local.matrix_unFiltered = &matrix_local;
     obj_local.isTo_pply__CCM = true; //! ie, do Apply the CCM.   
     uint k_clusterCount__min = nclusters;
     if(nclusters >= matrix_input->nrows) {
       if(nclusters != UINT_MAX) {
	 fprintf(stderr, "!!\t Manuyally set the cluster-count = 'nrows/2': Your configuraiton does not follow the minimum-requirements: we the 'nclusters'=%u attribute to be set to less than %u vertices, a case which does Not hold; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  nclusters, matrix_input->nrows, __FUNCTION__, __FILE__, __LINE__);
	 k_clusterCount__min = matrix_input->nrows / 2;
       } else {
	 k_clusterCount__min = 2;
       } 
     }
     obj_local.k_clusterCount__min = k_clusterCount__min;
     obj_local.k_clusterCount__max = k_clusterCount__min;
     free__s_kt_clusterAlg_hca_resultObject_t(&(self->obj_result_HCA)); //! ie, de-allcoat ethe HCA-object, ie, as a 'safe-guard' (oekseth, 06. amr. 2017).
     //s_kt_matrix_base_t shallow_matrix = initAndReturn__notAllocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix);
     const bool is_ok = kt_matrix__cluster_inputMatrix__kt_clusterAlg_kruskal(__matrix_masked, &(self->obj_result_HCA), &(self->obj_result_kMean), /*ccm-config=*/&(obj_local), /*isAn__adjcencyMatrix=*/true); //e_kt_clusterAlg_hca_type_centroid, self->clusterConfig); ///*isTo_transposeMatrix=*/false);
     assert(is_ok); //! ie, what we expect
     assert(self->obj_result_HCA.nrows > 0);
     assert(self->obj_result_HCA.mapOf_nodes != NULL);
   } else if(alg_type == e_hpLysis_clusterAlg_random_best) {     //! hwere we assuem no prior knowledge of clsuter-count. 
     //!
     //! Generla cofnigurations:
     const bool __isTo_selectBest = true; const bool __isTo_useAverage = false;
     //!
     //! Comptue the random-clusters (oekseth, 06. jul. 2017):
     __randomClustering__hpLysis_api(self, matrix_input, __isTo_selectBest, __isTo_useAverage);
   } else if(alg_type == e_hpLysis_clusterAlg_random_worst) {     
     //!
     //! Generla cofnigurations:
     const bool __isTo_selectBest = false; const bool __isTo_useAverage = false;
     //!
     //! Comptue the random-clusters (oekseth, 06. jul. 2017):
     __randomClustering__hpLysis_api(self, matrix_input, __isTo_selectBest, __isTo_useAverage);
   } else if(alg_type == e_hpLysis_clusterAlg_random_average) {     
     //!
     //! Generla cofnigurations:
     const bool __isTo_selectBest = true; const bool __isTo_useAverage = true;
     //!
     //! Comptue the random-clusters (oekseth, 06. jul. 2017):
     __randomClustering__hpLysis_api(self, matrix_input, __isTo_selectBest, __isTo_useAverage);
   } else {
     fprintf(stderr, "!!\t Algorithm-enum Not described: Add support for alg-type=%u, at %s:%d\n", (uint)alg_type, __FILE__, __LINE__);
     assert(false); //! ie, as we then need to 'add support for this'.
   }

   // printf("at %s:%d\n", __FILE__, __LINE__);
   
   if(__haveApplied__hca == true) {
     // printf("at %s:%d\n", __FILE__, __LINE__);
     if( (nclusters != 0) && (nclusters < self->obj_result_HCA.nrows)) {
       // printf("at %s:%d\n", __FILE__, __LINE__);
       self->obj_result_kMean = cuttree__kt_clusterAlg_hca(&(self->obj_result_HCA), nclusters);
     } else {
       //! Then we use a 'fxied parition-strategy' to sub-divide the HCA-tree into clusters  (oekseth, 06.f eb. 2017):
       // printf("at %s:%d\n", __FILE__, __LINE__);
       // if(self->config.config__CCM__dynamicKMeans.isTo_pply__CCM) {
       // printf("at %s:%d\n", __FILE__, __LINE__);
       // printf("apply HCA-to-kMeans-processing, at %s:%d\n", __FILE__, __LINE__);
       const s_kt_matrix_t *matrix = matrix_input; // &(self->matrixResult__inputToClustering);
       assert(matrix);
       assert(matrix->nrows == matrix->ncols); //! ie, as we 'otehriwse' will nee to 'get' a correct data-input-verison' to 'this'.
       {
	 const uint nrows = self->obj_result_HCA.nrows;
	 //fprintf(stderr, "nrows=%u VS matrix=%u, alg_type=\"%s\", at %s:%d\n", nrows, matrix->nrows, get_stringOf__e_hpLysis_clusterAlg_t(alg_type), __FILE__, __LINE__);
	 assert(matrix->nrows == nrows);
       }
       //! Get a 'shallow copy' of the current matrix.
       s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(matrix);
       assert(matrix->nrows == mat_shallow.nrows);
       self->config.config__CCM__dynamicKMeans.matrix_unFiltered = &mat_shallow;
       //! Apply logics:
       const bool is_ok = applyCCMFiltering__hca(self);
       assert(is_ok);
     } 
   }
   //}
// } //! else: then we assume the user is Not interested in
  //!
  //! De-allocate the locally allocated matrix:
   if(obj_1__ != matrix_input) {
    assert(matrix_input->matrix == obj_cpy_ranksEachRow.matrix); //! ie, as we then assume 'they are the same object':
    if(matrix_input->matrix != self->matrixResult__inputToClustering.matrix) {
      free__s_kt_matrix(&obj_cpy_ranksEachRow);
    } //! else we asusem 'the latter is to be used by the caller'.
  }
  if(matrix__simAndfiltered__isAllocated) {free__s_kt_matrix(&matrix__simAndfiltered); matrix__simAndfiltered__isAllocated = false;}
  //! ---------------------------------------
  self->objectHistory__prev__alg_type = alg_type;
  //self->objectHistory__prev__inputMatrix = obj_1__;
  //! ---------------------------------------------
  //! 
  //! Update the input-object witht he parameters used 'before our lcoatioin modifciatons':
  self->config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = beforeCall__inputMatrix__isAnAdjecencyMatrix;
  //self->config.corrMetric_prior_use = __local__corrMetric__prior_use;
  self->config.corrMetric_prior = beforeCall__corrMetric_prior;
  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}


//!   @brief improve k-means-clsutering through idneitficiaotn of intial centroids (oekseth, 06. feb. 2017).
bool cluster__kMeansTwoPass__hpLysis_api(s_hpLysis_api_t *self, const e_hpLysis_clusterAlg_t alg_type, const s_kt_matrix_t *obj_1__, const uint nclusters, const uint npass, const s_hpLysis_api__config_t config__firstPass, const e_hpLysis_clusterAlg_t alg_type_firstPass) {
  assert(self); assert(obj_1__);
  if(!self || !obj_1__ || !obj_1__->nrows || !obj_1__->ncols) {
    fprintf(stderr, "!!\t Your configuraiton does not follow the minimum-requirements: we expect botht eh input-object and input-matrix to contain data; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
    return false;
  }
  //! ------------------------------------------------------------------------------------------------
  if(!isOf_type_HCA__e_hpLysis_clusterAlg_t(alg_type_firstPass)) {
    fprintf(stderr, "!!\t Your configuraiton does not follow the minimum-requirements: please use a different algorithm-type than SOM (as Self ORgnaising MAps (SOMs) are stored in a dedicated result-object); if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
    return false;    
  } else if(!isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(alg_type)) {
    fprintf(stderr, "!!\t Your configuraiton does not follow the minimum-requirements: please use a \"e_hpLysis_clusterAlg_kCluster_\" clsuter-algorithm as the \"alg_type\" input-paremter; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
    return false;    
  }
  /* if( (nclusters == 0) || (nclusters == UINT_MAX) ) { */
  /*   // TODO: cosndier 'then' usign an 'autaotmed proceudr'e to idneityf the 'k-coutn fo clsuters'. */
  /*   fprintf(stderr, "!!(not-supported)\t the \"nclusters\" paremter was Nto set: please request support for this option. In brief your configuraiton does not follow the minimum-requirements: if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__); */
  /*   assert(false); //! ie, an heads-up. */
  /*   return false;     */
  /* } */
  //! ------------------------------------------------------------------------------------------------
  //!
  //! Idneitfy intial clsuters:
  s_hpLysis_api_t obj_firstPass = setToEmpty__s_hpLysis_api_t(NULL, NULL);
  obj_firstPass.config = config__firstPass;//! ie, the lcoal cofngiruation.
  // TODO: consider providng 'explict' fucntion-arguments to set [”elow] to 'other valeus' (oekseth, 06. feb. 2017).
  obj_firstPass.randomConfig.config = self->randomConfig.config;
  obj_firstPass.randomConfig.config_init = self->randomConfig.config_init;
  //obj_firstPass.kdConfig = self->kdConfig; //! ie, the kd-tree-config-object.
  { //! Call the algorithm:
    const bool is_ok = cluster__hpLysis_api(&obj_firstPass, alg_type_firstPass, obj_1__, nclusters, npass);
    if(is_ok == false) {
      free__s_hpLysis_api_t(&obj_firstPass);
      assert(false);
      return false;
    } 
  }  
  //!
  //! Update our "s_kt_randomGenerator_vector_t randomConfig" object:
  //! Note: in this 'appraoch'w e iterate throught eh k-means-result-object and then 'set't he vector to 'defualt values'.
  bool is_exported = false;
  { //! Try: update the 'mappings' wrt. the comptued cluster-memberships:
    const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(obj_firstPass.obj_result_kMean);
    if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
      is_exported = true;
      //! Then Update the mapping:
      if(self->randomConfig.mapOf_columnToRandomScore != NULL) {free_1d_list_uint(&(self->randomConfig.mapOf_columnToRandomScore));}
      const uint empty_0 = 0;
      const uint list_size = obj_result->cnt_vertex;
      self->randomConfig.mapOf_columnToRandomScore = allocate_1d_list_uint(list_size, empty_0);
      //! Copy the enw-cosntructed mappigns:
      for(uint i = 0; i < list_size; i++) {
	uint member_id = obj_result->vertex_clusterId[i];
	//printf("[%u] w/clusterId=%u, at %s:%d\n", i, member_id, __FILE__, __LINE__);
	/* assert(member_id != UINT_MAX); */
	/* assert(member_id < nclusters);	 */
	if(member_id >= nclusters) {member_id = 0;} //! ie, a 'fall-back'
	self->randomConfig.mapOf_columnToRandomScore[i] = member_id;
      }
    }
  }
  if(is_exported == false) {
    // TODO: cosnider adding support for extracting 'clsuter-sets' from the other algorithms 'we supprot' (oekseth, 06. feb. 2017).
    fprintf(stderr, "!!\t There are no cluster-data to export, which either could be due to all vetices being in the same ccluster, or erronous exprot-configurations, eg, where the latter may indicate that your function-API-configuraiton is in-consistent. To introspect upon the latter, while we expected one of the following algrotihsm to have been appleid, none of the follwoing hold result-data: k-means, k-median, k-medoid, disjoint, SOM, HCA. In brief please udpaye your exprot-rotuine and/or your algorithm-call-cofniguraiton. What we esuggest is to first read the API-docuemtantion, and thereafter (ie, if the latter does Not help) contact the senior develoepr at [eosekth@gamil.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return true;
  }    
  //!
  //! De-allcoate result-data wrt. the 'first run':  
  free__s_hpLysis_api_t(&obj_firstPass);
  //! ------------------------------------------------------------------------------
  //!
  //! The 'main run':
  const bool is_ok = cluster__hpLysis_api(self, alg_type, obj_1__, nclusters, npass);  
  //!
  //! De-allcoate and return:
  return is_ok;
}

//! @brief 'extracts' a 'k-means-result-set' using the "ccm_enum" CCM to idneitfy the ideal 'nclsuters' cluster-count (oekseth, 06. mar. 2017).
bool hcaPostStep__dynamicK____hpLysis_api(s_hpLysis_api_t *self, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum) {
  assert(self);
  if(self->obj_result_HCA.nrows == 0) {
    fprintf(stderr, "!!\t The HCA-result-object is Not set: please request an HCA-algorithm-based coomptuation before using this funciton-call. For questions we suggest to read the documetnaiton: if the latter does not help then cotnact the senior developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n",  __FUNCTION__, __FILE__, __LINE__); 
    /*stop-exeuction=*/return false;
  }
  s_kt_matrix_t *matrix = &(self->matrixResult__inputToClustering);
  assert(matrix);
  if(matrix->nrows != matrix->ncols) {
    fprintf(stderr, "!!\t The result-matrix with dimeions=[%u, %u] does Not seem like an ajdency-matrix: please 'set' the matrix 'to be' and ajdency-amtrix, ie, before before using this funciton-call. For questions we suggest to read the documetnaiton: if the latter does not help then cotnact the senior developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", matrix->nrows, matrix->ncols,  __FUNCTION__, __FILE__, __LINE__); 
    /*stop-exeuction=*/return false;
  }
  assert(matrix->nrows == matrix->ncols); //! ie, as we 'otehriwse' will nee to 'get' a correct data-input-verison' to 'this'.
  //!
  //! Updat ehte CCM-cofngiruaiton:
  self->config.config__CCM__dynamicKMeans.ccm_enum = ccm_enum;
  //!
  //! 
  //! Get a 'shallow copy' of the current matrix.
  s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(matrix);
  self->config.config__CCM__dynamicKMeans.matrix_unFiltered = &mat_shallow;
  { //! Valditet ehtat the HCA-data-sets are comptued:
    const uint nrows = self->obj_result_HCA.nrows;
    assert(nrows > 0);
  }
  //! Apply logics:
  const bool is_ok = applyCCMFiltering__hca(self);
  assert(is_ok);  
  //!
  //! @return
  return true;
}


#define __M__matrixIsEmpty(self, warning) ({fprintf(stderr, "!!\t The simliarty-matrix is Not set, though you requested an export-format-type which 'requires' a simlairty-amtrix-format to be computed. %s (Note:  in 'this' confgurtion-object \"corrMetric_prior_use = '%s'.) Differently told, please update your configuration-object (eg, prior to a \"cluster__hpLysis_api(..)\" API-fucntion-call). For questions we suggest to read the documetnaiton: if the latter does not help then cotnact the senior developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", warning, (self->config.corrMetric_prior_use) ? "true" : "false", __FUNCTION__, __FILE__, __LINE__); /*sop exeuction=*/return false;})
#define __M__matrixIsEmpty__floatRetVal(self, warning) ({fprintf(stderr, "!!\t The simliarty-matrix is Not set, though you requested an export-format-type which 'requires' a simlairty-amtrix-format to be computed. %s (Note:  in 'this' confgurtion-object \"corrMetric_prior_use = '%s'.) Differently told, please update your configuration-object (eg, prior to a \"cluster__hpLysis_api(..)\" API-fucntion-call). For questions we suggest to read the documetnaiton: if the latter does not help then cotnact the senior developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", warning, (self->config.corrMetric_prior_use) ? "true" : "false", __FUNCTION__, __FILE__, __LINE__); /*sop exeuction=*/return T_FLOAT_MAX;})

//! Export a resutl-set stored in teh s_hpLysis_api_t object  (oekseth, 06. jan. 2016).
bool export__hpLysis_api(s_hpLysis_api_t *self, const char *stringOf_file, FILE *file_out, const e_hpLysis_export_formatOf_t exportFormat, s_kt_matrix_t *alternativeMatrixToUse) {
  assert(self);
  //! start: generic-configuraiton: ---------------------------------------------------------------
  const char *stringOf_dataLabel = "clusterExport.hpLysis";
  const char *stringOf_id = "hpLysis_api"; //"inputData";
  const char *suffix = "tsv";
  //! -----------------------
  const bool isTo_useMatrixFormat = ( (exportFormat == e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV) || 
				      (exportFormat == e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_TSV) ||
				      (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_TSV) 
				      );
  const bool isTo_inResult_isTo_useJavaScript_syntax = ( 
							(exportFormat == e_hpLysis_export_formatOf_similarity_matrix__syntax_JS)
							|| (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS) 
							 );
  const bool isTo_inResult_isTo_useJSON_syntax = (
						  (exportFormat == e_hpLysis_export_formatOf_similarity_matrix__syntax_JSON)
						  || (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JSON) 
						  );
  //! finish: generic-configuraiton: ---------------------------------------------------------------
  //!
  //! 
  //! Start:file-opening --------------------------------------------------
  s_kt_matrix_t *matrix = &(self->matrixResult__inputToClustering);
  if(alternativeMatrixToUse && alternativeMatrixToUse->matrix) {matrix = alternativeMatrixToUse;}; 
  /* if(!matrix->nrows && !matrix->matrix) { // && (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix)) {       */
  /*   matrix = alternativeMatrixToUse; //self->objectHistory__prev__inputMatrix; */
  /* }       */
  if(!matrix->nrows || !matrix->ncols) {__M__matrixIsEmpty(self, "To summarize, please validate taht your have set \"<object>.corrMetric_prior_use = true\" ");} //! ie, abort.
  FILE *old_handler = NULL;
  //! --- 
  if(stringOf_file && strlen(stringOf_file)) {
    //const bool is_ok = 
    export_config__s_kt_matrix_t(matrix, stringOf_file);
    // assert(is_ok);
    //export__singleCall__s_kt_matrix_t(matrix, stringOf_file, NULL);
    assert(matrix->stream_out != NULL); //! ie, as we expect [ªbov€] to have been impluct set.
  } else {
    if(file_out == NULL) {
      //printf("\t stdout, at %s:%d\n", __FILE__, __LINE__);
      file_out = stdout;
    } //! ie, the dfeulat alternative.
    matrix->stream_out = file_out;
    //printf("\t stream_out=%p, at %s:%d\n", file_out, __FILE__, __LINE__);
    // self->matrixResult__inputToClustering.stream_out = file_out;
  }
  //! finish:file-opening --------------------------------------------------

  if(
     (exportFormat == e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV) 
     || (exportFormat == e_hpLysis_export_formatOf_similarity_matrix__syntax_JS)
     || (exportFormat == e_hpLysis_export_formatOf_similarity_matrix__syntax_JSON)
     || (exportFormat == e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_TSV)
     || (exportFormat == e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_Tex)
     || (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_TSV) 
     || (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS) 
     || (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JSON) 
     // || 
     ) {
    //! Start: -----------------------------------------------------------------------------------------------
    //! Apply lgioics:
    //! Update the object:
    if(exportFormat == e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_TSV) {
      const bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(matrix, stringOf_file, NULL, /*config_isToTranspose__whenGeneratingOutput=*/true, /*exportFormat=*/e_kt_matrix__exportFormats_tsvMatrix, e_kt_matrix__exportFormats___extremeToPrint_undef);
      assert(is_ok);
    } else if(exportFormat == e_hpLysis_export_formatOf_similarity_matrix_summary_deviation__syntax_Tex) {
      const bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(matrix, stringOf_file, NULL, /*config_isToTranspose__whenGeneratingOutput=*/true, /*exportFormat=*/e_kt_matrix__exportFormats_tex, e_kt_matrix__exportFormats___extremeToPrint_undef);
      assert(is_ok);
    } else if(
	      (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_TSV) 
	      || (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JS) 
	      || (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix_JSON) 
	      ) { 
      const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
      if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
	s_kt_matrix_t obj_cpy; setTo_empty__s_kt_matrix_t(&obj_cpy);
	if( (alternativeMatrixToUse != matrix) || (self->config.clusterConfig.inputMatrix__isAnAdjecencyMatrix) ) { //! then we assume an internal matrix has been used, ie, a matrix which we asusme is an adjcnecy-matrix:
	  init__copy__s_kt_matrix(&obj_cpy, matrix, /*isTo_updateNames=*/true);
	} else { //! then we comptue an ajdcneyc-matrix using the input (oekseth, 06. feb. 2017):
	  const bool is_ok = kt_matrix__distancematrix(self->config.corrMetric_prior, matrix, NULL,  &obj_cpy, self->config.clusterConfig);
	  assert(is_ok); //! ie, what we expect.
	}
	//!
	//! Apply filters:
	assert(obj_result->cnt_vertex <= obj_cpy.nrows);
	assert(obj_result->cnt_vertex <= obj_cpy.ncols);
	setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(&obj_cpy, obj_result->vertex_clusterId, obj_result->cnt_vertex, obj_result->vertex_clusterId);

	  //! Export:
	  export_config_extensive_s_kt_matrix_t(&obj_cpy, NULL, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, /*matrix-variable-suffix=*/stringOf_id, 
					      false, //(bool)(stringOf_id != NULL), 
					      /*cnt_dataObjects_toGenerate=*/1); //cnt_objs_toExport); 
	export__s_kt_matrix_t(&obj_cpy);
	//!
	//! De-allocate local object:
	free__s_kt_matrix(&obj_cpy);
      } else {
	fprintf(stderr, "!!\t Your fixed-cluster-object is empty: please valdiate that your used eitehr a k-means algorithm or a disjoitn-algorithm for data-construction. In breif we will now drop the exprort-rotuine, ie, the late latter object is emptuy. For questiosn pelase read the docuemtantion, and if neccessary cotnat the senbior develoepr at [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      }
      
    } else {
      /* printf("\t stream_out=%p, at %s:%d\n", file_out, __FILE__, __LINE__); */
      /* printf("\t stream_out=%p, at %s:%d\n", matrix->stream_out, __FILE__, __LINE__); */
      //printf("\n Exports at %s:%d\n", __FILE__, __LINE__);
      assert(matrix->stream_out != NULL); //! ie, as we expect [ªbov€] to have been impluct set.
      export_config_extensive_s_kt_matrix_t(matrix, NULL, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, /*matrix-variable-suffix=*/stringOf_id, 
					    false, //(bool)(stringOf_id != NULL), 
					    /*cnt_dataObjects_toGenerate=*/1); //cnt_objs_toExport); 
      export__s_kt_matrix_t(matrix);
    }
    //! finish: -----------------------------------------------------------------------------------------------
  } else {
    bool is_exported = false;
    e_kt_clusterAlg_exportFormat_t local_format = e_kt_clusterAlg_exportFormat_vertex_toCentroidIds;
    if(exportFormat == e_hpLysis_export_formatOf_clusterResults_vertex_toClusterIds) {
      local_format = e_kt_clusterAlg_exportFormat_vertex_toClusterIds;
    } else if(exportFormat == e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds) {
      local_format = e_kt_clusterAlg_exportFormat_vertex_toCentroidIds;
    } else if(exportFormat == e_hpLysis_export_formatOf_clusterResults_distanceToCentroids) {
      local_format = e_kt_clusterAlg_exportFormat_distanceToCentroids;
      //} else if(exportFormat == e_hpLysis_export_formatOf_clusterResults_) {      
    } else {
      fprintf(stderr, "!!\t Add support for exportFormat=%u: for euqeisonts please cotnact the senior devleoper at [oesketh@gmail.com]. Observaiton at [%s]:%s:%d\n", exportFormat, __FUNCTION__, __FILE__, __LINE__);
      assert(false); // ie, as heads-up.    
    }
    const char *seperator_cols = "\t";   const char *seperator_newLine = "\n";
    

    { //! Try: "k-means" OR "disjoinit:
      const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
      if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
	is_exported = true;
	//! Then Apply:
	assert(matrix);
	if(obj_result->cnt_vertex == matrix->nrows) {
	  assert(obj_result->cnt_vertex <= matrix->nrows); //! ie, as the "nameOf_rows" would otheriwse be wrong/errnoss to use.
	  const bool is_ok = printHumanReadableResult__extensive__s_kt_clusterAlg_fixed_resultObject_t(obj_result, matrix->nameOf_rows, matrix->stream_out, seperator_cols, seperator_newLine, local_format);
	  assert(is_ok);
	} else {
	  const bool is_ok = printHumanReadableResult__extensive__s_kt_clusterAlg_fixed_resultObject_t(obj_result, NULL, matrix->stream_out, seperator_cols, seperator_newLine, local_format);
	  assert(is_ok);
	}

      }
    }
    { //! Try: "SOM":
      const s_kt_clusterAlg_SOM_resultObject_t *obj_result = &(self->obj_result_SOM);
      if( (obj_result->nrows != 0) && (obj_result->vertex_clusterid != NULL) ) {
	if(obj_result->nrows <= matrix->nrows) { //! ie, as the "nameOf_rows" would otheriwse be wrong/errnoss to use.	
	  assert(obj_result->nrows <= matrix->nrows); //! ie, as the "nameOf_rows" would otheriwse be wrong/errnoss to use.
	  is_exported = true;
	  const bool is_ok = printHumanReadableResult__extensive__s_kt_clusterAlg_SOM_resultObject_t(obj_result, matrix->nameOf_rows, matrix->stream_out, seperator_cols, seperator_newLine, local_format);
	  assert(is_ok);
	} else {
	  is_exported = true;
	  const bool is_ok = printHumanReadableResult__extensive__s_kt_clusterAlg_SOM_resultObject_t(obj_result, NULL, matrix->stream_out, seperator_cols, seperator_newLine, local_format);
	  assert(is_ok);
	}
      }
    }
    { //! Try: "HCA":
      const s_kt_clusterAlg_hca_resultObject_t *obj_result = &(self->obj_result_HCA);
      if( (obj_result->nrows != 0) && (obj_result->mapOf_nodes != NULL) ) {
	assert(obj_result->nrows != UINT_MAX);
	is_exported = true;
	const bool is_ok = printHumanReadableResult__extensive__s_kt_clusterAlg_hca_resultObject_t(obj_result, matrix->nameOf_rows, matrix->stream_out, seperator_cols, seperator_newLine, local_format);
	assert(is_ok);
      }
    }
    if(is_exported == false) {
      fprintf(stderr, "!!\t There are no cluster-data to export, which either could be due to all vetices being in the same ccluster, or erronous exprot-configurations, eg, where the latter may indicate that your function-API-configuraiton is in-consistent. To introspect upon the latter, while we expected one of the following algrotihsm to have been appleid, none of the follwoing hold result-data: k-means, k-median, k-medoid, disjoint, SOM, HCA. In brief please udpaye your exprot-rotuine and/or your algorithm-call-cofniguraiton. What we esuggest is to first read the API-docuemtantion, and thereafter (ie, if the latter does Not help) contact the senior develoepr at [eosekth@gamil.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      return false; //! ie, as the cofnigruation was in-cosnsitent.
    }
  }

  //! --------------------
  //!
  //! Close:
  if(!file_out) {
    closeFileHandler__s_kt_matrix_t(matrix);      
  }
  // if(old_handler != NULL) 
  {
    matrix->stream_out = old_handler;
  }
  //! ------------------------------
  //! At this euxeicotni-point we asusem the oepraiton was a scucesss:
  return true;
}

//! Add the stringss in our "map_indexToName" to oru "arr_names_clustering" (oekseth, 06. arp. 2017).
static void __addStrings__toObj(s_kt_list_1d_string_t *arr_names_clustering, char **map_indexToName, const uint nrows) {
  assert(arr_names_clustering); assert(nrows > 0); // assert(map_indexToName);
  for(uint i = 0; i < nrows; i++) {
    /* //! Set the string-id: */
    if(map_indexToName == NULL) {
      set_stringConst__concat__s_kt_list_1d_string(arr_names_clustering, /*global_index=*/i, "vertex:%d", /*clusterId=*/(int)i);
    } else {
      set_stringConst__s_kt_list_1d_string(arr_names_clustering, /*global_index=*/i, map_indexToName[i]);
    }
  }
}

/**
   @return the set of clusters, ie, if any
   @date 05.01.2019
   @author Ole Kristian Ekseth (oekseth).
   @remarks if the HCA-tree is used (and the HCA has not yet been partioned into a tree), then the "self" object is ubdated with a pariting of HCA tree.
**/
s_kt_list_1d_uint_t getClusters__hpLysis_api(s_hpLysis_api_t *self) {
  assert(self);
  s_kt_list_1d_uint_t clusters = setToEmpty__s_kt_list_1d_uint_t();
  //if(alternativeMatrixToUse && alternativeMatrixToUse->matrix) {matrix = alternativeMatrixToUse;}; 
  { //! Investigate: "k-means":
    const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
    if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
      uint nrows = obj_result->cnt_vertex; uint ncols = 1;
      //! Allocate:
      clusters = init__s_kt_list_1d_uint_t(nrows);
      //! Set cluster-memberships:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	clusters.list[row_id] = obj_result->vertex_clusterId[row_id];
      }
      //!
      //! @return
      return clusters;
    }
  }
  
  { //! Then try extracting the clusters for the HCA-object:
      const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
      if(self->obj_result_HCA.nrows > 0) { //! then we asusme HCA ahs been compteud.
	const uint nrows = self->obj_result_HCA.nrows;
	assert(nrows > 0);
      //      if(!sOf_type_HCA__e_hpLysis_clusterAlg_t(/*alg_type_firstPass=*/)) 	
	if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
	  const bool is_ok = hcaPostStep__dynamicK____hpLysis_api(self, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette);
	  if(is_ok && self->obj_result_kMean.vertex_clusterId) {
	    //! Allocate:
	    clusters = init__s_kt_list_1d_uint_t(nrows);
	    //! Set cluster-memberships:
	    for(uint row_id = 0; row_id < nrows; row_id++) {
	      clusters.list[row_id] = obj_result->vertex_clusterId[row_id];
	    }
	    //!
	    //! @return
	    return clusters;	    
	  }
	}
      }
    }
  //! -----------------------------------------------------------------------------------------------
  //!
  //! Then handle usage-error, and then return:
#if(false) //! then we try being less rude (oesketh, 06. apr. 2018).
  fprintf(stderr, "(empty) \t cluster results are empty: could indicte that the algorithm, used to compute cluster partitions, wre not able to identify any distinct clusters (from your data set). If the latter sounds odd, then please send a request to Dr. Ekseth [oekseth@gmail.com], eg, to get tips for any pre-processing steps of your code. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#else
  if(false) {fprintf(stderr, "!!\t Was unable to load cluster memberships: if this sounds odd, then please contact the senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
  //fprintf(stderr, "!!\t Function is wrongly used: no cluster-result-object is set, ie, please valdiate your usage: if reading docuemtnaiton and tutorials does Not help, we suggest contacting the senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  // assert(false); //! ie, an heads-up:
  #endif
  return clusters; // setToEmptyAndReturn__s_kt_matrix_t();
}

/**
   @brief export the cluster-membershisp assicated to the 'last' comptaution (oesketh, 06. mar. 2017).
   @param <self> is the objec twhich hold the clster-results.
   @return a matrix whihc hold the clsuter-emberships.
   @remarks the contents of teh result will denepdn upon the following cocniditons:
   -- Case(k-clusters-known): export "[vertex] = clusterId"
   -- else:Case(hca-clusters-known): export triplet=(child, parent, score)
   -- else:Case(SOM-clusters-known): export triplet=(vertex, som_x, som_y).
 **/
s_kt_matrix_t export_clusterMemberships__hpLysis_api(s_hpLysis_api_t *self) {
  assert(self);
  s_kt_matrix_t *matrix = &(self->matrixResult__inputToClustering);
  //if(alternativeMatrixToUse && alternativeMatrixToUse->matrix) {matrix = alternativeMatrixToUse;}; 
  char **map_indexToName = (matrix) ? matrix->nameOf_rows : NULL;
  { //! Investigate: "k-means":
    const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
    if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
      uint nrows = obj_result->cnt_vertex; uint ncols = 1;
      //! Allocate:
      s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, ncols);
      //!
      //! Set headers: columns: 
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/0, "cluster_id", /*addFor_column=*/true);
      //!
      //! Set headers: rows: ... and values:
      for(uint i = 0; i < nrows; i++) {
	//! Set the string-id:
	if(map_indexToName == NULL) {
	  allocOnStack__char__sprintf(2000, str_local, "vertex[%u]", i); //! ie, intate a new variable "str_local". 
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, str_local, /*addFor_column=*/false);
	} else {
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, map_indexToName[i], /*addFor_column=*/false);
	}
	//! Set the cluster-ids:
	mat_result.matrix[i][0] = obj_result->vertex_clusterId[i];
      }
      
      //!
      //! @return
      return mat_result;
    }
  }
  { //! Try: "HCA":
    const s_kt_clusterAlg_hca_resultObject_t *obj_result = &(self->obj_result_HCA);
    if( (obj_result->nrows != 0) && (obj_result->mapOf_nodes != NULL) ) {
      assert(obj_result->nrows != UINT_MAX);
      //!
      //! Iterat ethrough the result-object: find number of relationships:
      uint cnt_rels = 0;
      for(uint i = 0; i < obj_result->nrows; i++) {
	Node_t node = obj_result->mapOf_nodes[i];
	const bool has_parent = (node.right != node.left) && (node.right != INT_MAX) && (node.right != (int)UINT_MAX);      
	if(has_parent) {
	  cnt_rels++;
	}
      }
      if(cnt_rels > 0) {
	//! Allocate:
	s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(cnt_rels, /*ncols=*/3);
	//!
	//! Set headers: columns: 
	set_stringConst__s_kt_matrix(&mat_result, /*index=*/0, "head",  /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_result, /*index=*/1, "tail",  /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_result, /*index=*/2, "score", /*addFor_column=*/true);
	//!
	//! Set headers: rows: ... and values:
	uint curr_rel = 0;
	//!
	//! Iterat ethrough the result-object:
	for(uint i = 0; i < obj_result->nrows; i++) {
	  Node_t node = obj_result->mapOf_nodes[i];
	  const bool has_parent = (node.right != node.left) && (node.right != INT_MAX) && (node.right != (int)UINT_MAX);      
	  if(has_parent) {
	    allocOnStack__char__sprintf(2000, str_local, "relation[%u]", i); //! ie, intate a new variable "str_local". 
	    //! Set string:
	    set_stringConst__s_kt_matrix(&mat_result, /*index=*/curr_rel, str_local, /*addFor_column=*/false);
	    //! Set value:
	    mat_result.matrix[curr_rel][0] = node.left;
	    mat_result.matrix[curr_rel][1] = node.right;
	    mat_result.matrix[curr_rel][2] = node.distance;
	    //! Increment:
	    curr_rel++;
	  }
	}	
	//!
	//! @return
	return mat_result;
      } else { //! then 'the topology is empty', ie, do Not write our any relationships:
	// TODO: consider an alternative pparoach ... ie, as 'this current appraoch' may be 'reagarded' as an cinaiton of 'an erro/feature in our proecued're (oekseth, 06. mar. 2017).
	return setToEmptyAndReturn__s_kt_matrix_t();
      }
    }
  }
  { //! Try: "SOM":
    const s_kt_clusterAlg_SOM_resultObject_t *obj_result = &(self->obj_result_SOM);
    if( (obj_result->nrows != 0) && (obj_result->vertex_clusterid != NULL) ) {
      uint nrows = obj_result->nrows; uint ncols = 2;
      //! Allocate:
      s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, ncols);
      //!
      //! Set headers: columns: 
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/0, "SOM(x)", /*addFor_column=*/true);
      set_stringConst__s_kt_matrix(&mat_result, /*index=*/0, "SOM(y)", /*addFor_column=*/true);
      //!
      //! Set headers: rows: ... and values:
      for(uint i = 0; i < nrows; i++) {
	//! Set the string-id:
	if(map_indexToName == NULL) {
	  allocOnStack__char__sprintf(2000, str_local, "vertex[%u]", i); //! ie, intate a new variable "str_local". 
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, str_local, /*addFor_column=*/false);
	} else {
	  set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, map_indexToName[i], /*addFor_column=*/false);
	}
	//! Set the cluster-ids "som(x, y):
	mat_result.matrix[i][0] = obj_result->vertex_clusterid[i][0];
	mat_result.matrix[i][1] = obj_result->vertex_clusterid[i][1];
      }
      
      //!
      //! @return
      return mat_result;
    }
  }
  //! -----------------------------------------------------------------------------------------------
  //!
  //! Then handle usage-error, and then return:
#if(true) //! then we try being less rude (oesketh, 06. apr. 2018).
  fprintf(stderr, "(empty) \t cluster results are empty: could indicte that the algorithm, used to compute cluster partitions, wre not able to identify any distinct clusters (from your data set). If the latter sounds odd, then please send a request to Dr. Ekseth [oekseth@gmail.com], eg, to get tips for any pre-processing steps of your code. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#else
  fprintf(stderr, "!!\t Function is wrongly used: no cluster-result-object is set, ie, please valdiate your usage: if reading docuemtnaiton and tutorials does Not help, we suggest contacting the senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  assert(false); //! ie, an heads-up:
  #endif
  return setToEmptyAndReturn__s_kt_matrix_t();
}



s_kt_clusterResult_condensed_t export__clusterAttributes__s_kt_clusterResult_condensed_t(s_hpLysis_api_t *self) {
  assert(self);
  s_kt_clusterResult_condensed_t obj_clusterResult = initAndReturn__s_kt_clusterResult_condensed_t();
  //! ------------------------
  //!
  //!

  //  printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

  s_kt_matrix_t *matrix = &(self->matrixResult__inputToClustering);
  //if(alternativeMatrixToUse && alternativeMatrixToUse->matrix) {matrix = alternativeMatrixToUse;}; 
  char **map_indexToName = (matrix) ? matrix->nameOf_rows : NULL;
  { //! Try: "HCA":
    const s_kt_clusterAlg_hca_resultObject_t *obj_result = &(self->obj_result_HCA);

    // assert(false); // FIXME: update [”elow] ... and use results stored in our 'k-means-appraoch' wrt. applicaiton of CCMs (to dynamcially splti clusterS) and wrt. the HCA-appraoch

    if( (obj_result->nrows != 0) && (obj_result->mapOf_nodes != NULL) ) {
      uint nrows = obj_result->nrows; // uint ncols = 1;
      assert(obj_result->nrows != UINT_MAX);
      //  printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      //!
      //! Iterat ethrough the result-object: find number of relationships:
      //uint cnt_rels = 0; 
      uint max_cntClusters = 0;
      for(uint i = 0; i < obj_result->nrows; i++) {
	Node_t node = obj_result->mapOf_nodes[i];
	//const bool has_parent = (node.right != node.left) && (node.right != INT_MAX) && (node.right != (int)UINT_MAX);      
	if(node.right < 0) {
	  const uint val_inv = (uint)(int)(-1)*node.right;
	  if(val_inv > max_cntClusters) {max_cntClusters = macro_max(max_cntClusters, val_inv);}
	} //! else hte ndoe is a real-world node.
	if(node.left < 0) {
	  const uint val_inv = (uint)(int)(-1)*node.left;
	  if(val_inv > max_cntClusters) {max_cntClusters = macro_max(max_cntClusters, val_inv);}
	} //! else hte ndoe is a real-world node.
	// if (has_parent) { 	  cnt_rels++; 	}
	// printf("[%u]\tpair: %d --> %d, at %s:%d\n", i, node.left, node.right, __FILE__, __LINE__);
      }
      const s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMean = &(self->obj_result_kMean);
      const bool isTo_useKMeans__forClusterMembershipId = true; // TODO: itnrospect upon the correctness using the altter as a 'defualt appraoch'.
      if( (obj_result_kMean->cnt_vertex != 0) && (obj_result_kMean->vertex_clusterId != NULL) ) {
	if(isTo_useKMeans__forClusterMembershipId == false) {obj_result_kMean = NULL;}
	; } else {
	obj_result_kMean = NULL; //! ie, as we then assuem altter is Not set. 
      }
      
      uint max_cntClusters_kMeans = 0;
      if(obj_result_kMean != NULL) {
	for(uint i = 0; i < nrows; i++) { 
	  const uint cluster_id = obj_result_kMean->vertex_clusterId[i];
	  if(cluster_id != UINT_MAX) {
	    max_cntClusters_kMeans = macro_max(max_cntClusters_kMeans, cluster_id);
	  }
	}
      }
      {
	//! 
	//! Allocate space of the object:
	assert(nrows > 0);	
	max_cntClusters++; max_cntClusters_kMeans++;
	const uint cnt_verticesTotal = nrows + max_cntClusters + max_cntClusters_kMeans;
	obj_clusterResult = init__s_kt_clusterResult_condensed_t(nrows, cnt_verticesTotal);

	//! 
	//! Set the cluster-ids:
	uint current_pos = 0;
	for(uint i = 0; i < max_cntClusters; i++) {
	  //  assert(cnt_vertex < (int)INT_MAX); //! ie, as we inn [”elow] use 'int' instead of the 'uint' data-type.
	  set_stringConst__concat__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/nrows + current_pos, "cluster:%d", /*clusterId=*/(int)i);
	  //! Increment: 
	  current_pos++;
	}
	if(obj_result_kMean != NULL) {
	  //! 
	  //! Set the cluster-ids:
	  for(uint i = 0; i < max_cntClusters_kMeans; i++) {
	    //assert(cnt_vertex < (int)INT_MAX); //! ie, as we inn [”elow] use 'int' instead of the 'uint' data-type.
	    set_stringConst__concat__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/nrows + current_pos, "clusterFixed:%d", /*clusterId=*/(int)i);
	    //! Increment: 
	    current_pos++;
	  }
	}
	//! -----------
	//! 
	//! Generate data-knowledge-overview:
	const t_float default_value__ = 0;
	const bool isTo_addInternalNodesIn__mapAttributeSet = true;
	const uint nrows_local = (isTo_addInternalNodesIn__mapAttributeSet) ? nrows*10 : nrows; //! where "nrows*3" is an overstiamtion, ie, to 'be on the safe side' wrt. memory-allocaitons.
	t_float *mapOf_1d_distanceToRoot_forMetric__nodeCount = allocate_1d_list_float(nrows_local, default_value__);
	t_float *mapOf_1d_distanceToRoot_forMetric__linkScoreSum = allocate_1d_list_float(nrows_local, default_value__);
	const bool is_ok = constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca(obj_result, /*matrix_result=*/NULL, mapOf_1d_distanceToRoot_forMetric__nodeCount, mapOf_1d_distanceToRoot_forMetric__linkScoreSum, /*norws=*/UINT_MAX, /*mapOf_localIdTo_globalId=*/NULL, isTo_addInternalNodesIn__mapAttributeSet);
	assert(is_ok);
	/* { */
	/*   uint cnt_ele = 0; get_count__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), &cnt_ele); */
	/*   printf("cnt_ele=%u, at %s:%d\n", cnt_ele, __FILE__, __LINE__); */
	/* } */
	//! 
	//! Add the stringss in our "map_indexToName" to oru "arr_names_clustering" (oekseth, 06. arp. 2017).
	__addStrings__toObj(&(obj_clusterResult.arr_names_clustering), map_indexToName, nrows);

	if(isTo_addInternalNodesIn__mapAttributeSet) {
	  //! 
	  //! Set the 'defualt' distance-count to the root:
	  assert(nrows <= cnt_verticesTotal);
	  for(uint i = 0; i < cnt_verticesTotal; i++) {	  
	    const uint head = i;
	    const t_float root_distance = mapOf_1d_distanceToRoot_forMetric__linkScoreSum[head];
	    if(root_distance != T_FLOAT_MAX) {
	      set_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_hcaResult_distanceFromRoot), head, /*score=*/root_distance);
	    }
	    const t_float root_count    = mapOf_1d_distanceToRoot_forMetric__nodeCount[head];
	    if(root_count != T_FLOAT_MAX) { //! then we update wrt. 
	      increment_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), head, /*score=*/root_count);
	    }
	    //	    printf("vertex[%u] has root_count=%f, and root_distance=%f,  at %s:%d\n", head, root_count, root_distance, __FILE__, __LINE__);
	  }
	}

	//! 
	//!
	//! Iterate through the result-object: 
	for(uint i = 0; i < obj_result->nrows; i++) {
	  Node_t node = obj_result->mapOf_nodes[i];
	  const bool has_parent = (node.right != node.left) && (node.right != INT_MAX); //&& (node.right != (int)UINT_MAX);      
	  // printf("[%u]\tpair: %d --> %d, has_parent='%s', at %s:%d\n", i, node.left, node.right, (has_parent) ? "true" : "false", __FILE__, __LINE__);
	    uint head = UINT_MAX; uint tail = UINT_MAX;
	    if(node.left < 0) {head = (uint)((int)nrows -1 + (-1)*node.left);} //! where '-1' is used as the 'count of imaginary' starts at '-1', ie, to avoid an arrouns index.
	    else {head = (uint)node.left;}	    
	    if(node.right < 0) {tail = (uint)((int)nrows -1 + (-1)*node.right);} //! where '-1' is used as the 'count of imaginary' starts at '-1', ie, to avoid an arrouns index.
	    else {tail = (uint)node.right;}	    
	    assert(head != UINT_MAX);
	    assert(head < cnt_verticesTotal);
	    assert(tail != UINT_MAX);
	    assert(tail < cnt_verticesTotal);

	  if(has_parent) {
	  //if(node.right != node.left)  {
	    //! Add the relationship: 
	    if(node.left >= 0) {
	      // printf("pair: %u --> %u, at %s:%d\n", head, tail, __FILE__, __LINE__);
	      add_scalar__allVars__s_kt_list_1d_pairFloat_t(&(obj_clusterResult.arrOf_clusterRelationships), head, tail, /*score=*/node.distance);
	      uint cluster_id = tail;
	      if(obj_result_kMean != NULL) {
		cluster_id = nrows + max_cntClusters + obj_result_kMean->vertex_clusterId[head];
	      } 
	      if(cluster_id != UINT_MAX) {
		//! Set the 'direct mapping', ie, similar to [ªbove], where lgocis is inlcuded iot. 'simplify a merge with the HCA-logics'.
		printf("(arr_vertexTo_centroid)\t %u-->%u, at %s:%d\n", head, cluster_id, __FILE__, __LINE__);
		set_scalar__s_kt_list_1d_uint_t(&(obj_clusterResult.arr_vertexTo_centroid), head, /*value=*/cluster_id);
		// allocOnStack__char__sprintf(2000, str_local, "relation[%u]", i); //! ie, intate a new variable "str_local". 
	      } else {
		set_scalar__s_kt_list_1d_uint_t(&(obj_clusterResult.arr_vertexTo_centroid), head, /*value=*/head); //! ie, as we then asusme the head-vertex 'is its own center'
	      }
	    } else {//! else we assuem the vertex 'is an imaginary vertex', ie, for whcih we udpate the tree-based strucutre:
	      // TODO: validate correnctess/descirptivness of [”elow] udpate:
	      // printf("img-pair: %u --> %u, at %s:%d\n", head, tail, __FILE__, __LINE__);
	      relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(&obj_clusterResult, head, tail, /*score=*/1);
	    }
	  }
	  if( (has_parent && (node.left >= 0) ) || isTo_useKMeans__forClusterMembershipId ) {
	    //!
	    //! Update our results wrt. 'total-count' (to childrne and parents) and 'root-distance'.
	    //! Note: Increment the 'root-node-count', ie, set the 'depth to the root-node': 	  
	    const t_float root_count    = mapOf_1d_distanceToRoot_forMetric__nodeCount[tail];
	    if(root_count != T_FLOAT_MAX) { //! then we update wrt. 
	      //! Increment the parent-count:
	      //increment_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), head, /*score=*/root_count);
	      increment_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_parent_countOfScoreSum), head, /*score=*/root_count);
	    }
	  }
	}	
	//!
	//!
	//! De-allcoate ª[bvoe]:
	free_1d_list_float(&mapOf_1d_distanceToRoot_forMetric__nodeCount); mapOf_1d_distanceToRoot_forMetric__nodeCount = NULL;
	free_1d_list_float(&mapOf_1d_distanceToRoot_forMetric__linkScoreSum); mapOf_1d_distanceToRoot_forMetric__linkScoreSum = NULL;
	//!
	//! @return
	return obj_clusterResult;
      } 
    }/*  else { //! then 'the topology is empty', ie, do Not write our any relationships: */
    /*   // TODO: consider an alternative pparoach ... ie, as 'this current appraoch' may be 'reagarded' as an cinaiton of 'an erro/feature in our proecued're (oekseth, 06. mar. 2017). */
    /*   return obj_clusterResult; //setToEmptyAndReturn__s_kt_matrix_t(); */
    /* } */
  }
  { //! Investigate: "k-means":    
    const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
    // printf("try-export-k-means w/cnt_vertex=%u, at %s:%d\n", obj_result->cnt_vertex, __FILE__, __LINE__);
    if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
      uint nrows = obj_result->cnt_vertex; uint ncols = 1;
      uint max_cntClusters = 0;       
      for(uint i = 0; i < nrows; i++) { 
	const uint cluster_id = obj_result->vertex_clusterId[i];
	if(cluster_id != UINT_MAX) {
	  max_cntClusters = macro_max(max_cntClusters, cluster_id);
	}
      }
      // printf("max_cntClusters=%u, at %s:%d\n", max_cntClusters, __FILE__, __LINE__);
      if(max_cntClusters == 0) {return obj_clusterResult;} //! ie, as no clusetreres were then allcoated.
      max_cntClusters++;
      //! 
      //! Allocate space of the object:
      assert(nrows > 0);
      obj_clusterResult = init__s_kt_clusterResult_condensed_t(nrows, max_cntClusters);
      //! 
      //! Set the cluster-ids:
      for(uint i = 0; i < max_cntClusters; i++) {
	//assert(cnt_vertex < (int)INT_MAX); //! ie, as we inn [”elow] use 'int' instead of the 'uint' data-type.
	// printf("set-cluster-id-at[%u], at %s:%d\n", i, __FILE__, __LINE__);
	set_stringConst__concat__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/nrows  + i, "cluster:%d", /*clusterId=*/(int)i);
      }
      { //! Validate that the 'current-pos' is correctly set:
	uint cnt_ele = 0; get_count__s_kt_list_1d_string_t(&(obj_clusterResult.arr_names_clustering), &cnt_ele);
	assert(cnt_ele == (nrows + max_cntClusters));

      }
      //!
      //! Allocate:
      //s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, ncols);
      //!
      //! Set headers: columns: 
      //set_stringConst__s_kt_matrix(&mat_result, /*index=*/0, "cluster_id", /*addFor_column=*/true);
      //!
      //! Set headers: rows: ... and values:
      
      //! Add the stringss in our "map_indexToName" to oru "arr_names_clustering" (oekseth, 06. arp. 2017).
      __addStrings__toObj(&(obj_clusterResult.arr_names_clustering), map_indexToName, nrows);

      //!
      //! Update the clsuter-relationships:
      for(uint i = 0; i < nrows; i++) {
	uint cluster_id = obj_result->vertex_clusterId[i];
	if(cluster_id == UINT_MAX) {cluster_id = i;} //! ie, as we then assume the cluster-id is the vertex itself.
	//! Set the cluster-ids:
	const uint head = i;
	//! Relate the head to the tail, and udpat ehte inverse relationship-mapping (oekseth, 06. apr. 2017):
	//	printf("\t cluster: %u-->%u, at %s:%d\n", head, cluster_id, __FILE__, __LINE__);
	relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(&obj_clusterResult, head, cluster_id, /*score=*/1);
      }
      
      //!
      //! @return
      return obj_clusterResult;
    }
  }
  { //! Try: "SOM":
    //  printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    const s_kt_clusterAlg_SOM_resultObject_t *obj_result = &(self->obj_result_SOM);
    if( (obj_result->nrows != 0) && (obj_result->vertex_clusterid != NULL) ) {
      uint nrows = obj_result->nrows; 
      // uint ncols = 2;
      uint max_cntClusters = 0;              uint max_cntClusters__y = 0;       
      for(uint i = 0; i < nrows; i++) { 
	{ //! For 'x':
	  const uint cluster_id = obj_result->vertex_clusterid[i][0];
	  if(cluster_id != UINT_MAX) {
	    max_cntClusters = macro_max(max_cntClusters, cluster_id);
	  }
	}
	{ //! For 'y':
	  const uint cluster_id = obj_result->vertex_clusterid[i][1];
	  if(cluster_id != UINT_MAX) {
	    max_cntClusters__y = macro_max(max_cntClusters__y, cluster_id);
	  }
	}
      }
      max_cntClusters++;       max_cntClusters__y++;
      uint cnt_clust_matrix = (max_cntClusters * max_cntClusters__y);
      uint cntClusters_total = cnt_clust_matrix;
      //      printf("cntClusters_total=%u, at [%s]:%s:%d\n", cntClusters_total, __FUNCTION__, __FILE__, __LINE__);
      if(cntClusters_total == 1) {return obj_clusterResult;} //! ie, as no clusetreres were then allcoated.
      cntClusters_total += (max_cntClusters + max_cntClusters__y); //! ie, givne the 'assumed' inseriton-process in [below].
      //! 
      //! Allocate space of the object:
      assert(nrows > 0);
      const uint cnt_verticesTotal = nrows + cntClusters_total;
      obj_clusterResult = init__s_kt_clusterResult_condensed_t(nrows, cntClusters_total);
      //! 
      //! Set the cluster-ids:
      uint pos_cluster = nrows;
      //! For the x-direction:
      for(uint i = 0; i < /*x=*/max_cntClusters; i++) {
	//assert(cnt_vertex < (int)INT_MAX); //! ie, as we inn [”elow] use 'int' instead of the 'uint' data-type.
	assert(pos_cluster < cnt_verticesTotal);
	set_stringConst__concat__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/pos_cluster, "som(%d, .)", /*clusterId=*/(int)i);
	//! Set the distance to the root: 
	set_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), /*head=*/pos_cluster, /*score=*/0);
	//! Increment: 
	pos_cluster++;
      }
      //! For the y-direction:
      for(uint i = 0; i < /*y=*/max_cntClusters__y; i++) {
	//assert(cnt_vertex < (int)INT_MAX); //! ie, as we inn [”elow] use 'int' instead of the 'uint' data-type.
	assert(pos_cluster < cnt_verticesTotal);
	set_stringConst__concat__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/pos_cluster, "som(. , %d)", /*clusterId=*/(int)i);
	//! Set the distance to the root: 
	set_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), /*head=*/pos_cluster, /*score=*/0);
	//! Increment:
	pos_cluster++;
      }
      const uint index_startPos = pos_cluster;
#define __MiF__pair_toIndex(_x, _y) ({const uint new_index = index_startPos + (_x*max_cntClusters__y) + _y; new_index;}) //! ie, return the udpated index.
      //! For the (x, y)-direction:      
      for(uint i = 0; i < /*|(x, ..)|*/max_cntClusters; i++) {
	for(uint y = 0; y < /*|(.., y)|*/max_cntClusters__y; y++) {
	  //  assert(cnt_vertex < (int)INT_MAX); //! ie, as we inn [”elow] use 'int' instead of the 'uint' data-type.
	  assert(pos_cluster < cnt_verticesTotal);
	  allocOnStack__char__sprintf(2000, str_local, "som(%u, %u)", i, y); //! ie, intate a new variable "str_local". 
	  set_stringConst__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/pos_cluster, str_local);
	  //! Relate the [ªbove] string-id to both "som(x, .)" and "som(., y)", where where the latter is our 'idea' wrt. viauzliation of SOM-relationshisp in a data-tree-INFOVIS-approach. 	  
	  //! Note: in below we associated the head to the tail, and udpat ehte inverse relationship-mapping (oekseth, 06. apr. 2017):
	  relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(&obj_clusterResult, pos_cluster, /*tail=*/i, /*score=*/1);
	  relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(&obj_clusterResult, pos_cluster, /*tail=*/y, /*score=*/1);
	  //! Set the distance to the root: 
	  set_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), /*head=*/pos_cluster, /*score=*/1);
	  //! Validate our back-track-appraoch:
	  const uint index_som = __MiF__pair_toIndex(i, y);
	  assert(pos_cluster == index_som); //! ie, to validate correctness of [below] 'vertex-som(x, y)' mapping-procedure:
	  //! Increment:
	  pos_cluster++;
	}
      }
      
      //!
      //! Set headers: rows: ... and values:
      for(uint i = 0; i < nrows; i++) {
	//! Set the string-id:
	if(map_indexToName == NULL) {
	  set_stringConst__concat__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/i, "vertex:%d", /*clusterId=*/(int)i);
	} else {
	  set_stringConst__s_kt_list_1d_string(&(obj_clusterResult.arr_names_clustering), /*global_index=*/i, map_indexToName[i]);
	}
	/* if(map_indexToName == NULL) { */
	/*   allocOnStack__char__sprintf(2000, str_local, "vertex[%u]", i); //! ie, intate a new variable "str_local".  */
	/*   set_stringConst__s_kt_matrix(&mat_result, /\*index=*\/i, str_local, /\*addFor_column=*\/false); */
	/* } else { */
	/*   set_stringConst__s_kt_matrix(&mat_result, /\*index=*\/i, map_indexToName[i], /\*addFor_column=*\/false); */
	/* } */
	//! Set the cluster-ids "som(x, y):
	const uint clust_x = obj_result->vertex_clusterid[i][0];
	const uint clust_y = obj_result->vertex_clusterid[i][1];
	//! Gethte 'glboal mapping-score':
	const uint index_som = __MiF__pair_toIndex(clust_x, clust_y);
	//! Note: in below we associated the head to the tail, and udpat ehte inverse relationship-mapping (oekseth, 06. apr. 2017):
	const uint head = i;
	relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(&obj_clusterResult, head, /*tail=*/index_som, /*score=*/1);
	//! Increment the 'root-node-count':
	increment_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), clust_y, /*score=*/1);
	increment_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_parent_countOfScoreSum), clust_x, /*score=*/1);
	//! Set the distance to the root: 
	set_scalar__s_kt_list_1d_float_t(&(obj_clusterResult.arr_clusterSets_child_countOfScoreSum), /*head=*/head, /*score=*/2);	//! ie, as we expect "vertex-->som(x, y)-->[som(x, .), som(., y)]"
      }
      
      //!
      //! @return
      return obj_clusterResult;
    }
  }



  //! -----------------------------------------------------------------------------------------------
  //!
  //! Then handle usage-error, and then return:
  fprintf(stderr, "!!\t Function is wrongly used: no cluster-result-object is set, ie, please valdiate your usage: if reading docuemtnaiton and tutorials does Not help, we suggest contacting the senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  assert(false); //! ie, an heads-up:
  //! ------------------------
  //!
  //! @return
  return obj_clusterResult;
}

//! Identifies a scalar describing the cosnsitency in clusters versus an input-matrix (oekseth, 06. feb. 2017).
t_float scalar_CCMclusterSimilarity__s_hpLysis_api_t(s_hpLysis_api_t *self, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse, const uint *alt_mapOf_clusterIds, s_kt_matrix_t *alternativeMatrixToUse) {
  s_kt_matrix_t *matrix = &(self->matrixResult__inputToClustering);
  if(alternativeMatrixToUse && alternativeMatrixToUse->matrix) {matrix = alternativeMatrixToUse;}; 
  /* if(!matrix->nrows && !matrix->matrix) { // && (exportFormat == e_hpLysis_export_formatOf_clusterResults_maskedSimliarityMatrix)) {       */
  /*   matrix = alternativeMatrixToUse; //self->objectHistory__prev__inputMatrix; */
  /* }       */
  if(!matrix->nrows || !matrix->ncols) {__M__matrixIsEmpty__floatRetVal(self, "To summarize, please validate taht your have set \"<object>.corrMetric_prior_use = true\" ");} //! ie, abort.
  const uint *mapOf_vertexToCentroid1 = alt_mapOf_clusterIds;
  if(alt_mapOf_clusterIds == NULL) {
    bool is_exported = false;
    { //! Try: "k-means" OR "disjoinit:
      const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
      if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
	is_exported = true;
	//! Then Apply:
	mapOf_vertexToCentroid1 = obj_result->vertex_clusterId;
      }
    }
    if(is_exported == false) {
      const s_kt_clusterAlg_fixed_resultObject_t *obj_result = &(self->obj_result_kMean);
      mapOf_vertexToCentroid1 = self->obj_result_kMean.vertex_clusterId; //! ie, a default assugnment.
      if(self->obj_result_HCA.nrows > 0) { //! then we asusme HCA ahs been compteud.
	const uint nrows = self->obj_result_HCA.nrows;
	assert(nrows > 0);
      //      if(!sOf_type_HCA__e_hpLysis_clusterAlg_t(/*alg_type_firstPass=*/)) 	
	if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
	  const bool is_ok = hcaPostStep__dynamicK____hpLysis_api(self, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette);
	  if(is_ok && self->obj_result_kMean.vertex_clusterId) {
	    is_exported = true;
	    //! Then Apply:
	    mapOf_vertexToCentroid1 = self->obj_result_kMean.vertex_clusterId;
	  }
	}
      }
    }
    if(is_exported == false) {
      // TODO: cosnider adding support for extracting 'clsuter-sets' from the other algorithms 'we supprot' (oekseth, 06. feb. 2017).
#ifndef NDEBUG
      if(false) {
	fprintf(stderr, "!!\t(cluster-result-empty)\t\t There are no cluster-data to load (for a matrix with [%u, %u] elements, and where aglorithm=\"%s\" has been applied), which either could be due to all vetices 'being in the same cluster' (ie, an 'inconclusive cluster'), or errnrorus exprot-cofnigruations, eg, where the latter may indicate that your function-API-configuraiton is in-consistent. To introspect upon the latter, while we expected one of the following algrotihsm to have been appleid, none of the follwoing hold result-data: k-means, k-median, k-medoid, disjoint, SOM, HCA. In brief please udpaye your exprot-rotuine and/or your algorithm-call-cofniguraiton. What we esuggest is to first read the API-docuemtantion, and thereafter (ie, if the latter does Not help) contact the senior develoepr at [eosekth@gamil.com]. Observaiton at [%s]:%s:%d\n", matrix->nrows, matrix->ncols, 
		get_stringOf__e_hpLysis_clusterAlg_t(self->objectHistory__prev__alg_type),
		__FUNCTION__, __FILE__, __LINE__);
      }
#else //! then we assume the cluster-algorithm did Not dientify any clsuter-results, ie, Noi need for a warning (oekseth, 06. jul. 2017).
#endif
      // assert(false); // FIXME: remove
      return T_FLOAT_MAX; //! ie, as the cofnigruation was in-cosnsitent.
    }    
  }
  //!
  //! Identify the max-clsuter-count:
  uint k_clusterCount = 0;
  { //! Fidn the max-cluster-count, and print warnigns if input is not cosnsitent:
    uint cnt_warnings = 0;
    for(uint i = 0; i < matrix->nrows; i++) {
      if(mapOf_vertexToCentroid1[i] != UINT_MAX) {
	k_clusterCount = macro_max(mapOf_vertexToCentroid1[i], k_clusterCount);	
      } else if(cnt_warnings < 10) { //! where the result 'of this' may be due to the 'fact' that the cluster-id is Not found.
#if(0 == 1)
	fprintf(stderr, "!!\t Seems like the cluster-membershiop is Not set: we expect teh clsuter-emberships to be set for all the vertices to evlauate, ie, please udpate your data-input. However, if the latter does not help, then pelase contact the senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#endif
	cnt_warnings++;
      }
    }
  }
  k_clusterCount++;

  //! 
  //! Update the result-matrix wrt. the "config__ccmMatrixBased__toUse" parameter:
  s_kt_matrix_cmpCluster_clusterDistance_config_t config_local  = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
  s_kt_matrix_cmpCluster_clusterDistance_t obj_local = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  //assert(obj_dataConfig.k_clusterCount != 0); assert(obj_dataConfig.k_clusterCount != UINT_MAX);
  if(matrix->nrows == matrix->ncols) {
    // printf("k_clusterCount=%u, at %s:%d\n", k_clusterCount, __FILE__, __LINE__);
    init__s_kt_matrix_cmpCluster_clusterDistance(&obj_local, mapOf_vertexToCentroid1, matrix->nrows, matrix->ncols, k_clusterCount, matrix->matrix, config_local);
    t_float score = T_FLOAT_MAX;
    if( (obj_local.cnt_cluster != UINT_MAX) && (obj_local.cnt_cluster > 1) ) {  //! then the data-set is Not inferred being empty (oekseth, 06. jul. 2017):
      assert(obj_local.cnt_cluster == k_clusterCount); //! ie, what we expect.
      //!
      //! Compute: 
      score = compute__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local, config__ccmMatrixBased__toUse, /*cmp_type=*/e_kt_matrix_cmpCluster_clusterDistance__config_metric__default);   
      // printf("score=%f, at %s:%d\n", score, __FILE__, __LINE__);
    }
    //!
    //! De-allcoate:
    free__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local);		  
    
    
    //!
    //! @return:
    return score;
  } else {
    fprintf(stderr, "!!\t The inptu-amtrix is Not symmetirc, ie, please provide a symmetitrc iniput-matrix. For quesitons  pelase cotnact the senior delvoeper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return T_FLOAT_MAX;
  }
}

//! Identify the optmal number of clsuters (and the 'optmality-factor') (oekseth, 06. mar. 2017).
bool standAlone__scalar_CCMclusterSimilarity__s_hpLysis_api_t(const s_kt_matrix_t *obj_1, const bool isAn__adjcencyMatrix, e_kt_correlationFunction_t metric_id,  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse, e_hpLysis_clusterAlg_t clusterAlg, t_float *scalarResult_ccmScore, uint *scalarResult_ncluster) {
  assert(obj_1); assert(obj_1->ncols); assert(obj_1->nrows);
  if(!obj_1 || !obj_1->ncols || !obj_1->nrows || !scalarResult_ccmScore || !scalarResult_ncluster) {return false;} //! ie, as the input-data was Not correctly set.
  //!
  //! Handle 'non-set-valeus':
  if(metric_id == e_kt_correlationFunction_undef) {metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;}
  if(config__ccmMatrixBased__toUse == e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef) {config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;} //! where latter is observed to be 'a close match' to our differnet gold-stadnard-data-evlauation-cases'.
  if(clusterAlg == e_hpLysis_clusterAlg_undef) {clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;}
  //!
  //! Allocate object:
  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = isAn__adjcencyMatrix; 
  obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = config__ccmMatrixBased__toUse;
  obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
  obj_hp.config.corrMetric_prior.metric_id = metric_id;
  obj_hp.config.corrMetric_prior_use = true;
#ifndef NDEBUG
  { //! Validate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
    const s_kt_matrix_t *matrix_input_1 = obj_1;
    assert(matrix_input_1);
    assert(matrix_input_1->nrows);
    assert(matrix_input_1->ncols);
    long long int cnt_interesting = 0;
    for(uint i = 0; i < matrix_input_1->nrows; i++) {
      for(uint j = 0; j < matrix_input_1->ncols; j++) {
	cnt_interesting += isOf_interest(matrix_input_1->matrix[i][j]); } }
    assert(cnt_interesting > 0); //! ie, as we otherwse have a poitnless input to the clsutering
  }  
#endif
  //! 
  //! Apply logics:
  const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, obj_1, /*nclusters=*/UINT_MAX, /*npass=*/100);
  assert(is_ok);
  //!
  //! Get the result-aprameters 'of our itnerest':
  *scalarResult_ncluster = obj_hp.dynamicKMeans__best__nClusters;
  *scalarResult_ccmScore = obj_hp.dynamicKMeans__best__score;
  //! 
  //! De-allcoate object, and return:
  free__s_hpLysis_api_t(&obj_hp);
  return true;
}
