#ifndef scc_tarjan_vertex_h
#define scc_tarjan_vertex_h
/**
   @file
   @brief Identify clusters of SCCs using the algorithm of Tarjan.
   @ingroup graph
   @author Ole Kristian Ekseth (oekseth)
**/
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "types.h"

/**
   @struct scc_tarjan_vertex
   @brief Identify the SCC-membership of a vertex.
   @author Ole Kristian Ekseth (oekseth)
   @ingroup graph
**/
struct scc_tarjan_vertex {
  //! The values to describe SCC-membership.
  uint index, putative_scc_id;
  /* //  bool is_alreadyInsertedInto_scc; */
  uint old_putative_index;
  //! @return the special marker used to state that a vertex is not member of any SCC.
  static uint get_noSCCmembership_marker() {return UINT_MAX -1;}
  //! @return true if putative_scc_id==index.
  const bool index_equals_putative_scc_id() const {
    return (index == putative_scc_id);
  }
  //! @return true if this vertex has not received any knowledge (at the point of calling this function).
  const bool is_empty() const {
    if( (index == UINT_MAX) && (putative_scc_id == UINT_MAX) ) {return true;}
    else {return false;}
  }

  //! @return true if index has a value.
  const bool index_has_value() const {
    if( (index != UINT_MAX) && (index != get_noSCCmembership_marker())) {return true;}
    else {return false;}
  }
  //! @return true if putative_scc_id has a value.
  const bool putative_scc_id_has_value() const {
    if( (putative_scc_id != UINT_MAX) && (putative_scc_id != get_noSCCmembership_marker())) {return true;}
    else {return false;}
  }

  //! @return true if the vertex is member of an SCC.
  const bool is_SCC_member() const {
    if(putative_scc_id_has_value() && index_has_value()) {return true;}
    else {return false;}
  }
  
  //! Mark the object as not member of any SCC.
  void mark_as_not_member_of_SCCs() {
    index = get_noSCCmembership_marker();
    putative_scc_id = get_noSCCmembership_marker();
  }

  //! The constructor:
scc_tarjan_vertex(uint _index = UINT_MAX, uint _putative_scc_id = UINT_MAX) :
  index(_index), putative_scc_id(_putative_scc_id)
    , old_putative_index(UINT_MAX)
    //    , is_alreadyInsertedInto_scc(false)
 {}
};


#endif
