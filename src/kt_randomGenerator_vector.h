#ifndef kt_randomGenerator_vector_h
#define kt_randomGenerator_vector_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_randomGenerator_vector
   @brief a structured appraoch to generate differnet randomness-distributions (oekseth, 06. feb. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
 **/

#include "math_generateDistribution.h"
# ifndef not_include_headerFiles
#include "e_kt_correlationFunction.h"
#include "hp_distance.h"
#endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).

/**
   @struct s_kt_randomGenerator_vector__config
   @brief a cofnigruation-block for our "s_kt_randomGenerator_vector" type-object (oekseth, 06. feb. 2017)
 **/
typedef struct s_kt_randomGenerator_vector__config {
  e_kt_randomGenerator_type_t typeOf_randomNess;
  bool isTo_initIntoIncrementalBlocks;
  s_kt_correlationMetric_t obj_metric;
  uint n_local_trials; //! which is used in our of our kmeans++ appraoches.
  bool config_centralityExtract__isToUse; //! is the 'switch' which descirbe if we 'in our first/intal randomziation-call' use randomzaiton 'in our appraoch'
# ifndef not_include_headerFiles
  s_kt_centrality_config__wrapper_ranks_t config_centralityExtract; //! which is used if "config_centralityExtract__isToUse" is set.
  #endif //! else we assueme that the caller has explcitly added these librarie s(or their own permtuaitons) to tehse (oekseth, 06. aug. 2018).
} s_kt_randomGenerator_vector__config_t;
//! @return an inaited s_kt_randomGenerator_vector__config_t object
s_kt_randomGenerator_vector__config_t init__s_kt_randomGenerator_vector__config_t(const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks);
/**
   @struct s_kt_randomGenerator_vector
   @brief provide logics for random generations using differnet random-appraoches and wrt. the mofiviation of current random-mappings (oekseht, 06. jan. 2017)
   @remarks to examplfiy the usage of this function:
   -- "isTo_initIntoIncrementalBlocks" which if set to false impleis that we investigate the effect of permtuating the 'previous ranodmizioatn-strategy', a 'case' which may be useful in order to provide a 'walk' (rather than 'splasehs') in a matrxi
   @remarks of interest is to know the importance of differnet randomzaiton-appraoches: to identify the effect of randizmiaotn-strategies on different data-sets. Givne the latter researhc-itnerest it is of interest/importance to describe a performance-evalaution of our random-evlauation-appraoch:
   # challenge(1): accuracy: 
   # challenge(2): coverage:
   # challenge(3): prediction: 


   // FIXME: wrt. [below] cosnider writing a k-means-permtautions ... where we select/use only the 'most important/cetral vertices in network' ... apply k-means-algorithm 'on this' ... thereafter use our 'closest-distance-base-dappraoch' <-- describe a new article wrt. 'scuh', ie, a eprmtatuion of the kmeans++ and minibatch algorithm.
   // FIXME: cosndier writing a sperate evlauation-appraoch wrt. [below].
   ... an example-case coserns the relationship/simliarty between "centrality-seleciton", "randomness"  and "cluster-accraucy", eg, wrt. the use-case: "for what cases does it suffice to use only the msot central vertices in k-means-clsutering (ie, wher ethe 'central vertices' are used to predict clsuter-accuracy for the complete data-set)?". From the latter we observe how it is psossilbe/stragithwrod to relate "centrlaity" to "network-represntivness": 
   // FIXME[conceptual]: is [”elow] assumpiotn correct? <-- try to expermeientally idneitfy 'gorups of data-sets and centrlaity-metrics' which are cosnistent wrt. the latter ... we evaluate ideal-clusters wrt. the enumerations = "|data|x|radnomenss|" <-- fixme: would this be correct ... ie, as 'some cluster-sets may have strongly shared charareritics? <-- consider to 'first cluster wrt. 'only the "randomenss"' using 'dynamic clsutering', and then 'merge the identifers' which are 'in the same class', ie, before we 'apply our clsutering'  <-- seems complex, ie, cosnider 'first a mnaul out-print of simliar simMetrics ... and then update our "kt_centrality" and our "e_kt_randomGenerator_type_t" with a fucntion to 'suggest' different classes of ranodmenss. 
   if the clusters are unchanged (when using only the most central vertices in evlauation) then the network is correctly/accuratly described by the cneltiraty-emasre in question. 
   .... 
 **/
typedef struct s_kt_randomGenerator_vector {
  long long int __counter__cntRandomCalls; //! which is used to 'hold' the numbe rof 'random calls' to this ojbect.
  s_kt_randomGenerator_vector__config_t config_init; //! which is used in the init-step of the randomness-configruait: an example-case conserns "k-means++" alogirhtm, where difference between "k-menas" and "k-means++" conserns the 'use' of a different random-inti-step in the "k-means++" algorithm (oekseht, 06. feb. 2017).
  s_kt_randomGenerator_vector__config_t config;
  uint nclusters;
  uint nelements;
  uint *mapOf_columnToRandomScore; //! The cluster number to which an element was assigned.   
  s_kt_matrix_base_t *matrix_input; //! which if set is assumed tor epresent an inptu-matrix.
} s_kt_randomGenerator_vector_t;

/**
   @brief initliazes an object used to idnetyf a subset of cases to evluate (oekseth, 06. jan. 2017).
   @param <nclusters> The number of clusters.
   @param <nelements> The number of elements to be clustered (i.e., the number of genes or microarrays to be clustered).
   @param <typeOf_randomNess> is the type of randomenss to be used in the idneitifciaotn of random vectors.
   @param <isTo_initIntoIncrementalBlocks> which if set to false re-uses ealrier randomziaotn.
   @param <matrix_input> which if Not set to NULL is used in a subset of our typeOf_randomNess randomziaton-metrics to idnetify vertices wich are 'in the same set' as idneitfed seed-vertices (oekseth, 06. feb. 2017).
   @return a new-intalized object.
 **/
s_kt_randomGenerator_vector_t initAndReturn__s_kt_randomGenerator_vector_t(const uint nclusters, const uint nelements, const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks, s_kt_matrix_base_t *matrix_input);
//! Initalise the s_kt_matrix_base_t object to defualt settings, and then return.
s_kt_randomGenerator_vector_t initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t();
//! Update the size-proeprty of the cluster-configruation: de-allcoate if the 'data have already been allocated':
static void updateSizeProp__s_kt_randomGenerator_vector_t(s_kt_randomGenerator_vector_t *self, const uint nclusters, const uint nrows) {
  assert(self);
  assert(nrows > 0); const uint empty_0 = 0;
  if(nrows <= nclusters) {
    fprintf(stderr, "!!\t (error:kMeans)\t You have set |rows|=%u <= |clusters|=%u, ie, for whcih the result will result in a pointless cluster-executioni. In brief we suggest you update the input-cofnigirautions, eg, by using the \"tut_*.c\" tutorials as a use-case. However, if the latter does not solve your issue, please concat senior developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nrows, nclusters, __FUNCTION__, __FILE__, __LINE__);
  }
  if(self->mapOf_columnToRandomScore != 0) {free_1d_list_uint(&(self->mapOf_columnToRandomScore));}
  //!
  //! Allocate:
  self->mapOf_columnToRandomScore = allocate_1d_list_uint(nrows, empty_0);
  self->mapOf_columnToRandomScore[0] = UINT_MAX;
  self->nelements = nrows;
  self->nclusters = nclusters; //! ie, default assumption.  
}
//! Copy from a different object, where we expect the matrix-diemsions to be unchanged while the clsuter-count to be different (oekseth, 06. mar. 2017)
static s_kt_randomGenerator_vector_t copy__sameMatrix__s_kt_randomGenerator_vector_t(const s_kt_randomGenerator_vector_t *parent, const uint nclusters) {
  s_kt_randomGenerator_vector_t self = initAndReturn__s_kt_randomGenerator_vector_t(nclusters, parent->nelements, e_kt_randomGenerator_type_hyLysisDefault, /*isTo_initIntoIncrementalBlocks=*/parent->config.isTo_initIntoIncrementalBlocks, parent->matrix_input);
  //!
  //! Set the cofnigruaiton:
  self.config = parent->config;
  self.config_init = parent->config_init;
  //!
  //! @return
  return self;
}
/**
   @brief Construct a subset of the vertices using the [abov€] mapping-table (oekseth, 06. feb. 2017).
   @param <keyMap_localToGlobal> is the key-mappings from the 'old object' to the 'local object': the vertices 'are in a local scope which are to be mapped to the global scope'.
   @param <superset> is the 'old' object to copy cofnigruatiosn from
   @param <matrix_alreadyCompressed> is a matrix which 'hold' valeus which are 'lareayd cofniveted int the comrpessed/subset of valeus'
   @param <nclusters> si the 'lcoal' coutn of clsuters to comptue for.
   @return the itnalized object
**/
s_kt_randomGenerator_vector_t initAndReturn__fromSuper__s_kt_randomGenerator_vector_t(const uint *keyMap_localToGlobal, const s_kt_randomGenerator_vector_t *superset, s_kt_matrix_base_t *matrix_alreadyCompressed, const uint nclusters);

//! De-allocates the s_kt_randomGenerator_vector_t object (oekseth, 06. jan. 2017)
void free__s_kt_randomGenerator_vector_t(s_kt_randomGenerator_vector_t *self);

//! @return true if we are to apply randomzaiton in the first step of the algorithm.
bool isToUse__randomziaitonInFirstIteration__s_kt_randomGenerator_vector_t(const s_kt_randomGenerator_vector_t *self);

/**
   @breif performs an initial random clustering, needed for k-means or k-median clustering.
   @param <self> uis the objhect which hold the data and cofigurations
   @remarks Elements (genes or microarrays) are randomly assigned to clusters. The number of elements in each cluster is chosen randomly, making sure that each cluster will receive at least one element.
**/
void randomassign__speicficType__math_generateDistributions(s_kt_randomGenerator_vector_t *self);



#endif //! EOF
