#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"
#include "alg_dbScan_brute.h"
#include "hp_ccm.h"

/**
   @brief examplifies how to use different DBSCAN permutations.
   @author Ole Kristian Ekseth  (oekseth,  06. jan. 2017).
   @remarks 
   - provides an interface, and tutoruial, for exploring different 'disjoint-forest' permtautions, eg, wrt. the algorithms of "DBSCAN" and "hp-cluster"
   compile: g++ tut_disjointCluster_2_algComparisonSingleAlg.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC
**/
int main() 
#else
  static void tut_disjointCluster_2_algComparisonSingleAlg()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //!
  const uint nrows = 10;     const uint ncols = 10; 
  s_kt_matrix_t mat_data = initAndReturn__s_kt_matrix(nrows, ncols);
  //const t_float score_notMatch = T_FLOAT_MAX;  const t_float score_match = 1;
  const t_float score_notMatch = 100000;  const t_float score_match = 1;
  //  const t_float score_notMatch = 100;  const t_float score_match = 1;
  //  const t_float score_notMatch = 100;  const t_float score_match = 1;
  //! Set defult score:
  for(uint i = 0; i < mat_data.nrows; i++) {
    for(uint k = 0; k < mat_data.ncols; k++) {mat_data.matrix[i][k] = score_notMatch;}}
  //! Add relationships:
#define __addRel(head, tail) ({assert(head < nrows); assert(tail < ncols); mat_data.matrix[head][tail] = score_match;})
  //! Not: for illustrative purposes we avoid inlcuding vertex=0 in any of the clusters.
  //  __addRel(1, 2);
  __addRel(1, 5);
  __addRel(5, 6);
  //__addRel(2, 5);
  __addRel(6, 7);
  __addRel(7, 8);
  __addRel(8, 9);
  //!
  //! A seperate sub-tree:
  __addRel(2, 3);
  __addRel(3, 4);
  //  __addRel(3, 6);
  // __addRel(2, 4);
  __addRel(4, 5);  
#undef __addRel
  //! Construct an ajdency-matrix:
  s_kt_matrix_t mat_adj = initAndReturn__s_kt_matrix(nrows, ncols);  
  for(uint i = 0; i < mat_data.nrows; i++) {
    for(uint j = 0; j < mat_data.ncols; j++) {
      if(mat_data.matrix[i][j] < score_notMatch) {
	mat_adj.matrix[i][j] = mat_data.matrix[i][j];}}}
  //! Compute:
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
  e_alg_dbScan_brute_t alg_id =
    //    e_alg_dbScan_brute_linkedListSet
    // e_alg_dbScan_brute_sciKitLearn
    //e_alg_dbScan_brute_splitData_kdTree
    //e_alg_dbScan_brute_splitData_brute_fast
    // e_alg_dbScan_brute_splitData_brute_slow
    e_alg_dbScan_brute_hpCluster
;
  const t_float epsilon = 100; const uint minpts = 0; //! which is used to set thresholds for sigicant (ie, central) rows (hence, transalting a dense amtrix into a sparse array). 
  //const t_float epsilon = 100; const uint minpts = nrows;
  s_kt_list_1d_uint_t clusters = setToEmpty__s_kt_list_1d_uint_t();
  t_float ccm_score = T_FLOAT_MAX; //!< hold the CCM-score.
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
  if(true) {
    s_kt_matrix_base_t mat_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_adj);
    clusters = computeAndReturn__alg_dbScan_brute(alg_id, &mat_shallow, epsilon, minpts);
    { //! Set each 'not-part-of-any-cluster' to their own clusters:
      //! Note: the interptaiton is correct for some use-cases, though not all, hence explaisn why 'the asusmption' is not globally enforced.
      uint max_clusterId = 0;
      for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
	if(clusters.list[row_id] != UINT_MAX) {
	  max_clusterId = macro_max(max_clusterId, clusters.list[row_id]);
	}
      }
      max_clusterId++;
      for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
	if(clusters.list[row_id] == UINT_MAX) {
	  clusters.list[row_id] = max_clusterId++;
	}
      }
    }
    //! Get the CCM-score:
    s_kt_matrix_base_t mat_shallow_data = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_data);
    const bool is_ok = ccm__singleMatrix__hp_ccm(ccm_enum, &mat_shallow_data, clusters.list, &ccm_score);
    assert(is_ok);
  } else { //! then we use the hpLysis-software for computation:
    //! Note: the resutls differes as different default cofniguraiton-settings are used. For details of the latter, see the "obj_hp.config.kdConfig" variable
    //! Note: below invovles additional default steps wrt. simliarty-metrics, hence the results may differ.
    const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/true; 
    //obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
    obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
    //obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
    if(false) {
      obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
      obj_hp.config.corrMetric_prior_use = true;
    }
    
    //! 
    //! Apply logics:
    // printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
    const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_data, /*nclusters=*/UINT_MAX, /*npass=*/100);
    //! Get the CCM-score:
     ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
    //! Get the clusters:
    clusters = getClusters__hpLysis_api(&obj_hp);
    //! De-allocte:
    free__s_hpLysis_api_t(&obj_hp);
  }
  assert(clusters.list_size > 0);
  //! 
  //! Write out the clusters:
  fprintf(stdout, "clusters(\"%s\":%f):\t", 
	  getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_enum),
	  ccm_score);
  for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
    if(clusters.list[row_id] != UINT_MAX) {
      fprintf(stdout, "%u\t", clusters.list[row_id]);
    } else {
      fprintf(stdout, "-\t");
    }	   
  }
  fprintf(stdout, "#! at %s:%d\n", __FILE__, __LINE__);
  //! De-allocate:   
  free__s_kt_list_1d_uint_t(&clusters);
  free__s_kt_matrix(&mat_data);
  free__s_kt_matrix(&mat_adj);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

