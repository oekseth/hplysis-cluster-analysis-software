    t_float sumOf_1_sizes = 0;
    //! First Comptue wrt. 'extremes' for 'Cluster 1':
    t_float sumOf_scores_min_1 = 0;     t_float sumOf_scores_max_1 = 0;
    for(uint head_id = 0; head_id < nrows; head_id++) {
      assert(head_id < self->mapOfCluster1_size);
      const t_float cntInCluster_1 = self->mapOfCluster1[head_id]; 
      t_float max_sum = T_FLOAT_MIN_ABS;       t_float min_sum = T_FLOAT_MAX;
      for(uint tail_id = 0; tail_id < ncols; tail_id++) {
	const t_float sum_and = self->matrixOf_coOccurence.matrix[head_id][tail_id];    
	assert(sum_and != T_FLOAT_MAX);
	//! update the max-score-property:
	max_sum = macro_max(max_sum, sum_and);
	min_sum = macro_min(max_sum, sum_and);
      }
      //! Update the global-score-property:
      if(max_sum != T_FLOAT_MIN_ABS) {
	sumOf_scores_max_1 += max_sum;
      } 
      if(min_sum != T_FLOAT_MAX) {
	sumOf_scores_min_1 += min_sum;
      } 
      sumOf_1_sizes += cntInCluster_1;
    }
    //! Tehreafter Comptue wrt. 'extremes' for 'Cluster 2':
    t_float sumOf_scores_min_2 = 0;     t_float sumOf_scores_max_2 = 0;
    t_float sumOf_2_sizes = 0;
    for(uint tail_id = 0; tail_id < ncols; tail_id++) {
      assert(tail_id < self->mapOfCluster2_size);
      const t_float cntInCluster_2 = self->mapOfCluster2[tail_id]; 
      t_float max_sum = T_FLOAT_MIN_ABS;       t_float min_sum = T_FLOAT_MAX;
      for(uint head_id = 0; head_id < nrows; head_id++) {
	const t_float sum_and = self->matrixOf_coOccurence.matrix[head_id][tail_id];    
	assert(sum_and != T_FLOAT_MAX);
	//! update the max-score-property:
	max_sum = macro_max(max_sum, sum_and);
	min_sum = macro_min(max_sum, sum_and);
      }
      //! Update the global-score-property:
      if(max_sum != T_FLOAT_MIN_ABS) {
	sumOf_scores_max_2 += max_sum;
      } 
      if(min_sum != T_FLOAT_MAX) {
	sumOf_scores_min_2 += min_sum;
      } 
      sumOf_2_sizes += cntInCluster_2;
    }
