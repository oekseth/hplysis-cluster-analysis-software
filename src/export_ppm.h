#include <stdint.h>
#include <stdlib.h>
#ifndef EXPORT_PPM_H
#define EXPORT_PPM_H

// Primitive container type for PPM image data
// PPM maxvalue parameter omitted, hard-coded to 255 for simplicity
typedef struct {
    size_t nrows, ncols;   // Image dimensions
    uint8_t *data;  // Interleaved triplets of color values in [0,255] range
} ppm_image_t;

ppm_image_t *
exportTo_image (
    float **matrix, const size_t nrows, const size_t ncols,
    char **mapOf_names_rows, char **mapOf_names_cols
);

int ppm_image_write ( const char *filename, ppm_image_t *ppm );
void ppm_image_finalize ( ppm_image_t *ppm );

#endif
