	  if(resultMatrix == NULL) {
	    s_allAgainstAll_config *config_allAgainstAll = &self;
	    for(uint row_id = 0; row_id < nrows_1; row_id++) {
	      for(uint col_id = 0; col_id < nrows_2; col_id++) {
		const t_float distance = resultMatrix_tmp[row_id][col_id];
		ktCorr__postProcess_afterCorrelation_for_value(row_id, col_id, distance, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, (data1 == data2));
	      }
	    }
	  }
	  //! 
	  //! De-allocat ehte lcoal matrix:
	  if(resultMatrix_tmp != resultMatrix) {
	    free_2d_list_float(&resultMatrix_tmp, nrows_2); resultMatrix_tmp = NULL;
	  }
