#ifndef e_correlationType_h
#define e_correlationType_h

/**
   @enum e_correlationType
   @brief an enum to identify the type of proxmity-metric to be computed, ie, how to uze the resultof our copmtued 2x2 contingency table.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
   @remarks for details wrt. each enum, see comments 'ilined' in our below enum-definition.
 **/
typedef enum e_correlationType {
  e_correlationType_proximity_altAlgortihmComputation_binary_russelRao, 
  //! Note: when compared to "Russel and Rao" difference is found/is seen wrt. the use of 'positive weight' to 'common-no-matches'.
  e_correlationType_proximity_altAlgortihmComputation_binary_simpleMatching,
    //! Note: compare the set of 'joint' matches against knowledge assoicated to each vertex (for a given pair).
  e_correlationType_proximity_altAlgortihmComputation_binary_jaccard,
//! Note: measure/identify distance and not similarity, ie, on contrast to Jaccard: when compared to the latter, we observe how Dice results in increased 'centrality'/'weight' given to the 'found in both the sets' case
  e_correlationType_proximity_altAlgortihmComputation_binary_dice_Czekanowski_Sorenson,
    //! Note: difference to Dice is seen by the use/'inclusion' of the 'no-match' property: gives/provides 'equal' significance to 'what they both have in common' as 'what they do not have in common', which for 'open-world-assumptions' (eg, for knowledge-based networks) should be used with carefulness, ie, as it is hard to evaluate significance (ie, 'identify')  every measurement/entity/compoent which two proteins are not described by.
  e_correlationType_proximity_altAlgortihmComputation_binary_rogensTanimoto,
//! Note: difference to 'Sokal and Sneath' is seen by the decreased with given to 'matches and no-matches', while increasing the 'reduction-factor' associated to 'internal differences': therefore expects the results (to be produced by this proximity-algorithm/approach) to be similar to both Jaccard and Sokal-Sneath.
  e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath,
    //! Note: 'give' increased weight to no-matches: when compared to Jaccard difference is observed/found wrt. the doubling of 'penalty' (for matches found in only one of the sets).
  e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath_measure2,
    //! Note: similar to Kulczynski wrt. the identification of relationships between 'none-matches' and 'matches': difference found wrt. the weighting/'inclusion' of none-matches. For 'controlled data-sets' (eg, defined selections) this 'inclusion of the unknown' may be of interest/importance, ie, to identify separation of vertex-clusters, eg through the comparison of 'clusters identified through disjointness' VS 'clusters identified through the 'forced-algorithm' of <i>Barnes-Hut</i>.
  e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath_measure3,
    //! Note: 'provide'/identifies a direct realtionships/correspondence between 'matched by both' and 'matched by one', ie, does not include/use 'what is unknown for both'; difference to Jaccard is seen by omission of the 'matches-match' count in the divisor, ie, results in an increased sensitivity wrt. the 'matches-only-one' case.
  e_correlationType_proximity_altAlgortihmComputation_binary_kulczynski,
  //! ---------
  e_correlationType_proximity_altAlgortihmComputation_binary_undef
} e_correlationType__t;


#endif //! EOF
