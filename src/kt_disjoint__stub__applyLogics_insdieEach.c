
  s_kt_forest_findDisjoint_t obj_disjoint; 
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
//fprintf(stderr, "nrows=%u, ncols=%u, at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
//assert(nrows == ncols); // TODO: consider re-including this (oekseth, 06. jul. 2017).
//assert(cnt_rows_1_xmtTransposed == cnt_cols_1_xmtTransposed); //! ie, as we oterhwise need to update [below]:
#endif

#ifndef __localConfig__specificEmptyValue //! then we use a defualt 'emptnyess-mask' locally specified by a suser (eg, set to '0'
#define __localConfig__specificEmptyValue T_FLOAT_MAX
#endif
/* #else */
/*   allocate__denseMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows, nrows, matrix_disjointInput->matrix, /\*mask=*\/NULL, /\*emptyValueIf_maskIsNotUsed=*\/T_FLOAT_MAX); */
/* #endif */

#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
  allocate__denseMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix_disjointInput->matrix, /*mask=*/NULL, /*emptyValueIf_maskIsNotUsed=*/__localConfig__specificEmptyValue);
  //! Idneitfy the disjoint regiosn:
  graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
//fprintf(stderr, "(\t\t)\tat %s:%d\n", __FILE__, __LINE__);
#else //! then we apply a complex intaition-procedure:
#if(__disjointConfig__useComplexInference__rowsDiffersFrom == 1)
allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix_disjointInput->matrix, /*mask=*/NULL, /*empty-value=*/__localConfig__specificEmptyValue, /*isTo_init=*/true, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/true, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/(nrows+ncols));
//! Idneitfy the disjoint regiosn:
//fprintf(stderr, "(\t\t)\tat %s:%d\n", __FILE__, __LINE__);
graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
#else //! then we assume 'that we have three different name-sapces': rows(first), cols(both), rows(second)
assert(matrix_disjointInput->matrix);
assert(matrix_disjointInput_2->matrix);
//fprintf(stderr, "(\t\t)\t at %s:%d\n", __FILE__, __LINE__);
    allocateAndApply__denseMatrix___differentNameSpaceInRowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows_1, nrows_2, ncols, matrix_disjointInput->matrix, matrix_disjointInput_2->matrix, __localConfig__specificEmptyValue);
//fprintf(stderr, "(\t\t::completed)\t at %s:%d\n", __FILE__, __LINE__);
#endif
#endif //! 'compelx-intiaiton-rpcoedure'.

//!
//! Iterate through the set of disjotin sets
/* #ifdef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable */
/* fprintf(stderr, "(no-clusteR) \t at %s:%d\n", __FILE__, __LINE__); */
/* #else */
/* fprintf(stderr, "(clstered-inferred)\tat %s:%d\n", __FILE__, __LINE__); */
/* #endif */
uint max_cntForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
{
  uint max_cntForests_tmp = 0; get_maxNumberOf_disjointForests(&obj_disjoint, &max_cntForests_tmp);
  assert(max_cntForests_tmp != UINT_MAX);
  // printf("max_cntForests_tmp=%u, max_cntForests=%u, at %s:%d\n", max_cntForests_tmp, max_cntForests, __FILE__, __LINE__);
  assert(max_cntForests == max_cntForests_tmp);
}
  if( (max_cntForests > 1) && (max_cntForests != UINT_MAX) ) {
    
    // FIXME: add a parallel wrapper-macro for [”elow]:
    
    // FIXME: add a new macor-apram for the case where 'the names-acpe oft eh rows are different from the names-apce of the columsn' .... callign our udpated "init__buildSubset_rowsDifferentFromColumns__fromMatrix__s_kt_matrix(..)" function
    

    for(uint forest_id = 0; forest_id < max_cntForests; forest_id++) {
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
      uint *arrOf_result_vertexMembers = NULL; uint arrOf_result_vertexMembers_size = 0;
      cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, &arrOf_result_vertexMembers, &arrOf_result_vertexMembers_size);
      //fprintf(stderr, "[forest_id::%u]:\tarrOf_result_row_size=%u, at %s:%d\n", forest_id, arrOf_result_vertexMembers_size, __FILE__, __LINE__);
#else  //! then we apply a complex intaition-procedure:
#if(__disjointConfig__useComplexInference__rowsDiffersFrom == 1)
      uint *arrOf_result_row = NULL;  uint arrOf_result_row_size = 0; //! where the latter is a temprao vriable:
      uint *arrOf_result_col = NULL;  uint arrOf_result_col_size = 0; //! where the latter is a temprao vriable:
      //! The test:
      //! Build a selit of the forest-ids:
      cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, 
											     &arrOf_result_row, &arrOf_result_row_size,
											     &arrOf_result_col, &arrOf_result_col_size);
      const uint arrOf_result_vertexMembers_size = arrOf_result_row_size;            
      //fprintf(stderr, "[forest_id::%u]:\tarrOf_result_row_size=[%u, %u], at %s:%d\n", forest_id, arrOf_result_row_size, arrOf_result_col_size,  __FILE__, __LINE__);
#else //! then we assume 'that we have three different name-sapces': rows(first), cols(both), rows(second)
      uint *arrOf_result_row = NULL;  uint arrOf_result_row_size = 0; //! where the latter is a temprao vriable:
      uint *arrOf_result_row_2 = NULL;  uint arrOf_result_row_size_2 = 0; //! where the latter is a temprao vriable:
      uint *arrOf_result_col = NULL;  uint arrOf_result_col_size = 0; //! where the latter is a temprao vriable:
      //! Build a selit of the forest-ids:
      cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, 
													     &arrOf_result_row, &arrOf_result_row_size,
													     &arrOf_result_row_2, &arrOf_result_row_size_2,
													     &arrOf_result_col, &arrOf_result_col_size);
      const uint arrOf_result_vertexMembers_size = macro_min(arrOf_result_row_size, arrOf_result_row_size_2);
      //fprintf(stderr, "arrOf_result_row_size_2=%u, arrOf_result_row_size=%u, at %s:%d\n", arrOf_result_row_size_2, arrOf_result_row_size, __FILE__, __LINE__);
      const uint nrows = macro_max(arrOf_result_row_size, arrOf_result_row_size_2);
#endif
#endif
      if(arrOf_result_vertexMembers_size > 0) { 
	// FIXME[JC]: suggestisonf or how to 'ahdnle the case with 'extremly small disjoitnf-reost-sielands ... ie, should/could 'these be safely ingored?
	//assert(arrOf_result_vertexMembers_size != 1); //! ie, as we expect our 'disjoitn-forest-case' to have handled 'this'.
	//!
	//! Allocate a local lsit 'of mappings':
	// TODO[performance]: cosnider to instead use a small-sizes list for 'small data-sets' <--- first idneitfy/desdibe the expected 'value-spread' wrt. 'dsijointenss'. 
	const uint default_value_uint = 0;
	uint *keyMap_localToGlobal = allocate_1d_list_uint(nrows, default_value_uint);
	//!
	//! Build a new local matrix-object:
	s_kt_matrix matrix_subset;
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
	const bool is_ok = init__buildSubset_fromMatrix__s_kt_matrix(&matrix_subset, matrix_disjointInput, arrOf_result_vertexMembers, arrOf_result_vertexMembers_size, keyMap_localToGlobal, /*isTo_updateNames=*/false);
	assert(is_ok); //! ie, as we otherwise would have a bug.
	const uint *mapOf_clusterVertices__toClusterIds__local = keyMap_localToGlobal;
	/* if(mapOf_clusterVertices__toClusterIds != NULL) { */
	/*   mapOf_clusterVertices__toClusterIds = allocate_1d_list_uint(nrows, default_value_uint */
	/*   assert(false); // FIXME: add something */
	/*   //mapOf_clusterVertices__toClusterIds__local = NULL; */
	/* } */
#else  //! then we apply a complex intaition-procedure:
/* #if(__disjointConfig__useComplexInference__rowsDiffersFrom == 2) //! then we assume 'that we have three different name-sapces': rows(first), cols(both), rows(second) */
/* 	printf("arrOf_result_row_size_2=%u, arrOf_result_row_size=%u, nrows=%u, at %s:%d\n", arrOf_result_row_size_2, arrOf_result_row_size, nrows, __FILE__, __LINE__); */
/* #endif */
	const bool is_ok = init__buildSubset_rowsDifferentFromColumns__fromMatrix__s_kt_matrix(&matrix_subset, matrix_disjointInput, 
											       arrOf_result_row, arrOf_result_row_size, 
											       arrOf_result_col, arrOf_result_col_size, 
											       /*keyMap_localToGlobal=*/NULL,
											       //keyMap_localToGlobal, 
											       /*isTo_updateNames=*/false);
	assert(is_ok); //! ie, as we otherwise would have a bug.
#if(__disjointConfig__useComplexInference__rowsDiffersFrom == 2) //! then we assume 'that we have three different name-sapces': rows(first), cols(both), rows(second)
	s_kt_matrix matrix_subset_2;
	assert(nrows >= arrOf_result_row_size);
	//assert(nrows >= arrOf_result_col_size);
	const bool is_ok_matrix2 = init__buildSubset_rowsDifferentFromColumns__fromMatrix__s_kt_matrix(&matrix_subset_2, matrix_disjointInput_2, 
												       arrOf_result_row_2, arrOf_result_row_size_2, 
												       arrOf_result_col, arrOf_result_col_size, 
												       /*keyMap_localToGlobal=*/NULL,
												       /*isTo_updateNames=*/false);
	assert(is_ok_matrix2); //! ie, as we otherwise would have a bug.
	s_kt_matrix *local_matrix_2 = &matrix_subset_2;
	/* uint *mapOf_clusterVertices__toClusterIds__local = NULL; */
	/* if(mapOf_clusterVertices__toClusterIds != NULL) { */
	/*   assert(false); // FIXME: add something */
	/* } */
#endif
	/* if(mapOf_clusterVertices__toClusterIds != NULL) { */
	/*   assert(false); // FIXME: add something */
	/* } */
	const uint *mapOf_clusterVertices__toClusterIds__local = arrOf_result_row;
#endif
	
	
	//!
	//! Perofrm the clsutering:
	s_kt_matrix *local_matrix = &matrix_subset;
#include __localConfig__applyLogics_pathTo_codeChunk //! ie, the 'logics specifally to be applied for the caller'.





	//! De-allcoat ethe lcoal list 'of mappigns':
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
	free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
#else  //! then we have used a complex intaition-procedure:
	//! De-allcote:
	free_1d_list_uint(&arrOf_result_row); arrOf_result_row = NULL;
	free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;	
#if(__disjointConfig__useComplexInference__rowsDiffersFrom == 2) //! then we assume 'that we have three different name-sapces': rows(first), cols(both), rows(second)
	free_1d_list_uint(&arrOf_result_row_2); arrOf_result_row_2 = NULL;
	free__s_kt_matrix(&matrix_subset_2);
#endif
#endif
	assert(keyMap_localToGlobal != mapOf_clusterVertices__toClusterIds); //! ie, as we expec thte latter to have been 'locally allcoated'.
	free_1d_list_uint(&keyMap_localToGlobal); keyMap_localToGlobal = NULL;
	free__s_kt_matrix(&matrix_subset);
	//#ifdef __disjointConfig__useComplexInference__rowsDiffersFrom
/* #if(__disjointConfig__useComplexInference__rowsDiffersFrom == 2) //! then we assume 'that we have three different name-sapces': rows(first), cols(both), rows(second) */
/* 	free__s_kt_matrix(&matrix_subset_2); */
/* #endif */
//#endif
      } else {
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
	assert(arrOf_result_vertexMembers == NULL);
#else
	assert(arrOf_result_row == NULL);
	assert(arrOf_result_col == NULL);
#endif
      }

    }
    
    //!
    //! De-allcoate the reserved memory:
    free_s_kt_forest_findDisjoint(&obj_disjoint);
  } else {
    //!
    //! De-allcoate the reserved memory:
    free_s_kt_forest_findDisjoint(&obj_disjoint);

    const s_kt_matrix *local_matrix = matrix_disjointInput;   
    uint *mapOf_clusterVertices__toClusterIds__local = NULL;
    const uint *keyMap_localToGlobal = mapOf_clusterVertices__toClusterIds; //! ie, a 'direct access.'
    /* if(mapOf_clusterVertices__toClusterIds != NULL) { */
    /*   // TODO: consider adding something <-- currently 'omitted */
    /* } */
#define __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
#include __localConfig__applyLogics_pathTo_codeChunk
    //! Reset:
#undef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
  }
