//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kd_tree.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hpLysis_api.h"//! to define "hpLysis__globalInit__kt_api()"
#include "hp_clusterFileCollection.h"

typedef struct s_kd_4 {
  FILE *file_out;
} s_kd_4_t;

static s_kd_4_t init_s_kd_4_t(const char *file_name) {
  //!
  s_kd_4_t self;
  //!
  self.file_out = fopen(file_name, "wb");
  //!
  printf("(info)\t Log-file generated at %s [from [%s]:%s:%d\n]", file_name, __FUNCTION__, __FILE__, __LINE__);
  //!
  assert(self.file_out);
  //!
  { //! Add header:
    const char *label_data = "#! row-name:";
    const char *label_data_dim_rows = "Rows";
    const char *label_data_dim_cols = "Columns";
    const char *str_case_label = "Algorithm";
    const char *config_minCnt = "DBSCAN-configuration";
    const char *time_total = "Time";
    // const char * = "";     
    fprintf(stdout, "%s\t%s\t%s\t%s\t%s\t%s\n", label_data, label_data_dim_rows, label_data_dim_cols, str_case_label, config_minCnt, time_total);
    fprintf(self.file_out, "%s\t%s\t%s\t%s\t%s\t%s\n", label_data, label_data_dim_rows, label_data_dim_cols, str_case_label, config_minCnt, time_total);
  }
  // FIXME: close thsi object
  //! 
  return self;
}

static void free_s_kd_4_t(s_kd_4_t *self) {
  fclose((self->file_out));
  self->file_out = NULL;
}

static void _add_to_result(s_kd_4_t *self, const char *label_data, const char *str_case_label, const float time_total, const uint config_minCnt, s_kt_matrix_t *mat_input) {
  assert(self->file_out);
  fprintf(self->file_out, "%s\t%u\t%u\t%s\t%u\t%.2f\n", label_data, mat_input->nrows, mat_input->ncols, str_case_label, config_minCnt, time_total);
}

static void apply_alg(s_kd_4_t *self, s_kt_matrix_t *mat_input, const char *label_data, s_kt_correlationMetric_t obj_metric) {
  uint config_minCnt = 1; 
  for(uint config_minCnt = 1; config_minCnt < 10; config_minCnt++) {
    //for(uint nn_numberOfNodes = 5; nn_numberOfNodes < 15; nn_numberOfNodes++) {
    for(uint case_id = 0; case_id < 4; case_id++) {
      for(uint radius_id = 0; radius_id < 3; radius_id++) {
	char str_case_label[1000]; memset(str_case_label, '\0', 1000); 
	t_float time_total = 0;
	//! 
	//! Build the kd-tree:
	start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
	s_kd_tree_t obj_kd = build_tree__s_kd_tree(mat_input);
	end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
	//! 
	s_kd_searchConfig_t obj_search;
	if(case_id == 0) { 
	  if(radius_id == 0) {
	    sprintf(str_case_label, "KD r-nearest: nn=5");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/5, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 1) {
	    sprintf(str_case_label, "KD r-nearest: nn=10");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 2) {
	    sprintf(str_case_label, "KD r-nearest: nn=20");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/20, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else {
	    assert(false); //! ie, then add support for this.
	  }
	  //! .------------------------------
	}else if(case_id == 1) { 
	  if(radius_id == 0) {
	    sprintf(str_case_label, "KD brute-fast: nn=5");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/5, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 1) {
	    sprintf(str_case_label, "KD brute-fast: nn=10");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 2) {
	    sprintf(str_case_label, "KD brute-fast: nn=20");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/20, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else {
	    assert(false); //! ie, then add support for this.
	  }
	} else if(case_id == 2) { 
	  //! .------------------------------
	  if(radius_id == 0) {
	    sprintf(str_case_label, "KD brute-fast XMT=sort: ball=5");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/UINT_MAX, UINT_MAX, /*ballsize=*/5, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 1) {
	    sprintf(str_case_label, "KD brute-fast XMT=sort: ball=10");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/UINT_MAX, UINT_MAX, /*ballsize=*/10, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 2) {
	    sprintf(str_case_label, "KD brute-fast XMT=sort: ball=20");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/UINT_MAX, UINT_MAX, /*ballsize=*/20, /*isTo_sort=*/false, &obj_metric);
	  } else {
	    assert(false); //! ie, then add support for this.
	  }
	  //! .------------------------------
	} else if(case_id == 3) { 
	  //! .------------------------------
	  if(radius_id == 0) {
	    sprintf(str_case_label, "KD brute, pre-compute: nn=5");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/5, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 1) {
	    sprintf(str_case_label, "KD brute, pre-compute: nn=10");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else if(radius_id == 2) {
	    sprintf(str_case_label, "KD brute, pre-compute: nn=20");
	    obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/20, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	  } else {
	    assert(false); //! ie, then add support for this.
	  }
	  //! .------------------------------
	  /* } else if(case_id == 3) { */
	  /*   sprintf(str_case_label, "KD brute-fast: nn=5"); */
	  /*   obj_search =  */
	  /* } else if(case_id == 4) { */
	  /* } else if(case_id == 5) { */
	  
	} else {
	  assert(false); //! ie, then add support for this.	  
	}
	//! ---------------------------
	// start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
	//s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
	//assert(cnt_search <= nrows);
	//! Compute the clusters:
	s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/config_minCnt);
	time_total = end_time_measurement("dbSCAN:KD:radius", 0); //! ie, stop the timer and write out the timiing-difference.
	t_float score_global = 0; uint cnt_withMembers = 0;
	for(uint i = 0; i < map_clusterId.list_size; i++) {
	  if(map_clusterId.list[i] != UINT_MAX) {score_global += map_clusterId.list[i]; cnt_withMembers++;}
	}
	fprintf(stderr, "#! time:%.2f\t sum(total)=%f, count(vertices-in-clusters)=%d, at %s:%d\n", time_total, score_global,cnt_withMembers,  __FILE__, __LINE__);
	//! 
	//! De-allocate:
	free__s_kt_list_1d_uint_t(&map_clusterId);
	free__s_kd_searchConfig_t(&obj_search);
	free__s_kd_tree(&obj_kd);
	//!
	//!
	_add_to_result(self, label_data, str_case_label, time_total, config_minCnt, mat_input);      
      }
    }
    //! ---------------- 
    // if(false)
      { //! Apply other algorithms:
      static const uint list_tut_time_2__clustAlg_size = 6;
      static e_hpLysis_clusterAlg list_tut_time_2__clustAlg[list_tut_time_2__clustAlg_size] = {
	e_hpLysis_clusterAlg_kCluster__AVG,
	//  e_hpLysis_clusterAlg_kCluster__medoid,  
	e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch,
	e_hpLysis_clusterAlg_HCA_max,
	e_hpLysis_clusterAlg_kruskal_hca,
	e_hpLysis_clusterAlg_disjoint_kdTree,
	e_hpLysis_clusterAlg_disjoint,
      };
      
      /* static const uint list_tut_time_2__sim_size = 1; */
      /* static e_kt_correlationFunction_t list_tut_time_2__sim[list_tut_time_2__sim_size] = { */
      /* 	e_kt_correlationFunction_groupOf_minkowski_euclid, */
      /* }; */
      //!
      //! 

      //for(uint sim_metric = 0; sim_metric < list_tut_time_2__sim_size; sim_metric++) {
      const s_kt_correlationMetric_t obj_sim = obj_metric;
      //const s_kt_correlationMetric_t obj_sim = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_metric, e_kt_categoryOf_correaltionPreStep_none); //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
      //const s_kt_correlationMetric_t obj_sim = s_kt_correlationMetric_t initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_groupOf_minkowski_euclid)sim_metric, e_kt_categoryOf_correaltionPreStep_none); //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
      //const s_kt_correlationMetric_t obj_sim = //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
      assert(obj_sim.metric_id != e_kt_correlationFunction_undef);
    //! Iterate throguh clsuter-algroithsm: 
      for(uint clust_alg = 0; clust_alg < list_tut_time_2__clustAlg_size; clust_alg++) {
	e_hpLysis_clusterAlg_t enum_clust = list_tut_time_2__clustAlg[clust_alg];
	//      const e_hpLysis_clusterAlg_t enum_clust = (e_hpLysis_clusterAlg_t)clust_alg;
	if( (enum_clust != e_hpLysis_clusterAlg_disjoint_MCL) 
	    && (enum_clust != e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) 
	    ) {
	  //!
	  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	  
	  // (Symmetric) adjacency matrix                                                                                                                            	  
	  //obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //true; // inputMatrix__isAnAdjecencyMatrix;	
	  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //true; // inputMatrix__isAnAdjecencyMatrix;	
	  //obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.
	  //obj_hp.config.corrMetric_prior_use = true; //! ie, use data-as-is.
	  //if(false) {
	  //! Specify that we are to use the k-means++-implmeantion defined/used in the "fast_Kmeans" software:
	  obj_hp.randomConfig.config_init.typeOf_randomNess = e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss; //! where the latter enum is define d in our "math_generateDistribution.h"
	  
	  
	  //! 
	  //! Compute the cluster:
	  printf("%s\t, at %s:%d\n", get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust), __FILE__, __LINE__);
	  start_time_measurement(); //! ie, start timer.
	  //s_kt_matrix_t mat_inp = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_2);
	  if(enum_clust == e_hpLysis_clusterAlg_disjoint) { 
	    /*   obj_hp.config.kdConfig.enum_id = e_kd_tree_searchStrategy_brute_fast; //! ie, then se tthe searhc-strategy deifned in oru "kd_tree.h" */
	    /*   enum_clust = e_hpLysis_clusterAlg_disjoint_kdTree; */
	    /*   obj_hp.config.kdConfig. */ //! where latter proeprteis refers to "kd_tree.h"
	    /*epsilon=*/obj_hp.config.kdConfig.ballsize = config_minCnt; // FIXME: olek: validate this.
	    /*minpts=*/obj_hp.config.kdConfig.nn = 10; // FIXME: olek: validate this.
	  } 
	  char str_case_label[1000]; memset(str_case_label, '\0', 1000); 
	  t_float time_total = 0;
	  sprintf(str_case_label, "%s: clusters=%u", get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust), config_minCnt);
	  start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_clust, 
						  mat_input,
						  //&(obj_shape_2.matrix), 
						  /*cnt_clusters=*/config_minCnt, /*npass=*/100);
	  //		  const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_clust, &(obj_shape_2.matrix), cnt_clusters, /*npass=*/1000);
	  assert(is_ok);
	  time_total = end_time_measurement(str_case_label, 0); //! ie, stop the timer and write out the timiing-difference.
	  //!
	  //!
	  _add_to_result(self, label_data, str_case_label, time_total, config_minCnt, mat_input);
	  //!
	  //! De-allcote:
	  free__s_hpLysis_api_t(&obj_hp);	  
	}
      }
    }
  }
}

static void _generateAndApplySyn_forDim(s_kd_4_t *self, const s_kt_correlationMetric_t obj_metric, const uint nrows, const uint ncols) {
  //const uint nrows = 100; const uint ncols = 5;
  //const uint nrows = 1000*10; const uint ncols = 5;
  //const uint nrows = 1000*1000; const uint ncols = 20;
  s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols);
  //!
  //! Set wild-card values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      const t_float score = rand();
      if(score != 0) {
	mat_input.matrix[row_id][col_id] = 1.0/score;
      }
    }
  }
  //!
  //! Generate results:
  const char *b_str_label_data = "sytentic-linear"; //! ie, the base (b) string.
  //char str_label_data[1000]; memset(str_label_data, '\0', 1000); sprintf("%s: dims:%u, %u", b_str_label_data, mat_input.nrows, mat_input.ncols);
  apply_alg(self, &mat_input, b_str_label_data, obj_metric);
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
}

static void tut_kd_4__compute(s_kd_4_t *self, const s_kt_correlationMetric_t obj_metric, s_kt_matrix_t *mat_list, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *stringOf_resultFile) { // e_kt_correlationFunction_t sim_pre, const e_hpLysis_clusterAlg clusterAlg, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm, const char *stringOf_resultFile) {
  assert(mat_list);
  assert(mat_input_size);
  //assert(clusterAlg != e_hpLysis_clusterAlg_undef);
  //!
  //!
  //!
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    s_kt_matrix_t mat_input = mat_list[data_id];
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }    
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
    //!
    //! Apply:
    apply_alg(self, &mat_input, stringOf_tagSample, obj_metric);    
  }
  
}

  
//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t __tut4_static__readDataFromObject__kd(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) {
	fprintf(stderr, "!!\t Unable to read file=\"%s\" w/tag=\"%s\", at %s:%d\n", data_obj.file_name, data_obj.tag, __FILE__, __LINE__);
	assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}


static void __eval__dataCollection__tut_kd_4(s_kd_4_t *self, const s_kt_correlationMetric_t obj_metric, const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = __tut4_static__readDataFromObject__kd(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    //!
    //! 
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
  }
  //!
  //! Apply logics: 
  tut_kd_4__compute(self, obj_metric, mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix);
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief provides a template for explroing the time-cost-effects of DBSCAN-strateiges, and its cofnigurations   
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017 and 06. dec. 2020).
   @remarks 
   -- compile: g++ -I../src/  -O2  -mssse3   -L ../src/  tut_kd_4_timing.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_tut
**/
int main()
  #endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif

  //!
  const char *file_name = "r-tut-kd-4.tsv";
  //! Init result-file:
  s_kd_4_t self = init_s_kd_4_t(file_name);
  
  s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
  if(false)
  {

    // FIXME: explroe effects of different dims ... update below ... when different alg-cobmaitnosn are tested ... 
    
    //!
    //! Load an arbitrary data-set:
    // const uint nrows = 1000; const uint ncols = 20;
    //const uint nrows = 1000; const uint ncols = 20;
    { const uint nrows = 500; const uint ncols = 10;
      _generateAndApplySyn_forDim(&self, obj_metric, nrows, ncols);
    }
    { const uint nrows = 500; const uint ncols = 100;
      _generateAndApplySyn_forDim(&self, obj_metric, nrows, ncols);
    }
    { const uint nrows = 500; const uint ncols = 500;
      _generateAndApplySyn_forDim(&self, obj_metric, nrows, ncols);
    }
    // ---------
    /* { const uint nrows = 5000; const uint ncols = 10; */
    /*   _generateAndApplySyn_forDim(&self, obj_metric, nrows, ncols); */
    /* } */
    /* { const uint nrows = 5000; const uint ncols = 100; */
    /*   _generateAndApplySyn_forDim(&self, obj_metric, nrows, ncols); */
    /* } */
    /* { const uint nrows = 5000; const uint ncols = 500; */
    /*   _generateAndApplySyn_forDim(&self, obj_metric, nrows, ncols); */
    /* }     */
  }

  {
    const char *resultPrefix = "tut-k4-4";
    //const char *resultPrefix = "tut_80DataSets_";
    {
#define __MiCo__useLocalVariablesIn__dataRealFileLoading 1
#ifdef __MiCo__useLocalVariablesIn__dataRealFileLoading
      const uint sizeOf__nrows = 100;
      const uint sizeOf__ncols = 100;
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      const bool isTo__evaluate__simMetric__MINE__Euclid = true;
      const char *stringOf_resultDir = "";
      // ---
      const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
      const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
      const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

      s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
      // printf("at %s:%d\n", __FILE__, __LINE__);
      //!
      //! We are interested in a more performacne-demanind approach: 

      fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
      fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
      //!
      fileRead_config__syn.isTo_transposeMatrix = false;
      fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
      fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
      fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
      fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
      //! --------------------------------------------
      //!
      //! File-specific cofnigurations: 
      s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
      //fileRead_config.isTo_transposeMatrix = true;
      fileRead_config.isTo_transposeMatrix = false;
      fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
      fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
      fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
      s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
      //fileRead_config.isTo_transposeMatrix = true;
      fileRead_config.isTo_transposeMatrix = true;
      //!

#endif

      { //const char *nameOf_experiment = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
	allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
	__eval__dataCollection__tut_kd_4(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset);
      }
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
    }
  }
  { //! Load for different real-life data-sets:
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
    const bool isTo_transposeMatrix = false;
    const char *resultPrefix = "tut-k4-4";
/*     { */
/* #include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used. */
/*       allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment); */
/*       __eval__dataCollection__tut_kd_4(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset); //, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix); */
/*       //__eval__dataCollection__tut_3(mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix); */
/*     } */
/*     {  */
/* #include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used. */
/*       allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment); */
/*       __eval__dataCollection__tut_kd_4(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /\*resultPrefix=*\/resultPrefix__dataSubset); //, /\*rowName_identifiesClusterId=*\/false, isTo_transposeMatrix); */
/*     } */
    {
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_kd_4(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset); //, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);
    }    
  }


  free_s_kd_4_t(&self);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
  
