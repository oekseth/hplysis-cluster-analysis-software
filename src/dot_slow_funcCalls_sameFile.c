typedef struct s_measure_codeStyle_functionRef_largeOverhead {
  t_float sumOf;
  t_float overhead[1000]; //! which is used to itrnoduce an overhead wrt. calls
} s_measure_codeStyle_functionRef_largeOverhead_t;

/* #ifndef __MiF__iF__funcName */
/* #  define __MiF__funcName(suffix) __funcName__prefix ## __funcName__mid ## _ ## suffix //! ie, concnate macro-names */
/* //#  define __MiF__addSuffixToFuncName(fun, suffix) fun ## _ ## suffix //! ie, concnate macro-names */
/* #endif */
#ifndef __MiF__iF__addSuffixToFuncName
//#  define __MiF__addSuffixToFuncName(suffix) __funcName__fast ## _ ## suffix //! ie, concnate macro-names
#  define __MiF__addSuffixToFuncName(fun, suffix) fun ## _ ## suffix //! ie, concnate macro-names
#  define __MiF__addSuffixToFuncName_main(suffix) __MiF__addSuffixToFuncName(__funcName__fast, suffix)
#endif

//!
//! Spefiy fucntiosn where the 'inner block' is in the same function.
//! Note: we do Not expect any sifngicant eprformance-differences when usign this appraoch: due to our exepciaotn that the blocks are sufivnly large (for effecitive hardware utlizaiotn).
#include "dot_slow_funcCalls_sameFile_blocks.c"
