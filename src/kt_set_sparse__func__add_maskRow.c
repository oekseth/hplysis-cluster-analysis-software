{

  const uint default_value_uint = 0;

  //! Allocate a temproary list of vertex-dis to be used:
  uint *tmpList_ids = allocate_1d_list_uint(ncols, default_value_uint);
  t_float *tmpList_score = NULL;   uint current_pos = 0;
  if(rowOf_scores) {
    const uint default_value_float = 0;
    tmpList_score = allocate_1d_list_float(ncols, default_value_float);
    //! Iterate:
    for(uint col_id = 0; col_id < ncols; col_id++) {
      if(
#if(__localConfig__mask__useMaskExplciit == 1) 
	 mask[col_id]
#else
	 isOf_interest(row[col_id])
#endif
	 ) {
	tmpList_ids[current_pos] = col_id;
	tmpList_score[current_pos] = row[col_id];
	current_pos++;
      }
    }
  } else {
    //! Iterate:
    for(uint col_id = 0; col_id < ncols; col_id++) {
      if(
#if(__localConfig__mask__useMaskExplciit == 1) 
	 mask[col_id]
#else
	 isOf_interest(row[col_id])
#endif
	 ) {
	tmpList_ids[current_pos] = col_id;
	//tmpList_score[current_pos] = row[col_id];
	current_pos++;
      }
    }
  }

  if(current_pos) { //! then we add values:
#if(__localConfig__isCalledFrom_2dObject == 1)
    add_rowSparse__s_kt_set_2dsparse_t(self, row_id, tmpList_ids, current_pos, tmpList_score);
#else
    add_rowSparse__s_kt_set_1dsparse_t(self, tmpList_ids, current_pos, tmpList_score);
#endif
  }

  //!
  //! De-allocate the objet:
  free_1d_list_uint(&tmpList_ids);
  if(rowOf_scores) {
    free_1d_list_float(&tmpList_score);
  }


}
