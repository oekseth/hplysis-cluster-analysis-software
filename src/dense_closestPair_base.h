#ifndef dense_closestPair_base_h
#define dense_closestPair_base_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


//#ifndef configIsCalledFromExternal_software
#include "def_intri.h"
#include "types.h"
#include "configure_hcaMem.h"
#include "log_hca.h"
#include "rowChunk.h"
//#endif //! else we assuem the caller uses a 'linker' to include 'these' (oekseth, 06. july 2016).

//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics:
#define __slow_update_globalMin_scoreAndIndex_forBucket(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs, bucket_id, row_id) ( { \
  const t_float min_distance = matrixOf_locallyTiled_minPairs[bucket_id]; \
  bool retVal_needToIterateThroughAllTiles = false; \
  if(min_distance <= mapOf_minTiles_value[bucket_id]) { \
    mapOf_minTiles_value[bucket_id] = min_distance; \
    mapOf_minTiles_value_atRowIndex[bucket_id] = row_id; \
  } else { \
  if(mapOf_minTiles_value_atRowIndex[bucket_id] == row_id) \
    retVal_needToIterateThroughAllTiles = true;		   \
  } \
  retVal_needToIterateThroughAllTiles; }) //! ie, return the result


//! The __cplusplus macro and C_CLUSTER_LIBRARY macro was included in this document by oekseth to make it possible to link to KnittingTools C++-library.
#ifdef __cplusplus
extern "C"{
#endif 


  //#ifndef NDEBUG
void  __forAll_update_globalMin_atBucketIndex(float *mapOf_minTiles_value, float *mapOf_minTiles_value_atRowIndex, float **matrixOf_locallyTiled_minPairs, const uint bucket_id, const uint row_endPosPlussOne, const bool config_useIntrisintitcs, s_rowChunk_t *obj_rowChunk);
//! Identify teh shortest paths for each bucket
  void __init_minPairs_eachColumnBucket(t_float **distmatrix, const uint nrows, const uint ncols, const uint cnt_bucketsEachRows, t_float **matrixOf_locallyTiled_minPairs, uint **matrixOf_locallyTiled_minPairs_columnPos, const uint bucket_size);
//! Identify the min-values for the 'global' column:
  void __init_minColumns_eachBucket(const uint nrows, const uint cnt_bucketsEachRows, t_float *mapOf_minTiles_value, t_float *mapOf_minTiles_value_atRowIndex, t_float **matrixOf_locallyTiled_minPairs, s_rowChunk_t *obj_rowChunk); //, uint **matrixOf_locallyTiled_minPairs_columnPos);
  //#endif

#ifdef __cplusplus
}
#endif



#endif //! EOF
