#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()

/**
   @brief present an "Hello World" app where we demonstrate how to use the 'generic' components of the hpLysis cluster-analysis API.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks disclaimer: this use-case is constructed to demonstrate the 'complete application' of generic logic found in our "hpLysis cluster-analysis software". Therefore, this "hello world" use-case-example may exhibit a complexity which may 'see overwhelming'. If latter is the case, we suggest first starting (your learning of hpLysis) with the nine examplies (listed when writing) "ls tut_hp_api_fileInputTight_*.c". 
   @remarks general(API): the logics we use (in this example) are listed in:
   -- <data-types> "types.h" and "def_memAlloc.h": the data-types, such as "uint" and "t_float"; the "t_float" data-type is either "float" or "double", and is used to allow swithcing between different 'needs' for resutl-accuracy (where "t_float = float" is the defualt asisgnment, an assignmetn based upon the needs in the life-science community). 
   -- "hp_api_fileInputTight.h": for cluster-analysis;
   -- "kt_matrix.h": the data-container "s_kt_matrix_t", used for both input, temporary storage, and export; 
   -- "kt_list_1d.h": for 1d-lists, such as "s_kt_list_1d_uint_t" to hold a list of positive integers (ie, "unsigned int").
   @remarks general(use-case): hpLysis is designed to efficiently identify patterns in data. Therefore, we believe an introduction-example should cover these aspects: to relate similarity to predicted clusters through application of well-defined metrics. In this "Hello world" the latter is exemplified: we describe how to: step(a) identify the similarity of the rows in a wild-card input-file (eg, the "IRIS" data-set); step(b) separately hypothesized partitions through application of two different cluster-algorithms; step(3) evaluate accuracy and similarity of cluster-results VS computed similarity-matrix. For the latter "step(3)" we use hpLysis support for "cluster Comparison Metrics (CCMs)": to first separately assess the consistency of each cluster-prediction, and thereafter to compare 'directly' the clsuter-prediction-results using multiple "gold x gold" CCMs. 
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //!
  //! Define an input-file to use in analysis:
  const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 

  //! Load the TSV-file:
  //! Note: advanced options in data-loading conserns noise-injection, both wrt. 'noise-functions' and 'merging of different data-sets'.
  s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(file_name, initAndReturn__s_kt_matrix_fileReadTuning_t()); //! where latter funciton is deifned in our "kt_matrix.h"
  //!
  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(1): compute simliarty-matrix:
  //! Compute simliarty using the MINE simliarty-metric:
  //! Note: for a comhenisve set of simlairty-emtric see our "e_kt_correlationFunction.h".
  s_kt_matrix_t mat_computedSim = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
  bool is_ok = apply__simMetric_dense__hp_api_fileInputTight(/*metric=*/e_kt_correlationFunction_groupOf_MINE_mic, &mat_input, /*mat_input_2=*/NULL, &mat_computedSim);
  assert(is_ok); //! ie, what is expected.
  //!
  //! Export simlairty-metric:
  is_ok = export__singleCall__s_kt_matrix_t(&mat_computedSim, /*result-file=*/"helloWorld_result_simMine.tsv", NULL);
  assert(is_ok); //! ie, what is expected.

  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(2): Apply clustering:
  //! Note: for a comprehensive lsit of the clsuter-algorithm "metric" used in below, see our "hpLysis_api.h".
  //! ------
  //! Step(a): Compute using a "Hierarchical Cluster Algorithm (HCA)":
  //! Note: in below call we implictly use a CCM-based appraoch to 'collect' k-means-clusters for HCA:
  s_kt_matrix_t mat_cluster_hca = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
  is_ok = apply__clustAlg__hp_api_fileInputTight(/*metric=*/e_hpLysis_clusterAlg_HCA_max, /*nclusters=*/UINT_MAX, &mat_input, /*result=*/&mat_cluster_hca);
  assert(is_ok); //! ie, what is expected.
  printf("nrows=%u, ncols=%u, at%s:%d\n", mat_cluster_hca.nrows, mat_cluster_hca.ncols, __FILE__, __LINE__);
  assert(mat_cluster_hca.ncols == 1); //! as we expect the HCA to have been 'translated' into a k-means-clustering-result.
  assert(mat_cluster_hca.nrows == mat_input.nrows); //! as we exepct each 'column' to hold/describe the cluster-id for eahc vertex.
  //! Export cluster-result:
  is_ok = export__singleCall__s_kt_matrix_t(&mat_cluster_hca, /*result-file=*/"helloWorld_result_clustAlg_hca_ccm_kMeans.tsv", NULL); assert(is_ok); //! ie, what is expected.
  //! ------
  //! Step(b): Compute using a "K-means-clustering":
  //! Note: in below call we implictly use a CCM-based appraoch to 'collect' best nclusters-k-means-clusters for k-means:
  s_kt_matrix_t mat_cluster_kMeans = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
  if(false) {
    is_ok = apply__clustAlg__hp_api_fileInputTight(/*metric=*/e_hpLysis_clusterAlg_HCA_average, /*nclusters=*/UINT_MAX, &mat_input, /*result=*/&mat_cluster_kMeans);
    assert(is_ok); //! ie, what is expected.
  } else {
    is_ok = apply__clustAlg__hp_api_fileInputTight(/*metric=*/e_hpLysis_clusterAlg_kCluster__AVG, /*nclusters=*/UINT_MAX, &mat_input, /*result=*/&mat_cluster_kMeans);
    assert(is_ok); //! ie, what is expected.
  }
  assert(is_ok); //! ie, what is expected.
  assert(mat_cluster_kMeans.ncols == 1); //! as we expect the result to 'store' a k-means-clustering-result.
  assert(mat_cluster_kMeans.nrows == mat_input.nrows); //! as we exepct each 'column' to hold/describe the cluster-id for eahc vertex.
  //! Export cluster-result:
  is_ok = export__singleCall__s_kt_matrix_t(&mat_cluster_kMeans, /*result-file=*/"helloWorld_result_clustAlg_ccm_kMeans.tsv", NULL); assert(is_ok); //! ie, what is expected.


  //! ------------------------------------------------------------------------------
  //!
  //! 
  //! Use-case(3): use "Cluster Comparison Metrics (CCMs)" to analyse the agreement between the above clustering:
  //! ------
  //! Pre-step: map the clustering-resutls into 'uint' lists, lists defined in our "kt_list_1d-h":
  assert(mat_input.nrows == mat_cluster_hca.nrows);
  assert(mat_cluster_kMeans.ncols == mat_cluster_hca.ncols);
  assert(mat_cluster_kMeans.nrows == mat_cluster_hca.nrows);
  s_kt_list_1d_uint_t list_hca    = init__s_kt_list_1d_uint_t(mat_cluster_hca.nrows);
  s_kt_list_1d_uint_t list_kMeans = init__s_kt_list_1d_uint_t(mat_cluster_hca.nrows);
  for(uint i = 0; i < list_hca.list_size; i++) {
    if(mat_cluster_hca.matrix[i][0] != T_FLOAT_MAX) { //! then a clsute-rid is set:
      list_hca.list[i] = (uint)mat_cluster_hca.matrix[i][0];
    }
    if(mat_cluster_kMeans.matrix[i][0] != T_FLOAT_MAX) { //! then a clsute-rid is set:
      list_kMeans.list[i] = (uint)mat_cluster_kMeans.matrix[i][0];
    }
  }
  //! ------
  { //! Step(a): assess accuracy of the above cluster-algorithms using the "Silhouette" CCM:
    t_float result_hca = 0;
    //! Comptue for: HCA-result:
    assert(mat_input.nrows == list_hca.list_size);
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_id = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
    bool is_ok = apply__ccm__matrixBased__hp_api_fileInputTight(/*metric=*/ccm_id, /*matrix=*/&mat_computedSim, &list_hca, &result_hca);
    assert(is_ok); //! ie, what is expected.    
    t_float result_kMeans = 0;
    //! Comptue for: kMeans-result:
    assert(mat_input.nrows == list_kMeans.list_size);
    is_ok = apply__ccm__matrixBased__hp_api_fileInputTight(/*metric=*/ccm_id, /*matrix=*/&mat_computedSim, &list_kMeans, &result_kMeans);
    assert(is_ok); //! ie, what is expected.    
    //! 
    //! Write out the result:
    const t_float worst_score_forEnum = getWorstCase_score__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_id); //! where altter function is defined in our "kt_matrix_cmpCluster.h": the interprtations are based on a mnaul investigation of closeness when suing sytnetic-gold-stadnard-datasets for evlauation, a work performed in the PhD work by (oekseth, 06. mar. 2017).
    printf("#\t For input-file=\"%s\" ccm=\"Silhouette\" gives score(HCA)=%f while score(kMeans)=%f. (To know what of the scores which are best, remember that the enum_id 'in question' has a worst-score of '%f'). Observation at %s:%d\n", file_name, result_hca, result_kMeans, worst_score_forEnum, __FILE__, __LINE__);
  }  
  //! ------
  { //! Step(b): describe simlairty of the CCMs using the metrics of: "Rands Index", "Rands Index (alt2)", "Rands Adjusted Index (ARI)", "Fowles-Mallows (FM)", and "Mirkin":
    //! Note: in comparsion of algorithms the use of muliple CCMs are regarded as important, ie, as different CCMs are (belived to) capture different traits clustering-results (eg, wrt. "betweenness" and "within" cluster-distance).
    t_float score_rand = 0;bool is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(/*metric=*/e_kt_matrix_cmpCluster_metric_randsIndex, &list_hca, &list_kMeans, &score_rand); assert(is_ok);
    t_float score_rand_alt = 0; is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(/*metric=*/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, &list_hca, &list_kMeans, &score_rand_alt); assert(is_ok);
    t_float score_ARI = 0; is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(/*metric=*/e_kt_matrix_cmpCluster_metric_randsAdjustedIndex, &list_hca, &list_kMeans, &score_ARI); assert(is_ok);
    t_float score_FM = 0; is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(/*metric=*/e_kt_matrix_cmpCluster_metric_FowlesMallows, &list_hca, &list_kMeans, &score_FM); assert(is_ok);
    t_float score_M = 0; is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(/*metric=*/e_kt_matrix_cmpCluster_metric_Mirkin, &list_hca, &list_kMeans, &score_M); assert(is_ok);
    //! 
    //! Write out the result:
    printf("#\t For input-file=\"%s\", Rands-index='%f', Rands-index(alt)='%f', ARI='%f', FM='%f', Mirkin='%f', at %s:%d\n", file_name, score_rand, score_rand_alt, score_ARI, score_FM, score_M, __FILE__, __LINE__);
  }
  //! De-allocate:
  free__s_kt_list_1d_uint_t(&list_hca);
  free__s_kt_list_1d_uint_t(&list_kMeans);
  free__s_kt_matrix(&mat_input);
  free__s_kt_matrix(&mat_computedSim);
  free__s_kt_matrix(&mat_cluster_kMeans);
  free__s_kt_matrix(&mat_cluster_hca);
  

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
