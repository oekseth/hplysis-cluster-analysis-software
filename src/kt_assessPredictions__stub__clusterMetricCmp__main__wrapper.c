for(uint enum_clusterResultAsInput_index = 0; enum_clusterResultAsInput_index < (uint)e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_undef; enum_clusterResultAsInput_index++) {
  const e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t enum_clusterResultAsInput = (e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput_t)enum_clusterResultAsInput_index;
  const char *stringOf_enum = NULL;
  if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__all) {stringOf_enum = "all";}
  else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__setBased) {stringOf_enum = "setBased";}
  else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased) {stringOf_enum = "matrixBased";}
  else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__subset_extremeCases) {stringOf_enum = "matrixBased_andHpLysisCorrMetrics_extreme";}
  else if(enum_clusterResultAsInput == e_describeClusterCmpSimilarities_forClusterComparisonMetrics__clusterResultAsInput__matrixBased__hpLysisCorrMetrics__all) {stringOf_enum = "matrixBased_andHpLysisCorrMetrics_all";}
  else {assert(false);} //! ie, as we then need to add support 'for this'.
  
  assert(stringOf_enum);
  char stringOf_resultPrefix__sub[2000] = {'\0'}; 
  if(obj_result) {
    sprintf(stringOf_resultPrefix__sub, "groupId_%u.tagLocalIndex_%u.tagNrows_%u.tagNclusters_%u.tagExperiment_%s.tagSample_%s.localMetricData_", obj_result->global_cntMeasurements_currentPos, enum_clusterResultAsInput_index, cnt_vertices, cnt_clusters, stringOf_enum, stringOf_resultPrefix);
  } else {
    sprintf(stringOf_resultPrefix__sub, "tagNrows_%u.tagNclusters_%u.tagExperiment_%s.tagSample_%s.localMetricData_", cnt_vertices, cnt_clusters, stringOf_enum, stringOf_resultPrefix);
  }
  //sprintf(stringOf_resultPrefix__sub, "%s.%s", stringOf_resultPrefix, stringOf_enum);;  
  //! -----------------------------------------------------
  //! -----------------------------------------------------
  //! 
  //! Apply the logics:
  /* assert(row_id_global > 0); */
  /* assert(matrixDimensionsCnt__cols > 0); */
#include "kt_assessPredictions__stub__clusterMetricCmp__main.c"  
 }

if(obj_result) { //! then icnrmenet the 'row-counter':
  obj_result->global_cntMeasurements_currentPos++;
 }
