 
/**

   // FIXME[jc]: may you validate correctness of [below]?

 * Absolute error bounded by 1e-6 for normalized inputs
 Returns a finite number for +inf input
   Returns -inf for nan and <= 0 inputs.
   Continuous error. 
   //! Src: "https://github.com/jhjourdan/SIMD-math-prims/blob/master/simd_math_prims.h"
... Generated in Sollya using :
    > f = remez(log(x)-(x-1)*log(2),
            [|1,(x-1)*(x-2), (x-1)*(x-2)*x, (x-1)*(x-2)*x*x,
              (x-1)*(x-2)*x*x*x|], [1,2], 1, 1e-8);
    > plot(f+(x-1)*log(2)-log(x), [1,2]);
    > f+(x-1)*log(2)

    ... 89.970756366f = 127 * log(2) - constant term of polynomial
*/
#define MiF__logapprox(val) ({ union { t_float f; int i; } valu; valu.f = val; t_float exp = valu.i >> 23; \
  t_float addcst = (val > 0) ? -89.970756366f : T_FLOAT_MIN; valu.i = (valu.i & 0x7FFFFF) | 0x3F800000; t_float x = valu.f; \
  x * (3.529304993f + x * (-2.461222105f + x * (1.130626167f + x * (-0.288739945f + x * 3.110401639e-2f)))) + (addcst + 0.69314718055995f*exp);}) 

// t_float test_value = 0.9; assert((int)log(test_value) == (int)MiF__logapprox(test_value));
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
int main(const int array_cnt, char **array) 
#else
  static int tut_entropy_x_log(const int array_cnt, char **array) 
#endif
{

  //!
  //! Add support for optional arguments:
  //! ----------------------------------
  //!
  //! Then identify and evluate the input-parameters:   
  const char *stringOf_enum = NULL; const char *stringOf_enum_par = "-enum="; 
  uint cnt_cases = 20;  const char *stringOf_cnt_cases_par = "-iter=";
  uint mulSize = 100 * 1000 * 1000; const char *stringOf_block_startPos = "-blockSize=";
  bool use_SSE = true; const char *stringOf_useSSE = "-sse";

  if(array && (array_cnt > 1) ) {
#define __MiF__setIfFound(result, para) ({const char *val = strstr(array[i], para); if(val) {val += strlen(para); result = val;}})
    for(uint i = 1; i < array_cnt; i++) {
      // printf("eval: \"%s\", at %s:%d\n", arg[i], __FILE__, __LINE__);
      __MiF__setIfFound(stringOf_enum, stringOf_enum_par);
      //! -------------- 
      const char *result = NULL;
      __MiF__setIfFound(result, stringOf_cnt_cases_par);       
      if(result) {cnt_cases = (uint)atoi(result);} //! ie, icnrement 'past' the operation.
      //! -------------- 
      result = NULL;
      __MiF__setIfFound(result, stringOf_block_startPos);       
      if(result) {mulSize = (uint)atoi(result);} //! ie, icnrement 'past' the operation.
      //! -------------- 
      result = NULL;
      __MiF__setIfFound(result, stringOf_useSSE);       
      if(result) {use_SSE = !use_SSE;}
    }
#undef __mif__setiffound
  }
  if(!cnt_cases || !mulSize) {
    fprintf(stderr, "!!\t Inptu paramters are invalid: no input-cases identifed: please investigate. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //! Define a wrapper to simplify code logics wrt. evaluation-cases:
#define __MiF__isToEval() ({bool isToEval = true; if(stringOf_enum && strlen(stringOf_enum) ) {isToEval = false; if(0 == strncmp(str_measurement, stringOf_enum, strlen(str_measurement))) {isToEval = true;}} isToEval;})

#define __MiF__endMeasurement__simple() ({ char str_tmp[1000];		\
      sprintf(str_tmp, "[%u]:%s", case_id, str_measurement);		\
      const t_float time_result = end_time_measurement(/*msg=*/str_tmp, prev_time_inSeconds); \
      fprintf(stdout, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);	\
      fprintf(stderr, "(%u, %f) ", cnt_eval, time_result);	             })
#define __MiF__apply() ({		  \
      __MiF__startMeasurement__newCase();		      \
      for(uint case_id = 1; case_id < cnt_cases; case_id++) { \
	cntCases_baseSize += case_id*mulSize; \
	const uint cnt_eval = cntCases_baseSize;	\
	start_time_measurement(); \
	t_float sum = 0; \
	uint i = 0; \
	if(use_SSE) {VECTOR_FLOAT_TYPE vec_result = __setSumSSE(); i = VECTOR_FLOAT_ITER_SIZE; const uint maxPos = cnt_eval - VECTOR_FLOAT_ITER_SIZE; for(; i < maxPos; i += VECTOR_FLOAT_ITER_SIZE) {vec_result = VECTOR_FLOAT_ADD(vec_result, __setSumSSE());} sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);} \
	/*! Comptue for the non-SSE-part: */ \
	for(; i < cnt_eval; i++) { \
	  __setSum(); \
	}					\
	__MiF__endMeasurement__simple();}})
  t_float prev_time_inSeconds = 0; 
  //    const uint mulSize = 10 * 1000 * 1000; 
  {const char *str_measurement = "binomial-p005(..)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({(t_float)binomial(cnt_eval, 0.005);})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "binomial-p05(..)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({(t_float)binomial(cnt_eval, 0.05);})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "log(..)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({mathLib_float_log((t_float)i);})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "log(1/..)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({mathLib_float_log(1.0/(t_float)i);})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "log-approx(1/..)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({MiF__logapprox(1.0/(t_float)i);})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "log(rand(..))";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({mathLib_float_log((t_float)rand());})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "uniform(..)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({uniform();})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      //#define __setSum() ({sum += (t_float)case_id;})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "modulo(rand(), 100)";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({(t_float)(rand() % 100);})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "rand()";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
#define __setSum__v() ({(t_float)rand();})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  {const char *str_measurement = "sum()";
    if(__MiF__isToEval()) {
      uint cntCases_baseSize = 0; 
      //#define __setSum() ({sum += mathLib_float_log((t_float)rand());})
#define __setSum__v() ({(t_float)case_id;})
#define __setSum() ({sum += __setSum__v();})
#define __setSumSSE() ({VECTOR_FLOAT_SET(__setSum__v(), __setSum__v(), __setSum__v(), __setSum__v());})
      __MiF__apply();
#undef __setSum
#undef __setSum__v
#undef __setSumSSE
    }
  }
  #undef __MiF__isToEval

  return 1;
}
