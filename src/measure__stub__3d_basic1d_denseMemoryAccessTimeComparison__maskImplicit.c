{
  { const char *stringOf_measureText = "dense row-iteration-3d(transposed::maskImplicit--naive)"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  if (isOf_interest(matrix_transposed[col_id][row_id]) && isOf_interest(matrix_transposed[col_id][row_id])) {
	    sumOf_values += matrix_transposed[col_id][row_id];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  { const char *stringOf_measureText = "dense row-iteration-3d(transposed::maskImplicit--improved)"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id = 0; row_id < nrows; row_id++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  if(isOf_interest(matrix_transposed[col_id][row_id])) {
	    sumOf_values += matrix_transposed[col_id][row_id];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  { const char *stringOf_measureText = "dense row-iteration-3d(maskImplicit)"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id = 0; row_id < nrows; row_id++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  if (isOf_interest(matrix[row_id][col_id] )) {
	    sumOf_values += matrix[row_id][col_id];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  { const char *stringOf_measureText = "dense row-iteration-3d(maskImplicit--mask(char)-insteadOf-mask(int))"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id = 0; row_id < nrows; row_id++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  if(isOf_interest(matrix[row_id][col_id])) {
	    sumOf_values += matrix[row_id][col_id];
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
}
//! ***************************************
{
  { const char *stringOf_measureText = "dense row-iteration-4d(transposed::maskImplicit--naive): traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
	    if( isOf_interest(matrix_transposed[col_id][row_id]) && isOf_interest(matrix_transposed[col_id_out][row_id_out])) {
	      sumOf_values += matrix_transposed[col_id_out][row_id_out];
	    }
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  { const char *stringOf_measureText = "dense row-iteration-4d(transposed::maskImplicit--improved): traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  if(isOf_interest(matrix_transposed[col_id][row_id])) {
	    for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
	      if( isOf_interest(matrix_transposed[col_id_out][row_id_out])) {
		sumOf_values += matrix_transposed[col_id_out][row_id_out];
	      }
	    }
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  { const char *stringOf_measureText = "dense row-iteration-4d(maskImplicit--naive): traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
	    if( isOf_interest(matrix[row_id][col_id_out]) && isOf_interest(matrix[row_id_out][col_id_out]) ) {
	      sumOf_values += matrix[row_id_out][col_id_out];
	    }
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
  { const char *stringOf_measureText = "dense row-iteration-4d(maskImplicit--improved): traversal-time for a (3d * (i)) symmetirc-matrix-implementaion: examplfieist eh time-cost assicated to Kendall's TAu-computation"; //! For a sparse set:
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	//for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  if(isOf_interest(matrix[row_id][col_id])) {
	    for(uint col_id_out = 0; col_id_out < col_id; col_id_out++) {
	      if ( isOf_interest(matrix[row_id_out][col_id_out])) {
		sumOf_values += matrix[row_id_out][col_id_out];
	      }
	    }
	  }
	}
      }
    }
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 
  }
}
