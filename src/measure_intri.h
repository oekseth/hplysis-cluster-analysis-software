#ifndef measure_intri_h
#define measure_intri_h

#include "def_intri.h"
#include "libs.h"

/**
   @file measure_intri
   @brief include a set of performance-evlauations wrt. basic intrisnitistc using Intels SSE (oekseth, 06. juni 2016).
   @author Ole Kristian Ekseth (oekseth)
 **/

//! The main assert function.
void measure_intri_main(const int array_cnt, char **array);

#endif
