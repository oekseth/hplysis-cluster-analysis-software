

// #include "dbscan.h"

namespace NWUClustering
{

	void run_dbscan_algo_uf(ClusteringAlgo& dbs)
	{			
		int tid, i, pid, j, k, npid, root, root1, root2;
 
        	// initialize some parameters
		dbs.m_clusters.clear();

		// get the neighbor of the first point and print them

		//cout << "DBSCAN ALGORITHMS============================" << endl;

		kdtree2_result_vector ne;
			
		// assign parent to itestf
		dbs.m_parents.resize(dbs.m_pts->m_i_num_points, -1);
		dbs.m_member.resize(dbs.m_pts->m_i_num_points, 0);
		dbs.m_corepoint.resize(dbs.m_pts->m_i_num_points, 0);

		int sch, maxthreads 
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
		  = omp_get_max_threads();
#else
		1;
#endif
		
		if(dbs.m_pts->m_i_num_points % maxthreads == 0)
			sch = dbs.m_pts->m_i_num_points/maxthreads;
		else
			sch = dbs.m_pts->m_i_num_points/maxthreads + 1;
		
		vector < vector <int > > merge;
		vector <int> init;
		merge.resize(maxthreads, init);
		for(tid = 0; tid < maxthreads; tid++)
			merge[tid].reserve(dbs.m_pts->m_i_num_points);
		
		vector < int > prID;
		prID.resize(dbs.m_pts->m_i_num_points, -1);

		vector<int>* ind = dbs.m_kdtree->getIndex();		

		double start = omp_get_wtime();	
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#pragma omp parallel private(root, root1, root2, tid, ne, npid, i, j, pid) shared(sch, ind) //, prID)
#endif
		{
			int lower, upper;
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
			tid = omp_get_thread_num();
#else
			tid = 0;
#endif

        		lower = sch * tid;
	        	upper = sch * (tid + 1);

        		if(upper > dbs.m_pts->m_i_num_points)
		            	upper = dbs.m_pts->m_i_num_points;

	        	for(i = lower; i < upper; i++)
    	    		{
				pid = (*ind)[i]; // CAN TRY RANDOMLY SELECTING
				dbs.m_parents[pid] = pid;
				prID[pid] = tid;
			}

#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
			#pragma omp barrier
#endif
			for(i = lower; i < upper; i++)
			{
				pid = (*ind)[i];

				ne.clear();
            			dbs.m_kdtree->r_nearest_around_point(pid, 0, dbs.m_epsSquare, ne);

				if(ne.size() >= dbs.m_minPts)
				{
					dbs.m_corepoint[pid] = 1;
					dbs.m_member[pid] = 1;
					
					// get the root containing pid
					root = pid;

					for (j = 0; j < ne.size(); j++)
    	            			{
						npid= ne[j].idx;
						if(prID[npid] != tid)
						{
							merge[tid].push_back(pid);
							merge[tid].push_back(npid);
							continue;
						}

						// get the root containing npid
						root1 = npid;
						root2 = root;

						if(dbs.m_corepoint[npid] == 1 || dbs.m_member[npid] == 0)
						{
							dbs.m_member[npid] = 1;
	
							// REMS algorithm to merge the trees
							while(dbs.m_parents[root1] != dbs.m_parents[root2])
							{
								if(dbs.m_parents[root1] < dbs.m_parents[root2])
								{
									if(dbs.m_parents[root1] == root1)
									{
										dbs.m_parents[root1] = dbs.m_parents[root2];
										root = dbs.m_parents[root2];
										break;
									}

				        	                    	// splicing
                	        				    	int z = dbs.m_parents[root1];
				            	                	dbs.m_parents[root1] = dbs.m_parents[root2];
                    	       						root1 = z;
								}
								else
								{
									if(dbs.m_parents[root2] == root2)
									{
										dbs.m_parents[root2] = dbs.m_parents[root1];
										root = dbs.m_parents[root1];
										break;
									}

					   	                        // splicing
				        	                    	int z = dbs.m_parents[root2];
            	                					dbs.m_parents[root2] = dbs.m_parents[root1];					
									root2 = z;
								}
							}
						}
					}
				}
			}
		}

		int v1, v2, size;
		// merge the trees that have not been merged yet
		double stop = omp_get_wtime() ;
		cout << "Local computation took " << stop - start << " seconds." << endl;

		//allocate and initiate locks
    		omp_lock_t *nlocks;
		nlocks = (omp_lock_t *) malloc(dbs.m_pts->m_i_num_points*sizeof(omp_lock_t));

		//start = stop;
		start = omp_get_wtime();

#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
		#pragma omp parallel for private(i) shared(nlocks)
#endif
    		for(i = 0; i < dbs.m_pts->m_i_num_points; i++) 
      			omp_init_lock(&nlocks[i]); // initialize locks

#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
		#pragma omp parallel for shared(maxthreads, merge, nlocks) private(i, v1, v2, root1, root2, size, tid)
#endif
		for(tid = 0; tid < maxthreads; tid++)
		{
			size = merge[tid].size()/2;

			for(i = 0; i < size; i++)
			{
                		v1 = merge[tid][2 * i];
				v2 = merge[tid][2 * i + 1];
		
				int con = 0;
				if(dbs.m_corepoint[v2] == 1)
					con = 1;
				else if(dbs.m_member[v2] == 0)
				{
                			omp_set_lock(&nlocks[v2]);
                    			if(dbs.m_member[v2] == 0) // if v2 is not a member yet
                    			{
                        			con = 1;
						dbs.m_member[v2] = 1;
                    			}
                    			omp_unset_lock(&nlocks[v2]);
				}

				if(con == 1)
				{
				
					// lock based approach for merging
					root1 = v1;
					root2 = v2;

					// REMS algorithm with splicing compression techniques
					while (dbs.m_parents[root1] != dbs.m_parents[root2]) 
					{
						if (dbs.m_parents[root1] < dbs.m_parents[root2])
						{
							if(dbs.m_parents[root1] == root1) // root1 is a root
							{
								omp_set_lock(&nlocks[root1]);
								int p_set = false;
								if(dbs.m_parents[root1] == root1) // if root1 is still a root
								{
									dbs.m_parents[root1] = dbs.m_parents[root2];
									p_set = true;
								}

								omp_unset_lock(&nlocks[root1]);
								if (p_set) // merge successful
    	      								break;
							}
	
							// splicing
							int z = dbs.m_parents[root1];
							dbs.m_parents[root1] = dbs.m_parents[root2];
							root1 = z;
							// root1 = dbs.m_parents[root1];
						}
						else
						{
							if(dbs.m_parents[root2] == root2) // root2 is a root
            	               				{
                	               				omp_set_lock(&nlocks[root2]);
                    	           				int p_set = false;
                        	       				if(dbs.m_parents[root2] == root2) // check if root2 is a root
                            	   				{
       				                         	   	dbs.m_parents[root2] = dbs.m_parents[root1];
                                			   		p_set = true;
	                               				}
    	                           				omp_unset_lock(&nlocks[root2]);
        	                       				if (p_set) // merge successful
            	                       					break;
                	           			}
							
								//splicing
				        		int z = dbs.m_parents[root2];
                           				dbs.m_parents[root2] = dbs.m_parents[root1];
 	                          			root2 = z;

							//root2 = dbs.m_parents[root2];
						}	
					}
				}
			}
		}

		stop = omp_get_wtime();
		free(nlocks);
		cout << "Merging took " << stop - start << " seconds."<< endl;

		for(tid = 0; tid < maxthreads; tid++)
			merge[tid].clear();
		
		merge.clear();
		ne.clear();
	}
   
	void run_dbscan_algo(ClusteringAlgo& dbs)
	{
		// classical DBSCAN algorithm (only sequential)
		int i, pid, j, k, npid;
		int cid = 1; // cluster id
		vector <int> c;
		c.reserve(dbs.m_pts->m_i_num_points);

           	// initialize some parameters
		dbs.m_noise.resize(dbs.m_pts->m_i_num_points, false);
            	dbs.m_visited.resize(dbs.m_pts->m_i_num_points, false);		
		dbs.m_pid_to_cid.resize(dbs.m_pts->m_i_num_points, 0);
		dbs.m_clusters.clear();

		// get the neighbor of the first point and print them

		//cout << "DBSCAN ALGORITHMS============================" << endl;

		kdtree2_result_vector ne;
		kdtree2_result_vector ne2;
		//kdtree2_result_vector ne3;
		ne.reserve(dbs.m_pts->m_i_num_points);
		ne2.reserve(dbs.m_pts->m_i_num_points);

		vector<int>* ind = dbs.m_kdtree->getIndex();

		double start = omp_get_wtime() ;		

		for(i = 0; i < dbs.m_pts->m_i_num_points; i++)
		{
			pid = (*ind)[i];

			if (!dbs.m_visited[pid])
			{
				dbs.m_visited[pid] = true;
				ne.clear();
				dbs.m_kdtree->r_nearest_around_point(/*id=*/pid, 0, dbs.m_epsSquare, /*result=*/ne);
				
				if(ne.size() < dbs.m_minPts)
					dbs.m_noise[pid] = true;
				else
				{
					// start a new cluster
					c.clear();
					c.push_back(pid);
					dbs.m_pid_to_cid[pid] = cid;

					// traverse the neighbors
					for (j = 0; j < ne.size(); j++) {
						npid= ne[j].idx;

						// not already visited
						if(!dbs.m_visited[npid])
						{
							dbs.m_visited[npid] = true;
	
							// go to neighbors
							ne2.clear();
							dbs.m_kdtree->r_nearest_around_point(npid, 0, dbs.m_epsSquare, ne2);

							// enough support
							if (ne2.size() >= dbs.m_minPts)
							{
								// join
								for(k = 0; k < ne2.size(); k++)
									ne.push_back(ne2[k]);
							}
						}

						// not already assigned to a cluster
						if (!dbs.m_pid_to_cid[npid])
						{
							c.push_back(npid);
							dbs.m_pid_to_cid[npid]=cid;
							dbs.m_noise[npid] = false;
						}
					}

					dbs.m_clusters.push_back(c);
					cid++;
				}
					
			}
		}
		
	        double stop = omp_get_wtime();
        	cout << "Local computation took " << stop - start << " seconds." << endl;
		cout << "No merging stage in classical DBSCAN"<< endl;
		ind = NULL;
		ne.clear();
		ne2.clear();
	}
};



                                                                                                                                                                                                                                              
// but if we are not using the rearranged data, then                                                                                                                                                                                                                                         
// we must always                              

/* // ****************************************************************** */



/* // */
/* // search record substructure */
/* // */
/* // one of these is created for each search. */
/* // this holds useful information  to be used */
/* // during the search */



/* static const t_float infinity = 1.0e38; */

/* class searchrecord { */

/* private: */
/*   friend class kt_tree_kd__; */
/*   friend class s_kd_tree_node; */

/*   //vector<float>& qv; */
/*   const t_float *row_1; */

/*   uint dim; */
/*   bool rearrange; */
/*   unsigned uint nn; // , nfound; */
/*   t_float ballsize; */
/*   uint centeridx, correltime; */

/*   kt_tree_kd___result_vector& result;  // results */
/*   const array2dfloat* data;  */
/*   const vector<int>& ind;  */
/*   // constructor */

/* public: */
/*   searchrecord(vector<float>& qv_in, kt_tree_kd__& tree_in, */
/* 	       kt_tree_kd___result_vector& result_in) :   */
/*     qv(qv_in), */
/*     result(result_in), */
/*     data(tree_in.data), */
/*     ind(tree_in.ind)  */
/*   { */
/*     dim = tree_in.dim; */
/*     rearrange = tree_in.rearrange; */
/*     ballsize = infinity;  */
/*     nn = 0;  */
/*   }; */
/* }; */



/* //  */
/* //        S_KD_TREE_NODE implementation */
/* // */

/* /\* // constructor *\/ */
/* /\* s_kd_tree_node::s_kd_tree_node(int dim) : box(dim) {  *\/ */
/* /\*   left = right = ;  *\/ */
/* /\*   // *\/ */
/* /\*   // all other construction is handled for real in the  *\/ */
/* /\*   // kt_tree_kd__ building operations. *\/ */
/* /\*   //  *\/ */
/* /\* } *\/ */


/* /\* // destructor *\/ */
/* /\* s_kd_tree_node::~s_kd_tree_node() { *\/ */
/* /\*   if (left != NULL) delete left;  *\/ */
/* /\*   if (right != NULL) delete right;  *\/ */
/* /\*   // maxbox and minbox  *\/ */
/* /\*   // will be automatically deleted in their own destructors.  *\/ */
/* /\* } *\/ */




/* // FIXME: use enums instead of [below] ....  */


/* void kdtree2::n_nearest(vector<float>& row_1, int nn, kdtree2_result_vector& result) { */
/*   searchrecord sr(row_1,*this,result); */
/*   vector<float> vdiff(dim,0.0);  */

/*   result.clear();  */

/*   sr->centeridx = -1; */
/*   sr->correltime = 0; */
/*   sr->nn = nn;  */

/*   root->search(sr);  */

/*   if (sort_results) sort(result.begin(), result.end()); */
  
/* } */

/* // search for n nearest to a given query vector 'row_1'. */

  
/* void kdtree2::n_nearest_around_point(int idxin, int correltime, int nn, */
/* 				     kdtree2_result_vector& result) { */
/*   vector<float> row_1(dim);  //  query vector */

/*   result.clear();  */

/*   for (int i=0; i<dim; i++) { */
/*     row_1[i] = the_data[idxin][i];  */
/*   } */
/*   // copy the query vector. */
  
/*   { */
/*     searchrecord sr(row_1, *this, result); */
/*     // construct the search record. */
/*     sr->centeridx = idxin; */
/*     sr->correltime = correltime; */
/*     sr->nn = nn;  */
/*     root->search(sr);  */
/*   } */

/*   if (sort_results) sort(result.begin(), result.end()); */
    
/* } */


/* void kdtree2::r_nearest(vector<float>& row_1, float r2, kdtree2_result_vector& result) { */
/* // search for all within a ball of a certain radius */
/*   searchrecord sr(row_1,*this,result); */
/*   vector<float> vdiff(dim,0.0);  */

/*   result.clear();  */

/*   sr->centeridx = -1; */
/*   sr->correltime = 0; */
/*   sr->nn = 0;  */
/*   sr->ballsize = r2;  */

/*   root->search(sr);  */

/*   if (sort_results) sort(result.begin(), result.end()); */
  
/* } */

/* int kdtree2::r_count(vector<float>& row_1, float r2) { */
/* // search for all within a ball of a certain radius */
/*   { */
/*     kdtree2_result_vector result;  */
/*     searchrecord sr(row_1,*this,result); */

/*     sr->centeridx = -1; */
/*     sr->correltime = 0; */
/*     sr->nn = 0;  */
/*     sr->ballsize = r2;  */
    
/*     root->search(sr);  */
/*     return(result.size()); */
/*   }   */
/* } */

/* void kdtree2::r_nearest_around_point(int idxin, int correltime, float r2, */
/* 				     kdtree2_result_vector& result) { */
/*   vector<float> row_1(dim);  //  query vector */

/*   result.clear();  */

/*   for (int i=0; i<dim; i++) { */
/*     row_1[i] = the_data[idxin][i];  */
/*   } */
/*   // copy the query vector. */
  
/*   { */
/*     searchrecord sr(row_1, *this, result); */
/*     // construct the search record. */
/*     sr->centeridx = idxin; */
/*     sr->correltime = correltime; */
/*     sr->ballsize = r2;  */
/*     sr->nn = 0;  */
/*     root->search(sr);  */
/*   } */

/*   if (sort_results) sort(result.begin(), result.end()); */
    
/* } */

/* int kdtree2::r_count_around_point(int idxin, int correltime, float r2)  */
/* { */
/*   vector<float> row_1(dim);  //  query vector */


/*   for (int i=0; i<dim; i++) { */
/*     row_1[i] = the_data[idxin][i];  */
/*   } */
/*   // copy the query vector. */
  
/*   { */
/*     kdtree2_result_vector result;  */
/*     searchrecord sr(row_1, *this, result); */
/*     // construct the search record. */
/*     sr->centeridx = idxin; */
/*     sr->correltime = correltime; */
/*     sr->ballsize = r2;  */
/*     sr->nn = 0;  */
/*     root->search(sr);  */
/*     return(result.size()); */
/*   } */

  
/* } */

