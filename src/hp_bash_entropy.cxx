#include "hp_api_entropy.h"

int main(const int array_cnt, char **array) {
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  //hpLysis__globalInit__kt_api();
  //! *************************************************************************
  //! Apply logics:
  const bool is_ok = fromTerminal__hp_api_entropy(array, array_cnt);
  assert(is_ok);
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  //hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  //! @return:
  return 0;
}
