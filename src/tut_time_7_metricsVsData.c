#include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hp_ccm.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"
#include "hp_clusterShapes.h"

static s_kt_matrix_t t7_initData_default(const uint nrows, const uint ncols) {
  s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols);
  for(uint i = 0; i < nrows; i++) {
    for(uint k = 0; k < ncols; k++) {
      mat_input.matrix[i][k] = (t_float)k;
    }
  }
  return mat_input;
}

static void t7_getScalar_free(s_kt_matrix_t *mat_input, s_kt_matrix_t *mat_data) {
  assert(mat_input);
  assert(mat_data);
  assert(mat_input->matrix != NULL);
  assert(mat_data->matrix != NULL);
  assert(mat_data->matrix != mat_input->matrix);    //! ie, as we (for simplcity) expect unioqueness.
  t_float scalar = 0;
  for(uint i = 0; i < mat_data->nrows; i++) {
    for(uint k = 0; k < mat_data->ncols; k++) {
      scalar += mat_data->matrix[i][k];
    }
  }
  if(scalar == 3.141592) {printf("Dummy: A test to ensure taht the compiler deos not understand that the matricesw are never used, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);} //! ie, as the steps we evlauate may otehrwise be dropped in the calcuation.
  //!
  //! De-allocate:
  free__s_kt_matrix(mat_input); mat_input = NULL;
  assert(mat_data);
  assert(mat_data->matrix != NULL);
  free__s_kt_matrix(mat_data);  mat_data  = NULL;
}

static void t7_exportResult_free(s_kt_matrix_t *mat_result, const char *filePrefix_global, const char *filePrefix_local) {
  //! Task: identify the minimum score in the matrix:
  t_float prop_min = T_FLOAT_MAX;
  for(uint i = 0; i < mat_result->nrows; i++) {
    for(uint k = 0; k < mat_result->ncols; k++) {
      const t_float s = mat_result->matrix[i][k];
      if(isOf_interest(s)) {
	if(s > 0.01) { // TODO: correct to use this offset?
	  prop_min = macro_min(prop_min, s);
	}
      }
    }
  }
  if(prop_min == T_FLOAT_MAX) {
    fprintf(stderr, "Note\t Matrix for case=\"%s\" was empty: drops cosntrcuting a result-file, at [%s]:%s:%d\n", filePrefix_local, __FUNCTION__, __FILE__, __LINE__); //! ie, as the steps we evlauate may otehrwise be dropped in the calcuation.
    //! De-allocate:
    free__s_kt_matrix(mat_result);  mat_result  = NULL;
    return; //! ie, as it is then no point in prinintg our a result-set.
  } else {
    //! NOte: export both verisons iot. get isnights into the acutal time-cost:
    {
      //! Then export:
      char file_name[1000]; memset(file_name, '\0', 1000); sprintf(file_name, "%s-%s-raw.tsv", filePrefix_global, filePrefix_local);
      printf("\t Export file:\"%s\", at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(mat_result, file_name, NULL);
    }
    //! Adjsut the values:
    for(uint i = 0; i < mat_result->nrows; i++) {
      for(uint k = 0; k < mat_result->ncols; k++) {
	const t_float s = mat_result->matrix[i][k];
	if(isOf_interest(s)) {
	  if(s > 0.01) { // TODO: correct to use this offset?
	    mat_result->matrix[i][k] = s/prop_min;
	  }
	}
      }
    }    
    {
      //! Then export:
      char file_name[1000]; memset(file_name, '\0', 1000); sprintf(file_name, "%s-%s-divByMin.tsv", filePrefix_global, filePrefix_local);
      printf("\t Export file:\"%s\", at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(mat_result, file_name, NULL);
    }
  }
  //! De-allocate:
  free__s_kt_matrix(mat_result);  mat_result  = NULL;  
}

#define __MiFLocal__setScore(obj, row_id, col_id, str_local) ({			\
      const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX); \
      assert(row_id < obj.nrows);	      \
      assert(col_id < obj.ncols);	      \
      /*! Store results: */						\
      obj.matrix[row_id][col_id] = time_result; })

#define __MiF__setString__row(obj, index) ({ \
      char tag[1000]; memset(tag, '\0', 1000); sprintf(tag, "nrows=%u", nrows); \
      assert(index < obj.nrows);			\
      set_stringConst__s_kt_matrix(&(obj), index, tag, /*addFor_column=*/false); } )
#define __MiF__setString__col(obj, index, tag) ({ \
      assert(index < obj.ncols);			\
      set_stringConst__s_kt_matrix(&(obj), index, tag, /*addFor_column=*/true); } )

//! Set the column-header:
static void t7_initResult_matrixHeader(s_kt_matrix_t *mat_result_time, const uint cnt_measurements_size, const uint nrows, const uint ncols) {
  uint col_id_local = 0;
  for(uint clust_alg = 0; clust_alg < cnt_measurements_size; clust_alg++) {
    //const e_hpLysis_clusterAlg_t enum_clust = (e_hpLysis_clusterAlg_t)clust_alg;
    char tag_local[4000]; memset(tag_local, '\0', 4000);
      sprintf(tag_local, "%u", clust_alg);
    //sprintf(tag_local, "%u,%u", nrows, ncols); //! ie, the dimension.
    //sprintf(tag_local, "%s_%s", get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(obj_sim.metric_id), get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust));
    __MiF__setString__col((*mat_result_time), /*index=*/col_id_local, tag_local);
    col_id_local++;
  }
}

// ***********************************************************'
struct tut_time7_ccmAlgDataSyn_algs {
  const char *str; int alg_id; uint niter; bool exploreAllSimMetrics;
};


//! @remarks this funciton explreos 20 different permutations of the disjoitn-aritcila-data
//! Note: this function is a permtaution of "tut_ccm_17_ccmAlgDataSyn.c"
static void __tut_time7__apply(const char *filePrefix, const uint nrows, const uint ncols, const uint cnt_measurements_size) {
  //static void __tut_time7__apply(const char *resultPrefix, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const bool onlyComputeFor_fixedInputMatrix) {
  //static void __tut_time7__apply(s_kt_matrix_setOf_t *mat_collection, s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix) {
#if false
  const char *filePrefix_local = "algCase1";  
  const uint map_algList_size = 6;
  struct tut_time7_ccmAlgDataSyn_algs map_algList[map_algList_size] = {
    //    e_alg_dbScan_brute_linkedListSet,
    {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, /*niter=*/100, false},
    {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
    {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
    {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
    {"miniBatch-100", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/100, false},
    {"miniBatch-1000", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/1000, false},
  };
#else
  const char *filePrefix_local = "algCase2";  
  const uint map_algList_size = 29 - 4;
  //const uint map_algList_size = 29;
  struct tut_time7_ccmAlgDataSyn_algs map_algList[map_algList_size] = {
    //    e_alg_dbScan_brute_linkedListSet,
    {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, /*niter=*/100, false},
    {"disjoint-DBSCAN-direct", e_hpLysis_clusterAlg_disjoint_DBSCAN, /*niter=*/100, false},
    {"disjoint-hpCluster-direct-mclPost", e_hpLysis_clusterAlg_disjoint_mclPostStrategy, /*niter=*/100, false},
    {"disjoint-DBSCAN-dynamic", e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, /*niter=*/100, false},
    {"disjoint-hpCluster-dynamic", e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, /*niter=*/100, false},
    {"disjoint-hpCluster-dynamic-bestSim", e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, /*niter=*/100, true},
    {"disjoint-KD-hpCluster-direct", e_hpLysis_clusterAlg_disjoint_kdTree, /*niter=*/100, false},
    {"disjoint-KD-hpCluster-dynamic", e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*niter=*/100, false},
    {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
    {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
    {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
    {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
    /* {"k-means-500", e_hpLysis_clusterAlg_kCluster__AVG, /\*niter=*\/500, false}, */
    /* {"k-means-500", e_hpLysis_clusterAlg_kCluster__AVG, /\*niter=*\/500, false},         */
    /* {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /\*niter=*\/1000, false}, */
    /* {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /\*niter=*\/1000, false}, */
    {"k-rank-100", e_hpLysis_clusterAlg_kCluster__rank, /*niter=*/100, false},
    {"k-rank-100", e_hpLysis_clusterAlg_kCluster__rank, /*niter=*/100, false},
    {"k-medoid-100", e_hpLysis_clusterAlg_kCluster__medoid, /*niter=*/100, false},
    {"k-medoid-100", e_hpLysis_clusterAlg_kCluster__medoid, /*niter=*/100, false},
    {"miniBatch-10", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/10, false},
    {"miniBatch-10", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/10, false},
    {"miniBatch-100", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/100, false},
    {"miniBatch-1000", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/1000, false},
    {"HCA-single", e_hpLysis_clusterAlg_HCA_single, /*niter=*/100, false},
    {"HCA-centroid", e_hpLysis_clusterAlg_HCA_centroid, /*niter=*/100, false},
    {"HCA-max",  e_hpLysis_clusterAlg_HCA_max,/*niter=*/100, false},
    {"HCA-average", e_hpLysis_clusterAlg_HCA_average, /*niter=*/100, false},
    {"HCA-Kruskal",  e_hpLysis_clusterAlg_kruskal_hca, /*niter=*/100, false},
    // {"HCA-Kruskal-dynamic", e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, /*niter=*/100, false},
    //!    {"disjoint-hpCluster-direct",  /*niter=*/100, false},
    //!    {, false},   
  };
#endif

  //!
  const uint cnt_cases_size = map_algList_size;
  s_kt_matrix_t mat_result_time = initAndReturn__s_kt_matrix(/*nrows=*/cnt_cases_size, /*ncols=*/cnt_measurements_size);
  //! 
  //! Task: init matrix-header for muliple column-fractions:
  t7_initResult_matrixHeader(&mat_result_time, cnt_measurements_size, nrows, ncols);

  //! Iterate through cases wrt. matrix-cluster-seperation:
  uint dim_index = 0;
  //for(uint case_id = 0; case_id < cnt_cases; case_id++) {
  //printf("case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
  //uint nrows = nrows_increase + (block_id * nrows_increase);
  assert(nrows != UINT_MAX);
  { 
    /* { //! then update the column-header:  */
    /*   allocOnStack__char__sprintf(2000, str, "tag:\"%s\";cnt-clusters=%u;nrows=%u", fileTag, cnt_clusters, nrows); */
    /*   set_stringConst__s_kt_matrix(&mat_resultCCM, /\*index=*\/dim_index, str, /\*addFor_column=*\/false); */
    /*   set_stringConst__s_kt_matrix(&mat_resultTime, /\*index=*\/dim_index, str, /\*addFor_column=*\/false); */
    /* } */
    //!
    t_float time_alg_first = T_FLOAT_MAX;
    //! Iterate through the algorithms:
    for(uint alg_id = 0; alg_id < map_algList_size; alg_id++) {
      const uint enum_id = alg_id;
      //! Construct the default prefix: 
      //if(dim_index == 0) { //! then update the column-header: 
      const char *str_obj = map_algList[alg_id].str;
      {
	allocOnStack__char__sprintf(2000, str, "%s", str_obj);
	set_stringConst__s_kt_matrix(&(mat_result_time), enum_id, str, /*addFor_column=*/false);	      ;
      }
	      
      //! Iterate through the clusters: 
      uint cnt_measurements = 0; //! a 'dummy' varialbe used to simpliyf future code-updates.
      for(uint block_id = 0; block_id < cnt_measurements_size; block_id++) {
	const uint cnt_clusters = (1 + block_id);
	//!
	s_kt_list_1d_uint_t clusters = setToEmpty__s_kt_list_1d_uint_t();	    
	t_float time_result = 0; //start_time_measurement();
	//! 
	//! Start: time-measurements:
	start_time_measurement();
	//!
	{ //! Compute clusters:
	  const e_hpLysis_clusterAlg_t alg_enum = (e_hpLysis_clusterAlg_t)map_algList[alg_id].alg_id;
	  fprintf(stderr, "\t Compute for algorithm:\"%s\", at %s:%d\n", map_algList[alg_id].str, __FILE__, __LINE__);
	  assert(alg_enum != e_hpLysis_clusterAlg_undef); //! was this would indicate an error.
	  //! Note: the resutls differes as different default cofniguraiton-settings are used. For details of the latter, see the "obj_hp.config.kdConfig" variable
	  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	  // time_result = (t_float)end_time_measurement(/*msg=*/map_algList[alg_id].str, FLT_MAX);	      
	  //! 
	  //! Apply logics:

	  if(false) {
	    obj_hp.config.corrMetric_prior_use = false;  //! ie, use data-as-is.
	    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //isAn__adjcencyMatrix;
	  } else {
	    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //isAn__adjcencyMatrix;
	    obj_hp.config.corrMetric_prior.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;		  
	    obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = false;
	  }
	  //uint ncols = nrows;
	  if(true) { //! then we explroe other alternatives:
	    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //isAn__adjcencyMatrix;
	    obj_hp.config.corrMetric_prior_use = true;  
	    //ncols = 3;
	    //		  ncols = 10;
	    //		  ncols = 24;
	  }
	  //!
	  s_kt_matrix_base_t mat_sample = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
	  s_kt_matrix_t mat_shal = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_sample);
	  //! 
	  printf("\t\t Start: compute-cluster, at %s:%d\n", __FILE__, __LINE__);		
	  time_result = 0; start_time_measurement();
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_enum, &(mat_shal), /*nclusters=*/cnt_clusters, /*npass=*/map_algList[alg_id].niter);
	  //! Stop measurements: 
	  const t_float time_result_local = (t_float)end_time_measurement(/*msg=*/map_algList[alg_id].str, FLT_MAX);	      
	  time_result = time_result_local; //! ie, updat ethe time-emasurement.
	  printf("\t\t OK:    compute-cluster, at %s:%d\n", __FILE__, __LINE__);
	  assert(is_ok);
	  free__s_kt_matrix_base_t(&mat_sample);	       
	  if(alg_id != 0) {
	    printf("\t\t Diff: %f\n", time_result / time_alg_first);
	  } else {
	    time_alg_first = time_result;
	  }
	  printf("--------------------\n");
	  // FIXME: ... 
	  //mat_resultTime.matrix[dim_index][alg_id] = time_result;

	  clusters = getClusters__hpLysis_api(&obj_hp);
	  //!
	  { //! To avoid the compilation from wrongly removing this code-chunk, we ... 
	    t_float max_clusterId = 0;
	    for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
	      max_clusterId = macro_max(max_clusterId, (t_float)clusters.list[row_id]);
	    }
	    if(max_clusterId == 3.141592) {printf("Dummy: A test to ensure taht the compiler deos not understand that the matricesw are never used, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);} //! ie, as the steps we evlauate may otehrwise be dropped in the calcuation.


	    //! Update the internal object:
	    free__s_kt_list_1d_uint_t(&clusters);

	  }
	  //! Get the CCM-score:
	  // t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
	  //! Get the clusters:
	  //! De-allocte:
	  free__s_hpLysis_api_t(&obj_hp);
	}
	{
	  //! ----------
	  //!
	  //! Step: stop time-measurement
	  //!
	  __MiFLocal__setScore(mat_result_time, /*case=*/enum_id, /*measurement=*/cnt_measurements, str_obj);
	  //!
	  cnt_measurements++;	    
	}
      } //! end: clsuters elvauted for a given algorithm.
      //! and at this poin the algorithms are evaluated.
      //! Increment the deimsnion-counter:
      dim_index++;	    
    }
  }
  // const uint cnt_clusters = case_id*config_clusterBase;
  //! *************************************************************************************** 
  //! 
  //! De-allocate: 
  //free__s_hp_clusterShapes_t(&obj_shape);      
  //! -------------------------------------------
  //! -------------------------------------------
  { //! Write out 'raw' matrix:
    t7_exportResult_free(&mat_result_time, filePrefix, filePrefix_local);    
  }
}

// ******************************************************************


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy/**
/*
   @brief Identify the time-cost of different base-buildign-block strategies
   @author Ole Kristian Ekseth (oekseth, 6. Jan. 2021).
   @remarks 
   -- result: construct a table for name([data-norm, sim-metric, CCM, alg]) x time(data(synt))
   @remarks Related tutorials:
   -- "tut_kd_1_cluster_multiple_simMetrics.c": exemplifes the use of data-normalization in evlaution of KD-trees; demosntrtes how muliple simlairty-metircs and CCMs may be used during the simliarty-metric-evaluation.
   -- "tut_sim_16_manyMany_singleInput_allMetrics_ccmEval.c": demonstrates how similartiy-metrics combiend with a CCM may be used to describe the simlairty between two matrices;
   -- "tut_ccm_17_ccmAlgDataSyn.c":  ... algorithms ... 
   -- "": 
   -- "": 
   -- "": 
   @remarks compile:
   -- g++ -O2 -g tut_time_7_metricsVsData.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_t-7.exe
   -- g++ -O2 -g  -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_16.exe
**/
int main() 
#else
  static void tut_time_7_metricsVsData(const char *filePrefix) //, const uint config_cnt_casesToEvaluate, const uint config_nrows_base, const uint config_clusterBase, const bool isTo_useRverseOrder_inInit, const bool isTo_setClusterSzei_toFixed_sizes, const t_float score_weak)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const char *filePrefix = "r-t-7";
  //  const char *filePrefix = "ccm_16_compareMatrixAndGold_twoVectors";
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif



  
  //! -------------------------------------------------
  if(false) // FIXME: remove!
  { //! Task: data-normalization:
    const uint cnt_cases_size        = (e_kt_normType_undef + 1);
    const uint cnt_measurements_size = 1;
    s_kt_matrix_t mat_result_time = initAndReturn__s_kt_matrix(/*nrows=*/cnt_cases_size, /*ncols=*/cnt_measurements_size);
    //!
    const uint nrows = 1000; const uint ncols = 1000;
    //! Set the column-header:
    t7_initResult_matrixHeader(&mat_result_time, cnt_measurements_size, nrows, ncols);

    //!
    //!
    for(uint enum_id = 0; enum_id <= e_kt_normType_undef; enum_id++) {
      //!
      { //! Set: row-header:
	const char *str = get_str__humanReadable__e_kt_normType_t((e_kt_normType_t)enum_id);
	assert(enum_id < mat_result_time.nrows);	
	set_stringConst__s_kt_matrix(&(mat_result_time), enum_id, str, /*addFor_column=*/false);
      }
      //! 
      //! Start: time-measurements:
      start_time_measurement();
      uint cnt_measurements = 0; //! a 'dummy' varialbe used to simpliyf future code-updates.
      //!
      for(uint times = 0; times < 100; times++) { //! ie, apply the steps muliple times to avoid the time-cost to approxmiate zero.
	s_kt_matrix_t mat_input = t7_initData_default(nrows, ncols);
	//! Apply data-noramlization:
	if(enum_id != e_kt_normType_undef) {
	  s_kt_matrix_t mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&(mat_input), (e_kt_normType_t)enum_id);
	  t7_getScalar_free(&mat_input, &mat_data);
	} else { //! then set the valeus to a 'signle case': used to investgate the case where no dat-anrosiaotn is applied, ie, a ground-zero reference-point.
	  s_kt_matrix_t mat_data = t7_initData_default(nrows, ncols);
	  t7_getScalar_free(&mat_input, &mat_data);
	}
      }
      //! ----------
      //!
      //! Step: stop time-measurement
      const char *str_local = get_str__humanReadable__e_kt_normType_t((e_kt_normType_t)enum_id);
      //!
      __MiFLocal__setScore(mat_result_time, /*case=*/enum_id, /*measurement=*/cnt_measurements, str_local);
    }
    //!
    //! Step: Export matrix, and then de-allocate:
    const char *str_local = "data-norm";
    t7_exportResult_free(&mat_result_time, filePrefix, str_local);
  }
  //! -------------------------------------------------
  //if(false) // FIXME: remove!
  for(uint ncols = 128; ncols < 1280; ncols += 128) {
    { //! Task: sim-metric:
#define __MiF__isTo_evaluate__local(sim_id) ((sim_id < e_kt_correlationFunction_groupOf_MINE_mic))
      //#define __MiF__isTo_evaluate__local(sim_id) (!isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id))
      uint cnt_cases_size        = 0;
      for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
	for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
	  if(__MiF__isTo_evaluate__local(sim_id)) { cnt_cases_size++; }
	}
      }
      const uint cnt_measurements_size = 1;
      s_kt_matrix_t mat_result_time = initAndReturn__s_kt_matrix(/*nrows=*/cnt_cases_size, /*ncols=*/cnt_measurements_size);
      //!
      //! Note: pow(2, 13) = 8192
      //const uint nrows = 8192; const uint ncols = 1024;
      const uint nrows = 1024;
      // const uint ncols = 128;
      //const uint nrows = 1000; const uint ncols = 1000;
      //! Set the column-header:
      t7_initResult_matrixHeader(&mat_result_time, cnt_measurements_size, nrows, ncols);


      //!
      //!
      uint enum_id = UINT_MAX;
      for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++) {
	e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
	for(uint  metric_idx = 0; metric_idx < (uint)e_kt_correlationFunction_undef; metric_idx++) {  
	  e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_idx;
	  //!
	  //!
	  //! Construct the reference-string:
	  char str_obj[1000]; 	
	  { //! Set: row-header:
	    char str_pre[1000]; memset(str_pre, '\0', 1000);
	    // printf("typeOf_correlationPreStep=%u, at %s:%d\n", typeOf_correlationPreStep, __FILE__, __LINE__);
	    if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {sprintf(str_pre, "rank");}
	    else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {sprintf(str_pre, "binary");}
	    else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none) {sprintf(str_pre, "none");}
	    else {assert(false);} //! ie, then add support for this.
	    //!
	    const char *str_metric = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id);
	    //!
	    //! Combine 'pre' and 'metric-base';
	    memset(str_obj, '\0', 1000); sprintf(str_obj, "%s %s", str_pre, str_metric);
	  }	
	  //!
	  if(__MiF__isTo_evaluate__local(metric_id)) {
	    if(enum_id != UINT_MAX) {
	      enum_id++;
	    } else {enum_id = 0;} //! ie, then the first index.
	  } else {
	    fprintf(stderr, "does NOT evaluate for %s, at %s:%d\n", str_obj, __FILE__, __LINE__);
	    continue;}	
	  assert(__MiF__isTo_evaluate__local(metric_id) == true); //! ie, what we expect
	  {
	    //!
	    assert(enum_id < mat_result_time.nrows);	
	    set_stringConst__s_kt_matrix(&(mat_result_time), enum_id, str_obj, /*addFor_column=*/false);
	  }
	  s_kt_correlationMetric_t obj_metric = initAndReturn__s_kt_correlationMetric_t(metric_id, typeOf_correlationPreStep);
	  //! 
	  //! Start: time-measurements:
	  start_time_measurement();
	  uint cnt_measurements = 0; //! a 'dummy' varialbe used to simpliyf future code-updates.
	  //!
	  //for(uint times = 0; times < 100; times++) { //! ie, apply the steps muliple times to avoid the time-cost to approxmiate zero.
	  //if(false) // FIXME: remove
	  {
	    s_kt_matrix_t mat_input = t7_initData_default(nrows, ncols);
	    //! Apply data-noramlization:
	    {
	      //!
	      //! Apply logics:
	      s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
	      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_input);
	      s_kt_matrix_base_t vec_result = initAndReturn__empty__s_kt_matrix_base_t();
	      // hp_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index; //! ie, an appraoch to reduce data-ndimesnality; used in algorithms such as minibatch and k-means++
	      const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_1,  &vec_result, /*config=*/hp_config);	    
	      assert(is_ok);
	      //!
	      s_kt_matrix_t mat_data = get_deepCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	      //!
	      //! De-allocate:
	      //! Note: two steps are used to simplify standardization.
	      free__s_kt_matrix_base_t(&vec_result);
	      t7_getScalar_free(&mat_input, &mat_data);	    
	    }
	  }
	  //! ----------
	  //!
	  //! Step: stop time-measurement
	  //const char *str_local = get_str__humanReadable__e_kt_normType_t((e_kt_normType_t)enum_id);
	  //!
	  __MiFLocal__setScore(mat_result_time, /*case=*/enum_id, /*measurement=*/cnt_measurements, str_obj);
	}
      }
      //!
      //! Step: Export matrix, and then de-allocate:
      const char *str_local = "sim-metric";
      char filePrefix_local[1000];
      memset(filePrefix_local, '\0', 1000); sprintf(filePrefix_local, "%s-%u", str_local, ncols);
      t7_exportResult_free(&mat_result_time, filePrefix, filePrefix_local);
#undef __MiF__isTo_evaluate__local
    }
    //!
    //!
    //!     
    // ***********************************************************************************************************'
    //! ----------------------------------------------- CCM: matrix
    { 
      // const uint config_cnt_casesToEvaluate = (uint)/*ccm-gold=*/e_kt_matrix_cmpCluster_metric_undef + /*ccm-matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef + list_metrics_size;
      uint cnt_cases_size        = e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
      const uint cnt_measurements_size = 10;
      s_kt_matrix_t mat_result_time = initAndReturn__s_kt_matrix(/*nrows=*/cnt_cases_size, /*ncols=*/cnt_measurements_size);
      //!
      //! Note: pow(2, 13) = 8192
      //const uint nrows = 8192; const uint ncols = 1024;
      const uint nrows = 1024;
      // const uint ncols = 128;
      //const uint nrows = 1000; const uint ncols = 1000;
      //! Set the column-header:
      //! 
      //! Task: init matrix-header for muliple column-fractions:
      t7_initResult_matrixHeader(&mat_result_time, cnt_measurements_size, nrows, ncols);


      //!
      //!
      uint enum_id = UINT_MAX;
      //for(uint pre_id = 0; pre_id < e_kt_categoryOf_correaltionPreStep_none; pre_id++)
      {
	//e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)pre_id;
	for(uint  metric_idx = 0; metric_idx < (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; metric_idx++) {  
	  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t metric_id = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)metric_idx;
	  //!
	  //!
	  //! Construct the reference-string:
	  char str_obj[1000]; 	
	  { //! Set: row-header:
	    //!
	    //! Combine 'pre' and 'metric-base';
	    memset(str_obj, '\0', 1000); sprintf(str_obj, "%s", getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(metric_id));
	  }	
	  //!
	  {
	    if(enum_id != UINT_MAX) {
	      enum_id++;
	    } else {enum_id = 0;} //! ie, then the first index.
	  }
	  {
	    //!
	    assert(enum_id < mat_result_time.nrows);	
	    set_stringConst__s_kt_matrix(&(mat_result_time), enum_id, str_obj, /*addFor_column=*/false);
	  }
	  //s_kt_correlationMetric_t obj_metric = initAndReturn__s_kt_correlationMetric_t(metric_id, typeOf_correlationPreStep);
	  uint cnt_measurements = 0; //! a 'dummy' varialbe used to simpliyf future code-updates.
	  //!
	  //for(uint times = 0; times < 100; times++) { //! ie, apply the steps muliple times to avoid the time-cost to approxmiate zero.
	  //! 
	  //! Task: iterate through different column-fracitonts
	  for(uint block_id = 0; block_id < mat_result_time.ncols; block_id++) {
	    //! Task: construct a sytentic matrix-objecT:
	    //! 
	    //! Start: time-measurements:
	    start_time_measurement();
	    //! 
	    //! Task:
	    t_float score_weak   = 100;
	    t_float score_strong = 1;
	    const uint cnt_clusters = (block_id + 1);
	    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/score_strong, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	    assert(is_ok);
	    //s_kt_matrix_t *mat_export = &(obj_shape.matrix);	
	    s_kt_matrix_t mat_input = obj_shape.matrix; //t7_initData_default(nrows, ncols);
	    //! Apply metric:
	    //! 
	    
	    {
	      //!
	      //! Apply logics:
	      s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
	      s_kt_matrix_base_t vec_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_input);
	      s_kt_matrix_base_t vec_result = initAndReturn__empty__s_kt_matrix_base_t();
	      // hp_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index; //! ie, an appraoch to reduce data-ndimesnality; used in algorithms such as minibatch and k-means++
	      t_float result_score = 0.1;
	      const bool is_ok = ccm__singleMatrix__hp_ccm(metric_id, &vec_1, obj_shape.map_vertexInternalToRealWorld, &result_score);
	      if(result_score == 3.141592) {printf("Dummy: A test to ensure taht the compiler deos not understand that the matricesw are never used, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);} //! ie, as the steps we evlauate may otehrwise be dropped in the calcuation.
	      assert(is_ok);
	      //!
	      //s_kt_matrix_t mat_data = get_deepCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_result);
	      //!
	      //! De-allocate:
	      //! Note: two steps are used to simplify standardization.
	      //free__s_kt_matrix_base_t(&vec_1);
	      //free__s_kt_matrix(&mat_input);
	      //t7_getScalar_free(&mat_input, &mat_data);	    
	    }
	    free__s_hp_clusterShapes_t(&obj_shape);
	    //! ----------
	    //!
	    //! Step: stop time-measurement
	    //const char *str_local = get_str__humanReadable__e_kt_normType_t((e_kt_normType_t)enum_id);
	    //!
	    __MiFLocal__setScore(mat_result_time, /*case=*/enum_id, /*measurement=*/cnt_measurements, str_obj);
	    //!
	    cnt_measurements++;
	  }
	}
      }
      //!
      //! Step: Export matrix, and then de-allocate:
      const char *str_local = "ccm-metric-matrix";
      char filePrefix_local[1000];
      memset(filePrefix_local, '\0', 1000); sprintf(filePrefix_local, "%s-%u", str_local, ncols);
      t7_exportResult_free(&mat_result_time, filePrefix, filePrefix_local);
#undef __MiF__isTo_evaluate__local      
    }
    //!
    //!
    //!     
    // ***********************************************************************************************************'
    //! ----------------------------------------------- alg: basic:
    {
      const uint cnt_measurements_size = 10;
      const uint nrows = 1024;      
      const char *str_local = "clust-basic";
      //!
      char filePrefix_local[1000];
      memset(filePrefix_local, '\0', 1000); sprintf(filePrefix_local, "%s-%u", str_local, ncols);
      //! Apply:
      __tut_time7__apply(filePrefix_local, nrows, ncols, cnt_measurements_size);
    }
  }
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! -----------
#undef __MiFLocal__setScore
#undef __MiF__setString__row
#undef __MiF__setString__col
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
  

