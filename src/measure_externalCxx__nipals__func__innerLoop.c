{

    kt_mathMacros_SSE__row_copy(row_t, row_t0, ncols_adjusted);  //! where "row_t0" is updated.

    kt_mathMacros_SSE__matrixTransposed_mult_vector(matrix_x, nrows_adjusted, ncols_adjusted, row_t, row_v); //! a 2d-loop with a simple mulitiplication: "B_mod[i] += X[i][j] * B[j]";
    kt_mathMacros_SSE__normalize(row_v, row_w, ncols_adjusted); //! w[u] = B_mod[i] / sqrt(\sum(B_mod[i] * B_mod[i]))

     
    kt_mathMacros_SSE__matrix_mult_vector(matrix_x, nrows_adjusted, ncols_adjusted, row_w, row_v); //! ie, "MultiplyMatrixbyVector(X, w, Vrow);", ie, "b_mod2[j] += X[i][j] * w[j]"
    // normaliz(Vrow, t);
    kt_mathMacros_SSE__normalize(row_v, row_t, ncols_adjusted); //! w[u] = B_mod[i] / sqrt(\sum(B_mod[i] * B_mod[i]))


     


    // const t_float tmpscalar = MultiplyVectorTransposedbyVector(Y, t); 
    // const t_float c = (tmpscalar != 0) ? tmpscalar/tmpscalar;  //dummy step, because it normalizes a constant
			
    // u=Yres*c
    //free(u);
    //
    //kt_mathMacros_SSE__matrix_mult_
    /**
     // FIXME[JC]: validate that [”elow] call correpsodns to:  
     c = tmpscalar/tmpscalar;  //dummy step, because it normalizes a constant;
     <-- an 'alternative' is      
     // kt_mathMacros_SSE__vector_mult_vector(row_y, ncols_adjusted, row_t); //! \sum( Y[i] * t[i]);
     -- through teh latter is 'poitnless', ie, as 'teh call' will result ina s asclaar, a sclar which si Not used.
    **/
    kt_mathMacros_SSE__row_copy(row_y, row_u, nrows_adjusted);  //! ie, MultiplyVectorandScalar(Y, c, u); //! u[i] = Y[i]*c, where c=1 = tmpscalar/tmpscalar;
    //! ie, 
			


    scalar_convergence_dt = 0;
    for (uint kk = 0; kk < ncols; kk++) {
      const t_float diff = row_t0[kk] - row_t[kk];
      scalar_convergence_dt += diff*diff;
      //	dt += (t0->GetElement(kk) - t->GetElement(kk)) * (t0->GetElement(kk) - t->GetElement(kk)); //! ie, "dt = sum( (t - t0)^2 )"
    }
      
    //if (cvIsNaN((double)dt)) {
    //    if(dt == 0) {
      //printf("dt=0, at %s:%d\n", __FILE__, __LINE__);
    	/* printf("\nproblem during PLS: NaN\n"); */
    	/* exit(2); */
    // }
    //printf("t - t0: %f\n", dt);
                       
    step++;
}
