#ifndef e_kt_set_sparse_colAttribute_h
#define e_kt_set_sparse_colAttribute_h
 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file e_kt_set_sparse_colAttribute
   @brief idnetifies the positions in a dense list of elements.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
/**
   @enum e_kt_set_sparse_colAttribute
   @brief defines the row-attriubte to be used when accessing the saprse data-set
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016)
 **/
typedef enum e_kt_set_sparse_colAttribute {
  e_kt_set_sparse_colAttribute_size,
  e_kt_set_sparse_colAttribute_currentPos,
  e_kt_set_sparse_colAttribute_undef
} e_kt_set_sparse_colAttribute_t;


#endif //! EOF
