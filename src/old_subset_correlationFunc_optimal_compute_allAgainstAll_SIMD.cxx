else {
    

    // FIXME: update [abov€] and [”elow] based on results from measurements of [ªbov€] (eg, wrt. "e_typeOf_optimization_distance_none_xmtOptimizeTransposed").
    bool debug_lastRow_seen = false;
    const uint total_cnt_iterations = nrows * ncols * ncols;
    //! Iterate:

    // FIXME: update [below] adn [ªbove] by 'computing only for index2 <= index1'

    // FIXME[jan-christian]: may you verify correctness of [below] 'euclidiaon transpsoe through memory-tiling'?

    //! Handle the case where the 'second tieration' is in a different name-space (eg, comaprsion of two different matrices as seen in the "kemans" algorithm).
    if(iterationIndex_2 == UINT_MAX) {iterationIndex_2 = ncols;}
    uint numberOf_chunks_cols_2 = iterationIndex_2 / SM;  
    uint chunkSize_col_last_2 = SM; __get_updatedNumberOF_lastSizeChunk_entry(iterationIndex_2, SM, numberOf_chunks_cols_2, chunkSize_col_last_2);


    // FIXME: try to improve [”elow] parallisation
    //#pragma omp parallel //for schedule(dynamic,3) //! where "3" is based on observaitons
    for(uint cnt_i = 0, i=0; cnt_i < numberOf_chunks_rows; cnt_i++, i += SM)  {
      const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_rows) ? SM : chunkSize_row_last;
	
      if(chunkSize_cnt_i != SM) {
	if(false) {printf("(info)\t last-row for i=%u, cnt_i=%u, and numberOf_chunks_rows=%u, at %s:%d\n", i, cnt_i, numberOf_chunks_cols, __FILE__, __LINE__);}
	assert(debug_lastRow_seen == false); debug_lastRow_seen = true;
      }
      if(needTo_useMask_evaluation == false) {
	for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_cols; cnt_index1++, index1 += SM) {
	  const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	  //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
	  uint numberOf_chunks_cols_local = 0; uint chunkSize_cols_last_local = 0;  
	  if(data1 == data2) {
	    __get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, numberOf_chunks_cols_local, chunkSize_cols_last_local);
	  } else { //! else the matrix is assumed not to be symmetric.
	    numberOf_chunks_cols_local = numberOf_chunks_cols_2; chunkSize_cols_last_local = chunkSize_col_last_2;
	  }
	  for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_cols_local; cnt_index2++, index2 += SM) {
	    const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_cols_local) ? SM : chunkSize_cols_last_local;
	    const uint chunkSize_index2_intris = (chunkSize_index2 == SM) ? chunkSize_index2 : 
	      //! Then we idneitfy the size of the inner intristinc code-block: 
	      (chunkSize_index2 > 0) ? chunkSize_index2 - 4 : 0; 

	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;

	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[i][index1];
		 i2 < chunkSize_cnt_i;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"		   
		   rres += ncols, rmul1 += ncols //! ie, as the 'inner index' for both res and mul1 is the same.		   
		   
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {
	      
	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      
	      uint k2 = 0; 
	      assert(index2 < ncols); //! ie, given [below]
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[i][index2];
		   k2 < chunkSize_index1; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmul2 += ncols
		   ) {		
		
		// FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		// _mm_prefetch (&data2[8], _MM_HINT_NTA);
		
		
		//! The operation:
		uint j2 = 0;
		if(chunkSize_index2_intris > 0) {
		  for (; j2 < chunkSize_index2_intris; j2 += 4) { //! which is used to 'move' the poiner to a new column:
		    //assert((i + i2) < ncols); //! and if wrong then udpate [below].
		    // FIXME: in [”elow] validate correctness of "index" wrt. the weight
		    VECTOR_FLOAT_TYPE term1  = VECTOR_FLOAT_LOAD(&rmul1[k2]); VECTOR_FLOAT_TYPE term2  = VECTOR_FLOAT_LOAD(&rmul2[j2]);  
		    const VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
		    //! Load the result-vector
		    VECTOR_FLOAT_TYPE arr_result_tmp = VECTOR_FLOAT_LOADU(&rres[j2]);
		    //! Add the values:
		    arr_result_tmp = VECTOR_FLOAT_ADD(mul, arr_result_tmp);
		    //! Store the result:
		    VECTOR_FLOAT_STORE(&rres[j2], arr_result_tmp);
		    //rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
		  }		
		} 
		for (; j2 < chunkSize_index2; j2++) { //! which is used to 'move' the poiner to a new column:
		  // FIXME: in [”elow] validate correctness of "index" wrt. the weight
		  rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
		}
	      }
	    }
	  }
	}
      } else { //! then we need to evalaute the mask-proeprty, ie, eitehr 'implcityly' or 'directly':
	  if(!mask1 || !mask2) { //! then we asusem that 'unused' columsn are described/prerenseted by "FLT_MIN" or "FLT_MAX"
	    for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_cols; cnt_index1++, index1 += SM) {
	      const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	      //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
	      uint numberOf_chunks_cols_local = 0; uint chunkSize_cols_last_local = 0;  
	      if(data1 == data2) {
		__get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, numberOf_chunks_cols_local, chunkSize_cols_last_local);
	      } else { //! else the matrix is assumed not to be symmetric.
		numberOf_chunks_cols_local = numberOf_chunks_cols_2; chunkSize_cols_last_local = chunkSize_col_last_2;
	      }
	      for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_cols_local; cnt_index2++, index2 += SM) {
		const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_cols_local) ? SM : chunkSize_cols_last_local;
		const uint chunkSize_index2_intris = (chunkSize_index2 == SM) ? chunkSize_index2 : 
		  //! Then we idneitfy the size of the inner intristinc code-block: 
		  (chunkSize_index2 > 0) ? chunkSize_index2 - 4 : 0; 

		//! --------------------------------------------------------------------------------------
		//! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
		uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;
		//! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
		/**
		   Note: similar to for "transposed == 0" we find this procedure/code rather complex/non-intutive. To help the understanding (and identificaiton of bugs: sadly it is diffuclt to rwrite code without any bugs, eg, wrt. splelling errors).  In below code we:
		   ----------------------------------------------------------------------------------------
		   data1-row[index1 + i2*ncols] ... column[index2] | | | | | | column[index2 + chunkSize_index1] 
		   .                        result[index1+0][...]          +=    data2-row[index1 + 0]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   .                        result[index1+1][...]          +=    data2-row[index1 + 1]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   .                        result[index1+2][...]          +=    data2-row[index1 + 2]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   .                        result[index1+k2][...]         +=    data2-row[index1 + k2]    ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   ----------------------------------------------------------------------------------------
		   From the above depidction of 'comptuation for each matrix-tile' we infer that the column-index for "result" is be the same as the column-index in is to be the same for "result" as for "data2": when 'overlapping' the rows of "data1" and "data2" "index2" is used in "result" to update the 'current estimates/comptuations'.
		**/
		for (i2 = 0, //! ie, for "index1" 
		       rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		       // FIXME: validate [below]
		       rmul1 = &data1[i][index1], rmask1 = (!mask1) ? NULL : &mask1[i][index1];
		     i2 < chunkSize_cnt_i;
		     i2++, 
		       //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"		   
		       rres += ncols, rmul1 += ncols, //! ie, as the 'inner index' for both res and mul1 is the same.		   
		       rmask1 += (!rmask1) ? 0 : ncols
		 
		       //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		     ) {

		  //! Prefetch the data:
		  // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
		  // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		  _mm_prefetch (&data1[8], _MM_HINT_NTA);
		  //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
		  if(mask1) {_mm_prefetch (&mask1[8], _MM_HINT_NTA);}
	      
		  uint k2 = 0; 
		  assert(index2 < ncols); //! ie, given [below]
		  for (k2 = 0, //! ie, for "index2" 
			 // FIXME: validate [below]
			 rmul2 = &data2[i][index2], 
			 rmask2 = (!mask2) ? NULL : &mask1[i][index2];
		       k2 < chunkSize_index1; k2++, // , rmul2 += chunkSize_cnt_i
			 //! Move to the same psotion in the next row:
			 // FIXME: validte [below]
			 rmask2 += (!rmask2) ? 0 : ncols, rmul2 += ncols
		       ) {		
	      
		    if(isOf_interest(rmul1[k2])) {

		      // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		      //! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		      // _mm_prefetch (&data2[8], _MM_HINT_NTA);


		      //! The operation:
		      uint j2 = 0;
		      if(chunkSize_index2_intris > 0) {
			for(; j2 < chunkSize_index2_intris; j2 += 4) { //! which is used to 'move' the poiner to a new column:
			  
			  assert(false); // FIXME[jc] <-- [”elow] (and for the 'mask[][]' alternative) seems wrong ... may you validate thsi 'is-wrong' asusmption?
			  if(isOf_interest(rmask2[j2])) {		      
			    //assert((i + i2) < ncols); //! and if wrong then udpate [below].
			    // FIXME: in [”elow] validate correctness of "index" wrt. the weight
			    VECTOR_FLOAT_TYPE term1  = VECTOR_FLOAT_LOAD(&rmul1[k2]); VECTOR_FLOAT_TYPE term2  = VECTOR_FLOAT_LOAD(&rmul2[j2]);  
			    const VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
			    //! Load the result-vector
			    VECTOR_FLOAT_TYPE arr_result_tmp = VECTOR_FLOAT_LOADU(&rres[j2]);
			    //! Add the values:
			    arr_result_tmp = VECTOR_FLOAT_ADD(mul, arr_result_tmp);
			    //! Store the result:
			    VECTOR_FLOAT_STORE(&rres[j2], arr_result_tmp);
			    //rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
			  }
			}
		      }
		      for (; j2 < chunkSize_index2; j2++) { //! which is used to 'move' the poiner to a new column:
			if(isOf_interest(rmask2[j2])) {		      
			  // FIXME: in [”elow] validate correctness of "index" wrt. the weight
			  rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
			}
		      }
		    }
		    // printf("cnt_iterations=%u, debug_max_inner=%u, at %s:%d\n", cnt_iterations, debug_max_inner, __FILE__, __LINE__);
		  }
		  assert(debug_cntIterations_inner <= (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
		}
	      }
	    }
	  } else {
	    for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_cols; cnt_index1++, index1 += SM) {
	      const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	      //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
	      uint numberOf_chunks_cols_local = 0; uint chunkSize_cols_last_local = 0;  
	      if(data1 == data2) {
		__get_updatedNumberOF_lastSizeChunk_entry(index1+1, SM, numberOf_chunks_cols_local, chunkSize_cols_last_local);
	      } else { //! else the matrix is assumed not to be symmetric.
		numberOf_chunks_cols_local = numberOf_chunks_cols; chunkSize_cols_last_local = chunkSize_col_last;
	      }
	      for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_cols_local; cnt_index2++, index2 += SM) {
		const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_cols_local) ? SM : chunkSize_cols_last_local;
		const uint chunkSize_index2_intris = (chunkSize_index2 == SM) ? chunkSize_index2 : 
		  //! Then we idneitfy the size of the inner intristinc code-block: 
		  (chunkSize_index2 > 0) ? chunkSize_index2 - 4 : 0; 

		//! --------------------------------------------------------------------------------------
		//! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
		uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;
		//! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
		/**
		   Note: similar to for "transposed == 0" we find this procedure/code rather complex/non-intutive. To help the understanding (and identificaiton of bugs: sadly it is diffuclt to rwrite code without any bugs, eg, wrt. splelling errors).  In below code we:
		   ----------------------------------------------------------------------------------------
		   data1-row[index1 + i2*ncols] ... column[index2] | | | | | | column[index2 + chunkSize_index1] 
		   .                        result[index1+0][...]          +=    data2-row[index1 + 0]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   .                        result[index1+1][...]          +=    data2-row[index1 + 1]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   .                        result[index1+2][...]          +=    data2-row[index1 + 2]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   .                        result[index1+k2][...]         +=    data2-row[index1 + k2]    ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
		   ----------------------------------------------------------------------------------------
		   From the above depidction of 'comptuation for each matrix-tile' we infer that the column-index for "result" is be the same as the column-index in is to be the same for "result" as for "data2": when 'overlapping' the rows of "data1" and "data2" "index2" is used in "result" to update the 'current estimates/comptuations'.
		**/
		for (i2 = 0, //! ie, for "index1" 
		       rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		       // FIXME: validate [below]
		       rmul1 = &data1[i][index1], rmask1 = (!mask1) ? NULL : &mask1[i][index1];
		     i2 < chunkSize_cnt_i;
		     i2++, 
		       //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"		   
		       rres += ncols, rmul1 += ncols, //! ie, as the 'inner index' for both res and mul1 is the same.		   
		       rmask1 += (!rmask1) ? 0 : ncols
		 
		       //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		     ) {

		  //! Prefetch the data:
		  // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
		  // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		  _mm_prefetch (&data1[8], _MM_HINT_NTA);
		  //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
		  if(mask1) {_mm_prefetch (&mask1[8], _MM_HINT_NTA);}
	      
		  uint k2 = 0; 
		  assert(index2 < ncols); //! ie, given [below]
		  for (k2 = 0, //! ie, for "index2" 
			 // FIXME: validate [below]
			 rmul2 = &data2[i][index2], 
			 rmask2 = (!mask2) ? NULL : &mask1[i][index2];
		       k2 < chunkSize_index1; k2++, // , rmul2 += chunkSize_cnt_i
			 //! Move to the same psotion in the next row:
			 // FIXME: validte [below]
			 rmask2 += (!rmask2) ? 0 : ncols, rmul2 += ncols
		       ) {		
	      
		    if(rmask1[k2]) {

		      // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		      //! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		      // _mm_prefetch (&data2[8], _MM_HINT_NTA);

		      //! The operation:
		      uint j2 = 0;
		      if(chunkSize_index2_intris > 0) {
			for (; j2 < chunkSize_index2_intris; j2 += 4) { //! which is used to 'move' the poiner to a new column:
			  assert(false); // FIXME[jc] <-- [”elow] (and for the 'mask[][]' alternative) seems wrong ... may you validate thsi 'is-wrong' asusmption?
			  if(rmask2[j2]) {
			    VECTOR_FLOAT_TYPE term1  = VECTOR_FLOAT_LOAD(&rmul1[k2]); VECTOR_FLOAT_TYPE term2  = VECTOR_FLOAT_LOAD(&rmul2[j2]);  
			    const VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD(term1, term2, weight, /*index=*/i + i2, typeOf_metric);
			    //! Load the result-vector
			    VECTOR_FLOAT_TYPE arr_result_tmp = VECTOR_FLOAT_LOADU(&rres[j2]);
			    //! Add the values:
			    arr_result_tmp = VECTOR_FLOAT_ADD(mul, arr_result_tmp);
			    //! Store the result:
			    VECTOR_FLOAT_STORE(&rres[j2], arr_result_tmp);
			    //assert((i + i2) < ncols); //! and if wrong then udpate [below].
			    // FIXME: in [”elow] validate correctness of "index" wrt. the weight
			    //rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);		      
			  }
			}
		      }
		      for (; j2 < chunkSize_index2; j2++) { //! which is used to 'move' the poiner to a new column:
			if(rmask2[j2]) {
			  // FIXME: in [”elow] validate correctness of "index" wrt. the weight
			  rres[j2] += __macro__get_distance(rmul1[k2], rmul2[j2], weight, /*index=*/i + i2, typeOf_metric);
			}
		      }
		    }
		    // printf("cnt_iterations=%u, debug_max_inner=%u, at %s:%d\n", cnt_iterations, debug_max_inner, __FILE__, __LINE__);
		  }
		}
	      }
	    }
	  }
      }
    }
    //printf("cnt_iterations=%u, total_cnt_iterations=%u, at %s:%d\n", cnt_iterations, total_cnt_iterations, __FILE__, __LINE__);
  }
