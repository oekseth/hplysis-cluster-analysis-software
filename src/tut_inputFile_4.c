#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.

/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a permtuation of "tut_inputFile_3.c".
   @remarks in this test-case we evaluate the perofmrancde of our data-sets using a colleciton of real-life data-sets collected (among others) by oekseth at "www.knittingTools.org". The result is used to examplify how data-collectiosin may be evlauted using our straight-forward ccm-evlauation-interface in oru "hp_clusterFileCollection".
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;

  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;

  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "binomial_p005";
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //obj_config.globalConfig__isToStore__inputMatrix__inFormat__csv = isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.


  assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  /* //! -------------------------------------------- */
  /* //! */
  /* //! File-specific cofnigurations:  */
  /* s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t(); */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = false; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;   */
  /* fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file. */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config; */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = true; */
  /* //! */
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //!
  //! We are interested in a more performacne-demanind approach: 
  const uint sizeOf__nrows = 500;
  const uint sizeOf__ncols = 500;
  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat = fileRead_config__syn;
  s_kt_matrix_t matrix_concatToAll; setTo_empty__s_kt_matrix_t(&matrix_concatToAll);
  if(config__nameOfDefaultVariable && strlen(config__nameOfDefaultVariable)) {
    matrix_concatToAll = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable, fileRead_config__syn__concat);
    //! 
    //! Update the configuraiton-object:
    assert(matrix_concatToAll.ncols > 0);
    fileRead_config__syn.mat_concat = &matrix_concatToAll;
  }

//! -------------------------------------------------------------------------
//!
//!
//! Intiate for the real-lfie data-sets expected ... where files are expected to be found in "tests/data/kt_mine/"
const uint mapOf_functionStrings_base_size = 25;
const char *mapOf_functionStrings_base[mapOf_functionStrings_base_size] = {
  //! Note: the [”elow] is among others based on an evaluation of results generated in our "results_clusterCmp_cmp_corrDataSets__result.html".
  //! cnt_clusters=2, size=15; seperation=ambiguous-overlapping; Trait="curved, intersecting"; Euclid oversimplifies/'extremifies' the speration, while canberra is uanble to capture the 'curved relationships': the other metrics are unable to capture the relationjships. 
  "tests/data/kt_mine/birthWeight_chineseChildren.tsv", //! "Birth weights of male Chinese in ounces (J Millis, Y Seng, The effect of age and parity of the mother on birth weight of the offspring, Annals of Human Genetics, Volume 19, pages 58-73, 1954.)"
  //! ... cnt_clusters=1, size=; seperation=periodic; Trait="curved, intersecting"; Euclid 
  "tests/data/kt_mine/butterFat_percentage_cows.tsv", //! "Butterfat (https://people.sc.fsu.edu/~jburkardt/)"
  //! cnt_clusters=1, size=11; seperation='well-defined'; Trait="curved, x-axis-start-point differs"; Euclid 
  "tests/data/kt_mine/fish_growth.tsv", //! "Rate of growth of Ameiurus melas in conditioned and unconditioned well water (Allee, Bowen, Welty, Oesting, The effect of homotypic conditioning of water on the growth of fishes, and chemical studies of the factors involved, Journal of Experimental Zoology, Volume 68, 1934, pages 183-213.)"
  //! ... cnt_clusters=2, size=4; seperation=ambiguous; Trait="curved, fluctating, discrete-sinus"; Euclid 
  // FIXME: consider using [below] as an example of 'periodic clsuters which are diicuflt to clsuter'.
  // FIXME: consider to transpose [below] .... figure out how to 'hanlde' the case where 'a ranking' would result in isngificnat seperation 
  "tests/data/kt_mine/galls_thorax_length_transposed.tsv", //! "Thorax length for 4 aphids sampled from 28 galls (R Sokal, J Rohlf, Introduction to Biostatistics, Dover, 2009.)"
  //! ... cnt_clusters=, size=; seperation=; Trait="curved, intersecting"; Euclid 
  "tests/data/kt_mine/galls_thorax_length_transposed.tsv",
  //! (all-to-simple)
  "tests/data/kt_mine/guinea_pig_strains_transposed.tsv", //! "Guinea pig litter sizes for two strains (https://people.sc.fsu.edu/~jburkardt/)"
  //! (all-to-simple)
  "tests/data/kt_mine/ktFiles_guinea_pigs_perctange_males_females.tsv", //! "Is there any significant change in distribution of male and female guinea-pig off-springs when the mother ages? (\"ON THE GENETICS OF THE SPOTTED PATTERN OF THE GUINEA PIG SEWALL\", WRIGHT AND HERMAN B. CHASE, University of Chicago, Chicago, Illinois, Received May 20., 1936)",  caption: "TABLE 17: Mean percentages of males and females for strain 35, at two periods, in relation to age of mother ... Data-set-name: Strain 35D (1926-1934) "
  //! ... cnt_clusters=, size=; seperation=; Trait="curved, intersecting"; Euclid 
  "tests/data/kt_mine/ktFiles_guinea_pigs_weight_ages_crossBreedings_STD.tsv", //! "Table 1. Mean weight and standard deviation at different ages of inbred families and control stock B, of guinea pigs. Uncorrected for litter size:in the data-set we compare the standard deviation (STD) (and not the mean-weight): our assumption is that a large correlation between standard errors indicates that the observations are comparable, ie, as the 'hidden error' may be 'affected' by the same non-controllable underlying errors. (\"GENETIC GROWTH DIFFERENTIATION IN GUINEA PIGS\", HUGH C. McPHEE and ORSON N.EATON (1931))",  caption: "In the figure we compare growth of guinea-pigs (a growth measured in grams) for guinea pig families of different cross-breeding. To assess the significance of the cross-breeding, the cross-breeding are compared to a non-crossed control-stock."
  //! ... cnt_clusters=, size=; seperation=; Trait="curved, intersecting"; Euclid 
  "tests/data/kt_mine/ktFiles_guinea_pigs_weight_ages_crossBreedings_weight.tsv", //! "Table 1. Mean weight and standard deviation at different ages of inbred families and control stock B, of guinea pigs. Uncorrected for litter size:in the data-set we compare the mean-weight (and not the standard deviation (STD)). (\"GENETIC GROWTH DIFFERENTIATION IN GUINEA PIGS\", HUGH C. McPHEE and ORSON N.EATON (1931))",  caption: "In the figure we compare growth of guinea-pigs (a growth measured in grams) for guinea pig families of different cross-breeding. To assess the significance of the cross-breeding, the cross-breeding are compared to a non-crossed control-stock."
  //! ... cnt_clusters=, size=; seperation=; Trait="curved, intersecting"; Euclid 
  "tests/data/kt_mine/ktFiles_guinea_pigs_weight_ages_firstYear.transposed.tsv", //! "Weight, at different ages, of crosses between different inbred families and groups of guinea pigs:Growth measured in grams in the first year of a guinea pig (\"GENETIC GROWTH DIFFERENTIATION IN GUINEA PIGS\", HUGH C. McPHEE and ORSON N.EATON (1931))",  caption: "In the figure we compare the weight of the cross-breeds to the non-cross-breeds: from the figure we derive correlations between weight and guinea pig family; the \"X\" denotes the family, where the male always is the first in all cases: a transposed version."
  "tests/data/kt_mine/ktFiles_guinea_pigs_weight_ages_firstYear.tsv", //! "Weight, at different ages, of crosses between different inbred families and groups of guinea pigs:Growth measured in grams in the first year of a guinea pig (\"GENETIC GROWTH DIFFERENTIATION IN GUINEA PIGS\", HUGH C. McPHEE and ORSON N.EATON (1931))",  caption: "In the figure we compare the weight of the cross-breeds to the non-cross-breeds: from the figure we derive correlations between weight and guinea pig family; the \"X\" denotes the family, where the male always is the first in all cases.", /*tag=*/NULL},
  "tests/data/kt_mine/ktFiles_guinea_pigs_weight_ages_secondYear.transposed.tsv", //! "Weight, at different ages, of crosses between different inbred families and groups of guinea pigs:Growth measured in grams in the second year of a guinea pig (\"GENETIC GROWTH DIFFERENTIATION IN GUINEA PIGS\", HUGH C. McPHEE and ORSON N.EATON (1931))",  caption: "In the figure we compare the weight of the cross-breeds to the non-cross-breeds: from the figure we derive correlations between weight and guinea pig family; the \"X\" denotes the family, where the male always is the first in all cases: a transposed version."
  "tests/data/kt_mine/ktFiles_guinea_pigs_weight_ages_secondYear.tsv", //! "Weight, at different ages, of crosses between different inbred families and groups of guinea pigs:Growth measured in grams in the second year of a guinea pig (\"GENETIC GROWTH DIFFERENTIATION IN GUINEA PIGS\", HUGH C. McPHEE and ORSON N.EATON (1931))",  caption: "In the figure we compare the weight of the cross-breeds to the non-cross-breeds: from the figure we derive correlations between weight and guinea pig family; the \"X\" denotes the family, where the male always is the first in all cases."
  //! cnt_clusters=2, size=4; seperation='change VS not-change, ie, a non-linear comparison'; Trait="curved, intersecting"; ....??... <-- asusmes other metrics will find thios challenging
  // FIXME: remember to transpose [”elow] 
  "tests/data/kt_mine/ktFiles_guine_pigs_perctange_spottingGrade.tsv", //! "How Guinea Pigs coat-spots change over time (\"ON THE GENETICS OF THE SPOTTED PATTERN OF THE GUINEA PIG SEWALL\", WRIGHT AND HERMAN B. CHASE, University of Chicago, Chicago, Illinois, Received May 20., 1936)",  caption: "A comparison of coat color between different strains: in the figure we plot distributions of 4 inbred strains of guinea pigs, 1916-22 in percentages. Grade 0 means solid color: X means a trace of white. The grades are at 5 0nternals ( I =2.5 to 7.5%). W (black eyed white) is distinguished from 20 (a trace of color). The median percentages of white are given separately for males and females in the last two columns"
  "tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_caged_pregnancy_after.tsv", //! "Sex ratio in litters of different size in groups of guinea-pigs kept under various physiological conditions and husbandry regimens:Caged singly from 8 weeks of age; handled daily; male present after first pregnancy oestrus (\"Sex ratio and litter size in the guinea-pig\",  M. Peaker and E. Hannah Research Institute, Journal of Re-production and Fertility (1996))",  caption: "How litter size correlate to number of mothers"
  "tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_caged_pregnancy_first.tsv", //! "Sex ratio in litters of different size in groups of guinea-pigs kept under various physiological conditions and husbandry regimens:Caged singly from 8 weeks of age; handled daily; male present at first pregnancy oestrus (\"Sex ratio and litter size in the guinea-pig\",  M. Peaker and E. Hannah Research Institute, Journal of Re-production and Fertility (1996))",  caption: "How litter size correlate to number of mothers"
  "tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_housed_pregnancy_after.tsv", //! "Sex ratio in litters of different size in groups of guinea-pigs kept under various physiological conditions and husbandry regimens:Housed communally from 8 weeks of age; male present throughout; second pregnancy (\"Sex ratio and litter size in the guinea-pig\",  M. Peaker and E. Hannah Research Institute, Journal of Re-production and Fertility (1996))",  caption: "How litter size correlate to number of mothers"
  //! ... cnt_clusters=2, size=3; seperation='ambigious: silamirlty in curve-increase'; Trait="curved, simliar-peaks"; 
  // FIXME: in [”elow] why does the 'labels wrt. the parsing' differs from the acutal data? <-- updated!
  "tests/data/kt_mine/ktFiles_sexAndLitterRatio_guinePigs_housed_pregnancy_first.tsv", //! "Sex ratio in litters of different size in groups of guinea-pigs kept under various physiological conditions and husbandry regimens:Housed communally from 8 weeks of age; male present throughout; first pregnancy (\"Sex ratio and litter size in the guinea-pig\",  M. Peaker and E. Hannah Research Institute, Journal of Re-production and Fertility (1996))",  caption: "How litter size correlate to number of mothers"
  //! if transposed nearly-all (ie, xmt. "species 2, 75 seawater") seems to have a simlar flucation
  "tests/data/kt_mine/limpes_oxygenConsumption.tsv", //! "Oxygen consumption rates for two species of limpets (F Rohlf.)"
  //! "mouse_litter.tsv": (diffuclt ot manually valdiate)
  "tests/data/kt_mine/mouse_litter.tsv", //! "Measurements of 5 individuals in each of 7 mouse litters (R Sokal, J Rohlf, Introduction to Biostatistics, Dover, 2009.)"
  "tests/data/kt_mine/pigeons_physicalFeature.tsv", //! "Distance from narial opening to beak tip for 5 domestic pigeons, 20 observations (E Olson, R Miller, Morphological Integration, University of Chicago, 1958.)"
  //! ("plat_height": seems all to simple, ie, Not considered)
  "tests/data/kt_mine/plant_height.tsv", //! "Plant height in centimeters in 4 plots (R Sokal, J Rohlf, Introduction to Biostatistics, Dover, 2009.)"
  "tests/data/kt_mine/rabbit_temperature.tsv", //! "Rabbit temperature after rinderpest inoculation (G Carter, C Mitchell, Methods for adapting the virus of rinderpest to rabbits, Science, Volume 28, pages 252-253, 1958.)"
  //! "sugar_pea_selections.tsv": Large variance in seperation: a few peaks: other meitrcs have diffuclity in estalbishing the differencwe wrt. the data-sets.
  "tests/data/kt_mine/sugar_pea_sections.tsv", //! "Effect of different sugars on length of pea sections (William Purves)"
  //! cnt_clusters=1, size=~20; seperation='clear: samve dsicrete sinus-vurve wrt. flcutaitons ... Metrics: none of the compared metric manages to idneitfy/describe this relationship
  "tests/data/kt_mine/weed_length.tsv",  //! "Jimson weed length/width ratios (A F Blakeslee, The globe mutant in the jimson weed, Genetics, Volume 6, pages 241-264.)"
};

  //! --------------------------
  //!
  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint mapOf_realLife_size = mapOf_functionStrings_base_size *  __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {      
    //! Budil the sample-string:
    const char *stringOf_sampleData_type = mapOf_functionStrings_base[base_id];
    assert(stringOf_sampleData_type);
    assert(strlen(stringOf_sampleData_type));
    //!
    //! ITerate through the different k-cluster-count-specifriciaotns:
    for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
      const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
      assert(current_pos < mapOf_realLife_size);
      //!
      //! Add the object:
      mapOf_realLife[current_pos].tag = stringOf_sampleData_type;
      mapOf_realLife[current_pos].file_name = stringOf_sampleData_type;
      mapOf_realLife[current_pos].fileRead_config = fileRead_config__syn;
      mapOf_realLife[current_pos].inputData__isAnAdjcencyMatrix = false;
      mapOf_realLife[current_pos].k_clusterCount = k_clusterCount;
      mapOf_realLife[current_pos].mapOf_vertexClusterId = NULL;
      mapOf_realLife[current_pos].mapOf_vertexClusterId_size = 0;
      mapOf_realLife[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
      mapOf_realLife[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      mapOf_realLife[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
      mapOf_realLife[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
      //!
      current_pos++;
    }
  }

  //! ----------------------------------
  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_concatToAll);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
