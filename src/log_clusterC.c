#include "log_clusterC.h"

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "CLUSTERC-mem-data-anlysis"
 *
 * "CLUSTERC-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "CLUSTERC-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with ClusterC-mem-data-anlysis. 
 */


//! Intiaite the object.
void s_log_clusterC_init(s_log_clusterC_t *self) {
  assert(self);
  for(uint enum_id = 0; enum_id < (uint)e_log_clusterC_typeOf_undef; enum_id++) {
    self->mapOf_times[enum_id] = 0;
    self->mapOf_counter[enum_id] = 0;
    self->stringOf_description[enum_id] = NULL;
  }
}

/**
   @brief Assicated a descirption-tag to the log-enum in question.
   @param <self> is the object to update
   @param <enum_id> is attribute to update in obj.
   @param <stringOf_description> is the descritpion to assicated to the log-enum.
**/
void s_log_clusterC_setDescription(s_log_clusterC_t *self, const e_log_clusterC_typeOf_t enum_id, char *stringOf_description) {
  assert(self); assert(enum_id != e_log_clusterC_typeOf_undef);
  self->stringOf_description[enum_id] = stringOf_description;
}

/**
   @brief Start the measurement with regard to time.
   @param <self> is the selfect to update
   @param <enum_id> is attribute to update in self.
**/
void start_time_measurement_clusterC(s_log_clusterC_t *self, const e_log_clusterC_typeOf_t enum_id) {
  assert(self); assert(enum_id != e_log_clusterC_typeOf_undef);
  self->_mapOf_times_internal_clock[enum_id] = times(&self->_mapOf_times_internal_tms[enum_id]); //tms_start);
}
/**
   @brief Ends the measurement with regard to time.
   @param <self> is the selfect to update
   @param <enum_id> is attribute to update in self.
**/
void end_time_measurement_clusterC(s_log_clusterC_t *self, const e_log_clusterC_typeOf_t enum_id) { //, const char *msg, double &user_time, double &system_time, const float naive_tickTime = T_FLOAT_MAX) {
  assert(self); assert(enum_id != e_log_clusterC_typeOf_undef);
  struct tms tms_end;
  const clock_t clock_time_start = self->_mapOf_times_internal_clock[enum_id]; 
  const clock_t clock_time_end = times(&tms_end); 
  const long clktck = sysconf(_SC_CLK_TCK);
  const clock_t t_real = clock_time_end - clock_time_start;
  const t_float time_in_seconds = (t_real != 0) ? t_real/(float)clktck : 0;
  //! Udpate:
  // self->_mapOf_times_internal_clock[enum_id] = t_real;
  self->mapOf_times[enum_id] += time_in_seconds; //_real; //! ie, icnrement wrt. the exeuction-time


  /* const float diff_user = tms_end.tms_utime - self->_mapOf_times_internal_tms[enum_id].tms_utime; */
  /* const t_float user_time = (diff_user) ? (diff_user)/((t_float)clktck) : 0; */
  /* printf("diff_user=%f, at %s:%d\n", user_time, __FILE__, __LINE__); */
  /*  */
  /* const float diff_system = tms_end.tms_stime - tms_start.tms_stime; */
  /* system_time = diff_system/((float)clktck); */
  /* if(msg) { */
  /*   if( (naive_tickTime == T_FLOAT_MAX) || (naive_tickTime == 0) || (time_in_seconds == 0) ) { */
  /*     if(naive_tickTime != 0) { */
  /* 	printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg); */
  /*     } //! else we assume the measurements are of insiginft valeu, ie, omit printing. */
  /*   } else { */
  /*     const float diff = naive_tickTime / time_in_seconds; */
  /*     printf("%.4fx performance-difference: tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg); */
  /*   } */
  /* } */
  /* //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg); */
  /* return time_in_seconds; */
}


/**
   @brief update the "s_log_clusterC_t" object with counter (oekseth, 06. july 2016).
   @param <self> the object to hold the s_log_clusterC_t object.
   @param <enum_id> the log-type to update
   @param <cnt> if not set to "0" nor "UINT_MAX" then wwe use this valeu to update the assciated s_log_clusterC_t structure     
**/
void log_updateCounter_clusterC(s_log_clusterC_t *self, const e_log_clusterC_typeOf_t enum_id, const uint cnt) {
  assert(self); assert(enum_id != e_log_clusterC_typeOf_undef);
  if(cnt != UINT_MAX)  {
    self->mapOf_counter[enum_id] += cnt;
    //printf("counter=%u, after cnt=%u, at %s:%d\n", self->mapOf_counter[enum_id], cnt, __FILE__, __LINE__);
    // assert(self->mapOf_counter[enum_id] < (1000*1000*100));
    //assert(false);
  }
}
/**
   @brief write out the resutls of the log_clusterC selfect.
   @param <self> is the object to write out the contents for
   @param <stream> is the stream to write the result to, eg, STDOUT
**/
void writeOut_object_clusterC(const s_log_clusterC_t *self, FILE *stream, const e_logCluster_typeOf_writeOut_t typeOf_logMessage, const char *stringOf_description) {
  assert(self); assert(stream); 
  // assert(e_log_clusterC_typeOf_undef == 22); //! ie, for [”elow] to be correctn.
  assert(typeOf_logMessage != e_logCluster_typeOf_writeOut_undef);

  if(typeOf_logMessage == e_logCluster_typeOf_writeOut_SOM) {
  fprintf(stream,
	  "#! Result(%s): summary: [all]='%f's"
	  //! Summary-attributes to idneitfy overall exeuction-time-properties:
	  ", [init]='%f's"
	  ", [somWorker-all]='%f's"
	  //", [somWorker-somAssign-all]='%f's"
	  ", [somWorker-somAssign-all]='%f's"

	  "\n \tResult(SOM:details::worker):"
	  "  [somWorker-init]='%f's"
	  ", [somWorker-step1-all]='%f's"
	  ", [somWorker-step1-STD]='%f's"
	  ", [somWorker-step1-initMask]='%f's"
	  ", [somWorker-step1-initRandom]='%f's"
	  //! The time-consumption of the 'main-iteriaotn-phase' in the SOM algorithm:
	  ", [somWorker-step2-all]='%f's"
	  ", [somWorker-step2-distanceMatris]='%f's"
	  ", [somWorker-step2-updateClosestPair]='%f's"

	  //! The time-consumption of the SOM assing phase:
	  "\n \tResult(SOM-details::assign):"
	  "  [somWorker-somAssign-distanceMatrix]='%f's"
	  ", [somWorker-somAssign-iteration]='%f's"
	  ", at %s:%d\n",
	  (stringOf_description && strlen(stringOf_description)) ? stringOf_description : "SOM",
	  (float)self->mapOf_times[e_log_clusterC_som_all],
	  (float)self->mapOf_times[e_log_clusterC_som_init],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_all],
	  (float)self->mapOf_times[e_log_clusterC_somAssign_all],

	  (float)self->mapOf_times[e_log_clusterC_somWorker_init],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step1_all],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step1_STD],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step1_initMask],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step1_initRandom],
	  //! The time-consumption of the 'main-iteriaotn-phase' in the SOM algorithm:
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step2_all],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step2_distanceMetric],
	  (float)self->mapOf_times[e_log_clusterC_somWorker_step2__update_closest_pairs],
	  //! The time-consumption of the SOM assing phase:
	  (float)self->mapOf_times[e_log_clusterC_somAssign_distanceMatrix],
	  (float)self->mapOf_times[e_log_clusterC_somAssign_iteration],
	  // (float)self->mapOf_times[e_log_clusterC_somWorker_],
	  // (float)self->mapOf_times[e_log_clusterC_somWorker_],

	  //! Idnetifiers to simplify future chagnes:
	  __FILE__, __LINE__);
  } else if(typeOf_logMessage == e_logCluster_typeOf_writeOut_kMeans) {
  fprintf(stream,
	  "#! Result(%s): summary: [all]='%f's"
	  //! Summary-attributes to idneitfy overall exeuction-time-properties:
	  ", [init]='%f's"
	  //", [k-Means-all]='%f's"
	  //", [k-MeansWorker-k-MeansAssign-all]='%f's"
	  //", [k-Means-k-MeansAssign-all]='%f's"
	  //"\n \tResult(K-MEANS:details::worker):"
	  /* "  [k-Means-init]='%f's" */
	  ", [k-Means-innerIteration-all]='%f's"
	  ", [k-Means-innerIteration-randomAssign]='%f's"
	  ", [k-Means-innerIteration-findCentroids]='%f's"
	  ", [k-Means-innerIteration-applyMetric-findClosestToCenter]='%f's"
	  /* //! The time-consumption of the 'main-iteriaotn-phase' in the K-MEANS algorithm: */
	  /* ", [k-MeansWorker-step2-all]='%f's" */
	  /* ", [k-MeansWorker-step2-distanceMatris]='%f's" */
	  /* ", [k-MeansWorker-step2-updateClosestPair]='%f's" */

	  //! The time-consumption of the K-MEANS assing phase:
	  //"\n \tResult(K-MEANS-details::assign):"
	  ", at %s:%d\n",
	  (stringOf_description && strlen(stringOf_description)) ? stringOf_description : "k-Means",
	  (float)self->mapOf_times[e_log_clusterC_kMeans_all],
	  (float)self->mapOf_times[e_log_clusterC_kMeans_init],
	  /* (float)self->mapOf_times[e_log_clusterC_kMeans_all_forPart_random], */
	  /* (float)self->mapOf_times[e_log_clusterC_kMeans_all_forPart_center], */
	  /* (float)self->mapOf_times[e_log_clusterC_kMeans_all_forPart_center], */
	  /* (float)self->mapOf_times[e_log_clusterC_somAssign_all], */

	  (float)self->mapOf_times[e_log_clusterC_kMean_iteration_all],
	  (float)self->mapOf_times[e_log_clusterC_kMean_iteration_randomAssign],
	  (float)self->mapOf_times[e_log_clusterC_kMean_iteration_findCenter],
	  (float)self->mapOf_times[e_log_clusterC_kMean_iteration_findAllDistances],
	  /* (float)self->mapOf_times[e_log_clusterC_somWorker_step1_all], */
	  /* (float)self->mapOf_times[e_log_clusterC_somWorker_step1_STD], */
	  /* (float)self->mapOf_times[e_log_clusterC_somWorker_step1_initRandom], */
	  /* (float)self->mapOf_times[e_log_clusterC_somWorker_step1_initMask], */
	  /* //! The time-consumption of the 'main-iteriaotn-phase' in the SOM algorithm: */
	  /* //(float)self->mapOf_times[e_log_clusterC_somWorker_step2_all], */
	  /* (float)self->mapOf_times[e_log_clusterC_somWorker_step2_distanceMetric], */
	  /* (float)self->mapOf_times[e_log_clusterC_somWorker_step2__update_closest_pairs], */
	  /* //! The time-consumption of the SOM assing phase: */
	  /* (float)self->mapOf_times[e_log_clusterC_somAssign_distanceMatrix], */
	  /* (float)self->mapOf_times[e_log_clusterC_somAssign_iteration], */
	  // (float)self->mapOf_times[e_log_clusterC_somWorker_],
	  // (float)self->mapOf_times[e_log_clusterC_somWorker_],

	  //! Idnetifiers to simplify future chagnes:
	  __FILE__, __LINE__);
  } else if(
	    (typeOf_logMessage == e_logCluster_typeOf_writeOut_cmpCases_6x) ||
	    (typeOf_logMessage == e_logCluster_typeOf_writeOut_cmpCases_12x)
	    ) {
    fprintf(stream,
	    "#! Result(%s):"
	    //! -
	    "  case(%s)='%f's"
	    ", case(%s)='%f's"
	    //! -
	    ", case(%s)='%f's"
	    ", case(%s)='%f's"
	    //! -
	    ", case(%s)='%f's"
	    ", case(%s)='%f's",
	    //! -
	    (stringOf_description && strlen(stringOf_description)) ? stringOf_description : "comparison",
	    //! ------
	    (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_1] == NULL) ? "1" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_1], 
	    (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_1],
	    //! ---
	    (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_2] == NULL) ? "2" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_2], 
	    (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_2],
	    //! ------
	    (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_3] == NULL) ? "3" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_3], 
	    (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_3],
	    //! ---
	    (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_4] == NULL) ? "4" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_4], 
	    (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_4],
	    //! ------
	    (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_5] == NULL) ? "5" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_5], 
	    (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_5],
	    //! ---
	    (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_6] == NULL) ? "6" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_6], 
	    (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_6]
	    );
    //! ------
    if(   (typeOf_logMessage == e_logCluster_typeOf_writeOut_cmpCases_12x) ) {
      fprintf(stream,
	      //! -
	      "  case(%s)='%f's"
	      ", case(%s)='%f's"
	      //! -
	      ", case(%s)='%f's"
	      ", case(%s)='%f's"
	      //! -
	      ", case(%s)='%f's"
	      ", case(%s)='%f's",
	      //! ------
	      (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_7] == NULL) ? "1" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_7], 
	      (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_7],
	      //! ---
	      (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_8] == NULL) ? "2" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_8], 
	      (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_8],
	      //! ------
	      (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_9] == NULL) ? "3" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_9], 
	      (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_9],
	      //! ---
	      (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_10] == NULL) ? "4" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_10], 
	      (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_10],
	      //! ------
	      (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_11] == NULL) ? "5" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_11], 
	      (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_11],
	      //! ---
	      (self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_12] == NULL) ? "6" : self->stringOf_description[e_log_clusterC_typeOf_caseEnumeration_12], 
	      (float)self->mapOf_times[e_log_clusterC_typeOf_caseEnumeration_12]
	      );
    }
    //! ------
    fprintf(stream,
	    ", at %s:%d\n",
	    __FILE__, __LINE__);
  } else {
    assert(false); // ie, as weh then need toa dd support for this case.
  }
}
