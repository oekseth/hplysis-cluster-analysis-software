#ifndef kt_api_config_h
#define kt_api_config_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_api_config
   @brief PRovide genrlaised cofngiruation-optiosn for appliciaon of data-filters (oekseth, 06. setp. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "kt_longFilesHandler.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "kt_matrix.h"
/**
   @struct s_kt_api_config
   @brief provide a generic performance-cofniguraiton-strategy for functions in our kt_api (oekseth, 06. des. 2016).
 **/
typedef struct s_kt_api_config {
  bool isTo_transposeMatrix;
  bool isTo_applyDisjointSeperation;
  bool isTo_parallisaiton__sharedMemory;
  bool inputMatrix__isAnAdjecencyMatrix;
  bool in2dCorrMatrix__for_zeroMaskMetrics__applyOptimization; //! ie, which if set to "true" impleis that we replace the "T_FLOAT_MAX" with a '0' value for 'zero-case-metrics': for tdetails wrt. 'such metrics' please see our "mayReplace_maskWith__zero__s_kt_correlationMetric_t(..)"
  //! -----
  const char *stringOf_exportResult__heatMapOf_clusters; //! which if not set to NULL imples that we use a heat-map where teh 'cluster-embershisp' are sued as a 'topolocial filter'.
  s_kt_longFilesHandler_t *fileHandler;
  //! -------------- 
  bool performance__testImapcactOf__slowPerformance; //! which is included to 'allow' an evlaution of the effect wrt. a non-improved applicaiton of distance-matrix-comptautions on data-sets.
} s_kt_api_config_t;


//! Intiate the "s_kt_api_config_t" object (oekseth, 06. des. 2016).
//#ifdef true // COMPILE_FULL
s_kt_api_config_t init__s_kt_api_config_t(bool isTo_transposeMatrix, bool isTo_applyDisjointSeperation, bool isTo_parallisaiton__sharedMemory, bool inputMatrix__isAnAdjecencyMatrix, s_kt_longFilesHandler_t *fileHandler = NULL);
/* #else */
/* s_kt_api_config_t init__s_kt_api_config_t(bool isTo_transposeMatrix, bool isTo_applyDisjointSeperation, bool isTo_parallisaiton__sharedMemory, bool inputMatrix__isAnAdjecencyMatrix); */
/* #endif //! SWIG */
//! A simplictc cofnigruation-object which xmt the tranpsoe-rpoerpty set the devfualt 'safe-gurard non-optmized' paramters (oekseth, 06. jan. 2017).
s_kt_api_config_t init__simple__s_kt_api_config_t(const bool isTo_transposeMatrix);

//static void dummy_compiler_test() {;}

#endif //! EOF
