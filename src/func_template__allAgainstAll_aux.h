#ifndef func_template__allAgainstAll_aux_h
#define func_template__allAgainstAll_aux_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file func_template__allAgainstAll_aux
   @brief provide auxziliary fucntiosn for testing the tiem-effect of non-optmized rotuines for distnace-computations.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/




//! Comptue teh sum of chunks for the case where all valeus are of interst.
static void __computeAllAgainstAll_get_sumsFor_chunks(const uint ncols, t_float **data1, t_float **data2, char** mask1, char** mask2, const t_float *weight, t_float **resultMatrix, const uint chunkSize_index1, const uint chunkSize_index2, const uint index1, const uint index2, t_float **listOf_inlineResults, const t_float tweight, const  e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const bool needTo_useMask_evaluation) {
  t_float *resultTable_sum1 = allocate_1d_list_float(chunkSize_index1, /*default-value=*/0);
  t_float *resultTable_denom1 = allocate_1d_list_float(chunkSize_index1, /*default-value=*/0);
  t_float *resultTable_sum2 = NULL; t_float *resultTable_denom2 = NULL;
  if(data1 != data2) {
    resultTable_sum2 = allocate_1d_list_float(chunkSize_index2, /*default-value=*/0);
    resultTable_denom2 = allocate_1d_list_float(chunkSize_index2, /*default-value=*/0);
  } else {
    resultTable_sum2 = resultTable_sum1;
    resultTable_denom2 = resultTable_denom1;
  }
  const uint ncols_intri = (ncols > 16) ? ncols - 4 : 0;
  if(needTo_useMask_evaluation == false) {
    for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
      //! Update:
      t_float scalar_denom1 = 0;
      resultTable_sum1[local_index1] = get_sumOf_values_denom_maskNotSet(local_index1 + index1, ncols, ncols_intri, data1[index1+local_index1], weight, &scalar_denom1);
      resultTable_denom1[local_index1] = scalar_denom1;
    }
    if(data1 != data2) {
      for(uint local_index1 = 0; local_index1 < chunkSize_index2; local_index1++) {
	//! Update:
	t_float scalar_denom1 = 0;
	resultTable_sum2[local_index1] = get_sumOf_values_denom_maskNotSet(local_index1 + index2, ncols, ncols_intri, data2[index2+local_index1], weight, &scalar_denom1);
	resultTable_denom2[local_index1] = scalar_denom1;
      }
    }
  } else {
    if(mask1 == NULL) {
      for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
	//! Update:
	t_float scalar_denom1 = 0;
	resultTable_sum1[local_index1] = get_sumOf_values_denom_mask_implicit(local_index1 + index1, ncols, ncols_intri, data1[index1+local_index1], weight, &scalar_denom1);
	resultTable_denom1[local_index1] = scalar_denom1;
      }
    } else {
      for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
	//! Update:
	t_float scalar_denom1 = 0;
	resultTable_sum1[local_index1] = get_sumOf_values_denom_mask_explicit(local_index1 + index1, ncols, ncols_intri, data1[index1+local_index1], weight, &scalar_denom1, mask1[local_index1 + index1]);
	resultTable_denom1[local_index1] = scalar_denom1;
      }
    }
    if(data1 != data2) {
      if(mask2 == NULL) {
	for(uint local_index1 = 0; local_index1 < chunkSize_index2; local_index1++) {
	  //! Update:
	  t_float scalar_denom1 = 0;
	  resultTable_sum2[local_index1] = get_sumOf_values_denom_mask_implicit(local_index1 + index2, ncols, ncols_intri, data2[index2+local_index1], weight, &scalar_denom1);
	  resultTable_denom2[local_index1] = scalar_denom1;
	}
      } else {
	for(uint local_index1 = 0; local_index1 < chunkSize_index2; local_index1++) {
	  //! Update:
	  t_float scalar_denom1 = 0;
	  resultTable_sum2[local_index1] = get_sumOf_values_denom_mask_explicit(local_index1 + index2, ncols, ncols_intri, data2[index2+local_index1], weight, &scalar_denom1, mask2[local_index1 + index2]);
	  resultTable_denom2[local_index1] = scalar_denom1;
	}
      }
    }
  }
  const uint chunkSize_index2_intro = (chunkSize_index2 >= 16) ? chunkSize_index2 - 4 : 0;
  t_float **matrix_tmp  = (resultMatrix) ? &resultMatrix[index1] : listOf_inlineResults;
  //! What we expect:
  assert(matrix_tmp);
  if(resultMatrix) {assert(resultMatrix[index1] == matrix_tmp[0]);}
  if(resultMatrix) {assert(resultMatrix[index1][0] == matrix_tmp[0][0]);}

  const uint offset_2 = (resultMatrix) ? index2 : 0;
  //#if(configure_generic_useMulInsteadOf_div == 1)
  if(tweight == 0) {
    const t_float default_value = (typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_spearMan) ? 1 : 0;
    for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1) {
      uint local_index2 = 0;		
      for(; local_index2 < chunkSize_index2; local_index2++) {		
	matrix_tmp[local_index1][offset_2+local_index2] = default_value;
      }
    }
  } else
    //#endif
    {
      for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1) {
	uint local_index2 = 0;
		  
	for(; local_index2 < chunkSize_index2; local_index2++) {		
	  //! Store the result:
	  matrix_tmp[local_index1][offset_2+local_index2] = 
	    compute_score_correlation(matrix_tmp[local_index1][offset_2+local_index2+0], 
				      resultTable_sum1[local_index1], resultTable_sum2[local_index2+0], tweight, 
				      resultTable_denom1[local_index1], resultTable_denom1[local_index2+0], 
				      typeOf_metric_correlation, /*sizeOf_i=*/ncols);
	}
      }
    }
  //! De-allcoate the locally reserve3d memory:
  free_1d_list_float(&resultTable_sum1);  free_1d_list_float(&resultTable_denom1);
  if(data1 != data2) {
    free_1d_list_float(&resultTable_sum2);  free_1d_list_float(&resultTable_denom2);
  }
}

//! Compute the weights of a correlation-matrix, using temporal resutls (eg, from tiling) as input.
//! @remarks for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
static void __ofsquaredTileCalcuclate_weights(s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *objCaller_postProcessResult, t_float **listOf_inlineResults, const uint chunkSize_index1, const uint chunkSize_index2, const uint index1, const uint index2) {
  #include "correlation__codeStub__allAgainstAllPostProcess__logSum.c"
}
// static void __ofsquaredTileCalcuclate_weights_inverted(s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights *objCaller_postProcessResult, t_float **listOf_inlineResults, const uint chunkSize_index1, const uint chunkSize_index2, const uint index1, const uint index2) {
//   #include "correlation__codeStub__allAgainstAllPostProcess__logSum_inverted.c"
// }

//! Idneitfy the min-value in the correlation-matrix, using temporal resutls (eg, from tiling) as input.
//! @remarks for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
static void __ofsquaredTileCalcuclate_minVal(s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *objCaller_postProcessResult, t_float **listOf_inlineResults, const uint chunkSize_index1, const uint chunkSize_index2, const uint index1, const uint index2) {
  #include "correlation__codeStub__allAgainstAllPostProcess__minVal.c"
}

//! Idneitfy the max-value in the correlation-matrix, using temporal resutls (eg, from tiling) as input.
//! @remarks for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
static void __ofsquaredTileCalcuclate_maxVal(s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *objCaller_postProcessResult, t_float **listOf_inlineResults, const uint chunkSize_index1, const uint chunkSize_index2, const uint index1, const uint index2) {
  #include "correlation__codeStub__allAgainstAllPostProcess__maxVal.c"
}


//! Idneitfy the sum of values in the correlation-matrix, using temporal resutls (eg, from tiling) as input.
//! @remarks for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
static void __ofsquaredTileCalcuclate_sumOf(s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *objCaller_postProcessResult, t_float **listOf_inlineResults, const uint chunkSize_index1, const uint chunkSize_index2, const uint index1, const uint index2) {
  #include "correlation__codeStub__allAgainstAllPostProcess__sum.c"
}


#endif //! EOF
