#include "kt_measureStub_time.h"
#undef __M__calledInsideFunction
#define __M__calledInsideFunction 1
#include "tut_time_1_data_syntetic_wellDefinedClusters.c"
#include "tut_time_2_clustAlg_syntAndReal.c"
#include "tut_time_3_ccm_synt.c"
#include "tut_time_4_ccm_rand.c"
#include "tut_time_5_ccm_kdTree_sse.c"

//! 
//! The wrapper call for "tut_time_*." test-stubs (oekseth, 06. otk. 2017).
bool kt_measureStub_time(const int array_cnt, char **array) {
  if( (0 == strncmp("tut_time_1_data_syntetic_wellDefinedClusters", array[1], strlen("tut_time_1_data_syntetic_wellDefinedClusters"))) ) {  
    tut_time_1_data_syntetic_wellDefinedClusters(array_cnt, array); //! (oekseth, 06. okt. 2017).
    return true;
  } else  if( (0 == strncmp("tut_time_3_ccm_synt", array[1], strlen("tut_time_3_ccm_synt"))) ) { tut_time_3_ccm_synt(array_cnt, array); return true;
  } else  if( (0 == strncmp("tut_time_4_ccm_rand", array[1], strlen("tut_time_4_ccm_rand"))) ) { tut_time_4_ccm_rand(array_cnt, array); return true;
  } else  if( (0 == strncmp("tut_time_5_ccm_kdTree_sse", array[1], strlen("tut_time_5_ccm_kdTree_sse"))) ) { tut_time_5_ccm_kdTree_sse(array_cnt, array); return true;
    /*
  } else  if( (0 == strncmp("", array[1], strlen(""))) ) { (array_cnt, array); return true;
    */
    /*
      } else  if( (0 == strncmp("", array[1], strlen(""))) ) {       return true;}
    */
  } else  if( (0 == strncmp("tut_time_2_clustAlg_syntAndReal", array[1], strlen("tut_time_2_clustAlg_syntAndReal"))) ) {  
    tut_time_2_clustAlg_syntAndReal(array_cnt, array);
    return true;
  }
  return false;
}
