#ifndef kt_forest_findDisjoint_h
#define kt_forest_findDisjoint_h

 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_forest_findDisjoint
   @brief a data-structure for idnetifying disjoint forests in sparse matrixes.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
#include "types.h"
#include "def_intri.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_set_2dsparse.h" 
#include "kt_set_1dsparse.h" 
//#include "log_clusterC.h"
#include "kt_list_1d.h"

/**
   @enum e_kt_forest_findDisjoint_inputRelationType
   @brief specfiy the type of relation-traversal to be used when computing the disjoint regions of a network (oekseth, 06. okt. 2016).
 **/
typedef enum e_kt_forest_findDisjoint_inputRelationType {
  e_kt_forest_findDisjoint_inputRelationType_sparseRelations,
  e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float,
  e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char
} e_kt_forest_findDisjoint_inputRelationType_t;
/**
   @enum e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices
   @brief specify how the start-vertices are to be selected, ie, an heuritc to optimize the algorithm based on certaiin assumptions conserning the topology of input-network(s) (oekseth, 06. okt. 2016).
 **/
typedef enum e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices {
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder,
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes,
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_increase,
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_decrease,
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_outgoing_sortedByRank_increase,
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_outgoing_sortedByRank_decrease,
} e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_t;
/**
   @enum e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal
   @brief specify the type of graph-traversal to be used (oekseth, 06. otk. 2016).
   @remarks describe/idneitites the graph-traversal-order: use "topology" or "direct mapping" (where the latter impleis taht we do Not order the vertex-accesses accodring to the itnermeidary vertices, ie, where we asusme that 'there will Not be fwer forks/merges if topology is used as an ordering-criteria').
 **/
typedef enum e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal {
  e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology,
  e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping,
  //e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_
} e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_t;


//! Specify the status-code wrt. disjotin-forest-eevlauation (oekseth, 06. des. 2016).
typedef enum e_kt_forest_findDisjoint_statusCode {
  e_kt_forest_findDisjoint_statusCode_emptyValues_some,
  e_kt_forest_findDisjoint_statusCode_emptyValues_none,
  e_kt_forest_findDisjoint_statusCode_undef
} e_kt_forest_findDisjoint_statusCode_t;

/**
   @struct s_kt_forest_findDisjoint
   @brief defines a structure of disjoint elements, ie, to hold the result of our disjouitn-element-processing  (oekseth, 06. okt. 2016).
 **/
typedef struct s_kt_forest_findDisjoint {
  const s_kt_set_2dsparse_t *listOf_relations;
  // FIXME: remember to itnaite [”elow] to a size of ....??...
  s_kt_set_1dsparse_t vertexStack_child;    s_kt_set_1dsparse_t vertexStack_parent; 
  uint _nrows; uint _ncols;
  uint *mapOf_vertexMembershipId;
  s_kt_set_2dsparse_t listOf_membership_vertexCollection;
  uint *mapOf_disjointForest_vertexId; //! which hold the centrality-vertex-id assicated to each disjoint forest
  char *mapOf_vertexMembershipId_childsAreAdded; //! which is used to 'handle' the case wehre vertices are udpated with a forest-id through not explcitly evalauted wr.t its childnre (eg, due to a non-topolgocal graph-traversal-cofngiruation).
  char **mask; t_float **matrix; t_float emptyValueIf_maskIsNotUsed; 
  e_kt_forest_findDisjoint_inputRelationType_t _inputType;
  e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_t typeOf_startVertices;
  e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_t typeOf_graphTraversalOrder;
  //bool _maskIsAllocated;
  //! ------------------------
  //!
  //! Status-code:
  e_kt_forest_findDisjoint_statusCode_t statusCode;
  //! ------------------------
  //!
  //! Counters for local-name-spaces:
  uint mapOf_vertexMembershipId_size;
  uint globalIndexOfset_rowId; 
  uint globalIndexOfset_columnId;
  //! ------------------------
  //!
  //! Specify a 'supportive data-structure' to hold/remember the different data-set-ids for each namespace ... used in construction of the "uint **mapOf_setMember" return-data-structure-id:
#define __config__kt_forest_finddisjoint__masCntRegions 10
  uint mapOf_nameSpaceDomain_offsetStart_rows[__config__kt_forest_finddisjoint__masCntRegions];  uint mapOf_nameSpaceDomain_offsetEnd_rows[__config__kt_forest_finddisjoint__masCntRegions];
  uint mapOf_nameSpaceDomain_offsetStart_columns[__config__kt_forest_finddisjoint__masCntRegions];  uint mapOf_nameSpaceDomain_offsetEnd_columns[__config__kt_forest_finddisjoint__masCntRegions];
  uint mapOf_nameSpaceDomain_offset_rows_cnt;   uint mapOf_nameSpaceDomain_offset_columns_cnt;
  //! ------------------------
  //! ------------------------
  //!
  //! Mapping-support for DB-scan permtautions (oekseth, 06. jul. 2017):
  const s_kt_list_1d_uint_t *obj_mapOf_rootVertices_pointer;
  bool obj_mapOf_rootVertices_pointer__forBothHeadAndTail; //! which is to be set to true if the MCL-algorithm-appraoch is to be used.
  
} s_kt_forest_findDisjoint_t;



//! --------------------------------------------------------------------------------------------------------------------------------------
//! --------------------------------------------------------------------------------------------------------------------------------------

//! Intiates an empty object
void setTo_Empty__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self);
//! @return an empty object of this type
s_kt_forest_findDisjoint_t get_empty__s_kt_forest_findDisjoint();
/**
   @brief allocates an s_kt_forest_findDisjoint_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <ncols> which is used if the useSeperateNameSpace_columns parameter is used, ie, to properly set a 'namespace-offset'.
   @param <listOf_relations> is the forest/graph/network to evlauate.
   @param <isTo_init> which shoudl be set to false if this is Not the first intaition-call to this fucniton
   @param <useSeperateNameSpace_rows> which should be set tot rue fi mulipel matices are to bee valuated seprately (ie, for different nam-spaces) in order to identfy disjoitn sets, eg, wehre 'ermge-point' is the feature/columnd ids.
   @param <useSeperateNameSpace_columns> which is ismilar to "useSeperateNameSpace_rows" with difference that we assuem that the rows are to be the 'merging-points.
   @param <totalCntUniqueVerticesToEvalauteForAllIterations> which is the total number of unique vertices to add (ie, for all 'calls' to this object wrt the idfferent input-name-spaces): 
 **/
void allocate__extensive__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, const s_kt_set_2dsparse_t *listOf_relations, const bool isTo_init, const bool useSeperateNameSpace_rows, const bool useSeperateNameSpace_columns, uint totalCntUniqueVerticesToEvalauteForAllIterations);

/**
   @brief allocates an s_kt_forest_findDisjoint_t object 
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <ncols> is the number of rows to allocate
   @param <matrix> is the dense list of interesting relations.
   @param <mask> which if Not set to NULL implies taht all valeus set to "mask[i][j] == 1" is expected to be a relation: if "mask" is used then we expect "matrix == NULL", ie, for consistency.
   // @param <isTo_transpose> which if set to true implies that we tanspsoe teh matrix, ie, before computing the relationships.
   @param <emptyValueIf_maskIsNotUsed> is the valeu which 'sates' that a vertex is empty in "matrix" (eg, "0" or "T_FLOAT_MAX"): if "mask" is used then we expect "mask == NULL"
   @param <isTo_init> which shoudl be set to false if this is Not the first intaition-call to this fucniton
   @param <useSeperateNameSpace_rows> which should be set tot rue fi mulipel matices are to bee valuated seprately (ie, for different nam-spaces) in order to identfy disjoitn sets, eg, wehre 'ermge-point' is the feature/columnd ids.
   @param <useSeperateNameSpace_columns> which is ismilar to "useSeperateNameSpace_rows" with difference that we assuem that the rows are to be the 'merging-points.
   @param <totalCntUniqueVerticesToEvalauteForAllIterations> which is the total number of unique vertices to add (ie, for all 'calls' to this object wrt the idfferent input-name-spaces): 
 **/
void allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, t_float **matrix, char **mask, const t_float emptyValueIf_maskIsNotUsed, const bool isTo_init, const bool useSeperateNameSpace_rows, const bool useSeperateNameSpace_columns, uint totalCntUniqueVerticesToEvalauteForAllIterations);

//! Allocate a disjtoint-object where we are to infer the disjotin-cases wrt. a non-adjcency matrix (oekseth, 06. des. 2016).
//! @remarks the results may be 'fetched' using our "cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(..)"
static void allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, t_float **matrix) {
  const t_float default_value_float__ = T_FLOAT_MAX; //! ie, the default 'topion'.
  assert(self); assert(matrix); assert(nrows != 0); assert(ncols != 0);   
  allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(self, nrows, ncols, matrix, /*mask=*/NULL, /*empty-value=*/default_value_float__, /*isTo_init=*/true, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/true, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/(nrows+ncols));
}
/**
   @brief Allocate and a apply the disjoint-matrix-construction: in this function we support 'disjoint-case-three' (where the latter is described in the Knitting-Tools docuemtaniton and assicated codde-examples) (oekseth, 06. des. 2016).
   @param <self> is the object to boht intaite and apply lgocis for
   @param <nrows_1> is the number of rows in matrix_1
   @param <nrows_2> is the number of rows in matrix_2
   @param <ncols> is the number of columns in matrix_1 and matrix_2
   @param <default_value_float__> is the 'empty value' in your matirx, eg, "T_FLOAT_MAX" or "1": if therea re no 'empty valeus' then your call is pointless (ie, as all regions will tehn be realted to each other): if we your in need 'of fidnign the empty regions' tehn we suggest usign hpLyssis "kt_matrix_filter" ANSI-C class-logics.
   @remarks 
   -- the results may be 'fetched' using our "cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__(..)"
   -- is a permtuation of our "allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(..)" where we facilates the 'merging' of two matrcies using the 'columns'æ as a 'point of itnersection'.
**/
void allocateAndApply__denseMatrix___differentNameSpaceInRowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows_1, const uint nrows_2, const uint ncols, t_float **matrix_1, t_float **matrix_2, const t_float default_value_float__);

/**
   @brief allocates an s_kt_forest_findDisjoint_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <listOf_relations> is the forest/graph/network to evlauate.
 **/
static void allocate__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const s_kt_set_2dsparse_t *listOf_relations) {
  allocate__extensive__s_kt_forest_findDisjoint(self, nrows, /*ncols=*/0, listOf_relations, /*isTo_init=*/true, /*useSeperateNameSpace_rows=*/false, /*useSeperateNameSpace_columns=*/false, /*totalCntUniqueVerticesToEvalauteForAllIterations=*/nrows);
}
/**
   @brief allocates an s_kt_forest_findDisjoint_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <ncols> is the number of rows to allocate
   @param <matrix> is the dense list of interesting relations.
   @param <mask> which if Not set to NULL implies taht all valeus set to "mask[i][j] == 1" is expected to be a relation: if "mask" is used then we expect "matrix == NULL", ie, for consistency.
   // @param <isTo_transpose> which if set to true implies that we tanspsoe teh matrix, ie, before computing the relationships.
   @param <emptyValueIf_maskIsNotUsed> is the valeu which 'sates' that a vertex is empty in "matrix" (eg, "0" or "T_FLOAT_MAX"): if "mask" is used then we expect "mask == NULL"
 **/
static void allocate__denseMatrix__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint nrows, const uint ncols, t_float **matrix, char **mask, const t_float emptyValueIf_maskIsNotUsed) {
  allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(self, nrows, ncols, matrix, mask, emptyValueIf_maskIsNotUsed, /*isTo_init=*/true, /*useSeperateNameSpace_rows=*/false, /*useSeperateNameSpace_columns=*/false, /*totalCntUniqueVerticesToEvalauteForAllIterations=*/nrows+ncols);
}

//! @return true if the object is configured to reverse the relatiosnhisp in the graph-iteraiotn-process (oesketh, 06. otk. 2016).
static inline __attribute__((always_inline)) bool isTo_reverseRelationiships_func(const s_kt_forest_findDisjoint_t *self) {
  assert(self);
  return (
	  (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes)  ||
	  (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_increase)  ||
	  (self->typeOf_startVertices == e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_decrease)  
	  );
}

static inline __attribute__((always_inline)) bool isCorrectlySet__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const bool printWarnignAndHandleObject_ifNotprperly_set) {
  bool is_ok = true;
  if(self->_nrows != self->_ncols) {
    if(self->typeOf_graphTraversalOrder == e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_topology) {
      is_ok = false;
      if(printWarnignAndHandleObject_ifNotprperly_set) {
	fprintf(stderr, "!!\t Seems like you have requested a topolgocial graph-traversal for a case where your inptu-matrix is Not symmeitrc, ie, given the speicfied matrix-diemsnions of [%u, %u]: to overcome this issue we will now change the cofnigruation to \"e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping\": for the future to avoid this erro then please use the latter option for a non-adjcency matrix: for quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", self->_nrows, self->_ncols, __FUNCTION__, __FILE__, __LINE__);
      }
      self->typeOf_graphTraversalOrder = e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_directMapping; //! ie, voercomes 'this issue'.
    }
    const bool isTo_reverseRelationiships = isTo_reverseRelationiships_func(self);
    if(isTo_reverseRelationiships) {
      is_ok = false;
      if(printWarnignAndHandleObject_ifNotprperly_set) {
	fprintf(stderr, "!!\t Seems like you have requested a revert-feature-ordering for a case where your inptu-matrix is Not symmeitrc, ie, given the speicfied matrix-diemsnions of [%u, %u]: to overcome this issue we will now change the cofnigruation to \"e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder\" (or any other non-referse configuraiton): for the future to avoid this erro then please use the latter option for a non-adjcency matrix: for quesitons please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", self->_nrows, self->_ncols, __FUNCTION__, __FILE__, __LINE__);
	assert(!isTo_reverseRelationiships_func(self));
      }
      self->typeOf_startVertices = e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder;
    }
  }

  return is_ok;
}
//! Merge two sets of vertices using a spasrse list (oesketh, 06. feb. 2017):
s_kt_set_2dsparse_t static__merge_sets__kt_forest_findDisjoint(const uint this_key, const uint parent_key, uint *mapOf_vertexMembershipId, s_kt_set_2dsparse_t *listOf_membership_vertexCollection_ref);

/**
   @brief configure the perforamcne-optmizaiton-strategy wrt. the graph-traversal (oekseth, 06. okt. 2016).
   @return false if we had to fix issues wrt. in-correct 'setting' of the configuraitons.
 **/
static bool metaConfigure__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_t typeOf_startVertices, const e_kt_forest_findDisjoint_configureAlgorithm__graphTraversal_t typeOf_graphTraversalOrder, const bool printWarningIfErorr) {
  assert(self);
  self->typeOf_startVertices = typeOf_startVertices;
  self->typeOf_graphTraversalOrder = typeOf_graphTraversalOrder;
  return isCorrectlySet__s_kt_forest_findDisjoint(self, /*printWarnignAndHandleObject_ifNotprperly_set=*/printWarningIfErorr);
}

//! De-allcoates an object of type s_kt_forest_findDisjoint
void free_s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self);
//! --------------------------------------------------------------------------------------------------------------------------------------
//! --------------------------------------------------------------------------------------------------------------------------------------

/**
   @brief idnetifies the forest-id assicated to a given vertex.
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <vertex_id> is the vertex to get the dsijoint-forest for
   @return the forest-id in qestion.
 **/
const uint get_foreestId_forVertex__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint vertex_id);
/**
   @brief idnetifies the forest-id assicated to a given vertex.
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <vertex_id> is the vertex to get the dsijoint-forest for
   @param <datasetId> ist he isneriton-point of the data-set which is evaluated
   @param <vertex_describesRow> which if set to false impleis taht we ivnestigate the name-space for the column
   @return the forest-id in qestion.
   @remarks in cotnrast to the "get_foreestId_forVertex__s_kt_forest_findDisjoint(..)" this funciton re-re-maps the vertex_id into a global namespcae-id, ie, to handle the case where muliple vertices are given as inpuut to the data-set.
 **/
const uint get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint vertex_id, const uint datasetId, const bool vertex_describesRow);
/**
   @brief construct a list of vertex-members for the forest-id in qeustion.
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call
   @param <arrOf_result_vertexMembers> is the dense list of vertex-members in forest_id
   @param <scalar_arrOf_result_vertexMembers> is the number of vertices/elmenets in "arrOf_result_vertexMembers"
   @remarks the funciton may be used as part of a do...while(..) loop in order for seperate clsuter-analsysis of disfjoitn-forests (eg,w rt. alll-agianst-all corrleaiton-analsysis and/or k-means clsteruiign-optmizaiotn).
 **/
void cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint **arrOf_result_vertexMembers, uint *scalar_arrOf_result_vertexMembers);
/**
   @brief extends the "cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(..)" to infer for cases hwere the name-spacess differs (eosketh, 06. des. 2016).
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call
   @param <arrOf_result_vertexMembers_row> is the dense list of vertex-members in forest_id: for rows
   @param <scalar_arrOf_result_vertexMembers_row> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for rows
   @param <arrOf_result_vertexMembers_col> is the dense list of vertex-members in forest_id: for cols
   @param <scalar_arrOf_result_vertexMembers_col> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for cols
   @remarks to itnaite this object please call eitehr "allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(..)" or a prmatuion fo the latter.
 **/
void cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint **arrOf_result_vertexMembers_row, uint *scalar_arrOf_result_vertexMembers_row, uint **arrOf_result_vertexMembers_col, uint *scalar_arrOf_result_vertexMembers_col);

/**
   @brief extends the "cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(..)" to 'handle' the sue-case where we have two input-matrices and where the 'elements' are merged in the column-interseciton-point (oekseth, 06. des. 2016).
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call
   @param <arrOf_result_vertexMembers_row_1> is the dense list of vertex-members in forest_id: for rows in matrix_1
   @param <scalar_arrOf_result_vertexMembers_row_1> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for rows in matrix_1
   @param <arrOf_result_vertexMembers_row_2> is the dense list of vertex-members in forest_id: for rows in matrix_2
   @param <scalar_arrOf_result_vertexMembers_row_2> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for rows in matrix_2
   @param <arrOf_result_vertexMembers_col> is the dense list of vertex-members in forest_id: for cols
   @param <scalar_arrOf_result_vertexMembers_col> is the number of vertices/elmenets in "arrOf_result_vertexMembers": for cols
   @remarks to itnaite this object please call eitehr "allocate__denseMatrix___differentNameSpaceInRowsAndCols__s_kt_forest_findDisjoint(..)" or a prmatuion fo the latter.
 **/
void cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint **arrOf_result_vertexMembers_row_1, uint *scalar_arrOf_result_vertexMembers_row_1, uint **arrOf_result_vertexMembers_row_2, uint *scalar_arrOf_result_vertexMembers_row_2, uint **arrOf_result_vertexMembers_col, uint *scalar_arrOf_result_vertexMembers_col);

static inline __attribute__((always_inline)) uint get_internalStartPos_of_verticesFrom_dataSetId__s_kt_forest_findDisjoint_t(const s_kt_forest_findDisjoint_t *self, const uint datasetId, const bool for_rows) {
  assert(self);
  if(for_rows) {
    assert(self->mapOf_nameSpaceDomain_offsetEnd_rows);
    return self->mapOf_nameSpaceDomain_offsetEnd_rows[datasetId];
  } else {
    assert(self->mapOf_nameSpaceDomain_offsetEnd_columns);
    return  self->mapOf_nameSpaceDomain_offsetEnd_columns[datasetId];
  }
}
/**
   @brief idnetify the datase-t-id for a vertex, ie, if muliple input-datase-ts are used (oekseth, 06. otk. 2016).
   @param <self> is the object which a disoint-forest has been comptued for.
   @param <internal_vertex_id> is the vertex to idneitfy the data-set-membership-id for
   @param <scalar_vertex_describesRow> which is set to false if the vertex describes a acolumn-name-space
   @return the data-set id of a given vertex.
 **/
static inline __attribute__((always_inline)) uint get_datasetIdOf_vertex__s_kt_forest_findDisjoint_t(const s_kt_forest_findDisjoint_t *self, const uint internal_vertex_id, bool *scalar_vertex_describesRow) {
  assert(self); 
  uint result_datasetId = UINT_MAX;
  for(uint datasetId = 0; datasetId < self->mapOf_nameSpaceDomain_offset_rows_cnt; datasetId++) {
    if(internal_vertex_id < self->mapOf_nameSpaceDomain_offsetEnd_rows[datasetId]) {
      if(datasetId < result_datasetId) {
	result_datasetId = datasetId;
      }
    }
  }
  for(uint datasetId = 0; datasetId < self->mapOf_nameSpaceDomain_offset_columns_cnt; datasetId++) {
    if(internal_vertex_id < self->mapOf_nameSpaceDomain_offsetEnd_columns[datasetId]) {
      if(datasetId < result_datasetId) {
	result_datasetId = datasetId;
      }
    }
  }
  assert(result_datasetId != UINT_MAX); //! ie, as we expec t'this' to have been found.
  const uint global_vertex_id = self->mapOf_nameSpaceDomain_offsetEnd_rows[result_datasetId] - internal_vertex_id;  
  //! -----------------------
  //!
  //! @return the global vertex-id:
  return global_vertex_id;
}
/* /\** */
/*    @brief construct a list of vertex-members for the forest-id in qeustion, seperating the  */
/*    @param <self> is the object which a disoint-forest has been comptued for. */
/*    @param <forest_id> is the itnernal forest-id to ge tthe data-set for: to get teh complete number of dsijoint-forests use the "get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, T_FLOAT_MAX, T_FLOAT_MAX);" function-call */
/*    @param <arrOf_result_vertexMembers> is the dense list of vertex-members in forest_id */
/*    @param <scalar_arrOf_result_vertexMembers> is the number of vertices/elmenets in "arrOf_result_vertexMembers" */
/*    @param <arrOf_result_vertexMembers_2> is the dense list of vertex-members in forest_id */
/*    @param <scalar_arrOf_result_vertexMembers_2> is the number of vertices/elmenets in "arrOf_result_vertexMembers" */
/*    @param <forestDescribes_rows> which if set to false impleist ath we 'seprate' the data-sets based on the columns (and not the rows). */
/*    @remarks the funciton may be used as part of a do...while(..) loop in order for seperate clsuter-analsysis of disfjoitn-forests (eg,w rt. alll-agianst-all corrleaiton-analsysis and/or k-means clsteruiign-optmizaiotn). */
/*  **\/ */
/* void cosntructlistOf_forestId_members_twoInputDatasets__s_kt_forest_findDisjoint(const s_kt_forest_findDisjoint_t *self, const uint forest_id, uint *arrOf_result_vertexMembers, uint *scalar_arrOf_result_vertexMembers, uint *arrOf_result_vertexMembers_2, uint *scalar_arrOf_result_vertexMembers_2, const bool forestDescribes_rows); */


//! @return the total number of vertices involved in disjoitn forests.
//! @remarks expect teh number of identified vertices to correspond to the number of unique vertices assoicated to realtions.
const uint get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self);
//! @return if the vertices in question are part of the same disjointForest
const bool partOf_same_disjointForest__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint key_1, const uint key_2);
//! @return a vertex which 'mauy be used as unification for the vertices in a given disjoint forest' (oekseth, 06. feb. 2016).
const uint get_centralityVertex_inDisjointforest__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint vertex_id);
//! Print the disjointForest-mappings
void print_disjointForestsMappings__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, char **mapOf_words);
//! Print the disjointForest-mappings for each vetex
void print_disjointForestMappings_forEachVertex__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, char **mapOf_words);

/**
   @brief identify the number of distjoint clsuters based on the min-max-filter
   @param <countThreshold_min> which is the numumum number of vertices which are to be in a given "idsjoitn forest" (ie, for the forest to be 'counted'): ignroed if countThreshold_min == countThreshold_max
   @param <countThreshold_max> similar to "countThreshold_min" with difference that we evlauate for the max-count: ignroed if countThreshold_min == countThreshold_max
   @return the total number of vertices involved in disjoitn forests.
   @remarks expect teh number of identified vertices to correspond to the number of unique vertices assoicated to realtions.
**/
const uint get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const uint countThreshold_min, const uint countThreshold_max);

//! Idneitfy the max-number-of forests foudn in the dsijoint-forest-object.
static void inline __attribute__((always_inline)) get_maxNumberOf_disjointForests(s_kt_forest_findDisjoint_t *self, uint *scalar_cntForests) {
  //! Note: the [”elow] 'count-proerpty' may be used in k-means-permtaution-algorithms to idnetify the ptautvie k-means-valeus to apply/try:
  *scalar_cntForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
  //assert_possibleOverhead(*scalar_cntForests == get_maxInsertedRowId(&(self->listOf_membership_vertexCollection)));
  //*scalar_cntForests = get_maxInsertedRowId(&(self->listOf_membership_vertexCollection));
}

//! --------------------------------------------------------------------------------------------------------------------------------------
//! --------------------------------------------------------------------------------------------------------------------------------------


/**
   @brief Executes the algorithm for identidcation of disjoint-forests (oekseth, 06. otk. 2016).
   @param <self> is the object to update
   @param <isTo_identifyCentrlaityVertex> which if set to true impluies that we select an arbitrary cetnrality-vertex in each forest to 'represent' a given forest
   @remarks 
   - this function  is the 'main funciton' in the disjoint-forest comptuation/idneitficiaotn.
   - an applicaiton of the "isTo_identifyCentrlaityVertex" parameter conserns the merging/unifciaotn of complex networks, ie, where a 'centrliaty-node' may be used to replace a set of connected synonyms.   
**/
void graph_disjointForests__s_kt_forest_findDisjoint(s_kt_forest_findDisjoint_t *self, const bool isTo_identifyCentrlaityVertex);



/**
   @brief identify the number of disjoitn clusters in a matrix (oekseth,. 06. okt. 2016)
   @param <nrows> is the number of row in the amtrix
   @param <ncols> is the number of columns in the matrix
   @param <matrix> is score-matrix of vertices: note that oru assumption of 'score matrix' is a generalizaiton of an adjcency-matrix, ie, where '0' is considered to reflect a relationship: in order to validate that thios interpretation is proerply understood we inlcud ehte "isTo_useImplictMask", where this proerpty si to be set to true if "mask == NULL", ie, to increase thte change that the funciton is properly used.
   @param <mask> if set then we combien the '0' filter with the "mask[i][j] == 1" test
   @param <isTo_useImplictMask> which if set to true impleis that we investigate for valeus which have "T_FLOAT_MAX" (and if a cell is set to the altter valeu then we asusme the lvaue is Not of interest);
   @param <countThreshold_min> which is the numumum number of vertices which are to be in a given "idsjoitn forest" (ie, for the forest to be 'counted'): ignroed if countThreshold_min == countThreshold_max
   @param <countThreshold_max> similar to "countThreshold_min" with difference that we evlauate for the max-count:  ignroed if countThreshold_min == countThreshold_max
   @param <inputMatrix__isAnAdjecencyMatrix> which is used to investgate/'find' the 'interpretiaotn-case' wrt. the matrix-names-space to use
   @return the number of interesting disjoitn clsuters: if a critial erorr we return UINT_MAX in NDEBUG while call an "assert(false);" in #ifndef NDEBUG
   @remarks 
   - if valeu-fitlering is of itnerest then we suggest using oru rich library of mask-filters, eg, wrt. functions found in our "mask_api.h";
   - no-match: use T_FLOAT_MAX if "mask == NULL", ie, a '0' is interpred as a relationship (ie, as stated in above paramter-description);
 **/
uint find_cntClusters_inMatrix__s_kt_forest_findDisjoint(const uint nrows, const uint ncols, t_float **matrix, char **mask, const bool isTo_useImplictMask, const uint countThreshold_min, const uint countThreshold_max, const bool inputMatrix__isAnAdjecencyMatrix);

#endif //! EOF
