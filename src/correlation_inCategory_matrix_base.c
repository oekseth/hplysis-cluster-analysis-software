#include "correlation_inCategory_matrix_base.h"

//! Copmute the optmial distance-matrix:
void ktCorr_matrixBase__optimal_compute_allAgainstAll_nonSIMD(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, s_allAgainstAll_config_t *config_allAgainstAll) { //const uint CLS, const bool isTo_use_continousSTripsOf_memory, uint iterationIndex_2) {

  //! Tehn inlcude the lgocis: a slow 'inner part' of 'this funciton' which is used to ivnesitgate the improtance of our SSE-tempalte-funciton wrt. perfomrqance.
  #include "ktCorr_matrixBase__optimal_compute_allAgainstAll_nonSIMD.c"
}



//!******************************************************************************************************************************************
//!******************************************************************************************************************************************
//!******************************************************************************************************************************************

//! Copmute the optmial distance-matrix:
void ktCorr_matrixBase__optimal_compute_allAgainstAll(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config_t *config_allAgainstAll) {
  assert(config_allAgainstAll);
  //void ktCorr_matrixBase__optimal_compute_allAgainstAll(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const uint CLS, const bool isTo_use_continousSTripsOf_memory, uint iterationIndex_2, const bool isTo_use_SIMD, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const e_cmp_masksAre_used_t  masksAre_used ) {
  
  // FIXME: add perfomrance-tets wrt. the isTo_use_SIMD option
  if(config_allAgainstAll->forNonTransposed_useFastImplementaiton_useSSE) {
    fprintf(stderr, "!!\t This 'call' has now been moved to our \"kt_distance.h\", ie, please udpate your function-calls: the latter is Not implcit supported (int thsi fucntion) to avodi cyclci depencneis between soruce-code-chunks: will now continue using a non-SSE implementaiton. Observation made by (oekseth, 06. okt. 2016), and expressed at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    // assert(false);
  } //config_allAgainstAll->CLS, config_allAgainstAll->isTo_use_continousSTripsOf_memory, config_allAgainstAll->iterationIndex_2, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, typeOf_metric_correlation, config_allAgainstAll->masksAre_used);}
  //else 
  {
    //! To simplify our code-writing we assume that [”elow] parameters are only used in the optmal code-mode:
    // TODO: consider to support [”elow] ... or write an ifnromative error-message.
    assert(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_undef);
    assert(config_allAgainstAll->s_inlinePostProcess == NULL);
    assert(typeOf_metric_correlation != e_typeOf_metric_correlation_pearson_undef);


    //! Then compute:
    ktCorr_matrixBase__optimal_compute_allAgainstAll_nonSIMD(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, typeOf_metric, transpose, config_allAgainstAll); //->CLS, config_allAgainstAll->isTo_use_continousSTripsOf_memory, config_allAgainstAll->iterationIndex_2);}
  }
}



//! @brief comptue the correlations in a matrix.
//! @remarks in this function we test the effect of computing 'sums seperately' VS 'the naive/old/previous method', eg, wrt. "sum1 += w*term1;" sum2 += w*term2;", "denom1 += w*term1*term1;", "denom2 += w*term2*term2;"  and "tweight += weight[i];" in the "graphAlgorithms_distance::correlation(..)" function.  Differently tols, we test the effect of both using temporary data-structures 'an not using temproary data-structures', ie, as correlation may be comptued using either a 'muliple mulitciaotn of the same values' or 'moving out the duplciatioe copmtautiosn into seperate lists/data-structures'.
//void ktCorr_compute_allAgainstAll_distanceMetric_correlation(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, bool transpose, const enum e_typeOf_optimization_distance typeOf_optimization ,  const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, e_cmp_masksAre_used_t  masksAre_used ) { 
void ktCorr_compute_allAgainstAll_distanceMetric_correlation(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, bool transpose,  s_allAgainstAll_config_t *config_allAgainstAll) { 
  assert(config_allAgainstAll);

  bool needTo_useMask_evaluation = (config_allAgainstAll->masksAre_used == e_cmp_masksAre_used_false); //! ie, investigate if the caller has declared that 'all valeus are of itnerest', eg, for the case where the "euclidc distance metric" is used in the k-means procedure, ie, where a acell-dsitance=0 is 'enough' to cpature/descirbe a non-set cell (ie, as the latter value will then be ingored form the comptaution).
  if(config_allAgainstAll->masksAre_used == e_cmp_masksAre_used_undef) {
    assert(data1); assert(data2);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncols, data1, data2, mask1, mask2, transpose, config_allAgainstAll->iterationIndex_2);
    config_allAgainstAll->masksAre_used = (needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  } 
  // const bool needTo_useMask_evaluation = graphAlgorithms_distance::matrixMakesUSeOf_mask(nrows, ncols, data1, data2, mask1, mask2, transpose, iterationIndex_2);
  // const e_cmp_masksAre_used_t  masksAre_used = (needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;


  // FIXME: write benchmark-tests for the different 'atklernatives' in this function.

  //const bool needTo_useMask_evaluation = graphAlgorithms_distance::matrixMakesUSeOf_mask(nrows, ncols, data1, data2, mask1, mask2, transpose, iterationIndex_2);
  
  if( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible) || (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_undef) ) {
    //! Note: [”elow] two chunks are a copy-paste of the euclidiaon algorithm, ie, with difference of the 'summations' to use.

    //! Then comptue the optmial distance-matrix:
    ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, 
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
						     /*type=*/e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly, 
#else
						     e_kt_correlationFunction_groupOf_minkowski_euclid,
#endif
						     transpose, typeOf_metric_correlation, config_allAgainstAll);
    //ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*type=*/e_kt_correlationFunction_spearman_or_correlation, transpose, config_allAgainstAll->CLS, config_allAgainstAll->isTo_use_continousSTripsOf_memory, config_allAgainstAll->iterationIndex_2,  /*isTo_use_SIMD=*/true, config_allAgainstAll->typeOf_postProcessing_afterCorrelation, config_allAgainstAll->s_inlinePostProcess, typeOf_metric_correlation, config_allAgainstAll->masksAre_used);
    if(resultMatrix) {
      //! Update, ie, adjust the score by the weight: we update the result:
      const uint iterationIndex_2 = (config_allAgainstAll) ? config_allAgainstAll->iterationIndex_2 : UINT_MAX;
      compute_score_correlation_forMatrix(nrows, ncols, data1, data2, mask1, mask2, resultMatrix, transpose, weight, typeOf_metric_correlation, needTo_useMask_evaluation, iterationIndex_2);
    } //! else we asusem the correlations has been comptued seperately in the "ktCorr_matrixBase__optimal_compute_allAgainstAll(..)" call.
  } else if(config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible_subOptimal_targetSpecific_code_instructionRewriting) {
    assert(resultMatrix != NULL); //! ie, as we do not expect the s_inlinePostProcess variable to be used for this 'comptuatonal task'.
    if(transpose == 0) {
      for(uint index1 = 0; index1 < nrows; index1++) {
	const uint nrows_local = (data1 == data2) ? index1+1 : nrows;
	for(uint index2 = 0; index2 < nrows_local; index2++) {
	  const uint sizeOf_i = ncols;
	  for(uint i = 0; i < sizeOf_i; i++)  {
	    if( ( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) ) {	
	      if(isOf_interest(data1[index1][i]) && isOf_interest(data2[index2][i])) {
		const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
		const t_float result_local = w*data1[index1][i]*data2[index2][i];
		resultMatrix[index1][index2] += result_local;
	      }
	    }
	  }
	}
      }
    } else { //! then we comptue for the 'transpsoed alternative':
      const uint sizeOf_i = nrows;
      for(uint i = 0; i < sizeOf_i; i++) {
	const t_float weight_local = (weight) ? weight[i] : 1;
	for(uint index1 = 0; index1 < ncols; index1++) {
	  if(!mask1 || mask1[i][index1]) {
	    if(isOf_interest(data1[i][index1])) {
	      const uint ncols_local = (data1 == data2) ? index1+1 : ncols;
	      for(uint index2 = 0; index2 < ncols_local; index2++) {
		if(!mask2 || mask2[i][index2]) {
		  if(isOf_interest(data2[i][index2])) {
		    const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
		    const t_float result_local = w*data1[i][index1]*data2[i][index2];
		    resultMatrix[index1][index2] += result_local;
		  }
		}
	      }
	    }
	  }
	}
      }
    }    

    if(resultMatrix) {
      //! Update, ie, adjust the score by the weight: we update the result:
      const uint iterationIndex_2 = (config_allAgainstAll) ? config_allAgainstAll->iterationIndex_2 : UINT_MAX;
      compute_score_correlation_forMatrix(nrows, ncols, data1, data2, mask1, mask2, resultMatrix, transpose, weight, typeOf_metric_correlation, needTo_useMask_evaluation, iterationIndex_2);
    } //! else we asusem the correlations has been comptued seperately in the "ktCorr_matrixBase__optimal_compute_allAgainstAll(..)" call.
  } else if( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmt_functionInlined) ) {
    assert(resultMatrix != NULL); //! ie, as we do not expect the s_inlinePostProcess variable to be used for this 'comptuatonal task'.


    assert(false); // FIXME: inlcude [”elow] ... ie, after we have updated/written our Perl-inlined compilation-script

    //! Ntoe: test the effect of 'inlining' both the "transpose" caluse and the fucntion-code itself.
    // if(transpose == 0) {
    //   for(uint index1 = 0; index1 < nrows; index1++) {
    // 	for(uint index2 = 0; index2 < nrows; index2++) {
    // 	  const uint internal_block_size = ncols; //! ie, as the 'funciton we call' iterates/investigates/travers each of the columns for the indices.
    // 	  if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_basic) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::correlation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_absolute) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::acorrelation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::ucorrelation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered_absolute) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::uacorrelation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else {
    // 	    assert(false); //! ie, then investigate this case.
    // 	    //! Then in optmized mode call the 'fall-back' function:
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::correlation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  }
    // 	}
    //   }
    // } else { //! then we compare the columns:
    //   for(uint index1 = 0; index1 < ncols; index1++) {
    // 	for(uint index2 = 0; index2 < ncols; index2++) {
    // 	  const uint internal_block_size = nrows; //! ie, as the 'funciton we call' iterates/investigates/travers each of the rows for the indices.
    // 	  if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_basic) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::correlation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_absolute) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::acorrelation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::ucorrelation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else if(typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_uncentered_absolute) {
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::uacorrelation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  } else {
    // 	    assert(false); //! ie, then investigate this case.
    // 	    //! Then in optmized mode call the 'fall-back' function:
    // 	    resultMatrix[index1][index2] = graphAlgorithms_distance::correlation(internal_block_size, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	  }
    // 	  //resultMatrix[index1][index2] = graphAlgorithms_distance::euclid(nrows, data1, data2, mask1, mask2, weight, index1, index2, transpose);
    // 	}
    //  }
  //}
  } else if( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none) || (!transpose && (
											     (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed) ||  (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal)
											     )) ) {
    //! Note: this approach is the 'default approach' which is seen in life-science research-software, eg, in the popular "C clustering software" lbirary/tool.
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.
    const uint sizeOf_i = (transpose == 0) ? ncols : nrows;
    for(uint index1 = 0; index1    < (transpose==0) ?  nrows : ncols; index1++) {
      for(uint index2 = 0; index2 <  (transpose==0) ?  nrows : ncols; index2++) {
	
	t_float result = 0.;
	t_float sum1 = 0.;   t_float sum2 = 0.;
	t_float denom1 = 0.;   t_float denom2 = 0.;
	t_float tweight = 0.;

      //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare the same column in mulitple row: if the column-size does not signficantly exceed the "L2" memory-cache-size (for a given computer-memory-hardware) then we expect an 'ideal' memory-allocation-scheme to provide/give a "linear" "n" speed-up (for every call to this function).
	if(transpose == 0) { /* Calculate the distance between two rows */
	  for(uint i = 0; i < sizeOf_i; i++)  {
	    if( ( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) ) {	
	      if(isOf_interest(data1[index1][i]) && isOf_interest(data2[index2][i])) {
		const t_float term1 = data1[index1][i]; 
		const t_float term2 = data2[index2][i];
		const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
		sum1 += w*term1;
		sum2 += w*term2;
		result += w*term1*term2;
		denom1 += w*term1*term1;
		denom2 += w*term2*term2;
		tweight += w;
	      }
	    } 
	  }
	} else { //! then we expect the code to go slower.
	  for(uint i = 0; i < sizeOf_i; i++) {
	    if( ( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) ) {
	      if(isOf_interest(data1[i][index1]) && isOf_interest(data2[i][index2])) {
		const t_float term1 = data1[index1][i];
		const t_float term2 = data2[index2][i];
		const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
		sum1 += w*term1;
		sum2 += w*term2;
		result += w*term1*term2;
		denom1 += w*term1*term1;
		denom2 += w*term2*term2;
		tweight += w;
	      }
	    }
	  }
	}
	//! Update, ie, adjust the score by the weight
	resultMatrix[index1][index2] = compute_score_correlation(result, sum1, sum2, tweight, denom1, denom2, typeOf_metric_correlation, sizeOf_i);
      }
    }
  } else if(transpose && 
	    ( (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed) ||  (config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal) ) ) {
    assert(resultMatrix != NULL); //! ie, as we do not expect the s_inlinePostProcess variable to be used for this 'comptuatonal task'.
    //! Then we optimize the matrix-sum-operation wrt. transposed matrix-computation:
    //! Note(1): a challenge with this approach is the need to store 'temporal' data, ie, a re-write of the 'orignal' function.
    //! Note(2): basic idea is to mvoe the 'variable step' in the 'middle of the comptuation', ie, instead of 'as the last/inner comptaution'. When studying the improved/udpated memroy-access-pattersn (of below implementaiton) we observe that instead of having "nrows * nrows * ncols"  'potential' cache-misses, the number of 'potential' cache-misses are reduced to ...??..    
    //! Note(3): when compared to [above] "e_typeOf_optimization_distance_none" we observe an optimization-effect of ...??..
    const uint sizeOf_i = nrows;

    if(config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed) {
      for(uint i = 0; i < sizeOf_i; i++) {
	const t_float weight_local = (weight) ? weight[i] : 1;
	//! Note: the order of [below] for-loops is due to the assumption/assertion/observation that the rows of "data1[i]" and "data2[i]" are accessed mulitple times, ie, we are itnerested in 'having the latter as long as possible in memory-cache'.
	//! Note(2): wrt. memory-cache-misses, we expect the data-sets to be assicated to "nrows * 3" memory-cache-misses.
	for(uint index1 = 0; index1 < ncols; index1++) {
	  if(!mask1 || mask1[i][index1]) {
	    if(isOf_interest(data1[i][index1])) {
	      for(uint index2 = 0; index2 < ncols; index2++) {
		if(!mask2 || mask2[i][index2]) {
		  if(isOf_interest(data2[i][index2])) {
		    const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
		    const t_float result_local = w*data1[index1][i]*data2[index2][i];
		    resultMatrix[index1][index2] += result_local;
		  }
		}
	      }
	    }
	  }
	}
      }
    } else { //! Then we evlaute/test the effect of having to load the data1[i] row into mmemory mulitple tiems, ie, the importance of order wrt. mulitiple for-loops.
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint i = 0; i < sizeOf_i; i++) {
	  if(!mask1 || mask1[i][index1]) {
	    if(isOf_interest(data1[i][index1])) {
	      const t_float weight_local = (weight) ? weight[i] : 1;
	      for(uint index2 = 0; index2 < ncols; index2++) {
		if(!mask2 || mask2[i][index2]) {
		  if(isOf_interest(data2[i][index2])) {
		    const t_float term1 = data1[index1][i];
		    const t_float term2 = data2[index2][i];
		    const t_float w = (weight) ? weight[i] : 1; // const t_float w = weight[i];
		    const t_float result_local = w*term1*term2;
		    resultMatrix[index1][index2] += result_local;
		  }
		}
	      }
	    }
	  }
	}
      }
    }

    if(resultMatrix) {
      //! Update, ie, adjust the score by the weight
      const uint iterationIndex_2 = (config_allAgainstAll) ? config_allAgainstAll->iterationIndex_2 : UINT_MAX;
      compute_score_correlation_forMatrix(nrows, ncols, data1, data2, mask1, mask2, resultMatrix, transpose, weight, typeOf_metric_correlation, needTo_useMask_evaluation, iterationIndex_2);
    } //! else we asusem the correlations has been comptued seperately in the "ktCorr_matrixBase__optimal_compute_allAgainstAll(..)" call.
  } else {
    assert(false); //! then we need to add support for this.
  }  
}


