#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.

/**
   @brief use logics in our "hp_clusterFileCollection" to perofmr a perfmroamcne-evlauation of small data-sets, both wr.t a subset of real-life and systentic cases (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a permtuation of "tut_inputFile_2.c", "tut_inputFile_3.c" and "tut_inputFile_4.c", where we use a colleciton of data-sets parsed in our "./data/local_downloaded/convert.pl" Perl-script.
   @remarks in this test-case we examplify how to ause an utato-generated set of objects: for a subset of the data-sets we 'specify' that the first-column 'cotantins' a "_BR_" idnetifer (ie, a "break" symbol) to idneitfy the gold-case-clsuter-groups. Simliaryt to the otehr/above/ealrier examples were here also provide logics for 'cocnatnating into' a emrge data-sets.
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;

  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;

  /* const uint config__kMeans__defValue__k__min = 2; */
  /* const uint config__kMeans__defValue__k__max = 4; */
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "binomial_p005";
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //obj_config.globalConfig__isToStore__inputMatrix__inFormat__csv = isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.


  //assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
  //const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  /* //! -------------------------------------------- */
  /* //! */
  /* //! File-specific cofnigurations:  */
  /* s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t(); */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = false; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;   */
  /* fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file. */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config; */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = true; */
  /* //! */
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //!
  //! We are interested in a more performacne-demanind approach: 
  const uint sizeOf__nrows = 500;
  const uint sizeOf__ncols = 500;
  fileRead_config.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config.fileIsRealLife = false; //! ie, the 'important part' of this.
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  s_kt_matrix_fileReadTuning_t fileRead_config__concat = fileRead_config;
  s_kt_matrix_t matrix_concatToAll; setTo_empty__s_kt_matrix_t(&matrix_concatToAll);
  if(config__nameOfDefaultVariable && strlen(config__nameOfDefaultVariable)) {
    matrix_concatToAll = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable, fileRead_config__concat);
    //! 
    //! Update the configuraiton-object:
    assert(matrix_concatToAll.ncols > 0);
    fileRead_config.mat_concat = &matrix_concatToAll;
  }
  //! -------------------------------------------------------------------------
  //!
  //!
  //! Intiate the colleciton of real-life data-sets:
  const uint mapOf_realLife_size = 66; //! ie, "wc -l ./data/local_downloaded/metaObj.c"
    s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = {
#include "data/local_downloaded/metaObj.c" //! ie, the colleciton of data-files, a colleciton where we asusme 'the caller mof this srcirpt' is situtated in the "src/" folder of the hpLysis software.
  };


  //! ----------------------------------
  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_concatToAll);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
