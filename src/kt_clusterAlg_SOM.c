#include "kt_clusterAlg_SOM.h"

#include "graphAlgorithms_som.h"

#include "math_generateDistribution.h"
#include "maskAllocate.h"
#include "matrix_transpose.h"
//! @return true if we asusem teh t_float-value is of interest.
static t_float isOf_interest_func(const t_float value) {
  return ( (value != FLT_MAX) && (value != T_FLOAT_MIN_ABS) );
}


#if(configure_isTo_useLog == 1)
static s_log_clusterC_t logFor_SOM;
#endif

/**
   Note:w rt.e comptuational compelxity we observe:
   * __update_closest_pairs(...) 
   ** internal: #(x-grid)*x(y-grid)*#(rows)*3 time
   ** called: #(iterations) time.
   -
   * compute STD: #(rows)*#(columns) <-- for the 'not a mask' case SSE may effectivly be used.
   * compute uniform(..):  internal: ...??.. ; called: #(x-grid)*x(y-grid)*#(rows)
   * 'the iterative proecudre': #(iterations)*  

   <-- scaffold optimization-strategies, and evalaute 'their' time-effect.
   
   <---- "math_generateDistribution__uniform(..)"


**/


// //! Note: this funciton is used to 'only' iterate throug half of the items in a [][] matrix.
// static void __get_updatedNumberOF_lastSizeChunk_entry(const uint nrows, const uint SM, uint *numberOf_chunks, uint *chunkSize_row_last) {
//   assert(nrows >= SM);
//   *numberOf_chunks = nrows / SM;
//   *chunkSize_row_last = nrows - (SM* *numberOf_chunks);
//   assert( (*chunkSize_row_last + SM* *numberOf_chunks) == nrows );
//   if(*chunkSize_row_last == 0) {*chunkSize_row_last = SM;}
//   assert(*chunkSize_row_last <= SM);
// }


static void __update_closest_pairs_improved_memoryTiled(const uint iobject, const t_float stddata_iobject, const uint nxgrid, const uint nygrid, const uint ncols, const t_float tau, const t_float radius, t_float **data, t_float ***celldata, const uint ixbest, const uint iybest, const uint CLS /* = 64 */, const bool isTo_useSSE /* = true */) {
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif
  assert( (ncols % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, to simplify our loops wrt. SSE intrisntictics  
  // assert( (nxgrid % cntBlocks_grid) == 0);
  // assert( (nygrid % cntBlocks_grid) == 0);
  assert( (ncols % VECTOR_FLOAT_ITER_SIZE) == 0); //! ie, to simplify our loops wrt. SSE intrisntictics
  assert(nxgrid < 300); //! ie, to simplify our code-writing.
  assert(nygrid < 300); //! ie, to simplify our code-writing.
  assert(CLS > 0); 
  //! ----------------------------------------------
  const uint SM = (uint)(CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  // assert(nrows > 0); 
  assert(ncols > 0); assert(SM > 0);
  assert( (ncols % SM) == 0); //! ie, what we expect [”elow]
  const uint numberOf_chunks_column = ncols / SM; const uint chunkSize_colum_last = SM;
  //! ----------------------------------------------


  t_float *__restrict__ rres;  const t_float *__restrict__ rmul1; 
  //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
  const t_float stddata_iobject_inv = (stddata_iobject != 0) ? 1/stddata_iobject : 1;
  const t_float sqrt_ncols = sqrt(ncols);
  if(isTo_useSSE == false) {
    //uint debug_cntIterated = 0;
    for(uint cnt_index_column = 0, index_row = 0; cnt_index_column < numberOf_chunks_column; cnt_index_column++, index_row += SM) {
      rmul1  = &data[iobject][index_row]; //! ie, the 'upper square' of the matrix we update.		   		   
      for(uint ix = 0; ix < nxgrid; ix++) {
	const uint sumOf_error_x = (ix-ixbest)*(ix-ixbest);
	for(uint iy = 0; iy < nygrid; iy++) {
	  //uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i;
	  uint i2 = 0;
	  //! Compute: (x - max(x))^2 + (y - max(y))^2
	  const uint sumOf_error = sumOf_error_x + (iy-iybest)*(iy-iybest);
	  if(sqrt(sumOf_error) < radius) {
	    for (i2 = 0, rres = &celldata[ix][iy][index_row]
		   ;
		 i2 < SM;
		 i2++ 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		   //rres += ncols,  rmul1 += ncols
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {
	      //celldata[ix][iy][i] += tau * (data[iobject][i]/stddata_iobject - celldata[ix][iy][i]);
	      const t_float rres_old = rres[i2];
	      const t_float rmul1_div = rmul1[i2] * stddata_iobject_inv;
	      const t_float tmp_mul = tau*( rmul1_div - rres_old );
	      rres[i2] = rres_old + tmp_mul;
	      //debug_cntIterated++;
	    }
	  }
	}
      }
    }


    
    {//! Tehn the 'post operaiton':
      for(uint ix = 0; ix < nxgrid; ix++) {
	const uint sumOf_error_x = (ix-ixbest)*(ix-ixbest);
	for(uint iy = 0; iy < nygrid; iy++) {
	  //! Compute: (x - max(x))^2 + (y - max(y))^2
	  const uint sumOf_error = sumOf_error_x + (iy-iybest)*(iy-iybest);
	  if(sqrt(sumOf_error) < radius) {
	    t_float sum = 0;
	    for(uint i = 0; i < ncols; i++) {
	      // TODO: instead of [”elow] use _mm_storeu_ps
	      t_float term = celldata[ix][iy][i];
	      //printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
	      term = term * term;
	      sum += term;
	    }
	    //if(sum>0) {
	    if(sum == 0) {sum = 1;}
	    {
	      // TODO[performance]: test the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
	      const t_float sum_inv = sqrt_ncols/sqrt(sum);
	      uint i = 0;
	      for(; i < ncols; i++) {
		celldata[ix][iy][i] *= sum_inv;
		//printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
	      }
	    } 
	  }
	}
      }
    }



    // printf("cnt=%.3E, at %s:%d\n", (float)debug_cntIterated, __FILE__, __LINE__);
  } else {

    // const uint ncolumns_intri = (uint)VECTOR_FLOAT_maxLenght_forLoop(ncols);
    const VECTOR_FLOAT_TYPE vec_tau = VECTOR_FLOAT_SET1(tau);
    const VECTOR_FLOAT_TYPE vec_stddata_iobject_inv = VECTOR_FLOAT_SET1(stddata_iobject_inv);
    //const uint ncolumns_intri = (uint)VECTOR_FLOAT_maxLenght_forLoop(ncols);
    const uint ncolumns_intri = ncols;
    // uint debug_cntIterated = 0;
    for(uint cnt_index_column = 0, index_row = 0; cnt_index_column < numberOf_chunks_column; cnt_index_column++, index_row += SM) {
      rmul1  = &data[iobject][index_row]; //! ie, the 'upper square' of the matrix we update.		   		   
      for(uint ix = 0; ix < nxgrid; ix++) {
	for(uint iy = 0; iy < nygrid; iy++) {
	  //uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i;
	  uint i2 = 0;
	  for (i2 = 0, rres = &celldata[ix][iy][index_row]
		 ;
	       i2 < SM;
	       i2 += VECTOR_FLOAT_ITER_SIZE
		 //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		 //rres += ncols,  rmul1 += ncols
		 //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
	       ) {
	    const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOAD(&rres[i2]);
	    const VECTOR_FLOAT_TYPE vec_inp = VECTOR_FLOAT_LOAD(&rmul1[i2]);
	    const VECTOR_FLOAT_TYPE vec_delta = VECTOR_FLOAT_SUB(VECTOR_FLOAT_MUL(vec_inp, vec_stddata_iobject_inv), vec_old);
	    const VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_tau, vec_delta);
	    //const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	    
	    
	    /* if(true) { // TODO: remove the contents of this block: */
	      
	    /*   assert(false); // FIXEM: write a permtuation of [”elow]! */

	    VECTOR_FLOAT_STORE(&rres[i2], VECTOR_FLOAT_ADD(vec_old, vec_mul));
	    //} else {
	      VECTOR_FLOAT_STORE(&rres[i2], VECTOR_FLOAT_ADD(vec_old, vec_mul));
	      //}
	    // debug_cntIterated += VECTOR_FLOAT_ITER_SIZE;
	    //rres[i2] = ( rmul1[i2] - rres[i2] );
	  }
	}
      }
    }



    { //! Then the 'post-operaiotn'
      for(uint ix = 0; ix < nxgrid; ix++) {
	const uint sumOf_error_x = (ix-ixbest)*(ix-ixbest);
	for(uint iy = 0; iy < nygrid; iy++) {
	  //! Compute: (x - max(x))^2 + (y - max(y))^2
	  const uint sumOf_error = sumOf_error_x + (iy-iybest)*(iy-iybest);
	  if(sqrt(sumOf_error) < radius) {
	    uint i = 0;
	    VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	    /* printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__); */
	    /* printf("valeus-first=[%f, %f, %f, %f] for ncolumns_intri=%u, at %s:%d\n", celldata[ix][iy][0], celldata[ix][iy][1], celldata[ix][iy][2], celldata[ix][iy][3], ncolumns_intri, __FILE__, __LINE__); */
	    /* printf("sum-of-first=%f for ncolumns_intri=%u, at %s:%d\n", VECTOR_FLOAT_storeAnd_horizontalSum(vec_input), ncolumns_intri, __FILE__, __LINE__); */
	    VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_MUL(vec_input, vec_input);
	    VECTOR_FLOAT_TYPE vec_sum = vec_tmp;
	    i += VECTOR_FLOAT_ITER_SIZE;
	    for (i=0; i < ncolumns_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	      VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	      VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_MUL(vec_input, vec_input);
	      //! Update teh count:
	      vec_sum = VECTOR_FLOAT_ADD(vec_sum, vec_tmp);
	    }
	    t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);
	    if(sum == 0) {sum = 1;}
	    {
	      // TODO[performance]: test the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
	      const t_float sum_inv = sqrt_ncols/sqrt(sum);
	      uint i = 0;
	      VECTOR_FLOAT_TYPE vec_toMul = VECTOR_FLOAT_SET1(sum_inv);
	      for (i=0; i < ncolumns_intri; i += VECTOR_FLOAT_ITER_SIZE) {
		//const VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_delta);
		const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
		const VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_toMul, vec_old);
		VECTOR_FLOAT_STORE(&celldata[ix][iy][i], VECTOR_FLOAT_ADD(vec_old, vec_mul));
	      }
	    }
	  }
	}
      }
    }
    // printf("cnt=%.3E, at %s:%d\n", (float)debug_cntIterated, __FILE__, __LINE__);
  }



  //const uint matrix_size = nrows * ncols;
#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif
}

static void __update_closest_pairs_improved_SSE(const uint iobject, const uint nxgrid, const uint nygrid, const uint ncols, const t_float tau, const t_float radius, t_float **data_dividedBy_stddate_iObject, t_float ***celldata, const uint ixbest, const uint iybest) {
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif


  /* { // FIXME: remove the cotnents of this block! */
  /*   t_float tmp = 10;      t_float tmp_2 = 10; */
  /*   for(uint ix = 0; ix < nxgrid; ix++) { */
  /*     for(uint iy = 0; iy < nygrid; iy++) { */
  /* 	for(uint i = 0; i < ncols; i++) { */
  /* 	  tmp += celldata[ix][iy][i]; */
  /* 	  tmp_2 *= data_dividedBy_stddate_iObject[iobject][i]; */
  /* 	} */
  /*     } */
  /*   } */
  /*   tmp = tmp * 0.5; //! ie, to get Valgrind warn about un-itnalised values. */
  /*   tmp_2 = tmp_2 * 0.5;  //! ie, to get Valgrind warn about un-itnalised values. */
  /*   tmp_2 = tmp_2 * tau;  //! ie, to get Valgrind warn about un-itnalised values. */
  /*   assert(tmp != T_FLOAT_MIN_ABS); //! ie, to 'increase the change chat Valgrind will give us an heads-up' */
  /*   assert(tmp_2 != T_FLOAT_MIN_ABS); //! ie, to 'increase the change chat Valgrind will give us an heads-up' */
  /*   if(tmp_2 != 0) { */
  /*     tmp_2 *2; //! ie, to 'rpvoke' a memroyc-rrecutpion-sissue'. */
  /*   } */
  /* } */

  const t_float sqrt_ncols = sqrt(ncols);
  // uint cnt_sumsIgnored = 0;
  const uint ncolumns_intri = (uint)VECTOR_FLOAT_maxLenght_forLoop(ncols);
  assert(ncolumns_intri <= ncols);
  assert(ncolumns_intri != 0); //! ie, as we otehrwise should Not have cfalled this fucniton.

  const VECTOR_FLOAT_TYPE vec_tau = VECTOR_FLOAT_SET1(tau);
  for(uint ix = 0; ix < nxgrid; ix++) {
    const uint sumOf_error_x = (ix-ixbest)*(ix-ixbest);
    for(uint iy = 0; iy < nygrid; iy++) {
      //! Compute: (x - max(x))^2 + (y - max(y))^2
      const uint sumOf_error = sumOf_error_x + (iy-iybest)*(iy-iybest);
      if(sqrt(sumOf_error) < radius) {
	//t_float sum = 0.;
	uint i = 0;
	/* { // FIXME: remove this block! */
	/*   t_float sum_tmp = 0; */
	/*   for(uint i = 0; i < ncols; i++) { */
	/*     sum_tmp += celldata[ix][iy][i]; */
	/*   } */
	/*   assert(ncolumns_intri <= ncols); //! ie, as we otehrwise have an isnconstnecy.	   */
	/*   if(sum_tmp == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	/* } */
	for (; i < ncolumns_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	  const VECTOR_FLOAT_TYPE vec_delta = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&data_dividedBy_stddate_iObject[iobject][i]), VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]));
	  /* { // FIXME: remove this 'bunch'. */
	  /*   t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_LOADU(&data_dividedBy_stddate_iObject[iobject][i])); */
	  /*   if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	  /* } */
	  /* { // FIXME: remove this 'bunch'. */
	  /*   t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_delta); */
	  /*   if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	  /* } */
	  const VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_tau, vec_delta);
	  const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	  VECTOR_FLOAT_STORE(&celldata[ix][iy][i], VECTOR_FLOAT_ADD(vec_old, vec_mul));
	}
	/* { // FIXME: remove this block! */
	/*   t_float sum_tmp = 0; */
	/*   for(uint i = 0; i < ncols; i++) { */
	/*     sum_tmp += celldata[ix][iy][i]; */
	/*   } */
	/*   assert(ncolumns_intri <= ncols); //! ie, as we otehrwise have an isnconstnecy.	   */
	/*   if(sum_tmp == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	/* } */
	for(; i < ncols; i++) {
	  celldata[ix][iy][i] += tau * (data_dividedBy_stddate_iObject[iobject][i] - celldata[ix][iy][i]);
	  // t_float term = celldata[ix][iy][i];
	  // term = term * term;
	  // sum += term;
	}
	/* { // FIXME: remove this block! */
	/*   t_float sum_tmp = 0; */
	/*   for(uint i = 0; i < ncols; i++) { */
	/*     sum_tmp += celldata[ix][iy][i]; */
	/*   } */
	/*   assert(ncolumns_intri <= ncols); //! ie, as we otehrwise have an isnconstnecy.	   */
	/*   if(sum_tmp == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	/* } */
	//! ---------------------------------------
	i = 0;
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	/* { // FIXME: remove this 'bunch'. */
	/*   t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_input); */
	/*   if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	/* } */
	VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_MUL(vec_input, vec_input);
	VECTOR_FLOAT_TYPE vec_sum = vec_tmp;
	i += VECTOR_FLOAT_ITER_SIZE;
	/* { // FIXME: remove this 'bunch'. */
	/*   t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_tmp); */
	/*   if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	/* } */
	for (; i < ncolumns_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	  VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	  /* { // FIXME: remove this 'bunch'. */
	  /*   t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_input); */
	  /*   if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	  /* } */
	  VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_MUL(vec_input, vec_input);
	  /* { // FIXME: remove this 'bunch'. */
	  /*   t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_tmp); */
	  /*   if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value. */
	  /* } */
	  //! Update teh count:
	  vec_sum = VECTOR_FLOAT_ADD(vec_sum, vec_tmp);
	}
	t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);
	
	//if(sum == 0) {t_float sum_tmp = 2;} //! FIXME: remove <-- primalry used to ffinds osurce of un-intalised value.

	for(; i < ncols; i++) {
	  // TODO: instead of [”elow] use _mm_storeu_ps
	  t_float term = celldata[ix][iy][i];
	  term = term * term;
	  sum += term;
	}
	//if(sum>0) {
	if(sum == 0) {sum = 1;}
	{
	  // TODO: text the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
	  const t_float sum_inv = sqrt_ncols/sqrt(sum);
	  uint i = 0;
	  VECTOR_FLOAT_TYPE vec_toMul = VECTOR_FLOAT_SET1(sum_inv);
	  for (i=0; i < ncolumns_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //const VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_mul, vec_delta);
	    const VECTOR_FLOAT_TYPE vec_old = VECTOR_FLOAT_LOADU(&celldata[ix][iy][i]);
	    const VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_toMul, vec_old);
	    VECTOR_FLOAT_STORE(&celldata[ix][iy][i], VECTOR_FLOAT_ADD(vec_old, vec_mul));
	  }
	  for(; i < ncols; i++) celldata[ix][iy][i] *= sum_inv;
	} 
	// else {cnt_sumsIgnored++;}
      }
    }
  }
  

  // printf("cnt_sumsIgnored=%u/(%u, %u), at %s:%d\n", cnt_sumsIgnored, nxgrid, nygrid, __FILE__, __LINE__);

#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif
}
static void __update_closest_pairs_improved(const uint iobject, const uint nxgrid, const uint nygrid, const uint ncols, const t_float tau, const t_float radius, t_float **data_dividedBy_stddate_iObject, t_float ***celldata, const uint ixbest, const uint iybest) {
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif
  const t_float sqrt_ncols = sqrt(ncols);
  // uint cnt_sumsIgnored = 0;
  for(uint ix = 0; ix < nxgrid; ix++) {
    const uint sumOf_error_x = (ix-ixbest)*(ix-ixbest);
    for(uint iy = 0; iy < nygrid; iy++) {
      //! Compute: (x - max(x))^2 + (y - max(y))^2
      const uint sumOf_error = sumOf_error_x + (iy-iybest)*(iy-iybest);
      if(sqrt(sumOf_error) < radius) {
	t_float sum = 0.;
	uint i = 0;
	for(; i < ncols; i++) {
	  celldata[ix][iy][i] += tau * (data_dividedBy_stddate_iObject[iobject][i] - celldata[ix][iy][i]);
	  // t_float term = celldata[ix][iy][i];
	  // term = term * term;
	  // sum += term;
	}
	for(uint i = 0; i < ncols; i++) {
	  // TODO: instead of [”elow] use _mm_storeu_ps
	  t_float term = celldata[ix][iy][i];
	  term = term * term;
	  sum += term;
	}
	//if(sum>0) {
	if(sum == 0) {sum = 1;}
	{
	  // TODO: test the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
	  const t_float sum_inv = sqrt_ncols/sqrt(sum);
	  uint i = 0;
	  for(; i < ncols; i++) celldata[ix][iy][i] *= sum_inv;
	} 
	// else {cnt_sumsIgnored++;}
      }
    }
  }
  

  // printf("cnt_sumsIgnored=%u/(%u, %u), at %s:%d\n", cnt_sumsIgnored, nxgrid, nygrid, __FILE__, __LINE__);

#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif
}
static void __update_closest_pairs(const uint iobject, const uint nxgrid, const uint nygrid, const uint ncols, const t_float tau, const t_float radius, t_float **data, char **mask, t_float ***celldata, const t_float stddata_iobject, const uint ixbest, const uint iybest) {
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif


  // assert(false); // TODO: try to update [”elow] wrt. tiling <-- first argue fr thy this 'would reduce the exeuciton-time-cost'.

  // assert(false); // TODO: provide an arguemtn of 'type "needTo_useMask_evaluation" ... and then use the latter in [”elwo]

  // TODO: make perofmrance-compiarosns for this fucniton ... ie, the tiem-effect of our tiling.

  for(uint ix = 0; ix < nxgrid; ix++) {
    const uint sumOf_error_x = (ix-ixbest)*(ix-ixbest);
    for(uint iy = 0; iy < nygrid; iy++) {
      //! Compute: (x - max(x))^2 + (y - max(y))^2
      const uint sumOf_error = sumOf_error_x + (iy-iybest)*(iy-iybest);

      // assert(false); // TODO: is it possible to use 'memory-tiling' in [below]? <--- in [”elow] we compute "listB[i] = (tau*(listA[i] - listB[i]))/sum(listB) for all ix, iy, ie, similar to cityblock." <-- modify our cityblock-procedure wrt. this.
	
      //! If the total error/radius is smaller than the 'radius-threshold', then we update our computation:
      if(sqrt(sumOf_error) < radius) {
	t_float sum = 0.;
	if(!mask) {
	  for(uint i = 0; i < ncols; i++) {
	    // TODO: re-write this drop [”elow] ... if caller 'knowns' that all values are of interest ... 
	    if(isOf_interest(data[iobject][i])) {
	      // TODO: instead of [”elow] use _mm_storeu_ps
	      // TODO: write an 'adjusted version' where we constuct a temproary matrix of "data[i][iobject]/stddata_iobject" ... ie, at 'the inut/start of the 'main' funciotn').
	      celldata[ix][iy][i] += tau * (data[iobject][i]/stddata_iobject - celldata[ix][iy][i]);
	    }
	  }
	} else {
	  for(uint i = 0; i < ncols; i++) {
	    if(mask[iobject][i] == 1) {
	      celldata[ix][iy][i] += tau * (data[iobject][i]/stddata_iobject - celldata[ix][iy][i]);
	    }
	  }
	}
	for(uint i = 0; i < ncols; i++) {
	  t_float term = celldata[ix][iy][i];
	  term = term * term;
	  sum += term;
	}
	//if(sum == 0) {sum = 1;}
	if(sum>0) 	 
	{
	  sum = sqrt(sum/ncols);
	  for(uint i = 0; i < ncols; i++) celldata[ix][iy][i] /= sum;
	}
	// else {cnt_sumsIgnored++;}
      }
    }
  }
  //printf("cnt_sumsIgnored=%u/(%u, %u), at %s:%d\n", cnt_sumsIgnored, nxgrid, nygrid, __FILE__, __LINE__);
#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2__update_closest_pairs); //! ie, intiate the object.
#endif

}

/**
   @brief compute a "celldata" 'model' from a a training-set "data".
   @remarks in contrast to "kmeans" clustering-algorithm this 'initaiton-algorithm' is not 'requried' to inspect all of the vectors (eg, rows in "data"). The latter is due to the itnerest/need to compute the "celldata" neuron-map, ie, where the "data" is anyhow expected to be a subset of the real-wolrd input-data-set.  
**/
void somworker(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float *weights, uint transpose, uint nxgrid, uint nygrid, t_float inittau, t_float*** celldata, const uint niter, const s_config_som_t *config) { //const bool isTo_use_continousSTripsOf_memory, const bool isTo_copmuteResults_optimal) {

  // TODO: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description; ... describe hopw the SOM may be used.
  // TODO: write an assert-class text-fucntion for this funciton;
  // TODO: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.

  // TODO: udpat ethe callers of this funciton with the isTo_use_continousSTripsOf_memory parameter ... adnt valdiate taht "delete" is used instead of "free"

  // TODO: try to write rotuiens (or use existing fucnitons) to 'be aware' of the followign assertion: "The data vectors should preferably be normalized (vector length is equal to one) before training the SOM" ["https://en.wikipedia.org/wiki/Self-organizing_map"].




#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_all); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_all); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_init); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_STD); //! ie, intiate the object.
#endif


  const uint nelements = (transpose==0) ? nrows : ncolumns;
  // TODO: wrt. [below] why do we only compute for one of the dimensions?
  const uint ndata = (transpose==0) ? ncolumns : nrows;



  // assert(false); // TODO: replace all occurences of "list_generic" and "list_uint"
  const t_float default_value_float = 0;
  t_float* stddata = allocate_1d_list_float(nelements, default_value_float); //list_generic<float>::allocate_list(/*size=*/nelements, /*empty-value=*/0, __FILE__, __LINE__);
  const bool matrixSize_Adjusted_wrt_SSE = ((nrows % VECTOR_FLOAT_ITER_SIZE) == 0) && ((ncolumns % VECTOR_FLOAT_ITER_SIZE) == 0); 

  const bool config_somWorker_useImproved___updateClosestPair = (!mask && config && config->config_somWorker_useImproved___updateClosestPair && !config->isTo_useImplictMask);
  const uint CLS = 64;
  const uint SM = (uint)(CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  const bool config_somWorker_useImproved___updateClosestPair_useTiling = (config) ? (config_somWorker_useImproved___updateClosestPair && config->config_somWorker_useImproved___updateClosestPair_useMemoryTiling && matrixSize_Adjusted_wrt_SSE  
									   && ( (ncolumns % SM) == 0)
										      )
    : false
    ;
  const bool config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = (config) ? matrixSize_Adjusted_wrt_SSE && config->config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics : false;

  const bool config_somWorker_useImproved___updateClosestPair_useTiling_SSE = (config) ? (config_somWorker_useImproved___updateClosestPair_useTiling && config->config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics) : false;
  // assert(false); // TODO: provide an arguemtn of 'type "needTo_useMask_evaluation" ... and then use the latter in [”elwo]

  


  /* Maximum radius in which nodes are adjusted */
  // TODO: explain why [below] 'hold'.
  const t_float maxradius = sqrt(nxgrid*nxgrid+nygrid*nygrid);

  /* Set the metric function as indicated by dist */

  // assert(false); // TODO: inlcude a erptmtuation of [”elow]

  const e_cmp_masksAre_used_t masksAre_used = (!config || (!config->isTo_useImplictMask && !mask) ) ? e_cmp_masksAre_used_false : e_cmp_masksAre_used_true;
  const bool needTo_useMask_evaluation = (masksAre_used == e_cmp_masksAre_used_true);
  t_float (*metric_each) (config_nonRank_each_t) = setmetric__correlationComparison__each(metric_id, weights, mask, mask, needTo_useMask_evaluation);
  void (*metric_oneToMany) (config_nonRank_oneToMany_t) = setmetric__correlationComparison__oneToMany(metric_id, weights, mask, mask, needTo_useMask_evaluation);
  // t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);

  struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
  setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);

  const bool isTo_copmuteResults_optimal = (config) ? config->isTo_copmuteResults_optimal : true;
  // const bool needTo_useMask_evaluation = (masksAre_used == e_cmp_masksAre_used_true);
  const bool isTo_useOptimizedMetricFor_kendall = isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id);
  if(isTo_useOptimizedMetricFor_kendall) { //(dist == 'k') ) {
    assert(transpose == 0); //! ie, to simplify our code.

    //! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure:
    //! Note In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
    init__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute, nrows, ncolumns, data, mask, needTo_useMask_evaluation);
  }


  //! Compute/calculate the standard deviation (STD) for each row or column:
  if(transpose==0) {



    if(!mask && config && config->config_somWorker_useImproved_STD && !config->isTo_useImplictMask) {
      if(config->config_somWorker_useImproved_STD_useSSEIntrisinistics && (ncolumns > 16) ) {
	const t_float ncols_sqrt_inv = 1/sqrt(ncolumns);
	const uint ncolumns_intri = (uint)VECTOR_FLOAT_maxLenght_forLoop(ncolumns);
	for(uint i = 0; i < nrows; i++) {
	  uint j = 0;

	  VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOADU(&data[i][j]);
	  VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_MUL(vec_input, vec_input);
	  VECTOR_FLOAT_TYPE vec_sum = vec_tmp;
	  j += VECTOR_FLOAT_ITER_SIZE;
	  for (j=0; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) {
	    VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOADU(&data[i][j]);
	    VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_MUL(vec_input, vec_input);
	    //! Update teh count:
	    vec_sum = VECTOR_FLOAT_ADD(vec_sum, vec_tmp);
	  }
	  stddata[i] = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);
	  for(; j < ncolumns; j++) {
	    t_float term = data[i][j];
	    term = term * term;
	    stddata[i] += term;
	  }
	  //! Note: we expect at least one row 'to ahve a valeus', ie, as we otherwise woudl expect our pre-proeccisng-rotuniens 'to have ingore dthw row in quesiton':
	  // TODO: add lgoicsl support wrt. [ªbove] assertion.
	  stddata[i] = sqrt(stddata[i])*ncols_sqrt_inv; //! ie, "sum of squares for distances"/"number of distances"
	  //if(stddata[i] > 0) stddata[i] = sqrt(stddata[i]/n); //! ie, "sum of squares for distances"/"number of distances"
	  //else stddata[i] = 1;
	}
      } else {
	const t_float ncols_sqrt_inv = 1/sqrt(ncolumns);
	const uint ncolumns_intri = (uint)VECTOR_FLOAT_maxLenght_forLoop(ncolumns);
	for(uint i = 0; i < nrows; i++) {
	  uint j = 0;

	  for(; j < ncolumns; j++) {
	    t_float term = data[i][j];
	    term = term * term;
	    stddata[i] += term;
	  }
	  //! Note: we expect at least one row 'to ahve a valeus', ie, as we otherwise woudl expect our pre-proeccisng-rotuniens 'to have ingore dthw row in quesiton':
	  // TODO: add lgoicsl support wrt. [ªbove] assertion.
	  assert_possibleOverhead(stddata[i] >= 0);
	  stddata[i] = sqrt(stddata[i])*ncols_sqrt_inv; //! ie, "sum of squares for distances"/"number of distances"
	  //if(stddata[i] > 0) stddata[i] = sqrt(stddata[i]/n); //! ie, "sum of squares for distances"/"number of distances"
	  //else stddata[i] = 1;
	}
      }
    } else {
      // TODO: instead of [”elow] use _mm_storeu_ps
      for(uint i = 0; i < nelements; i++) {
	uint n = 0;
	for(uint j = 0; j < ndata; j++) {
	  // TODO: write a case where we (a) does not test for 'mask' and (b) text for "FLT_MAX"
	  // TODO: for the 'mask == null' and 'FLT_MAX == 0' case write an SSE 'version' of [below]
	  if(!mask || mask[i][j]) {
	    t_float term = data[i][j];
	    term = term * term;
	    stddata[i] += term;
	    n++;
	  }
	}
	// TODO: why is [below] measure infomrative?
	// TODO: text the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
	if(stddata[i] > 0) stddata[i] = sqrt(stddata[i]/n); //! ie, "sum of squares for distances"/"number of distances"
	else stddata[i] = 1;
      }
    }
  } else {
    for(uint i = 0; i < nelements; i++) {
      uint n = 0;
      for(uint j = 0; j < ndata; j++) {
	if(!mask || mask[j][i]) {
	  t_float term = data[j][i];
          term = term * term;
          stddata[i] += term;
          n++;
        }
      }
      // TODO: figure out the time-cost of [”elow] ... and cosndier to pre-computing a square-root-table. <-- consider dorpping this, ie, givne the in-signficant time-cost.
      // TODO: text the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
      if(stddata[i] > 0) stddata[i] = sqrt(stddata[i]/n); //! ie, "sum of squares for distances"/"number of distances"
      else stddata[i] = 1;
    }
  }

#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_STD); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_initMask); //! ie, intiate the object.
#endif

  // TODO: if we use a 'compiel-time-pre-comptued uniform(..)' call, then ... investigate result-dfiference if we use a 'multi-step uniform-look-up-table' (ie, to reduce the overhead wrt. the table-size of the uniform-values.

  //! start: ------------------------------------------------------------------- Memory allocation
  /* Randomize the order in which genes or arrays will be used */
  const uint default_value_uint = 0;
  uint *index = allocate_1d_list_uint(nelements, default_value_uint); //list_generic<uint>::allocate_list(/*size=*/nelements, /*empty-value=*/0, __FILE__, __LINE__);
  for(uint i = 0; i < nelements; i++) index[i] = i;

  if(transpose == 0) {assert(nelements == nrows);} //! ie, what we expect.


  // FIXME: validate correctness of [”elow] ... if of "cntElements_grid" ... and if [”elow] is correct then consider updating our aritlce wrt. a comment of correctness wrt. rthe clsuterC-implemetnaiton.
  //  const uint cntElements_grid

  for(uint i = 0; i < nelements; i++) {
    // TODO: beofre optimziing [below] 'identiyt' the relative time-cost-signficance fo the unifrom(..) proecuedre
    const uint j = (uint) (i + (nelements-i)*math_generateDistribution__uniform()); //! ie, identify/'provide' a number which has a uniform likelyhood of being in range [i, nelements].
    const uint ix = index[j];
    // if(transpose == 0) {assert(ix < ncolumns);} //! ie, given the expected usage-pattern
    assert(ix >= 0);
    assert(ix != INT_MAX); // TODO: validate corretness of this.
    index[j] = index[i];
    index[i] = ix;
  }
  //! Allocate memory for the 'local' distance-mask:
  char **dummymask = NULL; 



  if(!config || !config->isTo_copmuteResults_optimal) { //! Then we explcitly state that each cell is of itnerest:  
  //if(isTo_copmuteResults_optimal == false) { //! Then we explcitly state that each cell is of itnerest:  
    if(transpose==0) {
      dummymask = alloc_generic_type_2d(char, dummymask, nygrid);
      //dummymask = new char*[nygrid];
      if(config && config->isTo_use_continousSTripsOf_memory) {
	dummymask[0] = alloc_generic_type_1d_xmtMemset(char, dummymask[0], ndata*nygrid);
	//dummymask[0] = new char[ndata*nygrid];
	uint offset = 0;
	for(uint i = 0; i < nygrid; i++) {
	  dummymask[i] = dummymask[0] + offset;
	  offset += ndata;
	  for(uint j = 0; j < ndata; j++) {dummymask[i][j] = 1;}
	}
      } else {
	for(uint i = 0; i < nygrid; i++) {
	  dummymask[i] = alloc_generic_type_1d_xmtMemset(char, dummymask[i], ndata);
	  //dummymask[i] = new char[ndata];
	  for(uint j = 0; j < ndata; j++) dummymask[i][j] = 1;
	}
      }
    } else {
      dummymask = alloc_generic_type_2d(char, dummymask, ndata);
      //dummymask = new char*[ndata];
      if(config && config->isTo_use_continousSTripsOf_memory) {
      //if(isTo_use_continousSTripsOf_memory) {
	dummymask[0] = alloc_generic_type_1d_xmtMemset(char, dummymask[0], ndata*nygrid);
	//dummymask[0] = new char[ndata*nygrid];
	uint offset = 0;
	for(uint i = 0; i < ndata; i++) {
	  dummymask[i] = dummymask[0] + offset;
	  offset += nygrid;
	  for(uint j = 0; j < nygrid; j++) {dummymask[i][j] = 1;}
	}
      } else {
	for(uint i = 0; i < ndata; i++) {
	  dummymask[i] = alloc_generic_type_1d_xmtMemset(char, dummymask[i], nygrid);
	  //dummymask[i] = new char[nygrid];
	  dummymask[i][0] = 1;
	}
      }
    }
  }
  /* Randomly initialize the nodes */
#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_initMask); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_initRandom); //! ie, intiate the object.
#endif
  // TODO: may a 2d-for-loop 'go faster' if we use a smaller number-range than "unit" <-- from earlier measurements the latter seesm nto to be the case.
  for(uint ix = 0; ix < nxgrid; ix++) {
    for(uint iy = 0; iy < nygrid; iy++) {
      t_float sum = 0.;
      for(uint i = 0; i < ndata; i++) {
	// TODO: 'identiyt' the relative time-cost-signficance fo the unifrom(..) proecuedre
	// TODO: consider to pre-comptue the "uniform" table ... and provide an 'extra' auxiliary-table to hold the 'sum of prefix-values' .. using this 'sum' to avoid the extra "ndata" iteration.
	const t_float term = -1.0 + 2.0*math_generateDistribution__uniform();
        celldata[ix][iy][i] = term;
	//printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
        sum += term * term;
      }
      // TODO: text the relative time-cost of the sqrt(...) funciton-call ... and then consider to pre-comptue for [0 .... sum], ie, to instead compute sqrt_table[sum]/sqrt(ndata)
      if(sum) {
	sum = sqrt(sum/ndata);
	// TODO: in [”elow] consider replacing "/" with "*"
	for(uint i = 0; i < ndata; i++) {
	  celldata[ix][iy][i] /= sum;
	  //printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
	}
      } else {
	for(uint i = 0; i < ndata; i++) {
	  celldata[ix][iy][i] = 0;
	  //printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
	}
      }
    }
  }
#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_initRandom); //! ie, intiate the object.
#endif
  //! end: ------------------------------------------------------------------- Memory allocation

  t_float ***distMatrix_preCopmtued = NULL; char **mask_dummy = NULL;
  // TODO: if [ªbov€] is allocted ... then de-allcoate it.
  // TODO: if [above] si allocated .. then remember to de-allcoate distMatrix_preCopmtued
  // assert(false); // TODO: consider to pre-compute the celldata-dist-matrix  .... ie, as we expect niter >> nxgrid
  //! Infer an all-against-all distance-matrix for the set of input-data.
  // if(isTo_copmuteResults_optimal) {
  //   math_generateDistribution__makedatamask(/*nrows=*/nrows, /*ncols=*/nelements, &distanceMatrix_preComputed, &mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);	  
  //   graphAlgorithms_distance::compute_allAgainstAll_distanceMetric(dist, /*nrows=*/nrows, /*ncols=*/nelements, /*data1=*/data, /*data2=*/data, weight, distMatrix_preCopmtued);
  // }

  // TODO: read up on the SOM algorithm, and then update [below].

  // TODO: seems like [below] investigaes one vertex for each "iter" .... is the latter observaiotn correct?

#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_init); //! ie, intiate the object.
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step1_all); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2_all); //! ie, intiate the object.
#endif


  t_float **data_dividedBy_stddate_iObject = NULL;
  //if(config && config->config_somWorker_useImproved___updateClosestPair && !config->isTo_useImplictMask) {
  if(config_somWorker_useImproved___updateClosestPair) {
    const t_float default_floatValue = 0;
    data_dividedBy_stddate_iObject = allocate_2d_list_float(nrows, ncolumns, default_floatValue);
  }

  //! Note: for each iteration identify a winning neuron (ie, an "ixbest, iybest" cell/points).
  /* Start the iteration */
  //const t_float default_value_float = 0;
  t_float *arrOf_result_tmp = NULL;
  if(transpose == false) {
    //printf("allcoated a temproary float-table of size=%u, at %s:%d\n", nygrid, __FILE__, __LINE__);
    arrOf_result_tmp = allocate_1d_list_float(nygrid, default_value_float);
  }

  for(uint iter = 0; iter < niter; iter++) { //! and in each iteration 'grab' an input-vector (eg, a row assicated to a vertex):
    uint ixbest = 0; uint iybest = 0; //! ie, store the best 'square':
    const uint iobject_tmp = iter % nelements;
    // TODO: updat the "index" to use "uint" ... and then all other 'cocurrences' ... and search for "-1"
    //! Note: in initation we (for general cases) expect the "index" to have randomized mappings.
    const uint iobject = index[iobject_tmp];
    //! Identify the closest 'feature square' to the 'curren index':
    //! Note: traverse each neuron in the neuron-map, and identify tthe 'neuron which produce the smallest distance':
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2_distanceMetric); //! ie, intiate the object.
#endif
    if(transpose == 0) {
      assert(iobject < nrows); // TODO: validate correctness of this assumptuion

      
      //printf("nrows=%u, nygrid=%u, at %s:%d\n", nrows, nygrid,__FILE__, __LINE__);

      //! Idneitfy the min-distance:
#include "func_kt_clusterAlg_SOM__minDistance.c"

      //printf("at %s:%d\n", __FILE__, __LINE__);

    } else {
      //float** celldatavector = new t_float*[ndata];
      t_float **celldatavector; celldatavector = alloc_generic_type_2d(t_float, celldatavector, ndata);
      for(uint i = 0; i < ndata; i++) {
        celldatavector[i] = &(celldata[ixbest][iybest][i]);
      }
      //t_float closest = 0; assert(false); // TODO: inlcude a erptmtuation of [”elow]
      s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, dummymask, weights, transpose, masksAre_used, nrows, ncolumns);
      t_float closest =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, celldatavector, /*index1=*/iobject, /*index2=*/iybest, config_metric, metric_each,
					   (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
      // t_float closest = metric(ndata,data, celldata[ixbest], mask,dummymask,weights, iobject, iybest,transpose); //! ie, compare the 'current index' (in the data-set) to the 'best index' (i
      //float closest = metric(ndata,data,celldatavector, mask,dummymask,weights,iobject,0,transpose);
      //! Identify the closets 'square':
      for(uint ix = 0; ix < nxgrid; ix++) {
	for(uint iy = 0; iy < nygrid; iy++) {
	  // TODO: try to re-write [below] into a mdoeifed all-agaisnta-ll-matrix-comptuation.
          for(uint i = 0; i < ndata; i++) {
            celldatavector[i] = &(celldata[ixbest][iybest][i]);
	  }

	  //t_float distance = 0; assert(false); // TODO: include a permtatuion of [”elow]:
	  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, dummymask, weights, transpose, masksAre_used, nrows, ncolumns);
	  t_float distance =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, celldata[ixbest], /*index1=*/iobject, /*index2=*/iybest, config_metric, metric_each,
						(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
	  // t_float closest = metric(ndata,data, celldata[ixbest], mask,dummymask,weights, iobject, iybest,transpose); //! ie, compare the 'current index' (in the data-set) to the 'best index' (i
          // const t_float distance = metric (ndata,data,celldatavector, mask,dummymask,weights,iobject,0,transpose);
          if(distance < closest) {
	    ixbest = ix;
            iybest = iy;
            closest = distance;
          }
        }
      }
      free_generic_type_2d(celldatavector);
      //delete [] celldatavector;
    }
#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2_distanceMetric); //! ie, intiate the object.
#endif
    //! 
    //! Update the 'max-trhesholds' and 'sigfncance'/'cosntribution' of update:
    const t_float radius = maxradius * (1. - ((float)iter)/((float)niter)); //! reduce the radius for every iteration.
    const t_float tau = inittau * (1. - ((float)iter)/((float)niter)); //! reduce the Tau for every iteration.
    //! Update the "celldata" with the closes pairs, for which we iterate through each of the grid-points:

    
    if(config && config->isTo_copmuteResults_optimal) { //! Then we explcitly state that each cell is of itnerest:  
    //if(isTo_copmuteResults_optimal) {
      //assert(iob
      assert(transpose == 0); //! ie, what we expect.
      assert(iobject < nrows);
      assert(ndata == ncolumns);
      if(config_somWorker_useImproved___updateClosestPair_useTiling) {

	const bool iso_useSSE = config_somWorker_useImproved___updateClosestPair_useTiling_SSE;
	__update_closest_pairs_improved_memoryTiled(/*base-index=*/iobject, stddata[iobject], /*nrows=*/nxgrid, /*ncols=*/nygrid, ncolumns, tau, radius, data, celldata, ixbest, iybest, CLS, iso_useSSE);
      } else if(config_somWorker_useImproved___updateClosestPair) 
	//if(true)
	{
	  //t_float **local_pointer = data;
	t_float **local_pointer = data_dividedBy_stddate_iObject;
	const t_float div_value = stddata[iobject];
	if(div_value != 0) {
	  if(div_value != 1) {
	    const t_float mul_value = 1/div_value;
	    //for(uint row_id = 0; row_id < nrows; row_id++) 
	    const uint row_id = iobject;
	    {
	      for(uint col_id = 0; col_id < ncolumns; col_id++) {
		const t_float val = data_dividedBy_stddate_iObject[row_id][col_id]*mul_value;
		data_dividedBy_stddate_iObject[row_id][col_id] = val;
	      }
	    }
	  } else {local_pointer = data;} //! ei, as the data is then un-changed.
	} else {
	  memset(data_dividedBy_stddate_iObject[0], 0, nrows*ncolumns); //! ie, then we update the values
	}

	// TODO: figure out why [”elow] acutally seems to increase the exeuciton-time <-- consider to drop [ªbove] seperate step.
	if(config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics) {
	  assert_possibleOverhead(transpose == false);
	  __update_closest_pairs_improved_SSE(/*base-index=*/iobject, /*nrows=*/nxgrid, /*ncols=*/nygrid, ncolumns, tau, radius, local_pointer, celldata, ixbest, iybest);
	} else {
	  __update_closest_pairs_improved(/*base-index=*/iobject, /*nrows=*/nxgrid, /*ncols=*/nygrid, ncolumns, tau, radius, local_pointer, celldata, ixbest, iybest);
	}
	} else {
	__update_closest_pairs(/*base-index=*/iobject, /*nrows=*/nxgrid, /*ncols=*/nygrid, ncolumns, tau, radius, data, mask, celldata, stddata[iobject], ixbest, iybest);
      }
    } else {
      for(uint ix = 0; ix < nxgrid; ix++) {
	for(uint iy = 0; iy < nygrid; iy++) {
	  //! Compute: (x - max(x))^2 + (y - max(y))^2
	  const uint sumOf_error = 
	    (ix-ixbest)*(ix-ixbest)
	    +
	    (iy-iybest)*(iy-iybest);
	
	  //! If the total error/radius is smaller than the 'radius-threshold', then we update our computation:
	  if(sqrt(sumOf_error) < radius) {
	    t_float sum = 0.;
	    if(!mask) {
	      for(uint i = 0; i < ndata; i++) {
		if(isOf_interest(data[i][iobject])) {
		  celldata[ix][iy][i] += tau * (data[i][iobject]/stddata[iobject]-celldata[ix][iy][i]);
		  //printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
		}
	      }
	    } else {
	      for(uint i = 0; i < ndata; i++) {
		if(mask[i][iobject] == 1) {
		  celldata[ix][iy][i] += tau * (data[i][iobject]/stddata[iobject]-celldata[ix][iy][i]);
		  //printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
		}
	      }
	    }
	    for(uint i = 0; i < ndata; i++) {
	      t_float term = celldata[ix][iy][i];
	      //	printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
	      term = term * term;
	      sum += term;
	    }
	    if(sum>0) {
	      sum = sqrt(sum/ndata);
	      for(uint i = 0; i < ndata; i++) {
		celldata[ix][iy][i] /= sum;
		//printf("celldata[%u][%u][%u] = %f, at %s:%d\n", ix, iy, i, celldata[ix][iy][i], __FILE__, __LINE__);
	      }
	    }
	  }
	}
      }
    }
  } //! and at this ecution-point we have completed the number of iterations.

  if(arrOf_result_tmp) {free_1d_list_float(&arrOf_result_tmp); arrOf_result_tmp = NULL;}

  //! De-allcoate the locally reserved memory:
  if(dummymask) {
    if(config && config->isTo_use_continousSTripsOf_memory) {
      free_generic_type_1d(dummymask[0]); //       delete [] dummymask[0];
    } else {
      if(transpose==0) {
	for(uint i = 0; i < nygrid; i++) free_generic_type_1d(dummymask[i]);
      } else {
	for(uint i = 0; i < ndata; i++) free_generic_type_1d(dummymask[i]);
      }
    }
  }
  free_generic_type_2d(dummymask);
  free_generic_type_2d(stddata);
  free_generic_type_1d(index);
  /* delete [] dummymask; */
  /* delete [] stddata; */
  /* delete [] index; */
  if(config && config->config_somWorker_useImproved___updateClosestPair && !config->isTo_useImplictMask) {
    assert(data_dividedBy_stddate_iObject);
    free_2d_list_float(&data_dividedBy_stddate_iObject, nrows);
  }

    if(isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id)) { //(dist == 'k') ) {
    //if(isTo_copmuteResults_optimal && (dist == 'k') ) {
      free__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);
    } //! else we assuemt he "s_correlationType_kendall_partialPreCompute_kendall" data-structure is empty.

#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_step2_all); //! ie, intiate the object.
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somWorker_all); //! ie, intiate the object.
#endif
  return;
}

/* ******************************************************************* */

/**
   @brief assign vectors in a "data" to a "celldata" data-set trained in the "somworker(..)" 'trainer'.
   @remarks ccolect the cluster-ids to ...
**/
void somassign(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep,  const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float *weights, uint transpose, const uint nxgrid, const uint nygrid, t_float*** celldata, uint **clusterid, const s_config_som_t *config) { //const bool isTo_use_continousSTripsOf_memory, const bool isTo_copmuteResults_optimal) {
//void somassign(const uint nrows, const uint ncolumns, t_float** data, char** mask, const t_float weights[], uint transpose, const uint nxgrid, const uint nygrid, t_float*** celldata, char dist, uint **clusterid, const bool isTo_use_continousSTripsOf_memory, const bool isTo_copmuteResults_optimal) {
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somAssign_all); //! ie, intiate the object.
#endif
  
  const t_float default_value_float = 0;
  t_float *arrOf_result_tmp = NULL;
  if(transpose == false) {
    //printf("allcoated a temproary float-table of size=%u, at %s:%d\n", nygrid, __FILE__, __LINE__);
    arrOf_result_tmp = allocate_1d_list_float(nygrid, default_value_float);
  }
  // TODO: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description; ... describe hopw the SOM may be used.
  // TODO: write an assert-class text-fucntion for this funciton;
  // TODO: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.

  // TODO: udpat ethe callers of this funciton with the isTo_use_continousSTripsOf_memory parameter ... adnt valdiate taht "delete" is used instead of "free"

  struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
  setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);

  const bool isTo_copmuteResults_optimal = (config) ? config->isTo_copmuteResults_optimal : true;
  const bool isTo_useOptimizedMetricFor_kendall = isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id);

  /* Set the metric function as indicated by dist */
  //assert(false); // TODO: include a permtaution of [”elow]:
  // t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);
  const e_cmp_masksAre_used_t masksAre_used = (config && !config->isTo_useImplictMask && !mask) ? e_cmp_masksAre_used_false : e_cmp_masksAre_used_true;
  const bool needTo_useMask_evaluation = (masksAre_used == e_cmp_masksAre_used_true);
  t_float (*metric_each) (config_nonRank_each_t) = setmetric__correlationComparison__each(metric_id, weights, mask, mask, needTo_useMask_evaluation);
  void (*metric_oneToMany) (config_nonRank_oneToMany_t) = setmetric__correlationComparison__oneToMany(metric_id, weights, mask, mask, needTo_useMask_evaluation);


  if(isTo_useOptimizedMetricFor_kendall) { //(dist == 'k') ) {
    assert(transpose == 0); //! ie, to simplify our code.

    //! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure:
    //! Note In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
    init__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute, nrows, ncolumns, data, mask, needTo_useMask_evaluation);
  }

  const uint ndata = (transpose==0) ? ncolumns : nrows;

  t_float ***distMatrix_preCopmtued = NULL;
  // TODO: if [above] si allocated .. then remember to de-allcoate distMatrix_preCopmtued
  // assert(false); // TODO: consider to pre-compute the celldata-dist-matrix  .... ie, as we expect niter >> nxgrid <--- the latter 'perofrmance-assumption' seems correct, ie, as we 'by defualt' have an "#(x-grid)*x(y-grid)*#(rows)*#(columns)" time-complexity <-- the latter assumpåtion may be wrong, ie, first try to optmize 'using our oridnary appraoches'.
  
  // TODO: ... after we have made use of our distnace-matrix-update ... write a parall wrapper 'for this'.



  if(transpose == 0) {
    assert(nygrid < 100000); //! else a try-catch-block may be of itnerest.
    char** dummymask = NULL;
    if(!config || !config->isTo_copmuteResults_optimal) { //! Then we explcitly state that each cell is of itnerest:  
      dummymask = alloc_generic_type_2d(char, dummymask, nygrid);
      //dummymask = new char*[nygrid];
      if(config && config->isTo_use_continousSTripsOf_memory) {
	// TODO: replace [”elow] with our macro-call
	const uint default_value_char = 1;
	const uint dummymask_size = nygrid*ncolumns;
	dummymask[0] = allocate_1d_list_char(dummymask_size, default_value_char); //list_generic<char>::allocate_list(/*size=*/nygrid*ncolumns, /*empty-value=*/1, __FILE__, __LINE__);
	uint offset = 0;
	for(uint i = 0; i < nygrid; i++) {
	  // TODO[assert]: validate that we in [below] does not need to mulipåly with "sizeof(..)" ie, write a seprte correctness-test for the case
	  dummymask[i] = dummymask[0] + offset;
	  offset += ncolumns;
	}
      } else {
	for(uint i = 0; i < nygrid; i++) {
	  dummymask[i] = alloc_generic_type_1d_xmtMemset(char, dummymask[i], ncolumns);
	  //dummymask[i] = new char[ncolumns];
	  for(uint j = 0; j < ncolumns; j++) dummymask[i][j] = 1;
	}
      }
    }
#if(configure_isTo_useLog == 1)
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somAssign_iteration); //! ie, intiate the object.
#endif
    for(uint i = 0; i < nrows; i++) {
      uint ixbest = 0;
      uint iybest = 0;
#if(configure_isTo_useLog == 1)
      start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somAssign_distanceMatrix); //! ie, intiate the object.
#endif
      //! Idneitfy the min-distance:
      const uint iobject = i; //(iybest != i) ? i : i;
      
#include "func_kt_clusterAlg_SOM__minDistance.c"



      /* s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /\*isTo_init=*\/true, mask, dummymask, weights, transpose, masksAre_used); */

      /* // t_float closest = 0; assert(false); // TODO: include a permtaution of [”elow]: */


      /* t_float closest =   kt_compare__each(metric_id, typeOf_correlationPreStep, ndata, data, celldata[ixbest], /\*index1=*\/i, /\*index2=*\/iybest, config_metric, metric_each, */
      /* 					   (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute); */





      /* // t_float closest = (distMatrix_preCopmtued) ? distMatrix_preCopmtued[ixbest][i][iybest] : metric(ndata,data,celldata[ixbest], mask,dummymask,weights,i,iybest,transpose); */
      /* // TODO: use an updated all-gaisnta-ll-ompctuationwr wrt. [”elow]  <-- expects a performance-icnrease of "n ... "n*log(n)"       */
      /* for(uint ix = 0; ix < nxgrid; ix++) { */
      /* 	// */
      /* 	kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, celldata[ixbest], /\*index1=*\/i,  &config_metric,  */
      /* 			      (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute, */
      /* 			      /\*arrOf_result=*\/NULL, */
      /* 			      metric_oneToMany); */


      /* 	assert(false); // TODO: drop using [”elow] code: */

      /* 	// assert(false); // TODO: update [below] calling for index1=fixed=i */
      /* 	for(uint iy = 0; iy < nygrid; iy++) { //! compare the 'actual data at i'  to 'the prediction at [ix][iy]': */
      /* 	  t_float distance = 0; assert(false); // TODO: icnldue a pertmatuion of [”elow] */
      /* 	  // const t_float distance = (distMatrix_preCopmtued) ? distMatrix_preCopmtued[ix][i][iy] : metric (ndata, /\*data1=*\/data, /\*data2=*\/celldata[ix], mask, /\*mask2=*\/dummymask,weights, /\*index1=*\/i, /\*index2=*\/iy, transpose); */
      /*     if(distance < closest) { //! then we have identifed a closre/'better' 'prediction', ie, update: */
      /* 	    ixbest = ix; */
      /*       iybest = iy; */
      /*       closest = distance; */
      /*     } */
      /*   } */
      /* } */
#if(configure_isTo_useLog == 1)
      end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somAssign_distanceMatrix); //! ie, intiate the object.
#endif
      //! Update the 'model' which best describe the 'vector':
      clusterid[i][0] = ixbest;
      clusterid[i][1] = iybest;
    }
    //! De-allcoate the locally reserved memory:
    if(dummymask) {
      if(config && config->isTo_use_continousSTripsOf_memory) {
	free_generic_type_1d(dummymask[0]); //delete [] dummymask[0];
      } else {
	for(uint i = 0; i < nygrid; i++) free_generic_type_1d(dummymask[i]); //delete [] dummymask[i];
      }
      free_generic_type_2d(dummymask); //      delete [] dummymask;
    }
  } else {
    assert(nrows < 100000); //! else a try-catch-block may be of itnerest.
    assert(ndata < 100000); //! else a try-catch-block may be of itnerest.
    t_float **celldatavector; celldatavector = alloc_generic_type_2d(t_float, celldatavector, ndata);
    //float** celldatavector = new t_float*[ndata];
    char **dummymask = NULL;
    if(!config || !config->isTo_copmuteResults_optimal) { //! Then we explcitly state that each cell is of itnerest:  
      dummymask = alloc_generic_type_2d(char, dummymask, nrows);
      //dummymask = new char*[nrows];
      if(config && config->isTo_use_continousSTripsOf_memory) {
	const uint default_value_char = 1;
	const uint dummymask_size = nrows;
	dummymask[0] = allocate_1d_list_char(dummymask_size, default_value_char); //list_generic<char>::allocate_list(/*size=*/nygrid*ncolumns, /*empty-value=*/1, __FILE__, __LINE__);
	//dummymask[0] = list_generic<char>::allocate_list(/*size=*/nrows, /*empty-value=*/1, __FILE__, __LINE__);
	for(uint i = 1; i < nrows; i++) {
	  dummymask[i] = dummymask[0] + i;
	}
      } else {
	for(uint i = 0; i < nrows; i++) {
	  const uint local_size = 1;
	  const char default_value_char = 0;
	  dummymask[i] = allocate_1d_list_char(local_size, default_value_char); //list_generic<char>::allocate_list(/*size=*/nygrid*ncolumns, /*empty-value=*/1, __FILE__, __LINE__);
	  //dummymask[i] = new char [1];
	  dummymask[i][0] = 1;
	}
      }
    }
    uint ixbest = 0;
    uint iybest = 0;
    for(uint i = 0; i < ncolumns; i++) {
      for(uint j = 0; j < ndata; j++) {
        celldatavector[j] = &(celldata[ixbest][iybest][j]);
      }
      // TODO: use an updated all-gaisnta-ll-ompctuationwr wrt. [”elow]  <-- expects a performance-icnrease of "n ... "n*log(n)"
      //t_float closest = 0; assert(false); // TODO: include a permtaution of [”elow]:
      s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, dummymask, weights, transpose, masksAre_used, nrows, ncolumns);
      t_float closest =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, celldatavector, /*index1=*/i, /*index2=*/0, config_metric, metric_each,
      					   (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);

      // t_float closest = metric(ndata,data,celldatavector, mask,dummymask,weights,i,0,transpose);
      for(uint ix = 0; ix < nxgrid; ix++) {
	for(uint iy = 0; iy < nygrid; iy++) {
          for(uint j = 0; j < ndata; j++) {
            celldatavector[j] = &(celldata[ix][iy][j]);
	  }
	  t_float distance =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, celldatavector, /*index1=*/i, /*index2=*/0, config_metric, metric_each,
						(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);

	  //t_float distance = 0; assert(false); // TODO: include a permtaution of [”elow]:
          // const t_float distance = metric(ndata,data,celldatavector, mask,dummymask,weights,i,0,transpose);
          if(distance < closest) {
	    ixbest = ix;
            iybest = iy;
            closest = distance;
          }
        }
      }
      //! Update the 'model' which best describe the 'vector':
      clusterid[i][0] = ixbest;
      clusterid[i][1] = iybest;
    }

    //! De-allcoate the locally reserved memory:
    if(dummymask) {
      if(config && config->isTo_use_continousSTripsOf_memory) {
	free_generic_type_1d(dummymask[0]);
	//delete [] dummymask[0];
      } else {
	for(uint i = 0; i < nrows; i++) free_generic_type_1d(dummymask[i]); //delete [] dummymask[i];
      } 
    }
    free_generic_type_2d(celldatavector); //    delete [] celldatavector;
    free_generic_type_2d(dummymask); //delete [] dummymask;
  }
#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somAssign_iteration); //! ie, intiate the object.
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_somAssign_all); //! ie, intiate the object.
#endif


    if(isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id)) { //(dist == 'k') ) {
    //if(isTo_copmuteResults_optimal && (dist == 'k') ) {
      free__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);
    } //! else we assuemt he "s_correlationType_kendall_partialPreCompute_kendall" data-structure is empty.

    if(arrOf_result_tmp != NULL) {
      free_1d_list_float(&arrOf_result_tmp); arrOf_result_tmp = NULL; //! (oekseth, 06. nov. 2016).
    }

  return;
}

/* ******************************************************************* */

/**
   @brief combined a 'trainer' and a 'model-applicaiton' to orngaise/cluster a dataset.
   @remarks The somcluster routine implements a self-organizing map (Kohonen) on a rectangular grid, using a given set of vectors. The distance measure to be used to find the similarity between genes and nodes is given by dist.
   - "SOM forms a semantic map where similar samples are mapped close together and dissimilar ones apart" ["https://en.wikipedia.org/wiki/Self-organizing_map"]
   @remarks in below we describe a few number of the parameters:
   # "nxgrid": The number of grid cells horizontally in the rectangular topology of clusters.
   # "nygrid": The number of grid cells horizontally in the rectangular topology of clusters.
   # "inittau": The initial value of tau, representing the neighborhood function.
   # "niter": The number of iterations to be performed.
   # "celldata": t_float[nxgrid][nygrid][ncolumns] iftranspose==0; t_float[nxgrid][nygrid][nrows]    iftranpose==1; The gene expression data for each node (cell) in the 2D grid. This can be interpreted as the centroid for the cluster corresponding to that cell. If celldata is NULL, then the centroids are not returned. Ifcelldata is not NULL, enough space should be allocated to store the centroid data before callingsomcluster.
   # "clusterid": iftranspose==0; int[ncolumns][2] iftranspose==1; For each item (gene or microarray) that is clustered, the coordinates of the cell in the 2D grid to which the item was assigned. Ifclusterid is NULL, the cluster assignments are not returned. Ifclusterid is not NULL, enough memory should be allocated to store the clustering information before calling somcluster.
**/
void somcluster__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float *weight, uint transpose, const uint nxgrid, const uint nygrid, t_float inittau, const uint niter,  t_float*** celldata, uint **clusterid, s_config_som_t *config) { //, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const bool isTo_copmuteResults_optimal, const bool isTo_useImplictMask) {

  if(nygrid == 0) {
    assert(false);
    fprintf(stderr, "!!\t(API-erorr)\t |Y-grid|=%u was specifriedi, which impleis a  potinless SOM-speifion, ie, where latter vlaues should be greater than zero. For quesitons please cotnact senior delveoper [oekseth@gmail.com]. Observat at [%s]:%s:%d\n", nygrid, __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if(nxgrid == 0) {
    assert(false);
    fprintf(stderr, "!!\t(API-erorr)\t |X-grid|=%u was specifriedi, which impleis a  potinless SOM-speifion, ie, where latter vlaues should be greater than zero. For quesitons please cotnact senior delveoper [oekseth@gmail.com]. Observat at [%s]:%s:%d\n", nxgrid, __FUNCTION__, __FILE__, __LINE__);
    return;
  }

  // TODO: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description; ... describe hopw the SOM may be used.
  // TODO: write an assert-class text-fucntion for this funciton;
  // TODO: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.

  // TODO: write example-code where we use the same trianign-set for muliple input-datasets ... and tehn use the exampel to explroe some of the liatterautre-proeprteis in the ltiterature-researh-networks.

  {
    const uint fastMinThres = VECTOR_FLOAT_ITER_SIZE * 10;
      bool setTo_slow = false;
      if( !transpose && (ncolumns < fastMinThres) ) {
	setTo_slow = true;
      } else if( transpose && (nrows < fastMinThres) ) {
	setTo_slow = true;	
      }
      if(setTo_slow && config) {
	assert(config);
	s_config_som_init_slow(config);
      }
  }

  if( (nygrid > nrows) || (nygrid > ncolumns) ) {
    fprintf(stderr, "!!\t You have requested an option Not currenlty supported, ie, to compute a SOM-network w/dimensions=|%u|x|%u| given an inptu-matris of |%u|x|%u|, ie, please udpate your call: to simplify the considerations we now abort the exeuction. For questions please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nxgrid, nygrid, nrows, ncolumns, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return;
  }


#if(configure_isTo_useLog == 1)
  s_log_clusterC_init(&logFor_SOM); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_som_all); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_som_init); //! ie, intiate the object.
#endif

  // TODO: udpat ethe callers of this funciton with the isTo_use_continousSTripsOf_memory parameter ... adnt valdiate taht "delete" is used instead of "free"

  const uint nobjects = (transpose==0) ? nrows : ncolumns;
  if(nobjects < 2) {
    assert(false); //! ie, as the call is then pointless.
    return;
  }

  bool needTo_useMask_evaluation = true;
  if(config && config->isTo_useImplictMask && mask) {
    assert(data);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncolumns, data, NULL, mask, NULL, transpose, /*iterationIndex_2=*/UINT_MAX);
    if(needTo_useMask_evaluation) {
      s_mask_api_config_t config_mask; setTo_empty__s_mask_api_config(&config_mask);
      applyImplicitMask__updateData__mask_api(data, mask, nrows, ncolumns, config_mask);
    }
    //needTo_useMask_evaluation = graphAlgorithms_distance::update_distanceMatrix_withImplicitMask_fromMaskMatrix(nrows, ncolumns, data, mask);
  }
  // assert(false); // TODO: make use of the needTo_useMask_evaluation parameter.

  const uint ndata = (transpose==0) ? ncolumns : nrows;
  const uint lcelldata = (celldata==NULL) ? 0 : 1;


  t_float **distanceMatrix_intermediate = NULL; char **mask_dummy = NULL;  //! Then we a transposed matrix out of the input-tranpose
  if((transpose == 1) && (config && config->isTo_invertMatrix_transposed) ) {
    //! Allocate memory, ie, using the 'inverse settings' as [above]:
    maskAllocate__makedatamask(/*nrows=*/ncolumns, /*ncols=*/nrows, &distanceMatrix_intermediate, &mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);	  
    if(mask) {mask_dummy = allocate_2d_list_char(ncolumns, nrows, 0);}
    //! TODO: consider to remove [below] asserts ... adn then add the parameters to [below] funciotn:
    //! Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, mask, distanceMatrix_intermediate, mask_dummy, /*use_SSE=*/true);
    //! Update, ie, 'invert' the variables:
    transpose = 0; const uint tmp = nrows; 
    nrows = ncolumns; ncolumns = tmp;    
    t_float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    char **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;
  }


  if(celldata == NULL) {
    const uint cnt_cell_rows = nxgrid*nygrid*ndata;
    assert(cnt_cell_rows < 100000); //! else a try-catch-block may be of itnerest.
    celldata = alloc_generic_type_3d(t_float, celldata, cnt_cell_rows); //new t_float **[cnt_cell_rows];
    // TODO[benchmark]: write a new code to test the effect/time-impact of [below] algorithms wrt. emmory-access-patterns.
    if(config && config->isTo_use_continousSTripsOf_memory) {
      //! Allocate:
      // TODO: use our macro-call instead of [”elow] iteraiton
      celldata[0] = alloc_generic_type_2d(t_float, celldata[0], cnt_cell_rows); //new t_float*[cnt_cell_rows];
      const t_float default_value_float = 0;
      celldata[0][0] = allocate_1d_list_float(ndata, default_value_float); //list_generic<float>::allocate_list(/*size=*/nelements, /*empty-value=*/0, __FILE__, __LINE__);
      //celldata[0][0] = list_generic<float>::allocate_list(/*size=*/cnt_cell_rows*ndata, /*empty-value=*/0, __FILE__, __LINE__);
      //! Update positions:
      uint offset_in = 0; uint offset_out = 0;
      for(uint i = 0; i < nxgrid; i++) {
	celldata[i] = celldata[0] + offset_in;
	for(uint j = 0; j < nygrid; j++) {
	  celldata[i][j] = celldata[0][0] + offset_out;
	  offset_out += ndata;
	}
	offset_in += nygrid*ndata;
      }
    } else {
      for(uint i = 0; i < nxgrid; i++) {
	const uint cnt_ele = nygrid*ndata;
	assert(celldata); assert(cnt_ele > 0);
	celldata[i] = alloc_generic_type_2d(t_float, celldata[i], cnt_ele);
	for(uint j = 0; j < nygrid; j++) {
	  const t_float default_value_float = 0;
	  celldata[i][j] = allocate_1d_list_float(ndata, default_value_float); //t_float, celldata[i][j], ndata);
	}
      }
    }
  }

#if(configure_isTo_useLog == 1)
  //end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_som_all); //! ie, intiate the object.
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_som_init); //! ie, intiate the object.
#endif


  //! --------------------------------------------------------------
  //! The operation:
  somworker(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, transpose, nxgrid, nygrid, inittau, celldata, niter, config); //isTo_use_continousSTripsOf_memory, isTo_copmuteResults_optimal);
  if(clusterid) { //! then we use the [above] comptued training-set:
    // TODO: when calling [below] ... then cosnider to 'remember' the 'pre-comptued ranks' (ie, if a 'sort-rank-correlation-metric' is ued in our "somassign(..)" procedure).
    somassign(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, mask, weight, transpose, nxgrid, nygrid, celldata, clusterid, config); //isTo_use_continousSTripsOf_memory, isTo_copmuteResults_optimal);
  }
  //! --------------------------------------------------------------
  //! De-allcoate locally reserved memory:
  if(lcelldata==0) {
    if(config && config->isTo_use_continousSTripsOf_memory) {
      free_generic_type_1d(celldata[0][0]);
      free_generic_type_2d(celldata[0]);
      free_generic_type_2d(celldata);
      /* delete [] celldata[0][0]; */
      /* delete [] celldata[0]; */
      /* delete [] celldata; */
    } else {
      for(uint i = 0; i < nxgrid; i++) {
	for(uint j = 0; j < nygrid; j++) {
	  free_generic_type_1d(celldata[i][j]); //	  delete [] celldata[i][j];
	}
      }

      for(uint i = 0; i < nxgrid; i++) {
	free_generic_type_1d(celldata[i]); //	delete [] celldata[i];
      }
      free_generic_type_2d(celldata); //      delete [] celldata;
    }
  }


  //! Ivnestigate if we are to 'invert' the results:
  if((transpose == 1) && config && config->isTo_invertMatrix_transposed) {
    assert(distanceMatrix_intermediate);
    //! 'Back-translate' the distance-matrix, ie, compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    assert(data != distanceMatrix_intermediate); //! Note: at this exeuction-point we assume that "data" is the transpsoied matrix, ie, given our precivous/earlier call to [below] function
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, NULL, distanceMatrix_intermediate, NULL, /*use_SSE=*/true);    

    t_float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    char **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;    
    maskAllocate__freedatamask(/*nelements=*/ncolumns, distanceMatrix_intermediate, mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);    
  }

#if(configure_isTo_useLog == 1)
  end_time_measurement_clusterC(&logFor_SOM, e_log_clusterC_som_all); //! ie, intiate the object.
  writeOut_object_clusterC(&logFor_SOM, stdout, e_logCluster_typeOf_writeOut_SOM, /*stringOf_description=*/NULL); //! ie, intiate the object.
#endif
  return;
}

