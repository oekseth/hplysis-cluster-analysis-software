{ //! Investigate the perfomrance-peanlity-overhead wrt. auxiliary routines:
  { const char *stringOf_measureText = "proximity(if-clause-branching::ideal-case): performance-penlatiy for evalauting a large number of small feature-vectors; the 'ideal case' (which we evaluate in this experiment) is expected to reflect the pre-processing-optmized prxomity-fucntison (whcih we do Not evalaute wrt. this measurement-category)";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  uint sumOf_case_1a = 2; uint sumOf_case_1b = 4; uint sumOf_case_2a = 6; uint sumOf_case_2b = 8;
	  sumOf_values += correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__RusselRao(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
	}
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
  { const char *stringOf_measureText = "proximity(if-clause-branching): performance-penlatiy for evalauting a large number of small feature-vectors; difference wrt. the 'ideal case' conserns the time-cost of if-caluses, a time-cost which we investigate by selecting the 'worst if-case-branch' (ie, the last enum-alternative)";
    //! -------------------------------
    //! Start the clock:
    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
    //!
    //!
    //! The experiemnt:
    t_float sumOf_values = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      //const uint row_id_out = 1; {
      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	for(uint col_id = 0; col_id < size_of_array; col_id++) {
	  uint sumOf_case_1a = 2; uint sumOf_case_1b = 4; uint sumOf_case_2a = 6; uint sumOf_case_2b = 8;
	  sumOf_values += kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, e_correlationType_proximity_altAlgortihmComputation_binary_kulczynski);
	}
      }
    }
    //! --------------
    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
  }
}
//! --------------------------------------------------------------------------


{ //! The time-cost of: optimized and navie implementations:
  //! -----------------------------------
  static const uint stringOf_proxTypes_size = 9;
  assert(stringOf_proxTypes_size == e_correlationType_proximity_altAlgortihmComputation_binary_undef);
  static const char *stringOf_proxTypes[stringOf_proxTypes_size] = {
    //! Inlcue the enum-names found in our "e_correlationType.h":
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_russelRao),
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_simpleMatching),
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_jaccard),
    //! ---
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_dice_Czekanowski_Sorenson),  
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_rogensTanimoto),
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath),
    //! ---
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath_measure2),  
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath_measure3),
    macro_getVarName(e_correlationType_proximity_altAlgortihmComputation_binary_kulczynski)
  };

  //! -----------------------------------
  { const char *stringOf_measureText_base = "proximity(all-against-all-comparison): ";
    //! -------------------------------
    for(uint enum_id = 0; enum_id < stringOf_proxTypes_size; enum_id++) {
      for(char config_useScoreThres_min = 0; config_useScoreThres_min < 2; config_useScoreThres_min++) {
	for(char config_useScoreThres_max = 0; config_useScoreThres_max < 2; config_useScoreThres_max++) {
	  for(char config_weight = 0; config_weight < 2; config_weight++) {
	    for(char config_mask = 0; config_mask < 3; config_mask++) {
	      for(char config_useTwoMatricesAsInput = 0; config_useTwoMatricesAsInput < 2; config_useTwoMatricesAsInput++) {
		for(char config_transpose = 0; config_transpose < 2; config_transpose++) {
		  for(char config_useFast = 0; config_useFast < 2; config_useFast++) {
		    if(config_useFast && config_transpose) { continue;} //! ie, as we then expect that a user will have tranpsoed a given nput-matrix berofre 'calling thsi fucniton', ie, an overlappig test which we do not ivnestiggtae, ie, as there are already too many tests.
		    const bool transpose = config_transpose;
		    //! -------------------------------------------
		    //! Generate the measurement-test:
		    char stringOf_measureText[2000]; memset(stringOf_measureText, '\0', 2000);
		    sprintf(stringOf_measureText, "%s for proximity-metric: %s; transpose='%s', use-fast='%s', use-threshold-min='%s', use-threshold-max='%s', use-weight='%s', mask-type='%s', use-two-differentMatrices-as-inpnut='%s'", 
			    stringOf_measureText_base, stringOf_proxTypes[enum_id],
			    (config_transpose) ? "true" : "false",
			    (config_useFast) ? "true" : "false",
			    (config_useScoreThres_min) ? "true" : "false",
			    (config_useScoreThres_max) ? "true" : "false",
			    (config_weight) ? "true" : "false",
			    (config_mask == 2) ? "maskExplicit" : (config_mask == 1) ? "maskImplicit" : "mask-none",
			    (config_useTwoMatricesAsInput) ? "true" : "false"
			    );

		    t_float **local_matrix_1 = matrix;   t_float **local_matrix_2 = matrix;
		    if(transpose == true) {
		      local_matrix_1 = matrix_transposed; local_matrix_2 = matrix_transposed;
		    }
		    t_float *local_weight = NULL;
		    if(config_weight) {local_weight = weight;}

		    if(config_useTwoMatricesAsInput) {
		      if(transpose == false) {
			local_matrix_2 = allocate_2d_list_float(nrows, size_of_array, default_value_float);
		      } else {
			local_matrix_2 = allocate_2d_list_float(size_of_array, nrows, default_value_float);
		      }
		    }
		    e_cmp_masksAre_used_t masksAre_used = e_cmp_masksAre_used_true;
		    char **local_mask1 = NULL; 	      char **local_mask2 = NULL;
		    bool masks_isAllocated = false;
		    if(config_mask == 0) {
		      masksAre_used = e_cmp_masksAre_used_false;
		    } else if(config_mask == 2) {
		      if(transpose == false) {
			local_mask1 = mask1; local_mask2 = mask2;
		      } else {
			masks_isAllocated = true;
			local_mask1 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
			local_mask2 = allocate_2d_list_char(size_of_array, nrows, default_value_char);
		      }
		    }
		    t_float scoreTrheashold_min = T_FLOAT_MAX; 	t_float scoreTrheashold_max = T_FLOAT_MAX;
		    if(config_useScoreThres_min) {scoreTrheashold_min = -10;}  //! ie, set to an 'arbitrary' value.
		    if(config_useScoreThres_max) {scoreTrheashold_min = 10;}   //! ie, set to an 'arbitrary' value.

		    //! 
		    //! Start the clock:
		    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
		    //!
		    //!
		    //! The experiemnt:
		    t_float sumOf_values = 0;
		    // printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", size_of_array, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
		    for(uint row_id = 0; row_id < nrows; row_id++) {
		      for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
			//const uint row_id_out = 1; {	    
			//const uint CLS = 64; const bool isTo_use_continousSTripsOf_memory = true; const uint iterationIndex_2 = UINT_MAX; const e_allAgainstAll_SIMD_inlinePostProcess_t typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef;
			s_kt_correlationConfig_t self; 
			if(config_useFast == true) {
			  s_kt_correlationConfig_init_useFast(&self, masksAre_used);
			} else {
			  s_kt_correlationConfig_init_useSlow(&self);
			}

			//! --------------
			if(local_mask1) {
			  assert(local_mask2);
			  assert(local_mask1[row_id]); 	      assert(local_mask2[row_id_out]); //! ie, as we expect rows are set for both of the latter
			}
			//! --------------
			sumOf_values += kt_proximity_altAlgortihmComputation(/*ncols=*/size_of_array, local_matrix_1, local_matrix_2, /*mask1=*/local_mask1, /*mask2=*/local_mask2, local_weight, row_id, row_id_out, /*transpose=*/transpose,  (e_correlationType__t)enum_id, scoreTrheashold_min, scoreTrheashold_max, self);
		      }
		    }
		    //! --------------
		    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, size_of_array, mapOf_timeCmp_forEachBucket[chunk_index]); 	  
		    if(config_useTwoMatricesAsInput) {
		      free_2d_list_float(&local_matrix_2, size_of_array);
		    }
		    if(masks_isAllocated) {
		      free_2d_list_char(&local_mask1, size_of_array);
		      free_2d_list_char(&local_mask2, size_of_array);
		    }
		  }
		}
	      }
	    }
	  }
	  //free_memory__s_allAgainstAll_config(&self);
        }
      }
    }
  }
  //! ----
}
//! --------------------------------------------------------------------------
