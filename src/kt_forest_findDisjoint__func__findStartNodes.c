

if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_sparseRelations) {
  //printf("-------------------------, at %s:%d\n", __FILE__, __LINE__);
  for(uint this_key = 0; this_key < nrows; this_key++) {
    uint list_size = 0;  const uint *list = get_sparseRow_ofIds__s_kt_set_2dsparse_t(self->listOf_relations, this_key, &list_size);
    if(list_size == 0) {
      continue; // ie, as this_key does not have any explictly asosicated/related vertices.
    }
    for(uint child_index = 0; child_index < list_size; child_index++) {
      const uint child_key = list[child_index];
      //assert(child_key < ); //! ie, as we expec thte "s_kt_set_2dsparse" objec tot have been itnalised to the max possible number of entites.
      assert(child_key != UINT_MAX);
      const uint update_index = (isTo_reverseRelationiships) ? child_key : this_key;
      //! Update the child-counter:
      //printf("mapOf_incomingRelationsCnt_size=%u, update_index=%u, at %s:%d\n", mapOf_incomingRelationsCnt_size, update_index, __FILE__, __LINE__);
      assert(update_index < mapOf_incomingRelationsCnt_size);
      if(mapOf_incomingRelationsCnt[update_index] == UINT_MAX) {
	mapOf_incomingRelationsCnt[update_index] = 1;
      } else {
	mapOf_incomingRelationsCnt[update_index]++;
      }
    }
  }
 } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_float) {
  //printf("-------------------------, at %s:%d\n", __FILE__, __LINE__);
  uint cnt_ofInterest = 0;
  for(uint this_key = 0; this_key < self->_nrows; this_key++) {
    for(uint child_key = 0; child_key < self->_ncols; child_key++) {
      if(self->matrix[this_key][child_key] != self->emptyValueIf_maskIsNotUsed) {
	const uint update_index = (isTo_reverseRelationiships) ? child_key : this_key;
	//! Update the child-counter:
	//printf("\t (has-relation)\t %u, at %s:%d\n", update_index, __FILE__, __LINE__);
	if(mapOf_incomingRelationsCnt[update_index] == UINT_MAX) {
	  mapOf_incomingRelationsCnt[update_index] = 1;
	} else {
	  mapOf_incomingRelationsCnt[update_index]++;
	}
	cnt_ofInterest++;
      }
    }
  }
  if(cnt_ofInterest == 0) {
    fprintf(stderr, "!!\t Seems like None of the matrix-cells were of itnerest, ie, where a call to this disjoitn-forest-procedure is poiintless: we asusmed theat the 'empty-value'='%f' (described throught ehe internal parameter of \"self->emptyValueIf_maskIsNotUsed\"). If the latter doudns odd, then please contact the senior devleoper at [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", self->emptyValueIf_maskIsNotUsed, __FUNCTION__, __FILE__, __LINE__);
    //assert(false); //! ie, an heads-up
  }
 } else if(self->_inputType == e_kt_forest_findDisjoint_inputRelationType_denseMatrix_char) {
  printf("-------------------------, at %s:%d\n", __FILE__, __LINE__);
  for(uint this_key = 0; this_key < self->_nrows; this_key++) {
    for(uint child_key = 0; child_key < self->_ncols; child_key++) {
      if(self->mask[this_key][child_key]) {
	const uint update_index = (isTo_reverseRelationiships) ? child_key : this_key;
	//! Update the child-counter:
	if(mapOf_incomingRelationsCnt[update_index] == UINT_MAX) {
	  mapOf_incomingRelationsCnt[update_index] = 1;
	} else {
	  mapOf_incomingRelationsCnt[update_index]++;
	}
      }
    }
  }
 }
//! 
//! ----------------------------------------------------
if(self->typeOf_startVertices != e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_rootNotes) { //! then we idneitfy the sroted set of ranks.
  //! Update the ranks:
  //printf("-------------------------, at %s:%d\n", __FILE__, __LINE__);
  uint *tmp_sorted = allocate_1d_list_uint(mapOf_incomingRelationsCnt_size, default_value_uint);
  uint *tmp_index = allocate_1d_list_uint(mapOf_incomingRelationsCnt_size, default_value_uint);
  uint *tmp_result = allocate_1d_list_uint(mapOf_incomingRelationsCnt_size, default_value_uint);
  quicksort_uint(mapOf_incomingRelationsCnt_size, mapOf_incomingRelationsCnt, tmp_index, tmp_result);
  uint current_pos = 0;
  //! Then we are interested in updated/computing the ranks:
  if(
     (self->typeOf_startVertices != e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_incoming_sortedByRank_decrease) ||
     (self->typeOf_startVertices != e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_countOfArcs_outgoing_sortedByRank_decrease)
     ) { //! Then we invert the sort-order, ie, for cases where the valeus i Not set to UINT-MAX
    uint max_cnt = 0; for(uint i = 0; i < mapOf_incomingRelationsCnt_size; i++) { if(tmp_result[i] != UINT_MAX) {max_cnt++;} }

    //assert(nrows < mapOf_incomingRelationsCnt_size);
    for(uint i = 0; i < mapOf_incomingRelationsCnt_size; i++) {
      if(mapOf_incomingRelationsCnt[i] != UINT_MAX) {
	mapOf_incomingRelationsCnt[i] = tmp_index[max_cnt - 1 - current_pos]; //! ie, the 'reverse-sorted index'.
	//printf("nrows=%u, max_cnt=%u, mapOf_incomingRelationsCnt[%u]=%u, at %s:%d\n", nrows, max_cnt, i, mapOf_incomingRelationsCnt[i], __FILE__, __LINE__);
	//assert(mapOf_incomingRelationsCnt[i] <= nrows);
	assert(mapOf_incomingRelationsCnt[i] <= max_cnt);
	current_pos++;
      }
    }
    assert(current_pos == max_cnt); //! ie, what we expect.
  } else { //! Then we use 'directly the rank-order speicifed in the [ªbove] sort-call
    //assert(nrows < mapOf_incomingRelationsCnt_size);
    for(uint i = 0; i < mapOf_incomingRelationsCnt_size; i++) {
      if(mapOf_incomingRelationsCnt[i] != UINT_MAX) {
	mapOf_incomingRelationsCnt[i] = tmp_index[current_pos]; //! ie, the 'reverse-sorted index'.
	assert(mapOf_incomingRelationsCnt[i] <= mapOf_incomingRelationsCnt_size);
	current_pos++;
      }
    }
  }
  for(uint i = current_pos; current_pos < mapOf_incomingRelationsCnt_size; current_pos++) {
    mapOf_incomingRelationsCnt[i] = UINT_MAX; //! ie, reset.
  }
  //! ------------------------
  //!
  //! De-allcoat ethe lcoally allocated memory-chunks:
  free_1d_list_uint(&tmp_sorted);
  free_1d_list_uint(&tmp_index);
  free_1d_list_uint(&tmp_result);
 }
