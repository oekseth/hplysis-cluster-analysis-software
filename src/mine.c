#ifndef __is_called_from_standAloneMine
#include "mine.h"
#endif
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#include <omp.h>
#endif
#include "func_hp3.h" //! which includes "hp3*..(..)" funcitosn, both wrt. the 'naive' and the 'improved' flavours/software-versions.
#include "rapidMic_core.h"
//#include "fast_log.h" //! which provide access to a 'fast-log' macro

/*   
    libmine core library
    
    This code is written by Davide Albanese <davide.albanese@gmail.com>
    and Michele Filosi <filosi@fbk.eu>.
    Copyright (C) 2012 Davide Albanese, Copyright (C) 2012 Michele 
    Filosi, Copyright (C) 2012 Fondazione Bruno Kessler.

    References:
    a) D. Reshef, Y. Reshef, H. Finucane, S. Grossman, G. McVean,
       P. Turnbaugh, E. Lander, M. Mitzenmacher, P. Sabeti. 
       Detecting novel associations in large datasets.
       Science 334, 6062 (2011)
    b) D. Albanese, M. Filosi, R. Visintainer, S. Riccadonna, G. Jurman,
       C. Furlanello.
       minerva and minepy: a C engine for the MINE suite and its R, 
       Python and MATLAB wrappers. 
       Bioinformatics first published online December 14, 2012
       doi:10.1093/bioinformatics/bts707. 
       http://bioinformatics.oxfordjournals.org/content/early/2013/01/06/bioinformatics.bts707
    
    This program is free software only for academic use.   you can redistribute it and/or modify
    it under the terms of the hpLysis documentation 
    
    

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    hpLysis documentation for more details.

    You should have received a copy of the hpLysis documentation
    along with this program. 
*/


/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */

 


// FIXME: before starting validate that a performance-improvement of approx. [2x ... 4x] is of interest .... ie, where we assume that using "float" is ok and where the 'major costs' are assicated matrix-operaitons ...  eg, when compared ot the work of "https://bioinformatics.oxfordjournals.org/content/29/3/407/F1.expansion.html"
// FIXME: wrt. [above] update [below] to use "float" instead of "dobule" <-- first validate oru assumption that the speed decreases 


//! Specify the container to be used to build for each parallel thread
typedef struct threadSafe_mine_compute {
  t_float *M_temp;
  t_sparseVec_counts *Q_map_temp, *Q_map, *P_map;
  // preAllocated_memory_t obj_preAllocated_memory; // = NULL;

} threadSafe_mine_compute_t;

static void threadSafe_mine_compute_allocate(const mine_problem *prob, const mine_score *score, threadSafe_mine_compute_t *obj) {
  assert(obj); assert(prob);
  //! ---------------------------------------------------------
  //! Allocate:
  //! ---------------------------------------------------------
  const t_sparseVec_counts default_value_sparseVec = 0;
  obj->Q_map_temp = allocate_1d_list_sparseVec_counts(prob->n, default_value_sparseVec); //(int *) malloc (prob->n * sizeof(int));
  //obj->Q_map_temp = (int *) malloc (prob->n * sizeof(int));
  if (obj->Q_map_temp == NULL) assert(false);

  obj->Q_map = allocate_1d_list_sparseVec_counts(prob->n, default_value_sparseVec);
  obj->P_map = allocate_1d_list_sparseVec_counts(prob->n, default_value_sparseVec);
  /* obj->Q_map = (int *) malloc (prob->n * sizeof(int)); */
  /* obj->P_map = (int *) malloc (prob->n * sizeof(int)); */
  if (obj->P_map == NULL) assert(false);
  if (obj->Q_map == NULL) assert(false);  

  obj->M_temp = (t_float *)malloc ((score->m[0]) * sizeof(t_float));
  if (obj->M_temp == NULL) assert(false);

  
  /* if(isTo_use_preAllocatedMemory()) { */
  /*   //    obj_preAllocated_memory =  */
  /*   allocate_preAllocated_memory(&(obj->obj_preAllocated_memory), prob->n); */
  /* } */
}
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)  
static void threadSafe_mine_compute_free(threadSafe_mine_compute_t *obj) {
  assert(obj);
  /* if(isTo_use_preAllocatedMemory()) { */
  /*   de_allocate(&(obj->obj_preAllocated_memory)); */
  /* } */
  free(obj->P_map);
  free(obj->Q_map);
  free(obj->Q_map_temp);
  free(obj->M_temp);
}
#endif


function_prefix void quicksort(t_float *a, int *idx, int l, int u) {
  int i, m, idx_temp;
  t_float a_temp;

  if (l >= u) {
    return;  
  }
  m = l;
  for (i=l+1; i<=u; i++)
    {
      if (a[i] < a[l])
	{
	  ++m;
	  
	  idx_temp = idx[m];
	  idx[m] = idx[i];
	  idx[i] = idx_temp;
       	  
	  a_temp = a[m];
	  a[m] = a[i];
	  a[i] = a_temp;
	}     
    }
  
  idx_temp = idx[l];
  idx[l] = idx[m];
  idx[m] = idx_temp;
  
  a_temp = a[l];
  a[l] = a[m];
  a[m] = a_temp;
  
  quicksort(a, idx, l, m-1);
  quicksort(a, idx, m+1, u);
}


int *argsort(t_float *a, int n) {
  t_float *a_cpy;
  int i, *idx;
  
  a_cpy = allocate_1d_list_float(/*size=*/n, /*empty-value=*/0); //(t_float *) malloc(n * sizeof(t_float));
  // a_cpy = (t_float *) malloc(n * sizeof(t_float));
  if (a_cpy == NULL) {
    return NULL;
  }
  
  idx = allocate_1d_list_int(/*size=*/n, /*empty-value=*/0); // (uint *) malloc(n * sizeof(uint));
  // idx = (int *) malloc(n * sizeof(int));
  if (idx == NULL)
    {
      free(a_cpy);
      return NULL;
    }
   
  /* fill a_cpy */
  memcpy(a_cpy, a, n * sizeof(t_float));

  /* fill idx */
  for (i=0; i<n; i++) {
    idx[i] = i;
  }
  
  quicksort(a_cpy, idx, 0, n-1);
  free(a_cpy);
  
  return idx;
}





/* Returns the map Q: D -> {0, ...,q-1}. 
 * See Algorithm 3 in SOM.
 * 
 * Parameters
 *   dy (IN): y-data sorted in increasing order
 *   n (IN): number of elements of dy
 *   y (IN): an integer greater than 1
 *   Q_map (OUT) : the map Q. Q_map must be a preallocated vector of 
 *                 size n
 *   q (OUT) : number of partitions in Q_map. q can be < y
 * 
 * Returns
 *   0
 */
function_prefix int EquipartitionYAxis(t_float *dy, int n, int y, t_sparseVec_counts *Q_map, int *q)
{
  int i, j, s, h;
  t_sparseVec_counts curr;
  t_float temp1, temp2;
  
  t_float rowsize = (t_float) n / (t_float) y;
  
  i = 0;
  h = 0;
  curr = 0;

  // FIXME: try to re-write ["elow] and then idnetify a use-case 'in our work'.
  //! Note: time=|n|*|n|, where the latter is an upper limit
  while (i < n) {
      s = 1;
      for (j=i+1; j<n; j++)
	{
	  if (dy[i] == dy[j])
	    ++s;
	  else 
	    break;
	}
      
      temp1 = fabs((t_float) h + (t_float) s - rowsize);
      temp2 = fabs((t_float) h - rowsize);
      if ((h != 0) && (temp1 >= temp2))
	{
	  ++curr;
	  h = 0;
	  temp1 = (t_float) n - (t_float) i;
	  temp2 = (t_float) y - (t_float) curr;
	  rowsize = temp1 / temp2;
	}

      /* #if 1 == 0 */
      for (j=0; j<s; j++) {
	Q_map[i+j] = curr;
      }
/* #else //! */
/*       memset(Q_map + i, curr, /\*size=*\/s); */
/* #endif */
      i += s;
      h += s;
    }
  
  *q = (int)curr + 1;
  
  return 0;
}


/* Returns the map P: D -> {0, ...,p-1}.
 * 
 * Parameters
 *   dx (IN) : x-data sorted in increasing order
 *   n (IN) : number of elements of dx
 *   Q_map (IN) : the map Q computed by EquipartitionYAxis sorted in 
 *                increasing order by dx-values
 *   P_map (OUT) : the map P. P_map must be a preallocated vector
 *                 of size n
 *   p (OUT) : number of partitions in P_map
 *
 * Returns
 *   0 on success, 1 if an error occurs
 */
function_prefix int GetClumpsPartition(t_float *dx, int n, t_sparseVec_counts *Q_map, t_sparseVec_counts *P_map, int *p)
{
  int i, j, flag, c, s;
  int *Q_tilde;
  
  i = 0;
  c = -1;
  
  Q_tilde = allocate_1d_list_int(/*size=*/n, /*empty-value=*/0); //(int *) malloc (n * sizeof(int));
  //Q_tilde = (int *) malloc (n * sizeof(int));
  if (Q_tilde == NULL) {
    return 1;
  }
  
  memcpy(Q_tilde, Q_map, n*sizeof(int));
  
  //! Note: time=|n|*|n|, where the latter is an upper limit
  while (i < n) {
      s = 1;
      flag = false;
      for (j=i+1; j<n; j++)
	{
	  if (dx[i] == dx[j])
	    {
	      if (Q_tilde[i] != Q_tilde[j])
		flag = true;
	      ++s;
	    }
	  else
	    break;
	}
      
      if ((s > 1) && (flag == true))
	{
#if 1 == 1 //! Note: this macro-alternative hadve signifanct 'impac't on a radnom inptu of numbers.
	  for (j=0; j<s; j++)
	    Q_tilde[i+j] = c;
#else
	  memset(Q_tilde + i, c, /*size=*/s);
#endif
	  --c;
	}
      
      i += s;
    }
  
  i = 0;
  P_map[0] = 0;
  for (j=1; j<n; j++)
    {
      if (Q_tilde[j] != Q_tilde[j-1])
	++i;
      P_map[j] = (t_sparseVec_counts)i;
    }
  
  *p = i + 1;
  free(Q_tilde);
  
  return 0;
}


/* Returns the map P: D -> {0, ...,p-1}.
 * 
 * Parameters
 *   dx (IN) : x-data sorted in increasing order
 *   n (IN) : number of elements of dx
 *   k_hat (IN) : maximum number of clumps
 *   Q_map (IN) : the map Q computed by EquipartitionYAxis sorted in 
 *                increasing order by dx-values
 *   P_map (OUT) : the map P. P_map must be a preallocated vector
 *                 of size n
 *   p (OUT) : number of partitions in P_map
 *
 * Returns
 *   0 on success, 1 if an error occurs
 */
function_prefix int GetSuperclumpsPartition(t_float *dx, int n, int k_hat, t_sparseVec_counts *Q_map, 
			    t_sparseVec_counts *P_map, int *p)
{
  int i, ret;
  t_float *dp;
   
  /* clumps */
  ret = GetClumpsPartition(dx, n, Q_map, P_map, p);
  if (ret) {
    return 1;
  }
 
  // FIXME: for each of the whiel-loops try to 'change them' into for-loops, ie, to simplify optimziation.
 
  /* superclumps */
  if (*p > k_hat)
    {
      dp = allocate_1d_list_float(/*size=*/n, /*empty-value=*/0); // (t_float *) malloc (n * sizeof(t_float));
      //dp = (t_float *) malloc (n * sizeof(t_float));
      if (dp == NULL)
	return 1;
      
      for (i=0; i<n; i++)
	dp[i] = (t_float) P_map[i];
      
      EquipartitionYAxis(dp, n, k_hat, P_map, p);
      
      free(dp);
    }
  
  return 0;
}


/* Returns (c_1, ..., c_k) */
function_prefix t_sparseVec *compute_c(t_sparseVec_counts *P_map, int p, int n)
{
  int i;
  t_sparseVec *c;  
  c = allocate_1d_list_sparseVec(/*size=*/p, /*empty-value=*/0); // (int *) malloc (p * sizeof(int));
  //c = (int *) malloc (p * sizeof(int));
  if (c == NULL) {
    return NULL;
  }
  
#if 1 == 0
  for (i=0; i<p; i++)
    c[i] = 0;
#endif //! else we assuemt eh [ªbove] 'default' memset-function has alreayd set this value correctly

  for (i=0; i<n; i++)
    c[(int)P_map[i]] += 1;

  /* assert(false); // FIXME: write intri-coce for ["elow] ... usign a new itnri-macro. ... for 'prefix-sum'. */

  /* assert(false); // FIXME: wrt. ["elow] write some asserts(..) to get an idnication of the 'max chunks' 'for these'. */
  /* assert(false); // FIXME: consider to 'convert' the lsits of "c" and "cumhist" into 8-bit "uchar" and 16-bit "lowuint" lists, ie, as the values 'in the lists' is the sum of index-elements <-- vill this 'property' also hold for  "c[i] += c[i-1];" and "cumhist[i][j] += cumhist[i-1][j];" ... ?? */


  for (i=1; i<p; i++)
    c[i] += c[i-1];
  
  return c;
}


/* Returns the cumulative histogram matrix along P_map */
function_prefix t_sparseVec **compute_cumhist(t_sparseVec_counts *Q_map, int q, t_sparseVec_counts *P_map, int p, int n, t_float ***cumhist_log)
{
  int i, j;
  t_sparseVec **cumhist;

  if(isTo_use_oldDataScheme() == false) { //! then we 'swap'/invert the counters:
    const uint tmp_q = q; q = p; p = tmp_q; //! then we 'invert' the distances.
  }

  if(isTo_use_continousSTripsOf_memory()) {
    cumhist = allocate_2d_list_sparseVec(/*size-in=*/q, /*size-out=*/p, /*default-value=*/0);
/* #if optmize_use_logValues_fromLocalTable == 1  */
/*     t_float **tmp = allocate_2d_list_float(/\*size-in=*\/q, /\*size-out=*\/p, /\*default-value=*\/0);   */
/*     if(true) {printf("allocated cumhist_log w/ise |%d||%d|, at %s:%d\n", q, p, __FILE__, __LINE__);} */
/*     *cumhist_log = tmp; //! ie, update. */
/* #endif */
  } else {
    cumhist = (t_sparseVec **) malloc (q * sizeof(t_sparseVec *));
    if (cumhist == NULL)
      return NULL;
  

    for (i=0; i<q; i++)
      {
	const t_sparseVec default_value_sparseVec = 0;
	cumhist[i] = allocate_1d_list_sparseVec(/*size=*/p, default_value_sparseVec); //(int *) malloc (p * sizeof(int));
	if (cumhist[i] == NULL)
	  {
	    /* if(isTo_use_continousSTripsOf_memory()) { */
	    /*   free(cumhist[0]); */
	    /* } else */ {
	      for (j=0; j<i; j++)
		free(cumhist[j]);
	    }
	    free(cumhist);
	    return NULL;
	  }
      
	for (j=0; j<p; j++)
	  cumhist[i][j] = 0;
      }
  }
#if optmize_use_logValues_fromLocalTable == 1 
    t_float **tmp = allocate_2d_list_float(/*size-in=*/q, /*size-out=*/p, /*default-value=*/0);  
    if(false) {printf("allocated cumhist_log w/ise |%d||%d|, at %s:%d\n", q, p, __FILE__, __LINE__);}
    *cumhist_log = tmp; //! ie, update.
#endif

  if(isTo_use_oldDataScheme()) { //! then we use the 'old alternative', ie, one value at each row, ie, ineffective  
    for (i=0; i<n; i++)
      cumhist[(int)Q_map[i]][(int)P_map[i]] += 1;
    
    for (i=0; i<q; i++)
      for (j=1; j<p; j++)
	cumhist[i][j] += cumhist[i][j-1];
  } else {

    // FIXME: write a correctness-test for ["elow] .. comparign to [ªbov€] .. when have complete the "isTo_use_oldDataScheme(..)" code-update.

    // FIXME: add support for computing prefix-sum using vectorization with and without parallisaiotn... and then test the time-effect of the latter.
    for (int i=0; i<n; i++) {
      /* assert(P_map[i] < q); //! ie, as we expec thte 'operation' to have 'sapped' teh counters. */
      /* assert(Q_map[i] < p); */
      cumhist[(int)P_map[i]][(int)Q_map[i]] += 1; //! ie, update the 'inverse pair'.
    }

    // FIXME[intri]: support [below] 

    // assert(false); // FIXME: write intri-coce for ["elow] ... usign a new itnri-macro. ... for 'prefix-sum'. <-- is 'this neccesary' for ["elow]?

    //! Note: wrt. correctenss of ["elow] one should remember that we have 'ivnerted' "q" and "p"
    for (int i=1; i<q; i++) {
      // memset(cumhist[i], 0.001, sizeof(int)*p);
      for (int j=0; j<p; j++) {
      	cumhist[i][j] += cumhist[i-1][j];
      }
    }
  }
#if optmize_use_logValues_fromLocalTable == 1 
  //! Then we comptue the log-values for the "cumhist" 2d-table:
  t_float **cumhist_log_local = *cumhist_log;
  // printf("Sets cumhist_log_local for an array of [%d][%d], elemnets, at [%s]:%s:%d\n", q, p, __FUNCTION__, __FILE__, __LINE__);
  for (int i=0; i<q; i++) {
    for (int j=0; j<p; j++) {
      // FIXME: consider to otpmize/imrpove [below] call ... using pre-computation <-- imrpovement would be singifcant.
      // TODO[log]: consider to drop ["elow] if-clause
      //if(cumhist[i][j] > 0)
      {
	const t_sparseVec value_local = cumhist[i][j];
	/* printf("get log for value[%u][%u] = %f, at %s:%d\n", (uint)i, (uint)j, (float)value_local, __FILE__, __LINE__); */
	/* printf("calls static_listOf_values_forIndex_midPoint=%p, at %s:%d\n", static_listOf_values_forIndex_midPoint, __FILE__, __LINE__); */
	/* printf("calls static_listOf_values_forIndex_midPoint[%u]=%f, and repv-value[%u][%u]=.., at %s:%d\n", (uint)value_local, static_listOf_values_forIndex_midPoint[(int)value_local], (uint)i, (uint)j, __FILE__, __LINE__); */
	//printf("calls static_listOf_values_forIndex_midPoint[%u]=%f, and repv-value=%f, at %s:%d\n", (uint)value_local, static_listOf_values_forIndex_midPoint[(int)value_local], (float)cumhist_log_local[i][j], __FILE__, __LINE__);

	//cumhist_log_local[i][j] = static_listOf_values_forIndex_midPoint[(int)value_local];
	// FIXME[criticla]:  remove [above] and incldue ["elow]
	cumhist_log_local[i][j] = log_index_fast(value_local);
	//printf("(log_loca[%d][%d])\t log(%f)=%f, at %s:%d\n", i, j, cumhist[i][j], cumhist_log_local[i][j], __FILE__, __LINE__);
	//! What we expect:
#ifndef NDEBUG
	if(cumhist[i][j] > 0) {
	  const t_float exp_1 = debug_get_floatValue_approx(cumhist_log_local[i][j]);
	  const t_float exp_2 = debug_get_floatValue_approx(log(cumhist[i][j]));
	  //printf("[%d][%d]\t (compare) value(%f)\t %f VS (%f==%f), at %s:%d\n", i, j, cumhist[i][j], exp_1, exp_2, log(cumhist[i][j]),  __FILE__, __LINE__); 
	  assert(exp_1 == exp_2);
	}
#endif
      } /* else { */
      /* 	printf("(log_loca-ignore[%d][%d])\t log(%f)=%f, at %s:%d\n", i, j, cumhist[i][j], cumhist_log_local[i][j], __FILE__, __LINE__); */
      /* } */
    }
  }
#endif
  return cumhist;
}


/* Initializes the I matrix */
function_prefix t_float **init_I(int p, int x)
{
  int i, j;
  t_float **I;
  
  if(isTo_use_oldDataScheme() == false) {const uint tmp = p; p = x; x = tmp;} //! ie, then we swaps the elements.

  // FIXME: improve ["elow]
  const uint size = x+1;
  if(isTo_use_continousSTripsOf_memory()) {
    const uint size_x = p+1;
    I = allocate_2d_list_float(/*size-in=*/size_x, /*size-out=*/size, /*default-value=*/0);  
  } else {
    I = (t_float **) malloc ((p+1) * sizeof(t_float *));
    if (I == NULL)
      return NULL;
    
    for (i=0; i<=p; i++)
      {
	I[i] = allocate_1d_list_float(/*size=*/size, /*empty-value=*/0); //(t_float *) malloc ((x+1) * sizeof(t_float));
	//I[i] = (t_float *) malloc ((x+1) * sizeof(t_float));
	if (I[i] == NULL)
	  {
	    for (j=0; j<i; j++)
	      free(I[j]);
	    free(I);
	    return NULL;
	  }
	
	for (j=0; j<=x; j++)
	  I[i][j] = 0.0;
      }
  }
  return I;
}


/* Computes the HP2Q matrix */
#if optmize_use_logValues_fromLocalTable == 1
function_prefix t_float **compute_HP2Q(t_sparseVec **cumhist, t_sparseVec *c, t_float *c_log, int q, int p)
#else
function_prefix t_float **compute_HP2Q(t_sparseVec **cumhist, t_sparseVec *c, int q, int p)
#endif
{
  int i, j, s, t;
  t_float **HP2Q;

  const uint size = (p+1);  
  if(isTo_use_continousSTripsOf_memory()) {
    const uint size_y = p + 1;
    HP2Q = allocate_2d_list_float(size_y, size, 0);
  } else {
    HP2Q = (t_float **) malloc ((p+1) * sizeof(t_float *));
    if (HP2Q == NULL) {
      return NULL;
    }
    
    for (i=0; i<=p; i++)
      {
	
	const t_float default_value_float = 0;
	HP2Q[i] = allocate_1d_list_float(/*size=*/size, default_value_float); //malloc ((p+1) * sizeof(t_float));       
	//HP2Q[i] = (t_float *) malloc (size * sizeof(t_float));
	if (HP2Q[i] == NULL)
	  {
	    for (j=0; j<i; j++)
	      free(HP2Q[j]);
	    free(HP2Q);
	    return NULL;
	  }
      }
  }
  // FIXME: try to use ["elow] as a starting-poitn for improvment .... is condeptual similar to computing scaler product of a transposed matrix.
  // FIXME[parallel]: ... if ["elow] has a 'high comptuational cost', then use parallel comptuation.
  if(isTo_use_oldDataScheme()) {
    for (t=3; t<=p; t++) {
      for (s=2; s<=t; s++) {
	HP2Q[s][t] = hp2q_dataScheme_old(cumhist, c, q, p, s, t); 
      }
    }
  } else {

    // FIXME: handle the special case where "q == 1" <-- seems like [below] does not amnage to opmize the code
    /* if(q == 1) { */
    /*   for (t=3; t<=p; t++) { */
    /* 	for (s=2; s < t; s++) { //! ie, due to the "s == y" conidtion in the "hp2q(..)" function. */
    /* 	  const t_float total = (t_float) (c[t-1] - c[s-1]); const t_float total_inv = 1 / total; */
    /* 	  HP2Q[t][s] = -1 * hp2q_dataScheme_new_item(cumhist, t, s, /\*i=*\/0, total_inv); */
    /* 	} */
    /* 	HP2Q[t][s] = 0.0; //hp2q(cumhist, c, q, p, s, t); */
    /*   } */
    /* } else  */

    //if(false) { //! which if 'used' results in a perofmrance-degredation.
    //if( (q > 2*VECTOR_FLOAT_ITER_SIZE )  && isTo_use_codeRewritingTo_vectorOptimization_1dLoops()) {
#if (defaultCompilation_alwaysUse_SSE_slow == 1) //! then we dos not 'avoid' the cases where the blocks are all to small.
    if( isTo_use_codeRewritingTo_vectorOptimization_1dLoops() ) 
#else
    if( (q > 4*VECTOR_FLOAT_ITER_SIZE )  && isTo_use_codeRewritingTo_vectorOptimization_1dLoops()) 
#endif
{
    //    if( (q > 6*VECTOR_FLOAT_ITER_SIZE )  && isTo_use_codeRewritingTo_vectorOptimization_1dLoops()) {
      // FIXME: seems like ["elow] code-chunk results in reduced/slower performacne ... which is due to ...??...
      const int q_intri = (int)SPARSEVEC__get_maxIntriLength(q);
      for (t=3; t<=p; t++) {
	s=2;
	
#if 1 == 2 //! which is 'not included' as there is no perofrmance-benefit 'of this'.
	// FIXME[jc]: seems like this is not working, ie, could be due to teh 's=2' offset: suggestions 'for how to handle this'?
	//if(t > 4*VECTOR_FLOAT_ITER_SIZE) 
	//{
	// FIXME: ["elow]
	const int t_intri = (int)VECTOR_FLOAT_maxLenght_forLoop(t-s) + s;
	//	const int t_intri = (int)VECTOR_FLOAT_maxLenght_forLoop(t-s) + s;
	for (; s < t_intri; s += VECTOR_FLOAT_ITER_SIZE) { //! ie, due to the "s == y" conidtion in the "hp2q(..)" function. 
	  // assert(s < (t + VECTOR_FLOAT_ITER_SIZE)); // FIXME: remvoe
#if VECTOR_FLOAT_ITER_SIZE == 4
	  // FIXME: if we use ["elow] then update our memory-access-patterns to use "-2" as an offset wrt. "HP2Q[][]"
	  VECTOR_FLOAT_STORE(&HP2Q[t][s-2], VECTOR_FLOAT_SET(
							     hp2q_dataScheme_new_intri(cumhist, c, q, p, s+0, t, q_intri),
							     hp2q_dataScheme_new_intri(cumhist, c, q, p, s+1, t, q_intri),  
							     hp2q_dataScheme_new_intri(cumhist, c, q, p, s+2, t, q_intri),  
							     hp2q_dataScheme_new_intri(cumhist, c, q, p, s+3, t, q_intri)  
							     ));
#elif VECTOR_FLOAT_ITER_SIZE == 2
	  // FIXME: if we use ["elow] then update our memory-access-patterns to use "-2" as an offset wrt. "HP2Q[][]"
	  VECTOR_FLOAT_STORE(&HP2Q[t][s-2], VECTOR_FLOAT_SET(
							     hp2q_dataScheme_new_intri(cumhist, c, q, p, s+0, t, q_intri),
							     hp2q_dataScheme_new_intri(cumhist, c, q, p, s+1, t, q_intri)
							     ));
#else
#error "!!\t Add support for this case"
#endif
	}
      }
#endif
	//! Note[article]: ["elow] "s == y" implicat handling is an exmaple of a 'local optmizaiton'.
	for (; s < t; s++) { //! ie, due to the "s == y" conidtion in the "hp2q(..)" function.
#if optmize_use_logValues_fromLocalTable == 1 
	  HP2Q[t][s] = hp2q_dataScheme_new_intri(cumhist, c, c_log, q, p, s, t, q_intri);  
#else
	  HP2Q[t][s] = hp2q_dataScheme_new_intri(cumhist, c, q, p, s, t, q_intri);  
#endif
	}
	HP2Q[t][s] = 0.0; //hp2q(cumhist, c, q, p, s, t); 
      }
    } else {
      for (t=3; t<=p; t++) {
	//! Note[article]: ["elow] "s == y" implicat handling is an exmaple of a 'local optmizaiton'.
	for (s=2; s < t; s++) { //! ie, due to the "s == y" conidtion in the "hp2q(..)" function.
#if optmize_use_logValues_fromLocalTable == 1 
	  HP2Q[t][s] = hp2q_dataScheme_new(/*i=*/0, cumhist, c, c_log, q, p, s, t); 
#else
	  HP2Q[t][s] = hp2q_dataScheme_new(/*i=*/0, cumhist, c, q, p, s, t); 
#endif
	}
	HP2Q[t][s] = 0.0; //hp2q(cumhist, c, q, p, s, t); 
      }
    }
  }
    
  return HP2Q;
}



/* Returns the normalized MI scores.
 *  
 * Parameters
 *   dx (IN) : x-data sorted in increasing order by dx-values
 *   dy (IN) : y-data sorted in increasing order by dx-values
 *   n (IN) : number of elements in dx and dy
 *   Q_map (IN) : the map Q computed by EquipartitionYAxis() sorted in 
 *                increasing order by dx-values
 *   q (IN) : number of partitions in Q_map
 *   P_map (IN) : the map P computed by GetSuperclumpsPartition() sorted 
 *                in increasing order by Dx-values
 *   p (IN) : number of partitions in P_map
 *   x (IN) : maximum grid size on dx-values
 *   score (OUT) : mutual information scores. score must be a 
 *                 preallocated array of dimension x-1
 * Returns
 *   0 on success, 1 if an error occurs
 */
function_prefix int OptimizeXAxis(t_float *dx, t_float *dy, int n, t_sparseVec_counts *Q_map, int q,    t_sparseVec_counts *P_map, int p, int x, t_float *score)
{
#if (typeOf_implementation_rapidMine == 1) 
  return ApproxOptimizeXAxis(dx, dy, n, Q_map, q, P_map, p, x, score);
#else //! then we make us eof the implementiaotn in the "mine.c", ie, the impelmetnaiton corresponding to the orignal algorithm.


  int i, s, t, l;
  t_sparseVec *c;
  t_sparseVec **cumhist;
  t_float **I, **HP2Q;
  t_float F, F_max, HQ, ct, cs;
  
  
  /* return score=0 if p=1 */
  if (p == 1)
    {
#if 1 == 1
      for (i=0; i<x-1; i++) {
	score[i] = 0.0;
      } 
#else
      memset(score, 0, (x-1));
#endif
      return 0;
    }
 
  /* compute c */
  c = compute_c(P_map, p, n);
  /* if (c == NULL) */
  /*   goto error_c; */

#if optmize_use_logValues_fromLocalTable == 1
  t_float *c_log = allocate_1d_list_float(/*size=*/p, /*empty-value=*/0); // (int *) malloc (p * sizeof(int));
  for(uint i = 0; i < (uint)p; i++) {
    const t_float value_log = log_index_fast(c[i]); 
    c_log[i] = value_log;
    //! What we expect:
    const t_float exp_1 = debug_get_floatValue_approx(value_log);
    const t_float exp_2 = debug_get_floatValue_approx(mathLib_float_log(c[i]));
    // printf("[%d][%d]\t (compare) value(%f)\t %f VS (%f==%f), at %s:%d\n", i, j, cumhist[i][j], exp_1, exp_2, log(cumhist[i][j]),  __FILE__, __LINE__); 
#ifndef NDEBUG
    if(exp_1 != exp_2) {
      fprintf(stderr, "!!\t(investigate)\t[%d]\t (compare) for value(log(%f))\t (%f VS %f) =~(%f VS %f), at %s:%d\n", (int)i, (t_float)c[i], 
	      (t_float)value_log, mathLib_float_log(c[i]),
	      exp_1, exp_2,  __FILE__, __LINE__); 
    }
#endif
    assert(exp_1 == exp_2);
    //assert(debug_get_floatValue_approx(c_log[i]) == debug_get_floatValue_approx(cumhist[i][j]));
  }
#endif

  // FIXME[critical]: include ["elow]
  
  t_float **cumhist_log = NULL; //allocate_2d_list_sparseVec(arr_tmp_size, /*size-out=*/arr_tmp_size, /*default-value=*/0);   assert(arr_tmp); 

  /* compute the cumulative histogram matrix along P_map */
  cumhist = compute_cumhist(Q_map, q, P_map, p, n, &cumhist_log);
  if (cumhist == NULL)
    goto error_cumhist;

  /* I matrix initialization */
  I = init_I(p, x);
  if (I == NULL)
    goto error_I;

  /* Precomputes the HP2Q matrix */
#if optmize_use_logValues_fromLocalTable == 1
  HP2Q = compute_HP2Q(cumhist, c, c_log, q, p);
#else
  HP2Q = compute_HP2Q(cumhist, c, q, p);
#endif

  if (HP2Q == NULL)
    goto error_HP2Q;

  
  

  /* compute H(Q) */
  //! Note: is a linear iteration through a list: <-- Time-cost: we observe hav an insgifnctn cost (when comapred to the matrix-iteration).
  if(isTo_use_oldDataScheme()) { //! ie, one value at each row, ie, ineffective
    HQ = hq_old(cumhist, q, p, n);
  } else {
#if optmize_use_logValues_fromLocalTable == 1 
    const t_float n_inv = (1.0/(t_float)n);
    // FIXME[critical]: ... below:
    const t_float total_inv_log = log_index_fast_fixedValueOf_1 - log_index_fast(n); // 0.2; //log_range_0_one(n_inv);
    //printf("total_inv_log= %f =? %f, given log(1)=%f and log(%d)=%f, , at %s:%d\n", total_inv_log, log(1.0/n_inv), log(1), n, log(n),  __FILE__, __LINE__);
    assert((int)total_inv_log == (int)log(n_inv));
    //const t_float total_inv_log = log_range_0_one(n_inv);
    HQ = hq_improved(cumhist, cumhist_log, q, p, n, total_inv_log);
#else
    HQ = hq_improved(cumhist, q, p, n);
#endif


  }

  // printf("HQ=%f, at %s:%d\n", HQ, __FILE__, __LINE__);
  
  /*
   * Find the optimal partitions of size 2
   * Algorithm 2 in SOM, lines 3-8
   */
  // FIXME: try to use ["elow] as a starting-poitn for improvment ... itnegrating the code of "hp3q()" and "hp3()" into 'this' ... expects a time-reduction of approx. 1.4x

  //! Time-cost: a cost-differenc of approx. 21x given a change from "0m2.208s" to "0m47.329s" for a 'input-list-size' of "40,000" (if, when comapred to an 'ignorance' of ["elow]).
  // FIXME: write a vectorizaiton of ["elow].

  //! Note[tiem]: [belo]w] chunk is repsonsible for approx 50 per-cent of the exuectione-time-cost.
  if(isTo_use_oldDataScheme()) { //! ie, one value at each row, ie, ineffective

    for (t=2; t<=p; t++) {
      F_max = mineAccuracy_threshold_minValue_float; //-T_FLOAT_MAX;
      for (s=1; s<=t; s++) {
	//! Then "delta(s, t)" - ....
	//! Note: [below] call to "s, t" is a 'classical' comparison between 'partitions' on a line
	const t_float left_side = hp3_old(c, s, t);
	const t_float right_side = hp3q_dataScheme_old(cumhist, c, q, p, s, t);
	F = left_side - right_side;
	// if(true) {printf("[t=%d][s=%d] F_max=%f, F = %f - %f  = %f, at %s:%d\n", t, s, F_max, left_side, right_side, F, __FILE__, __LINE__);}
	if (F > F_max) {
	  assert(!isnan(HQ)); 	  assert(!isnan(F));
	  I[t][2] = HQ + F;
	  assert(!isnan(I[t][2]));
	  F_max = F;
	}
      }
      if(false) {printf("[t=%d] F_max=%f, at %s:%d\n", t, F_max, __FILE__, __LINE__);}
    }
  } else {
    // float **matrixOf_shannon = allocate_2d_list_float( .. );     assert(matrixOf_shannon);
    
#if (defaultCompilation_alwaysUse_SSE_slow == 1) //! then we dos not 'avoid' the cases where the blocks are all to small.
    if(isTo_use_codeRewritingTo_vectorOptimization_1dLoops() == false) 
#else
    if( (isTo_use_codeRewritingTo_vectorOptimization_1dLoops() == false) || (q < (VECTOR_FLOAT_ITER_SIZE*4)) )  
#endif
      {
    //if(isTo_use_codeRewritingTo_vectorOptimization_1dLoops() == false) {
      for (int t=2; t<=p; t++) {
	F_max = mineAccuracy_threshold_minValue_float; //-T_FLOAT_MAX;
	//! Note[article]: ["elow] "s == y" implicat handling is an exmaple of a 'local optmizaiton'.
	const t_float total = (t_float) c[t-1]; const t_float total_inv = 1/total;
#if optmize_use_logValues_fromLocalTable == 1 
	// FIXME: instead of [below] ... cosnider using a look-up table for the "c[t]" values.
	const t_float total_inv_log = log_index_fast_fixedValueOf_1 - c_log[t-1]; 
	const t_float total_inv_log_tmp = log_index_fast_fixedValueOf_1 - log_index_fast(total); // 0.2; //log_range_0_one(n_inv);   
	VECTOR_FLOAT_TYPE vec_total_inv_log = VECTOR_FLOAT_SET1(total_inv_log);
	//printf("(compare)\t %f =? %s, at %S:%d\n", );
	assert(debug_get_floatValue_approx(total_inv_log) == debug_get_floatValue_approx(total_inv_log_tmp));
	assert(debug_get_floatValue_approx(total_inv_log) == debug_get_floatValue_approx(log(total_inv)));

	//const t_float total_inv_log = log_range_0_one(total_inv);   
	//const t_float total_inv_log = log_range_0_one(total_inv); 
	const t_float result_init = 0 - hp3q_dataScheme_new(cumhist, cumhist_log, q, p, /*s=*/t, t, total, total_inv, total_inv_log); 	
#else  
	const t_float result_init = 0 - hp3q_dataScheme_new(cumhist,  q, p, /*s=*/t, t, total, total_inv);   
#endif
/* 	if(true) {printf("[t=%d][s=%d] F_max=%f, F = %f - %f  = %f, at %s:%d\n", t, t, F_max, left_side, right_side, result_max, __FILE__, __LINE__);} */

/* #if (mineAccuracy_threshold_minValue_float_useAbs == 1) */
/* 	t_float F_max = result_init */
/* 	assert(!isnan(HQ)); 	  assert(!isnan(F_max));  */
/* 	I[2][t] = HQ + F_max;  // ie, due to the 'default' 'setting' of "s == t" in our "hp3q(..)" which returns "0"  */
/* 	assert(!isnan(I[2][t])); */
/* #else //! then we expect mineAccuracy_threshold_minValue_float to be approx 0 (ie, the min-value of a psotive flaot-value): whe thereofre need to test if the vlaue is cretarer thant the min-value */
	
/* 	if(result_init > mineAccuracy_threshold_minValue_float) { */
/* 	  F_max = result_init; */
/* 	  assert(!isnan(HQ)); 	  assert(!isnan(F_max));  */
/* 	  I[2][t] = HQ + F_max;  // ie, due to the 'default' 'setting' of "s == t" in our "hp3q(..)" which returns "0" */
/* 	  assert(!isnan(I[2][t])); */
/* 	} */
/* #endif */
	int s = 1;
	for (; s <= t; s++) {	
#if optmize_use_logValues_fromLocalTable == 1
	  const t_float left_side = hp3_improved(c, c_log, s, t, total_inv, total_inv_log);
#else
	  const t_float left_side = hp3_improved(c, s, t, total_inv); 
#endif
#if optmize_use_logValues_fromLocalTable == 1 
	  const t_float total_inv_log = log_index_fast_fixedValueOf_1 - log_index_fast(total); //log_range_0_one(total_inv);
	  assert((int)total_inv_log == (int)log(total_inv));
	  const t_float right_side = hp3q_dataScheme_new(cumhist, cumhist_log, q, p, s, t, total, total_inv, total_inv_log);
#else
	  const t_float right_side = hp3q_dataScheme_new(cumhist, q, p, s, t, total, total_inv);
#endif
	  const t_float result_max = left_side - right_side;

	  // if(true) {printf("[t=%d][s=%d] F_max=%f, F = %f - %f  = %f, at %s:%d\n", t, s, F_max, left_side, right_side, result_max, __FILE__, __LINE__);}

	  if (result_max > F_max) {
	    I[2][t] = HQ + result_max;
	    assert(!isnan(I[2][t]));
	    F_max = result_max;
	  }
	}
	if(false) {printf("[t=%d] F_max=%f, at %s:%d\n", t, F_max, __FILE__, __LINE__);}
      }
    } else {
      const int q_intri = (isTo_use_codeRewritingTo_vectorOptimization_1dLoops()) ? (int)SPARSEVEC__get_maxIntriLength(q) : 0;

      for (int t=2; t<=p; t++) {
	//! Note[article]: ["elow] "s == y" implicat handling is an exmaple of a 'local optmizaiton'.
	const t_float total = (t_float) c[t-1]; const t_float total_inv = 1/total;
	VECTOR_FLOAT_TYPE vec_total_inv = VECTOR_FLOAT_SET1(total_inv);
	SPARSEVEC_TYPE vec_c_forT = SPARSEVEC_SET1(c[t-1]);
#if optmize_use_logValues_fromLocalTable == 1 
	// FIXME: instead of [below] ... cosnider using a look-up table for the "c[t]" values.
	const t_float total_inv_log = log_index_fast_fixedValueOf_1 - c_log[t-1]; 
	const t_float total_inv_log_tmp = log_index_fast_fixedValueOf_1 - log_index_fast(total); // 0.2; //log_range_0_one(n_inv);   
	VECTOR_FLOAT_TYPE vec_total_inv_log = VECTOR_FLOAT_SET1(total_inv_log);
	//printf("(compare)\t %f =? %s, at %S:%d\n", );
	assert((int)total_inv_log == (int)total_inv_log_tmp);
	assert((int)total_inv_log == (int)log(total_inv));
	//const t_float total_inv_log = log_range_0_one(total_inv);   
	//const t_float total_inv_log = log_range_0_one(total_inv); 
	t_float F_max = hp3q_dataScheme_new(cumhist, cumhist_log, q, p, /*s=*/t, t, total, total_inv, total_inv_log); 
	assert(!isnan(HQ)); 	  assert(!isnan(F_max)); 
	I[2][t] = HQ + F_max;  // ie, due to the 'default' 'setting' of "s == t" in our "hp3q(..)" which returns "0"
	assert(!isnan(I[2][t]));
#else  
	t_float F_max = hp3q_dataScheme_new(cumhist,  q, p, /*s=*/t, t, total, total_inv);   
	assert(!isnan(HQ)); 	  assert(!isnan(F_max)); 
	I[2][t] = HQ + F_max;  // ie, due to the 'default' 'setting' of "s == t" in our "hp3q(..)" which returns "0" 
	assert(!isnan(I[2][t]));
#endif



	int s=1;
	if(q_intri > 0) { //! ie, to aovid makign use of a 'function assicated to code-overhead'.
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1_SINGLE(F_max); //! ie, to reduce the number of if-calsues.
	  const int t_intri = (int)VECTOR_FLOAT_maxLenght_forLoop(t-s) + s;
	  //for (; s < t_intri; s += (VECTOR_FLOAT_ITER_SIZE*2)) {
	  for (; s < t_intri; s += VECTOR_FLOAT_ITER_SIZE) {
#if optmize_use_logValues_fromLocalTable == 1
#if VECTOR_FLOAT_ITER_SIZE == 4
	    VECTOR_FLOAT_TYPE vec_tmp_1 =  VECTOR_FLOAT_SET(
							    // FIXME[critical]: include ["elow]
							    hp3q_dataScheme_new_intri(cumhist, cumhist_log,  q, p, s, t, total_inv, vec_total_inv, vec_total_inv_log, q_intri), 
							    hp3q_dataScheme_new_intri(cumhist, cumhist_log,  q, p, s+1, t, total_inv, vec_total_inv, vec_total_inv_log, q_intri),
							    hp3q_dataScheme_new_intri(cumhist, cumhist_log,  q, p, s+2, t, total_inv, vec_total_inv, vec_total_inv_log, q_intri),
							    hp3q_dataScheme_new_intri(cumhist, cumhist_log,  q, p, s+3, t, total_inv, vec_total_inv, vec_total_inv_log, q_intri)
							    );							  
#elif VECTOR_FLOAT_ITER_SIZE == 2
	    VECTOR_FLOAT_TYPE vec_tmp_1 =  VECTOR_FLOAT_SET(
							    hp3q_dataScheme_new_intri(cumhist, cumhist_log,  q, p, s, t, total_inv, vec_total_inv, vec_total_inv_log, q_intri),
							    hp3q_dataScheme_new_intri(cumhist, cumhist_log,  q, p, s+1, t, total_inv, vec_total_inv, vec_total_inv_log, q_intri)
							    );
#else  
#error "!!\t Add support for this case"
#endif
#else
#if VECTOR_FLOAT_ITER_SIZE == 4
	    VECTOR_FLOAT_TYPE vec_tmp_1 =  VECTOR_FLOAT_SET(
							    // FIXME[critical]: include ["elow]
							    hp3q_dataScheme_new_intri(cumhist, q, p, s, t, total_inv, vec_total_inv, q_intri), 
							    hp3q_dataScheme_new_intri(cumhist, q, p, s+1, t, total_inv, vec_total_inv, q_intri),
							    hp3q_dataScheme_new_intri(cumhist, q, p, s+2, t, total_inv, vec_total_inv, q_intri),
							    hp3q_dataScheme_new_intri(cumhist, q, p, s+3, t, total_inv, vec_total_inv, q_intri)
							    );							  
#elif VECTOR_FLOAT_ITER_SIZE == 2
	    VECTOR_FLOAT_TYPE vec_tmp_1 =  VECTOR_FLOAT_SET(
							    hp3q_dataScheme_new_intri(cumhist, q, p, s, t, total_inv, vec_total_inv, q_intri),
							    hp3q_dataScheme_new_intri(cumhist, q, p, s+1, t, total_inv, vec_total_inv, q_intri)
							    );
#else
#error "!!\t Add support for this case"
#endif
#endif
	    VECTOR_FLOAT_TYPE vec_tmp_2 =  hp3_improved_intri(c, vec_c_forT, s, t, vec_total_inv);  
	    //! Update if we have foudn a better score:

	    // assert(false); // FIXME: validate correctness of ["elow] ... ie, consider to instead ....??...
	    vec_result = VECTOR_FLOAT_MAX(vec_result, VECTOR_FLOAT_SUB(vec_tmp_1, vec_tmp_2));
	  }
	  //! Update the max-value:
	  F_max = VECTOR_FLOAT_storeAnd_horizontalMax(vec_result); 
	  assert(!isnan(HQ)); 	  assert(!isnan(F_max));
	  I[2][t] = HQ + F_max;
	  assert(!isnan(I[2][t]));
	} 
	for (; s <= t; s++) {
	  //! Note: teh "hp3q(..)" function traverses "q"
	  // FIXME: test teh effect of using _mm_store_ps(..) for both the "t" for-loop and the "s" for-loop ... ie, if the latter has an effect, then add a new 'seperationtion' named "isTo_use_codeRewritingTo_vectorOptimization_twoForLoops"
	  // assert(VECTOR_FLOAT_ITER_SIZE <= 4);

	
	  t_float result_tmp_hp3 = 0;
	  //! Note: if we dorp a call to the "hp3(..)" funciton, the exec-time for "optmize_usePReComptued_floats" && "-cnt-features=10000" changes from .. "0m3.663s" to "0m3.311s"
	  /* #define functPermutation_hp3 0 */
	  /* #if functPermutation_hp3 == 0 */
	  /* 	! Note[article]: [below] call resutls in a "1.02x" time-imrpvoement (when comapred to the "hp3(..)" function). */
#if optmize_use_logValues_fromLocalTable == 1
	  result_tmp_hp3 = hp3_improved(c, c_log, s, t, total_inv, total_inv_log);
#else
	  result_tmp_hp3 = hp3_improved(c, s, t, total_inv); 
#endif


	  /* #else // functPermutation_hp3 == 2 */
	  /* 	result_tmp_hp3 = 0.1; */
	  /* #endif */

	  // FIXME: instead of [below] ... cosnider using a look-up table for the "c[t]" values.
#if optmize_use_logValues_fromLocalTable == 1 
	  const t_float total_inv_log = log_index_fast_fixedValueOf_1 - log_index_fast(total); //log_range_0_one(total_inv);
	  assert((int)total_inv_log == (int)log(total_inv));
	  t_float result_tmp_hp3q = hp3q_dataScheme_new(cumhist, cumhist_log, q, p, s, t, total, total_inv, total_inv_log);
#else
	  t_float result_tmp_hp3q = hp3q_dataScheme_new(cumhist, q, p, s, t, total, total_inv);
#endif
	  const t_float result_max = result_tmp_hp3 - result_tmp_hp3q;
	  if (result_max > F_max) {
	    // FIXME: instead of ["elow] use _mm_max_ps <-- use a temporary I[t] list ... and then update the max 'of this'. <-- why shoulød 'this' have anyy optimizaiton ... ie, try to investigate/compare. <-- first try/investigate the possible 'de-activation-opton'.
	    // FIXME: validate corretncess of [below] ... ie, as we overwrite a vlaeus for each time.
	    // FIXME: udpate ["elow] ... transposion both the allcoatioon-process and the assicated memory-accesses.
	    I[2][t] = HQ + result_max;
	    assert(!isnan(I[2][t]));
	    F_max = result_max;
	  }
	}
      }
    }
  }
  
  /*
   * Inductively build the rest of the table of
   * optimal partitions
   * Algorithm 2 in SOM, lines 10-17
   */
  //! Note[time: [below] coe 'consumes' approx. 10 per-cent of the total exeuctione-time
  // FIXME: try to use ["elow] as a starting-poitn for improvment ... is condeptual similar to computing scaler product of a transposed matrix.
  // FIXME[parallel]: ... if ["elow] has a 'high comptuational cost', then use parallel comptuation.
  for (l=3; l<=x; l++)     {
    if(isTo_use_oldDataScheme()) { //! ie, one value at each row, ie, ineffective
      for (t=l; t<=p; t++) 	{
	ct = (t_float) c[t-1];
	F_max = mineAccuracy_threshold_minValue_float; //-T_FLOAT_MAX;
	for (s=l-1; s<=t; s++) 	    {

	  F = __compute_function_F_naive(c, I, HP2Q, s, t, ct, HQ);
 	  
	  if(false) {printf("[l=%d][t=%d][s=%d] F=%f, F_max=%f, at %s:%d\n", l, t, s, F, F_max, __FILE__, __LINE__);}
	  
	  if (F > F_max)   {
	    assert(!isnan(HQ)); 	  assert(!isnan(F));
	    I[t][l] = HQ + F;
	    assert(!isnan(I[t][l]));
	    F_max = F;
	  }
	}
      }
    } else {
#if (defaultCompilation_alwaysUse_SSE_slow == 1) //! then we dos not 'avoid' the cases where the blocks are all to small.
      if(isTo_use_codeRewritingTo_vectorOptimization_1dLoops()) 
#else //! Note: optmizaiton-inpsectiosn indicates that threshodls does not have a sigifcnat effect in ["elow] code-chunk, ie, explains why 'we use the same thresholds here and in [ªbove]'.
      if(isTo_use_codeRewritingTo_vectorOptimization_1dLoops()) 
#endif
{
      //if(isTo_use_codeRewritingTo_vectorOptimization_1dLoops() && (p > (VECTOR_FLOAT_ITER_SIZE * 1)) ) {
	//int s=l-1;
	for (int t=l; t<=p; t++) { //! and in each iteration update "I[t][l]", a result which is then used in the next 'sequential' iterations.
	  int s=l-1;
	  const t_float ct = (t_float) c[t-1];
	  const t_float ct_inv = (ct != 0) ? (1.0 / ct) : 0;
	  t_float F_max = mineAccuracy_threshold_minValue_float; //T_FLOAT_MIN_ABS;
	  
	  // TODO[aritcle]: use ["elow] observation wrt. 'no optmizaiotn' to udpate our artilce ... ie, that 'small isngifcnat chunks does not result in a perofmrance-increase'.
#if 1 == 2 //! which if set increases the total exuection-time by a factor of 1.19x
	  const int diff = t-s;
	  if(diff > 20) {
	    const VECTOR_FLOAT_TYPE vec_ct = VECTOR_FLOAT_SET1(ct);
	    const VECTOR_FLOAT_TYPE vec_ct_inv = VECTOR_FLOAT_SET1(ct_inv);   
	    const VECTOR_FLOAT_TYPE vec_HQ = VECTOR_FLOAT_SET1(HQ);
	    const int t_intri = (int)VECTOR_FLOAT_maxLenght_forLoop(diff) + s;
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1_SINGLE(F_max); //! ie, to reduce the number of if-calsues.
	    for (; s <= t_intri; s += VECTOR_FLOAT_ITER_SIZE) {
	      const VECTOR_FLOAT_TYPE vec_tmp = __compute_function_F_intr(c, I, HP2Q, t, s, vec_ct, vec_ct_inv, vec_HQ);
	      vec_result = VECTOR_FLOAT_MAX(vec_result, vec_tmp);
	    }
	    //! Update the max-value:
	    F_max = VECTOR_FLOAT_storeAnd_horizontalMax(vec_result); 
	    I[2][t] = HQ + F_max;
	    assert(!isnan(I[2][t]));
	  }
#endif

	  //! Note: we start at "s=l-1" iot. read through all earlier pre-compuations	  
	  for (; s <= t; s++) {
	    const t_float F = __compute_function_F(c, I, HP2Q, t, s, ct, ct_inv, HQ);
	    
	    if (F > F_max) {
	      I[l][t] = HQ + F; //! ie, then update the max-value for I[t][l]. <-- remember to inlcude the HQ value 'during the max-comptuation'
	      assert(!isnan(I[l][t]));
	      F_max = F;
	      assert(!isnan(HQ)); 	  assert(!isnan(F));
	    }
	  }
	}
      } else {      
	for (int t=l; t<=p; t++) { //! and in each iteration update "I[t][l]", a result which is then used in the next 'sequential' iterations.
	  int s=l-1;
	  const t_float ct = (t_float) c[t-1];
	  const t_float ct_inv = (ct != 0) ? (1.0 / ct) : 0;
#if 1 == 2 // FIXME[jc]: seems like on average 'this inline-appoarach' resutls in a lsower code then ["elow] <-- any ideas/expalnations 'for such'?
	  const t_float F = __compute_function_F(c, I, HP2Q, t, s, ct, ct_inv, HQ);
	  t_float F_max = F;
	  I[l][t] = HQ + F; //! ie, then update the max-value for I[t][l]. <-- remember to inlcude the HQ value 'during the max-comptuation'
	  assert(!isnan(I[l][t]));
	  assert(!isnan(HQ)); 	  assert(!isnan(F));
	  s++;
#else //! then we make use of the 'default' strategy:
	  t_float F_max = mineAccuracy_threshold_minValue_float; //T_FLOAT_MIN_ABS;
#endif


	  //! Note: we start at "s=l-1" iot. read through all earlier pre-compuations
	  for (; s <= t; s++) {
	    const t_float F = __compute_function_F(c, I, HP2Q, t, s, ct, ct_inv, HQ);

	    if(false) {printf("[l=%d][t=%d][s=%d] F=%f, F_max=%f, at %s:%d\n", l, t, s, F, F_max, __FILE__, __LINE__);}

	    if (F > F_max) {
	      assert(!isnan(HQ));
	      assert(!isnan(F));
	      I[l][t] = HQ + F; //! ie, then update the max-value for I[t][l]. <-- remember to inlcude the HQ value 'during the max-comptuation'
	      assert(!isnan(I[l][t]));
	      F_max = F;
	    }
	  }
	}
      }
    }
  }
  
  { /* Algorithm 2 in SOM, line 19 */

    
    //! Note: ["elow] code-chunk is assicated to an ins-isngifnt exuection-time, ie, which explains why itnrisntict optimiziaotn is Not used.
    //! Note: in ["elow] we use the same memory-access-pattern wrt. "isTo_use_oldDataScheme()" and "isTo_use_oldDataScheme() == false". Thsi givent eh 'access-pattern' wrt. teh 'followign/trailing "score" operation.
    if(isTo_use_oldDataScheme()) {
      for (i=p+1; i<=x; i++) {
	assert(!isnan(I[p][p]));
	I[p][i] = I[p][p];
      }
      /* score */
      for (i=2; i<=x; i++) {
	score[i-2] = I[p][i] / MIN(log_index(i), log_index(q));
#if default_value_avoidErrors_nan == 1
	assert(!isnan(I[p][i]));
	assert(!isnan(log_index(q)));
	assert(!isnan(log_index(i)));
	assert(!isnan(score[i-2]));	  
#endif
      }
    } else {
      
      // FIXME[perofrmance] set ["elow] to "true"
      if(false) { 
     
	// FIXME: try to imrpvoe ["elow] using our 'old' code.
	/* for (i=p+1; i<=x; i++) { */
	/* 	I[i][p] = I[p][p]; */
	/* } */
	/* score */
#if 1 == 0
	const t_float value_log_preComputed = (t_float)log_index(q);
	const t_float value_log_preComputed_inv = (value_log_preComputed != 0) ? 1.0 / value_log_preComputed : 0;
#endif
	const int p_tmp = MIN(p, x);
	for (i=2; i <= p_tmp; i++) {
	  //      for (; i<=x; i++) {
	  const t_float value_log_i = log_index(i);
	  const t_float value_log_i_inv = (value_log_i != 0) ? 1.0 / value_log_i : 0;
#if 1 == 0
	
	  assert(!isnan(I[i][p)));
	  score[i-2] = I[i][p] * MIN(value_log_i_inv, value_log_preComputed_inv);
	  assert(!isnan(score[i-2)));
#else
	  const t_float value_log_preComputed = (t_float)log_index(q);
	  const t_float value_log_preComputed_inv = (value_log_preComputed != 0) ? 1.0 / value_log_preComputed : 0;
	  score[i-2] = I[i][p] * MIN(value_log_i_inv, value_log_preComputed_inv);
	  assert(!isnan(I[i][p]));
	  assert(!isnan(score[i-2]));
#endif
	}
	for (i=p+1; i<=x; i++) {
	  I[i][p] = I[p][p];
	  //      for (; i<=x; i++) {
	  const t_float value_log_i = log_index(i);
	  const t_float value_log_i_inv = (value_log_i != 0) ? 1.0 / value_log_i : 0;
#if 1 == 0
	
	  score[i-2] = I[i][p] * MIN(value_log_i_inv, value_log_preComputed_inv);
#else
	  const t_float value_log_preComputed = (t_float)log_index(q);
	  const t_float value_log_preComputed_inv = (value_log_preComputed != 0) ? 1.0 / value_log_preComputed : 0;
	  score[i-2] = I[i][p] * MIN(value_log_i_inv, value_log_preComputed_inv);
	  assert(!isnan(I[i][p]));
	  assert(!isnan(score[i-2]));
#endif
	  /* #if 1 == 0 */
	  /* 	score[i-2] = I[i][p] / MIN(log_index(i), value_log_preComputed); */
	  /* #else */
	  /* 	score[i-2] = I[i][p] / MIN(log_index(i), log_index(q)); */
	  /* #endif */
	}
      } else {
	for (i=p+1; i<=x; i++) {
	  assert(!isnan(I[p][p]));
	  I[i][p] = I[p][p];
	}
	/* score */
	for (i=2; i<=x; i++) {
	  score[i-2] = I[i][p] / MIN(log_index(i), log_index(q));
	}
      }
      // assert(false); // FIXME: udpate ["elow]
    }
  }

  if(false) {
    for (i=2; i<=x; i++) {
      printf("score[%u]=%f, at %s:%d\n", i-2, score[i-2], __FILE__, __LINE__);
    }
  }
  
  /* start frees */
  /* if(isTo_use_continousSTripsOf_memory()) { */
  /*   free(HP2Q[0]); */
  /* } else { */
    for (i=0; i<=p; i++)
      free(HP2Q[i]);
    //  }
  free(HP2Q);

  /* if(isTo_use_continousSTripsOf_memory()) { */
  /*   const uint size_x = x+1; */
  /*   free_2d_list_float(&I, size_x); */
  /*   //free(I[0]); */
  /* } else { */
    if(isTo_use_oldDataScheme()) {
      for (i=0; i<=p; i++) {
	free(I[i]);
      }
    } else {
      for (int i=0; i<=x; i++) {
	free(I[i]);
      }
    }
  free(I);
  //  }

#if optmize_use_logValues_fromLocalTable == 1 
  free_2d_list_float(&cumhist_log, q);
  free_1d_list_float(&c_log);
#endif

  if(isTo_use_oldDataScheme()) {
    if(isTo_use_continousSTripsOf_memory()) {
      free_2d_list_float(&cumhist, q);
      //free(cumhist[0]);
    } else {
      for (i=0; i<q; i++) {
	free(cumhist[i]);
      }
    }
  } else {
    /* if(isTo_use_continousSTripsOf_memory()) { */
    /*   free(cumhist[0]); */
    /* } else */ {
      for (i=0; i<p; i++) {
	free(cumhist[i]);
      }
    }
    free(cumhist);
  }


  
  free (c);
  // end frees

  return 0;
  
  /* gotos*/
 error_HP2Q:
  /* if(isTo_use_continousSTripsOf_memory()) { */
  /*   free(I[0]); */
  /* } else */ {
    if(isTo_use_oldDataScheme()) {
      for (i=0; i<=p; i++) {
	free(I[i]);
      }
    } else {
      for (int i=0; i<=x; i++) {
	free(I[i]);
      }
    }
  }
  free(I);
 error_I:
  /* if(isTo_use_continousSTripsOf_memory()) { */
  /*   free(cumhist[0]); */
  /* } else  */{
    if(isTo_use_oldDataScheme()) {  
      for (i=0; i<q; i++) {
	free(cumhist[i]);
      }
    } else {
      for (i=0; i<p; i++) {
	free(cumhist[i]);
      }
    }
  }
#if optmize_use_logValues_fromLocalTable == 1 
  free_2d_list_float(&cumhist_log, q);
#endif

  free(cumhist);
 error_cumhist:
  free(c);
  //error_c:
  return 1;
#endif
}


/* Returns an initialized mine_score structure.
 * Returns NULL if an error occurs.
 */
function_prefix mine_score *init_score(mine_problem *prob, mine_parameter *param)
{
  int i, j;
  t_float B;
  mine_score *score;

  
  B = MAX(pow(prob->n, param->alpha), 4);
  
  score = (mine_score *) malloc (sizeof(mine_score));
  /* if (score == NULL) */
  /*   goto error_score; */

  score->n = MAX((int) floor(B/2.0), 2) - 1;
  const uint size_n = (uint)score->n;
  const int empty_value = 0;
  score->m = allocate_1d_list_int(size_n, empty_value); // (uint *) malloc(score->n * sizeof(uint));
  //score->m = (int *) malloc(score->n * sizeof(int));
  if (score->m == NULL)
    goto error_score_m;
  
  for (i=0; i<score->n; i++)
    score->m[i] = (int) floor((t_float) B / (t_float) (i+2)) - 1;
  
  score->M = (t_float **) malloc (score->n * sizeof(t_float *));
  if (score->M == NULL)
    goto error_score_M;
   
  for (i=0; i<score->n; i++)
    {
      const float empty_value = 0;
      const uint size_n = (uint)score->m[i];
      score->M[i] = allocate_1d_list_float(size_n, empty_value); //(t_float *)malloc(total_size * sizeof(t_float)); 
      //score->M[i] = (t_float *) malloc ((score->m[i]) * sizeof(t_float));
      if (score->M[i] == NULL)
	{
	  for (j=0; j<i; j++)
	    free(score->M[j]);
	  goto error_score_M_i;
	}
    }
  
  return score;
 
 error_score_M_i:
  free(score->M);
 error_score_M:
  free(score->m);
 error_score_m:
  free(score);
  //error_score:
  return NULL;
}

/* Computes the maximum normalized mutual information scores and 
 * returns a mine_score structure. Returns NULL if an error occurs.
 * Algorithm 5, page 14, SOM
 */
mine_score *mine_compute_score_notAllocate_logValues(mine_problem *prob, mine_parameter *param, const int *sortedArr_prob_x, const int *sortedArr_prob_y) {
  //int i, j, k, p, q;
  t_float *xx, *yy, *xy, *yx;
  //t_float  *xy, *yx;
  //int *ix, *iy;


  mine_score *score;



  score = init_score(prob, param);
  /* if (score == NULL) */
  /*   goto error_score; */

  xx = (t_float *) malloc (prob->n * sizeof(t_float));
  // if (xx == NULL)    goto error_xx;  
  yy = (t_float *) malloc (prob->n * sizeof(t_float)); 
  // if (yy == NULL)     goto error_yy;
  
  xy = (t_float *) malloc (prob->n * sizeof(t_float));
  /* if (xy == NULL) */
  /*   goto error_xy; */
  
  yx = (t_float *) malloc (prob->n * sizeof(t_float));
  /* if (yx == NULL) */
  /*   goto error_yx; */

  
  

  //! Investgiate if we are to make use of parallisation
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
  /* const uint cnt_threads_explicit_set = 4; */
  /* omp_set_num_threads(cnt_threads_explicit_set); // eg, to use 4 threads for all consecutive parallel regions */
  /* assert(omp_get_num_threads() == cnt_threads_explicit_set); */
  //const uint cnt_threads = (uint)omp_get_max_threads();
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
  const uint cnt_threads = (uint)omp_get_max_threads();
#else
  const uint cnt_threads = 1;
#endif
  assert(cnt_threads >= 1); //! ie, as otherwise would inidcate an error.
  // printf("cnt-trheads=%u/%u, at %s:%d\n", (uint)omp_get_num_threads(), (uint)omp_get_max_threads(), __FILE__, __LINE__);
  //! Allocate:
  threadSafe_mine_compute_t *listOf_threadSafeMemory = (threadSafe_mine_compute_t*)malloc(cnt_threads * sizeof(threadSafe_mine_compute_t));
  for(uint thread_id = 0; thread_id < cnt_threads; thread_id++) {
    threadSafe_mine_compute_allocate(prob, score, &listOf_threadSafeMemory[thread_id]);
  }
  //! --------------------
#else //! then we use a 'serial' non-parallel version:
  t_sparseVec_counts *Q_map_temp = NULL, *Q_map = NULL, *P_map = NULL;
  
  t_float *M_temp = (t_float *)malloc ((score->m[0]) * sizeof(t_float));
  /* if (M_temp == NULL) */
  /*   goto error_tmp; */

  const t_sparseVec_counts default_value_sparseVec = 0;
  Q_map_temp = allocate_1d_list_sparseVec_counts(prob->n, default_value_sparseVec); //(int *) malloc (prob->n * sizeof(int)); // (int *) malloc (prob->n * sizeof(int));
  /* if (Q_map_temp == NULL) */
  /*   goto error_tmp; */
  
  Q_map = allocate_1d_list_sparseVec_counts(prob->n, default_value_sparseVec); //(int *) malloc (prob->n * sizeof(int));
  /* if (Q_map == NULL) */
  /*   goto error_tmp; */
  
  P_map = allocate_1d_list_sparseVec_counts(prob->n, default_value_sparseVec); //allo(t *) malloc (prob->n * sizeof(int));
  /* if (P_map == NULL) */
  /*   goto error_tmp; */

#endif  
  // FIXME: try to use ["elow] as a starting-poitn for improvment.

  /* build xx, yy, xy, yx */
  for (int i=0; i<prob->n; i++)
    {
      xx[i] = prob->x[sortedArr_prob_x[i]];
      yy[i] = prob->y[sortedArr_prob_y[i]];
      xy[i] = prob->x[sortedArr_prob_y[i]];
      yx[i] = prob->y[sortedArr_prob_x[i]];
    }

  int ret = 0;


  // FIXME: include [below]

  //! Investgiate if we are to make use of parallisation
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#if optmize_parallel_1dLoop_useFast_scheduleFast == 1
  // FIXME: expereimetn with different valeus for ["elow]
#pragma omp parallel for schedule(dynamic,3) //! where "3" is based on observaitons
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(guided,3)
  //#pragma omp parallel for schedule(dynamic,4)
#elif optmize_parallel_1dLoop_useFast == 1 //! ie, then a 'slower' parallisation-scheudling-routine is used.
  // FIXME: expereimetn with different valeus for ["elow]
#pragma omp parallel for //schedule(dynamic,3) //! where "3" is based on observaitons
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
#endif
#endif
  
  /* x vs. y */
  // FIXME: update our benchmark-code to write a code-test for matrix-iteration with 4 unique loops, and then try to integrate "OptimizeXAxis(..)" into this.
  // FIXME[parallel]: ... if ["elow] has a 'high comptuational cost', then use parallel comptuation.
  for (int i=0; i<score->n; i++)
    {
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      //! Then we 'enable' thread-safe access to teh temproary data-structures:
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
      const uint thread_id = (uint)omp_get_thread_num();     
#else
      const uint thread_id = 0;
#endif
      //printf("thread_id=%u, at %s:%d\n", thread_id, __FILE__, __LINE__);
      assert(thread_id < cnt_threads);
      t_sparseVec_counts *Q_map_temp = listOf_threadSafeMemory[thread_id].Q_map_temp;
      t_sparseVec_counts *Q_map = listOf_threadSafeMemory[thread_id].Q_map;
      t_sparseVec_counts *P_map = listOf_threadSafeMemory[thread_id].P_map;
      //int *M_map = listOf_threadSafeMemory[thread_id].M_map;
#endif

      const int k = MAX((int) (param->c * (score->m[i]+1)), 1);

            //! Note[time]: ["elow code-hcunk is repsonislbe for 22 per-cent of the exeuction-time

      int q = 0;

#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      ret = EquipartitionYAxis(yy, prob->n, i+2, Q_map, &q);
      if(ret) { } // TODO: consider to add ahnhandler to this case
#else 
      ret = EquipartitionYAxis(yy, prob->n, i+2, Q_map, &q);
      /* if (ret) */
      /* 	goto error_tmp; */
#endif
      /* sort Q by x */
      for (int j=0; j<prob->n; j++)
  	Q_map_temp[sortedArr_prob_y[j]] = Q_map[j];
      for (int j=0; j<prob->n; j++)
  	Q_map[j] = Q_map_temp[sortedArr_prob_x[j]];
      
      //! Note[time]: if the 'trail of this' is ingored, then ["elow code-hcunk  ["elow code-hcunk is repsonislbe for 22 per-cent of the exeuction-time
      int p = 0;
      ret = GetSuperclumpsPartition(xx, prob->n, k, Q_map,
      				    P_map, &p);
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      // TODO: consider to add ahnhandler to this case
#else 
      /* if (ret) */
      /* 	goto error_tmp; */
#endif
      //! Note[time]: the ["elow] is repsonislbe for 93 per-cent of the exeuction-time
      if (param->est == EST_MIC_APPROX)
      	ret = OptimizeXAxis(xx, yx, prob->n, Q_map, q, P_map, p,
      			    score->m[i]+1, score->M[i]);
      else /* EST_MIC_E */
      	ret = OptimizeXAxis(xx, yx, prob->n, Q_map, q, P_map, p,
      			    MIN(i+2, score->m[i]+1), score->M[i]);

      //printf("i: %i, x: %i , max_y_approx: %i, max_y_e: %i\n", i, i+2, score->m[i]+1, MIN(i+2, score->m[i]+1));

#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      // TODO: consider to add ahnhandler to this case
#else 
      /* if (ret) */
      /* 	goto error_tmp; */
#endif
    }

  // assert(false); // FIXME: remove

  //! Investgiate if we are to make use of parallisation
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#if optmize_parallel_1dLoop_useFast_scheduleFast == 1
  // FIXME: expereimetn with different valeus for ["elow]
#pragma omp parallel for schedule(dynamic,3) //! where "3" is based on observaitons
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
#elif optmize_parallel_1dLoop_useFast == 1 //! ie, then a 'slower' parallisation-scheudling-routine is used.
  // FIXME: expereimetn with different valeus for ["elow]
#pragma omp parallel for //schedule(dynamic,3) //! where "3" is based on observaitons
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
#endif   
#endif
  /* y vs. x */
  // FIXME: update our benchmark-code to write a code-test for matrix-iteration with 4 unique loops, and then try to integrate "OptimizeXAxis(..)" into this.
  for (int i=0; i<score->n; i++)
    {
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      //! Then we 'enable' thread-safe access to teh temproary data-structures:
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
      const uint thread_id = (uint)omp_get_thread_num();
#else
      const uint thread_id = 0;
#endif
      assert(thread_id < cnt_threads);
      t_sparseVec_counts *Q_map_temp = listOf_threadSafeMemory[thread_id].Q_map_temp;
      t_sparseVec_counts *Q_map = listOf_threadSafeMemory[thread_id].Q_map;
      t_sparseVec_counts *P_map = listOf_threadSafeMemory[thread_id].P_map;
      t_float *M_temp = listOf_threadSafeMemory[thread_id].M_temp;
#endif

/* #if default_value_avoidErrors_nan == 1 */
/*       assert(!isnan(score->m[i])); //! ei, what we then expect. */
/* #endif */
      const int k = MAX((int) (param->c * (score->m[i]+1)), 1);

      int q = 0;
      ret = EquipartitionYAxis(xx, prob->n, i+2, Q_map, &q);
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      // TODO: consider to add ahnhandler to this case
#else 
      /* if (ret) */
      /* 	goto error_tmp; */
#endif
      /* sort Q by y */
      for (int j=0; j<prob->n; j++)
  	Q_map_temp[sortedArr_prob_x[j]] = Q_map[j];
      for (int j=0; j<prob->n; j++)
  	Q_map[j] = Q_map_temp[sortedArr_prob_y[j]];

      //! Note: main-complexity in [below] is a while-loop: calling [below] has an approx. cost of "n"
      int p = 0;
      ret = GetSuperclumpsPartition(yy, prob->n, k, Q_map,
  				    P_map, &p);
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      // TODO: consider to add ahnhandler to this case
#else 
      /* if (ret) */
      /* 	goto error_tmp; */
#endif
      
      if (param->est == EST_MIC_APPROX)
  	ret = OptimizeXAxis(yy, xy, prob->n, Q_map, q, P_map, p,
  			    score->m[i]+1, M_temp);
      else /* EST_MIC_E */
  	ret = OptimizeXAxis(yy, xy, prob->n, Q_map, q, P_map, p,
  			    MIN(i+2, score->m[i]+1), M_temp);

      //printf("i: %i, y: %i , max_x_approx: %i, max_x_e: %i\n", i, i+2, score->m[i]+1, MIN(i+2, score->m[i]+1));
#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)
      // TODO: consider to add ahnhandler to this case
#else 
      /* if (ret) */
      /* 	goto error_tmp; */
#endif
      // FIXME: try to integrate ["elow] in [ªbove].
      if (param->est == EST_MIC_APPROX)
  	for (int j=0; j < score->m[i]; j++)
  	  score->M[j][i] = MAX(M_temp[j], score->M[j][i]);
      else {/* EST_MIC_E */
  	for (int j=0; j<MIN(i+1, score->m[i]); j++) {
#if default_value_avoidErrors_nan == 1
	  assert(!isnan(M_temp[j]));
	  assert(!isnan(score->M[j][i]));
#endif
  	  score->M[j][i] = M_temp[j]; 
	}
      }
   }

#if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)  
  //! De-allocate:  
  assert(listOf_threadSafeMemory);
  for(uint thread_id = 0; thread_id < cnt_threads; thread_id++) {
    threadSafe_mine_compute_free(&listOf_threadSafeMemory[thread_id]);
  }
  free(listOf_threadSafeMemory);
#else
  free(M_temp);
  free(P_map);
  free(Q_map);
  free(Q_map_temp);
#endif


  free(yx);
  free(xy);
  free(yy);
  free(xx);



  return score;
    
  //error_0:
  //free(iy);
 /* error_iy: */
/*   //free(ix); */
/*  error_ix: */
/* #if (optmize_parallel_1dLoop_useFast_scheduleFast == 1) || (optmize_parallel_1dLoop_useFast == 1)   */
/*   //! De-allocate:   */
/*   assert(listOf_threadSafeMemory); */
/*   for(uint thread_id = 0; thread_id < cnt_threads; thread_id++) { */
/*     threadSafe_mine_compute_free(&listOf_threadSafeMemory[thread_id]); */
/*   } */
/*   free(listOf_threadSafeMemory); */
/* #else */
/*   free(M_temp); */
/*   free(P_map); */
/*   free(Q_map_temp); */
/*   free(Q_map); */
/* #endif */
 /*  error_tmp: */
 /* /\* error_Q_temp: *\/ */
 /*  free(yx); */
 /* /\* error_yx: *\/ */
 /* /\*  free(xy); *\/ */
 /* /\* error_xy: *\/ */
 /* /\*  free(yy); *\/ */
 /* /\* error_yy: *\/ */
 /* /\*  free(xx); *\/ */
 /* /\* error_xx: *\/ */
 /* /\*  for (int i=0; i<score->n; i++) *\/ */
 /* /\*    free(score->M[i]); *\/ */
 /* /\*  free(score->M); *\/ */
 /* /\*  free(score->m); *\/ */
 /* /\*  free(score); *\/ */
 /* /\* error_score: *\/ */
  return NULL;
}

/* Computes the maximum normalized mutual information scores and 
 * returns a mine_score structure. Returns NULL if an error occurs.
 * Algorithm 5, page 14, SOM
 */
mine_score *mine_compute_score(mine_problem *prob, mine_parameter *param) {
  assert(prob);
  //! Intalize the optmized log-orutine:
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 0) //! ie, as we then expec thte 'data-strucutre' tio Not have been loaded at 'run-time'.
  allocate_optmize_usePReComptued_floats();
#endif


  int *ix = argsort(prob->x, prob->n);   int *iy = argsort(prob->y, prob->n);
  //if (iy == NULL)     goto error_iy;
  //if (ix == NULL)     goto error_ix;  

  mine_score *ret_object = mine_compute_score_notAllocate_logValues(prob, param, /*sortedArr_prob_x=*/ix, /*sortedArr_prob_y=*/iy);

  //! Free the optmized log-orutine: 
#if(configInit__isToAllocate__mineScore__atLibraryLoading == 0)
  free_optmize_usePReComptued_floats();
#endif
  free(iy);   free(ix);

  //! @return
  return ret_object;
}

/* This function checks the parameters. It should be called 
 * before calling mine_compute_score(). It returns NULL if 
 * the parameters are feasible, otherwise an error message
 * is returned.
 */
char *mine_check_parameter(mine_parameter *param)
{
  if ((param->alpha <= 0.0) || (param->alpha > 1.0))
    return "alpha must be in (0, 1.0]";
  
  if (param->c <= 0.0)
    return "c must be > 0.0";
  
  if ((param->est != 0) && (param->est != 1))
      return "est must be in {0, 1}";

  return NULL;
}


/* Returns the Maximal Information Coefficient (MIC). */
t_float mine_mic(mine_score *score)
{
  int i, j;
  //  #if default_value_avoidErrors_nan == 1
  t_float score_max = mineAccuracy_threshold_minValue_float; //T_FLOAT_MIN_ABS;
/* #else */
/*   t_float score_max = 0.0; */
/* #endif   */

  for (i=0; i<score->n; i++) {
    for (j=0; j<score->m[i]; j++) {
      if (score->M[i][j] > score_max) {
	score_max = score->M[i][j]; 
      }
    }
  }
  
  return score_max;
}


/* Returns the Maximum Asymmetry Score (MAS). */
t_float mine_mas(mine_score *score)
{
  int i, j;
  t_float score_curr;
  t_float score_max = 0.0;
  
  for (i=0; i<score->n; i++)
    for (j=0; j<score->m[i]; j++)
      {
	score_curr = (t_float)fabs((t_float)(score->M[i][j] - score->M[j][i]));
	if (score_curr > score_max)
	  score_max = score_curr;
      }
  
  return score_max;
}


/* /\* Returns the Maximum Edge Value (MEV). *\/ */
/* t_float mine_mev(mine_score *score) */
/* { */
/*   int i, j; */
/*   t_float score_max = 0.0; */
  
/*   for (i=0; i<score->n; i++) */
/*     for (j=0; j<score->m[i]; j++) */
/*       if (((j==0) || (i==0)) && score->M[i][j] > score_max) */
/* 	score_max = score->M[i][j]; */
  
/*   return score_max; */
/* } */

/* Returns the Maximum Edge Value (MEV). */
t_float mine_mev(mine_score *score) {
  t_float score_max = mineAccuracy_threshold_minValue_float; //T_FLOAT_MIN_ABS;  
  // printf("score_max=%f, at %s:%d\n", score_max, __FILE__, __LINE__);
  // FIXME: parallise: <-- update our code as examplfied at "http://www.techdarting.com/2013/06/openmp-min-max-reduction-code.html", using:      #pragma omp parallel for reduction(max : score_max)
  //! Note: if we use a 'code-rewrite' we expect a perofmrnace-increase of "n"
  if(isTo_use_codeRewritingTo_reduceExeuctionTime()) {    
  //if(false) {
    //! First test for "i==0":
    uint cnt_updated = 0;
    const uint size_i_n = score->m[0];
    for (uint j=0; j< size_i_n; j++) {     
      // printf("compare:\t %.3f VS %.3f, at %s:%d\n", score->M[0][j], score_max, __FILE__, __LINE__);
      assert(!isnan(score->M[0][j])); //! ie, what we expect.
      if(score->M[0][j] > score_max) {
	// FIXME: use intrinsics ... <-- first update our time-benchmarks
	assert(score->M[0][j] >= 0); // ie, as the 'old verison' then would otehrwise contain a bug.
	score_max = score->M[0][j];
	cnt_updated++;
      }
    }
    //! The test for "j==0":
    for (uint i=0; i<score->n; i++) {
      assert(!isnan(score->M[i][0])); //! ie, what we expect.
      if(score->M[i][0] > score_max) {
	// FIXME: use intrinsics ...  <-- first update our time-benchmarks
	score_max = score->M[i][0];
	cnt_updated++;
      }
    }  
    assert(cnt_updated > 0); //! ie, what we expect.
  } else {
    uint cnt_updated = 0;
    for (uint i=0; i<score->n; i++) {
      for (uint j=0; j<score->m[i]; j++) {
	// FIXME: use intrinsics ... and drop/rewrite this for-loop
	assert(!isnan(score->M[i][j])); //! ie, what we expect.
	if (((j==0) || (i==0)) && score->M[i][j] > score_max) {
	  score_max = score->M[i][j];
	  cnt_updated++;
	}
      }
    }  
    //assert(cnt_updated > 0); //! ie, what we expect.
  }
  return score_max;
}



/* Returns the Minimum Cell Number (MCN) with eps >= 0. */
t_float mine_mcn(mine_score *score, t_float eps)
{
  int i, j;
  t_float log_xy;
  t_float score_min = mineAccuracy_threshold_minValue_float; //T_FLOAT_MAX;
  t_float delta = 0.0001; /* avoids overestimation of mcn */
  t_float mic = mine_mic(score);
  const t_float thresohld = ((1.0 - eps) * mic);
  for (i=0; i<score->n; i++) {
    for (j=0; j<score->m[i]; j++) {
      const int local_index = (i+2) * (j+2);
      log_xy = log_index(local_index) / log_index(2.0);
      if(false) {printf("(compare)\t (%f >? %f) && (%f <? %f), at %s:%d\n", score->M[i][j]+delta,  thresohld, log_xy, score_min, __FILE__, __LINE__);}
      if (((score->M[i][j]+delta) >= thresohld)
	  && (log_xy < score_min)) {
	score_min = log_xy;
      }
    }
  }
  if(score_min != mineAccuracy_threshold_minValue_float) {
    return score_min;
  } else {return 0;} // FIXME: validate correnctess fo 'this'.
}


/* /\* Returns the Minimum Cell Number (MCN) with eps = 1 - MIC. *\/ */
/* t_float mine_mcn_general(mine_score *score) */
/* { */
/*   int i, j; */
/*   t_float log_xy; */
/*   t_float score_min = DBL_MAX; */
/*   t_float delta = 0.0001; /\* avoids overestimation of mcn *\/ */
/*   t_float mic = mine_mic(score); */
  
/*   for (i=0; i<score->n; i++) */
/*     for (j=0; j<score->m[i]; j++) */
/*       { */
/* 	log_xy = log_index((i+2) * (j+2)) / log_index(2.0); */
/* 	if (((score->M[i][j]+delta) >= (mic * mic))  */
/* 	    && (log_xy < score_min)) */
/* 	  score_min = log_xy; */
/*       } */

/*   return score_min; */
/* } */

/* Returns the Minimum Cell Number (MCN) with eps = 1 - MIC. */
t_float mine_mcn_general(mine_score *score) {
  t_float log_xy;
  t_float score_min = mineAccuracy_threshold_minValue_float; //T_FLOAT_MAX;
  t_float delta = 0.0001; /* avoids overestimation of mcn */
  t_float mic = mine_mic(score); //, /*dmmy=*/DBL_MAX);

  // printf("mine-score=%f, at %s:%d\n", mic, __FILE__, __LINE__);
  
  // FIXME: parallise: <-- update our code as examplfied at "http://www.techdarting.com/2013/06/openmp-min-max-reduction-code.html", using:      #pragma omp parallel for reduction(min : score_min)

  //! Note: if we use a 'code-rewrite' we expect a perofmrnace-increase of as much as "9x/2", ie, as a 'random input' would imply that only half of the scores were evaluated.
  //if(isTo_use_codeRewritingTo_reduceExeuctionTime()) {    
  if(false) {
    const t_float mic_mult_delta = (mic * mic) - delta;
    //const t_float value_log_2 = log(2.0);
    assert(log(0) < log(10)); // else update ["elow]
    assert(log(1) < log(10)); // else update ["elow]

    if(false) 
      {
	{ //! Validate that the log-function 'provides' the invese nuymbers when compared to the 'ordinary' numbers:
	  const t_float val1 = 0.1;  const t_float val1_log_2 = log2(val1);
	  const t_float val2 = 0.3;  const t_float val2_log_2 = log2(val2);
	  assert(val1 < val2); assert(val1_log_2 < val2_log_2);
	}
	{ //! Validate that the log-function 'provides' the invese nuymbers when compared to the 'ordinary' numbers:
	  const t_float val1 = 2;  const t_float val1_log_2 = log2(val1);
	  const t_float val2 = 4;  const t_float val2_log_2 = log2(val2);
	  assert(val1 < val2); assert(val1_log_2 < val2_log_2);
	}
	{ //! Test the POSIX-GNU-C "log2(..)" functioanltiy, and proeprties wrt. the latter
	  const t_float val = 2;
	  const t_float val_log_2 = log2(val);
	  const t_float val_log_2_alt = log(val)/log(2);
	  printf("val=%f, val_log=%f, val_log_exp=%f, at %s:%d\n", val, val_log_2, val_log_2_alt, __FILE__, __LINE__);
	  assert(val_log_2 == val_log_2_alt);      
	}
	{ //! Test the 'back-track':
	  const t_float val = 2;
	  const t_float val_log = log(val);
	  const t_float val_log_exp = exp(val_log);
	  printf("val=%f, val_log=%f, val_log_exp=%f, at %s:%d\n", val, val_log, val_log_exp, __FILE__, __LINE__);
	  assert(val_log_exp == val);      
	}	
      }
    // assert(false); // FIXME: validate correctness of [ªbove] and then update [below]:
    //score_min = DBL_MIN;    //! ie, use the 'inverse max'

    for (uint i=0; i<score->n; i++) {
      const float i_pluus = (i+2)*2;
      /* const t_float log_xy_first = (t_float)(i_pluss * (score->m[i] +1)); */
      /* if(log_xy_first > score_min) { //! ie, do not evaluate/test memory before we know that the values 'are inside the limit': */
      uint j_startPos = 0;
      // FIXME: ask jan-crhistian to validate correctness of [below]


      assert(false); // FIXME: valdiate correctenss of ["below] ... ie, as the score is 'incremnetally updated'.
      if(score_min != T_FLOAT_MIN) { j_startPos = (uint)((score_min - i_pluus)/2);} //! ie, "(i+2)*(j+2) >? score_min ==> j >? (score_min - (i+2)*2)/2 = (score_min - i_pluss)/2"
      if(j_startPos <= score->m[i]) {
	for (uint j=j_startPos; j < score->m[i]; j++) {
	  // FIXME: if [ªbov€] is correct ... then 'partion' [below] ... 
	  const t_float log_xy = (t_float)(i_pluus * j);
	  //	log_xy = log((i+2) * (j+2)) / value_log_2;
	  // FIXME: re-order [below] ... where we use const const uint j_tmp = score->m[i]; t_float log_xy = log((i+2) * (j_tmp+2)) / log(2.0);
	  // FIXME: wrt. [ªbove] ... test if "log(0) >? log(10)" ... and then update
	  if(log_xy < score_min) { //! ie, do not evaluate/test memory before we know that the values 'are inside the limit':
	    if(score->M[i][j] >= mic_mult_delta) {
	      score_min = log_xy;
	    }
	  }
	}
      } //! else we assume the vetex is not of interest.
    }
    if(score_min != mineAccuracy_threshold_minValue_float) {
      score_min = log2(score_min);
    }
  } else {
    for (uint i=0; i<score->n; i++) {
      for (uint j=0; j<score->m[i]; j++) {
	log_xy  = log((i+2) * (j+2)) / log(2.0);
	//log_xy = log_index((i+2) * (j+2)) / log(2.0);
	// FIXME: re-order [below] ... where we use const const uint j_tmp = score->m[i]; t_float log_xy = log((i+2) * (j_tmp+2)) / log(2.0);
	// FIXME: wrt. [ªbove] ... test if "log(0) >? log(10)" ... and then update
	assert(!isnan(score->M[i][j])); 	assert(!isinf(score->M[i][j]));
	if (((score->M[i][j]+delta) >= (mic * mic)) && (log_xy < score_min)) {
	  score_min = log_xy;
	}
      }
    }
  }

  if(score_min != mineAccuracy_threshold_minValue_float) {
    return score_min;
  } else {return 0;} // FIXME: validate correnctess fo 'this'.
}


/* /\* Returns the e Generalized Mean Information Coefficient (GMIC) *\/ */
/* t_float mine_gmic(mine_score *score, t_float p) */
/* { */
/*   int i, j, k, Z, B; */
/*   mine_score *score_sub, *C_star; */
/*   t_float gmic; */
  
/*   /\* alloc score_sub *\/ */
/*   score_sub = (mine_score *) malloc (sizeof(mine_score)); */
  
/*   /\* alloc C_star *\/ */
/*   C_star = (mine_score *) malloc (sizeof(mine_score)); */
/*   C_star->m = (int *) malloc(score->n * sizeof(int)); */
/*   C_star->M = (t_float **) malloc (score->n * sizeof(t_float *)); */
/*   for (i=0; i<score->n; i++) */
/*     C_star->M[i] = (t_float *) malloc ((score->m[i]) * sizeof(t_float));   */
     
/*   /\* prepare score_sub *\/ */
/*   score_sub->M = score->M; */

/*   /\* prepare C_star *\/ */
/*   C_star->n = score->n; */
/*   for (i=0; i<C_star->n; i++) */
/*     C_star->m[i] = score->m[i]; */
   

/*   // FIXME: try to use ["elow] as a starting-poitn for improvment. ... seems as a simple 3d-matrix-operation */
/*   // FIXME[parallel]: ... if ["elow] has a 'high comptuational cost', then use parallel comptuation. */

/*   /\* compute C_star *\/ */
/*   for (i=0; i<score->n; i++) */
/*     for (j=0; j<score->m[i]; j++) */
/*       { */
/* 	B = (i+2) * (j+2); */
/* 	score_sub->n = MAX((int) floor(B/2.0), 2) - 1; */
/* 	score_sub->m = (int *) malloc(score_sub->n * sizeof(int)); */
/* 	for (k=0; k<score_sub->n; k++) */
/* 	  score_sub->m[k] = (int) floor((t_float) B / (t_float) (k+2)) - 1; */
	
/* 	C_star->M[i][j] = mine_mic(score_sub); */
/* 	free(score_sub->m); */
/*       } */
  
/*   /\* p=0 -> geometric mean *\/ */
/*   if (p == 0.0) */
/*     { */
/*       Z = 0; */
/*       gmic = 1.0; */
/*       for (i=0; i<C_star->n; i++) */
/* 	for (j=0; j<C_star->m[i]; j++) */
/* 	  { */
/* 	    gmic *= C_star->M[i][j]; */
/* 	    Z++; */
/* 	  } */
/*       gmic = pow(gmic, (t_float) Z); */
/*     } */
/*   /\* p!=0 -> generalized mean *\/ */
/*   else */
/*     { */
/*       Z = 0; */
/*       gmic = 0.0; */
/*       for (i=0; i<C_star->n; i++) */
/* 	for (j=0; j<C_star->m[i]; j++) */
/* 	  { */
/* 	    gmic += pow(C_star->M[i][j], p); */
/* 	    Z++; */
/* 	  } */
/*       gmic /= (t_float) Z; */
/*       gmic = pow(gmic, 1.0/p); */
/*     } */

/*   free(score_sub); */
/*   if (C_star->n != 0) */
/*     { */
/*       free(C_star->m); */
/*       for (i=0; i<C_star->n; i++) */
/*  	free(C_star->M[i]); */
/*       free(C_star->M); */
/*     } */
/*   free(C_star); */

/*   return gmic; */
/* } */

/* Returns the e Generalized Mean Information Coefficient (GMIC) */
t_float mine_gmic(mine_score *score, t_float p) {
  uint Z;
  mine_score *score_sub, *C_star = NULL;
  t_float gmic;
  // FIXME: wrt. benchmarks ... test different combinations fo the isTo_computeCorrelationsDuring_operation variable 
  const int isTo_computeCorrelationsDuring_operation = !(isTo_use_codeRewritingTo_reduceExeuctionTime());

  /* alloc score_sub */
  score_sub = (mine_score *) malloc (sizeof(mine_score));
  
  if(isTo_computeCorrelationsDuring_operation == false) {
    /* alloc C_star */
    C_star = (mine_score *) malloc (sizeof(mine_score));
    C_star->m = (int *) malloc(score->n * sizeof(int));
    C_star->M = (t_float **) malloc (score->n * sizeof(t_float *));
    /* if(isTo_use_continousSTripsOf_memory()) { */
    /*   int total_size = 0; */
    /*   for (uint i=0; i<score->n; i++) { */
    /* 	total_size += score->m[i]; */
    /*   } */
    /*   C_star->M[0] = (t_float *) malloc(total_size * sizeof(t_float)); */
    /*   assert(C_star->M[0]); */
    /*   uint offset_current = score->m[0]; */
    /*   for (uint i=1; i<score->n; i++) { */
    /* 	C_star->M[i] = C_star->M[0] + offset_current; */
    /* 	offset_current += score->m[i]; */
    /*   } */
    /* } else  */{
      for (uint i=0; i<score->n; i++) {
	C_star->M[i] = (t_float *) malloc ((score->m[i]) * sizeof(t_float));  
      }
    }
  }
  /* prepare score_sub */
  score_sub->M = score->M; //! ie, a 'shallow copy'.

  if(isTo_computeCorrelationsDuring_operation == false) {
    /* prepare C_star */
    C_star->n = score->n;
    for (uint i=0; i<C_star->n; i++) {
      C_star->m[i] = score->m[i]; //! ie, a 'shallow copy' of the poiner-references to "uint *m", ie, number of elemnets in score->M[][] 
    }
  }


  /* compute C_star */
  // FIXME: parallise.
  // FIXME: update [below] wrt. memory-allocaitons <-- first figure out how the C_star->M[i] is used.
  // FIXME: try to use ["elow] as a starting-poitn for improvment. ... seems as a simple 3d-matrix-operation
  // FIXME[parallel]: ... if ["elow] has a 'high comptuational cost', then use parallel comptuation.
  Z = 0;       gmic = 0.0; // FIXME: add code to 'merge these' after the parallel comptuation
  for (int i=0; i<score->n; i++) { 
    for (int j=0; j<score->m[i]; j++) { //! ie, for each cell score->M[i][j]
      const int B = (i+2) * (j+2);
      const int score_n = MAX((int) floor(B/2.0), 2) - 1; //! evaluate at least one cell.
      
      //! Get the max-score:
      t_float score_max = 0;

       
      if(isTo_use_codeRewritingTo_reduceExeuctionTime()) { //! then we reduce teh number of memory-allocations.
	// FIXME: evaluate the time-benefit of this approach. <-- 'major benefit' is expected to be wrt. the 'lack of need' for memory-allocation .. which is of expcial importance in parallel applicaitons. <-- seems like the imrpovement is small 
	assert(score_sub->M);
	assert(score_n <= score->n); //! ie, as we otherwise may be outside the allocated memory.
	// FIXME: identify the max number of iterations in ["elow] ... and thereafter esitamte the beneift of our parallisation-approach.
	for (int k=0; k < score_n; k++) {
	  assert(score_sub->M[k]);
	  //const uint score_m_k = (int) floor((t_float) B / (t_float) (k+2)) - 1;
	  // FIXME: validate correntcness of [below], where hte "min(..)" macro is used to ensure taht assert(score_m_k <= score->m[i]) 'holds'.
	  const int score_m_k = MIN(
				     (int) floor((t_float) B / (t_float) (k+2)) - 1,
				     score->m[i]);
	  //assert(score_m_k <= score->m[i]); //! ie, as we otherwise may be outside the allocated memory.
	  for (int j=0; j < score_m_k; j++) {
	    if (score_sub->M[k][j] > score_max) {
	      score_max = score_sub->M[k][j];
	    }
	  }
	}	
      } else {
	score_sub->n = score_n;
	// FIXME: after we have benchmarked the effect of muliple allcoations ... consider instea to allocate ["elow] only once.
	score_sub->m = (int *) malloc(score_sub->n * sizeof(int));
	for (int k=0; k<score_sub->n; k++) { //! then decrease linearly the row-size.
	  score_sub->m[k] = (int) floor((t_float) B / (t_float) (k+2)) - 1;
	}
	// FIXME: [below] call is a 2d-for-loop ... ie, ty to improve the memory-allcoation-scheme. <-- the function iterates through [score->n][score->m[i]] to find the max-score (of score->M[i][j]).
	score_max = mine_mic(score_sub);
	free(score_sub->m);
      }
      if(isTo_computeCorrelationsDuring_operation == true) {
	if (p == 0.0) {
	  gmic *= score_max;  Z++;
	} else {
	  gmic += pow(score_max, p);   Z++;
	}
      } else {
	//! Update the score:
	C_star->M[i][j] = score_max;
      }
    }
  }
  
  /* p=0 -> geometric mean */
  // FIXME: parallise. <-- consider to move to [above] ... 'merging' thread-seperate "gmic" and "Z" variables after the parallization
  if (p == 0.0) {
      if(isTo_computeCorrelationsDuring_operation == false) {
	Z = 0;       gmic = 1.0;
	for (int i=0; i<C_star->n; i++) {
	  for (int j=0; j<C_star->m[i]; j++) {
	    gmic *= C_star->M[i][j];  Z++;
	  }
	}
      }
      //! Then we compute
      gmic = pow(gmic, (t_float) Z);
  } else { /* p!=0 -> generalized mean */
    if(isTo_computeCorrelationsDuring_operation == false) {
      Z = 0;       gmic = 0.0;
      for (int i=0; i<C_star->n; i++) {
	for (int j=0; j<C_star->m[i]; j++) {
	  gmic += pow(C_star->M[i][j], p); 	  Z++;
	}
      }
    } 
    //! Then we compute
    gmic /= (t_float) Z;
    gmic = pow(gmic, 1.0/p);
  }

  //! De-allocate locally reserved memory:
  free(score_sub);
  if(isTo_computeCorrelationsDuring_operation == false) {
    if (C_star->n != 0) {
      free(C_star->m);
      /* if(isTo_use_continousSTripsOf_memory()) { */
      /*  	free(C_star->M[0]); */
      /* } else */ {
	for (int i=0; i<C_star->n; i++) {
	  free(C_star->M[i]);
	}
      }
      free(C_star->M);
    }
    free(C_star);
  }

  return gmic;
}



/* Returns the Total Information Coefficient (TIC). */
t_float mine_tic(mine_score *score)
{
  int i, j;
  t_float tic = 0.0;
  
  for (i=0; i<score->n; i++)
    for (j=0; j<score->m[i]; j++)
      tic += score->M[i][j];
  //printf("score=%f, at %s:%d\n", tic, __FILE__, __LINE__);
  return tic;
}


/* This function frees the memory used by the 
 * mine_score structure.
 */
void mine_free_score(mine_score **score)
{
  int i;
  mine_score *score_ptr = *score;

  if (score_ptr != NULL)
    {
      if (score_ptr->n != 0)
	{
	  free(score_ptr->m);
	  for (i=0; i<score_ptr->n; i++)
	    free(score_ptr->M[i]);
	  free(score_ptr->M);
	}
      
      free(score_ptr);
      score_ptr = NULL;
    }
}

