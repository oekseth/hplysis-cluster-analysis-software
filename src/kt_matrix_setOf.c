#include "kt_matrix_setOf.h"

//! Initiates the s_kt_matrix_setOf_t object  (oekseth, 06. jan. 2017).
s_kt_matrix_setOf_t initAndReturn__s_kt_matrix_setOf_t(const uint list_size, const uint nrows, const uint ncols) {
  s_kt_matrix_setOf_t self;
  if(list_size > 0) {    //! then we allocate the matrix:
    self.list_size = list_size;
    self.list = alloc_generic_type_1d_xmtMemset(s_kt_matrix_t, self.list, list_size);
    assert(self.list);
    if(nrows && ncols) {
      for(uint list_id = 0; list_id < list_size; list_id++) {
	init__s_kt_matrix(&(self.list[list_id]), nrows, ncols, /*isTo_allocateWeightColumns=*/false);
      }
    } else { //! then we set all matrices to empty:
      for(uint list_id = 0; list_id < list_size; list_id++) {
	setTo_empty__s_kt_matrix_t(&(self.list[list_id]));
      }
    }
  } else {
    self.list_size = 0;
    self.list = NULL;
  }
  //! @return
  return self;
}


//! De-allcoates the s_kt_matrix_setOf_t object (oekseth, 06. jan. 2017)
void free__s_kt_matrix_setOf_t(s_kt_matrix_setOf_t *self) {
  assert(self);  
  //! 
  //! De-allcoate:
  if(self->list) {
    assert(self->list_size > 0);
    for(uint i = 0; i < self->list_size; i++) {
      free__s_kt_matrix(&(self->list[i]));
    }
    free_generic_type_1d(self->list); self->list = NULL; self->list_size = 0;
  }
}



//! Initiates a given s_kt_matrix_t matrix  (oekseth, 06. jan. 2017).
bool setMatrixSize__s_kt_matrix_setOf_t(s_kt_matrix_setOf_t *self, const uint list_id, const uint nrows, const uint ncols) {
  assert(self); assert(list_id != UINT_MAX);
  if(list_id < self->list_size) {
    if(nrows && ncols) {
      init__s_kt_matrix(&(self->list[list_id]), nrows, ncols, /*isTo_allocateWeightColumns=*/false);
    } else {
      setTo_empty__s_kt_matrix_t(&(self->list[list_id]));
    }
    //! At this exec-point we asusme the optiaont was a success:
    return true;
  } else {return false;}
}
