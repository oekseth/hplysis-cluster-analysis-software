#ifndef e_kt_eigenVectorCentrality_h
#define e_kt_eigenVectorCentrality_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file e_kt_eigenVectorCentrality
   @brief identies the different types of enums wrt. eigen-vector centrality.
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
**/


/**
   @enum e_kt_eigenVectorCentrality
   @brief identies the different types of enums wrt. eigen-vector centrality.
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
**/
typedef enum e_kt_eigenVectorCentrality {
  e_kt_eigenVectorCentrality_eachValue_sumOf_scalar,
  e_kt_eigenVectorCentrality_eachValue_sumOf_log,
  e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow,
  e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue,
  //e_kt_eigenVectorCentrality_
  e_kt_eigenVectorCentrality_none, //! ie, then we asusem teh 'pwoer-metod' is Not to be applied.
  e_kt_eigenVectorCentrality_undef
} e_kt_eigenVectorCentrality_t;

//! Define a macro-presentaiton of the above enums:
#define __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar                 0
#define __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_log                    1
#define __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow   2
#define __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue   3


/**
   @enum e_kt_eigenVectorCentrality_postProcess
   @brief identies the different types of post-processing after each element has been 'summed' wrt. "e_kt_eigenVectorCentrality".
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
**/
typedef enum e_kt_eigenVectorCentrality_postProcess {
  e_kt_eigenVectorCentrality_postProcess_result,
  e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result,
  e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log,
  e_kt_eigenVectorCentrality_postProcess_log,
  e_kt_eigenVectorCentrality_postProcess_logPlussOne,
  e_kt_eigenVectorCentrality_postProcess_none,
  e_kt_eigenVectorCentrality_postProcess_undef
} e_kt_eigenVectorCentrality_postProcess_t;


//! Define a macro-presentaiton of the above enums:
#define __macro__e_kt_eigenVectorCentrality_postProcess_result              0
#define __macro__e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result  1
#define __macro__e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log  2
#define __macro__e_kt_eigenVectorCentrality_postProcess_log                 3
#define __macro__e_kt_eigenVectorCentrality_postProcess_logPlussOne         4
#define __macro__e_kt_eigenVectorCentrality_postProcess_none                5

/**
   @enum e_kt_eigenVectorCentrality_typeOf_weightUse
   @brief describes how the results of/from "e_kt_eigenVectorCentrality_t" and "e_kt_eigenVectorCentrality_postProcess_t" is to be applied/used.
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
**/
typedef enum e_kt_eigenVectorCentrality_typeOf_weightUse {
  e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum,
  e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply,
  e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog,
  /* e_kt_eigenVectorCentrality_typeOf_weightUse_divide, */
  /* e_kt_eigenVectorCentrality_typeOf_weightUse_pow_2, */
  /* e_kt_eigenVectorCentrality_typeOf_weightUse_pow_3, */
  e_kt_eigenVectorCentrality_typeOf_weightUse_none,
  e_kt_eigenVectorCentrality_typeOf_weightUse_undef
} e_kt_eigenVectorCentrality_typeOf_weightUse_t;


//! Define a macro-presentaiton of the above enums:
#define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum     1
#define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightMultiply 2
#define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_each_sum_multiply_and_weightLog 3
#define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_none                            4
/* #define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_divide   3 */
/* #define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_pow_2    4 */
/* #define __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_pow_3    5 */
// #define __macro__


/**
   @enum e_kt_centrality_typeOf_postAdjustment
   @brief descirbe post-processing operaitons to be sued to 'smooth' the enum-resutls, eg, wrt. 'reducing' skewness of 'otlayers'.
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016).
 **/
typedef enum e_kt_centrality_typeOf_postAdjustment {
  e_kt_centrality_typeOf_postAdjustment_meanOffset_sub,
  e_kt_centrality_typeOf_postAdjustment_meanOffset_sub_abs,
  e_kt_centrality_typeOf_postAdjustment_meanOffset_div,
  e_kt_centrality_typeOf_postAdjustment_log,
  e_kt_centrality_typeOf_postAdjustment_log_shannon,
  e_kt_centrality_typeOf_postAdjustment_none,
  e_kt_centrality_typeOf_postAdjustment_undef
} e_kt_centrality_typeOf_postAdjustment_t;

#endif //! EOF
