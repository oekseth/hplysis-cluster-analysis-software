#ifndef correlation_macros__distanceMeasures_wrapper_h
#define correlation_macros__distanceMeasures_wrapper_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlation_macros__distanceMeasures_wrapper.h
   @brief wrapper-functiosn for gneierc access to the distance-functions in our "correlation_macros__distanceMeasures" moduel/file.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/

#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance(val1, val2, weight, weight_index, typeOf_metric) ({ \
      t_float term = 0;							\
      /*if(typeOf_metric == e_kt_correlationFunction_spearman_or_correlation) { term = val1 * val2;}*/ \
      if(typeOf_metric == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) { term = correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly(val1, val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {term = correlation_macros__distanceMeasures__cityBlock(val1, val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { term = correlation_macros__distanceMeasures__euclid(val1, val2); } \
      else {assert(false);}						\
const t_float weight_local = (weight) ? weight[weight_index] : 1;		\
const t_float result_local = weight_local*term;				\
result_local;								\
})
#define __macro__get_distance_weight(val1, val2, weight, weight_index, typeOf_metric) ({ \
      t_float term = 0;							\
      if(typeOf_metric == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) { term = val1 * val2;} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {term = mathLib_float_abs(val1 - val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { term = val1 - val2; term = term*term; } \
      else {assert(false);}						\
      const t_float result_local = correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index); result_local;})
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_nonWeight(val1, val2, typeOf_metric) ({ \
      t_float term = 0;							\
      if(typeOf_metric == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) { term = val1 * val2;} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {term = mathLib_float_abs(val1 - val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { term = val1 - val2; term = term*term; } \
      else {assert(false);}						\
const t_float result_local = term;				\
result_local;								\
})
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD_notUseWeight(val1, val2, typeOf_metric) ({ \
  VECTOR_FLOAT_TYPE mul; \
  if(typeOf_metric == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) { mul = correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly__SSE(val1, val2); } \
  else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {mul = correlation_macros__distanceMeasures__cityBlock_SSE(val1, val2);} \
  else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { mul = correlation_macros__distanceMeasures__euclid_SSE(val1, val2); } \
  else {assert(false);}							\
  mul;}) //! ie, return the result.
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD(val1, val2, weight, weight_index, typeOf_metric) ({ \
      VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD_notUseWeight(val1, val2, typeOf_metric); \
      /*FIXME: validate correctness of using VECTOR_FLOAT_SET(..) and Not VECTOR_FLOAT_SET1(..) in [”elow] */ \
      if(weight) {mul = VECTOR_FLOAT_MUL(mul, VECTOR_FLOAT_SET(weight[weight_index]));} \
      mul;								\
    })
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD_useWeights(val1, val2, vec_weight, typeOf_metric) ({ \
  VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD_notUseWeight(val1, val2, typeOf_metric); \
  mul = VECTOR_FLOAT_MUL(mul, vec_weight);				\
mul;								\
})
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD_notuseWeights(val1, val2, typeOf_metric) ({ \
  VECTOR_FLOAT_TYPE mul; \
      if(typeOf_metric == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) { mul = VECTOR_FLOAT_MUL(term1, term2); } \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {mul = VECTOR_FLOAT_SUB(term1, term2); mul = VECTOR_FLOAT_MAX(VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET_zero(), mul), mul); ;} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { mul = VECTOR_FLOAT_SUB(term1, term2); mul = VECTOR_FLOAT_MUL(mul, mul); } \
      }									\
      else {assert(false);}						\
mul;								\
})
// *********************************************************************************
#else
#define __macro__get_distance(val1, val2, weight, weight_index, typeOf_metric) ({ \
      t_float term = 0;							\
      /*if(typeOf_metric == e_kt_correlationFunction_spearman_or_correlation) { term = val1 * val2;}*/ \
      /*if(typeOf_metric == e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly) { term = correlation_macros__distanceMeasures__spearman_or_correlation_sumOnly(val1, val2);} else */ \
      if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {term = correlation_macros__distanceMeasures__cityBlock(val1, val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { term = correlation_macros__distanceMeasures__euclid(val1, val2); } \
      else {assert(false);}						\
const t_float weight_local = (weight) ? weight[weight_index] : 1;		\
 const t_float result_local = weight_local*term;			\
result_local;								\
    })
#define __macro__get_distance_weight(val1, val2, weight, weight_index, typeOf_metric) ({ \
      t_float term = 0;							\
       if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {term = mathLib_float_abs(val1 - val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { term = val1 - val2; term = term*term; } \
      else {assert(false);}						\
      const t_float result_local = correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index); result_local;})
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_nonWeight(val1, val2, typeOf_metric) ({ \
      t_float term = 0;							\
      if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {term = mathLib_float_abs(val1 - val2);} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { term = val1 - val2; term = term*term; } \
      else {assert(false);}						\
const t_float result_local = term;				\
result_local;								\
})
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD_notUseWeight(val1, val2, typeOf_metric) ({ \
  VECTOR_FLOAT_TYPE mul; \
   if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {mul = correlation_macros__distanceMeasures__cityBlock_SSE(val1, val2);} \
  else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { mul = correlation_macros__distanceMeasures__euclid_SSE(val1, val2); } \
  else {assert(false);}							\
  mul;}) //! ie, return the result.
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD(val1, val2, weight, weight_index, typeOf_metric) ({ \
      VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD_notUseWeight(val1, val2, typeOf_metric); \
      /*FIXME: validate correctness of using VECTOR_FLOAT_SET(..) and Not VECTOR_FLOAT_SET1(..) in [”elow] */ \
      if(weight) {mul = VECTOR_FLOAT_MUL(mul, VECTOR_FLOAT_SET(weight[weight_index]));} \
      mul;								\
    })
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD_useWeights(val1, val2, vec_weight, typeOf_metric) ({ \
  VECTOR_FLOAT_TYPE mul = __macro__get_distance_SIMD_notUseWeight(val1, val2, typeOf_metric); \
  mul = VECTOR_FLOAT_MUL(mul, vec_weight);				\
mul;								\
})
//! @return the distances assicated to "e_kt_correlationFunction_t"
#define __macro__get_distance_SIMD_notuseWeights(val1, val2, typeOf_metric) ({ \
  VECTOR_FLOAT_TYPE mul; \
      if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_cityblock) {mul = VECTOR_FLOAT_SUB(term1, term2); mul = VECTOR_FLOAT_MAX(VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET_zero(), mul), mul); ;} \
      else if(typeOf_metric == e_kt_correlationFunction_groupOf_minkowski_euclid) { mul = VECTOR_FLOAT_SUB(term1, term2); mul = VECTOR_FLOAT_MUL(mul, mul); } \
      }									\
      else {assert(false);}						\
mul;								\
})

#endif




#endif
