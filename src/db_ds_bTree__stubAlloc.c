  __Mi__localType *newnode = NULL;   __Mi__localType__wrapper upKey;
const enum e_db_cmpResult value = __MiF__search__recursive(*root, key, &upKey, &newnode, result_set);
  if (value == e_db_cmpResult_InsertIt) {
    __Mi__localType *uproot = *root;
    *root = (__Mi__localType*)malloc(sizeof(__Mi__localType));
    (*root)->n = 1; //! ie, as the 'enw node' at 'init' has only one key'.
    (*root)->key[0] = upKey; //! the 'index' of the .... 
    (*root)->pointers[0] = uproot; //! the 'old' root.
    (*root)->pointers[1] = newnode; //! ie, the 'node-refrence' assicated to 'key'
  }/*End of if */
