#ifndef mask_api_h
#define mask_api_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file mask_api
   @brief provide logics for mask application and inferences (ie, strategies to 'hide')
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/


#include "mask_base.h"

/**
   @enum e_mask_api_config_typeOf_emptyValue
   @brief specify the type of empty valeu to be sued when setting the masks.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/
typedef enum e_mask_api_config_typeOf_emptyValue {
  e_mask_api_config_typeOf_emptyValue_0, //! which may boost the perfomrance of some distnace-meausrew chile reduce the accyrasy of other distance-measures, ie, should be used 'with conciusnness'.
  e_mask_api_config_typeOf_emptyValue_T_FLOAT_MAX
} e_mask_api_config_typeOf_emptyValue_t;

/**
   @enum e_mask_api_config_typeOf_SSE
   @brief specifies the type of SSE-optmizaiton to be used
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remakrs is the enum if of specific itnerest wrt. evalaution of different code-optmizaiton-tehcneies, eg, when comparring different comptuer-hardwares support/facialtition for comptuation of corrleaiton-emtrics, where the correlatiobn-metrics may be seen as the main-bottleneck in clustering of complex data-sets, eg, for clustering throug "Self Organisgn Maps" (SOMs) and "k-means clsutering"
 **/
typedef enum e_mask_api_config_typeOf_SSE {
  e_mask_api_config_typeOf_SSE_fast,
  // e_mask_api_config_typeOf_SSE_slower,
  e_mask_api_config_typeOf_SSE_undef //! which if used implies that SSE is not used/utlized.
} e_mask_api_config_typeOf_SSE_t;


/**
   @enum e_mask_api_valueRangeClassification
   @brief idnetifies the value-range classifciaotn of a given sub-data-set (eg, for a givne matrix) (oekseth, 06. otk. 2016).
 **/
typedef enum e_mask_api_valueRangeClassification {
  e_mask_api_valueRangeClassification_char_signed,
  e_mask_api_valueRangeClassification_char_unsigned,
  e_mask_api_valueRangeClassification_16_bit_signed,
  e_mask_api_valueRangeClassification_16_bit_unsigned,
  e_mask_api_valueRangeClassification_32_bit_signed,
  e_mask_api_valueRangeClassification_32_bit_unsigned,
  e_mask_api_valueRangeClassification_64_bit_signed,
  e_mask_api_valueRangeClassification_64_bit_unsigned,
  e_mask_api_valueRangeClassification_float,
  e_mask_api_valueRangeClassification_undef
} e_mask_api_valueRangeClassification_t;

/**
   @struct s_mask_api_config
   @brief provide configruations wrt. mask-filtering.
   @remarks the cofngiruatiosn are conserned with both interpretaiton and configuration
   @remarks if score-sthresholds are Not of itnerest, then set the scoreTrheashold_min to the same value as scoreTrheashold_max, eg, to set both parameters to T_FLOAT_MAX
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks if score-sthresholds are Not of itnerest, then set the scoreTrheashold_min to the same value as scoreTrheashold_max, eg, to set both parameters to T_FLOAT_MAX
 **/
typedef struct s_mask_api_config {
  //! 
  //! Configruations wrt. data-interpretaiton:
  t_float scoreThreshold_min;  t_float scoreThreshold_max;
  e_mask_api_config_typeOf_emptyValue_t typeOf_implictEmptyValue_toSet; //! which is used to 'know' the implict value to set, eg, "0" or T_FLOAT_MAX
  //!
  //! Configruations wrt. SSE time-performance:
  e_mask_api_config_typeOf_SSE_t typeOf_optimization_SSE; //! which if used describes the type of SSE-optmizaiton to use.

  //!
  //! Local configurations:
  bool localConfig_allocateMatrix;
} s_mask_api_config_t;

//! Set the s_mask_api_config_t object to default values.
void setTo_empty__s_mask_api_config(s_mask_api_config_t *self);

//! Explictly update the values in data with emptyValue_toSet for each valeu set to T_FLOAT_MAX
void replace_T_FLOAT_MAX_withNewEmptyValue__mask_api(t_float **data, const uint nrows, const uint ncols, const t_float emptyValue_toSet, const s_mask_api_config_t self); // __attribute__((always_inline));

//! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then return an updated matrix (where implcit amkss are sued), ie,  for cases where "mask[row_id][column_id] == 0".
//! @return a enw-allcoated matrix with implcit masks (ie, if any).
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss, implict maksses expected to be set to T_FLOAT_MAX
t_float **applyImplicitMask__constructNewMatrix__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, const s_mask_api_config_t self);
//! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then update "data" with the "T_FLOAT_MAX" for cases where "mask[row_id][column_id] == 0"
void applyImplicitMask__updateData__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, s_mask_api_config_t self);
//! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then update "data" with the "T_FLOAT_MAX" for cases where "mask[row_id][column_id] == 0"
void applyImplicitMask_fromUintMask__updateData__mask_api(t_float **data, uint **mask, const uint nrows, const uint ncols, s_mask_api_config_t self);
//! 'Translates' a "uint **" boolean mask into a a char-matrix.
//! @remakrs included iot. to be back-compatible with 'old' "cluster.c" library.
char **convert_booleanMatrix_toCharMatrix__mask_api(const uint nrows, const uint ncolumns, uint **mask, const bool isTo_use_continousSTripsOf_memory);



//! Identifeis the min-max valeus in a matrix, an evaluation which 'dismisses' the evlauation of mask-values.
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss: implcit makss are assuemd to 'conform' to the speciaioant in the "self" s_mask_api_config_t input-parameter-object.
//! @remarks in this funciton we make use of the "self->typeOf_implictEmptyValue_toSet" to identify the 'valeus to ignroe' if "scoreThreshold_min == scoreThreshold_max" and if "mask == NULL"
//! @remarks set scalar_max and scalar_min to T_FLOAT_MAX if no valeus are idnietifed wrt. the user-deifned 'search-trehsholds'.
void get_minMax__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, t_float *scalar_min, t_float *scalar_max,  bool *scalar_hasDecimals, const s_mask_api_config_t self);

/**
   @brief idnetifies the valeu-range of the data matrix (oekseth, 06. otk. 2016).
   @return an enum-representation of the vlaue-range
 **/
const e_mask_api_valueRangeClassification_t get_valueRange_forMatrix__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, const s_mask_api_config_t self);

//! Identifeis the min-max valeus in a matrix, an evaluation which 'dismisses' the evlauation of mask-values.
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss: implcit makss are assuemd to 'conform' to the speciaioant in the "self" s_mask_api_config_t input-parameter-object.
//! @remarks in this funciton we make use of the "self.typeOf_implictEmptyValue_toSet" to identify the 'valeus to ignroe' if "scoreThreshold_min == scoreThreshold_max" and if "mask == NULL"
//! @remarks set scalar_max and scalar_min to T_FLOAT_MAX if no valeus are idnietifed wrt. the user-deifned 'search-trehsholds'.
void get_minMax__allValuesOf_interest_mask_api(t_float **data, const uint nrows, const uint ncols, t_float *scalar_min, t_float *scalar_max, s_mask_api_config_t self);

//! @breif cosntruct a '0|1' matrix from the input-data
//! @return a binary eitehr-or matrix based on the input-data
t_float **construct_binaryMatrix__mask_api(t_float **data, const uint nrows, const uint ncols);
//! @breif cosntruct a '0|1' matrix from the input-data
//! @return a binary eitehr-or matrix based on the input-data
t_float **construct_binaryRow__atIndex__mask_api(t_float **data, const uint index1, const uint nrows, const uint ncols, const bool isTo_storeResult_atIndex0);
//! @breif cosntruct a '0|1' row from the input-data
//! @return a binary eitehr-or matrix based on the input-data
t_float *construct_binaryRow__mask_api(const t_float *row, const uint ncols);
//! @breif cosntruct a '0|1' row from the input-data
void construct_binaryRow__updateDirectly__mask_api(t_float *row, const uint ncols);

#endif //! EOF
