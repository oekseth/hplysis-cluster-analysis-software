#ifndef hp_entropy_h
#define hp_entropy_h
//#include "def_intri.h"
#include "kt_matrix.h"
#include "kt_entropy.h"
#include "kt_matrix_base_uint.h"
#include "kt_list_2d.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @brief Compute enotrpy (oekseth, 06. jul. 2017).
   @param <list_count> is the vector holdignt eh inptu-data to copmtue the entropy for.
   @param <max_theoreticCountSize> is the max-possbile count-sum whihc may be encountered in the inptu-data.
   @param <enum_entropy> is the entopry-type be intiated for, eg, wrt. the "a" parameter used in Dirichlet-entropy-emtics.
   @param <isTo_applyPostScaling> hich if set to true impleis taht we adjsut the result by a log-nroamizaiotn.
   @param <scalar_result> is used to hold the result of the cotmpatuion.
 **/
void compute_entropy__list__hp_entropy(const s_kt_list_1d_uint_t *list_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, t_float *scalar_result);
/**
   @brief Compute enotrpy (oekseth, 06. jul. 2017).
   @param <matrix_count> is the matrix of vectors holdignt eh inptu-data to copmtue the entropy for.
   @param <max_theoreticCountSize> is the max-possbile count-sum whihc may be encountered in the inptu-data.
   @param <enum_entropy> is the entopry-type be intiated for, eg, wrt. the "a" parameter used in Dirichlet-entropy-emtics.
   @param <isTo_applyPostScaling> hich if set to true impleis taht we adjsut the result by a log-nroamizaiotn.
   @param <list_result> is used to hold the result of the cotmpatuion.
 **/
void compute_entropy__matrix__hp_entropy(const s_kt_matrix_base_uint_t *mat_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_list_1d_float_t *list_result);

/**
   @brief Compute enotrpy-frequency (oekseth, 06. jul. 2017).
   @param <mat_count> is the vector holdignt eh inptu-data to copmtue the entropy for.
   @param <max_theoreticCountSize> is the max-possbile count-sum whihc may be encountered in the inptu-data.
   @param <enum_entropy> is the entopry-type be intiated for, eg, wrt. the "a" parameter used in Dirichlet-entropy-emtics.
   @param <isTo_applyPostScaling> hich if set to true impleis taht we adjsut the result by a log-nroamizaiotn.
   @param <list_result> is used to hold the result of the cotmpatuion.
 **/
void compute_freq__list__hp_entropy(const s_kt_list_1d_uint_t *list_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_list_1d_float_t *list_result);
/**
   @brief Compute enotrpy-frequency (oekseth, 06. jul. 2017).
   @param <mat_count> is the matrix of vectors holdignt eh inptu-data to copmtue the entropy for.
   @param <max_theoreticCountSize> is the max-possbile count-sum whihc may be encountered in the inptu-data.
   @param <enum_entropy> is the entopry-type be intiated for, eg, wrt. the "a" parameter used in Dirichlet-entropy-emtics.
   @param <isTo_applyPostScaling> hich if set to true impleis taht we adjsut the result by a log-nroamizaiotn.
   @param <mat_result> is used to hold the result of the cotmpatuion.
 **/
void compute_freq__matrix__hp_entropy(const s_kt_matrix_base_uint_t *mat_count, uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_matrix_base_t *mat_result);

/**
   @struct s_hp_entropy_fileReadConf
   @brief provide a gneric cofnigguriaotn-aprpaoch to reaidng data-files (oekseth, 06. feb. 2017).
   @remarks is 
 **/
typedef struct s_hp_entropy_fileReadConf {
  const char *input_file;
  const char *result_file;
  char config_file_sep;
  char config_file_sep_newLine;
  //! --------
  bool config_rows__allHaveSameLength;
  bool config_rows__ignoreFirstRow;
  bool config_rows__ingoreFirstColumn; //! which si to be set ot true if the first column in each row describes a stirng-idntifer.   
  bool isTo_exportForEach;
  t_float result_summary_sum;
  uint result_summary_sum_cntRows;
} s_hp_entropy_fileReadConf_t;

/**
   @param <input_file> is the input-file to laod.
   @param <result_file>  optional: iof set then we expor thte reuslt to the file in quesiton.
   @param <config_col_sep> is the sperator between the columsn, eg, ',' or '\t'.
 **/
s_hp_entropy_fileReadConf_t init__s_kt_list_fileReadConf_t(const char *input_file, const char *result_file, const char config_col_sep);

/**
   @brief comptues entopry for an input-file (oekseth, 06. jul. 2017).
   @param <file_config> how the file is to be parsed and how the result is to be sored/exported.
   @param <max_theoreticCountSize> is the max-count in the glboal inptu-data eg, wrt. the max-gene-expresiosn-value.
   @param <enum_entropy> is the tyep fo entopry to apply.
   @param <isTo_applyPostScaling> which if set to true impleis that the result is scaled wr.t the loaightm.
   @param <mat_result> optional_ if set then we use this strucutre to store the result.
 **/
void fromFile__compute_entropy__matrix__hp_entropy(s_hp_entropy_fileReadConf_t *file_config, const uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_matrix_base_t *mat_result);
/**
   @brief comptues entopry-frequency for an input-file (oekseth, 06. jul. 2017).
   @param <file_config> how the file is to be parsed and how the result is to be sored/exported.
   @param <max_theoreticCountSize> is the amx-count in the glboal inptu-data eg, wrt. the max-gene-expresiosn-value.
   @param <enum_entropy> is the tyep fo entopry to apply.
   @param <isTo_applyPostScaling> which if set to true impleis that the result is scaled wr.t the loaightm.
   @param <mat_result> optional: if set then we use this strucutre to store the result.
 **/
void fromFile__compute_freq__matrix__hp_entropy(s_hp_entropy_fileReadConf_t *file_config, const uint max_theoreticCountSize, const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, s_kt_matrix_base_t *mat_result);

/**
   @brief comptue entropy for all float-scores in a sparse list of values (oekseth, 06. sept. 2017). 
   @param <enum_entropy> is the tyep fo entopry to apply.
   @param <isTo_applyPostScaling> which if set to true impleis that the result is scaled wr.t the loaightm.
   @param <obj_sparse> is the sparse lsit of scores to copute enotpry for. 
   @param <scalar_result> hold the comptued entopry of the comptaution.
 **/
static void fromFloats__slow__hp_entropy(const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, const s_kt_list_1d_float_t *obj_scores, t_float *scalar_result) {
  assert(obj_scores); assert(scalar_result); *scalar_result = T_FLOAT_MAX;
  //!
  //! Call:
  uint cnt_max = 0;
  for(uint i = 0; i < obj_scores->list_size; i++) {
    if(isOf_interest(obj_scores->list[i])) {cnt_max = i+1;}
  }
  if(cnt_max != 0) {
    //! 
    //! Initiate: 
    s_kt_entropy_t obj_enum = initSimple__s_kt_entropy(/*max_theoreticCountSize=*/UINT_MAX, /*vector-length=*/cnt_max, enum_entropy);
    obj_enum.config__useNaiveLogComputationAppraoch = true;
    kt_entropy__vector_entropy__slowFloats(&obj_enum, obj_scores->list, cnt_max, isTo_applyPostScaling, scalar_result);
    //    printf("(entropy-chunk)\t score=%f given size=%u at %s:%d\n", *scalar_result, cnt_max, __FILE__, __LINE__);
    //! De-allocate:
    free__s_kt_entropy(&obj_enum);
  }
}
/**
   @brief comptue entropy for all float-scores in a sparse list of values (oekseth, 06. sept. 2017). 
   @param <enum_entropy> is the tyep fo entopry to apply.
   @param <isTo_applyPostScaling> which if set to true impleis that the result is scaled wr.t the loaightm.
   @param <obj_sparse> is the sparse lsit of scores to copute enotpry for. 
   @param <scalar_result> hold the comptued entopry of the comptaution.
 **/
static void from2dSparseList__slow__hp_entropy(const e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, const s_kt_list_2d_kvPair_t *obj_sparse, t_float *scalar_result) {
  assert(scalar_result);
  assert(obj_sparse);
  s_kt_list_1d_float_t obj_scores = translate_toFloats__s_kt_list_2d_kvPair_t(obj_sparse);
  if(obj_scores.list_size == 0) {*scalar_result = T_FLOAT_MAX;} //! ie, as no reuslts were comptued.
  else {
    fromFloats__slow__hp_entropy(enum_entropy, isTo_applyPostScaling, &obj_scores, scalar_result);
    //!
    //! De-allocate: 
    free__s_kt_list_1d_float_t(&obj_scores);
  }
  // free_1d_list_float(&arr_scores); arr_scores = NULL; arr_scores_size = 0;
}

#endif //! EOF.
