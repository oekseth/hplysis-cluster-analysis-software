{ //! The function for perofmrance-testing:

  //! Note: we expect the "stringOf_measureText" to be set in the caller.

  /* const uint arrOf_cnt_buckets_size = 7; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40, */
  /* 												  80, 160 */
  /* 												  //, 320, 740, 1000 */
  /* }; */

/* #ifndef __macro_funcName_toCall */
/* #error "!!\t The funciton-anem to call is not specirfied, ie, please investigate you call, eg, wrt. \"kt_compute_eigenVector_centrality(..)\" or an itnernal fucntion-name" */
/* #endif */

  printf("\n-----------\t start-block:: for tag=\"%s\", at %s:%d\n", stringOf_measureText, __FILE__, __LINE__);

  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
    { 

      //! Allcoate an itniate memory
      const t_float default_value_float = 0;
      t_float *arrOf_result = allocate_1d_list_float(size_of_array, default_value_float);
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      for(uint i = 0; i < size_of_array; i++) {
	for(uint out = 0; out < size_of_array; out++) {
	  matrix[i][out] = 1/(out+2) + (out+1)/(i+1); //! ie, itnaite.
	}
      }
      //!
      //! The experiemnt:
      start_time_measurement();  //! ie, start measurement



      /*   t_float bothtails = 0; */
      /*   t_float lefttails = 0;  */
      /*   t_float righttails = 0; */
      /*   //t_float score_possible = size_of_array/(1+i); */
      /*   const t_float score_possible = 0.010; 	const uint ncols = 15000;  */
      //! Compute:
      //__macro_funcName_toCall(matrix, size_of_array, size_of_array, arrOf_result, self);
      kt_compute_eigenVector_centrality(matrix, size_of_array, size_of_array, arrOf_result, self);
	  
      /*   //! Update: */
      /*   result += righttails; */
	  
      /*   ; //result += __get_artimetnic_results(i, i); */
      /* } */
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
      { //! write out the sum opf valeus, ie, to avoid an 'optmized compaition' from 'removing' the call
	t_float sum = 0; for(uint i = 0; i < size_of_array; i++) { sum += arrOf_result[i]; } 
	printf("info:sum\t sum=%.2f, at %s:%d\n", sum, __FILE__, __LINE__);
      }

      //! De-allcoate lcoally reserved memory:
      free_1d_list_float(&arrOf_result); 	free_2d_list_float(&matrix, size_of_array);

    }
  }
}
