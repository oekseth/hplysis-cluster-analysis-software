#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
// ----------------
#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "kt_list_1d.h"
#include "hp_distance.h"
// ***********************************************************
#include "kt_storage_resultOfClustering.h" //!  lgocis for summarizing core-attributes of the data, explroed across different sim-metrics
//#include "kt_distributionTypesC.h" //! which simplfies access to distrubion-types.
// ***********************************************************
#include "hp_frameWork_CCM_boundaries.h" // FIXME: update our aritlce with this.


/**
   @brief caputre similiarty of CCMS wrt. the cases of 'cluster-predications are equal' versus 'cluster-rpedictions are Not equal'.
   @author Ole Kristian Ekseth (oekseth, 26. Dec. 2018).
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
   @remarks compile:
   -- g++ -O2 -g tut_hp_frameWork_CCM_boundaries-2.h -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -std=c++11 -o  tut_16.exe
   @remarks Explroe research-qeussiotsn of:
   -- are there any cases where "Rand's Index" identifies a CCM-score (for the tested topology)?
   -- real-life examples of when different clusters should be seen 'as the same'?
   @reamrks 
   --  idea (of this evaluation-algorithm-pipeline): idneitfy cases where the CCM-growth-factor changes substaintlly (which if true would stronly inlfuence clsuter-repioncts (Eq. \ref{}))  .... code-permtautiopn. compute for a single CCM (eg, DBI)  <-- udpate framwork by ...??...  ... use Kendall's Tau to idneitfy the most dissibimilar metrics  ... plto the dissimilarity using a rank-base searhd (both for the  scores, and the invrerted scores) ... plot the different results  using Krusla'ls MST <-- % Note: insted we select the 't' best scores ... ie, to reduce the memory-footprint of this appraoch ... and to make the aprpaoch mroe dynamic ... eg, to allow/enalbe userrs to specy abirayt number of hytpeosis to explore ... hower, groups bettween the extrem edges might be as interesting (as they seperate from the others) ... this argues for includign all the ... and then onstruct/depcit/nalayse the clsters (of scores) which form ... 
**/
int main() 
#else
  static void apply_tut_framwork_2_compareMatrixAndGold_twoVectors(const char *filePrefix) //, const uint config_cnt_casesToEvaluate, const uint config_nrows_base, const uint config_clusterBase, const bool isTo_useRverseOrder_inInit, const bool isTo_setClusterSzei_toFixed_sizes, const t_float score_weak)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const char *filePrefix = "r-16";
  //  const char *filePrefix = "ccm_16_compareMatrixAndGold_twoVectors";
#endif
  



  
  const uint map_cluster_size = 15;
  const uint map_cluster[map_cluster_size] = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30};
  //if(false)  { const uint cluster_id = 0; // FIXME: remove, and inlcude bleow
  for(uint cluster_id = 0; cluster_id < map_cluster_size; cluster_id++) {
    const uint cnt_clusters = map_cluster[cluster_id];
    // ---
    const char *filePrefix_base = "r-hp_frameWork_Case2-CCM_boundaries_helloWorldCaseTest-";  //! which is sued if tempraoryb data are exproeted, eg, PPM-images.
    allocOnStack__char__sprintf(2000, filePrefix, "%s-c%u-", filePrefix_base, cnt_clusters);
    //yconst char *file_name_result = "r-hp_frameWork_CCM_boundaries_helloWorldCaseTest.tsv";
    allocOnStack__char__sprintf(2000, file_name_result, "%ssummary.tsv", filePrefix);
    s_eval_ccm_defineSyntMatrix_comparisonBasis_t self_result = init_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(file_name_result);
    self_result.ccm_enum = e_kt_matrix_cmpCluster_metric_randsIndex; //! ie, the default choise.
    //
    const uint cnt_data_iterations = 20; //! where this attribute needs to be fixed when analsysing hytpsis, ie, to simplify the engerration of tailore-made hytpsos to compare with.
   const bool isTo_exportDimMatrix = true;  
    //! -----------------------------------------------------
    //! 
    { //! Define Hyptoisis-conditions:
      //! Note: the below boudnaries are the result of ane arlier copmtuation; to maise the sepration between the cases, we (in below/this configuraiton) makes sue of the three wrost scores, and trhee best scores, observed/gaind forr the emtric qin question:
      //! Note: reprsentivness of the results ... 
      const uint arr_sound_size = 2;
      t_float arr_sound[arr_sound_size][2] = {{-1.020300     ,  -0.906805},  {-0.906805       -0.793303}};
      //!
      //! Set the scores:
      for(uint k = 0; k < arr_sound_size; k++) {
	set_scalar__s_kt_list_1d_tripleT_Float_t(&(self_result.list_range[e_eval_ccm_booleanSensitivity_Posititve]), /*index=*/k, MF__initVal__s_ktType_tripleT_Float(arr_sound[k][0], arr_sound[k][1], /*isTo_print=*/0) );
      }
      const uint arr_poor_size = 2;
      t_float arr_poor[arr_poor_size][2] = {{0.795700      ,  0.909196}, {0.909196,        1.000000}}; 
      //!
      //! Set the scores:
      for(uint k = 0; k < arr_poor_size; k++) {
	set_scalar__s_kt_list_1d_tripleT_Float_t(&(self_result.list_range[e_eval_ccm_booleanSensitivity_Negative]), /*index=*/k, MF__initVal__s_ktType_tripleT_Float(arr_poor[k][0], arr_poor[k][1], /*isTo_print=*/0) );
      }      
    }
    //! -----------------------------------------------------
    //! 
    { //! Define different cruve-shaped hyptosis:     
      //! ------------------------------------
      if(true) { //! then we define a chunk of different hypothesis; note: an iterative
	//! Note: an alternative is to provide data from external files, eg, to capture attributes different real-life data-sets.
	assert(self_result.mat_linearOrderOfCCMScores_hypo.nrows == 0); //! as we expect this to be empty.
	//!
	//! Set the hypothesis:
	const uint hypo_cnt = 3;
	const char *str_hypo[hypo_cnt] = {"linear", "reverse", "bell-shaped"};
	self_result.mat_linearOrderOfCCMScores_hypo = initAndReturn__s_kt_matrix(/*nrows=*/hypo_cnt, /*ncols=*/cnt_data_iterations);
	//!
	//! Set the data:
	for(uint row_id = 0; row_id < self_result.mat_linearOrderOfCCMScores_hypo.nrows; row_id++) {
	  //! Set: row-name
	  set_stringConst__s_kt_matrix(&(self_result.mat_linearOrderOfCCMScores_hypo), /*row_id=*/row_id, str_hypo[row_id], /*addFor_column=*/false);
	  t_float score_inc = 0.0;
	  for(uint col_id = 0; col_id < self_result.mat_linearOrderOfCCMScores_hypo.ncols; col_id++) {
	    //! Set: column-name
	    if(row_id == 0) {
	      allocOnStack__char__sprintf(2000, str_local, "%s-%u", "index", col_id);
	      set_stringConst__s_kt_matrix(&(self_result.mat_linearOrderOfCCMScores_hypo), /*row_id=*/col_id, str_local, /*addFor_column=*/true);
	    }
	    //! Set: score:
	    if(row_id == 0) { self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = (t_float)col_id; }
	    else if(row_id == 1) { self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = (t_float)(cnt_data_iterations - col_id); }
	    else if(row_id == 2) { //! then a bell-shaped-curve is cosntruted
	      if(col_id < (uint)( (t_float) self_result.mat_linearOrderOfCCMScores_hypo.ncols / 2) ) {
		score_inc -= 1.0;
	      } else {
		score_inc += 1.0;
	      }
	      self_result.mat_linearOrderOfCCMScores_hypo.matrix[row_id][col_id] = score_inc;
	    } else { assert(false); } //! ie, then add supprot for this
	  }
	}
      }
    }
    //! -----------------------------------------------------
    //!     
    { //! Explore data-shapes:
      //!      
      //! Note: based on our other/related measurements, we have that below configurations should results in a high separation between CCMs (hence, the representative's of below configuration):
      const uint nrows = 100;
      //! Apply logcs:
      const bool isTo_exportArtificalData = false;
      const bool apply_postClustering = false;
      const uint weights_map_size = 5;
      const uint weights_map[weights_map_size][2] = {
	{100,1},{100, 25},{100, 50}, 
	{100, 99}
	, {1000, 99}
      }; //! where the ",{100, 95},{95, 100}" is used to explore the effects of cases where there is hardly any separation, as seen for cases where Eculdidena simlairty faind it challengign to handle otuliers (as exemplfied in resutls generate from our "tut-2" results).
    //!
      //! ------------------------------
      //!
      //! Apply:
      { //! Compute for: 'positive-cases':
	//! 
	for(uint weight_index = 0; weight_index < weights_map_size; weight_index++) {
	  const t_float score_weak   = weights_map[weight_index][0];
	  const t_float score_strong = weights_map[weight_index][1];
	  //!
	  assert(cnt_clusters < nrows);
	  //! 	  
	  //! Configure:
	  self_result.eval_case = e_eval_ccm_booleanSensitivity_Posititve;
	  allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix, "case-Positive", "linearTestCase", nrows, cnt_clusters, (uint)score_weak, (uint)score_strong, cnt_data_iterations);
	  s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters);
	  //! ------------------------------
	  if(false) {
	    clear_ArtifcicalData_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result);} //! which is used as a safe-guard (both wrt. future code-changes, and if/when this code-chunk is copy-pasted).
	  //! 
	  //! Call:
	  apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_artiFicialData, cnt_data_iterations, /*isTo_apply_postClustering=*/false, &self_result, isTo_exportDimMatrix);
	}
      }
      // ---------
      { //! Compute for: 'negative-cases': doubles the number of clusters; motivaiton of this is to 
	// FIXME ... (conceptual) ... we assuem that the weight-difference shoudl NOT inlfuence the overrall predicciton-acuracy ... ie, as the 'weights' ollowins the same distrubions. Hence, ... 
	// FIXME ... (conceptual) ... 		
	uint cnt_clusters_local = cnt_clusters * 2;
	if(cnt_clusters_local > nrows) {cnt_clusters_local = cnt_clusters;}
	//! 
	//! Configure:
	for(uint weight_index = 0; weight_index < weights_map_size; weight_index++) {
	  const t_float score_weak   = weights_map[weight_index][0];
	  const t_float score_strong = weights_map[weight_index][1];
	  //! 
	  self_result.eval_case = e_eval_ccm_booleanSensitivity_Negative;
	  allocOnStack__char__sprintf(2000, filePrefix_local, "%s-%s-%s-n%u-c%u-f%ut%u-d%u", filePrefix, "case-Negative", "linearTestCase", nrows, cnt_clusters_local, (uint)score_weak, (uint)score_strong, cnt_data_iterations);
	  s_eval_ccm_defineSyntMatrix_t self_artiFicialData = init_s_eval_ccm_defineSyntMatrix_t(filePrefix_local, nrows, score_weak, score_strong, cnt_clusters_local);
	  //! 
	  apply_comparison_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_artiFicialData, cnt_data_iterations, /*isTo_apply_postClustering=*/false, &self_result, isTo_exportDimMatrix);
	}
      }
    }
    //!
    //! Export result-set, and then de-allocte:
    free_s_eval_ccm_defineSyntMatrix_comparisonBasis_t(&self_result);
  }


  // -------------------------------------------------------------------
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
  
