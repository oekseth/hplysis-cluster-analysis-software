  const uint fraction_colNoise = 0;
  const uint nrows = nrows_base*mult_factor*numberOf_matrices_stepFactor;       
  const uint ncols = nrows;  //! ie, to 'allow' the use of "_directScore_" corlreaiotn-emtirc in our evlauation.

assert(nrows >= 10); //! ie, to have a 'meanigful' comaprsin
uint groupId = 0;

  //!
  //! Iterate throguh the mathemathical sample-functions:
  for(uint base_id = 0; base_id < mapOf_functionStrings_base_size; base_id++) {
    //!
    //! Configure:
    const char *stringOf_tagSample = mapOf_functionStrings_base[base_id];
    const char *stringOf_sampleData_type = stringOf_tagSample;
    const bool isTo_transposeMatrix = mapOf_functionStrings_base__classificaiton[base_id].isTo_transpose;
    const char *stringOf_sampleData_type_realLife = NULL;
    assert(stringOf_sampleData_type); 	    
    assert(strlen(stringOf_sampleData_type));
    //groupId = groupId_mul*base_id;
    assert(groupId_mul >= 1);
    groupId = (base_id*cnt_runs) + (groupId_mul-1);
    groupId_max = macro_max(groupId_max, groupId);
    //! 
    //! Apply lgoics:
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases__stub__correlate.c"

  }

  //!
  //! Simliarty for the real-life-data-set:
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    const char *stringOf_tagSample = mapOf_realLife[data_id];
    assert(stringOf_tagSample);
    assert(strlen(stringOf_tagSample));
    //!
    //! Configure:
    const char *stringOf_sampleData_type = NULL;
    const char *stringOf_sampleData_type_realLife =stringOf_tagSample;
    const bool isTo_transposeMatrix = mapOf_realLife_classificaiton[data_id].isTo_transpose;
    //! 
    //! Apply lgoics:
    assert(mult_factor > 0);
    const uint fraction_colNoise = (mult_factor - 1); //! which is used to 'make menaingufll use of the "mult_factor" wrt. the rela-lfie-data-sets, ie, where we 'add' a random-noise (instead of increasing the fixed row-column-size).
    //! Note: an example of [”elow] "groupID" is: 'first run': [after-syntetic]: 2*|syntetic| + data_id
    //! Note: an example of [”elow] "groupID" is: 'second run': |sytntetic| + 
    //! Note: an example of [”elow] "groupID" is: 
    assert(groupId_mul >= 1);
    groupId = (cnt_runs * mapOf_functionStrings_base_size) + (data_id*cnt_runs) + (groupId_mul-1);
    groupId_max = macro_max(groupId_max, groupId);
    //groupId = (2*mapOf_functionStrings_base_size) + groupId_mul * data_id;
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases__stub__correlate.c"
  }  
