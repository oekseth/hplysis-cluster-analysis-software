{
  //! Speicy the matrix-sizes:
  const uint arrOf_cnt_buckets_size = 10; const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 
												 2, 3, 4, 5, 5, 7, 8, 9, 10,
												  //11, 12, 13, 14, 15
												  // 80, 160
												  //, 320, 740, 1000
  };
    const uint size_each_bucket = VECTOR_FLOAT_ITER_SIZE * 300;
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_cnt_buckets_size, default_value_float);

  if(testFor_optimized) { const char *stringOf_measureText = "vector(1): SSE-dot-product-vectors";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__dotProduct_vector(ncols, _rmul1, _rmul2);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
  //! ------------------------- 
  if(testFor_optimized)   { const char *stringOf_measureText = "vector(1): SSE-normalize";
#define __use_Result_to_updateTableComparisonResults 0
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__normalize(_row_input, _row_result, ncols_adjusted);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
  { const char *stringOf_measureText = "vector(1): naive-normalize";
    // Matrix<float> *matrix_x = get_pls_Cpp_matrix_FromFloat(matrix_x, nrows, ncols);
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:      
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      for(uint i = 0; i < size_of_array; i++) {
	PLS::normaliz(t, /*result=*/t0);
      }
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
    }
#undef __use_Result_to_updateTableComparisonResults
      //delete matrix_x
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "vector(1): SSE multiply-scalar";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__vec_mul_scalar(_row_input, ncols_adjusted, scalar, _row_result);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
    }
      if(testFor_naive) { const char *stringOf_measureText = "vector(1): naive multiply-scalar";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      const t_float scalar = 0.1;
      for(uint i = 0; i < size_of_array; i++) {
	PLS::MultiplyVectorandScalar(t, scalar,  /*result=*/t0);
      }
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
    }
#undef __use_Result_to_updateTableComparisonResults
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "vector(1): SSE row-mean";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__computeGauss__mean__forRow(_row, ncols_adjusted, ncols_inverted);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "vector(1): naive row-mean";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      for(uint i = 0; i < size_of_array; i++) {
	PLS::mean(t,  /*result=*/t0);
      }
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
    }
#undef __use_Result_to_updateTableComparisonResults
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "vector(1): SSE row-STD";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__computeGauss__STD__vector(_row, scalar_mean, ncols_adjusted, ncols_inv);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "vector(1): naive row-STD";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      for(uint i = 0; i < size_of_array; i++) {
	PLS::std(s, t,  /*result=*/t0);
      }
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete s;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
    }
#undef __use_Result_to_updateTableComparisonResults
    // // void std(Matrix<float> *M, Vector<float> *mean, Vector<float> *retvector);
    // void std(Vector<float> *M, Vector<float> *mean, Vector<float> *retvector);
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "vector(2): SSE row-subtract";
#define __use_Result_to_updateTableComparisonResults 0
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__row_subtractFrom_self_2InputVectors(row, ncols_adjusted, row_input_1, row_input_2, index);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "vector(2): naive row-subtract";
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      PLS::SubtractFromMatrix(X, t,  /*result=*/t0);
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
    //PLS pls_obj(); 
	// void SubtractFromMatrix(Matrix<float> *M, Vector<float> *t, Vector<float> *p);
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "vector(3): SSE Gauss Z-scores";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__computeGauss__ZScores__vector__inverted(_row, ncols_adjusted, row_mean, _row_std, _row_result);})
#define __macro__isTo_useInnerIteration 1
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "vector(3): naive Gauss Z-scores";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      PLS::zscore(X, t,  /*result=*/t0);
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
	// void zscore(Vector<float> *M, Vector<float> *mean, Vector<float> *std);
  }
  //! ------------------------- 
  { const char *stringOf_measureText = "matrix(1) transpose";
#define __use_Result_to_updateTableComparisonResults 0
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__transpose(nrows, ncols, matrix, result);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
  //! ------------------------- 
  { const char *stringOf_measureText = "matrix(1)::vector(1): mean";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__computeGauss__mean(matrix, nrows_adjusted, nrows, ncols_adjusted, ncols, _row_result);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
  if(testFor_naive){ const char *stringOf_measureText = "matrix(1)::vector(1): naive mean";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      PLS::mean(X,  /*result=*/t0);
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
    // void mean(Matrix<float> *M, Vector<float> *retvector) ;
  }
    if(testFor_optimized) { const char *stringOf_measureText = "matrix(1)::vector(1): SSE multiply w/transposed";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__matrixTransposed_mult_vector(matrix, nrows_adjusted, ncols_adjusted, _row_input, _row_result);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "matrix(1)::vector(1): naive multiply w/transposed";  
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      PLS::MultiplyTransposeMatrixbyVector(X, t, /*result=*/t0);
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
    // void MultiplyTransposeMatrixbyVector(Matrix<float> *M, Vector<float> *v, Vector<float> *retvector);
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "matrix(1)::vector(1): SSE multiply non-transposed";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__matrix_mult_vector(matrix, nrows_adjusted, ncols_adjusted, _row_input, _row_result);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "matrix(1)::vector(1): naive multiply non-transposed";
    // float MultiplyVectorTransposedbyVector(Vector<float> *v1, Vector<float> *v2);
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      for(uint i = 0; i < size_of_array; i++) {
	PLS::MultiplyVectorTransposedbyVector(t, /*result=*/t0);
      }
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
  }
    if(testFor_optimized) { const char *stringOf_measureText = "vector(2): SSE multiply non-transposed";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__vector_mult_vector(r_1, ncols_adjusted, row_std, _row_result);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
  { const char *stringOf_measureText = "vector(2): naive multiply non-transposed";
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      PLS::MultiplyMatrixbyVector(X, t, /*result=*/t0);
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
    //PLS pls_obj(); 
    // void MultiplyMatrixbyVector(Matrix<float> *M, Vector<float> *v, Vector<float> *retvector);
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "matrix(1)::vector(1): SSE Gauss-STD";
#define __use_Result_to_updateTableComparisonResults 0
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__computeGauss__STD(matrix, nrows_adjusted, ncols_adjusted, ncols, mapOf_means);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "matrix(1)::vector(1): naive Gauss-STD";
    PLS pls_obj(); 
    // void std(Matrix<float> *M, Vector<float> *mean, Vector<float> *retvector);
  }
  //  assert(False); // FXIME: include [”elow] in [ªbvoe]
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "matrix(1)::vector(2): SSE subtract";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__matrix_subtractFrom_self_2InputVectors(matrix, nrows_adjusted, ncols_adjusted, _row_input_1, _row_input_2);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "matrix(1)::vector(2): naive subtract";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      
      for(uint i = 0; i < size_of_array; i++) {
	PLS::SubtractFromVector(t, /*result=*/t0, 10.0, 0.1);
      }
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
	// void SubtractFromVector(Vector<float> *v, Vector<float> *t, float c, float bl);
  }
  //! ------------------------- 
    if(testFor_optimized) { const char *stringOf_measureText = "matrix(1)::vector(3): SSE Gauss-Z-scores";
#define __use_Result_to_updateTableComparisonResults 1
#define __macro_funcName_toCall() ({kt_mathMacros_SSE__computeGauss__ZScores(matrix, nrows_adjusted, ncols_adjusted, _row_mean, _row_std);})
#define __macro__isTo_useInnerIteration 0
    //! Compute:
#include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
    //! Clear:
#undef __use_Result_to_updateTableComparisonResults
#undef __macro_funcName_toCall
#undef __macro__isTo_useInnerIteration
  }
    if(testFor_naive){ const char *stringOf_measureText = "matrix(1)::vector(3): naive Gauss-Z-scores";
    //PLS pls_obj(); 
#define __use_Result_to_updateTableComparisonResults 0
    for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {    
      const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * size_each_bucket; //! eg, for "const uint size_each_bucket = 1000;"
      const uint nrows = size_of_array;       const uint ncols = size_of_array;
      const t_float default_value_float = 10;
      t_float **matrix = allocate_2d_list_float(size_of_array, size_of_array, default_value_float);
      t_float *r_1 = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *r_2 = allocate_1d_list_float(size_of_array, default_value_float);      
      Matrix<float> *X = get_pls_Cpp_matrix_FromFloat(matrix, nrows, ncols);
      Vector<float> *s = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t = get_pls_Cpp_vector_FromFloat(r_1, ncols);
      Vector<float> *t0 = get_pls_Cpp_vector_FromFloat(r_2, ncols);
      //PLS pls_obj(); 
      //! -------------------------------
      //! Start the clock:
      start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
      //!
      //! The experiemnt:
      //const t_float scalar = 0.1;
      
      PLS::zscore(X, t, /*result=*/t0);
      const t_float resultOf_lastMeasurement = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, mapOf_timeCmp_forEachBucket[cnt_buckets_index]); 
#if(__use_Result_to_updateTableComparisonResults == 1)
      mapOf_timeCmp_forEachBucket[cnt_buckets_index] = resultOf_lastMeasurement; //! ie, then udpate the bucket-score.
#endif

      delete t; delete t0; delete X;
      free_1d_list_float(&r_1);
      free_1d_list_float(&r_2);
      free_2d_list_float(&matrix, size_of_array);
    }
#undef __use_Result_to_updateTableComparisonResults
	// void zscore(Matrix<float> *M, Vector<float> *mean, Vector<float> *std);
  }
  //! ------------------------- 
//   { const char *stringOf_measureText = "matrix(1)::vector(1): SSE-";
// #define __use_Result_to_updateTableComparisonResults 0
// #define __macro_funcName_toCall() ({;})
// #define __macro__isTo_useInnerIteration 0
//     //! Compute:
// #include "measure_externalCxx__nipals__templateFunc__2dMatrix.c"
//     //! Clear:
// #undef __use_Result_to_updateTableComparisonResults
// #undef __macro_funcName_toCall
// #undef __macro__isTo_useInnerIteration
//   }




  //!
  //!  De-allocate locally reserved memory
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}
