#include "hp_entropy.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result. 
#include "kt_matrix.h"
#include "hp_distance.h"
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
#include "aux_sysCalls.h"
#include "kt_aux_matrix_norm.h" //! which hold the data-nraomizoant-enums.
#include "hp_image_segment.h"


static const t_float staticAssumption_score_min = -50;
static const t_float staticAssumption_score_max =  50;
// static const t_float staticAssumption_score_min = -100;
//static const t_float staticAssumption_score_min = -60;
//static const t_float staticAssumption_score_max = 1000;
static const bool staticConfig_doNOTExport_runTimeData = true;
//static const bool staticConfig_doNOTExport_runTimeData = false; //! which then implies that we gnerate huge amounts of tmeporary data. 


#ifndef MF__system
#define MF__system(string_call) ({const int ret_val = system(string_call); if(ret_val == -1) {fprintf(stderr, "!!\t Was Not able to exeucte command=\"%s\", ie, please investigate. Observiaont at [%s]:%s:%d\n", string_call, __FUNCTION__, __FILE__, __LINE__);}})
  MF__system(str_local); //! ie, then create the directory, assumign we are on a LINUX-compatible system.  
#endif //! #ifndef MF__system


#undef config_computeRankAgainstAllDataComb
// FIXME: ...
// FIXME: ...
// FIXME: ... correctly sets the ranks for the [0, 1, 2, 3, 4, 0, 1, 2, ...] case.
#define config_computeRankAgainstAllDataComb_ 1 // FIXME: remove this and use above.
#define config_useSyntDataSets 1

#if config_useSyntDataSets == 1
static const uint _cnt_cases = 6;
static const uint  cnt_cases = 6; //! which needs to be "<=" the maximum number of described 'sytnetic use-casees'.

#else
//#define tut3_arr_fileNamesInput_size 5
//static const uint tut3_fileNameDim[2] = {2150, 2149};
#define tut3_arr_fileNamesInput_size 6
static const uint tut3_fileNameDim[2] = {21, 21};
static const uint _cnt_cases = tut3_arr_fileNamesInput_size;
static const uint  cnt_cases = tut3_arr_fileNamesInput_size; //! which needs to be "<=" the maximum number of described 'sytnetic use-casees'.

    static const char *tut3_arr_fileNamesInput[tut3_arr_fileNamesInput_size] = {
      /*      
"preReshape_vegas_tm5_1989_lrg_export.tsv",
"preReshape_vegas_tm5_1994_lrg_export.tsv",
"preReshape_vegas_tm5_1999_lrg_export.tsv",
"preReshape_vegas_tm5_2004_lrg_export.tsv",
"preReshape_vegas_tm5_2009_lrg_export.tsv",
      */
      "vegas_som_inputData/vegas_tm5_1984_lrg_export_downSamplefrac0.01_cubic.tsv",
      "vegas_som_inputData/vegas_tm5_1989_lrg_export_downSamplefrac0.01_cubic.tsv",
      "vegas_som_inputData/vegas_tm5_1994_lrg_export_downSamplefrac0.01_cubic.tsv",
      "vegas_som_inputData/vegas_tm5_1999_lrg_export_downSamplefrac0.01_cubic.tsv",
      "vegas_som_inputData/vegas_tm5_2004_lrg_export_downSamplefrac0.01_cubic.tsv",
      "vegas_som_inputData/vegas_tm5_2009_lrg_export_downSamplefrac0.01_cubic.tsv",           
//     "preReshape_vegas_tm5_1989_lrg_export.tsv",
    };

s_kt_list_1d_float_t list_scalarImages[tut3_arr_fileNamesInput_size];
s_kt_matrix_t mat_scalarImages[tut3_arr_fileNamesInput_size];
#endif

typedef enum e_tut3_rgbToScalar {
  /**
     @brief provide a strategy to merge RGB pixel scores into a scalar value
   **/
  e_tut3_rgbToScalar_min,
  e_tut3_rgbToScalar_max,
  e_tut3_rgbToScalar_sum,
  //! 
  e_tut3_rgbToScalar_Hue,
  e_tut3_rgbToScalar_Saturation,
  e_tut3_rgbToScalar_mult_Hue_Saturation,
  e_tut3_rgbToScalar_maxHS_div_minHS, //! ie, (max(saturation, hue) / min(saturation, hue))
  // e_tut3_rgbToScalar_,
  //! ----------
  e_tut3_rgbToScalar_undef
} e_tut3_rgbToScalar_t;


typedef enum e_tut3_histogram {
  /**
     @brief defines differnet types of histopgram strategies
     @remarks the base-eenums are defined in our "kt_list_2d" strucutre.
   */
  // FIXME: make use of this 
  e_tut3_histogram_count_cntBins10,
  e_tut3_histogram_count_cntBins100,
  e_tut3_histogram_count_cntBins1000,  
  e_tut3_histogram_count_averageOnBins_cntBins10,
  e_tut3_histogram_count_averageOnBins_cntBins100,
  e_tut3_histogram_count_averageOnBins_cntBins1000,
  e_tut3_histogram_count_sum_cntBins10,  
  e_tut3_histogram_count_sum_cntBins100,  
  e_tut3_histogram_count_sum_cntBins1000,  
  e_tut3_histogram_undef,
} e_tut3_histogram_t;


static s_kt_list_1d_float_t convertTo_histo(const s_kt_list_1d_float_t &data, const e_tut3_histogram_t enum_id) {
  e_kt_histogram_scoreType_t histo_enum = e_kt_histogram_scoreType_count;
  uint cnt_buckets = 10;
  if(enum_id == e_tut3_histogram_count_cntBins10) {
    histo_enum = e_kt_histogram_scoreType_count;
    cnt_buckets = 10 ;
  } else if(enum_id == e_tut3_histogram_count_cntBins100) {
    histo_enum = e_kt_histogram_scoreType_count;
    cnt_buckets = 100;
  } else if(enum_id == e_tut3_histogram_count_cntBins1000) {
    histo_enum = e_kt_histogram_scoreType_count;
    cnt_buckets = 1000;
  } else if(enum_id == e_tut3_histogram_count_averageOnBins_cntBins10) {
    histo_enum = e_kt_histogram_scoreType_count_averageOnBins;
    cnt_buckets = 10;
  } else if(enum_id == e_tut3_histogram_count_averageOnBins_cntBins100) {
    histo_enum = e_kt_histogram_scoreType_count_averageOnBins;
    cnt_buckets = 100;
  } else if(enum_id == e_tut3_histogram_count_averageOnBins_cntBins1000) {
    histo_enum = e_kt_histogram_scoreType_count_averageOnBins;
    cnt_buckets = 1000;    
  } else if(enum_id == e_tut3_histogram_count_sum_cntBins10) {
    histo_enum = e_kt_histogram_scoreType_sum;
    cnt_buckets = 10;
  } else if(enum_id == e_tut3_histogram_count_sum_cntBins100) {
    histo_enum = e_kt_histogram_scoreType_sum;
    cnt_buckets = 100;
  } else if(enum_id == e_tut3_histogram_count_sum_cntBins1000) {
    histo_enum = e_kt_histogram_scoreType_sum;
    cnt_buckets = 1000;        
  } else if(enum_id == e_tut3_histogram_undef) {
    s_kt_list_1d_float_t ret = copy__s_kt_list_1d_float_t(&data);
    return ret;
  } else {assert(false);} //! ie, then add support for this enum.
  s_kt_list_1d_float_t ret = applyTo_list__kt_aux_matrix_binning(&data, cnt_buckets, /*isTo_forMinMax_useGlobal=*/false, histo_enum);
  return ret;
}
static s_kt_matrix_t convertTo_histo_matrix(const s_kt_matrix_t &data, const e_tut3_histogram_t enum_id) {
  //  s_kt_matrix_t ret = initAndReturn__s_kt_matrix(/*nrows=*/data.nrows,
  s_kt_matrix_t ret = setToEmptyAndReturn__s_kt_matrix_t();
  for(uint row_id = 0; row_id < data.nrows; row_id++) {
    //! Cosntruct a shallow lsit (ie, do NOT allcoate memory):
    s_kt_list_1d_float_t shallow = setToEmpty__s_kt_list_1d_float_t();
    shallow.list = data.matrix[row_id];
    shallow.list_size = data.ncols;
    s_kt_list_1d_float_t list = convertTo_histo(shallow, enum_id);
    assert(list.list_size > 0);
    if(row_id == 0) { //! ie, to get the nubmer of clolumsn.
      ret = initAndReturn__s_kt_matrix(data.nrows, list.list_size);
    } else {assert(ret.ncols == list.list_size);}  //! as we exxpec tthe column-size to be conistent.
    assert(row_id < ret.nrows);
    //! Copy:
    for(uint i = 0; i < list.list_size; i++) {
      ret.matrix[row_id][i] = list.list[i];
    }
    //! De-allcoate temorary struct: 
    free__s_kt_list_1d_float_t(&list);
  }
  return ret;
}



typedef enum e_tut_entropy3_combineInputData_scalar {
  // FIXME: update our algorithm-descrption writh this strategy
  e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_data0,
  e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_dataEntropyBest,
  e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_dataEntropyWorst,
  //! 
  e_tut_entropy3_combineInputData_scalar_undef //! which if set implies that the input-data is NOT combined/merggd (during loading).
} e_tut_entropy3_combineInputData_scalar_t;

typedef struct s_tut_entropy_bestMetric {
  t_float score_best[2];
  s_kt_list_1d_string_t score_best_str;
  t_float score_worst[2];
  s_kt_list_1d_string_t score_worst_str;
  //!
  //! Store histograms:
  s_kt_matrix_t matHisto_all; //! ie, histogram for: all points
  s_kt_matrix_t matHisto_metricBase; //! ie, histogram for all base-mtrics (eg, entopry-metrics):
  s_kt_matrix_t matHisto_norm; //! ie, histogram for: all nomrlaizaiton-metrics
  s_kt_matrix_t matHisto_RGB; //! ie, histogram for: all RGB-adjustment-metrics
  s_kt_matrix_t matHisto_histoPre; 
  // s_kt_matrix_t matHisto_; //! ie, histogram for:     
  //!
  //!   
} s_tut_entropy_bestMetric_t;

static void free_s_tut_entropy_bestMetric_t(s_tut_entropy_bestMetric_t &self) {
  { //!
    //! Write out the histograms:
    { //! Apply:
      char buff[2000]; sprintf(buff, "result_entr_histogram_%s.tsv", "all");
      export__singleCall__s_kt_matrix_t(&(self.matHisto_all), /*file-name-result=*/buff, NULL);
    }
    { //! Apply:
      char buff[2000]; sprintf(buff, "result_entr_histogram_%s.tsv", "metricBase");
      export__singleCall__s_kt_matrix_t(&(self.matHisto_metricBase), /*file-name-result=*/buff, NULL);
    }
    { //! Apply:
      char buff[2000]; sprintf(buff, "result_entr_histogram_%s.tsv", "norm");
      export__singleCall__s_kt_matrix_t(&(self.matHisto_norm), /*file-name-result=*/buff, NULL);
    }
    { //! Apply:
      char buff[2000]; sprintf(buff, "result_entr_histogram_%s.tsv", "RGB");
      export__singleCall__s_kt_matrix_t(&(self.matHisto_RGB), /*file-name-result=*/buff, NULL);
    }
    { //! Apply:
      char buff[2000]; sprintf(buff, "result_entr_histogram_%s.tsv", "histoPre");
      export__singleCall__s_kt_matrix_t(&(self.matHisto_histoPre), /*file-name-result=*/buff, NULL);
    }        
    //!
    //! De-allcoate the histograms:
    free__s_kt_matrix(&(self.matHisto_all));
    free__s_kt_matrix(&(self.matHisto_metricBase));
    free__s_kt_matrix(&(self.matHisto_norm));
    free__s_kt_matrix(&(self.matHisto_RGB));
    free__s_kt_matrix(&(self.matHisto_histoPre));            
  }
  //!  
  free__s_kt_list_1d_string(&(self.score_best_str));
  self.score_best[0] = T_FLOAT_MAX;
  self.score_best[1] = T_FLOAT_MAX;
  //!
  free__s_kt_list_1d_string(&(self.score_worst_str));
  self.score_worst[0] = T_FLOAT_MIN_ABS;
  self.score_worst[1] = T_FLOAT_MIN_ABS;  
}

static s_tut_entropy_bestMetric_t init_bestMetric() {
  s_tut_entropy_bestMetric_t self;
  self.score_best[0] = T_FLOAT_MAX;
  self.score_best[1] = T_FLOAT_MAX;
  self.score_best_str = init_andReturn__s_kt_list_1d_string_t(/*nrows=*/2);
  //!
  self.score_worst[0] = T_FLOAT_MIN_ABS;
  self.score_worst[1] = T_FLOAT_MIN_ABS;
  self.score_worst_str = init_andReturn__s_kt_list_1d_string_t(/*nrows=*/2);
  //!
  //! Init: histogram-result-matrices:
  const uint cnt_buckets = (staticAssumption_score_max - staticAssumption_score_min);
  assert(cnt_buckets < 10000); //! ie, as othersizes mgith indicate an erorr (givne our current assumpåtionsl
  //!
  self.matHisto_all        = initAndReturn_defaultValue__s_kt_matrix(/*nrows=*/1, /*ncols=*/cnt_buckets+1, /*start-count=*/0);
  self.matHisto_metricBase = initAndReturn_defaultValue__s_kt_matrix(/*nrows=*/2*(e_kt_entropy_genericType_undef+1), /*ncols=*/cnt_buckets+1, /*start-count=*/0);
  { //! Sets row-names:
    for(uint row_id = 0; row_id < self.matHisto_metricBase.nrows; row_id++) {
      char str[300]; sprintf(str, "metric:%u", row_id); //! where we sue 'uint' instead of a desciprtive string to simplify our code-generaiotn.
      set_stringConst__s_kt_matrix(&(self.matHisto_metricBase), row_id, str, /*addFor_column=*/false);      
    }
  }
  self.matHisto_norm       = initAndReturn_defaultValue__s_kt_matrix(/*nrows=*/e_kt_normType_undef+1, /*ncols=*/cnt_buckets+1, /*start-count=*/0);
  { //! Sets row-names:
    for(uint row_id = 0; row_id < self.matHisto_norm.nrows; row_id++) {
      char str[300]; sprintf(str, "metric:%u", row_id); //! where we sue 'uint' instead of a desciprtive string to simplify our code-generaiotn.
      set_stringConst__s_kt_matrix(&(self.matHisto_norm), row_id, str, /*addFor_column=*/false);      
    }
  }
  self.matHisto_RGB        = initAndReturn_defaultValue__s_kt_matrix(/*nrows=*/e_tut3_rgbToScalar_undef+1, /*ncols=*/cnt_buckets+1, /*start-count=*/0);
  { //! Sets row-names:
    for(uint row_id = 0; row_id < self.matHisto_RGB.nrows; row_id++) {
      char str[300]; sprintf(str, "metric:%u", row_id); //! where we sue 'uint' instead of a desciprtive string to simplify our code-generaiotn.
      set_stringConst__s_kt_matrix(&(self.matHisto_RGB), row_id, str, /*addFor_column=*/false);      
    }
  }
  self.matHisto_histoPre   = initAndReturn_defaultValue__s_kt_matrix(/*nrows=*/e_tut3_histogram_undef+1, /*ncols=*/cnt_buckets+1, /*start-count=*/0);
  { //! Sets row-names:
    for(uint row_id = 0; row_id < self.matHisto_histoPre.nrows; row_id++) {
      char str[300]; sprintf(str, "metric:%u", row_id); //! where we sue 'uint' instead of a desciprtive string to simplify our code-generaiotn.
      set_stringConst__s_kt_matrix(&(self.matHisto_histoPre), row_id, str, /*addFor_column=*/false);      
    }
  }  
  //!
  //!
  return self;
}


typedef struct s_tut_entropy {
  const char *exportDir;
  e_tut3_histogram_t enum_histoAfterDataLoad;
  e_kt_normType_t enum_norm_afterDataLoad;
  e_kt_normType_t enum_normAfterMetricMerge;
  s_tut_entropy_bestMetric_t *bestMetric;
  s_kt_correlationMetric_t corrMetric_betweenSamples;
  e_tut_entropy3_combineInputData_scalar_t enum_combineData;
  e_tut3_rgbToScalar_t enum_RGB;
} s_tut_entropy3_t;

//! Descripve core attributes of the enum:
#define s_tut_entropy_setFormatString(self, buff, fileSuffix) ({  sprintf(buff, "%s/h%u_n%d_m%u_s%d_sP%d_n%u_rgb%u_%s.tsv", self->exportDir, self->enum_histoAfterDataLoad, self->enum_norm_afterDataLoad, self->enum_combineData, self->corrMetric_betweenSamples.metric_id, self->corrMetric_betweenSamples.typeOf_correlationPreStep, self->enum_normAfterMetricMerge, self->enum_RGB, fileSuffix);})


static void free__s_tut_entropy3_t(s_tut_entropy3_t self) {
  ; //! ie, left for the future
}
static const char *getStr_norm(s_tut_entropy3_t *self) {
  return get_str__humanReadable__e_kt_normType_t(self->enum_norm_afterDataLoad);
}


static void update_bestMetric(s_tut_entropy_bestMetric_t *self, s_tut_entropy3_t *spec, s_kt_matrix_t &mat_corrHypo, const bool is_relative) {
  /**
     @brief update the struct with the best-performing metric-comb.
     @param <self> the object holding the lcoal config-spec.
     @param <mat_corrHypo> the result of applying Kendall to a given (metric, hypthesis).
     @remarks to best undertand this funciton note that it is called from:
     -- static void _export_matrixRelativeToIdealHypo(s_tut_entropy3_t *self, s_kt_matrix_t &mat_corrHypo, s_kt_correlationMetric_t obj_metric, const char *str_result);
     -- static s_kt_matrix_t _correlateAndExport(..);
  **/
  //! Note: only iterat ehtoruhg hte first 'column', ie, as we assume the first column describes the null-hyptoehssi to test:
  for(uint metric_id = 0; metric_id < mat_corrHypo.nrows; metric_id++) {
    const t_float score = mat_corrHypo.matrix[metric_id][0];
    if(isOf_interest(score) && (score != 0) && (isinf(score) == false) && (isnan(score) == false) ) {
      if(self->score_best[is_relative] > score) {
	self->score_best[is_relative] = score;
	//! Update string:
	// free__s_kt_list_1d_string(&(self->score_best_str));
	const uint index = is_relative;
	const char *str = getAndReturn_string__s_kt_matrix(&mat_corrHypo, index, /*addFor_column=*/false);
	if(str && strlen(str)) {
	  assert(strlen(str) < 1000);
	  char buff[3000];
	  sprintf(buff, "%s %s", getStr_norm(spec), str);
	  char buff_desc[2000]; s_tut_entropy_setFormatString(spec, buff_desc, "<selectBest>");
	  printf("\t(new-Best-score:%s)\t \"%s\":%f -- %s\n", (is_relative) ? "relative" : "abs", buff, score, buff_desc);
	  set_stringConst__s_kt_list_1d_string(&(self->score_best_str), /*index=*/index, buff);
	}
      }
      //!
      if(self->score_worst[is_relative] < score) {
	self->score_worst[is_relative] = score;
	//! Update string:
	// free__s_kt_list_1d_string(&(self->score_worst_str));
	const uint index = is_relative;
	const char *str = getAndReturn_string__s_kt_matrix(&mat_corrHypo, index, /*addFor_column=*/false);
	if(str && strlen(str)) {
	  assert(strlen(str) < 1000);
	  char buff[3000];
	  sprintf(buff, "%s %s", getStr_norm(spec), str);
	  char buff_desc[2000]; s_tut_entropy_setFormatString(spec, buff_desc, "<selectWorst>");
	  printf("\t(new-Worst-score:%s)\t \"%s\":%f -- %s\n", (is_relative) ? "relative" : "abs", buff, score, buff_desc);
	  set_stringConst__s_kt_list_1d_string(&(self->score_worst_str), /*index=*/index, buff);
	}
      }
      { //! Update histograms for: (1) all metric-combinations, (2) seperattely for each entopry-metric combination, (3) seperately for each norm-before mtric.
	//! Note. Usage. Provides insight into the importance of the building-lbocks (eg, wrt. which building-blocks to skip from the evlauation). 
	t_float s = score;
	const t_float score_min = staticAssumption_score_min;
	const t_float score_max = staticAssumption_score_max;
	//! Pre-adjust:
	if(s < score_min) {s = score_min;} //! ie, adjust.
	if(s > score_max) {s = score_max;}
	//! Adjsut to corresponding index:
	const uint index = (uint)(s - score_min);
	//! Update: all:
	assert(index < self->matHisto_all.ncols);
#ifdef config_computeRankAgainstAllDataComb
    assert(false) // FIXME: correclty set below:
#else
	assert(self->matHisto_all.matrix[0][index] != T_FLOAT_MAX); //! ie, as we asusme this is iniated.
	self->matHisto_all.matrix[0][index] += 1; //! ie, icnrease the histogram-count.
#endif	
	//! Update: metric-base
	if(metric_id < self->matHisto_metricBase.nrows) {
	  //! Note: th number of emtrics may variy depeding on which expeirement, hence of ismplifd assumption wrt. below: 
	  assert(index < self->matHisto_metricBase.ncols);
	  assert(self->matHisto_metricBase.matrix[metric_id][index] != T_FLOAT_MAX); //! ie, as we asusme this is iniated.
	  self->matHisto_metricBase.matrix[metric_id][index] += 1; //! ie, icnrease the histogram-count.
	}
	//! Update: metric-base
	assert(spec->enum_norm_afterDataLoad < self->matHisto_norm.nrows);
	assert(index < self->matHisto_norm.ncols);
	self->matHisto_norm.matrix[spec->enum_norm_afterDataLoad][index] += 1; //! ie, icnrease the histogram-count.
	//! Update: RGB-influcne: 
	assert(spec->enum_RGB < self->matHisto_RGB.nrows);
	assert(index < self->matHisto_RGB.ncols);
	self->matHisto_RGB.matrix[spec->enum_RGB][index] += 1; //! ie, icnrease the histogram-count.	
	//! Update:
	// self->matHisto_.matrix[spec->][index] += 1; //! ie, icnrease the histogram-count.
      }      
    }
  }
}






void apply_norm_afterDataLoad(s_tut_entropy3_t *self, s_kt_list_1d_uint_t &obj) {
  if(self->enum_norm_afterDataLoad == e_kt_normType_undef) {return;} //! ie, as the data-nroamizaitoin is then NOT to be used.
  //! Convert to a floating-point-number (ie, tor educe the number of API-fucnitons to 'have'):
  s_kt_list_1d_float_t tmp = init__s_kt_list_1d_float_t(obj.list_size);
  for(uint i = 0; i < tmp.list_size; i++) {
    tmp.list[i] = (t_float)obj.list[i];
  }
  //! Apply:
  s_kt_list_1d_float_t tmp_new = init__s_kt_list_1d_float_t(obj.list_size);
  //! Normalise socres: score(i) = (score(i) - AVG(score))/(max(score)-AVG(score)) ... ie, to avodi 'large differences in scales' from clustteirng the 'ability' of our 'signature-comarpsion-appraoch' (oekseth, 06. mai. 2017).
  apply_norm__list_matrix_norm(tmp.list, tmp_new.list, tmp_new.list_size,  self->enum_norm_afterDataLoad);
  //! Step: adjust the valeus iot. avoid enagtive inptu-values.
  t_float val_min = T_FLOAT_MAX;
  for(uint k = 0; k < tmp.list_size; k++) {
    if(isOf_interest(tmp_new.list[k])) {
      val_min = macro_min(val_min, tmp_new.list[k]);
    }
  }
  if(val_min < 0) {val_min *= -1;} //! ie, then adjust its sign
  else {val_min = 0;}
  //! Copy backwards:
  for(uint i = 0; i < tmp.list_size; i++) {
    obj.list[i] = (uint)(val_min + tmp_new.list[i]);
  }  
  //! De-allcoate:
  free__s_kt_list_1d_float_t(&tmp);
  free__s_kt_list_1d_float_t(&tmp_new);
}

static s_tut_entropy3_t init__s_tut_entropy3_t(const char *dir, e_kt_normType_t enum_norm,  s_kt_correlationMetric_t corrMetric_betweenSamples, s_tut_entropy_bestMetric_t *bestMetric, e_tut_entropy3_combineInputData_scalar_t enum_combineData, e_kt_normType_t enum_normAfterMetricMerge) {
  s_tut_entropy3_t self;
  self.enum_histoAfterDataLoad = e_tut3_histogram_undef;
  self.enum_norm_afterDataLoad = enum_norm; // e_kt_normType_undef;
  self.enum_normAfterMetricMerge = enum_normAfterMetricMerge;
  self.bestMetric = bestMetric;
  self.corrMetric_betweenSamples =  corrMetric_betweenSamples;
  self.enum_combineData = enum_combineData;
  self.enum_RGB = e_tut3_rgbToScalar_undef;
  assert(dir);
  assert(strlen(dir) > 0);
  self.exportDir = dir;
  //! Create directy if it does NOT exist:
  if(false) {
    char str_local[2000] = {'\0'}; sprintf(str_local, "mkdir -p %s", dir);
    MF__system(str_local);
  }
  //! Emopve any current files:
  // removeFiles_inFolder__aux_sysCalls(self.exportDir); //! Note: by defualt NOT inlcud to aovid any erornous file-removal.
  //! @return the itnlaised object
  return self;
}




static void _export_toFile(s_tut_entropy3_t *self, s_kt_matrix_t &mat, const char *fileSuffix) {
  if(staticConfig_doNOTExport_runTimeData) {return;}
  assert(mat.nrows > 0);
  assert(mat.ncols > 0);
  assert(mat.matrix != NULL);
  assert(fileSuffix);
  assert(strlen(fileSuffix) > 0);
  char buff[2000]; s_tut_entropy_setFormatString(self, buff, fileSuffix);
  if(mat.nrows > 0) {
    //! Apply:
    export__singleCall__s_kt_matrix_t(&mat, /*file-name-result=*/buff, NULL);
  }
}

static void _export_dev_toFile(s_tut_entropy3_t *self, s_kt_matrix_t &mat, const char *fileSuffix) {
  return; // FIXME: remove. <-- used for evlauting the performacne-cost of this operaiton
  assert(mat.nrows > 0);
  assert(mat.ncols > 0);
  assert(mat.matrix != NULL);
  assert(fileSuffix);
  assert(strlen(fileSuffix) > 0);
  char buff[2000];
  s_tut_entropy_setFormatString(self, buff, fileSuffix); // FIXME: is this call correct? <-- if wrong, then update similar code-chunks (ie, as the error may have proapgated).
  // char buff[2000];
  sprintf(buff, "%s/n%d_m%u_s%d_sP%d_n%u_%s.tsv", self->exportDir, self->enum_norm_afterDataLoad, self->enum_combineData, self->corrMetric_betweenSamples.metric_id, self->corrMetric_betweenSamples.typeOf_correlationPreStep, self->enum_normAfterMetricMerge, fileSuffix);
  //sprintf(buff, "%s/res_n%d_merge%u_sim%d_simP%d_normVectorMerge%u_%s", self->exportDir, self->enum_norm_afterDataLoad, self->enum_combineData, self->corrMetric_betweenSamples.metric_id, self->corrMetric_betweenSamples.typeOf_correlationPreStep, self->enum_normAfterMetricMerge, fileSuffix);
  // sprintf(buff, "%s/res_n%d_merge%u_sim%d_simP%d_%s", self->exportDir, self->enum_norm_afterDataLoad, self->enum_combineData, self->corrMetric_betweenSamples.metric_id, self->corrMetric_betweenSamples.typeOf_correlationPreStep, fileSuffix);
  // sprintf(buff, "%s/res_n%d_%s", self->exportDir, self->enum_norm_afterDataLoad, fileSuffix);
  //sprintf(buff, "%s/%s", self->exportDir, fileSuffix);
  //! Apply:
  extractAndExport__deviations__singleCall__s_kt_matrix_t(&mat, buff, NULL, /*config_isToTranspose__whenGeneratingOutput=*/false, /*exportFormat=*/e_kt_matrix__exportFormats_tsvMatrix, e_kt_matrix__exportFormats___extremeToPrint_undef);
}

static void _updateMatrixWith_columnHeader_entropyEnum(s_kt_matrix_t &mat_result, const uint index, const e_kt_entropy_genericType_t enum_id, const bool postScaling) {
  if(NULL == getAndReturn_string__s_kt_matrix(&mat_result, index, /*addFor_column=*/true)) { //! then we inset the entropy-metric-description.
    const char *str = get_stringOfEnum__e_kt_entropy_genericType_t(enum_id);
    assert(str != NULL);
    assert(strlen(str) > 0);
    assert(strlen(str) < 1000); //! ie, what we epect in below.
    if(postScaling == false) {
      set_stringConst__s_kt_matrix(&mat_result, index, str, /*addFor_column=*/true);
    } else {
      //! Constuct new string:
      char buffer[1100];
      sprintf(buffer, "%s w/postScale", str);
      set_stringConst__s_kt_matrix(&mat_result, index, buffer, /*addFor_column=*/true);
    }
  }
}


static void _compute_entropy_all(s_tut_entropy3_t *self, s_kt_list_1d_uint_t &list_count, s_kt_matrix_t &mat_result, const uint index_data) {
  assert(index_data < mat_result.nrows);
  assert((2*e_kt_entropy_genericType_undef)==mat_result.ncols); //! ie, number of entropy-metrics we evalaute for.
  //! Apply data-nroamziaotn, if, if specified:
  apply_norm_afterDataLoad(self, list_count);
  //!
  //! Comptue entropy:
  if(false) {
    t_float result = 0;
    assert(false);
    // compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, e_kt_entropy_genericType_ML_Shannon, /*isTo_applyPostScaling=*/false, &result);
    printf("#! Result: score=%f, at %s:%d\n", result, __FILE__, __LINE__);
  } else { //! then we examplfiy how to comptue for all fot he differnet entropy-metrics: 
    if(true) { //! then we apply  a pre-step 'nullyifying' all values above a cettain trheshold:
      for(uint i = 0; i < list_count.list_size; i++) {
	if((int)list_count.list[i]  <= -1) { //! then we asusme the value is NOT set <-- FIXME: vlaidate this asusmtpion (which seems to be arisign when ipnut-value = "T_FLOAT_MAX"). <-- for images we asusme this is correct (as we do NOT expect any negative valeus when thy desicrbe RGB-color-values).
	  list_count.list[i] = 0; // FIXME: validte this converison.
	}
      }
    }
    uint index = 0;
    for(uint entropy_index = 0; entropy_index < e_kt_entropy_genericType_undef; entropy_index++) {
      for(uint postScaling = 0; postScaling < 2; postScaling++) {
	const e_kt_entropy_genericType_t enum_id = (e_kt_entropy_genericType_t)entropy_index;
	uint max_theoreticCountSize = 0;
	for(uint row_id = 0; row_id < list_count.list_size; row_id++) {
	  const uint thres = 100*1000;
	  if(list_count.list[row_id] > thres) {
	    list_count.list[row_id] = thres; // FIXME: validate this ... used to remove odd values
	  }
	  max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count.list[row_id]);
	}
	assert(list_count.list_size > 0); //! ie, what we expect.
	t_float result = 0;	
	compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, enum_id, /*isTo_applyPostScaling=*/(bool)postScaling, &result);
	// printf("#! Result: score[%s|scale:%s]=%f, at %s:%d\n", get_stringOfEnum__e_kt_entropy_genericType_t(enum_id), (postScaling) ? "true " : "false", result, __FILE__, __LINE__);
	//! Insert a descriptive column-header: 
	_updateMatrixWith_columnHeader_entropyEnum(mat_result, /*index-data=*/index, enum_id, postScaling);
	//!
	//! Update:
	mat_result.matrix[index_data][index] = result;
	index += 1;
      }
    }
  }
}

#if config_useSyntDataSets == 1
static const char *_get_syntheticData(const uint i, const uint cnt_features, s_kt_list_1d_uint_t &list_count) {
  //! Note: complexities in below ordering is used to reflect the "SimpsonGeneric" entropy-metric.
  uint nrows = cnt_features;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
  //!
  //! Set values:
  const char *prefix = NULL;
  if(i == 5) { //! CASE:  'all numbers are equal, size:20', 'all numbers are equal: size:400',
    //!
    //! Allocate:
    list_count = init__s_kt_list_1d_uint_t(nrows);
    for(uint row_id = 0; row_id < nrows; row_id++) {list_count.list[row_id] = 1;  }
    prefix = "Equal=1";
  } else if(i == 3) { //! CASE:  'all numbers are equal, size:20', 'all numbers are equal: size:400',
    list_count = init__s_kt_list_1d_uint_t(nrows);
    for(uint row_id = 0; row_id < nrows; row_id++) {list_count.list[row_id] = 400;  }      
    prefix = "Equal=400";
  } else if(i == 2) { //! CASE: 	'numbers are different: a +1 increment between each number, size:20', 'numbers are different: a +1 increment between each number, size:400',
    //!
    //! Allocate:
    list_count = init__s_kt_list_1d_uint_t(nrows);
    for(uint row_id = 0; row_id < nrows; row_id++) {list_count.list[row_id] = row_id;  }
    prefix = "incremental";
  } else if(i == 1) { //! CASE: 	'numbers are different: a +1 increment between each number, size:20', 'numbers are different: a +1 increment between each number, size:400',
    //!
    prefix = "incremental, size*100";
    nrows *= 100;
    // nrows = 400;
    //! Allocate:
    list_count = init__s_kt_list_1d_uint_t(nrows);
    for(uint row_id = 0; row_id < nrows; row_id++) {list_count.list[row_id] = row_id;  }      
  } else if(i == 0) { //! CASE:  'numbers are different: a +2 increment between each number, size:20', 'numbers are different: a *10 increment between each number, size:400'
    //!
    //! Allocate:
    list_count = init__s_kt_list_1d_uint_t(nrows);
    for(uint row_id = 0; row_id < nrows; row_id++) {list_count.list[row_id] = row_id*10;  }
    prefix = "incremental*10";
  } else if(i == 4) { //! CASE:  'numbers are different: a +2 increment between each number, size:20', 'numbers are different: a *10 increment between each number, size:400'
    nrows = 400; 
    //!
    //! Allocate:
    list_count = init__s_kt_list_1d_uint_t(nrows);
    for(uint row_id = 0; row_id < nrows; row_id++) {list_count.list[row_id] = row_id*10;  }
    prefix = "incremental*10, size*100";
  } else {
    assert(false); //! ie, as we did not expect this.
  }        
  //! 
  //! Insert a row-header:
  // printf("[%d]: %s\n", i, prefix);
  assert(prefix != NULL);
  assert(strlen(prefix) > 0);
  return prefix;
}
#else //! #if config_useSyntDataSets == 1
static const char *_get_syntheticData(const uint i, const uint cnt_features, s_kt_list_1d_uint_t &list_count) {
  assert(i < tut3_arr_fileNamesInput_size);
  const uint nrows = list_scalarImages[i].list_size;
  assert(nrows > 0); //! ie, as we assuume the input-data is allocated:
  list_count = init__s_kt_list_1d_uint_t(nrows);
  //!
  //! Step: dientify the correct offset-value, ie, to convert a +- number into an 'unisigned pluss (+) number':
  t_float val_min = T_FLOAT_MAX;
  for(uint k = 0; k < nrows; k++) {
    const t_float s = list_scalarImages[i].list[k];
    if(isOf_interest(s)) {
      val_min = macro_min(val_min, s);
    }
  }
  if(val_min < 0) {val_min *= -1;} //! ie, then adjust its sign
  else {val_min = 0;}
  //! Apply the ajustment:
  for(uint k = 0; k < nrows; k++) {
    const t_float s = list_scalarImages[i].list[k];
    if(isOf_interest(s)) {
      list_count.list[k] = (uint)(val_min + s);
    }
  }    
  //!
  //! Step: return the data-name:
  return tut3_arr_fileNamesInput[i];

}
#endif //! #if config_useSyntDataSets == 1

static s_kt_matrix_t _computeEntropy_for_synMatrix(s_tut_entropy3_t *self, const uint cnt_metrics, s_kt_matrix_t &mat_hypo, const uint cnt_cases, const uint cnt_features, const bool isTo_appendHypoMatrix_toSampleDataSet) {
  assert(mat_hypo.nrows > 0); //! ie, as we expect at least one hypohesis to be set.
  assert(cnt_metrics > 0);
  assert(cnt_cases > 0);
  assert(cnt_cases <= _cnt_cases); //! ie, as we otherwise have an incsontency.
  s_kt_matrix_t mat_result = setToEmptyAndReturn__s_kt_matrix_t();
  if(isTo_appendHypoMatrix_toSampleDataSet) {    
    mat_result =  initAndReturn__s_kt_matrix(/*nrows=*/(cnt_cases  + mat_hypo.nrows), /*ncols=*/(cnt_metrics));
  } else {
    assert(cnt_cases > 0);
    assert(cnt_metrics > 0);
    mat_result =  initAndReturn__s_kt_matrix(/*nrows=*/(cnt_cases), /*ncols=*/(cnt_metrics));
  }
  //! Explore different combinations:
  const bool isTo_useEntropyInPostProcessing = ( (self->enum_combineData == e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_dataEntropyBest) || (self->enum_combineData == e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_dataEntropyWorst) );
  s_kt_list_1d_uint_t list_count_data0 = setToEmpty__s_kt_list_1d_uint_t(); 
  for(uint i = 0; i < cnt_cases; i++) {

    //!
    //! Allocate:
    s_kt_list_1d_uint_t list_count  = setToEmpty__s_kt_list_1d_uint_t(); 
    const char *prefix = _get_syntheticData(i, cnt_features, list_count); //! ie, consturct sysnthetic feature-set.
    set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, prefix, /*addFor_column=*/false);      
    if(isTo_useEntropyInPostProcessing || (self->enum_combineData == e_tut_entropy3_combineInputData_scalar_undef) ) {
      //! Compute entropy for [list] x [entropy-metrics] 
      _compute_entropy_all(self, list_count, mat_result, /*index_data=*/i);
    } else if(self->enum_combineData == e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_data0) {
      if(i == 0) { //! then we assume the data is the 'gold-reference-data' (ie, to idneitfy the effect of not applying any metric).
	for(uint row_id = 0; row_id < list_count.list_size; row_id++) {list_count.list[row_id] = 1;  } //! ie, set to a constnt: "data[i] / data[i] = 1"
	// _compute_entropy_all(self, list_count, mat_result, /*index_data=*/i);
      } else {
	for(uint row_id = 0; row_id < list_count.list_size; row_id++) {
	  if(row_id < list_count_data0.list_size) {
	    const t_float h = (t_float)list_count_data0.list[row_id];
	    const t_float s = (t_float)list_count.list[row_id];
	    if((h != 0) && (s != 0) ) {
	      t_float res = ( (macro_max(h, s) / macro_min(h, s) ) );
	      if(res < 0) {res = 0;} // FIXME: try to idneityf a straagy where this is not nessary ... by abyb adjsutign by teh extreme-min-value
	      list_count.list[row_id] = (uint)res;
	    } else { //!
	      list_count.list[row_id] = 0; // TODO: cosnier suign  adiffernet symbol.
	    }	      
	  } else { //!
	    list_count.list[row_id] = 0; // TODO: cosnier suign  adiffernet symbol.
	  }
	}
      }
      _compute_entropy_all(self, list_count, mat_result, /*index_data=*/i);
    } else {
      assert(false); //! ie, then add support for this combination.
    }
    //!
    //! De-allocates:
    if(i != 0) { 
      free__s_kt_list_1d_uint_t(&list_count);
    } else {
      list_count_data0 = list_count; //! ie, remember.
    }
  }
  if(isTo_useEntropyInPostProcessing) { //! then we apply a seperate post-processing-step:
    uint reference_index = 0;
    if(self->enum_combineData == e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_dataEntropyBest) {
      assert(mat_result.ncols > 0);
      assert(mat_result.nrows > 0);

      // FIXME: consider computing seperately for each enoptry-metric:
      t_float score = T_FLOAT_MIN_ABS;
      for(uint i = 0; i < mat_result.nrows; i++) {
	assert(i < mat_result.nrows); //! note: added due to odd memory-err.r
	for(uint k = 0; k < mat_result.ncols; k++) {
	  assert(i < mat_result.nrows); //! note: added due to odd memory-err.r
	  assert(k < mat_result.ncols); //! note: added due to odd memory-err.r
	  const t_float s = mat_result.matrix[i][k];
	  if(isOf_interest(s) && (score < s) ) {
	    score = s;
	    reference_index = i;
	  }
	}
	assert(i < mat_result.nrows); //! note: added due to odd memory-err.r
      }
    } else if(self->enum_combineData == e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_dataEntropyWorst) {
      assert(mat_result.ncols > 0);
      assert(mat_result.nrows > 0);
      // FIXME: consider computing seperately for each enoptry-metric:
      t_float score = T_FLOAT_MAX;
      for(uint i = 0; i < mat_result.nrows; i++) {
	for(uint k = 0; k < mat_result.ncols; k++) {
	  const t_float s = mat_result.matrix[i][k];
	  if(isOf_interest(s) && (score > s) ) {
	    score = s;
	    reference_index = i;
	  }
	}
      }
    } else {
      assert(false); //! ie, then add support for this
    }
    _get_syntheticData(reference_index, cnt_features, list_count_data0); //! ie, consturct sysnthetic feature-set.
    //!
    //! De-allcoate the old matrix, and then allocate a new one:
    free__s_kt_matrix(&mat_result);
    if(isTo_appendHypoMatrix_toSampleDataSet) {
      assert(cnt_cases > 0);
      mat_result =  initAndReturn__s_kt_matrix(/*nrows=*/(cnt_cases  + mat_hypo.nrows), /*ncols=*/(cnt_metrics));
    } else {
      assert(cnt_cases > 0);
      mat_result =  initAndReturn__s_kt_matrix(/*nrows=*/(cnt_cases), /*ncols=*/(cnt_metrics));
    }
    //!
    //! Compute the entorpy:
    for(uint i = 0; i < cnt_cases; i++) {
      if(i == reference_index) { //! then we assume the data is the 'gold-reference-data':
	//! skip.
      } else {
	//! Get the scores: 
	s_kt_list_1d_uint_t list_count  = setToEmpty__s_kt_list_1d_uint_t(); 
	const char *prefix = _get_syntheticData(i, cnt_features, list_count); //! ie, consturct sysnthetic feature-set.
	set_stringConst__s_kt_matrix(&mat_result, /*index=*/i, prefix, /*addFor_column=*/false);
	//!
	//! Adjust the scores:
	for(uint row_id = 0; row_id < list_count.list_size; row_id++) {
	  if( (row_id < list_count_data0.list_size) && ( row_id < list_count.list_size) ){
	    const t_float h = (t_float)list_count_data0.list[row_id];
	    const t_float s1 = (t_float)list_count.list[row_id];
	    if((h != 0) && (s1 != 0) ) {
	      t_float s = ( (macro_max(h, s1) / macro_min(h, s1) ) );
	      assert(isnan(s) == false);
	      assert(s != T_FLOAT_MAX);
	      if(s < 0) {s = 0.0;}  // FIXME: v alidate this.
	      //! Step(): adjust wrt. negative values
	      list_count.list[row_id] = (uint)s;
	    } else { //!
	      list_count.list[row_id] = 0; // TODO: cosnier suign  adiffernet symbol.
	    }	      
	  } else { //!
	    list_count.list[row_id] = 0; // TODO: cosnier suign  adiffernet symbol.
	  }
	}
	//! Step(): compute
	_compute_entropy_all(self, list_count, mat_result, /*index_data=*/i);
	//!
	//! De-allocates:
	if(i != reference_index) { 
	  free__s_kt_list_1d_uint_t(&list_count);
	}
      }	
    }      
  }
  //!
  //! De-allocates:      
  free__s_kt_list_1d_uint_t(&list_count_data0);
  if(isTo_appendHypoMatrix_toSampleDataSet == false) {
    return mat_result; //! ie, then drop adding the hypotehsis to the resuls-tdata-set.
  }
  //!
  //! Apply for the hypothesis:
  for(uint i = 0; i < mat_hypo.nrows; i++) {
    const uint index = cnt_cases + i;
    //const uint index = cnt_metrics + i;
    assert(index < mat_result.nrows);
    //!
    //! Convert data to "unsigned int (uint)": a performance-optimziation strategy which redcues execution-time (eg, for lgoaritm computaitons).
    s_kt_list_1d_uint_t list_count = init__s_kt_list_1d_uint_t(mat_hypo.ncols);
    //! Step: adjust the valeus iot. avoid enagtive inptu-values.
    t_float val_min = T_FLOAT_MAX;
    for(uint k = 0; k < mat_hypo.ncols; k++) {
      const t_float s = mat_hypo.matrix[i][k];
      if(isOf_interest(s)) {
	val_min = macro_min(val_min, s);
      }
    }
    if(val_min < 0) {val_min *= -1;} //! ie, then adjust its sign
    else {val_min = 0;}
    for(uint k = 0; k < mat_hypo.ncols; k++) {
      list_count.list[k] = (uint)(val_min + mat_hypo.matrix[i][k]);
    }
    // assert(mat_hypo.ncols == mat_result.ncols);
    //! 
    //! Insert a row-header:
    const char *str = getAndReturn_string__s_kt_matrix(&mat_hypo, i, /*addFor_column=*/false);
    if(str && strlen(str)) {
      assert(strlen(str) < 1000);
      //char buff[3000];
      //sprintf(buff, "%s %s", getStr_norm(spec), str);
      // printf("\t(new-Best-score)\t \"%s\":%f\n", buff, score);
      // set_stringConst__s_kt_list_1d_string(&(self->score_best_str), /*index=*/index, buff);
      set_stringConst__s_kt_matrix(&mat_result, index, str, /*addFor_column=*/false);      
    }    
    //! Compute entropy for [list] x [entropy-metrics] 
    _compute_entropy_all(self, list_count, mat_result, /*index_data=*/index);
    //!
    //! De-allocates:
    free__s_kt_list_1d_uint_t(&list_count);    
  }
  
  return mat_result;
}


//! Comptue the relative difference bettween the 'best' hypothesis VS the correlated data.
static void _export_matrixRelativeToIdealHypo(s_tut_entropy3_t *self, s_kt_matrix_t &mat_corrHypo, s_kt_correlationMetric_t obj_metric, const char *str_result) {
  s_kt_list_1d_float_t h1 = init__s_kt_list_1d_float_t(mat_corrHypo.ncols);
  s_kt_list_1d_float_t h2 = init__s_kt_list_1d_float_t(mat_corrHypo.ncols);
  for(uint k = 0; k < mat_corrHypo.ncols; k++) {
    h1.list[k] =  (t_float)k;
    h2.list[k] = (t_float)(mat_corrHypo.ncols - k); //! ie, the inverse 
    //h1.list[k] = h2.list[k] = (t_float)k;
  }
  t_float h = 0; //! which holds the result of the comparison.
  bool is_ok = apply__rows_hp_distance(obj_metric, h1.list, h2.list, h1.list_size, &h);
  assert(is_ok);
  //!
  //! De-allocates:
  free__s_kt_list_1d_float_t(&h1);      
  free__s_kt_list_1d_float_t(&h2);      
  // s_kt_matrix_base_t mat_hypo = initAndReturn__s_kt_matrix(/*nrows=*/cnt_hypo, /*ncols=*/cnt_cases + cnt_hypo); //mat_corr.ncols);
  // const t_float h = vec_hyp.list[k];
  if((h == 0) || (false == isOf_interest(h))) {
    return; //! ie, as there is not point in contiuing.
  }

  assert(mat_corrHypo.nrows > 0);
  s_kt_matrix_t mat_rel = initAndReturn__copy__s_kt_matrix(&mat_corrHypo, /*isTo_updateNames = */true);
  //assert(vec_hyp.list != NULL);
  //assert(vec_hyp.list_size == mat_rel.ncols);
  //! Adjust the scores:
  for(uint i = 0; i < mat_rel.nrows; i++) {
    for(uint k = 0; k < mat_rel.ncols; k++) {
      const t_float s = mat_rel.matrix[i][k];
      mat_rel.matrix[i][k] = T_FLOAT_MAX; //! ie, to clear the old value.
      if((h != 0) && (isOf_interest(h))) {
	if((s != 0) && (isOf_interest(s))) {
	  mat_rel.matrix[i][k] = s/h; //! ie, the difference betweehn 'actuator' versus 'gold-truth'
	}
      }
    }
  }
  char buff[500]; sprintf(buff, "%s-relative", str_result);
  update_bestMetric(self->bestMetric, self, mat_rel, /*is_relative*/false);
  _export_toFile(self, mat_rel, /*file-name-result=*/buff);
  //! De-allocate:
  free__s_kt_matrix(&mat_rel);
}


static s_kt_matrix_t _correlateAndExport(s_tut_entropy3_t *self, s_kt_matrix_t &_mat_corr, s_kt_matrix_t &mat_hypo, s_kt_correlationMetric_t obj_metric, const char *str_result, const bool isTo_return = false) {
  //! Note: when correlating the entopry-measurements with the hypoithesies there is a requiremen for count(features['data-points']) == count('cnt-cases' + 'cnt-hypo'). This due to a 1:1 comparison-strategy between the feuatres (a '1:1' strategy which is used to avodi amigies associated with sparse pariwise sialmirty-metrics).
  // //! Tranpose the correlation-matrix.
  // s_kt_matrix_t mat_corr_transp = initAndReturn_transpos__s_kt_matrix(&mat_corr);
  bool is_allocated = false;
  s_kt_matrix_t mat_corr = _mat_corr;
  if(mat_corr.ncols != mat_hypo.ncols) {
    is_allocated = true;
    mat_corr = initAndReturn_transpos__s_kt_matrix(&_mat_corr); // FIXME: shoudl we isntead correlate the 'mat_hypo'?
    assert(mat_corr.ncols == mat_hypo.ncols); //! ie, a requiement for process to infer correlation.
  }
  // printf("mat_corr.ncols:%d == mat_hypo.ncols:%d\n", mat_corr.ncols , mat_hypo.ncols);
  assert(mat_corr.ncols == mat_hypo.ncols); //! ie, a requiement for process to infer correlation.
  

  //! Get shallow matrix-copies:
  s_kt_matrix_base_t corr_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_corr);
  s_kt_matrix_base_t hypo_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_hypo);
  assert(hypo_shallow.matrix == mat_hypo.matrix);
  assert(corr_shallow.matrix == mat_corr.matrix);
  s_kt_matrix_t mat_corrHypo = setToEmptyAndReturn__s_kt_matrix_t();
  s_kt_matrix_base_t mat_corrHypo_base = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_corrHypo);
  //! Compute correlation
  s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
  bool is_ok = apply__hp_distance(obj_metric, &corr_shallow, &hypo_shallow, &mat_corrHypo_base, hp_config);
  assert(is_ok);
  assert(mat_corrHypo_base.nrows > 0); //! ie, as we expect the operation was sucdcessful
  assert(mat_corrHypo_base.ncols > 0); //! ie, as we expect the operation was sucdcessful
  mat_corrHypo.nrows = mat_corrHypo_base.nrows;
  mat_corrHypo.ncols = mat_corrHypo_base.ncols;
  mat_corrHypo.matrix = mat_corrHypo_base.matrix;
  assert(mat_corrHypo.nrows > 0); //! ie, as we expect the operation was sucdcessful
  assert(mat_corrHypo.ncols > 0); //! ie, as we expect the operation was sucdcessful
  assert(mat_corrHypo_base.matrix != NULL);
  assert(mat_corrHypo.matrix != NULL);
  { //! Export:
    { //! Add headers:
      for(uint ind = 0; ind < mat_corrHypo.nrows; ind++) {
	const char *str = getAndReturn_string__s_kt_matrix(&mat_corr, ind, /*addFor_column=*/false);
	if(str && strlen(str)) {
	  set_stringConst__s_kt_matrix(&mat_corrHypo, ind, str, /*addFor_column=*/false);
	}
      }
      for(uint ind = 0; ind < mat_corrHypo.ncols; ind++) {
	const char *str = getAndReturn_string__s_kt_matrix(&mat_hypo, ind, /*addFor_column=*/false);
	if(str && strlen(str)) {
	  set_stringConst__s_kt_matrix(&mat_corrHypo, ind, str, /*addFor_column=*/true);
	}
      }	      
    }
    assert(strlen(str_result) < 100); //! ie, what we expect in below.
    char buff[500];
    if(obj_metric.metric_id == e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock) { //! then we asusme that hyptoehsis are compareds
      sprintf(buff, "%s_vsHypoKendall", str_result);
    } else {
      sprintf(buff, "%s_vsHypo", str_result);
    }
    _export_toFile(self, mat_corrHypo, /*file-name-result=*/buff);
    {
      //! Note: to simplify comparison seperate resutls-graphs are constructed for 'raw comparison' and 'relative to ideal expectation'.
      _export_matrixRelativeToIdealHypo(self, mat_corrHypo, obj_metric, buff);
    }
  }
  //! Update knowledge of gthe best metric:
  if(obj_metric.metric_id == e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock) { //! then we asusme that hyptoehsis are compareds
    update_bestMetric(self->bestMetric, self, mat_corrHypo, /*is_relative*/false);
  }
  //! De-allocate:
  if(isTo_return == false) {
    free__s_kt_matrix(&mat_corrHypo);
  }
  if(is_allocated) {
    free__s_kt_matrix(&mat_corr);
  }  
  return mat_corrHypo;
}

static s_kt_matrix_t _generate_matrix_hypo(s_tut_entropy3_t &self, const uint cnt_cases, const bool isTo_appendHypoMatrix_toSampleDataSet) {
  assert(cnt_cases != 0);
  assert(cnt_cases != UINT_MAX);
  const uint cnt_hypo = 4;
  s_kt_matrix_t mat_hypo = setToEmptyAndReturn__s_kt_matrix_t();
#ifdef config_computeRankAgainstAllDataComb_ // FIXME: remove "_"
  //! Then adjust the hypo-matrix to include all values ... 
  //assert(false) // FIXME: correclty set below:
      const uint cnt_innerForLoop = cnt_cases; //! which is used to repeat the raniing-strategy for each data-entry.
#else
  const uint cnt_innerForLoop = 1; //! which is used to repeat the raniing-strategy for each data-entry.
#endif
  if(isTo_appendHypoMatrix_toSampleDataSet) {
    assert(cnt_hypo > 0);
    mat_hypo = initAndReturn__s_kt_matrix(/*nrows=*/cnt_hypo, /*ncols=*/(cnt_cases*cnt_innerForLoop) + cnt_hypo); //mat_corr.ncols);
  } else {
    assert(cnt_hypo > 0);
    mat_hypo = initAndReturn__s_kt_matrix(/*nrows=*/cnt_hypo, /*ncols=*/cnt_cases*cnt_innerForLoop); //mat_corr.ncols);
  }
  // s_kt_matrix_t mat_hypo = initAndReturn__s_kt_matrix(/*nrows=*/mat_corr.ncols, /*ncols=*/cnt_hypo);
  for(uint hyp = 0; hyp < cnt_hypo; hyp++) {
    for(uint data_id = 0; data_id < mat_hypo.ncols; data_id++) {
      t_float score = 1;
      const char *prefix = NULL;
      if(hyp == 0) { score = (t_float)data_id; prefix = "correct";} //! ie, inicremental
      else if(hyp == 1) { score = (t_float)(cnt_hypo-data_id); prefix = "inverted";} //! ie, decremental
      else if(hyp == 2) { score = (t_float)(1); prefix = "fixed w/score=1";} //! ie, fxied-low
      else if(hyp == 3) { score = (t_float)(1000); prefix = "fixed w/score=1000";} //! ie, fixed-high
      else {assert(false);} //! ie, then add support for this.
      //!
      assert(hyp < mat_hypo.nrows);
      assert(data_id < mat_hypo.ncols);
      if(data_id == 0) { //! Then set the data-id:
	char str[100]; sprintf(str, "hypo:%u -- %s", hyp, prefix);
	set_stringConst__s_kt_matrix(&mat_hypo, hyp, str, /*addFor_column=*/false);
      }
      mat_hypo.matrix[hyp][data_id] = score;
      //mat_hypo.matrix[data_id][hyp] = score;
    }
  }


  //! -------------------
  //if(false)
  { //! Hypothesis-ranking. Compare  each of the rank-matrix-rows to   ['correct hypotehsis', 'inverted hypotehsis',].
    //! Idea: Identify the impact of NOT using relative difference (before computing results).
    
    //! Correlate mesurements VS hypothesis.
    assert(mat_hypo.nrows > 0);
    s_kt_matrix_t mat_hypo_ranked = initAndReturn_ranks__s_kt_matrix(&mat_hypo);
    {
      //! Note: Use "Kendall's Tau" to get the number of steps neccesary to move 'data' to the hypothesis (ie, an itnitutvie measure of simlairty).
      s_kt_correlationMetric_t  obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank);
      //! Apply:
      // printf("result:{nrows:%u, ncols:%u}, hypo:{nrows:%u, ncols:%u}\n", mat_transp_rank.nrows, mat_transp_rank.ncols, mat_hypo.nrows, mat_hypo.ncols);
      const char *str_result = "res_hypoVShypo";
      _correlateAndExport(&self, mat_hypo, mat_hypo, obj_metric, str_result);
      //! Compute the ranks seperately for each metric:
      str_result = "res_hypoVShypoRanked";
      _correlateAndExport(&self, mat_hypo, mat_hypo_ranked, obj_metric, str_result);
      //! -------------------
    }
    {
      //! Note: Use "Kendall's Tau" to get the number of steps neccesary to move 'data' to the hypothesis (ie, an itnitutvie measure of simlairty).
      s_kt_correlationMetric_t  obj_metric =     initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank); //! ie, Spearman
      //! Apply:
      // printf("result:{nrows:%u, ncols:%u}, hypo:{nrows:%u, ncols:%u}\n", mat_transp_rank.nrows, mat_transp_rank.ncols, mat_hypo.nrows, mat_hypo.ncols);
      const char *str_result = "res_spearman-hypoVShypo";
      _correlateAndExport(&self, mat_hypo, mat_hypo, obj_metric, str_result);
      //! Compute the ranks seperately for each metric:
      str_result = "res_spearman-hypoVShypoRanked";
      _correlateAndExport(&self, mat_hypo, mat_hypo_ranked, obj_metric, str_result);
      //! -------------------
    }
    {
      //! Note: Use "Kendall's Tau" to get the number of steps neccesary to move 'data' to the hypothesis (ie, an itnitutvie measure of simlairty).
      s_kt_correlationMetric_t  obj_metric =     initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none); //! ie, Pearson
      //! Apply:
      // printf("result:{nrows:%u, ncols:%u}, hypo:{nrows:%u, ncols:%u}\n", mat_transp_rank.nrows, mat_transp_rank.ncols, mat_hypo.nrows, mat_hypo.ncols);
      const char *str_result = "res_pearson-hypoVShypo";
      _correlateAndExport(&self, mat_hypo, mat_hypo, obj_metric, str_result);
	//! Compute the ranks seperately for each metric:
      str_result = "res_pearson-hypoVShypoRanked";
      _correlateAndExport(&self, mat_hypo, mat_hypo_ranked, obj_metric, str_result);
      //! -------------------
    }            
    //! De-allocate:
    free__s_kt_matrix(&mat_hypo_ranked);
  }
  
  return mat_hypo;
}


//static const uint cnt_entropyClasses = 1; // FIXME: remvoe thsi, and include belwo, when we have reoslved coneptaul issues wrt. merging of entropy-metric-classes.
static const uint cnt_entropyClasses = 8; //! ie, '5' different entropy-class-assumptions.


static s_kt_list_1d_float_t _computeCorrVector_rowGroup(s_tut_entropy3_t *self, const s_kt_matrix_t &mat_features, const s_kt_list_1d_uint_t &list_rowId, uint reference_index) {
//static s_kt_list_1d_float_t _computeCorrVector_rowGroup(s_tut_entropy3_t *self, s_kt_matrix_t &mat_result, const s_kt_matrix_t &mat_features, const s_kt_list_1d_uint_t &list_rowId) {
  /**
     @brief construct a correlation vector from a group of rows "list_rowId".
     @return the correlation-scores to the feature-vector in question.
     @remarks to best undertand this funciton note that it is called from:
     -- static void _computeCorrMatrix_andExport(..)
     -- main_tut_entropy_3(...)
   **/
  // FIXME: make use of this function.
  assert(list_rowId.current_pos > 0);
  assert(list_rowId.list_size > 0);
  //! Compute simliaites across all data-points inside the same 'metric-group':
  //! Step(1): initiate a new entropy-feature-matrix:
  // s_kt_matrix_t mat_hyp =  initAndReturn__s_kt_matrix(/*nrows=*/(1), /*ncols=*/(mat_features.ncols));
  // s_kt_matrix_t mat_tmp =  initAndReturn__s_kt_matrix(/*nrows=*/(list_rowId.current_pos), /*ncols=*/(mat_features.ncols));
  // FIXME: below seems to become wrong wrt. imsiones of the reuslt-matirx: ...
  //s_kt_matrix_t mat_hyp =  initAndReturn__s_kt_matrix(/*nrows=*/(mat_features.ncols), /*ncols=*/(1));
  //s_kt_matrix_t mat_tmp =  initAndReturn__s_kt_matrix(/*nrows=*/(mat_features.ncols), /*ncols=*/(list_rowId.current_pos));
  //s_kt_matrix_t mat_tmp =  initAndReturn__s_kt_matrix(/*nrows=*/(), /*ncols=*/());
  s_kt_matrix_t mat_tmp =  initAndReturn__s_kt_matrix(/*nrows=*/(1), /*ncols=*/(list_rowId.current_pos));  
  assert(mat_features.nrows > 0);
  s_kt_matrix_t mat_hyp =  initAndReturn__s_kt_matrix(/*nrows=*/(mat_features.ncols), /*ncols=*/(list_rowId.current_pos));
  //! 
  // FIXME: add support for different selection-ctieria wrt. reference-index (eg, "median", "rank", ..., "k-means++", ...). Then make use of the same strategies when constructing a reference-vector (eg, when comparing images). 
  //uint reference_index = 0;
  
  //! Step: explore all data-id points:
  for(uint i = 0; i < list_rowId.current_pos; i++) {
    const uint metric_id = list_rowId.list[i];
    //! Update the reference-index: 
    mat_tmp.matrix[0][i] = mat_features.matrix[metric_id][reference_index]; //! ie, unify the data-points?
    // if(reference_index == i) {continue;}
    assert(metric_id < mat_features.nrows);
    for(uint data_id = 0; data_id < mat_features.ncols; data_id++) {
      mat_hyp.matrix[data_id][i] = mat_features.matrix[metric_id][data_id]; //! ie, unify the data-points?
      //mat_tmp.matrix[data_id][i] = mat_features.matrix[metric_id][data_id]; //! ie, unify the data-points?
      if(data_id == reference_index) {
	// mat_hyp.matrix[reference_index][i] = mat_features.matrix[metric_id][data_id]; //! ie, unify the data-points?
	//mat_hyp.matrix[data_id][0] = mat_features.matrix[metric_id][data_id]; //! ie, unify the data-points?
      }
    }
  }
  //! 
  if(self->enum_normAfterMetricMerge != e_kt_normType_undef) {
    //! Step(): apply normalization
    //! Note: the data-norm strategy may be nessary iot. avoid giving some metrics a wrongly/erronous preference/weight.
    //! Apply for main-matrix: 
    assert(mat_tmp.nrows > 0);
    s_kt_matrix_t mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_tmp, self->enum_normAfterMetricMerge);
    free__s_kt_matrix(&mat_tmp);
    mat_tmp = mat_norm;
    //! Apply for hypo-matrix:
    assert(mat_hyp.nrows > 0);
    mat_norm = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&mat_hyp, self->enum_normAfterMetricMerge);
    free__s_kt_matrix(&mat_hyp);
    mat_hyp = mat_norm;    
    // FIXME: ... use: self->enum_normAfterMetricMerge;
    // FIXME: ... update our 'global-config' with specifciaont of which data-norm strategy to use ... then add a for-loop to construct for all cases.   
  }
  //! 
  //! Step(): Correlatate the data: 
  s_kt_matrix_base_t mat_result = initAndReturn__empty__s_kt_matrix_base_t();  
  {
    s_kt_matrix_base_t hyp_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_hyp);
    s_kt_matrix_base_t tmp_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_tmp);
    //! Step(): Intiate the strucutres nesssary fro teh 
    s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
    //! Compute correlation
    bool is_ok = apply__hp_distance(/*obj_metric=*/self->corrMetric_betweenSamples, &tmp_shallow, &hyp_shallow, &mat_result, hp_config);
    assert(is_ok);
    //!
    //! What we expect: 
    assert(mat_result.nrows > 0); //! ie, as we expect the operation was sucdcessful
    assert(mat_result.ncols > 0); //! ie, as we expect the operation was sucdcessful
    // FIXME: validate correctness of below assertion/assumption:
    assert(mat_result.nrows == 1);
    // printf("mat_result:{%u, %u}, mat_hyp:{%u, %u}, mat_tmp:{%u, %u}, mat_features:{%u, %u}\n", mat_result.nrows, mat_result.ncols, mat_hyp.nrows, mat_hyp.ncols, mat_tmp.nrows, mat_tmp.ncols, mat_features.nrows, mat_features.ncols);
    assert(mat_result.nrows == mat_tmp.nrows);
    assert(mat_result.ncols == mat_hyp.nrows);
    assert(mat_result.ncols == mat_features.ncols); //! ie, as we expect tha data to be stored in the 'columns'
  }
  //! Step(): Cosntuct hte feautre-set, de-allcoate result-data-structs, and then return:
  s_kt_list_1d_float_t list_corr = init__s_kt_list_1d_float_t(mat_result.ncols);
  for(uint k = 0; k < mat_result.ncols; k++) { list_corr.list[k] = mat_result.matrix[0][k];} //! ie, the copy-operation.
  //! De-allocate:
  free__s_kt_matrix(&mat_tmp);
  free__s_kt_matrix(&mat_hyp);
  free__s_kt_matrix_base_t(&mat_result);
  //! @return
  return list_corr; 
}


static uint _get_index_entropyMetric(const uint search_entropy, const uint search_postScale) {
  uint pos = 0;
    for(uint entropy_index = 0; entropy_index < e_kt_entropy_genericType_undef; entropy_index++) {
      for(uint postScaling = 0; postScaling < 2; postScaling++) {
	if((entropy_index == search_entropy) && (postScaling == search_postScale) ) {return pos;}
	pos += 1;
      }
    }
    assert(false);  //! as we id NOT expect reachign this exepc-point
    return pos;
}

static s_kt_list_1d_uint_t _get_entropyClass(const uint entropy_classIndex, const uint cnt_metrics) {
  s_kt_list_1d_uint_t list_toCompare = init__s_kt_list_1d_uint_t(cnt_metrics); // mat.nrows);
  /**
     @return the set of classes assoiated to the metric in question.
   **/
  // if(list_toCompare.current_pos != 0) {list_isTo_use.list[pos] = false;} /* ie, Init: by def. not use an 'already evlauated comb' */ 
#define _add(entropy, postScale) ({ \
	const uint pos = _get_index_entropyMetric(/*search_entropy=*/entropy, /*search_postScale=*/postScale); \
	push__s_kt_list_1d_uint_t(&list_toCompare, pos);}); //! a macro used to simplify future coe-changes.
	//list_toCompare.push(pos)}); //! a macro used to simplify future coe-changes.
    //! :
    // for(uint metric_id = 0; metric_id < mat.nrows; metric_id++) { list_isTo_use.list[metric_id] = false;} //!  ie use all.
    //!
    //!    
    // FIXME: below is pure guess-work: try constructing differnet evla-cases based onc ocnrete observations (from the 'real' and 'sytnethci' data):
    if(entropy_classIndex == 1) {
      _add(e_kt_entropy_genericType_ML_Shannon, false);
      _add(e_kt_entropy_genericType_Gini, false);
      _add(e_kt_entropy_genericType_HCDT, false);
      _add(e_kt_entropy_genericType_HHI, false);
    } else if(entropy_classIndex == 2) {
      _add(e_kt_entropy_genericType_ML_Shannon, true);
      _add(e_kt_entropy_genericType_Gini, true);
      _add(e_kt_entropy_genericType_HCDT, true);
      _add(e_kt_entropy_genericType_HHI, true);
    } else if(entropy_classIndex == 3) {
      _add(e_kt_entropy_genericType_ML_Shannon, false);
      _add(e_kt_entropy_genericType_Gini, false);
      _add(e_kt_entropy_genericType_ML_Shannon, true);
      _add(e_kt_entropy_genericType_Gini, true);
      _add(e_kt_entropy_genericType_HCDT, false);
      _add(e_kt_entropy_genericType_HHI, false);                        
    } else if(entropy_classIndex == 4) {
      _add(e_kt_entropy_genericType_HCDT, false);                        
      _add(e_kt_entropy_genericType_Renyi, false);                        
      _add(e_kt_entropy_genericType_PT_1, false);                        
      _add(e_kt_entropy_genericType_PT_2, false);                        
      _add(e_kt_entropy_genericType_AD, false);
      _add(e_kt_entropy_genericType_HHI, false);                        
    } else if(entropy_classIndex == 5)  {     
      _add(e_kt_entropy_genericType_HCDT, true);                        
      _add(e_kt_entropy_genericType_Renyi, true);                        
      _add(e_kt_entropy_genericType_PT_1, true);                        
      _add(e_kt_entropy_genericType_PT_2, true);                        
      _add(e_kt_entropy_genericType_AD, true);
      _add(e_kt_entropy_genericType_HHI, true);                        
    } else if(entropy_classIndex == 6)  {
      _add(e_kt_entropy_genericType_HCDT, false);                        
      _add(e_kt_entropy_genericType_Renyi, false);                        
      _add(e_kt_entropy_genericType_PT_1, false);                        
      _add(e_kt_entropy_genericType_PT_2, false);                        
      _add(e_kt_entropy_genericType_AD, false);
      _add(e_kt_entropy_genericType_HHI, false);                        
      _add(e_kt_entropy_genericType_HCDT, true);                        
      _add(e_kt_entropy_genericType_Renyi, true);                        
      _add(e_kt_entropy_genericType_PT_1, true);                        
      _add(e_kt_entropy_genericType_PT_2, true);                        
      _add(e_kt_entropy_genericType_AD, true);
      _add(e_kt_entropy_genericType_HHI, true);                        
    } else if(entropy_classIndex == 7)  {           
       _add(e_kt_entropy_genericType_ML_Shannon, false);
      _add(e_kt_entropy_genericType_Gini, false);
      _add(e_kt_entropy_genericType_ML_Shannon, true);
      _add(e_kt_entropy_genericType_Gini, true);
      _add(e_kt_entropy_genericType_HCDT, false);                        
      _add(e_kt_entropy_genericType_Renyi, false);                        
      _add(e_kt_entropy_genericType_PT_1, false);                        
      _add(e_kt_entropy_genericType_PT_2, false);                        
      _add(e_kt_entropy_genericType_AD, false);
      _add(e_kt_entropy_genericType_HHI, false);                        
      _add(e_kt_entropy_genericType_HCDT, true);                        
      _add(e_kt_entropy_genericType_Renyi, true);                        
      _add(e_kt_entropy_genericType_PT_1, true);                        
      _add(e_kt_entropy_genericType_PT_2, true);                        
      _add(e_kt_entropy_genericType_AD, true);
      _add(e_kt_entropy_genericType_HHI, true);     
    } else {
      assert(false); //! ie, then add support for this
    }
    return list_toCompare;
#undef _add      
}

static void _computeCorrMatrix_andExport(s_tut_entropy3_t &self, const char *_str_result, const char *_str_result_rank, s_kt_matrix_t &mat, s_kt_matrix_t &mat_hypo, s_kt_matrix_t &mat_result, const bool isToCompute_entropy_classIndex) {
  //!
  //! Cosntruct local-specific input-strings: 
  char str_result[3000];      sprintf(str_result,      "%s-entC-%s", _str_result,      (isToCompute_entropy_classIndex) ? "true" : "false");
  char str_result_rank[3000]; sprintf(str_result_rank, "%s-entC-%s", _str_result_rank, (isToCompute_entropy_classIndex) ? "true" : "false");

  // assert(entropy_classIndex < cnt_entropyClasses);
  //!
  //! Idneityf which row to export:
  uint biggest_metric_id = UINT_MAX; //! whcih is used to drop metrics above a 
  // s_kt_list_1d_uint_t list_isTo_use  = init__s_kt_list_1d_uint_t(mat.nrows);
  // s_kt_list_1d_uint_t list_toCompare = init__s_kt_list_1d_uint_t(mat.nrows);
  // for(uint metric_id = 0; metric_id < mat.nrows; metric_id++) { list_isTo_use.list[metric_id] = true;} //!  ie use all.
  /* if(entropy_classIndex == 0) { */
  /*   ; //! then do nothing. */
  /* } else { */
  /* } */
  //!
  //! 
  s_kt_matrix_t mat_corr = setToEmptyAndReturn__s_kt_matrix_t();
  if(isToCompute_entropy_classIndex == false) {
    assert(mat.nrows > 0);
#ifdef config_computeRankAgainstAllDataComb_
    mat_corr = initAndReturn__s_kt_matrix(/*nrows=*/mat.nrows, /*ncols=*/mat.ncols * cnt_cases); //! ie, store data-pints for all combinations of refrence-data-frams.
    //printf("\t(computeCorrMatrix_andExpo)\t rows:%u, mat.cols:%u, cnt_cases:%u => mat_corr.ncols=%u, mat_hypo.ncols:%u\n", mat.nrows, mat.ncols, cnt_cases, mat_corr.ncols, mat_hypo.ncols);
    assert(mat_corr.ncols == mat_hypo.ncols); //! ie, what we expect.
#else
      mat_corr = initAndReturn__s_kt_matrix(/*nrows=*/mat.nrows, /*ncols=*/mat.ncols);
#endif
    //!
    //! Compute: Seperately for each metric compute the relative difference:
    for(uint metric_id = 0; metric_id < mat.nrows; metric_id++) {
      // if(list_isTo_use.list[metric_id] == false) {continue;} //! ie, then drop evlaiting this
      { //! Set the string:
	const char *str = getAndReturn_string__s_kt_matrix(&mat_result, metric_id, /*addFor_column=*/true);
	if(str && strlen(str)) {
	  set_stringConst__s_kt_matrix(&mat_corr, metric_id, str, /*addFor_column=*/false);
	}
      }
#ifdef config_computeRankAgainstAllDataComb_
      for(uint ref_id = 0; ref_id < cnt_cases; ref_id++) //! ie, as we then iterate through all data-cases.
#else
      const uint ref_id = 0; //! ie, the first index, eg, as seen in the John-paper.
#endif
    {
      const t_float v1 = mat.matrix[metric_id][ref_id]; //! which for the 'entropy-class' strategy becomes: ...
      if(isOf_interest(v1) && (v1 != 0) ) {
	for(uint data_id = 0; data_id < mat.ncols; data_id++) {
	  const t_float v2 = mat.matrix[metric_id][data_id]; //! which for the 'entropy-class' strategy becomes: "v2 = mat.matrix[metric_id(i) ... metric_id(k)][data_id]"
	  if(isOf_interest(v2) && (v2 != 0) ) {
	    //if(self.corrMetric_betweenSamples.metric_id ==  e_kt_correlationFunction_undef) {
	    const uint pos_col = (ref_id * cnt_cases) + data_id;
	    assert(pos_col < mat_corr.ncols);
	    mat_corr.matrix[metric_id][pos_col] = v1/v2; //! which for the 'entropy-class' strategy becomes d(v1, v2).
	    //mat_corr.matrix[metric_id][data_id] = v1/v2; //! which for the 'entropy-class' strategy becomes d(v1, v2).
	    //} else {
	    // FIXME: add something.
	    //}
	  }
	}
      }
    }
    }
  } else { //! then we compute for a subset
    assert(cnt_entropyClasses > 0);
#ifdef config_computeRankAgainstAllDataComb_
    mat_corr = initAndReturn__s_kt_matrix(/*nrows=*/cnt_entropyClasses, /*ncols=*/mat.ncols * cnt_cases); //! ie, store data-pints for all combinations of refrence-data-frams.
    // printf("\t(computeCorrMatrix_andExpo)\t rows:%u, mat.cols:%u, cnt_cases:%u => mat_corr.ncols=%u, mat_hypo.ncols:%u\n", mat.nrows, mat.ncols, cnt_cases, mat_corr.ncols, mat_hypo.ncols);
    assert(mat_corr.ncols == mat_hypo.ncols); //! ie, what we expect.
#else
    mat_corr = initAndReturn__s_kt_matrix(/*nrows=*/cnt_entropyClasses, /*ncols=*/mat.ncols);
    //mat_corr = initAndReturn__s_kt_matrix(/*nrows=*/mat.nrows, /*ncols=*/mat.ncols);
#endif
    //
    // s_kt_list_1d_uint_t list_toCompare = init__s_kt_list_1d_uint_t(mat.nrows);
    //! 
    //! Compute correlation-matrix
    for(uint entropy_classIndex = 1; entropy_classIndex < cnt_entropyClasses; entropy_classIndex++) {
      assert(entropy_classIndex < mat_corr.nrows);
      //! Step(): Get the metrics to be used for 'class = entropy_classIndex' in question: 
      s_kt_list_1d_uint_t list_rowId = _get_entropyClass(entropy_classIndex, /*cnt-metrics=*/mat.nrows);
      //! Step(): construct a feature-matrix: each feature represents the correlation between 'norm(matrix[..metrics..][center])' versus 'norm(matrix[..metrics..][...])'. Apply pariwise simlairty-metric:
      // FIXME: what are the features to consist of (ie, what are we to compare with)? <-- 'data-values'. To exemplify, when evlauating all entorpy-metircs sperately we construct a matrix "mat_corr.matrix[metric_id][data_id] = v1/v2;". However, when merging metrics, each 'merge-step' implies that 'metric-id' = fixed = index(0). Then a new challenge arise wrt. how merging the  
      // FIXME: does b elow produce correct results? <-- interested in getting [metric=row=1], and cols=data
      loint pos_insert = 0;
#ifdef config_computeRankAgainstAllDataComb_ //! then we are interested in exploring the effects of all reference-points:
      for(uint reference_index = 0; reference_index < cnt_cases; reference_index++) 
#else
      const uint reference_index = 0;
#endif
      {
	s_kt_list_1d_float_t list_corr = _computeCorrVector_rowGroup(&self, /*mat_features=*/mat, list_rowId, reference_index);      
	// printf("listSize:%u, mat_corr.ncols:%u, cnt_cases:%u\n", list_corr.list_size, mat_corr.ncols, cnt_cases);
	assert(list_corr.list_size == cnt_cases);
	assert(pos_insert < mat_corr.ncols);
	//assert(list_corr.list_size == mat_corr.ncols);
	//! Merge rehstuls into the 'main-metric':
	for(uint i = 0; i < list_corr.list_size; i++) {
	  assert(pos_insert < mat_corr.ncols);
	  mat_corr.matrix[entropy_classIndex][pos_insert] = list_corr.list[i];
	  pos_insert += 1;
	}
	//!
	//! De-allocates:
	free__s_kt_list_1d_float_t(&list_corr);
      }
      //! 
      //! Set a descriptive string-reference (for both columns and rows):
      { //! Set the string:
	char str[3000]; sprintf(str, "metricGroup:%u", entropy_classIndex);
	set_stringConst__s_kt_matrix(&mat_corr, entropy_classIndex, str, /*addFor_column=*/false);
      }
      //!
      //! De-allocates:
      free__s_kt_list_1d_uint_t(&list_rowId);
      // free__s_kt_list_1d_uint_t(&list_toCompare);
    }
    // mat_corr = _correlateAndExport(&self, mat_features, mat_features, obj_metric, str_result_buff, isTo_return = true);
    // printf("(after-entopry-vector-ops)\t dim(%u, %u), hypo.ncols=%u\n", mat_corr.nrows, mat_corr.ncols, mat_hypo.ncols);
    assert(mat_corr.ncols == mat_hypo.ncols); //! ie, what we expect.
    // mat_corr = initAndReturn__s_kt_matrix(/*nrows=*/1, /*ncols=*/mat.ncols); //! ie, as we unify metrics into one step.
    assert(mat_corr.ncols == mat_hypo.ncols); //! ie, what we expect.


    //! Step(3): de-allcoate:
    // free__s_kt_matrix(&mat_features);
  }

  //! 
  //! Export:
   _export_toFile(&self, mat_corr, /*file-name-result=*/str_result);
 //! Compute the ranks seperately for each metric:
   assert(mat_corr.nrows > 0);
  s_kt_matrix_t mat_corr_rank = initAndReturn_ranks__s_kt_matrix(&mat_corr);
  _export_toFile(&self, mat_corr_rank, /*file-name-result=*/str_result_rank);
  //! 
  //! -------------------
  { //! Hypothesis-ranking. Compare  each of the distance-matrix-rows to   ['correct hypotehsis', 'inverted hypotehsis',].
    //! Idea: Identify best-matching hypotehhis: for each [metric][..] identify how close it is to hyptoehsis [...].
    
    //! Correlate mesurements VS hypothesis.
    {
      //! Note: Use "Kendall's Tau" to get the number of steps neccesary to move 'data' to the hypothesis (ie, an itnitutvie measure of simlairty).
      s_kt_correlationMetric_t  obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank);
      //! Apply:
      _correlateAndExport(&self, mat_corr, mat_hypo, obj_metric, str_result);
    }
    {
      s_kt_correlationMetric_t  obj_metric =     initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank); //! ie, Spearman
      char buff[2000];
      sprintf(buff, "Spearman-%s", str_result);
      _correlateAndExport(&self, mat_corr, mat_hypo, obj_metric, buff);
    }
    {
      s_kt_correlationMetric_t  obj_metric =     initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none); //! ie, Pearson
      char buff[2000];
      sprintf(buff, "Pearson-%s", str_result);
      _correlateAndExport(&self, mat_corr, mat_hypo, obj_metric, buff);
    }	  
  }
  //! -------------------
  //! 
  //! -------------------
  { //! Correlate metric-depdency: compute MINE correlation (for both the 'rank' and 'deviation' matrix). Use these results to infer/evaluate dependecy between metrics, hence as a selection-ctieria when selecting different combaintions of metics (to be as a signature-metric).
    //! Note: use "MINE" to get the mutual information between the measurements.
    s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_MINE_mic, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
    //! Apply:
    char str_file[1000];
    assert(str_result);
    assert(strlen(str_result));
    sprintf(str_file, "%s-correlation_MINE", str_result);
    _correlateAndExport(&self, mat_corr, mat_corr, obj_metric, str_file);
  }
  //! -------------------		
  //! De-allocate:
  free__s_kt_matrix(&mat_corr);
  free__s_kt_matrix(&mat_corr_rank);
}

static void main_tut_entropy_3(s_tut_entropy3_t &self, s_kt_matrix_t &mat_result, s_kt_matrix_t &mat_hypo) {
//static void main_tut_entropy_3(s_tut_entropy3_t &self) {
//static void main_tut_entropy_3(s_tut_entropy3_t &self,  s_tut_entropy_bestMetric_t *bestMetric) {



  //! 
  { //! Apply logics for export:
    assert(mat_result.nrows > 0);
    s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_result);
    //! Validate the transposion:
    assert(mat_transp.ncols == mat_result.nrows);
    assert(mat_transp.nrows == mat_result.ncols);
    //! Compute the ranks seperately for each metric:
    s_kt_matrix_t mat_transp_rank = initAndReturn_ranks__s_kt_matrix(&mat_transp);
    //! Fetches a nromalized version:
    s_kt_matrix_t mat_transp_norm = initAndReturn__copy__normalized__s_kt_matrix_t(&mat_transp);
    //! 
    //! Export:
    _export_toFile(&self, mat_result, /*file-name-result=*/"data");
    _export_toFile(&self, mat_transp, /*file-name-result=*/"data_transp");
    _export_toFile(&self, mat_transp_rank, /*file-name-result=*/"data_transp_ranked");
    _export_toFile(&self, mat_transp_norm, /*file-name-result=*/"data_transp_norm");
    _export_dev_toFile(&self, mat_transp, "data_transp_dev");
    
    //! ---------------------------------------
    //!
    //!    
    { //! Correlate the result-entropy-matrix. For each "metric,data" combination compute the distance between "data(0) VS data(1...)".
      //! Note: ivnestiages both the 'raw data' and the 'normlized data', which is motivaed by the observation/interest in (a) observing the impact of none-smoothed data, and (b) the impact of smoothed/nromalsied ata (where the nromaison-proceudre makes use of "(value-average)/(max-averate)").
      for(uint use_norm = 0; use_norm < 2; use_norm++) {
	s_kt_matrix_t mat = mat_transp;
	const char *str_result = "relDiff";
	const char *str_result_rank = "relDiffR";
	if(use_norm) {
	  mat = mat_transp_norm;
	  str_result = "relDiff_norm";
	  str_result_rank = "relDiff_normR";
	}
	// assert( (cnt_metrics) == (mat.nrows)); //! ie, what we expect in below:
	//assert((cnt_metrics + mat_hypo.nrows) == (mat.nrows)); //! ie, what we expect in below:
	//!
	//! Compute correlation-matrix
#ifndef tut4_notUseEntropyMetricsDirectly
	for(uint isToCompute_entropy_classIndex = 0; isToCompute_entropy_classIndex < 2; isToCompute_entropy_classIndex++)
#else
	  const uint isToCompute_entropy_classIndex = 0;
#endif
	  {
	  //for(uint entropy_classIndex = 0; entropy_classIndex < cnt_entropyClasses; entropy_classIndex++) {
	  _computeCorrMatrix_andExport(self, str_result, str_result_rank, mat, mat_hypo, mat_result, isToCompute_entropy_classIndex);
	}
      }
    }
    //! ---------------------------------------
    //!
    //! De-allocate
    free__s_kt_matrix(&mat_transp);
    //! De-allocate
    free__s_kt_matrix(&mat_transp_norm);    
    //! De-allocate
    free__s_kt_matrix(&mat_transp_rank);  
  }

}

#if config_useSyntDataSets == 0
static t_float _get_scalar_fromHisto(const s_ktType_tripleUChar_t obj_rgb, const e_tut3_rgbToScalar_t type_conversion) {
  s_hp_image_pixel_t pix = init__s_hp_image_pixel_t();
  pix.RGB[0] = obj_rgb.head;
  pix.RGB[1] = obj_rgb.tail;
  pix.RGB[2] = obj_rgb.score;
  t_float hue = 0;
  t_float saturation = 0;
  t_float value = 0;
  RGBtoHSV__s_hp_image_pixel_t_(pix, &hue, &saturation, &value);
  //!
  //! Step(): Select + apply the scalar transformation to use:
  t_float valTo_insert = T_FLOAT_MAX;
  if(type_conversion == e_tut3_rgbToScalar_min) {
    valTo_insert = (t_float)macro_min(pix.RGB[0], macro_min(pix.RGB[1], pix.RGB[2]));
  } else if(type_conversion == e_tut3_rgbToScalar_max) {
    valTo_insert = (t_float)macro_max(pix.RGB[0], macro_max(pix.RGB[1], pix.RGB[2]));
  } else if(type_conversion == e_tut3_rgbToScalar_sum) {
    valTo_insert = (t_float)pix.RGB[0] + (t_float)pix.RGB[1] + (t_float)pix.RGB[2];
  } else if(type_conversion == e_tut3_rgbToScalar_Hue) {	  	  
    if(hue != T_FLOAT_MAX) {
      valTo_insert = hue;
    }
  } else if(type_conversion == e_tut3_rgbToScalar_Saturation) {	  	  
    if(saturation != T_FLOAT_MAX) {
      valTo_insert = saturation;
    }
  } else if(type_conversion == e_tut3_rgbToScalar_mult_Hue_Saturation) {	  	  
    if( (hue != T_FLOAT_MAX) && (saturation!= T_FLOAT_MAX) ) {
      valTo_insert = hue * saturation;
    }
  } else if(type_conversion == e_tut3_rgbToScalar_maxHS_div_minHS) {	  	  
    if( (hue != T_FLOAT_MAX) && (saturation!= T_FLOAT_MAX) ) {
      t_float num = macro_max(hue, saturation);
      t_float dNum = macro_min(hue, saturation);
      if( (num != 0) && (dNum != 0) ) {
	valTo_insert = num / dNum;
      }
    }
  } else {assert(false);} //! ie, then add support for this enum.
  return valTo_insert;
}

static void _load_dataFiles_applyRGB_merge(const e_tut3_rgbToScalar_t type_conversion, const e_tut3_histogram_t enum_norm) {
  for(uint i = 0; i < tut3_arr_fileNamesInput_size; i++) {
    const char *fileName = tut3_arr_fileNamesInput[i];
    assert(fileName);
    assert(strlen(fileName));
    //! Step(): Load RGB-data:
    // s_kt_list_1d_tripleUChar_t obj_rgb = initFromFile__s_kt_list_1d_tripleUChar_t(fileName);
    s_kt_list_1d_tripleUChar_t obj_rgb = initFromFile__sizeKnown_s_kt_list_1d_tripleUChar_t(fileName, /*cnt-rows=*/4620350);
    //s_kt_list_1d_tripleUChar_t obj_rgb = init__s_kt_list_1d_tripleUChar_t(4620350);
    assert(obj_rgb.list_size > 0);
    // printf("(after-parsing)\t read %u lines, at %s:%d\n", obj_rgb.list_size, __FILE__, __LINE__);
    //!
    //! Step(): Convert to a scalar, and then insert into a given list: 
    s_kt_list_1d_float_t tmp = init__s_kt_list_1d_float_t(obj_rgb.list_size);
    for(uint k = 0; k < obj_rgb.list_size; k++) {
      const t_float valTo_insert = _get_scalar_fromHisto(obj_rgb.list[k], type_conversion);
      //!
      //! Step(): Add the value:
      if(valTo_insert != T_FLOAT_MAX) {
	tmp.list[k] = valTo_insert;
      }
    }
    //!
    //! Step():  De-allocate:
    free__s_kt_list_1d_tripleUChar_t(&obj_rgb);
    //! Step(): Update the bloal data-set:
    if(enum_norm != e_tut3_histogram_undef) { //! then transalte the meausmenets into a histogram.
      s_kt_list_1d_float_t res_histo = convertTo_histo(tmp, enum_norm);
      free__s_kt_list_1d_float_t(&tmp);
      tmp = res_histo;
    }
    //! 
    list_scalarImages[i] = tmp;
  }
}

static void load_dataFiles_applyRGB_merge_toMatrix(const e_tut3_rgbToScalar_t type_conversion, const e_tut3_histogram_t enum_norm) {
  for(uint i = 0; i < tut3_arr_fileNamesInput_size; i++) {
    const char *fileName = tut3_arr_fileNamesInput[i];
    assert(fileName);
    assert(strlen(fileName));
    //! Step(): Load RGB-data:
    // s_kt_list_1d_tripleUChar_t obj_rgb = initFromFile__s_kt_list_1d_tripleUChar_t(fileName);
    s_kt_list_1d_tripleUChar_t obj_rgb = initFromFile__sizeKnown_s_kt_list_1d_tripleUChar_t(fileName, /*cnt-rows=*/4620350);
    //s_kt_list_1d_tripleUChar_t obj_rgb = init__s_kt_list_1d_tripleUChar_t(4620350);
    assert(obj_rgb.list_size > 0);
    // printf("(after-parsing)\t read %u lines, at %s:%d\n", obj_rgb.list_size, __FILE__, __LINE__);
    //!
    //! Step(): Convert to a scalar, and then insert into a given list: 
    // s_kt_list_1d_float_t tmp_list = init__s_kt_list_1d_float_t(obj_rgb.list_size);
    assert(tut3_fileNameDim[0] > 0);
    assert(tut3_fileNameDim[1] > 0);    
    loint pos = 0;
    s_kt_matrix_t tmp = initAndReturn__s_kt_matrix(tut3_fileNameDim[0], tut3_fileNameDim[1]);
    for(uint row_id = 0; row_id < tmp.nrows; row_id++) {
      for(uint col_id = 0; col_id < tmp.ncols; col_id++) {
	// for(uint k = 0; k < obj_rgb.list_size; k++) {
	if(pos < obj_rgb.list_size) {
	  const t_float valTo_insert = _get_scalar_fromHisto(obj_rgb.list[pos], type_conversion);
	  //!
	  //! Step(): Add the value:
	  if(valTo_insert != T_FLOAT_MAX) {
	    tmp.matrix[row_id][col_id] = valTo_insert;
	  }	  
	  pos++;
	}
      }
    }
    //!
    //! Step():  De-allocate:
    free__s_kt_list_1d_tripleUChar_t(&obj_rgb);
    //! Step(): Update the bloal data-set:
    if(enum_norm != e_tut3_histogram_undef) { //! then transalte the meausmenets into a histogram.
      s_kt_matrix_t res_histo = convertTo_histo_matrix(tmp, enum_norm);
      free__s_kt_matrix(&tmp);
      tmp = res_histo;
    }
    //! 
    mat_scalarImages[i] = tmp;
    //!
    // free__s_kt_list_1d_float_t(&tmp_list);
  }
}
#endif //! #if config_useSyntDataSets == 0

//! Evaluate a subset of dat-anromszaiton-strategies: 
static const uint norm_size = 7;
static const e_kt_normType_t norm_arr[norm_size] = {
  e_kt_normType_undef,
  e_kt_normType_avgRelative_abs,
  e_kt_normType_avgRelative,
  e_kt_normType_rankRelative,
  e_kt_normType_rankRelative_abs,
  e_kt_normType_relative_STD,
  e_kt_normType_relative_STD_abs,
};


static void applyToAll_tut_entropy_3_rankAcrossFiles()  {
  // printf("uint(max)=%.2f\n", (t_float)UINT_MAX * 0.001);
  
  s_tut_entropy_bestMetric_t bestMetric = init_bestMetric();

  for(uint histo_id = 0; histo_id <= e_tut3_histogram_undef; histo_id++) {
    //! Note: motviation for applying histograms is to try capturing the signature of colro-value-spreads.
    e_tut3_histogram_t enum_histoAfterDataLoad = (e_tut3_histogram_t)histo_id;

#if config_useSyntDataSets == 1
    const uint rgb_conv = 0;
#else
    for(uint rgb_conv = 0; rgb_conv < e_tut3_rgbToScalar_undef; rgb_conv++)
#endif //! #if config_useSyntDataSets == 0
      {
	e_tut3_rgbToScalar_t type_conversion = (e_tut3_rgbToScalar_t)rgb_conv;

	for(uint norm_id = 0; norm_id < norm_size; norm_id++) {
	  //!
	  //! Load data, and compute entropy
	  //const uint cnt_features = 200*1000;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
	  //  const uint cnt_features = 20*1000;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
	  uint cnt_features = 20;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.
	  //const bool isTo_appendHypoMatrix_toSampleDataSet = true;
	  const bool isTo_appendHypoMatrix_toSampleDataSet = false;
	  //! Step: construct collection of hyptoehsis
	  s_kt_correlationMetric_t  obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_undef, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank); // , self->enum_normAfterMetricMerge);    
	  e_kt_normType_t enum_normAfterMetricMerge = e_kt_normType_rankRelative_abs; 
	  //e_tut3_rgbToScalar_t type_conversion = e_tut3_rgbToScalar_Hue;
	  s_tut_entropy3_t self = init__s_tut_entropy3_t("res_entropy", norm_arr[norm_id], obj_metric, &bestMetric, e_tut_entropy3_combineInputData_scalar_undef, enum_normAfterMetricMerge);
	  self.enum_RGB = type_conversion;
	  self.enum_histoAfterDataLoad = enum_histoAfterDataLoad;
	  s_kt_matrix_t mat_hypo = _generate_matrix_hypo(self, /*cnt-cases=*/cnt_cases, isTo_appendHypoMatrix_toSampleDataSet); // , enum_normAfterMetricMerge);

#if config_useSyntDataSets == 0
	  _load_dataFiles_applyRGB_merge(type_conversion, self.enum_histoAfterDataLoad);
#endif //! #if config_useSyntDataSets == 0
	  for(uint combineData_id = 0; combineData_id <= e_tut_entropy3_combineInputData_scalar_undef; combineData_id++) { //! where "<=" is used to ensure that we evaluate also for the case where data is NOT combiend prior to  entropy-operation
	    //!
	    //!
	    s_tut_entropy3_t self = init__s_tut_entropy3_t("res_entropy", norm_arr[norm_id], obj_metric, &bestMetric, (e_tut_entropy3_combineInputData_scalar_t)combineData_id, enum_normAfterMetricMerge);
	    self.enum_RGB = type_conversion;
	    self.enum_histoAfterDataLoad = enum_histoAfterDataLoad;
	    const uint cnt_metrics = (2*e_kt_entropy_genericType_undef);
	    //! Compute Entropy-matrix [metric-id][data-id] = entropy(list[i] or mat_hypo[i]);
	    //! Note: the entropy in below is (??) computed indepodencly for each data-row, hence the seleciotn of which data-row to use does not finleucne the overall prediciton-outcome.
	    s_kt_matrix_t mat_result = _computeEntropy_for_synMatrix(&self, cnt_metrics, mat_hypo, cnt_cases, cnt_features,  isTo_appendHypoMatrix_toSampleDataSet);
      
      
	    //for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
#define tut3_getMetricIdFromIndex(ind) ({s_kt_correlationMetric_t obj = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t((e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t)ind); obj.metric_id;})
	    static const uint tut3_cnt_sim_metric = e_kt_correlationMetric_initCategory_timeComplexitySet__medium_undef;
	    for(uint sim_id = 0; sim_id < tut3_cnt_sim_metric; sim_id++) {	    
	      //for(uint sim_id = 1; sim_id <= e_kt_correlationFunction_undef; sim_id++) {
	      if(false == isTo_use_directScore__e_kt_correlationFunction(tut3_getMetricIdFromIndex(sim_id))) {
		e_kt_correlationFunction_t sim_metric = tut3_getMetricIdFromIndex(sim_id);
		//if(false == isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id)) {
		//e_kt_correlationFunction_t sim_metric = (e_kt_correlationFunction_t)sim_id;
		for(uint post_index = 0; post_index <= e_kt_categoryOf_correaltionPreStep_none; post_index++) {
		  //! Set the corrleation-metric:
		  s_kt_correlationMetric_t corrMetric_betweenSamples;   init__s_kt_correlationMetric_t(&corrMetric_betweenSamples, (e_kt_correlationFunction_t)sim_id, (e_kt_categoryOf_correaltionPreStep_t)post_index);
		  //! Init object:
		  e_kt_normType_t enum_normAfterMetricMerge = e_kt_normType_rankRelative_abs; // FIXME: iterate throuygh combinations ... exploring this.
		  s_tut_entropy3_t self = init__s_tut_entropy3_t("res_entropy", norm_arr[norm_id], corrMetric_betweenSamples, &bestMetric, (e_tut_entropy3_combineInputData_scalar_t)combineData_id, enum_normAfterMetricMerge);
		  self.enum_RGB = type_conversion;
		  self.enum_histoAfterDataLoad = enum_histoAfterDataLoad;
		  //! 
		  //! Then apply:
		  main_tut_entropy_3(self, mat_result, mat_hypo); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
		  //main_tut_entropy_3(self); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
		  free__s_tut_entropy3_t(self);
		}
	      }
	    }
	    //! De-allocate
	    free__s_tut_entropy3_t(self);
	    free__s_kt_matrix(&mat_result);   
	  }
	  //! De-allocate:
	  free__s_kt_matrix(&mat_hypo);
	  free__s_tut_entropy3_t(self);
	}
#if config_useSyntDataSets == 0 //! then de-allcoate: 
	for(uint i = 0; i < tut3_arr_fileNamesInput_size; i++) {
	  free__s_kt_list_1d_float_t(&list_scalarImages[i]);
	}
#endif //! #if config_useSyntDataSets == 0
      }
  }
  free_s_tut_entropy_bestMetric_t(bestMetric);
  // FIXME: compare ... use clsuters ... permatuion of "tut_hypothesis_whyResultDiffers_1.c"

}

static void computeForCombination_tut_entropy_3_rankAcrossFiles(s_tut_entropy_bestMetric_t &bestMetric, s_tut_entropy3_t &self, const uint cnt_features) {
  //!
  //! 
  const bool isTo_appendHypoMatrix_toSampleDataSet = false;
  s_kt_matrix_t mat_hypo = _generate_matrix_hypo(self, /*cnt-cases=*/cnt_cases, isTo_appendHypoMatrix_toSampleDataSet); // , enum_normAfterMetricMerge);
  //!
  //! Load data:
#if config_useSyntDataSets == 0
  _load_dataFiles_applyRGB_merge(/*type_conversion=*/self.enum_RGB, self.enum_histoAfterDataLoad);
#endif //! #if config_useSyntDataSets == 0
  //! 
  //! Compute Entropy-matrix [metric-id][data-id] = entropy(list[i] or mat_hypo[i]);
  const uint cnt_metrics = (2*e_kt_entropy_genericType_undef); // FIXME: need to be adjsutd if we start supporting/epxlroing for other metirc-combinations
  s_kt_matrix_t mat_result = _computeEntropy_for_synMatrix(&self, cnt_metrics, mat_hypo, cnt_cases, cnt_features,  isTo_appendHypoMatrix_toSampleDataSet);
  //!
  //! 
  //! 
  //! Then apply:
  main_tut_entropy_3(self, mat_result, mat_hypo); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
  //main_tut_entropy_3(self); //norm_arr[norm_id],  corrMetric_betweenSamples, &bestMetric);
  //! De-allocate
  free__s_kt_matrix(&mat_result);
  free__s_kt_matrix(&mat_hypo);
#if config_useSyntDataSets == 0 //! then de-allcoate: 
  for(uint i = 0; i < tut3_arr_fileNamesInput_size; i++) {
    free__s_kt_list_1d_float_t(&list_scalarImages[i]);
  }
#endif //! #if config_useSyntDataSets == 0
}



#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief examplfies how to comptue Renyi's entropy-score seperately for each row in a matrix.
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2019).
   @remarks describe basic lgoics wrt. our "hp_entropy.h" API.
   @remarks compilation: g++ -I.  -g -O0  -mssse3   -L .  tut_entropy_3_rankAcrossFiles.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_entro; ./tut_entro
**/
int main()
#else
  static void tut_entropy_3_rankAcrossFiles()   
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif   

  if(true) {
    applyToAll_tut_entropy_3_rankAcrossFiles(); //! ie, then comptue for all metrics
  } else { //! then copmtue for a specified metirc:
    //!
    e_tut3_histogram_t enum_histoAfterDataLoad = e_tut3_histogram_undef;
    uint norm_id = 0; //! "0":e_kt_normType_undef,
    // e_tut3_rgbToScalar_t type_conversion = e_tut3_rgbToScalar_Hue;
    // uint combineData_id = e_tut_entropy3_combineInputData_scalar_undef;
    uint cnt_features = 20;    //! Note: seems like the number of frows does NOT ifnlcue the entorpy-score.

    s_kt_correlationMetric_t  obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_undef, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank); // , self->enum_normAfterMetricMerge);
    e_kt_normType_t enum_normAfterMetricMerge = e_kt_normType_rankRelative_abs; // FIXME: iterate throuygh combinations ... exploring this.
    //!
    //! Initate strucutres: 
    s_tut_entropy_bestMetric_t bestMetric = init_bestMetric();
    // -------------------------
    /**
       //! Note: below is based on our observaitons when 'histogram-option'  is Used
       //! Note: the below reuslts demosntrates how th use of histogram may produce results sigcnatnly better than the use of time-cost-heavy metrics (ie, where 'histo + Kendall' produce better treuslt that 'noneHisto + MINE'). 

	(new-Best-score:abs)	 "Raw hypo:0 -- correct":1.687500 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
	(new-Worst-score:abs)	 "Raw hypo:0 -- correct":1.687500 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
	(new-Best-score:abs)	 "Raw hypo:0 -- correct":1.017857 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
	(new-Worst-score:abs)	 "Raw hypo:0 -- correct":2.000000 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
	(new-Best-score:abs)	 "Raw hypo:0 -- correct":-1.125000 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
	(new-Best-score:abs)	 "Raw hypo:0 -- correct":-1.333333 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
	(new-Worst-score:abs)	 "Raw hypo:0 -- correct":15.000004 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
	(new-Best-score:abs)	 "Raw hypo:0 -- correct":-14.970004 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
	(new-Worst-score:abs)	 "Raw hypo:0 -- correct":6238.001953 -- res_entropy/h0_n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
(after-parsing)	 read 4620350 lines, at tut_entropy_3_rankAcrossFiles.c:1378
(after-parsing)	 read 4620350 lines, at tut_entropy_3_rankAcrossFiles.c:1378
(after-parsing)	 read 4620350 lines, at tut_entropy_3_rankAcrossFiles.c:1378
(after-parsing)	 read 4620350 lines, at tut_entropy_3_rankAcrossFiles.c:1378
(after-parsing)	 read 4620350 lines, at tut_entropy_3_rankAcrossFiles.c:1378
	(new-Worst-score:abs)	 "Raw ML":136961.468750 -- res_entropy/h0_n12_m0_s0_sP0_n3_rgb0_<selectWorst>.tsv
	(new-Worst-score:abs)	 "Raw ML":138868.546875 -- res_entropy/h0_n12_m0_s0_sP0_n3_rgb0_<selectWorst>.tsv
	(new-Worst-score:abs)	 "Raw ML":29817622528.000000 -- res_entropy/h0_n12_m0_s0_sP0_n3_rgb0_<selectWorst>.tsv
	(new-Best-score:abs)	 "Raw ML":-36.999920 -- res_entropy/h0_n12_m0_s0_sP0_n3_rgb0_<selectBest>.tsv
	(new-Best-score:abs)	 "Raw ML":-47.523987 -- res_entropy/h0_n12_m0_s0_sP0_n3_rgb0_<selectBest>.tsv
	(new-Best-score:abs)	 "Raw ML":-58.911499 -- res_entropy/h0_n12_m0_s0_sP0_n3_rgb0_<selectBest>.tsv
	(new-Worst-score:abs)	 "Raw ML":255211815755523054900871803468328730624.000000 -- res_entropy/h0_n12_m2_s0_sP0_n3_rgb0_<selectWorst>.tsv
     **/
    /**
       //! Note: below is based on our observaitons when 'histogram-option'  is NOT used.
        (new-Best-score:abs)     "Raw hypo:0 -- correct":1.687500 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
        (new-Worst-score:abs)    "Raw hypo:0 -- correct":1.687500 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
        (new-Best-score:abs)     "Raw hypo:0 -- correct":1.017857 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
        (new-Worst-score:abs)    "Raw hypo:0 -- correct":2.000000 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
        (new-Best-score:abs)     "Raw hypo:0 -- correct":-1.125000 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
        (new-Best-score:abs)     "Raw hypo:0 -- correct":-1.333333 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
        (new-Worst-score:abs)    "Raw hypo:0 -- correct":15.000004 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
        (new-Best-score:abs)     "Raw hypo:0 -- correct":-14.970004 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectBest>.tsv
        (new-Worst-score:abs)    "Raw hypo:0 -- correct":6238.001953 -- res_entropy/n12_m3_s93_sP0_n3_rgb0_<selectWorst>.tsv
        (new-Worst-score:abs)    "Raw ML":inf -- res_entropy/n12_m0_s0_sP0_n3_rgb0_<selectWorst>.tsv
        (new-Best-score:abs)     "Raw ML":-36.969124 -- res_entropy/n12_m0_s0_sP0_n3_rgb0_<selectBest>.tsv
        (new-Best-score:abs)     "Raw ML":-40.304455 -- res_entropy/n12_m0_s0_sP0_n3_rgb0_<selectBest>.tsv
        (new-Best-score:abs)     "Raw ML":-57.197357 -- res_entropy/n12_m0_s0_sP0_n3_rgb0_<selectBest>.tsv
	//!
	//! Above is used to dientify the correct cluster-prediction-scores:
------------------------------------------------------------------------------------------------------
-- Note: "entC-false" implies that we (a) compute a correlationi-matrix (ie, "encC"), and (b) do not merge muliple entorpy-metrics (ie, "false").
src/res_entropy $ rgrep -P "4.000000\t0.000000\t1.000000\t2.000000\t3.000000"  *
h9_n12_m0_s0_sP0_n3_rgb0_data_transp_ranked.tsv:ML      4.000000        0.000000        1.000000        2.000000        3.000000
h9_n12_m0_s0_sP0_n3_rgb0_data_transp_ranked.tsv:ML w/postScale  4.000000        0.000000        1.000000        2.000000        3.000000
h9_n12_m0_s0_sP0_n3_rgb0_relDiffR-entC-false.tsv:PT1    4.000000        0.000000        1.000000        2.000000        3.000000
h9_n12_m0_s0_sP0_n3_rgb0_relDiffR-entC-false.tsv:PT1 w/postScale        4.000000        0.000000        1.000000        2.000000        3.000000

-- note: difference between "entC-false" versus "data_transp_ranked" is that "entC-false" comptues the relative difference "hypo/data(i)" for the "ranked(data)"  exported in our "data_transp_ranked"
src/res_entropy $ rgrep -P "0.000000\t1.000000\t2.000000\t3.000000\t4.000000"  *
h9_n12_m0_s0_sP0_n3_rgb0_data_transp_ranked.tsv:SimpsonGeneric  0.000000        1.000000        2.000000        3.000000        4.000000
h9_n12_m0_s0_sP0_n3_rgb0_data_transp_ranked.tsv:SimpsonGeneric w/postScale      0.000000        1.000000        2.000000        3.000000        4.000000
h9_n12_m0_s0_sP0_n3_rgb0_relDiffR-entC-false.tsv:SimpsonGeneric 0.000000        1.000000        2.000000        3.000000        4.000000
h9_n12_m0_s0_sP0_n3_rgb0_relDiffR-entC-false.tsv:SimpsonGeneric w/postScale     0.000000        1.000000        2.000000        3.000000        4.000000
h9_n12_m0_s0_sP0_n3_rgb0_relDiff_normR-entC-false.tsv:SimpsonGeneric    0.000000        1.000000        2.000000        3.000000        4.000000
h9_n12_m0_s0_sP0_n3_rgb0_relDiff_normR-entC-false.tsv:SimpsonGeneric w/postScale        0.000000        1.000000        2.000000        3.000000        4.000000
------------------------------------------------------------------------------------------------------

 
     **/
    const uint arr_simMetrics[2] = {0, 93}; //! ie, based on expiermntal observations; "0":e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, "93":MINE
    uint arr_combineData_id[2] = {0, 3}; //! ie, based on expiermntal observations; "0":e_tut_entropy3_combineInputData_scalar_toUint_div_maxMin_data0, "3":none.
    for(uint sim = 0; sim < 2; sim++) {
      for(uint comb = 0; comb < 2; comb++) {	
	obj_metric.metric_id = (e_kt_correlationFunction_t)sim;
	uint combineData_id = comb;
	// obj_metric.metric_id = (e_kt_correlationFunction_t)93;
	e_tut3_rgbToScalar_t type_conversion = (e_tut3_rgbToScalar_t)0; //! "0":e_tut3_rgbToScalar_min,
	enum_normAfterMetricMerge = (e_kt_normType_t)3; //! "3":e_kt_normType_rankRelative_abs,  <-- details: see "kt_aux_matrix_norm.h"
	s_tut_entropy3_t self = init__s_tut_entropy3_t("res_entropy", norm_arr[norm_id], obj_metric, &bestMetric, (e_tut_entropy3_combineInputData_scalar_t)combineData_id, enum_normAfterMetricMerge);
	self.enum_RGB = type_conversion;
	self.enum_histoAfterDataLoad = enum_histoAfterDataLoad;
	//!
	//! Compute:
	computeForCombination_tut_entropy_3_rankAcrossFiles(bestMetric, self, cnt_features);
	//! De-allocate
	free__s_tut_entropy3_t(self);
	free_s_tut_entropy_bestMetric_t(bestMetric);
      }
    }
  }
  
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

