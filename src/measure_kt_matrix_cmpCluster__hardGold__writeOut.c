{
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  init__s_kt_matrix(&obj_matrixInput, cnt_cases, nrows_total, /*isTo_allocateWeightColumns=*/false);
  uint cntClusters_curr 
    = (uint)(t_float)cnt_clusters_total/(t_float)(cnt_cases+2);
  assert(cntClusters_curr > 0);
  for(uint case_id = 0; case_id < cnt_cases; case_id++) {
    s_hp_clusterShapes_t obj_gold = __Mi__init();
    //! Apply rotuine:
    const uint cnt_clusterStartOffset = cntClusters_curr;
    const uint cnt_clusters_local = cntClusters_curr++;
    // printf("#\t cnt_clusters_local=%u, at %s:%d\n", cnt_clusters_local, __FILE__, __LINE__);
    assert(cnt_clusters_local <= nrows_total);
    assert(cnt_clusters_local >= 1);
    const bool is_ok = __Mi__buildSample();
    assert(is_ok);
    //!
    //! Update the result-matrix:
    assert(obj_gold.map_clusterMembers);
    assert(obj_gold.map_clusterMembers_size == obj_matrixInput.ncols);
    assert(case_id < obj_matrixInput.ncols);
    for(uint row_id = 0; row_id < nrows_total; row_id++) {
      const uint cluster_id = obj_gold.map_clusterMembers[row_id];
      assert(cluster_id != UINT_MAX); //! ie, as we expec thte latter to have been set.
      obj_matrixInput.matrix[case_id][row_id] = cluster_id;
    }
    // tagLocalIndex_0.tagNrows_10.tagNclusters_3.tagExperiment_all.tagSample_clusterCmpEvaluation_case2.localMetricData_.subCategory_dataXmetrics.ranks_adjustedWrtGoldStandard_disjointMasks.tsv
    //if(case_id == 0) 
      {
	//! Then write out the matrix:
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__%s.matrix.tsv", resultDir_path, dataBlock_currentPos, case_id, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads
	const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&(obj_gold.matrix), path_w_dir, NULL);
	assert(is_ok_ex);
    }
    //!
    //! De-allocate:
    free__s_hp_clusterShapes_t(&obj_gold);
  }
  //!
  //! Write out: 
  char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__%s.tsv", resultDir_path, dataBlock_currentPos, cnt_cases, stringOf_measurement); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads
  const bool is_ok_ex = export__singleCall__s_kt_matrix_t(&obj_matrixInput, path_w_dir, NULL);
  assert(is_ok_ex);
  //!
  //! De-allocate:
  free__s_kt_matrix(&obj_matrixInput);
}

//!
//! Update the global counter:
dataBlock_currentPos++;
