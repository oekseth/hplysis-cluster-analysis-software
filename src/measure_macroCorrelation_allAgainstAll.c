/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "correlation_macros__distanceMeasures.h"



//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}




int main() {

  assert(false); // FIXME: compare the perfomrance with ... our "correlationType_proximity_altAlgortihmComputation.cxx" 
  assert(false); // FIXME: test differnet premautions of the "correlationFunc_optimal_compute_allAgainstAll_SIMD.cxx" ... ie, both (a) 'compute compsotions/sums during procssing' and (b) 'use delayed comptaution of sums' (ie, fore the cases where the amsks are nto set).
  assert(false);   // FIXME: ... write a 'sytnetic benchmark' of the follwoign code-set 'when comapred to our impelemtatnion: .... as a future wok try to describe "https://en.wikipedia.org/wiki/Strassen_algorithm" wrt. matrix-operations ... eg, wrt. "http://www.cquestions.com/2011/09/strassens-matrix-multiplication-program.html" ... "http://math.ewha.ac.kr/~jylee/SciComp/sc-crandall/strassen.c" ...  "https://github.com/sangeeths/stanford-algos-1/blob/master/strassen-recursive-matrix-multiplication.c" <-- first ask Jan-Christain to evaluate/'suggest' the perofmrance-benefit.
  assert(false); // FIXME: comapre [ªbove] witht hte time-cost wr.t the naive impelemtnatison ... found in our "distance_2rows_slow.h"
  assert(false); // FIXME: add seomthing.

  assert(false); // FIXME: ... <-- bonus: performance: consider to add/test support wrt. lgo-comptuations for values in range "0 ... 1" ... eg, search for all 'calls' to "VECTOR_FLOAT_LOG" ... using our mien-software-otpmziation ... includign the latter lbirary into our 'compiatlion' ... or 'cator out' the correlation-emtrics inq eustion

}
