#include "correlationType_spearman.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */



//! A naive Spearman-implemetnaiton using the non-optmized "cluster.c" as a perofmrance-comparison-use-case.
t_float __kt_spearman_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {

  const t_float defaultValue_float = 0;
  t_float *arrOf_scores_1 = allocate_1d_list_float(n, defaultValue_float); t_float *arrOf_scores_2 = allocate_1d_list_float(n, defaultValue_float);

  
  // FIXME: wrt. [below] ... write an atlernative funciton which uses _mm_load_ps(..) ... and then comapre the exeuciton-time ... support case where ... [”elow] input-variable is set ... ie, where we do not need to investgiate for mask-proeprties.
  // const bool needTo_useMask_evaluation = graphAlgorithms_distance::matrixMakesUSeOf_mask(nrows, ncols, data1, data2, mask1, mask2, transpose, iterationIndex_2);

  uint m = 0;
  if(transpose == 0) {
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.
    for(uint i = 0; i < n; i++) { 
      if((!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i])) {
	// if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	arrOf_scores_1[m] = data1[index1][i];
	arrOf_scores_2[m] = data2[index2][i];
	// arrOf_scores_1.insert(m, data1[index1][i]);
	// arrOf_scores_2.insert(m, data2[index2][i]);
	m++;
      }
    }
  } else { 
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare the same column in mulitple row: if the column-size does not signficantly exceed the "L2" memory-cache-size (for a given computer-memory-hardware) then we expect an 'ideal' memory-allocation-scheme to provide/give a "linear" "n" speed-up (for every call to this function).
    //printf("\tcol_id=[...%u][%u]---[...%u][%u], at %s:%d\n", n, index1, n, index2,  __FILE__, __LINE__);
    for(uint i = 0; i < n; i++) {
      if((!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2])) {
	arrOf_scores_1[m] = data1[i][index1];
	arrOf_scores_2[m] = data2[i][index2];
	// arrOf_scores_1.insert(m, data1[i][index1]);
	// arrOf_scores_2.insert(m, data2[i][index2]);
	m++;
      }
    }
  }
  if(m==0) {
    free_1d_list_float(&arrOf_scores_1);  free_1d_list_float(&arrOf_scores_2); 
    return 0;
  }
  // assert(arrOf_scores_1.get_current_pos());
  // assert(arrOf_scores_2.get_current_pos());
  
  //! Infer the ranks:
  //! Note[memory-access-patterns]: (1) sequential ((a) in merge-sorts "n*log(n)" procedure partial sequential, and (b) sequential (wrt. distance-updates)); (2) a quential search without any (noteworthy) cache-misses.
  
  // FIXME: wrt. [below] first update our "get_rank(..)" procedure ... ie, as we are interested in building a new matrix based on 'the rank of the sorted items' <-- in this context remember to test if "data1" and "data2" points 'the same emmory': fi they do, then construct to seperate sorted lists.
  // FIXME: instead of [bbelow] first construct a new matrix of 'ranks' ... re-using memory-allocations 'between each run' ... and then 'perform' the ordinary matrix-iteration 
  // FIXME: wrt. [ªove] compare the time to compute a matrix .... when comparing the differnece of our 'pre-comptuation of scores' <-- expect a performance-increase of "2*n": if this approach is used 'as-is' then there will be "n*n*2" sort-calls, with an overall comlextity of "n*n*2*n*log(n). To understand/calidate the latter observation/assertion, remember that this funciton is called for every cell "result[index1][index2]", ie, "n*n" calls, and where this function calsl the sort-orutine "2x". Instead, if we .. if we sort the ...??...  ....
  // FIXME: if [above] fixme/note 'is correct', then update our aritlce-test with an assertion that we for the "spearman" distnace-matrix combined with algorithms of "k-means" and "k-median" manages a perofmrnace-increase of more than "2*n*n". <-- write benchamrk/evlauation tests to demosntrate this difficerne ... and then inspect/test the effects of parallisation.
  // FIXME: wrt. [ªbove] .... for transpose ... if the matrix is not already trasnposed then we first need to transpose it .... or to insert the elements dynamically in the list <-- test both appraoches both 'syntactical' and 'actual'.
  // FIXME: wrt. [below] calls enusre that we do not use sorting 'if the call is inside a for-loop already in parallel'.
  t_float *arrOf_ranks_1 = getrank(n, arrOf_scores_1, e_distance_rank_typeOf_computation_ideal);
  t_float *arrOf_ranks_2 = getrank(n, arrOf_scores_2, e_distance_rank_typeOf_computation_ideal);
  free_1d_list_float(&arrOf_scores_1);  free_1d_list_float(&arrOf_scores_2); 

  //! What we expect:
  // assert(arrOf_ranks_1.get_current_pos() == m);
  // assert(arrOf_ranks_2.get_current_pos() == m);

  const t_float avgrank = 0.5*(m-1); /* Average rank */

  t_float denom1 = 0.;
  t_float denom2 = 0.;
  t_float result = 0;
  //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.
  for(uint i = 0; i < m; i++) { 
    const t_float value1 = arrOf_ranks_1[i];
    const t_float value2 = arrOf_ranks_2[i];
    //! What we expect:
    assert(value1 != T_FLOAT_MAX);
    assert(value2 != T_FLOAT_MAX);
    // assert(value1 < n);
    // assert(value2 < );
    //! Compute the Spearmans rank-correlation (oekseth, 06. sept. 2016):
    //! Note: the [”elow] macro-funciton is spected to be found in our "correlation_macros__distanceMeasures.h" (oekseth, 06. sept. 2016).
    correlation_macros__distanceMeasures__spearman(value1, value2, result, denom1, denom2);
  }
  free_1d_list_float(&arrOf_ranks_1);  free_1d_list_float(&arrOf_ranks_2); 
  //arrOf_ranks_1.free_memory(); arrOf_ranks_2.free_memory();
  //! 
  /* Note: denom1 and denom2 cannot be calculated directly from the number
   * of elements. Iftwo elements have the same rank, the squared sum of
   * their ranks will change [author of the C clustering library]
   */  
  
// #if(configure_generic_useMulInsteadOf_div == 1)
//   const t_float expected_value = (typeOf_metric_correlation == e_typeOf_metric_correlation_pearson_spearMan) ? 1 : 0;
// #else
  const t_float expected_value = compute_score_correlation(result, /*sum1=*/0, /*sum2=*/0, /*tweight=*/1, denom1, denom2, /*typeOf_metric_correlation=*/e_typeOf_metric_correlation_pearson_spearMan, /*sizeOf_i=*/m);
  //#endif
  
  const uint cnt_elements = m; 
  assert(cnt_elements > 0);
#if(configure_generic_useMulInsteadOf_div == 1)
  const t_float cnt_elements_inv = 1/cnt_elements;
  const t_float scalar_result = correlation_macros__distanceMeasures__postProcessCorrelation__spearman(result, denom1, denom2, cnt_elements, cnt_elements_inv);
#else
  const t_float scalar_result = correlation_macros__distanceMeasures__postProcessCorrelation__spearman__slow(result, denom1, denom2, cnt_elements);
#endif

  // //printf("result=%f, at %s:%d\n", result, __FILE__, __LINE__);
  // result /= m;
  // denom1 /= m;
  // denom2 /= m;
  // result -= avgrank * avgrank;
  // denom1 -= avgrank * avgrank;
  // denom2 -= avgrank * avgrank;
  // if(denom1 <= 0) return 1; /* include '<' to deal with roundoff errors */
  // if(denom2 <= 0) return 1; /* include '<' to deal with roundoff errors */
  // assert((denom1*denom2) != 0);
  // result = result / sqrt(denom1*denom2);
  // result = 1. - result;

  //! Validate constency in oru appraoch:
  assert(scalar_result == expected_value); //! and if this does nto hold, tehn it coudl be roruning-errors, ie, similar to our expereince wrt. the "Mine-sofwtare-optmizaiton" (oekseth, 06. sept. 2016).

  return scalar_result;
}

//! Comptue Spearmnas correlation-coefficent for pre-comptued ranks.
//! @return the correlation-socre.
//! @remarks the funciton shoudl be called 'driectly' if (a) there are no mask-properties and (b) there there are muliple calls where teh same rank-resutls are used (eg, when one index is correlated to muliple indexes).
t_float kt_spearman_forPreComputedRanks(const s_allAgainstAll_config_t self, const s_correlation_rank_rowPair_t obj_correlation) {
  //! Identify the max-number-of-entries
  const uint cnt_elements = obj_correlation.cnt_setValues_inRanks;
  if(cnt_elements > 0) {
    const t_float avgrank = 0.5*(cnt_elements-1); /* Average rank */

    const t_float *arrOf_ranks_1 = obj_correlation.arrOf_ranks_index1;
    const t_float *arrOf_ranks_2 = obj_correlation.arrOf_ranks_index2;
    assert(arrOf_ranks_1);       assert(arrOf_ranks_2); assert(arrOf_ranks_1 != arrOf_ranks_2); //! ie, what we expect.

    t_float denom1 = 0.;
    t_float denom2 = 0.;
    t_float result = 0;
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.
      
    // FIXME: experiement with this threshold.
    uint col_id = 0;
    if(cnt_elements > 1000) {
      uint cnt_elements_intri = cnt_elements - VECTOR_FLOAT_ITER_SIZE;
      if(((t_float)cnt_elements/(t_float)VECTOR_FLOAT_ITER_SIZE) && ((t_float)VECTOR_FLOAT_ITER_SIZE)) {
	cnt_elements_intri = cnt_elements; //! ie, as we then assume that "cnt_elements" is divisable by "VECTOR_FLOAT_ITER_SIZE"
      }
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET_zero();
      VECTOR_FLOAT_TYPE vec_denom_1 = VECTOR_FLOAT_SET_zero(); VECTOR_FLOAT_TYPE vec_denom_2 = VECTOR_FLOAT_SET_zero();
	
      for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) { 
	VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOAD(&arrOf_ranks_1[col_id]);
	VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOAD(&arrOf_ranks_2[col_id]);
	//! Apply the aritmethics:
	//! Note: the [”elow] macro-funciton is spected to be found in our "correlation_macros__distanceMeasures.h" (oekseth, 06. sept. 2016).
	correlation_macros__distanceMeasures__spearman_SSE(vec_1, vec_2, vec_result, vec_denom1, vec_denom2);
      }
      //! Update the scalars:
      result = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_result);
      denom1 = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_denom_1);
      denom2 = VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_denom_2);
    } 
    if(col_id != cnt_elements) {  //! Tehn the non-SSE-iteration:
      for(; col_id < cnt_elements; col_id++) { 
	const t_float value1 = arrOf_ranks_1[col_id];
	const t_float value2 = arrOf_ranks_2[col_id];
	//! Compute the Spearmans rank-correlation (oekseth, 06. sept. 2016):
	//! Note: the [”elow] macro-funciton is spected to be found in our "correlation_macros__distanceMeasures.h" (oekseth, 06. sept. 2016).
	correlation_macros__distanceMeasures__spearman(value1, value2, result, denom1, denom2);
      }
    }
      
      
    const t_float expected_value = compute_score_correlation(result, /*sum1=*/0, /*sum2=*/0, /*tweight=*/1, denom1, denom2, /*typeOf_metric_correlation=*/e_typeOf_metric_correlation_pearson_spearMan, /*sizeOf_i=*/cnt_elements);
      
    assert(cnt_elements > 0);
#if(configure_generic_useMulInsteadOf_div == 1)
    const t_float cnt_elements_inv = 1/cnt_elements;
    const t_float scalar_result = correlation_macros__distanceMeasures__postProcessCorrelation__spearman(result, denom1, denom2, cnt_elements, cnt_elements_inv);
#else
    const t_float scalar_result = correlation_macros__distanceMeasures__postProcessCorrelation__spearman__slow(result, denom1, denom2, cnt_elements);
#endif


    assert(scalar_result == expected_value); //! and if this does nto hold, tehn it coudl be roruning-errors, ie, similar to our expereince wrt. the "Mine-sofwtare-optmizaiton" (oekseth, 06. sept. 2016).


    //! @return the result:
    return scalar_result;
  } else {
    return 1; //! ie, were "1" is assumed to describe a "no-match" in Spearmsns correlation-rank-metric-function
  }
}

//! Comptue Spearmnas correlation-coefficent.
//! @return the correlation-socre.
t_float kt_spearman(uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], uint index1, uint index2, uint transpose, const s_allAgainstAll_config_t self) {
  //  printf("# at ....., %s:%d\n", __FILE__, __LINE__);
  if( (transpose == 0) && self.forNonTransposed_useFastImplementaiton) {
    //    printf("\t at ....., %s:%d\n", __FILE__, __LINE__);
    //! Note: an example of how this function may be used is seen in our"ktCorr_compute_allAgainstAll_distanceMetric_spearman(...)", an all-against-all- correlation-comptaution-call found in our "correlation_inCategory_rank_spearman.cxx".
    
    //! Comptue the ranks for thw two rows:
    s_correlation_rank_rowPair_t obj_correlation;
    s_correlation_rank_rowPair_allocate(&obj_correlation, ncols);
    s_correlation_rank_rowPair_compute_nonTransposed(&obj_correlation, index1, index2, ncols, data1, data2, mask1, mask2, /*performance-appraoch=*/e_distance_rank_typeOf_computation_ideal, /*needTo_useMask_evaluation=*/s_allAgainstAll_config__needTo_useMask_evaluation(&self));

    //! Compute the ranks based on the correlations:
    const t_float result_value = kt_spearman_forPreComputedRanks(self, obj_correlation);
    //! De-alcoate:
    s_correlation_rank_rowPair_free(&obj_correlation);   
    return result_value;
  } else {
    return __kt_spearman_slow(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose);
  }  
}


