#include "math_generateDistribution_incompleteBeta.h"


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "math_baseCmp_operations_alglib.h"

#include "math_generateDistribution_incompleteBeta_subFunctions.c"


/*************************************************************************
Student's t distribution

Computes the integral from minus infinity to t of the Student
t distribution with integer k > 0 degrees of freedom:

                                     t
                                     -
                                    | |
             -                      |         2   -(k+1)/2
            | ( (k+1)/2 )           |  (     x   )
      ----------------------        |  ( 1 + --- )        dx
                    -               |  (      k  )
      sqrt( k pi ) | ( k/2 )        |
                                  | |
                                   -
                                  -inf.

Relation to incomplete beta integral:

       1 - stdtr(k,t) = 0.5 * incbet( k/2, 1/2, z )
where
       z = k/(k + t**2).

For t < -2, this is the method of computation.  For higher t,
a direct method is derived from integration by parts.
Since the function is symmetric about t=0, the area under the
right tail of the density is found by calling the function
with -t instead of t.

ACCURACY:

Tested at random 1 <= k <= 25.  The "domain" refers to t.
                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE     -100,-2      50000       5.9e-15     1.4e-15
   IEEE     -2,100      500000       2.7e-15     4.9e-17

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float studenttdistribution(const uint ncols, t_float score) {
    /* t_float x; */
    /* t_float rk; */
    /* t_float z; */
    /* t_float f; */
    /* t_float tz; */
    /* t_float xsqk; */
    /* ae_int_t j; */

  // assert(false); // FIXME (a) udpate the java-doc [above] and (b) make 'this' accessible from the heade-rfile ... and (c) update our "matrix_deviation.c" wrt. "t-test"

    t_float result;

    assert(ncols > 0);
    if(ncols == 0) {
      fprintf(stderr, "!!\t Seems lincolse no elements were in the feature-vector expected to have bene the result of your correlation-coeffcient=%f, ie, please investigate correcntesss of your call: observat at [%s]:%s:%d\n", score, __FUNCTION__, __FILE__, __LINE__);
      return 0;
    }

    if(score == 0) { 
      result = 0.5;
      return result;
    }
    
    if(score < -2.0) {
      const t_float rncols = (t_float)(ncols);
      const t_float z = rncols/(rncols + score*score);

      
      const t_float result = 0.5*incompletebeta(0.5*rncols, 0.5, z);
      return result;
    }
    const t_float x = (score >= 0) ? score : -score; //! ie, a psotive score.

    const t_float rncols = (t_float)(ncols);
    const t_float z = 1.0+x*x/rncols;

    t_float p;

    if( ncols %2 !=0 ) {
      const t_float xsqncols = x/mathLib_float_sqrt( rncols ); 
      p = mathLib_float_atan(xsqncols);
      if( ncols > 1 ) {
	t_float f = 1.0;
	t_float tz = 1.0;
	uint j = 3;
	// FIXME: evalaute the time-cost of [below] ... and thereafter consider to pre-compute [below] for ... 
	while( (j <= ncols -2 ) && (tz/f > T_FLOAT_MIN ) )  {
	  tz = tz*((j-1)/(z*j));
	  f = f+tz;
	  j = j+2;
	}
	p = p+f*xsqncols/z;
      }
      p = p*2.0/VECTOR___const__PI;
    } else {
      t_float f = 1.0;
      t_float tz = 1.0;
      uint j = 2;
      // FIXME: evalaute the time-cost of [below] ... and thereafter consider to pre-compute [below] for ... 
      while( (j <= ncols -2) && ( tz/f > T_FLOAT_MIN) ) {
	tz = tz*((j-1)/(z*j));
	f = f+tz;
	j = j+2;
      }
      p = f*x/mathLib_float_sqrt(z*rncols);
    }
    if( score < (t_float)(0) ) {
        p = -p;
    }
    result = 0.5+0.5*p;
    return result;
}


/*************************************************************************
Functional inverse of Student's t distribution

Given probability p, finds the argument t such that stdtr(k,t)
is equal to p.

ACCURACY:

Tested at random 1 <= k <= 100.  The "domain" refers to p:
                     Relative error:
arithmetic   domain     # trials      peak         rms
   IEEE    .001,.999     25000       5.7e-15     8.0e-16
   IEEE    10^-6,.001    25000       2.0e-12     2.9e-14

Cephes Math Library Release 2.8:  June, 2000
Copyright 1984, 1987, 1995, 2000 by Stephen L. Moshier
*************************************************************************/
t_float invstudenttdistribution(const uint ncols, t_float score) {
    t_float t;
    t_float rk;
    t_float z;
    ae_int_t rflg;
    t_float result;


    //ae_assert((k>0&&ae_fp_greater(p,(t_float)(0)))&&ae_fp_less(p,(t_float)(1)), "Domain error in InvStudentTDistribution", _state);
    rk = (t_float)(ncols);
    if( ae_fp_greater(score, 0.25)&&ae_fp_less(score, 0.75) )
    {
        if( ae_fp_eq(score, 0.5) )
        {
            result = (t_float)(0);
            return result;
        }
        z = 1.0-2.0*score;
        z = invincompletebeta(0.5, 0.5*rk, ae_fabs(z));
        t = ae_sqrt(rk*z/(1.0-z));
        if( ae_fp_less(score, 0.5) )
        {
            t = -t;
        }
        result = t;
        return result;
    }
    rflg = -1;
    if( ae_fp_greater_eq(score, 0.5) )
    {
        score = 1.0 - score;
        rflg = 1;
    }
    z = invincompletebeta(0.5*rk, 0.5, 2.0*score);
    if( ae_fp_less(ae_maxrealnumber*z,rk) )
    {
        result = rflg*ae_maxrealnumber;
        return result;
    }
    t = ae_sqrt(rk/z-rk);
    result = rflg*t;
    return result;
}


