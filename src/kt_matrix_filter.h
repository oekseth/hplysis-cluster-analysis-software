#ifndef kt_matrix_filter_h
#define kt_matrix_filter_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_distance
   @brief provide lgocis for application of filters, eg, to combine STD-computaiton with valeu-threshodls and disjoint network-filtering.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "matrix_deviation.h"
#include "kt_matrix.h"
/**
   @enum e_kt_matrix_filter_orderOfFilters_eitherOr
   @brief the type of either-or mask-filtering (oekseth, 06. okt. 2016)
   @remarks is used to idneitfy the filter-order to be applied to a givne input-matrix.
 **/
typedef enum e_kt_matrix_filter_orderOfFilters_eitherOr {
  e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent,
  e_kt_matrix_filter_orderOfFilters_eitherOr_absoluteValueAboveThreshold,
  e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff,
  e_kt_matrix_filter_orderOfFilters_eitherOr_STD,
  e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis,
  e_kt_matrix_filter_orderOfFilters_eitherOr_skewness,
  e_kt_matrix_filter_orderOfFilters_eitherOr_undef //! which is used as the 'size' proerpty
} e_kt_matrix_filter_orderOfFilters_eitherOr_t;

//! @return a string-rpresentaiotn of the enum.
static const char *getString__e_kt_matrix_filter_orderOfFilters_eitherOr_t(const e_kt_matrix_filter_orderOfFilters_eitherOr_t  enum_id ) {
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_percentPrecent) {return "percentPrecent";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_absoluteValueAboveThreshold) {return "absoluteValueAboveThreshold";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_minMaxDiff) {return "minMaxDiff";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_STD) {return "STD";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_kurtosis) {return "kurtosis";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_skewness) {return "skewness";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_eitherOr_undef) {return "<undef>";}
  //if(enum_id == ) {return "";}
  
  //!
  //! Not foudn:
  return NULL;
}
//! @return an enum-rperesention for teh enum in quesiton.
static const e_kt_matrix_filter_orderOfFilters_eitherOr_t get_enum__e_kt_matrix_filter_orderOfFilters_eitherOr_t(const char *str) {
  assert(str); assert(strlen(str));
  for(uint i = 0; i < e_kt_matrix_filter_orderOfFilters_eitherOr_undef; i++) {
    const char *cmp = getString__e_kt_matrix_filter_orderOfFilters_eitherOr_t((e_kt_matrix_filter_orderOfFilters_eitherOr_t)i);
    assert(cmp); assert(strlen(cmp));
    if(strncasecmp(cmp, str, strlen(cmp))) {
      return (e_kt_matrix_filter_orderOfFilters_eitherOr_t)i;
    }
  }
  //!
  //! Not found:
  return e_kt_matrix_filter_orderOfFilters_eitherOr_undef;
}
/**
   @enum e_kt_matrix_filter_orderOfFilters_cellMask
   @brief the type of mask-fitlers to be applied seperately for each cell (oekseth, 06. okt. 2016)
   @remarks 
   - is used to idneitfy the filter-order to be applied to a givne input-matrix.
   - applicaiotn of this mask-filter is expected to (a) result in a fragmented input-matrix, (b) result in a specifity-boost wrt. valeus (ie, incrased specirity as we avodit eh rows and columsn to be "eitehr inlcued or discarded") and (c) demonstrate the improtance of our "kt_forest_findDisjoint" ANSI-C module for disjtoin-forest-comptautions.
 **/
typedef enum e_kt_matrix_filter_orderOfFilters_cellMask {
  e_kt_matrix_filter_orderOfFilters_cellMask_minMax,
  e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent,
  e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent,
  e_kt_matrix_filter_orderOfFilters_cellMask_undef //! which is used as the 'size' proerpty
} e_kt_matrix_filter_orderOfFilters_cellMask_t;

//! @return a string-rpresentaiotn of the enum.
static const char *getString__e_kt_matrix_filter_orderOfFilters_cellMask_t(const  e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id) {
  if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_minMax) {return "minMax";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent) {return   "inRange-mean-percent";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_median_percent) {return "inRange-median-percent";}
  if(enum_id == e_kt_matrix_filter_orderOfFilters_cellMask_undef) {return "<undef>";}
  
  //!
  //! Not foudn:
  return NULL;
}
//! @return an enum-rperesention for teh enum in quesiton.
static const e_kt_matrix_filter_orderOfFilters_cellMask_t  get_enum__e_kt_matrix_filter_orderOfFilters_cellMask_t(const char *str) {
  assert(str); assert(strlen(str));
  for(uint i = 0; i < e_kt_matrix_filter_orderOfFilters_cellMask_undef; i++) {
    const char *cmp = getString__e_kt_matrix_filter_orderOfFilters_cellMask_t((e_kt_matrix_filter_orderOfFilters_cellMask_t)i);
    assert(cmp); assert(strlen(cmp));
    if(strncasecmp(cmp, str, strlen(cmp))) {
      return (e_kt_matrix_filter_orderOfFilters_cellMask_t)i;
    }
  }
  //!
  //! Not found:
  return e_kt_matrix_filter_orderOfFilters_cellMask_undef;
}

/**
   @enum e_kt_matrix_filter_adjustValues
   @brief adjust the valeus in order to identify certain patters in the data (oekseth, 06. okt. 2016)
   @remarks is used to idneitfy the filter-order to be applied to a givne input-matrix.
 **/
typedef enum e_kt_matrix_filter_adjustValues {
  e_kt_matrix_filter_adjustValues_log,
  e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows,
  e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_cols,
  e_kt_matrix_filter_adjustValues_center_normalization_rows,
  e_kt_matrix_filter_adjustValues_center_normalization_cols,
  e_kt_matrix_filter_adjustValues_undef //! which is used as the 'size' proerpty
} e_kt_matrix_filter_adjustValues_t;

//! @return a string-rpresentaiotn of the enum.
static const char *getString__e_kt_matrix_filter_adjustValues_t(const e_kt_matrix_filter_adjustValues_t enum_id ) {
  if(enum_id == e_kt_matrix_filter_adjustValues_log) {return "log";}
  if(enum_id == e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_rows) {return "center_subtractOrAdd_minMax_rows";}
  if(enum_id == e_kt_matrix_filter_adjustValues_center_subtractOrAdd_minMax_cols) {return "center_subtractOrAdd_minMax_cols";}
  if(enum_id == e_kt_matrix_filter_adjustValues_center_normalization_rows) {return "center_normalization_rows";}
  if(enum_id == e_kt_matrix_filter_adjustValues_center_normalization_cols) {return "center_normalization_cols";}
  if(enum_id == e_kt_matrix_filter_adjustValues_undef) {return "<undef>";}
  
  //!
  //! Not foudn:
  return NULL;
}
//! @return an enum-rperesention for teh enum in quesiton.
static const e_kt_matrix_filter_adjustValues_t get_enum__e_kt_matrix_filter_adjustValues_t(const char *str) {
  assert(str); assert(strlen(str));
  for(uint i = 0; i < e_kt_matrix_filter_adjustValues_undef; i++) {
    const char *cmp = getString__e_kt_matrix_filter_adjustValues_t((e_kt_matrix_filter_adjustValues_t)i);
    assert(cmp); assert(strlen(cmp));
    if(strncasecmp(cmp, str, strlen(cmp))) {
      return (e_kt_matrix_filter_adjustValues_t)i;
    }
  }
  //!
  //! Not found:
  return e_kt_matrix_filter_adjustValues_undef;
}

/**
   @enum e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd
   @brief (oekseth, 06. okt. 2016)
 **/
typedef enum e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd {
  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median,
  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean,
  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD,
  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none
} e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t;

//! @return a string-rpresentaiotn of the enum.
static const char *getString__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(const e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t enum_id ) {
  if(enum_id == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median) {return "median";}
  if(enum_id == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean) {return "mean";}
  if(enum_id == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD) {return "STD";}
  if(enum_id == e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none) {return "none";}
  
  //!
  //! Not foudn:
  return NULL;
}
//! @return an enum-rperesention for teh enum in quesiton.
static const e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t get_enum__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(const char *str) {
  assert(str); assert(strlen(str));
  for(uint i = 0; i < e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none; i++) {
    const char *cmp = getString__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t((e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t)i);
    assert(cmp); assert(strlen(cmp));
    if(strncasecmp(cmp, str, strlen(cmp))) {
      return (e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t)i;
    }
  }
  //!
  //! Not found:
  return e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none;
}

#ifndef macro_min
#define macro_min(x, y)	((x) < (y) ? (x) : (y))
#endif

//! @return the e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t representaiton of e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t
static e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t get_enum_fromString__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(const char *stringOf_enum) {
  assert(stringOf_enum);
  assert(strlen(stringOf_enum));
  if(0 == strncmp("median", stringOf_enum, macro_min(strlen(stringOf_enum), strlen("median")))) {
    return e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_median;
  } else if(0 == strncmp("mean", stringOf_enum, macro_min(strlen(stringOf_enum), strlen("mean")) )) {
    return e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_mean;
  } else if(0 == strncmp("STD", stringOf_enum, macro_min(strlen(stringOf_enum), strlen("STD")) )) {
    return e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_STD;
  } else if(0 == strncmp("none", stringOf_enum, macro_min(strlen(stringOf_enum), strlen("none")) )) {
    return e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none;
  }
  //!
  //! Warn if the user-configuraiton was not properly supported:
  fprintf(stderr, "!!\t None of the if-clauses matched for input-string=\"%s\", ie, please investigate your call: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_enum, __FUNCTION__, __FILE__, __LINE__);
  return e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_none;
}

/**
   @struct s_kt_matrix_filter
   @brief provide filter-logics for a matrix of values (oekseth, 06. otk. 2016)
   @remarks while the "row-masks" results in a direct 'removal' of enties the "column-masks" result in a seperate 'masking' (ie, a partial usage). From the latter we observe that the specifciaotn of maks results in a fragmented array of values, ie, for which a non-masked optmzied software results in signficant incrase in equeciotn-time (as the non-optmized software-implemtantiosn evalaute large number of cases which will never be considered/used). 
 **/
typedef struct s_kt_matrix_filter {
  //! --------------------------------
  //!
  //! Thresholds which are to be applied to rows: filter rows wrt. 'either-or':
  t_float thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_row[2];  t_float thresh_minMax__eitherOr_percentValuesAboveCountForScalarValue_columns[2];
  t_float thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent_scalarAbsMin;
  t_float thresh_minMax__eitherOr_absoluteValueMinValue_row_cols_perCent[2]; 
  t_float thresh_minMax__eitherOr_valueDiff_rows[2];   t_float thresh_minMax__eitherOr_valueDiff_cols[2]; 
  t_float thresh_minMax__eitherOr_STD_rows[2];   t_float thresh_minMax__eitherOr_STD_cols[2];
  t_float thresh_minMax__eitherOr_kurtosis_rows[2];   t_float thresh_minMax__eitherOr_kurtosis_cols[2];
  t_float thresh_minMax__eitherOr_skewness_rows[2];   t_float thresh_minMax__eitherOr_skewness_cols[2];

  //! --------------------------------
  //!
  //! Thresholds which are to be applied to rows: filter rows and columns wrt. 'speicfic masking of each cell', ie, where parts of rous--columns may be of interest.
  t_float thresh_minMax__cellMask_valueUpperLower_matrix[2]; 
  t_float thresh_minMax__cellMask_meanAbsDiff_row[2];    t_float thresh_minMax__cellMask_meanAbsDiff_column[2]; //! ie, use the difference from the "mean" to filter values
  t_float thresh_minMax__cellMask_medianAbsDiff_row[2];    t_float thresh_minMax__cellMask_medianAbsDiff_column[2]; //! ie, use the difference from the "median" to filter values
 
  /* //! -------------------------------- */
  /* //! */
  /* //! Results wrt. the thresholds: */
  /* bool *mapOfVertex_result_ofInterest; //! where we set a 'truth-value' for each vertex based on [abov€] filter-criteria */

  
  // FIXME: (a) update our web-interface wrt. [ªbove] 'enw-added features' ... and thereafter (b) write a FAQ-scaffold which dewscribed/otuliens the cosnierations wrt. [ªbove].
  // FIXME: write lgocis wrt. 'this'  (a) compute for the rows ... (b) try to 'codnecne/remap the rows into a new matrix' ... (c) copmtue for the columns ... and update a mask-table ... (d) ''remove columsn which are never of interest, budilng a new consdnesed matrix with back\-traking index-table-look-ups' ... (e) write test-cases using different random-generated data-sets .... (f) test wrt. combiantion of different/mulipel matrices .... (g) test wrt. comptuation fo dsjoitn-forests

  //! --------------------------------
  //!
  //! Configuraiton-settings to adjust he valeus after thresholds are applied:
  /* bool isToAdjust_useLogTransform; */   /* bool isToNormalize_rows; */   /* bool isToNormalize_cols; */
  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t typeOf_adjust_subtractOrAdd_rows;
  e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t typeOf_adjust_subtractOrAdd_cols;

  //! --------------------------------
  //!
  //! Temporary data:
  t_float **matrix; uint nrows; uint ncols;
  char *mapOf_interesting_rows;   
  char *mapOf_interesting_cols; 

  //! --------------------------------
  //! 
  //! The order to apply the filters:
  e_kt_matrix_filter_orderOfFilters_eitherOr_t orderOf_filter_eitherOr[e_kt_matrix_filter_orderOfFilters_eitherOr_undef]; 
  e_kt_matrix_filter_orderOfFilters_cellMask_t orderOf_filter_cellMask[e_kt_matrix_filter_orderOfFilters_cellMask_undef];
  e_kt_matrix_filter_adjustValues_t orderOf_adjustment[e_kt_matrix_filter_adjustValues_undef];
  //! Local counters:
  uint orderOf_filter_eitherOr_pos;
  uint orderOf_filter_cellMask_pos;
  uint orderOf_adjustment_pos;
  
  //! --------------------------------
} s_kt_matrix_filter_t;

//! Intiaite the object
void setTo_empty__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self);
//! Intiaite the object
void allocate__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const bool isTo_init, t_float **matrix, const uint nrows, const uint ncols);
//! Intiaite the object using the "s_kt_matrix_t" object (oekseth, 06. apr. 2017)
static void allocate__fromObj__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const s_kt_matrix_t *mat, const bool isTo_init, const char *str_typeOf_adjust_subtractOrAdd_rows, const char *str_typeOf_adjust_subtractOrAdd_cols) {
  assert(self); assert(mat); 
  allocate__s_kt_matrix_filter_t(self, isTo_init, mat->matrix, mat->nrows, mat->ncols);
  if(str_typeOf_adjust_subtractOrAdd_rows && strlen(str_typeOf_adjust_subtractOrAdd_rows)) {
    self->typeOf_adjust_subtractOrAdd_rows = get_enum__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(str_typeOf_adjust_subtractOrAdd_rows);
  }
  if(str_typeOf_adjust_subtractOrAdd_cols && strlen(str_typeOf_adjust_subtractOrAdd_cols)) {
    self->typeOf_adjust_subtractOrAdd_cols = get_enum__e_kt_matrix_filter_typeOfAdjust_SubtractOrAdd_t(str_typeOf_adjust_subtractOrAdd_cols);
  }
}
//! Use pre-defined configuraitons to intiate the s_kt_matrix_filter_t object.
s_kt_matrix_filter_t init_setDefaultValues__s_kt_matrix_filter(t_float **matrix, const uint nrows, const uint ncols);
//! De-allocate the object
void free__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self);
//! -------------------------------------------------------------
//! -------------------------------------------------------------
//! Apply either-or filters
void applyFilter__eitherOr__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_id);
//! Apply cell-mask filters
void applyFilter__cellMask__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id);
//! Apply value-adjustment filters
void adjustValue__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_adjustValues_t enum_id);

/**
   @brief Apply the iether-or filters, cell-mask filters and the adjustment-filters (oekseth, 06. otk. 2016).
   @param <orderOf_filter_eitherOr> is the an enum of type "orderOf_filter_eitherOr" where the [index_id] is the filter-id to apply and the "index_id" describes the applicaiton-order and the
   @param <orderOf_filter_cellMask> is the an enum of type "e_kt_matrix_filter_orderOfFilters_cellMask_t" where the [index_id] is the filter-id to apply and the "index_id" describes the applicaiton-order and the 
   @param <orderOf_adjustment> is the an enum of type "e_kt_matrix_filter_adjustValues_t" where the [index_id] is the filter-id to apply and the "index_id" describes the applicaiton-order and the 

   @remarks the result is an updated s_kt_matrix_filter_t object wrt.: "mapOf_interesting_rows", "mapOf_interesting_cols" and "matrix", where the "matrix" is udpated wrt. empty-valeus using the "T_FLOAT_MAX" proerpty to speicfy 'emptyness'.
**/
void apply_s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_eitherOr_t orderOf_filter_eitherOr[e_kt_matrix_filter_orderOfFilters_eitherOr_undef], const e_kt_matrix_filter_orderOfFilters_cellMask_t orderOf_filter_cellMask[e_kt_matrix_filter_orderOfFilters_cellMask_undef], const e_kt_matrix_filter_adjustValues_t orderOf_adjustment[e_kt_matrix_filter_adjustValues_undef]);
//void apply_s_kt_matrix_filter_t(s_kt_matrix_filter_t *self, const uint orderOf_filter_eitherOr[e_kt_matrix_filter_orderOfFilters_eitherOr_undef], const uint orderOf_filter_cellMask[e_kt_matrix_filter_orderOfFilters_cellMask_undef], const uint orderOf_adjustment[e_kt_matrix_filter_adjustValues_undef]) ;


//! -------------------------------------------------------------
//! -------------------------------------------------------------

//! Update the filter-applicaiton-order or the "e_kt_matrix_filter_orderOfFilters_eitherOr_t" enum.
//! @remarks is only updated if there does not exist any previous 'enums' for this, ie, applied only once.
void updateOrderOf_filter__eitherOr__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_id);
//! Handle a stirng-input wrt. the [ªbove] funciotn. 
static void string__updateOrderOf_filter__eitherOr__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const char *str) {
  assert(self); assert(str); assert(strlen(str));
  const e_kt_matrix_filter_orderOfFilters_eitherOr_t enum_id = get_enum__e_kt_matrix_filter_orderOfFilters_eitherOr_t(str);
  if(enum_id != e_kt_matrix_filter_orderOfFilters_eitherOr_undef) {
    //! Apply logics:
    updateOrderOf_filter__eitherOr__kt_matrix_filter_t(self, enum_id);
  }
}
//! Update the filter-applicaiton-order or the "e_kt_matrix_filter_orderOfFilters_cellMask_t" enum.
//! @remarks is only updated if there does not exist any previous 'enums' for this, ie, applied only once.
void updateOrderOf_filter__cellMask__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id);
//! Handle a stirng-input wrt. the [ªbove] funciotn. 
static void string__updateOrderOf_filter__cellMask__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const char *str) {
  assert(self); assert(str); assert(strlen(str));
  const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_id = get_enum__e_kt_matrix_filter_orderOfFilters_cellMask_t(str);
  if(enum_id != e_kt_matrix_filter_orderOfFilters_cellMask_undef) {
    //! Apply logics:
    updateOrderOf_filter__cellMask__kt_matrix_filter_t(self, enum_id);
  }
}
//! Update the filter-applicaiton-order or the "e_kt_matrix_filter_adjustValues_t" enum.
//! @remarks is only updated if there does not exist any previous 'enums' for this, ie, applied only once.
void updateOrderOf_adjust__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const e_kt_matrix_filter_adjustValues_t enum_id);
//! Handle a stirng-input wrt. the [ªbove] funciotn. 
static void string__updateOrderOf_adjust__kt_matrix_filter_t(s_kt_matrix_filter_t *self, const char *str) {
  assert(self); assert(str); assert(strlen(str));
  const e_kt_matrix_filter_adjustValues_t enum_id = get_enum__e_kt_matrix_filter_adjustValues_t(str);
  if(enum_id != e_kt_matrix_filter_adjustValues_undef) {
    //! Apply logics:
    updateOrderOf_adjust__kt_matrix_filter_t(self, enum_id);
  }
}
/* //! Apply the filters based on the cufferent configuraiton-objects */
/* void apply_internalOrderSpec__s_kt_matrix_filter_t(s_kt_matrix_filter_t *self); */

#endif //! EOF
