#ifndef quick_sort_h
#define quick_sort_h
#include "libs.h"
//#include "list.h"
 
/**
   @Name: quick_sort -- Sorts input data based on the quick-sort algorithm,
   - Follows the principle (of this algo) comparing a uniqe pair only once, terby doing the opartion in-place with regard to memory consumption.
   - Using templates, types of input data for comparison may be abitrary.
   @Major_change: 23.06.2011 by Ole Kristian Ekseth (oekseth)
 */
typedef class quick_sort {
 private:
    /**! Swaps the elements */
  template<typename T> void swap(T &a, T &b) {const T temp = b; b = a; a = temp;}

  /**
     uint partition(..)       -- Moves r such that [ .,.,.,.,.,.,r |,|,|,|,|,| ]
     (i.e. those to left are lower than r, and those to right are bigger.)
     @Changed: 23.06.2011 by oekseth.
   */
  template <typename T>  uint partition(T *array, const uint p, const uint r) {
    const T x = array[r-1];
    int i = p - 1;
    for(uint j = p; j < r -1; j++) {
      if(array[j] <= x) {swap(array[++i], array[j]);}      
    }
    swap(array[++i], array[r-1]);
    return ++i;
  }

    /**
     uint partition_randomized(..)       -- Moves r such that [ .,.,.,.,.,.,r |,|,|,|,|,| ]
     (i.e. those to left are lower than r, and those to right are bigger.)
     @Changed: 24.06.2011 by oekseth.
   */
  template<typename T> uint partition_randomized(T *array, const uint p, const uint r) {
    const uint diff = r -p;
    const int i = rand() % diff + p;
    swap(array[i], array[r-1]);
    return partition(array, p, r);
  }

    /**
     quick_procedure_randomized -- The recursive procedure setting the pivot element in the middle, terby achiving in-place memory usage.
     @Changed: 23.06.2011 by oekseth
  */
  template <typename T> void quick_procedure_randomized(T *array, const uint p, const uint r) {
    if(p < (r-1)) {
      const uint q = partition_randomized(array, p, r);
      if(q>1)      quick_procedure_randomized(array, p, q-1);
      quick_procedure_randomized(array, q, r);
      
    }
  }


  /**
     quick_procedure -- The recursive procedure setting the pivot element in the middle, terby achiving in-place memory usage.
     @Changed: 23.06.2011 by oekseth
  */
  template <typename T> void quick_procedure(T *array, const uint p, const uint r) {
    if(p < (r-1)) {
      //printf("\t\t(");for(uint i = 0;i < r; i++) printf("%d, ", array[i]); printf(")\n");
      const uint q = partition(array, p, r);
      //      printf("#\tDivides into [%u, %u] and [%u, %u]\n", p, q, q, r);
      //      printf("\t\t("); for(uint i = 0;i < r; i++) printf("%d, ", array[i]); printf(")\n");
      //      printf("\t\t("); for(uint i = p;i < r; i++) printf("%d, ", array[i]); printf(")\n");
      
      if(q>1)      quick_procedure(array, p, q-1);
      quick_procedure(array, q, r);
      
    }
  }

 public:
  /**
     sort_array_randomized -- Sorts the array by moving elements forward
     until a a key that is greater than 'this' is found.
     @Changed: 24.06.2011 by oekseth
   */
  template <typename T> void sort_array_randomized(T *array, const uint size) {
    quick_procedure_randomized(array, 0, size);
  }
  /**
     sort_array -- Sorts the array by moving elements forward
     until a a key that is greater than 'this' is found.
     @Changed: 23.06.2011 by oekseth
   */
  template <typename T> void sort_array(T *array, const uint size) {
    quick_procedure(array, 0, size);
  }
  void delete_class() {}//{array.delete_class(); }
  quick_sort() {};//array = list_t(0);}

  //! Asserts the class
  static bool assert_class() {
    printf("--\tAsserts class 'quick_sort'.\n");
    quick_sort temp = quick_sort();
    const uint array_size = 10;
    int sorted_array[array_size] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    temp.sort_array(sorted_array, array_size);    
    //printf("array[%u] = %d\n", 0, sorted_array[0]); 
    for(uint i = 1; i < array_size; i++) {
      //printf("array[%u] = %d\n", i, sorted_array[i]); 
      assert(sorted_array[i] >=sorted_array[i-1]);
    }
    if(true) { // Uses a different data type, utilizing the template functions
      float sorted_array[array_size] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
      temp.sort_array(sorted_array, array_size);    
      //printf("array[%u] = %d\n", 0, sorted_array[0]); 
      for(uint i = 1; i < array_size; i++) {
	//printf("array[%u] = %d\n", i, sorted_array[i]); 
	assert(sorted_array[i] >=sorted_array[i-1]);
      }
    }
    if(true) { // Uses a randomized sort
      float sorted_array[array_size] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
      temp.sort_array_randomized(sorted_array, array_size);    
      //printf("array[%u] = %d\n", 0, sorted_array[0]); 
      for(uint i = 1; i < array_size; i++) {
	//printf("array[%u] = %d\n", i, sorted_array[i]); 
	assert(sorted_array[i] >=sorted_array[i-1]);
      }
    }
    //delete [] sorted_array;
    temp.delete_class();
    printf("ok\tAssert of class 'quick_sort' complete.\n");
    return true;
  }
} quick_sort_t;
#endif
