/* #ifndef metricMacro_innerProduct_h */
/* #define metricMacro_innerProduct_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_innerProduct
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "innerProduct".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics


//! @return the correlations-core for: inner-product
#define correlation_macros__distanceMeasures__innerProduct__innerProduct(term1, term2) ({ \
  const t_float mult = term1 * term2; \
  mult;})
#define correlation_macros__distanceMeasures__innerProduct__innerProduct__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(term1, term2);	\
  mult;})



//! @brief identify the correlations-core for: harmonic mean
#define correlation_macros__distanceMeasures__innerProduct__harmonicMean(term1, term2) ({ \
  const t_float mult = term1 * term2; \
  const t_float denumerator = term1 + term2; \
  /*! Update the result-object: */					\
  const t_float result = ( (mult != 0) && (denumerator != 0) ) ? (mult / denumerator) : 0; \
  result; }) //! ie, return
#define correlation_macros__distanceMeasures__innerProduct__harmonicMean__postProcess(distance) ({ \
      metric_macro_defaultFunctions__postProcess__multiplyBy_constant(distance, /*constant=*/2);})
// FXIME: validate that [”elow] does Not reuslt in INFFINITY valeus.
#define correlation_macros__distanceMeasures__innerProduct__harmonicMean__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(term1, term2);	\
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2); \
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_DIV(mult, denumerator);	\
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result;}) //! ie, return.

//! @return the correlations-core for: both "Cosine" and "Kumar Hassebrook" and "Jaccard" and "Dice" (where the differnece in the latter scores are first seen/comptued/deifentated when we 'adjust' the overal sum of values), ie, after all featrues i  the two comapred feature-rrows are ifnerred/comptued/summed).
#define correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice(term1, term2, obj) ({ \
  const t_float mult = term1 * term2; \
  const t_float sq_term1 = term1 * term1; \
  const t_float sq_term2 = term2 * term2; \
  /*! Update the result-object: */					\
  obj.numerator += mult; obj.denumeratorLimitedTo_row1 += sq_term1; obj.denumeratorLimitedTo_row2 += sq_term2;	\
})
#define correlation_macros__distanceMeasures__innerProduct__cosine_and_KumarHassebrook_or_Jaccard_or_Dice__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(term1, term2);	\
      const VECTOR_FLOAT_TYPE sq_term1 = VECTOR_FLOAT_MUL(term1, term1); \
      const VECTOR_FLOAT_TYPE sq_term2 = VECTOR_FLOAT_MUL(term2, term2); \
      /*! Update the result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, mult); obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, sq_term1); obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, sq_term2); \
    })
#define correlation_macros__distanceMeasures__innerProduct__cosine__postProcess(obj) ({ \
      const t_float numerator   = obj.numerator;	\
      const t_float denumerator = mathLib_float_sqrt(obj.denumeratorLimitedTo_row1) + mathLib_float_sqrt(obj.denumeratorLimitedTo_row2); \
      metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator); })
#define correlation_macros__distanceMeasures__innerProduct__KumarHassebrook_or_Jaccard__postProcess(obj) ({ \
  const t_float numerator   = obj.numerator; \
  const t_float denumerator = obj.denumeratorLimitedTo_row1 + obj.denumeratorLimitedTo_row2 - obj.numerator; \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator); })

#define correlation_macros__distanceMeasures__innerProduct__Dice__postProcess(obj) ({ \
  const t_float numerator   = 2*obj.numerator; \
  const t_float denumerator = obj.denumeratorLimitedTo_row1 + obj.denumeratorLimitedTo_row2; \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator); })

// FIXME: write a new fucntion 'reflecting' DiceAlt2

#define correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef(term1, term2, obj) ({ \
      const t_float diff = macro_minus(term1, term2);			\
      const t_float mult = diff * diff;					\
      const t_float sq_term1 = term1 * term1;				\
      const t_float sq_term2 = term2 * term2;				\
      const t_float sq_term_1_2 = term1 * term2;			\
  /*! Update the result-object: */					\
  obj.numerator += mult; obj.denumeratorLimitedTo_row1 += sq_term1; obj.denumeratorLimitedTo_row2 += sq_term2; obj.denumeratorLimitedTo_row_1and2 += sq_term_1_2;	\
})
#define correlation_macros__distanceMeasures__innerProduct__Jaccard_or_Dice_altDef__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_SUB(term1, term2);	\
      const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(diff, diff);	\
      const VECTOR_FLOAT_TYPE sq_term1 = VECTOR_FLOAT_MUL(term1, term1); \
      const VECTOR_FLOAT_TYPE sq_term2 = VECTOR_FLOAT_MUL(term2, term2); \
      const VECTOR_FLOAT_TYPE sq_term_1_2 = VECTOR_FLOAT_MUL(term1, term2); \
      /*! Update the result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, mult); obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, sq_term1); obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, sq_term2); \
      obj.denumeratorLimitedTo_row_1and2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row_1and2, sq_term_1_2); \
    })
#define correlation_macros__distanceMeasures__innerProduct__JaccardAltDef__postProcess(obj) ({ \
  const t_float numerator   = obj.numerator; \
  const t_float denumerator = obj.denumeratorLimitedTo_row1 + obj.denumeratorLimitedTo_row2 - obj.denumeratorLimitedTo_row_1and2; \
  t_float result = metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator); \
  result;}) //! where "1 - result" is used in order to 'get the same result' as in ["Jaccard", "Dice"], ie, as given/seen in ref=["http://users.uom.gr/~kouiruki/sung.pdf"]
//metric_macro_defaultFunctions__postProcess__1_minusValue_xmtDefaultAdjustment(result);}) //! where "1 - result" is used in order to 'get the same result' as in "Dice" ["http://users.uom.gr/~kouiruki/sung.pdf"]
#define correlation_macros__distanceMeasures__innerProduct__DiceAltDef__postProcess(obj) ({ \
  const t_float numerator   = obj.numerator; \
  const t_float denumerator = obj.denumeratorLimitedTo_row1 + obj.denumeratorLimitedTo_row2; \
  t_float result = metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator); \
  result;}) //! where "1 - result" is used in order to 'get the same result' as in "Dice" ["http://users.uom.gr/~kouiruki/sung.pdf"]
//assert(result <= 1); 
//metric_macro_defaultFunctions__postProcess__1_minusValue_xmtDefaultAdjustment(result);}) //! where "1 - result" is used in order to 'get the same result' as in "Dice" ["http://users.uom.gr/~kouiruki/sung.pdf"]


//! @return the temporal sample-coveraicne ("https://en.wikipedia.org/wiki/Distance_correlation").
#define correlation_macros__distanceMeasures__innerProduct__sampleCoVariance(value1, value2) ({ value1 * value2;}) //! ie, return teh inner-product
#define correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__SSE(value1, value2) ({ VECTOR_FLOAT_MUL(value1,  value2);}) //! ie, return teh inner-product
#define correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__postProcess(result, ncolsAndWeight, ncolsAndWeight_inverse) ({ \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result) == false);  \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result) == false);  \
  /*! -- */ \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(ncolsAndWeight_inverse) == false);  \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(ncolsAndWeight_inverse) == false);  \
  /*! -- */ \
  const t_float result_updated = result * (ncolsAndWeight_inverse * ncolsAndWeight_inverse); \
  /*! -- */ \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_updated) == false);  \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_updated) == false);  \
  /*! -- */ \
  const t_float m_ret_val = (result_updated != 0) ? metric_macro_defaultFunctions__postProcess__validateEmptyCase(mathLib_float_sqrt_abs(result_updated)) : 0; \
  /*! -- */ \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(m_ret_val) == false);  \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(m_ret_val) == false);  \
  /*! -- */ \
  m_ret_val;}) //! ie, return teh square-root.

//! Copmute the "sample-correlation", a correlatoin which also descirbes/'is' the "Brownian Co-variance":
#define correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance(value1, value2, obj) ({ \
  obj.numerator = correlation_macros__distanceMeasures__innerProduct__sampleCoVariance(value1, value2); \
  obj.denumeratorLimitedTo_row1 = value1 * value1; \
  obj.denumeratorLimitedTo_row2 = value2 * value2; \
})
#define correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__SSE(value1, value2, obj) ({ \
  obj.numerator = correlation_macros__distanceMeasures__innerProduct__sampleCoVariance__SSE(value1, value2); \
  obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_MUL(value1, value1);	\
  obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_MUL(value2, value2); \
})
// FIXME: vlaidat ethe correctness of Not 'takign the quare-root' wrt. the de-numerator in [”elow]
// FIXME: in [”elow] valdiate that the "macro_max(0, " ..)" 'ensures' that we manage to handle the NANV-valeus proerply
//! @remarks the "sample-correlation" nor "browiand correlation" nor "sampleCorrelation" are described ion ["http://users.uom.gr/~kouiruki/sung.pdf"], ie, our work extends the latter
#define correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__postProcess(obj) ({ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
  const t_float ncols_mult_inverse = ncolsAndWeight_inverse * ncolsAndWeight_inverse; \
  const t_float numerator = mathLib_float_sqrt(obj.numerator * ncolsAndWeight_inverse); \
  const t_float denumerator_row1 = obj.denumeratorLimitedTo_row1;				\
  const t_float denumerator_row2 = obj.denumeratorLimitedTo_row2;				\
  t_float result_updated = (numerator / (mathLib_float_sqrt(denumerator_row1 * denumerator_row2 * ncolsAndWeight_inverse)));  \
  result_updated = macro_max(result_updated, 0);  \
  metric_macro_defaultFunctions__postProcess__validateEmptyCase(result_updated); }) //! ie, return the result.

// FIXME: combien [”elow] adn [above] ... wrt. the "e_kt_correlationFunction_groupOp_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator," enum

#define correlation_macros__distanceMeasures__innerProduct__sampleCorrelation_or_BrownianCoVariance__permutated_innerRootInDenumerator__postProcess(obj) ({ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
  const t_float ncols_mult_inverse = ncolsAndWeight_inverse * ncolsAndWeight_inverse; \
  const t_float numerator = mathLib_float_sqrt(obj.numerator * ncolsAndWeight_inverse); \
  const t_float denumerator_row1 = obj.denumeratorLimitedTo_row1 * ncolsAndWeight_inverse;				\
  const t_float denumerator_row2 = obj.denumeratorLimitedTo_row2 * ncolsAndWeight_inverse;				\
  t_float result_updated = (numerator / (mathLib_float_sqrt(mathLib_float_sqrt(denumerator_row1) * mathLib_float_sqrt(denumerator_row2)))); \
  result_updated = macro_max(result_updated, 0);  \
  metric_macro_defaultFunctions__postProcess__validateEmptyCase(result_updated); }) //! ie, return the result.


/* //! Compute measure for: Brownian */
/* #define correlation_macros__distanceMeasures__brownian(value1, value2, term) ({ \ */
/*   const t_float max_val = max(value1, value2);				\ */
/*   /\*! Values are of interest only if they are non-negative: *\/ \ */
/*   if(max_val > 0) {term = sqrt(min(value1, value2) / max_val);} else {term = 0;} \ */
/*   /\*term = value1 - value2; *\/ \ */
/*   term = term*term;}) */
/* #define correlation_macros__distanceMeasures__brownian_getRetVal(value1, value2) ({ \ */
/*   t_float term = 0; correlation_macros__distanceMeasures__brownian(value1, value2, term); \ */
/*   term;}) //! ie, terun  */
/* //! Compute measure for: Brownian and adjusted by a weigth-factor */
/* #define correlation_macros__distanceMeasures__brownian_weight(value1, value2, term, weight, weight_index) ({ \ */
/*   t_float term = 0; correlation_macros__distanceMeasures__brownian(value1, value2, term); \ */
/*   const t_float ret_val = correlation_macros__distanceMeasures__multiplyBy_weight(term, weight, weight_index); \ */
/*   ret_val;}) //! ie, return the adjusted weight */
/* //! Compute measure for: Brownian and adjusted by a weigth-factor */
/* #define correlation_macros__distanceMeasures__brownian_weight_getRetVal(value1, value2, weight, weight_index) ({ \ */
/*   t_float term = 0; const t_float ret_Val = correlation_macros__distanceMeasures__brownian_weight(value1, value2, term, weight, weight_index); \ */
/*   ret_Val;}) //! ie, return */


/* // FIXME[performance] write perofrmance-tests to evlauat ethe tiem-effects of using 'these' SSE-matcros Vs the non-SSe-macros. */
/* // FIXME: write correctness-tests to validate that the borwnain correlation does not reslt in 'un-expected' NANSs (and/or enusre that hte caller ahdnles such a case) <-- ensure/validate that the callers of 'this' handles/'fixes' NAN-values. */

/* //! Compute measure for: Brownian */
/* #define correlation_macros__distanceMeasures__brownian_SSE(val1, val2) ({ \ */
/*   VECTOR_FLOAT_TYPE mul; \ */
/* 	const VECTOR_FLOAT_TYPE vec_max = VECTOR_FLOAT_MAX(VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET_zero(), term2), term1); \ */
/* 	const VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_MIN(VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET_zero(), term2), term1); \ */
/* 	const VECTOR_FLOAT_TYPE vec_divFactor = VECTOR_FLOAT_DIV(vec_min, vec_max); \ */
/* 	const VECTOR_FLOAT_TYPE vec_compareMax_zeros = VECTOR_FLOAT_CMPLT(vec_max, VECTOR_FLOAT_SET_zero()); \ */
/* 	const VECTOR_FLOAT_TYPE vec_ofInterest = VECTOR_FLOAT_ANDNOT(vec_compareMax_zeros, vec_max); \ */
/* 	const VECTOR_FLOAT_TYPE vec_valuesWithAnswer = VECTOR_FLOAT_AND(vec_compareMax_zeros, VECTOR_FLOAT_SQRT(vec_divFactor)); \ */
/* 	mul = VECTOR_FLOAT_OR(vec_ofInterest, vec_valuesWithAnswer); \ */
/* 	mul;}) //! ie, return. */

/* //! Compute measure for: Brownian and adjusted by a weigth-factor */
/* // FIXME[correcntess] valdiate that we do not need to 'reverse the order' after [”elow] SSE-comptaution. */
/* #define correlation_macros__distanceMeasures__brownian_SSE_weight(val1, val2, vec_weight) ({ \ */
/*   VECTOR_FLOAT_TYPE mul = correlation_macros__distanceMeasures__brownian_SSE(val1, val2); \ */
/*   mul = VECTOR_FLOAT_MUL(mul, VECTOR_FLOAT_SET(vec_weight));		\ */
/*   mul;}) //! ie, return. */


/* #define correlation_macros__distanceMeasures__innerProduct__innerProduct(term1, term2) ({ \ */
/*   const t_float mult = term1 * term2; \ */
/*   mult;}) */
/* #define correlation_macros__distanceMeasures__innerProduct__innerProduct__SSE(term1, term2) ({ \ */
/*       const VECTOR_FLOAT_TYPE mult = VECTOR_FLOAT_MUL(term1, term2);	\ */
/*   mult;}) */



// #endif //! EOF
