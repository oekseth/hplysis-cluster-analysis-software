#include "measure_kt_forest_findDisjoint.h"
  /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "kt_forest_findDisjoint.h"
#include "kt_matrix_filter.h"
#include "math_generateDistribution.h"
#include "correlation_macros__distanceMeasures.h"
#include "kt_matrix.h"
static  const uint arrOf_chunkSizes_size = 7;
//! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
static  const uint base_size =  VECTOR_INT_ITER_SIZE_short;
static  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      
							      20*base_size,
							      40*base_size, 
							      80*base_size, 
							      160*base_size, 
							      320*base_size, 
							      640*base_size, 
							      //2*kilo*base_size,
							      //3*kilo*base_size, 4*kilo*base_size,
							      //80*kilo*base_size, 160*kilo*base_size,
							      //300*kilo*base_size, 600*kilo*base_size,
};
static const uint cnt_row_fractions = 10;
static const t_float mapOf_fraction[cnt_row_fractions] = {
  0.125, 0.25, 0.5, 0.75, 1.0, 
  1.125, 1.25, 1.5, 1.75, 2.0, 
};



static void __add_relation(t_float **matrix, const uint head, const uint tail) {
  assert(matrix[head]); 
  //assert(head != tail);  
  matrix[head][tail] = 1;
}

static void correctnessTests_case1__allOf_interest() {
  const t_float default_value_float__ = 0;
  const uint nrows = 10;   const uint ncols = 10;  //! ie, an adjencency-matrix:
  t_float **matrix = allocate_2d_list_float(nrows, ncols, default_value_float__); 
  const t_float default_value_float = T_FLOAT_MAX; //! ie, 'state' tha we by defualt does not have any relatiosnhips.
  for(uint row_id = 0; row_id < nrows; row_id++) {for(uint col_id = 0; col_id < ncols; col_id++) {matrix[row_id][col_id] = default_value_float;}}
  //! ---------------------------
  //! Add relationships for forest(1), ie, investigate wrt.: (a) transitvity, (b) 'forest-merge-points' and (c) 'self-cyclic relationships':
  __add_relation(matrix, /*head=*/0, /*tail=*/1);  __add_relation(matrix, /*head=*/1, /*tail=*/2); __add_relation(matrix, /*head=*/3, /*tail=*/2); __add_relation(matrix, /*head=*/2, /*tail=*/3);
  //! Add relationships for forest(2): 'reciprocal iseland':
  __add_relation(matrix, /*head=*/4, /*tail=*/5);  __add_relation(matrix, /*head=*/5, /*tail=*/4); 

  { //! First cosntruct the object 'by hand' and thereafter vlaidate the results
  //! ----------------------------------------------------------------------------------------------------------------------------------------
    const bool isTo_useImplictMask = false;     char **mask = NULL;
    s_kt_set_2dsparse_t listOf_relations; allocate__s_kt_set_2dsparse_t(&listOf_relations, nrows, ncols, matrix, mask, isTo_useImplictMask, /*isTo_allocateFor_scores=*/false);
    /* if(true) { //! then write out the relations: */
    /*   for(uint row_id = 0; row_id < nrows;  */
    /* } */

    s_kt_forest_findDisjoint self; allocate__s_kt_forest_findDisjoint(&self, nrows, &listOf_relations);
    //! -----------------------------
    //!
    //! Infer the disjotin sets:
    graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);
    
    //! -----------------------------
    //!
    //! Identify the number of itneresting clusters:
    const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
    const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
    printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
    //! Validate the result:
    //! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
    assert(cnt_total_vertices == nrows); //! ie, the totla number of unique vertices inserted in [above]
    assert(cnt_interesting_disjointForests == 1); //! ie, what we expect.

    
    //! -----------------------------
    //! De-allocate:
    free_s_kt_forest_findDisjoint(&self);
    free_s_kt_set_2dsparse_t(&listOf_relations);    
  }
/*   { //! Invesigate wrt. a 'genrlaized case' where we are intersted in the number of cycles: */
/* #define __config_kt_forest_findDisjoint__hideErrorMEssage //! ie, to avodi the funciton from comlainign abotu our itneiionally point-less call */
/*     const uint cnt_clusters = find_cntClusters_inMatrix__s_kt_forest_findDisjoint(nrows, ncols, matrix, /\*mask=*\/NULL, /\*isTo_useImplictMask=*\/false, /\*countThreshold_min=*\/UINT_MAX, /\*countThreshold_max=*\/UINT_MAX); */
/* #undef __config_kt_forest_findDisjoint__hideErrorMEssage */
/*     assert(cnt_clusters == 2); //! ie, what we expect. */
/*   } */

  //! ----------------------
  //! De-allocate, ie, complete:
  free_2d_list_float(&matrix, nrows);
}

static void correctnessTests_case1() {
  const t_float default_value_float__ = 0;
  const uint nrows = 10;   const uint ncols = 10;  //! ie, an adjencency-matrix:
  t_float **matrix = allocate_2d_list_float(nrows, ncols, default_value_float__); 
  const t_float default_value_float = T_FLOAT_MAX; //! ie, 'state' tha we by defualt does not have any relatiosnhips.
  for(uint row_id = 0; row_id < nrows; row_id++) {for(uint col_id = 0; col_id < ncols; col_id++) {matrix[row_id][col_id] = default_value_float;}}
  //! ---------------------------
  //! Add relationships for forest(1), ie, investigate wrt.: (a) transitvity, (b) 'forest-merge-points' and (c) 'self-cyclic relationships':
  __add_relation(matrix, /*head=*/0, /*tail=*/1);  __add_relation(matrix, /*head=*/1, /*tail=*/2); __add_relation(matrix, /*head=*/3, /*tail=*/2); __add_relation(matrix, /*head=*/2, /*tail=*/3);
  //! Add relationships for forest(2): 'reciprocal iseland':
  __add_relation(matrix, /*head=*/4, /*tail=*/5);  __add_relation(matrix, /*head=*/5, /*tail=*/4); 

  const bool isTo_useImplictMask = true;     
  { //! First cosntruct the object 'by hand' and thereafter vlaidate the results
  //! ----------------------------------------------------------------------------------------------------------------------------------------
    char **mask = NULL;
    {
      s_kt_set_2dsparse_t listOf_relations; allocate__s_kt_set_2dsparse_t(&listOf_relations, nrows, ncols, matrix, mask, isTo_useImplictMask, /*isTo_allocateFor_scores=*/false);    
      s_kt_forest_findDisjoint self; allocate__s_kt_forest_findDisjoint(&self, nrows, &listOf_relations);
      //! -----------------------------
      //!
      //! Infer the disjotin sets:
      graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);
      
      //! -----------------------------
      //!
      //! Identify the number of itneresting clusters:
      const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
      const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
      printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
      //! Validate the result:
      //! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
      assert(cnt_total_vertices == 6); //! ie, the totla number of unique vertices inserted in [above]
      assert(cnt_interesting_disjointForests == 2); //! ie, what we expect.    
      //! -----------------------------
      //! De-allocate:
      free_s_kt_forest_findDisjoint(&self);
      free_s_kt_set_2dsparse_t(&listOf_relations);    
    }
    { //! A similar test where we use a dense matrix as nput:
      s_kt_forest_findDisjoint self; allocate__denseMatrix__s_kt_forest_findDisjoint(&self, nrows, ncols, matrix, mask, default_value_float);
      //! -----------------------------
      //!
      //! Infer the disjotin sets:
      graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);      
      //! -----------------------------
      //!
      //! Identify the number of itneresting clusters:
      const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
      const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
      printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
      //! Validate the result:
      //! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
      assert(cnt_total_vertices == 6); //! ie, the totla number of unique vertices inserted in [above]
      assert(cnt_interesting_disjointForests == 2); //! ie, what we expect.    
      //! -----------------------------
      //! De-allocate:
      free_s_kt_forest_findDisjoint(&self);
    }
  }
  { //! Invesigate wrt. a 'genrlaized case' where we are intersted in the number of cycles:
    const uint cnt_clusters = find_cntClusters_inMatrix__s_kt_forest_findDisjoint(nrows, ncols, matrix, /*mask=*/NULL, /*isTo_useImplictMask=*/isTo_useImplictMask, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX, /*inputMatrix__isAnAdjecencyMatrix=*/(nrows == ncols));
    assert(cnt_clusters == 2); //! ie, what we expect.
  }

  //! ----------------------
  //! De-allocate, ie, complete:
  free_2d_list_float(&matrix, nrows);
}

//! Test the correctenss of our approaches
static void test_correctness() {
  correctnessTests_case1();
  correctnessTests_case1__allOf_interest();
}

//! Apply extensive perfomrance-tests:
static void extensive_compare_for_differentMaskDegrees() {
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_chunkSizes_size, default_value_float);

  //! -------------------------------
  //! Perform muliple iteraitons 
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint ncols = arrOf_chunkSizes[chunk_index];
    for(uint row_fraction = 0; row_fraction < cnt_row_fractions; row_fraction++) {
      const uint nrows = macro_max((uint)(t_float)(VECTOR_FLOAT_ITER_SIZE*((ncols * mapOf_fraction[row_fraction]/VECTOR_FLOAT_ITER_SIZE))), VECTOR_FLOAT_ITER_SIZE*4);
      // FIXME: incldue [ªbove] and remvoe [”elow]
      //const uint nrows = ncols;
      const uint size_of_array = ncols;

      //!
      //! Iterate through difference sparseness-degrees:
      const uint mapOf_sparseness_size = 4;
      const t_float mapOf_sparseness[mapOf_sparseness_size] = {
	0.0125, 0.025, 0.05, 0.075
      };
      for(uint sparse_id = 0; sparse_id < mapOf_sparseness_size; sparse_id++) {
	//! Allocate the matrix:
	t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
	//! Generate the valeus using a unifrom fistrubiton:
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint col_id = 0; col_id < ncols; col_id++) {
	    matrix[row_id][col_id] = 2 + math_generateDistribution__uniform();
	  }
	}
	//!
	char stringOf_measureText[1000]; memset(stringOf_measureText, '\0', 1000);
	sprintf(stringOf_measureText, "evaluate mask-set for a sparse-fraction=%f, for which we apply matrix-evaluation for a \"%s\" data-filter for a data-set with a distribution=\"uniform\"", mapOf_sparseness[sparse_id], "mean-percent cell-mask" );
	const char *stringOf_measureText_base = stringOf_measureText;	  
	{ //! Apply the filter: measure the time seprately for the matrix-filter-case:
	  //! -------------------------------
	  s_kt_matrix_filter self = init_setDefaultValues__s_kt_matrix_filter(matrix, nrows, ncols);	
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  //! The operation:
	  const e_kt_matrix_filter_orderOfFilters_cellMask_t enum_type = e_kt_matrix_filter_orderOfFilters_cellMask_inRangeFor_mean_percent;
	  //! Speficy the sparseness-filter:
	  self.thresh_minMax__cellMask_meanAbsDiff_row[0] = mapOf_sparseness[sparse_id]*100; 	  self.thresh_minMax__cellMask_meanAbsDiff_row[1] = mapOf_sparseness[sparse_id]*100;
	  //! Apply the logics, ie, fitler wrt. the cells:
	  applyFilter__cellMask__s_kt_matrix_filter_t(&self, enum_type);
	  
	  //! Write out the exeuciotn-time:
	  __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, ncols, mapOf_timeCmp_forEachBucket[chunk_index]); 	
	  free__s_kt_matrix_filter_t(&self);
	}
	{ //! Identify disjtoint-networks, both wrt. exeuciton-time and dsitjoint-forest-count:
	  uint cntOf_interest = 0; uint cnt_notOfItnerest = 0;
	  { //! First vlaidate taht tehre exists valeus which are of interest and not-of-interest:
	    for(uint row_id = 0; row_id < nrows; row_id++) { for(uint col_id = 0; col_id < ncols; col_id++) { if(isOf_interest(matrix[row_id][col_id])) {cntOf_interest++;} else {cnt_notOfItnerest++;} } }
	  }
	  if(!cnt_notOfItnerest || !cntOf_interest) {
	    printf("(info)\t the filter-ops sparsness-degree=%f did not result in any 'hidden' regions, ie, cntOf_interest=%u, and cnt_notOfItnerest=%u, ie, no point in disjoint-forest-computation: for details see [%s]:%s:%d\n", mapOf_sparseness[sparse_id],  cntOf_interest, cnt_notOfItnerest, __FUNCTION__, __FILE__, __LINE__);
	    // assert(false); // FIXME: remove
	  } else {
#include "measure_func__disjointForest.c"
	  }
	  //assert(false); // FIXME: .... add tests for different implemetnaiton-strategies ... 

	}	
	//! ----------------------------
	//! De-allcoate meory:
	assert(matrix);       free_2d_list_float(&matrix, nrows);
      }
    }
  }


  //! ----------------------------
  //! De-allcoate meory:
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}

//! ***********************************************************************
//!       Macros used in correctness-testing/harnessing of our matrix-disjotin-cases:
//! ***********************************************************************
#define __inLove(a, b) ({assert(partOf_same_disjointForest__s_kt_forest_findDisjoint(&obj_disjoint, a, b));}) //! ie, a macro to both simplify our testing and 'make us smile' (oekseth, 06. des. 201&).
#define __atWar(a, b) ({assert(false == partOf_same_disjointForest__s_kt_forest_findDisjoint(&obj_disjoint, a, b));}) //! ie, a macro to both simplify our testing and 'make us smile' (oekseth, 06. des. 201&).
//! Extend __inLove(..) to handle seperate cases wrt. Boys and girls (or: rows and columns).
#define __inLove__girlsBoys(a, b) ({ \
  const uint b_adjusted = b + globalStartPos_offset1; \
  __inLove(a, b_adjusted);})
#define __inLove__girlsAllToBeautiful(a, b) ({ \
  const uint b_adjusted = b + globalStartPos_offset2; \
  __inLove(a, b_adjusted);})
//! Extend __atWar(..) to handle seperate cases wrt. Boys and girls (or: rows and columns).
#define __atWar__girlsBoys(a, b) ({ \
  const uint b_adjusted = b + globalStartPos_offset1; \
  __atWar(a, b_adjusted);})

#define __atWar__girlsAllToBeautiful(a, b) ({ \
  const uint b_adjusted = b + globalStartPos_offset2; \
  __atWar(a, b_adjusted);})

  //! vlaidate that all 'entites' are found in the arrOf_result set:
#define __assert_listsAreEqualTo_clusterMemberships(arrOf_cmp, cnt_expected) ({ \
    assert(cnt_expected == arrOf_result_size); \
    assert(arrOf_result); \
    for(uint i = 0; i < cnt_expected; i++) { bool isFouund = false; const uint key = arrOf_result[i]; \
    for(uint k = 0; k < cnt_expected; k++) {if(key == arrOf_cmp[k]) {isFouund = true;}} assert(isFouund);} })
//! @return a local offset wr.t the valeus:
#define __assert__adjustInternalDisjointIndex__toLocalIndex(key) ({ \
  if(key >= globalStartPos_offset2) {key -= globalStartPos_offset2;} \
  else if(key >= globalStartPos_offset1) {key -= globalStartPos_offset1;} \
  key;}) //! ie, return the updated key:
//! Extend "__assert_listsAreEqualTo_clusterMemberships(..)" to handle seperate cases wrt. Boys and girls (or: rows and columns).
#define __assert_listsAreEqualTo_clusterMemberships__girlsBoys(arrOf_cmp, cnt_expected) ({ \
    assert(cnt_expected == arrOf_result_size); \
    assert(arrOf_result); \
    for(uint i = 0; i < cnt_expected; i++) { bool isFouund = false; \
    uint key = __assert__adjustInternalDisjointIndex__toLocalIndex(key) ;\
    for(uint k = 0; k < cnt_expected; k++) {if(key == arrOf_cmp[k]) {isFouund = true;}} assert(isFouund);} })
//! ***********************************************************************
//! ***********************************************************************

//! Test for: "nrows == ncols":
static void test_disjointCase_1() {
  const t_float default_value_float__ = 0;
  const uint nrows = 10;   const uint ncols = 10;  //! ie, an adjencency-matrix:
  t_float **matrix = allocate_2d_list_float(nrows, ncols, default_value_float__); 
  const t_float default_value_float = T_FLOAT_MAX; //! ie, 'state' tha we by defualt does not have any relatiosnhips.
  for(uint row_id = 0; row_id < nrows; row_id++) {for(uint col_id = 0; col_id < ncols; col_id++) {matrix[row_id][col_id] = default_value_float__;}}
  //! ---------------------------
  //! Add relationships for forest(1), ie, investigate wrt.: (a) transitvity, (b) 'forest-merge-points' and (c) 'self-cyclic relationships':
  __add_relation(matrix, /*head=*/0, /*tail=*/1);  __add_relation(matrix, /*head=*/1, /*tail=*/2); __add_relation(matrix, /*head=*/3, /*tail=*/2); __add_relation(matrix, /*head=*/2, /*tail=*/3);
  //! Add a new forst(2): relationships for forest(2): 'reciprocal iseland':
  __add_relation(matrix, /*head=*/4, /*tail=*/5);  __add_relation(matrix, /*head=*/5, /*tail=*/4); 
  //! Add a new forst(3): a chaned 'transitive' relationship: 
  __add_relation(matrix, /*head=*/7, /*tail=*/8);   __add_relation(matrix, /*head=*/8, /*tail=*/9); 
  //!
  //!
  //! ------------------ Test correctness: -----------------------------
  {
    //! -----------------------------
    //!
    //! Apply logics: infer the disjotin sets:
    assert(nrows == ncols); //! ie, what we expect in this etest.case
    s_kt_forest_findDisjoint obj_disjoint; allocate__denseMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix, /*mask=*/NULL, /*empty-value=*/default_value_float__);
    graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
    
    //! -----------------------------
    //!
    //! Validate the result:
#include "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"

    //! ----------------------------
    //!
    //! De-allocate the locally allcoated data:
    free_s_kt_forest_findDisjoint(&obj_disjoint);	  
  }
  { //! Test our "kt_disjoint__stub__applyLogics_insdieEach.c", using a 'dummy udpate-sequence':
    //! Frist set a tmp-matrix-element to 'saisfy' the sepectiosn in our "kt_disjoint__stub__applyLogics_insdieEach.c"
    s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
    matrix_tmp.matrix = matrix;  matrix_tmp.nrows = nrows;     matrix_tmp.ncols = ncols;
    s_kt_matrix *matrix_disjointInput = &matrix_tmp;   const uint *keyMap_localToGlobal = NULL; //! ie, as we 'process the compelte chunk of vertices in one step'.
    //! Terheafter 'cofngure' our test-appraoch to use our [above validate'd teest-code-chunk:
#define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case1.c"  //! ie, the same test-code-chunk as used in ª[bove]
  //! -----------------------------------------------------------------
  //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue 0 //! ie, to reflect [ªbove] assumption
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue

    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>
  }
  //! ----------------------------
  //!
  //! De-allocate matrix:
  free_2d_list_float(&matrix, nrows);
}

//!
//! Test for: "nrows != ncols":
static void test_disjointCase_2() {
  { //! Test for the case where: nrows << ncols:
    const t_float default_value_float__ = 0;
    const uint nrows = 10;   const uint ncols = 20;  //! ie, an adjencency-matrix:
    t_float **matrix = allocate_2d_list_float(nrows, ncols, default_value_float__); 
    const t_float default_value_float = T_FLOAT_MAX; //! ie, 'state' tha we by defualt does not have any relatiosnhips.
    for(uint row_id = 0; row_id < nrows; row_id++) {for(uint col_id = 0; col_id < ncols; col_id++) {matrix[row_id][col_id] = default_value_float__;}}
    //! ---------------------------
    //! Add relationships for forest(1), ie, investigate wrt.: (a) transitvity, (b) 'forest-merge-points' and (c) 'self-cyclic relationships':
    const uint m_forest1_row_size = 3; const uint m_forest1_row[m_forest1_row_size] = {1, 2, 3};
    const uint m_forest1_col_size = 4; const uint m_forest1_col[m_forest1_col_size] = {6, 7, 8, 9};
    __add_relation(matrix, 1, 6); 
    __add_relation(matrix, 1, 7);
    __add_relation(matrix, 2, 7);  __add_relation(matrix, 2, 8); 
    __add_relation(matrix, 3, 8); __add_relation(matrix, 3, 9);
    //! Add a new forst(2): relationships for forest(2): valdiate that we do not wrongly 'merge' with [ªbove] forest:
    const uint m_forest2_row_size = 2; const uint m_forest2_row[m_forest2_row_size] = {4, 5};
    const uint m_forest2_col_size = 2; const uint m_forest2_col[m_forest2_col_size] = {0, 1};
    __add_relation(matrix, 4, 0); __add_relation(matrix, 4, 1); __add_relation(matrix, 5, 1); //! ie, a 'simple fortk'.
      //! Add a new forst(3): a singular relation, where the 'tail' is at the 'boundary' of the column-diemsion:
    const uint m_forest3_row_size = 1; const uint m_forest3_row[m_forest3_row_size] = {8};
    const uint m_forest3_col_size = 1; const uint m_forest3_col[m_forest3_col_size] = {19};
    __add_relation(matrix, 8, 19); 

    
    //!
    //!
    //! ------------------ Test correctness: -----------------------------
    {
      //! -----------------------------
      //!
      //! Apply logics: infer the disjotin sets:
      assert(nrows != ncols); //! ie, what we expect in this test-case.
      s_kt_forest_findDisjoint obj_disjoint; 
      allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix, /*mask=*/NULL, /*empty-value=*/default_value_float__, /*isTo_init=*/true, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/true, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/(nrows+ncols));
      //allocate__denseMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix, /*mask=*/NULL, );
      graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
      //!
      //! Validate correctness: 
      #include "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case2.c"
      free_s_kt_forest_findDisjoint(&obj_disjoint);
    }      
    { //! Test our "kt_disjoint__stub__applyLogics_insdieEach.c", using a 'dummy udpate-sequence':
      //! Frist set a tmp-matrix-element to 'saisfy' the sepectiosn in our "kt_disjoint__stub__applyLogics_insdieEach.c"
      s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
      matrix_tmp.matrix = matrix;  matrix_tmp.nrows = nrows;     matrix_tmp.ncols = ncols;
      s_kt_matrix *matrix_disjointInput = &matrix_tmp;   const uint *keyMap_localToGlobal = NULL; //! ie, as we 'process the compelte chunk of vertices in one step'.
      //! Terheafter 'cofngure' our test-appraoch to use our [above validate'd teest-code-chunk:
#define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case2.c"  //! ie, the same test-code-chunk as used in ª[bove]
      //! -----------------------------------------------------------------
      //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue 0 //! ie, to reflect [ªbove] assumption
#define __disjointConfig__useComplexInference__rowsDiffersFrom 1
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
      //! -----------------------------
      //!
      //! De-allocate reserved memory:
      //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>
    }
  
    //! ----------------------------
    //!
    //! De-allocate matrix:
    free_2d_list_float(&matrix, nrows);
  }
}


//! Test for: "nrows_1 != nrows_2":
//! Note: idea is to make two seperate calls, where teh 'column-name-sapces' remains unchagned in the second call (whiel the row-name-sapces gets 'increased'/elevated
  //! Ntoe: when reading [”welow] exmaples remember the compelxity assicated to workign with three different name-sapces, ie, which disjtoin-case-3' describes.
static void test_disjointCase_3() {
  const t_float default_value_float__ = 0;
  const uint nrows_1 = 10; const uint nrows_2 = 13;   const uint ncols = 20;  //! ie, an adjencency-matrix:
  t_float **matrix   = allocate_2d_list_float(nrows_1, ncols, default_value_float__); 
  t_float **matrix_2 = allocate_2d_list_float(nrows_2, ncols, default_value_float__); 
  const t_float default_value_float = T_FLOAT_MAX; //! ie, 'state' tha we by defualt does not have any relatiosnhips.
  for(uint row_id = 0; row_id < nrows_1; row_id++) {for(uint col_id = 0; col_id < ncols; col_id++) {matrix[row_id][col_id] = default_value_float__;}}
  for(uint row_id = 0; row_id < nrows_2; row_id++) {for(uint col_id = 0; col_id < ncols; col_id++) {matrix_2[row_id][col_id] = default_value_float__;}}
  //! ---------------------------
  //! Add relationships for forest(1), ie, investigate wrt.: (a) transitvity, (b) 'forest-merge-points' and (c) 'self-cyclic relationships':
  const uint m_forest1_row_size_1 = 3; const uint m_forest1_row_1[m_forest1_row_size_1] = {1, 2, 3};
  const uint m_forest1_row_size_2 = 2; const uint m_forest1_row_2[m_forest1_row_size_2] = {5, 11};
  const uint m_forest1_col_size = 4; const uint m_forest1_col[m_forest1_col_size] = {6, 7, 8, 9};
  __add_relation(matrix, 1, 6);    __add_relation(matrix_2, 5, 6); 
  __add_relation(matrix, 1, 7);    __add_relation(matrix_2, 11, 9); 
  __add_relation(matrix, 2, 7); __add_relation(matrix, 2, 8); 
  __add_relation(matrix, 3, 8); __add_relation(matrix, 3, 9);
  //! ---------------------------
  //! Add relationships for forest(2): a minmal example where main-copmpelxity conserns the three differnet-name-spaces: if these name-spaces' are forogtten' then the result is errors in the result, i,e a case which we (through below relation-seritons) implcitly evaluate wrt. matrix_2.rows=[1, 2]
  const uint m_forest2_row_size_1 = 2; const uint m_forest2_row_1[m_forest2_row_size_1] = {4, 5};
  const uint m_forest2_row_size_2 = 1; const uint m_forest2_row_2[m_forest2_row_size_2] = {1};
  const uint m_forest2_col_size = 2; const uint m_forest2_col[m_forest1_col_size] = {0, 1};
  __add_relation(matrix, 4, 0); __add_relation(matrix_2, 1, 0); __add_relation(matrix_2, 1, 1); __add_relation(matrix, 5, 1); //! ie, a 'simple fortk', where "matrix_2" 'serves as the fork'.
  //! Add a new forst(3): a singular relation, where the 'tail' is at the 'boundary' of the column-diemsion:
  const uint m_forest3_row_size_1 = 1; const uint m_forest3_row_1[m_forest3_row_size_1] = {8};
  const uint m_forest3_row_size_2 = 1; const uint m_forest3_row_2[m_forest3_row_size_2] = {8};
  const uint m_forest3_col_size = 1; const uint m_forest3_col[m_forest1_col_size] = {19};
  __add_relation(matrix, 8, 19); __add_relation(matrix_2, 8, 19); 

    //!
    //!
    //! ------------------ Test correctness: -----------------------------
  {
    //! -----------------------------
    //!
    //! Apply logics: infer the disjotin sets:
    assert(nrows_1 != ncols); //! ie, what we expect in this test-case.
    assert(nrows_1 != nrows_2); //! ie, what we expect in this test-case.
    s_kt_forest_findDisjoint obj_disjoint; 
    allocateAndApply__denseMatrix___differentNameSpaceInRowsAndCols__twoInputMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows_1, nrows_2, ncols, matrix, matrix_2, default_value_float__);
    //!
    //! Test the correctness:
#include "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case3.c"
    free_s_kt_forest_findDisjoint(&obj_disjoint);
  }


  { //! Test our "kt_disjoint__stub__applyLogics_insdieEach.c", using a 'dummy udpate-sequence':
    //! Frist set a tmp-matrix-element to 'saisfy' the sepectiosn in our "kt_disjoint__stub__applyLogics_insdieEach.c"
    s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
    assert(matrix);
    matrix_tmp.matrix = matrix;  matrix_tmp.nrows = nrows_1;         matrix_tmp.ncols = ncols;
    //! ---
    s_kt_matrix_t matrix_tmp_2; setTo_empty__s_kt_matrix_t(&matrix_tmp_2);
    assert(matrix_2);
    matrix_tmp_2.matrix = matrix_2;  matrix_tmp_2.nrows = nrows_2;     matrix_tmp_2.ncols = ncols;
    //! ---
    s_kt_matrix *matrix_disjointInput   = &matrix_tmp;   const uint *keyMap_localToGlobal = NULL; //! ie, as we 'process the compelte chunk of vertices in one step'.
    assert(matrix_disjointInput->matrix);
    s_kt_matrix *matrix_disjointInput_2 = &matrix_tmp_2;
    assert(matrix_disjointInput_2->matrix);
    //! Terheafter 'cofngure' our test-appraoch to use our [above validate'd teest-code-chunk:
#define __localConfig__applyLogics_pathTo_codeChunk "measure_kt_forest_findDisjoint__stub__validateTestResultFor_case3.c"  //! ie, the same test-code-chunk as used in ª[bove]
    //! -----------------------------------------------------------------
    //! Apply the disjoitn-forest-comptuatiosn:
#define __localConfig__specificEmptyValue 0 //! ie, to reflect [ªbove] assumption
#define __disjointConfig__useComplexInference__rowsDiffersFrom 2
  uint *mapOf_clusterVertices__toClusterIds = NULL;
#include "kt_disjoint__stub__applyLogics_insdieEach.c"
#undef __localConfig__applyLogics_pathTo_codeChunk
#undef __localConfig__specificEmptyValue
#undef __disjointConfig__useComplexInference__rowsDiffersFrom
    //! -----------------------------
    //!
    //! De-allocate reserved memory:
    //! Note: <no need to de-allcoate, ie, as the ª[bove] cinldeud code-chnhk si expected 'fixing the issue'>
  }
  //free_s_kt_forest_findDisjoint(&obj_disjoint);

  //! ----------------------------
  //!
  //! De-allocate matrix:
  free_2d_list_float(&matrix, nrows_1);
  free_2d_list_float(&matrix_2, nrows_2);
}

//! Test for: 
static void test_disjointCase_4() {

  assert(false); // FIXME: consider 'dropping this' .... ie, using the logics in [ªbove] "test_disjointCase_3()"

}

//! The main assert function.
//! @remarks we evalaute the performance-capbilitaies and diffneret disjtoint-forest-comtpaution-permtuations in our "measure_kt_matrix_filter.c" (oekseth, 06. otk. 2016).
void measure_kt_forest_findDisjoint__main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  if(isTo_processAll || (0 == strncmp("test_correctness", array[2], strlen("test_correctness"))) ) {
    test_correctness();
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("extensive_compare_for_differentMaskDegrees", array[2], strlen("extensive_compare_for_differentMaskDegrees"))) ) {
    extensive_compare_for_differentMaskDegrees();
    cnt_Tests_evalauted++;
  }
  // --------------------------------------------------
  if(isTo_processAll || (0 == strncmp("disjointCase-1", array[2], strlen("disjointCase-1"))) ) {
    test_disjointCase_1(); //! case: "nrows == ncols"
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("disjointCase-2", array[2], strlen("disjointCase-2"))) ) {
    test_disjointCase_2(); //! case: "nrows != ncols"
    cnt_Tests_evalauted++;
  }
  if(isTo_processAll || (0 == strncmp("disjointCase-3", array[2], strlen("disjointCase-3"))) ) {
    test_disjointCase_3(); //! case: "nrows_1 != nrows_2", ie, where two different matrices are used 'as input', where the latter is a subset of the mroe 'general' speciicioatn: "nrows_1 != nrows_2 && (nrows_1 != ncols || nrows_2 != ncols)".
    cnt_Tests_evalauted++;
  }




  assert(false); // FIXME: figure out why 'too many disjtoint-forests are identifed'.

  assert(false); // ok: FIXME: udpate our "clusterC_cmp.html" wrt. our udpated disjtoin-amtrix-procedure
  //! -------------------
  assert(false); // FIXME: take the "kt_matrix_filter.c" object as inptu ... write an algrotihnm-implemtnation wehre we ... use a pre-test to investigate if all vertices are related to a set of features ... for which we ....??...
  //! -------------------




  assert(false); //  FIXME: wrt. our "measure_kt_forest_findDisjoint.c" ... compelte writing of ... a simple perfomranc-etes wehre we evlauate/test ... exeuction-time of differnet 'sarpseness-degrees' (when comared to a naive 3d-matrix-summation)
  assert(false); // FIXME: wrt. our "measure_kt_forest_findDisjoint.c" ... compelte writing of ... application-test-case ... number of clusters when data si generated from different probaiblity-funciton-generators ... and for differnet min-max-filter-criterias ... ie, ineidates teh 'optmziaotn-factor which may be gained from applicaiotn of dsijoitn-mask-filters in correlation-analsysis'.
  //! -------------------
  assert(false); // FIXME: correctnness-test: "__init_structure_wrt_previousElements(...)" .... for different combiantiosn/use-cases ... where correctness is evalated throgu/using/by  ....??....
  assert(false); // FIXME: wrt. the mappigns-cheme-compelxities ... write-asserts-for ... [ get_foreestId_forVertex__s_kt_forest_findDisjoint(..) ... get_foreestId_forVertex__atDataSetInsertionId__s_kt_forest_findDisjoint(..) cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(...) .... get_datasetIdOf_vertex__s_kt_forest_findDisjoint_t(...) ]







  //! -------------------
  assert(false); // FIXME: write an algrotihnm-implemtnation wehre we ... as an optmizaiton-step .... before comptaution ... count the number features (in the set) and thereafter first investigate for the features 'with the hgihest number of relationships' (an appraoch which is hoped to reduce the time-cost assicated to the marging of entites).

  assert(false); // FIXME: wrt. our "measure_kt_forest_findDisjoint.c" ... compelte writing of ... a simple perfomranc-etes wehre we evlauate/test ... for [ªbove] 'permtuations' of distance-metric-inference


  assert(false); // ok: FIXME: wrt. the 'ocneptual implemetnaito' .... describe why there is 'no need' for a sorting-step during the data-margin ... ie, in contrast to our C++-implemetnation .... we assume that "each vertex is repsonisble for its own cosnistency" (ie, a vertex will only be member of one set of entites). From the latter we infer two sets will be unique, ie, for which we do Not need to remove overlapping vertex enittes/sets: the latter expalisn why sorting (during the emrging-process) is potinless, ie, for whcih we infeer that our C++-implmeentaiotn has a locial/un-neccesary overhead.

  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}


#undef __inLove
#undef __atWar
#undef __inLove__girlsBoys
#undef __atWar__girlsBoys
#undef __assert_listsAreEqualTo_clusterMemberships
#undef __assert__adjustInternalDisjointIndex__toLocalIndex
#undef __assert_listsAreEqualTo_clusterMemberships__girlsBoys
