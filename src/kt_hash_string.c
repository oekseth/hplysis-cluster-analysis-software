#include "kt_hash_string.h"
#include <inttypes.h> //! where latter is 'needed' for the "uint_32_t" data-type.
//#include <cstdint>

//! Src: "https://en.wikipedia.org/wiki/Jenkins_hash_function"
static uint32_t __getHash__jenkins_one_at_a_time_hash(const uint8_t* key, size_t length) {
  size_t i = 0;
  uint32_t hash = 0;
  while (i != length) {
    hash += key[i++];
    hash += hash << 10;
    hash ^= hash >> 6;
  }
  hash += hash << 3;
  hash ^= hash >> 11;
  hash += hash << 15;
  return hash;
}

static uint alt_hashFunciton__s_kt_hash_string_t(const char *str) {
  const uint str_size = (uint)strlen(str);   assert(str_size > 0); 
  /* // Use folding on a string, summed 4 bytes at a time */
  /* long sfold(String s, int M) { */
  /*   int intLength = s.length() / 4; */
  uint sum = 0;
  uint str_size_part = (uint)((t_float)str_size * 0.25);
  if(str_size_part == 0) {str_size_part = 1;}
  uint index_end = 0;
  for (uint j = 0; j < str_size_part; j++) {
    const uint index_start = j*4;
    index_end = index_start + 4;
    if(index_end > str_size) {index_end = str_size;}
    long mult_prev = 1;
    for (int k = index_start; k < index_end; k++) {
      sum += str[k] * mult_prev;
      mult_prev *= 256;
    }
  }
  if(index_end < str_size) {
    long mult_prev = 1;
    for (int k = index_end; k < str_size; k++) {
      sum += str[k] * mult_prev;
      mult_prev *= 256;
    }
  }
  const uint hash_index = mathLib_float_abs(sum);
  // TODO: cosnider using a different cotnant than [”elow]
  const uint max_index = 1000*1000;
  return hash_index % max_index;
  //  return(Math.abs(sum) % M);
}

static uint __getHash__jenkins_one_at_a_time_hash_forString(const s_kt_hash_string_t *self, const char *str) {
  assert(str);
#if(1 == 1)
  return alt_hashFunciton__s_kt_hash_string_t(str);
#else 
  assert(sizeof(uint8_t) >= sizeof(char)); //! ie, what is expected in [below] converison. 
  const uint str_size = (uint)strlen(str);   assert(str_size > 0); 
  const uint8_t *key_str = (const uint8_t *)str;         /* read 32-bit chunks */
  const uint32_t hash_index = __getHash__jenkins_one_at_a_time_hash(key_str, str_size);
  assert(hash_index < (uint32_t)UINT_MAX); //! ie, as we otehrwise need/require a differnet data-type for our list-index-storage.
  uint hash_index_loc = (uint)hash_index;
  return hash_index_loc;
#endif
}

//! @return the key of the string: if Not fodun hten UINT_MAX is returned.
uint get_key__kt_hash_string(const s_kt_hash_string_t *self, const char *str) {
/*   assert(str); assert(strlen(str)); */
/*   return __add_string__kt_hash_string(self, str, /\*isTo_insertIfNotFound=*\/false); */
/* } */
/* //! @return the index at where the string was inserted. */
/* static uint __add_string__kt_hash_string(s_kt_hash_string_t *self, const char *str, const bool isTo_insertIfNotFound) { */
  assert(str); assert(strlen(str));
  const uint hash_index = __getHash__jenkins_one_at_a_time_hash_forString(self, str);
  //! 
  //! Search through keys with same value:
  uint internal_indexKey = UINT_MAX;
  if(hash_index < self->map_collisions.list_size) { //! then the key may be generated at at earlier time-poin:
    //! Iterate through set: 
    //! 
    const uint list_members_size = self->map_collisions.list[hash_index].current_pos;
    if(list_members_size > 0) { //! then we search through the vertex
      const uint *list_members = self->map_collisions.list[hash_index].list;
      assert(list_members);
      //! 
      //! Test if there are any keys which mathces the searvhed-for string-index: 
      for(uint i = 0; i < list_members_size; i++) {
	if(internal_indexKey == UINT_MAX) {
	  const uint key = list_members[i];
	  assert(key < self->obj_strings_cntInserted); //! ie, what we expect for cosnsitnecy.
	  const char *str_cmp = self->obj_strings.nameOf_rows[key];
	  assert(str_cmp); assert(strlen(str_cmp));
	  if(
	     (strlen(str_cmp) == strlen(str)) &&
	     (0 == strcmp(str_cmp, str)) ) {
	    //! Then we ahve foudn the assited key: 
	    internal_indexKey = key;
	  }
	}
      }
    }
  }
  return internal_indexKey;
}
//! @return the index at where the string was inserted.
uint add_string__kt_hash_string(s_kt_hash_string_t *self, const char *str) {
  uint internal_indexKey = get_key__kt_hash_string(self, str);
  const bool isTo_insertIfNotFound = true;
  //! Handle case where string is uqnieu: 
  if(isTo_insertIfNotFound && (internal_indexKey == UINT_MAX) ) { //! then add the string:
    const uint hash_index = __getHash__jenkins_one_at_a_time_hash_forString(self, str);
    assert(hash_index < UINT_MAX);
    internal_indexKey = self->obj_strings_cntInserted; (self->obj_strings_cntInserted)++;
    set_stringConst__s_kt_list_1d_string(&(self->obj_strings), /*index=*/internal_indexKey, str);
    //! 
    //! Update the result-container: 
    const uint currPos_prev = (hash_index < self->map_collisions.list_size) ? self->map_collisions.list[hash_index].current_pos : 0;
    // printf("hash[%u] .= {%u:\"%s\"}, at %s:%d\n", hash_index, internal_indexKey, str, __FILE__, __LINE__);
    push__s_kt_list_2d_uint_t(&(self->map_collisions), hash_index, /*value=*/internal_indexKey);
    const uint currPos_after = self->map_collisions.list[hash_index].current_pos;
    assert( (currPos_prev + 1) == currPos_after);
  }
  //!
  //! @return the internal uqniue index of the string: 
  if(isTo_insertIfNotFound) {
    assert(internal_indexKey != UINT_MAX);
  }
  //! -------------------------------------------------------------------------------
  //! 
  //! 
  //  return __add_string__kt_hash_string(self, str, /*isTo_insertIfNotFound=*/true);
  return internal_indexKey;
}

s_kt_hash_string_t init__fromHash__s_kt_hash_string_t(s_kt_list_1d_string_t *obj_words, char word_seperator) {
  s_kt_hash_string_t self = init__s_kt_hash_string_t();
  assert(obj_words);
  if(obj_words && obj_words->nrows) {
    if(word_seperator != '\0') {
      for(uint key = 0; key < obj_words->nrows; key++) {
	const char *str_cmp = obj_words->nameOf_rows[key];;
	if(!str_cmp) {continue;} //! ie, as the string was Not set.
	assert(str_cmp); assert(strlen(str_cmp));
	const char *str_curr = str_cmp;
	assert(str_curr);
	const char *str_cmp_end = str_cmp + strlen(str_cmp);
	//!
	//!
	do {
	  assert(str_curr);
	  const char *str_curr_next = strchr(str_curr, word_seperator);
	  //	  if(str_curr_next != NULL); //! ie, where latter assumption is based on the Linux-man-page.
	  if(str_curr_next && ((*str_curr_next == word_seperator) ) ) {
	    //! 
	    //! Add the string:
	    const uint cnt_chars = str_curr_next - str_curr;
	    if(cnt_chars > 0) {
	      //assert(cnt_chars > 0);
	      assert(str_curr[cnt_chars] == word_seperator);
	      //! Allocate:
	      const char empty_0 = 0;
	      char *tmp_str = allocate_1d_list_char((cnt_chars+1), empty_0);
	      memcpy(tmp_str, str_curr, sizeof(char)*cnt_chars);
	      //! Insert: 	    
	      //	    printf("(add-word)\t %s\t\t #\t give sep='%c', at %s:%d\n", tmp_str, word_seperator, __FILE__, __LINE__);
	      assert(strlen(tmp_str) == cnt_chars);
	      add_string__kt_hash_string(&self, tmp_str);
	      //! De-allocate:
	      free_1d_list_char(&tmp_str);
	    }
	    //! Increment for 'next-run':
	    str_curr = str_curr_next + 1; //! ie, jump 'above/past' the sperator.
	  } else { //! then the string was Not found.
	    assert(str_curr);
	    assert(strlen(str_curr));
	    assert(str_curr < str_cmp_end);
	    // printf("(add-word)\t %s\t\t #\t at %s:%d\n", str_curr, __FILE__, __LINE__);
	    add_string__kt_hash_string(&self, str_curr);
	    str_curr = str_cmp_end;
	  }
	} while(str_curr < str_cmp_end);
      }
    } else { //! then we isnert seperately for each string:
      for(uint key = 0; key < obj_words->nrows; key++) {
	const char *str_cmp = obj_words->nameOf_rows[key];;
	assert(str_cmp); assert(strlen(str_cmp));
	add_string__kt_hash_string(&self, str_cmp);
      }
    }
  } 
  //! 
  //! @return
  return self;
}

//! Dump the key-name mappoigns to the file-pointer in question. 
void dump__s_kt_hash_string_t(const s_kt_hash_string_t *self, FILE *fPointer) {
  assert(self);
  if(fPointer == NULL) {fPointer = stdout;}
  fprintf(fPointer, "#! Dumps result: [hash-key, internal-key, string], at %s:%d\n", __FILE__, __LINE__);
  for(uint key = 0; key < self->obj_strings_cntInserted; key++) {
    const char *str_cmp = self->obj_strings.nameOf_rows[key];;
    assert(str_cmp); assert(strlen(str_cmp));
    fprintf(fPointer, "%u\t%u\t%s\n", __getHash__jenkins_one_at_a_time_hash_forString(self, str_cmp), key, str_cmp);
  }
}

//! Dump the key-name mappoigns to the file-pointer in question. 
void dump__sortByBucket__s_kt_hash_string_t(const s_kt_hash_string_t *self, FILE *fPointer) {
  assert(self);
  if(fPointer == NULL) {fPointer = stdout;}
  fprintf(fPointer, "#! Dumps result: [hash-key, internal-key, string], at %s:%d\n", __FILE__, __LINE__);
  for(uint hash_index = 0; hash_index < self->map_collisions.list_size; hash_index++) {
    const uint list_members_size = self->map_collisions.list[hash_index].current_pos;
    if(list_members_size > 0) { //! then we search through the vertex
      const uint *list_members = self->map_collisions.list[hash_index].list;
      assert(list_members);
      fprintf(fPointer, "#[%u]\t", hash_index);
      //! 
      //! Iterat ethrough the bucke-members:
      for(uint i = 0; i < list_members_size; i++) {
	const uint key = list_members[i];
	assert(key < self->obj_strings_cntInserted); //! ie, what we expect for cosnsitnecy.
	const char *str_cmp = self->obj_strings.nameOf_rows[key];
	assert(str_cmp); assert(strlen(str_cmp));
	fprintf(fPointer, "%s\t", str_cmp);
      }
      fprintf(fPointer, "\n");
    }
  }
}





/**
   @brief cosntruct a unqiue set/colleciton of strings (oekseth, 06. agu. 2017).
   @param <obj_words> is the collection of words to idneity unqiueness of, ie, to insert into the returned object.
   @param <word_seperator> seperates the words in each sentence: if '\0' is sued then we use the 'compelte stinrgs'.
   @return an itnalised verison of the hash-object
**/
s_kt_hash_string_2dSparse_t init__fromHash__s_kt_hash_string_2dSparse_t(s_kt_list_1d_string_t *obj_words, char word_seperator_keyValues, char word_seperator_valueValues, const bool isTo_useDifferentMappingTables_forKeysIndexes) {
  //  printf("sep='%u' = '%c', at %s:%d\n", word_seperator_keyValues, word_seperator_keyValues, __FILE__, __LINE__);
  s_kt_hash_string_2dSparse_t self = setTo_empty__s_kt_hash_string_2dSparse_t();
  self.isTo_useDifferentMappingTables_forKeysIndexes = isTo_useDifferentMappingTables_forKeysIndexes;
  assert(word_seperator_keyValues != '\0');
  assert(obj_words);
  if(obj_words && obj_words->nrows) {
    /* for(uint key = 0; key < obj_words->nrows; key++) { */
    
    /* } */
    //      printf("sep='%u' = '%c', at %s:%d\n", word_seperator_keyValues, word_seperator_keyValues, __FILE__, __LINE__);
    for(uint key = 0; key < obj_words->nrows; key++) {
      //printf("sep='%u' = '%c', at %s:%d\n", word_seperator_keyValues, word_seperator_keyValues, __FILE__, __LINE__);
      const char *str_line = obj_words->nameOf_rows[key];;
      if(str_line == NULL) {continue;}
      assert(str_line); assert(strlen(str_line));
      //  printf("\t(eval-line-sep)\t \"%s\", sep='%c', at %s:%d\n", str_line,  word_seperator_keyValues, __FILE__, __LINE__);
      const char *str_curr_next = strchr(str_line, word_seperator_keyValues);
      //	  if(str_curr_next != NULL); //! ie, where latter assumption is based on the Linux-man-page.
      if(str_curr_next && ((*str_curr_next == word_seperator_keyValues) ) ) {
	//	printf("\t\t(eval-line)\t \"%s\", at %s:%d\n", str_curr,  __FILE__, __LINE__);
	//! 
	//! Add the string:
	const uint cnt_chars = str_curr_next - str_line;
	if(cnt_chars > 0) {
	  //! Allocate:
	  const char empty_0 = 0;
	  char *tmp_str = allocate_1d_list_char((cnt_chars+1), empty_0);
	  memcpy(tmp_str, str_line, sizeof(char)*cnt_chars);
	  assert(strlen(tmp_str) == cnt_chars);
	  assert(strlen(tmp_str));
	  const char *str_chomp = MF__kt_string__Chomp(tmp_str); //! ie, remove 'before-white-space' and 'trailing-white-spaces'.
	  if(strlen(str_chomp) == 0) {
	    //! De-allocate:
	    free_1d_list_char(&tmp_str);
	    continue;
	  }
	  //! Insert: 	    
	  //printf("(add-word)\t %s\t\t #\t give sep='%c', at %s:%d\n", tmp_str, word_seperator, __FILE__, __LINE__);
	  const uint head_id = add_string__kt_hash_string(&(self.obj_dense), str_chomp);
	  assert(head_id != UINT_MAX);
	  //! De-allocate:
	  free_1d_list_char(&tmp_str);
	  //!
	  //! 
	  str_curr_next++; //! ie, move 'past' the seperator
	  if(word_seperator_valueValues != '\0') {
	    const char *str_curr = str_curr_next;
	    assert(str_curr);
	    const char *str_cmp_end = str_curr + strlen(str_curr);
	    //!
	    //!
	    do {
	      assert(str_curr);
	      const char word_seperator = word_seperator_valueValues;
	      const char *str_curr_next = strchr(str_curr, word_seperator);
	      //	  if(str_curr_next != NULL); //! ie, where latter assumption is based on the Linux-man-page.
	      if(str_curr_next && ((*str_curr_next == word_seperator) ) ) {
		//! 
		//! Add the string:
		const uint cnt_chars = str_curr_next - str_curr;
		if(cnt_chars > 0) {
		  //assert(cnt_chars > 0);
		  assert(str_curr[cnt_chars] == word_seperator);
		  //! Allocate:
		  const char empty_0 = 0;
		  char *tmp_str = allocate_1d_list_char((cnt_chars+1), empty_0);
		  memcpy(tmp_str, str_curr, sizeof(char)*cnt_chars);
		  const char *str_chomp_2 = MF__kt_string__Chomp(tmp_str); //! ie, remove 'before-white-space' and 'trailing-white-spaces'.
		  if(strlen(str_chomp_2) != 0) {
		    //! Insert: 	    
		    //	    printf("(add-word)\t %s\t\t #\t give sep='%c', at %s:%d\n", tmp_str, word_seperator, __FILE__, __LINE__);
		    assert(strlen(tmp_str) == cnt_chars);
		    uint tail_id = UINT_MAX;
		    //		const char *tmp_str = str_curr;
		    assert(strlen(tmp_str));
		    if(isTo_useDifferentMappingTables_forKeysIndexes) {
		      tail_id = add_string__kt_hash_string(&(self.obj_sparse), str_chomp_2);
		    } else {
		      tail_id = add_string__kt_hash_string(&(self.obj_dense), str_chomp_2);
		    }
		    assert(tail_id != UINT_MAX); 
		    //!
		    //! Add the tail-rleation:
		    //		    printf("\t\t(add-string-pair)\t (%u, %u)=(\"%s\", \"%s\"), at %s:%d\n", head_id, tail_id, self.obj_dense.obj_strings.nameOf_rows[head_id], str_chomp_2, __FILE__, __LINE__);
		    push__s_kt_list_2d_uint_t(&(self.obj_2d), head_id, tail_id);
		  }
		  //add_string__kt_hash_string(&self, tmp_str);
		  //! De-allocate:
		  assert(tmp_str);
		  free_1d_list_char(&tmp_str); tmp_str = NULL;
		}
		//! Increment for 'next-run':
		str_curr = str_curr_next + 1; //! ie, jump 'above/past' the sperator.
	      } else { //! then the string was Not found.
		assert(str_curr);
		assert(strlen(str_curr));
		assert(str_curr < str_cmp_end);
		// printf("(add-word)\t %s\t\t #\t at %s:%d\n", str_curr, __FILE__, __LINE__);
		uint tail_id = UINT_MAX;
		const char empty_0 = 0;
		const uint cnt_chars = strlen(str_curr);
		char *tmp_str = allocate_1d_list_char((cnt_chars+1), empty_0);
		memcpy(tmp_str, str_curr, sizeof(char)*cnt_chars);
		const char *str_chomp_2 = MF__kt_string__Chomp(tmp_str); //! ie, remove 'before-white-space' and 'trailing-white-spaces'.
		if(strlen(str_chomp_2) != 0) {
		//MF__kt_string__Chomp(tmp_str); //! ie, remove 'before-white-space' and 'trailing-white-spaces'.
		//const char *tmp_str = str_curr;
		  assert(strlen(tmp_str));
		  if(isTo_useDifferentMappingTables_forKeysIndexes) {
		    tail_id = add_string__kt_hash_string(&(self.obj_sparse), str_chomp_2);
		  } else {
		    tail_id = add_string__kt_hash_string(&(self.obj_dense), str_chomp_2);
		  }
		  assert(tail_id != UINT_MAX); 
		  //!
		  //! Add the tail-rleation:
		  //		  printf("\t\t(add-string-pair)\t (%u, %u), at %s:%d\n", head_id, tail_id, __FILE__, __LINE__);
		  push__s_kt_list_2d_uint_t(&(self.obj_2d), head_id, tail_id);
		}
		// add_string__kt_hash_string(&self, str_curr);
		str_curr = str_cmp_end;
		//! De-allocate: 
		free_1d_list_char(&tmp_str); tmp_str = NULL;
	      }
	    } while(str_curr < str_cmp_end);
	  } else { //! then we splti the strings into two parts:
	    assert(str_curr_next); 
	    if(strlen(str_curr_next) == 0) {
	      fprintf(stderr, "(empty-rows-for-seperator): \"%s\", at %s:%d\n", str_line, __FILE__, __LINE__);
	    } else {
	      assert(strlen(str_curr_next));
	      uint tail_id = UINT_MAX;
	      /* const char *tmp_str = str_curr_next; assert(strlen(tmp_str)); */
	      /* assert(strlen(tmp_str)); */
	      //! Allocate:
	      const char empty_0 = 0;
	      uint cnt_chars = strlen(str_curr_next);
	      //	    if(cnt_chars == 0) { str_curr_next = str_l; cnt_chars = strlen(str_curr_next);}
	      if(cnt_chars > 0) {
		char *tmp_str = allocate_1d_list_char((cnt_chars+1), empty_0);
		assert(cnt_chars > 0);
		tmp_str[cnt_chars] = '\0';
		memcpy(tmp_str, str_line, sizeof(char)*cnt_chars);
		const char *str_chomp_2 = MF__kt_string__Chomp(tmp_str); //! ie, remove 'before-white-space' and 'trailing-white-spaces'.
		//	      printf("%s\n", str_chomp_2);
		if(strlen(str_chomp_2) != 0) {
		  //	    MF__kt_string__Chomp(tmp_str); //! ie, remove 'before-white-space' and 'trailing-white-spaces'.
		  if(isTo_useDifferentMappingTables_forKeysIndexes) {
		    tail_id = add_string__kt_hash_string(&(self.obj_sparse), str_chomp_2);
		  } else {
		    tail_id = add_string__kt_hash_string(&(self.obj_dense), str_chomp_2);
		  }
		  assert(tail_id != UINT_MAX); 
		  //!
		  //! Add the tail-rleation:
		  //	      printf("\t\t(add-string-pair)\t (%u, %u), at %s:%d\n", head_id, tail_id, __FILE__, __LINE__);
		  push__s_kt_list_2d_uint_t(&(self.obj_2d), head_id, tail_id);
		}
		//! De-allocate: 
		free_1d_list_char(&tmp_str); tmp_str = NULL;
	      }
	    }
	  }	
	}	
	//	add_string__kt_hash_string(&self, str_cmp);
      } else {
	fprintf(stderr, "!!(ingore-term)\t \"%s\", given search-sep='%c', and 'first-tab'='%s', at %s:%d\n", str_line, word_seperator_keyValues, strchr(str_line, '\t'),  __FILE__, __LINE__);
      }
    }
  } 
  return self;
}
