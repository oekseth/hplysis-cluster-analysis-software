uint cnt_toEvaluate = self->nrows;
if(config.isTo_use_KD_treeInSimMetricComputations) {
  cnt_toEvaluate = macro_min(objAdj_2d_fromKD.list_size, cnt_toEvaluate);
 }
//!
//! Iterate
if(config.isTo_use_KD_treeInSimMetricComputations) {
  printf("us-kd-tree, at %s:%d\n", __FILE__, __LINE__);
 }
for(uint row_id = 0; row_id < cnt_toEvaluate; row_id++) {
  const uint clus_in  = map_vertexToCluster[row_id]; 
  assert(matrix[row_id]);
  t_float localSum__inSame = 0;       t_float localSum__differentClusters = 0;
  
  uint cnt_vertices_internal = 0;
  if(config.isTo_use_KD_treeInSimMetricComputations) {
    //printf("use-kd, at %s:%d\n", __FILE__, __LINE__);
    //! Then idneitfy the siamrlity through iteration/travesal of KD-tree and rank-seleciton.
    assert(row_id < objAdj_2d_fromKD.list_size);
    uint cnt_tails = objAdj_2d_fromKD.list[row_id].current_pos;
    assert(cnt_tails <= objAdj_2d_fromKD.list[row_id].list_size); //! ie, as we otehriwse have an in-conssitnecy.
    if(cnt_tails > 0) {
      printf("#(%u)=%u, at %s:%d\n", row_id, cnt_tails, __FILE__, __LINE__);
      for(uint k = 0; k < cnt_tails; k++) {
	const s_ktType_kvPair_t obj_this = objAdj_2d_fromKD.list[row_id].list[k];
	if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	  const uint tail_id = obj_this.head;
	  assert(tail_id != UINT_MAX); 
	  assert(tail_id < self->nrows);
	  const t_float score = obj_this.tail;
	  printf("%u\t%u\t%f \t #! %s:%d\n", row_id, tail_id, score, __FILE__, __LINE__);
	  const uint clus_out = map_vertexToCluster[tail_id];
	  if( (clus_out >= self->cnt_cluster) || (clus_in >= self->cnt_cluster)) {continue;} //! ie, as we then asusme that no well-defiend clsuters were foudn for these two vertices.
	  //!
	  //! Update the scorer:
	  __MiFd__score; //! eg,: self->vertexTo_clusters[row_id][clus_out] += score;
	  //!
	  //! Update the cluster--cluster-memberships:
#ifdef __MiFd__score_updateResultContainer 
	  __MiFd__score_updateResultContainer; //! eg,: self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++;
#endif
	}
      }
    }
  } else {
    for(uint col_id = 0; col_id < self->nrows; col_id++) {
      t_float score = T_FLOAT_MAX;
      if(config.metric_complexClusterCmp_vertexCmp_isTo_use == true) { //! then we apply a 'complex comparison-strategy':
	{
	  assert(false); // FIXME: validate [”elow]:
	  score = kt_compare__each__maskImplicit(config.metric_complexClusterCmp_obj.metric_id, config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, self->nrows, self->nrows, matrix, matrix, row_id, col_id);
	}
	// printf("score[%u][%u]=%f, at %s:%d\n", row_id, col_id, score, __FILE__, __LINE__);
      } else {score = matrix[row_id][col_id];}
      if(score != T_FLOAT_MAX) {
	// printf("score[%u][%u]=%f, at %s:%d\n", row_id, col_id, score, __FILE__, __LINE__);
	cnt_vertices_internal++;	  
	const uint clus_out = map_vertexToCluster[col_id];
	if( (clus_out >= self->cnt_cluster) || (clus_in >= self->cnt_cluster)) {continue;} //! ie, as we then asusme that no well-defiend clsuters were foudn for these two vertices.
	//!
	//! Update the scorer:
	__MiFd__score;	    	      
	//!
	//! Update the cluster--cluster-memberships:
#ifdef __MiFd__score_updateResultContainer 
	__MiFd__score_updateResultContainer;
#endif
      }
    }
  }	    
 }

#undef __MiFd__score_updateResultContainer
