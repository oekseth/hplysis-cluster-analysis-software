//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hpLysis_api.h"
#include "tut_wrapper_image_segment.h"
#include "hp_image_segment.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "export_ppm.h"
#include <climits>
#include <cfloat>
//#endif
//#include "hp_distance.h"
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
//#include "aux_sysCalls.h"
//#include "kt_aux_matrix_norm.h" //! which hold the data-nraomizoant-enums.
//#include "hp_image_segment.h"

/**
   FIXME:
   .): write a main-wrapper
   .): "hp_image_segment": test: pasring: txt-file
   .): "hp_image_segment": test: ppm-file
   .): "hp_image_segment": test: bmp-file-parsing 
   .): "hp_image_segment": test: 
   .): "hp_image_segment": test: 
   .): "hp_image_segment": test: 
   .): 
   .): 
   .): image-back-forth: blur an image
   .): image-back-forth: normalise an image
   .): image-back-forth: infer the hue of an image
   .): image-back-forth: 
   .): image-back-forth: 
   // FIXME: move below to a new tut: 
   // FIXME: move below to a new tut: 
   // FIXME: move below to a new tut: "tut_image_4_normalize.c"
   // FIXME: move below to a new tut: "tut_image__.c"
   // FIXME: move below to a new tut: "tut_image_1_helloWorld.c" Foxued on semgntaiton. Turns a matrix inoit a collecion of triplets, and then apply a clustering-algorithm, before a new iamge is contstructed based on the average score.
   .): image-back-forth: clustering: k-means: average RGB-values
   .): image-back-forth: clustering: k-means: 
   .): image-back-forth: clustering: k-means: 
   .): image-back-forth: clustering: 
   .): image-back-forth: 
   .): 
   .): 
   .): 
 **/

/**
   @brief explroes a large number of differnt blurring-algorihms.
   @author Ole Kristian Ekseth (oekseth, 06. nov. 2020).
   @remarks
   --- exemplifes strategies for image-parsing, and simplfied lgocis to validat coissiten in export-lgocis with import-logics    
   @remarks
   -- compile: g++ -I.  -g -O0  -mssse3   -L .  tut_image_2_exploreImpactOfConfigurations_blur.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_img; ./tut_img
**/
int main() { // const int array_cnt, char **array) {
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif
  const char *result_file = "tmp-1.ppm";
  //!
  { //!
    //! Step: Construct a sytentic matrix:
    //! Note: we here construct a clsuter-shape. This to provide the stepping-stoens for future, and mroe compelx, examples
    const uint nrows = 100;
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/1000, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/0);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/20);    
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/40,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/60);    
    { //! Step: Export to PPM:
      //s_kt_matrix_t matrix = &(obj_shape.matrix); //! ie, a shallow copy.
      ppm_image_t *o_image = exportTo_image(obj_shape.matrix.matrix, obj_shape.matrix.nrows, obj_shape.matrix.ncols, NULL, NULL);
      ppm_image_write(result_file, o_image);
      //! De-allcote:
      ppm_image_finalize(o_image);
      //! De-allocate:
      // free__s_hp_image_segment_t(&obj_image);
    }
    //!
    //! De-alloctate:
    free__s_hp_clusterShapes_t(&obj_shape);
  }
  //! ------------------------------------------------------
  //! Step: Import from PPM:
  const char *input_file = result_file;
  //const char *input_file = "tmp-blur.ppm";
  s_hp_image_segment_t o_img = ppm_import__s_hp_image_segment_t(input_file);
  //! ------------------------------------------------------
  { //! Case: Apply Blurring: explore all possible combinations.
    //if(false) // FIXME: remove
    { //! Case: combien basic blur-algortihm with a radius:
      for(uint enum_ind = 0; enum_ind < e__blur_undef; enum_ind++) {
	const e__blur enum_id = (e__blur)enum_ind; //! en enum defined in: "kt_aux_matrix_blur.h"
	for(uint radius_ind = 1; radius_ind < 4;  radius_ind++) {
	  const uint radius = radius_ind; //(uint)pow(3, radius_ind); //! ie, 3^1=3, 3^2 = 9, ....
	  //!
	  //! Ops: blur:
	  s_hp_image_segment_t o_blur = apply_blur__s_hp_image_segment_t(&o_img, enum_id, /*blur-radius=*/(t_float)radius);
	  //!
	  //! Export:
	  const uint tmp_cmd_size = strlen(input_file)*3 + 200;
	  const char empty_c = '\0';
	  char *new_file  = allocate_1d_list_char(tmp_cmd_size, empty_c);
	  sprintf(new_file, "%s-blur-%s-radius-%u.ppm", input_file, getStr_e__blur(enum_id), radius);
	  //! The call:
	  export__s_hp_image_segment_t(&o_blur, new_file);
	  
	  free_1d_list_char(&new_file);      
	  //
	  //     
	  //! De-allcoate:      
	  free__s_hp_image_segment_t(&o_blur);
	}
      }
    }
    //! ------------------------------------------------------
    //if(false) // FIXME: remove
    { //! Case: use a nroamsiaotn-metric:
      for(uint enum_ind = 0; enum_ind < e_kt_normType_undef; enum_ind++) {
	//! Note: for this test-data, the 'blur-norm-relative' works fine.
	const e_kt_normType_t enum_id = (e_kt_normType_t)enum_ind; //! en enum defined in: "kt_aux_matrix_norm.h"
	{
	  //!
	  //! Ops: blur:
	  s_hp_image_segment_t o_blur = apply_blur__s_hp_image_segment_t(&o_img, enum_id); 	  //! The call:
	  //!
	  //! Export:
	  const uint tmp_cmd_size = strlen(input_file)*3 + 200;
	  const char empty_c = '\0';
	  char *new_file  = allocate_1d_list_char(tmp_cmd_size, empty_c);
	  sprintf(new_file, "%s-blurNorm-%s.ppm", input_file, get_str__humanReadable__e_kt_normType_t(enum_id));
	  //!
	  export__s_hp_image_segment_t(&o_blur, new_file);	  
	  //! De-allcoate:      
	  free_1d_list_char(&new_file);      
	  //
	  //     
	  //! De-allcoate:      
	  free__s_hp_image_segment_t(&o_blur);
	}
      }
    }
  }
  //! ------------------------------------------------------
  { //! Case: Fetch scalar preorptes fromthe RGB, eg, HUE:
    {
      for(uint enum_ind = 0; enum_ind < e_hp_image_segment_undef; enum_ind++) {
	const e_hp_image_segment_t enum_id = (e_hp_image_segment_t)enum_ind; //! en enum defined in: "hp_image_segment.h"
	for(uint radius_ind = 1; radius_ind < 4;  radius_ind++) {
	  const uint radius = radius_ind; //(uint)pow(3, radius_ind); //! ie, 3^1=3, 3^2 = 9, ....
	  //!
	  //!
	  s_kt_matrix_base_t mat_res = fetch_features__s_hp_image_segment_t(&o_img, enum_id, radius); //, scaling);
	  //!
	  //! Export:
	  const uint tmp_cmd_size = strlen(input_file)*3 + 200;
	  const char empty_c = '\0';
	  char *new_file  = allocate_1d_list_char(tmp_cmd_size, empty_c);
	  sprintf(new_file, "%s-segment-%s.ppm", input_file, getStr_e_hp_image_segment(enum_id));
	  //!
	  ppm_image_t *o_image = exportTo_image(mat_res.matrix, mat_res.nrows, mat_res.ncols, NULL, NULL);
	  ppm_image_write(result_file, o_image);
	  //! De-allcote:
	  ppm_image_finalize(o_image);
	  //! De-allcoate:      
	  free_1d_list_char(&new_file);
	  free__s_kt_matrix_base_t(&mat_res);
	}
      }
    }
  }
  //! ------------------------------------------------------
  //if(false) // FIXME: remove
  { //! Case: Effects of 'binning':
    {
      for(uint enum_ind = 0; enum_ind < e_kt_histogram_scoreType_undef; enum_ind++) {
	const e_kt_histogram_scoreType_t enum_id = (e_kt_histogram_scoreType_t)enum_ind; //! en enum defined in: "kt_list_2d.h"
	for(uint radius_ind = 2; radius_ind < 5;  radius_ind++) {
	  const uint radius = radius_ind; //(uint)pow(3, radius_ind); //! ie, 3^1=3, 3^2 = 9, ....
	  //!
	  for(uint isTo_forMinMax_useGlobal = 0; isTo_forMinMax_useGlobal < 2; isTo_forMinMax_useGlobal++) {
	    //!
	    s_hp_image_segment_t o_blur = apply_binning__s_hp_image_segment_t(&o_img, /*cnt_bins=*/radius, (bool)isTo_forMinMax_useGlobal, enum_id);
	    //!
	    //! Export:
	    const uint tmp_cmd_size = strlen(input_file)*3 + 200;
	    const char empty_c = '\0';
	    char *new_file  = allocate_1d_list_char(tmp_cmd_size, empty_c);
	    if(isTo_forMinMax_useGlobal == 1) {
	      sprintf(new_file, "%s-histogram-%s-radius-%u-adjustLocally.ppm", input_file, getStr_e_kt_histogram_scoreType_t(enum_id), radius);
	    } else {
	      sprintf(new_file, "%s-histogram-%s-radius-%u.ppm", input_file, getStr_e_kt_histogram_scoreType_t(enum_id), radius);
	    }
	    //! The call:
	    export__s_hp_image_segment_t(&o_blur, new_file);
	    //! De-allcoate:      
	    free_1d_list_char(&new_file);      
	    //
	    //     
	    //! De-allcoate:      
	    free__s_hp_image_segment_t(&o_blur);
	  }
	}
      }
    }
  }      
  //! ------------------------------------------------------
  //! ------------------------------------------------------
  //! ------------------------------------------------------
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif  
}
